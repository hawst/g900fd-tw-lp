.class Lcom/samsung/musicplus/player/FullPlayerView$9;
.super Ljava/lang/Object;
.source "FullPlayerView.java"

# interfaces
.implements Lcom/samsung/musicplus/bitmapcache/BitmapCache$Publisher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/player/FullPlayerView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/player/FullPlayerView;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/player/FullPlayerView;)V
    .locals 0

    .prologue
    .line 780
    iput-object p1, p0, Lcom/samsung/musicplus/player/FullPlayerView$9;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailPublishImage(Landroid/net/Uri;J)V
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "elapsedTime"    # J

    .prologue
    .line 800
    new-instance v0, Lcom/samsung/musicplus/player/FullPlayerView$9$2;

    invoke-direct {v0, p0, p1}, Lcom/samsung/musicplus/player/FullPlayerView$9$2;-><init>(Lcom/samsung/musicplus/player/FullPlayerView$9;Landroid/net/Uri;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/player/FullPlayerView$9$2;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 814
    return-void
.end method

.method public onPublishImage(Landroid/net/Uri;Landroid/graphics/Bitmap;J)V
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;
    .param p3, "elapsedTime"    # J

    .prologue
    .line 784
    new-instance v0, Lcom/samsung/musicplus/player/FullPlayerView$9$1;

    invoke-direct {v0, p0, p1, p2}, Lcom/samsung/musicplus/player/FullPlayerView$9$1;-><init>(Lcom/samsung/musicplus/player/FullPlayerView$9;Landroid/net/Uri;Landroid/graphics/Bitmap;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/player/FullPlayerView$9$1;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 796
    return-void
.end method
