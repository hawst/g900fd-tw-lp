.class Lcom/samsung/musicplus/player/FullPlayerView$15;
.super Ljava/lang/Object;
.source "FullPlayerView.java"

# interfaces
.implements Lcom/samsung/musicplus/bitmapcache/BitmapCache$Publisher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/player/FullPlayerView;->updateButtonThumbnailImage(Landroid/net/Uri;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/player/FullPlayerView;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/player/FullPlayerView;)V
    .locals 0

    .prologue
    .line 1605
    iput-object p1, p0, Lcom/samsung/musicplus/player/FullPlayerView$15;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailPublishImage(Landroid/net/Uri;J)V
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "elapsedTime"    # J

    .prologue
    .line 1615
    const/4 v0, 0x0

    .line 1616
    .local v0, "b":Landroid/graphics/Bitmap;
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView$15;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    invoke-virtual {v1, v0}, Lcom/samsung/musicplus/player/FullPlayerView;->updateButtonThumbnailImage(Landroid/graphics/Bitmap;)V

    .line 1617
    return-void
.end method

.method public onPublishImage(Landroid/net/Uri;Landroid/graphics/Bitmap;J)V
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;
    .param p3, "elapsedTime"    # J

    .prologue
    .line 1610
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView$15;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    invoke-virtual {v0, p2}, Lcom/samsung/musicplus/player/FullPlayerView;->updateButtonThumbnailImage(Landroid/graphics/Bitmap;)V

    .line 1611
    return-void
.end method
