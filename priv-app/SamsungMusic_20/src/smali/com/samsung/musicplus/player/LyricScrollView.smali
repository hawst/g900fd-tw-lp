.class public Lcom/samsung/musicplus/player/LyricScrollView;
.super Landroid/widget/LinearLayout;
.source "LyricScrollView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/player/LyricScrollView$OnLyricTouchListener;
    }
.end annotation


# instance fields
.field private mTouchListener:Lcom/samsung/musicplus/player/LyricScrollView$OnLyricTouchListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 43
    return-void
.end method


# virtual methods
.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 61
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 53
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public setTabListener(Lcom/samsung/musicplus/player/LyricScrollView$OnLyricTouchListener;)V
    .locals 0
    .param p1, "l"    # Lcom/samsung/musicplus/player/LyricScrollView$OnLyricTouchListener;

    .prologue
    .line 46
    iput-object p1, p0, Lcom/samsung/musicplus/player/LyricScrollView;->mTouchListener:Lcom/samsung/musicplus/player/LyricScrollView$OnLyricTouchListener;

    .line 47
    return-void
.end method
