.class Lcom/samsung/musicplus/player/PlayerView$4;
.super Ljava/lang/Object;
.source "PlayerView.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/player/PlayerView;->startAnimation(Landroid/view/animation/Animation;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/player/PlayerView;

.field final synthetic val$visible:Z


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/player/PlayerView;Z)V
    .locals 0

    .prologue
    .line 561
    iput-object p1, p0, Lcom/samsung/musicplus/player/PlayerView$4;->this$0:Lcom/samsung/musicplus/player/PlayerView;

    iput-boolean p2, p0, Lcom/samsung/musicplus/player/PlayerView$4;->val$visible:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 573
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView$4;->this$0:Lcom/samsung/musicplus/player/PlayerView;

    # getter for: Lcom/samsung/musicplus/player/PlayerView;->mListContainer:Landroid/view/View;
    invoke-static {v0}, Lcom/samsung/musicplus/player/PlayerView;->access$700(Lcom/samsung/musicplus/player/PlayerView;)Landroid/view/View;

    move-result-object v1

    iget-boolean v0, p0, Lcom/samsung/musicplus/player/PlayerView$4;->val$visible:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 574
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView$4;->this$0:Lcom/samsung/musicplus/player/PlayerView;

    invoke-virtual {v0}, Lcom/samsung/musicplus/player/PlayerView;->setVisibleIfLyricOn()V

    .line 576
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView$4;->this$0:Lcom/samsung/musicplus/player/PlayerView;

    # getter for: Lcom/samsung/musicplus/player/PlayerView;->mNowPlayingListDownOut:Landroid/view/animation/Animation;
    invoke-static {v0}, Lcom/samsung/musicplus/player/PlayerView;->access$800(Lcom/samsung/musicplus/player/PlayerView;)Landroid/view/animation/Animation;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 577
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView$4;->this$0:Lcom/samsung/musicplus/player/PlayerView;

    invoke-virtual {v0}, Lcom/samsung/musicplus/player/PlayerView;->requestShowLyricView()V

    .line 579
    :cond_0
    return-void

    .line 573
    :cond_1
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 569
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 565
    return-void
.end method
