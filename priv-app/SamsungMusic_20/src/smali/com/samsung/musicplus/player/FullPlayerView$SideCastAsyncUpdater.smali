.class public Lcom/samsung/musicplus/player/FullPlayerView$SideCastAsyncUpdater;
.super Lcom/samsung/musicplus/widget/content/ContentAsyncQueryHandler;
.source "FullPlayerView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/player/FullPlayerView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SideCastAsyncUpdater"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/player/FullPlayerView;


# direct methods
.method public constructor <init>(Lcom/samsung/musicplus/player/FullPlayerView;Landroid/content/ContentResolver;)V
    .locals 6
    .param p2, "res"    # Landroid/content/ContentResolver;

    .prologue
    const/4 v4, 0x0

    .line 2469
    iput-object p1, p0, Lcom/samsung/musicplus/player/FullPlayerView$SideCastAsyncUpdater;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    .line 2470
    sget-object v2, Lcom/samsung/musicplus/provider/MusicContents;->MUSIC_CONTENTS_URI:Landroid/net/Uri;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->PLAYER_SIDE_CAST_COLUMNS:[Ljava/lang/String;
    invoke-static {p1}, Lcom/samsung/musicplus/player/FullPlayerView;->access$4600(Lcom/samsung/musicplus/player/FullPlayerView;)[Ljava/lang/String;

    move-result-object v3

    move-object v0, p0

    move-object v1, p2

    move-object v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/samsung/musicplus/widget/content/ContentAsyncQueryHandler;-><init>(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    .line 2471
    return-void
.end method

.method private compareSameSongList(Landroid/database/Cursor;Landroid/database/Cursor;)Z
    .locals 10
    .param p1, "newCursor"    # Landroid/database/Cursor;
    .param p2, "oldCursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 2513
    if-nez p1, :cond_1

    if-nez p2, :cond_1

    .line 2544
    :cond_0
    :goto_0
    return v4

    .line 2517
    :cond_1
    if-nez p1, :cond_6

    move v0, v5

    .line 2518
    .local v0, "newCount":I
    :goto_1
    if-nez p2, :cond_7

    move v2, v5

    .line 2520
    .local v2, "oldCount":I
    :goto_2
    if-nez v0, :cond_2

    if-eqz v2, :cond_0

    .line 2526
    :cond_2
    if-nez p1, :cond_3

    if-nez p2, :cond_5

    :cond_3
    if-eqz p1, :cond_4

    if-eqz p2, :cond_5

    :cond_4
    if-eq v0, v2, :cond_8

    :cond_5
    move v4, v5

    .line 2528
    goto :goto_0

    .line 2517
    .end local v0    # "newCount":I
    .end local v2    # "oldCount":I
    :cond_6
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    goto :goto_1

    .line 2518
    .restart local v0    # "newCount":I
    :cond_7
    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v2

    goto :goto_2

    .line 2531
    .restart local v2    # "oldCount":I
    :cond_8
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 2532
    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    .line 2533
    invoke-direct {p0, p2}, Lcom/samsung/musicplus/player/FullPlayerView$SideCastAsyncUpdater;->getIdIndex(Landroid/database/Cursor;)I

    move-result v3

    .line 2534
    .local v3, "oldIndex":I
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/player/FullPlayerView$SideCastAsyncUpdater;->getIdIndex(Landroid/database/Cursor;)I

    move-result v1

    .line 2538
    .local v1, "newIndex":I
    :cond_9
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-interface {p2, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    cmp-long v6, v6, v8

    if-eqz v6, :cond_a

    move v4, v5

    .line 2539
    goto :goto_0

    .line 2541
    :cond_a
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    .line 2542
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v6

    if-nez v6, :cond_9

    goto :goto_0
.end method

.method private getIdIndex(Landroid/database/Cursor;)I
    .locals 2
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 2506
    :try_start_0
    const-string v1, "audio_id"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 2508
    :goto_0
    return v1

    .line 2507
    :catch_0
    move-exception v0

    .line 2508
    .local v0, "ex":Ljava/lang/IllegalArgumentException;
    const-string v1, "_id"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    goto :goto_0
.end method


# virtual methods
.method protected onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 6
    .param p1, "token"    # I
    .param p2, "cookie"    # Ljava/lang/Object;
    .param p3, "data"    # Landroid/database/Cursor;

    .prologue
    .line 2475
    const-string v1, "MusicSideCast"

    const-string v2, "onQueryComplete() start"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2478
    const/4 v0, 0x0

    .line 2480
    .local v0, "oldCursor":Landroid/database/Cursor;
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView$SideCastAsyncUpdater;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mRecommendedMusicAdapter:Lcom/samsung/musicplus/player/GLRecommendedMusicAdapter;
    invoke-static {v1}, Lcom/samsung/musicplus/player/FullPlayerView;->access$4500(Lcom/samsung/musicplus/player/FullPlayerView;)Lcom/samsung/musicplus/player/GLRecommendedMusicAdapter;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 2481
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView$SideCastAsyncUpdater;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mRecommendedMusicAdapter:Lcom/samsung/musicplus/player/GLRecommendedMusicAdapter;
    invoke-static {v1}, Lcom/samsung/musicplus/player/FullPlayerView;->access$4500(Lcom/samsung/musicplus/player/FullPlayerView;)Lcom/samsung/musicplus/player/GLRecommendedMusicAdapter;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/musicplus/player/GLRecommendedMusicAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 2483
    :cond_0
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView$SideCastAsyncUpdater;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    new-instance v2, Lcom/samsung/musicplus/player/GLRecommendedMusicAdapter;

    iget-object v3, p0, Lcom/samsung/musicplus/player/FullPlayerView$SideCastAsyncUpdater;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    iget-object v3, v3, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const v4, 0x7f0c0041

    const v5, 0x7f0c0042

    invoke-direct {v2, v3, p3, v4, v5}, Lcom/samsung/musicplus/player/GLRecommendedMusicAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;II)V

    # setter for: Lcom/samsung/musicplus/player/FullPlayerView;->mRecommendedMusicAdapter:Lcom/samsung/musicplus/player/GLRecommendedMusicAdapter;
    invoke-static {v1, v2}, Lcom/samsung/musicplus/player/FullPlayerView;->access$4502(Lcom/samsung/musicplus/player/FullPlayerView;Lcom/samsung/musicplus/player/GLRecommendedMusicAdapter;)Lcom/samsung/musicplus/player/GLRecommendedMusicAdapter;

    .line 2487
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView$SideCastAsyncUpdater;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mGLRecommendedMusic:Lcom/samsung/musicplus/glwidget/GLGalleryView;
    invoke-static {v1}, Lcom/samsung/musicplus/player/FullPlayerView;->access$3300(Lcom/samsung/musicplus/player/FullPlayerView;)Lcom/samsung/musicplus/glwidget/GLGalleryView;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView$SideCastAsyncUpdater;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mRecommendedMusicAdapter:Lcom/samsung/musicplus/player/GLRecommendedMusicAdapter;
    invoke-static {v2}, Lcom/samsung/musicplus/player/FullPlayerView;->access$4500(Lcom/samsung/musicplus/player/FullPlayerView;)Lcom/samsung/musicplus/player/GLRecommendedMusicAdapter;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/samsung/musicplus/glwidget/GLGalleryView;->setAdapter(Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;)V

    .line 2488
    invoke-direct {p0, p3, v0}, Lcom/samsung/musicplus/player/FullPlayerView$SideCastAsyncUpdater;->compareSameSongList(Landroid/database/Cursor;Landroid/database/Cursor;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2489
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView$SideCastAsyncUpdater;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mGLRecommendedMusic:Lcom/samsung/musicplus/glwidget/GLGalleryView;
    invoke-static {v1}, Lcom/samsung/musicplus/player/FullPlayerView;->access$3300(Lcom/samsung/musicplus/player/FullPlayerView;)Lcom/samsung/musicplus/glwidget/GLGalleryView;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lcom/samsung/musicplus/glwidget/GLGalleryView;->setSelection(I)V

    .line 2491
    :cond_1
    if-eqz v0, :cond_2

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_2

    .line 2492
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 2494
    :cond_2
    const-string v1, "MusicSideCast"

    const-string v2, "onQueryComplete() finish"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2501
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView$SideCastAsyncUpdater;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # invokes: Lcom/samsung/musicplus/player/FullPlayerView;->updateTotalCount(Landroid/database/Cursor;)V
    invoke-static {v1, p3}, Lcom/samsung/musicplus/player/FullPlayerView;->access$4700(Lcom/samsung/musicplus/player/FullPlayerView;Landroid/database/Cursor;)V

    .line 2502
    return-void
.end method
