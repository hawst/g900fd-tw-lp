.class public Lcom/samsung/musicplus/player/NowPlayingCursorLoader;
.super Ljava/lang/Object;
.source "NowPlayingCursorLoader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/player/NowPlayingCursorLoader$WorkerThreadHandler;,
        Lcom/samsung/musicplus/player/NowPlayingCursorLoader$OnQueryCompleteListener;
    }
.end annotation


# instance fields
.field private mListener:Lcom/samsung/musicplus/player/NowPlayingCursorLoader$OnQueryCompleteListener;

.field private mLooper:Landroid/os/Looper;

.field final mResolver:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/content/ContentResolver;",
            ">;"
        }
    .end annotation
.end field

.field private mService:Lcom/samsung/musicplus/service/IPlayerService;

.field private mUiHandler:Landroid/os/Handler;

.field private mWorkerThreadHandler:Lcom/samsung/musicplus/player/NowPlayingCursorLoader$WorkerThreadHandler;


# direct methods
.method public constructor <init>(Landroid/content/ContentResolver;Lcom/samsung/musicplus/service/IPlayerService;Lcom/samsung/musicplus/player/NowPlayingCursorLoader$OnQueryCompleteListener;)V
    .locals 1
    .param p1, "cr"    # Landroid/content/ContentResolver;
    .param p2, "service"    # Lcom/samsung/musicplus/service/IPlayerService;
    .param p3, "listener"    # Lcom/samsung/musicplus/player/NowPlayingCursorLoader$OnQueryCompleteListener;

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    new-instance v0, Lcom/samsung/musicplus/player/NowPlayingCursorLoader$1;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/player/NowPlayingCursorLoader$1;-><init>(Lcom/samsung/musicplus/player/NowPlayingCursorLoader;)V

    iput-object v0, p0, Lcom/samsung/musicplus/player/NowPlayingCursorLoader;->mUiHandler:Landroid/os/Handler;

    .line 36
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/samsung/musicplus/player/NowPlayingCursorLoader;->mResolver:Ljava/lang/ref/WeakReference;

    .line 37
    iput-object p2, p0, Lcom/samsung/musicplus/player/NowPlayingCursorLoader;->mService:Lcom/samsung/musicplus/service/IPlayerService;

    .line 38
    iput-object p3, p0, Lcom/samsung/musicplus/player/NowPlayingCursorLoader;->mListener:Lcom/samsung/musicplus/player/NowPlayingCursorLoader$OnQueryCompleteListener;

    .line 39
    invoke-direct {p0}, Lcom/samsung/musicplus/player/NowPlayingCursorLoader;->getWorkerThreadHandler()Lcom/samsung/musicplus/player/NowPlayingCursorLoader$WorkerThreadHandler;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/player/NowPlayingCursorLoader;->mWorkerThreadHandler:Lcom/samsung/musicplus/player/NowPlayingCursorLoader$WorkerThreadHandler;

    .line 40
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/NowPlayingCursorLoader;->startRequery()V

    .line 41
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/player/NowPlayingCursorLoader;)Lcom/samsung/musicplus/service/IPlayerService;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/player/NowPlayingCursorLoader;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/samsung/musicplus/player/NowPlayingCursorLoader;->mService:Lcom/samsung/musicplus/service/IPlayerService;

    return-object v0
.end method

.method static synthetic access$002(Lcom/samsung/musicplus/player/NowPlayingCursorLoader;Lcom/samsung/musicplus/service/IPlayerService;)Lcom/samsung/musicplus/service/IPlayerService;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/player/NowPlayingCursorLoader;
    .param p1, "x1"    # Lcom/samsung/musicplus/service/IPlayerService;

    .prologue
    .line 21
    iput-object p1, p0, Lcom/samsung/musicplus/player/NowPlayingCursorLoader;->mService:Lcom/samsung/musicplus/service/IPlayerService;

    return-object p1
.end method

.method static synthetic access$100(Lcom/samsung/musicplus/player/NowPlayingCursorLoader;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/player/NowPlayingCursorLoader;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/samsung/musicplus/player/NowPlayingCursorLoader;->mUiHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/musicplus/player/NowPlayingCursorLoader;)Lcom/samsung/musicplus/player/NowPlayingCursorLoader$OnQueryCompleteListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/player/NowPlayingCursorLoader;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/samsung/musicplus/player/NowPlayingCursorLoader;->mListener:Lcom/samsung/musicplus/player/NowPlayingCursorLoader$OnQueryCompleteListener;

    return-object v0
.end method

.method private getWorkerThreadHandler()Lcom/samsung/musicplus/player/NowPlayingCursorLoader$WorkerThreadHandler;
    .locals 3

    .prologue
    .line 44
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "NowPlayingCursorWorker"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 45
    .local v0, "thread":Landroid/os/HandlerThread;
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 46
    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/musicplus/player/NowPlayingCursorLoader;->mLooper:Landroid/os/Looper;

    .line 47
    new-instance v1, Lcom/samsung/musicplus/player/NowPlayingCursorLoader$WorkerThreadHandler;

    iget-object v2, p0, Lcom/samsung/musicplus/player/NowPlayingCursorLoader;->mLooper:Landroid/os/Looper;

    invoke-direct {v1, p0, v2}, Lcom/samsung/musicplus/player/NowPlayingCursorLoader$WorkerThreadHandler;-><init>(Lcom/samsung/musicplus/player/NowPlayingCursorLoader;Landroid/os/Looper;)V

    return-object v1
.end method


# virtual methods
.method public release()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 56
    iget-object v0, p0, Lcom/samsung/musicplus/player/NowPlayingCursorLoader;->mWorkerThreadHandler:Lcom/samsung/musicplus/player/NowPlayingCursorLoader$WorkerThreadHandler;

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/player/NowPlayingCursorLoader$WorkerThreadHandler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 57
    iget-object v0, p0, Lcom/samsung/musicplus/player/NowPlayingCursorLoader;->mLooper:Landroid/os/Looper;

    if-eqz v0, :cond_0

    .line 58
    iget-object v0, p0, Lcom/samsung/musicplus/player/NowPlayingCursorLoader;->mLooper:Landroid/os/Looper;

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 59
    iput-object v1, p0, Lcom/samsung/musicplus/player/NowPlayingCursorLoader;->mLooper:Landroid/os/Looper;

    .line 61
    :cond_0
    return-void
.end method

.method public final startRequery()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 51
    iget-object v0, p0, Lcom/samsung/musicplus/player/NowPlayingCursorLoader;->mWorkerThreadHandler:Lcom/samsung/musicplus/player/NowPlayingCursorLoader$WorkerThreadHandler;

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/player/NowPlayingCursorLoader$WorkerThreadHandler;->removeMessages(I)V

    .line 52
    iget-object v0, p0, Lcom/samsung/musicplus/player/NowPlayingCursorLoader;->mWorkerThreadHandler:Lcom/samsung/musicplus/player/NowPlayingCursorLoader$WorkerThreadHandler;

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/musicplus/player/NowPlayingCursorLoader$WorkerThreadHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 53
    return-void
.end method
