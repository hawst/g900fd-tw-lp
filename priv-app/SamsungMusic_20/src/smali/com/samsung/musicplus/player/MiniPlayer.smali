.class public Lcom/samsung/musicplus/player/MiniPlayer;
.super Lcom/samsung/musicplus/player/PlayerView;
.source "MiniPlayer.java"


# instance fields
.field private mAlbumArtButton:Landroid/view/View;

.field private mBuffering:Landroid/widget/ProgressBar;

.field private mIsKeyPadShowing:Z

.field private mK2hdTag:Landroid/widget/ImageView;

.field private mPerSonalView:Landroid/widget/ImageView;

.field private mUhaTag:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "root"    # Landroid/view/View;

    .prologue
    .line 64
    invoke-direct {p0, p1, p2}, Lcom/samsung/musicplus/player/PlayerView;-><init>(Landroid/content/Context;Landroid/view/View;)V

    .line 61
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/player/MiniPlayer;->mIsKeyPadShowing:Z

    .line 65
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getListType()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/player/MiniPlayer;->updateControllerView(I)V

    .line 66
    return-void
.end method

.method private hasSong()Z
    .locals 4

    .prologue
    .line 366
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getTitle()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getListItemCount()I

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getCurrentAudioId()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isHideCondition()Z
    .locals 1

    .prologue
    .line 362
    iget-boolean v0, p0, Lcom/samsung/musicplus/player/MiniPlayer;->mIsKeyPadShowing:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/samsung/musicplus/player/MiniPlayer;->hasSong()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setVisibility()V
    .locals 4

    .prologue
    .line 319
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/MiniPlayer;->getParentView()Landroid/view/View;

    move-result-object v0

    .line 320
    .local v0, "root":Landroid/view/View;
    invoke-direct {p0}, Lcom/samsung/musicplus/player/MiniPlayer;->isHideCondition()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 321
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 323
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/MiniPlayer;->onStop()V

    .line 337
    :goto_0
    return-void

    .line 325
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 327
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/MiniPlayer;->onStart()V

    .line 328
    invoke-direct {p0}, Lcom/samsung/musicplus/player/MiniPlayer;->updateBuffering()V

    .line 329
    new-instance v1, Lcom/samsung/musicplus/player/MiniPlayer$1;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/player/MiniPlayer$1;-><init>(Lcom/samsung/musicplus/player/MiniPlayer;)V

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method private updateBuffering()V
    .locals 4

    .prologue
    .line 172
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->isPreparing()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getDuration()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    .line 173
    iget-object v0, p0, Lcom/samsung/musicplus/player/MiniPlayer;->mBuffering:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 177
    :goto_0
    return-void

    .line 175
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/player/MiniPlayer;->mBuffering:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0
.end method

.method private updateK2hdTagView()V
    .locals 2

    .prologue
    .line 226
    iget-object v0, p0, Lcom/samsung/musicplus/player/MiniPlayer;->mK2hdTag:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    .line 227
    const-string v0, "MiniPlayer"

    const-string v1, "updateK2hdTagView: mK2hdTag is null"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 239
    :goto_0
    return-void

    .line 231
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/player/MiniPlayer;->mUhaTag:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/musicplus/player/MiniPlayer;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/musicplus/util/UiUtils;->isK2HD(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 232
    iget-object v0, p0, Lcom/samsung/musicplus/player/MiniPlayer;->mK2hdTag:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 236
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/player/MiniPlayer;->mK2hdTag:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private updateMiniPlayerNowPlayingInfo(Landroid/content/Context;Lcom/samsung/musicplus/player/MiniPlayer;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "views"    # Lcom/samsung/musicplus/player/MiniPlayer;

    .prologue
    .line 244
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getTitle()Ljava/lang/String;

    move-result-object v1

    .line 245
    .local v1, "title":Ljava/lang/String;
    invoke-virtual {p2}, Lcom/samsung/musicplus/player/MiniPlayer;->getTitleTextView()Landroid/widget/TextView;

    move-result-object v2

    .line 246
    .local v2, "titleView":Landroid/widget/TextView;
    if-eqz v2, :cond_0

    .line 252
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "<b>"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "</b>"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    .line 253
    .local v0, "prefix":Landroid/text/Spanned;
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b003f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 255
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getArtist()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 257
    .end local v0    # "prefix":Landroid/text/Spanned;
    :cond_0
    return-void
.end method

.method private updatePersonalModeView()V
    .locals 4

    .prologue
    .line 277
    sget-boolean v2, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_SUPPORT_SECRET_BOX:Z

    if-nez v2, :cond_1

    .line 301
    :cond_0
    :goto_0
    return-void

    .line 281
    :cond_1
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getCurrentPath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/musicplus/util/UiUtils;->isLocalContents(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 282
    iget-object v2, p0, Lcom/samsung/musicplus/player/MiniPlayer;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/samsung/musicplus/library/storage/PrivateMode;->isPrivateDirMounted(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 283
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getPersonalMode()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_3

    .line 284
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/MiniPlayer;->getParentView()Landroid/view/View;

    move-result-object v0

    .line 285
    .local v0, "parent":Landroid/view/View;
    const v2, 0x7f0d0058

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 286
    .local v1, "personalStub":Landroid/view/View;
    instance-of v2, v1, Landroid/view/ViewStub;

    if-eqz v2, :cond_2

    .line 287
    check-cast v1, Landroid/view/ViewStub;

    .end local v1    # "personalStub":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 288
    const v2, 0x7f0d0063

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/samsung/musicplus/player/MiniPlayer;->mPerSonalView:Landroid/widget/ImageView;

    .line 290
    :cond_2
    iget-object v2, p0, Lcom/samsung/musicplus/player/MiniPlayer;->mPerSonalView:Landroid/widget/ImageView;

    if-eqz v2, :cond_0

    .line 291
    iget-object v2, p0, Lcom/samsung/musicplus/player/MiniPlayer;->mPerSonalView:Landroid/widget/ImageView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 298
    .end local v0    # "parent":Landroid/view/View;
    :cond_3
    iget-object v2, p0, Lcom/samsung/musicplus/player/MiniPlayer;->mPerSonalView:Landroid/widget/ImageView;

    if-eqz v2, :cond_0

    .line 299
    iget-object v2, p0, Lcom/samsung/musicplus/player/MiniPlayer;->mPerSonalView:Landroid/widget/ImageView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private updateUhqTagView()V
    .locals 5

    .prologue
    const/16 v4, 0x8

    .line 204
    iget-object v2, p0, Lcom/samsung/musicplus/player/MiniPlayer;->mUhaTag:Landroid/widget/ImageView;

    if-nez v2, :cond_0

    .line 205
    const-string v2, "MiniPlayer"

    const-string v3, "updateUhqTagView: mUhaTag is null."

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    :goto_0
    return-void

    .line 209
    :cond_0
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getCurrentPath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/musicplus/util/UiUtils;->isLocalContents(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 210
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getSamplingRate()I

    move-result v1

    .line 211
    .local v1, "samplingRate":I
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getBitDepth()I

    move-result v0

    .line 212
    .local v0, "bitDepth":I
    invoke-static {}, Lcom/samsung/musicplus/util/UiUtils;->isSupportUhqa()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {v1, v0}, Lcom/samsung/musicplus/util/UiUtils;->isUhqa(II)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 213
    iget-object v2, p0, Lcom/samsung/musicplus/player/MiniPlayer;->mUhaTag:Landroid/widget/ImageView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 216
    :cond_1
    iget-object v2, p0, Lcom/samsung/musicplus/player/MiniPlayer;->mUhaTag:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 219
    .end local v0    # "bitDepth":I
    .end local v1    # "samplingRate":I
    :cond_2
    const-string v2, "MiniPlayer"

    const-string v3, "updateUhqTagView: getCurrentPath() isn\'t local content."

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    iget-object v2, p0, Lcom/samsung/musicplus/player/MiniPlayer;->mUhaTag:Landroid/widget/ImageView;

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public adjustMiniPlayerLayout()V
    .locals 0

    .prologue
    .line 372
    invoke-super {p0}, Lcom/samsung/musicplus/player/PlayerView;->adjustMiniPlayerLayout()V

    .line 373
    return-void
.end method

.method protected getAnimatedPlayButtonView(Landroid/view/View;)Lcom/samsung/musicplus/widget/AnimatedToggleButton;
    .locals 1
    .param p1, "root"    # Landroid/view/View;

    .prologue
    .line 356
    const v0, 0x7f0d00fb

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;

    return-object v0
.end method

.method public isVisible()Z
    .locals 3

    .prologue
    .line 340
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/MiniPlayer;->getParentView()Landroid/view/View;

    move-result-object v1

    .line 341
    .local v1, "root":Landroid/view/View;
    const/4 v0, 0x0

    .line 342
    .local v0, "result":Z
    if-eqz v1, :cond_1

    .line 343
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-nez v2, :cond_0

    .line 344
    const/4 v0, 0x1

    .line 351
    :goto_0
    return v0

    .line 346
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 349
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onCreateView(Landroid/view/View;)V
    .locals 3
    .param p1, "root"    # Landroid/view/View;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/samsung/musicplus/player/MiniPlayer;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c00b9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/player/MiniPlayer;->setThumbNailSize(I)V

    .line 76
    const v0, 0x7f0d00f3

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/samsung/musicplus/player/MiniPlayer;->mBuffering:Landroid/widget/ProgressBar;

    .line 77
    const v0, 0x7f0d00fd

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/player/MiniPlayer;->mAlbumArtButton:Landroid/view/View;

    .line 78
    const v0, 0x7f0d005a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/samsung/musicplus/player/MiniPlayer;->mUhaTag:Landroid/widget/ImageView;

    .line 79
    const v0, 0x7f0d005b

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/samsung/musicplus/player/MiniPlayer;->mK2hdTag:Landroid/widget/ImageView;

    .line 81
    invoke-super {p0, p1}, Lcom/samsung/musicplus/player/PlayerView;->onCreateView(Landroid/view/View;)V

    .line 83
    const v0, 0x7f020055

    const v1, 0x7f020054

    invoke-virtual {p0, v0, v1}, Lcom/samsung/musicplus/player/MiniPlayer;->setShuffleResources(II)V

    .line 85
    const v0, 0x7f020056

    const v1, 0x7f020058

    const v2, 0x7f020057

    invoke-virtual {p0, v0, v1, v2}, Lcom/samsung/musicplus/player/MiniPlayer;->setRepeatResources(III)V

    .line 88
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 158
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/MiniPlayer;->getAlbumArtView()Landroid/widget/ImageView;

    move-result-object v0

    .line 159
    .local v0, "albumArt":Landroid/widget/ImageView;
    if-eqz v0, :cond_0

    .line 160
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 162
    :cond_0
    invoke-super {p0}, Lcom/samsung/musicplus/player/PlayerView;->onDestroy()V

    .line 163
    return-void
.end method

.method public onStart()V
    .locals 0

    .prologue
    .line 151
    invoke-super {p0}, Lcom/samsung/musicplus/player/PlayerView;->onStart()V

    .line 152
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/MiniPlayer;->updateRepeatIcon()V

    .line 153
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/MiniPlayer;->updateShuffleIcon()V

    .line 154
    return-void
.end method

.method public setAlbumArtClickListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1, "l"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 166
    iget-object v0, p0, Lcom/samsung/musicplus/player/MiniPlayer;->mAlbumArtButton:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 167
    iget-object v0, p0, Lcom/samsung/musicplus/player/MiniPlayer;->mAlbumArtButton:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 169
    :cond_0
    return-void
.end method

.method protected setBackground(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 311
    return-void
.end method

.method protected setHoverFocusImage(Landroid/view/View;I)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # I

    .prologue
    const/16 v2, 0xa

    const/16 v1, 0x9

    .line 121
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 147
    .end local p1    # "v":Landroid/view/View;
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 123
    .restart local p1    # "v":Landroid/view/View;
    :pswitch_1
    if-ne p2, v1, :cond_1

    move-object v0, p1

    .line 124
    check-cast v0, Landroid/widget/ImageView;

    const v1, 0x7f020085

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 126
    sget-boolean v0, Lcom/samsung/musicplus/util/Version;->LIBRARY_API_1:Z

    if-eqz v0, :cond_0

    .line 127
    iget-object v0, p0, Lcom/samsung/musicplus/player/MiniPlayer;->mContext:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/samsung/musicplus/library/hardware/AirView;->performHapticFeedback(Landroid/content/Context;Landroid/view/View;)V

    goto :goto_0

    .line 129
    :cond_1
    if-ne p2, v2, :cond_0

    .line 130
    check-cast p1, Landroid/widget/ImageView;

    .end local p1    # "v":Landroid/view/View;
    const v0, 0x7f020052

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 134
    .restart local p1    # "v":Landroid/view/View;
    :pswitch_2
    if-ne p2, v1, :cond_2

    move-object v0, p1

    .line 135
    check-cast v0, Landroid/widget/ImageView;

    const v1, 0x7f020084

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 137
    sget-boolean v0, Lcom/samsung/musicplus/util/Version;->LIBRARY_API_1:Z

    if-eqz v0, :cond_0

    .line 138
    iget-object v0, p0, Lcom/samsung/musicplus/player/MiniPlayer;->mContext:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/samsung/musicplus/library/hardware/AirView;->performHapticFeedback(Landroid/content/Context;Landroid/view/View;)V

    goto :goto_0

    .line 140
    :cond_2
    if-ne p2, v2, :cond_0

    .line 141
    check-cast p1, Landroid/widget/ImageView;

    .end local p1    # "v":Landroid/view/View;
    const v0, 0x7f02004e

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 121
    :pswitch_data_0
    .packed-switch 0x7f0d00f9
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected setPlayButtonPausedState(Landroid/widget/ImageView;)V
    .locals 2
    .param p1, "playBtn"    # Landroid/widget/ImageView;

    .prologue
    .line 96
    const v0, 0x7f020051

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 97
    iget-object v0, p0, Lcom/samsung/musicplus/player/MiniPlayer;->mContext:Landroid/content/Context;

    const v1, 0x7f100194

    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 99
    return-void
.end method

.method protected setPlayButtonPlayingState(Landroid/widget/ImageView;)V
    .locals 2
    .param p1, "playBtn"    # Landroid/widget/ImageView;

    .prologue
    .line 107
    const v0, 0x7f020050

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 108
    iget-object v0, p0, Lcom/samsung/musicplus/player/MiniPlayer;->mContext:Landroid/content/Context;

    const v1, 0x7f100193

    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 110
    return-void
.end method

.method public setSoftKeyVisibility(Z)V
    .locals 0
    .param p1, "isShowing"    # Z

    .prologue
    .line 314
    iput-boolean p1, p0, Lcom/samsung/musicplus/player/MiniPlayer;->mIsKeyPadShowing:Z

    .line 315
    invoke-direct {p0}, Lcom/samsung/musicplus/player/MiniPlayer;->setVisibility()V

    .line 316
    return-void
.end method

.method public setTitleScroll(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 306
    return-void
.end method

.method protected updateHoverView()V
    .locals 1

    .prologue
    .line 267
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/MiniPlayer;->getTitleTextView()Landroid/widget/TextView;

    move-result-object v0

    .line 268
    .local v0, "title":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 271
    :cond_0
    invoke-super {p0}, Lcom/samsung/musicplus/player/PlayerView;->updateHoverView()V

    .line 272
    return-void
.end method

.method public updatePlayState()V
    .locals 1

    .prologue
    .line 188
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->isPlaying()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/player/MiniPlayer;->updatePlayState(Z)V

    .line 189
    return-void
.end method

.method public updateSongInfo()V
    .locals 1

    .prologue
    .line 195
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/player/MiniPlayer;->initializeTitleView(Z)V

    .line 196
    invoke-direct {p0}, Lcom/samsung/musicplus/player/MiniPlayer;->setVisibility()V

    .line 197
    iget-object v0, p0, Lcom/samsung/musicplus/player/MiniPlayer;->mContext:Landroid/content/Context;

    invoke-direct {p0, v0, p0}, Lcom/samsung/musicplus/player/MiniPlayer;->updateMiniPlayerNowPlayingInfo(Landroid/content/Context;Lcom/samsung/musicplus/player/MiniPlayer;)V

    .line 198
    invoke-direct {p0}, Lcom/samsung/musicplus/player/MiniPlayer;->updateUhqTagView()V

    .line 199
    invoke-direct {p0}, Lcom/samsung/musicplus/player/MiniPlayer;->updateK2hdTagView()V

    .line 200
    return-void
.end method

.method public updateTotalTime()Z
    .locals 1

    .prologue
    .line 261
    invoke-direct {p0}, Lcom/samsung/musicplus/player/MiniPlayer;->updateBuffering()V

    .line 262
    invoke-super {p0}, Lcom/samsung/musicplus/player/PlayerView;->updateTotalTime()Z

    move-result v0

    return v0
.end method

.method public updateTrackAllInfo()V
    .locals 0

    .prologue
    .line 181
    invoke-direct {p0}, Lcom/samsung/musicplus/player/MiniPlayer;->updateBuffering()V

    .line 182
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/MiniPlayer;->updatePlayState()V

    .line 183
    invoke-direct {p0}, Lcom/samsung/musicplus/player/MiniPlayer;->updatePersonalModeView()V

    .line 184
    invoke-super {p0}, Lcom/samsung/musicplus/player/PlayerView;->updateTrackAllInfo()V

    .line 185
    return-void
.end method
