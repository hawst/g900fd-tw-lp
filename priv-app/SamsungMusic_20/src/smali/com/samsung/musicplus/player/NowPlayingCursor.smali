.class public Lcom/samsung/musicplus/player/NowPlayingCursor;
.super Landroid/database/AbstractCursor;
.source "NowPlayingCursor.java"


# static fields
.field private static final NOW_PLAYING_COLUMNS:[Ljava/lang/String;

.field private static final NOW_PLAYING_SIMPLE_COLUMNS:[Ljava/lang/String;


# instance fields
.field private mCurPos:I

.field private mCurrentPlaylistCursor:Landroid/database/Cursor;

.field private mCursorIdxs:[J

.field private mNowPlaying:[J

.field private mResolver:Landroid/content/ContentResolver;

.field private mService:Lcom/samsung/musicplus/service/IPlayerService;

.field private mSize:I

.field private mUri:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 65
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v2

    const-string v1, "title"

    aput-object v1, v0, v3

    const-string v1, "artist"

    aput-object v1, v0, v4

    const-string v1, "duration"

    aput-object v1, v0, v5

    const-string v1, "album_id"

    aput-object v1, v0, v6

    sput-object v0, Lcom/samsung/musicplus/player/NowPlayingCursor;->NOW_PLAYING_SIMPLE_COLUMNS:[Ljava/lang/String;

    .line 76
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v2

    const-string v1, "title"

    aput-object v1, v0, v3

    const-string v1, "artist"

    aput-object v1, v0, v4

    const-string v1, "duration"

    aput-object v1, v0, v5

    const-string v1, "album_id"

    aput-object v1, v0, v6

    const/4 v1, 0x5

    const-string v2, "bit_depth"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "is_secretbox"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "sampling_rate"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/musicplus/player/NowPlayingCursor;->NOW_PLAYING_COLUMNS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/ContentResolver;Landroid/net/Uri;[J)V
    .locals 1
    .param p1, "resolver"    # Landroid/content/ContentResolver;
    .param p2, "uri"    # Landroid/net/Uri;
    .param p3, "list"    # [J

    .prologue
    .line 84
    invoke-direct {p0}, Landroid/database/AbstractCursor;-><init>()V

    .line 85
    iput-object p1, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mResolver:Landroid/content/ContentResolver;

    .line 86
    iput-object p2, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mUri:Landroid/net/Uri;

    .line 88
    if-nez p3, :cond_0

    .line 89
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mSize:I

    .line 102
    :goto_0
    return-void

    .line 91
    :cond_0
    iput-object p3, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mNowPlaying:[J

    .line 92
    iget-object v0, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mNowPlaying:[J

    array-length v0, v0

    iput v0, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mSize:I

    .line 94
    iget-object v0, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mNowPlaying:[J

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/player/NowPlayingCursor;->getRealCursor([J)Landroid/database/Cursor;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mCurrentPlaylistCursor:Landroid/database/Cursor;

    .line 95
    iget-object v0, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mCurrentPlaylistCursor:Landroid/database/Cursor;

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/player/NowPlayingCursor;->getRealCursorIndex(Landroid/database/Cursor;)[J

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mCursorIdxs:[J

    .line 96
    iget-object v0, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mCursorIdxs:[J

    array-length v0, v0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/samsung/musicplus/service/IPlayerService;Landroid/content/ContentResolver;)V
    .locals 4
    .param p1, "service"    # Lcom/samsung/musicplus/service/IPlayerService;
    .param p2, "resolver"    # Landroid/content/ContentResolver;

    .prologue
    .line 106
    invoke-direct {p0}, Landroid/database/AbstractCursor;-><init>()V

    .line 108
    if-nez p1, :cond_0

    .line 109
    const-string v0, "NowPlayingCursor"

    const-string v1, "NowPlayingCursor : service is null."

    new-instance v2, Ljava/lang/Throwable;

    const-string v3, "NowPlayingCursor"

    invoke-direct {v2, v3}, Ljava/lang/Throwable;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v1, v2}, Lcom/samsung/musicplus/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 112
    :cond_0
    iput-object p1, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mService:Lcom/samsung/musicplus/service/IPlayerService;

    .line 113
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getCurrentBaseUri()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mUri:Landroid/net/Uri;

    .line 114
    iput-object p2, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mResolver:Landroid/content/ContentResolver;

    .line 115
    invoke-direct {p0}, Lcom/samsung/musicplus/player/NowPlayingCursor;->makeNowPlayingCursor2()V

    .line 117
    return-void
.end method

.method private dump()V
    .locals 4

    .prologue
    .line 345
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 346
    .local v1, "where":Ljava/lang/StringBuilder;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mSize:I

    if-ge v0, v2, :cond_1

    .line 347
    iget-object v2, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mNowPlaying:[J

    aget-wide v2, v2, v0

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 348
    iget v2, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mSize:I

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_0

    .line 349
    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 346
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 352
    :cond_1
    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 353
    const-string v2, "NowPlayingCursor: "

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 354
    return-void
.end method

.method public static getNowPlayingArgs()Lcom/samsung/musicplus/util/ListUtils$QueryArgs;
    .locals 3

    .prologue
    .line 130
    new-instance v0, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    invoke-direct {v0}, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;-><init>()V

    .line 132
    .local v0, "args":Lcom/samsung/musicplus/util/ListUtils$QueryArgs;
    sget-object v1, Lcom/samsung/musicplus/player/NowPlayingCursor;->NOW_PLAYING_COLUMNS:[Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    iput-object v1, v0, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->audioIdCol:Ljava/lang/String;

    .line 133
    sget-object v1, Lcom/samsung/musicplus/player/NowPlayingCursor;->NOW_PLAYING_COLUMNS:[Ljava/lang/String;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    iput-object v1, v0, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->text1Col:Ljava/lang/String;

    .line 134
    sget-object v1, Lcom/samsung/musicplus/player/NowPlayingCursor;->NOW_PLAYING_COLUMNS:[Ljava/lang/String;

    const/4 v2, 0x2

    aget-object v1, v1, v2

    iput-object v1, v0, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->text2Col:Ljava/lang/String;

    .line 135
    sget-object v1, Lcom/samsung/musicplus/player/NowPlayingCursor;->NOW_PLAYING_COLUMNS:[Ljava/lang/String;

    const/4 v2, 0x3

    aget-object v1, v1, v2

    iput-object v1, v0, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->durationCol:Ljava/lang/String;

    .line 138
    sget-object v1, Lcom/samsung/musicplus/player/NowPlayingCursor;->NOW_PLAYING_COLUMNS:[Ljava/lang/String;

    const/4 v2, 0x4

    aget-object v1, v1, v2

    iput-object v1, v0, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->albumIdCol:Ljava/lang/String;

    .line 139
    sget-object v1, Lcom/samsung/musicplus/player/NowPlayingCursor;->NOW_PLAYING_COLUMNS:[Ljava/lang/String;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    iput-object v1, v0, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->bitDepthCol:Ljava/lang/String;

    .line 140
    sget-object v1, Lcom/samsung/musicplus/player/NowPlayingCursor;->NOW_PLAYING_COLUMNS:[Ljava/lang/String;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    iput-object v1, v0, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->secretBoxCol:Ljava/lang/String;

    .line 141
    sget-object v1, Lcom/samsung/musicplus/player/NowPlayingCursor;->NOW_PLAYING_COLUMNS:[Ljava/lang/String;

    const/4 v2, 0x7

    aget-object v1, v1, v2

    iput-object v1, v0, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->samplingRateCol:Ljava/lang/String;

    .line 142
    return-object v0
.end method

.method private getRealCursor([J)Landroid/database/Cursor;
    .locals 10
    .param p1, "list"    # [J

    .prologue
    const/4 v4, 0x0

    .line 240
    iget-object v0, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mUri:Landroid/net/Uri;

    if-nez v0, :cond_1

    .line 242
    const-string v0, "MusicCursor"

    const-string v1, "getRealCursor() But uri is null, is service dying? or check service connection"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 270
    :cond_0
    :goto_0
    return-object v4

    .line 247
    :cond_1
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    .line 248
    .local v9, "where":Ljava/lang/StringBuilder;
    const-string v0, "_id IN ("

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 249
    array-length v8, p1

    .line 250
    .local v8, "size":I
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_1
    if-ge v7, v8, :cond_3

    .line 251
    aget-wide v0, p1, v7

    invoke-virtual {v9, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 252
    add-int/lit8 v0, v8, -0x1

    if-ge v7, v0, :cond_2

    .line 253
    const-string v0, ","

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 250
    :cond_2
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 256
    :cond_3
    const-string v0, ")"

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 262
    iget-object v0, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mResolver:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mUri:Landroid/net/Uri;

    invoke-virtual {p0}, Lcom/samsung/musicplus/player/NowPlayingCursor;->getColumnNames()[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v5, "_id"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 267
    .local v6, "c":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    move-object v4, v6

    .line 270
    goto :goto_0
.end method

.method private getRealCursorIndex(Landroid/database/Cursor;)[J
    .locals 6
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 274
    if-nez p1, :cond_0

    .line 275
    const/4 v4, 0x0

    new-array v2, v4, [J

    .line 287
    :goto_0
    return-object v2

    .line 277
    :cond_0
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v3

    .line 278
    .local v3, "size":I
    new-array v2, v3, [J

    .line 279
    .local v2, "index":[J
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 280
    const-string v4, "_id"

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    .line 282
    .local v0, "colidx":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v3, :cond_1

    .line 283
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    aput-wide v4, v2, v1

    .line 284
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    .line 282
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 286
    :cond_1
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    goto :goto_0
.end method

.method private makeNowPlayingCursor()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 166
    :try_start_0
    iget-object v4, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v4}, Lcom/samsung/musicplus/service/IPlayerService;->getQueue()[J

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mNowPlaying:[J
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 170
    :goto_0
    iget-object v4, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mNowPlaying:[J

    array-length v4, v4

    iput v4, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mSize:I

    .line 174
    iget v4, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mSize:I

    if-nez v4, :cond_1

    .line 215
    :cond_0
    :goto_1
    return-void

    .line 167
    :catch_0
    move-exception v1

    .line 168
    .local v1, "ex":Landroid/os/RemoteException;
    new-array v4, v6, [J

    iput-object v4, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mNowPlaying:[J

    goto :goto_0

    .line 178
    .end local v1    # "ex":Landroid/os/RemoteException;
    :cond_1
    iget-object v4, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mNowPlaying:[J

    invoke-direct {p0, v4}, Lcom/samsung/musicplus/player/NowPlayingCursor;->getRealCursor([J)Landroid/database/Cursor;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mCurrentPlaylistCursor:Landroid/database/Cursor;

    .line 179
    iget-object v4, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mCurrentPlaylistCursor:Landroid/database/Cursor;

    invoke-direct {p0, v4}, Lcom/samsung/musicplus/player/NowPlayingCursor;->getRealCursorIndex(Landroid/database/Cursor;)[J

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mCursorIdxs:[J

    .line 180
    iget-object v4, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mCursorIdxs:[J

    array-length v3, v4

    .line 187
    .local v3, "size":I
    if-nez v3, :cond_2

    .line 190
    :try_start_1
    iget-object v4, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mService:Lcom/samsung/musicplus/service/IPlayerService;

    const/4 v5, 0x0

    iget v6, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mSize:I

    invoke-interface {v4, v5, v6}, Lcom/samsung/musicplus/service/IPlayerService;->removeTracks(II)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 191
    :catch_1
    move-exception v0

    .line 192
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_1

    .line 195
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_2
    iget-object v4, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mNowPlaying:[J

    iget-object v5, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mCursorIdxs:[J

    invoke-direct {p0, v4, v5}, Lcom/samsung/musicplus/player/NowPlayingCursor;->removeDeletedItemsInDb([J[J)I

    move-result v2

    .line 196
    .local v2, "removednum":I
    if-lez v2, :cond_0

    .line 198
    :try_start_2
    iget-object v4, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v4}, Lcom/samsung/musicplus/service/IPlayerService;->getQueue()[J

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mNowPlaying:[J

    .line 199
    iget-object v4, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mNowPlaying:[J

    array-length v4, v4

    iput v4, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mSize:I

    .line 200
    iget v4, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mSize:I

    if-nez v4, :cond_0

    .line 201
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mCursorIdxs:[J
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_1

    .line 204
    :catch_2
    move-exception v0

    .line 205
    .restart local v0    # "e":Landroid/os/RemoteException;
    new-array v4, v6, [J

    iput-object v4, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mNowPlaying:[J

    goto :goto_1
.end method

.method private makeNowPlayingCursor2()V
    .locals 2

    .prologue
    .line 147
    const/4 v1, 0x0

    new-array v1, v1, [J

    iput-object v1, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mNowPlaying:[J

    .line 149
    :try_start_0
    iget-object v1, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mService:Lcom/samsung/musicplus/service/IPlayerService;

    if-eqz v1, :cond_0

    .line 150
    iget-object v1, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v1}, Lcom/samsung/musicplus/service/IPlayerService;->getOrganizedQueue()[J

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mNowPlaying:[J
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 155
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mNowPlaying:[J

    array-length v1, v1

    iput v1, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mSize:I

    .line 156
    iget v1, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mSize:I

    if-nez v1, :cond_1

    .line 162
    :goto_1
    return-void

    .line 152
    :catch_0
    move-exception v0

    .line 153
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 160
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_1
    iget-object v1, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mNowPlaying:[J

    invoke-direct {p0, v1}, Lcom/samsung/musicplus/player/NowPlayingCursor;->getRealCursor([J)Landroid/database/Cursor;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mCurrentPlaylistCursor:Landroid/database/Cursor;

    .line 161
    iget-object v1, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mCurrentPlaylistCursor:Landroid/database/Cursor;

    invoke-direct {p0, v1}, Lcom/samsung/musicplus/player/NowPlayingCursor;->getRealCursorIndex(Landroid/database/Cursor;)[J

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mCursorIdxs:[J

    goto :goto_1
.end method

.method private removeDeletedItemsInDb([J[J)I
    .locals 10
    .param p1, "prevList"    # [J
    .param p2, "realList"    # [J

    .prologue
    const/4 v6, 0x0

    .line 223
    const/4 v3, 0x0

    .line 224
    .local v3, "removed":I
    :try_start_0
    array-length v7, p1

    add-int/lit8 v2, v7, -0x1

    .local v2, "i":I
    :goto_0
    if-ltz v2, :cond_1

    .line 225
    aget-wide v4, p1, v2

    .line 226
    .local v4, "trackid":J
    invoke-static {p2, v4, v5}, Ljava/util/Arrays;->binarySearch([JJ)I

    move-result v0

    .line 227
    .local v0, "crsridx":I
    if-gez v0, :cond_0

    .line 228
    const-string v7, "MusicNowPlaying"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "item no longer exists in db: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/musicplus/library/iLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    iget-object v7, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v7, v4, v5}, Lcom/samsung/musicplus/service/IPlayerService;->removeTrack(J)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v7

    add-int/2addr v3, v7

    .line 224
    :cond_0
    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    .line 233
    .end local v0    # "crsridx":I
    .end local v2    # "i":I
    .end local v4    # "trackid":J
    :catch_0
    move-exception v1

    .line 234
    .local v1, "ex":Landroid/os/RemoteException;
    new-array v7, v6, [J

    iput-object v7, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mNowPlaying:[J

    move v3, v6

    .line 235
    .end local v1    # "ex":Landroid/os/RemoteException;
    .end local v3    # "removed":I
    :cond_1
    return v3
.end method


# virtual methods
.method public close()V
    .locals 1

    .prologue
    .line 449
    iget-object v0, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mCurrentPlaylistCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 450
    iget-object v0, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mCurrentPlaylistCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 452
    :cond_0
    return-void
.end method

.method public deactivate()V
    .locals 1

    .prologue
    .line 434
    iget-object v0, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mCurrentPlaylistCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 435
    iget-object v0, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mCurrentPlaylistCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->deactivate()V

    .line 437
    :cond_0
    return-void
.end method

.method public getColumnNames()[Ljava/lang/String;
    .locals 2

    .prologue
    .line 428
    sget-object v0, Lcom/samsung/musicplus/provider/MusicContents;->MUSIC_CONTENTS_URI:Landroid/net/Uri;

    iget-object v1, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/samsung/musicplus/player/NowPlayingCursor;->NOW_PLAYING_COLUMNS:[Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/samsung/musicplus/player/NowPlayingCursor;->NOW_PLAYING_SIMPLE_COLUMNS:[Ljava/lang/String;

    goto :goto_0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 292
    iget v0, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mSize:I

    return v0
.end method

.method public getDouble(I)D
    .locals 2
    .param p1, "column"    # I

    .prologue
    .line 404
    iget-object v0, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mCurrentPlaylistCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 405
    iget-object v0, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mCurrentPlaylistCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v0

    .line 407
    :goto_0
    return-wide v0

    :cond_0
    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    goto :goto_0
.end method

.method public getFloat(I)F
    .locals 1
    .param p1, "column"    # I

    .prologue
    .line 396
    iget-object v0, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mCurrentPlaylistCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 397
    iget-object v0, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mCurrentPlaylistCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getFloat(I)F

    move-result v0

    .line 399
    :goto_0
    return v0

    :cond_0
    const/high16 v0, -0x40800000    # -1.0f

    goto :goto_0
.end method

.method public getInt(I)I
    .locals 2
    .param p1, "column"    # I

    .prologue
    .line 377
    :try_start_0
    iget-object v1, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mCurrentPlaylistCursor:Landroid/database/Cursor;

    invoke-interface {v1, p1}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 380
    :goto_0
    return v1

    .line 378
    :catch_0
    move-exception v0

    .line 379
    .local v0, "ex":Ljava/lang/Exception;
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/player/NowPlayingCursor;->onChange(Z)V

    .line 380
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getLong(I)J
    .locals 4
    .param p1, "column"    # I

    .prologue
    .line 387
    :try_start_0
    iget-object v1, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mCurrentPlaylistCursor:Landroid/database/Cursor;

    invoke-interface {v1, p1}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v2

    .line 390
    :goto_0
    return-wide v2

    .line 388
    :catch_0
    move-exception v0

    .line 389
    .local v0, "ex":Ljava/lang/Exception;
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/player/NowPlayingCursor;->onChange(Z)V

    .line 390
    const-wide/16 v2, 0x0

    goto :goto_0
.end method

.method public getShort(I)S
    .locals 1
    .param p1, "column"    # I

    .prologue
    .line 368
    iget-object v0, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mCurrentPlaylistCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 369
    iget-object v0, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mCurrentPlaylistCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getShort(I)S

    move-result v0

    .line 371
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public getString(I)Ljava/lang/String;
    .locals 2
    .param p1, "column"    # I

    .prologue
    .line 359
    :try_start_0
    iget-object v1, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mCurrentPlaylistCursor:Landroid/database/Cursor;

    invoke-interface {v1, p1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 362
    :goto_0
    return-object v1

    .line 360
    :catch_0
    move-exception v0

    .line 361
    .local v0, "ex":Ljava/lang/Exception;
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/player/NowPlayingCursor;->onChange(Z)V

    .line 362
    const-string v1, ""

    goto :goto_0
.end method

.method public getType(I)I
    .locals 1
    .param p1, "column"    # I

    .prologue
    .line 412
    iget-object v0, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mCurrentPlaylistCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 413
    iget-object v0, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mCurrentPlaylistCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getType(I)I

    move-result v0

    .line 415
    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public isClosed()Z
    .locals 1

    .prologue
    .line 441
    iget-object v0, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mCurrentPlaylistCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 442
    iget-object v0, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mCurrentPlaylistCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    .line 444
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isNull(I)Z
    .locals 1
    .param p1, "column"    # I

    .prologue
    .line 420
    iget-object v0, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mCurrentPlaylistCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 421
    iget-object v0, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mCurrentPlaylistCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    .line 423
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public moveItem(II)V
    .locals 2
    .param p1, "from"    # I
    .param p2, "to"    # I

    .prologue
    .line 337
    :try_start_0
    iget-object v0, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v0, p1, p2}, Lcom/samsung/musicplus/service/IPlayerService;->moveQueueItem(II)V

    .line 338
    iget-object v0, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v0}, Lcom/samsung/musicplus/service/IPlayerService;->getQueue()[J

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mNowPlaying:[J

    .line 339
    const/4 v0, -0x1

    iget v1, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mCurPos:I

    invoke-virtual {p0, v0, v1}, Lcom/samsung/musicplus/player/NowPlayingCursor;->onMove(II)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 342
    :goto_0
    return-void

    .line 340
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public onMove(II)Z
    .locals 5
    .param p1, "oldPosition"    # I
    .param p2, "newPosition"    # I

    .prologue
    const/4 v1, 0x1

    .line 297
    if-ne p1, p2, :cond_0

    .line 315
    :goto_0
    return v1

    .line 301
    :cond_0
    iget-object v4, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mNowPlaying:[J

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mCursorIdxs:[J

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mNowPlaying:[J

    array-length v4, v4

    if-ge p2, v4, :cond_1

    iget-object v4, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mCurrentPlaylistCursor:Landroid/database/Cursor;

    if-nez v4, :cond_2

    .line 303
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 310
    :cond_2
    iget-object v4, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mNowPlaying:[J

    aget-wide v2, v4, p2

    .line 311
    .local v2, "newid":J
    iget-object v4, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mCursorIdxs:[J

    invoke-static {v4, v2, v3}, Ljava/util/Arrays;->binarySearch([JJ)I

    move-result v0

    .line 312
    .local v0, "crsridx":I
    iget-object v4, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mCurrentPlaylistCursor:Landroid/database/Cursor;

    invoke-interface {v4, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 313
    iput p2, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mCurPos:I

    goto :goto_0
.end method

.method public removeItem(I)Z
    .locals 4
    .param p1, "which"    # I

    .prologue
    .line 320
    :try_start_0
    iget-object v1, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v1, p1, p1}, Lcom/samsung/musicplus/service/IPlayerService;->removeTracks(II)I

    move-result v1

    if-nez v1, :cond_0

    .line 321
    const/4 v1, 0x0

    .line 332
    :goto_0
    return v1

    .line 323
    :cond_0
    move v0, p1

    .line 324
    .local v0, "i":I
    iget v1, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mSize:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mSize:I

    .line 325
    :goto_1
    iget v1, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mSize:I

    if-ge v0, v1, :cond_1

    .line 326
    iget-object v1, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mNowPlaying:[J

    iget-object v2, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mNowPlaying:[J

    add-int/lit8 v3, v0, 0x1

    aget-wide v2, v2, v3

    aput-wide v2, v1, v0

    .line 327
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 329
    :cond_1
    const/4 v1, -0x1

    iget v2, p0, Lcom/samsung/musicplus/player/NowPlayingCursor;->mCurPos:I

    invoke-virtual {p0, v1, v2}, Lcom/samsung/musicplus/player/NowPlayingCursor;->onMove(II)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 332
    .end local v0    # "i":I
    :goto_2
    const/4 v1, 0x1

    goto :goto_0

    .line 330
    :catch_0
    move-exception v1

    goto :goto_2
.end method

.method public requery()Z
    .locals 1

    .prologue
    .line 456
    const/4 v0, 0x1

    return v0
.end method
