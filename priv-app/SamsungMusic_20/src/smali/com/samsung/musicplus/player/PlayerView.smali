.class public Lcom/samsung/musicplus/player/PlayerView;
.super Ljava/lang/Object;
.source "PlayerView.java"

# interfaces
.implements Lcom/samsung/musicplus/library/hardware/AirView$OnAirViewPopupListener;
.implements Lcom/samsung/musicplus/settings/PlayerSettingPreference;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/player/PlayerView$HoverListener;,
        Lcom/samsung/musicplus/player/PlayerView$CustomSeekBarListener;,
        Lcom/samsung/musicplus/player/PlayerView$TextUpdateHandler;
    }
.end annotation


# static fields
.field private static final DECODE_COMPELETE:I = 0xc8

.field protected static final FLAG_GL_GALLERY:Z = true

.field protected static final MAJOR_MOVE:I = 0x64

.field private static final MAX_PROGRESS:I = 0x3e8

.field private static final REFRESH:I = 0x1

.field private static final TAG:Ljava/lang/String; = "MusicUi"


# instance fields
.field private mAlbum:Landroid/widget/TextView;

.field private mAlbumArt:Landroid/widget/ImageView;

.field private mAlbumView:Landroid/view/View;

.field protected mAlbumViewShowing:Z

.field private mArtist:Landroid/widget/TextView;

.field protected mContext:Landroid/content/Context;

.field private mCurrentListType:I

.field protected mCurrentTime:Landroid/widget/TextView;

.field private mDuration:J

.field private mFfRewTouchListener:Lcom/samsung/musicplus/widget/control/ForwardRewindListener;

.field private final mHoverListner:Lcom/samsung/musicplus/player/PlayerView$HoverListener;

.field private mIsAlbumLoaded:Z

.field private mIsPrepared:Z

.field protected mKeepLyricShowingState:Z

.field private mKeyWord:Ljava/lang/String;

.field private mList:I

.field private mListButton:Landroid/widget/ImageView;

.field private mListContainer:Landroid/view/View;

.field private mNext:Landroid/view/View;

.field private mNowPlayingListDownOut:Landroid/view/animation/Animation;

.field private mNowPlayingListUpIn:Landroid/view/animation/Animation;

.field protected mParentView:Landroid/view/View;

.field protected mPause:Z

.field private mPlay:Landroid/widget/ImageView;

.field private mPlayImageResource:I

.field private mPosOverride:J

.field private mPrev:Landroid/view/View;

.field private mRepeatButton:Landroid/widget/ImageView;

.field private mRepeatResAll:I

.field private mRepeatResOff:I

.field private mRepeatResOne:I

.field private mScrubbingBar:Lcom/samsung/musicplus/widget/progress/MusicProgressBarScrubbing;

.field private mSeekBar:Landroid/widget/SeekBar;

.field private final mSeekBarAirListener:Lcom/samsung/musicplus/library/hardware/AirView$OnAirSeekBarPopupListener;

.field private mSeekListener:Lcom/samsung/musicplus/player/PlayerView$CustomSeekBarListener;

.field private mShuffleButton:Landroid/widget/ImageView;

.field private mShuffleResOff:I

.field private mShuffleResOn:I

.field protected mStop:Z

.field private final mTextUpdateHandler:Lcom/samsung/musicplus/player/PlayerView$TextUpdateHandler;

.field private mTheme:I

.field private mThumbSize:I

.field private mThumbnailButton:Landroid/view/View;

.field private mThumbnailImageView:Landroid/widget/ImageView;

.field private mThumbnailSelectorLayout:Landroid/view/View;

.field private mTitle:Landroid/widget/TextView;

.field protected mTotalTime:Landroid/widget/TextView;

.field private mViewUpdateHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "root"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x0

    const/4 v3, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 248
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 118
    new-instance v0, Lcom/samsung/musicplus/player/PlayerView$TextUpdateHandler;

    invoke-direct {v0, p0, v4}, Lcom/samsung/musicplus/player/PlayerView$TextUpdateHandler;-><init>(Lcom/samsung/musicplus/player/PlayerView;Lcom/samsung/musicplus/player/PlayerView$1;)V

    iput-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mTextUpdateHandler:Lcom/samsung/musicplus/player/PlayerView$TextUpdateHandler;

    .line 122
    iput-boolean v2, p0, Lcom/samsung/musicplus/player/PlayerView;->mPause:Z

    .line 124
    iput-boolean v2, p0, Lcom/samsung/musicplus/player/PlayerView;->mStop:Z

    .line 126
    iput v3, p0, Lcom/samsung/musicplus/player/PlayerView;->mList:I

    .line 170
    iput-boolean v1, p0, Lcom/samsung/musicplus/player/PlayerView;->mKeepLyricShowingState:Z

    .line 173
    const/16 v0, 0x190

    iput v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mThumbSize:I

    .line 176
    iput-boolean v1, p0, Lcom/samsung/musicplus/player/PlayerView;->mIsAlbumLoaded:Z

    .line 179
    iput-boolean v1, p0, Lcom/samsung/musicplus/player/PlayerView;->mIsPrepared:Z

    .line 191
    const v0, 0x7f02007e

    iput v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mShuffleResOn:I

    .line 193
    const v0, 0x7f02007d

    iput v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mShuffleResOff:I

    .line 195
    const v0, 0x7f02007f

    iput v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mRepeatResOne:I

    .line 197
    const v0, 0x7f020081

    iput v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mRepeatResOff:I

    .line 199
    const v0, 0x7f020080

    iput v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mRepeatResAll:I

    .line 201
    new-instance v0, Lcom/samsung/musicplus/player/PlayerView$1;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/player/PlayerView$1;-><init>(Lcom/samsung/musicplus/player/PlayerView;)V

    iput-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mViewUpdateHandler:Landroid/os/Handler;

    .line 385
    const v0, 0x7f02006e

    iput v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mPlayImageResource:I

    .line 403
    invoke-static {}, Lcom/samsung/musicplus/util/UiUtils;->getTheme()I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mTheme:I

    .line 675
    iput-boolean v2, p0, Lcom/samsung/musicplus/player/PlayerView;->mAlbumViewShowing:Z

    .line 817
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mPosOverride:J

    .line 819
    new-instance v0, Lcom/samsung/musicplus/player/PlayerView$CustomSeekBarListener;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/player/PlayerView$CustomSeekBarListener;-><init>(Lcom/samsung/musicplus/player/PlayerView;)V

    iput-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mSeekListener:Lcom/samsung/musicplus/player/PlayerView$CustomSeekBarListener;

    .line 1252
    iput v3, p0, Lcom/samsung/musicplus/player/PlayerView;->mCurrentListType:I

    .line 1510
    new-instance v0, Lcom/samsung/musicplus/player/PlayerView$7;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/player/PlayerView$7;-><init>(Lcom/samsung/musicplus/player/PlayerView;)V

    iput-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mSeekBarAirListener:Lcom/samsung/musicplus/library/hardware/AirView$OnAirSeekBarPopupListener;

    .line 1633
    new-instance v0, Lcom/samsung/musicplus/player/PlayerView$HoverListener;

    invoke-direct {v0, p0, v4}, Lcom/samsung/musicplus/player/PlayerView$HoverListener;-><init>(Lcom/samsung/musicplus/player/PlayerView;Lcom/samsung/musicplus/player/PlayerView$1;)V

    iput-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mHoverListner:Lcom/samsung/musicplus/player/PlayerView$HoverListener;

    .line 249
    iput-object p1, p0, Lcom/samsung/musicplus/player/PlayerView;->mContext:Landroid/content/Context;

    .line 250
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getListType()I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mList:I

    .line 251
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getKey()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mKeyWord:Ljava/lang/String;

    .line 252
    invoke-virtual {p0, p2}, Lcom/samsung/musicplus/player/PlayerView;->onCreateView(Landroid/view/View;)V

    .line 253
    return-void
.end method

.method static synthetic access$100(Lcom/samsung/musicplus/player/PlayerView;JZ)V
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/player/PlayerView;
    .param p1, "x1"    # J
    .param p3, "x2"    # Z

    .prologue
    .line 108
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/musicplus/player/PlayerView;->queueNextRefresh(JZ)V

    return-void
.end method

.method static synthetic access$1000(Lcom/samsung/musicplus/player/PlayerView;)J
    .locals 2
    .param p0, "x0"    # Lcom/samsung/musicplus/player/PlayerView;

    .prologue
    .line 108
    iget-wide v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mDuration:J

    return-wide v0
.end method

.method static synthetic access$1100(Lcom/samsung/musicplus/player/PlayerView;)Lcom/samsung/musicplus/widget/progress/MusicProgressBarScrubbing;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/player/PlayerView;

    .prologue
    .line 108
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mScrubbingBar:Lcom/samsung/musicplus/widget/progress/MusicProgressBarScrubbing;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/musicplus/player/PlayerView;Landroid/graphics/Bitmap;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/player/PlayerView;
    .param p1, "x1"    # Landroid/graphics/Bitmap;

    .prologue
    .line 108
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/player/PlayerView;->onAlbumLoadFinished(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method static synthetic access$300(Lcom/samsung/musicplus/player/PlayerView;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/player/PlayerView;

    .prologue
    .line 108
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mTitle:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/musicplus/player/PlayerView;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/player/PlayerView;

    .prologue
    .line 108
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mArtist:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/musicplus/player/PlayerView;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/player/PlayerView;

    .prologue
    .line 108
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mAlbum:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/samsung/musicplus/player/PlayerView;Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/player/PlayerView;
    .param p1, "x1"    # Landroid/view/View;
    .param p2, "x2"    # Landroid/view/MotionEvent;

    .prologue
    .line 108
    invoke-direct {p0, p1, p2}, Lcom/samsung/musicplus/player/PlayerView;->scrubbingByProgress(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$700(Lcom/samsung/musicplus/player/PlayerView;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/player/PlayerView;

    .prologue
    .line 108
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mListContainer:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$800(Lcom/samsung/musicplus/player/PlayerView;)Landroid/view/animation/Animation;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/player/PlayerView;

    .prologue
    .line 108
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mNowPlayingListDownOut:Landroid/view/animation/Animation;

    return-object v0
.end method

.method static synthetic access$900(Lcom/samsung/musicplus/player/PlayerView;)J
    .locals 2
    .param p0, "x0"    # Lcom/samsung/musicplus/player/PlayerView;

    .prologue
    .line 108
    iget-wide v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mPosOverride:J

    return-wide v0
.end method

.method static synthetic access$902(Lcom/samsung/musicplus/player/PlayerView;J)J
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/player/PlayerView;
    .param p1, "x1"    # J

    .prologue
    .line 108
    iput-wide p1, p0, Lcom/samsung/musicplus/player/PlayerView;->mPosOverride:J

    return-wide p1
.end method

.method private ensureList()V
    .locals 3

    .prologue
    .line 627
    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerView;->mListContainer:Landroid/view/View;

    if-nez v1, :cond_0

    .line 628
    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerView;->mParentView:Landroid/view/View;

    const v2, 0x7f0d0109

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 629
    .local v0, "stub":Landroid/view/ViewStub;
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/musicplus/player/PlayerView;->mListContainer:Landroid/view/View;

    .line 630
    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerView;->mListContainer:Landroid/view/View;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 632
    .end local v0    # "stub":Landroid/view/ViewStub;
    :cond_0
    return-void
.end method

.method private getListButtonAirView(I)Landroid/view/View;
    .locals 12
    .param p1, "theme"    # I

    .prologue
    .line 1479
    if-nez p1, :cond_1

    .line 1480
    const v1, 0x7f040013

    .line 1484
    .local v1, "layout":I
    :goto_0
    iget-object v7, p0, Lcom/samsung/musicplus/player/PlayerView;->mContext:Landroid/content/Context;

    invoke-static {v7}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v7, v1, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 1487
    .local v5, "root":Landroid/view/View;
    const v7, 0x7f0d006d

    invoke-virtual {v5, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 1488
    .local v6, "title":Landroid/widget/TextView;
    const/4 v2, 0x0

    .line 1489
    .local v2, "name":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getListType()I

    move-result v7

    invoke-static {v7}, Lcom/samsung/musicplus/util/ListUtils;->getPreDefinedList(I)I

    move-result v7

    const v8, 0x20004

    if-ne v7, v8, :cond_0

    .line 1490
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getKey()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 1491
    .local v4, "playlistId":I
    if-lez v4, :cond_0

    .line 1492
    iget-object v7, p0, Lcom/samsung/musicplus/player/PlayerView;->mContext:Landroid/content/Context;

    invoke-static {v7, v4}, Lcom/samsung/musicplus/util/UiUtils;->getPlaylistName(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    .line 1495
    .end local v4    # "playlistId":I
    :cond_0
    if-eqz v2, :cond_2

    .line 1496
    invoke-virtual {v6, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1502
    :goto_1
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getListItemCount()I

    move-result v0

    .line 1503
    .local v0, "count":I
    const v7, 0x7f0d006e

    invoke-virtual {v5, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 1504
    .local v3, "number":Landroid/widget/TextView;
    iget-object v7, p0, Lcom/samsung/musicplus/player/PlayerView;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0f000a

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {v7, v8, v0, v9}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1506
    return-object v5

    .line 1482
    .end local v0    # "count":I
    .end local v1    # "layout":I
    .end local v2    # "name":Ljava/lang/String;
    .end local v3    # "number":Landroid/widget/TextView;
    .end local v5    # "root":Landroid/view/View;
    .end local v6    # "title":Landroid/widget/TextView;
    :cond_1
    const v1, 0x7f040012

    .restart local v1    # "layout":I
    goto :goto_0

    .line 1498
    .restart local v2    # "name":Ljava/lang/String;
    .restart local v5    # "root":Landroid/view/View;
    .restart local v6    # "title":Landroid/widget/TextView;
    :cond_2
    const v7, 0x7f1000b3

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1
.end method

.method private isEllipsis(Landroid/widget/TextView;)Z
    .locals 1
    .param p1, "v"    # Landroid/widget/TextView;

    .prologue
    .line 1729
    invoke-static {p1}, Lcom/samsung/musicplus/library/view/TextViewCompat;->isEllipsis(Landroid/widget/TextView;)Z

    move-result v0

    return v0
.end method

.method private modifyBitmap(Landroid/view/View;Landroid/graphics/Bitmap;)V
    .locals 16
    .param p1, "v"    # Landroid/view/View;
    .param p2, "bm"    # Landroid/graphics/Bitmap;

    .prologue
    .line 269
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getWidth()I

    move-result v12

    .line 270
    .local v12, "vwidth":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getHeight()I

    move-result v11

    .line 271
    .local v11, "vheight":I
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    .line 272
    .local v3, "bwidth":I
    invoke-virtual/range {p2 .. p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    .line 273
    .local v2, "bheight":I
    int-to-float v13, v12

    int-to-float v14, v3

    div-float v9, v13, v14

    .line 274
    .local v9, "scalex":F
    int-to-float v13, v11

    int-to-float v14, v2

    div-float v10, v13, v14

    .line 275
    .local v10, "scaley":F
    invoke-static {v9, v10}, Ljava/lang/Math;->max(FF)F

    move-result v13

    const v14, 0x40a9999a    # 5.3f

    mul-float v8, v13, v14

    .line 277
    .local v8, "scale":F
    sget-object v5, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    .line 278
    .local v5, "config":Landroid/graphics/Bitmap$Config;
    invoke-static {v12, v11, v5}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 279
    .local v1, "bg":Landroid/graphics/Bitmap;
    new-instance v4, Landroid/graphics/Canvas;

    invoke-direct {v4, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 280
    .local v4, "c":Landroid/graphics/Canvas;
    new-instance v7, Landroid/graphics/Paint;

    invoke-direct {v7}, Landroid/graphics/Paint;-><init>()V

    .line 281
    .local v7, "paint":Landroid/graphics/Paint;
    const/4 v13, 0x1

    invoke-virtual {v7, v13}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 282
    const/4 v13, 0x1

    invoke-virtual {v7, v13}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 283
    new-instance v13, Landroid/graphics/BlurMaskFilter;

    const v14, 0x409ccccd    # 4.9f

    sget-object v15, Landroid/graphics/BlurMaskFilter$Blur;->NORMAL:Landroid/graphics/BlurMaskFilter$Blur;

    invoke-direct {v13, v14, v15}, Landroid/graphics/BlurMaskFilter;-><init>(FLandroid/graphics/BlurMaskFilter$Blur;)V

    invoke-virtual {v7, v13}, Landroid/graphics/Paint;->setMaskFilter(Landroid/graphics/MaskFilter;)Landroid/graphics/MaskFilter;

    .line 285
    new-instance v6, Landroid/graphics/Matrix;

    invoke-direct {v6}, Landroid/graphics/Matrix;-><init>()V

    .line 286
    .local v6, "matrix":Landroid/graphics/Matrix;
    neg-int v13, v3

    div-int/lit8 v13, v13, 0x2

    int-to-float v13, v13

    neg-int v14, v2

    div-int/lit8 v14, v14, 0x2

    int-to-float v14, v14

    invoke-virtual {v6, v13, v14}, Landroid/graphics/Matrix;->setTranslate(FF)V

    .line 288
    const/high16 v13, 0x41200000    # 10.0f

    invoke-virtual {v6, v13}, Landroid/graphics/Matrix;->postRotate(F)Z

    .line 289
    invoke-virtual {v6, v8, v8}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 290
    div-int/lit8 v13, v12, 0x2

    int-to-float v13, v13

    div-int/lit8 v14, v11, 0x2

    int-to-float v14, v14

    invoke-virtual {v6, v13, v14}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 292
    move-object/from16 v0, p2

    invoke-virtual {v4, v0, v6, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    .line 293
    new-instance v13, Landroid/graphics/drawable/BitmapDrawable;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/musicplus/player/PlayerView;->mContext:Landroid/content/Context;

    invoke-virtual {v14}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    invoke-direct {v13, v14, v1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 294
    return-void
.end method

.method private onAlbumLoadFinished(Landroid/graphics/Bitmap;)V
    .locals 3
    .param p1, "bm"    # Landroid/graphics/Bitmap;

    .prologue
    .line 1234
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/PlayerView;->getAlbumArtView()Landroid/widget/ImageView;

    move-result-object v0

    .line 1235
    .local v0, "v":Landroid/widget/ImageView;
    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v2, p0, Lcom/samsung/musicplus/player/PlayerView;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2, p1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1236
    iget-boolean v1, p0, Lcom/samsung/musicplus/player/PlayerView;->mIsPrepared:Z

    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2}, Lcom/samsung/musicplus/player/PlayerView;->updatePrepareLoading(ZZ)V

    .line 1239
    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerView;->mThumbnailImageView:Landroid/widget/ImageView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerView;->mThumbnailSelectorLayout:Landroid/view/View;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerView;->mThumbnailSelectorLayout:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    .line 1241
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/player/PlayerView;->updateButtonThumbnailImage(Landroid/graphics/Bitmap;)V

    .line 1243
    :cond_0
    return-void
.end method

.method private prepareAlbumView()V
    .locals 5

    .prologue
    .line 638
    invoke-direct {p0}, Lcom/samsung/musicplus/player/PlayerView;->prepareCommonView()V

    .line 639
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/samsung/musicplus/player/PlayerView;->mAlbumViewShowing:Z

    .line 641
    iget-object v3, p0, Lcom/samsung/musicplus/player/PlayerView;->mContext:Landroid/content/Context;

    check-cast v3, Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    .line 642
    .local v2, "manager":Landroid/app/FragmentManager;
    const-string v3, "now_playing_list"

    invoke-virtual {v2, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 643
    .local v0, "fg":Landroid/app/Fragment;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Fragment;->isDetached()Z

    move-result v3

    if-nez v3, :cond_0

    .line 644
    const-string v3, "MusicUi"

    const-string v4, "NowPlayingListFragment detached!"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 645
    invoke-virtual {v2}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 646
    .local v1, "ft":Landroid/app/FragmentTransaction;
    invoke-virtual {v1, v0}, Landroid/app/FragmentTransaction;->detach(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 647
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 649
    .end local v1    # "ft":Landroid/app/FragmentTransaction;
    :cond_0
    return-void
.end method

.method private prepareCommonView()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 678
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mListButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 679
    iget-boolean v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mAlbumViewShowing:Z

    if-eqz v0, :cond_1

    .line 680
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mListButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 681
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mThumbnailSelectorLayout:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 682
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mThumbnailImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 683
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/player/PlayerView;->updateButtonThumbnailImage(Landroid/graphics/Bitmap;)V

    .line 690
    :cond_0
    :goto_0
    return-void

    .line 686
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mThumbnailSelectorLayout:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 687
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mListButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private prepareListView()V
    .locals 5

    .prologue
    .line 656
    invoke-direct {p0}, Lcom/samsung/musicplus/player/PlayerView;->prepareCommonView()V

    .line 657
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/musicplus/player/PlayerView;->mAlbumViewShowing:Z

    .line 659
    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerView;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 660
    .local v0, "manager":Landroid/app/FragmentManager;
    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    const v2, 0x7f0d0062

    iget v3, p0, Lcom/samsung/musicplus/player/PlayerView;->mList:I

    iget-object v4, p0, Lcom/samsung/musicplus/player/PlayerView;->mKeyWord:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/samsung/musicplus/player/NowPlayingListFragment;->getInstance(ILjava/lang/String;)Landroid/app/Fragment;

    move-result-object v3

    const-string v4, "now_playing_list"

    invoke-virtual {v1, v2, v3, v4}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 664
    return-void
.end method

.method private queueNextRefresh(JZ)V
    .locals 3
    .param p1, "delay"    # J
    .param p3, "stop"    # Z

    .prologue
    const/4 v2, 0x1

    .line 918
    if-nez p3, :cond_0

    .line 919
    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerView;->mViewUpdateHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 920
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerView;->mViewUpdateHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 921
    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerView;->mViewUpdateHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0, p1, p2}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 923
    .end local v0    # "msg":Landroid/os/Message;
    :cond_0
    return-void
.end method

.method private scrubbingByProgress(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x0

    .line 897
    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerView;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/musicplus/util/UiUtils;->isEasyMode(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 914
    :goto_0
    return v2

    .line 900
    :cond_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 901
    .local v0, "action":I
    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 904
    :pswitch_1
    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerView;->mScrubbingBar:Lcom/samsung/musicplus/widget/progress/MusicProgressBarScrubbing;

    invoke-virtual {v1, p1, p2}, Lcom/samsung/musicplus/widget/progress/MusicProgressBarScrubbing;->setScrubbingSpeed(Landroid/view/View;Landroid/view/MotionEvent;)I

    goto :goto_0

    .line 909
    :pswitch_2
    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerView;->mScrubbingBar:Lcom/samsung/musicplus/widget/progress/MusicProgressBarScrubbing;

    invoke-virtual {v1}, Lcom/samsung/musicplus/widget/progress/MusicProgressBarScrubbing;->cancelScrubbingSpeedInfoPopup()V

    goto :goto_0

    .line 901
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private setAirView(Landroid/view/View;Lcom/samsung/musicplus/library/hardware/AirView$OnAirViewPopupListener;IZ)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;
    .param p2, "listener"    # Lcom/samsung/musicplus/library/hardware/AirView$OnAirViewPopupListener;
    .param p3, "type"    # I
    .param p4, "enableAnimation"    # Z

    .prologue
    .line 1717
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/musicplus/util/UiUtils;->checkHoverUI(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1718
    const/16 v3, 0x258

    const/4 v5, 0x0

    const/4 v6, 0x1

    move-object v0, p1

    move-object v1, p2

    move v2, p3

    move v4, p4

    invoke-static/range {v0 .. v6}, Lcom/samsung/musicplus/library/hardware/AirView;->setView(Landroid/view/View;Lcom/samsung/musicplus/library/hardware/AirView$OnAirViewPopupListener;IIZZZ)V

    .line 1720
    :cond_0
    return-void
.end method

.method private setAirViewSeekBar(Landroid/widget/SeekBar;Lcom/samsung/musicplus/library/hardware/AirView$OnAirSeekBarPopupListener;)V
    .locals 1
    .param p1, "seekbar"    # Landroid/widget/SeekBar;
    .param p2, "listener"    # Lcom/samsung/musicplus/library/hardware/AirView$OnAirSeekBarPopupListener;

    .prologue
    .line 1723
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/musicplus/util/UiUtils;->checkHoverUI(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1724
    invoke-static {p1, p2}, Lcom/samsung/musicplus/library/hardware/AirView;->setView(Landroid/widget/SeekBar;Lcom/samsung/musicplus/library/hardware/AirView$OnAirSeekBarPopupListener;)V

    .line 1726
    :cond_0
    return-void
.end method

.method private setCurrentTimeTalkback()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1009
    const/4 v1, 0x0

    .line 1010
    .local v1, "talkback":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/musicplus/player/PlayerView;->mCurrentTime:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1011
    .local v0, "currentTime":Ljava/lang/String;
    const-string v2, "-"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1012
    iget-object v2, p0, Lcom/samsung/musicplus/player/PlayerView;->mContext:Landroid/content/Context;

    const v3, 0x7f10019d

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1016
    :goto_0
    iget-object v2, p0, Lcom/samsung/musicplus/player/PlayerView;->mCurrentTime:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1017
    return-void

    .line 1014
    :cond_0
    iget-object v2, p0, Lcom/samsung/musicplus/player/PlayerView;->mContext:Landroid/content/Context;

    invoke-static {v2, v0}, Lcom/samsung/musicplus/util/UiUtils;->getDurationTalkback(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private setOnClickListener(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 449
    new-instance v0, Lcom/samsung/musicplus/player/PlayerView$3;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/player/PlayerView$3;-><init>(Lcom/samsung/musicplus/player/PlayerView;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 476
    return-void
.end method

.method private startAnimation(Landroid/view/animation/Animation;Z)V
    .locals 1
    .param p1, "ani"    # Landroid/view/animation/Animation;
    .param p2, "visible"    # Z

    .prologue
    .line 561
    new-instance v0, Lcom/samsung/musicplus/player/PlayerView$4;

    invoke-direct {v0, p0, p2}, Lcom/samsung/musicplus/player/PlayerView$4;-><init>(Lcom/samsung/musicplus/player/PlayerView;Z)V

    invoke-virtual {p1, v0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 581
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mListContainer:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->bringToFront()V

    .line 582
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mListContainer:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 583
    return-void
.end method


# virtual methods
.method public adjustMiniPlayerLayout()V
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x0

    .line 1922
    const/4 v4, 0x1

    iget-object v5, p0, Lcom/samsung/musicplus/player/PlayerView;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    iget v5, v5, Landroid/content/res/Configuration;->orientation:I

    if-ne v4, v5, :cond_1

    .line 1949
    :cond_0
    :goto_0
    return-void

    .line 1925
    :cond_1
    iget-object v4, p0, Lcom/samsung/musicplus/player/PlayerView;->mParentView:Landroid/view/View;

    const v5, 0x7f0d00f6

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1926
    .local v0, "controller":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 1927
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v3

    .line 1928
    .local v3, "width":I
    iget-object v4, p0, Lcom/samsung/musicplus/player/PlayerView;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0c00a4

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 1930
    .local v2, "minWidth":I
    iget-object v4, p0, Lcom/samsung/musicplus/player/PlayerView;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0c00a3

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 1932
    .local v1, "leastWidth":I
    if-ge v3, v2, :cond_3

    .line 1933
    iget-object v4, p0, Lcom/samsung/musicplus/player/PlayerView;->mShuffleButton:Landroid/widget/ImageView;

    invoke-virtual {v4, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1934
    iget-object v4, p0, Lcom/samsung/musicplus/player/PlayerView;->mRepeatButton:Landroid/widget/ImageView;

    invoke-virtual {v4, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1935
    if-ge v3, v1, :cond_2

    .line 1936
    iget-object v4, p0, Lcom/samsung/musicplus/player/PlayerView;->mNext:Landroid/view/View;

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 1937
    iget-object v4, p0, Lcom/samsung/musicplus/player/PlayerView;->mPrev:Landroid/view/View;

    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 1939
    :cond_2
    iget-object v4, p0, Lcom/samsung/musicplus/player/PlayerView;->mNext:Landroid/view/View;

    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    .line 1940
    iget-object v4, p0, Lcom/samsung/musicplus/player/PlayerView;->mPrev:Landroid/view/View;

    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 1943
    :cond_3
    iget-object v4, p0, Lcom/samsung/musicplus/player/PlayerView;->mShuffleButton:Landroid/widget/ImageView;

    invoke-virtual {v4, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1944
    iget-object v4, p0, Lcom/samsung/musicplus/player/PlayerView;->mRepeatButton:Landroid/widget/ImageView;

    invoke-virtual {v4, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1945
    iget-object v4, p0, Lcom/samsung/musicplus/player/PlayerView;->mNext:Landroid/view/View;

    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    .line 1946
    iget-object v4, p0, Lcom/samsung/musicplus/player/PlayerView;->mPrev:Landroid/view/View;

    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public final cancelFfRew()V
    .locals 1

    .prologue
    .line 431
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mFfRewTouchListener:Lcom/samsung/musicplus/widget/control/ForwardRewindListener;

    if-eqz v0, :cond_0

    .line 432
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mFfRewTouchListener:Lcom/samsung/musicplus/widget/control/ForwardRewindListener;

    invoke-virtual {v0}, Lcom/samsung/musicplus/widget/control/ForwardRewindListener;->cancel()V

    .line 434
    :cond_0
    return-void
.end method

.method public changeViewsVisibility(I)V
    .locals 1
    .param p1, "visiblity"    # I

    .prologue
    .line 437
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mAlbumArt:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 438
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mPrev:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 439
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mNext:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 440
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mPlay:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 441
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mTotalTime:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 442
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mCurrentTime:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 443
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, p1}, Landroid/widget/SeekBar;->setVisibility(I)V

    .line 444
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mShuffleButton:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 445
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mRepeatButton:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 446
    return-void
.end method

.method protected clearHoverViewState()V
    .locals 3

    .prologue
    const/16 v2, 0xa

    .line 553
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mHoverListner:Lcom/samsung/musicplus/player/PlayerView$HoverListener;

    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerView;->mListButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/musicplus/player/PlayerView$HoverListener;->setHoverState(Landroid/view/View;I)V

    .line 554
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mHoverListner:Lcom/samsung/musicplus/player/PlayerView$HoverListener;

    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerView;->mThumbnailButton:Landroid/view/View;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/musicplus/player/PlayerView$HoverListener;->setHoverState(Landroid/view/View;I)V

    .line 555
    return-void
.end method

.method public getAirView(Landroid/view/View;)Landroid/view/View;
    .locals 4
    .param p1, "parent"    # Landroid/view/View;

    .prologue
    .line 1382
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 1398
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1384
    :sswitch_0
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerView;->mContext:Landroid/content/Context;

    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getPrevUri()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/util/UiUtils;->getTitle(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Lcom/samsung/musicplus/player/PlayerView;->mTheme:I

    invoke-static {v0, v1, v2}, Lcom/samsung/musicplus/util/UiUtils;->getAirTextView(Landroid/content/Context;Ljava/lang/String;I)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 1387
    :sswitch_1
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerView;->mContext:Landroid/content/Context;

    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getNextUri()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/util/UiUtils;->getTitle(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Lcom/samsung/musicplus/player/PlayerView;->mTheme:I

    invoke-static {v0, v1, v2}, Lcom/samsung/musicplus/util/UiUtils;->getAirTextView(Landroid/content/Context;Ljava/lang/String;I)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 1390
    :sswitch_2
    iget v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mTheme:I

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/player/PlayerView;->getListButtonAirView(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 1392
    :sswitch_3
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mContext:Landroid/content/Context;

    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getAlbum()Ljava/lang/String;

    move-result-object v1

    iget v2, p0, Lcom/samsung/musicplus/player/PlayerView;->mTheme:I

    invoke-static {v0, v1, v2}, Lcom/samsung/musicplus/util/UiUtils;->getAirTextView(Landroid/content/Context;Ljava/lang/String;I)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 1394
    :sswitch_4
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mTitle:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerView;->mArtist:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/musicplus/player/PlayerView;->mAlbum:Landroid/widget/TextView;

    iget v3, p0, Lcom/samsung/musicplus/player/PlayerView;->mTheme:I

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/samsung/musicplus/player/PlayerView;->getTitleAirView(Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;I)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 1382
    :sswitch_data_0
    .sparse-switch
        0x7f0d00f9 -> :sswitch_0
        0x7f0d00fc -> :sswitch_1
        0x7f0d0111 -> :sswitch_4
        0x7f0d0116 -> :sswitch_2
        0x7f0d0119 -> :sswitch_3
    .end sparse-switch
.end method

.method public getAlbumArtView()Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 1774
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mAlbumArt:Landroid/widget/ImageView;

    return-object v0
.end method

.method protected getAlbumTextView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 1765
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mAlbum:Landroid/widget/TextView;

    return-object v0
.end method

.method protected getAlbumView()Landroid/view/View;
    .locals 1

    .prologue
    .line 1891
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mAlbumView:Landroid/view/View;

    return-object v0
.end method

.method protected getAlbumViewVisibility()Z
    .locals 1

    .prologue
    .line 1900
    iget-boolean v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mAlbumViewShowing:Z

    return v0
.end method

.method protected getAnimatedPlayButtonView(Landroid/view/View;)Lcom/samsung/musicplus/widget/AnimatedToggleButton;
    .locals 1
    .param p1, "root"    # Landroid/view/View;

    .prologue
    .line 382
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getArtistTextView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 1756
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mArtist:Landroid/widget/TextView;

    return-object v0
.end method

.method protected getCurrentTimeView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 1810
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mCurrentTime:Landroid/widget/TextView;

    return-object v0
.end method

.method protected getListButtonView()Landroid/view/View;
    .locals 1

    .prologue
    .line 1828
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mListButton:Landroid/widget/ImageView;

    return-object v0
.end method

.method protected getListContainer()Landroid/view/View;
    .locals 1

    .prologue
    .line 1819
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mListContainer:Landroid/view/View;

    return-object v0
.end method

.method protected getNextButtonView()Landroid/view/View;
    .locals 1

    .prologue
    .line 1792
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mNext:Landroid/view/View;

    return-object v0
.end method

.method protected getParentView()Landroid/view/View;
    .locals 1

    .prologue
    .line 1738
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mParentView:Landroid/view/View;

    return-object v0
.end method

.method protected getPlayButtonView()Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 1783
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mPlay:Landroid/widget/ImageView;

    return-object v0
.end method

.method protected getRepeatButtonView()Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 1864
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mRepeatButton:Landroid/widget/ImageView;

    return-object v0
.end method

.method protected getSeekBarListener()Landroid/widget/SeekBar$OnSeekBarChangeListener;
    .locals 1

    .prologue
    .line 1882
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mSeekListener:Lcom/samsung/musicplus/player/PlayerView$CustomSeekBarListener;

    return-object v0
.end method

.method protected getSeekBarView()Landroid/view/View;
    .locals 1

    .prologue
    .line 1837
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mSeekBar:Landroid/widget/SeekBar;

    return-object v0
.end method

.method protected getShuffleButtonView()Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 1855
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mShuffleButton:Landroid/widget/ImageView;

    return-object v0
.end method

.method protected getThumbnailButtonView()Landroid/view/View;
    .locals 1

    .prologue
    .line 1873
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mThumbnailSelectorLayout:Landroid/view/View;

    return-object v0
.end method

.method protected getTitleAirView(Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;I)Landroid/view/View;
    .locals 15
    .param p1, "titleView"    # Landroid/widget/TextView;
    .param p2, "artistView"    # Landroid/widget/TextView;
    .param p3, "albumView"    # Landroid/widget/TextView;
    .param p4, "theme"    # I

    .prologue
    .line 1409
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-nez p3, :cond_2

    .line 1410
    :cond_0
    const-string v13, "MusicUi"

    const-string v14, "getTitleAirView() - view is null, so return!!"

    invoke-static {v13, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1411
    const/4 v9, 0x0

    .line 1468
    :cond_1
    :goto_0
    return-object v9

    .line 1415
    :cond_2
    if-nez p4, :cond_3

    .line 1416
    const v8, 0x7f040010

    .line 1420
    .local v8, "layout":I
    :goto_1
    iget-object v13, p0, Lcom/samsung/musicplus/player/PlayerView;->mContext:Landroid/content/Context;

    invoke-static {v13}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v13

    const/4 v14, 0x0

    invoke-virtual {v13, v8, v14}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v9

    .line 1422
    .local v9, "popup":Landroid/view/View;
    invoke-virtual/range {p1 .. p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v12

    .line 1423
    .local v12, "titleText":Ljava/lang/CharSequence;
    invoke-virtual/range {p2 .. p2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    .line 1424
    .local v6, "artistText":Ljava/lang/CharSequence;
    invoke-virtual/range {p3 .. p3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    .line 1426
    .local v3, "albumText":Ljava/lang/CharSequence;
    invoke-direct/range {p0 .. p1}, Lcom/samsung/musicplus/player/PlayerView;->isEllipsis(Landroid/widget/TextView;)Z

    move-result v11

    .line 1427
    .local v11, "titleEllipsised":Z
    move-object/from16 v0, p2

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/player/PlayerView;->isEllipsis(Landroid/widget/TextView;)Z

    move-result v5

    .line 1428
    .local v5, "artistEllipsised":Z
    move-object/from16 v0, p3

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/player/PlayerView;->isEllipsis(Landroid/widget/TextView;)Z

    move-result v2

    .line 1430
    .local v2, "albumEllipsised":Z
    if-eqz v5, :cond_4

    if-eqz v11, :cond_4

    .line 1431
    const v13, 0x7f0d0067

    invoke-virtual {v9, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    .line 1432
    .local v10, "title":Landroid/widget/TextView;
    const v13, 0x7f0d0068

    invoke-virtual {v9, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 1434
    .local v4, "artist":Landroid/widget/TextView;
    invoke-virtual {v10, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1435
    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1436
    const/4 v13, 0x0

    invoke-virtual {v10, v13}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1437
    const/4 v13, 0x0

    invoke-virtual {v4, v13}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 1418
    .end local v2    # "albumEllipsised":Z
    .end local v3    # "albumText":Ljava/lang/CharSequence;
    .end local v4    # "artist":Landroid/widget/TextView;
    .end local v5    # "artistEllipsised":Z
    .end local v6    # "artistText":Ljava/lang/CharSequence;
    .end local v8    # "layout":I
    .end local v9    # "popup":Landroid/view/View;
    .end local v10    # "title":Landroid/widget/TextView;
    .end local v11    # "titleEllipsised":Z
    .end local v12    # "titleText":Ljava/lang/CharSequence;
    :cond_3
    const v8, 0x7f04000f

    .restart local v8    # "layout":I
    goto :goto_1

    .line 1438
    .restart local v2    # "albumEllipsised":Z
    .restart local v3    # "albumText":Ljava/lang/CharSequence;
    .restart local v5    # "artistEllipsised":Z
    .restart local v6    # "artistText":Ljava/lang/CharSequence;
    .restart local v9    # "popup":Landroid/view/View;
    .restart local v11    # "titleEllipsised":Z
    .restart local v12    # "titleText":Ljava/lang/CharSequence;
    :cond_4
    if-eqz v11, :cond_5

    if-eqz v2, :cond_5

    .line 1439
    const v13, 0x7f0d0067

    invoke-virtual {v9, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    .line 1440
    .restart local v10    # "title":Landroid/widget/TextView;
    const v13, 0x7f0d006a

    invoke-virtual {v9, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 1442
    .local v1, "album":Landroid/widget/TextView;
    invoke-virtual {v10, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1443
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1444
    const/4 v13, 0x0

    invoke-virtual {v10, v13}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1445
    const/4 v13, 0x0

    invoke-virtual {v1, v13}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 1446
    .end local v1    # "album":Landroid/widget/TextView;
    .end local v10    # "title":Landroid/widget/TextView;
    :cond_5
    if-eqz v5, :cond_6

    if-eqz v2, :cond_6

    .line 1447
    const v13, 0x7f0d0069

    invoke-virtual {v9, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 1448
    .restart local v4    # "artist":Landroid/widget/TextView;
    const v13, 0x7f0d006a

    invoke-virtual {v9, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 1450
    .restart local v1    # "album":Landroid/widget/TextView;
    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1451
    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1452
    const/4 v13, 0x0

    invoke-virtual {v4, v13}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1453
    const/4 v13, 0x0

    invoke-virtual {v1, v13}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 1455
    .end local v1    # "album":Landroid/widget/TextView;
    .end local v4    # "artist":Landroid/widget/TextView;
    :cond_6
    const v13, 0x7f0d006b

    invoke-virtual {v9, v13}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 1457
    .local v7, "full":Landroid/widget/TextView;
    if-eqz v11, :cond_7

    .line 1458
    invoke-virtual {v7, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1459
    const/4 v13, 0x0

    invoke-virtual {v7, v13}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 1460
    :cond_7
    if-eqz v5, :cond_8

    .line 1461
    invoke-virtual {v7, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1462
    const/4 v13, 0x0

    invoke-virtual {v7, v13}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 1463
    :cond_8
    if-eqz v2, :cond_1

    .line 1464
    invoke-virtual {v7, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1465
    const/4 v13, 0x0

    invoke-virtual {v7, v13}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method public getTitleTextView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 1747
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mTitle:Landroid/widget/TextView;

    return-object v0
.end method

.method protected getTotalTimeView()Landroid/widget/TextView;
    .locals 1

    .prologue
    .line 1801
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mTotalTime:Landroid/widget/TextView;

    return-object v0
.end method

.method public getUiHandler()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 1918
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mViewUpdateHandler:Landroid/os/Handler;

    return-object v0
.end method

.method protected initializeTitleView(Z)V
    .locals 8
    .param p1, "enableAirView"    # Z

    .prologue
    const/4 v6, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 1124
    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerView;->mTitle:Landroid/widget/TextView;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerView;->mArtist:Landroid/widget/TextView;

    if-eqz v1, :cond_1

    .line 1160
    :cond_0
    :goto_0
    return-void

    .line 1128
    :cond_1
    const-string v1, "MusicUi"

    const-string v2, "initializeTitleView()"

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1129
    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerView;->mParentView:Landroid/view/View;

    const v2, 0x7f0d00d6

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/samsung/musicplus/player/PlayerView;->mTitle:Landroid/widget/TextView;

    .line 1135
    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerView;->mTitle:Landroid/widget/TextView;

    if-eqz v1, :cond_2

    .line 1136
    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerView;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v1, v4, v3}, Landroid/widget/TextView;->setLayerType(ILandroid/graphics/Paint;)V

    .line 1139
    :cond_2
    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerView;->mParentView:Landroid/view/View;

    const v2, 0x7f0d00d5

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/samsung/musicplus/player/PlayerView;->mArtist:Landroid/widget/TextView;

    .line 1140
    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerView;->mArtist:Landroid/widget/TextView;

    if-eqz v1, :cond_3

    .line 1141
    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerView;->mArtist:Landroid/widget/TextView;

    invoke-virtual {v1, v4, v3}, Landroid/widget/TextView;->setLayerType(ILandroid/graphics/Paint;)V

    .line 1144
    :cond_3
    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerView;->mParentView:Landroid/view/View;

    const v2, 0x7f0d00d7

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/samsung/musicplus/player/PlayerView;->mAlbum:Landroid/widget/TextView;

    .line 1145
    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerView;->mAlbum:Landroid/widget/TextView;

    if-eqz v1, :cond_4

    .line 1146
    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerView;->mAlbum:Landroid/widget/TextView;

    invoke-virtual {v1, v4, v3}, Landroid/widget/TextView;->setLayerType(ILandroid/graphics/Paint;)V

    .line 1149
    :cond_4
    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerView;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/musicplus/util/UiUtils;->checkHoverUI(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1154
    sget-boolean v1, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_SUPPORT_FINGER_AIR_VIEW:Z

    if-eqz v1, :cond_0

    .line 1155
    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerView;->mParentView:Landroid/view/View;

    const v2, 0x7f0d0111

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1156
    .local v0, "hoverPlace":Landroid/view/View;
    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerView;->mTitle:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/musicplus/player/PlayerView;->mArtist:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/samsung/musicplus/player/PlayerView;->mAlbum:Landroid/widget/TextView;

    move-object v5, p0

    move v7, v6

    invoke-static/range {v0 .. v7}, Lcom/samsung/musicplus/library/hardware/AirView;->setView(Landroid/view/View;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;ILcom/samsung/musicplus/library/hardware/AirView$OnAirViewPopupListener;ZZ)V

    goto :goto_0
.end method

.method isLyricViewShowing()Z
    .locals 1

    .prologue
    .line 595
    const/4 v0, 0x0

    return v0
.end method

.method public isNowPlayingListShown()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 667
    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerView;->mListContainer:Landroid/view/View;

    if-nez v1, :cond_1

    .line 670
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerView;->mListContainer:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected obtainPlayButtonView(Landroid/view/View;)Landroid/widget/ImageView;
    .locals 3
    .param p1, "root"    # Landroid/view/View;

    .prologue
    .line 361
    const v2, 0x7f0d00fa

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 362
    .local v1, "play":Landroid/widget/ImageView;
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/player/PlayerView;->getAnimatedPlayButtonView(Landroid/view/View;)Lcom/samsung/musicplus/widget/AnimatedToggleButton;

    move-result-object v0

    .line 373
    .local v0, "animatedPlay":Lcom/samsung/musicplus/widget/AnimatedToggleButton;
    if-eqz v0, :cond_0

    .line 374
    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->setVisibility(I)V

    .line 377
    :cond_0
    return-object v1
.end method

.method protected onCreateView(Landroid/view/View;)V
    .locals 3
    .param p1, "root"    # Landroid/view/View;

    .prologue
    .line 299
    iput-object p1, p0, Lcom/samsung/musicplus/player/PlayerView;->mParentView:Landroid/view/View;

    .line 301
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mParentView:Landroid/view/View;

    const v1, 0x7f0d0054

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mAlbumArt:Landroid/widget/ImageView;

    .line 303
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mParentView:Landroid/view/View;

    const v1, 0x7f0d010b

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mAlbumView:Landroid/view/View;

    .line 305
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/player/PlayerView;->obtainPlayButtonView(Landroid/view/View;)Landroid/widget/ImageView;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mPlay:Landroid/widget/ImageView;

    .line 307
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mPlay:Landroid/widget/ImageView;

    iget v1, p0, Lcom/samsung/musicplus/player/PlayerView;->mPlayImageResource:I

    invoke-virtual {p0, v0, v1}, Lcom/samsung/musicplus/player/PlayerView;->setPlayButtonImageResource(Landroid/widget/ImageView;I)V

    .line 309
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mPlay:Landroid/widget/ImageView;

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/player/PlayerView;->setOnClickListener(Landroid/view/View;)V

    .line 311
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mParentView:Landroid/view/View;

    const v1, 0x7f0d00f9

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mPrev:Landroid/view/View;

    .line 312
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mParentView:Landroid/view/View;

    const v1, 0x7f0d00fc

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mNext:Landroid/view/View;

    .line 313
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mPrev:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerView;->mNext:Landroid/view/View;

    invoke-virtual {p0, v0, v1}, Lcom/samsung/musicplus/player/PlayerView;->setNextPrevListener(Landroid/view/View;Landroid/view/View;)V

    .line 315
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mParentView:Landroid/view/View;

    const v1, 0x7f0d00f4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mSeekBar:Landroid/widget/SeekBar;

    .line 316
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mSeekBar:Landroid/widget/SeekBar;

    if-eqz v0, :cond_0

    .line 317
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mSeekBar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerView;->mSeekListener:Lcom/samsung/musicplus/player/PlayerView$CustomSeekBarListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 318
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mSeekBar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerView;->mSeekListener:Lcom/samsung/musicplus/player/PlayerView$CustomSeekBarListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 319
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mSeekBar:Landroid/widget/SeekBar;

    new-instance v1, Lcom/samsung/musicplus/player/PlayerView$2;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/player/PlayerView$2;-><init>(Lcom/samsung/musicplus/player/PlayerView;)V

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 328
    :cond_0
    new-instance v0, Lcom/samsung/musicplus/widget/progress/MusicProgressBarScrubbing;

    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerView;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/musicplus/player/PlayerView;->mParentView:Landroid/view/View;

    invoke-direct {v0, v1, v2}, Lcom/samsung/musicplus/widget/progress/MusicProgressBarScrubbing;-><init>(Landroid/content/Context;Landroid/view/View;)V

    iput-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mScrubbingBar:Lcom/samsung/musicplus/widget/progress/MusicProgressBarScrubbing;

    .line 330
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mParentView:Landroid/view/View;

    const v1, 0x7f0d00f5

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mCurrentTime:Landroid/widget/TextView;

    .line 331
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mParentView:Landroid/view/View;

    const v1, 0x7f0d00cb

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mTotalTime:Landroid/widget/TextView;

    .line 333
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mParentView:Landroid/view/View;

    const v1, 0x7f0d0116

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mListButton:Landroid/widget/ImageView;

    .line 334
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mListButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 335
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mListButton:Landroid/widget/ImageView;

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/player/PlayerView;->setOnClickListener(Landroid/view/View;)V

    .line 338
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mParentView:Landroid/view/View;

    const v1, 0x7f0d0118

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mThumbnailImageView:Landroid/widget/ImageView;

    .line 339
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mParentView:Landroid/view/View;

    const v1, 0x7f0d0117

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mThumbnailSelectorLayout:Landroid/view/View;

    .line 340
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mParentView:Landroid/view/View;

    const v1, 0x7f0d0119

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mThumbnailButton:Landroid/view/View;

    .line 341
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mThumbnailButton:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 342
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mThumbnailButton:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/player/PlayerView;->setOnClickListener(Landroid/view/View;)V

    .line 345
    :cond_2
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mParentView:Landroid/view/View;

    const v1, 0x7f0d00f7

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mShuffleButton:Landroid/widget/ImageView;

    .line 346
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mShuffleButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_3

    .line 347
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mShuffleButton:Landroid/widget/ImageView;

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/player/PlayerView;->setOnClickListener(Landroid/view/View;)V

    .line 350
    :cond_3
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mParentView:Landroid/view/View;

    const v1, 0x7f0d00f8

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mRepeatButton:Landroid/widget/ImageView;

    .line 351
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mRepeatButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_4

    .line 352
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mRepeatButton:Landroid/widget/ImageView;

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/player/PlayerView;->setOnClickListener(Landroid/view/View;)V

    .line 355
    :cond_4
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mContext:Landroid/content/Context;

    const v1, 0x7f050009

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mNowPlayingListDownOut:Landroid/view/animation/Animation;

    .line 357
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mContext:Landroid/content/Context;

    const v1, 0x7f05000a

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mNowPlayingListUpIn:Landroid/view/animation/Animation;

    .line 358
    return-void
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 527
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 500
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mPause:Z

    .line 501
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/PlayerView;->clearHoverViewState()V

    .line 502
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 509
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mPause:Z

    .line 510
    return-void
.end method

.method public onStart()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 483
    iput-boolean v2, p0, Lcom/samsung/musicplus/player/PlayerView;->mStop:Z

    .line 484
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/PlayerView;->refreshNow()J

    move-result-wide v0

    .line 485
    .local v0, "next":J
    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/musicplus/player/PlayerView;->queueNextRefresh(JZ)V

    .line 487
    iget-object v2, p0, Lcom/samsung/musicplus/player/PlayerView;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/samsung/musicplus/util/UiUtils;->checkHoverUI(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 488
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/PlayerView;->updateHoverView()V

    .line 491
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/PlayerView;->updateShuffleIcon()V

    .line 492
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/PlayerView;->updateRepeatIcon()V

    .line 493
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 517
    iput-boolean v1, p0, Lcom/samsung/musicplus/player/PlayerView;->mStop:Z

    .line 518
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/player/PlayerView;->setTitleScroll(Z)V

    .line 519
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mViewUpdateHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 520
    return-void
.end method

.method public refreshNow()J
    .locals 18

    .prologue
    .line 931
    sget-object v11, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-nez v11, :cond_1

    .line 932
    const-wide/16 v6, 0x1f4

    .line 1005
    :cond_0
    :goto_0
    return-wide v6

    .line 935
    :cond_1
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getBuffering()I

    move-result v2

    .line 936
    .local v2, "buffering":I
    if-ltz v2, :cond_2

    .line 940
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/musicplus/player/PlayerView;->mSeekBar:Landroid/widget/SeekBar;

    mul-int/lit16 v12, v2, 0x3e8

    div-int/lit8 v12, v12, 0x64

    invoke-virtual {v11, v12}, Landroid/widget/SeekBar;->setSecondaryProgress(I)V

    .line 943
    :cond_2
    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/samsung/musicplus/player/PlayerView;->mPosOverride:J

    const-wide/16 v14, 0x0

    cmp-long v11, v12, v14

    if-gez v11, :cond_4

    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getPosition()J

    move-result-wide v4

    .line 944
    .local v4, "pos":J
    :goto_1
    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/samsung/musicplus/player/PlayerView;->mDuration:J

    const-wide/16 v14, 0x0

    cmp-long v11, v12, v14

    if-gtz v11, :cond_3

    .line 945
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/player/PlayerView;->updateTotalTime()Z

    .line 954
    :cond_3
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/musicplus/player/PlayerView;->mParentView:Landroid/view/View;

    const v12, 0x7f0d0108

    invoke-virtual {v11, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/samsung/musicplus/player/PlayerView;->requestForceLayout(Landroid/view/View;)V

    .line 955
    const-wide/16 v12, 0x0

    cmp-long v11, v4, v12

    if-ltz v11, :cond_5

    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/samsung/musicplus/player/PlayerView;->mDuration:J

    const-wide/16 v14, 0x0

    cmp-long v11, v12, v14

    if-lez v11, :cond_5

    .line 956
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/musicplus/player/PlayerView;->mCurrentTime:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/musicplus/player/PlayerView;->mContext:Landroid/content/Context;

    const-wide/16 v14, 0x3e8

    div-long v14, v4, v14

    invoke-static {v12, v14, v15}, Lcom/samsung/musicplus/util/UiUtils;->makeTimeString(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 957
    const-wide/16 v12, 0x3e8

    mul-long/2addr v12, v4

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/samsung/musicplus/player/PlayerView;->mDuration:J

    div-long/2addr v12, v14

    long-to-int v3, v12

    .line 958
    .local v3, "progress":I
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/musicplus/player/PlayerView;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v11, v3}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 965
    invoke-direct/range {p0 .. p0}, Lcom/samsung/musicplus/player/PlayerView;->setCurrentTimeTalkback()V

    .line 967
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->isPlaying()Z

    move-result v11

    if-nez v11, :cond_6

    .line 968
    const-wide/16 v6, 0x1f4

    goto :goto_0

    .line 943
    .end local v3    # "progress":I
    .end local v4    # "pos":J
    :cond_4
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/samsung/musicplus/player/PlayerView;->mPosOverride:J

    goto :goto_1

    .line 972
    .restart local v4    # "pos":J
    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/player/PlayerView;->updateTotalTime()Z

    move-result v11

    if-nez v11, :cond_6

    .line 977
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/musicplus/player/PlayerView;->mCurrentTime:Landroid/widget/TextView;

    const-wide/16 v14, 0x0

    cmp-long v11, v4, v14

    if-lez v11, :cond_8

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/musicplus/player/PlayerView;->mContext:Landroid/content/Context;

    const-wide/16 v14, 0x3e8

    div-long v14, v4, v14

    invoke-static {v11, v14, v15}, Lcom/samsung/musicplus/util/UiUtils;->makeTimeString(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v11

    :goto_2
    invoke-virtual {v12, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 981
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/musicplus/player/PlayerView;->mSeekBar:Landroid/widget/SeekBar;

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/samsung/musicplus/player/PlayerView;->mDuration:J

    const-wide/16 v16, 0x0

    cmp-long v11, v14, v16

    if-lez v11, :cond_9

    const/16 v11, 0x3e8

    :goto_3
    invoke-virtual {v12, v11}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 985
    :cond_6
    invoke-direct/range {p0 .. p0}, Lcom/samsung/musicplus/player/PlayerView;->setCurrentTimeTalkback()V

    .line 989
    const-wide/16 v12, 0x3e8

    const-wide/16 v14, 0x3e8

    rem-long v14, v4, v14

    sub-long v6, v12, v14

    .line 993
    .local v6, "remaining":J
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/musicplus/player/PlayerView;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v11}, Landroid/widget/SeekBar;->getWidth()I

    move-result v10

    .line 994
    .local v10, "width":I
    if-nez v10, :cond_7

    .line 995
    const/16 v10, 0x140

    .line 998
    :cond_7
    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/samsung/musicplus/player/PlayerView;->mDuration:J

    int-to-long v14, v10

    div-long v8, v12, v14

    .line 999
    .local v8, "smoothrefreshtime":J
    cmp-long v11, v8, v6

    if-gtz v11, :cond_0

    .line 1002
    const-wide/16 v12, 0x14

    cmp-long v11, v8, v12

    if-gez v11, :cond_a

    .line 1003
    const-wide/16 v6, 0x14

    goto/16 :goto_0

    .line 977
    .end local v6    # "remaining":J
    .end local v8    # "smoothrefreshtime":J
    .end local v10    # "width":I
    :cond_8
    const-string v11, "--:--"

    goto :goto_2

    .line 981
    :cond_9
    const/4 v11, 0x0

    goto :goto_3

    .restart local v6    # "remaining":J
    .restart local v8    # "smoothrefreshtime":J
    .restart local v10    # "width":I
    :cond_a
    move-wide v6, v8

    .line 1005
    goto/16 :goto_0
.end method

.method protected requestForceLayout(Landroid/view/View;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 220
    if-eqz p1, :cond_0

    .line 221
    invoke-virtual {p1}, Landroid/view/View;->forceLayout()V

    .line 223
    :cond_0
    return-void
.end method

.method protected requestShowLyricView()V
    .locals 0

    .prologue
    .line 558
    return-void
.end method

.method protected setAirView(Landroid/view/View;I)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;
    .param p2, "gravity"    # I

    .prologue
    .line 1704
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/musicplus/util/UiUtils;->checkHoverUI(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1705
    invoke-static {p1, p2}, Lcom/samsung/musicplus/library/hardware/AirView;->setView(Landroid/view/View;I)V

    .line 1707
    :cond_0
    return-void
.end method

.method protected setAirView(Landroid/view/View;Lcom/samsung/musicplus/library/hardware/AirView$OnAirViewPopupListener;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;
    .param p2, "listener"    # Lcom/samsung/musicplus/library/hardware/AirView$OnAirViewPopupListener;

    .prologue
    .line 1710
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/musicplus/util/UiUtils;->checkHoverUI(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1711
    const/4 v0, 0x0

    invoke-static {p1, p2, v0}, Lcom/samsung/musicplus/library/hardware/AirView;->setView(Landroid/view/View;Lcom/samsung/musicplus/library/hardware/AirView$OnAirViewPopupListener;Z)V

    .line 1713
    :cond_0
    return-void
.end method

.method public setAlbumtViewVisibility(Z)V
    .locals 0
    .param p1, "showing"    # Z

    .prologue
    .line 1909
    iput-boolean p1, p0, Lcom/samsung/musicplus/player/PlayerView;->mAlbumViewShowing:Z

    .line 1910
    return-void
.end method

.method protected setBackground(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "bm"    # Landroid/graphics/Bitmap;

    .prologue
    .line 262
    return-void
.end method

.method protected setHoverFocusImage(Landroid/view/View;I)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # I

    .prologue
    const/16 v2, 0xa

    const/16 v1, 0x9

    .line 1667
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 1701
    .end local p1    # "v":Landroid/view/View;
    :cond_0
    :goto_0
    return-void

    .line 1669
    .restart local p1    # "v":Landroid/view/View;
    :sswitch_0
    if-ne p2, v1, :cond_1

    move-object v0, p1

    .line 1670
    check-cast v0, Landroid/widget/ImageView;

    const v1, 0x7f020071

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1671
    sget-boolean v0, Lcom/samsung/musicplus/util/Version;->LIBRARY_API_1:Z

    if-eqz v0, :cond_0

    .line 1672
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mContext:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/samsung/musicplus/library/hardware/AirView;->performHapticFeedback(Landroid/content/Context;Landroid/view/View;)V

    goto :goto_0

    .line 1674
    :cond_1
    if-ne p2, v2, :cond_0

    .line 1675
    check-cast p1, Landroid/widget/ImageView;

    .end local p1    # "v":Landroid/view/View;
    const v0, 0x7f02006f

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 1679
    .restart local p1    # "v":Landroid/view/View;
    :sswitch_1
    if-ne p2, v1, :cond_2

    move-object v0, p1

    .line 1680
    check-cast v0, Landroid/widget/ImageView;

    const v1, 0x7f020068

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1681
    sget-boolean v0, Lcom/samsung/musicplus/util/Version;->LIBRARY_API_1:Z

    if-eqz v0, :cond_0

    .line 1682
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mContext:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/samsung/musicplus/library/hardware/AirView;->performHapticFeedback(Landroid/content/Context;Landroid/view/View;)V

    goto :goto_0

    .line 1684
    :cond_2
    if-ne p2, v2, :cond_0

    .line 1685
    check-cast p1, Landroid/widget/ImageView;

    .end local p1    # "v":Landroid/view/View;
    const v0, 0x7f02006a

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 1689
    .restart local p1    # "v":Landroid/view/View;
    :sswitch_2
    if-ne p2, v1, :cond_3

    move-object v0, p1

    .line 1690
    check-cast v0, Landroid/widget/ImageView;

    const v1, 0x7f02007c

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1691
    sget-boolean v0, Lcom/samsung/musicplus/util/Version;->LIBRARY_API_1:Z

    if-eqz v0, :cond_0

    .line 1692
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mContext:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/samsung/musicplus/library/hardware/AirView;->performHapticFeedback(Landroid/content/Context;Landroid/view/View;)V

    goto :goto_0

    .line 1694
    :cond_3
    if-ne p2, v2, :cond_0

    .line 1695
    check-cast p1, Landroid/widget/ImageView;

    .end local p1    # "v":Landroid/view/View;
    const v0, 0x7f020069

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 1667
    :sswitch_data_0
    .sparse-switch
        0x7f0d00f9 -> :sswitch_0
        0x7f0d00fc -> :sswitch_1
        0x7f0d0116 -> :sswitch_2
    .end sparse-switch
.end method

.method protected setNextPrevListener(Landroid/view/View;Landroid/view/View;)V
    .locals 3
    .param p1, "prev"    # Landroid/view/View;
    .param p2, "next"    # Landroid/view/View;

    .prologue
    .line 422
    new-instance v0, Lcom/samsung/musicplus/widget/ConvertTouchEventListener;

    invoke-direct {v0}, Lcom/samsung/musicplus/widget/ConvertTouchEventListener;-><init>()V

    .line 423
    .local v0, "key":Lcom/samsung/musicplus/widget/ConvertTouchEventListener;
    new-instance v1, Lcom/samsung/musicplus/widget/control/ForwardRewindListener;

    iget-object v2, p0, Lcom/samsung/musicplus/player/PlayerView;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/samsung/musicplus/widget/control/ForwardRewindListener;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/musicplus/player/PlayerView;->mFfRewTouchListener:Lcom/samsung/musicplus/widget/control/ForwardRewindListener;

    .line 424
    invoke-virtual {p1, v0}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 425
    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerView;->mFfRewTouchListener:Lcom/samsung/musicplus/widget/control/ForwardRewindListener;

    invoke-virtual {p1, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 426
    invoke-virtual {p2, v0}, Landroid/view/View;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 427
    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerView;->mFfRewTouchListener:Lcom/samsung/musicplus/widget/control/ForwardRewindListener;

    invoke-virtual {p2, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 428
    return-void
.end method

.method protected setPlayButtonImage(I)V
    .locals 1
    .param p1, "resId"    # I

    .prologue
    .line 397
    iput p1, p0, Lcom/samsung/musicplus/player/PlayerView;->mPlayImageResource:I

    .line 398
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mPlay:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 399
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mPlay:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 401
    :cond_0
    return-void
.end method

.method protected setPlayButtonImageResource(Landroid/widget/ImageView;I)V
    .locals 1
    .param p1, "playBtn"    # Landroid/widget/ImageView;
    .param p2, "imageResource"    # I

    .prologue
    .line 389
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mPlay:Landroid/widget/ImageView;

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 391
    return-void
.end method

.method protected setPlayButtonPausedState(Landroid/widget/ImageView;)V
    .locals 1
    .param p1, "playBtn"    # Landroid/widget/ImageView;

    .prologue
    .line 1309
    new-instance v0, Lcom/samsung/musicplus/player/PlayerView$5;

    invoke-direct {v0, p0, p1}, Lcom/samsung/musicplus/player/PlayerView$5;-><init>(Lcom/samsung/musicplus/player/PlayerView;Landroid/widget/ImageView;)V

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->post(Ljava/lang/Runnable;)Z

    .line 1317
    return-void
.end method

.method protected setPlayButtonPlayingState(Landroid/widget/ImageView;)V
    .locals 1
    .param p1, "playBtn"    # Landroid/widget/ImageView;

    .prologue
    .line 1324
    new-instance v0, Lcom/samsung/musicplus/player/PlayerView$6;

    invoke-direct {v0, p0, p1}, Lcom/samsung/musicplus/player/PlayerView$6;-><init>(Lcom/samsung/musicplus/player/PlayerView;Landroid/widget/ImageView;)V

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->post(Ljava/lang/Runnable;)Z

    .line 1332
    return-void
.end method

.method protected setRepeatResources(III)V
    .locals 0
    .param p1, "resOne"    # I
    .param p2, "resOff"    # I
    .param p3, "resAll"    # I

    .prologue
    .line 775
    iput p1, p0, Lcom/samsung/musicplus/player/PlayerView;->mRepeatResOne:I

    .line 776
    iput p2, p0, Lcom/samsung/musicplus/player/PlayerView;->mRepeatResOff:I

    .line 777
    iput p3, p0, Lcom/samsung/musicplus/player/PlayerView;->mRepeatResAll:I

    .line 778
    return-void
.end method

.method protected setSeekBar(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1, "v"    # Landroid/widget/SeekBar;

    .prologue
    .line 1846
    iput-object p1, p0, Lcom/samsung/musicplus/player/PlayerView;->mSeekBar:Landroid/widget/SeekBar;

    .line 1847
    return-void
.end method

.method protected setShuffleResources(II)V
    .locals 0
    .param p1, "resOn"    # I
    .param p2, "resOff"    # I

    .prologue
    .line 770
    iput p1, p0, Lcom/samsung/musicplus/player/PlayerView;->mShuffleResOn:I

    .line 771
    iput p2, p0, Lcom/samsung/musicplus/player/PlayerView;->mShuffleResOff:I

    .line 772
    return-void
.end method

.method protected final setTheme(I)V
    .locals 0
    .param p1, "theme"    # I

    .prologue
    .line 409
    iput p1, p0, Lcom/samsung/musicplus/player/PlayerView;->mTheme:I

    .line 410
    return-void
.end method

.method protected setThumbNailSize(I)V
    .locals 0
    .param p1, "size"    # I

    .prologue
    .line 1249
    iput p1, p0, Lcom/samsung/musicplus/player/PlayerView;->mThumbSize:I

    .line 1250
    return-void
.end method

.method protected setTitleScroll(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 1168
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mTitle:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 1169
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setSelected(Z)V

    .line 1171
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mArtist:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 1172
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mArtist:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setSelected(Z)V

    .line 1174
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mAlbum:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    .line 1175
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mAlbum:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setSelected(Z)V

    .line 1177
    :cond_2
    return-void
.end method

.method protected setVisibleIfLyricOn()V
    .locals 0

    .prologue
    .line 603
    return-void
.end method

.method public showPrepareLoading(Z)V
    .locals 3
    .param p1, "isLocalSong"    # Z

    .prologue
    const/4 v2, 0x0

    .line 1072
    const-string v0, "MusicUi"

    const-string v1, "showPrepareLoading"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1073
    iput-boolean v2, p0, Lcom/samsung/musicplus/player/PlayerView;->mIsAlbumLoaded:Z

    .line 1074
    iput-boolean v2, p0, Lcom/samsung/musicplus/player/PlayerView;->mIsPrepared:Z

    .line 1075
    if-nez p1, :cond_0

    .line 1077
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mParentView:Landroid/view/View;

    const v1, 0x7f0d0056

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1079
    :cond_0
    return-void
.end method

.method protected toggleLyricView()Z
    .locals 1

    .prologue
    .line 590
    const/4 v0, 0x1

    return v0
.end method

.method protected togglePlayerView()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 531
    invoke-direct {p0}, Lcom/samsung/musicplus/player/PlayerView;->ensureList()V

    .line 533
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mListContainer:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mListContainer:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 550
    :cond_0
    :goto_0
    return-void

    .line 538
    :cond_1
    iget-boolean v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mAlbumViewShowing:Z

    if-eqz v0, :cond_3

    .line 539
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/PlayerView;->isLyricViewShowing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 540
    iput-boolean v1, p0, Lcom/samsung/musicplus/player/PlayerView;->mKeepLyricShowingState:Z

    .line 541
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/PlayerView;->toggleLyricView()Z

    .line 543
    :cond_2
    invoke-direct {p0}, Lcom/samsung/musicplus/player/PlayerView;->prepareListView()V

    .line 544
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mNowPlayingListUpIn:Landroid/view/animation/Animation;

    invoke-direct {p0, v0, v1}, Lcom/samsung/musicplus/player/PlayerView;->startAnimation(Landroid/view/animation/Animation;Z)V

    goto :goto_0

    .line 546
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/PlayerView;->requestShowLyricView()V

    .line 547
    invoke-direct {p0}, Lcom/samsung/musicplus/player/PlayerView;->prepareAlbumView()V

    .line 548
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mNowPlayingListDownOut:Landroid/view/animation/Animation;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/samsung/musicplus/player/PlayerView;->startAnimation(Landroid/view/animation/Animation;Z)V

    goto :goto_0
.end method

.method protected togglePlayerViewWithoutAnimation()V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 606
    invoke-direct {p0}, Lcom/samsung/musicplus/player/PlayerView;->ensureList()V

    .line 608
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mAlbumView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 609
    invoke-direct {p0}, Lcom/samsung/musicplus/player/PlayerView;->prepareCommonView()V

    .line 610
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mAlbumView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 611
    iput-boolean v1, p0, Lcom/samsung/musicplus/player/PlayerView;->mAlbumViewShowing:Z

    .line 612
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mListContainer:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 619
    :goto_0
    return-void

    .line 614
    :cond_0
    invoke-direct {p0}, Lcom/samsung/musicplus/player/PlayerView;->prepareCommonView()V

    .line 615
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mAlbumView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 616
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mAlbumViewShowing:Z

    .line 617
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mListContainer:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public toggleRepeat()V
    .locals 0

    .prologue
    .line 765
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->toggleRepeat()V

    .line 766
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/PlayerView;->updateRepeatIcon()V

    .line 767
    return-void
.end method

.method public toggleShuffle()V
    .locals 0

    .prologue
    .line 760
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->toggleShuffle()V

    .line 761
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/PlayerView;->updateShuffleIcon()V

    .line 762
    return-void
.end method

.method protected updateAlbumArt(I)V
    .locals 1
    .param p1, "listType"    # I

    .prologue
    .line 1183
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mAlbumArt:Landroid/widget/ImageView;

    invoke-virtual {p0, v0, p1}, Lcom/samsung/musicplus/player/PlayerView;->updateAlbumArt(Landroid/widget/ImageView;I)V

    .line 1184
    return-void
.end method

.method protected updateAlbumArt(Landroid/widget/ImageView;I)V
    .locals 11
    .param p1, "iv"    # Landroid/widget/ImageView;
    .param p2, "listType"    # I

    .prologue
    const/4 v2, 0x0

    .line 1193
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getAlbumId()J

    move-result-wide v6

    .line 1195
    .local v6, "artId":J
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iget v1, p0, Lcom/samsung/musicplus/player/PlayerView;->mThumbSize:I

    invoke-static {p2, v0, v1}, Lcom/samsung/musicplus/util/AlbumArtUtils;->getArtCacheKey(ILjava/lang/Object;I)Ljava/lang/String;

    move-result-object v10

    .line 1196
    .local v10, "key":Ljava/lang/String;
    invoke-static {v10}, Lcom/samsung/musicplus/util/AlbumArtUtils;->getSingleArtCache(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    .line 1198
    .local v9, "d":Landroid/graphics/drawable/Drawable;
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mViewUpdateHandler:Landroid/os/Handler;

    const/16 v1, 0xc8

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 1199
    if-eqz v9, :cond_1

    .line 1200
    invoke-virtual {p1, v9}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1201
    iget-boolean v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mIsPrepared:Z

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/samsung/musicplus/player/PlayerView;->updatePrepareLoading(ZZ)V

    .line 1203
    instance-of v0, v9, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v0, :cond_0

    .line 1204
    check-cast v9, Landroid/graphics/drawable/BitmapDrawable;

    .end local v9    # "d":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v9}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v8

    .line 1205
    .local v8, "bitmap":Landroid/graphics/Bitmap;
    invoke-virtual {p0, v8}, Lcom/samsung/musicplus/player/PlayerView;->setBackground(Landroid/graphics/Bitmap;)V

    .line 1207
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mThumbnailImageView:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mThumbnailSelectorLayout:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mThumbnailSelectorLayout:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 1209
    invoke-virtual {p0, v8}, Lcom/samsung/musicplus/player/PlayerView;->updateButtonThumbnailImage(Landroid/graphics/Bitmap;)V

    .line 1225
    .end local v8    # "bitmap":Landroid/graphics/Bitmap;
    :cond_0
    :goto_0
    return-void

    .line 1218
    .restart local v9    # "d":Landroid/graphics/drawable/Drawable;
    :cond_1
    invoke-virtual {p0, v2}, Lcom/samsung/musicplus/player/PlayerView;->setBackground(Landroid/graphics/Bitmap;)V

    .line 1219
    invoke-virtual {p1, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1220
    const v0, 0x7f020039

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 1221
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 1223
    invoke-static {}, Lcom/samsung/musicplus/util/AlbumArtLoader;->getAlbumArtLoader()Lcom/samsung/musicplus/util/IAlbumArtLoader;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerView;->mContext:Landroid/content/Context;

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iget v4, p0, Lcom/samsung/musicplus/player/PlayerView;->mThumbSize:I

    iget-object v5, p0, Lcom/samsung/musicplus/player/PlayerView;->mViewUpdateHandler:Landroid/os/Handler;

    move v2, p2

    invoke-interface/range {v0 .. v5}, Lcom/samsung/musicplus/util/IAlbumArtLoader;->loadArtwork(Landroid/content/Context;ILjava/lang/Object;ILandroid/os/Handler;)V

    goto :goto_0
.end method

.method public updateAlbumLoading(Z)V
    .locals 1
    .param p1, "albumParsed"    # Z

    .prologue
    .line 1038
    iget-boolean v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mIsPrepared:Z

    invoke-virtual {p0, v0, p1}, Lcom/samsung/musicplus/player/PlayerView;->updatePrepareLoading(ZZ)V

    .line 1039
    return-void
.end method

.method protected updateButtonThumbnailImage(Landroid/graphics/Bitmap;)V
    .locals 4
    .param p1, "source"    # Landroid/graphics/Bitmap;

    .prologue
    .line 698
    const-string v2, "MusicUi"

    const-string v3, "UpdateButtonThumbnailImage start!"

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 700
    iget-object v2, p0, Lcom/samsung/musicplus/player/PlayerView;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c003b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 702
    .local v1, "thumbnailSize":I
    if-nez p1, :cond_0

    .line 703
    iget-object v2, p0, Lcom/samsung/musicplus/player/PlayerView;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f02003b

    invoke-static {v2, v3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object p1

    .line 707
    :cond_0
    const/4 v2, 0x1

    invoke-static {p1, v1, v1, v2}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 725
    .local v0, "bm":Landroid/graphics/Bitmap;
    iget-object v2, p0, Lcom/samsung/musicplus/player/PlayerView;->mThumbnailImageView:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 726
    return-void
.end method

.method public updateControllerView(I)V
    .locals 2
    .param p1, "listType"    # I

    .prologue
    const/4 v1, 0x0

    .line 1261
    iget v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mCurrentListType:I

    if-eq v0, p1, :cond_0

    .line 1264
    iput p1, p0, Lcom/samsung/musicplus/player/PlayerView;->mCurrentListType:I

    .line 1267
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mPrev:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1268
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mNext:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1270
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mListButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mAlbumViewShowing:Z

    if-eqz v0, :cond_3

    .line 1271
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mListButton:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1276
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mSeekBar:Landroid/widget/SeekBar;

    if-eqz v0, :cond_2

    .line 1277
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mSeekBar:Landroid/widget/SeekBar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setEnabled(Z)V

    .line 1278
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getThumb()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    const/16 v1, 0xff

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 1279
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mSeekBar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerView;->mSeekBarAirListener:Lcom/samsung/musicplus/library/hardware/AirView$OnAirSeekBarPopupListener;

    invoke-direct {p0, v0, v1}, Lcom/samsung/musicplus/player/PlayerView;->setAirViewSeekBar(Landroid/widget/SeekBar;Lcom/samsung/musicplus/library/hardware/AirView$OnAirSeekBarPopupListener;)V

    .line 1281
    :cond_2
    return-void

    .line 1272
    :cond_3
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mThumbnailImageView:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mAlbumViewShowing:Z

    if-nez v0, :cond_1

    .line 1273
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mThumbnailImageView:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method protected updateHoverView()V
    .locals 4

    .prologue
    const/16 v3, 0x3031

    const/4 v2, 0x0

    .line 1339
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mPlay:Landroid/widget/ImageView;

    invoke-virtual {p0, v0, v3}, Lcom/samsung/musicplus/player/PlayerView;->setAirView(Landroid/view/View;I)V

    .line 1340
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mShuffleButton:Landroid/widget/ImageView;

    invoke-virtual {p0, v0, v3}, Lcom/samsung/musicplus/player/PlayerView;->setAirView(Landroid/view/View;I)V

    .line 1341
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mRepeatButton:Landroid/widget/ImageView;

    invoke-virtual {p0, v0, v3}, Lcom/samsung/musicplus/player/PlayerView;->setAirView(Landroid/view/View;I)V

    .line 1343
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/musicplus/library/hardware/AirView;->isSimpleAirView(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1344
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mPrev:Landroid/view/View;

    invoke-virtual {p0, v0, v3}, Lcom/samsung/musicplus/player/PlayerView;->setAirView(Landroid/view/View;I)V

    .line 1345
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mNext:Landroid/view/View;

    invoke-virtual {p0, v0, v3}, Lcom/samsung/musicplus/player/PlayerView;->setAirView(Landroid/view/View;I)V

    .line 1346
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mListButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 1347
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mListButton:Landroid/widget/ImageView;

    const/16 v1, 0x3133

    invoke-virtual {p0, v0, v1}, Lcom/samsung/musicplus/player/PlayerView;->setAirView(Landroid/view/View;I)V

    .line 1352
    :cond_0
    sget-boolean v0, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_SUPPORT_FINGER_AIR_VIEW:Z

    if-eqz v0, :cond_2

    .line 1353
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mNext:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerView;->mHoverListner:Lcom/samsung/musicplus/player/PlayerView$HoverListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 1354
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mPrev:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerView;->mHoverListner:Lcom/samsung/musicplus/player/PlayerView$HoverListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 1355
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mListButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 1356
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mListButton:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerView;->mHoverListner:Lcom/samsung/musicplus/player/PlayerView$HoverListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 1358
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mThumbnailButton:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 1359
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mThumbnailButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerView;->mHoverListner:Lcom/samsung/musicplus/player/PlayerView$HoverListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 1363
    :cond_2
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mNext:Landroid/view/View;

    invoke-direct {p0, v0, p0, v2, v2}, Lcom/samsung/musicplus/player/PlayerView;->setAirView(Landroid/view/View;Lcom/samsung/musicplus/library/hardware/AirView$OnAirViewPopupListener;IZ)V

    .line 1364
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mPrev:Landroid/view/View;

    invoke-direct {p0, v0, p0, v2, v2}, Lcom/samsung/musicplus/player/PlayerView;->setAirView(Landroid/view/View;Lcom/samsung/musicplus/library/hardware/AirView$OnAirViewPopupListener;IZ)V

    .line 1366
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mNext:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHoverPopup()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/HoverPopupWindow;->setPopupGravity(I)V

    .line 1368
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mPrev:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getHoverPopup()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/HoverPopupWindow;->setPopupGravity(I)V

    .line 1371
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mListButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_3

    .line 1372
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mListButton:Landroid/widget/ImageView;

    invoke-direct {p0, v0, p0, v2, v2}, Lcom/samsung/musicplus/player/PlayerView;->setAirView(Landroid/view/View;Lcom/samsung/musicplus/library/hardware/AirView$OnAirViewPopupListener;IZ)V

    .line 1375
    :cond_3
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mThumbnailButton:Landroid/view/View;

    if-eqz v0, :cond_4

    .line 1376
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mThumbnailButton:Landroid/view/View;

    invoke-direct {p0, v0, p0, v2, v2}, Lcom/samsung/musicplus/player/PlayerView;->setAirView(Landroid/view/View;Lcom/samsung/musicplus/library/hardware/AirView$OnAirViewPopupListener;IZ)V

    .line 1378
    :cond_4
    return-void
.end method

.method public updateNowPlayingList(Landroid/content/Intent;)V
    .locals 4
    .param p1, "i"    # Landroid/content/Intent;

    .prologue
    .line 750
    iget-object v2, p0, Lcom/samsung/musicplus/player/PlayerView;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    .line 752
    .local v1, "manager":Landroid/app/FragmentManager;
    const-string v2, "now_playing_list"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 753
    .local v0, "fg":Landroid/app/Fragment;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Fragment;->isResumed()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 754
    const-string v2, "MusicUi"

    const-string v3, "now playing view update!"

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 755
    check-cast v0, Lcom/samsung/musicplus/player/NowPlayingListFragment;

    .end local v0    # "fg":Landroid/app/Fragment;
    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/player/NowPlayingListFragment;->updateNowPlayingTitle(Landroid/content/Intent;)V

    .line 757
    :cond_0
    return-void
.end method

.method public updateNowPlayingListView()V
    .locals 3

    .prologue
    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 734
    iget-boolean v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mAlbumViewShowing:Z

    if-eqz v0, :cond_1

    .line 735
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mAlbumView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 736
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mListContainer:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 737
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mListContainer:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 744
    :cond_0
    :goto_0
    return-void

    .line 740
    :cond_1
    invoke-direct {p0}, Lcom/samsung/musicplus/player/PlayerView;->ensureList()V

    .line 741
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mAlbumView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 742
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mListContainer:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public updatePlayState(Z)V
    .locals 3
    .param p1, "isPlaying"    # Z

    .prologue
    .line 1288
    if-eqz p1, :cond_1

    .line 1289
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mPlay:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/player/PlayerView;->setPlayButtonPlayingState(Landroid/widget/ImageView;)V

    .line 1295
    :goto_0
    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerView;->mSeekBar:Landroid/widget/SeekBar;

    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->isDmrPlaying()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, p1

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setEnabled(Z)V

    .line 1296
    if-eqz p1, :cond_0

    .line 1297
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mSeekBar:Landroid/widget/SeekBar;

    const/16 v1, 0x80

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/widget/SeekBar;->performAccessibilityAction(ILandroid/os/Bundle;)Z

    .line 1300
    :cond_0
    return-void

    .line 1291
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mPlay:Landroid/widget/ImageView;

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/player/PlayerView;->setPlayButtonPausedState(Landroid/widget/ImageView;)V

    goto :goto_0

    .line 1295
    :cond_2
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public updatePrepareLoading(Z)V
    .locals 1
    .param p1, "prepared"    # Z

    .prologue
    .line 1027
    iget-boolean v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mIsAlbumLoaded:Z

    invoke-virtual {p0, p1, v0}, Lcom/samsung/musicplus/player/PlayerView;->updatePrepareLoading(ZZ)V

    .line 1028
    return-void
.end method

.method public updatePrepareLoading(ZZ)V
    .locals 4
    .param p1, "prepared"    # Z
    .param p2, "albumParsed"    # Z

    .prologue
    .line 1050
    iput-boolean p1, p0, Lcom/samsung/musicplus/player/PlayerView;->mIsPrepared:Z

    .line 1051
    iput-boolean p2, p0, Lcom/samsung/musicplus/player/PlayerView;->mIsAlbumLoaded:Z

    .line 1053
    const-string v1, "MusicUi"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "updatePrepareLoading mIsAlbumLoaded : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/samsung/musicplus/player/PlayerView;->mIsAlbumLoaded:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " mIsPrepared : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/samsung/musicplus/player/PlayerView;->mIsPrepared:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1056
    if-eqz p2, :cond_0

    if-eqz p1, :cond_0

    .line 1057
    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerView;->mParentView:Landroid/view/View;

    const v2, 0x7f0d0056

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1058
    .local v0, "v":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 1059
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1062
    .end local v0    # "v":Landroid/view/View;
    :cond_0
    return-void
.end method

.method public updateRepeatIcon()V
    .locals 3

    .prologue
    .line 795
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mRepeatButton:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    .line 815
    :goto_0
    return-void

    .line 799
    :cond_0
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getRepeat()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 801
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mRepeatButton:Landroid/widget/ImageView;

    iget v1, p0, Lcom/samsung/musicplus/player/PlayerView;->mRepeatResOff:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 802
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mRepeatButton:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerView;->mContext:Landroid/content/Context;

    const v2, 0x7f10019b

    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 805
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mRepeatButton:Landroid/widget/ImageView;

    iget v1, p0, Lcom/samsung/musicplus/player/PlayerView;->mRepeatResOne:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 806
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mRepeatButton:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerView;->mContext:Landroid/content/Context;

    const v2, 0x7f100199

    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 809
    :pswitch_2
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mRepeatButton:Landroid/widget/ImageView;

    iget v1, p0, Lcom/samsung/musicplus/player/PlayerView;->mRepeatResAll:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 810
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mRepeatButton:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerView;->mContext:Landroid/content/Context;

    const v2, 0x7f10019a

    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 799
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public updateShuffleIcon()V
    .locals 3

    .prologue
    .line 781
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mShuffleButton:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    .line 792
    :goto_0
    return-void

    .line 785
    :cond_0
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getShuffle()I

    move-result v0

    if-nez v0, :cond_1

    .line 789
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mShuffleButton:Landroid/widget/ImageView;

    iget v1, p0, Lcom/samsung/musicplus/player/PlayerView;->mShuffleResOff:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 790
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mShuffleButton:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerView;->mContext:Landroid/content/Context;

    const v2, 0x7f10019f

    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 786
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mShuffleButton:Landroid/widget/ImageView;

    iget v1, p0, Lcom/samsung/musicplus/player/PlayerView;->mShuffleResOn:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 787
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mShuffleButton:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerView;->mContext:Landroid/content/Context;

    const v2, 0x7f1001a0

    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public updateSongInfo()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1113
    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/player/PlayerView;->initializeTitleView(Z)V

    .line 1114
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView;->mTextUpdateHandler:Lcom/samsung/musicplus/player/PlayerView$TextUpdateHandler;

    invoke-virtual {v0}, Lcom/samsung/musicplus/player/PlayerView$TextUpdateHandler;->updateText()V

    .line 1115
    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/player/PlayerView;->setTitleScroll(Z)V

    .line 1116
    return-void
.end method

.method public updateTotalTime()Z
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 1098
    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerView;->mTotalTime:Landroid/widget/TextView;

    if-nez v1, :cond_1

    .line 1106
    :cond_0
    :goto_0
    return v0

    .line 1102
    :cond_1
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getDuration()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/samsung/musicplus/player/PlayerView;->mDuration:J

    .line 1103
    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerView;->mTotalTime:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/musicplus/player/PlayerView;->mContext:Landroid/content/Context;

    iget-wide v4, p0, Lcom/samsung/musicplus/player/PlayerView;->mDuration:J

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    invoke-static {v2, v4, v5}, Lcom/samsung/musicplus/util/UiUtils;->makeTimeString(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1104
    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerView;->mTotalTime:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/musicplus/player/PlayerView;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/musicplus/player/PlayerView;->mTotalTime:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/musicplus/util/UiUtils;->getDurationTalkback(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1106
    iget-wide v2, p0, Lcom/samsung/musicplus/player/PlayerView;->mDuration:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected updateTrackAllInfo()V
    .locals 1

    .prologue
    .line 1085
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getListType()I

    move-result v0

    .line 1086
    .local v0, "listType":I
    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/player/PlayerView;->updateControllerView(I)V

    .line 1087
    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/player/PlayerView;->updateAlbumArt(I)V

    .line 1088
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/PlayerView;->updateSongInfo()V

    .line 1089
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/PlayerView;->updateTotalTime()Z

    .line 1090
    return-void
.end method
