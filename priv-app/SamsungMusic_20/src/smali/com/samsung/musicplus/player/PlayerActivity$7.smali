.class Lcom/samsung/musicplus/player/PlayerActivity$7;
.super Landroid/content/BroadcastReceiver;
.source "PlayerActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/player/PlayerActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/player/PlayerActivity;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/player/PlayerActivity;)V
    .locals 0

    .prologue
    .line 1000
    iput-object p1, p0, Lcom/samsung/musicplus/player/PlayerActivity$7;->this$0:Lcom/samsung/musicplus/player/PlayerActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x0

    .line 1003
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 1004
    .local v0, "action":Ljava/lang/String;
    const-string v5, "MusicUi"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "mAudioReceiver : action - "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1005
    const-string v5, "android.media.RINGER_MODE_CHANGED"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 1006
    const-string v5, "android.media.EXTRA_RINGER_MODE"

    invoke-virtual {p2, v5, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 1007
    .local v2, "mode":I
    if-nez v2, :cond_1

    .line 1008
    iget-object v5, p0, Lcom/samsung/musicplus/player/PlayerActivity$7;->this$0:Lcom/samsung/musicplus/player/PlayerActivity;

    # getter for: Lcom/samsung/musicplus/player/PlayerActivity;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;
    invoke-static {v5}, Lcom/samsung/musicplus/player/PlayerActivity;->access$400(Lcom/samsung/musicplus/player/PlayerActivity;)Lcom/samsung/musicplus/library/audio/SecAudioManager;

    move-result-object v5

    invoke-virtual {v5, v9}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->isStreamMute(I)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1009
    iget-object v5, p0, Lcom/samsung/musicplus/player/PlayerActivity$7;->this$0:Lcom/samsung/musicplus/player/PlayerActivity;

    const/4 v6, 0x1

    # setter for: Lcom/samsung/musicplus/player/PlayerActivity;->mIsVolumeMute:Z
    invoke-static {v5, v6}, Lcom/samsung/musicplus/player/PlayerActivity;->access$502(Lcom/samsung/musicplus/player/PlayerActivity;Z)Z

    .line 1010
    iget-object v5, p0, Lcom/samsung/musicplus/player/PlayerActivity$7;->this$0:Lcom/samsung/musicplus/player/PlayerActivity;

    # getter for: Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;
    invoke-static {v5}, Lcom/samsung/musicplus/player/PlayerActivity;->access$000(Lcom/samsung/musicplus/player/PlayerActivity;)Lcom/samsung/musicplus/player/FullPlayerView;

    move-result-object v5

    invoke-virtual {v5, v8}, Lcom/samsung/musicplus/player/FullPlayerView;->updateVolumeBar(I)V

    .line 1048
    .end local v2    # "mode":I
    :cond_0
    :goto_0
    return-void

    .line 1013
    .restart local v2    # "mode":I
    :cond_1
    iget-object v5, p0, Lcom/samsung/musicplus/player/PlayerActivity$7;->this$0:Lcom/samsung/musicplus/player/PlayerActivity;

    # getter for: Lcom/samsung/musicplus/player/PlayerActivity;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;
    invoke-static {v5}, Lcom/samsung/musicplus/player/PlayerActivity;->access$400(Lcom/samsung/musicplus/player/PlayerActivity;)Lcom/samsung/musicplus/library/audio/SecAudioManager;

    move-result-object v5

    invoke-virtual {v5, v9}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->getStreamVolume(I)I

    move-result v4

    .line 1014
    .local v4, "volume":I
    iget-object v5, p0, Lcom/samsung/musicplus/player/PlayerActivity$7;->this$0:Lcom/samsung/musicplus/player/PlayerActivity;

    # getter for: Lcom/samsung/musicplus/player/PlayerActivity;->mIsVolumeMute:Z
    invoke-static {v5}, Lcom/samsung/musicplus/player/PlayerActivity;->access$500(Lcom/samsung/musicplus/player/PlayerActivity;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1015
    iget-object v5, p0, Lcom/samsung/musicplus/player/PlayerActivity$7;->this$0:Lcom/samsung/musicplus/player/PlayerActivity;

    # setter for: Lcom/samsung/musicplus/player/PlayerActivity;->mIsVolumeMute:Z
    invoke-static {v5, v8}, Lcom/samsung/musicplus/player/PlayerActivity;->access$502(Lcom/samsung/musicplus/player/PlayerActivity;Z)Z

    .line 1016
    iget-object v5, p0, Lcom/samsung/musicplus/player/PlayerActivity$7;->this$0:Lcom/samsung/musicplus/player/PlayerActivity;

    # getter for: Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;
    invoke-static {v5}, Lcom/samsung/musicplus/player/PlayerActivity;->access$000(Lcom/samsung/musicplus/player/PlayerActivity;)Lcom/samsung/musicplus/player/FullPlayerView;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/samsung/musicplus/player/FullPlayerView;->updateVolumeBar(I)V

    goto :goto_0

    .line 1018
    :cond_2
    iget-object v5, p0, Lcom/samsung/musicplus/player/PlayerActivity$7;->this$0:Lcom/samsung/musicplus/player/PlayerActivity;

    # getter for: Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;
    invoke-static {v5}, Lcom/samsung/musicplus/player/PlayerActivity;->access$000(Lcom/samsung/musicplus/player/PlayerActivity;)Lcom/samsung/musicplus/player/FullPlayerView;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/samsung/musicplus/player/FullPlayerView;->updateVolumeItem(I)V

    goto :goto_0

    .line 1021
    .end local v2    # "mode":I
    .end local v4    # "volume":I
    :cond_3
    const-string v5, "android.media.VOLUME_CHANGED_ACTION"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 1022
    iget-object v5, p0, Lcom/samsung/musicplus/player/PlayerActivity$7;->this$0:Lcom/samsung/musicplus/player/PlayerActivity;

    # getter for: Lcom/samsung/musicplus/player/PlayerActivity;->mVolumeUpdateBlock:Z
    invoke-static {v5}, Lcom/samsung/musicplus/player/PlayerActivity;->access$200(Lcom/samsung/musicplus/player/PlayerActivity;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1028
    const-string v5, "android.media.EXTRA_VOLUME_STREAM_TYPE"

    invoke-virtual {p2, v5, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 1029
    .local v3, "stream":I
    if-ne v3, v9, :cond_0

    .line 1030
    iget-object v5, p0, Lcom/samsung/musicplus/player/PlayerActivity$7;->this$0:Lcom/samsung/musicplus/player/PlayerActivity;

    # getter for: Lcom/samsung/musicplus/player/PlayerActivity;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;
    invoke-static {v5}, Lcom/samsung/musicplus/player/PlayerActivity;->access$400(Lcom/samsung/musicplus/player/PlayerActivity;)Lcom/samsung/musicplus/library/audio/SecAudioManager;

    move-result-object v5

    invoke-virtual {v5, v9}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->getStreamVolume(I)I

    move-result v4

    .line 1031
    .restart local v4    # "volume":I
    iget-object v5, p0, Lcom/samsung/musicplus/player/PlayerActivity$7;->this$0:Lcom/samsung/musicplus/player/PlayerActivity;

    # getter for: Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;
    invoke-static {v5}, Lcom/samsung/musicplus/player/PlayerActivity;->access$000(Lcom/samsung/musicplus/player/PlayerActivity;)Lcom/samsung/musicplus/player/FullPlayerView;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/samsung/musicplus/player/FullPlayerView;->updateVolumeItem(I)V

    .line 1032
    iget-object v5, p0, Lcom/samsung/musicplus/player/PlayerActivity$7;->this$0:Lcom/samsung/musicplus/player/PlayerActivity;

    # getter for: Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;
    invoke-static {v5}, Lcom/samsung/musicplus/player/PlayerActivity;->access$000(Lcom/samsung/musicplus/player/PlayerActivity;)Lcom/samsung/musicplus/player/FullPlayerView;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/musicplus/player/FullPlayerView;->isVolumeBarShowing()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1035
    iget-object v5, p0, Lcom/samsung/musicplus/player/PlayerActivity$7;->this$0:Lcom/samsung/musicplus/player/PlayerActivity;

    # getter for: Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;
    invoke-static {v5}, Lcom/samsung/musicplus/player/PlayerActivity;->access$000(Lcom/samsung/musicplus/player/PlayerActivity;)Lcom/samsung/musicplus/player/FullPlayerView;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/samsung/musicplus/player/FullPlayerView;->invalidateVolumeBar(I)V

    goto :goto_0

    .line 1038
    .end local v3    # "stream":I
    .end local v4    # "volume":I
    :cond_4
    const-string v5, "com.samsung.musicplus.action.AUDIO_PATH_CHANGED"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1041
    const-string v5, "is_bt"

    invoke-virtual {p2, v5, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 1043
    .local v1, "isBT":Z
    if-nez v1, :cond_0

    .line 1044
    iget-object v5, p0, Lcom/samsung/musicplus/player/PlayerActivity$7;->this$0:Lcom/samsung/musicplus/player/PlayerActivity;

    invoke-virtual {v5}, Lcom/samsung/musicplus/player/PlayerActivity;->closeOptionsMenu()V

    .line 1045
    iget-object v5, p0, Lcom/samsung/musicplus/player/PlayerActivity$7;->this$0:Lcom/samsung/musicplus/player/PlayerActivity;

    # getter for: Lcom/samsung/musicplus/player/PlayerActivity;->mPlayerView:Lcom/samsung/musicplus/player/FullPlayerView;
    invoke-static {v5}, Lcom/samsung/musicplus/player/PlayerActivity;->access$000(Lcom/samsung/musicplus/player/PlayerActivity;)Lcom/samsung/musicplus/player/FullPlayerView;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/musicplus/player/FullPlayerView;->showVolumePanel()V

    goto/16 :goto_0
.end method
