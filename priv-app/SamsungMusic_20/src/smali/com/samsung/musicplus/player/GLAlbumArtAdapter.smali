.class public Lcom/samsung/musicplus/player/GLAlbumArtAdapter;
.super Lcom/samsung/musicplus/glwidget/GLGalleryCursorAdapter;
.source "GLAlbumArtAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/player/GLAlbumArtAdapter$OnGetAlbumArtUriListener;
    }
.end annotation


# instance fields
.field private mDx:I

.field private mDy:I

.field private mListener:Lcom/samsung/musicplus/player/GLAlbumArtAdapter$OnGetAlbumArtUriListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/database/Cursor;II)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cursor"    # Landroid/database/Cursor;
    .param p3, "dxDimens"    # I
    .param p4, "dyDimens"    # I

    .prologue
    .line 29
    invoke-direct {p0, p2}, Lcom/samsung/musicplus/glwidget/GLGalleryCursorAdapter;-><init>(Landroid/database/Cursor;)V

    .line 30
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/player/GLAlbumArtAdapter;->mDx:I

    .line 31
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p4}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/player/GLAlbumArtAdapter;->mDy:I

    .line 32
    return-void
.end method


# virtual methods
.method protected bindView(Landroid/content/Context;Landroid/view/View;Landroid/database/Cursor;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 41
    return-void
.end method

.method protected getAlbArtUri(Landroid/content/Context;Landroid/database/Cursor;)Landroid/net/Uri;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 45
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getListType()I

    move-result v1

    invoke-static {v1}, Lcom/samsung/musicplus/util/AlbumArtUtils;->getArtWorkUri(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 46
    .local v0, "uri":Landroid/net/Uri;
    const-string v1, "album_id"

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    int-to-long v2, v1

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 48
    iget-object v1, p0, Lcom/samsung/musicplus/player/GLAlbumArtAdapter;->mListener:Lcom/samsung/musicplus/player/GLAlbumArtAdapter$OnGetAlbumArtUriListener;

    if-eqz v1, :cond_0

    .line 49
    iget-object v1, p0, Lcom/samsung/musicplus/player/GLAlbumArtAdapter;->mListener:Lcom/samsung/musicplus/player/GLAlbumArtAdapter$OnGetAlbumArtUriListener;

    invoke-interface {p2}, Landroid/database/Cursor;->getPosition()I

    move-result v2

    invoke-interface {v1, v2, v0}, Lcom/samsung/musicplus/player/GLAlbumArtAdapter$OnGetAlbumArtUriListener;->getAlbArtUri(ILandroid/net/Uri;)V

    .line 51
    :cond_0
    return-object v0
.end method

.method protected getMarkerViewCount(Landroid/content/Context;Landroid/database/Cursor;)I
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 56
    const/4 v0, 0x1

    return v0
.end method

.method public setAlbArtUriListener(Lcom/samsung/musicplus/player/GLAlbumArtAdapter$OnGetAlbumArtUriListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/musicplus/player/GLAlbumArtAdapter$OnGetAlbumArtUriListener;

    .prologue
    .line 35
    iput-object p1, p0, Lcom/samsung/musicplus/player/GLAlbumArtAdapter;->mListener:Lcom/samsung/musicplus/player/GLAlbumArtAdapter$OnGetAlbumArtUriListener;

    .line 36
    return-void
.end method

.method protected updateMarkerViews(Landroid/content/Context;Landroid/database/Cursor;[Lcom/samsung/musicplus/glwidget/GLGalleryView$MarkerViewInfo;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "cursor"    # Landroid/database/Cursor;
    .param p3, "markers"    # [Lcom/samsung/musicplus/glwidget/GLGalleryView$MarkerViewInfo;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 61
    const-string v2, "is_secretbox"

    invoke-interface {p2, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p2, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    if-ne v2, v0, :cond_0

    .line 63
    .local v0, "isPrivate":Z
    :goto_0
    if-eqz v0, :cond_1

    .line 64
    aget-object v2, p3, v1

    const v3, 0x7f04000c

    iput v3, v2, Lcom/samsung/musicplus/glwidget/GLGalleryView$MarkerViewInfo;->resourceId:I

    .line 65
    aget-object v2, p3, v1

    iget v3, p0, Lcom/samsung/musicplus/player/GLAlbumArtAdapter;->mDx:I

    int-to-float v3, v3

    iput v3, v2, Lcom/samsung/musicplus/glwidget/GLGalleryView$MarkerViewInfo;->x:F

    .line 66
    aget-object v1, p3, v1

    iget v2, p0, Lcom/samsung/musicplus/player/GLAlbumArtAdapter;->mDy:I

    int-to-float v2, v2

    iput v2, v1, Lcom/samsung/musicplus/glwidget/GLGalleryView$MarkerViewInfo;->y:F

    .line 70
    :goto_1
    return-void

    .end local v0    # "isPrivate":Z
    :cond_0
    move v0, v1

    .line 61
    goto :goto_0

    .line 68
    .restart local v0    # "isPrivate":Z
    :cond_1
    aget-object v1, p3, v1

    const/4 v2, -0x1

    iput v2, v1, Lcom/samsung/musicplus/glwidget/GLGalleryView$MarkerViewInfo;->resourceId:I

    goto :goto_1
.end method
