.class Lcom/samsung/musicplus/player/FullPlayerView$3;
.super Landroid/os/Handler;
.source "FullPlayerView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/player/FullPlayerView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/player/FullPlayerView;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/player/FullPlayerView;)V
    .locals 0

    .prologue
    .line 462
    iput-object p1, p0, Lcom/samsung/musicplus/player/FullPlayerView$3;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method private onLyricLoadFinished(Ljava/lang/String;)V
    .locals 5
    .param p1, "lyric"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x4

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 483
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView$3;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # setter for: Lcom/samsung/musicplus/player/FullPlayerView;->mIsLyricsLoaded:Z
    invoke-static {v0, v2}, Lcom/samsung/musicplus/player/FullPlayerView;->access$302(Lcom/samsung/musicplus/player/FullPlayerView;Z)Z

    .line 484
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView$3;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mPreferences:Landroid/content/SharedPreferences;
    invoke-static {v0}, Lcom/samsung/musicplus/player/FullPlayerView;->access$400(Lcom/samsung/musicplus/player/FullPlayerView;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "lyric"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 485
    const-string v0, "MusicUi"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "LYRIC_DECODED : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 487
    if-eqz p1, :cond_3

    const-string v0, ""

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 488
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView$3;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # invokes: Lcom/samsung/musicplus/player/FullPlayerView;->ensureLyricView()Z
    invoke-static {v0}, Lcom/samsung/musicplus/player/FullPlayerView;->access$500(Lcom/samsung/musicplus/player/FullPlayerView;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 489
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView$3;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mLyricText:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/samsung/musicplus/player/FullPlayerView;->access$600(Lcom/samsung/musicplus/player/FullPlayerView;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 490
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView$3;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mLyricScroll:Landroid/widget/ScrollView;
    invoke-static {v0}, Lcom/samsung/musicplus/player/FullPlayerView;->access$700(Lcom/samsung/musicplus/player/FullPlayerView;)Landroid/widget/ScrollView;

    move-result-object v0

    invoke-virtual {v0, v3, v3}, Landroid/widget/ScrollView;->scrollTo(II)V

    .line 491
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView$3;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mLyricViewShowing:Z
    invoke-static {v0}, Lcom/samsung/musicplus/player/FullPlayerView;->access$800(Lcom/samsung/musicplus/player/FullPlayerView;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 492
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView$3;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # invokes: Lcom/samsung/musicplus/player/FullPlayerView;->setVisibilityLyricView(I)V
    invoke-static {v0, v3}, Lcom/samsung/musicplus/player/FullPlayerView;->access$900(Lcom/samsung/musicplus/player/FullPlayerView;I)V

    .line 493
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView$3;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mLyricView:Lcom/samsung/musicplus/player/LyricScrollView;
    invoke-static {v0}, Lcom/samsung/musicplus/player/FullPlayerView;->access$1000(Lcom/samsung/musicplus/player/FullPlayerView;)Lcom/samsung/musicplus/player/LyricScrollView;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView$3;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    iget-object v1, v1, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    const v2, 0x7f050008

    invoke-static {v1, v2}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/player/LyricScrollView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 505
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView$3;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # invokes: Lcom/samsung/musicplus/player/FullPlayerView;->refreshTopTitleView()V
    invoke-static {v0}, Lcom/samsung/musicplus/player/FullPlayerView;->access$1100(Lcom/samsung/musicplus/player/FullPlayerView;)V

    .line 507
    :cond_1
    return-void

    .line 496
    :cond_2
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView$3;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # invokes: Lcom/samsung/musicplus/player/FullPlayerView;->setVisibilityLyricView(I)V
    invoke-static {v0, v4}, Lcom/samsung/musicplus/player/FullPlayerView;->access$900(Lcom/samsung/musicplus/player/FullPlayerView;I)V

    goto :goto_0

    .line 500
    :cond_3
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView$3;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mLyricText:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/samsung/musicplus/player/FullPlayerView;->access$600(Lcom/samsung/musicplus/player/FullPlayerView;)Landroid/widget/TextView;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView$3;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mLyricView:Lcom/samsung/musicplus/player/LyricScrollView;
    invoke-static {v0}, Lcom/samsung/musicplus/player/FullPlayerView;->access$1000(Lcom/samsung/musicplus/player/FullPlayerView;)Lcom/samsung/musicplus/player/LyricScrollView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 501
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView$3;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mLyricText:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/samsung/musicplus/player/FullPlayerView;->access$600(Lcom/samsung/musicplus/player/FullPlayerView;)Landroid/widget/TextView;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 502
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView$3;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # invokes: Lcom/samsung/musicplus/player/FullPlayerView;->setVisibilityLyricView(I)V
    invoke-static {v0, v4}, Lcom/samsung/musicplus/player/FullPlayerView;->access$900(Lcom/samsung/musicplus/player/FullPlayerView;I)V

    goto :goto_0
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 466
    iget v1, p1, Landroid/os/Message;->what:I

    sparse-switch v1, :sswitch_data_0

    .line 480
    :goto_0
    return-void

    .line 468
    :sswitch_0
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView$3;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView$3;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;
    invoke-static {v2}, Lcom/samsung/musicplus/player/FullPlayerView;->access$200(Lcom/samsung/musicplus/player/FullPlayerView;)Lcom/samsung/musicplus/library/audio/SecAudioManager;

    move-result-object v2

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->getStreamVolume(I)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/samsung/musicplus/player/FullPlayerView;->updateVolumeItem(I)V

    goto :goto_0

    .line 471
    :sswitch_1
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView$3;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    invoke-virtual {v1}, Lcom/samsung/musicplus/player/FullPlayerView;->hideVolumePanel()V

    goto :goto_0

    .line 474
    :sswitch_2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    .line 475
    .local v0, "lyric":Ljava/lang/String;
    invoke-direct {p0, v0}, Lcom/samsung/musicplus/player/FullPlayerView$3;->onLyricLoadFinished(Ljava/lang/String;)V

    goto :goto_0

    .line 466
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x12c -> :sswitch_2
    .end sparse-switch
.end method
