.class Lcom/samsung/musicplus/player/PlayerView$7;
.super Ljava/lang/Object;
.source "PlayerView.java"

# interfaces
.implements Lcom/samsung/musicplus/library/hardware/AirView$OnAirSeekBarPopupListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/player/PlayerView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# static fields
.field private static final POPUP_INTERVAL:I = 0xa

.field private static final POPUP_Y_POSITION:I = 0x19

.field private static final POSITION_CENCTER:I = 0x1

.field private static final POSITION_LEFT:I = 0x2

.field private static final POSITION_NONE:I = 0x0

.field private static final POSITION_RIGHT:I = 0x3


# instance fields
.field private mAirViewPosition:I

.field private mHoverPosition:I

.field private mSeekInfoPopup:Landroid/view/View;

.field private mX:I

.field final synthetic this$0:Lcom/samsung/musicplus/player/PlayerView;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/player/PlayerView;)V
    .locals 1

    .prologue
    .line 1510
    iput-object p1, p0, Lcom/samsung/musicplus/player/PlayerView$7;->this$0:Lcom/samsung/musicplus/player/PlayerView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1520
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/musicplus/player/PlayerView$7;->mAirViewPosition:I

    return-void
.end method

.method private calculatePositionAndUpdateBackground(I)I
    .locals 9
    .param p1, "progress"    # I

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v3, -0x1

    const v5, 0x3f19999a    # 0.6f

    .line 1599
    iget-object v4, p0, Lcom/samsung/musicplus/player/PlayerView$7;->mSeekInfoPopup:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v4

    div-int/lit8 v0, v4, 0x2

    .line 1600
    .local v0, "popupHalfWidth":I
    iget-object v4, p0, Lcom/samsung/musicplus/player/PlayerView$7;->this$0:Lcom/samsung/musicplus/player/PlayerView;

    iget-object v4, v4, Lcom/samsung/musicplus/player/PlayerView;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v4

    iget v1, v4, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 1601
    .local v1, "screenWidth":I
    const/4 v2, 0x0

    .line 1603
    .local v2, "x":I
    iget v4, p0, Lcom/samsung/musicplus/player/PlayerView$7;->mHoverPosition:I

    if-ge v4, v0, :cond_2

    .line 1604
    iget v4, p0, Lcom/samsung/musicplus/player/PlayerView$7;->mAirViewPosition:I

    if-ne v4, v7, :cond_1

    .line 1629
    :cond_0
    :goto_0
    return v3

    .line 1607
    :cond_1
    iget-object v3, p0, Lcom/samsung/musicplus/player/PlayerView$7;->mSeekInfoPopup:Landroid/view/View;

    const v4, 0x7f02000f

    invoke-virtual {v3, v4}, Landroid/view/View;->setBackgroundResource(I)V

    .line 1608
    iput v7, p0, Lcom/samsung/musicplus/player/PlayerView$7;->mAirViewPosition:I

    .line 1609
    int-to-float v3, v0

    mul-float/2addr v3, v5

    float-to-int v3, v3

    iput v3, p0, Lcom/samsung/musicplus/player/PlayerView$7;->mX:I

    :goto_1
    move v3, v2

    .line 1629
    goto :goto_0

    .line 1610
    :cond_2
    iget v4, p0, Lcom/samsung/musicplus/player/PlayerView$7;->mHoverPosition:I

    sub-int v4, v1, v4

    if-ge v4, v0, :cond_3

    .line 1611
    iget v4, p0, Lcom/samsung/musicplus/player/PlayerView$7;->mAirViewPosition:I

    if-eq v4, v8, :cond_0

    .line 1614
    iget-object v3, p0, Lcom/samsung/musicplus/player/PlayerView$7;->mSeekInfoPopup:Landroid/view/View;

    const v4, 0x7f020010

    invoke-virtual {v3, v4}, Landroid/view/View;->setBackgroundResource(I)V

    .line 1615
    iput v8, p0, Lcom/samsung/musicplus/player/PlayerView$7;->mAirViewPosition:I

    .line 1616
    int-to-float v3, v0

    mul-float/2addr v3, v5

    float-to-int v3, v3

    neg-int v3, v3

    iput v3, p0, Lcom/samsung/musicplus/player/PlayerView$7;->mX:I

    goto :goto_1

    .line 1618
    :cond_3
    iget v3, p0, Lcom/samsung/musicplus/player/PlayerView$7;->mAirViewPosition:I

    if-ne v3, v6, :cond_4

    .line 1622
    rem-int/lit8 v3, p1, 0xa

    mul-int/lit8 v2, v3, -0x1

    .line 1627
    :goto_2
    const/4 v3, 0x0

    iput v3, p0, Lcom/samsung/musicplus/player/PlayerView$7;->mX:I

    goto :goto_1

    .line 1624
    :cond_4
    iget-object v3, p0, Lcom/samsung/musicplus/player/PlayerView$7;->mSeekInfoPopup:Landroid/view/View;

    const v4, 0x7f02000e

    invoke-virtual {v3, v4}, Landroid/view/View;->setBackgroundResource(I)V

    .line 1625
    iput v6, p0, Lcom/samsung/musicplus/player/PlayerView$7;->mAirViewPosition:I

    goto :goto_2
.end method

.method private updateProgressAirView(Landroid/widget/SeekBar;I)Landroid/view/View;
    .locals 10
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I

    .prologue
    const-wide/16 v8, 0x3e8

    const/4 v3, 0x0

    .line 1574
    iget-object v4, p0, Lcom/samsung/musicplus/player/PlayerView$7;->this$0:Lcom/samsung/musicplus/player/PlayerView;

    iget-object v4, v4, Lcom/samsung/musicplus/player/PlayerView;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/samsung/musicplus/library/hardware/AirView;->isEnableProgress(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1587
    :goto_0
    return-object v3

    .line 1577
    :cond_0
    iget-object v4, p0, Lcom/samsung/musicplus/player/PlayerView$7;->mSeekInfoPopup:Landroid/view/View;

    if-nez v4, :cond_1

    .line 1578
    iget-object v4, p0, Lcom/samsung/musicplus/player/PlayerView$7;->this$0:Lcom/samsung/musicplus/player/PlayerView;

    iget-object v4, v4, Lcom/samsung/musicplus/player/PlayerView;->mContext:Landroid/content/Context;

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    const v5, 0x7f040017

    invoke-virtual {v4, v5, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/musicplus/player/PlayerView$7;->mSeekInfoPopup:Landroid/view/View;

    .line 1582
    :cond_1
    iget-object v3, p0, Lcom/samsung/musicplus/player/PlayerView$7;->this$0:Lcom/samsung/musicplus/player/PlayerView;

    # getter for: Lcom/samsung/musicplus/player/PlayerView;->mDuration:J
    invoke-static {v3}, Lcom/samsung/musicplus/player/PlayerView;->access$1000(Lcom/samsung/musicplus/player/PlayerView;)J

    move-result-wide v4

    int-to-long v6, p2

    mul-long/2addr v4, v6

    div-long v0, v4, v8

    .line 1583
    .local v0, "pos":J
    iget-object v3, p0, Lcom/samsung/musicplus/player/PlayerView$7;->this$0:Lcom/samsung/musicplus/player/PlayerView;

    iget-object v3, v3, Lcom/samsung/musicplus/player/PlayerView;->mContext:Landroid/content/Context;

    div-long v4, v0, v8

    invoke-static {v3, v4, v5}, Lcom/samsung/musicplus/util/UiUtils;->makeTimeString(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    .line 1584
    .local v2, "time":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/musicplus/player/PlayerView$7;->mSeekInfoPopup:Landroid/view/View;

    const v4, 0x7f0d007b

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1586
    invoke-direct {p0, p2}, Lcom/samsung/musicplus/player/PlayerView$7;->calculatePositionAndUpdateBackground(I)I

    .line 1587
    iget-object v3, p0, Lcom/samsung/musicplus/player/PlayerView$7;->mSeekInfoPopup:Landroid/view/View;

    goto :goto_0
.end method


# virtual methods
.method public getHoverChangedView(Landroid/widget/SeekBar;IZ)Landroid/view/View;
    .locals 1
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromUser"    # Z

    .prologue
    .line 1560
    if-eqz p3, :cond_0

    .line 1561
    invoke-direct {p0, p1, p2}, Lcom/samsung/musicplus/player/PlayerView$7;->updateProgressAirView(Landroid/widget/SeekBar;I)Landroid/view/View;

    move-result-object v0

    .line 1563
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getStartTrackingHoverView(Landroid/widget/SeekBar;I)Landroid/view/View;
    .locals 1
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I

    .prologue
    .line 1554
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/musicplus/player/PlayerView$7;->mAirViewPosition:I

    .line 1555
    invoke-direct {p0, p1, p2}, Lcom/samsung/musicplus/player/PlayerView$7;->updateProgressAirView(Landroid/widget/SeekBar;I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getStopTrackingHoverView(Landroid/widget/SeekBar;)Landroid/view/View;
    .locals 1
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 1549
    const/4 v0, 0x0

    return-object v0
.end method

.method public getXposition()I
    .locals 1

    .prologue
    .line 1544
    iget v0, p0, Lcom/samsung/musicplus/player/PlayerView$7;->mX:I

    return v0
.end method

.method public getYposition()I
    .locals 1

    .prologue
    .line 1534
    const/16 v0, 0x19

    return v0
.end method

.method public setHoverXPosition(I)V
    .locals 0
    .param p1, "xPos"    # I

    .prologue
    .line 1539
    iput p1, p0, Lcom/samsung/musicplus/player/PlayerView$7;->mHoverPosition:I

    .line 1540
    return-void
.end method
