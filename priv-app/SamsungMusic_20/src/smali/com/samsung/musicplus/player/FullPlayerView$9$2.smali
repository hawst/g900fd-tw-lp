.class Lcom/samsung/musicplus/player/FullPlayerView$9$2;
.super Landroid/os/AsyncTask;
.source "FullPlayerView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/player/FullPlayerView$9;->onFailPublishImage(Landroid/net/Uri;J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/musicplus/player/FullPlayerView$9;

.field final synthetic val$uri:Landroid/net/Uri;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/player/FullPlayerView$9;Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 800
    iput-object p1, p0, Lcom/samsung/musicplus/player/FullPlayerView$9$2;->this$1:Lcom/samsung/musicplus/player/FullPlayerView$9;

    iput-object p2, p0, Lcom/samsung/musicplus/player/FullPlayerView$9$2;->val$uri:Landroid/net/Uri;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 800
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/player/FullPlayerView$9$2;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 5
    .param p1, "voids"    # [Ljava/lang/Void;

    .prologue
    const/4 v4, 0x0

    .line 803
    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mAverageColors:Ljava/util/HashMap;
    invoke-static {}, Lcom/samsung/musicplus/player/FullPlayerView;->access$2400()Ljava/util/HashMap;

    move-result-object v1

    monitor-enter v1

    .line 804
    :try_start_0
    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mAverageColors:Ljava/util/HashMap;
    invoke-static {}, Lcom/samsung/musicplus/player/FullPlayerView;->access$2400()Ljava/util/HashMap;

    move-result-object v0

    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView$9$2;->val$uri:Landroid/net/Uri;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 805
    monitor-exit v1

    .line 811
    :goto_0
    return-object v4

    .line 809
    :cond_0
    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mAverageColors:Ljava/util/HashMap;
    invoke-static {}, Lcom/samsung/musicplus/player/FullPlayerView;->access$2400()Ljava/util/HashMap;

    move-result-object v0

    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView$9$2;->val$uri:Landroid/net/Uri;

    iget-object v3, p0, Lcom/samsung/musicplus/player/FullPlayerView$9$2;->this$1:Lcom/samsung/musicplus/player/FullPlayerView$9;

    iget-object v3, v3, Lcom/samsung/musicplus/player/FullPlayerView$9;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mDefaultAverageColor:I
    invoke-static {v3}, Lcom/samsung/musicplus/player/FullPlayerView;->access$2500(Lcom/samsung/musicplus/player/FullPlayerView;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 810
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
