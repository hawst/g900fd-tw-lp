.class Lcom/samsung/musicplus/player/FullPlayerView$7;
.super Landroid/os/Handler;
.source "FullPlayerView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/player/FullPlayerView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/player/FullPlayerView;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/player/FullPlayerView;)V
    .locals 0

    .prologue
    .line 643
    iput-object p1, p0, Lcom/samsung/musicplus/player/FullPlayerView$7;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public dispatchMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 647
    invoke-super {p0, p1}, Landroid/os/Handler;->dispatchMessage(Landroid/os/Message;)V

    .line 648
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView$7;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mVolumeLimitController:Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;
    invoke-static {v0}, Lcom/samsung/musicplus/player/FullPlayerView;->access$2100(Lcom/samsung/musicplus/player/FullPlayerView;)Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;->isFadeProgressing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 649
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView$7;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mPlaySongRequestHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/musicplus/player/FullPlayerView;->access$2200(Lcom/samsung/musicplus/player/FullPlayerView;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x0

    iget v2, p1, Landroid/os/Message;->arg1:I

    iget v3, p1, Landroid/os/Message;->arg2:I

    invoke-static {p0, v1, v2, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 654
    :goto_0
    return-void

    .line 652
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView$7;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    iget v1, p1, Landroid/os/Message;->arg1:I

    # invokes: Lcom/samsung/musicplus/player/FullPlayerView;->requsetPlayNewSong(I)V
    invoke-static {v0, v1}, Lcom/samsung/musicplus/player/FullPlayerView;->access$2300(Lcom/samsung/musicplus/player/FullPlayerView;I)V

    goto :goto_0
.end method
