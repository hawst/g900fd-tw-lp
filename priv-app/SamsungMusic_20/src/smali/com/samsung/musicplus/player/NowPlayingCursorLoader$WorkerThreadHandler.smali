.class Lcom/samsung/musicplus/player/NowPlayingCursorLoader$WorkerThreadHandler;
.super Landroid/os/Handler;
.source "NowPlayingCursorLoader.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/player/NowPlayingCursorLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "WorkerThreadHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/player/NowPlayingCursorLoader;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/player/NowPlayingCursorLoader;Landroid/os/Looper;)V
    .locals 0
    .param p2, "looper"    # Landroid/os/Looper;

    .prologue
    .line 67
    iput-object p1, p0, Lcom/samsung/musicplus/player/NowPlayingCursorLoader$WorkerThreadHandler;->this$0:Lcom/samsung/musicplus/player/NowPlayingCursorLoader;

    .line 68
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 69
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 77
    iget-object v0, p0, Lcom/samsung/musicplus/player/NowPlayingCursorLoader$WorkerThreadHandler;->this$0:Lcom/samsung/musicplus/player/NowPlayingCursorLoader;

    # getter for: Lcom/samsung/musicplus/player/NowPlayingCursorLoader;->mService:Lcom/samsung/musicplus/service/IPlayerService;
    invoke-static {v0}, Lcom/samsung/musicplus/player/NowPlayingCursorLoader;->access$000(Lcom/samsung/musicplus/player/NowPlayingCursorLoader;)Lcom/samsung/musicplus/service/IPlayerService;

    move-result-object v0

    if-nez v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/samsung/musicplus/player/NowPlayingCursorLoader$WorkerThreadHandler;->this$0:Lcom/samsung/musicplus/player/NowPlayingCursorLoader;

    sget-object v1, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    # setter for: Lcom/samsung/musicplus/player/NowPlayingCursorLoader;->mService:Lcom/samsung/musicplus/service/IPlayerService;
    invoke-static {v0, v1}, Lcom/samsung/musicplus/player/NowPlayingCursorLoader;->access$002(Lcom/samsung/musicplus/player/NowPlayingCursorLoader;Lcom/samsung/musicplus/service/IPlayerService;)Lcom/samsung/musicplus/service/IPlayerService;

    .line 80
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/player/NowPlayingCursorLoader$WorkerThreadHandler;->this$0:Lcom/samsung/musicplus/player/NowPlayingCursorLoader;

    # getter for: Lcom/samsung/musicplus/player/NowPlayingCursorLoader;->mUiHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/musicplus/player/NowPlayingCursorLoader;->access$100(Lcom/samsung/musicplus/player/NowPlayingCursorLoader;)Landroid/os/Handler;

    move-result-object v1

    iget-object v0, p0, Lcom/samsung/musicplus/player/NowPlayingCursorLoader$WorkerThreadHandler;->this$0:Lcom/samsung/musicplus/player/NowPlayingCursorLoader;

    # getter for: Lcom/samsung/musicplus/player/NowPlayingCursorLoader;->mUiHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/musicplus/player/NowPlayingCursorLoader;->access$100(Lcom/samsung/musicplus/player/NowPlayingCursorLoader;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x0

    new-instance v4, Lcom/samsung/musicplus/player/NowPlayingCursor;

    iget-object v0, p0, Lcom/samsung/musicplus/player/NowPlayingCursorLoader$WorkerThreadHandler;->this$0:Lcom/samsung/musicplus/player/NowPlayingCursorLoader;

    # getter for: Lcom/samsung/musicplus/player/NowPlayingCursorLoader;->mService:Lcom/samsung/musicplus/service/IPlayerService;
    invoke-static {v0}, Lcom/samsung/musicplus/player/NowPlayingCursorLoader;->access$000(Lcom/samsung/musicplus/player/NowPlayingCursorLoader;)Lcom/samsung/musicplus/service/IPlayerService;

    move-result-object v5

    iget-object v0, p0, Lcom/samsung/musicplus/player/NowPlayingCursorLoader$WorkerThreadHandler;->this$0:Lcom/samsung/musicplus/player/NowPlayingCursorLoader;

    iget-object v0, v0, Lcom/samsung/musicplus/player/NowPlayingCursorLoader;->mResolver:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentResolver;

    invoke-direct {v4, v5, v0}, Lcom/samsung/musicplus/player/NowPlayingCursor;-><init>(Lcom/samsung/musicplus/service/IPlayerService;Landroid/content/ContentResolver;)V

    invoke-virtual {v2, v3, v4}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 86
    return-void
.end method
