.class public Lcom/samsung/musicplus/player/FullPlayerView;
.super Lcom/samsung/musicplus/player/PlayerView;
.source "FullPlayerView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/samsung/musicplus/player/LyricScrollView$OnLyricTouchListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;,
        Lcom/samsung/musicplus/player/FullPlayerView$SideCastQueryInfo;,
        Lcom/samsung/musicplus/player/FullPlayerView$SideCastAsyncUpdater;,
        Lcom/samsung/musicplus/player/FullPlayerView$AlbumViewGestureDetector;
    }
.end annotation


# static fields
.field public static final CURRENT_COLOR_SAVED:Ljava/lang/String; = "CURRENT_COLOR_SAVED"

.field public static final DEFAULT_BGAVG_COLOR_SAVED:Ljava/lang/String; = "DEFAULT_BG_AVG_COLOR_SAVED"

.field private static final DEFAULT_NEXT_FOUCS_VIEW_ID:I = 0x7f0d0121

.field private static final GRADIENT_END_COLOR:I = -0xebebec

.field private static final GRADIENT_START_COLOR:I = -0x515152

.field private static final HIDE_VOLUME:I = 0x2

.field private static final HIDE_VOLUME_TIME:I = 0xbb8

.field public static final LYRIC_DECODED:I = 0x12c

.field public static final PLAY_NEXT:I = 0x67

.field public static final PLAY_PREV:I = 0x66

.field private static final POSTERIZED_FLAG:Z = true

.field private static final SAME_GENRE_QUERY_WHERE:Ljava/lang/String; = "is_music=1 AND genre_name=?"

.field private static final TAG:Ljava/lang/String; = "MusicUi"

.field private static final TOGGLE_LYRIC_PANNEL:I = 0x65

.field private static final UPDATE_VOLUME_UI:I = 0x1

.field private static final VOLUME_FADE_IN_DURATION:I = 0x4b0

.field private static final VOLUME_FADE_OUT_DURATION:I = 0x2bc

.field private static mAverageColors:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/net/Uri;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private final PLAYER_SIDE_CAST_COLUMNS:[Ljava/lang/String;

.field private k2hdTag:Landroid/widget/ImageView;

.field private mAddPlaylist:Landroid/widget/ImageView;

.field private mAddToPlayListShowing:Z

.field private mAlbumArtAdapter:Lcom/samsung/musicplus/player/GLAlbumArtAdapter;

.field private mAlbumArtUriListener:Lcom/samsung/musicplus/player/GLAlbumArtAdapter$OnGetAlbumArtUriListener;

.field private mAlbumTop:Landroid/widget/TextView;

.field private mAlbumUriPosition:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/net/Uri;",
            ">;"
        }
    .end annotation
.end field

.field private mArtistTop:Landroid/widget/TextView;

.field private mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

.field private mBackgroundColors:[I

.field private mBackgroundDrawable:Landroid/graphics/drawable/GradientDrawable;

.field private mChangePlayerItem:Landroid/view/MenuItem;

.field private mCurrentColor:I

.field private mCurrentSongPosition:I

.field private final mDefaultAlbumArtBitmap:Landroid/graphics/Bitmap;

.field private mDefaultAverageColor:I

.field private mDurationSize:I

.field private mDurationTextMargin:I

.field private mEasyModeDurationSize:I

.field private mEasyModeDurationTextMargin:I

.field private mEasyModeOptionTopMargin:I

.field private mEasyModePaddingLeft:I

.field private mEasyModePaddingRight:I

.field private mEasyModePaddingTop:I

.field private mEasyModeProgressPaddingTop:I

.field private mEasyModeSubTextsize:I

.field private mEasyModeTextsize:I

.field private mEasyModeTitleSubColor:I

.field private mEmptyViewStub:Landroid/view/View;

.field private mFormatBuilder:Ljava/lang/StringBuilder;

.field private mFormatter:Ljava/util/Formatter;

.field private mGLAlbumArt:Lcom/samsung/musicplus/glwidget/GLGalleryView;

.field private mGLAlbumArtOnAnimationListener:Lcom/samsung/musicplus/glwidget/GLGalleryView$OnAnimationListener;

.field private mGLAlbumArtOnPublishImageListener:Lcom/samsung/musicplus/bitmapcache/BitmapCache$Publisher;

.field private mGLParticlesTouchNotifyListener:Lcom/samsung/musicplus/widget/TouchNotifyRelativeLayout$OnTouchNotifyListener;

.field private mGLRecommendedMusic:Lcom/samsung/musicplus/glwidget/GLGalleryView;

.field private mGestureDetector:Landroid/view/GestureDetector;

.field private mIsHdmiConnected:Z

.field private mIsLyricsLoaded:Z

.field private mIsStarred:Z

.field private mIsVolumeBarShowing:Z

.field private mLoader:Lcom/samsung/musicplus/player/NowPlayingCursorLoader;

.field private mLyricDownOut:Landroid/view/animation/Animation;

.field private mLyricScroll:Landroid/widget/ScrollView;

.field private mLyricText:Landroid/widget/TextView;

.field private mLyricUpIn:Landroid/view/animation/Animation;

.field private mLyricView:Lcom/samsung/musicplus/player/LyricScrollView;

.field private mLyricViewShowing:Z

.field private mNoItemImageView:Landroid/widget/ImageView;

.field private mNoItemTextView:Landroid/widget/TextView;

.field private mNoUpdateAlbumArtPosition:Z

.field private mNowplayignQueryCompleteListener:Lcom/samsung/musicplus/player/NowPlayingCursorLoader$OnQueryCompleteListener;

.field private mOldAlbum:Landroid/widget/ImageView;

.field private final mOnClickListener:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView$OnItemClickListener;

.field private mParticleView:Landroid/view/View;

.field private mParticlesInCoord:[F

.field private mParticlesOutCoord:[F

.field private mPlaySongRequestHandler:Landroid/os/Handler;

.field private mPosition:I

.field private mPreferences:Landroid/content/SharedPreferences;

.field private final mQueueReceiver:Landroid/content/BroadcastReceiver;

.field private mRecommendedMusicAdapter:Lcom/samsung/musicplus/player/GLRecommendedMusicAdapter;

.field private mRepeatBtn:Landroid/widget/ImageView;

.field private mRepeatGroup:Landroid/widget/LinearLayout;

.field private mRepeatText:Landroid/widget/TextView;

.field private mShuffleBtn:Landroid/widget/ImageView;

.field private mShuffleGroup:Landroid/widget/LinearLayout;

.field private mShuffleText:Landroid/widget/TextView;

.field private mSideCastAdapter:Lcom/samsung/musicplus/player/PlayerSideCastAdapter;

.field private mSideCastGridView:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;

.field private mSideCastTextView:Landroid/widget/TextView;

.field private mSideCastUpdater:Lcom/samsung/musicplus/player/FullPlayerView$SideCastAsyncUpdater;

.field private mSongCountString:Ljava/lang/String;

.field private mStarButton:Landroid/widget/ImageView;

.field private mTextMainSize:I

.field private mTextSubSize:I

.field private mTitleSubColor:I

.field private mTitleTop:Landroid/widget/TextView;

.field private mToast:Landroid/widget/Toast;

.field private mTopTitleView:Landroid/view/View;

.field private mUhaTagView:Landroid/widget/TextView;

.field private final mUiHandler:Landroid/os/Handler;

.field private mViewScreenCoordinates:[I

.field private final mVolumeChangeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field private mVolumeDmrPanel:Landroid/view/View;

.field private mVolumeItem:Landroid/view/MenuItem;

.field private mVolumeLimitController:Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;

.field private mVolumePanel:Landroid/view/View;

.field private mVolumePopupWindow:Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;

.field private mVolumeSeekBar:Landroid/widget/SeekBar;

.field private mVolumeText:Landroid/widget/TextView;

.field private multiWindowRect:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

.field private tagsText:Landroid/widget/TextView;

.field private uhqTag:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;Landroid/os/Bundle;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "root"    # Landroid/view/View;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x0

    const/4 v1, -0x1

    .line 1061
    invoke-direct {p0, p1, p2}, Lcom/samsung/musicplus/player/PlayerView;-><init>(Landroid/content/Context;Landroid/view/View;)V

    .line 170
    iput-boolean v3, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mIsStarred:Z

    .line 182
    iput-boolean v3, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mIsLyricsLoaded:Z

    .line 234
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mAlbumUriPosition:Landroid/util/SparseArray;

    .line 240
    new-array v0, v2, [F

    iput-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mParticlesInCoord:[F

    .line 245
    new-array v0, v2, [I

    iput-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mViewScreenCoordinates:[I

    .line 251
    new-array v0, v2, [F

    iput-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mParticlesOutCoord:[F

    .line 286
    new-array v0, v2, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mBackgroundColors:[I

    .line 291
    new-instance v0, Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mBackgroundDrawable:Landroid/graphics/drawable/GradientDrawable;

    .line 293
    new-instance v0, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;

    invoke-direct {v0}, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mVolumeLimitController:Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;

    .line 295
    iput v3, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mCurrentColor:I

    .line 452
    new-instance v0, Lcom/samsung/musicplus/player/FullPlayerView$2;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/player/FullPlayerView$2;-><init>(Lcom/samsung/musicplus/player/FullPlayerView;)V

    iput-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mQueueReceiver:Landroid/content/BroadcastReceiver;

    .line 462
    new-instance v0, Lcom/samsung/musicplus/player/FullPlayerView$3;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/player/FullPlayerView$3;-><init>(Lcom/samsung/musicplus/player/FullPlayerView;)V

    iput-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mUiHandler:Landroid/os/Handler;

    .line 598
    new-instance v0, Lcom/samsung/musicplus/player/FullPlayerView$5;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/player/FullPlayerView$5;-><init>(Lcom/samsung/musicplus/player/FullPlayerView;)V

    iput-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mAlbumArtUriListener:Lcom/samsung/musicplus/player/GLAlbumArtAdapter$OnGetAlbumArtUriListener;

    .line 605
    new-instance v0, Lcom/samsung/musicplus/player/FullPlayerView$6;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/player/FullPlayerView$6;-><init>(Lcom/samsung/musicplus/player/FullPlayerView;)V

    iput-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mNowplayignQueryCompleteListener:Lcom/samsung/musicplus/player/NowPlayingCursorLoader$OnQueryCompleteListener;

    .line 643
    new-instance v0, Lcom/samsung/musicplus/player/FullPlayerView$7;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/player/FullPlayerView$7;-><init>(Lcom/samsung/musicplus/player/FullPlayerView;)V

    iput-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mPlaySongRequestHandler:Landroid/os/Handler;

    .line 662
    new-instance v0, Lcom/samsung/musicplus/player/FullPlayerView$8;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/player/FullPlayerView$8;-><init>(Lcom/samsung/musicplus/player/FullPlayerView;)V

    iput-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mGLAlbumArtOnAnimationListener:Lcom/samsung/musicplus/glwidget/GLGalleryView$OnAnimationListener;

    .line 780
    new-instance v0, Lcom/samsung/musicplus/player/FullPlayerView$9;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/player/FullPlayerView$9;-><init>(Lcom/samsung/musicplus/player/FullPlayerView;)V

    iput-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mGLAlbumArtOnPublishImageListener:Lcom/samsung/musicplus/bitmapcache/BitmapCache$Publisher;

    .line 817
    new-instance v0, Lcom/samsung/musicplus/player/FullPlayerView$10;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/player/FullPlayerView$10;-><init>(Lcom/samsung/musicplus/player/FullPlayerView;)V

    iput-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mGLParticlesTouchNotifyListener:Lcom/samsung/musicplus/widget/TouchNotifyRelativeLayout$OnTouchNotifyListener;

    .line 910
    iput-boolean v4, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mLyricViewShowing:Z

    .line 1453
    iput-object v5, p0, Lcom/samsung/musicplus/player/FullPlayerView;->uhqTag:Landroid/widget/ImageView;

    .line 1455
    iput-object v5, p0, Lcom/samsung/musicplus/player/FullPlayerView;->k2hdTag:Landroid/widget/ImageView;

    .line 1457
    iput-object v5, p0, Lcom/samsung/musicplus/player/FullPlayerView;->tagsText:Landroid/widget/TextView;

    .line 1723
    iput v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mTitleSubColor:I

    .line 1725
    iput v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mEasyModeTitleSubColor:I

    .line 1727
    iput v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mTextMainSize:I

    .line 1729
    iput v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mTextSubSize:I

    .line 1731
    iput v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mDurationTextMargin:I

    .line 1733
    iput v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mEasyModeTextsize:I

    .line 1735
    iput v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mEasyModeSubTextsize:I

    .line 1737
    iput v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mDurationSize:I

    .line 1739
    iput v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mEasyModeDurationSize:I

    .line 1741
    iput v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mEasyModePaddingLeft:I

    .line 1743
    iput v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mEasyModePaddingRight:I

    .line 1745
    iput v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mEasyModePaddingTop:I

    .line 1747
    iput v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mEasyModeProgressPaddingTop:I

    .line 1749
    iput v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mEasyModeDurationTextMargin:I

    .line 1751
    iput v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mEasyModeOptionTopMargin:I

    .line 1972
    iput-boolean v3, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mAddToPlayListShowing:Z

    .line 2231
    new-instance v0, Lcom/samsung/musicplus/player/FullPlayerView$16;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/player/FullPlayerView$16;-><init>(Lcom/samsung/musicplus/player/FullPlayerView;)V

    iput-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mOnClickListener:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView$OnItemClickListener;

    .line 2456
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "title"

    aput-object v1, v0, v4

    const-string v1, "album_id"

    aput-object v1, v0, v2

    const/4 v1, 0x3

    const-string v2, "album"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "artist"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "is_secretbox"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->PLAYER_SIDE_CAST_COLUMNS:[Ljava/lang/String;

    .line 2789
    iput-boolean v3, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mIsVolumeBarShowing:Z

    .line 3017
    new-instance v0, Lcom/samsung/musicplus/player/FullPlayerView$22;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/player/FullPlayerView$22;-><init>(Lcom/samsung/musicplus/player/FullPlayerView;)V

    iput-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mVolumeChangeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    .line 3197
    iput-boolean v3, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mIsHdmiConnected:Z

    .line 1062
    const v0, 0x7f020039

    invoke-static {p1, v0}, Lcom/samsung/musicplus/util/AlbumArtUtils;->getDefaulAlbumArt(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mDefaultAlbumArtBitmap:Landroid/graphics/Bitmap;

    .line 1064
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->getInstance(Landroid/content/Context;)Lcom/samsung/musicplus/library/audio/SecAudioManager;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

    .line 1065
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    const-string v1, "music_player_pref"

    invoke-virtual {v0, v1, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mPreferences:Landroid/content/SharedPreferences;

    .line 1066
    new-instance v0, Landroid/view/GestureDetector;

    new-instance v1, Lcom/samsung/musicplus/player/FullPlayerView$AlbumViewGestureDetector;

    invoke-direct {v1, p0, v5}, Lcom/samsung/musicplus/player/FullPlayerView$AlbumViewGestureDetector;-><init>(Lcom/samsung/musicplus/player/FullPlayerView;Lcom/samsung/musicplus/player/FullPlayerView$1;)V

    invoke-direct {v0, p1, v1}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mGestureDetector:Landroid/view/GestureDetector;

    .line 1068
    new-instance v0, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;

    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

    const/4 v3, 0x3

    invoke-virtual {v1, v3}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->getStreamMaxVolume(I)I

    move-result v4

    move-object v1, p0

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;-><init>(Lcom/samsung/musicplus/player/FullPlayerView;Landroid/content/Context;Landroid/view/View;ILcom/samsung/musicplus/player/FullPlayerView$1;)V

    iput-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mVolumePopupWindow:Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;

    .line 1072
    if-eqz p3, :cond_1

    .line 1073
    invoke-direct {p0, p3}, Lcom/samsung/musicplus/player/FullPlayerView;->restoreDefaultDBAVGColor(Landroid/os/Bundle;)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mDefaultAverageColor:I

    .line 1074
    invoke-direct {p0, p3}, Lcom/samsung/musicplus/player/FullPlayerView;->restoreSavedBackgroundColor(Landroid/os/Bundle;)I

    move-result v6

    .line 1088
    .local v6, "backgroundColor":I
    :cond_0
    :goto_0
    invoke-direct {p0, v6}, Lcom/samsung/musicplus/player/FullPlayerView;->setBackground(I)V

    .line 1089
    invoke-direct {p0, p2}, Lcom/samsung/musicplus/player/FullPlayerView;->initializeView(Landroid/view/View;)V

    .line 1090
    return-void

    .line 1076
    .end local v6    # "backgroundColor":I
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mDefaultAlbumArtBitmap:Landroid/graphics/Bitmap;

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/player/FullPlayerView;->getAverageColor(Landroid/graphics/Bitmap;)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mDefaultAverageColor:I

    .line 1077
    iget v6, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mDefaultAverageColor:I

    .line 1079
    .restart local v6    # "backgroundColor":I
    sget-object v0, Lcom/samsung/musicplus/player/FullPlayerView;->mAverageColors:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    .line 1080
    sget-object v0, Lcom/samsung/musicplus/util/AlbumArtUtils;->URI_ARTWORK:Landroid/net/Uri;

    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getAlbumId()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v8

    .line 1082
    .local v8, "uri":Landroid/net/Uri;
    sget-object v0, Lcom/samsung/musicplus/player/FullPlayerView;->mAverageColors:Ljava/util/HashMap;

    invoke-virtual {v0, v8}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    .line 1083
    .local v7, "color":Ljava/lang/Integer;
    if-eqz v7, :cond_0

    .line 1084
    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v6

    goto :goto_0

    .line 286
    nop

    :array_0
    .array-data 4
        -0x515152
        -0xebebec    # -1.9683E38f
    .end array-data
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/player/FullPlayerView;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/player/FullPlayerView;

    .prologue
    .line 150
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mOldAlbum:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/musicplus/player/FullPlayerView;)Lcom/samsung/musicplus/player/NowPlayingCursorLoader;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/player/FullPlayerView;

    .prologue
    .line 150
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mLoader:Lcom/samsung/musicplus/player/NowPlayingCursorLoader;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/samsung/musicplus/player/FullPlayerView;)Lcom/samsung/musicplus/player/LyricScrollView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/player/FullPlayerView;

    .prologue
    .line 150
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mLyricView:Lcom/samsung/musicplus/player/LyricScrollView;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/samsung/musicplus/player/FullPlayerView;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/player/FullPlayerView;

    .prologue
    .line 150
    invoke-direct {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->refreshTopTitleView()V

    return-void
.end method

.method static synthetic access$1200(Lcom/samsung/musicplus/player/FullPlayerView;Landroid/view/MotionEvent;Landroid/view/MotionEvent;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/player/FullPlayerView;
    .param p1, "x1"    # Landroid/view/MotionEvent;
    .param p2, "x2"    # Landroid/view/MotionEvent;

    .prologue
    .line 150
    invoke-direct {p0, p1, p2}, Lcom/samsung/musicplus/player/FullPlayerView;->handleMotionEvent(Landroid/view/MotionEvent;Landroid/view/MotionEvent;)V

    return-void
.end method

.method static synthetic access$1400(Lcom/samsung/musicplus/player/FullPlayerView;)Landroid/view/GestureDetector;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/player/FullPlayerView;

    .prologue
    .line 150
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mGestureDetector:Landroid/view/GestureDetector;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/samsung/musicplus/player/FullPlayerView;)Landroid/util/SparseArray;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/player/FullPlayerView;

    .prologue
    .line 150
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mAlbumUriPosition:Landroid/util/SparseArray;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/samsung/musicplus/player/FullPlayerView;)Lcom/samsung/musicplus/glwidget/GLGalleryView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/player/FullPlayerView;

    .prologue
    .line 150
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mGLAlbumArt:Lcom/samsung/musicplus/glwidget/GLGalleryView;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/samsung/musicplus/player/FullPlayerView;)Lcom/samsung/musicplus/player/GLAlbumArtAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/player/FullPlayerView;

    .prologue
    .line 150
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mAlbumArtAdapter:Lcom/samsung/musicplus/player/GLAlbumArtAdapter;

    return-object v0
.end method

.method static synthetic access$1702(Lcom/samsung/musicplus/player/FullPlayerView;Lcom/samsung/musicplus/player/GLAlbumArtAdapter;)Lcom/samsung/musicplus/player/GLAlbumArtAdapter;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/player/FullPlayerView;
    .param p1, "x1"    # Lcom/samsung/musicplus/player/GLAlbumArtAdapter;

    .prologue
    .line 150
    iput-object p1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mAlbumArtAdapter:Lcom/samsung/musicplus/player/GLAlbumArtAdapter;

    return-object p1
.end method

.method static synthetic access$1800(Lcom/samsung/musicplus/player/FullPlayerView;)Lcom/samsung/musicplus/player/GLAlbumArtAdapter$OnGetAlbumArtUriListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/player/FullPlayerView;

    .prologue
    .line 150
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mAlbumArtUriListener:Lcom/samsung/musicplus/player/GLAlbumArtAdapter$OnGetAlbumArtUriListener;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/samsung/musicplus/player/FullPlayerView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/player/FullPlayerView;

    .prologue
    .line 150
    iget-boolean v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mNoUpdateAlbumArtPosition:Z

    return v0
.end method

.method static synthetic access$1902(Lcom/samsung/musicplus/player/FullPlayerView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/player/FullPlayerView;
    .param p1, "x1"    # Z

    .prologue
    .line 150
    iput-boolean p1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mNoUpdateAlbumArtPosition:Z

    return p1
.end method

.method static synthetic access$200(Lcom/samsung/musicplus/player/FullPlayerView;)Lcom/samsung/musicplus/library/audio/SecAudioManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/player/FullPlayerView;

    .prologue
    .line 150
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/samsung/musicplus/player/FullPlayerView;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/player/FullPlayerView;

    .prologue
    .line 150
    invoke-direct {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->updateGLAlbumartPosition()V

    return-void
.end method

.method static synthetic access$2100(Lcom/samsung/musicplus/player/FullPlayerView;)Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/player/FullPlayerView;

    .prologue
    .line 150
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mVolumeLimitController:Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/samsung/musicplus/player/FullPlayerView;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/player/FullPlayerView;

    .prologue
    .line 150
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mPlaySongRequestHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/samsung/musicplus/player/FullPlayerView;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/player/FullPlayerView;
    .param p1, "x1"    # I

    .prologue
    .line 150
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/player/FullPlayerView;->requsetPlayNewSong(I)V

    return-void
.end method

.method static synthetic access$2400()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 150
    sget-object v0, Lcom/samsung/musicplus/player/FullPlayerView;->mAverageColors:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/samsung/musicplus/player/FullPlayerView;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/player/FullPlayerView;

    .prologue
    .line 150
    iget v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mDefaultAverageColor:I

    return v0
.end method

.method static synthetic access$2600(Lcom/samsung/musicplus/player/FullPlayerView;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/player/FullPlayerView;
    .param p1, "x1"    # I

    .prologue
    .line 150
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/player/FullPlayerView;->setBackground(I)V

    return-void
.end method

.method static synthetic access$2700(Lcom/samsung/musicplus/player/FullPlayerView;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/player/FullPlayerView;

    .prologue
    .line 150
    invoke-direct {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->hideLyricView()V

    return-void
.end method

.method static synthetic access$2800(Lcom/samsung/musicplus/player/FullPlayerView;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/player/FullPlayerView;

    .prologue
    .line 150
    iget v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mCurrentSongPosition:I

    return v0
.end method

.method static synthetic access$2900(Lcom/samsung/musicplus/player/FullPlayerView;Landroid/graphics/Bitmap;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/player/FullPlayerView;
    .param p1, "x1"    # Landroid/graphics/Bitmap;

    .prologue
    .line 150
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/player/FullPlayerView;->getAverageColor(Landroid/graphics/Bitmap;)I

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/samsung/musicplus/player/FullPlayerView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/player/FullPlayerView;

    .prologue
    .line 150
    iget-boolean v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mIsLyricsLoaded:Z

    return v0
.end method

.method static synthetic access$3000(Lcom/samsung/musicplus/player/FullPlayerView;)[I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/player/FullPlayerView;

    .prologue
    .line 150
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mViewScreenCoordinates:[I

    return-object v0
.end method

.method static synthetic access$302(Lcom/samsung/musicplus/player/FullPlayerView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/player/FullPlayerView;
    .param p1, "x1"    # Z

    .prologue
    .line 150
    iput-boolean p1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mIsLyricsLoaded:Z

    return p1
.end method

.method static synthetic access$3100(Lcom/samsung/musicplus/player/FullPlayerView;)[F
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/player/FullPlayerView;

    .prologue
    .line 150
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mParticlesInCoord:[F

    return-object v0
.end method

.method static synthetic access$3200(Lcom/samsung/musicplus/player/FullPlayerView;)[F
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/player/FullPlayerView;

    .prologue
    .line 150
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mParticlesOutCoord:[F

    return-object v0
.end method

.method static synthetic access$3300(Lcom/samsung/musicplus/player/FullPlayerView;)Lcom/samsung/musicplus/glwidget/GLGalleryView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/player/FullPlayerView;

    .prologue
    .line 150
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mGLRecommendedMusic:Lcom/samsung/musicplus/glwidget/GLGalleryView;

    return-object v0
.end method

.method static synthetic access$3400(Lcom/samsung/musicplus/player/FullPlayerView;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/player/FullPlayerView;

    .prologue
    .line 150
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mParticleView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$3500(Lcom/samsung/musicplus/player/FullPlayerView;Landroid/view/MotionEvent;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/player/FullPlayerView;
    .param p1, "x1"    # Landroid/view/MotionEvent;

    .prologue
    .line 150
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/player/FullPlayerView;->particlesCountForMotionEvent(Landroid/view/MotionEvent;)I

    move-result v0

    return v0
.end method

.method static synthetic access$3600(Lcom/samsung/musicplus/player/FullPlayerView;IFFI)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/player/FullPlayerView;
    .param p1, "x1"    # I
    .param p2, "x2"    # F
    .param p3, "x3"    # F
    .param p4, "x4"    # I

    .prologue
    .line 150
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/musicplus/player/FullPlayerView;->emitParticles(IFFI)V

    return-void
.end method

.method static synthetic access$3700(Lcom/samsung/musicplus/player/FullPlayerView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/player/FullPlayerView;

    .prologue
    .line 150
    iget-boolean v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mAddToPlayListShowing:Z

    return v0
.end method

.method static synthetic access$3800(Lcom/samsung/musicplus/player/FullPlayerView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/player/FullPlayerView;

    .prologue
    .line 150
    iget-boolean v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mIsVolumeBarShowing:Z

    return v0
.end method

.method static synthetic access$3802(Lcom/samsung/musicplus/player/FullPlayerView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/player/FullPlayerView;
    .param p1, "x1"    # Z

    .prologue
    .line 150
    iput-boolean p1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mIsVolumeBarShowing:Z

    return p1
.end method

.method static synthetic access$3900(Lcom/samsung/musicplus/player/FullPlayerView;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/player/FullPlayerView;

    .prologue
    .line 150
    invoke-direct {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->setInvisibleTitleView()V

    return-void
.end method

.method static synthetic access$400(Lcom/samsung/musicplus/player/FullPlayerView;)Landroid/content/SharedPreferences;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/player/FullPlayerView;

    .prologue
    .line 150
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mPreferences:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic access$4100(Lcom/samsung/musicplus/player/FullPlayerView;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/player/FullPlayerView;

    .prologue
    .line 150
    iget v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mPosition:I

    return v0
.end method

.method static synthetic access$4201(Lcom/samsung/musicplus/player/FullPlayerView;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/player/FullPlayerView;
    .param p1, "x1"    # I

    .prologue
    .line 150
    invoke-super {p0, p1}, Lcom/samsung/musicplus/player/PlayerView;->updateAlbumArt(I)V

    return-void
.end method

.method static synthetic access$4300(Lcom/samsung/musicplus/player/FullPlayerView;Landroid/net/Uri;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/player/FullPlayerView;
    .param p1, "x1"    # Landroid/net/Uri;

    .prologue
    .line 150
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/player/FullPlayerView;->updateButtonThumbnailImage(Landroid/net/Uri;)V

    return-void
.end method

.method static synthetic access$4400(Lcom/samsung/musicplus/player/FullPlayerView;)Lcom/samsung/musicplus/player/PlayerSideCastAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/player/FullPlayerView;

    .prologue
    .line 150
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mSideCastAdapter:Lcom/samsung/musicplus/player/PlayerSideCastAdapter;

    return-object v0
.end method

.method static synthetic access$4500(Lcom/samsung/musicplus/player/FullPlayerView;)Lcom/samsung/musicplus/player/GLRecommendedMusicAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/player/FullPlayerView;

    .prologue
    .line 150
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mRecommendedMusicAdapter:Lcom/samsung/musicplus/player/GLRecommendedMusicAdapter;

    return-object v0
.end method

.method static synthetic access$4502(Lcom/samsung/musicplus/player/FullPlayerView;Lcom/samsung/musicplus/player/GLRecommendedMusicAdapter;)Lcom/samsung/musicplus/player/GLRecommendedMusicAdapter;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/player/FullPlayerView;
    .param p1, "x1"    # Lcom/samsung/musicplus/player/GLRecommendedMusicAdapter;

    .prologue
    .line 150
    iput-object p1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mRecommendedMusicAdapter:Lcom/samsung/musicplus/player/GLRecommendedMusicAdapter;

    return-object p1
.end method

.method static synthetic access$4600(Lcom/samsung/musicplus/player/FullPlayerView;)[Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/player/FullPlayerView;

    .prologue
    .line 150
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->PLAYER_SIDE_CAST_COLUMNS:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$4700(Lcom/samsung/musicplus/player/FullPlayerView;Landroid/database/Cursor;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/player/FullPlayerView;
    .param p1, "x1"    # Landroid/database/Cursor;

    .prologue
    .line 150
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/player/FullPlayerView;->updateTotalCount(Landroid/database/Cursor;)V

    return-void
.end method

.method static synthetic access$4800(Lcom/samsung/musicplus/player/FullPlayerView;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/player/FullPlayerView;
    .param p1, "x1"    # I

    .prologue
    .line 150
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/player/FullPlayerView;->setVolume(I)V

    return-void
.end method

.method static synthetic access$4900(Lcom/samsung/musicplus/player/FullPlayerView;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/player/FullPlayerView;

    .prologue
    .line 150
    invoke-direct {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->showSmartVolumeToast()V

    return-void
.end method

.method static synthetic access$500(Lcom/samsung/musicplus/player/FullPlayerView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/player/FullPlayerView;

    .prologue
    .line 150
    invoke-direct {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->ensureLyricView()Z

    move-result v0

    return v0
.end method

.method static synthetic access$5000(Lcom/samsung/musicplus/player/FullPlayerView;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/player/FullPlayerView;

    .prologue
    .line 150
    invoke-direct {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->resetHideVolumeTimer()V

    return-void
.end method

.method static synthetic access$5100(Lcom/samsung/musicplus/player/FullPlayerView;)Landroid/widget/SeekBar$OnSeekBarChangeListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/player/FullPlayerView;

    .prologue
    .line 150
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mVolumeChangeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    return-object v0
.end method

.method static synthetic access$5200(Lcom/samsung/musicplus/player/FullPlayerView;)Ljava/lang/StringBuilder;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/player/FullPlayerView;

    .prologue
    .line 150
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mFormatBuilder:Ljava/lang/StringBuilder;

    return-object v0
.end method

.method static synthetic access$5300(Lcom/samsung/musicplus/player/FullPlayerView;)Ljava/util/Formatter;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/player/FullPlayerView;

    .prologue
    .line 150
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mFormatter:Ljava/util/Formatter;

    return-object v0
.end method

.method static synthetic access$5400(Lcom/samsung/musicplus/player/FullPlayerView;)Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/player/FullPlayerView;

    .prologue
    .line 150
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->multiWindowRect:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    return-object v0
.end method

.method static synthetic access$600(Lcom/samsung/musicplus/player/FullPlayerView;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/player/FullPlayerView;

    .prologue
    .line 150
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mLyricText:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$700(Lcom/samsung/musicplus/player/FullPlayerView;)Landroid/widget/ScrollView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/player/FullPlayerView;

    .prologue
    .line 150
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mLyricScroll:Landroid/widget/ScrollView;

    return-object v0
.end method

.method static synthetic access$800(Lcom/samsung/musicplus/player/FullPlayerView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/player/FullPlayerView;

    .prologue
    .line 150
    iget-boolean v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mLyricViewShowing:Z

    return v0
.end method

.method static synthetic access$900(Lcom/samsung/musicplus/player/FullPlayerView;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/player/FullPlayerView;
    .param p1, "x1"    # I

    .prologue
    .line 150
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/player/FullPlayerView;->setVisibilityLyricView(I)V

    return-void
.end method

.method private availableWfdErrorDialogShowing(I)Z
    .locals 6
    .param p1, "exceptionalCase"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2127
    iget-object v4, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    const-string v5, "music_player_pref"

    invoke-virtual {v4, v5, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 2129
    .local v1, "sp":Landroid/content/SharedPreferences;
    invoke-static {p1}, Lcom/samsung/musicplus/dialog/WfdErrorDialog;->getWfdExceptionalCaseKey(I)Ljava/lang/String;

    move-result-object v0

    .line 2130
    .local v0, "key":Ljava/lang/String;
    if-nez v0, :cond_1

    move v2, v3

    .line 2135
    :cond_0
    :goto_0
    return v2

    .line 2132
    :cond_1
    const-string v4, "wfd_group_play"

    if-eq v0, v4, :cond_0

    .line 2135
    invoke-interface {v1, v0, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    if-eqz v4, :cond_0

    move v2, v3

    goto :goto_0
.end method

.method private changeVolume(I)V
    .locals 3
    .param p1, "volumeAdjust"    # I

    .prologue
    .line 3060
    if-eqz p1, :cond_1

    .line 3061
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->isDmrPlaying()Z

    move-result v0

    if-nez v0, :cond_2

    .line 3063
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

    const/4 v1, 0x3

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->adjustStreamVolume(III)V

    .line 3064
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mVolumePanel:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mVolumePanel:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3065
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mVolumePanel:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    .line 3067
    :cond_0
    invoke-direct {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->showSmartVolumeToast()V

    .line 3082
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->showVolumePanel()V

    .line 3083
    return-void

    .line 3070
    :cond_2
    packed-switch p1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 3075
    :pswitch_1
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->dlnaDmrVolumeDown()V

    goto :goto_0

    .line 3072
    :pswitch_2
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->dlnaDmrVolumeUp()V

    goto :goto_0

    .line 3070
    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private checkStateAndShowDialog()V
    .locals 2

    .prologue
    .line 2084
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->checkWfdExceptionalCase()I

    move-result v0

    .line 2086
    .local v0, "exceptionalCase":I
    if-nez v0, :cond_1

    .line 2087
    invoke-direct {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->showChangeDeviceDialog()V

    .line 2094
    :cond_0
    :goto_0
    return-void

    .line 2090
    :cond_1
    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    .line 2091
    invoke-direct {p0, v0}, Lcom/samsung/musicplus/player/FullPlayerView;->showWfdErrorDialog(I)V

    goto :goto_0
.end method

.method private doCheckInvalidCase()Z
    .locals 1

    .prologue
    .line 1838
    iget v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mTitleSubColor:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mEasyModeTitleSubColor:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mTextMainSize:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mTextSubSize:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mEasyModeTextsize:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mEasyModeSubTextsize:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mDurationSize:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mEasyModeDurationSize:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mEasyModePaddingTop:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mEasyModePaddingLeft:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mEasyModePaddingRight:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mEasyModeProgressPaddingTop:I

    if-gez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private doUpdateSeekBarView(Landroid/widget/SeekBar;)V
    .locals 2
    .param p1, "seekbar"    # Landroid/widget/SeekBar;

    .prologue
    .line 1907
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->getSeekBarView()Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 1908
    if-eqz p1, :cond_0

    .line 1909
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/SeekBar;->setVisibility(I)V

    .line 1910
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->getSeekBarListener()Landroid/widget/SeekBar$OnSeekBarChangeListener;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 1912
    :cond_0
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/player/FullPlayerView;->setSeekBar(Landroid/widget/SeekBar;)V

    .line 1913
    return-void
.end method

.method private doUpdateTextView(Z)V
    .locals 12
    .param p1, "easyMode"    # Z

    .prologue
    const/4 v11, 0x0

    .line 1869
    const/4 v7, 0x0

    .line 1871
    .local v7, "textFont":Landroid/graphics/Typeface;
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->getCurrentTimeView()Landroid/widget/TextView;

    move-result-object v0

    .line 1872
    .local v0, "curTime":Landroid/widget/TextView;
    invoke-virtual {v0}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1873
    .local v4, "params":Landroid/view/ViewGroup$MarginLayoutParams;
    if-eqz p1, :cond_0

    .line 1874
    iget v3, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mEasyModeTextsize:I

    .line 1875
    .local v3, "mainTextSize":I
    iget v6, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mEasyModeSubTextsize:I

    .line 1876
    .local v6, "subTextSize":I
    iget v5, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mEasyModeTitleSubColor:I

    .line 1877
    .local v5, "subColor":I
    iget v2, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mEasyModeDurationSize:I

    .line 1878
    .local v2, "durationSize":I
    iget v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mEasyModeDurationTextMargin:I

    .line 1879
    .local v1, "durationMargin":I
    const-string v9, "roboto-regular"

    invoke-static {v9, v11}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v7

    .line 1889
    :goto_0
    int-to-float v9, v2

    invoke-virtual {v0, v11, v9}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1890
    iput v1, v4, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 1891
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1892
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->getTotalTimeView()Landroid/widget/TextView;

    move-result-object v8

    .line 1893
    .local v8, "total":Landroid/widget/TextView;
    invoke-virtual {v8}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    .end local v4    # "params":Landroid/view/ViewGroup$MarginLayoutParams;
    check-cast v4, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1894
    .restart local v4    # "params":Landroid/view/ViewGroup$MarginLayoutParams;
    int-to-float v9, v2

    invoke-virtual {v8, v11, v9}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1895
    iput v1, v4, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 1896
    invoke-virtual {v8, v4}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1898
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->getTitleTextView()Landroid/widget/TextView;

    move-result-object v9

    int-to-float v10, v3

    invoke-virtual {v9, v11, v10}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1899
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->getTitleTextView()Landroid/widget/TextView;

    move-result-object v9

    invoke-virtual {v9, v7}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 1900
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->getArtistTextView()Landroid/widget/TextView;

    move-result-object v9

    int-to-float v10, v6

    invoke-virtual {v9, v11, v10}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1901
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->getArtistTextView()Landroid/widget/TextView;

    move-result-object v9

    invoke-virtual {v9, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1902
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->getAlbumTextView()Landroid/widget/TextView;

    move-result-object v9

    int-to-float v10, v6

    invoke-virtual {v9, v11, v10}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 1903
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->getAlbumTextView()Landroid/widget/TextView;

    move-result-object v9

    invoke-virtual {v9, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 1904
    return-void

    .line 1881
    .end local v1    # "durationMargin":I
    .end local v2    # "durationSize":I
    .end local v3    # "mainTextSize":I
    .end local v5    # "subColor":I
    .end local v6    # "subTextSize":I
    .end local v8    # "total":Landroid/widget/TextView;
    :cond_0
    iget v3, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mTextMainSize:I

    .line 1882
    .restart local v3    # "mainTextSize":I
    iget v6, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mTextSubSize:I

    .line 1883
    .restart local v6    # "subTextSize":I
    iget v5, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mTitleSubColor:I

    .line 1884
    .restart local v5    # "subColor":I
    iget v2, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mDurationSize:I

    .line 1885
    .restart local v2    # "durationSize":I
    iget v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mDurationTextMargin:I

    .line 1886
    .restart local v1    # "durationMargin":I
    const-string v9, "roboto-light"

    invoke-static {v9, v11}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v7

    goto :goto_0
.end method

.method private emitParticles(IFFI)V
    .locals 1
    .param p1, "amount"    # I
    .param p2, "x"    # F
    .param p3, "y"    # F
    .param p4, "color"    # I

    .prologue
    .line 2419
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mParticleView:Landroid/view/View;

    invoke-static {v0, p1, p2, p3, p4}, Lcom/samsung/musicplus/library/view/ParticleEffectCompat;->addDots(Landroid/view/View;IFFI)V

    .line 2420
    return-void
.end method

.method private ensureEasyModeView(Landroid/view/View;)V
    .locals 2
    .param p1, "root"    # Landroid/view/View;

    .prologue
    const/4 v1, 0x1

    .line 1793
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mShuffleText:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mRepeatText:Landroid/widget/TextView;

    if-nez v0, :cond_2

    .line 1794
    :cond_0
    const v0, 0x7f0d011e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mShuffleText:Landroid/widget/TextView;

    .line 1795
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mShuffleText:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 1796
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mShuffleText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setSelected(Z)V

    .line 1797
    :cond_1
    const v0, 0x7f0d0120

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mRepeatText:Landroid/widget/TextView;

    .line 1798
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mRepeatText:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    .line 1799
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mRepeatText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setSelected(Z)V

    .line 1801
    :cond_2
    return-void
.end method

.method private ensureLyricView()Z
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 989
    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mLyricView:Lcom/samsung/musicplus/player/LyricScrollView;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mLyricText:Landroid/widget/TextView;

    if-eqz v2, :cond_0

    move v2, v3

    .line 1016
    :goto_0
    return v2

    .line 992
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->getParentView()Landroid/view/View;

    move-result-object v0

    .line 993
    .local v0, "root":Landroid/view/View;
    const v2, 0x7f0d010d

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewStub;

    .line 995
    .local v1, "stub":Landroid/view/ViewStub;
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->isNowPlayingListShown()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 996
    iput-boolean v4, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mLyricViewShowing:Z

    .line 999
    :cond_1
    if-eqz v1, :cond_2

    .line 1000
    invoke-virtual {v1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 1001
    const v2, 0x7f0d005d

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/samsung/musicplus/player/LyricScrollView;

    iput-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mLyricView:Lcom/samsung/musicplus/player/LyricScrollView;

    .line 1003
    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mLyricView:Lcom/samsung/musicplus/player/LyricScrollView;

    const v5, 0x7f0d005f

    invoke-virtual {v2, v5}, Lcom/samsung/musicplus/player/LyricScrollView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ScrollView;

    iput-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mLyricScroll:Landroid/widget/ScrollView;

    .line 1004
    const v2, 0x7f0d0060

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mLyricText:Landroid/widget/TextView;

    .line 1005
    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mLyricText:Landroid/widget/TextView;

    new-instance v5, Lcom/samsung/musicplus/player/FullPlayerView$13;

    invoke-direct {v5, p0}, Lcom/samsung/musicplus/player/FullPlayerView$13;-><init>(Lcom/samsung/musicplus/player/FullPlayerView;)V

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 1012
    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mLyricView:Lcom/samsung/musicplus/player/LyricScrollView;

    invoke-virtual {v2, v4}, Lcom/samsung/musicplus/player/LyricScrollView;->setFocusable(Z)V

    .line 1013
    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mLyricView:Lcom/samsung/musicplus/player/LyricScrollView;

    invoke-virtual {v2}, Lcom/samsung/musicplus/player/LyricScrollView;->bringToFront()V

    move v2, v3

    .line 1014
    goto :goto_0

    :cond_2
    move v2, v4

    .line 1016
    goto :goto_0
.end method

.method private ensureTopTitleView()V
    .locals 3

    .prologue
    .line 1298
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mTopTitleView:Landroid/view/View;

    if-nez v1, :cond_0

    .line 1299
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->getParentView()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f0d010e

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    .line 1300
    .local v0, "stub":Landroid/view/ViewStub;
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mTopTitleView:Landroid/view/View;

    .line 1301
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mTopTitleView:Landroid/view/View;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1303
    .end local v0    # "stub":Landroid/view/ViewStub;
    :cond_0
    return-void
.end method

.method private ensureVolumeDmrPanel()Z
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 3102
    iget-object v5, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mVolumeDmrPanel:Landroid/view/View;

    if-eqz v5, :cond_0

    .line 3103
    iget-object v4, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mVolumeDmrPanel:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->bringToFront()V

    .line 3147
    :goto_0
    return v3

    .line 3107
    :cond_0
    const v5, 0x7f04006a

    const v6, 0x7f0d012f

    invoke-direct {p0, v5, v6}, Lcom/samsung/musicplus/player/FullPlayerView;->inflateVolumePanel(II)Landroid/view/View;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mVolumeDmrPanel:Landroid/view/View;

    .line 3110
    iget-object v5, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mVolumeDmrPanel:Landroid/view/View;

    if-eqz v5, :cond_1

    .line 3111
    iget-object v5, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mVolumeDmrPanel:Landroid/view/View;

    const v6, 0x7f0d0130

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 3112
    .local v2, "volumeUp":Landroid/view/View;
    iget-object v5, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mVolumeDmrPanel:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->bringToFront()V

    .line 3113
    iget-object v5, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    const v6, 0x7f1001ab

    invoke-virtual {v5, v6}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 3114
    new-instance v5, Lcom/samsung/musicplus/player/FullPlayerView$23;

    invoke-direct {v5, p0}, Lcom/samsung/musicplus/player/FullPlayerView$23;-><init>(Lcom/samsung/musicplus/player/FullPlayerView;)V

    invoke-virtual {v2, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 3122
    iget-object v5, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mVolumeDmrPanel:Landroid/view/View;

    const v6, 0x7f0d0131

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 3123
    .local v1, "volumeDown":Landroid/view/View;
    iget-object v5, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    const v6, 0x7f1001a9

    invoke-virtual {v5, v6}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 3124
    new-instance v5, Lcom/samsung/musicplus/player/FullPlayerView$24;

    invoke-direct {v5, p0}, Lcom/samsung/musicplus/player/FullPlayerView$24;-><init>(Lcom/samsung/musicplus/player/FullPlayerView;)V

    invoke-virtual {v1, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 3132
    iget-object v5, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mVolumeDmrPanel:Landroid/view/View;

    const v6, 0x7f0d0132

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 3133
    .local v0, "mute":Landroid/view/View;
    iget-object v5, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    const v6, 0x7f1001aa

    invoke-virtual {v5, v6}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v5

    invoke-virtual {v0, v5}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 3134
    new-instance v5, Lcom/samsung/musicplus/player/FullPlayerView$25;

    invoke-direct {v5, p0}, Lcom/samsung/musicplus/player/FullPlayerView$25;-><init>(Lcom/samsung/musicplus/player/FullPlayerView;)V

    invoke-virtual {v0, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 3142
    invoke-virtual {p0, v2, v4}, Lcom/samsung/musicplus/player/FullPlayerView;->setAirView(Landroid/view/View;I)V

    .line 3143
    invoke-virtual {p0, v1, v4}, Lcom/samsung/musicplus/player/FullPlayerView;->setAirView(Landroid/view/View;I)V

    goto :goto_0

    .end local v0    # "mute":Landroid/view/View;
    .end local v1    # "volumeDown":Landroid/view/View;
    .end local v2    # "volumeUp":Landroid/view/View;
    :cond_1
    move v3, v4

    .line 3147
    goto :goto_0
.end method

.method private ensureVolumePanel()Z
    .locals 5

    .prologue
    const/4 v4, 0x3

    const/4 v2, 0x1

    .line 2854
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mVolumePanel:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 2855
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mVolumePanel:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->bringToFront()V

    move v1, v2

    .line 2915
    :goto_0
    return v1

    .line 2859
    :cond_0
    const v1, 0x7f040069

    const v3, 0x7f0d012b

    invoke-direct {p0, v1, v3}, Lcom/samsung/musicplus/player/FullPlayerView;->inflateVolumePanel(II)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mVolumePanel:Landroid/view/View;

    .line 2860
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mVolumePanel:Landroid/view/View;

    if-eqz v1, :cond_3

    .line 2861
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mVolumePanel:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->bringToFront()V

    .line 2862
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mVolumePanel:Landroid/view/View;

    const v3, 0x7f0d012c

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mVolumeText:Landroid/widget/TextView;

    .line 2863
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mVolumePanel:Landroid/view/View;

    const v3, 0x7f0d012e

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/SeekBar;

    iput-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mVolumeSeekBar:Landroid/widget/SeekBar;

    .line 2864
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mVolumeSeekBar:Landroid/widget/SeekBar;

    iget-object v3, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mVolumeChangeListener:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v1, v3}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 2865
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mVolumeSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v1, v4}, Landroid/widget/SeekBar;->setMode(I)V

    .line 2866
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mVolumeSeekBar:Landroid/widget/SeekBar;

    iget-object v3, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

    invoke-virtual {v3, v4}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->getStreamMaxVolume(I)I

    move-result v3

    invoke-virtual {v1, v3}, Landroid/widget/SeekBar;->setMax(I)V

    .line 2868
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mVolumePanel:Landroid/view/View;

    const v3, 0x7f0d012d

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 2869
    .local v0, "soundAlive":Landroid/view/View;
    new-instance v1, Lcom/samsung/musicplus/player/FullPlayerView$20;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/player/FullPlayerView$20;-><init>(Lcom/samsung/musicplus/player/FullPlayerView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 2898
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/musicplus/util/UiUtils;->checkHoverUI(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2899
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/musicplus/library/hardware/AirView;->isSimpleAirView(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2900
    const/16 v1, 0x3031

    invoke-virtual {p0, v0, v1}, Lcom/samsung/musicplus/player/FullPlayerView;->setAirView(Landroid/view/View;I)V

    :cond_1
    :goto_1
    move v1, v2

    .line 2913
    goto :goto_0

    .line 2903
    :cond_2
    new-instance v1, Lcom/samsung/musicplus/player/FullPlayerView$21;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/player/FullPlayerView$21;-><init>(Lcom/samsung/musicplus/player/FullPlayerView;)V

    invoke-virtual {p0, v0, v1}, Lcom/samsung/musicplus/player/FullPlayerView;->setAirView(Landroid/view/View;Lcom/samsung/musicplus/library/hardware/AirView$OnAirViewPopupListener;)V

    goto :goto_1

    .line 2915
    .end local v0    # "soundAlive":Landroid/view/View;
    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getAverageColor(Landroid/graphics/Bitmap;)I
    .locals 1
    .param p1, "bm"    # Landroid/graphics/Bitmap;

    .prologue
    .line 348
    invoke-static {p1}, Lcom/samsung/musicplus/util/bitmap/CollectColor;->getMostImportantColor(Landroid/graphics/Bitmap;)I

    move-result v0

    return v0
.end method

.method private getBackgroundStartColor(Landroid/graphics/Bitmap;)I
    .locals 1
    .param p1, "bm"    # Landroid/graphics/Bitmap;

    .prologue
    .line 362
    if-nez p1, :cond_0

    iget v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mDefaultAverageColor:I

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/player/FullPlayerView;->getAverageColor(Landroid/graphics/Bitmap;)I

    move-result v0

    goto :goto_0
.end method

.method private getLyricLoader()Lcom/samsung/musicplus/util/LyricsLoader;
    .locals 1

    .prologue
    .line 881
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/musicplus/util/ID3TagLyricParser;->getInstance(Landroid/content/Context;)Lcom/samsung/musicplus/util/ID3TagLyricParser;

    move-result-object v0

    return-object v0
.end method

.method private getPlayingSongMoodValue()I
    .locals 4

    .prologue
    .line 2639
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getCurrentAudioId()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/samsung/musicplus/util/UiUtils;->getMoodValue(Landroid/content/Context;J)I

    move-result v0

    .line 2640
    .local v0, "mood":I
    const-string v1, "MusicSideCast"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "findSimilarSongGroup, moodId : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2642
    return v0
.end method

.method private getRandomSongs()[J
    .locals 14

    .prologue
    const/4 v13, 0x1

    const/4 v12, 0x0

    .line 2676
    const-string v0, "MusicSideCast"

    const-string v1, "findSimilarSongGroup, chooseRandomSongIds()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2678
    const/4 v6, 0x0

    .line 2679
    .local v6, "c":Landroid/database/Cursor;
    const/4 v8, 0x0

    .line 2681
    .local v8, "ids":[J
    :try_start_0
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/musicplus/provider/MusicContents;->MUSIC_CONTENTS_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView;->PLAYER_SIDE_CAST_COLUMNS:[Ljava/lang/String;

    const-string v3, "title != \'\' AND is_music=1"

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 2683
    invoke-static {v6}, Lcom/samsung/musicplus/util/ListUtils;->getSongListForCursor(Landroid/database/Cursor;)[J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v8

    .line 2685
    if-eqz v6, :cond_0

    .line 2686
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 2690
    :cond_0
    array-length v11, v8

    .line 2693
    .local v11, "size":I
    const/4 v9, 0x1

    .line 2695
    .local v9, "loop":I
    if-nez v11, :cond_3

    .line 2696
    const-string v0, "MusicSideCast"

    const-string v1, "findSimilarSongGroup - ramdomsong is null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2710
    :cond_1
    return-object v12

    .line 2685
    .end local v9    # "loop":I
    .end local v11    # "size":I
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 2686
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0

    .line 2698
    .restart local v9    # "loop":I
    .restart local v11    # "size":I
    :cond_3
    if-ne v11, v13, :cond_4

    .line 2699
    new-array v12, v13, [J

    .line 2705
    .local v12, "temp":[J
    :goto_0
    new-instance v10, Ljava/util/Random;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-direct {v10, v0, v1}, Ljava/util/Random;-><init>(J)V

    .line 2706
    .local v10, "rand":Ljava/util/Random;
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_1
    if-ge v7, v9, :cond_1

    .line 2707
    invoke-virtual {v10, v11}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    aget-wide v0, v8, v0

    aput-wide v0, v12, v7

    .line 2706
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 2701
    .end local v7    # "i":I
    .end local v10    # "rand":Ljava/util/Random;
    .end local v12    # "temp":[J
    :cond_4
    div-int/lit8 v9, v11, 0x2

    .line 2702
    new-array v12, v9, [J

    .restart local v12    # "temp":[J
    goto :goto_0
.end method

.method private getSimilarGenreSongs(Ljava/lang/String;)[J
    .locals 8
    .param p1, "genre"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    .line 2646
    if-nez p1, :cond_1

    .line 2648
    const-string v0, "MusicSideCast"

    const-string v1, "findSimilarSongGroup, genre is null. So it will go to random music."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v7

    .line 2670
    :cond_0
    :goto_0
    return-object v0

    .line 2653
    :cond_1
    const-string v0, "MusicSideCast"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "findSimilarSongGroup, genre : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2655
    const/4 v6, 0x0

    .line 2657
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/musicplus/provider/MusicContents;->MUSIC_CONTENTS_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView;->PLAYER_SIDE_CAST_COLUMNS:[Ljava/lang/String;

    const-string v3, "is_music=1 AND genre_name=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 2662
    if-eqz v6, :cond_2

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-gtz v0, :cond_4

    .line 2668
    :cond_2
    if-eqz v6, :cond_3

    .line 2669
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 2670
    const/4 v6, 0x0

    :cond_3
    move-object v0, v7

    goto :goto_0

    .line 2665
    :cond_4
    :try_start_1
    invoke-static {v6}, Lcom/samsung/musicplus/util/ListUtils;->getSongListForCursor(Landroid/database/Cursor;)[J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 2668
    if-eqz v6, :cond_0

    .line 2669
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 2670
    const/4 v6, 0x0

    goto :goto_0

    .line 2668
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_5

    .line 2669
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 2670
    const/4 v6, 0x0

    :cond_5
    throw v0
.end method

.method private getSimilarSongsQuerySelection()Lcom/samsung/musicplus/player/FullPlayerView$SideCastQueryInfo;
    .locals 9

    .prologue
    const/4 v6, 0x1

    const/4 v8, 0x0

    const/4 v4, 0x0

    .line 2589
    new-instance v1, Lcom/samsung/musicplus/player/FullPlayerView$SideCastQueryInfo;

    invoke-direct {v1}, Lcom/samsung/musicplus/player/FullPlayerView$SideCastQueryInfo;-><init>()V

    .line 2591
    .local v1, "info":Lcom/samsung/musicplus/player/FullPlayerView$SideCastQueryInfo;
    invoke-direct {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->getPlayingSongMoodValue()I

    move-result v2

    .line 2593
    .local v2, "mood":I
    sget-object v5, Lcom/samsung/musicplus/provider/MusicContents;->MUSIC_CONTENTS_URI:Landroid/net/Uri;

    iput-object v5, v1, Lcom/samsung/musicplus/player/FullPlayerView$SideCastQueryInfo;->uri:Landroid/net/Uri;

    .line 2594
    iget-object v5, p0, Lcom/samsung/musicplus/player/FullPlayerView;->PLAYER_SIDE_CAST_COLUMNS:[Ljava/lang/String;

    iput-object v5, v1, Lcom/samsung/musicplus/player/FullPlayerView$SideCastQueryInfo;->projection:[Ljava/lang/String;

    .line 2597
    if-ltz v2, :cond_2

    .line 2598
    iget-object v5, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    invoke-static {v5, v2}, Lcom/samsung/musicplus/util/UiUtils;->getSameMoodSongIds(Landroid/content/Context;I)[J

    move-result-object v3

    .line 2603
    .local v3, "songs":[J
    array-length v5, v3

    if-nez v5, :cond_0

    .line 2604
    new-array v3, v6, [J

    .line 2605
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getCurrentAudioId()J

    move-result-wide v6

    aput-wide v6, v3, v8

    .line 2607
    :cond_0
    const-string v5, "_id"

    invoke-static {v5, v3}, Lcom/samsung/musicplus/util/UiUtils;->convertAudioIdsToSelection(Ljava/lang/String;[J)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v1, Lcom/samsung/musicplus/player/FullPlayerView$SideCastQueryInfo;->selection:Ljava/lang/String;

    .line 2608
    iput-object v4, v1, Lcom/samsung/musicplus/player/FullPlayerView$SideCastQueryInfo;->selectionArgs:[Ljava/lang/String;

    .line 2635
    .end local v1    # "info":Lcom/samsung/musicplus/player/FullPlayerView$SideCastQueryInfo;
    :cond_1
    :goto_0
    return-object v1

    .line 2610
    .end local v3    # "songs":[J
    .restart local v1    # "info":Lcom/samsung/musicplus/player/FullPlayerView$SideCastQueryInfo;
    :cond_2
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getGenre()Ljava/lang/String;

    move-result-object v0

    .line 2611
    .local v0, "genre":Ljava/lang/String;
    const-string v5, "is_music=1 AND genre_name=?"

    iput-object v5, v1, Lcom/samsung/musicplus/player/FullPlayerView$SideCastQueryInfo;->selection:Ljava/lang/String;

    .line 2612
    new-array v5, v6, [Ljava/lang/String;

    aput-object v0, v5, v8

    iput-object v5, v1, Lcom/samsung/musicplus/player/FullPlayerView$SideCastQueryInfo;->selectionArgs:[Ljava/lang/String;

    .line 2616
    invoke-direct {p0, v0}, Lcom/samsung/musicplus/player/FullPlayerView;->getSimilarGenreSongs(Ljava/lang/String;)[J

    move-result-object v3

    .line 2617
    .restart local v3    # "songs":[J
    if-nez v3, :cond_1

    .line 2618
    invoke-direct {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->getRandomSongs()[J

    move-result-object v3

    .line 2623
    if-nez v3, :cond_3

    move-object v1, v4

    .line 2624
    goto :goto_0

    .line 2631
    :cond_3
    const-string v5, "_id"

    invoke-static {v5, v3}, Lcom/samsung/musicplus/util/UiUtils;->convertAudioIdsToSelection(Ljava/lang/String;[J)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v1, Lcom/samsung/musicplus/player/FullPlayerView$SideCastQueryInfo;->selection:Ljava/lang/String;

    .line 2632
    iput-object v4, v1, Lcom/samsung/musicplus/player/FullPlayerView$SideCastQueryInfo;->selectionArgs:[Ljava/lang/String;

    goto :goto_0
.end method

.method private handleAlbumFlingEvent(I)V
    .locals 6
    .param p1, "direction"    # I

    .prologue
    .line 366
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->getAlbumArtView()Landroid/widget/ImageView;

    move-result-object v0

    .line 367
    .local v0, "albumArt":Landroid/widget/ImageView;
    invoke-virtual {v0}, Landroid/widget/ImageView;->getAnimation()Landroid/view/animation/Animation;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 419
    :goto_0
    return-void

    .line 371
    :cond_0
    iget-object v4, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mOldAlbum:Landroid/widget/ImageView;

    if-nez v4, :cond_1

    .line 374
    new-instance v4, Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    invoke-direct {v4, v5}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    iput-object v4, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mOldAlbum:Landroid/widget/ImageView;

    .line 375
    iget-object v4, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mOldAlbum:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 376
    iget-object v4, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mOldAlbum:Landroid/widget/ImageView;

    sget-object v5, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 377
    iget-object v4, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mOldAlbum:Landroid/widget/ImageView;

    const v5, 0x7f020039

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 378
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->getParentView()Landroid/view/View;

    move-result-object v4

    const v5, 0x7f0d010c

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 380
    .local v1, "albumRoom":Landroid/view/ViewGroup;
    if-eqz v1, :cond_1

    .line 381
    iget-object v4, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mOldAlbum:Landroid/widget/ImageView;

    const/4 v5, 0x0

    invoke-virtual {v1, v4, v5}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 387
    .end local v1    # "albumRoom":Landroid/view/ViewGroup;
    :cond_1
    const/16 v4, 0x67

    if-ne p1, v4, :cond_2

    .line 388
    iget-object v4, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    const v5, 0x7f05000c

    invoke-static {v4, v5}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v2

    .line 389
    .local v2, "in":Landroid/view/animation/Animation;
    iget-object v4, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    const v5, 0x7f05000d

    invoke-static {v4, v5}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v3

    .line 394
    .local v3, "out":Landroid/view/animation/Animation;
    :goto_1
    new-instance v4, Lcom/samsung/musicplus/player/FullPlayerView$1;

    invoke-direct {v4, p0, p1, v2}, Lcom/samsung/musicplus/player/FullPlayerView$1;-><init>(Lcom/samsung/musicplus/player/FullPlayerView;ILandroid/view/animation/Animation;)V

    invoke-virtual {v3, v4}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 416
    const/4 v4, 0x4

    invoke-direct {p0, v4}, Lcom/samsung/musicplus/player/FullPlayerView;->setVisibilityLyricView(I)V

    .line 417
    iget-object v4, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mOldAlbum:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 418
    iget-object v4, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mOldAlbum:Landroid/widget/ImageView;

    invoke-virtual {v4, v3}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 391
    .end local v2    # "in":Landroid/view/animation/Animation;
    .end local v3    # "out":Landroid/view/animation/Animation;
    :cond_2
    iget-object v4, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    const v5, 0x7f05000b

    invoke-static {v4, v5}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v2

    .line 392
    .restart local v2    # "in":Landroid/view/animation/Animation;
    iget-object v4, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    const v5, 0x7f05000e

    invoke-static {v4, v5}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v3

    .restart local v3    # "out":Landroid/view/animation/Animation;
    goto :goto_1
.end method

.method private handleMotionEvent(Landroid/view/MotionEvent;Landroid/view/MotionEvent;)V
    .locals 5
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;

    .prologue
    .line 517
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v1

    .line 518
    .local v1, "distX":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    sub-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v2

    .line 520
    .local v2, "distY":F
    cmpl-float v3, v1, v2

    if-lez v3, :cond_1

    const/high16 v3, 0x42c80000    # 100.0f

    cmpl-float v3, v1, v3

    if-lez v3, :cond_1

    .line 521
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->isLyricViewShowing()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 522
    const/4 v3, 0x4

    invoke-direct {p0, v3}, Lcom/samsung/musicplus/player/FullPlayerView;->setVisibilityLyricView(I)V

    .line 524
    :cond_0
    invoke-direct {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->refreshTopTitleView()V

    .line 529
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->getAlbumView()Landroid/view/View;

    move-result-object v0

    .line 530
    .local v0, "albumView":Landroid/view/View;
    if-eqz v0, :cond_1

    .line 531
    invoke-virtual {v0, p1}, Landroid/view/View;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    .line 541
    .end local v0    # "albumView":Landroid/view/View;
    :cond_1
    return-void
.end method

.method private hasTopTitleView()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1291
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->getParentView()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f0d010e

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mTopTitleView:Landroid/view/View;

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private hideLyricView()V
    .locals 1

    .prologue
    .line 981
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mLyricView:Lcom/samsung/musicplus/player/LyricScrollView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mLyricView:Lcom/samsung/musicplus/player/LyricScrollView;

    invoke-virtual {v0}, Lcom/samsung/musicplus/player/LyricScrollView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 982
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mLyricView:Lcom/samsung/musicplus/player/LyricScrollView;

    invoke-virtual {v0}, Lcom/samsung/musicplus/player/LyricScrollView;->clearAnimation()V

    .line 983
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/player/FullPlayerView;->setVisibilityLyricView(I)V

    .line 984
    invoke-direct {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->refreshTopTitleView()V

    .line 986
    :cond_0
    return-void
.end method

.method private inflateVolumePanel(II)Landroid/view/View;
    .locals 5
    .param p1, "layout"    # I
    .param p2, "panelId"    # I

    .prologue
    .line 3159
    const/4 v3, 0x1

    iget-object v4, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget v4, v4, Landroid/content/res/Configuration;->orientation:I

    if-ne v3, v4, :cond_0

    .line 3161
    const v2, 0x7f0d0107

    .line 3168
    .local v2, "volumnRoot":I
    iget-object v4, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->getParentView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    invoke-static {v4, p1, v3}, Landroid/view/ViewStub;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 3170
    .local v0, "buttonRoot":Landroid/view/View;
    invoke-virtual {v0, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 3172
    .local v1, "target":Landroid/view/View;
    const/4 v3, 0x4

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 3173
    .end local v0    # "buttonRoot":Landroid/view/View;
    .end local v1    # "target":Landroid/view/View;
    .end local v2    # "volumnRoot":I
    :goto_0
    return-object v1

    .line 3165
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private initEasyModeViewAttr()V
    .locals 2

    .prologue
    .line 1804
    invoke-direct {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->doCheckInvalidCase()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1805
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0100

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mTextMainSize:I

    .line 1807
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0104

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mTextSubSize:I

    .line 1809
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0053

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mTitleSubColor:I

    .line 1810
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c00c9

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mDurationSize:I

    .line 1812
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c00ec

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mDurationTextMargin:I

    .line 1814
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0045

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mEasyModeTitleSubColor:I

    .line 1816
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c00d5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mEasyModeTextsize:I

    .line 1818
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c00d6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mEasyModeSubTextsize:I

    .line 1820
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c00cc

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mEasyModeDurationSize:I

    .line 1822
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c00d0

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mEasyModePaddingTop:I

    .line 1824
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c00ed

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mEasyModeProgressPaddingTop:I

    .line 1826
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c00ce

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mEasyModePaddingLeft:I

    .line 1828
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c00cf

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mEasyModePaddingRight:I

    .line 1830
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c00ca

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mEasyModeDurationTextMargin:I

    .line 1832
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c00cd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mEasyModeOptionTopMargin:I

    .line 1835
    :cond_0
    return-void
.end method

.method private initGLGallery(Landroid/view/View;)V
    .locals 6
    .param p1, "root"    # Landroid/view/View;

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x0

    .line 2266
    sget-object v2, Lcom/samsung/musicplus/player/FullPlayerView;->mAverageColors:Ljava/util/HashMap;

    if-nez v2, :cond_0

    .line 2267
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    sput-object v2, Lcom/samsung/musicplus/player/FullPlayerView;->mAverageColors:Ljava/util/HashMap;

    .line 2270
    :cond_0
    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    const v3, 0x7f020039

    invoke-static {v2, v3}, Lcom/samsung/musicplus/util/AlbumArtUtils;->getDefaulAlbumArt(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 2273
    .local v0, "defaultAlbumArt":Landroid/graphics/Bitmap;
    const v2, 0x7f0d0055

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/samsung/musicplus/glwidget/GLGalleryView;

    iput-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mGLAlbumArt:Lcom/samsung/musicplus/glwidget/GLGalleryView;

    .line 2275
    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mGLAlbumArt:Lcom/samsung/musicplus/glwidget/GLGalleryView;

    if-eqz v2, :cond_1

    .line 2276
    const v2, 0x7f0d0054

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 2277
    .local v1, "view":Landroid/view/View;
    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    .line 2279
    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mGLAlbumArt:Lcom/samsung/musicplus/glwidget/GLGalleryView;

    invoke-interface {v2, v4}, Lcom/samsung/musicplus/glwidget/GLGalleryView;->setVisibility(I)V

    .line 2280
    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mGLAlbumArt:Lcom/samsung/musicplus/glwidget/GLGalleryView;

    invoke-interface {v2, v4}, Lcom/samsung/musicplus/glwidget/GLGalleryView;->setOpaque(Z)V

    .line 2282
    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mGLAlbumArt:Lcom/samsung/musicplus/glwidget/GLGalleryView;

    invoke-interface {v2, v0}, Lcom/samsung/musicplus/glwidget/GLGalleryView;->setDefaultAlbumArt(Landroid/graphics/Bitmap;)V

    .line 2284
    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mGLAlbumArt:Lcom/samsung/musicplus/glwidget/GLGalleryView;

    iget-object v3, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mGLAlbumArtOnAnimationListener:Lcom/samsung/musicplus/glwidget/GLGalleryView$OnAnimationListener;

    invoke-interface {v2, v3}, Lcom/samsung/musicplus/glwidget/GLGalleryView;->setOnAnimationListener(Lcom/samsung/musicplus/glwidget/GLGalleryView$OnAnimationListener;)V

    .line 2286
    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mGLAlbumArt:Lcom/samsung/musicplus/glwidget/GLGalleryView;

    const/4 v3, 0x1

    invoke-interface {v2, v3}, Lcom/samsung/musicplus/glwidget/GLGalleryView;->setAdapterWrap(Z)V

    .line 2288
    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mGLAlbumArt:Lcom/samsung/musicplus/glwidget/GLGalleryView;

    new-instance v3, Lcom/samsung/musicplus/player/FullPlayerView$17;

    invoke-direct {v3, p0}, Lcom/samsung/musicplus/player/FullPlayerView$17;-><init>(Lcom/samsung/musicplus/player/FullPlayerView;)V

    invoke-interface {v2, v3}, Lcom/samsung/musicplus/glwidget/GLGalleryView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 2296
    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mGLAlbumArt:Lcom/samsung/musicplus/glwidget/GLGalleryView;

    check-cast v2, Landroid/view/View;

    invoke-direct {p0, v2}, Lcom/samsung/musicplus/player/FullPlayerView;->initSmartClip(Landroid/view/View;)V

    .line 2299
    .end local v1    # "view":Landroid/view/View;
    :cond_1
    const v2, 0x7f0d0126

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/samsung/musicplus/glwidget/GLGalleryView;

    iput-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mGLRecommendedMusic:Lcom/samsung/musicplus/glwidget/GLGalleryView;

    .line 2301
    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mGLRecommendedMusic:Lcom/samsung/musicplus/glwidget/GLGalleryView;

    if-eqz v2, :cond_3

    .line 2302
    const v2, 0x7f0d0125

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 2303
    .restart local v1    # "view":Landroid/view/View;
    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    .line 2305
    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mGLRecommendedMusic:Lcom/samsung/musicplus/glwidget/GLGalleryView;

    invoke-interface {v2, v4}, Lcom/samsung/musicplus/glwidget/GLGalleryView;->setVisibility(I)V

    .line 2306
    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mGLRecommendedMusic:Lcom/samsung/musicplus/glwidget/GLGalleryView;

    invoke-interface {v2, v4}, Lcom/samsung/musicplus/glwidget/GLGalleryView;->setOpaque(Z)V

    .line 2309
    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mGLRecommendedMusic:Lcom/samsung/musicplus/glwidget/GLGalleryView;

    invoke-interface {v2, v0}, Lcom/samsung/musicplus/glwidget/GLGalleryView;->setDefaultAlbumArt(Landroid/graphics/Bitmap;)V

    .line 2312
    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mRecommendedMusicAdapter:Lcom/samsung/musicplus/player/GLRecommendedMusicAdapter;

    if-eqz v2, :cond_2

    .line 2313
    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mGLRecommendedMusic:Lcom/samsung/musicplus/glwidget/GLGalleryView;

    iget-object v3, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mRecommendedMusicAdapter:Lcom/samsung/musicplus/player/GLRecommendedMusicAdapter;

    invoke-interface {v2, v3}, Lcom/samsung/musicplus/glwidget/GLGalleryView;->setAdapter(Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;)V

    .line 2315
    :cond_2
    new-instance v2, Lcom/samsung/musicplus/player/FullPlayerView$SideCastAsyncUpdater;

    iget-object v3, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-direct {v2, p0, v3}, Lcom/samsung/musicplus/player/FullPlayerView$SideCastAsyncUpdater;-><init>(Lcom/samsung/musicplus/player/FullPlayerView;Landroid/content/ContentResolver;)V

    iput-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mSideCastUpdater:Lcom/samsung/musicplus/player/FullPlayerView$SideCastAsyncUpdater;

    .line 2317
    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mGLRecommendedMusic:Lcom/samsung/musicplus/glwidget/GLGalleryView;

    new-instance v3, Lcom/samsung/musicplus/player/FullPlayerView$18;

    invoke-direct {v3, p0}, Lcom/samsung/musicplus/player/FullPlayerView$18;-><init>(Lcom/samsung/musicplus/player/FullPlayerView;)V

    invoke-interface {v2, v3}, Lcom/samsung/musicplus/glwidget/GLGalleryView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 2342
    .end local v1    # "view":Landroid/view/View;
    :cond_3
    return-void
.end method

.method private initParticlesView(Landroid/view/View;)V
    .locals 2
    .param p1, "root"    # Landroid/view/View;

    .prologue
    .line 2393
    sget-boolean v1, Lcom/samsung/musicplus/util/Version;->LIBRARY_OVER_API_2:Z

    if-nez v1, :cond_0

    .line 2408
    :goto_0
    return-void

    :cond_0
    move-object v0, p1

    .line 2397
    check-cast v0, Lcom/samsung/musicplus/widget/TouchNotifyRelativeLayout;

    .line 2398
    .local v0, "layout":Lcom/samsung/musicplus/widget/TouchNotifyRelativeLayout;
    sget-boolean v1, Lcom/samsung/musicplus/util/Features;->FLAG_SUPPORT_PARTICLE_EFFECT:Z

    if-eqz v1, :cond_1

    .line 2399
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/musicplus/library/view/ParticleEffectCompat;->getInstance(Landroid/content/Context;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mParticleView:Landroid/view/View;

    .line 2400
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mParticleView:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/widget/TouchNotifyRelativeLayout;->addView(Landroid/view/View;)V

    .line 2401
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mParticleView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->bringToFront()V

    .line 2405
    :cond_1
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mGLParticlesTouchNotifyListener:Lcom/samsung/musicplus/widget/TouchNotifyRelativeLayout$OnTouchNotifyListener;

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/widget/TouchNotifyRelativeLayout;->setOnTouchNotifyListener(Lcom/samsung/musicplus/widget/TouchNotifyRelativeLayout$OnTouchNotifyListener;)V

    goto :goto_0
.end method

.method private initSideCastView(Landroid/view/View;)V
    .locals 5
    .param p1, "root"    # Landroid/view/View;

    .prologue
    .line 2247
    const v0, 0x7f0d0125

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;

    iput-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mSideCastGridView:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;

    .line 2248
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mSideCastGridView:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;

    if-nez v0, :cond_0

    .line 2257
    :goto_0
    return-void

    .line 2252
    :cond_0
    new-instance v0, Lcom/samsung/musicplus/player/PlayerSideCastAdapter;

    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    const v2, 0x7f040064

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/musicplus/player/PlayerSideCastAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;I)V

    iput-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mSideCastAdapter:Lcom/samsung/musicplus/player/PlayerSideCastAdapter;

    .line 2254
    new-instance v0, Lcom/samsung/musicplus/player/FullPlayerView$SideCastAsyncUpdater;

    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/samsung/musicplus/player/FullPlayerView$SideCastAsyncUpdater;-><init>(Lcom/samsung/musicplus/player/FullPlayerView;Landroid/content/ContentResolver;)V

    iput-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mSideCastUpdater:Lcom/samsung/musicplus/player/FullPlayerView$SideCastAsyncUpdater;

    .line 2255
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mSideCastGridView:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;

    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mSideCastAdapter:Lcom/samsung/musicplus/player/PlayerSideCastAdapter;

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 2256
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mSideCastGridView:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;

    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mOnClickListener:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->setOnItemClickListener(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView$OnItemClickListener;)V

    goto :goto_0
.end method

.method private initSmartClip(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 2354
    new-instance v0, Lcom/samsung/musicplus/player/FullPlayerView$19;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/player/FullPlayerView$19;-><init>(Lcom/samsung/musicplus/player/FullPlayerView;)V

    invoke-static {p1, v0}, Lcom/samsung/android/smartclip/SmartClipMetaUtils;->setDataExtractionListener(Landroid/view/View;Lcom/samsung/android/smartclip/SmartClipDataExtractionListener;)Z

    .line 2386
    return-void
.end method

.method private initializeTopTitleView(Landroid/view/View;Z)V
    .locals 8
    .param p1, "v"    # Landroid/view/View;
    .param p2, "enableAirView"    # Z

    .prologue
    const/4 v6, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 1311
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mTitleTop:Landroid/widget/TextView;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mArtistTop:Landroid/widget/TextView;

    if-eqz v1, :cond_1

    .line 1356
    :cond_0
    :goto_0
    return-void

    .line 1315
    :cond_1
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    if-eq v1, v2, :cond_0

    .line 1318
    const v1, 0x7f0d0128

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mTitleTop:Landroid/widget/TextView;

    .line 1325
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mTitleTop:Landroid/widget/TextView;

    if-eqz v1, :cond_2

    .line 1326
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mTitleTop:Landroid/widget/TextView;

    invoke-virtual {v1, v4, v3}, Landroid/widget/TextView;->setLayerType(ILandroid/graphics/Paint;)V

    .line 1327
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mTitleTop:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setSelected(Z)V

    .line 1330
    :cond_2
    const v1, 0x7f0d0129

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mArtistTop:Landroid/widget/TextView;

    .line 1331
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mArtistTop:Landroid/widget/TextView;

    if-eqz v1, :cond_3

    .line 1332
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mArtistTop:Landroid/widget/TextView;

    invoke-virtual {v1, v4, v3}, Landroid/widget/TextView;->setLayerType(ILandroid/graphics/Paint;)V

    .line 1335
    :cond_3
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mAlbumTop:Landroid/widget/TextView;

    if-nez v1, :cond_4

    .line 1336
    const v1, 0x7f0d012a

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mAlbumTop:Landroid/widget/TextView;

    .line 1338
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mAlbumTop:Landroid/widget/TextView;

    if-eqz v1, :cond_4

    .line 1339
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mAlbumTop:Landroid/widget/TextView;

    invoke-virtual {v1, v4, v3}, Landroid/widget/TextView;->setLayerType(ILandroid/graphics/Paint;)V

    .line 1343
    :cond_4
    if-eqz p2, :cond_0

    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/musicplus/util/UiUtils;->checkHoverUI(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1348
    sget-boolean v1, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_SUPPORT_FINGER_AIR_VIEW:Z

    if-eqz v1, :cond_0

    .line 1349
    const v1, 0x7f0d0127

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1350
    .local v0, "hoverPlace":Landroid/view/View;
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mTitleTop:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mArtistTop:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mAlbumTop:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 1351
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mTitleTop:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mArtistTop:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mAlbumTop:Landroid/widget/TextView;

    move-object v5, p0

    move v7, v6

    invoke-static/range {v0 .. v7}, Lcom/samsung/musicplus/library/hardware/AirView;->setView(Landroid/view/View;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;ILcom/samsung/musicplus/library/hardware/AirView$OnAirViewPopupListener;ZZ)V

    goto/16 :goto_0
.end method

.method private initializeView(Landroid/view/View;)V
    .locals 3
    .param p1, "root"    # Landroid/view/View;

    .prologue
    const v2, 0x7f0d0121

    .line 1196
    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mStarButton:Landroid/widget/ImageView;

    .line 1197
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mStarButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 1198
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mStarButton:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1199
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mStarButton:Landroid/widget/ImageView;

    invoke-direct {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->isSupportFavorite()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1200
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mStarButton:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x2

    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v1, :cond_0

    .line 1203
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mStarButton:Landroid/widget/ImageView;

    const v1, 0x7f0d00f4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setNextFocusDownId(I)V

    .line 1204
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->getSeekBarView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setNextFocusUpId(I)V

    .line 1211
    :cond_0
    const v0, 0x7f0d011a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mAddPlaylist:Landroid/widget/ImageView;

    .line 1212
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mAddPlaylist:Landroid/widget/ImageView;

    if-eqz v0, :cond_3

    .line 1213
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mAddPlaylist:Landroid/widget/ImageView;

    invoke-virtual {v0, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1220
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    const v1, 0x7f050007

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mLyricDownOut:Landroid/view/animation/Animation;

    .line 1221
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    const v1, 0x7f050008

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mLyricUpIn:Landroid/view/animation/Animation;

    .line 1223
    const v0, 0x7f0d011d

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mShuffleGroup:Landroid/widget/LinearLayout;

    .line 1224
    const v0, 0x7f0d011f

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mRepeatGroup:Landroid/widget/LinearLayout;

    .line 1226
    const v0, 0x7f0d00f7

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mShuffleBtn:Landroid/widget/ImageView;

    .line 1227
    const v0, 0x7f0d00f8

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mRepeatBtn:Landroid/widget/ImageView;

    .line 1230
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/player/FullPlayerView;->initGLGallery(Landroid/view/View;)V

    .line 1234
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/player/FullPlayerView;->initParticlesView(Landroid/view/View;)V

    .line 1235
    invoke-direct {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->updateEasyModeView()V

    .line 1236
    return-void

    .line 1199
    :cond_2
    const/16 v0, 0x8

    goto/16 :goto_0

    .line 1215
    :cond_3
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v1, "add_to_playlist"

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1217
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/player/FullPlayerView;->hideAddToPlaylist(Z)V

    goto :goto_1
.end method

.method private isConnectedWfd()Z
    .locals 4

    .prologue
    .line 2185
    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    const-string v3, "display"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/display/DisplayManager;

    .line 2186
    .local v0, "dm":Landroid/hardware/display/DisplayManager;
    invoke-static {v0}, Lcom/samsung/musicplus/library/wifi/DisplayManagerCompat;->getWifiDisplayStatus(Landroid/hardware/display/DisplayManager;)Lcom/samsung/musicplus/library/wifi/WifiDisplayStatusCompat;

    move-result-object v1

    .line 2187
    .local v1, "status":Lcom/samsung/musicplus/library/wifi/WifiDisplayStatusCompat;
    if-eqz v1, :cond_0

    const/4 v2, 0x2

    invoke-virtual {v1}, Lcom/samsung/musicplus/library/wifi/WifiDisplayStatusCompat;->getActiveDisplayState()I

    move-result v3

    if-ne v2, v3, :cond_0

    .line 2190
    const/4 v2, 0x1

    .line 2192
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private isSupportAddPlayList()Z
    .locals 2

    .prologue
    .line 1504
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getListType()I

    move-result v0

    .line 1505
    .local v0, "listType":I
    const v1, 0x2000d

    if-eq v0, v1, :cond_0

    const v1, 0x2000b

    if-eq v0, v1, :cond_0

    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/musicplus/util/UiUtils;->isEasyMode(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private isSupportFavorite()Z
    .locals 2

    .prologue
    .line 1498
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getListType()I

    move-result v0

    .line 1499
    .local v0, "listType":I
    const v1, 0x2000d

    if-eq v0, v1, :cond_0

    const v1, 0x2000b

    if-eq v0, v1, :cond_0

    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/musicplus/util/UiUtils;->isEasyMode(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private particlesCountForMotionEvent(Landroid/view/MotionEvent;)I
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 2426
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 2432
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 2428
    :pswitch_1
    const/16 v0, 0xf

    goto :goto_0

    .line 2430
    :pswitch_2
    const/4 v0, 0x3

    goto :goto_0

    .line 2426
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private refreshTopTitleView()V
    .locals 1

    .prologue
    .line 1260
    invoke-direct {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->hasTopTitleView()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1261
    invoke-direct {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->updateTopTitleView()V

    .line 1262
    invoke-direct {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->updateTopTitleInfo()V

    .line 1263
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->setVisibleIfLyricOn()V

    .line 1265
    :cond_0
    return-void
.end method

.method private requsetPlayNewSong(I)V
    .locals 3
    .param p1, "position"    # I

    .prologue
    const/high16 v2, 0x3f800000    # 1.0f

    .line 658
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->isPlaying()Z

    move-result v0

    invoke-static {p1, v0}, Lcom/samsung/musicplus/util/ServiceUtils;->setQueuePosition(IZ)V

    .line 659
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mVolumeLimitController:Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;

    const/16 v1, 0x4b0

    invoke-virtual {v0, v2, v2, v1}, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;->setLimitVolume(FFI)V

    .line 660
    return-void
.end method

.method private resetHideVolumeTimer()V
    .locals 4

    .prologue
    const/4 v1, 0x2

    .line 3009
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mUiHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 3010
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mUiHandler:Landroid/os/Handler;

    const-wide/16 v2, 0xbb8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 3011
    return-void
.end method

.method private restoreDefaultDBAVGColor(Landroid/os/Bundle;)I
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/util/NoSuchElementException;
        }
    .end annotation

    .prologue
    .line 436
    const-string v0, "DEFAULT_BG_AVG_COLOR_SAVED"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 437
    new-instance v0, Ljava/util/NoSuchElementException;

    const-string v1, "Saved state does not contain DEFAULT_BG_AVG_COLOR_SAVED key"

    invoke-direct {v0, v1}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 440
    :cond_0
    const-string v0, "DEFAULT_BG_AVG_COLOR_SAVED"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method private restoreSavedBackgroundColor(Landroid/os/Bundle;)I
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/util/NoSuchElementException;
        }
    .end annotation

    .prologue
    .line 427
    const-string v1, "CURRENT_COLOR_SAVED"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 428
    new-instance v1, Ljava/util/NoSuchElementException;

    const-string v2, "Saved state does not contain CURRENT_COLOR_SAVED key"

    invoke-direct {v1, v2}, Ljava/util/NoSuchElementException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 431
    :cond_0
    const-string v1, "CURRENT_COLOR_SAVED"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 432
    .local v0, "restoredColor":I
    return v0
.end method

.method private setBackground(I)V
    .locals 2
    .param p1, "color"    # I

    .prologue
    .line 307
    iget v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mCurrentColor:I

    if-ne v0, p1, :cond_1

    .line 317
    :cond_0
    :goto_0
    return-void

    .line 310
    :cond_1
    iput p1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mCurrentColor:I

    .line 311
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mBackgroundColors:[I

    const/4 v1, 0x0

    aput p1, v0, v1

    .line 312
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mBackgroundDrawable:Landroid/graphics/drawable/GradientDrawable;

    sget-object v1, Landroid/graphics/drawable/GradientDrawable$Orientation;->TOP_BOTTOM:Landroid/graphics/drawable/GradientDrawable$Orientation;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setOrientation(Landroid/graphics/drawable/GradientDrawable$Orientation;)V

    .line 313
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mBackgroundDrawable:Landroid/graphics/drawable/GradientDrawable;

    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mBackgroundColors:[I

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setColors([I)V

    .line 314
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mParentView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 315
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mParentView:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mBackgroundDrawable:Landroid/graphics/drawable/GradientDrawable;

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method private setButtonsNextFocus(I)V
    .locals 2
    .param p1, "nextViewId"    # I

    .prologue
    .line 1049
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->getShuffleButtonView()Landroid/widget/ImageView;

    move-result-object v1

    .line 1050
    .local v1, "shuffle":Landroid/widget/ImageView;
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->getRepeatButtonView()Landroid/widget/ImageView;

    move-result-object v0

    .line 1052
    .local v0, "repeat":Landroid/widget/ImageView;
    if-eqz v1, :cond_0

    .line 1053
    invoke-virtual {v1, p1}, Landroid/widget/ImageView;->setNextFocusUpId(I)V

    .line 1055
    :cond_0
    if-eqz v0, :cond_1

    .line 1056
    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setNextFocusUpId(I)V

    .line 1058
    :cond_1
    return-void
.end method

.method private setChangePlayerItemConnected(Z)V
    .locals 2
    .param p1, "isConnected"    # Z

    .prologue
    .line 2177
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mChangePlayerItem:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    .line 2178
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mChangePlayerItem:Landroid/view/MenuItem;

    if-eqz p1, :cond_1

    const v0, 0x7f020076

    :goto_0
    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 2180
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mChangePlayerItem:Landroid/view/MenuItem;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 2182
    :cond_0
    return-void

    .line 2178
    :cond_1
    const v0, 0x7f020078

    goto :goto_0
.end method

.method private setChangePlayerItemVisibility(Z)V
    .locals 1
    .param p1, "visible"    # Z

    .prologue
    .line 2163
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mChangePlayerItem:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    .line 2164
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mChangePlayerItem:Landroid/view/MenuItem;

    invoke-interface {v0, p1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 2166
    :cond_0
    return-void
.end method

.method private setEmptyView()V
    .locals 2

    .prologue
    .line 2720
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mEmptyViewStub:Landroid/view/View;

    if-nez v0, :cond_0

    .line 2721
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->getParentView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0d008e

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mEmptyViewStub:Landroid/view/View;

    .line 2722
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mEmptyViewStub:Landroid/view/View;

    instance-of v0, v0, Landroid/view/ViewStub;

    if-eqz v0, :cond_0

    .line 2723
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mEmptyViewStub:Landroid/view/View;

    check-cast v0, Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 2726
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mEmptyViewStub:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2727
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->setEmptyViewText()V

    .line 2728
    invoke-direct {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->setEmptyViewIcon()V

    .line 2729
    return-void
.end method

.method private setEmptyViewIcon()V
    .locals 2

    .prologue
    .line 2741
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->getParentView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0d0048

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mNoItemImageView:Landroid/widget/ImageView;

    .line 2742
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mNoItemImageView:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2743
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mGLRecommendedMusic:Lcom/samsung/musicplus/glwidget/GLGalleryView;

    if-eqz v0, :cond_0

    .line 2744
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mGLRecommendedMusic:Lcom/samsung/musicplus/glwidget/GLGalleryView;

    const/4 v1, 0x4

    invoke-interface {v0, v1}, Lcom/samsung/musicplus/glwidget/GLGalleryView;->setVisibility(I)V

    .line 2746
    :cond_0
    return-void
.end method

.method private setInvisibleEmptyView()V
    .locals 2

    .prologue
    .line 2749
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mEmptyViewStub:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 2750
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mEmptyViewStub:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 2752
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mGLRecommendedMusic:Lcom/samsung/musicplus/glwidget/GLGalleryView;

    if-eqz v0, :cond_1

    .line 2753
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mGLRecommendedMusic:Lcom/samsung/musicplus/glwidget/GLGalleryView;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/samsung/musicplus/glwidget/GLGalleryView;->setVisibility(I)V

    .line 2755
    :cond_1
    return-void
.end method

.method private setInvisibleTitleView()V
    .locals 4

    .prologue
    const/4 v3, 0x4

    .line 1394
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mTopTitleView:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 1395
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mTopTitleView:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1397
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->isSideCastView()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1398
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->getParentView()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f0d0111

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1400
    .local v0, "titleBottomView":Landroid/view/View;
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1401
    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 1403
    .end local v0    # "titleBottomView":Landroid/view/View;
    :cond_1
    return-void
.end method

.method private setVisibilityListButton(Z)V
    .locals 2
    .param p1, "easyMode"    # Z

    .prologue
    .line 2196
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->getListButtonView()Landroid/view/View;

    move-result-object v0

    .line 2197
    .local v0, "v":Landroid/view/View;
    if-nez v0, :cond_1

    .line 2208
    :cond_0
    :goto_0
    return-void

    .line 2201
    :cond_1
    if-eqz p1, :cond_2

    .line 2202
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 2204
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->getAlbumViewVisibility()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2205
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private setVisibilityLyricView(I)V
    .locals 2
    .param p1, "visible"    # I

    .prologue
    const v0, 0x7f0d0121

    .line 1029
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mLyricView:Lcom/samsung/musicplus/player/LyricScrollView;

    if-eqz v1, :cond_1

    .line 1030
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mLyricView:Lcom/samsung/musicplus/player/LyricScrollView;

    invoke-virtual {v1, p1}, Lcom/samsung/musicplus/player/LyricScrollView;->setVisibility(I)V

    .line 1031
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mLyricView:Lcom/samsung/musicplus/player/LyricScrollView;

    invoke-virtual {v0}, Lcom/samsung/musicplus/player/LyricScrollView;->getId()I

    move-result v0

    :cond_0
    invoke-direct {p0, v0}, Lcom/samsung/musicplus/player/FullPlayerView;->setButtonsNextFocus(I)V

    .line 1036
    :goto_0
    return-void

    .line 1034
    :cond_1
    invoke-direct {p0, v0}, Lcom/samsung/musicplus/player/FullPlayerView;->setButtonsNextFocus(I)V

    goto :goto_0
.end method

.method private setVolume(I)V
    .locals 3
    .param p1, "progress"    # I

    .prologue
    .line 3091
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

    const/4 v1, 0x3

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->setStreamVolume(III)V

    .line 3092
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->showVolumePanel()V

    .line 3093
    return-void
.end method

.method private showAddToPlaylistView()V
    .locals 8

    .prologue
    const v7, 0x7f0d0107

    const v6, 0x7f060001

    const/high16 v5, 0x7f060000

    const/4 v4, 0x1

    .line 1978
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->hideVolumePanel()V

    .line 1979
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->getAlbumViewVisibility()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1980
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->togglePlayerView()V

    .line 1983
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->isLyricViewShowing()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1984
    iput-boolean v4, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mKeepLyricShowingState:Z

    .line 1985
    invoke-direct {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->setInvisibleTitleView()V

    .line 1986
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->toggleLyricView()Z

    .line 1989
    :cond_1
    iget-object v3, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    check-cast v3, Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    .line 1990
    .local v2, "manager":Landroid/app/FragmentManager;
    iput-boolean v4, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mAddToPlayListShowing:Z

    .line 1991
    const-string v3, "add_to_playlist"

    invoke-virtual {v2, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 2009
    :goto_0
    return-void

    .line 1994
    :cond_2
    invoke-virtual {v2}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 1995
    .local v0, "ft":Landroid/app/FragmentTransaction;
    invoke-virtual {v0, v5, v6, v5, v6}, Landroid/app/FragmentTransaction;->setCustomAnimations(IIII)Landroid/app/FragmentTransaction;

    .line 1997
    const-string v3, "add_to_playlist"

    invoke-virtual {v0, v3}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 1998
    invoke-static {}, Lcom/samsung/musicplus/contents/extra/AddToPlaylistFragment;->getNewInstance()Landroid/app/Fragment;

    move-result-object v3

    const-string v4, "add_to_playlist"

    invoke-virtual {v0, v7, v3, v4}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 2000
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 2002
    iget-object v3, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mParentView:Landroid/view/View;

    invoke-virtual {v3, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 2003
    .local v1, "fullPlayerView":Landroid/view/View;
    if-eqz v1, :cond_3

    .line 2004
    invoke-virtual {v1}, Landroid/view/View;->bringToFront()V

    .line 2007
    :cond_3
    const v3, 0x7f0d008c

    invoke-direct {p0, v3}, Lcom/samsung/musicplus/player/FullPlayerView;->setButtonsNextFocus(I)V

    goto :goto_0
.end method

.method private showChangeDeviceDialog()V
    .locals 4

    .prologue
    .line 2099
    iget-boolean v2, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mPause:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "device_dialog"

    invoke-virtual {v2, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v2

    if-nez v2, :cond_0

    .line 2101
    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 2105
    .local v1, "ft":Landroid/app/FragmentTransaction;
    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    check-cast v2, Landroid/app/Activity;

    invoke-virtual {v2}, Landroid/app/Activity;->isResumed()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2106
    sget-boolean v2, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_SUPPORT_WIFIDISPLAY_OLD:Z

    if-eqz v2, :cond_1

    .line 2107
    new-instance v0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;

    invoke-direct {v0}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;-><init>()V

    .line 2108
    .local v0, "deviceDialog":Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;
    const-string v2, "device_dialog"

    invoke-virtual {v0, v1, v2}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->show(Landroid/app/FragmentTransaction;Ljava/lang/String;)I

    .line 2115
    .end local v0    # "deviceDialog":Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;
    .end local v1    # "ft":Landroid/app/FragmentTransaction;
    :cond_0
    :goto_0
    return-void

    .line 2110
    .restart local v1    # "ft":Landroid/app/FragmentTransaction;
    :cond_1
    new-instance v0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;

    invoke-direct {v0}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;-><init>()V

    .line 2111
    .local v0, "deviceDialog":Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;
    const-string v2, "device_dialog"

    invoke-virtual {v0, v1, v2}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->show(Landroid/app/FragmentTransaction;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private showSmartVolumeToast()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 2970
    sget-boolean v2, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_SMART_VOLUME:Z

    if-eqz v2, :cond_0

    .line 2971
    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    const-string v3, "music_service_pref"

    const/4 v4, 0x4

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 2973
    .local v0, "preference":Landroid/content/SharedPreferences;
    const-string v2, "smart_volume"

    invoke-interface {v0, v2, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 2974
    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->getStreamVolume(I)I

    move-result v1

    .line 2975
    .local v1, "volume":I
    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    const v3, 0x7f100162

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/musicplus/player/FullPlayerView;->showToast(Ljava/lang/String;)V

    .line 2978
    .end local v0    # "preference":Landroid/content/SharedPreferences;
    .end local v1    # "volume":I
    :cond_0
    return-void
.end method

.method private showToast(Ljava/lang/String;)V
    .locals 2
    .param p1, "message"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 2921
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mToast:Landroid/widget/Toast;

    if-nez v0, :cond_0

    .line 2922
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    invoke-static {v0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mToast:Landroid/widget/Toast;

    .line 2927
    :goto_0
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2928
    return-void

    .line 2924
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0, p1}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    .line 2925
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0, v1}, Landroid/widget/Toast;->setDuration(I)V

    goto :goto_0
.end method

.method private showWfdErrorDialog(I)V
    .locals 3
    .param p1, "exceptionalCase"    # I

    .prologue
    .line 2118
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/player/FullPlayerView;->availableWfdErrorDialogShowing(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2119
    new-instance v0, Lcom/samsung/musicplus/dialog/WfdErrorDialog;

    invoke-direct {v0, p1}, Lcom/samsung/musicplus/dialog/WfdErrorDialog;-><init>(I)V

    .line 2120
    .local v0, "dialog":Lcom/samsung/musicplus/dialog/WfdErrorDialog;
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "wfd_error"

    invoke-virtual {v0, v1, v2}, Lcom/samsung/musicplus/dialog/WfdErrorDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 2124
    .end local v0    # "dialog":Lcom/samsung/musicplus/dialog/WfdErrorDialog;
    :goto_0
    return-void

    .line 2122
    :cond_0
    invoke-direct {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->showChangeDeviceDialog()V

    goto :goto_0
.end method

.method private toggleStarred()V
    .locals 6

    .prologue
    .line 2211
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getCurrentPath()Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getCurrentAudioId()J

    move-result-wide v2

    iget-boolean v4, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mIsStarred:Z

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/samsung/musicplus/util/FileOperationTask;->toggleFavorites(Landroid/content/Context;Ljava/lang/String;JZZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mIsStarred:Z

    .line 2213
    return-void
.end method

.method private updateButtonThumbnailImage(Landroid/net/Uri;)V
    .locals 4
    .param p1, "albumArtUri"    # Landroid/net/Uri;

    .prologue
    .line 1595
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->getThumbnailButtonView()Landroid/view/View;

    move-result-object v1

    .line 1597
    .local v1, "v":Landroid/view/View;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-nez v2, :cond_0

    .line 1598
    if-nez p1, :cond_1

    .line 1599
    const/4 v2, 0x0

    invoke-super {p0, v2}, Lcom/samsung/musicplus/player/PlayerView;->updateButtonThumbnailImage(Landroid/graphics/Bitmap;)V

    .line 1622
    :cond_0
    :goto_0
    return-void

    .line 1601
    :cond_1
    invoke-static {}, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->getCache()Lcom/samsung/musicplus/bitmapcache/BitmapCache;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->getBitmap(Landroid/net/Uri;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1602
    .local v0, "b":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_2

    .line 1603
    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/player/FullPlayerView;->updateButtonThumbnailImage(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 1605
    :cond_2
    invoke-static {}, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->getCache()Lcom/samsung/musicplus/bitmapcache/BitmapCache;

    move-result-object v2

    new-instance v3, Lcom/samsung/musicplus/player/FullPlayerView$15;

    invoke-direct {v3, p0}, Lcom/samsung/musicplus/player/FullPlayerView$15;-><init>(Lcom/samsung/musicplus/player/FullPlayerView;)V

    invoke-virtual {v2, p1, v3}, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->loadBitmap(Landroid/net/Uri;Lcom/samsung/musicplus/bitmapcache/BitmapCache$Publisher;)V

    goto :goto_0
.end method

.method private updateEasyModeView()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 1772
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/musicplus/util/UiUtils;->isEasyMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1773
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->getParentView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/player/FullPlayerView;->ensureEasyModeView(Landroid/view/View;)V

    .line 1774
    invoke-virtual {p0, v2}, Lcom/samsung/musicplus/player/FullPlayerView;->initializeTitleView(Z)V

    .line 1775
    invoke-direct {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->initEasyModeViewAttr()V

    .line 1777
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mShuffleText:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mRepeatText:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 1778
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mShuffleText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1779
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mRepeatText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1781
    :cond_0
    invoke-direct {p0, v2}, Lcom/samsung/musicplus/player/FullPlayerView;->updateShuffleGroupPadding(Z)V

    .line 1782
    invoke-direct {p0, v2}, Lcom/samsung/musicplus/player/FullPlayerView;->updateRepeatGroupPadding(Z)V

    .line 1784
    invoke-direct {p0, v2}, Lcom/samsung/musicplus/player/FullPlayerView;->updateOptionViewMargin(Z)V

    .line 1786
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->getParentView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0d0123

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/player/FullPlayerView;->doUpdateSeekBarView(Landroid/widget/SeekBar;)V

    .line 1787
    invoke-direct {p0, v2}, Lcom/samsung/musicplus/player/FullPlayerView;->doUpdateTextView(Z)V

    .line 1788
    invoke-direct {p0, v2}, Lcom/samsung/musicplus/player/FullPlayerView;->updateProgressTextPadding(Z)V

    .line 1790
    :cond_1
    return-void
.end method

.method private updateExtraButtons()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1510
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mAddPlaylist:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 1511
    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mAddPlaylist:Landroid/widget/ImageView;

    invoke-direct {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->isSupportAddPlayList()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1513
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mStarButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    .line 1514
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mStarButton:Landroid/widget/ImageView;

    invoke-direct {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->isSupportFavorite()Z

    move-result v2

    if-eqz v2, :cond_3

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1516
    :cond_1
    return-void

    .line 1511
    :cond_2
    const/4 v0, 0x4

    goto :goto_0

    .line 1514
    :cond_3
    const/16 v1, 0x8

    goto :goto_1
.end method

.method private updateGLAlbumartPosition()V
    .locals 3

    .prologue
    .line 637
    const-string v0, "MusicUi"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "updateGLAlbumartPosition skipped: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mNoUpdateAlbumArtPosition:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 638
    iget-boolean v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mNoUpdateAlbumArtPosition:Z

    if-nez v0, :cond_0

    .line 639
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mGLAlbumArt:Lcom/samsung/musicplus/glwidget/GLGalleryView;

    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getListPosition()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/samsung/musicplus/glwidget/GLGalleryView;->setSelection(I)V

    .line 641
    :cond_0
    return-void
.end method

.method private updateNormalView()V
    .locals 3

    .prologue
    const/16 v1, 0x8

    const/4 v2, 0x0

    .line 1849
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mShuffleText:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mRepeatText:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 1850
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mShuffleText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1851
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mRepeatText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1853
    :cond_0
    invoke-direct {p0, v2}, Lcom/samsung/musicplus/player/FullPlayerView;->updateShuffleGroupPadding(Z)V

    .line 1854
    invoke-direct {p0, v2}, Lcom/samsung/musicplus/player/FullPlayerView;->updateRepeatGroupPadding(Z)V

    .line 1856
    invoke-direct {p0, v2}, Lcom/samsung/musicplus/player/FullPlayerView;->updateOptionViewMargin(Z)V

    .line 1858
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->getParentView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0d00f4

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/player/FullPlayerView;->doUpdateSeekBarView(Landroid/widget/SeekBar;)V

    .line 1859
    invoke-direct {p0, v2}, Lcom/samsung/musicplus/player/FullPlayerView;->doUpdateTextView(Z)V

    .line 1860
    invoke-direct {p0, v2}, Lcom/samsung/musicplus/player/FullPlayerView;->updateProgressTextPadding(Z)V

    .line 1861
    return-void
.end method

.method private updateOptionViewMargin(Z)V
    .locals 5
    .param p1, "easyMode"    # Z

    .prologue
    .line 1959
    iget-object v3, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c00e4

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 1961
    .local v2, "topMargin":I
    if-eqz p1, :cond_0

    .line 1962
    iget v2, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mEasyModeOptionTopMargin:I

    .line 1964
    :cond_0
    iget-object v3, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mParentView:Landroid/view/View;

    const v4, 0x7f0d0108

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1965
    .local v0, "optionView":Landroid/view/View;
    if-eqz v0, :cond_1

    .line 1966
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1967
    .local v1, "params":Landroid/view/ViewGroup$MarginLayoutParams;
    iput v2, v1, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 1968
    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1970
    .end local v1    # "params":Landroid/view/ViewGroup$MarginLayoutParams;
    :cond_1
    return-void
.end method

.method private updateProgressTextPadding(Z)V
    .locals 3
    .param p1, "easyMode"    # Z

    .prologue
    const/4 v2, 0x0

    .line 1946
    const/4 v0, 0x0

    .line 1947
    .local v0, "paddingTop":I
    if-eqz p1, :cond_0

    .line 1948
    iget v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mEasyModeProgressPaddingTop:I

    .line 1950
    :cond_0
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mCurrentTime:Landroid/widget/TextView;

    if-eqz v1, :cond_1

    .line 1951
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mCurrentTime:Landroid/widget/TextView;

    invoke-virtual {v1, v2, v0, v2, v2}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 1953
    :cond_1
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mTotalTime:Landroid/widget/TextView;

    if-eqz v1, :cond_2

    .line 1954
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mTotalTime:Landroid/widget/TextView;

    invoke-virtual {v1, v2, v0, v2, v2}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 1956
    :cond_2
    return-void
.end method

.method private updateRepeatGroupPadding(Z)V
    .locals 4
    .param p1, "easyMode"    # Z

    .prologue
    .line 1931
    const/4 v1, 0x0

    .line 1932
    .local v1, "marginTop":I
    const/4 v0, 0x0

    .line 1933
    .local v0, "marginRight":I
    if-eqz p1, :cond_0

    .line 1934
    iget v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mEasyModePaddingTop:I

    .line 1935
    iget v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mEasyModePaddingRight:I

    .line 1937
    :cond_0
    iget-object v3, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mRepeatBtn:Landroid/widget/ImageView;

    if-eqz v3, :cond_1

    .line 1938
    iget-object v3, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mRepeatBtn:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1939
    .local v2, "params":Landroid/view/ViewGroup$MarginLayoutParams;
    iput v1, v2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 1940
    iput v0, v2, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    .line 1941
    iget-object v3, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mRepeatBtn:Landroid/widget/ImageView;

    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1943
    .end local v2    # "params":Landroid/view/ViewGroup$MarginLayoutParams;
    :cond_1
    return-void
.end method

.method private updateShuffleGroupPadding(Z)V
    .locals 4
    .param p1, "easyMode"    # Z

    .prologue
    .line 1916
    const/4 v1, 0x0

    .line 1917
    .local v1, "marginTop":I
    const/4 v0, 0x0

    .line 1918
    .local v0, "marginLeft":I
    if-eqz p1, :cond_0

    .line 1919
    iget v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mEasyModePaddingTop:I

    .line 1920
    iget v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mEasyModePaddingLeft:I

    .line 1922
    :cond_0
    iget-object v3, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mShuffleBtn:Landroid/widget/ImageView;

    if-eqz v3, :cond_1

    .line 1923
    iget-object v3, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mShuffleBtn:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 1924
    .local v2, "params":Landroid/view/ViewGroup$MarginLayoutParams;
    iput v1, v2, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 1925
    iput v0, v2, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 1926
    iget-object v3, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mShuffleBtn:Landroid/widget/ImageView;

    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1928
    .end local v2    # "params":Landroid/view/ViewGroup$MarginLayoutParams;
    :cond_1
    return-void
.end method

.method private updateTagsView()V
    .locals 8

    .prologue
    const/16 v5, 0x8

    const/4 v7, 0x0

    .line 1462
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getSamplingRate()I

    move-result v2

    .line 1463
    .local v2, "samplingRate":I
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getBitDepth()I

    move-result v0

    .line 1464
    .local v0, "bitDepth":I
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->getParentView()Landroid/view/View;

    move-result-object v1

    .line 1466
    .local v1, "parent":Landroid/view/View;
    const v4, 0x7f0d0059

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 1468
    .local v3, "tagsStub":Landroid/view/View;
    instance-of v4, v3, Landroid/view/ViewStub;

    if-eqz v4, :cond_0

    .line 1469
    check-cast v3, Landroid/view/ViewStub;

    .end local v3    # "tagsStub":Landroid/view/View;
    invoke-virtual {v3}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 1470
    const v4, 0x7f0d005a

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    iput-object v4, p0, Lcom/samsung/musicplus/player/FullPlayerView;->uhqTag:Landroid/widget/ImageView;

    .line 1471
    const v4, 0x7f0d005b

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    iput-object v4, p0, Lcom/samsung/musicplus/player/FullPlayerView;->k2hdTag:Landroid/widget/ImageView;

    .line 1472
    const v4, 0x7f0d005c

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/samsung/musicplus/player/FullPlayerView;->tagsText:Landroid/widget/TextView;

    .line 1475
    :cond_0
    invoke-static {}, Lcom/samsung/musicplus/util/UiUtils;->isSupportUhqa()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-static {v2, v0}, Lcom/samsung/musicplus/util/UiUtils;->isUhqa(II)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getCurrentPath()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/musicplus/util/UiUtils;->isLocalContents(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1477
    iget-object v4, p0, Lcom/samsung/musicplus/player/FullPlayerView;->uhqTag:Landroid/widget/ImageView;

    invoke-virtual {v4, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1482
    :goto_0
    iget-object v4, p0, Lcom/samsung/musicplus/player/FullPlayerView;->uhqTag:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getVisibility()I

    move-result v4

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/samsung/musicplus/util/UiUtils;->isK2HD(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1483
    iget-object v4, p0, Lcom/samsung/musicplus/player/FullPlayerView;->k2hdTag:Landroid/widget/ImageView;

    invoke-virtual {v4, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 1488
    :goto_1
    iget-object v4, p0, Lcom/samsung/musicplus/player/FullPlayerView;->uhqTag:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getVisibility()I

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/samsung/musicplus/player/FullPlayerView;->k2hdTag:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getVisibility()I

    move-result v4

    if-nez v4, :cond_4

    .line 1489
    :cond_1
    iget-object v4, p0, Lcom/samsung/musicplus/player/FullPlayerView;->tagsText:Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    invoke-static {v6, v0}, Lcom/samsung/musicplus/util/UiUtils;->getBitDepth(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    invoke-static {v6, v2}, Lcom/samsung/musicplus/util/UiUtils;->getSamplingRate(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1491
    iget-object v4, p0, Lcom/samsung/musicplus/player/FullPlayerView;->tagsText:Landroid/widget/TextView;

    invoke-virtual {v4, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1495
    :goto_2
    return-void

    .line 1479
    :cond_2
    iget-object v4, p0, Lcom/samsung/musicplus/player/FullPlayerView;->uhqTag:Landroid/widget/ImageView;

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 1485
    :cond_3
    iget-object v4, p0, Lcom/samsung/musicplus/player/FullPlayerView;->k2hdTag:Landroid/widget/ImageView;

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    .line 1493
    :cond_4
    iget-object v4, p0, Lcom/samsung/musicplus/player/FullPlayerView;->tagsText:Landroid/widget/TextView;

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2
.end method

.method private updateTopTitleInfo()V
    .locals 3

    .prologue
    .line 1273
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mTitleTop:Landroid/widget/TextView;

    if-eqz v1, :cond_1

    .line 1276
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->getTitleTextView()Landroid/widget/TextView;

    move-result-object v0

    .line 1277
    .local v0, "title":Landroid/widget/TextView;
    if-nez v0, :cond_0

    .line 1278
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->updateSongInfo()V

    .line 1280
    :cond_0
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mTitleTop:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->getTitleTextView()Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1282
    .end local v0    # "title":Landroid/widget/TextView;
    :cond_1
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mArtistTop:Landroid/widget/TextView;

    if-eqz v1, :cond_2

    .line 1283
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mArtistTop:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->getArtistTextView()Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1285
    :cond_2
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mAlbumTop:Landroid/widget/TextView;

    if-eqz v1, :cond_3

    .line 1286
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mAlbumTop:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->getAlbumTextView()Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1288
    :cond_3
    return-void
.end method

.method private updateTopTitleView()V
    .locals 2

    .prologue
    .line 1268
    invoke-direct {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->ensureTopTitleView()V

    .line 1269
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mTopTitleView:Landroid/view/View;

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/samsung/musicplus/player/FullPlayerView;->initializeTopTitleView(Landroid/view/View;Z)V

    .line 1270
    return-void
.end method

.method private updateTotalCount(Landroid/database/Cursor;)V
    .locals 6
    .param p1, "data"    # Landroid/database/Cursor;

    .prologue
    const v5, 0x7f0f000a

    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 2553
    const/4 v0, 0x0

    .line 2554
    .local v0, "songCount":I
    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_1

    .line 2555
    :cond_0
    invoke-direct {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->setEmptyView()V

    .line 2556
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v1, v5, v0, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mSongCountString:Ljava/lang/String;

    .line 2563
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->getParentView()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f0d0124

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mSideCastTextView:Landroid/widget/TextView;

    .line 2564
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mSideCastTextView:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    const v4, 0x7f100172

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mSongCountString:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2566
    return-void

    .line 2559
    :cond_1
    invoke-direct {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->setInvisibleEmptyView()V

    .line 2560
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v1, v5, v0, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mSongCountString:Ljava/lang/String;

    goto :goto_0
.end method

.method private updateUhqTagView()V
    .locals 7

    .prologue
    .line 1431
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getCurrentPath()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/musicplus/util/UiUtils;->isLocalContents(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1432
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getSamplingRate()I

    move-result v2

    .line 1433
    .local v2, "samplingRate":I
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getBitDepth()I

    move-result v0

    .line 1434
    .local v0, "bitDepth":I
    invoke-static {}, Lcom/samsung/musicplus/util/UiUtils;->isSupportUhqa()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-static {v2, v0}, Lcom/samsung/musicplus/util/UiUtils;->isUhqa(II)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1435
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->getParentView()Landroid/view/View;

    move-result-object v1

    .line 1436
    .local v1, "parent":Landroid/view/View;
    const v4, 0x7f0d0057

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 1437
    .local v3, "uhaTagStub":Landroid/view/View;
    instance-of v4, v3, Landroid/view/ViewStub;

    if-eqz v4, :cond_0

    .line 1438
    check-cast v3, Landroid/view/ViewStub;

    .end local v3    # "uhaTagStub":Landroid/view/View;
    invoke-virtual {v3}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 1439
    const v4, 0x7f0d0065

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mUhaTagView:Landroid/widget/TextView;

    .line 1441
    :cond_0
    iget-object v4, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mUhaTagView:Landroid/widget/TextView;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    invoke-static {v6, v0}, Lcom/samsung/musicplus/util/UiUtils;->getBitDepth(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    invoke-static {v6, v2}, Lcom/samsung/musicplus/util/UiUtils;->getSamplingRate(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1443
    iget-object v4, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mUhaTagView:Landroid/widget/TextView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1451
    .end local v0    # "bitDepth":I
    .end local v1    # "parent":Landroid/view/View;
    .end local v2    # "samplingRate":I
    :cond_1
    :goto_0
    return-void

    .line 1448
    :cond_2
    iget-object v4, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mUhaTagView:Landroid/widget/TextView;

    if-eqz v4, :cond_1

    .line 1449
    iget-object v4, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mUhaTagView:Landroid/widget/TextView;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public appendSavedInstanceState(Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 444
    if-nez p1, :cond_0

    .line 445
    const/4 p1, 0x0

    .line 449
    .end local p1    # "outState":Landroid/os/Bundle;
    :goto_0
    return-object p1

    .line 447
    .restart local p1    # "outState":Landroid/os/Bundle;
    :cond_0
    const-string v0, "CURRENT_COLOR_SAVED"

    iget v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mCurrentColor:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 448
    const-string v0, "DEFAULT_BG_AVG_COLOR_SAVED"

    iget v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mDefaultAverageColor:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public checkWfdExceptionalCase()I
    .locals 3

    .prologue
    .line 2139
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    const-string v2, "display"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/display/DisplayManager;

    .line 2140
    .local v0, "dm":Landroid/hardware/display/DisplayManager;
    invoke-static {v0}, Lcom/samsung/musicplus/library/wifi/DisplayManagerCompat;->checkExceptionalCase(Landroid/hardware/display/DisplayManager;)I

    move-result v1

    return v1
.end method

.method public doAllShareButton()V
    .locals 1

    .prologue
    .line 2076
    sget-boolean v0, Lcom/samsung/musicplus/util/Version;->LIBRARY_OVER_API_1:Z

    if-eqz v0, :cond_0

    .line 2077
    invoke-direct {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->checkStateAndShowDialog()V

    .line 2081
    :goto_0
    return-void

    .line 2079
    :cond_0
    invoke-direct {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->showChangeDeviceDialog()V

    goto :goto_0
.end method

.method public getAirView(Landroid/view/View;)Landroid/view/View;
    .locals 4
    .param p1, "parent"    # Landroid/view/View;

    .prologue
    .line 1360
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1366
    invoke-super {p0, p1}, Lcom/samsung/musicplus/player/PlayerView;->getAirView(Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    .line 1362
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mTitleTop:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mArtistTop:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mAlbumTop:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/samsung/musicplus/player/FullPlayerView;->getTitleAirView(Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;I)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 1360
    :pswitch_data_0
    .packed-switch 0x7f0d0127
        :pswitch_0
    .end packed-switch
.end method

.method protected getAnimatedPlayButtonView(Landroid/view/View;)Lcom/samsung/musicplus/widget/AnimatedToggleButton;
    .locals 1
    .param p1, "root"    # Landroid/view/View;

    .prologue
    .line 3185
    const v0, 0x7f0d011c

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;

    return-object v0
.end method

.method public getSoundAliveButton()Landroid/view/View;
    .locals 1

    .prologue
    .line 3210
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mVolumePopupWindow:Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;

    invoke-virtual {v0}, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->getSoundAliveButton()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getVolumeSeekBar()Landroid/view/View;
    .locals 1

    .prologue
    .line 3214
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mVolumePopupWindow:Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;

    invoke-virtual {v0}, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->getVolumeSeekBar()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public handleAlbumFling(I)V
    .locals 1
    .param p1, "direction"    # I

    .prologue
    .line 563
    packed-switch p1, :pswitch_data_0

    .line 576
    :goto_0
    return-void

    .line 565
    :pswitch_0
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/samsung/musicplus/util/ServiceUtils;->playPrev(Z)V

    goto :goto_0

    .line 568
    :pswitch_1
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->playNext()V

    goto :goto_0

    .line 563
    nop

    :pswitch_data_0
    .packed-switch 0x66
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public hideAddToPlaylist(Z)V
    .locals 4
    .param p1, "force"    # Z

    .prologue
    const/4 v3, 0x0

    .line 2021
    if-nez p1, :cond_1

    iget-boolean v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mStop:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mPause:Z

    if-eqz v0, :cond_1

    .line 2023
    :cond_0
    iput-boolean v3, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mAddToPlayListShowing:Z

    .line 2035
    :goto_0
    return-void

    .line 2026
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v1, "add_to_playlist"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/app/FragmentManager;->popBackStackImmediate(Ljava/lang/String;I)Z

    .line 2028
    iput-boolean v3, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mAddToPlayListShowing:Z

    .line 2030
    const v0, 0x7f0d0121

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/player/FullPlayerView;->setButtonsNextFocus(I)V

    .line 2032
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->requestShowLyricView()V

    .line 2034
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->setVisibleIfLyricOn()V

    goto :goto_0
.end method

.method public hideVolumePanel()V
    .locals 1

    .prologue
    .line 2837
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mVolumePopupWindow:Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;

    invoke-virtual {v0}, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->hide()V

    .line 2846
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mIsVolumeBarShowing:Z

    .line 2847
    return-void
.end method

.method public invalidateVolumeBar(I)V
    .locals 1
    .param p1, "volume"    # I

    .prologue
    .line 2944
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->isDmrPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2967
    :goto_0
    return-void

    .line 2948
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

    invoke-virtual {v0}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->isMediaSilentMode()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2951
    const/4 p1, 0x0

    .line 2954
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mVolumePopupWindow:Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->setVolume(I)V

    .line 2955
    invoke-direct {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->resetHideVolumeTimer()V

    .line 2966
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/player/FullPlayerView;->updateVolumeItem(I)V

    goto :goto_0
.end method

.method isLyricViewShowing()Z
    .locals 1

    .prologue
    .line 1020
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mLyricView:Lcom/samsung/musicplus/player/LyricScrollView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mLyricView:Lcom/samsung/musicplus/player/LyricScrollView;

    invoke-virtual {v0}, Lcom/samsung/musicplus/player/LyricScrollView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isSideCastView()Z
    .locals 1

    .prologue
    .line 2437
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mSideCastGridView:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mGLRecommendedMusic:Lcom/samsung/musicplus/glwidget/GLGalleryView;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isVisibleNormalVolumePanel()Z
    .locals 1

    .prologue
    .line 3218
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mVolumePopupWindow:Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;

    invoke-virtual {v0}, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->isVisibleNormalVolumePanel()Z

    move-result v0

    return v0
.end method

.method public isVolumeBarShowing()Z
    .locals 1

    .prologue
    .line 2940
    iget-boolean v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mIsVolumeBarShowing:Z

    return v0
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 1169
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mAddToPlayListShowing:Z

    .line 1170
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->requestShowLyricView()V

    .line 1171
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->setVisibleIfLyricOn()V

    .line 1172
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 2053
    iget-boolean v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mPause:Z

    if-eqz v1, :cond_0

    .line 2073
    :goto_0
    return-void

    .line 2057
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 2058
    .local v0, "viewId":I
    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    .line 2064
    :sswitch_0
    iget-boolean v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mAddToPlayListShowing:Z

    if-eqz v1, :cond_1

    .line 2065
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/player/FullPlayerView;->hideAddToPlaylist(Z)V

    goto :goto_0

    .line 2060
    :sswitch_1
    invoke-direct {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->toggleStarred()V

    .line 2061
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mStarButton:Landroid/widget/ImageView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->sendAccessibilityEvent(I)V

    goto :goto_0

    .line 2067
    :cond_1
    invoke-direct {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->showAddToPlaylistView()V

    goto :goto_0

    .line 2058
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0d011a -> :sswitch_0
        0x7f0d0121 -> :sswitch_1
    .end sparse-switch
.end method

.method protected onCreateView(Landroid/view/View;)V
    .locals 5
    .param p1, "root"    # Landroid/view/View;

    .prologue
    .line 580
    invoke-super {p0, p1}, Lcom/samsung/musicplus/player/PlayerView;->onCreateView(Landroid/view/View;)V

    .line 581
    new-instance v2, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    check-cast v1, Landroid/app/Activity;

    invoke-direct {v2, v1}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;-><init>(Landroid/app/Activity;)V

    iput-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView;->multiWindowRect:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    .line 582
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->getAlbumView()Landroid/view/View;

    move-result-object v0

    .line 583
    .local v0, "albumView":Landroid/view/View;
    new-instance v1, Landroid/view/GestureDetector;

    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    new-instance v3, Lcom/samsung/musicplus/player/FullPlayerView$AlbumViewGestureDetector;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Lcom/samsung/musicplus/player/FullPlayerView$AlbumViewGestureDetector;-><init>(Lcom/samsung/musicplus/player/FullPlayerView;Lcom/samsung/musicplus/player/FullPlayerView$1;)V

    invoke-direct {v1, v2, v3}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mGestureDetector:Landroid/view/GestureDetector;

    .line 584
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mFormatBuilder:Ljava/lang/StringBuilder;

    .line 585
    new-instance v1, Ljava/util/Formatter;

    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mFormatBuilder:Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/util/Formatter;-><init>(Ljava/lang/Appendable;Ljava/util/Locale;)V

    iput-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mFormatter:Ljava/util/Formatter;

    .line 587
    if-eqz v0, :cond_0

    .line 588
    new-instance v1, Lcom/samsung/musicplus/player/FullPlayerView$4;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/player/FullPlayerView$4;-><init>(Lcom/samsung/musicplus/player/FullPlayerView;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 596
    :cond_0
    return-void
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1176
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mLoader:Lcom/samsung/musicplus/player/NowPlayingCursorLoader;

    if-eqz v1, :cond_0

    .line 1177
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mLoader:Lcom/samsung/musicplus/player/NowPlayingCursorLoader;

    invoke-virtual {v1}, Lcom/samsung/musicplus/player/NowPlayingCursorLoader;->release()V

    .line 1178
    iput-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mLoader:Lcom/samsung/musicplus/player/NowPlayingCursorLoader;

    .line 1181
    :cond_0
    invoke-direct {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->getLyricLoader()Lcom/samsung/musicplus/util/LyricsLoader;

    move-result-object v0

    .line 1182
    .local v0, "l":Lcom/samsung/musicplus/util/LyricsLoader;
    if-eqz v0, :cond_1

    .line 1183
    invoke-interface {v0}, Lcom/samsung/musicplus/util/LyricsLoader;->release()V

    .line 1186
    :cond_1
    sget-boolean v1, Lcom/samsung/musicplus/util/Features;->FLAG_SUPPORT_PARTICLE_EFFECT:Z

    if-eqz v1, :cond_2

    .line 1187
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mParticleView:Landroid/view/View;

    if-eqz v1, :cond_2

    .line 1188
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mParticleView:Landroid/view/View;

    invoke-static {v1}, Lcom/samsung/musicplus/library/view/ParticleEffectCompat;->destroy(Landroid/view/View;)V

    .line 1191
    :cond_2
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mUiHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1192
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->getUiHandler()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 1193
    return-void
.end method

.method public onHandleLyricTouchEvent(Landroid/view/MotionEvent;)V
    .locals 1
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 3178
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mGestureDetector:Landroid/view/GestureDetector;

    if-eqz v0, :cond_0

    .line 3179
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 3181
    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 1104
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mGLAlbumArt:Lcom/samsung/musicplus/glwidget/GLGalleryView;

    if-eqz v0, :cond_0

    .line 1105
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mGLAlbumArt:Lcom/samsung/musicplus/glwidget/GLGalleryView;

    invoke-interface {v0}, Lcom/samsung/musicplus/glwidget/GLGalleryView;->onPause()V

    .line 1107
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mGLRecommendedMusic:Lcom/samsung/musicplus/glwidget/GLGalleryView;

    if-eqz v0, :cond_1

    .line 1108
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mGLRecommendedMusic:Lcom/samsung/musicplus/glwidget/GLGalleryView;

    invoke-interface {v0}, Lcom/samsung/musicplus/glwidget/GLGalleryView;->onPause()V

    .line 1110
    :cond_1
    sget-boolean v0, Lcom/samsung/musicplus/util/Features;->FLAG_SUPPORT_PARTICLE_EFFECT:Z

    if-eqz v0, :cond_2

    .line 1111
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mParticleView:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 1112
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mParticleView:Landroid/view/View;

    invoke-static {v0}, Lcom/samsung/musicplus/library/view/ParticleEffectCompat;->clearEffect(Landroid/view/View;)V

    .line 1116
    :cond_2
    sget-boolean v0, Lcom/samsung/musicplus/util/Features;->FLAG_SUPPORT_GALLERY_VOLUME_EFFECT:Z

    if-eqz v0, :cond_3

    .line 1117
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mVolumeLimitController:Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;->setLockVolumeLimitControl(Z)V

    .line 1119
    :cond_3
    invoke-super {p0}, Lcom/samsung/musicplus/player/PlayerView;->onPause()V

    .line 1120
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 1124
    invoke-super {p0}, Lcom/samsung/musicplus/player/PlayerView;->onResume()V

    .line 1126
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mGLAlbumArt:Lcom/samsung/musicplus/glwidget/GLGalleryView;

    if-eqz v0, :cond_0

    .line 1127
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mGLAlbumArt:Lcom/samsung/musicplus/glwidget/GLGalleryView;

    invoke-interface {v0}, Lcom/samsung/musicplus/glwidget/GLGalleryView;->onResume()V

    .line 1129
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mGLRecommendedMusic:Lcom/samsung/musicplus/glwidget/GLGalleryView;

    if-eqz v0, :cond_1

    .line 1130
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mGLRecommendedMusic:Lcom/samsung/musicplus/glwidget/GLGalleryView;

    invoke-interface {v0}, Lcom/samsung/musicplus/glwidget/GLGalleryView;->onResume()V

    .line 1132
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->getStreamVolume(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/player/FullPlayerView;->updateVolumeItem(I)V

    .line 1134
    sget-boolean v0, Lcom/samsung/musicplus/util/Features;->FLAG_SUPPORT_GALLERY_VOLUME_EFFECT:Z

    if-eqz v0, :cond_2

    .line 1135
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mVolumeLimitController:Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;->setLockVolumeLimitControl(Z)V

    .line 1137
    :cond_2
    return-void
.end method

.method public onStart()V
    .locals 3

    .prologue
    .line 1141
    invoke-super {p0}, Lcom/samsung/musicplus/player/PlayerView;->onStart()V

    .line 1144
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 1145
    .local v0, "f":Landroid/content/IntentFilter;
    const-string v1, "com.samsung.musicplus.action.QUEUE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1146
    const-string v1, "com.samsung.musicplus.action.META_EDITED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1147
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mQueueReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1148
    invoke-static {}, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->getCache()Lcom/samsung/musicplus/bitmapcache/BitmapCache;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mGLAlbumArtOnPublishImageListener:Lcom/samsung/musicplus/bitmapcache/BitmapCache$Publisher;

    invoke-virtual {v1, v2}, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->addOnPublishImageListener(Lcom/samsung/musicplus/bitmapcache/BitmapCache$Publisher;)V

    .line 1150
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 1154
    invoke-super {p0}, Lcom/samsung/musicplus/player/PlayerView;->onStop()V

    .line 1156
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mQueueReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1157
    invoke-static {}, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->getCache()Lcom/samsung/musicplus/bitmapcache/BitmapCache;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mGLAlbumArtOnPublishImageListener:Lcom/samsung/musicplus/bitmapcache/BitmapCache$Publisher;

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->removeOnPublishImageListener(Lcom/samsung/musicplus/bitmapcache/BitmapCache$Publisher;)V

    .line 1159
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->stopObserving()V

    .line 1160
    return-void
.end method

.method public refreshEasyModeView()V
    .locals 1

    .prologue
    .line 1754
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/musicplus/util/UiUtils;->isEasyMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1755
    invoke-direct {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->updateEasyModeView()V

    .line 1759
    :goto_0
    invoke-direct {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->updateExtraButtons()V

    .line 1766
    return-void

    .line 1757
    :cond_0
    invoke-direct {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->updateNormalView()V

    goto :goto_0
.end method

.method protected requestShowLyricView()V
    .locals 1

    .prologue
    .line 2039
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->isLyricViewShowing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2040
    iget-boolean v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mAlbumViewShowing:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mAddToPlayListShowing:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mKeepLyricShowingState:Z

    if-eqz v0, :cond_0

    .line 2041
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->toggleLyricView()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2042
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mKeepLyricShowingState:Z

    .line 2046
    :cond_0
    invoke-super {p0}, Lcom/samsung/musicplus/player/PlayerView;->requestShowLyricView()V

    .line 2047
    return-void
.end method

.method protected setBackground(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "bm"    # Landroid/graphics/Bitmap;

    .prologue
    .line 321
    invoke-super {p0, p1}, Lcom/samsung/musicplus/player/PlayerView;->setBackground(Landroid/graphics/Bitmap;)V

    .line 322
    if-eqz p1, :cond_0

    .line 323
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/player/FullPlayerView;->getBackgroundStartColor(Landroid/graphics/Bitmap;)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/player/FullPlayerView;->setBackground(I)V

    .line 326
    :cond_0
    return-void
.end method

.method public setChangePlayerItem(Landroid/view/MenuItem;)V
    .locals 0
    .param p1, "changePlayerItem"    # Landroid/view/MenuItem;

    .prologue
    .line 1093
    iput-object p1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mChangePlayerItem:Landroid/view/MenuItem;

    .line 1094
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->updateChangePlayerItem()V

    .line 1095
    return-void
.end method

.method public setChangePlayerItemEnabled(Z)V
    .locals 2
    .param p1, "enabled"    # Z

    .prologue
    .line 2169
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mChangePlayerItem:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    .line 2170
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mChangePlayerItem:Landroid/view/MenuItem;

    if-eqz p1, :cond_1

    const v0, 0x7f020078

    :goto_0
    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 2172
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mChangePlayerItem:Landroid/view/MenuItem;

    invoke-interface {v0, p1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 2174
    :cond_0
    return-void

    .line 2170
    :cond_1
    const v0, 0x7f020077

    goto :goto_0
.end method

.method public setCurrentSongPosition(I)V
    .locals 0
    .param p1, "position"    # I

    .prologue
    .line 3206
    iput p1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mCurrentSongPosition:I

    .line 3207
    return-void
.end method

.method protected setEmptyViewText()V
    .locals 4

    .prologue
    .line 2732
    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f100106

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 2733
    .local v0, "noItemText":Ljava/lang/CharSequence;
    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0051

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 2735
    .local v1, "noItemTextColor":I
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->getParentView()Landroid/view/View;

    move-result-object v2

    const v3, 0x7f0d0049

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mNoItemTextView:Landroid/widget/TextView;

    .line 2736
    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mNoItemTextView:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 2737
    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mNoItemTextView:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 2738
    return-void
.end method

.method public setHdmiConnected(Z)V
    .locals 0
    .param p1, "isConnected"    # Z

    .prologue
    .line 3200
    iput-boolean p1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mIsHdmiConnected:Z

    .line 3201
    return-void
.end method

.method public setNowPlayingCursorLoader()V
    .locals 4

    .prologue
    .line 1529
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mLoader:Lcom/samsung/musicplus/player/NowPlayingCursorLoader;

    if-nez v0, :cond_0

    .line 1530
    new-instance v0, Lcom/samsung/musicplus/player/NowPlayingCursorLoader;

    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    iget-object v3, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mNowplayignQueryCompleteListener:Lcom/samsung/musicplus/player/NowPlayingCursorLoader$OnQueryCompleteListener;

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/musicplus/player/NowPlayingCursorLoader;-><init>(Landroid/content/ContentResolver;Lcom/samsung/musicplus/service/IPlayerService;Lcom/samsung/musicplus/player/NowPlayingCursorLoader$OnQueryCompleteListener;)V

    iput-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mLoader:Lcom/samsung/musicplus/player/NowPlayingCursorLoader;

    .line 1533
    :cond_0
    return-void
.end method

.method public setSelectedRecommendPosition(I)V
    .locals 1
    .param p1, "i"    # I

    .prologue
    .line 3189
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mGLRecommendedMusic:Lcom/samsung/musicplus/glwidget/GLGalleryView;

    if-eqz v0, :cond_0

    .line 3190
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mGLRecommendedMusic:Lcom/samsung/musicplus/glwidget/GLGalleryView;

    invoke-interface {v0, p1}, Lcom/samsung/musicplus/glwidget/GLGalleryView;->setSelection(I)V

    .line 3191
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mGLRecommendedMusic:Lcom/samsung/musicplus/glwidget/GLGalleryView;

    invoke-interface {v0}, Lcom/samsung/musicplus/glwidget/GLGalleryView;->requestRender()V

    .line 3193
    :cond_0
    return-void
.end method

.method protected setTitleScroll(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 1240
    invoke-super {p0, p1}, Lcom/samsung/musicplus/player/PlayerView;->setTitleScroll(Z)V

    .line 1242
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mTitleTop:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 1243
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mTitleTop:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setSelected(Z)V

    .line 1245
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mArtistTop:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 1246
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mArtistTop:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setSelected(Z)V

    .line 1248
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mAlbumTop:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    .line 1249
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mAlbumTop:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setSelected(Z)V

    .line 1251
    :cond_2
    return-void
.end method

.method protected setVisibleIfLyricOn()V
    .locals 6

    .prologue
    const v5, 0x7f0d0111

    const/4 v4, 0x4

    const/4 v3, 0x0

    .line 1371
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->isLyricViewShowing()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1372
    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mTopTitleView:Landroid/view/View;

    if-eqz v2, :cond_0

    .line 1373
    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mTopTitleView:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1375
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->isSideCastView()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1376
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->getParentView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1378
    .local v0, "titleBottomView":Landroid/view/View;
    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1379
    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 1391
    .end local v0    # "titleBottomView":Landroid/view/View;
    :cond_1
    :goto_0
    return-void

    .line 1382
    :cond_2
    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mTopTitleView:Landroid/view/View;

    if-eqz v2, :cond_3

    .line 1383
    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mTopTitleView:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1385
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->getParentView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 1386
    .local v1, "v":Landroid/view/View;
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 1387
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->getAlbumViewVisibility()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mAddToPlayListShowing:Z

    if-nez v2, :cond_1

    .line 1388
    invoke-virtual {v1}, Landroid/view/View;->bringToFront()V

    goto :goto_0
.end method

.method public setVolumeItem(Landroid/view/MenuItem;)V
    .locals 2
    .param p1, "volumeItem"    # Landroid/view/MenuItem;

    .prologue
    .line 1098
    iput-object p1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mVolumeItem:Landroid/view/MenuItem;

    .line 1099
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->getStreamVolume(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/player/FullPlayerView;->updateVolumeItem(I)V

    .line 1100
    return-void
.end method

.method public showVolumePanel()V
    .locals 3

    .prologue
    .line 2792
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->getStreamVolume(I)I

    move-result v0

    .line 2794
    .local v0, "streamVolume":I
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->isDmrPlaying()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2795
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mVolumePopupWindow:Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;

    invoke-virtual {v1}, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->showNormal()V

    .line 2799
    :goto_0
    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/player/FullPlayerView;->updateVolumeBar(I)V

    .line 2833
    return-void

    .line 2797
    :cond_0
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mVolumePopupWindow:Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;

    invoke-virtual {v1}, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->showDmr()V

    goto :goto_0
.end method

.method public startRequery()V
    .locals 6

    .prologue
    .line 2441
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mSideCastAdapter:Lcom/samsung/musicplus/player/PlayerSideCastAdapter;

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mGLRecommendedMusic:Lcom/samsung/musicplus/glwidget/GLGalleryView;

    if-eqz v1, :cond_1

    .line 2442
    :cond_0
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mSideCastUpdater:Lcom/samsung/musicplus/player/FullPlayerView$SideCastAsyncUpdater;

    if-eqz v1, :cond_1

    .line 2443
    const-string v1, "MusicSideCast"

    const-string v2, "startRequery()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2444
    invoke-direct {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->getSimilarSongsQuerySelection()Lcom/samsung/musicplus/player/FullPlayerView$SideCastQueryInfo;

    move-result-object v0

    .line 2445
    .local v0, "info":Lcom/samsung/musicplus/player/FullPlayerView$SideCastQueryInfo;
    if-nez v0, :cond_2

    .line 2446
    const-string v1, "MusicSideCast"

    const-string v2, "query info is null"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2447
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/samsung/musicplus/player/FullPlayerView;->updateTotalCount(Landroid/database/Cursor;)V

    .line 2454
    .end local v0    # "info":Lcom/samsung/musicplus/player/FullPlayerView$SideCastQueryInfo;
    :cond_1
    :goto_0
    return-void

    .line 2449
    .restart local v0    # "info":Lcom/samsung/musicplus/player/FullPlayerView$SideCastQueryInfo;
    :cond_2
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mSideCastUpdater:Lcom/samsung/musicplus/player/FullPlayerView$SideCastAsyncUpdater;

    iget-object v2, v0, Lcom/samsung/musicplus/player/FullPlayerView$SideCastQueryInfo;->uri:Landroid/net/Uri;

    iget-object v3, v0, Lcom/samsung/musicplus/player/FullPlayerView$SideCastQueryInfo;->projection:[Ljava/lang/String;

    iget-object v4, v0, Lcom/samsung/musicplus/player/FullPlayerView$SideCastQueryInfo;->selection:Ljava/lang/String;

    iget-object v5, v0, Lcom/samsung/musicplus/player/FullPlayerView$SideCastQueryInfo;->selectionArgs:[Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/samsung/musicplus/player/FullPlayerView$SideCastAsyncUpdater;->startObserving(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    goto :goto_0
.end method

.method public stopObserving()V
    .locals 1

    .prologue
    .line 1163
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mSideCastUpdater:Lcom/samsung/musicplus/player/FullPlayerView$SideCastAsyncUpdater;

    if-eqz v0, :cond_0

    .line 1164
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mSideCastUpdater:Lcom/samsung/musicplus/player/FullPlayerView$SideCastAsyncUpdater;

    invoke-virtual {v0}, Lcom/samsung/musicplus/player/FullPlayerView$SideCastAsyncUpdater;->stopObserving()V

    .line 1166
    :cond_0
    return-void
.end method

.method protected toggleLyricView()Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 914
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->isNowPlayingListShown()Z

    move-result v3

    if-nez v3, :cond_0

    iget-boolean v3, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mAddToPlayListShowing:Z

    if-eqz v3, :cond_1

    :cond_0
    move v1, v2

    .line 977
    :goto_0
    return v1

    .line 917
    :cond_1
    iget-object v3, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mPreferences:Landroid/content/SharedPreferences;

    const-string v4, "lyric"

    invoke-interface {v3, v4, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 918
    iget-object v3, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mLyricView:Lcom/samsung/musicplus/player/LyricScrollView;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mLyricText:Landroid/widget/TextView;

    if-nez v3, :cond_4

    .line 919
    :cond_2
    const-string v1, "MusicUi"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Do not toggle, Lyrics view : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mLyricView:Lcom/samsung/musicplus/player/LyricScrollView;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "LyricText : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mLyricText:Landroid/widget/TextView;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 924
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->isLocalTrack()Z

    move-result v1

    if-nez v1, :cond_3

    .line 925
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    const v3, 0x7f1001ba

    invoke-virtual {v1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/samsung/musicplus/player/FullPlayerView;->showToast(Ljava/lang/String;)V

    :cond_3
    move v1, v2

    .line 927
    goto :goto_0

    .line 929
    :cond_4
    iget-object v3, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mLyricText:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    .line 931
    .local v0, "lyric":Ljava/lang/CharSequence;
    if-eqz v0, :cond_6

    const-string v3, ""

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    .line 932
    iget-object v3, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mLyricView:Lcom/samsung/musicplus/player/LyricScrollView;

    invoke-virtual {v3}, Lcom/samsung/musicplus/player/LyricScrollView;->getVisibility()I

    move-result v3

    if-nez v3, :cond_5

    .line 933
    iget-object v3, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mLyricDownOut:Landroid/view/animation/Animation;

    new-instance v4, Lcom/samsung/musicplus/player/FullPlayerView$11;

    invoke-direct {v4, p0}, Lcom/samsung/musicplus/player/FullPlayerView$11;-><init>(Lcom/samsung/musicplus/player/FullPlayerView;)V

    invoke-virtual {v3, v4}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 949
    iget-object v3, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mLyricView:Lcom/samsung/musicplus/player/LyricScrollView;

    iget-object v4, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mLyricDownOut:Landroid/view/animation/Animation;

    invoke-virtual {v3, v4}, Lcom/samsung/musicplus/player/LyricScrollView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 950
    const/4 v3, 0x4

    invoke-direct {p0, v3}, Lcom/samsung/musicplus/player/FullPlayerView;->setVisibilityLyricView(I)V

    .line 951
    iput-boolean v2, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mLyricViewShowing:Z

    goto :goto_0

    .line 953
    :cond_5
    iget-object v3, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mLyricUpIn:Landroid/view/animation/Animation;

    new-instance v4, Lcom/samsung/musicplus/player/FullPlayerView$12;

    invoke-direct {v4, p0}, Lcom/samsung/musicplus/player/FullPlayerView$12;-><init>(Lcom/samsung/musicplus/player/FullPlayerView;)V

    invoke-virtual {v3, v4}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 970
    iget-object v3, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mLyricView:Lcom/samsung/musicplus/player/LyricScrollView;

    iget-object v4, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mLyricUpIn:Landroid/view/animation/Animation;

    invoke-virtual {v3, v4}, Lcom/samsung/musicplus/player/LyricScrollView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 971
    invoke-direct {p0, v2}, Lcom/samsung/musicplus/player/FullPlayerView;->setVisibilityLyricView(I)V

    .line 972
    iput-boolean v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mLyricViewShowing:Z

    goto/16 :goto_0

    .end local v0    # "lyric":Ljava/lang/CharSequence;
    :cond_6
    move v1, v2

    .line 977
    goto/16 :goto_0
.end method

.method public toggleMute()V
    .locals 3

    .prologue
    .line 3096
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

    invoke-virtual {v1}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->isMediaSilentMode()Z

    move-result v0

    .line 3097
    .local v0, "isMute":Z
    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

    if-nez v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v2, v1}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->setMediaSilentMode(Z)V

    .line 3098
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->showVolumePanel()V

    .line 3099
    return-void

    .line 3097
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected togglePlayerView()V
    .locals 3

    .prologue
    .line 1706
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->getAlbumViewVisibility()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1707
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->hideVolumePanel()V

    .line 1710
    :cond_0
    invoke-super {p0}, Lcom/samsung/musicplus/player/PlayerView;->togglePlayerView()V

    .line 1711
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/player/FullPlayerView;->hideAddToPlaylist(Z)V

    .line 1713
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->getAlbumViewVisibility()Z

    move-result v1

    if-eqz v1, :cond_2

    const v1, 0x7f0d0121

    :goto_0
    invoke-direct {p0, v1}, Lcom/samsung/musicplus/player/FullPlayerView;->setButtonsNextFocus(I)V

    .line 1715
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->getAlbumViewVisibility()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1716
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mParentView:Landroid/view/View;

    const v2, 0x7f0d0107

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1717
    .local v0, "v":Landroid/view/View;
    if-eqz v0, :cond_1

    .line 1718
    invoke-virtual {v0}, Landroid/view/View;->bringToFront()V

    .line 1721
    .end local v0    # "v":Landroid/view/View;
    :cond_1
    return-void

    .line 1713
    :cond_2
    const v1, 0x7f0d008c

    goto :goto_0
.end method

.method public toggleVolumePannel()V
    .locals 3

    .prologue
    .line 2758
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->getStreamVolume(I)I

    move-result v0

    .line 2760
    .local v0, "streamVolume":I
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->isDmrPlaying()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2761
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mVolumePopupWindow:Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;

    invoke-virtual {v1}, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->toggleNormal()V

    .line 2765
    :goto_0
    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/player/FullPlayerView;->updateVolumeBar(I)V

    .line 2787
    return-void

    .line 2763
    :cond_0
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mVolumePopupWindow:Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;

    invoke-virtual {v1}, Lcom/samsung/musicplus/player/FullPlayerView$VolumePopupWindow;->toggleDmr()V

    goto :goto_0
.end method

.method protected updateAlbumArt(I)V
    .locals 2
    .param p1, "listType"    # I

    .prologue
    .line 1543
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mGLAlbumArt:Lcom/samsung/musicplus/glwidget/GLGalleryView;

    if-eqz v0, :cond_1

    .line 1544
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getListPosition()I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mPosition:I

    .line 1545
    iget v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mPosition:I

    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mGLAlbumArt:Lcom/samsung/musicplus/glwidget/GLGalleryView;

    invoke-interface {v1}, Lcom/samsung/musicplus/glwidget/GLGalleryView;->getSelectedItemPosition()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 1546
    invoke-direct {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->updateGLAlbumartPosition()V

    .line 1549
    :cond_0
    new-instance v0, Lcom/samsung/musicplus/player/FullPlayerView$14;

    invoke-direct {v0, p0, p1}, Lcom/samsung/musicplus/player/FullPlayerView$14;-><init>(Lcom/samsung/musicplus/player/FullPlayerView;I)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/player/FullPlayerView$14;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 1592
    :cond_1
    return-void
.end method

.method protected updateButtonThumbnailImage(Landroid/graphics/Bitmap;)V
    .locals 3
    .param p1, "source"    # Landroid/graphics/Bitmap;

    .prologue
    .line 1626
    if-nez p1, :cond_0

    .line 1627
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mAlbumArtAdapter:Lcom/samsung/musicplus/player/GLAlbumArtAdapter;

    if-eqz v1, :cond_0

    .line 1628
    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mAlbumUriPosition:Landroid/util/SparseArray;

    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getListPosition()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 1629
    .local v0, "albumArtUri":Landroid/net/Uri;
    if-eqz v0, :cond_0

    .line 1630
    invoke-static {}, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->getCache()Lcom/samsung/musicplus/bitmapcache/BitmapCache;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->getBitmap(Landroid/net/Uri;)Landroid/graphics/Bitmap;

    move-result-object p1

    .line 1634
    .end local v0    # "albumArtUri":Landroid/net/Uri;
    :cond_0
    invoke-super {p0, p1}, Lcom/samsung/musicplus/player/PlayerView;->updateButtonThumbnailImage(Landroid/graphics/Bitmap;)V

    .line 1635
    return-void
.end method

.method public updateChangePlayerItem()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 2144
    sget-boolean v0, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_SUPPORT_ALLSHARE:Z

    if-eqz v0, :cond_0

    sget-boolean v0, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_MASS_PROJECT:Z

    if-eqz v0, :cond_2

    .line 2145
    :cond_0
    invoke-direct {p0, v2}, Lcom/samsung/musicplus/player/FullPlayerView;->setChangePlayerItemVisibility(Z)V

    .line 2160
    :cond_1
    :goto_0
    return-void

    .line 2148
    :cond_2
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/musicplus/util/KnoxGateManager;->isKnoxModeOn(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 2149
    invoke-direct {p0, v2}, Lcom/samsung/musicplus/player/FullPlayerView;->setChangePlayerItemVisibility(Z)V

    goto :goto_0

    .line 2152
    :cond_3
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/musicplus/library/audio/SideSyncManager;->isSideSyncConnected(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2153
    invoke-virtual {p0, v2}, Lcom/samsung/musicplus/player/FullPlayerView;->setChangePlayerItemEnabled(Z)V

    goto :goto_0

    .line 2156
    :cond_4
    iget-boolean v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mIsHdmiConnected:Z

    if-nez v0, :cond_7

    move v0, v1

    :goto_1
    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/player/FullPlayerView;->setChangePlayerItemEnabled(Z)V

    .line 2157
    iget-boolean v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mIsHdmiConnected:Z

    if-nez v0, :cond_1

    .line 2158
    invoke-direct {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->isConnectedWfd()Z

    move-result v0

    if-nez v0, :cond_5

    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->isDmrPlaying()Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_5
    move v2, v1

    :cond_6
    invoke-direct {p0, v2}, Lcom/samsung/musicplus/player/FullPlayerView;->setChangePlayerItemConnected(Z)V

    goto :goto_0

    :cond_7
    move v0, v2

    .line 2156
    goto :goto_1
.end method

.method public updateControllerView(I)V
    .locals 1
    .param p1, "listType"    # I

    .prologue
    .line 1639
    invoke-super {p0, p1}, Lcom/samsung/musicplus/player/PlayerView;->updateControllerView(I)V

    .line 1640
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/musicplus/util/UiUtils;->isEasyMode(Landroid/content/Context;)Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/player/FullPlayerView;->setVisibilityListButton(Z)V

    .line 1641
    return-void
.end method

.method protected updateHoverView()V
    .locals 4

    .prologue
    const/16 v3, 0x3031

    .line 1645
    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mStarButton:Landroid/widget/ImageView;

    if-eqz v2, :cond_0

    .line 1646
    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mStarButton:Landroid/widget/ImageView;

    invoke-virtual {p0, v2, v3}, Lcom/samsung/musicplus/player/FullPlayerView;->setAirView(Landroid/view/View;I)V

    .line 1648
    :cond_0
    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mAddPlaylist:Landroid/widget/ImageView;

    if-eqz v2, :cond_1

    .line 1649
    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mAddPlaylist:Landroid/widget/ImageView;

    invoke-virtual {p0, v2, v3}, Lcom/samsung/musicplus/player/FullPlayerView;->setAirView(Landroid/view/View;I)V

    .line 1652
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->getShuffleButtonView()Landroid/widget/ImageView;

    move-result-object v1

    .line 1653
    .local v1, "shuffle":Landroid/view/View;
    if-eqz v1, :cond_2

    .line 1654
    const/16 v2, 0x3135

    invoke-virtual {p0, v1, v2}, Lcom/samsung/musicplus/player/FullPlayerView;->setAirView(Landroid/view/View;I)V

    .line 1657
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->getRepeatButtonView()Landroid/widget/ImageView;

    move-result-object v0

    .line 1658
    .local v0, "repeat":Landroid/view/View;
    if-eqz v0, :cond_3

    .line 1659
    const/16 v2, 0x3133

    invoke-virtual {p0, v0, v2}, Lcom/samsung/musicplus/player/FullPlayerView;->setAirView(Landroid/view/View;I)V

    .line 1662
    :cond_3
    invoke-super {p0}, Lcom/samsung/musicplus/player/PlayerView;->updateHoverView()V

    .line 1663
    return-void
.end method

.method updateLyricsView()V
    .locals 6

    .prologue
    const/4 v5, 0x4

    .line 888
    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mParentView:Landroid/view/View;

    const v3, 0x7f0d0107

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 889
    .local v0, "fullPlayerView":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 890
    invoke-virtual {v0}, Landroid/view/View;->bringToFront()V

    .line 892
    :cond_0
    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mPreferences:Landroid/content/SharedPreferences;

    const-string v3, "lyric"

    const/4 v4, 0x1

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 893
    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mLyricView:Lcom/samsung/musicplus/player/LyricScrollView;

    if-eqz v2, :cond_1

    .line 894
    invoke-direct {p0, v5}, Lcom/samsung/musicplus/player/FullPlayerView;->setVisibilityLyricView(I)V

    .line 897
    :cond_1
    invoke-direct {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->getLyricLoader()Lcom/samsung/musicplus/util/LyricsLoader;

    move-result-object v1

    .line 898
    .local v1, "loader":Lcom/samsung/musicplus/util/LyricsLoader;
    if-eqz v1, :cond_2

    .line 899
    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mUiHandler:Landroid/os/Handler;

    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getCurrentPath()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/samsung/musicplus/util/LyricsLoader;->loadLyrics(Landroid/os/Handler;Ljava/lang/String;)V

    .line 900
    const-string v2, "MusicUi"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "updateLyricsView : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 908
    .end local v1    # "loader":Lcom/samsung/musicplus/util/LyricsLoader;
    :cond_2
    :goto_0
    return-void

    .line 903
    :cond_3
    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mLyricView:Lcom/samsung/musicplus/player/LyricScrollView;

    if-eqz v2, :cond_4

    .line 904
    invoke-direct {p0, v5}, Lcom/samsung/musicplus/player/FullPlayerView;->setVisibilityLyricView(I)V

    .line 906
    :cond_4
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->setVisibleIfLyricOn()V

    goto :goto_0
.end method

.method public updateNowPlayingListView()V
    .locals 0

    .prologue
    .line 1520
    invoke-super {p0}, Lcom/samsung/musicplus/player/PlayerView;->updateNowPlayingListView()V

    .line 1521
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->setNowPlayingCursorLoader()V

    .line 1522
    return-void
.end method

.method public updatePlayerOptionButtons()V
    .locals 0

    .prologue
    .line 1406
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->updateShuffleIcon()V

    .line 1407
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->updateRepeatIcon()V

    .line 1408
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->updateChangePlayerItem()V

    .line 1409
    return-void
.end method

.method public updateRepeatIcon()V
    .locals 3

    .prologue
    .line 1683
    invoke-super {p0}, Lcom/samsung/musicplus/player/PlayerView;->updateRepeatIcon()V

    .line 1685
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mRepeatText:Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 1702
    :goto_0
    return-void

    .line 1689
    :cond_0
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getRepeat()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 1691
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mRepeatText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    const v2, 0x7f10019b

    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1694
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mRepeatText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    const v2, 0x7f100199

    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1697
    :pswitch_2
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mRepeatText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    const v2, 0x7f10019a

    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1689
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public updateShuffleIcon()V
    .locals 3

    .prologue
    .line 1667
    invoke-super {p0}, Lcom/samsung/musicplus/player/PlayerView;->updateShuffleIcon()V

    .line 1669
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mShuffleText:Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 1679
    :goto_0
    return-void

    .line 1673
    :cond_0
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getShuffle()I

    move-result v0

    if-nez v0, :cond_1

    .line 1677
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mShuffleText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    const v2, 0x7f10019f

    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1674
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mShuffleText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    const v2, 0x7f1001a0

    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public updateStarredIcon()V
    .locals 4

    .prologue
    .line 2216
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mStarButton:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->isSupportFavorite()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2229
    :cond_0
    :goto_0
    return-void

    .line 2220
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mStarButton:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 2221
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getCurrentAudioId()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lcom/samsung/musicplus/util/UiUtils;->isFavorite(Landroid/content/Context;J)Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mIsStarred:Z

    .line 2222
    iget-boolean v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mIsStarred:Z

    if-eqz v0, :cond_2

    .line 2223
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mStarButton:Landroid/widget/ImageView;

    const v1, 0x7f02007a

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2224
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mStarButton:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    const v2, 0x7f100189

    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 2226
    :cond_2
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mStarButton:Landroid/widget/ImageView;

    const v1, 0x7f020079

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 2227
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mStarButton:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    const v2, 0x7f100188

    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method protected updateTrackAllInfo()V
    .locals 1

    .prologue
    .line 1413
    invoke-super {p0}, Lcom/samsung/musicplus/player/PlayerView;->updateTrackAllInfo()V

    .line 1414
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->updateChangePlayerItem()V

    .line 1415
    invoke-virtual {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->updateLyricsView()V

    .line 1416
    sget-boolean v0, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_K2HD:Z

    if-eqz v0, :cond_0

    .line 1417
    invoke-direct {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->updateTagsView()V

    .line 1421
    :goto_0
    invoke-direct {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->updateExtraButtons()V

    .line 1422
    return-void

    .line 1419
    :cond_0
    invoke-direct {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->updateUhqTagView()V

    goto :goto_0
.end method

.method public updateVolumeBar(I)V
    .locals 1
    .param p1, "volume"    # I

    .prologue
    .line 2931
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/player/FullPlayerView;->invalidateVolumeBar(I)V

    .line 2934
    invoke-direct {p0}, Lcom/samsung/musicplus/player/FullPlayerView;->resetHideVolumeTimer()V

    .line 2936
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

    invoke-virtual {v0}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->dismissVolumePanel()V

    .line 2937
    return-void
.end method

.method public updateVolumeItem(I)V
    .locals 6
    .param p1, "volume"    # I

    .prologue
    .line 2992
    const-string v0, "MusicUi"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "updateVolumeButton volume : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2993
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->isDmrPlaying()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mVolumeItem:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    .line 2994
    if-lez p1, :cond_1

    .line 2995
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mVolumeItem:Landroid/view/MenuItem;

    const v1, 0x7f020098

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 2996
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mVolumeItem:Landroid/view/MenuItem;

    const-string v1, "%s %d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    const v5, 0x7f1001a8

    invoke-virtual {v4, v5}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 3003
    :cond_0
    :goto_0
    return-void

    .line 2999
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mVolumeItem:Landroid/view/MenuItem;

    const v1, 0x7f020099

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 3000
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mVolumeItem:Landroid/view/MenuItem;

    iget-object v1, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mContext:Landroid/content/Context;

    const v2, 0x7f1001aa

    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setTitle(Ljava/lang/CharSequence;)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public updateVolumeItemWithDealy(I)V
    .locals 4
    .param p1, "delay"    # I

    .prologue
    const/4 v1, 0x1

    .line 2987
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mUiHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 2988
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView;->mUiHandler:Landroid/os/Handler;

    int-to-long v2, p1

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 2989
    return-void
.end method

.method public volumeDown()V
    .locals 1

    .prologue
    .line 3041
    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/player/FullPlayerView;->changeVolume(I)V

    .line 3042
    return-void
.end method

.method public volumeUp()V
    .locals 1

    .prologue
    .line 3045
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/player/FullPlayerView;->changeVolume(I)V

    .line 3046
    return-void
.end method
