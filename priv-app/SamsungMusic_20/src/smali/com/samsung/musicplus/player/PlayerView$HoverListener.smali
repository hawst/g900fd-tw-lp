.class Lcom/samsung/musicplus/player/PlayerView$HoverListener;
.super Ljava/lang/Object;
.source "PlayerView.java"

# interfaces
.implements Landroid/view/View$OnHoverListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/player/PlayerView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "HoverListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/player/PlayerView;


# direct methods
.method private constructor <init>(Lcom/samsung/musicplus/player/PlayerView;)V
    .locals 0

    .prologue
    .line 1635
    iput-object p1, p0, Lcom/samsung/musicplus/player/PlayerView$HoverListener;->this$0:Lcom/samsung/musicplus/player/PlayerView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/musicplus/player/PlayerView;Lcom/samsung/musicplus/player/PlayerView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/musicplus/player/PlayerView;
    .param p2, "x1"    # Lcom/samsung/musicplus/player/PlayerView$1;

    .prologue
    .line 1635
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/player/PlayerView$HoverListener;-><init>(Lcom/samsung/musicplus/player/PlayerView;)V

    return-void
.end method


# virtual methods
.method public onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, 0x0

    .line 1638
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/musicplus/library/hardware/AirView;->isEnableAirViewInforPreview(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1639
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 1645
    .local v0, "action":I
    invoke-virtual {p2, v3}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 1646
    iget-object v1, p0, Lcom/samsung/musicplus/player/PlayerView$HoverListener;->this$0:Lcom/samsung/musicplus/player/PlayerView;

    invoke-virtual {v1, p1, v0}, Lcom/samsung/musicplus/player/PlayerView;->setHoverFocusImage(Landroid/view/View;I)V

    .line 1649
    .end local v0    # "action":I
    :cond_0
    return v3
.end method

.method public setHoverState(Landroid/view/View;I)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;
    .param p2, "action"    # I

    .prologue
    .line 1653
    if-eqz p1, :cond_0

    .line 1654
    iget-object v0, p0, Lcom/samsung/musicplus/player/PlayerView$HoverListener;->this$0:Lcom/samsung/musicplus/player/PlayerView;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/musicplus/player/PlayerView;->setHoverFocusImage(Landroid/view/View;I)V

    .line 1656
    :cond_0
    return-void
.end method
