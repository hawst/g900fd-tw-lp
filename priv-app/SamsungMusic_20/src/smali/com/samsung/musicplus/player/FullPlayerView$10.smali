.class Lcom/samsung/musicplus/player/FullPlayerView$10;
.super Ljava/lang/Object;
.source "FullPlayerView.java"

# interfaces
.implements Lcom/samsung/musicplus/widget/TouchNotifyRelativeLayout$OnTouchNotifyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/player/FullPlayerView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field mRect:Landroid/graphics/Rect;

.field final synthetic this$0:Lcom/samsung/musicplus/player/FullPlayerView;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/player/FullPlayerView;)V
    .locals 1

    .prologue
    .line 817
    iput-object p1, p0, Lcom/samsung/musicplus/player/FullPlayerView$10;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 866
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView$10;->mRect:Landroid/graphics/Rect;

    return-void
.end method

.method private checkConditionForParticle()Z
    .locals 1

    .prologue
    .line 863
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView$10;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    invoke-virtual {v0}, Lcom/samsung/musicplus/player/FullPlayerView;->getAlbumViewVisibility()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView$10;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mAddToPlayListShowing:Z
    invoke-static {v0}, Lcom/samsung/musicplus/player/FullPlayerView;->access$3700(Lcom/samsung/musicplus/player/FullPlayerView;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView$10;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mIsVolumeBarShowing:Z
    invoke-static {v0}, Lcom/samsung/musicplus/player/FullPlayerView;->access$3800(Lcom/samsung/musicplus/player/FullPlayerView;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isInsideView([ILandroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "screenCoord"    # [I
    .param p2, "view"    # Landroid/view/View;
    .param p3, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 869
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 870
    .local v0, "x":F
    invoke-virtual {p3}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    .line 871
    .local v1, "y":F
    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView$10;->mRect:Landroid/graphics/Rect;

    invoke-virtual {p2, v2}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/musicplus/player/FullPlayerView$10;->mRect:Landroid/graphics/Rect;

    float-to-int v3, v0

    float-to-int v4, v1

    invoke-virtual {v2, v3, v4}, Landroid/graphics/Rect;->contains(II)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onTouchNotify(Landroid/view/View;Landroid/view/MotionEvent;)V
    .locals 9
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 820
    invoke-direct {p0}, Lcom/samsung/musicplus/player/FullPlayerView$10;->checkConditionForParticle()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 821
    const-string v4, "MusicUi"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "onTouchNotify x: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " y: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " rawX: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " rawY: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " isLyricViewShowing: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/musicplus/player/FullPlayerView$10;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    invoke-virtual {v6}, Lcom/samsung/musicplus/player/FullPlayerView;->isLyricViewShowing()Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 824
    const/4 v0, 0x0

    .line 825
    .local v0, "bm":Landroid/graphics/Bitmap;
    iget-object v4, p0, Lcom/samsung/musicplus/player/FullPlayerView$10;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mGLAlbumArt:Lcom/samsung/musicplus/glwidget/GLGalleryView;
    invoke-static {v4}, Lcom/samsung/musicplus/player/FullPlayerView;->access$1600(Lcom/samsung/musicplus/player/FullPlayerView;)Lcom/samsung/musicplus/glwidget/GLGalleryView;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/samsung/musicplus/player/FullPlayerView$10;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    invoke-virtual {v4}, Lcom/samsung/musicplus/player/FullPlayerView;->isLyricViewShowing()Z

    move-result v4

    if-nez v4, :cond_0

    .line 826
    iget-object v4, p0, Lcom/samsung/musicplus/player/FullPlayerView$10;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mGLAlbumArt:Lcom/samsung/musicplus/glwidget/GLGalleryView;
    invoke-static {v4}, Lcom/samsung/musicplus/player/FullPlayerView;->access$1600(Lcom/samsung/musicplus/player/FullPlayerView;)Lcom/samsung/musicplus/glwidget/GLGalleryView;

    move-result-object v4

    invoke-interface {v4}, Lcom/samsung/musicplus/glwidget/GLGalleryView;->asView()Landroid/view/View;

    move-result-object v2

    .line 827
    .local v2, "mGLAlbumArtview":Landroid/view/View;
    if-eqz v2, :cond_0

    .line 828
    iget-object v4, p0, Lcom/samsung/musicplus/player/FullPlayerView$10;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mViewScreenCoordinates:[I
    invoke-static {v4}, Lcom/samsung/musicplus/player/FullPlayerView;->access$3000(Lcom/samsung/musicplus/player/FullPlayerView;)[I

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 829
    iget-object v4, p0, Lcom/samsung/musicplus/player/FullPlayerView$10;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mViewScreenCoordinates:[I
    invoke-static {v4}, Lcom/samsung/musicplus/player/FullPlayerView;->access$3000(Lcom/samsung/musicplus/player/FullPlayerView;)[I

    move-result-object v4

    invoke-direct {p0, v4, v2, p2}, Lcom/samsung/musicplus/player/FullPlayerView$10;->isInsideView([ILandroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 830
    iget-object v4, p0, Lcom/samsung/musicplus/player/FullPlayerView$10;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mParticlesInCoord:[F
    invoke-static {v4}, Lcom/samsung/musicplus/player/FullPlayerView;->access$3100(Lcom/samsung/musicplus/player/FullPlayerView;)[F

    move-result-object v4

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v5

    iget-object v6, p0, Lcom/samsung/musicplus/player/FullPlayerView$10;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mViewScreenCoordinates:[I
    invoke-static {v6}, Lcom/samsung/musicplus/player/FullPlayerView;->access$3000(Lcom/samsung/musicplus/player/FullPlayerView;)[I

    move-result-object v6

    aget v6, v6, v7

    int-to-float v6, v6

    sub-float/2addr v5, v6

    aput v5, v4, v7

    .line 831
    iget-object v4, p0, Lcom/samsung/musicplus/player/FullPlayerView$10;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mParticlesInCoord:[F
    invoke-static {v4}, Lcom/samsung/musicplus/player/FullPlayerView;->access$3100(Lcom/samsung/musicplus/player/FullPlayerView;)[F

    move-result-object v4

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v5

    iget-object v6, p0, Lcom/samsung/musicplus/player/FullPlayerView$10;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mViewScreenCoordinates:[I
    invoke-static {v6}, Lcom/samsung/musicplus/player/FullPlayerView;->access$3000(Lcom/samsung/musicplus/player/FullPlayerView;)[I

    move-result-object v6

    aget v6, v6, v8

    int-to-float v6, v6

    sub-float/2addr v5, v6

    aput v5, v4, v8

    .line 832
    iget-object v4, p0, Lcom/samsung/musicplus/player/FullPlayerView$10;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mGLAlbumArt:Lcom/samsung/musicplus/glwidget/GLGalleryView;
    invoke-static {v4}, Lcom/samsung/musicplus/player/FullPlayerView;->access$1600(Lcom/samsung/musicplus/player/FullPlayerView;)Lcom/samsung/musicplus/glwidget/GLGalleryView;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/musicplus/player/FullPlayerView$10;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mParticlesInCoord:[F
    invoke-static {v5}, Lcom/samsung/musicplus/player/FullPlayerView;->access$3100(Lcom/samsung/musicplus/player/FullPlayerView;)[F

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/musicplus/player/FullPlayerView$10;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mParticlesOutCoord:[F
    invoke-static {v6}, Lcom/samsung/musicplus/player/FullPlayerView;->access$3200(Lcom/samsung/musicplus/player/FullPlayerView;)[F

    move-result-object v6

    invoke-interface {v4, v5, v6}, Lcom/samsung/musicplus/glwidget/GLGalleryView;->getAlbumArtForCoord([F[F)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 837
    .end local v2    # "mGLAlbumArtview":Landroid/view/View;
    :cond_0
    if-nez v0, :cond_1

    iget-object v4, p0, Lcom/samsung/musicplus/player/FullPlayerView$10;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mGLRecommendedMusic:Lcom/samsung/musicplus/glwidget/GLGalleryView;
    invoke-static {v4}, Lcom/samsung/musicplus/player/FullPlayerView;->access$3300(Lcom/samsung/musicplus/player/FullPlayerView;)Lcom/samsung/musicplus/glwidget/GLGalleryView;

    move-result-object v4

    if-eqz v4, :cond_1

    iget-object v4, p0, Lcom/samsung/musicplus/player/FullPlayerView$10;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    invoke-virtual {v4}, Lcom/samsung/musicplus/player/FullPlayerView;->isLyricViewShowing()Z

    move-result v4

    if-nez v4, :cond_1

    .line 838
    iget-object v4, p0, Lcom/samsung/musicplus/player/FullPlayerView$10;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mGLRecommendedMusic:Lcom/samsung/musicplus/glwidget/GLGalleryView;
    invoke-static {v4}, Lcom/samsung/musicplus/player/FullPlayerView;->access$3300(Lcom/samsung/musicplus/player/FullPlayerView;)Lcom/samsung/musicplus/glwidget/GLGalleryView;

    move-result-object v4

    invoke-interface {v4}, Lcom/samsung/musicplus/glwidget/GLGalleryView;->asView()Landroid/view/View;

    move-result-object v3

    .line 839
    .local v3, "mGLRecommendedMusicview":Landroid/view/View;
    if-eqz v3, :cond_1

    .line 840
    iget-object v4, p0, Lcom/samsung/musicplus/player/FullPlayerView$10;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mViewScreenCoordinates:[I
    invoke-static {v4}, Lcom/samsung/musicplus/player/FullPlayerView;->access$3000(Lcom/samsung/musicplus/player/FullPlayerView;)[I

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 841
    iget-object v4, p0, Lcom/samsung/musicplus/player/FullPlayerView$10;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mViewScreenCoordinates:[I
    invoke-static {v4}, Lcom/samsung/musicplus/player/FullPlayerView;->access$3000(Lcom/samsung/musicplus/player/FullPlayerView;)[I

    move-result-object v4

    invoke-direct {p0, v4, v3, p2}, Lcom/samsung/musicplus/player/FullPlayerView$10;->isInsideView([ILandroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 842
    iget-object v4, p0, Lcom/samsung/musicplus/player/FullPlayerView$10;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mParticlesInCoord:[F
    invoke-static {v4}, Lcom/samsung/musicplus/player/FullPlayerView;->access$3100(Lcom/samsung/musicplus/player/FullPlayerView;)[F

    move-result-object v4

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v5

    iget-object v6, p0, Lcom/samsung/musicplus/player/FullPlayerView$10;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mViewScreenCoordinates:[I
    invoke-static {v6}, Lcom/samsung/musicplus/player/FullPlayerView;->access$3000(Lcom/samsung/musicplus/player/FullPlayerView;)[I

    move-result-object v6

    aget v6, v6, v7

    int-to-float v6, v6

    sub-float/2addr v5, v6

    aput v5, v4, v7

    .line 843
    iget-object v4, p0, Lcom/samsung/musicplus/player/FullPlayerView$10;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mParticlesInCoord:[F
    invoke-static {v4}, Lcom/samsung/musicplus/player/FullPlayerView;->access$3100(Lcom/samsung/musicplus/player/FullPlayerView;)[F

    move-result-object v4

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v5

    iget-object v6, p0, Lcom/samsung/musicplus/player/FullPlayerView$10;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mViewScreenCoordinates:[I
    invoke-static {v6}, Lcom/samsung/musicplus/player/FullPlayerView;->access$3000(Lcom/samsung/musicplus/player/FullPlayerView;)[I

    move-result-object v6

    aget v6, v6, v8

    int-to-float v6, v6

    sub-float/2addr v5, v6

    aput v5, v4, v8

    .line 844
    iget-object v4, p0, Lcom/samsung/musicplus/player/FullPlayerView$10;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mGLRecommendedMusic:Lcom/samsung/musicplus/glwidget/GLGalleryView;
    invoke-static {v4}, Lcom/samsung/musicplus/player/FullPlayerView;->access$3300(Lcom/samsung/musicplus/player/FullPlayerView;)Lcom/samsung/musicplus/glwidget/GLGalleryView;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/musicplus/player/FullPlayerView$10;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mParticlesInCoord:[F
    invoke-static {v5}, Lcom/samsung/musicplus/player/FullPlayerView;->access$3100(Lcom/samsung/musicplus/player/FullPlayerView;)[F

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/musicplus/player/FullPlayerView$10;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mParticlesOutCoord:[F
    invoke-static {v6}, Lcom/samsung/musicplus/player/FullPlayerView;->access$3200(Lcom/samsung/musicplus/player/FullPlayerView;)[F

    move-result-object v6

    invoke-interface {v4, v5, v6}, Lcom/samsung/musicplus/glwidget/GLGalleryView;->getAlbumArtForCoord([F[F)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 849
    .end local v3    # "mGLRecommendedMusicview":Landroid/view/View;
    :cond_1
    sget-boolean v4, Lcom/samsung/musicplus/util/Features;->FLAG_SUPPORT_PARTICLE_EFFECT:Z

    if-eqz v4, :cond_2

    .line 851
    iget-object v4, p0, Lcom/samsung/musicplus/player/FullPlayerView$10;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mParticleView:Landroid/view/View;
    invoke-static {v4}, Lcom/samsung/musicplus/player/FullPlayerView;->access$3400(Lcom/samsung/musicplus/player/FullPlayerView;)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->bringToFront()V

    .line 852
    if-eqz v0, :cond_2

    .line 853
    iget-object v4, p0, Lcom/samsung/musicplus/player/FullPlayerView$10;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mParticlesOutCoord:[F
    invoke-static {v4}, Lcom/samsung/musicplus/player/FullPlayerView;->access$3200(Lcom/samsung/musicplus/player/FullPlayerView;)[F

    move-result-object v4

    aget v4, v4, v7

    float-to-int v4, v4

    iget-object v5, p0, Lcom/samsung/musicplus/player/FullPlayerView$10;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mParticlesOutCoord:[F
    invoke-static {v5}, Lcom/samsung/musicplus/player/FullPlayerView;->access$3200(Lcom/samsung/musicplus/player/FullPlayerView;)[F

    move-result-object v5

    aget v5, v5, v8

    float-to-int v5, v5

    invoke-virtual {v0, v4, v5}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v1

    .line 855
    .local v1, "color":I
    iget-object v4, p0, Lcom/samsung/musicplus/player/FullPlayerView$10;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    iget-object v5, p0, Lcom/samsung/musicplus/player/FullPlayerView$10;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # invokes: Lcom/samsung/musicplus/player/FullPlayerView;->particlesCountForMotionEvent(Landroid/view/MotionEvent;)I
    invoke-static {v5, p2}, Lcom/samsung/musicplus/player/FullPlayerView;->access$3500(Lcom/samsung/musicplus/player/FullPlayerView;Landroid/view/MotionEvent;)I

    move-result v5

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v7

    # invokes: Lcom/samsung/musicplus/player/FullPlayerView;->emitParticles(IFFI)V
    invoke-static {v4, v5, v6, v7, v1}, Lcom/samsung/musicplus/player/FullPlayerView;->access$3600(Lcom/samsung/musicplus/player/FullPlayerView;IFFI)V

    .line 860
    .end local v0    # "bm":Landroid/graphics/Bitmap;
    .end local v1    # "color":I
    :cond_2
    return-void
.end method
