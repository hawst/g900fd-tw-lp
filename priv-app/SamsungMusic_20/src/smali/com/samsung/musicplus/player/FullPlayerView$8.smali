.class Lcom/samsung/musicplus/player/FullPlayerView$8;
.super Ljava/lang/Object;
.source "FullPlayerView.java"

# interfaces
.implements Lcom/samsung/musicplus/glwidget/GLGalleryView$OnAnimationListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/player/FullPlayerView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/player/FullPlayerView;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/player/FullPlayerView;)V
    .locals 0

    .prologue
    .line 662
    iput-object p1, p0, Lcom/samsung/musicplus/player/FullPlayerView$8;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationFinished(Lcom/samsung/musicplus/glwidget/GLGalleryView;)V
    .locals 8
    .param p1, "view"    # Lcom/samsung/musicplus/glwidget/GLGalleryView;

    .prologue
    const/4 v7, 0x0

    const/high16 v6, 0x3f800000    # 1.0f

    .line 666
    invoke-interface {p1}, Lcom/samsung/musicplus/glwidget/GLGalleryView;->getSelectedItemPosition()I

    move-result v3

    .line 668
    .local v3, "position":I
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getListPosition()I

    move-result v4

    if-eq v4, v3, :cond_0

    .line 669
    iget-object v4, p0, Lcom/samsung/musicplus/player/FullPlayerView$8;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mPlaySongRequestHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/samsung/musicplus/player/FullPlayerView;->access$2200(Lcom/samsung/musicplus/player/FullPlayerView;)Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/os/Handler;->removeMessages(I)V

    .line 670
    iget-object v4, p0, Lcom/samsung/musicplus/player/FullPlayerView$8;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mPlaySongRequestHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/samsung/musicplus/player/FullPlayerView;->access$2200(Lcom/samsung/musicplus/player/FullPlayerView;)Landroid/os/Handler;

    move-result-object v4

    const/4 v5, -0x1

    invoke-virtual {v4, v7, v3, v5}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v2

    .line 671
    .local v2, "msg":Landroid/os/Message;
    iget-object v4, p0, Lcom/samsung/musicplus/player/FullPlayerView$8;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mPlaySongRequestHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/samsung/musicplus/player/FullPlayerView;->access$2200(Lcom/samsung/musicplus/player/FullPlayerView;)Landroid/os/Handler;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 677
    .end local v2    # "msg":Landroid/os/Message;
    :goto_0
    iget-object v4, p0, Lcom/samsung/musicplus/player/FullPlayerView$8;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mAlbumUriPosition:Landroid/util/SparseArray;
    invoke-static {v4}, Lcom/samsung/musicplus/player/FullPlayerView;->access$1500(Lcom/samsung/musicplus/player/FullPlayerView;)Landroid/util/SparseArray;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 678
    .local v0, "albumArtUri":Landroid/net/Uri;
    const/4 v1, 0x0

    .line 685
    .local v1, "color":Ljava/lang/Integer;
    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mAverageColors:Ljava/util/HashMap;
    invoke-static {}, Lcom/samsung/musicplus/player/FullPlayerView;->access$2400()Ljava/util/HashMap;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .end local v1    # "color":Ljava/lang/Integer;
    check-cast v1, Ljava/lang/Integer;

    .line 687
    .restart local v1    # "color":Ljava/lang/Integer;
    if-nez v1, :cond_1

    .line 688
    iget-object v4, p0, Lcom/samsung/musicplus/player/FullPlayerView$8;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    iget-object v5, p0, Lcom/samsung/musicplus/player/FullPlayerView$8;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mDefaultAverageColor:I
    invoke-static {v5}, Lcom/samsung/musicplus/player/FullPlayerView;->access$2500(Lcom/samsung/musicplus/player/FullPlayerView;)I

    move-result v5

    # invokes: Lcom/samsung/musicplus/player/FullPlayerView;->setBackground(I)V
    invoke-static {v4, v5}, Lcom/samsung/musicplus/player/FullPlayerView;->access$2600(Lcom/samsung/musicplus/player/FullPlayerView;I)V

    .line 692
    :goto_1
    return-void

    .line 673
    .end local v0    # "albumArtUri":Landroid/net/Uri;
    .end local v1    # "color":Ljava/lang/Integer;
    :cond_0
    iget-object v4, p0, Lcom/samsung/musicplus/player/FullPlayerView$8;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    const/4 v5, 0x1

    # setter for: Lcom/samsung/musicplus/player/FullPlayerView;->mIsLyricsLoaded:Z
    invoke-static {v4, v5}, Lcom/samsung/musicplus/player/FullPlayerView;->access$302(Lcom/samsung/musicplus/player/FullPlayerView;Z)Z

    .line 674
    iget-object v4, p0, Lcom/samsung/musicplus/player/FullPlayerView$8;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mVolumeLimitController:Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;
    invoke-static {v4}, Lcom/samsung/musicplus/player/FullPlayerView;->access$2100(Lcom/samsung/musicplus/player/FullPlayerView;)Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;

    move-result-object v4

    const/16 v5, 0x4b0

    invoke-virtual {v4, v6, v6, v5}, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;->setLimitVolume(FFI)V

    goto :goto_0

    .line 690
    .restart local v0    # "albumArtUri":Landroid/net/Uri;
    .restart local v1    # "color":Ljava/lang/Integer;
    :cond_1
    iget-object v4, p0, Lcom/samsung/musicplus/player/FullPlayerView$8;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v5

    # invokes: Lcom/samsung/musicplus/player/FullPlayerView;->setBackground(I)V
    invoke-static {v4, v5}, Lcom/samsung/musicplus/player/FullPlayerView;->access$2600(Lcom/samsung/musicplus/player/FullPlayerView;I)V

    goto :goto_1
.end method

.method public onAnimationStarted(Lcom/samsung/musicplus/glwidget/GLGalleryView;)V
    .locals 2
    .param p1, "view"    # Lcom/samsung/musicplus/glwidget/GLGalleryView;

    .prologue
    .line 696
    iget-object v0, p0, Lcom/samsung/musicplus/player/FullPlayerView$8;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/musicplus/player/FullPlayerView;->mIsLyricsLoaded:Z
    invoke-static {v0, v1}, Lcom/samsung/musicplus/player/FullPlayerView;->access$302(Lcom/samsung/musicplus/player/FullPlayerView;Z)Z

    .line 697
    return-void
.end method

.method public onAnimationUpdate(Lcom/samsung/musicplus/glwidget/GLGalleryView;F)V
    .locals 16
    .param p1, "view"    # Lcom/samsung/musicplus/glwidget/GLGalleryView;
    .param p2, "fraction"    # F

    .prologue
    .line 701
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/musicplus/player/FullPlayerView$8;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # invokes: Lcom/samsung/musicplus/player/FullPlayerView;->hideLyricView()V
    invoke-static {v12}, Lcom/samsung/musicplus/player/FullPlayerView;->access$2700(Lcom/samsung/musicplus/player/FullPlayerView;)V

    .line 702
    invoke-interface/range {p1 .. p1}, Lcom/samsung/musicplus/glwidget/GLGalleryView;->isFlinging()Z

    move-result v12

    if-eqz v12, :cond_0

    .line 777
    :goto_0
    return-void

    .line 707
    :cond_0
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/musicplus/player/FullPlayerView$8;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mGLAlbumArt:Lcom/samsung/musicplus/glwidget/GLGalleryView;
    invoke-static {v12}, Lcom/samsung/musicplus/player/FullPlayerView;->access$1600(Lcom/samsung/musicplus/player/FullPlayerView;)Lcom/samsung/musicplus/glwidget/GLGalleryView;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/musicplus/player/FullPlayerView$8;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mCurrentSongPosition:I
    invoke-static {v13}, Lcom/samsung/musicplus/player/FullPlayerView;->access$2800(Lcom/samsung/musicplus/player/FullPlayerView;)I

    move-result v13

    invoke-interface {v12, v13}, Lcom/samsung/musicplus/glwidget/GLGalleryView;->getAlbumLastPosition(I)Lcom/samsung/musicplus/glwidget/layout/IAlbumArtInfo$PositionInfo;

    move-result-object v11

    .line 709
    .local v11, "position":Lcom/samsung/musicplus/glwidget/layout/IAlbumArtInfo$PositionInfo;
    if-eqz v11, :cond_1

    .line 710
    iget v12, v11, Lcom/samsung/musicplus/glwidget/layout/IAlbumArtInfo$PositionInfo;->alpha:F

    const v13, 0x3e4ccccd    # 0.2f

    sub-float/2addr v12, v13

    const/4 v13, 0x0

    cmpl-float v12, v12, v13

    if-ltz v12, :cond_6

    iget v12, v11, Lcom/samsung/musicplus/glwidget/layout/IAlbumArtInfo$PositionInfo;->alpha:F

    const v13, 0x3e4ccccd    # 0.2f

    sub-float v10, v12, v13

    .line 711
    .local v10, "outSide":F
    :goto_1
    iget v12, v11, Lcom/samsung/musicplus/glwidget/layout/IAlbumArtInfo$PositionInfo;->alpha:F

    const v13, 0x3dcccccd    # 0.1f

    add-float/2addr v12, v13

    const/high16 v13, 0x3f800000    # 1.0f

    cmpl-float v12, v12, v13

    if-lez v12, :cond_7

    const/high16 v6, 0x3f800000    # 1.0f

    .line 712
    .local v6, "insSide":F
    :goto_2
    iget v12, v11, Lcom/samsung/musicplus/glwidget/layout/IAlbumArtInfo$PositionInfo;->basePosition:I

    packed-switch v12, :pswitch_data_0

    .line 720
    :pswitch_0
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/musicplus/player/FullPlayerView$8;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mVolumeLimitController:Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;
    invoke-static {v12}, Lcom/samsung/musicplus/player/FullPlayerView;->access$2100(Lcom/samsung/musicplus/player/FullPlayerView;)Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;

    move-result-object v12

    const/high16 v13, 0x3f800000    # 1.0f

    const/high16 v14, 0x3f800000    # 1.0f

    const/4 v15, 0x0

    invoke-virtual {v12, v13, v14, v15}, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;->setLimitVolume(FFI)V

    .line 724
    .end local v6    # "insSide":F
    .end local v10    # "outSide":F
    :cond_1
    :goto_3
    const/4 v3, 0x0

    .line 725
    .local v3, "color1":Ljava/lang/Integer;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/musicplus/player/FullPlayerView$8;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mAlbumUriPosition:Landroid/util/SparseArray;
    invoke-static {v12}, Lcom/samsung/musicplus/player/FullPlayerView;->access$1500(Lcom/samsung/musicplus/player/FullPlayerView;)Landroid/util/SparseArray;

    move-result-object v12

    invoke-interface/range {p1 .. p1}, Lcom/samsung/musicplus/glwidget/GLGalleryView;->getSelectedItemPosition()I

    move-result v13

    invoke-virtual {v12, v13}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/net/Uri;

    .line 732
    .local v5, "curUri":Landroid/net/Uri;
    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mAverageColors:Ljava/util/HashMap;
    invoke-static {}, Lcom/samsung/musicplus/player/FullPlayerView;->access$2400()Ljava/util/HashMap;

    move-result-object v12

    invoke-virtual {v12, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    .end local v3    # "color1":Ljava/lang/Integer;
    check-cast v3, Ljava/lang/Integer;

    .line 735
    .restart local v3    # "color1":Ljava/lang/Integer;
    if-nez v3, :cond_2

    .line 736
    const-string v12, "MusicUi"

    const-string v13, "onAnimationUpdate can\'t get color1"

    invoke-static {v12, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 737
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/musicplus/player/FullPlayerView$8;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mDefaultAverageColor:I
    invoke-static {v12}, Lcom/samsung/musicplus/player/FullPlayerView;->access$2500(Lcom/samsung/musicplus/player/FullPlayerView;)I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 740
    :cond_2
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/musicplus/player/FullPlayerView$8;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mAlbumArtAdapter:Lcom/samsung/musicplus/player/GLAlbumArtAdapter;
    invoke-static {v12}, Lcom/samsung/musicplus/player/FullPlayerView;->access$1700(Lcom/samsung/musicplus/player/FullPlayerView;)Lcom/samsung/musicplus/player/GLAlbumArtAdapter;

    move-result-object v12

    invoke-virtual {v12}, Lcom/samsung/musicplus/player/GLAlbumArtAdapter;->getCount()I

    move-result v12

    int-to-float v1, v12

    .line 741
    .local v1, "adapterCount":F
    const/4 v12, 0x0

    cmpg-float v12, p2, v12

    if-gez v12, :cond_8

    .line 743
    invoke-interface/range {p1 .. p1}, Lcom/samsung/musicplus/glwidget/GLGalleryView;->getSelectedItemPosition()I

    move-result v12

    add-int/lit8 v8, v12, -0x1

    .line 744
    .local v8, "nextPosition":I
    if-gez v8, :cond_3

    .line 745
    int-to-float v12, v8

    add-float/2addr v12, v1

    float-to-int v8, v12

    .line 747
    :cond_3
    move/from16 v0, p2

    neg-float v0, v0

    move/from16 p2, v0

    .line 756
    :cond_4
    :goto_4
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/musicplus/player/FullPlayerView$8;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mAlbumUriPosition:Landroid/util/SparseArray;
    invoke-static {v12}, Lcom/samsung/musicplus/player/FullPlayerView;->access$1500(Lcom/samsung/musicplus/player/FullPlayerView;)Landroid/util/SparseArray;

    move-result-object v12

    invoke-virtual {v12, v8}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/net/Uri;

    .line 757
    .local v9, "nextUri":Landroid/net/Uri;
    const/4 v4, 0x0

    .line 766
    .local v4, "color2":Ljava/lang/Integer;
    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mAverageColors:Ljava/util/HashMap;
    invoke-static {}, Lcom/samsung/musicplus/player/FullPlayerView;->access$2400()Ljava/util/HashMap;

    move-result-object v12

    invoke-virtual {v12, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    .end local v4    # "color2":Ljava/lang/Integer;
    check-cast v4, Ljava/lang/Integer;

    .line 769
    .restart local v4    # "color2":Ljava/lang/Integer;
    if-nez v4, :cond_5

    .line 770
    const-string v12, "MusicUi"

    const-string v13, "onAnimationUpdate can\'t get color2"

    invoke-static {v12, v13}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 771
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/musicplus/player/FullPlayerView$8;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mDefaultAverageColor:I
    invoke-static {v12}, Lcom/samsung/musicplus/player/FullPlayerView;->access$2500(Lcom/samsung/musicplus/player/FullPlayerView;)I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 773
    :cond_5
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v12

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v13

    const/high16 v14, 0x3f000000    # 0.5f

    invoke-static {v12, v13, v14}, Lcom/samsung/musicplus/glwidget/utils/GLGalleryUtils;->mixColorsRGB(IIF)I

    move-result v7

    .line 775
    .local v7, "m":I
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v12

    move/from16 v0, p2

    invoke-static {v12, v7, v0}, Lcom/samsung/musicplus/glwidget/utils/GLGalleryUtils;->mixColorsRGB(IIF)I

    move-result v2

    .line 776
    .local v2, "color":I
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/musicplus/player/FullPlayerView$8;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # invokes: Lcom/samsung/musicplus/player/FullPlayerView;->setBackground(I)V
    invoke-static {v12, v2}, Lcom/samsung/musicplus/player/FullPlayerView;->access$2600(Lcom/samsung/musicplus/player/FullPlayerView;I)V

    goto/16 :goto_0

    .line 710
    .end local v1    # "adapterCount":F
    .end local v2    # "color":I
    .end local v3    # "color1":Ljava/lang/Integer;
    .end local v4    # "color2":Ljava/lang/Integer;
    .end local v5    # "curUri":Landroid/net/Uri;
    .end local v7    # "m":I
    .end local v8    # "nextPosition":I
    .end local v9    # "nextUri":Landroid/net/Uri;
    :cond_6
    const/4 v10, 0x0

    goto/16 :goto_1

    .line 711
    .restart local v10    # "outSide":F
    :cond_7
    iget v12, v11, Lcom/samsung/musicplus/glwidget/layout/IAlbumArtInfo$PositionInfo;->alpha:F

    const v13, 0x3dcccccd    # 0.1f

    add-float v6, v12, v13

    goto/16 :goto_2

    .line 714
    .restart local v6    # "insSide":F
    :pswitch_1
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/musicplus/player/FullPlayerView$8;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mVolumeLimitController:Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;
    invoke-static {v12}, Lcom/samsung/musicplus/player/FullPlayerView;->access$2100(Lcom/samsung/musicplus/player/FullPlayerView;)Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;

    move-result-object v12

    const/4 v13, 0x0

    invoke-virtual {v12, v10, v6, v13}, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;->setLimitVolume(FFI)V

    goto/16 :goto_3

    .line 717
    :pswitch_2
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/musicplus/player/FullPlayerView$8;->this$0:Lcom/samsung/musicplus/player/FullPlayerView;

    # getter for: Lcom/samsung/musicplus/player/FullPlayerView;->mVolumeLimitController:Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;
    invoke-static {v12}, Lcom/samsung/musicplus/player/FullPlayerView;->access$2100(Lcom/samsung/musicplus/player/FullPlayerView;)Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;

    move-result-object v12

    const/4 v13, 0x0

    invoke-virtual {v12, v6, v10, v13}, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;->setLimitVolume(FFI)V

    goto/16 :goto_3

    .line 750
    .end local v6    # "insSide":F
    .end local v10    # "outSide":F
    .restart local v1    # "adapterCount":F
    .restart local v3    # "color1":Ljava/lang/Integer;
    .restart local v5    # "curUri":Landroid/net/Uri;
    :cond_8
    invoke-interface/range {p1 .. p1}, Lcom/samsung/musicplus/glwidget/GLGalleryView;->getSelectedItemPosition()I

    move-result v12

    add-int/lit8 v8, v12, 0x1

    .line 751
    .restart local v8    # "nextPosition":I
    int-to-float v12, v8

    cmpl-float v12, v12, v1

    if-lez v12, :cond_4

    .line 752
    int-to-float v12, v8

    sub-float/2addr v12, v1

    float-to-int v8, v12

    goto/16 :goto_4

    .line 712
    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
