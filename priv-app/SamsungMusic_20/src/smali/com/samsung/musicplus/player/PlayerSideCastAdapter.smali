.class public Lcom/samsung/musicplus/player/PlayerSideCastAdapter;
.super Lcom/samsung/musicplus/widget/list/CommonListAdapter;
.source "PlayerSideCastAdapter.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "MusicSideCast"


# direct methods
.method public constructor <init>(Landroid/content/Context;ILandroid/database/Cursor;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "layout"    # I
    .param p3, "c"    # Landroid/database/Cursor;
    .param p4, "flags"    # I

    .prologue
    .line 23
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;I)V

    .line 24
    invoke-direct {p0, p1, p3}, Lcom/samsung/musicplus/player/PlayerSideCastAdapter;->initialize(Landroid/content/Context;Landroid/database/Cursor;)V

    .line 25
    return-void
.end method

.method private initialize(Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "c"    # Landroid/database/Cursor;

    .prologue
    .line 28
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c00f6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/player/PlayerSideCastAdapter;->mAlbumArtSize:I

    .line 30
    invoke-virtual {p0, p2}, Lcom/samsung/musicplus/player/PlayerSideCastAdapter;->setColumnIndex(Landroid/database/Cursor;)V

    .line 31
    return-void
.end method


# virtual methods
.method protected bindTextView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "c"    # Landroid/database/Cursor;

    .prologue
    .line 57
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->bindTextView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    .line 59
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;

    .line 60
    .local v0, "vh":Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;
    iget-object v1, v0, Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;->text3:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 61
    iget-object v1, v0, Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;->text3:Landroid/widget/TextView;

    iget v2, p0, Lcom/samsung/musicplus/player/PlayerSideCastAdapter;->mText3Index:I

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 63
    :cond_0
    return-void
.end method

.method protected newTextView(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 47
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->newTextView(Landroid/view/View;)V

    .line 49
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;

    .line 50
    .local v0, "vh":Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;
    const v1, 0x7f0d00a1

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v0, Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;->text3:Landroid/widget/TextView;

    .line 52
    invoke-virtual {p1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 53
    return-void
.end method

.method public setColumnIndex(Landroid/database/Cursor;)V
    .locals 1
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 34
    if-nez p1, :cond_0

    .line 43
    :goto_0
    return-void

    .line 38
    :cond_0
    const-string v0, "_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/player/PlayerSideCastAdapter;->mAudioIndex:I

    .line 39
    const-string v0, "title"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/player/PlayerSideCastAdapter;->mText1Index:I

    .line 40
    const-string v0, "artist"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/player/PlayerSideCastAdapter;->mText2Index:I

    .line 41
    const-string v0, "album"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/player/PlayerSideCastAdapter;->mText3Index:I

    .line 42
    const-string v0, "album_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/player/PlayerSideCastAdapter;->mAlbumIdIndex:I

    goto :goto_0
.end method
