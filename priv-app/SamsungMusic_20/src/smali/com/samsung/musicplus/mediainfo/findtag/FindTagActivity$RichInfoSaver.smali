.class Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$RichInfoSaver;
.super Landroid/os/AsyncTask;
.source "FindTagActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "RichInfoSaver"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# static fields
.field static final ENCRYPTED_RICHINFO_DATA:Ljava/lang/String; = "/EncryptedData/CipherData/CipherValue"


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;)V
    .locals 0

    .prologue
    .line 523
    iput-object p1, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$RichInfoSaver;->this$0:Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method private getEncryptedParam(Ljava/io/InputStream;)[B
    .locals 12
    .param p1, "in"    # Ljava/io/InputStream;

    .prologue
    const/4 v11, 0x1

    const/4 v8, 0x0

    .line 621
    const/4 v2, 0x0

    .line 622
    .local v2, "encryptedData":[B
    new-instance v6, Ljava/io/InputStreamReader;

    invoke-direct {v6, p1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    .line 626
    .local v6, "r":Ljava/io/Reader;
    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v4

    .line 627
    .local v4, "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    const/4 v9, 0x1

    invoke-virtual {v4, v9}, Lorg/xmlpull/v1/XmlPullParserFactory;->setNamespaceAware(Z)V

    .line 628
    invoke-virtual {v4}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v7

    .line 629
    .local v7, "xpp":Lorg/xmlpull/v1/XmlPullParser;
    invoke-interface {v7, v6}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 631
    new-instance v0, Ljava/lang/StringBuffer;

    const-string v9, ""

    invoke-direct {v0, v9}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 632
    .local v0, "current":Ljava/lang/StringBuffer;
    invoke-interface {v7}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v3

    .line 633
    .local v3, "eventType":I
    :goto_0
    if-eq v3, v11, :cond_1

    .line 634
    packed-switch v3, :pswitch_data_0

    .line 656
    :cond_0
    :goto_1
    invoke-interface {v7}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v3

    goto :goto_0

    .line 636
    :pswitch_0
    const-string v9, "/"

    invoke-virtual {v0, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 637
    invoke-interface {v7}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v0, v9}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_4

    goto :goto_1

    .line 658
    .end local v0    # "current":Ljava/lang/StringBuffer;
    .end local v3    # "eventType":I
    .end local v4    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v7    # "xpp":Lorg/xmlpull/v1/XmlPullParser;
    :catch_0
    move-exception v1

    .line 659
    .local v1, "e":Lorg/xmlpull/v1/XmlPullParserException;
    invoke-virtual {v1}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V

    .line 661
    :try_start_1
    invoke-virtual {v6}, Ljava/io/Reader;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3

    .line 690
    .end local v1    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    :goto_2
    return-object v8

    .line 640
    .restart local v0    # "current":Ljava/lang/StringBuffer;
    .restart local v3    # "eventType":I
    .restart local v4    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .restart local v7    # "xpp":Lorg/xmlpull/v1/XmlPullParser;
    :pswitch_1
    :try_start_2
    const-string v9, "/"

    invoke-virtual {v0, v9}, Ljava/lang/StringBuffer;->lastIndexOf(Ljava/lang/String;)I

    move-result v9

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->length()I

    move-result v10

    invoke-virtual {v0, v9, v10}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;
    :try_end_2
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_4

    goto :goto_1

    .line 666
    .end local v0    # "current":Ljava/lang/StringBuffer;
    .end local v3    # "eventType":I
    .end local v4    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v7    # "xpp":Lorg/xmlpull/v1/XmlPullParser;
    :catch_1
    move-exception v1

    .line 667
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    .line 669
    :try_start_3
    invoke-virtual {v6}, Ljava/io/Reader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_2

    .line 670
    :catch_2
    move-exception v5

    .line 671
    .local v5, "ie":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 648
    .end local v1    # "e":Ljava/io/IOException;
    .end local v5    # "ie":Ljava/io/IOException;
    .restart local v0    # "current":Ljava/lang/StringBuffer;
    .restart local v3    # "eventType":I
    .restart local v4    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .restart local v7    # "xpp":Lorg/xmlpull/v1/XmlPullParser;
    :pswitch_2
    :try_start_4
    const-string v9, "/EncryptedData/CipherData/CipherValue"

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    .line 649
    invoke-interface {v7}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->getBytes()[B

    move-result-object v9

    const/4 v10, 0x0

    invoke-static {v9, v10}, Landroid/util/Base64;->decode([BI)[B
    :try_end_4
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_4 .. :try_end_4} :catch_4

    move-result-object v2

    goto :goto_1

    .line 662
    .end local v0    # "current":Ljava/lang/StringBuffer;
    .end local v3    # "eventType":I
    .end local v4    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v7    # "xpp":Lorg/xmlpull/v1/XmlPullParser;
    .local v1, "e":Lorg/xmlpull/v1/XmlPullParserException;
    :catch_3
    move-exception v5

    .line 663
    .restart local v5    # "ie":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 674
    .end local v1    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    .end local v5    # "ie":Ljava/io/IOException;
    :catch_4
    move-exception v1

    .line 675
    .local v1, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v1}, Ljava/lang/NullPointerException;->printStackTrace()V

    .line 677
    :try_start_5
    invoke-virtual {v6}, Ljava/io/Reader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_5

    goto :goto_2

    .line 678
    :catch_5
    move-exception v5

    .line 679
    .restart local v5    # "ie":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 685
    .end local v1    # "e":Ljava/lang/NullPointerException;
    .end local v5    # "ie":Ljava/io/IOException;
    .restart local v0    # "current":Ljava/lang/StringBuffer;
    .restart local v3    # "eventType":I
    .restart local v4    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .restart local v7    # "xpp":Lorg/xmlpull/v1/XmlPullParser;
    :cond_1
    :try_start_6
    invoke-virtual {v6}, Ljava/io/Reader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_6

    move-object v8, v2

    .line 690
    goto :goto_2

    .line 686
    :catch_6
    move-exception v1

    .line 687
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 634
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 15
    .param p1, "url"    # [Ljava/lang/String;

    .prologue
    .line 552
    const/4 v10, 0x0

    .line 553
    .local v10, "isSaved":Z
    const/4 v0, 0x0

    .line 554
    .local v0, "bis":Ljava/io/InputStream;
    const/4 v7, 0x0

    .line 556
    .local v7, "fos":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v6, Ljava/net/URL;

    const/4 v13, 0x0

    aget-object v13, p1, v13

    invoke-direct {v6, v13}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 557
    .local v6, "findTagUrl":Ljava/net/URL;
    invoke-virtual {v6}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v4

    .line 560
    .local v4, "conn":Ljava/net/URLConnection;
    if-nez v4, :cond_2

    .line 561
    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_10
    .catch Ljava/lang/StringIndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_9
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v13

    .line 597
    if-eqz v0, :cond_0

    .line 598
    :try_start_1
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 604
    :cond_0
    :goto_0
    if-eqz v7, :cond_1

    .line 605
    :try_start_2
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 611
    .end local v4    # "conn":Ljava/net/URLConnection;
    .end local v6    # "findTagUrl":Ljava/net/URL;
    :cond_1
    :goto_1
    return-object v13

    .line 600
    .restart local v4    # "conn":Ljava/net/URLConnection;
    .restart local v6    # "findTagUrl":Ljava/net/URL;
    :catch_0
    move-exception v5

    .line 601
    .local v5, "e":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 607
    .end local v5    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v5

    .line 608
    .restart local v5    # "e":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 563
    .end local v5    # "e":Ljava/io/IOException;
    :cond_2
    :try_start_3
    invoke-virtual {v4}, Ljava/net/URLConnection;->connect()V

    .line 566
    invoke-virtual {v4}, Ljava/net/URLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v13

    invoke-direct {p0, v13}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$RichInfoSaver;->getEncryptedParam(Ljava/io/InputStream;)[B

    move-result-object v3

    .line 568
    .local v3, "bytes":[B
    if-nez v3, :cond_4

    .line 569
    const/4 v13, 0x0

    invoke-static {v13}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_10
    .catch Ljava/lang/StringIndexOutOfBoundsException; {:try_start_3 .. :try_end_3} :catch_9
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v13

    .line 597
    if-eqz v0, :cond_3

    .line 598
    :try_start_4
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 604
    :cond_3
    :goto_2
    if-eqz v7, :cond_1

    .line 605
    :try_start_5
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2

    goto :goto_1

    .line 607
    :catch_2
    move-exception v5

    .line 608
    .restart local v5    # "e":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 600
    .end local v5    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v5

    .line 601
    .restart local v5    # "e":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 571
    .end local v5    # "e":Ljava/io/IOException;
    :cond_4
    :try_start_6
    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, v3}, Ljava/io/ByteArrayInputStream;-><init>([B)V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_10
    .catch Ljava/lang/StringIndexOutOfBoundsException; {:try_start_6 .. :try_end_6} :catch_9
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 579
    .end local v0    # "bis":Ljava/io/InputStream;
    .local v1, "bis":Ljava/io/InputStream;
    :try_start_7
    iget-object v13, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$RichInfoSaver;->this$0:Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;

    # getter for: Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->mFilePath:Ljava/lang/String;
    invoke-static {v13}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->access$600(Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;)Ljava/lang/String;

    move-result-object v13

    const-string v14, "."

    invoke-virtual {v13, v14}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v9

    .line 581
    .local v9, "index":I
    iget-object v13, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$RichInfoSaver;->this$0:Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;

    # getter for: Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->mFilePath:Ljava/lang/String;
    invoke-static {v13}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->access$600(Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;)Ljava/lang/String;

    move-result-object v13

    const/4 v14, 0x0

    invoke-virtual {v13, v14, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v13

    const-string v14, ".xml"

    invoke-virtual {v13, v14}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 583
    .local v11, "path":Ljava/lang/String;
    new-instance v8, Ljava/io/FileOutputStream;

    new-instance v13, Ljava/io/File;

    invoke-direct {v13, v11}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {v8, v13}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_11
    .catch Ljava/lang/StringIndexOutOfBoundsException; {:try_start_7 .. :try_end_7} :catch_e
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 585
    .end local v7    # "fos":Ljava/io/FileOutputStream;
    .local v8, "fos":Ljava/io/FileOutputStream;
    const/4 v12, 0x0

    .line 586
    .local v12, "read":I
    const/16 v13, 0x400

    :try_start_8
    new-array v2, v13, [B

    .line 587
    .local v2, "buffer":[B
    :goto_3
    invoke-virtual {v1, v2}, Ljava/io/InputStream;->read([B)I

    move-result v12

    const/4 v13, -0x1

    if-eq v12, v13, :cond_7

    .line 588
    const/4 v13, 0x0

    invoke-virtual {v8, v2, v13, v12}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4
    .catch Ljava/lang/StringIndexOutOfBoundsException; {:try_start_8 .. :try_end_8} :catch_f
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    goto :goto_3

    .line 591
    .end local v2    # "buffer":[B
    :catch_4
    move-exception v5

    move-object v7, v8

    .end local v8    # "fos":Ljava/io/FileOutputStream;
    .restart local v7    # "fos":Ljava/io/FileOutputStream;
    move-object v0, v1

    .line 592
    .end local v1    # "bis":Ljava/io/InputStream;
    .end local v3    # "bytes":[B
    .end local v4    # "conn":Ljava/net/URLConnection;
    .end local v6    # "findTagUrl":Ljava/net/URL;
    .end local v9    # "index":I
    .end local v11    # "path":Ljava/lang/String;
    .end local v12    # "read":I
    .restart local v0    # "bis":Ljava/io/InputStream;
    .restart local v5    # "e":Ljava/io/IOException;
    :goto_4
    :try_start_9
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 597
    if-eqz v0, :cond_5

    .line 598
    :try_start_a
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_7

    .line 604
    :cond_5
    :goto_5
    if-eqz v7, :cond_6

    .line 605
    :try_start_b
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_8

    .line 611
    .end local v5    # "e":Ljava/io/IOException;
    :cond_6
    :goto_6
    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v13

    goto/16 :goto_1

    .line 590
    .end local v0    # "bis":Ljava/io/InputStream;
    .end local v7    # "fos":Ljava/io/FileOutputStream;
    .restart local v1    # "bis":Ljava/io/InputStream;
    .restart local v2    # "buffer":[B
    .restart local v3    # "bytes":[B
    .restart local v4    # "conn":Ljava/net/URLConnection;
    .restart local v6    # "findTagUrl":Ljava/net/URL;
    .restart local v8    # "fos":Ljava/io/FileOutputStream;
    .restart local v9    # "index":I
    .restart local v11    # "path":Ljava/lang/String;
    .restart local v12    # "read":I
    :cond_7
    const/4 v10, 0x1

    .line 597
    if-eqz v1, :cond_8

    .line 598
    :try_start_c
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_5

    .line 604
    :cond_8
    :goto_7
    if-eqz v8, :cond_9

    .line 605
    :try_start_d
    invoke-virtual {v8}, Ljava/io/FileOutputStream;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_6

    :cond_9
    move-object v7, v8

    .end local v8    # "fos":Ljava/io/FileOutputStream;
    .restart local v7    # "fos":Ljava/io/FileOutputStream;
    move-object v0, v1

    .line 609
    .end local v1    # "bis":Ljava/io/InputStream;
    .restart local v0    # "bis":Ljava/io/InputStream;
    goto :goto_6

    .line 600
    .end local v0    # "bis":Ljava/io/InputStream;
    .end local v7    # "fos":Ljava/io/FileOutputStream;
    .restart local v1    # "bis":Ljava/io/InputStream;
    .restart local v8    # "fos":Ljava/io/FileOutputStream;
    :catch_5
    move-exception v5

    .line 601
    .restart local v5    # "e":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_7

    .line 607
    .end local v5    # "e":Ljava/io/IOException;
    :catch_6
    move-exception v5

    .line 608
    .restart local v5    # "e":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    move-object v7, v8

    .end local v8    # "fos":Ljava/io/FileOutputStream;
    .restart local v7    # "fos":Ljava/io/FileOutputStream;
    move-object v0, v1

    .line 610
    .end local v1    # "bis":Ljava/io/InputStream;
    .restart local v0    # "bis":Ljava/io/InputStream;
    goto :goto_6

    .line 600
    .end local v2    # "buffer":[B
    .end local v3    # "bytes":[B
    .end local v4    # "conn":Ljava/net/URLConnection;
    .end local v6    # "findTagUrl":Ljava/net/URL;
    .end local v9    # "index":I
    .end local v11    # "path":Ljava/lang/String;
    .end local v12    # "read":I
    :catch_7
    move-exception v5

    .line 601
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 607
    :catch_8
    move-exception v5

    .line 608
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 593
    .end local v5    # "e":Ljava/io/IOException;
    :catch_9
    move-exception v5

    .line 594
    .local v5, "e":Ljava/lang/StringIndexOutOfBoundsException;
    :goto_8
    :try_start_e
    invoke-virtual {v5}, Ljava/lang/StringIndexOutOfBoundsException;->printStackTrace()V
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    .line 597
    if-eqz v0, :cond_a

    .line 598
    :try_start_f
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_b

    .line 604
    .end local v5    # "e":Ljava/lang/StringIndexOutOfBoundsException;
    :cond_a
    :goto_9
    if-eqz v7, :cond_6

    .line 605
    :try_start_10
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_a

    goto :goto_6

    .line 607
    :catch_a
    move-exception v5

    .line 608
    .local v5, "e":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 600
    .local v5, "e":Ljava/lang/StringIndexOutOfBoundsException;
    :catch_b
    move-exception v5

    .line 601
    .local v5, "e":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_9

    .line 596
    .end local v5    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v13

    .line 597
    :goto_a
    if-eqz v0, :cond_b

    .line 598
    :try_start_11
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_11
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_c

    .line 604
    :cond_b
    :goto_b
    if-eqz v7, :cond_c

    .line 605
    :try_start_12
    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V
    :try_end_12
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_12} :catch_d

    .line 609
    :cond_c
    :goto_c
    throw v13

    .line 600
    :catch_c
    move-exception v5

    .line 601
    .restart local v5    # "e":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_b

    .line 607
    .end local v5    # "e":Ljava/io/IOException;
    :catch_d
    move-exception v5

    .line 608
    .restart local v5    # "e":Ljava/io/IOException;
    invoke-virtual {v5}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_c

    .line 596
    .end local v0    # "bis":Ljava/io/InputStream;
    .end local v5    # "e":Ljava/io/IOException;
    .restart local v1    # "bis":Ljava/io/InputStream;
    .restart local v3    # "bytes":[B
    .restart local v4    # "conn":Ljava/net/URLConnection;
    .restart local v6    # "findTagUrl":Ljava/net/URL;
    :catchall_1
    move-exception v13

    move-object v0, v1

    .end local v1    # "bis":Ljava/io/InputStream;
    .restart local v0    # "bis":Ljava/io/InputStream;
    goto :goto_a

    .end local v0    # "bis":Ljava/io/InputStream;
    .end local v7    # "fos":Ljava/io/FileOutputStream;
    .restart local v1    # "bis":Ljava/io/InputStream;
    .restart local v8    # "fos":Ljava/io/FileOutputStream;
    .restart local v9    # "index":I
    .restart local v11    # "path":Ljava/lang/String;
    .restart local v12    # "read":I
    :catchall_2
    move-exception v13

    move-object v7, v8

    .end local v8    # "fos":Ljava/io/FileOutputStream;
    .restart local v7    # "fos":Ljava/io/FileOutputStream;
    move-object v0, v1

    .end local v1    # "bis":Ljava/io/InputStream;
    .restart local v0    # "bis":Ljava/io/InputStream;
    goto :goto_a

    .line 593
    .end local v0    # "bis":Ljava/io/InputStream;
    .end local v9    # "index":I
    .end local v11    # "path":Ljava/lang/String;
    .end local v12    # "read":I
    .restart local v1    # "bis":Ljava/io/InputStream;
    :catch_e
    move-exception v5

    move-object v0, v1

    .end local v1    # "bis":Ljava/io/InputStream;
    .restart local v0    # "bis":Ljava/io/InputStream;
    goto :goto_8

    .end local v0    # "bis":Ljava/io/InputStream;
    .end local v7    # "fos":Ljava/io/FileOutputStream;
    .restart local v1    # "bis":Ljava/io/InputStream;
    .restart local v8    # "fos":Ljava/io/FileOutputStream;
    .restart local v9    # "index":I
    .restart local v11    # "path":Ljava/lang/String;
    .restart local v12    # "read":I
    :catch_f
    move-exception v5

    move-object v7, v8

    .end local v8    # "fos":Ljava/io/FileOutputStream;
    .restart local v7    # "fos":Ljava/io/FileOutputStream;
    move-object v0, v1

    .end local v1    # "bis":Ljava/io/InputStream;
    .restart local v0    # "bis":Ljava/io/InputStream;
    goto :goto_8

    .line 591
    .end local v3    # "bytes":[B
    .end local v4    # "conn":Ljava/net/URLConnection;
    .end local v6    # "findTagUrl":Ljava/net/URL;
    .end local v9    # "index":I
    .end local v11    # "path":Ljava/lang/String;
    .end local v12    # "read":I
    :catch_10
    move-exception v5

    goto :goto_4

    .end local v0    # "bis":Ljava/io/InputStream;
    .restart local v1    # "bis":Ljava/io/InputStream;
    .restart local v3    # "bytes":[B
    .restart local v4    # "conn":Ljava/net/URLConnection;
    .restart local v6    # "findTagUrl":Ljava/net/URL;
    :catch_11
    move-exception v5

    move-object v0, v1

    .end local v1    # "bis":Ljava/io/InputStream;
    .restart local v0    # "bis":Ljava/io/InputStream;
    goto :goto_4
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 523
    check-cast p1, [Ljava/lang/String;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$RichInfoSaver;->doInBackground([Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 2
    .param p1, "result"    # Ljava/lang/Boolean;

    .prologue
    .line 538
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$RichInfoSaver;->this$0:Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->mIsSavingResults:Z
    invoke-static {v0, v1}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->access$1502(Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;Z)Z

    .line 539
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$RichInfoSaver;->this$0:Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;

    # invokes: Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->hideProgressDialog()V
    invoke-static {v0}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->access$400(Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;)V

    .line 540
    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 543
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$RichInfoSaver;->this$0:Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->setResult(I)V

    .line 547
    :goto_0
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$RichInfoSaver;->this$0:Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;

    invoke-virtual {v0}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->finish()V

    .line 548
    return-void

    .line 545
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$RichInfoSaver;->this$0:Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->setResult(I)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 523
    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$RichInfoSaver;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 3

    .prologue
    .line 529
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$RichInfoSaver;->this$0:Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;

    const/4 v1, 0x1

    # setter for: Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->mIsSavingResults:Z
    invoke-static {v0, v1}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->access$1502(Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;Z)Z

    .line 530
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$RichInfoSaver;->this$0:Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;

    new-instance v1, Landroid/app/ProgressDialog;

    iget-object v2, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$RichInfoSaver;->this$0:Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;

    invoke-direct {v1, v2}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    # setter for: Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0, v1}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->access$1002(Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    .line 531
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$RichInfoSaver;->this$0:Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;

    # getter for: Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->access$1000(Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 532
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$RichInfoSaver;->this$0:Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;

    # getter for: Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->access$1000(Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$RichInfoSaver;->this$0:Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;

    invoke-virtual {v1}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f100148

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 533
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$RichInfoSaver;->this$0:Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;

    # getter for: Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->access$1000(Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 534
    return-void
.end method
