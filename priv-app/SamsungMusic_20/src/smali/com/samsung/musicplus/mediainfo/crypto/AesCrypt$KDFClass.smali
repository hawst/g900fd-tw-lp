.class Lcom/samsung/musicplus/mediainfo/crypto/AesCrypt$KDFClass;
.super Ljava/lang/Object;
.source "AesCrypt.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/mediainfo/crypto/AesCrypt;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "KDFClass"
.end annotation


# static fields
.field private static final InitSeed:[B

.field private static final KDF_KEYLEN:I = 0x10

.field private static final SHA1_DIGEST_VALUELEN:I = 0x14


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 183
    const/16 v0, 0x40

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/samsung/musicplus/mediainfo/crypto/AesCrypt$KDFClass;->InitSeed:[B

    return-void

    :array_0
    .array-data 1
        0x54t
        -0x27t
        -0x13t
        0x7ft
        0x5at
        0x14t
        -0x2et
        0xbt
        -0x18t
        0x78t
        -0x59t
        -0x75t
        0x70t
        0x1et
        -0x18t
        -0x57t
        0x4ct
        0x42t
        -0x63t
        -0x25t
        0x39t
        -0xft
        -0x40t
        0x61t
        -0x15t
        -0x1bt
        -0x6ct
        -0x31t
        0xdt
        -0x53t
        0x19t
        -0x48t
        -0x24t
        -0x56t
        -0x17t
        -0x1ft
        -0x5ft
        0x23t
        0x3et
        -0x5t
        0x4t
        0x77t
        0x50t
        -0x66t
        -0x42t
        0x27t
        0x3dt
        0x11t
        0x7dt
        0xft
        0x2et
        0x3ct
        0x3ft
        -0x37t
        0x26t
        0x62t
        0x18t
        -0x75t
        -0x6dt
        0x75t
        -0x80t
        0x3t
        -0x2t
        0x3bt
    .end array-data
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 182
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static SecCryptoKDF()[B
    .locals 20

    .prologue
    .line 202
    sget-object v16, Lcom/samsung/musicplus/mediainfo/crypto/AesCrypt$KDFClass;->InitSeed:[B

    .line 204
    .local v16, "seedKey":[B
    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v17, v0

    div-int/lit8 v3, v17, 0x14

    .line 205
    .local v3, "D":I
    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v17, v0

    rem-int/lit8 v5, v17, 0x14

    .line 207
    .local v5, "chk":I
    if-lez v5, :cond_0

    .line 208
    add-int/lit8 v3, v3, 0x1

    .line 211
    :cond_0
    mul-int/lit8 v17, v3, 0x14

    add-int/lit8 v17, v17, 0x1

    move/from16 v0, v17

    new-array v4, v0, [B

    .line 213
    .local v4, "T":[B
    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v17, v0

    add-int/lit8 v12, v17, 0x4

    .line 214
    .local v12, "length":I
    const/4 v9, 0x0

    .line 215
    .local v9, "idx":I
    new-array v10, v12, [B

    .line 216
    .local v10, "inVal":[B
    const/16 v17, 0x5

    move/from16 v0, v17

    new-array v6, v0, [B

    .line 218
    .local v6, "counter":[B
    const/4 v13, 0x0

    .line 220
    .local v13, "md":Ljava/security/MessageDigest;
    :try_start_0
    const-string v17, "SHA-1"

    invoke-static/range {v17 .. v17}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v13

    .line 226
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    if-ge v8, v3, :cond_6

    .line 227
    const/16 v17, 0x3

    add-int/lit8 v18, v8, 0x1

    move/from16 v0, v18

    int-to-byte v0, v0

    move/from16 v18, v0

    aput-byte v18, v6, v17

    .line 228
    const/4 v11, 0x0

    .local v11, "j":I
    :goto_1
    if-ge v11, v12, :cond_4

    .line 229
    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v17, v0

    move/from16 v0, v17

    if-ge v11, v0, :cond_2

    .line 230
    aget-byte v17, v16, v11

    aput-byte v17, v10, v11

    .line 228
    :cond_1
    :goto_2
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 221
    .end local v8    # "i":I
    .end local v11    # "j":I
    :catch_0
    move-exception v7

    .line 222
    .local v7, "e":Ljava/security/NoSuchAlgorithmException;
    invoke-virtual {v7}, Ljava/security/NoSuchAlgorithmException;->printStackTrace()V

    .line 223
    const/4 v15, 0x0

    .line 249
    .end local v7    # "e":Ljava/security/NoSuchAlgorithmException;
    :goto_3
    return-object v15

    .line 231
    .restart local v8    # "i":I
    .restart local v11    # "j":I
    :cond_2
    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v17, v0

    add-int/lit8 v17, v17, 0x4

    move/from16 v0, v17

    if-ge v11, v0, :cond_3

    .line 232
    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v17, v0

    sub-int v17, v11, v17

    aget-byte v17, v6, v17

    aput-byte v17, v10, v11

    goto :goto_2

    .line 233
    :cond_3
    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v17, v0

    add-int/lit8 v17, v17, 0x4

    move/from16 v0, v17

    if-ge v11, v0, :cond_1

    .line 234
    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v17, v0

    sub-int v17, v11, v17

    add-int/lit8 v17, v17, -0x4

    aget-byte v17, v16, v17

    aput-byte v17, v10, v11

    goto :goto_2

    .line 237
    :cond_4
    invoke-virtual {v13, v10}, Ljava/security/MessageDigest;->update([B)V

    .line 238
    invoke-virtual {v13}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v14

    .line 239
    .local v14, "outVal":[B
    const/4 v11, 0x0

    :goto_4
    const/16 v17, 0x14

    move/from16 v0, v17

    if-ge v11, v0, :cond_5

    .line 240
    aget-byte v17, v14, v11

    aput-byte v17, v4, v9

    .line 241
    add-int/lit8 v9, v9, 0x1

    .line 239
    add-int/lit8 v11, v11, 0x1

    goto :goto_4

    .line 226
    :cond_5
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 244
    .end local v11    # "j":I
    .end local v14    # "outVal":[B
    :cond_6
    invoke-virtual {v13, v4}, Ljava/security/MessageDigest;->update([B)V

    .line 245
    invoke-virtual {v13}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v14

    .line 247
    .restart local v14    # "outVal":[B
    const/16 v17, 0x10

    move/from16 v0, v17

    new-array v15, v0, [B

    .line 248
    .local v15, "secretKey":[B
    const/16 v17, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x10

    move/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-static {v14, v0, v15, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_3
.end method
