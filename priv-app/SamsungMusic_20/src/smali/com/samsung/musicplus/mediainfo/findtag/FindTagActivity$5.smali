.class Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$5;
.super Ljava/lang/Thread;
.source "FindTagActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->requestUMSinfo()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;

.field final synthetic val$responseHandler:Lorg/apache/http/client/ResponseHandler;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;Lorg/apache/http/client/ResponseHandler;)V
    .locals 0

    .prologue
    .line 264
    iput-object p1, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$5;->this$0:Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;

    iput-object p2, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$5;->val$responseHandler:Lorg/apache/http/client/ResponseHandler;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    const/4 v9, 0x5

    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 267
    const-string v1, "http://ums.samsungmobile.com/http_client/find_url.jsp"

    .line 268
    .local v1, "url":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$5;->this$0:Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;

    const-string v4, "phone"

    invoke-virtual {v3, v4}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/telephony/TelephonyManager;

    .line 269
    .local v7, "tm":Landroid/telephony/TelephonyManager;
    invoke-virtual {v7}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v3

    if-ne v3, v9, :cond_3

    .line 275
    const/4 v6, 0x0

    .line 279
    .local v6, "imsi":Ljava/lang/String;
    invoke-virtual {v7}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-eq v3, v9, :cond_0

    invoke-virtual {v7}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    const/4 v4, 0x6

    if-ne v3, v4, :cond_1

    .line 281
    :cond_0
    invoke-virtual {v7}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v8, 0x3

    invoke-virtual {v3, v4, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    const-string v4, "etc"

    invoke-virtual {v3, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 287
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 289
    .local v5, "params":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v3, "pver"

    const-string v4, "1.0"

    invoke-virtual {v5, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 290
    sget-boolean v3, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoUtils;->sIsOpenRom:Z

    if-eqz v3, :cond_2

    .line 293
    const-string v3, "ptype"

    const-string v4, "O"

    invoke-virtual {v5, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 299
    :goto_0
    const-string v3, "imsi"

    invoke-virtual {v5, v3, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 301
    new-instance v0, Lcom/samsung/musicplus/util/HTTPRequestHelper;

    iget-object v3, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$5;->val$responseHandler:Lorg/apache/http/client/ResponseHandler;

    invoke-direct {v0, v3}, Lcom/samsung/musicplus/util/HTTPRequestHelper;-><init>(Lorg/apache/http/client/ResponseHandler;)V

    .local v0, "helper":Lcom/samsung/musicplus/util/HTTPRequestHelper;
    move-object v3, v2

    move-object v4, v2

    .line 302
    invoke-virtual/range {v0 .. v5}, Lcom/samsung/musicplus/util/HTTPRequestHelper;->performPost(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/Map;Ljava/util/Map;)V

    .line 308
    .end local v0    # "helper":Lcom/samsung/musicplus/util/HTTPRequestHelper;
    .end local v5    # "params":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v6    # "imsi":Ljava/lang/String;
    :goto_1
    return-void

    .line 283
    .restart local v6    # "imsi":Ljava/lang/String;
    :cond_1
    iget-object v2, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$5;->this$0:Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;

    # getter for: Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->mRichInfoResponseHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->access$1100(Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v8}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1

    .line 297
    .restart local v5    # "params":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    :cond_2
    const-string v3, "ptype"

    const-string v4, "T"

    invoke-virtual {v5, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 306
    .end local v5    # "params":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    .end local v6    # "imsi":Ljava/lang/String;
    :cond_3
    iget-object v2, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$5;->this$0:Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;

    # getter for: Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->mRichInfoResponseHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->access$1100(Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v8}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_1
.end method
