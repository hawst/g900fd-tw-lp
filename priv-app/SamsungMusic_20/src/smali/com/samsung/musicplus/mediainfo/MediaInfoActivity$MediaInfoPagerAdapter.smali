.class public Lcom/samsung/musicplus/mediainfo/MediaInfoActivity$MediaInfoPagerAdapter;
.super Landroid/support/v13/app/FragmentPagerAdapter;
.source "MediaInfoActivity.java"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "MediaInfoPagerAdapter"
.end annotation


# instance fields
.field private mDefaultFragment:Lcom/samsung/musicplus/mediainfo/MediaInfoDefaultFragment;

.field private mFragments:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/app/Fragment;",
            ">;"
        }
    .end annotation
.end field

.field private mPageTitles:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;


# direct methods
.method public constructor <init>(Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;Landroid/app/Activity;I)V
    .locals 2
    .param p2, "a"    # Landroid/app/Activity;
    .param p3, "listType"    # I

    .prologue
    .line 457
    iput-object p1, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity$MediaInfoPagerAdapter;->this$0:Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;

    .line 458
    invoke-virtual {p2}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/support/v13/app/FragmentPagerAdapter;-><init>(Landroid/app/FragmentManager;)V

    .line 451
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity$MediaInfoPagerAdapter;->mFragments:Ljava/util/List;

    .line 453
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity$MediaInfoPagerAdapter;->mPageTitles:Ljava/util/List;

    .line 459
    # getter for: Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mListType:I
    invoke-static {p1}, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->access$500(Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;)I

    move-result v0

    invoke-static {v0}, Lcom/samsung/musicplus/mediainfo/MediaInfoDefaultFragment;->newInstance(I)Lcom/samsung/musicplus/mediainfo/MediaInfoDefaultFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity$MediaInfoPagerAdapter;->mDefaultFragment:Lcom/samsung/musicplus/mediainfo/MediaInfoDefaultFragment;

    .line 460
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity$MediaInfoPagerAdapter;->mDefaultFragment:Lcom/samsung/musicplus/mediainfo/MediaInfoDefaultFragment;

    const v1, 0x7f10011a

    invoke-virtual {p0, v0, v1}, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity$MediaInfoPagerAdapter;->addFragment(Landroid/app/Fragment;I)V

    .line 461
    return-void
.end method


# virtual methods
.method public final addFragment(Landroid/app/Fragment;I)V
    .locals 2
    .param p1, "fg"    # Landroid/app/Fragment;
    .param p2, "title"    # I

    .prologue
    .line 474
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity$MediaInfoPagerAdapter;->mFragments:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 475
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity$MediaInfoPagerAdapter;->mPageTitles:Ljava/util/List;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 476
    invoke-virtual {p0}, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity$MediaInfoPagerAdapter;->notifyDataSetChanged()V

    .line 477
    return-void
.end method

.method public clearFragments()V
    .locals 2

    .prologue
    .line 480
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity$MediaInfoPagerAdapter;->mFragments:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 481
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity$MediaInfoPagerAdapter;->mPageTitles:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 482
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity$MediaInfoPagerAdapter;->mDefaultFragment:Lcom/samsung/musicplus/mediainfo/MediaInfoDefaultFragment;

    const v1, 0x7f10011a

    invoke-virtual {p0, v0, v1}, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity$MediaInfoPagerAdapter;->addFragment(Landroid/app/Fragment;I)V

    .line 483
    invoke-virtual {p0}, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity$MediaInfoPagerAdapter;->notifyDataSetChanged()V

    .line 484
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 470
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity$MediaInfoPagerAdapter;->mFragments:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public getCurrentTitle(I)Ljava/lang/String;
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 495
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity$MediaInfoPagerAdapter;->this$0:Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;

    invoke-virtual {v0}, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity$MediaInfoPagerAdapter;->mPageTitles:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getItem(I)Landroid/app/Fragment;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 465
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity$MediaInfoPagerAdapter;->mFragments:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Fragment;

    return-object v0
.end method

.method public onPageScrollStateChanged(I)V
    .locals 0
    .param p1, "state"    # I

    .prologue
    .line 488
    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 0
    .param p1, "position"    # I
    .param p2, "positionOffset"    # F
    .param p3, "positionOffsetPixels"    # I

    .prologue
    .line 492
    return-void
.end method

.method public onPageSelected(I)V
    .locals 5
    .param p1, "position"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 500
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity$MediaInfoPagerAdapter;->this$0:Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;

    # getter for: Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mHeaderTitle:Landroid/widget/TextView;
    invoke-static {v0}, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->access$600(Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;)Landroid/widget/TextView;

    move-result-object v3

    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity$MediaInfoPagerAdapter;->this$0:Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;

    invoke-virtual {v0}, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity$MediaInfoPagerAdapter;->mPageTitles:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 501
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity$MediaInfoPagerAdapter;->this$0:Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;

    # getter for: Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mArrowLeft:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->access$700(Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;)Landroid/widget/ImageView;

    move-result-object v3

    if-lez p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 502
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity$MediaInfoPagerAdapter;->this$0:Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;

    # getter for: Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mArrowRight:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->access$800(Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;)Landroid/widget/ImageView;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity$MediaInfoPagerAdapter;->getCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-ge p1, v3, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 503
    return-void

    :cond_0
    move v0, v2

    .line 501
    goto :goto_0

    :cond_1
    move v1, v2

    .line 502
    goto :goto_1
.end method

.method public setMetaDataInfo(Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;)V
    .locals 2
    .param p1, "metaData"    # Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;

    .prologue
    .line 510
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity$MediaInfoPagerAdapter;->mDefaultFragment:Lcom/samsung/musicplus/mediainfo/MediaInfoDefaultFragment;

    if-eqz v0, :cond_0

    .line 511
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity$MediaInfoPagerAdapter;->mDefaultFragment:Lcom/samsung/musicplus/mediainfo/MediaInfoDefaultFragment;

    iget-object v1, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity$MediaInfoPagerAdapter;->this$0:Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;

    invoke-virtual {v1}, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/samsung/musicplus/mediainfo/MediaInfoDefaultFragment;->setBasicMetaInfo(Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;Landroid/content/Context;)V

    .line 513
    :cond_0
    return-void
.end method
