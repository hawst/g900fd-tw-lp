.class public Lcom/samsung/musicplus/mediainfo/findtag/FindTagResultDialog;
.super Landroid/app/DialogFragment;
.source "FindTagResultDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/mediainfo/findtag/FindTagResultDialog$FindTagResultListAdapter;
    }
.end annotation


# instance fields
.field private mFindTags:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$FindTag;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 29
    return-void
.end method

.method public constructor <init>(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$FindTag;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 31
    .local p1, "findTags":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$FindTag;>;"
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagResultDialog;->mFindTags:Ljava/util/ArrayList;

    .line 33
    return-void
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 1
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 56
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    .line 57
    invoke-virtual {p0}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagResultDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 58
    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 64
    const/4 v1, -0x1

    if-le p2, v1, :cond_1

    .line 65
    invoke-virtual {p0}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagResultDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 66
    .local v0, "a":Landroid/app/Activity;
    instance-of v1, v0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;

    if-eqz v1, :cond_0

    .line 67
    check-cast v0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;

    .end local v0    # "a":Landroid/app/Activity;
    iget-object v1, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagResultDialog;->mFindTags:Ljava/util/ArrayList;

    invoke-virtual {v1, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$FindTag;

    iget-object v1, v1, Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$FindTag;->url:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->saveRichInfoData(Ljava/lang/String;)V

    .line 69
    :cond_0
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagResultDialog;->onDismiss(Landroid/content/DialogInterface;)V

    .line 73
    :goto_0
    return-void

    .line 71
    :cond_1
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagResultDialog;->onCancel(Landroid/content/DialogInterface;)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 37
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 38
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagResultDialog;->setRetainInstance(Z)V

    .line 39
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 43
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagResultDialog;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f100150

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    .line 45
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    iget-object v1, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagResultDialog;->mFindTags:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagResultDialog;->mFindTags:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_1

    .line 46
    :cond_0
    const v1, 0x7f100102

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f100114

    invoke-virtual {v1, v2, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 51
    :goto_0
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    return-object v1

    .line 48
    :cond_1
    new-instance v1, Lcom/samsung/musicplus/mediainfo/findtag/FindTagResultDialog$FindTagResultListAdapter;

    invoke-virtual {p0}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagResultDialog;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const v3, 0x7f040045

    iget-object v4, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagResultDialog;->mFindTags:Ljava/util/ArrayList;

    invoke-direct {v1, p0, v2, v3, v4}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagResultDialog$FindTagResultListAdapter;-><init>(Lcom/samsung/musicplus/mediainfo/findtag/FindTagResultDialog;Landroid/content/Context;ILjava/util/ArrayList;)V

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_0
.end method
