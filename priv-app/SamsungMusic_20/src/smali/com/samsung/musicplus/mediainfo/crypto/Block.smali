.class public Lcom/samsung/musicplus/mediainfo/crypto/Block;
.super Ljava/lang/Object;
.source "Block.java"


# instance fields
.field public data:[B


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    const/16 v0, 0x8

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/samsung/musicplus/mediainfo/crypto/Block;->data:[B

    .line 21
    return-void
.end method


# virtual methods
.method public copy(Lcom/samsung/musicplus/mediainfo/crypto/Block;)V
    .locals 3
    .param p1, "src"    # Lcom/samsung/musicplus/mediainfo/crypto/Block;

    .prologue
    .line 70
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v1, 0x8

    if-ge v0, v1, :cond_0

    .line 71
    iget-object v1, p0, Lcom/samsung/musicplus/mediainfo/crypto/Block;->data:[B

    iget-object v2, p1, Lcom/samsung/musicplus/mediainfo/crypto/Block;->data:[B

    aget-byte v2, v2, v0

    aput-byte v2, v1, v0

    .line 70
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 73
    :cond_0
    return-void
.end method

.method public pad(ILcom/samsung/musicplus/mediainfo/crypto/Block;)V
    .locals 3
    .param p1, "n"    # I
    .param p2, "pad"    # Lcom/samsung/musicplus/mediainfo/crypto/Block;

    .prologue
    .line 88
    move v0, p1

    .local v0, "i":I
    :goto_0
    const/16 v1, 0x8

    if-ge v0, v1, :cond_0

    .line 89
    iget-object v1, p0, Lcom/samsung/musicplus/mediainfo/crypto/Block;->data:[B

    iget-object v2, p2, Lcom/samsung/musicplus/mediainfo/crypto/Block;->data:[B

    aget-byte v2, v2, v0

    aput-byte v2, v1, v0

    .line 88
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 92
    :cond_0
    return-void
.end method

.method public read(Ljava/io/InputStream;)I
    .locals 4
    .param p1, "f"    # Ljava/io/InputStream;

    .prologue
    .line 50
    const/4 v1, 0x0

    .line 55
    .local v1, "tot":I
    :goto_0
    const/16 v2, 0x8

    if-ge v1, v2, :cond_0

    .line 56
    :try_start_0
    iget-object v2, p0, Lcom/samsung/musicplus/mediainfo/crypto/Block;->data:[B

    rsub-int/lit8 v3, v1, 0x8

    invoke-virtual {p1, v2, v1, v3}, Ljava/io/InputStream;->read([BII)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 57
    .local v0, "n":I
    if-gtz v0, :cond_1

    .line 65
    .end local v0    # "n":I
    :cond_0
    :goto_1
    return v1

    .line 60
    .restart local v0    # "n":I
    :cond_1
    add-int/2addr v1, v0

    goto :goto_0

    .line 62
    .end local v0    # "n":I
    :catch_0
    move-exception v2

    goto :goto_1
.end method

.method public write(Ljava/io/OutputStream;)Z
    .locals 5
    .param p1, "f"    # Ljava/io/OutputStream;

    .prologue
    const/4 v1, 0x0

    .line 26
    :try_start_0
    iget-object v2, p0, Lcom/samsung/musicplus/mediainfo/crypto/Block;->data:[B

    const/4 v3, 0x0

    const/16 v4, 0x8

    invoke-virtual {p1, v2, v3, v4}, Ljava/io/OutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 32
    const/4 v1, 0x1

    :goto_0
    return v1

    .line 27
    :catch_0
    move-exception v0

    .line 28
    .local v0, "e":Ljava/io/IOException;
    sget-object v2, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string v3, "Failed to write output"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public write(Ljava/io/OutputStream;I)Z
    .locals 4
    .param p1, "f"    # Ljava/io/OutputStream;
    .param p2, "n"    # I

    .prologue
    const/4 v1, 0x0

    .line 38
    :try_start_0
    iget-object v2, p0, Lcom/samsung/musicplus/mediainfo/crypto/Block;->data:[B

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3, p2}, Ljava/io/OutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 44
    const/4 v1, 0x1

    :goto_0
    return v1

    .line 39
    :catch_0
    move-exception v0

    .line 40
    .local v0, "e":Ljava/io/IOException;
    sget-object v2, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string v3, "Failed to write output"

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public xor(Lcom/samsung/musicplus/mediainfo/crypto/Block;)V
    .locals 4
    .param p1, "src"    # Lcom/samsung/musicplus/mediainfo/crypto/Block;

    .prologue
    .line 77
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v1, 0x8

    if-ge v0, v1, :cond_0

    .line 78
    iget-object v1, p0, Lcom/samsung/musicplus/mediainfo/crypto/Block;->data:[B

    aget-byte v2, v1, v0

    iget-object v3, p1, Lcom/samsung/musicplus/mediainfo/crypto/Block;->data:[B

    aget-byte v3, v3, v0

    xor-int/2addr v2, v3

    int-to-byte v2, v2

    aput-byte v2, v1, v0

    .line 77
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 80
    :cond_0
    return-void
.end method
