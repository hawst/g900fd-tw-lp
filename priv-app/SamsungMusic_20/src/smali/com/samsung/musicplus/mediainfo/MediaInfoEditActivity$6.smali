.class Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity$6;
.super Ljava/lang/Object;
.source "MediaInfoEditActivity.java"

# interfaces
.implements Landroid/text/InputFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;)V
    .locals 0

    .prologue
    .line 432
    iput-object p1, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity$6;->this$0:Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;
    .locals 5
    .param p1, "source"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "end"    # I
    .param p4, "dest"    # Landroid/text/Spanned;
    .param p5, "dstart"    # I
    .param p6, "dend"    # I

    .prologue
    const v4, 0x7f100097

    .line 436
    move-object v2, p1

    .line 437
    .local v2, "result":Ljava/lang/CharSequence;
    const-string v3, "[\\*/\\\\\\?:<>\\|\"]+"

    invoke-static {v3}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    .line 438
    .local v1, "ps":Ljava/util/regex/Pattern;
    invoke-virtual {v1, v2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 439
    .local v0, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 440
    iget-object v3, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity$6;->this$0:Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;

    # getter for: Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mToast:Landroid/widget/Toast;
    invoke-static {v3}, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->access$700(Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;)Landroid/widget/Toast;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 441
    iget-object v3, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity$6;->this$0:Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;

    # getter for: Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mToast:Landroid/widget/Toast;
    invoke-static {v3}, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->access$700(Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/widget/Toast;->setText(I)V

    .line 442
    iget-object v3, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity$6;->this$0:Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;

    # getter for: Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mToast:Landroid/widget/Toast;
    invoke-static {v3}, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->access$700(Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 444
    :cond_0
    const-string v3, ""

    invoke-virtual {v0, v3}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 447
    :cond_1
    invoke-static {v2}, Lcom/samsung/musicplus/util/EmojiList;->hasEmojiString(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 448
    iget-object v3, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity$6;->this$0:Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;

    # getter for: Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mToast:Landroid/widget/Toast;
    invoke-static {v3}, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->access$700(Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;)Landroid/widget/Toast;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 449
    iget-object v3, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity$6;->this$0:Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;

    # getter for: Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mToast:Landroid/widget/Toast;
    invoke-static {v3}, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->access$700(Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/widget/Toast;->setText(I)V

    .line 450
    iget-object v3, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity$6;->this$0:Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;

    # getter for: Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mToast:Landroid/widget/Toast;
    invoke-static {v3}, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->access$700(Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 452
    :cond_2
    const/4 v1, 0x0

    .line 453
    invoke-static {}, Lcom/samsung/musicplus/util/EmojiList;->getPattern()Ljava/util/regex/Pattern;

    move-result-object v1

    .line 454
    const/4 v0, 0x0

    .line 455
    invoke-virtual {v1, v2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 456
    const-string v3, ""

    invoke-virtual {v0, v3}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 458
    :cond_3
    return-object v2
.end method
