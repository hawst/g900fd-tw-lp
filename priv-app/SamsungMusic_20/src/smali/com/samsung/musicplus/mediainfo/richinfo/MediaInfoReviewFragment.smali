.class public Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoReviewFragment;
.super Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoBaseFragment;
.source "MediaInfoReviewFragment.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoBaseFragment;-><init>()V

    return-void
.end method

.method private setContents()V
    .locals 4

    .prologue
    .line 26
    invoke-virtual {p0}, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoReviewFragment;->getView()Landroid/view/View;

    move-result-object v2

    .line 27
    .local v2, "root":Landroid/view/View;
    const v3, 0x7f0d00f1

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 28
    .local v0, "contentView":Landroid/widget/TextView;
    const/4 v1, 0x0

    .line 29
    .local v1, "review":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoReviewFragment;->mParsedData:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoReviewFragment;->mParsedData:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;

    iget-object v3, v3, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;->albumDetailInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;

    if-eqz v3, :cond_0

    .line 30
    iget-object v3, p0, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoReviewFragment;->mParsedData:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;

    iget-object v3, v3, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;->albumDetailInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;

    iget-object v1, v3, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;->review:Ljava/lang/String;

    .line 32
    :cond_0
    if-eqz v1, :cond_1

    const-string v3, ""

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 33
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 35
    :cond_1
    return-void
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 21
    invoke-super {p0, p1}, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoBaseFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 22
    invoke-direct {p0}, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoReviewFragment;->setContents()V

    .line 23
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 16
    const v0, 0x7f04004f

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
