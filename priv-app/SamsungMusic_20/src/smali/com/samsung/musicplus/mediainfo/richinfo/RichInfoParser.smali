.class public Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;
.super Ljava/lang/Object;
.source "RichInfoParser.java"


# static fields
.field static final ALBUM:Ljava/lang/String; = "/Album"

.field static final ALBUMID:Ljava/lang/String; = "/AlbumID"

.field static final ALBUMS:Ljava/lang/String; = "/Albums"

.field static final ALBUM_ARTIST:Ljava/lang/String; = "/Album/AlbumSimpleInfo/Artist"

.field static final ALBUM_ARTIST_ID:Ljava/lang/String; = "/Album/AlbumSimpleInfo/Artist/ID"

.field static final ALBUM_ARTIST_NAME:Ljava/lang/String; = "/Album/AlbumSimpleInfo/Artist/Name"

.field static final ALBUM_ARTWORK:Ljava/lang/String; = "/Album/AlbumSimpleInfo/Artwork"

.field static final ALBUM_COPYRIGHT:Ljava/lang/String; = "/Album/Copyright"

.field static final ALBUM_CREDIT:Ljava/lang/String; = "/Album/Credits/Credit"

.field static final ALBUM_CREDITS:Ljava/lang/String; = "/Album/Credits"

.field static final ALBUM_CREDIT_ID:Ljava/lang/String; = "/Album/Credits/Credit/CreditID"

.field static final ALBUM_CREDIT_PERFORMER:Ljava/lang/String; = "/Album/Credits/Credit/PerformerSimpleInfo"

.field static final ALBUM_CREDIT_PERFORMER_ID:Ljava/lang/String; = "/Album/Credits/Credit/PerformerSimpleInfo/ID"

.field static final ALBUM_CREDIT_PERFORMER_NAME:Ljava/lang/String; = "/Album/Credits/Credit/PerformerSimpleInfo/Name"

.field static final ALBUM_CREDIT_ROLE:Ljava/lang/String; = "/Album/Credits/Credit/Role"

.field static final ALBUM_DATE:Ljava/lang/String; = "/Album/Date"

.field static final ALBUM_DETAIL_INFO:Ljava/lang/String; = "/AlbumDetailInfo"

.field static final ALBUM_DISC:Ljava/lang/String; = "/Album/Discs/Disc"

.field static final ALBUM_DISCS:Ljava/lang/String; = "/Album/Discs"

.field static final ALBUM_DISC_MEDIA_NUMBER:Ljava/lang/String; = "/Album/Discs/Disc/CurrentMediaNumber"

.field static final ALBUM_DISC_TOTAL_TIME:Ljava/lang/String; = "/Album/Discs/Disc/TotalRunningTime"

.field static final ALBUM_DISC_TRACKS:Ljava/lang/String; = "/Album/Discs/Disc/Tracks"

.field static final ALBUM_GENRE_DESC:Ljava/lang/String; = "/Album/GenreDesc"

.field static final ALBUM_ID:Ljava/lang/String; = "/Album/AlbumSimpleInfo/AlbumID"

.field static final ALBUM_INFO:Ljava/lang/String; = "/AlbumInfo"

.field static final ALBUM_IS_PICK:Ljava/lang/String; = "/Album/AlbumSimpleInfo/IsPick"

.field static final ALBUM_RATING:Ljava/lang/String; = "/Album/AlbumSimpleInfo/Rating"

.field static final ALBUM_RELEASE:Ljava/lang/String; = "/Album/Releases/Release"

.field static final ALBUM_RELEASES:Ljava/lang/String; = "/Album/Releases"

.field static final ALBUM_RELEASE_LABEL:Ljava/lang/String; = "/Album/Releases/Release/Label"

.field static final ALBUM_RELEASE_MEDIA:Ljava/lang/String; = "/Album/Releases/Release/Media"

.field static final ALBUM_REVIEW:Ljava/lang/String; = "/Album/Review"

.field static final ALBUM_SIMILAR_ALBUM:Ljava/lang/String; = "/Album/SimilarAlbums/SimilarAlbum"

.field static final ALBUM_SIMILAR_ALBUMS:Ljava/lang/String; = "/Album/SimilarAlbums"

.field static final ALBUM_SIMPLE_INFO:Ljava/lang/String; = "/AlbumSimpleInfo"

.field static final ALBUM_STYLE:Ljava/lang/String; = "/Album/Styles/Style"

.field static final ALBUM_STYLES:Ljava/lang/String; = "/Album/Styles"

.field static final ALBUM_STYLE_ID:Ljava/lang/String; = "/Album/Styles/Style/ID"

.field static final ALBUM_STYLE_NAME:Ljava/lang/String; = "/Album/Styles/Style/Name"

.field static final ALBUM_THEME:Ljava/lang/String; = "/Album/Themes/Theme"

.field static final ALBUM_TITLE:Ljava/lang/String; = "/Album/AlbumSimpleInfo/Title"

.field static final ALBUM_TONE:Ljava/lang/String; = "/Album/Tones/Tone"

.field static final ALBUM_TONES:Ljava/lang/String; = "/Album/Tones"

.field static final ALBUM_TONE_ID:Ljava/lang/String; = "/Album/Tones/Tone/ID"

.field static final ALBUM_TONE_NAME:Ljava/lang/String; = "/Album/Tones/Tone/Name"

.field static final ARTIST:Ljava/lang/String; = "/Artist"

.field static final ARTIST_INFO:Ljava/lang/String; = "/ArtistInfo"

.field static final ARTWORK:Ljava/lang/String; = "/Artwork"

.field static final ARTWORK_URI:Ljava/lang/String; = "uri"

.field static final BIOGRAPHY:Ljava/lang/String; = "/Biography"

.field private static final CLASSTAG:Ljava/lang/String;

.field static final COMPOSER:Ljava/lang/String; = "/Composer"

.field static final COMPOSERS:Ljava/lang/String; = "/Composers"

.field static final COMPOSER_INFO:Ljava/lang/String; = "/ComposerInfo"

.field static final COPYRIGHT:Ljava/lang/String; = "/Copyright"

.field static final CREDIT:Ljava/lang/String; = "/Credit"

.field static final CREDITS:Ljava/lang/String; = "/Credits"

.field static final CREDIT_ID:Ljava/lang/String; = "/CreditID"

.field static final CREDIT_INFO:Ljava/lang/String; = "/CreditInfo"

.field static final CURRENT_MEDIA_NUMBER:Ljava/lang/String; = "/CurrentMediaNumber"

.field static final DATE:Ljava/lang/String; = "/Date"

.field static final DISC:Ljava/lang/String; = "/Disc"

.field static final DISCS:Ljava/lang/String; = "/Discs"

.field static final DISC_INFO:Ljava/lang/String; = "/DiscInfo"

.field static final FOLLOWER:Ljava/lang/String; = "/Follower"

.field static final FOLLOWERS:Ljava/lang/String; = "/Followers"

.field static final GENRE_DESC:Ljava/lang/String; = "/GenreDesc"

.field static final GENRE_INFO:Ljava/lang/String; = "/GenreInfo"

.field static final ID:Ljava/lang/String; = "/ID"

.field static final IMAGE:Ljava/lang/String; = "/Image"

.field static final INFLUENCE_ARTIST:Ljava/lang/String; = "/InfluenceArtist"

.field static final INFLUENCE_ARTISTS:Ljava/lang/String; = "/InfluenceArtists"

.field static final ISRC:Ljava/lang/String; = "/ISRC"

.field static final IS_PICK:Ljava/lang/String; = "/IsPick"

.field static final LABEL:Ljava/lang/String; = "/Label"

.field static final MEDIA:Ljava/lang/String; = "/Media"

.field static final NAME:Ljava/lang/String; = "/Name"

.field static final PERFORMER:Ljava/lang/String; = "/Performer"

.field static final PERFORMERS:Ljava/lang/String; = "/Performers"

.field static final PERFORMER_ALBUM:Ljava/lang/String; = "/Performer/Albums/AlbumSimpleInfo"

.field static final PERFORMER_ALBUMS:Ljava/lang/String; = "/Performer/Albums"

.field static final PERFORMER_BIOGRAPHY:Ljava/lang/String; = "/Performer/Biography"

.field static final PERFORMER_DETAIL_INFO:Ljava/lang/String; = "/PerformerDetailInfo"

.field static final PERFORMER_FOLLOWER:Ljava/lang/String; = "/Performer/Followers/Follower"

.field static final PERFORMER_FOLLOWERS:Ljava/lang/String; = "/Performer/Followers"

.field static final PERFORMER_INFLUENCE_ARTIST:Ljava/lang/String; = "/Performer/InfluenceArtists/InfluenceArtist"

.field static final PERFORMER_INFLUENCE_ARTISTS:Ljava/lang/String; = "/Performer/InfluenceArtists"

.field static final PERFORMER_INFO:Ljava/lang/String; = "/PerformerInfo"

.field static final PERFORMER_SIMILAR_ARTIST:Ljava/lang/String; = "/Performer/SimilarArtists/SimilarArtist"

.field static final PERFORMER_SIMILAR_ARTISTS:Ljava/lang/String; = "/Performer/SimilarArtists"

.field static final PERFORMER_SIMPLEINFO:Ljava/lang/String; = "/Performer/PerformerSimpleInfo"

.field static final PERFORMER_SIMPLEINFO_ID:Ljava/lang/String; = "/Performer/PerformerSimpleInfo/ID"

.field static final PERFORMER_SIMPLEINFO_NAME:Ljava/lang/String; = "/Performer/PerformerSimpleInfo/Name"

.field static final PERFORMER_SIMPLE_INFO:Ljava/lang/String; = "/PerformerSimpleInfo"

.field static final PLAY_LENGTH:Ljava/lang/String; = "/PlayLength"

.field static final RATING:Ljava/lang/String; = "/Rating"

.field static final RELEASE:Ljava/lang/String; = "/Release"

.field static final RELEASES:Ljava/lang/String; = "/Releases"

.field static final RELEASE_INFO:Ljava/lang/String; = "/ReleaseInfo"

.field static final REVIEW:Ljava/lang/String; = "/Review"

.field static final ROLE:Ljava/lang/String; = "/Role"

.field static final ROOT:Ljava/lang/String; = "/SM_RichInfo"

.field static final SIMILAR_ALBUM:Ljava/lang/String; = "/SimilarAlbum"

.field static final SIMILAR_ALBUMS:Ljava/lang/String; = "/SimilarAlbums"

.field static final SIMILAR_ALBUM_INFO:Ljava/lang/String; = "/SimilarAlbumInfo"

.field static final SIMILAR_ARTIST:Ljava/lang/String; = "/SimilarArtist"

.field static final SIMILAR_ARTISTS:Ljava/lang/String; = "/SimilarArtists"

.field static final STYLE:Ljava/lang/String; = "/Style"

.field static final STYLES:Ljava/lang/String; = "/Styles"

.field static final STYLE_ID:Ljava/lang/String; = "/StyleID"

.field static final STYLE_INFO:Ljava/lang/String; = "/StyleInfo"

.field static final THEME:Ljava/lang/String; = "/Theme"

.field static final THEMES:Ljava/lang/String; = "/Themes"

.field static final THEME_INFO:Ljava/lang/String; = "/ThemeInfo"

.field static final TITLE:Ljava/lang/String; = "/Title"

.field static final TONE:Ljava/lang/String; = "/Tone"

.field static final TONES:Ljava/lang/String; = "/Tones"

.field static final TONE_ID:Ljava/lang/String; = "/ToneID"

.field static final TONE_INFO:Ljava/lang/String; = "/ToneInfo"

.field static final TOTAL_RUNNING_TIME:Ljava/lang/String; = "/TotalRunningTime"

.field static final TRACK:Ljava/lang/String; = "/Track"

.field static final TRACKID:Ljava/lang/String; = "/TrackID"

.field static final TRACKS:Ljava/lang/String; = "/Tracks"

.field static final TRACK_COMPOSER:Ljava/lang/String; = "/Track/Composers/Composer"

.field static final TRACK_COMPOSERS:Ljava/lang/String; = "/Track/Composers"

.field static final TRACK_COMPOSER_ID:Ljava/lang/String; = "/Track/Composers/Composer/ID"

.field static final TRACK_COMPOSER_NAME:Ljava/lang/String; = "/Track/Composers/Composer/Name"

.field static final TRACK_GENRE_DESC:Ljava/lang/String; = "/Track/GenreDesc"

.field static final TRACK_ID:Ljava/lang/String; = "/Track/TrackID"

.field static final TRACK_ISRC:Ljava/lang/String; = "/Track/ISRC"

.field static final TRACK_IS_PICK:Ljava/lang/String; = "/Track/IsPick"

.field static final TRACK_PERFORMERS:Ljava/lang/String; = "/Track/Performers"

.field static final TRACK_PERFORMER_SIMPLE_INFO:Ljava/lang/String; = "/Track/Performers/PerformerSimpleInfo"

.field static final TRACK_PERFORMER_SIMPLE_INFO_ID:Ljava/lang/String; = "/Track/Performers/PerformerSimpleInfo/ID"

.field static final TRACK_PERFORMER_SIMPLE_INFO_NAME:Ljava/lang/String; = "/Track/Performers/PerformerSimpleInfo/NAME"

.field static final TRACK_PLAY_LENGTH:Ljava/lang/String; = "/Track/PlayLength"

.field static final TRACK_TITLE:Ljava/lang/String; = "/Track/Title"

.field private static mAlbum:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;

.field private static mAlbumInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;

.field private static mArtistInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

.field private static mArtworkInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtworkInfo;

.field private static mCredit:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$Credit;

.field private static mDisc:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$Disc;

.field private static mPerformerInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$PerformerDetailInfo;

.field private static mReleaseInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ReleaseInfo;

.field private static mRichInfoData:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;

.field private static mStyleInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$StyleInfo;

.field private static mToneInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ToneInfo;

.field private static mTrack:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->CLASSTAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static parseEndTag(Ljava/lang/String;)V
    .locals 3
    .param p0, "current"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 539
    const-string v0, "/SM_RichInfo/Track"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 540
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mTrack:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;

    if-eqz v0, :cond_0

    .line 541
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mRichInfoData:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;

    sget-object v1, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mTrack:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;

    iput-object v1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;->trackInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;

    .line 542
    sput-object v2, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mTrack:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;

    .line 645
    :cond_0
    :goto_0
    return-void

    .line 544
    :cond_1
    const-string v0, "/SM_RichInfo/Track/Composers/Composer"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 545
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mArtistInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mTrack:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mTrack:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;

    iget-object v0, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;->composers:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 546
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mTrack:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;

    iget-object v0, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;->composers:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mArtistInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 547
    sput-object v2, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mArtistInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    goto :goto_0

    .line 549
    :cond_2
    const-string v0, "/SM_RichInfo/Track/Performers/PerformerSimpleInfo"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 550
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mTrack:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mArtistInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mTrack:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;

    iget-object v0, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;->performers:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 551
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mTrack:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;

    iget-object v0, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;->performers:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mArtistInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 552
    sput-object v2, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mArtistInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    goto :goto_0

    .line 554
    :cond_3
    const-string v0, "/SM_RichInfo/Album"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 555
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbum:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;

    if-eqz v0, :cond_0

    .line 556
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mRichInfoData:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;

    sget-object v1, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbum:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;

    iput-object v1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;->albumDetailInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;

    .line 557
    sput-object v2, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbum:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;

    goto :goto_0

    .line 559
    :cond_4
    const-string v0, "/SM_RichInfo/Album/AlbumSimpleInfo"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 560
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbumInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbum:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;

    if-eqz v0, :cond_0

    .line 561
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbum:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;

    sget-object v1, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbumInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;

    iput-object v1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;->albumSimpleInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;

    .line 562
    sput-object v2, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbumInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;

    goto :goto_0

    .line 564
    :cond_5
    const-string v0, "/SM_RichInfo/Album/AlbumSimpleInfo/Artwork"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 565
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mArtworkInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtworkInfo;

    if-eqz v0, :cond_0

    .line 566
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbumInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;

    sget-object v1, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mArtworkInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtworkInfo;

    iput-object v1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;->artwork:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtworkInfo;

    .line 567
    sput-object v2, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mArtworkInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtworkInfo;

    goto/16 :goto_0

    .line 569
    :cond_6
    const-string v0, "/SM_RichInfo/Album/AlbumSimpleInfo/Artist"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 570
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mArtistInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    if-eqz v0, :cond_0

    .line 571
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbumInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;

    sget-object v1, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mArtistInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    iput-object v1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;->artist:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    .line 572
    sput-object v2, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mArtistInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    goto/16 :goto_0

    .line 574
    :cond_7
    const-string v0, "/SM_RichInfo/Album/Releases/Release"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 575
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mReleaseInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ReleaseInfo;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbum:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbum:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;

    iget-object v0, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;->releases:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 576
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbum:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;

    iget-object v0, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;->releases:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mReleaseInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ReleaseInfo;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 577
    sput-object v2, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mReleaseInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ReleaseInfo;

    goto/16 :goto_0

    .line 579
    :cond_8
    const-string v0, "/SM_RichInfo/Album/Credits/Credit"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 580
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mCredit:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$Credit;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbum:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbum:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;

    iget-object v0, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;->credits:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 581
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbum:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;

    iget-object v0, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;->credits:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mCredit:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$Credit;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 582
    sput-object v2, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mCredit:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$Credit;

    goto/16 :goto_0

    .line 588
    :cond_9
    const-string v0, "/SM_RichInfo/Album/Discs/Disc"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 589
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mDisc:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$Disc;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbum:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbum:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;

    iget-object v0, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;->discs:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 590
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbum:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;

    iget-object v0, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;->discs:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mDisc:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$Disc;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 591
    sput-object v2, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mDisc:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$Disc;

    goto/16 :goto_0

    .line 593
    :cond_a
    const-string v0, "/SM_RichInfo/Album/Discs/Disc/Tracks/Track"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 594
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mDisc:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$Disc;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mTrack:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;

    if-eqz v0, :cond_0

    .line 595
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mDisc:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$Disc;

    iget-object v0, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$Disc;->tracks:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mTrack:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 596
    sput-object v2, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mTrack:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;

    goto/16 :goto_0

    .line 598
    :cond_b
    const-string v0, "/SM_RichInfo/Album/Discs/Disc/Tracks/Track/Composers/Composer"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 599
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mTrack:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mArtistInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    if-eqz v0, :cond_0

    .line 600
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mTrack:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;

    iget-object v0, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;->composers:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mArtistInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 601
    sput-object v2, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mArtistInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    goto/16 :goto_0

    .line 603
    :cond_c
    const-string v0, "/SM_RichInfo/Album/Styles/Style"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 604
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mStyleInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$StyleInfo;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbum:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbum:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;

    iget-object v0, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;->styles:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 605
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbum:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;

    iget-object v0, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;->styles:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mStyleInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$StyleInfo;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 606
    sput-object v2, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mStyleInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$StyleInfo;

    goto/16 :goto_0

    .line 608
    :cond_d
    const-string v0, "/SM_RichInfo/Album/Tones/Tone"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 609
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mToneInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ToneInfo;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbum:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbum:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;

    iget-object v0, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;->tones:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 610
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbum:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;

    iget-object v0, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;->tones:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mToneInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ToneInfo;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 611
    sput-object v2, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mToneInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ToneInfo;

    goto/16 :goto_0

    .line 613
    :cond_e
    const-string v0, "/SM_RichInfo/Album/SimilarAlbums/SimilarAlbum"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 614
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbumInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbum:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbum:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;

    iget-object v0, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;->similarAlbums:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 615
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbum:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;

    iget-object v0, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;->similarAlbums:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbumInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 616
    sput-object v2, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbumInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;

    goto/16 :goto_0

    .line 618
    :cond_f
    const-string v0, "/SM_RichInfo/Performer"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 619
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mPerformerInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$PerformerDetailInfo;

    if-eqz v0, :cond_0

    .line 620
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mRichInfoData:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;

    sget-object v1, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mPerformerInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$PerformerDetailInfo;

    iput-object v1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;->performerDetailInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$PerformerDetailInfo;

    .line 621
    sput-object v2, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mPerformerInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$PerformerDetailInfo;

    goto/16 :goto_0

    .line 623
    :cond_10
    const-string v0, "/SM_RichInfo/Performer/Albums/AlbumSimpleInfo"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 624
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbumInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;

    if-eqz v0, :cond_0

    .line 625
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mPerformerInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$PerformerDetailInfo;

    iget-object v0, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$PerformerDetailInfo;->albums:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbumInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 626
    sput-object v2, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbumInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;

    goto/16 :goto_0

    .line 628
    :cond_11
    const-string v0, "/SM_RichInfo/Performer/SimilarArtists/SimilarArtist"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 629
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mArtistInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    if-eqz v0, :cond_0

    .line 630
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mPerformerInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$PerformerDetailInfo;

    iget-object v0, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$PerformerDetailInfo;->similarArtists:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mArtistInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 631
    sput-object v2, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mArtistInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    goto/16 :goto_0

    .line 633
    :cond_12
    const-string v0, "/SM_RichInfo/Performer/InfluenceArtists/InfluenceArtist"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 634
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mArtistInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    if-eqz v0, :cond_0

    .line 635
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mPerformerInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$PerformerDetailInfo;

    iget-object v0, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$PerformerDetailInfo;->influenceArtists:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mArtistInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 636
    sput-object v2, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mArtistInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    goto/16 :goto_0

    .line 638
    :cond_13
    const-string v0, "/SM_RichInfo/Performer/Followers/Follower"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 639
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mArtistInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    if-eqz v0, :cond_0

    .line 640
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mPerformerInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$PerformerDetailInfo;

    iget-object v0, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$PerformerDetailInfo;->followers:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mArtistInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 641
    sput-object v2, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mArtistInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    goto/16 :goto_0
.end method

.method private static parseStartTag(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p0, "current"    # Ljava/lang/String;
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 426
    const-string v0, "/SM_RichInfo/Track"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 427
    new-instance v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;

    invoke-direct {v0}, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;-><init>()V

    sput-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mTrack:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;

    .line 531
    :cond_0
    :goto_0
    return-void

    .line 428
    :cond_1
    const-string v0, "/SM_RichInfo/Track/Composers"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 429
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mTrack:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;

    if-eqz v0, :cond_0

    .line 430
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mTrack:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;->composers:Ljava/util/ArrayList;

    goto :goto_0

    .line 432
    :cond_2
    const-string v0, "/SM_RichInfo/Track/Composers/Composer"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 433
    new-instance v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    invoke-direct {v0}, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;-><init>()V

    sput-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mArtistInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    goto :goto_0

    .line 434
    :cond_3
    const-string v0, "/SM_RichInfo/Track/Performers"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 435
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mTrack:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;

    if-eqz v0, :cond_0

    .line 436
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mTrack:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;->performers:Ljava/util/ArrayList;

    goto :goto_0

    .line 438
    :cond_4
    const-string v0, "/SM_RichInfo/Track/Performers/PerformerSimpleInfo"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 439
    new-instance v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    invoke-direct {v0}, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;-><init>()V

    sput-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mArtistInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    goto :goto_0

    .line 440
    :cond_5
    const-string v0, "/SM_RichInfo/Album"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 441
    new-instance v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;

    invoke-direct {v0}, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;-><init>()V

    sput-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbum:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;

    goto :goto_0

    .line 442
    :cond_6
    const-string v0, "/SM_RichInfo/Album/AlbumSimpleInfo"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 443
    new-instance v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;

    invoke-direct {v0}, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;-><init>()V

    sput-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbumInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;

    goto :goto_0

    .line 444
    :cond_7
    const-string v0, "/SM_RichInfo/Album/AlbumSimpleInfo/Artwork@uri"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 445
    new-instance v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtworkInfo;

    invoke-direct {v0}, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtworkInfo;-><init>()V

    sput-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mArtworkInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtworkInfo;

    .line 446
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mArtworkInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtworkInfo;

    iput-object p1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtworkInfo;->uri:Ljava/lang/String;

    goto :goto_0

    .line 447
    :cond_8
    const-string v0, "/SM_RichInfo/Album/AlbumSimpleInfo/Artist"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 448
    new-instance v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    invoke-direct {v0}, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;-><init>()V

    sput-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mArtistInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    goto/16 :goto_0

    .line 449
    :cond_9
    const-string v0, "/SM_RichInfo/Album/Releases"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 450
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbum:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;

    if-eqz v0, :cond_0

    .line 451
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbum:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;->releases:Ljava/util/ArrayList;

    goto/16 :goto_0

    .line 453
    :cond_a
    const-string v0, "/SM_RichInfo/Album/Releases/Release"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 454
    new-instance v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ReleaseInfo;

    invoke-direct {v0}, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ReleaseInfo;-><init>()V

    sput-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mReleaseInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ReleaseInfo;

    goto/16 :goto_0

    .line 455
    :cond_b
    const-string v0, "/SM_RichInfo/Album/Credits"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 456
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbum:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;

    if-eqz v0, :cond_0

    .line 457
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbum:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;->credits:Ljava/util/ArrayList;

    goto/16 :goto_0

    .line 459
    :cond_c
    const-string v0, "/SM_RichInfo/Album/Credits/Credit"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 460
    new-instance v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$Credit;

    invoke-direct {v0}, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$Credit;-><init>()V

    sput-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mCredit:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$Credit;

    goto/16 :goto_0

    .line 461
    :cond_d
    const-string v0, "/SM_RichInfo/Album/Credits/Credit/PerformerSimpleInfo"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 462
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mCredit:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$Credit;

    new-instance v1, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    invoke-direct {v1}, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;-><init>()V

    iput-object v1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$Credit;->PerformerSimpleInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    goto/16 :goto_0

    .line 463
    :cond_e
    const-string v0, "/SM_RichInfo/Album/Discs"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 464
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbum:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;

    if-eqz v0, :cond_0

    .line 465
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbum:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;->discs:Ljava/util/ArrayList;

    goto/16 :goto_0

    .line 467
    :cond_f
    const-string v0, "/SM_RichInfo/Album/Discs/Disc"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 468
    new-instance v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$Disc;

    invoke-direct {v0}, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$Disc;-><init>()V

    sput-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mDisc:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$Disc;

    goto/16 :goto_0

    .line 469
    :cond_10
    const-string v0, "/SM_RichInfo/Album/Discs/Disc/Tracks"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 470
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mDisc:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$Disc;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$Disc;->tracks:Ljava/util/ArrayList;

    goto/16 :goto_0

    .line 471
    :cond_11
    const-string v0, "/SM_RichInfo/Album/Discs/Disc/Tracks/Track"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 472
    new-instance v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;

    invoke-direct {v0}, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;-><init>()V

    sput-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mTrack:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;

    goto/16 :goto_0

    .line 473
    :cond_12
    const-string v0, "/SM_RichInfo/Album/Discs/Disc/Tracks/Track/Composers"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 474
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mTrack:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;

    if-eqz v0, :cond_0

    .line 475
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mTrack:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;->composers:Ljava/util/ArrayList;

    goto/16 :goto_0

    .line 477
    :cond_13
    const-string v0, "/SM_RichInfo/Album/Discs/Disc/Tracks/Track/Composers/Composer"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 478
    new-instance v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    invoke-direct {v0}, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;-><init>()V

    sput-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mArtistInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    goto/16 :goto_0

    .line 479
    :cond_14
    const-string v0, "/SM_RichInfo/Album/Styles"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 480
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbum:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;

    if-eqz v0, :cond_0

    .line 481
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbum:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;->styles:Ljava/util/ArrayList;

    goto/16 :goto_0

    .line 483
    :cond_15
    const-string v0, "/SM_RichInfo/Album/Styles/Style"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 484
    new-instance v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$StyleInfo;

    invoke-direct {v0}, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$StyleInfo;-><init>()V

    sput-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mStyleInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$StyleInfo;

    goto/16 :goto_0

    .line 485
    :cond_16
    const-string v0, "/SM_RichInfo/Album/Tones"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_17

    .line 486
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbum:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;

    if-eqz v0, :cond_0

    .line 487
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbum:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;->tones:Ljava/util/ArrayList;

    goto/16 :goto_0

    .line 489
    :cond_17
    const-string v0, "/SM_RichInfo/Album/Tones/Tone"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_18

    .line 490
    new-instance v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ToneInfo;

    invoke-direct {v0}, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ToneInfo;-><init>()V

    sput-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mToneInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ToneInfo;

    goto/16 :goto_0

    .line 491
    :cond_18
    const-string v0, "/SM_RichInfo/Album/SimilarAlbums"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_19

    .line 492
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbum:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;

    if-eqz v0, :cond_0

    .line 493
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbum:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;->similarAlbums:Ljava/util/ArrayList;

    goto/16 :goto_0

    .line 495
    :cond_19
    const-string v0, "/SM_RichInfo/Album/SimilarAlbums/SimilarAlbum"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1a

    .line 496
    new-instance v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;

    invoke-direct {v0}, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;-><init>()V

    sput-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbumInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;

    goto/16 :goto_0

    .line 497
    :cond_1a
    const-string v0, "/SM_RichInfo/Album/SimilarAlbums/SimilarAlbum/Artist"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1b

    .line 498
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbumInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;

    new-instance v1, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    invoke-direct {v1}, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;-><init>()V

    iput-object v1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;->artist:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    goto/16 :goto_0

    .line 502
    :cond_1b
    const-string v0, "/SM_RichInfo/Album/SimilarAlbums/SimilarAlbum/Artwork@uri"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 503
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbumInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;

    new-instance v1, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtworkInfo;

    invoke-direct {v1}, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtworkInfo;-><init>()V

    iput-object v1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;->artwork:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtworkInfo;

    .line 504
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbumInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;

    iget-object v0, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;->artwork:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtworkInfo;

    iput-object p1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtworkInfo;->uri:Ljava/lang/String;

    goto/16 :goto_0

    .line 505
    :cond_1c
    const-string v0, "/SM_RichInfo/Performer"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1d

    .line 506
    new-instance v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$PerformerDetailInfo;

    invoke-direct {v0}, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$PerformerDetailInfo;-><init>()V

    sput-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mPerformerInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$PerformerDetailInfo;

    goto/16 :goto_0

    .line 507
    :cond_1d
    const-string v0, "/SM_RichInfo/Performer/PerformerSimpleInfo"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1e

    .line 508
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mPerformerInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$PerformerDetailInfo;

    new-instance v1, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    invoke-direct {v1}, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;-><init>()V

    iput-object v1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$PerformerDetailInfo;->performerSimpleInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    goto/16 :goto_0

    .line 509
    :cond_1e
    const-string v0, "/SM_RichInfo/Performer/Albums"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 510
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mPerformerInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$PerformerDetailInfo;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$PerformerDetailInfo;->albums:Ljava/util/ArrayList;

    goto/16 :goto_0

    .line 511
    :cond_1f
    const-string v0, "/SM_RichInfo/Performer/Albums/AlbumSimpleInfo"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_20

    .line 512
    new-instance v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;

    invoke-direct {v0}, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;-><init>()V

    sput-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbumInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;

    goto/16 :goto_0

    .line 513
    :cond_20
    const-string v0, "/SM_RichInfo/Performer/Albums/AlbumSimpleInfo/Artist"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_21

    .line 514
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbumInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;

    new-instance v1, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    invoke-direct {v1}, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;-><init>()V

    iput-object v1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;->artist:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    goto/16 :goto_0

    .line 515
    :cond_21
    const-string v0, "/SM_RichInfo/Performer/Albums/AlbumSimpleInfo/Artwork@uri"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_22

    .line 516
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbumInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;

    new-instance v1, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtworkInfo;

    invoke-direct {v1}, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtworkInfo;-><init>()V

    iput-object v1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;->artwork:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtworkInfo;

    .line 517
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbumInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;

    iget-object v0, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;->artwork:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtworkInfo;

    iput-object p1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtworkInfo;->uri:Ljava/lang/String;

    goto/16 :goto_0

    .line 518
    :cond_22
    const-string v0, "/SM_RichInfo/Performer/SimilarArtists"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_23

    .line 519
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mPerformerInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$PerformerDetailInfo;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$PerformerDetailInfo;->similarArtists:Ljava/util/ArrayList;

    goto/16 :goto_0

    .line 520
    :cond_23
    const-string v0, "/SM_RichInfo/Performer/SimilarArtists/SimilarArtist"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_24

    .line 521
    new-instance v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    invoke-direct {v0}, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;-><init>()V

    sput-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mArtistInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    goto/16 :goto_0

    .line 522
    :cond_24
    const-string v0, "/SM_RichInfo/Performer/InfluenceArtists"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_25

    .line 523
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mPerformerInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$PerformerDetailInfo;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$PerformerDetailInfo;->influenceArtists:Ljava/util/ArrayList;

    goto/16 :goto_0

    .line 524
    :cond_25
    const-string v0, "/SM_RichInfo/Performer/InfluenceArtists/InfluenceArtist"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_26

    .line 525
    new-instance v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    invoke-direct {v0}, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;-><init>()V

    sput-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mArtistInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    goto/16 :goto_0

    .line 526
    :cond_26
    const-string v0, "/SM_RichInfo/Performer/Followers"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_27

    .line 527
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mPerformerInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$PerformerDetailInfo;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$PerformerDetailInfo;->followers:Ljava/util/ArrayList;

    goto/16 :goto_0

    .line 528
    :cond_27
    const-string v0, "/SM_RichInfo/Performer/Followers/Follower"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 529
    new-instance v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    invoke-direct {v0}, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;-><init>()V

    sput-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mArtistInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    goto/16 :goto_0
.end method

.method private static parseTag(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p0, "current"    # Ljava/lang/String;
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 655
    const-string v2, "/SM_RichInfo/Track/TrackID"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 656
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mTrack:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;

    if-eqz v0, :cond_0

    .line 657
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mTrack:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;

    iput-object p1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;->trackId:Ljava/lang/String;

    .line 924
    :cond_0
    :goto_0
    return-void

    .line 659
    :cond_1
    const-string v2, "/SM_RichInfo/Track/Title"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 660
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mTrack:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;

    if-eqz v0, :cond_0

    .line 661
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mTrack:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;

    iput-object p1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;->title:Ljava/lang/String;

    goto :goto_0

    .line 663
    :cond_2
    const-string v2, "/SM_RichInfo/Track/GenreDesc"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 664
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mTrack:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;

    if-eqz v0, :cond_0

    .line 665
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mTrack:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;

    iput-object p1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;->genreDesc:Ljava/lang/String;

    goto :goto_0

    .line 667
    :cond_3
    const-string v2, "/SM_RichInfo/Track/PlayLength"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 669
    :try_start_0
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mTrack:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;

    if-eqz v0, :cond_0

    .line 670
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mTrack:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;

    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;->playLength:I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 672
    :catch_0
    move-exception v0

    goto :goto_0

    .line 674
    :cond_4
    const-string v2, "/SM_RichInfo/Track/IsPick"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 675
    sget-object v2, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mTrack:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;

    if-eqz v2, :cond_0

    .line 676
    sget-object v2, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mTrack:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;

    const-string v3, "true"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    :goto_1
    iput-boolean v0, v2, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;->isPick:Z

    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_1

    .line 678
    :cond_6
    const-string v2, "/SM_RichInfo/Track/ISRC"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 679
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mTrack:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;

    if-eqz v0, :cond_0

    .line 680
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mTrack:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;

    iput-object p1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;->isrc:Ljava/lang/String;

    goto :goto_0

    .line 682
    :cond_7
    const-string v2, "/SM_RichInfo/Track/Composers/Composer/ID"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 683
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mArtistInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    if-eqz v0, :cond_0

    .line 684
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mArtistInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    iput-object p1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;->id:Ljava/lang/String;

    goto :goto_0

    .line 686
    :cond_8
    const-string v2, "/SM_RichInfo/Track/Composers/Composer/Name"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 687
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mArtistInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    if-eqz v0, :cond_0

    .line 688
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mArtistInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    iput-object p1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;->name:Ljava/lang/String;

    goto/16 :goto_0

    .line 690
    :cond_9
    const-string v2, "/SM_RichInfo/Track/Performers/PerformerSimpleInfo/ID"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 691
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mArtistInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    if-eqz v0, :cond_0

    .line 692
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mArtistInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    iput-object p1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;->id:Ljava/lang/String;

    goto/16 :goto_0

    .line 694
    :cond_a
    const-string v2, "/SM_RichInfo/Track/Performers/PerformerSimpleInfo/NAME"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 695
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mArtistInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    if-eqz v0, :cond_0

    .line 696
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mArtistInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    iput-object p1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;->name:Ljava/lang/String;

    goto/16 :goto_0

    .line 698
    :cond_b
    const-string v2, "/SM_RichInfo/Album/AlbumSimpleInfo/AlbumID"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 699
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbumInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;

    if-eqz v0, :cond_0

    .line 700
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbumInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;

    iput-object p1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;->albumId:Ljava/lang/String;

    goto/16 :goto_0

    .line 702
    :cond_c
    const-string v2, "/SM_RichInfo/Album/AlbumSimpleInfo/Title"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 703
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbumInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;

    if-eqz v0, :cond_0

    .line 704
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbumInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;

    iput-object p1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;->title:Ljava/lang/String;

    goto/16 :goto_0

    .line 706
    :cond_d
    const-string v2, "/SM_RichInfo/Album/AlbumSimpleInfo/Rating"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 707
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbumInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;

    if-eqz v0, :cond_0

    .line 709
    :try_start_1
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbumInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;

    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;->rating:I
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    .line 710
    :catch_1
    move-exception v0

    goto/16 :goto_0

    .line 713
    :cond_e
    const-string v2, "/SM_RichInfo/Album/AlbumSimpleInfo/Artist/ID"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_f

    .line 714
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mArtistInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    if-eqz v0, :cond_0

    .line 715
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mArtistInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    iput-object p1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;->id:Ljava/lang/String;

    goto/16 :goto_0

    .line 717
    :cond_f
    const-string v2, "/SM_RichInfo/Album/AlbumSimpleInfo/Artist/Name"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 718
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mArtistInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    if-eqz v0, :cond_0

    .line 719
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mArtistInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    iput-object p1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;->name:Ljava/lang/String;

    goto/16 :goto_0

    .line 721
    :cond_10
    const-string v2, "/SM_RichInfo/Album/AlbumSimpleInfo/IsPick"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_12

    .line 722
    sget-object v2, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbumInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;

    if-eqz v2, :cond_0

    .line 723
    sget-object v2, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbumInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;

    const-string v3, "true"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_11

    :goto_2
    iput-boolean v0, v2, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;->isPick:Z

    goto/16 :goto_0

    :cond_11
    move v0, v1

    goto :goto_2

    .line 725
    :cond_12
    const-string v2, "/SM_RichInfo/Album/AlbumSimpleInfo/Artwork"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_13

    .line 726
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mArtworkInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtworkInfo;

    if-eqz v0, :cond_0

    .line 727
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mArtworkInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtworkInfo;

    iput-object p1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtworkInfo;->artwork:Ljava/lang/String;

    goto/16 :goto_0

    .line 729
    :cond_13
    const-string v2, "/SM_RichInfo/Album/GenreDesc"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_14

    .line 730
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbum:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;

    if-eqz v0, :cond_0

    .line 731
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbum:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;

    iput-object p1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;->genreDesc:Ljava/lang/String;

    goto/16 :goto_0

    .line 733
    :cond_14
    const-string v2, "/SM_RichInfo/Album/Review"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_15

    .line 734
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbum:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;

    if-eqz v0, :cond_0

    .line 735
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbum:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;

    iput-object p1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;->review:Ljava/lang/String;

    goto/16 :goto_0

    .line 737
    :cond_15
    const-string v2, "/SM_RichInfo/Album/Date"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_16

    .line 738
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbum:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;

    if-eqz v0, :cond_0

    .line 739
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbum:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;

    iput-object p1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;->date:Ljava/lang/String;

    goto/16 :goto_0

    .line 741
    :cond_16
    const-string v2, "/SM_RichInfo/Album/Releases/Release/Media"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_17

    .line 742
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mReleaseInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ReleaseInfo;

    if-eqz v0, :cond_0

    .line 743
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mReleaseInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ReleaseInfo;

    iput-object p1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ReleaseInfo;->media:Ljava/lang/String;

    goto/16 :goto_0

    .line 745
    :cond_17
    const-string v2, "/SM_RichInfo/Album/Releases/Release/Label"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_18

    .line 746
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mReleaseInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ReleaseInfo;

    if-eqz v0, :cond_0

    .line 747
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mReleaseInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ReleaseInfo;

    iput-object p1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ReleaseInfo;->label:Ljava/lang/String;

    goto/16 :goto_0

    .line 749
    :cond_18
    const-string v2, "/SM_RichInfo/Album/Credits/Credit/CreditID"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_19

    .line 750
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mCredit:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$Credit;

    if-eqz v0, :cond_0

    .line 751
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mCredit:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$Credit;

    iput-object p1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$Credit;->creditId:Ljava/lang/String;

    goto/16 :goto_0

    .line 753
    :cond_19
    const-string v2, "/SM_RichInfo/Album/Credits/Credit/Role"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1a

    .line 754
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mCredit:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$Credit;

    if-eqz v0, :cond_0

    .line 755
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mCredit:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$Credit;

    iput-object p1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$Credit;->role:Ljava/lang/String;

    goto/16 :goto_0

    .line 757
    :cond_1a
    const-string v2, "/SM_RichInfo/Album/Credits/Credit/PerformerSimpleInfo/ID"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1b

    .line 758
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mCredit:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$Credit;

    if-eqz v0, :cond_0

    .line 759
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mCredit:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$Credit;

    iget-object v0, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$Credit;->PerformerSimpleInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    iput-object p1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;->id:Ljava/lang/String;

    goto/16 :goto_0

    .line 761
    :cond_1b
    const-string v2, "/SM_RichInfo/Album/Credits/Credit/PerformerSimpleInfo/Name"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1c

    .line 762
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mCredit:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$Credit;

    if-eqz v0, :cond_0

    .line 763
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mCredit:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$Credit;

    iget-object v0, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$Credit;->PerformerSimpleInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    iput-object p1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;->name:Ljava/lang/String;

    goto/16 :goto_0

    .line 765
    :cond_1c
    const-string v2, "/SM_RichInfo/Album/Discs/Disc/TotalRunningTime"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1d

    .line 766
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mDisc:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$Disc;

    if-eqz v0, :cond_0

    .line 768
    :try_start_2
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mDisc:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$Disc;

    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$Disc;->totalRunningTime:I
    :try_end_2
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_0

    .line 769
    :catch_2
    move-exception v0

    goto/16 :goto_0

    .line 772
    :cond_1d
    const-string v2, "/SM_RichInfo/Album/Discs/Disc/CurrentMediaNumber"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1e

    .line 773
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mDisc:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$Disc;

    if-eqz v0, :cond_0

    .line 775
    :try_start_3
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mDisc:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$Disc;

    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$Disc;->currentMediaNumber:I
    :try_end_3
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_3} :catch_3

    goto/16 :goto_0

    .line 776
    :catch_3
    move-exception v0

    goto/16 :goto_0

    .line 780
    :cond_1e
    const-string v2, "/SM_RichInfo/Album/Discs/Disc/Tracks/Track/TrackID"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1f

    .line 781
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mTrack:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;

    if-eqz v0, :cond_0

    .line 782
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mTrack:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;

    iput-object p1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;->trackId:Ljava/lang/String;

    goto/16 :goto_0

    .line 784
    :cond_1f
    const-string v2, "/SM_RichInfo/Album/Discs/Disc/Tracks/Track/Title"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_20

    .line 785
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mTrack:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;

    if-eqz v0, :cond_0

    .line 786
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mTrack:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;

    iput-object p1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;->title:Ljava/lang/String;

    goto/16 :goto_0

    .line 788
    :cond_20
    const-string v2, "/SM_RichInfo/Album/Discs/Disc/Tracks/Track/GenreDesc"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_21

    .line 789
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mTrack:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;

    if-eqz v0, :cond_0

    .line 790
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mTrack:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;

    iput-object p1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;->genreDesc:Ljava/lang/String;

    goto/16 :goto_0

    .line 792
    :cond_21
    const-string v2, "/SM_RichInfo/Album/Discs/Disc/Tracks/Track/PlayLength"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_22

    .line 793
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mTrack:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;

    if-eqz v0, :cond_0

    .line 795
    :try_start_4
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mTrack:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;

    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;->playLength:I
    :try_end_4
    .catch Ljava/lang/NumberFormatException; {:try_start_4 .. :try_end_4} :catch_4

    goto/16 :goto_0

    .line 796
    :catch_4
    move-exception v0

    goto/16 :goto_0

    .line 799
    :cond_22
    const-string v2, "/SM_RichInfo/Album/Discs/Disc/Tracks/Track/IsPick"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_24

    .line 800
    sget-object v2, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mTrack:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;

    if-eqz v2, :cond_0

    .line 801
    sget-object v2, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mTrack:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;

    const-string v3, "true"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_23

    :goto_3
    iput-boolean v0, v2, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;->isPick:Z

    goto/16 :goto_0

    :cond_23
    move v0, v1

    goto :goto_3

    .line 803
    :cond_24
    const-string v2, "/SM_RichInfo/Album/Discs/Disc/Tracks/Track/ISRC"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_25

    .line 804
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mTrack:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;

    if-eqz v0, :cond_0

    .line 805
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mTrack:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;

    iput-object p1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;->isrc:Ljava/lang/String;

    goto/16 :goto_0

    .line 807
    :cond_25
    const-string v2, "/SM_RichInfo/Album/Discs/Disc/Tracks/Track/Composers/Composer/ID"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_26

    .line 808
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mArtistInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    if-eqz v0, :cond_0

    .line 809
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mArtistInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    iput-object p1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;->id:Ljava/lang/String;

    goto/16 :goto_0

    .line 811
    :cond_26
    const-string v2, "/SM_RichInfo/Album/Discs/Disc/Tracks/Track/Composers/Composer/Name"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_27

    .line 812
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mArtistInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    if-eqz v0, :cond_0

    .line 813
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mArtistInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    iput-object p1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;->name:Ljava/lang/String;

    goto/16 :goto_0

    .line 815
    :cond_27
    const-string v2, "/SM_RichInfo/Album/Styles/Style/ID"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_28

    .line 816
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mStyleInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$StyleInfo;

    if-eqz v0, :cond_0

    .line 817
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mStyleInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$StyleInfo;

    iput-object p1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$StyleInfo;->styleId:Ljava/lang/String;

    goto/16 :goto_0

    .line 819
    :cond_28
    const-string v2, "/SM_RichInfo/Album/Styles/Style/Name"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_29

    .line 820
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mStyleInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$StyleInfo;

    if-eqz v0, :cond_0

    .line 821
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mStyleInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$StyleInfo;

    iput-object p1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$StyleInfo;->name:Ljava/lang/String;

    goto/16 :goto_0

    .line 823
    :cond_29
    const-string v2, "/SM_RichInfo/Album/Tones/Tone/ID"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2a

    .line 824
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mToneInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ToneInfo;

    if-eqz v0, :cond_0

    .line 825
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mToneInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ToneInfo;

    iput-object p1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ToneInfo;->toneId:Ljava/lang/String;

    goto/16 :goto_0

    .line 827
    :cond_2a
    const-string v2, "/SM_RichInfo/Album/Tones/Tone/Name"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2b

    .line 828
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mToneInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ToneInfo;

    if-eqz v0, :cond_0

    .line 829
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mToneInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ToneInfo;

    iput-object p1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ToneInfo;->name:Ljava/lang/String;

    goto/16 :goto_0

    .line 831
    :cond_2b
    const-string v2, "/SM_RichInfo/Album/SimilarAlbums/SimilarAlbum/AlbumID"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2c

    .line 832
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbumInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;

    if-eqz v0, :cond_0

    .line 833
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbumInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;

    iput-object p1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;->albumId:Ljava/lang/String;

    goto/16 :goto_0

    .line 835
    :cond_2c
    const-string v2, "/SM_RichInfo/Album/SimilarAlbums/SimilarAlbum/Title"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2d

    .line 836
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbumInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;

    if-eqz v0, :cond_0

    .line 837
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbumInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;

    iput-object p1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;->title:Ljava/lang/String;

    goto/16 :goto_0

    .line 839
    :cond_2d
    const-string v2, "/SM_RichInfo/Album/SimilarAlbums/SimilarAlbum/Rating"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2e

    .line 840
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbumInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;

    if-eqz v0, :cond_0

    .line 842
    :try_start_5
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbumInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;

    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;->rating:I
    :try_end_5
    .catch Ljava/lang/NumberFormatException; {:try_start_5 .. :try_end_5} :catch_5

    goto/16 :goto_0

    .line 843
    :catch_5
    move-exception v0

    goto/16 :goto_0

    .line 846
    :cond_2e
    const-string v2, "/SM_RichInfo/Album/SimilarAlbums/SimilarAlbum/Artist/ID"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2f

    .line 847
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbumInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;

    if-eqz v0, :cond_0

    .line 848
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbumInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;

    iget-object v0, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;->artist:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    iput-object p1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;->id:Ljava/lang/String;

    goto/16 :goto_0

    .line 850
    :cond_2f
    const-string v2, "/SM_RichInfo/Album/SimilarAlbums/SimilarAlbum/Artist/Name"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_30

    .line 851
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbumInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;

    if-eqz v0, :cond_0

    .line 852
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbumInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;

    iget-object v0, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;->artist:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    iput-object p1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;->name:Ljava/lang/String;

    goto/16 :goto_0

    .line 854
    :cond_30
    const-string v2, "/SM_RichInfo/Album/SimilarAlbums/SimilarAlbum/IsPick"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_32

    .line 855
    sget-object v2, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbumInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;

    if-eqz v2, :cond_0

    .line 856
    sget-object v2, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbumInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;

    const-string v3, "true"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_31

    :goto_4
    iput-boolean v0, v2, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;->isPick:Z

    goto/16 :goto_0

    :cond_31
    move v0, v1

    goto :goto_4

    .line 858
    :cond_32
    const-string v2, "/SM_RichInfo/Album/SimilarAlbums/SimilarAlbum/Artwork"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_33

    .line 859
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbumInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbumInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;

    iget-object v0, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;->artwork:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtworkInfo;

    if-eqz v0, :cond_0

    .line 860
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbumInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;

    iget-object v0, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;->artwork:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtworkInfo;

    iput-object p1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtworkInfo;->artwork:Ljava/lang/String;

    goto/16 :goto_0

    .line 862
    :cond_33
    const-string v2, "/SM_RichInfo/Performer/PerformerSimpleInfo/ID"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_34

    .line 863
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mPerformerInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$PerformerDetailInfo;

    iget-object v0, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$PerformerDetailInfo;->performerSimpleInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    iput-object p1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;->id:Ljava/lang/String;

    goto/16 :goto_0

    .line 864
    :cond_34
    const-string v2, "/SM_RichInfo/Performer/PerformerSimpleInfo/Name"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_35

    .line 865
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mPerformerInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$PerformerDetailInfo;

    iget-object v0, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$PerformerDetailInfo;->performerSimpleInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    iput-object p1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;->name:Ljava/lang/String;

    goto/16 :goto_0

    .line 866
    :cond_35
    const-string v2, "/SM_RichInfo/Performer/Biography"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_36

    .line 867
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mPerformerInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$PerformerDetailInfo;

    iput-object p1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$PerformerDetailInfo;->biography:Ljava/lang/String;

    goto/16 :goto_0

    .line 868
    :cond_36
    const-string v2, "/SM_RichInfo/Performer/Albums/AlbumSimpleInfo/AlbumID"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_37

    .line 869
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbumInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;

    if-eqz v0, :cond_0

    .line 870
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbumInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;

    iput-object p1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;->albumId:Ljava/lang/String;

    goto/16 :goto_0

    .line 872
    :cond_37
    const-string v2, "/SM_RichInfo/Performer/Albums/AlbumSimpleInfo/Title"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_38

    .line 873
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbumInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;

    if-eqz v0, :cond_0

    .line 874
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbumInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;

    iput-object p1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;->title:Ljava/lang/String;

    goto/16 :goto_0

    .line 876
    :cond_38
    const-string v2, "/SM_RichInfo/Performer/Albums/AlbumSimpleInfo/Rating"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_39

    .line 877
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbumInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;

    if-eqz v0, :cond_0

    .line 879
    :try_start_6
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbumInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;

    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;->rating:I
    :try_end_6
    .catch Ljava/lang/NumberFormatException; {:try_start_6 .. :try_end_6} :catch_6

    goto/16 :goto_0

    .line 880
    :catch_6
    move-exception v0

    goto/16 :goto_0

    .line 883
    :cond_39
    const-string v2, "/SM_RichInfo/Performer/Albums/AlbumSimpleInfo/Artist/ID"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3a

    .line 884
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbumInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;

    if-eqz v0, :cond_0

    .line 885
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbumInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;

    iget-object v0, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;->artist:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    iput-object p1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;->id:Ljava/lang/String;

    goto/16 :goto_0

    .line 887
    :cond_3a
    const-string v2, "/SM_RichInfo/Performer/Albums/AlbumSimpleInfo/Artist/Name"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3b

    .line 888
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbumInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;

    if-eqz v0, :cond_0

    .line 889
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbumInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;

    iget-object v0, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;->artist:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    iput-object p1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;->name:Ljava/lang/String;

    goto/16 :goto_0

    .line 891
    :cond_3b
    const-string v2, "/SM_RichInfo/Performer/Albums/AlbumSimpleInfo/IsPick"

    invoke-virtual {p0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3d

    .line 892
    sget-object v2, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbumInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;

    if-eqz v2, :cond_0

    .line 893
    sget-object v2, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbumInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;

    const-string v3, "true"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3c

    :goto_5
    iput-boolean v0, v2, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;->isPick:Z

    goto/16 :goto_0

    :cond_3c
    move v0, v1

    goto :goto_5

    .line 895
    :cond_3d
    const-string v0, "/SM_RichInfo/Performer/Albums/AlbumSimpleInfo/Artwork"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3e

    .line 896
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbumInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;

    if-eqz v0, :cond_0

    .line 897
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mAlbumInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;

    iget-object v0, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;->artwork:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtworkInfo;

    iput-object p1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtworkInfo;->artwork:Ljava/lang/String;

    goto/16 :goto_0

    .line 899
    :cond_3e
    const-string v0, "/SM_RichInfo/Performer/SimilarArtists/SimilarArtist/ID"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3f

    .line 900
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mArtistInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    if-eqz v0, :cond_0

    .line 901
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mArtistInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    iput-object p1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;->id:Ljava/lang/String;

    goto/16 :goto_0

    .line 903
    :cond_3f
    const-string v0, "/SM_RichInfo/Performer/SimilarArtists/SimilarArtist/Name"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_40

    .line 904
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mArtistInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    if-eqz v0, :cond_0

    .line 905
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mArtistInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    iput-object p1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;->name:Ljava/lang/String;

    goto/16 :goto_0

    .line 907
    :cond_40
    const-string v0, "/SM_RichInfo/Performer/InfluenceArtists/InfluenceArtist/ID"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_41

    .line 908
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mArtistInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    if-eqz v0, :cond_0

    .line 909
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mArtistInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    iput-object p1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;->id:Ljava/lang/String;

    goto/16 :goto_0

    .line 911
    :cond_41
    const-string v0, "/SM_RichInfo/Performer/InfluenceArtists/InfluenceArtist/Name"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_42

    .line 912
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mArtistInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    if-eqz v0, :cond_0

    .line 913
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mArtistInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    iput-object p1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;->name:Ljava/lang/String;

    goto/16 :goto_0

    .line 915
    :cond_42
    const-string v0, "/SM_RichInfo/Performer/Followers/Follower/ID"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_43

    .line 916
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mArtistInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    if-eqz v0, :cond_0

    .line 917
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mArtistInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    iput-object p1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;->id:Ljava/lang/String;

    goto/16 :goto_0

    .line 919
    :cond_43
    const-string v0, "/SM_RichInfo/Performer/Followers/Follower/Name"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 920
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mArtistInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    if-eqz v0, :cond_0

    .line 921
    sget-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mArtistInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    iput-object p1, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;->name:Ljava/lang/String;

    goto/16 :goto_0
.end method

.method private static parseXML(Ljava/io/InputStream;)Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;
    .locals 11
    .param p0, "input"    # Ljava/io/InputStream;

    .prologue
    const/4 v10, 0x1

    const/4 v6, 0x0

    .line 317
    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v3

    .line 318
    .local v3, "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    const/4 v7, 0x1

    invoke-virtual {v3, v7}, Lorg/xmlpull/v1/XmlPullParserFactory;->setNamespaceAware(Z)V

    .line 319
    invoke-virtual {v3}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v5

    .line 320
    .local v5, "xpp":Lorg/xmlpull/v1/XmlPullParser;
    const-string v7, "ISO-8859-1"

    invoke-interface {v5, p0, v7}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/InputStream;Ljava/lang/String;)V

    .line 323
    new-instance v0, Ljava/lang/StringBuffer;

    const-string v7, ""

    invoke-direct {v0, v7}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 324
    .local v0, "current":Ljava/lang/StringBuffer;
    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_2

    move-result v2

    .line 326
    .local v2, "eventType":I
    :goto_0
    if-eq v2, v10, :cond_2

    .line 327
    packed-switch v2, :pswitch_data_0

    .line 378
    :cond_0
    :goto_1
    :pswitch_0
    :try_start_1
    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->next()I
    :try_end_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_2

    move-result v2

    goto :goto_0

    .line 337
    :pswitch_1
    :try_start_2
    const-string v7, "/"

    invoke-virtual {v0, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 338
    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 345
    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeCount()I

    move-result v7

    if-eqz v7, :cond_1

    .line 346
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_2
    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeCount()I

    move-result v7

    if-ge v4, v7, :cond_0

    .line 353
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "@"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-interface {v5, v4}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v5, v4}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->parseStartTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 346
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 357
    .end local v4    # "i":I
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    const-string v8, ""

    invoke-static {v7, v8}, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->parseStartTag(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_1

    .line 384
    .end local v0    # "current":Ljava/lang/StringBuffer;
    .end local v2    # "eventType":I
    .end local v3    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v5    # "xpp":Lorg/xmlpull/v1/XmlPullParser;
    :catch_0
    move-exception v1

    .line 385
    .local v1, "e":Lorg/xmlpull/v1/XmlPullParserException;
    const-string v7, "RichInfo"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v9, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->CLASSTAG:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " (2)message : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v1}, Lorg/xmlpull/v1/XmlPullParserException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 394
    .end local v1    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    :goto_3
    return-object v6

    .line 361
    .restart local v0    # "current":Ljava/lang/StringBuffer;
    .restart local v2    # "eventType":I
    .restart local v3    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .restart local v5    # "xpp":Lorg/xmlpull/v1/XmlPullParser;
    :pswitch_2
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->parseEndTag(Ljava/lang/String;)V

    .line 362
    const-string v7, "/"

    invoke-virtual {v0, v7}, Ljava/lang/StringBuffer;->lastIndexOf(Ljava/lang/String;)I

    move-result v7

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->length()I

    move-result v8

    invoke-virtual {v0, v7, v8}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;
    :try_end_3
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_2

    goto/16 :goto_1

    .line 387
    .end local v0    # "current":Ljava/lang/StringBuffer;
    .end local v2    # "eventType":I
    .end local v3    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v5    # "xpp":Lorg/xmlpull/v1/XmlPullParser;
    :catch_1
    move-exception v1

    .line 388
    .local v1, "e":Ljava/io/IOException;
    const-string v7, "RichInfo"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v9, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->CLASSTAG:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " (3)message : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 372
    .end local v1    # "e":Ljava/io/IOException;
    .restart local v0    # "current":Ljava/lang/StringBuffer;
    .restart local v2    # "eventType":I
    .restart local v3    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .restart local v5    # "xpp":Lorg/xmlpull/v1/XmlPullParser;
    :pswitch_3
    :try_start_4
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->parseTag(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_4 .. :try_end_4} :catch_2

    goto/16 :goto_1

    .line 390
    .end local v0    # "current":Ljava/lang/StringBuffer;
    .end local v2    # "eventType":I
    .end local v3    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v5    # "xpp":Lorg/xmlpull/v1/XmlPullParser;
    :catch_2
    move-exception v1

    .line 391
    .local v1, "e":Ljava/lang/NullPointerException;
    const-string v7, "RichInfo"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v9, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->CLASSTAG:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " (4)message : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v1}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 379
    .end local v1    # "e":Ljava/lang/NullPointerException;
    .restart local v0    # "current":Ljava/lang/StringBuffer;
    .restart local v2    # "eventType":I
    .restart local v3    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .restart local v5    # "xpp":Lorg/xmlpull/v1/XmlPullParser;
    :catch_3
    move-exception v1

    .line 380
    .local v1, "e":Lorg/xmlpull/v1/XmlPullParserException;
    :try_start_5
    const-string v7, "RichInfo"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v9, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->CLASSTAG:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " (1)message : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v1}, Lorg/xmlpull/v1/XmlPullParserException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 381
    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->next()I
    :try_end_5
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_5 .. :try_end_5} :catch_0
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_5 .. :try_end_5} :catch_2

    move-result v2

    .line 382
    goto/16 :goto_0

    .line 394
    .end local v1    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    :cond_2
    sget-object v6, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mRichInfoData:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;

    goto/16 :goto_3

    .line 327
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static parsingRichInfoXML(Ljava/io/InputStream;)Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;
    .locals 1
    .param p0, "input"    # Ljava/io/InputStream;

    .prologue
    .line 310
    new-instance v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;

    invoke-direct {v0}, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;-><init>()V

    sput-object v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->mRichInfoData:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;

    .line 311
    invoke-static {p0}, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoParser;->parseXML(Ljava/io/InputStream;)Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;

    move-result-object v0

    return-object v0
.end method
