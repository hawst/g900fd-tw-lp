.class public Lcom/samsung/musicplus/mediainfo/findtag/ShazamNetwork;
.super Ljava/lang/Object;
.source "ShazamNetwork.java"


# static fields
.field public static final AMG_ARTWORK_URL:Ljava/lang/String; = "http://image.allmusic.com/02/"

.field public static final APPLICATION_IDENTIFIER:Ljava/lang/String; = "Samsung_FT_Android_GA__3.0.0"

.field public static final CHART:Ljava/lang/String; = "chart"

.field public static final CONFIG:Ljava/lang/String; = "config"

.field public static final CONFIG_SERVICE:Ljava/lang/String; = "cn=Config,cn=Samsung,cn=services"

.field public static final CRYPT_TOKEN:Ljava/lang/String; = "025B58C0"

.field public static final CURRENT_TIME:Ljava/lang/String; = "currentTime"

.field public static final DEVICEMODEL:Ljava/lang/String;

.field public static final DO_RECOGNITION_1:Ljava/lang/String; = "doRecognition1"

.field public static final ELEMENTS:Ljava/lang/String; = "elements"

.field public static final ORBIT_AMG_FIELDS:Ljava/lang/String; = "amgFields"

.field public static final ORBIT_APPLICATION_IDENTIFIER:Ljava/lang/String; = "applicationIdentifier"

.field public static final ORBIT_CONTENT_TYPE:Ljava/lang/String; = "multipart/form-data"

.field public static final ORBIT_CONTENT_TYPE2:Ljava/lang/String; = "application/octet-stream"

.field public static final ORBIT_COVERART:Ljava/lang/String; = "coverartSize"

.field public static final ORBIT_CTYPT_TOKEN:Ljava/lang/String; = "cryptToken"

.field public static final ORBIT_DEVICEID:Ljava/lang/String; = "deviceId"

.field public static final ORBIT_DEVICEMODEL:Ljava/lang/String; = "deviceModel"

.field public static final ORBIT_DO_ROCOGNITION:Ljava/lang/String; = "orbit/DoRecognition1"

.field public static final ORBIT_LANGUAGE:Ljava/lang/String; = "language"

.field public static final ORBIT_RELEASE_URL:Ljava/lang/String; = "http://ssung.meta.shazamid.com/"

.field public static final ORBIT_REQUESTID:Ljava/lang/String; = "requestId"

.field public static final ORBIT_REQUEST_CONFIG:Ljava/lang/String; = "orbit/RequestConfig1"

.field public static final ORBIT_REQUEST_RESULTS:Ljava/lang/String; = "orbit/RequestResults1"

.field public static final ORBIT_SAMPLE:Ljava/lang/String; = "sample"

.field public static final ORBIT_SAMPLE_BYTES:Ljava/lang/String; = "sampleBytes"

.field public static final ORBIT_SERVICE:Ljava/lang/String; = "service"

.field public static final ORBIT_TAGDATE:Ljava/lang/String; = "tagDate"

.field public static final ORBIT_TEST_URL:Ljava/lang/String; = "http://partnertesting-orbit.shazamteam.net/"

.field public static final ORBIT_TIMEOUT:Ljava/lang/String; = "timeout"

.field public static final ORBIT_URL:Ljava/lang/String; = "http://ssung.meta.shazamid.com/"

.field public static final REQUEST_CONFIG_1:Ljava/lang/String; = "requestConfig1"

.field public static final SERVICE:Ljava/lang/String; = "service"

.field public static final UMS_FIND_URL:Ljava/lang/String; = "http://ums.samsungmobile.com/http_client/find_url.jsp"

.field public static final VALUE:Ljava/lang/String; = "value"


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 55
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Landroid/os/Build;->BRAND:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/mediainfo/findtag/ShazamNetwork;->DEVICEMODEL:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 4
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
