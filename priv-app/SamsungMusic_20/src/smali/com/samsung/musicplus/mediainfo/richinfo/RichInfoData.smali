.class public Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;
.super Ljava/lang/Object;
.source "RichInfoData.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$PerformerDetailInfo;,
        Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;,
        Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;,
        Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ToneInfo;,
        Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$StyleInfo;,
        Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$Disc;,
        Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$Credit;,
        Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ReleaseInfo;,
        Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;,
        Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtworkInfo;,
        Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;,
        Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$GenreInfo;
    }
.end annotation


# instance fields
.field public albumDetailInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;

.field public performerDetailInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$PerformerDetailInfo;

.field public trackInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    new-instance v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;

    invoke-direct {v0}, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;->trackInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;

    .line 16
    new-instance v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;

    invoke-direct {v0}, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;->albumDetailInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;

    .line 17
    new-instance v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$PerformerDetailInfo;

    invoke-direct {v0}, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$PerformerDetailInfo;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;->performerDetailInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$PerformerDetailInfo;

    .line 18
    return-void
.end method
