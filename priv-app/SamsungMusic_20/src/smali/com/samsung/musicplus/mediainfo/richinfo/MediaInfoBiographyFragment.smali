.class public Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoBiographyFragment;
.super Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoBaseFragment;
.source "MediaInfoBiographyFragment.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoBaseFragment;-><init>()V

    return-void
.end method

.method private setContents()V
    .locals 12

    .prologue
    const/4 v11, 0x0

    .line 29
    invoke-virtual {p0}, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoBiographyFragment;->getView()Landroid/view/View;

    move-result-object v5

    .line 30
    .local v5, "root":Landroid/view/View;
    const v7, 0x7f0d00f0

    invoke-virtual {v5, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 32
    .local v0, "albumView":Landroid/widget/ImageView;
    iget-object v7, p0, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoBiographyFragment;->mParsedData:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoBiographyFragment;->mParsedData:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;

    iget-object v7, v7, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;->albumDetailInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;

    iget-object v7, v7, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;->albumSimpleInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;

    if-eqz v7, :cond_0

    .line 34
    iget-object v7, p0, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoBiographyFragment;->mParsedData:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;

    iget-object v7, v7, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;->albumDetailInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;

    iget-object v7, v7, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;->albumSimpleInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;

    iget-object v1, v7, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;->artwork:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtworkInfo;

    .line 35
    .local v1, "artworkInfo":Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtworkInfo;
    if-eqz v1, :cond_2

    iget-object v7, v1, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtworkInfo;->artwork:Ljava/lang/String;

    if-eqz v7, :cond_2

    iget-object v7, v1, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtworkInfo;->artwork:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_2

    .line 36
    iget-object v7, v1, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtworkInfo;->artwork:Ljava/lang/String;

    invoke-static {v7}, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoUtils;->getArtwork(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 37
    .local v3, "bm":Landroid/graphics/Bitmap;
    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 38
    invoke-virtual {v0, v11}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 48
    .end local v1    # "artworkInfo":Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtworkInfo;
    .end local v3    # "bm":Landroid/graphics/Bitmap;
    :cond_0
    :goto_0
    const v7, 0x7f0d00f1

    invoke-virtual {v5, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 49
    .local v4, "contentView":Landroid/widget/TextView;
    iget-object v7, p0, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoBiographyFragment;->mParsedData:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;

    if-eqz v7, :cond_1

    .line 50
    iget-object v7, p0, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoBiographyFragment;->mParsedData:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;

    iget-object v7, v7, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;->performerDetailInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$PerformerDetailInfo;

    iget-object v2, v7, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$PerformerDetailInfo;->biography:Ljava/lang/String;

    .line 51
    .local v2, "biography":Ljava/lang/String;
    if-eqz v2, :cond_1

    const-string v7, ""

    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_1

    .line 52
    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 55
    .end local v2    # "biography":Ljava/lang/String;
    :cond_1
    return-void

    .line 39
    .end local v4    # "contentView":Landroid/widget/TextView;
    .restart local v1    # "artworkInfo":Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtworkInfo;
    :cond_2
    if-eqz v1, :cond_0

    iget-object v7, v1, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtworkInfo;->uri:Ljava/lang/String;

    if-eqz v7, :cond_0

    iget-object v7, v1, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtworkInfo;->uri:Ljava/lang/String;

    invoke-virtual {v7}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_0

    .line 41
    invoke-virtual {p0}, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoBiographyFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f0c0098

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 43
    .local v6, "size":I
    new-instance v7, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoUtils$AlbumArtLoader;

    invoke-direct {v7}, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoUtils$AlbumArtLoader;-><init>()V

    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/Object;

    aput-object v0, v8, v11

    const/4 v9, 0x1

    iget-object v10, v1, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtworkInfo;->uri:Ljava/lang/String;

    aput-object v10, v8, v9

    const/4 v9, 0x2

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {v7, v8}, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoUtils$AlbumArtLoader;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 44
    invoke-virtual {v0, v11}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 24
    invoke-super {p0, p1}, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoBaseFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 25
    invoke-direct {p0}, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoBiographyFragment;->setContents()V

    .line 26
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 19
    const v0, 0x7f04004f

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
