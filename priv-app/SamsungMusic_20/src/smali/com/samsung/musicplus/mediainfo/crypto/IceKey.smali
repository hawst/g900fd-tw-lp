.class public Lcom/samsung/musicplus/mediainfo/crypto/IceKey;
.super Ljava/lang/Object;
.source "IceKey.java"


# static fields
.field private static final keyrot:[I

.field private static final pBox:[I

.field private static final sMod:[[I

.field private static final sXor:[[I

.field private static spBox:[[I

.field private static spBoxInitialised:Z


# instance fields
.field private keySchedule:[[I

.field private rounds:I

.field private size:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x4

    .line 24
    sput-boolean v3, Lcom/samsung/musicplus/mediainfo/crypto/IceKey;->spBoxInitialised:Z

    .line 26
    new-array v0, v2, [[I

    new-array v1, v2, [I

    fill-array-data v1, :array_0

    aput-object v1, v0, v3

    new-array v1, v2, [I

    fill-array-data v1, :array_1

    aput-object v1, v0, v4

    new-array v1, v2, [I

    fill-array-data v1, :array_2

    aput-object v1, v0, v5

    new-array v1, v2, [I

    fill-array-data v1, :array_3

    aput-object v1, v0, v6

    sput-object v0, Lcom/samsung/musicplus/mediainfo/crypto/IceKey;->sMod:[[I

    .line 38
    new-array v0, v2, [[I

    new-array v1, v2, [I

    fill-array-data v1, :array_4

    aput-object v1, v0, v3

    new-array v1, v2, [I

    fill-array-data v1, :array_5

    aput-object v1, v0, v4

    new-array v1, v2, [I

    fill-array-data v1, :array_6

    aput-object v1, v0, v5

    new-array v1, v2, [I

    fill-array-data v1, :array_7

    aput-object v1, v0, v6

    sput-object v0, Lcom/samsung/musicplus/mediainfo/crypto/IceKey;->sXor:[[I

    .line 50
    const/16 v0, 0x20

    new-array v0, v0, [I

    fill-array-data v0, :array_8

    sput-object v0, Lcom/samsung/musicplus/mediainfo/crypto/IceKey;->pBox:[I

    .line 58
    const/16 v0, 0x10

    new-array v0, v0, [I

    fill-array-data v0, :array_9

    sput-object v0, Lcom/samsung/musicplus/mediainfo/crypto/IceKey;->keyrot:[I

    return-void

    .line 26
    :array_0
    .array-data 4
        0x14d
        0x139
        0x1f9
        0x171
    .end array-data

    :array_1
    .array-data 4
        0x17b
        0x177
        0x13f
        0x187
    .end array-data

    :array_2
    .array-data 4
        0x169
        0x1bd
        0x1c3
        0x18d
    .end array-data

    :array_3
    .array-data 4
        0x18d
        0x1a9
        0x18b
        0x1f9
    .end array-data

    .line 38
    :array_4
    .array-data 4
        0x83
        0x85
        0x9b
        0xcd
    .end array-data

    :array_5
    .array-data 4
        0xcc
        0xa7
        0xad
        0x41
    .end array-data

    :array_6
    .array-data 4
        0x4b
        0x2e
        0xd4
        0x33
    .end array-data

    :array_7
    .array-data 4
        0xea
        0xcb
        0x2e
        0x4
    .end array-data

    .line 50
    :array_8
    .array-data 4
        0x1
        0x80
        0x400
        0x2000
        0x80000
        0x200000
        0x1000000
        0x40000000    # 2.0f
        0x8
        0x20
        0x100
        0x4000
        0x10000
        0x800000
        0x4000000
        0x20000000
        0x4
        0x10
        0x200
        0x8000
        0x20000
        0x400000
        0x8000000
        0x10000000
        0x2
        0x40
        0x800
        0x1000
        0x40000
        0x100000
        0x2000000
        -0x80000000
    .end array-data

    .line 58
    :array_9
    .array-data 4
        0x0
        0x1
        0x2
        0x3
        0x2
        0x1
        0x3
        0x0
        0x1
        0x3
        0x2
        0x0
        0x3
        0x1
        0x0
        0x2
    .end array-data
.end method

.method public constructor <init>(I)V
    .locals 2
    .param p1, "level"    # I

    .prologue
    const/4 v1, 0x1

    .line 141
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 142
    sget-boolean v0, Lcom/samsung/musicplus/mediainfo/crypto/IceKey;->spBoxInitialised:Z

    if-nez v0, :cond_0

    .line 143
    invoke-direct {p0}, Lcom/samsung/musicplus/mediainfo/crypto/IceKey;->spBoxInit()V

    .line 144
    sput-boolean v1, Lcom/samsung/musicplus/mediainfo/crypto/IceKey;->spBoxInitialised:Z

    .line 147
    :cond_0
    if-ge p1, v1, :cond_1

    .line 148
    iput v1, p0, Lcom/samsung/musicplus/mediainfo/crypto/IceKey;->size:I

    .line 149
    const/16 v0, 0x8

    iput v0, p0, Lcom/samsung/musicplus/mediainfo/crypto/IceKey;->rounds:I

    .line 155
    :goto_0
    iget v0, p0, Lcom/samsung/musicplus/mediainfo/crypto/IceKey;->rounds:I

    const/4 v1, 0x3

    filled-new-array {v0, v1}, [I

    move-result-object v0

    sget-object v1, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[I

    iput-object v0, p0, Lcom/samsung/musicplus/mediainfo/crypto/IceKey;->keySchedule:[[I

    .line 156
    return-void

    .line 151
    :cond_1
    iput p1, p0, Lcom/samsung/musicplus/mediainfo/crypto/IceKey;->size:I

    .line 152
    mul-int/lit8 v0, p1, 0x10

    iput v0, p0, Lcom/samsung/musicplus/mediainfo/crypto/IceKey;->rounds:I

    goto :goto_0
.end method

.method private gf_exp7(II)I
    .locals 2
    .param p1, "b"    # I
    .param p2, "m"    # I

    .prologue
    .line 89
    if-nez p1, :cond_0

    .line 90
    const/4 v1, 0x0

    .line 96
    :goto_0
    return v1

    .line 93
    :cond_0
    invoke-direct {p0, p1, p1, p2}, Lcom/samsung/musicplus/mediainfo/crypto/IceKey;->gf_mult(III)I

    move-result v0

    .line 94
    .local v0, "x":I
    invoke-direct {p0, p1, v0, p2}, Lcom/samsung/musicplus/mediainfo/crypto/IceKey;->gf_mult(III)I

    move-result v0

    .line 95
    invoke-direct {p0, v0, v0, p2}, Lcom/samsung/musicplus/mediainfo/crypto/IceKey;->gf_mult(III)I

    move-result v0

    .line 96
    invoke-direct {p0, p1, v0, p2}, Lcom/samsung/musicplus/mediainfo/crypto/IceKey;->gf_mult(III)I

    move-result v1

    goto :goto_0
.end method

.method private gf_mult(III)I
    .locals 2
    .param p1, "a"    # I
    .param p2, "b"    # I
    .param p3, "m"    # I

    .prologue
    .line 66
    const/4 v0, 0x0

    .line 68
    .local v0, "res":I
    :cond_0
    :goto_0
    if-eqz p2, :cond_2

    .line 69
    and-int/lit8 v1, p2, 0x1

    if-eqz v1, :cond_1

    .line 70
    xor-int/2addr v0, p1

    .line 73
    :cond_1
    shl-int/lit8 p1, p1, 0x1

    .line 74
    ushr-int/lit8 p2, p2, 0x1

    .line 76
    const/16 v1, 0x100

    if-lt p1, v1, :cond_0

    .line 77
    xor-int/2addr p1, p3

    goto :goto_0

    .line 81
    :cond_2
    return v0
.end method

.method private perm32(I)I
    .locals 3
    .param p1, "x"    # I

    .prologue
    .line 101
    const/4 v1, 0x0

    .line 102
    .local v1, "res":I
    const/4 v0, 0x0

    .line 104
    .local v0, "i":I
    :goto_0
    if-eqz p1, :cond_1

    .line 105
    and-int/lit8 v2, p1, 0x1

    if-eqz v2, :cond_0

    .line 106
    sget-object v2, Lcom/samsung/musicplus/mediainfo/crypto/IceKey;->pBox:[I

    aget v2, v2, v0

    or-int/2addr v1, v2

    .line 108
    :cond_0
    add-int/lit8 v0, v0, 0x1

    .line 109
    ushr-int/lit8 p1, p1, 0x1

    goto :goto_0

    .line 112
    :cond_1
    return v1
.end method

.method private roundFunc(I[I)I
    .locals 11
    .param p1, "p"    # I
    .param p2, "subkey"    # [I

    .prologue
    const v10, 0xffc00

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 228
    ushr-int/lit8 v4, p1, 0x10

    and-int/lit16 v4, v4, 0x3ff

    ushr-int/lit8 v5, p1, 0xe

    shl-int/lit8 v6, p1, 0x12

    or-int/2addr v5, v6

    and-int/2addr v5, v10

    or-int v2, v4, v5

    .line 229
    .local v2, "tl":I
    and-int/lit16 v4, p1, 0x3ff

    shl-int/lit8 v5, p1, 0x2

    and-int/2addr v5, v10

    or-int v3, v4, v5

    .line 233
    .local v3, "tr":I
    aget v4, p2, v9

    xor-int v5, v2, v3

    and-int v0, v4, v5

    .line 234
    .local v0, "al":I
    xor-int v1, v0, v3

    .line 235
    .local v1, "ar":I
    xor-int/2addr v0, v2

    .line 237
    aget v4, p2, v7

    xor-int/2addr v0, v4

    .line 238
    aget v4, p2, v8

    xor-int/2addr v1, v4

    .line 240
    sget-object v4, Lcom/samsung/musicplus/mediainfo/crypto/IceKey;->spBox:[[I

    aget-object v4, v4, v7

    ushr-int/lit8 v5, v0, 0xa

    aget v4, v4, v5

    sget-object v5, Lcom/samsung/musicplus/mediainfo/crypto/IceKey;->spBox:[[I

    aget-object v5, v5, v8

    and-int/lit16 v6, v0, 0x3ff

    aget v5, v5, v6

    or-int/2addr v4, v5

    sget-object v5, Lcom/samsung/musicplus/mediainfo/crypto/IceKey;->spBox:[[I

    aget-object v5, v5, v9

    ushr-int/lit8 v6, v1, 0xa

    aget v5, v5, v6

    or-int/2addr v4, v5

    sget-object v5, Lcom/samsung/musicplus/mediainfo/crypto/IceKey;->spBox:[[I

    const/4 v6, 0x3

    aget-object v5, v5, v6

    and-int/lit16 v6, v1, 0x3ff

    aget v5, v5, v6

    or-int/2addr v4, v5

    return v4
.end method

.method private scheduleBuild([III)V
    .locals 11
    .param p1, "kb"    # [I
    .param p2, "n"    # I
    .param p3, "krot_idx"    # I

    .prologue
    .line 162
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    const/16 v8, 0x8

    if-ge v3, v8, :cond_3

    .line 164
    sget-object v8, Lcom/samsung/musicplus/mediainfo/crypto/IceKey;->keyrot:[I

    add-int v9, p3, v3

    aget v6, v8, v9

    .line 165
    .local v6, "kr":I
    iget-object v8, p0, Lcom/samsung/musicplus/mediainfo/crypto/IceKey;->keySchedule:[[I

    add-int v9, p2, v3

    aget-object v7, v8, v9

    .line 167
    .local v7, "subkey":[I
    const/4 v4, 0x0

    .local v4, "j":I
    :goto_1
    const/4 v8, 0x3

    if-ge v4, v8, :cond_0

    .line 168
    iget-object v8, p0, Lcom/samsung/musicplus/mediainfo/crypto/IceKey;->keySchedule:[[I

    add-int v9, p2, v3

    aget-object v8, v8, v9

    const/4 v9, 0x0

    aput v9, v8, v4

    .line 167
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 171
    :cond_0
    const/4 v4, 0x0

    :goto_2
    const/16 v8, 0xf

    if-ge v4, v8, :cond_2

    .line 173
    rem-int/lit8 v2, v4, 0x3

    .line 175
    .local v2, "curr_sk":I
    const/4 v5, 0x0

    .local v5, "k":I
    :goto_3
    const/4 v8, 0x4

    if-ge v5, v8, :cond_1

    .line 176
    add-int v8, v6, v5

    and-int/lit8 v8, v8, 0x3

    aget v1, p1, v8

    .line 177
    .local v1, "curr_kb":I
    and-int/lit8 v0, v1, 0x1

    .line 179
    .local v0, "bit":I
    aget v8, v7, v2

    shl-int/lit8 v8, v8, 0x1

    or-int/2addr v8, v0

    aput v8, v7, v2

    .line 180
    add-int v8, v6, v5

    and-int/lit8 v8, v8, 0x3

    ushr-int/lit8 v9, v1, 0x1

    xor-int/lit8 v10, v0, 0x1

    shl-int/lit8 v10, v10, 0xf

    or-int/2addr v9, v10

    aput v9, p1, v8

    .line 175
    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    .line 171
    .end local v0    # "bit":I
    .end local v1    # "curr_kb":I
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 162
    .end local v2    # "curr_sk":I
    .end local v5    # "k":I
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 184
    .end local v4    # "j":I
    .end local v6    # "kr":I
    .end local v7    # "subkey":[I
    :cond_3
    return-void
.end method

.method private spBoxInit()V
    .locals 11

    .prologue
    const/16 v10, 0x400

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 119
    const/4 v4, 0x4

    filled-new-array {v4, v10}, [I

    move-result-object v4

    sget-object v5, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v5, v4}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [[I

    sput-object v4, Lcom/samsung/musicplus/mediainfo/crypto/IceKey;->spBox:[[I

    .line 121
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v10, :cond_0

    .line 122
    ushr-int/lit8 v4, v1, 0x1

    and-int/lit16 v0, v4, 0xff

    .line 123
    .local v0, "col":I
    and-int/lit8 v4, v1, 0x1

    and-int/lit16 v5, v1, 0x200

    ushr-int/lit8 v5, v5, 0x8

    or-int v2, v4, v5

    .line 126
    .local v2, "row":I
    sget-object v4, Lcom/samsung/musicplus/mediainfo/crypto/IceKey;->sXor:[[I

    aget-object v4, v4, v6

    aget v4, v4, v2

    xor-int/2addr v4, v0

    sget-object v5, Lcom/samsung/musicplus/mediainfo/crypto/IceKey;->sMod:[[I

    aget-object v5, v5, v6

    aget v5, v5, v2

    invoke-direct {p0, v4, v5}, Lcom/samsung/musicplus/mediainfo/crypto/IceKey;->gf_exp7(II)I

    move-result v4

    shl-int/lit8 v3, v4, 0x18

    .line 127
    .local v3, "x":I
    sget-object v4, Lcom/samsung/musicplus/mediainfo/crypto/IceKey;->spBox:[[I

    aget-object v4, v4, v6

    invoke-direct {p0, v3}, Lcom/samsung/musicplus/mediainfo/crypto/IceKey;->perm32(I)I

    move-result v5

    aput v5, v4, v1

    .line 129
    sget-object v4, Lcom/samsung/musicplus/mediainfo/crypto/IceKey;->sXor:[[I

    aget-object v4, v4, v7

    aget v4, v4, v2

    xor-int/2addr v4, v0

    sget-object v5, Lcom/samsung/musicplus/mediainfo/crypto/IceKey;->sMod:[[I

    aget-object v5, v5, v7

    aget v5, v5, v2

    invoke-direct {p0, v4, v5}, Lcom/samsung/musicplus/mediainfo/crypto/IceKey;->gf_exp7(II)I

    move-result v4

    shl-int/lit8 v3, v4, 0x10

    .line 130
    sget-object v4, Lcom/samsung/musicplus/mediainfo/crypto/IceKey;->spBox:[[I

    aget-object v4, v4, v7

    invoke-direct {p0, v3}, Lcom/samsung/musicplus/mediainfo/crypto/IceKey;->perm32(I)I

    move-result v5

    aput v5, v4, v1

    .line 132
    sget-object v4, Lcom/samsung/musicplus/mediainfo/crypto/IceKey;->sXor:[[I

    aget-object v4, v4, v8

    aget v4, v4, v2

    xor-int/2addr v4, v0

    sget-object v5, Lcom/samsung/musicplus/mediainfo/crypto/IceKey;->sMod:[[I

    aget-object v5, v5, v8

    aget v5, v5, v2

    invoke-direct {p0, v4, v5}, Lcom/samsung/musicplus/mediainfo/crypto/IceKey;->gf_exp7(II)I

    move-result v4

    shl-int/lit8 v3, v4, 0x8

    .line 133
    sget-object v4, Lcom/samsung/musicplus/mediainfo/crypto/IceKey;->spBox:[[I

    aget-object v4, v4, v8

    invoke-direct {p0, v3}, Lcom/samsung/musicplus/mediainfo/crypto/IceKey;->perm32(I)I

    move-result v5

    aput v5, v4, v1

    .line 135
    sget-object v4, Lcom/samsung/musicplus/mediainfo/crypto/IceKey;->sXor:[[I

    aget-object v4, v4, v9

    aget v4, v4, v2

    xor-int/2addr v4, v0

    sget-object v5, Lcom/samsung/musicplus/mediainfo/crypto/IceKey;->sMod:[[I

    aget-object v5, v5, v9

    aget v5, v5, v2

    invoke-direct {p0, v4, v5}, Lcom/samsung/musicplus/mediainfo/crypto/IceKey;->gf_exp7(II)I

    move-result v3

    .line 136
    sget-object v4, Lcom/samsung/musicplus/mediainfo/crypto/IceKey;->spBox:[[I

    aget-object v4, v4, v9

    invoke-direct {p0, v3}, Lcom/samsung/musicplus/mediainfo/crypto/IceKey;->perm32(I)I

    move-result v5

    aput v5, v4, v1

    .line 121
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_0

    .line 138
    .end local v0    # "col":I
    .end local v2    # "row":I
    .end local v3    # "x":I
    :cond_0
    return-void
.end method


# virtual methods
.method public blockSize()I
    .locals 1

    .prologue
    .line 298
    const/16 v0, 0x8

    return v0
.end method

.method public clear()V
    .locals 4

    .prologue
    .line 216
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lcom/samsung/musicplus/mediainfo/crypto/IceKey;->rounds:I

    if-ge v0, v2, :cond_1

    .line 217
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_1
    const/4 v2, 0x3

    if-ge v1, v2, :cond_0

    .line 218
    iget-object v2, p0, Lcom/samsung/musicplus/mediainfo/crypto/IceKey;->keySchedule:[[I

    aget-object v2, v2, v0

    const/4 v3, 0x0

    aput v3, v2, v1

    .line 217
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 216
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 221
    .end local v1    # "j":I
    :cond_1
    return-void
.end method

.method public decrypt([B[B)V
    .locals 6
    .param p1, "ciphertext"    # [B
    .param p2, "plaintext"    # [B

    .prologue
    const/4 v5, 0x4

    .line 270
    const/4 v1, 0x0

    .local v1, "l":I
    const/4 v2, 0x0

    .line 272
    .local v2, "r":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v5, :cond_0

    .line 273
    aget-byte v3, p1, v0

    and-int/lit16 v3, v3, 0xff

    mul-int/lit8 v4, v0, 0x8

    rsub-int/lit8 v4, v4, 0x18

    shl-int/2addr v3, v4

    or-int/2addr v1, v3

    .line 274
    add-int/lit8 v3, v0, 0x4

    aget-byte v3, p1, v3

    and-int/lit16 v3, v3, 0xff

    mul-int/lit8 v4, v0, 0x8

    rsub-int/lit8 v4, v4, 0x18

    shl-int/2addr v3, v4

    or-int/2addr v2, v3

    .line 272
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 277
    :cond_0
    iget v3, p0, Lcom/samsung/musicplus/mediainfo/crypto/IceKey;->rounds:I

    add-int/lit8 v0, v3, -0x1

    :goto_1
    if-lez v0, :cond_1

    .line 278
    iget-object v3, p0, Lcom/samsung/musicplus/mediainfo/crypto/IceKey;->keySchedule:[[I

    aget-object v3, v3, v0

    invoke-direct {p0, v2, v3}, Lcom/samsung/musicplus/mediainfo/crypto/IceKey;->roundFunc(I[I)I

    move-result v3

    xor-int/2addr v1, v3

    .line 279
    iget-object v3, p0, Lcom/samsung/musicplus/mediainfo/crypto/IceKey;->keySchedule:[[I

    add-int/lit8 v4, v0, -0x1

    aget-object v3, v3, v4

    invoke-direct {p0, v1, v3}, Lcom/samsung/musicplus/mediainfo/crypto/IceKey;->roundFunc(I[I)I

    move-result v3

    xor-int/2addr v2, v3

    .line 277
    add-int/lit8 v0, v0, -0x2

    goto :goto_1

    .line 282
    :cond_1
    const/4 v0, 0x0

    :goto_2
    if-ge v0, v5, :cond_2

    .line 283
    rsub-int/lit8 v3, v0, 0x3

    and-int/lit16 v4, v2, 0xff

    int-to-byte v4, v4

    aput-byte v4, p2, v3

    .line 284
    rsub-int/lit8 v3, v0, 0x7

    and-int/lit16 v4, v1, 0xff

    int-to-byte v4, v4

    aput-byte v4, p2, v3

    .line 286
    ushr-int/lit8 v2, v2, 0x8

    .line 287
    ushr-int/lit8 v1, v1, 0x8

    .line 282
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 289
    :cond_2
    return-void
.end method

.method public encrypt([B[B)V
    .locals 6
    .param p1, "plaintext"    # [B
    .param p2, "ciphertext"    # [B

    .prologue
    const/4 v5, 0x4

    .line 246
    const/4 v1, 0x0

    .local v1, "l":I
    const/4 v2, 0x0

    .line 248
    .local v2, "r":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v5, :cond_0

    .line 249
    aget-byte v3, p1, v0

    and-int/lit16 v3, v3, 0xff

    mul-int/lit8 v4, v0, 0x8

    rsub-int/lit8 v4, v4, 0x18

    shl-int/2addr v3, v4

    or-int/2addr v1, v3

    .line 250
    add-int/lit8 v3, v0, 0x4

    aget-byte v3, p1, v3

    and-int/lit16 v3, v3, 0xff

    mul-int/lit8 v4, v0, 0x8

    rsub-int/lit8 v4, v4, 0x18

    shl-int/2addr v3, v4

    or-int/2addr v2, v3

    .line 248
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 253
    :cond_0
    const/4 v0, 0x0

    :goto_1
    iget v3, p0, Lcom/samsung/musicplus/mediainfo/crypto/IceKey;->rounds:I

    if-ge v0, v3, :cond_1

    .line 254
    iget-object v3, p0, Lcom/samsung/musicplus/mediainfo/crypto/IceKey;->keySchedule:[[I

    aget-object v3, v3, v0

    invoke-direct {p0, v2, v3}, Lcom/samsung/musicplus/mediainfo/crypto/IceKey;->roundFunc(I[I)I

    move-result v3

    xor-int/2addr v1, v3

    .line 255
    iget-object v3, p0, Lcom/samsung/musicplus/mediainfo/crypto/IceKey;->keySchedule:[[I

    add-int/lit8 v4, v0, 0x1

    aget-object v3, v3, v4

    invoke-direct {p0, v1, v3}, Lcom/samsung/musicplus/mediainfo/crypto/IceKey;->roundFunc(I[I)I

    move-result v3

    xor-int/2addr v2, v3

    .line 253
    add-int/lit8 v0, v0, 0x2

    goto :goto_1

    .line 258
    :cond_1
    const/4 v0, 0x0

    :goto_2
    if-ge v0, v5, :cond_2

    .line 259
    rsub-int/lit8 v3, v0, 0x3

    and-int/lit16 v4, v2, 0xff

    int-to-byte v4, v4

    aput-byte v4, p2, v3

    .line 260
    rsub-int/lit8 v3, v0, 0x7

    and-int/lit16 v4, v1, 0xff

    int-to-byte v4, v4

    aput-byte v4, p2, v3

    .line 262
    ushr-int/lit8 v2, v2, 0x8

    .line 263
    ushr-int/lit8 v1, v1, 0x8

    .line 258
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 265
    :cond_2
    return-void
.end method

.method public keySize()I
    .locals 1

    .prologue
    .line 293
    iget v0, p0, Lcom/samsung/musicplus/mediainfo/crypto/IceKey;->size:I

    mul-int/lit8 v0, v0, 0x8

    return v0
.end method

.method public set([B)V
    .locals 10
    .param p1, "key"    # [B

    .prologue
    const/16 v9, 0x8

    const/4 v8, 0x4

    const/4 v7, 0x0

    .line 189
    new-array v2, v8, [I

    .line 191
    .local v2, "kb":[I
    iget v3, p0, Lcom/samsung/musicplus/mediainfo/crypto/IceKey;->rounds:I

    if-ne v3, v9, :cond_2

    .line 192
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v8, :cond_0

    .line 193
    rsub-int/lit8 v3, v0, 0x3

    mul-int/lit8 v4, v0, 0x2

    aget-byte v4, p1, v4

    and-int/lit16 v4, v4, 0xff

    shl-int/lit8 v4, v4, 0x8

    mul-int/lit8 v5, v0, 0x2

    add-int/lit8 v5, v5, 0x1

    aget-byte v5, p1, v5

    and-int/lit16 v5, v5, 0xff

    or-int/2addr v4, v5

    aput v4, v2, v3

    .line 192
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 196
    :cond_0
    invoke-direct {p0, v2, v7, v7}, Lcom/samsung/musicplus/mediainfo/crypto/IceKey;->scheduleBuild([III)V

    .line 210
    :cond_1
    return-void

    .line 200
    .end local v0    # "i":I
    :cond_2
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_1
    iget v3, p0, Lcom/samsung/musicplus/mediainfo/crypto/IceKey;->size:I

    if-ge v0, v3, :cond_1

    .line 203
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_2
    if-ge v1, v8, :cond_3

    .line 204
    rsub-int/lit8 v3, v1, 0x3

    mul-int/lit8 v4, v0, 0x8

    mul-int/lit8 v5, v1, 0x2

    add-int/2addr v4, v5

    aget-byte v4, p1, v4

    and-int/lit16 v4, v4, 0xff

    shl-int/lit8 v4, v4, 0x8

    mul-int/lit8 v5, v0, 0x8

    mul-int/lit8 v6, v1, 0x2

    add-int/2addr v5, v6

    add-int/lit8 v5, v5, 0x1

    aget-byte v5, p1, v5

    and-int/lit16 v5, v5, 0xff

    or-int/2addr v4, v5

    aput v4, v2, v3

    .line 203
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 207
    :cond_3
    mul-int/lit8 v3, v0, 0x8

    invoke-direct {p0, v2, v3, v7}, Lcom/samsung/musicplus/mediainfo/crypto/IceKey;->scheduleBuild([III)V

    .line 208
    iget v3, p0, Lcom/samsung/musicplus/mediainfo/crypto/IceKey;->rounds:I

    add-int/lit8 v3, v3, -0x8

    mul-int/lit8 v4, v0, 0x8

    sub-int/2addr v3, v4

    invoke-direct {p0, v2, v3, v9}, Lcom/samsung/musicplus/mediainfo/crypto/IceKey;->scheduleBuild([III)V

    .line 200
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method
