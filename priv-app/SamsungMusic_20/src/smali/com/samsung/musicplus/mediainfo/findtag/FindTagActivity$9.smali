.class Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$9;
.super Landroid/os/Handler;
.source "FindTagActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;)V
    .locals 0

    .prologue
    .line 433
    iput-object p1, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$9;->this$0:Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x1

    .line 437
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 441
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v5, "URL"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 444
    .local v4, "url":Ljava/lang/String;
    const-string v5, "http://ums.samsungmobile.com/http_client/find_url.jsp"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 447
    const-string v5, "RESPONSE"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 450
    .local v3, "ums":Ljava/lang/String;
    const-string v5, "FAIL"

    invoke-virtual {v3, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 451
    const/4 v5, 0x0

    sput-boolean v5, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoUtils;->sHasShop:Z

    .line 454
    :cond_0
    const-string v5, "SUCCESS"

    invoke-virtual {v3, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 455
    sput-boolean v6, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoUtils;->sHasShop:Z

    .line 456
    const-string v5, "OUTPUT="

    invoke-virtual {v3, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    add-int/lit8 v5, v5, 0x7

    invoke-virtual {v3, v5}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v5

    sput-object v5, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoUtils;->sShopUrl:Ljava/lang/String;

    .line 458
    :cond_1
    iget-object v5, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$9;->this$0:Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;

    # getter for: Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->mRichInfoResponseHandler:Landroid/os/Handler;
    invoke-static {v5}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->access$1100(Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;)Landroid/os/Handler;

    move-result-object v5

    invoke-virtual {v5, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 493
    .end local v3    # "ums":Ljava/lang/String;
    :cond_2
    :goto_0
    return-void

    .line 461
    :cond_3
    iget-object v5, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$9;->this$0:Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;

    const-string v6, "RESPONSE"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/musicplus/mediainfo/findtag/NetworkResponseParser;->parseResponse(Ljava/lang/String;)Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;

    move-result-object v6

    # setter for: Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->mNetworkData:Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;
    invoke-static {v5, v6}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->access$1302(Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;)Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;

    .line 465
    const-string v5, "http://ssung.meta.shazamid.com/orbit/RequestConfig1"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_4

    iget-object v5, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$9;->this$0:Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;

    # getter for: Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->mNetworkData:Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;
    invoke-static {v5}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->access$1300(Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;)Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;

    move-result-object v5

    iget-boolean v5, v5, Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;->isError:Z

    if-eqz v5, :cond_4

    .line 467
    iget-object v5, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$9;->this$0:Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;

    # getter for: Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->mRichInfoResponseHandler:Landroid/os/Handler;
    invoke-static {v5}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->access$1100(Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;)Landroid/os/Handler;

    move-result-object v5

    invoke-virtual {v5, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 471
    :cond_4
    const-string v5, "http://ssung.meta.shazamid.com/orbit/RequestConfig1"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 472
    iget-object v5, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$9;->this$0:Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;

    # getter for: Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->mNetworkData:Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;
    invoke-static {v5}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->access$1300(Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;)Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;

    move-result-object v5

    iget-boolean v5, v5, Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;->isError:Z

    if-eqz v5, :cond_5

    .line 473
    iget-object v5, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$9;->this$0:Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;

    # getter for: Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->mRichInfoResponseHandler:Landroid/os/Handler;
    invoke-static {v5}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->access$1100(Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;)Landroid/os/Handler;

    move-result-object v5

    invoke-virtual {v5, v7}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 477
    :goto_1
    iget-object v5, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$9;->this$0:Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;

    # getter for: Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->mRichInfoResponseHandler:Landroid/os/Handler;
    invoke-static {v5}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->access$1100(Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;)Landroid/os/Handler;

    move-result-object v5

    const/4 v6, 0x6

    invoke-virtual {v5, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 475
    :cond_5
    iget-object v6, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$9;->this$0:Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;

    iget-object v5, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$9;->this$0:Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;

    # getter for: Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->mNetworkData:Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;
    invoke-static {v5}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->access$1300(Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;)Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;

    move-result-object v5

    iget-object v5, v5, Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;->requestConfig:Ljava/util/HashMap;

    const-string v7, "service"

    invoke-virtual {v5, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    # setter for: Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->mServiceName:Ljava/lang/String;
    invoke-static {v6, v5}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->access$1402(Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_1

    .line 478
    :cond_6
    const-string v5, "http://ssung.meta.shazamid.com/orbit/DoRecognition1"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 481
    new-instance v2, Landroid/os/Message;

    invoke-direct {v2}, Landroid/os/Message;-><init>()V

    .line 482
    .local v2, "msgRequestResult":Landroid/os/Message;
    const/4 v5, 0x4

    iput v5, v2, Landroid/os/Message;->what:I

    .line 483
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 484
    .local v1, "data":Landroid/os/Bundle;
    const-string v5, "requestId"

    iget-object v6, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$9;->this$0:Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;

    # getter for: Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->mNetworkData:Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;
    invoke-static {v6}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->access$1300(Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;)Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;

    move-result-object v6

    iget-object v6, v6, Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;->requestId:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 485
    invoke-virtual {v2, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 486
    iget-object v5, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$9;->this$0:Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;

    # getter for: Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->mRichInfoResponseHandler:Landroid/os/Handler;
    invoke-static {v5}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->access$1100(Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;)Landroid/os/Handler;

    move-result-object v5

    invoke-virtual {v5, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 487
    .end local v1    # "data":Landroid/os/Bundle;
    .end local v2    # "msgRequestResult":Landroid/os/Message;
    :cond_7
    const-string v5, "http://ssung.meta.shazamid.com/orbit/RequestResults1"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 490
    iget-object v5, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$9;->this$0:Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;

    const-string v6, "RESPONSE"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/musicplus/mediainfo/findtag/NetworkResponseParser;->parseResponse(Ljava/lang/String;)Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;

    move-result-object v6

    # setter for: Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->mNetworkData:Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;
    invoke-static {v5, v6}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->access$1302(Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;)Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;

    .line 491
    iget-object v5, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$9;->this$0:Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;

    # getter for: Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->mRichInfoResponseHandler:Landroid/os/Handler;
    invoke-static {v5}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->access$1100(Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;)Landroid/os/Handler;

    move-result-object v5

    const/4 v6, 0x5

    invoke-virtual {v5, v6}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto/16 :goto_0
.end method
