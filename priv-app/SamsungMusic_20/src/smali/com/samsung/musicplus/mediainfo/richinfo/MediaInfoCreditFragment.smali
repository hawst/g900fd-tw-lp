.class public Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoCreditFragment;
.super Landroid/app/ListFragment;
.source "MediaInfoCreditFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoCreditFragment$CreditListAdapter;
    }
.end annotation


# instance fields
.field private mCreditAdapter:Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoCreditFragment$CreditListAdapter;

.field private mParsedData:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Landroid/app/ListFragment;-><init>()V

    .line 58
    return-void
.end method

.method private setContents()V
    .locals 4

    .prologue
    .line 49
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoCreditFragment;->mParsedData:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;

    if-nez v0, :cond_0

    .line 56
    :goto_0
    return-void

    .line 53
    :cond_0
    new-instance v0, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoCreditFragment$CreditListAdapter;

    invoke-virtual {p0}, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoCreditFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const v2, 0x7f04004d

    iget-object v3, p0, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoCreditFragment;->mParsedData:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;

    iget-object v3, v3, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;->albumDetailInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;

    iget-object v3, v3, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;->credits:Ljava/util/ArrayList;

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoCreditFragment$CreditListAdapter;-><init>(Landroid/content/Context;ILjava/util/ArrayList;)V

    iput-object v0, p0, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoCreditFragment;->mCreditAdapter:Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoCreditFragment$CreditListAdapter;

    .line 55
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoCreditFragment;->mCreditAdapter:Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoCreditFragment$CreditListAdapter;

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoCreditFragment;->setListAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_0
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 43
    invoke-super {p0, p1}, Landroid/app/ListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 44
    invoke-direct {p0}, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoCreditFragment;->setContents()V

    .line 45
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 28
    invoke-super {p0, p1}, Landroid/app/ListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 29
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoCreditFragment;->setRetainInstance(Z)V

    .line 30
    invoke-virtual {p0}, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoCreditFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 31
    .local v0, "a":Landroid/app/Activity;
    instance-of v1, v0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;

    if-eqz v1, :cond_0

    .line 32
    check-cast v0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;

    .end local v0    # "a":Landroid/app/Activity;
    invoke-virtual {v0}, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->getRichInfoData()Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoCreditFragment;->mParsedData:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;

    .line 34
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 38
    const v0, 0x7f04004a

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
