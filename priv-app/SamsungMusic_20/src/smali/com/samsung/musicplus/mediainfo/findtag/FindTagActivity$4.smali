.class Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$4;
.super Ljava/lang/Object;
.source "FindTagActivity.java"

# interfaces
.implements Lcom/samsung/musicplus/library/audio/FindTag$OnFindTagResultListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;)V
    .locals 0

    .prologue
    .line 240
    iput-object p1, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$4;->this$0:Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResult([B)V
    .locals 3
    .param p1, "result"    # [B

    .prologue
    .line 245
    invoke-static {}, Landroid/os/Looper;->prepare()V

    .line 247
    new-instance v1, Landroid/os/Message;

    invoke-direct {v1}, Landroid/os/Message;-><init>()V

    .line 248
    .local v1, "message":Landroid/os/Message;
    const/4 v2, 0x2

    iput v2, v1, Landroid/os/Message;->what:I

    .line 249
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 250
    .local v0, "data":Landroid/os/Bundle;
    const-string v2, "sigdata"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 251
    invoke-virtual {v1, v0}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 252
    iget-object v2, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$4;->this$0:Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;

    # getter for: Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->mRichInfoResponseHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->access$1100(Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 254
    invoke-static {}, Landroid/os/Looper;->loop()V

    .line 255
    return-void
.end method
