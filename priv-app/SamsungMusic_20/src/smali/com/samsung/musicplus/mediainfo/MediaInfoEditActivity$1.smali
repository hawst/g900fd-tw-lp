.class Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity$1;
.super Ljava/lang/Object;
.source "MediaInfoEditActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->setCustomCancelDone()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;)V
    .locals 0

    .prologue
    .line 144
    iput-object p1, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity$1;->this$0:Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 148
    iget-object v2, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity$1;->this$0:Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;

    invoke-virtual {v2}, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity$1;->this$0:Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;

    # getter for: Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mBaseUri:Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->access$000(Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/musicplus/util/UiUtils;->getFilePathFromContentUri(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 150
    .local v1, "filePath":Ljava/lang/String;
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 151
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 152
    iget-object v2, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity$1;->this$0:Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;

    # invokes: Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->saveMetaData()V
    invoke-static {v2}, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->access$100(Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;)V

    .line 158
    :goto_0
    iget-object v2, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity$1;->this$0:Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;

    invoke-virtual {v2}, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->finish()V

    .line 159
    return-void

    .line 154
    :cond_0
    iget-object v2, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity$1;->this$0:Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;

    invoke-virtual {v2}, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f1001b4

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
