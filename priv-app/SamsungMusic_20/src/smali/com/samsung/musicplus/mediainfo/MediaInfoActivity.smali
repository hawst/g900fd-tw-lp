.class public Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;
.super Lcom/samsung/musicplus/MusicBaseActivity;
.source "MediaInfoActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/mediainfo/MediaInfoActivity$MediaInfoPagerAdapter;
    }
.end annotation


# static fields
.field private static final MAX_COUNT:I = 0x6

.field private static final MENU_EDIT:I = 0x7f0d01ea

.field private static final MENU_SEARCH:I = 0x7f0d01cb

.field private static final MENU_TAGS:I = 0x7f0d01eb

.field private static final RESULT_FIND_TAG:I = 0x1

.field private static final RESULT_META_DATA_EDIT:I = 0x0

.field private static final SAVED_INSTANCE_STATE_CURRENT_ITEM:Ljava/lang/String; = "saved_instance_state_current_item"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mAdapter:Lcom/samsung/musicplus/mediainfo/MediaInfoActivity$MediaInfoPagerAdapter;

.field private mArrowLeft:Landroid/widget/ImageView;

.field private mArrowRight:Landroid/widget/ImageView;

.field private mBaseUri:Ljava/lang/String;

.field private mCurrentItem:I

.field private mHeaderTitle:Landroid/widget/TextView;

.field private mListType:I

.field private mMetaData:Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;

.field private final mMetaDataObserver:Landroid/database/ContentObserver;

.field private mPager:Landroid/support/v4/view/ViewPager;

.field private mParsedData:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54
    const-class v0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/samsung/musicplus/MusicBaseActivity;-><init>()V

    .line 88
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mCurrentItem:I

    .line 124
    new-instance v0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity$1;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity$1;-><init>(Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mMetaDataObserver:Landroid/database/ContentObserver;

    .line 448
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;)Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mMetaData:Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;

    return-object v0
.end method

.method static synthetic access$002(Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;)Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;
    .param p1, "x1"    # Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;

    .prologue
    .line 52
    iput-object p1, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mMetaData:Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;

    return-object p1
.end method

.method static synthetic access$100(Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mBaseUri:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;)Lcom/samsung/musicplus/mediainfo/MediaInfoActivity$MediaInfoPagerAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mAdapter:Lcom/samsung/musicplus/mediainfo/MediaInfoActivity$MediaInfoPagerAdapter;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;
    .param p1, "x1"    # I

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->movePage(I)V

    return-void
.end method

.method static synthetic access$500(Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;

    .prologue
    .line 52
    iget v0, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mListType:I

    return v0
.end method

.method static synthetic access$600(Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mHeaderTitle:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$700(Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mArrowLeft:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$800(Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mArrowRight:Landroid/widget/ImageView;

    return-object v0
.end method

.method private hasRichInfoData()Z
    .locals 1

    .prologue
    .line 309
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mMetaData:Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;

    iget-object v0, v0, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;->data:Ljava/lang/String;

    invoke-static {v0}, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoUtils;->parsingRichInfoData(Ljava/lang/String;)Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mParsedData:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;

    .line 310
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mParsedData:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private hideHeaderAndLogo()V
    .locals 4

    .prologue
    const/16 v3, 0x8

    .line 387
    const v2, 0x7f0d00e7

    invoke-virtual {p0, v2}, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 388
    .local v0, "headerView":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 389
    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 392
    :cond_0
    const v2, 0x7f0d00d2

    invoke-virtual {p0, v2}, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 393
    .local v1, "logoView":Landroid/view/View;
    if-eqz v1, :cond_1

    .line 394
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 396
    :cond_1
    return-void
.end method

.method private isHasId3Tag()Z
    .locals 8

    .prologue
    .line 248
    const/4 v1, 0x0

    .line 249
    .local v1, "ishasId3Tag":Z
    const/4 v2, 0x0

    .line 250
    .local v2, "retriever":Lcom/samsung/musicplus/library/metadata/SecMediaMetaDataRetriever;
    iget-object v3, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mMetaData:Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mMetaData:Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;

    iget-wide v4, v3, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;->size:J

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-lez v3, :cond_0

    .line 251
    invoke-virtual {p0}, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/musicplus/library/drm/DrmManager;->getInstance(Landroid/content/Context;)Lcom/samsung/musicplus/library/drm/DrmManager;

    move-result-object v0

    .line 252
    .local v0, "dm":Lcom/samsung/musicplus/library/drm/DrmManager;
    iget-object v3, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mMetaData:Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;

    iget-object v3, v3, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;->data:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/samsung/musicplus/library/drm/DrmManager;->isDrm(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 253
    new-instance v2, Lcom/samsung/musicplus/library/metadata/SecMediaMetaDataRetriever;

    .end local v2    # "retriever":Lcom/samsung/musicplus/library/metadata/SecMediaMetaDataRetriever;
    iget-object v3, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mMetaData:Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;

    iget-object v3, v3, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;->data:Ljava/lang/String;

    invoke-direct {v2, v3}, Lcom/samsung/musicplus/library/metadata/SecMediaMetaDataRetriever;-><init>(Ljava/lang/String;)V

    .line 257
    .end local v0    # "dm":Lcom/samsung/musicplus/library/drm/DrmManager;
    .restart local v2    # "retriever":Lcom/samsung/musicplus/library/metadata/SecMediaMetaDataRetriever;
    :cond_0
    if-eqz v2, :cond_3

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lcom/samsung/musicplus/library/metadata/SecMediaMetaDataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_1

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/samsung/musicplus/library/metadata/SecMediaMetaDataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_1

    const/4 v3, 0x7

    invoke-virtual {v2, v3}, Lcom/samsung/musicplus/library/metadata/SecMediaMetaDataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_3

    .line 261
    :cond_1
    const/4 v1, 0x1

    .line 266
    :goto_0
    if-eqz v2, :cond_2

    .line 267
    invoke-virtual {v2}, Lcom/samsung/musicplus/library/metadata/SecMediaMetaDataRetriever;->release()V

    .line 270
    :cond_2
    return v1

    .line 263
    :cond_3
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private isRecording()Z
    .locals 2

    .prologue
    .line 274
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mMetaData:Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mMetaData:Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;

    iget v0, v0, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;->recordingType:I

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mMetaData:Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;

    iget-object v0, v0, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;->mimeType:Ljava/lang/String;

    sget-object v1, Lcom/samsung/musicplus/provider/MusicContents$Audio$Recordings;->MIME_TYPE_AMR:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private launchFindTag()V
    .locals 5

    .prologue
    .line 284
    const-string v3, "connectivity"

    invoke-virtual {p0, v3}, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 285
    .local v0, "cm":Landroid/net/ConnectivityManager;
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v2

    .line 287
    .local v2, "info":Landroid/net/NetworkInfo;
    if-nez v2, :cond_0

    .line 288
    invoke-virtual {p0}, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils;->showNetworkErrorToast(Landroid/content/Context;)V

    .line 294
    :goto_0
    return-void

    .line 290
    :cond_0
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-class v4, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;

    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 291
    .local v1, "i":Landroid/content/Intent;
    const-string v3, "path"

    iget-object v4, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mMetaData:Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;

    iget-object v4, v4, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;->data:Ljava/lang/String;

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 292
    const/4 v3, 0x1

    invoke-virtual {p0, v1, v3}, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method private launchMediaInfoEdit()V
    .locals 3

    .prologue
    .line 278
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 279
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "uri"

    iget-object v2, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mBaseUri:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 280
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 281
    return-void
.end method

.method private movePage(I)V
    .locals 4
    .param p1, "direction"    # I

    .prologue
    .line 405
    iget-object v2, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mAdapter:Lcom/samsung/musicplus/mediainfo/MediaInfoActivity$MediaInfoPagerAdapter;

    invoke-virtual {v2}, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity$MediaInfoPagerAdapter;->getCount()I

    move-result v0

    .line 406
    .local v0, "count":I
    iget-object v2, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    .line 407
    .local v1, "position":I
    if-gez p1, :cond_1

    if-lez v1, :cond_1

    .line 409
    iget-object v2, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mPager:Landroid/support/v4/view/ViewPager;

    add-int/lit8 v3, v1, -0x1

    invoke-virtual {v2, v3}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 414
    :cond_0
    :goto_0
    return-void

    .line 410
    :cond_1
    if-lez p1, :cond_0

    add-int/lit8 v2, v0, -0x1

    if-ge v1, v2, :cond_0

    .line 412
    iget-object v2, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mPager:Landroid/support/v4/view/ViewPager;

    add-int/lit8 v3, v1, 0x1

    invoke-virtual {v2, v3}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    goto :goto_0
.end method

.method private showHeaderAndLogo()V
    .locals 6

    .prologue
    .line 350
    const v3, 0x7f0d00ed

    invoke-virtual {p0, v3}, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 351
    .local v1, "headerViewStub":Landroid/view/View;
    instance-of v3, v1, Landroid/view/ViewStub;

    if-eqz v3, :cond_0

    .line 352
    check-cast v1, Landroid/view/ViewStub;

    .end local v1    # "headerViewStub":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 353
    const v3, 0x7f0d00e9

    invoke-virtual {p0, v3}, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mHeaderTitle:Landroid/widget/TextView;

    .line 354
    iget-object v3, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mHeaderTitle:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mAdapter:Lcom/samsung/musicplus/mediainfo/MediaInfoActivity$MediaInfoPagerAdapter;

    iget-object v5, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v5}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity$MediaInfoPagerAdapter;->getCurrentTitle(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 355
    const v3, 0x7f0d00e8

    invoke-virtual {p0, v3}, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mArrowLeft:Landroid/widget/ImageView;

    .line 356
    const v3, 0x7f0d00ea

    invoke-virtual {p0, v3}, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mArrowRight:Landroid/widget/ImageView;

    .line 358
    new-instance v0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity$2;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity$2;-><init>(Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;)V

    .line 374
    .local v0, "arrowListener":Landroid/view/View$OnClickListener;
    iget-object v3, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mArrowLeft:Landroid/widget/ImageView;

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 375
    iget-object v3, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mArrowRight:Landroid/widget/ImageView;

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 376
    iget-object v4, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mArrowLeft:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v3}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v3

    if-lez v3, :cond_2

    const/4 v3, 0x1

    :goto_0
    invoke-virtual {v4, v3}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 380
    .end local v0    # "arrowListener":Landroid/view/View$OnClickListener;
    :cond_0
    const v3, 0x7f0d00ef

    invoke-virtual {p0, v3}, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 381
    .local v2, "logoViewStub":Landroid/view/View;
    instance-of v3, v2, Landroid/view/ViewStub;

    if-eqz v3, :cond_1

    .line 382
    check-cast v2, Landroid/view/ViewStub;

    .end local v2    # "logoViewStub":Landroid/view/View;
    invoke-virtual {v2}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 384
    :cond_1
    return-void

    .line 376
    .restart local v0    # "arrowListener":Landroid/view/View$OnClickListener;
    :cond_2
    const/4 v3, 0x0

    goto :goto_0
.end method

.method private showRichInfoData()V
    .locals 1

    .prologue
    .line 301
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mMetaData:Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->hasRichInfoData()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 302
    invoke-direct {p0}, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->showHeaderAndLogo()V

    .line 306
    :goto_0
    return-void

    .line 304
    :cond_0
    invoke-direct {p0}, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->hideHeaderAndLogo()V

    goto :goto_0
.end method

.method private updateTitle()V
    .locals 3

    .prologue
    .line 346
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mHeaderTitle:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mAdapter:Lcom/samsung/musicplus/mediainfo/MediaInfoActivity$MediaInfoPagerAdapter;

    iget-object v2, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity$MediaInfoPagerAdapter;->getCurrentTitle(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 347
    return-void
.end method


# virtual methods
.method public attachRichInfoFragments()V
    .locals 5

    .prologue
    .line 314
    iget-object v2, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mParsedData:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mAdapter:Lcom/samsung/musicplus/mediainfo/MediaInfoActivity$MediaInfoPagerAdapter;

    invoke-virtual {v2}, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity$MediaInfoPagerAdapter;->getCount()I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_1

    .line 343
    :cond_0
    :goto_0
    return-void

    .line 318
    :cond_1
    iget-object v2, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mParsedData:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;

    iget-object v0, v2, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;->albumDetailInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;

    .line 319
    .local v0, "albumInfo":Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;
    iget-object v2, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mParsedData:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;

    iget-object v1, v2, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;->performerDetailInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$PerformerDetailInfo;

    .line 321
    .local v1, "performerInfo":Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$PerformerDetailInfo;
    iget-object v2, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;->discs:Ljava/util/ArrayList;

    if-eqz v2, :cond_2

    iget-object v2, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;->discs:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-gtz v2, :cond_3

    :cond_2
    iget-object v2, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;->similarAlbums:Ljava/util/ArrayList;

    if-eqz v2, :cond_4

    iget-object v2, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;->similarAlbums:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_4

    .line 323
    :cond_3
    iget-object v2, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mAdapter:Lcom/samsung/musicplus/mediainfo/MediaInfoActivity$MediaInfoPagerAdapter;

    new-instance v3, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoTrackListFragment;

    invoke-direct {v3}, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoTrackListFragment;-><init>()V

    const v4, 0x7f10017b

    invoke-virtual {v2, v3, v4}, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity$MediaInfoPagerAdapter;->addFragment(Landroid/app/Fragment;I)V

    .line 326
    :cond_4
    iget-object v2, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;->albumSimpleInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;

    if-eqz v2, :cond_5

    iget-object v2, v1, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$PerformerDetailInfo;->biography:Ljava/lang/String;

    if-eqz v2, :cond_5

    .line 327
    iget-object v2, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mAdapter:Lcom/samsung/musicplus/mediainfo/MediaInfoActivity$MediaInfoPagerAdapter;

    new-instance v3, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoBiographyFragment;

    invoke-direct {v3}, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoBiographyFragment;-><init>()V

    const v4, 0x7f100035

    invoke-virtual {v2, v3, v4}, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity$MediaInfoPagerAdapter;->addFragment(Landroid/app/Fragment;I)V

    .line 330
    :cond_5
    iget-object v2, v1, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$PerformerDetailInfo;->albums:Ljava/util/ArrayList;

    if-eqz v2, :cond_6

    iget-object v2, v1, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$PerformerDetailInfo;->albums:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-gtz v2, :cond_7

    :cond_6
    iget-object v2, v1, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$PerformerDetailInfo;->similarArtists:Ljava/util/ArrayList;

    if-eqz v2, :cond_8

    iget-object v2, v1, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$PerformerDetailInfo;->similarArtists:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_8

    .line 332
    :cond_7
    iget-object v2, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mAdapter:Lcom/samsung/musicplus/mediainfo/MediaInfoActivity$MediaInfoPagerAdapter;

    new-instance v3, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoDiscographyFragment;

    invoke-direct {v3}, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoDiscographyFragment;-><init>()V

    const v4, 0x7f100059

    invoke-virtual {v2, v3, v4}, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity$MediaInfoPagerAdapter;->addFragment(Landroid/app/Fragment;I)V

    .line 335
    :cond_8
    iget-object v2, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;->review:Ljava/lang/String;

    if-eqz v2, :cond_9

    .line 336
    iget-object v2, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mAdapter:Lcom/samsung/musicplus/mediainfo/MediaInfoActivity$MediaInfoPagerAdapter;

    new-instance v3, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoReviewFragment;

    invoke-direct {v3}, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoReviewFragment;-><init>()V

    const v4, 0x7f10013d

    invoke-virtual {v2, v3, v4}, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity$MediaInfoPagerAdapter;->addFragment(Landroid/app/Fragment;I)V

    .line 339
    :cond_9
    iget-object v2, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;->credits:Ljava/util/ArrayList;

    if-eqz v2, :cond_a

    iget-object v2, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;->credits:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_a

    .line 340
    iget-object v2, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mAdapter:Lcom/samsung/musicplus/mediainfo/MediaInfoActivity$MediaInfoPagerAdapter;

    new-instance v3, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoCreditFragment;

    invoke-direct {v3}, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoCreditFragment;-><init>()V

    const v4, 0x7f100048

    invoke-virtual {v2, v3, v4}, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity$MediaInfoPagerAdapter;->addFragment(Landroid/app/Fragment;I)V

    .line 342
    :cond_a
    iget-object v2, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mPager:Landroid/support/v4/view/ViewPager;

    iget v3, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mCurrentItem:I

    invoke-virtual {v2, v3}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    goto/16 :goto_0
.end method

.method public getRichInfoData()Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;
    .locals 1

    .prologue
    .line 445
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mParsedData:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;

    return-object v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 418
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 419
    packed-switch p2, :pswitch_data_0

    .line 441
    :cond_0
    :goto_0
    :pswitch_0
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/musicplus/MusicBaseActivity;->onActivityResult(IILandroid/content/Intent;)V

    .line 442
    return-void

    .line 423
    :pswitch_1
    invoke-virtual {p0}, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils;->showNetworkErrorToast(Landroid/content/Context;)V

    goto :goto_0

    .line 426
    :pswitch_2
    invoke-virtual {p0}, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f1001b4

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 430
    :pswitch_3
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mPager:Landroid/support/v4/view/ViewPager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 431
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mAdapter:Lcom/samsung/musicplus/mediainfo/MediaInfoActivity$MediaInfoPagerAdapter;

    invoke-virtual {v0}, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity$MediaInfoPagerAdapter;->clearFragments()V

    .line 432
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mPager:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mAdapter:Lcom/samsung/musicplus/mediainfo/MediaInfoActivity$MediaInfoPagerAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 433
    invoke-direct {p0}, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->showRichInfoData()V

    .line 434
    invoke-direct {p0}, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->updateTitle()V

    goto :goto_0

    .line 419
    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v2, 0x20002

    .line 92
    invoke-super {p0, p1}, Lcom/samsung/musicplus/MusicBaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 93
    const v1, 0x7f04004e

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->setContentView(I)V

    .line 95
    if-nez p1, :cond_3

    .line 96
    invoke-virtual {p0}, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 97
    .local v0, "intent":Landroid/content/Intent;
    if-eqz v0, :cond_0

    .line 98
    const-string v1, "uri"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mBaseUri:Ljava/lang/String;

    .line 99
    const-string v1, "list_type"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mListType:I

    .line 106
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mBaseUri:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 107
    invoke-virtual {p0}, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mBaseUri:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils;->getMetaData(Landroid/content/Context;Landroid/net/Uri;)Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mMetaData:Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;

    .line 110
    :cond_1
    new-instance v1, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity$MediaInfoPagerAdapter;

    iget v2, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mListType:I

    invoke-direct {v1, p0, p0, v2}, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity$MediaInfoPagerAdapter;-><init>(Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;Landroid/app/Activity;I)V

    iput-object v1, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mAdapter:Lcom/samsung/musicplus/mediainfo/MediaInfoActivity$MediaInfoPagerAdapter;

    .line 111
    const v1, 0x7f0d00ee

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v4/view/ViewPager;

    iput-object v1, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mPager:Landroid/support/v4/view/ViewPager;

    .line 112
    iget-object v1, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mPager:Landroid/support/v4/view/ViewPager;

    iget-object v2, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mAdapter:Lcom/samsung/musicplus/mediainfo/MediaInfoActivity$MediaInfoPagerAdapter;

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 113
    iget-object v1, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mPager:Landroid/support/v4/view/ViewPager;

    iget-object v2, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mAdapter:Lcom/samsung/musicplus/mediainfo/MediaInfoActivity$MediaInfoPagerAdapter;

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 114
    iget-object v1, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mPager:Landroid/support/v4/view/ViewPager;

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setOffscreenPageLimit(I)V

    .line 116
    invoke-direct {p0}, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->showRichInfoData()V

    .line 118
    iget-object v1, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mBaseUri:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 119
    invoke-virtual {p0}, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mBaseUri:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mMetaDataObserver:Landroid/database/ContentObserver;

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 122
    :cond_2
    return-void

    .line 102
    :cond_3
    const-string v1, "uri"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mBaseUri:Ljava/lang/String;

    .line 103
    const-string v1, "list_type"

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mListType:I

    .line 104
    const-string v1, "saved_instance_state_current_item"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mCurrentItem:I

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 173
    invoke-virtual {p0}, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f120007

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 174
    invoke-super {p0, p1}, Lcom/samsung/musicplus/MusicBaseActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 231
    invoke-virtual {p0}, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mMetaDataObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 232
    invoke-super {p0}, Lcom/samsung/musicplus/MusicBaseActivity;->onDestroy()V

    .line 234
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 238
    packed-switch p1, :pswitch_data_0

    .line 243
    invoke-super {p0, p1, p2}, Lcom/samsung/musicplus/MusicBaseActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    return v0

    .line 240
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mMetaData:Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;

    invoke-static {p0, v0}, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils;->searchMediaInfo(Landroid/app/Activity;Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;)V

    .line 241
    const/4 v0, 0x0

    goto :goto_0

    .line 238
    nop

    :pswitch_data_0
    .packed-switch 0x54
        :pswitch_0
    .end packed-switch
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 206
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 226
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Lcom/samsung/musicplus/MusicBaseActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v2

    return v2

    .line 208
    :sswitch_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mBaseUri:Ljava/lang/String;

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/musicplus/util/UiUtils;->getFilePathFromContentUri(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 210
    .local v1, "filePath":Ljava/lang/String;
    if-eqz v1, :cond_0

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 211
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 212
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 213
    invoke-direct {p0}, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->launchMediaInfoEdit()V

    goto :goto_0

    .line 218
    .end local v0    # "file":Ljava/io/File;
    .end local v1    # "filePath":Ljava/lang/String;
    :sswitch_1
    invoke-direct {p0}, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->launchFindTag()V

    goto :goto_0

    .line 221
    :sswitch_2
    iget-object v2, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mMetaData:Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;

    invoke-static {p0, v2}, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils;->searchMediaInfo(Landroid/app/Activity;Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;)V

    goto :goto_0

    .line 206
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0d01cb -> :sswitch_2
        0x7f0d01ea -> :sswitch_0
        0x7f0d01eb -> :sswitch_1
    .end sparse-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 7
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const v6, 0x7f0d01cb

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 179
    sget-boolean v2, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_ENABLE_EDIT_META:Z

    if-eqz v2, :cond_0

    .line 180
    const v2, 0x7f0d01ea

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 181
    .local v0, "editmeta":Landroid/view/MenuItem;
    iget-object v2, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mMetaData:Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;

    if-eqz v2, :cond_3

    const-string v2, "audio/mpeg"

    iget-object v3, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mMetaData:Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;

    iget-object v3, v3, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;->mimeType:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-direct {p0}, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->isHasId3Tag()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 182
    invoke-interface {v0, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 188
    .end local v0    # "editmeta":Landroid/view/MenuItem;
    :cond_0
    :goto_0
    sget-boolean v2, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_ENABLE_FIND_TAG:Z

    if-eqz v2, :cond_1

    .line 189
    const v2, 0x7f0d01eb

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 190
    .local v1, "tags":Landroid/view/MenuItem;
    invoke-interface {v1, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 191
    invoke-interface {v1, v4}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 192
    iget-object v2, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mMetaData:Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mMetaData:Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;

    iget v2, v2, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;->samplingRate:I

    iget-object v3, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mMetaData:Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;

    iget v3, v3, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;->bitDepth:I

    invoke-static {v2, v3}, Lcom/samsung/musicplus/util/UiUtils;->isUhqa(II)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 193
    invoke-interface {v1, v5}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 196
    .end local v1    # "tags":Landroid/view/MenuItem;
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/musicplus/util/KnoxGateManager;->isKnoxModeOn(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-direct {p0}, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->isRecording()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 197
    :cond_2
    invoke-interface {p1, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 201
    :goto_1
    invoke-super {p0, p1}, Lcom/samsung/musicplus/MusicBaseActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v2

    return v2

    .line 184
    .restart local v0    # "editmeta":Landroid/view/MenuItem;
    :cond_3
    invoke-interface {v0, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 199
    .end local v0    # "editmeta":Landroid/view/MenuItem;
    :cond_4
    invoke-interface {p1, v6}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    invoke-interface {v2, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_1
.end method

.method protected onResume()V
    .locals 4

    .prologue
    .line 146
    invoke-super {p0}, Lcom/samsung/musicplus/MusicBaseActivity;->onResume()V

    .line 147
    const/4 v1, 0x0

    .line 148
    .local v1, "filePath":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mBaseUri:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 149
    invoke-virtual {p0}, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mBaseUri:Ljava/lang/String;

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/musicplus/util/UiUtils;->getFilePathFromContentUri(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 152
    :cond_0
    if-eqz v1, :cond_2

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 153
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 154
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_1

    .line 155
    invoke-virtual {p0}, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->finish()V

    .line 161
    .end local v0    # "file":Ljava/io/File;
    :cond_1
    :goto_0
    return-void

    .line 158
    :cond_2
    sget-object v2, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->TAG:Ljava/lang/String;

    const-string v3, "onResume : filePath is null! finish() will be called."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    invoke-virtual {p0}, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->finish()V

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 165
    const-string v0, "uri"

    iget-object v1, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mBaseUri:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    const-string v0, "list_type"

    iget v1, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mListType:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 167
    const-string v0, "saved_instance_state_current_item"

    iget-object v1, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 168
    invoke-super {p0, p1}, Lcom/samsung/musicplus/MusicBaseActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 169
    return-void
.end method
