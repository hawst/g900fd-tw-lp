.class public Lcom/samsung/musicplus/mediainfo/MediaInfoDefaultFragment;
.super Landroid/app/Fragment;
.source "MediaInfoDefaultFragment.java"


# instance fields
.field private mMetaData:Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    return-void
.end method

.method public static newInstance(I)Lcom/samsung/musicplus/mediainfo/MediaInfoDefaultFragment;
    .locals 3
    .param p0, "listType"    # I

    .prologue
    .line 25
    new-instance v1, Lcom/samsung/musicplus/mediainfo/MediaInfoDefaultFragment;

    invoke-direct {v1}, Lcom/samsung/musicplus/mediainfo/MediaInfoDefaultFragment;-><init>()V

    .line 26
    .local v1, "f":Lcom/samsung/musicplus/mediainfo/MediaInfoDefaultFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 27
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "list_type"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 28
    invoke-virtual {v1, v0}, Lcom/samsung/musicplus/mediainfo/MediaInfoDefaultFragment;->setArguments(Landroid/os/Bundle;)V

    .line 29
    return-object v1
.end method

.method private setItemText(IILjava/lang/String;)V
    .locals 1
    .param p1, "viewId"    # I
    .param p2, "str"    # I
    .param p3, "text"    # Ljava/lang/String;

    .prologue
    .line 104
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/samsung/musicplus/mediainfo/MediaInfoDefaultFragment;->setItemText(IILjava/lang/String;Ljava/lang/String;)V

    .line 105
    return-void
.end method

.method private setItemText(IILjava/lang/String;Ljava/lang/String;)V
    .locals 9
    .param p1, "viewId"    # I
    .param p2, "str"    # I
    .param p3, "text"    # Ljava/lang/String;
    .param p4, "description"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 108
    invoke-virtual {p0}, Lcom/samsung/musicplus/mediainfo/MediaInfoDefaultFragment;->getView()Landroid/view/View;

    move-result-object v2

    .line 109
    .local v2, "root":Landroid/view/View;
    if-nez v2, :cond_1

    .line 144
    :cond_0
    :goto_0
    return-void

    .line 112
    :cond_1
    invoke-virtual {v2, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 113
    .local v1, "frame":Landroid/view/View;
    invoke-virtual {p0}, Lcom/samsung/musicplus/mediainfo/MediaInfoDefaultFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/musicplus/util/UiUtils;->isTalkBackEnabled(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 114
    invoke-virtual {v1, v7}, Landroid/view/View;->setFocusable(Z)V

    .line 119
    :goto_1
    if-nez p3, :cond_3

    .line 120
    const/16 v4, 0x8

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 116
    :cond_2
    invoke-virtual {v1, v8}, Landroid/view/View;->setFocusable(Z)V

    goto :goto_1

    .line 122
    :cond_3
    const v4, 0x7f0d00e1

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 123
    .local v3, "title":Landroid/widget/TextView;
    const v4, 0x7f0d00e2

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 125
    .local v0, "content":Landroid/widget/TextView;
    const v4, 0x7f0d00da

    if-eq p1, v4, :cond_4

    const v4, 0x7f0d00db

    if-ne p1, v4, :cond_5

    .line 126
    :cond_4
    const-string v4, "%d"

    new-array v5, v7, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p3

    .line 128
    :cond_5
    invoke-virtual {p0}, Lcom/samsung/musicplus/mediainfo/MediaInfoDefaultFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4, p2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 129
    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 131
    if-eqz p4, :cond_6

    .line 132
    invoke-virtual {v0, p4}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 139
    :cond_6
    const v4, 0x7f0d00e0

    if-eq p1, v4, :cond_7

    const v4, 0x7f0d00d6

    if-eq p1, v4, :cond_7

    const v4, 0x7f0d00d5

    if-eq p1, v4, :cond_7

    const v4, 0x7f0d00d7

    if-ne p1, v4, :cond_0

    .line 141
    :cond_7
    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setSelected(Z)V

    goto :goto_0
.end method

.method private setMetaDataInfo(Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;)V
    .locals 9
    .param p1, "metaData"    # Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;

    .prologue
    .line 52
    if-nez p1, :cond_0

    .line 91
    :goto_0
    return-void

    .line 55
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/mediainfo/MediaInfoDefaultFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 58
    .local v2, "context":Landroid/content/Context;
    invoke-virtual {p0}, Lcom/samsung/musicplus/mediainfo/MediaInfoDefaultFragment;->getView()Landroid/view/View;

    move-result-object v5

    const v6, 0x7f0d00d4

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 59
    .local v1, "albumView":Landroid/widget/ImageView;
    iget-wide v6, p1, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;->albumId:J

    invoke-static {v2, v6, v7}, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils;->getAlbumArt(Landroid/content/Context;J)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 60
    .local v0, "albumArt":Landroid/graphics/Bitmap;
    if-nez v0, :cond_1

    .line 61
    const v5, 0x7f02003a

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 67
    :goto_1
    invoke-virtual {p0, p1, v2}, Lcom/samsung/musicplus/mediainfo/MediaInfoDefaultFragment;->setBasicMetaInfo(Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;Landroid/content/Context;)V

    .line 69
    iget-wide v6, p1, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;->duration:J

    invoke-static {v2, v6, v7}, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils;->getTrackLength(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v4

    .line 70
    .local v4, "trackLength":Ljava/lang/String;
    const v5, 0x7f0d00d8

    const v6, 0x7f10017a

    invoke-static {v2, v4}, Lcom/samsung/musicplus/util/UiUtils;->getDurationTalkback(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v5, v6, v4, v7}, Lcom/samsung/musicplus/mediainfo/MediaInfoDefaultFragment;->setItemText(IILjava/lang/String;Ljava/lang/String;)V

    .line 73
    const v5, 0x7f0d00d9

    const v6, 0x7f100090

    iget-object v7, p1, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;->genre:Ljava/lang/String;

    invoke-static {v2, v7}, Lcom/samsung/musicplus/util/UiUtils;->transUnknownString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v5, v6, v7}, Lcom/samsung/musicplus/mediainfo/MediaInfoDefaultFragment;->setItemText(IILjava/lang/String;)V

    .line 75
    const v5, 0x7f0d00da

    const v6, 0x7f10012d

    iget-object v7, p1, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;->year:Ljava/lang/String;

    invoke-direct {p0, v5, v6, v7}, Lcom/samsung/musicplus/mediainfo/MediaInfoDefaultFragment;->setItemText(IILjava/lang/String;)V

    .line 77
    const v5, 0x7f0d00db

    const v6, 0x7f10017c

    iget-object v7, p1, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;->track:Ljava/lang/String;

    invoke-direct {p0, v5, v6, v7}, Lcom/samsung/musicplus/mediainfo/MediaInfoDefaultFragment;->setItemText(IILjava/lang/String;)V

    .line 79
    const v5, 0x7f0d00dc

    const v6, 0x7f10008f

    iget-object v7, p1, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;->mimeType:Ljava/lang/String;

    invoke-direct {p0, v5, v6, v7}, Lcom/samsung/musicplus/mediainfo/MediaInfoDefaultFragment;->setItemText(IILjava/lang/String;)V

    .line 81
    const v5, 0x7f0d00dd

    const v6, 0x7f100036

    iget v7, p1, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;->bitDepth:I

    invoke-static {v2, v7}, Lcom/samsung/musicplus/util/UiUtils;->getBitDepth(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v5, v6, v7}, Lcom/samsung/musicplus/mediainfo/MediaInfoDefaultFragment;->setItemText(IILjava/lang/String;)V

    .line 84
    const v5, 0x7f0d00de

    const v6, 0x7f100145

    iget v7, p1, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;->samplingRate:I

    invoke-static {v2, v7}, Lcom/samsung/musicplus/util/UiUtils;->getSamplingRate(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0, v5, v6, v7}, Lcom/samsung/musicplus/mediainfo/MediaInfoDefaultFragment;->setItemText(IILjava/lang/String;)V

    .line 87
    iget-wide v6, p1, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;->size:J

    invoke-static {v2, v6, v7}, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils;->getFileSize(Landroid/content/Context;J)[Ljava/lang/String;

    move-result-object v3

    .line 88
    .local v3, "fileSize":[Ljava/lang/String;
    const v5, 0x7f0d00df

    const v6, 0x7f10015c

    const/4 v7, 0x0

    aget-object v7, v3, v7

    const/4 v8, 0x1

    aget-object v8, v3, v8

    invoke-direct {p0, v5, v6, v7, v8}, Lcom/samsung/musicplus/mediainfo/MediaInfoDefaultFragment;->setItemText(IILjava/lang/String;Ljava/lang/String;)V

    .line 90
    const v5, 0x7f0d00e0

    const v6, 0x7f10011f

    iget-object v7, p1, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;->data:Ljava/lang/String;

    invoke-direct {p0, v5, v6, v7}, Lcom/samsung/musicplus/mediainfo/MediaInfoDefaultFragment;->setItemText(IILjava/lang/String;)V

    goto/16 :goto_0

    .line 63
    .end local v3    # "fileSize":[Ljava/lang/String;
    .end local v4    # "trackLength":Ljava/lang/String;
    :cond_1
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_1
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 34
    const v0, 0x7f040043

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 39
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 40
    invoke-static {}, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils;->getMetaData()Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoDefaultFragment;->mMetaData:Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;

    .line 41
    iget-object v1, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoDefaultFragment;->mMetaData:Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;

    if-eqz v1, :cond_0

    .line 42
    iget-object v1, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoDefaultFragment;->mMetaData:Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;

    invoke-direct {p0, v1}, Lcom/samsung/musicplus/mediainfo/MediaInfoDefaultFragment;->setMetaDataInfo(Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;)V

    .line 45
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/mediainfo/MediaInfoDefaultFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 46
    .local v0, "a":Landroid/app/Activity;
    instance-of v1, v0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;

    if-eqz v1, :cond_1

    .line 47
    check-cast v0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;

    .end local v0    # "a":Landroid/app/Activity;
    invoke-virtual {v0}, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->attachRichInfoFragments()V

    .line 49
    :cond_1
    return-void
.end method

.method public setBasicMetaInfo(Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;Landroid/content/Context;)V
    .locals 3
    .param p1, "metaData"    # Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 95
    const v0, 0x7f0d00d6

    const v1, 0x7f100178

    iget-object v2, p1, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;->title:Ljava/lang/String;

    invoke-static {p2, v2}, Lcom/samsung/musicplus/util/UiUtils;->transUnknownString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/musicplus/mediainfo/MediaInfoDefaultFragment;->setItemText(IILjava/lang/String;)V

    .line 97
    const v0, 0x7f0d00d5

    const v1, 0x7f100026

    iget-object v2, p1, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;->artist:Ljava/lang/String;

    invoke-static {p2, v2}, Lcom/samsung/musicplus/util/UiUtils;->transUnknownString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/musicplus/mediainfo/MediaInfoDefaultFragment;->setItemText(IILjava/lang/String;)V

    .line 100
    const v0, 0x7f0d00d7

    const v1, 0x7f10001e

    iget-object v2, p1, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;->album:Ljava/lang/String;

    invoke-static {p2, v2}, Lcom/samsung/musicplus/util/UiUtils;->transUnknownString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/musicplus/mediainfo/MediaInfoDefaultFragment;->setItemText(IILjava/lang/String;)V

    .line 101
    return-void
.end method
