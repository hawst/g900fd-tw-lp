.class public Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoBaseFragment;
.super Landroid/app/Fragment;
.source "MediaInfoBaseFragment.java"


# instance fields
.field private mFormatBuilder:Ljava/lang/StringBuilder;

.field private mFormatter:Ljava/util/Formatter;

.field protected mParsedData:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 18
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 46
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoBaseFragment;->mFormatBuilder:Ljava/lang/StringBuilder;

    .line 48
    new-instance v0, Ljava/util/Formatter;

    iget-object v1, p0, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoBaseFragment;->mFormatBuilder:Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/util/Formatter;-><init>(Ljava/lang/Appendable;Ljava/util/Locale;)V

    iput-object v0, p0, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoBaseFragment;->mFormatter:Ljava/util/Formatter;

    return-void
.end method


# virtual methods
.method protected addSubTitle(II)Landroid/view/View;
    .locals 7
    .param p1, "subTitle"    # I
    .param p2, "number"    # I

    .prologue
    .line 33
    invoke-virtual {p0}, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoBaseFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "layout_inflater"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 35
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v3, 0x7f04004b

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 36
    .local v2, "v":Landroid/view/View;
    const v3, 0x7f0d00ec

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 37
    .local v1, "textView":Landroid/widget/TextView;
    if-gez p2, :cond_0

    .line 38
    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(I)V

    .line 43
    :goto_0
    return-object v2

    .line 40
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoBaseFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v3, p1, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method protected addTitleItem(Ljava/lang/String;I)Landroid/view/View;
    .locals 8
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "number"    # I

    .prologue
    const/4 v7, 0x0

    .line 51
    invoke-virtual {p0}, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoBaseFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "layout_inflater"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 53
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v4, 0x7f04004c

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 54
    .local v3, "v":Landroid/view/View;
    const v4, 0x7f0d00e1

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 55
    .local v2, "titleView":Landroid/widget/TextView;
    if-lez p2, :cond_0

    .line 56
    iget-object v4, p0, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoBaseFragment;->mFormatBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 57
    const v4, 0x7f10013e

    invoke-virtual {p0, v4}, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoBaseFragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 58
    .local v1, "listItemFormat":Ljava/lang/String;
    iget-object v4, p0, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoBaseFragment;->mFormatter:Ljava/util/Formatter;

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    const/4 v6, 0x1

    aput-object p1, v5, v6

    invoke-virtual {v4, v1, v5}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 63
    .end local v1    # "listItemFormat":Ljava/lang/String;
    :goto_0
    return-object v3

    .line 60
    :cond_0
    invoke-virtual {v2, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 24
    invoke-super {p0, p1}, Landroid/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 25
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoBaseFragment;->setRetainInstance(Z)V

    .line 26
    invoke-virtual {p0}, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoBaseFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 27
    .local v0, "a":Landroid/app/Activity;
    instance-of v1, v0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;

    if-eqz v1, :cond_0

    .line 28
    check-cast v0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;

    .end local v0    # "a":Landroid/app/Activity;
    invoke-virtual {v0}, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->getRichInfoData()Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoBaseFragment;->mParsedData:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;

    .line 30
    :cond_0
    return-void
.end method
