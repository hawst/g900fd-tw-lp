.class public Lcom/samsung/musicplus/mediainfo/findtag/FindTagResultDialog$FindTagResultListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "FindTagResultDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/mediainfo/findtag/FindTagResultDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "FindTagResultListAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$FindTag;",
        ">;"
    }
.end annotation


# instance fields
.field private mAlbumArtSize:I

.field private mContext:Landroid/content/Context;

.field private mFindTags:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$FindTag;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/samsung/musicplus/mediainfo/findtag/FindTagResultDialog;


# direct methods
.method public constructor <init>(Lcom/samsung/musicplus/mediainfo/findtag/FindTagResultDialog;Landroid/content/Context;ILjava/util/ArrayList;)V
    .locals 2
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "textViewResourceId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$FindTag;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 84
    .local p4, "findTags":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$FindTag;>;"
    iput-object p1, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagResultDialog$FindTagResultListAdapter;->this$0:Lcom/samsung/musicplus/mediainfo/findtag/FindTagResultDialog;

    .line 85
    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 86
    iput-object p2, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagResultDialog$FindTagResultListAdapter;->mContext:Landroid/content/Context;

    .line 87
    iput-object p4, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagResultDialog$FindTagResultListAdapter;->mFindTags:Ljava/util/ArrayList;

    .line 88
    invoke-virtual {p1}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagResultDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0099

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagResultDialog$FindTagResultListAdapter;->mAlbumArtSize:I

    .line 90
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v7, 0x0

    .line 94
    iget-object v5, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagResultDialog$FindTagResultListAdapter;->mContext:Landroid/content/Context;

    check-cast v5, Landroid/app/Activity;

    invoke-virtual {v5}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    .line 95
    .local v2, "inflater":Landroid/view/LayoutInflater;
    const v5, 0x7f040045

    invoke-virtual {v2, v5, p3, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 96
    const v5, 0x7f0d00e1

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 97
    .local v3, "track":Landroid/widget/TextView;
    const v5, 0x7f0d00e2

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 98
    .local v1, "artist":Landroid/widget/TextView;
    const v5, 0x7f0d00e3

    invoke-virtual {p2, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 100
    .local v0, "albumView":Landroid/widget/ImageView;
    iget-object v5, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagResultDialog$FindTagResultListAdapter;->mFindTags:Ljava/util/ArrayList;

    invoke-virtual {v5, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$FindTag;

    iget-object v4, v5, Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$FindTag;->albumArtUrl:Ljava/lang/String;

    .line 101
    .local v4, "url":Ljava/lang/String;
    if-eqz v4, :cond_0

    const-string v5, ""

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 102
    new-instance v5, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoUtils$AlbumArtLoader;

    invoke-direct {v5}, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoUtils$AlbumArtLoader;-><init>()V

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    aput-object v0, v6, v7

    const/4 v7, 0x1

    aput-object v4, v6, v7

    const/4 v7, 0x2

    iget v8, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagResultDialog$FindTagResultListAdapter;->mAlbumArtSize:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v5, v6}, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoUtils$AlbumArtLoader;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 104
    :cond_0
    iget-object v5, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagResultDialog$FindTagResultListAdapter;->mFindTags:Ljava/util/ArrayList;

    invoke-virtual {v5, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$FindTag;

    iget-object v5, v5, Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$FindTag;->trackName:Ljava/lang/String;

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 105
    iget-object v5, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagResultDialog$FindTagResultListAdapter;->mFindTags:Ljava/util/ArrayList;

    invoke-virtual {v5, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$FindTag;

    iget-object v5, v5, Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$FindTag;->artistName:Ljava/lang/String;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 106
    return-object p2
.end method
