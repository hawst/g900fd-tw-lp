.class public Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;
.super Ljava/lang/Object;
.source "NetworkData.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$FindTag;,
        Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$Track;
    }
.end annotation


# instance fields
.field public encryptedData:Ljava/lang/String;

.field public errorCode:Ljava/lang/String;

.field public errorLanguage:Ljava/lang/String;

.field public errorMessage:Ljava/lang/String;

.field public errorReference:Ljava/lang/String;

.field public errorText:Ljava/lang/String;

.field public isError:Z

.field public requestConfig:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field requestId:Ljava/lang/String;

.field resultFindTags:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$FindTag;",
            ">;"
        }
    .end annotation
.end field

.field resultTracks:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$Track;",
            ">;"
        }
    .end annotation
.end field

.field public shopUrl:Ljava/lang/String;

.field public timeStamp:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 7
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;->isError:Z

    .line 26
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;->requestConfig:Ljava/util/HashMap;

    .line 32
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;->resultTracks:Ljava/util/ArrayList;

    .line 37
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;->resultFindTags:Ljava/util/ArrayList;

    .line 65
    return-void
.end method
