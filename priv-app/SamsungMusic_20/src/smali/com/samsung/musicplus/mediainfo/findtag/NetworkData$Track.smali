.class Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$Track;
.super Ljava/lang/Object;
.source "NetworkData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Track"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$Track$Artist;
    }
.end annotation


# instance fields
.field public album:Ljava/lang/String;

.field public artists:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$Track$Artist;",
            ">;"
        }
    .end annotation
.end field

.field public coverUrl:Ljava/lang/String;

.field public genre:Ljava/lang/String;

.field public recommendation:Ljava/lang/String;

.field public title:Ljava/lang/String;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$Track;->artists:Ljava/util/ArrayList;

    .line 58
    return-void
.end method
