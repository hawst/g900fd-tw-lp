.class Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$3;
.super Landroid/os/Handler;
.source "FindTagActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;)V
    .locals 0

    .prologue
    .line 183
    iput-object p1, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$3;->this$0:Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const v3, 0x7f10008d

    .line 186
    const/4 v0, 0x0

    .line 187
    .local v0, "message":Ljava/lang/String;
    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    .line 234
    :cond_0
    :goto_0
    # getter for: Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->TAG_MESSAGE_STEP_BY_STEP:Z
    invoke-static {}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->access$100()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$3;->this$0:Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;

    # getter for: Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v2}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->access$1000(Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;)Landroid/app/ProgressDialog;

    move-result-object v2

    if-eqz v2, :cond_1

    if-eqz v0, :cond_1

    .line 235
    iget-object v2, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$3;->this$0:Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;

    # getter for: Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v2}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->access$1000(Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;)Landroid/app/ProgressDialog;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 237
    :cond_1
    return-void

    .line 189
    :pswitch_0
    # getter for: Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->TAG_MESSAGE_STEP_BY_STEP:Z
    invoke-static {}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->access$100()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 190
    iget-object v2, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$3;->this$0:Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;

    const v3, 0x7f100044

    invoke-virtual {v2, v3}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 192
    :cond_2
    iget-object v2, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$3;->this$0:Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;

    # invokes: Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->requestConfig()V
    invoke-static {v2}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->access$200(Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;)V

    goto :goto_0

    .line 195
    :pswitch_1
    # getter for: Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->TAG_MESSAGE_STEP_BY_STEP:Z
    invoke-static {}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->access$100()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 196
    iget-object v2, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$3;->this$0:Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;

    invoke-virtual {v2, v3}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 198
    :cond_3
    iget-object v2, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$3;->this$0:Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "sigdata"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v3

    # invokes: Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->doRecognition([B)V
    invoke-static {v2, v3}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->access$300(Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;[B)V

    goto :goto_0

    .line 201
    :pswitch_2
    iget-object v2, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$3;->this$0:Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;

    # invokes: Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->hideProgressDialog()V
    invoke-static {v2}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->access$400(Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;)V

    .line 205
    iget-object v2, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$3;->this$0:Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;

    const/4 v3, 0x3

    invoke-virtual {v2, v3}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->setResult(I)V

    .line 206
    iget-object v2, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$3;->this$0:Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;

    invoke-virtual {v2}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->finish()V

    goto :goto_0

    .line 209
    :pswitch_3
    # getter for: Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->TAG_MESSAGE_STEP_BY_STEP:Z
    invoke-static {}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->access$100()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 210
    iget-object v2, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$3;->this$0:Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;

    invoke-virtual {v2, v3}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 212
    :cond_4
    iget-object v2, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$3;->this$0:Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "requestId"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->requestResults(Ljava/lang/String;)V
    invoke-static {v2, v3}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->access$500(Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;Ljava/lang/String;)V

    goto :goto_0

    .line 215
    :pswitch_4
    # getter for: Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->TAG_MESSAGE_STEP_BY_STEP:Z
    invoke-static {}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->access$100()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 216
    iget-object v2, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$3;->this$0:Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;

    const v3, 0x7f100092

    invoke-virtual {v2, v3}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 218
    :cond_5
    invoke-static {}, Lcom/samsung/musicplus/library/audio/FindTag;->getInstance()Lcom/samsung/musicplus/library/audio/FindTag;

    move-result-object v1

    .line 219
    .local v1, "tag":Lcom/samsung/musicplus/library/audio/FindTag;
    iget-object v2, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$3;->this$0:Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;

    # getter for: Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->mFilePath:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->access$600(Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$3;->this$0:Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;

    # getter for: Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->mResultListener:Lcom/samsung/musicplus/library/audio/FindTag$OnFindTagResultListener;
    invoke-static {v3}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->access$700(Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;)Lcom/samsung/musicplus/library/audio/FindTag$OnFindTagResultListener;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/samsung/musicplus/library/audio/FindTag;->startParsingSignature(Ljava/lang/String;Lcom/samsung/musicplus/library/audio/FindTag$OnFindTagResultListener;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 220
    const-string v2, "RichInfo"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    # getter for: Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->CLASSTAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->access$800()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "internalRecording() is fail RichInfoUtils.sPath is "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$3;->this$0:Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;

    # getter for: Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->mFilePath:Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->access$600(Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 223
    iget-object v2, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$3;->this$0:Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;

    # invokes: Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->hideProgressDialog()V
    invoke-static {v2}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->access$400(Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;)V

    .line 224
    iget-object v2, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$3;->this$0:Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;

    # invokes: Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->cancelAllOperations()V
    invoke-static {v2}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->access$000(Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;)V

    goto/16 :goto_0

    .line 228
    .end local v1    # "tag":Lcom/samsung/musicplus/library/audio/FindTag;
    :pswitch_5
    iget-object v2, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$3;->this$0:Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;

    # invokes: Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->displayFindTagResults()V
    invoke-static {v2}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->access$900(Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;)V

    goto/16 :goto_0

    .line 187
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_5
        :pswitch_4
    .end packed-switch
.end method
