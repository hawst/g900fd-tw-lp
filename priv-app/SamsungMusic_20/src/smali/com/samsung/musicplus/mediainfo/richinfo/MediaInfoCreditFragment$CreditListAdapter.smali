.class Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoCreditFragment$CreditListAdapter;
.super Landroid/widget/ArrayAdapter;
.source "MediaInfoCreditFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoCreditFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CreditListAdapter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoCreditFragment$CreditListAdapter$ViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$Credit;",
        ">;"
    }
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private mCredits:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$Credit;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;ILjava/util/ArrayList;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "layout"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$Credit;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 71
    .local p3, "credits":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$Credit;>;"
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 72
    iput-object p1, p0, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoCreditFragment$CreditListAdapter;->mContext:Landroid/content/Context;

    .line 73
    iput-object p3, p0, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoCreditFragment$CreditListAdapter;->mCredits:Ljava/util/ArrayList;

    .line 74
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 78
    move-object v1, p2

    .line 79
    .local v1, "row":Landroid/view/View;
    new-instance v2, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoCreditFragment$CreditListAdapter$ViewHolder;

    invoke-direct {v2, p0}, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoCreditFragment$CreditListAdapter$ViewHolder;-><init>(Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoCreditFragment$CreditListAdapter;)V

    .line 81
    .local v2, "vh":Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoCreditFragment$CreditListAdapter$ViewHolder;
    if-nez v1, :cond_0

    .line 82
    iget-object v3, p0, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoCreditFragment$CreditListAdapter;->mContext:Landroid/content/Context;

    check-cast v3, Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 83
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v3, 0x7f04004d

    const/4 v4, 0x0

    invoke-virtual {v0, v3, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 84
    const v3, 0x7f0d00e1

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v2, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoCreditFragment$CreditListAdapter$ViewHolder;->title:Landroid/widget/TextView;

    .line 85
    const v3, 0x7f0d00e2

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, v2, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoCreditFragment$CreditListAdapter$ViewHolder;->content:Landroid/widget/TextView;

    .line 86
    invoke-virtual {v1, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 90
    .end local v0    # "inflater":Landroid/view/LayoutInflater;
    :goto_0
    iget-object v4, v2, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoCreditFragment$CreditListAdapter$ViewHolder;->title:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoCreditFragment$CreditListAdapter;->mCredits:Ljava/util/ArrayList;

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$Credit;

    iget-object v3, v3, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$Credit;->role:Ljava/lang/String;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 91
    iget-object v4, v2, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoCreditFragment$CreditListAdapter$ViewHolder;->content:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoCreditFragment$CreditListAdapter;->mCredits:Ljava/util/ArrayList;

    invoke-virtual {v3, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$Credit;

    iget-object v3, v3, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$Credit;->PerformerSimpleInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    iget-object v3, v3, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;->name:Ljava/lang/String;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 93
    return-object v1

    .line 88
    :cond_0
    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    .end local v2    # "vh":Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoCreditFragment$CreditListAdapter$ViewHolder;
    check-cast v2, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoCreditFragment$CreditListAdapter$ViewHolder;

    .restart local v2    # "vh":Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoCreditFragment$CreditListAdapter$ViewHolder;
    goto :goto_0
.end method

.method public isEnabled(I)Z
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 98
    const/4 v0, 0x0

    return v0
.end method
