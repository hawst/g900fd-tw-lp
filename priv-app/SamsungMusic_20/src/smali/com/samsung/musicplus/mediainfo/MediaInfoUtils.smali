.class public Lcom/samsung/musicplus/mediainfo/MediaInfoUtils;
.super Ljava/lang/Object;
.source "MediaInfoUtils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;
    }
.end annotation


# static fields
.field private static final BIT_DEPTH:Ljava/lang/String; = "bit_depth"

.field private static final CHOOSER_THEME_DEVICE_DEFAULT:I = 0x1

.field private static final CHOOSER_THEME_DEVICE_DEFAULT_LIGHT:I = 0x2

.field private static final GENRE_NAME:Ljava/lang/String; = "genre_name"

.field private static final MEDIA_META_DATA_COLS:[Ljava/lang/String;

.field private static final SAMPLING_RATE:Ljava/lang/String; = "sampling_rate"

.field private static sMetaData:Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 43
    const/16 v0, 0xf

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "artist"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "album"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "genre_name"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "duration"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "track"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "year"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "mime_type"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "bit_depth"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "sampling_rate"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "_size"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "_data"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "album_id"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "recordingtype"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils;->MEDIA_META_DATA_COLS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    return-void
.end method

.method public static adjustMetaData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p0, "title"    # Ljava/lang/String;
    .param p1, "artist"    # Ljava/lang/String;
    .param p2, "album"    # Ljava/lang/String;

    .prologue
    .line 123
    sget-object v0, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils;->sMetaData:Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;

    iput-object p0, v0, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;->title:Ljava/lang/String;

    .line 124
    sget-object v0, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils;->sMetaData:Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;

    iput-object p1, v0, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;->artist:Ljava/lang/String;

    .line 125
    sget-object v0, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils;->sMetaData:Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;

    iput-object p2, v0, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;->album:Ljava/lang/String;

    .line 126
    return-void
.end method

.method public static getAlbumArt(Landroid/content/Context;J)Landroid/graphics/Bitmap;
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "albumId"    # J

    .prologue
    .line 168
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c0098

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 170
    .local v1, "size":I
    invoke-static {p0, p1, p2, v1, v1}, Lcom/samsung/musicplus/util/AlbumArtUtils;->getArtworkQuick(Landroid/content/Context;JII)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 171
    .local v0, "bm":Landroid/graphics/Bitmap;
    return-object v0
.end method

.method public static getFileSize(Landroid/content/Context;J)[Ljava/lang/String;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "size"    # J

    .prologue
    .line 187
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "0.##"

    invoke-direct {v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    .line 188
    .local v0, "df":Ljava/text/DecimalFormat;
    long-to-double v2, p1

    .line 190
    .local v2, "result":D
    long-to-double v4, p1

    const-wide/high16 v6, 0x4130000000000000L    # 1048576.0

    div-double v2, v4, v6

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    cmpl-double v1, v2, v4

    if-ltz v1, :cond_0

    .line 191
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const v6, 0x7f10015f

    invoke-virtual {p0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v1, v4

    const/4 v4, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v5

    const v6, 0x7f1001a3

    invoke-virtual {p0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v1, v4

    .line 201
    :goto_0
    return-object v1

    .line 195
    :cond_0
    long-to-double v4, p1

    const-wide/high16 v6, 0x4090000000000000L    # 1024.0

    div-double v2, v4, v6

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    cmpl-double v1, v2, v4

    if-ltz v1, :cond_1

    .line 196
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const v6, 0x7f10015e

    invoke-virtual {p0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v1, v4

    const/4 v4, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v5

    const v6, 0x7f1001a2

    invoke-virtual {p0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v1, v4

    goto :goto_0

    .line 201
    :cond_1
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v4, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f10015d

    invoke-virtual {p0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v1, v4

    const/4 v4, 0x1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const v6, 0x7f1001a1

    invoke-virtual {p0, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v1, v4

    goto :goto_0
.end method

.method public static getMetaData()Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;
    .locals 1

    .prologue
    .line 86
    sget-object v0, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils;->sMetaData:Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;

    return-object v0
.end method

.method public static getMetaData(Landroid/content/Context;Landroid/net/Uri;)Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 90
    const/4 v7, 0x0

    .line 91
    .local v7, "data":Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;
    const/4 v6, 0x0

    .line 93
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils;->MEDIA_META_DATA_COLS:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 94
    if-eqz v6, :cond_0

    .line 95
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 96
    new-instance v8, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;

    invoke-direct {v8}, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 97
    .end local v7    # "data":Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;
    .local v8, "data":Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;
    :try_start_1
    sget-object v0, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils;->MEDIA_META_DATA_COLS:[Ljava/lang/String;

    const/4 v1, 0x1

    aget-object v0, v0, v1

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v8, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;->title:Ljava/lang/String;

    .line 98
    sget-object v0, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils;->MEDIA_META_DATA_COLS:[Ljava/lang/String;

    const/4 v1, 0x2

    aget-object v0, v0, v1

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v8, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;->artist:Ljava/lang/String;

    .line 99
    sget-object v0, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils;->MEDIA_META_DATA_COLS:[Ljava/lang/String;

    const/4 v1, 0x3

    aget-object v0, v0, v1

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v8, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;->album:Ljava/lang/String;

    .line 100
    sget-object v0, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils;->MEDIA_META_DATA_COLS:[Ljava/lang/String;

    const/4 v1, 0x4

    aget-object v0, v0, v1

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v8, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;->genre:Ljava/lang/String;

    .line 101
    sget-object v0, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils;->MEDIA_META_DATA_COLS:[Ljava/lang/String;

    const/4 v1, 0x5

    aget-object v0, v0, v1

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, v8, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;->duration:J

    .line 102
    sget-object v0, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils;->MEDIA_META_DATA_COLS:[Ljava/lang/String;

    const/4 v1, 0x6

    aget-object v0, v0, v1

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v8, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;->track:Ljava/lang/String;

    .line 103
    sget-object v0, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils;->MEDIA_META_DATA_COLS:[Ljava/lang/String;

    const/4 v1, 0x7

    aget-object v0, v0, v1

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v8, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;->year:Ljava/lang/String;

    .line 104
    sget-object v0, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils;->MEDIA_META_DATA_COLS:[Ljava/lang/String;

    const/16 v1, 0x8

    aget-object v0, v0, v1

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v8, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;->mimeType:Ljava/lang/String;

    .line 105
    sget-object v0, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils;->MEDIA_META_DATA_COLS:[Ljava/lang/String;

    const/16 v1, 0x9

    aget-object v0, v0, v1

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v8, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;->bitDepth:I

    .line 106
    sget-object v0, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils;->MEDIA_META_DATA_COLS:[Ljava/lang/String;

    const/16 v1, 0xa

    aget-object v0, v0, v1

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v8, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;->samplingRate:I

    .line 107
    sget-object v0, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils;->MEDIA_META_DATA_COLS:[Ljava/lang/String;

    const/16 v1, 0xb

    aget-object v0, v0, v1

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, v8, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;->size:J

    .line 108
    sget-object v0, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils;->MEDIA_META_DATA_COLS:[Ljava/lang/String;

    const/16 v1, 0xc

    aget-object v0, v0, v1

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v8, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;->data:Ljava/lang/String;

    .line 109
    sget-object v0, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils;->MEDIA_META_DATA_COLS:[Ljava/lang/String;

    const/16 v1, 0xd

    aget-object v0, v0, v1

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, v8, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;->albumId:J

    .line 110
    sget-object v0, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils;->MEDIA_META_DATA_COLS:[Ljava/lang/String;

    const/16 v1, 0xe

    aget-object v0, v0, v1

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v8, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;->recordingType:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-object v7, v8

    .line 114
    .end local v8    # "data":Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;
    .restart local v7    # "data":Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;
    :cond_0
    if-eqz v6, :cond_1

    .line 115
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 118
    :cond_1
    sput-object v7, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils;->sMetaData:Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;

    .line 119
    return-object v7

    .line 114
    :catchall_0
    move-exception v0

    :goto_0
    if-eqz v6, :cond_2

    .line 115
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0

    .line 114
    .end local v7    # "data":Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;
    .restart local v8    # "data":Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;
    :catchall_1
    move-exception v0

    move-object v7, v8

    .end local v8    # "data":Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;
    .restart local v7    # "data":Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;
    goto :goto_0
.end method

.method public static getTrackLength(Landroid/content/Context;J)Ljava/lang/String;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "duration"    # J

    .prologue
    .line 175
    const-wide/16 v0, 0x3e8

    div-long v0, p1, v0

    invoke-static {p0, v0, v1}, Lcom/samsung/musicplus/util/UiUtils;->makeTimeString(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static searchMediaInfo(Landroid/app/Activity;Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;)V
    .locals 8
    .param p0, "a"    # Landroid/app/Activity;
    .param p1, "metaData"    # Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;

    .prologue
    const v7, 0x7f1001bb

    const/high16 v6, 0x10000000

    .line 138
    const-string v2, "audio/*"

    .line 139
    .local v2, "mime":Ljava/lang/String;
    const/4 v3, 0x0

    .line 141
    .local v3, "title":Ljava/lang/String;
    if-eqz p1, :cond_0

    .line 142
    iget-object v4, p1, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;->artist:Ljava/lang/String;

    if-eqz v4, :cond_2

    iget-object v4, p1, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;->artist:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_2

    .line 143
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4}, Ljava/lang/StringBuffer;-><init>()V

    iget-object v5, p1, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;->artist:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    iget-object v5, p1, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;->title:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    .line 150
    :cond_0
    :goto_0
    if-nez v3, :cond_1

    .line 151
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "<"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p0, v7}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ">"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 154
    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-string v4, "android.intent.action.MEDIA_SEARCH"

    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 155
    .local v0, "i":Landroid/content/Intent;
    invoke-virtual {v0, v6}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 156
    const-string v4, "query"

    invoke-virtual {v0, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 157
    const-string v5, "android.intent.extra.title"

    if-nez p1, :cond_3

    invoke-virtual {p0, v7}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v4

    :goto_1
    invoke-virtual {v0, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 159
    const-string v4, "android.intent.extra.focus"

    invoke-virtual {v0, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 160
    const-string v5, "theme"

    invoke-static {}, Lcom/samsung/musicplus/util/UiUtils;->isWhiteTheme()Z

    move-result v4

    if-eqz v4, :cond_4

    const/4 v4, 0x2

    :goto_2
    invoke-virtual {v0, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 162
    const v4, 0x7f10014f

    invoke-virtual {p0, v4}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v1

    .line 163
    .local v1, "intent":Landroid/content/Intent;
    invoke-virtual {v1, v6}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 164
    invoke-virtual {p0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 165
    return-void

    .line 146
    .end local v0    # "i":Landroid/content/Intent;
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_2
    iget-object v3, p1, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;->title:Ljava/lang/String;

    goto :goto_0

    .line 157
    .restart local v0    # "i":Landroid/content/Intent;
    :cond_3
    iget-object v4, p1, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;->title:Ljava/lang/String;

    goto :goto_1

    .line 160
    :cond_4
    const/4 v4, 0x1

    goto :goto_2
.end method

.method public static showNetworkErrorToast(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 209
    const v0, 0x7f10004f

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 210
    return-void
.end method
