.class public Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$PerformerDetailInfo;
.super Ljava/lang/Object;
.source "RichInfoData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PerformerDetailInfo"
.end annotation


# instance fields
.field public albums:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;",
            ">;"
        }
    .end annotation
.end field

.field public biography:Ljava/lang/String;

.field followers:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;",
            ">;"
        }
    .end annotation
.end field

.field influenceArtists:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;",
            ">;"
        }
    .end annotation
.end field

.field performerSimpleInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

.field public similarArtists:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
