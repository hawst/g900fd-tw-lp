.class Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$7;
.super Ljava/lang/Thread;
.source "FindTagActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->doRecognition([B)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;

.field final synthetic val$recordedSample:[B

.field final synthetic val$responseHandler:Lorg/apache/http/client/ResponseHandler;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;[BLorg/apache/http/client/ResponseHandler;)V
    .locals 0

    .prologue
    .line 338
    iput-object p1, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$7;->this$0:Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;

    iput-object p2, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$7;->val$recordedSample:[B

    iput-object p3, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$7;->val$responseHandler:Lorg/apache/http/client/ResponseHandler;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 14

    .prologue
    const/4 v12, 0x5

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v13, 0x0

    .line 341
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 342
    .local v1, "formatBuilder":Ljava/lang/StringBuilder;
    new-instance v2, Ljava/util/Formatter;

    sget-object v8, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v2, v1, v8}, Ljava/util/Formatter;-><init>(Ljava/lang/Appendable;Ljava/util/Locale;)V

    .line 343
    .local v2, "formatter":Ljava/util/Formatter;
    const-string v0, "%04d-%02d-%02dT%02d:%02d:%02d"

    .line 345
    .local v0, "date":Ljava/lang/String;
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v6

    .line 346
    .local v6, "time":Ljava/util/Calendar;
    invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 347
    const/4 v8, 0x6

    new-array v8, v8, [Ljava/lang/Object;

    invoke-virtual {v6, v10}, Ljava/util/Calendar;->get(I)I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v13

    invoke-virtual {v6, v11}, Ljava/util/Calendar;->get(I)I

    move-result v9

    add-int/lit8 v9, v9, 0x1

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-virtual {v6, v12}, Ljava/util/Calendar;->get(I)I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v11

    const/4 v9, 0x3

    const/16 v10, 0xb

    invoke-virtual {v6, v10}, Ljava/util/Calendar;->get(I)I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x4

    const/16 v10, 0xc

    invoke-virtual {v6, v10}, Ljava/util/Calendar;->get(I)I

    move-result v10

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    const/16 v9, 0xd

    invoke-virtual {v6, v9}, Ljava/util/Calendar;->get(I)I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v12

    invoke-virtual {v2, v0, v8}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v5

    .line 351
    .local v5, "tagDate":Ljava/lang/String;
    invoke-virtual {v2}, Ljava/util/Formatter;->close()V

    .line 353
    iget-object v8, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$7;->this$0:Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;

    # invokes: Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->getBaseOrbitParameters(Z)Ljava/util/List;
    invoke-static {v8, v13}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->access$1200(Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;Z)Ljava/util/List;

    move-result-object v4

    .line 354
    .local v4, "parts":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/musicplus/library/http/multipart/Part;>;"
    new-instance v8, Lcom/samsung/musicplus/library/http/multipart/StringPart;

    const-string v9, "tagDate"

    invoke-static {v5}, Lcom/samsung/musicplus/mediainfo/crypto/IceCrypt;->encrypt_string(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v8, v9, v10}, Lcom/samsung/musicplus/library/http/multipart/StringPart;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 356
    new-instance v8, Lcom/samsung/musicplus/library/http/multipart/StringPart;

    const-string v9, "sampleBytes"

    iget-object v10, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$7;->val$recordedSample:[B

    array-length v10, v10

    invoke-static {v10}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/samsung/musicplus/mediainfo/crypto/IceCrypt;->encrypt_string(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v8, v9, v10}, Lcom/samsung/musicplus/library/http/multipart/StringPart;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v4, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 358
    new-instance v8, Lcom/samsung/musicplus/library/http/multipart/FilePart;

    const-string v9, "sample"

    new-instance v10, Lcom/samsung/musicplus/library/http/multipart/ByteArrayPartSource;

    const-string v11, "sign.sig"

    iget-object v12, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$7;->val$recordedSample:[B

    invoke-static {v12}, Lcom/samsung/musicplus/mediainfo/crypto/IceCrypt;->encrypt_byte([B)[B

    move-result-object v12

    invoke-direct {v10, v11, v12}, Lcom/samsung/musicplus/library/http/multipart/ByteArrayPartSource;-><init>(Ljava/lang/String;[B)V

    invoke-direct {v8, v9, v10}, Lcom/samsung/musicplus/library/http/multipart/FilePart;-><init>(Ljava/lang/String;Lcom/samsung/musicplus/library/http/multipart/PartSource;)V

    invoke-interface {v4, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 361
    const-string v7, "http://ssung.meta.shazamid.com/orbit/DoRecognition1"

    .line 362
    .local v7, "url":Ljava/lang/String;
    new-instance v3, Lcom/samsung/musicplus/util/HTTPRequestHelper;

    iget-object v8, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$7;->val$responseHandler:Lorg/apache/http/client/ResponseHandler;

    invoke-direct {v3, v8}, Lcom/samsung/musicplus/util/HTTPRequestHelper;-><init>(Lorg/apache/http/client/ResponseHandler;)V

    .line 364
    .local v3, "helper":Lcom/samsung/musicplus/util/HTTPRequestHelper;
    new-array v8, v13, [Lcom/samsung/musicplus/library/http/multipart/Part;

    invoke-interface {v4, v8}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v8

    check-cast v8, [Lcom/samsung/musicplus/library/http/multipart/Part;

    invoke-virtual {v3, v7, v8}, Lcom/samsung/musicplus/util/HTTPRequestHelper;->performPost(Ljava/lang/String;[Lcom/samsung/musicplus/library/http/multipart/Part;)V

    .line 365
    return-void
.end method
