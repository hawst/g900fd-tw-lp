.class public Lcom/samsung/musicplus/mediainfo/findtag/NetworkResponseParser;
.super Ljava/lang/Object;
.source "NetworkResponseParser.java"


# static fields
.field private static final CLASSTAG:Ljava/lang/String;

.field private static final CONFIG_CHART:I = 0x2

.field private static final CONFIG_SERVICE:I = 0x1

.field private static final ENCRYPTED_DATA:Ljava/lang/String; = "/shazamResponse/requestResults1/tracks/track/findTag/EncryptedData/CipherData/CipherValue"

.field private static final FIND_TAG:Ljava/lang/String; = "/shazamResponse/requestResults1/tracks/track/findTag/track"

.field private static final FIND_TAG_AMGID:Ljava/lang/String; = "/shazamResponse/requestResults1/tracks/track/findTag/track@amgid"

.field private static final FIND_TAG_ARTIST:Ljava/lang/String; = "/shazamResponse/requestResults1/tracks/track/findTag/track/Album/AlbumSimpleInfo/Artist/Name"

.field private static final FIND_TAG_ARTWORK:Ljava/lang/String; = "/shazamResponse/requestResults1/tracks/track/findTag/track/Album/AlbumSimpleInfo/Artwork@uri"

.field private static final FIND_TAG_TRACK:Ljava/lang/String; = "/shazamResponse/requestResults1/tracks/track/findTag/track/Album/AlbumSimpleInfo/Title"

.field private static final FIND_TAG_URL:Ljava/lang/String; = "/shazamResponse/requestResults1/tracks/track/findTag/track@href"

.field private static final SHAZAM_CONFIG_KEY:Ljava/lang/String; = "/elements/config@key"

.field private static final SHAZAM_CONFIG_VALUE:Ljava/lang/String; = "/elements/config@value"

.field private static final SHAZAM_DO_RECOGNITION:Ljava/lang/String; = "/shazamResponse/doRecognition1"

.field private static final SHAZAM_ERROR:Ljava/lang/String; = "/shazamResponse/error"

.field private static final SHAZAM_ERROR_CODE:Ljava/lang/String; = "/shazamResponse/error@code"

.field private static final SHAZAM_ERROR_MESSAGE:Ljava/lang/String; = "/shazamResponse/error@message"

.field private static final SHAZAM_REQUESTID:Ljava/lang/String; = "/requestId@id"

.field private static final SHAZAM_REQUEST_CONFIG:Ljava/lang/String; = "/shazamResponse/requestConfig1"

.field private static final SHAZAM_TIME_STAMP:Ljava/lang/String; = "/shazamResponse@timestamp"

.field private static final SHAZAM_TRACK:Ljava/lang/String; = "/track"

.field private static final SHAZAM_TRACKS:Ljava/lang/String; = "/shazamResponse/requestResult1/tracks"

.field private static final SHAZAM_TRACK_ALBUM:Ljava/lang/String; = "/track/talbum"

.field private static final SHAZAM_TRACK_ARTIST:Ljava/lang/String; = "/track/tartists/tartist"

.field private static final SHAZAM_TRACK_ARTISTS:Ljava/lang/String; = "/track/tartists"

.field private static final SHAZAM_TRACK_ARTIST_ID:Ljava/lang/String; = "/track/tartists/tartist@id"

.field private static final SHAZAM_TRACK_COVERURL:Ljava/lang/String; = "/track/tcoverurl"

.field private static final SHAZAM_TRACK_GENRE:Ljava/lang/String; = "/track/tgenre"

.field private static final SHAZAM_TRACK_ID:Ljava/lang/String; = "/track@id"

.field private static final SHAZAM_TRACK_RECOMMENDATIONS:Ljava/lang/String; = "/track/trecommendations"

.field private static final SHAZAM_TRACK_TITLE:Ljava/lang/String; = "/track/ttitle"

.field private static mArtist:Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$Track$Artist;

.field private static mFindTag:Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$FindTag;

.field private static mNetworkResponseData:Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;

.field private static mRequestConfigTag:I

.field private static mTrack:Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$Track;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkResponseParser;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkResponseParser;->CLASSTAG:Ljava/lang/String;

    .line 23
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkResponseParser;->mNetworkResponseData:Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;

    .line 199
    const/4 v0, -0x1

    sput v0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkResponseParser;->mRequestConfigTag:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static parseEndTag(Ljava/lang/String;)V
    .locals 3
    .param p0, "current"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 341
    const-string v0, "/shazamResponse/requestResult1/tracks/track"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 342
    sget-object v0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkResponseParser;->mTrack:Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$Track;

    if-eqz v0, :cond_0

    .line 343
    sget-object v0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkResponseParser;->mNetworkResponseData:Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;

    iget-object v0, v0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;->resultTracks:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/musicplus/mediainfo/findtag/NetworkResponseParser;->mTrack:Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$Track;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 344
    sput-object v2, Lcom/samsung/musicplus/mediainfo/findtag/NetworkResponseParser;->mTrack:Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$Track;

    .line 357
    :cond_0
    :goto_0
    return-void

    .line 346
    :cond_1
    const-string v0, "/shazamResponse/requestResult1/tracks/track/tartists/tartist"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 347
    sget-object v0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkResponseParser;->mTrack:Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$Track;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkResponseParser;->mArtist:Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$Track$Artist;

    if-eqz v0, :cond_0

    .line 348
    sget-object v0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkResponseParser;->mTrack:Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$Track;

    iget-object v0, v0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$Track;->artists:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/musicplus/mediainfo/findtag/NetworkResponseParser;->mArtist:Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$Track$Artist;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 349
    sput-object v2, Lcom/samsung/musicplus/mediainfo/findtag/NetworkResponseParser;->mArtist:Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$Track$Artist;

    goto :goto_0

    .line 351
    :cond_2
    const-string v0, "/shazamResponse/requestResults1/tracks/track/findTag/track"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 352
    sget-object v0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkResponseParser;->mFindTag:Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$FindTag;

    if-eqz v0, :cond_0

    .line 353
    sget-object v0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkResponseParser;->mNetworkResponseData:Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;

    iget-object v0, v0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;->resultFindTags:Ljava/util/ArrayList;

    sget-object v1, Lcom/samsung/musicplus/mediainfo/findtag/NetworkResponseParser;->mFindTag:Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$FindTag;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 354
    sput-object v2, Lcom/samsung/musicplus/mediainfo/findtag/NetworkResponseParser;->mFindTag:Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$FindTag;

    goto :goto_0
.end method

.method public static parseResponse(Ljava/io/InputStream;)Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;
    .locals 5
    .param p0, "input"    # Ljava/io/InputStream;

    .prologue
    const/4 v4, 0x1

    .line 48
    new-instance v2, Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;

    invoke-direct {v2}, Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;-><init>()V

    sput-object v2, Lcom/samsung/musicplus/mediainfo/findtag/NetworkResponseParser;->mNetworkResponseData:Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;

    .line 49
    new-instance v1, Ljava/io/InputStreamReader;

    invoke-direct {v1, p0}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    .line 51
    .local v1, "reader":Ljava/io/Reader;
    :try_start_0
    invoke-static {v1}, Lcom/samsung/musicplus/mediainfo/findtag/NetworkResponseParser;->parseXML(Ljava/io/Reader;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 54
    :try_start_1
    invoke-virtual {v1}, Ljava/io/Reader;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 60
    :goto_0
    sget-object v2, Lcom/samsung/musicplus/mediainfo/findtag/NetworkResponseParser;->mNetworkResponseData:Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;

    return-object v2

    .line 55
    :catch_0
    move-exception v0

    .line 56
    .local v0, "e":Ljava/io/IOException;
    sget-object v2, Lcom/samsung/musicplus/mediainfo/findtag/NetworkResponseParser;->mNetworkResponseData:Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;

    iput-boolean v4, v2, Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;->isError:Z

    .line 57
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 53
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v2

    .line 54
    :try_start_2
    invoke-virtual {v1}, Ljava/io/Reader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 58
    :goto_1
    throw v2

    .line 55
    :catch_1
    move-exception v0

    .line 56
    .restart local v0    # "e":Ljava/io/IOException;
    sget-object v3, Lcom/samsung/musicplus/mediainfo/findtag/NetworkResponseParser;->mNetworkResponseData:Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;

    iput-boolean v4, v3, Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;->isError:Z

    .line 57
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method

.method public static parseResponse(Ljava/lang/String;)Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;
    .locals 5
    .param p0, "response"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    .line 32
    new-instance v2, Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;

    invoke-direct {v2}, Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;-><init>()V

    sput-object v2, Lcom/samsung/musicplus/mediainfo/findtag/NetworkResponseParser;->mNetworkResponseData:Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;

    .line 33
    new-instance v1, Ljava/io/StringReader;

    invoke-direct {v1, p0}, Ljava/io/StringReader;-><init>(Ljava/lang/String;)V

    .line 35
    .local v1, "reader":Ljava/io/Reader;
    :try_start_0
    invoke-static {v1}, Lcom/samsung/musicplus/mediainfo/findtag/NetworkResponseParser;->parseXML(Ljava/io/Reader;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 38
    :try_start_1
    invoke-virtual {v1}, Ljava/io/Reader;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 44
    :goto_0
    sget-object v2, Lcom/samsung/musicplus/mediainfo/findtag/NetworkResponseParser;->mNetworkResponseData:Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;

    return-object v2

    .line 39
    :catch_0
    move-exception v0

    .line 40
    .local v0, "e":Ljava/io/IOException;
    sget-object v2, Lcom/samsung/musicplus/mediainfo/findtag/NetworkResponseParser;->mNetworkResponseData:Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;

    iput-boolean v4, v2, Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;->isError:Z

    .line 41
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 37
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v2

    .line 38
    :try_start_2
    invoke-virtual {v1}, Ljava/io/Reader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    .line 42
    :goto_1
    throw v2

    .line 39
    :catch_1
    move-exception v0

    .line 40
    .restart local v0    # "e":Ljava/io/IOException;
    sget-object v3, Lcom/samsung/musicplus/mediainfo/findtag/NetworkResponseParser;->mNetworkResponseData:Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;

    iput-boolean v4, v3, Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;->isError:Z

    .line 41
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1
.end method

.method private static parseStartTag(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p0, "current"    # Ljava/lang/String;
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    .line 210
    const-string v0, "/shazamResponse@timestamp"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 211
    sget-object v0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkResponseParser;->mNetworkResponseData:Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;

    iput-object p1, v0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;->timeStamp:Ljava/lang/String;

    .line 275
    :cond_0
    :goto_0
    return-void

    .line 212
    :cond_1
    const-string v0, "/shazamResponse/requestConfig1/elements/config@key"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 213
    const-string v0, "service"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 214
    sput v1, Lcom/samsung/musicplus/mediainfo/findtag/NetworkResponseParser;->mRequestConfigTag:I

    goto :goto_0

    .line 215
    :cond_2
    const-string v0, "chart"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 216
    const/4 v0, 0x2

    sput v0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkResponseParser;->mRequestConfigTag:I

    goto :goto_0

    .line 218
    :cond_3
    const-string v0, "/shazamResponse/requestConfig1/elements/config@value"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 221
    sget v0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkResponseParser;->mRequestConfigTag:I

    packed-switch v0, :pswitch_data_0

    .line 242
    :goto_1
    const/4 v0, -0x1

    sput v0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkResponseParser;->mRequestConfigTag:I

    goto :goto_0

    .line 223
    :pswitch_0
    sget-object v0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkResponseParser;->mNetworkResponseData:Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;

    iget-object v0, v0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;->requestConfig:Ljava/util/HashMap;

    const-string v1, "service"

    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 232
    :pswitch_1
    sget-object v0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkResponseParser;->mNetworkResponseData:Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;

    iget-object v0, v0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;->requestConfig:Ljava/util/HashMap;

    const-string v1, "chart"

    invoke-virtual {v0, v1, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 243
    :cond_4
    const-string v0, "/shazamResponse/doRecognition1/requestId@id"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 245
    sget-object v0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkResponseParser;->mNetworkResponseData:Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;

    iput-object p1, v0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;->requestId:Ljava/lang/String;

    goto :goto_0

    .line 251
    :cond_5
    const-string v0, "/shazamResponse/error@code"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 252
    sget-object v0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkResponseParser;->mNetworkResponseData:Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;

    iput-boolean v1, v0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;->isError:Z

    .line 253
    sget-object v0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkResponseParser;->mNetworkResponseData:Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;

    iput-object p1, v0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;->errorCode:Ljava/lang/String;

    goto :goto_0

    .line 255
    :cond_6
    const-string v0, "/shazamResponse/error@message"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 256
    sget-object v0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkResponseParser;->mNetworkResponseData:Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;

    iput-object p1, v0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;->errorMessage:Ljava/lang/String;

    goto :goto_0

    .line 257
    :cond_7
    const-string v0, "/shazamResponse/requestResult1/tracks/track@id"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 258
    new-instance v0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$Track;

    invoke-direct {v0}, Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$Track;-><init>()V

    sput-object v0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkResponseParser;->mTrack:Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$Track;

    goto :goto_0

    .line 259
    :cond_8
    const-string v0, "/shazamResponse/requestResult1/tracks/track/tartists/tartist@id"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 260
    sget-object v0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkResponseParser;->mTrack:Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$Track;

    if-eqz v0, :cond_0

    .line 261
    new-instance v0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$Track$Artist;

    sget-object v1, Lcom/samsung/musicplus/mediainfo/findtag/NetworkResponseParser;->mTrack:Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$Track;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {v0, v1}, Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$Track$Artist;-><init>(Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$Track;)V

    sput-object v0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkResponseParser;->mArtist:Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$Track$Artist;

    .line 262
    sget-object v0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkResponseParser;->mArtist:Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$Track$Artist;

    iput-object p1, v0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$Track$Artist;->id:Ljava/lang/String;

    goto/16 :goto_0

    .line 264
    :cond_9
    const-string v0, "/shazamResponse/requestResults1/tracks/track/findTag/track@amgid"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 265
    new-instance v0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$FindTag;

    invoke-direct {v0}, Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$FindTag;-><init>()V

    sput-object v0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkResponseParser;->mFindTag:Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$FindTag;

    goto/16 :goto_0

    .line 266
    :cond_a
    const-string v0, "/shazamResponse/requestResults1/tracks/track/findTag/track@href"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 267
    sget-object v0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkResponseParser;->mFindTag:Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$FindTag;

    if-eqz v0, :cond_0

    .line 268
    sget-object v0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkResponseParser;->mFindTag:Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$FindTag;

    iput-object p1, v0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$FindTag;->url:Ljava/lang/String;

    goto/16 :goto_0

    .line 270
    :cond_b
    const-string v0, "/shazamResponse/requestResults1/tracks/track/findTag/track/Album/AlbumSimpleInfo/Artwork@uri"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 271
    sget-object v0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkResponseParser;->mFindTag:Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$FindTag;

    if-eqz v0, :cond_0

    .line 272
    sget-object v0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkResponseParser;->mFindTag:Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$FindTag;

    iput-object p1, v0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$FindTag;->albumArtUrl:Ljava/lang/String;

    goto/16 :goto_0

    .line 221
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static parseTag(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p0, "current"    # Ljava/lang/String;
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 282
    const-string v0, ""

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 287
    const-string v0, "Error"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 288
    sget-object v0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkResponseParser;->mNetworkResponseData:Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;->isError:Z

    .line 334
    :cond_0
    :goto_0
    return-void

    .line 297
    :cond_1
    const-string v0, "/shazamResponse/error"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 298
    sget-object v0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkResponseParser;->mNetworkResponseData:Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;

    iput-object p1, v0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;->errorText:Ljava/lang/String;

    goto :goto_0

    .line 299
    :cond_2
    const-string v0, "/shazamResponse/requestResult1/tracks/track/ttitle"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 300
    sget-object v0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkResponseParser;->mTrack:Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$Track;

    if-eqz v0, :cond_0

    .line 301
    sget-object v0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkResponseParser;->mTrack:Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$Track;

    iput-object p1, v0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$Track;->title:Ljava/lang/String;

    goto :goto_0

    .line 303
    :cond_3
    const-string v0, "/shazamResponse/requestResult1/tracks/track/tartists/tartist"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 304
    sget-object v0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkResponseParser;->mArtist:Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$Track$Artist;

    if-eqz v0, :cond_0

    .line 305
    sget-object v0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkResponseParser;->mArtist:Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$Track$Artist;

    iput-object p1, v0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$Track$Artist;->name:Ljava/lang/String;

    goto :goto_0

    .line 307
    :cond_4
    const-string v0, "/shazamResponse/requestResult1/tracks/track/tcoverurl"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 308
    sget-object v0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkResponseParser;->mTrack:Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$Track;

    if-eqz v0, :cond_0

    .line 309
    sget-object v0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkResponseParser;->mTrack:Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$Track;

    iput-object p1, v0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$Track;->coverUrl:Ljava/lang/String;

    goto :goto_0

    .line 311
    :cond_5
    const-string v0, "/shazamResponse/requestResult1/tracks/track/talbum"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 312
    sget-object v0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkResponseParser;->mTrack:Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$Track;

    if-eqz v0, :cond_0

    .line 313
    sget-object v0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkResponseParser;->mTrack:Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$Track;

    iput-object p1, v0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$Track;->album:Ljava/lang/String;

    goto :goto_0

    .line 315
    :cond_6
    const-string v0, "/shazamResponse/requestResult1/tracks/track/tgenre"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 316
    sget-object v0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkResponseParser;->mTrack:Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$Track;

    if-eqz v0, :cond_0

    .line 317
    sget-object v0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkResponseParser;->mTrack:Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$Track;

    iput-object p1, v0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$Track;->genre:Ljava/lang/String;

    goto :goto_0

    .line 319
    :cond_7
    const-string v0, "/shazamResponse/requestResult1/tracks/track/trecommendations"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 320
    sget-object v0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkResponseParser;->mTrack:Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$Track;

    if-eqz v0, :cond_0

    .line 321
    sget-object v0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkResponseParser;->mTrack:Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$Track;

    iput-object p1, v0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$Track;->recommendation:Ljava/lang/String;

    goto :goto_0

    .line 323
    :cond_8
    const-string v0, "/shazamResponse/requestResults1/tracks/track/findTag/EncryptedData/CipherData/CipherValue"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 324
    sget-object v0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkResponseParser;->mNetworkResponseData:Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;

    iput-object p1, v0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;->encryptedData:Ljava/lang/String;

    goto :goto_0

    .line 325
    :cond_9
    const-string v0, "/shazamResponse/requestResults1/tracks/track/findTag/track/Album/AlbumSimpleInfo/Title"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 326
    sget-object v0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkResponseParser;->mFindTag:Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$FindTag;

    if-eqz v0, :cond_0

    .line 327
    sget-object v0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkResponseParser;->mFindTag:Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$FindTag;

    iput-object p1, v0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$FindTag;->trackName:Ljava/lang/String;

    goto/16 :goto_0

    .line 329
    :cond_a
    const-string v0, "/shazamResponse/requestResults1/tracks/track/findTag/track/Album/AlbumSimpleInfo/Artist/Name"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 330
    sget-object v0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkResponseParser;->mFindTag:Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$FindTag;

    if-eqz v0, :cond_0

    .line 331
    sget-object v0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkResponseParser;->mFindTag:Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$FindTag;

    iput-object p1, v0, Lcom/samsung/musicplus/mediainfo/findtag/NetworkData$FindTag;->artistName:Ljava/lang/String;

    goto/16 :goto_0
.end method

.method private static parseXML(Ljava/io/Reader;)V
    .locals 9
    .param p0, "reader"    # Ljava/io/Reader;

    .prologue
    const/4 v8, 0x1

    .line 66
    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v3

    .line 67
    .local v3, "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    const/4 v6, 0x1

    invoke-virtual {v3, v6}, Lorg/xmlpull/v1/XmlPullParserFactory;->setNamespaceAware(Z)V

    .line 68
    invoke-virtual {v3}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v5

    .line 69
    .local v5, "xpp":Lorg/xmlpull/v1/XmlPullParser;
    invoke-interface {v5, p0}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 71
    new-instance v0, Ljava/lang/StringBuffer;

    const-string v6, ""

    invoke-direct {v0, v6}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 72
    .local v0, "current":Ljava/lang/StringBuffer;
    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v2

    .line 73
    .local v2, "eventType":I
    :goto_0
    if-eq v2, v8, :cond_2

    .line 74
    packed-switch v2, :pswitch_data_0

    .line 129
    :cond_0
    :goto_1
    :pswitch_0
    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v2

    goto :goto_0

    .line 84
    :pswitch_1
    const-string v6, "/"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 85
    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 92
    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeCount()I

    move-result v6

    if-eqz v6, :cond_1

    .line 93
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_2
    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeCount()I

    move-result v6

    if-ge v4, v6, :cond_0

    .line 100
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "@"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {v5, v4}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeName(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5, v4}, Lorg/xmlpull/v1/XmlPullParser;->getAttributeValue(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/musicplus/mediainfo/findtag/NetworkResponseParser;->parseStartTag(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 104
    .end local v4    # "i":I
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    const-string v7, ""

    invoke-static {v6, v7}, Lcom/samsung/musicplus/mediainfo/findtag/NetworkResponseParser;->parseStartTag(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_1

    .line 131
    .end local v0    # "current":Ljava/lang/StringBuffer;
    .end local v2    # "eventType":I
    .end local v3    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v5    # "xpp":Lorg/xmlpull/v1/XmlPullParser;
    :catch_0
    move-exception v1

    .line 132
    .local v1, "e":Lorg/xmlpull/v1/XmlPullParserException;
    invoke-virtual {v1}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V

    .line 138
    .end local v1    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    :cond_2
    :goto_3
    return-void

    .line 108
    .restart local v0    # "current":Ljava/lang/StringBuffer;
    .restart local v2    # "eventType":I
    .restart local v3    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .restart local v5    # "xpp":Lorg/xmlpull/v1/XmlPullParser;
    :pswitch_2
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/musicplus/mediainfo/findtag/NetworkResponseParser;->parseEndTag(Ljava/lang/String;)V

    .line 110
    const-string v6, "/"

    invoke-virtual {v0, v6}, Ljava/lang/StringBuffer;->lastIndexOf(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->length()I

    move-result v7

    invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;
    :try_end_1
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_1

    .line 133
    .end local v0    # "current":Ljava/lang/StringBuffer;
    .end local v2    # "eventType":I
    .end local v3    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v5    # "xpp":Lorg/xmlpull/v1/XmlPullParser;
    :catch_1
    move-exception v1

    .line 134
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 120
    .end local v1    # "e":Ljava/io/IOException;
    .restart local v0    # "current":Ljava/lang/StringBuffer;
    .restart local v2    # "eventType":I
    .restart local v3    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .restart local v5    # "xpp":Lorg/xmlpull/v1/XmlPullParser;
    :pswitch_3
    :try_start_2
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v5}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/musicplus/mediainfo/findtag/NetworkResponseParser;->parseTag(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_1

    .line 135
    .end local v0    # "current":Ljava/lang/StringBuffer;
    .end local v2    # "eventType":I
    .end local v3    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v5    # "xpp":Lorg/xmlpull/v1/XmlPullParser;
    :catch_2
    move-exception v1

    .line 136
    .local v1, "e":Ljava/lang/NullPointerException;
    invoke-virtual {v1}, Ljava/lang/NullPointerException;->printStackTrace()V

    goto :goto_3

    .line 74
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
