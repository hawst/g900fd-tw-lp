.class Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity$3;
.super Ljava/lang/Object;
.source "MediaInfoEditActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;)V
    .locals 0

    .prologue
    .line 287
    iput-object p1, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity$3;->this$0:Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 9
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const v8, 0x7f0d00e2

    .line 291
    if-nez p2, :cond_1

    .line 339
    :cond_0
    :goto_0
    return-void

    .line 295
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v6

    const v7, 0x7f0d00d6

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/EditText;

    .line 297
    .local v5, "titleField":Landroid/widget/EditText;
    invoke-virtual {p2}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v6

    const v7, 0x7f0d00d5

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    .line 299
    .local v3, "artistField":Landroid/widget/EditText;
    invoke-virtual {p2}, Landroid/view/View;->getRootView()Landroid/view/View;

    move-result-object v6

    const v7, 0x7f0d00d7

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    .line 302
    .local v1, "albumField":Landroid/widget/EditText;
    iget-object v6, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity$3;->this$0:Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;

    # getter for: Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mMetaRetriever:Lcom/samsung/musicplus/library/metadata/SecMediaMetaDataRetriever;
    invoke-static {v6}, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->access$200(Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;)Lcom/samsung/musicplus/library/metadata/SecMediaMetaDataRetriever;

    move-result-object v6

    if-eqz v6, :cond_4

    iget-object v6, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity$3;->this$0:Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;

    # getter for: Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mMetaRetriever:Lcom/samsung/musicplus/library/metadata/SecMediaMetaDataRetriever;
    invoke-static {v6}, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->access$200(Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;)Lcom/samsung/musicplus/library/metadata/SecMediaMetaDataRetriever;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/musicplus/library/metadata/SecMediaMetaDataRetriever;->isUnicode()Z

    move-result v6

    if-nez v6, :cond_4

    if-eqz p3, :cond_4

    .line 303
    iget-object v6, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity$3;->this$0:Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;

    # getter for: Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mMetaData:Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;
    invoke-static {v6}, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->access$300(Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;)Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;

    move-result-object v6

    iget-object v6, v6, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;->data:Ljava/lang/String;

    if-eqz v6, :cond_0

    .line 306
    iget-object v6, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity$3;->this$0:Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;

    # getter for: Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mMetaData:Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;
    invoke-static {v6}, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->access$300(Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;)Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;

    move-result-object v6

    iget-object v6, v6, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;->data:Ljava/lang/String;

    const-string v7, "audio/mpeg"

    invoke-static {v6, v7}, Lcom/samsung/musicplus/library/metadata/MetaDataConverter;->openFile(Ljava/lang/String;Ljava/lang/String;)I

    .line 308
    iget-object v6, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity$3;->this$0:Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;

    const/4 v7, 0x0

    # invokes: Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->getMetadatabyCharset(II)Ljava/lang/CharSequence;
    invoke-static {v6, v7, p3}, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->access$400(Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;II)Ljava/lang/CharSequence;

    move-result-object v4

    .line 309
    .local v4, "title":Ljava/lang/CharSequence;
    if-eqz v5, :cond_2

    invoke-virtual {v5}, Landroid/widget/EditText;->getVisibility()I

    move-result v6

    if-nez v6, :cond_2

    if-eqz v4, :cond_2

    .line 310
    invoke-virtual {v5, v4}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 311
    invoke-virtual {v5}, Landroid/widget/EditText;->length()I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/widget/EditText;->setSelection(I)V

    .line 314
    :cond_2
    iget-object v6, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity$3;->this$0:Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;

    const/4 v7, 0x1

    # invokes: Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->getMetadatabyCharset(II)Ljava/lang/CharSequence;
    invoke-static {v6, v7, p3}, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->access$400(Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;II)Ljava/lang/CharSequence;

    move-result-object v2

    .line 315
    .local v2, "artist":Ljava/lang/CharSequence;
    if-eqz v3, :cond_3

    invoke-virtual {v3}, Landroid/widget/EditText;->getVisibility()I

    move-result v6

    if-nez v6, :cond_3

    if-eqz v2, :cond_3

    .line 316
    invoke-virtual {v3, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 317
    invoke-virtual {v3}, Landroid/widget/EditText;->length()I

    move-result v6

    invoke-virtual {v3, v6}, Landroid/widget/EditText;->setSelection(I)V

    .line 320
    :cond_3
    iget-object v6, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity$3;->this$0:Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;

    const/4 v7, 0x2

    # invokes: Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->getMetadatabyCharset(II)Ljava/lang/CharSequence;
    invoke-static {v6, v7, p3}, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->access$400(Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;II)Ljava/lang/CharSequence;

    move-result-object v0

    .line 321
    .local v0, "album":Ljava/lang/CharSequence;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/widget/EditText;->getVisibility()I

    move-result v6

    if-nez v6, :cond_0

    if-eqz v0, :cond_0

    .line 322
    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 323
    invoke-virtual {v1}, Landroid/widget/EditText;->length()I

    move-result v6

    invoke-virtual {v1, v6}, Landroid/widget/EditText;->setSelection(I)V

    goto/16 :goto_0

    .line 326
    .end local v0    # "album":Ljava/lang/CharSequence;
    .end local v2    # "artist":Ljava/lang/CharSequence;
    .end local v4    # "title":Ljava/lang/CharSequence;
    :cond_4
    if-eqz v5, :cond_5

    .line 327
    iget-object v6, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity$3;->this$0:Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;

    # getter for: Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mMetaData:Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;
    invoke-static {v6}, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->access$300(Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;)Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;

    move-result-object v6

    iget-object v6, v6, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;->title:Ljava/lang/String;

    invoke-virtual {v5, v6}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 328
    invoke-virtual {v5}, Landroid/widget/EditText;->length()I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/widget/EditText;->setSelection(I)V

    .line 330
    :cond_5
    if-eqz v3, :cond_6

    .line 331
    iget-object v6, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity$3;->this$0:Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;

    # getter for: Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mMetaData:Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;
    invoke-static {v6}, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->access$300(Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;)Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;

    move-result-object v6

    iget-object v6, v6, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;->artist:Ljava/lang/String;

    invoke-virtual {v3, v6}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 332
    invoke-virtual {v3}, Landroid/widget/EditText;->length()I

    move-result v6

    invoke-virtual {v3, v6}, Landroid/widget/EditText;->setSelection(I)V

    .line 334
    :cond_6
    if-eqz v1, :cond_0

    .line 335
    iget-object v6, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity$3;->this$0:Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;

    # getter for: Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mMetaData:Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;
    invoke-static {v6}, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->access$300(Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;)Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;

    move-result-object v6

    iget-object v6, v6, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;->album:Ljava/lang/String;

    invoke-virtual {v1, v6}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 336
    invoke-virtual {v1}, Landroid/widget/EditText;->length()I

    move-result v6

    invoke-virtual {v1, v6}, Landroid/widget/EditText;->setSelection(I)V

    goto/16 :goto_0
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 343
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    return-void
.end method
