.class public Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoDiscographyFragment;
.super Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoBaseFragment;
.source "MediaInfoDiscographyFragment.java"


# static fields
.field private static final MAX_RATING:I = 0x5


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoBaseFragment;-><init>()V

    return-void
.end method

.method private addAlbumItem(Landroid/graphics/Bitmap;Ljava/lang/String;Ljava/lang/String;II)Landroid/view/View;
    .locals 13
    .param p1, "bm"    # Landroid/graphics/Bitmap;
    .param p2, "artworkUri"    # Ljava/lang/String;
    .param p3, "text1"    # Ljava/lang/String;
    .param p4, "size"    # I
    .param p5, "rating"    # I

    .prologue
    .line 77
    invoke-virtual {p0}, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoDiscographyFragment;->getActivity()Landroid/app/Activity;

    move-result-object v9

    invoke-virtual {v9}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    const-string v10, "layout_inflater"

    invoke-virtual {v9, v10}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/LayoutInflater;

    .line 79
    .local v3, "inflater":Landroid/view/LayoutInflater;
    const v9, 0x7f040049

    const/4 v10, 0x0

    invoke-virtual {v3, v9, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v8

    .line 80
    .local v8, "v":Landroid/view/View;
    const v9, 0x7f0d00e3

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 81
    .local v1, "albumView":Landroid/widget/ImageView;
    const v9, 0x7f0d00e1

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 83
    .local v7, "titelView":Landroid/widget/TextView;
    if-eqz p1, :cond_1

    .line 84
    invoke-virtual {v1, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 88
    :cond_0
    :goto_0
    move-object/from16 v0, p3

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 91
    const v9, 0x7f0d00eb

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/LinearLayout;

    .line 92
    .local v4, "ratingView":Landroid/widget/LinearLayout;
    const/4 v9, 0x0

    invoke-virtual {v4, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 93
    move/from16 v0, p5

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoDiscographyFragment;->getRatingDrawableId(I)I

    move-result p5

    .line 94
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    move/from16 v0, p5

    if-ge v2, v0, :cond_2

    .line 95
    const v9, 0x7f04008a

    const/4 v10, 0x0

    invoke-virtual {v3, v9, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    .line 96
    .local v6, "starOn":Landroid/view/View;
    invoke-virtual {v4, v6}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 94
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 85
    .end local v2    # "i":I
    .end local v4    # "ratingView":Landroid/widget/LinearLayout;
    .end local v6    # "starOn":Landroid/view/View;
    :cond_1
    if-eqz p2, :cond_0

    const-string v9, ""

    invoke-virtual {v9, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_0

    .line 86
    new-instance v9, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoUtils$AlbumArtLoader;

    invoke-direct {v9}, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoUtils$AlbumArtLoader;-><init>()V

    const/4 v10, 0x3

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object v1, v10, v11

    const/4 v11, 0x1

    aput-object p2, v10, v11

    const/4 v11, 0x2

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-virtual {v9, v10}, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoUtils$AlbumArtLoader;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 98
    .restart local v2    # "i":I
    .restart local v4    # "ratingView":Landroid/widget/LinearLayout;
    :cond_2
    const/4 v2, 0x0

    :goto_2
    rsub-int/lit8 v9, p5, 0x5

    if-ge v2, v9, :cond_3

    .line 99
    const v9, 0x7f040089

    const/4 v10, 0x0

    invoke-virtual {v3, v9, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 100
    .local v5, "starOff":Landroid/view/View;
    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 98
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 103
    .end local v5    # "starOff":Landroid/view/View;
    :cond_3
    return-object v8
.end method

.method private getRatingDrawableId(I)I
    .locals 1
    .param p1, "rating"    # I

    .prologue
    .line 109
    if-gtz p1, :cond_1

    .line 110
    const/4 p1, 0x0

    .line 122
    :cond_0
    :goto_0
    return p1

    .line 111
    :cond_1
    const/4 v0, 0x1

    if-ne p1, v0, :cond_2

    .line 112
    const/4 p1, 0x1

    goto :goto_0

    .line 113
    :cond_2
    const/4 v0, 0x2

    if-lt p1, v0, :cond_3

    const/4 v0, 0x3

    if-gt p1, v0, :cond_3

    .line 114
    const/4 p1, 0x2

    goto :goto_0

    .line 115
    :cond_3
    const/4 v0, 0x4

    if-lt p1, v0, :cond_4

    const/4 v0, 0x5

    if-gt p1, v0, :cond_4

    .line 116
    const/4 p1, 0x3

    goto :goto_0

    .line 117
    :cond_4
    const/4 v0, 0x6

    if-lt p1, v0, :cond_5

    const/4 v0, 0x7

    if-gt p1, v0, :cond_5

    .line 118
    const/4 p1, 0x4

    goto :goto_0

    .line 119
    :cond_5
    const/16 v0, 0x8

    if-lt p1, v0, :cond_0

    const/16 v0, 0x9

    if-gt p1, v0, :cond_0

    .line 120
    const/4 p1, 0x5

    goto :goto_0
.end method

.method private setContents()V
    .locals 14

    .prologue
    const/4 v13, -0x1

    .line 35
    invoke-virtual {p0}, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoDiscographyFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v3, 0x7f0d00d3

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/view/ViewGroup;

    .line 37
    .local v12, "viewGroup":Landroid/view/ViewGroup;
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoDiscographyFragment;->mParsedData:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;

    if-nez v0, :cond_1

    .line 74
    :cond_0
    return-void

    .line 40
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoDiscographyFragment;->mParsedData:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;

    iget-object v0, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;->performerDetailInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$PerformerDetailInfo;

    iget-object v0, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$PerformerDetailInfo;->albums:Ljava/util/ArrayList;

    if-eqz v0, :cond_3

    .line 41
    invoke-virtual {p0}, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoDiscographyFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0c0099

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v4

    .line 43
    .local v4, "albumArtSize":I
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoDiscographyFragment;->mParsedData:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;

    iget-object v0, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;->performerDetailInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$PerformerDetailInfo;

    iget-object v0, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$PerformerDetailInfo;->albums:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v11

    .line 44
    .local v11, "size":I
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_0
    if-ge v10, v11, :cond_3

    .line 45
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoDiscographyFragment;->mParsedData:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;

    iget-object v0, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;->performerDetailInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$PerformerDetailInfo;

    iget-object v0, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$PerformerDetailInfo;->albums:Ljava/util/ArrayList;

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;

    .line 46
    .local v6, "albumInfo":Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoDiscographyFragment;->mParsedData:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;

    iget-object v0, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;->performerDetailInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$PerformerDetailInfo;

    iget-object v0, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$PerformerDetailInfo;->albums:Ljava/util/ArrayList;

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;

    iget-object v9, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;->artwork:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtworkInfo;

    .line 47
    .local v9, "artworkInfo":Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtworkInfo;
    const/4 v2, 0x0

    .line 48
    .local v2, "artworkUri":Ljava/lang/String;
    const/4 v8, 0x0

    .line 49
    .local v8, "artwork":Ljava/lang/String;
    const/4 v1, 0x0

    .line 50
    .local v1, "bm":Landroid/graphics/Bitmap;
    if-eqz v9, :cond_2

    .line 51
    iget-object v2, v9, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtworkInfo;->uri:Ljava/lang/String;

    .line 52
    iget-object v8, v9, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtworkInfo;->artwork:Ljava/lang/String;

    .line 53
    const-string v0, ""

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    if-eqz v8, :cond_2

    .line 54
    invoke-static {v8}, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoUtils;->getArtwork(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 57
    :cond_2
    iget-object v3, v6, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;->title:Ljava/lang/String;

    iget v5, v6, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;->rating:I

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoDiscographyFragment;->addAlbumItem(Landroid/graphics/Bitmap;Ljava/lang/String;Ljava/lang/String;II)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v12, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 44
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 62
    .end local v1    # "bm":Landroid/graphics/Bitmap;
    .end local v2    # "artworkUri":Ljava/lang/String;
    .end local v4    # "albumArtSize":I
    .end local v6    # "albumInfo":Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;
    .end local v8    # "artwork":Ljava/lang/String;
    .end local v9    # "artworkInfo":Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtworkInfo;
    .end local v10    # "i":I
    .end local v11    # "size":I
    :cond_3
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoDiscographyFragment;->mParsedData:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;

    iget-object v0, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;->performerDetailInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$PerformerDetailInfo;

    iget-object v0, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$PerformerDetailInfo;->similarArtists:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoDiscographyFragment;->mParsedData:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;

    iget-object v0, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;->performerDetailInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$PerformerDetailInfo;

    iget-object v0, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$PerformerDetailInfo;->similarArtists:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v11

    .line 64
    .restart local v11    # "size":I
    if-lez v11, :cond_0

    .line 66
    const v0, 0x7f10015b

    invoke-virtual {p0, v0, v13}, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoDiscographyFragment;->addSubTitle(II)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v12, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 68
    const/4 v10, 0x0

    .restart local v10    # "i":I
    :goto_1
    if-ge v10, v11, :cond_0

    .line 69
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoDiscographyFragment;->mParsedData:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;

    iget-object v0, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;->performerDetailInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$PerformerDetailInfo;

    iget-object v0, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$PerformerDetailInfo;->similarArtists:Ljava/util/ArrayList;

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    .line 70
    .local v7, "artistInfo":Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;
    iget-object v0, v7, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;->name:Ljava/lang/String;

    invoke-virtual {p0, v0, v13}, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoDiscographyFragment;->addTitleItem(Ljava/lang/String;I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v12, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 68
    add-int/lit8 v10, v10, 0x1

    goto :goto_1
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 30
    invoke-super {p0, p1}, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoBaseFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 31
    invoke-direct {p0}, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoDiscographyFragment;->setContents()V

    .line 32
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 25
    const v0, 0x7f040042

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
