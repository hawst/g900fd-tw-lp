.class public Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoTrackListFragment;
.super Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoBaseFragment;
.source "MediaInfoTrackListFragment.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoBaseFragment;-><init>()V

    return-void
.end method

.method private addAlbumItem(Landroid/graphics/Bitmap;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Landroid/view/View;
    .locals 10
    .param p1, "bm"    # Landroid/graphics/Bitmap;
    .param p2, "artworkUri"    # Ljava/lang/String;
    .param p3, "text1"    # Ljava/lang/String;
    .param p4, "text2"    # Ljava/lang/String;
    .param p5, "size"    # I

    .prologue
    const/4 v8, 0x0

    .line 83
    invoke-virtual {p0}, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoTrackListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 84
    .local v2, "context":Landroid/content/Context;
    const-string v6, "layout_inflater"

    invoke-virtual {v2, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/LayoutInflater;

    .line 86
    .local v3, "inflater":Landroid/view/LayoutInflater;
    const v6, 0x7f040049

    const/4 v7, 0x0

    invoke-virtual {v3, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 87
    .local v5, "v":Landroid/view/View;
    const v6, 0x7f0d00e3

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 88
    .local v0, "albumView":Landroid/widget/ImageView;
    const v6, 0x7f0d00e1

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 89
    .local v4, "titleView":Landroid/widget/TextView;
    const v6, 0x7f0d00e2

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 90
    .local v1, "artistView":Landroid/widget/TextView;
    invoke-virtual {v1, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 92
    if-eqz p1, :cond_1

    .line 93
    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 98
    :cond_0
    :goto_0
    invoke-static {v2, p3}, Lcom/samsung/musicplus/util/UiUtils;->transUnknownString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 99
    invoke-static {v2, p4}, Lcom/samsung/musicplus/util/UiUtils;->transUnknownString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 101
    return-object v5

    .line 94
    :cond_1
    if-eqz p2, :cond_0

    const-string v6, ""

    invoke-virtual {v6, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 95
    new-instance v6, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoUtils$AlbumArtLoader;

    invoke-direct {v6}, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoUtils$AlbumArtLoader;-><init>()V

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    aput-object v0, v7, v8

    const/4 v8, 0x1

    aput-object p2, v7, v8

    const/4 v8, 0x2

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v7, v8

    invoke-virtual {v6, v7}, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoUtils$AlbumArtLoader;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method private setContents()V
    .locals 15

    .prologue
    .line 33
    invoke-virtual {p0}, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoTrackListFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v3, 0x7f0d00d3

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/view/ViewGroup;

    .line 35
    .local v14, "viewGroup":Landroid/view/ViewGroup;
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoTrackListFragment;->mParsedData:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;

    if-nez v0, :cond_1

    .line 80
    :cond_0
    return-void

    .line 39
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoTrackListFragment;->mParsedData:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;

    iget-object v0, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;->albumDetailInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;

    iget-object v0, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;->discs:Ljava/util/ArrayList;

    if-eqz v0, :cond_3

    .line 40
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoTrackListFragment;->mParsedData:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;

    iget-object v0, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;->albumDetailInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;

    iget-object v0, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;->discs:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v11

    .line 41
    .local v11, "size":I
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    if-ge v9, v11, :cond_3

    .line 42
    const v3, 0x7f100058

    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoTrackListFragment;->mParsedData:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;

    iget-object v0, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;->albumDetailInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;

    iget-object v0, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;->discs:Ljava/util/ArrayList;

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$Disc;

    iget v0, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$Disc;->currentMediaNumber:I

    invoke-virtual {p0, v3, v0}, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoTrackListFragment;->addSubTitle(II)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v14, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 46
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoTrackListFragment;->mParsedData:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;

    iget-object v0, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;->albumDetailInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;

    iget-object v0, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;->discs:Ljava/util/ArrayList;

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$Disc;

    iget-object v0, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$Disc;->tracks:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v13

    .line 47
    .local v13, "trackSize":I
    const/4 v10, 0x0

    .local v10, "j":I
    :goto_1
    if-ge v10, v13, :cond_2

    .line 48
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoTrackListFragment;->mParsedData:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;

    iget-object v0, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;->albumDetailInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;

    iget-object v0, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;->discs:Ljava/util/ArrayList;

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$Disc;

    iget-object v0, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$Disc;->tracks:Ljava/util/ArrayList;

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;

    .line 49
    .local v12, "trackInfo":Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;
    iget-object v0, v12, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;->title:Ljava/lang/String;

    add-int/lit8 v3, v10, 0x1

    invoke-virtual {p0, v0, v3}, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoTrackListFragment;->addTitleItem(Ljava/lang/String;I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v14, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 47
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    .line 41
    .end local v12    # "trackInfo":Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$TrackInfo;
    :cond_2
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 54
    .end local v9    # "i":I
    .end local v10    # "j":I
    .end local v11    # "size":I
    .end local v13    # "trackSize":I
    :cond_3
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoTrackListFragment;->mParsedData:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;

    iget-object v0, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;->albumDetailInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;

    iget-object v0, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;->similarAlbums:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 55
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoTrackListFragment;->mParsedData:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;

    iget-object v0, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;->albumDetailInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;

    iget-object v0, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;->similarAlbums:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v11

    .line 56
    .restart local v11    # "size":I
    if-lez v11, :cond_0

    .line 58
    const v0, 0x7f10015a

    const/4 v3, -0x1

    invoke-virtual {p0, v0, v3}, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoTrackListFragment;->addSubTitle(II)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v14, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 60
    invoke-virtual {p0}, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoTrackListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v3, 0x7f0c0099

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v5

    .line 62
    .local v5, "albumArtSize":I
    const/4 v9, 0x0

    .restart local v9    # "i":I
    :goto_2
    if-ge v9, v11, :cond_0

    .line 63
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoTrackListFragment;->mParsedData:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;

    iget-object v0, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;->albumDetailInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;

    iget-object v0, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;->similarAlbums:Ljava/util/ArrayList;

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;

    .line 64
    .local v6, "albumInfo":Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoTrackListFragment;->mParsedData:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;

    iget-object v0, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;->albumDetailInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;

    iget-object v0, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;->similarAlbums:Ljava/util/ArrayList;

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;

    iget-object v8, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;->artwork:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtworkInfo;

    .line 65
    .local v8, "artworkInfo":Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtworkInfo;
    const/4 v2, 0x0

    .line 66
    .local v2, "artworkUri":Ljava/lang/String;
    const/4 v7, 0x0

    .line 67
    .local v7, "artwork":Ljava/lang/String;
    const/4 v1, 0x0

    .line 68
    .local v1, "bm":Landroid/graphics/Bitmap;
    if-eqz v8, :cond_4

    .line 69
    iget-object v2, v8, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtworkInfo;->uri:Ljava/lang/String;

    .line 70
    iget-object v7, v8, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtworkInfo;->artwork:Ljava/lang/String;

    .line 71
    if-eqz v7, :cond_4

    const-string v0, ""

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 72
    invoke-static {v7}, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoUtils;->getArtwork(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 75
    :cond_4
    iget-object v3, v6, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;->title:Ljava/lang/String;

    iget-object v0, v6, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;->artist:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;

    iget-object v4, v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ArtistInfo;->name:Ljava/lang/String;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoTrackListFragment;->addAlbumItem(Landroid/graphics/Bitmap;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v14, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 62
    add-int/lit8 v9, v9, 0x1

    goto :goto_2
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 28
    invoke-super {p0, p1}, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoBaseFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 29
    invoke-direct {p0}, Lcom/samsung/musicplus/mediainfo/richinfo/MediaInfoTrackListFragment;->setContents()V

    .line 30
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 23
    const v0, 0x7f040042

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
