.class public Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;
.super Ljava/lang/Object;
.source "MediaInfoUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/mediainfo/MediaInfoUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MetaData"
.end annotation


# instance fields
.field public album:Ljava/lang/String;

.field public albumId:J

.field public artist:Ljava/lang/String;

.field public bitDepth:I

.field public data:Ljava/lang/String;

.field public duration:J

.field public genre:Ljava/lang/String;

.field public mimeType:Ljava/lang/String;

.field public recordingType:I

.field public samplingRate:I

.field public size:J

.field public title:Ljava/lang/String;

.field public track:Ljava/lang/String;

.field public year:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
