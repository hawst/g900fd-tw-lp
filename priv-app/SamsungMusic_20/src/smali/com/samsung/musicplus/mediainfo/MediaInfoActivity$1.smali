.class Lcom/samsung/musicplus/mediainfo/MediaInfoActivity$1;
.super Landroid/database/ContentObserver;
.source "MediaInfoActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;Landroid/os/Handler;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Handler;

    .prologue
    .line 124
    iput-object p1, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity$1;->this$0:Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 3
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    .line 131
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity$1;->this$0:Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;

    iget-object v1, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity$1;->this$0:Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;

    invoke-virtual {v1}, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity$1;->this$0:Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;

    # getter for: Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mBaseUri:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->access$100(Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils;->getMetaData(Landroid/content/Context;Landroid/net/Uri;)Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;

    move-result-object v1

    # setter for: Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mMetaData:Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;
    invoke-static {v0, v1}, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->access$002(Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;)Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;

    .line 132
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity$1;->this$0:Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;

    # getter for: Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mMetaData:Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;
    invoke-static {v0}, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->access$000(Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;)Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;

    move-result-object v0

    if-nez v0, :cond_1

    .line 133
    # getter for: Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->access$200()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Meta info changed and original file does not exist!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity$1;->this$0:Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->finishActivity(I)V

    .line 135
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity$1;->this$0:Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;

    invoke-virtual {v0}, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->finish()V

    .line 141
    :cond_0
    :goto_0
    return-void

    .line 137
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity$1;->this$0:Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;

    # getter for: Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mAdapter:Lcom/samsung/musicplus/mediainfo/MediaInfoActivity$MediaInfoPagerAdapter;
    invoke-static {v0}, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->access$300(Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;)Lcom/samsung/musicplus/mediainfo/MediaInfoActivity$MediaInfoPagerAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 138
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity$1;->this$0:Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;

    # getter for: Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mAdapter:Lcom/samsung/musicplus/mediainfo/MediaInfoActivity$MediaInfoPagerAdapter;
    invoke-static {v0}, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->access$300(Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;)Lcom/samsung/musicplus/mediainfo/MediaInfoActivity$MediaInfoPagerAdapter;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity$1;->this$0:Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;

    # getter for: Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->mMetaData:Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;
    invoke-static {v1}, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;->access$000(Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;)Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity$MediaInfoPagerAdapter;->setMetaDataInfo(Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;)V

    goto :goto_0
.end method
