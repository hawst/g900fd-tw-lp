.class public Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumDetailInfo;
.super Ljava/lang/Object;
.source "RichInfoData.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AlbumDetailInfo"
.end annotation


# instance fields
.field public albumSimpleInfo:Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;

.field copyRight:Ljava/lang/String;

.field public credits:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$Credit;",
            ">;"
        }
    .end annotation
.end field

.field date:Ljava/lang/String;

.field public discs:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$Disc;",
            ">;"
        }
    .end annotation
.end field

.field genreDesc:Ljava/lang/String;

.field releases:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ReleaseInfo;",
            ">;"
        }
    .end annotation
.end field

.field public review:Ljava/lang/String;

.field public similarAlbums:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$AlbumInfo;",
            ">;"
        }
    .end annotation
.end field

.field styles:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$StyleInfo;",
            ">;"
        }
    .end annotation
.end field

.field themeInfo:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field tones:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoData$ToneInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
