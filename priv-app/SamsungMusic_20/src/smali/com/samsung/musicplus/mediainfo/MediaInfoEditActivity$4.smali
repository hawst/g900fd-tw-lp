.class Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity$4;
.super Ljava/lang/Object;
.source "MediaInfoEditActivity.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;)V
    .locals 0

    .prologue
    .line 391
    iput-object p1, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity$4;->this$0:Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .param p1, "s"    # Landroid/text/Editable;

    .prologue
    .line 415
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "count"    # I
    .param p4, "after"    # I

    .prologue
    .line 410
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 3
    .param p1, "s"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "before"    # I
    .param p4, "count"    # I

    .prologue
    .line 395
    iget-object v1, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity$4;->this$0:Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;

    # getter for: Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mTitle:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->access$500(Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 401
    .local v0, "title":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity$4;->this$0:Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;

    # getter for: Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mTitle:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->access$500(Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;)Landroid/widget/EditText;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/EditText;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_0

    .line 402
    iget-object v1, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity$4;->this$0:Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;

    # getter for: Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mCustomDone:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->access$600(Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;)Landroid/widget/TextView;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 406
    :goto_0
    return-void

    .line 404
    :cond_0
    iget-object v1, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity$4;->this$0:Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;

    # getter for: Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mCustomDone:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->access$600(Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;)Landroid/widget/TextView;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setEnabled(Z)V

    goto :goto_0
.end method
