.class public Lcom/samsung/musicplus/mediainfo/crypto/IceCrypt;
.super Ljava/lang/Object;
.source "IceCrypt.java"


# static fields
.field private static final CLASSTAG:Ljava/lang/String;

.field private static final ENCTYPT:Z = true

.field private static final KEY:[B

.field private static final LEVEL:I = 0x1


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-class v0, Lcom/samsung/musicplus/mediainfo/crypto/IceCrypt;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/mediainfo/crypto/IceCrypt;->CLASSTAG:Ljava/lang/String;

    .line 22
    const/16 v0, 0x8

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/samsung/musicplus/mediainfo/crypto/IceCrypt;->KEY:[B

    return-void

    :array_0
    .array-data 1
        0x25t
        -0x80t
        0x4ct
        -0x75t
        -0x4ft
        -0x51t
        -0x49t
        0x0t
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static build_iv(Lcom/samsung/musicplus/mediainfo/crypto/Block;)V
    .locals 3
    .param p0, "iv"    # Lcom/samsung/musicplus/mediainfo/crypto/Block;

    .prologue
    .line 204
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v1, 0x8

    if-ge v0, v1, :cond_0

    .line 205
    iget-object v1, p0, Lcom/samsung/musicplus/mediainfo/crypto/Block;->data:[B

    const/4 v2, 0x0

    aput-byte v2, v1, v0

    .line 204
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 207
    :cond_0
    return-void
.end method

.method private static decrypt_stream([BLjava/io/InputStream;Ljava/io/OutputStream;)Z
    .locals 11
    .param p0, "passwd"    # [B
    .param p1, "inf"    # Ljava/io/InputStream;
    .param p2, "outf"    # Ljava/io/OutputStream;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 212
    new-instance v3, Lcom/samsung/musicplus/mediainfo/crypto/Block;

    invoke-direct {v3}, Lcom/samsung/musicplus/mediainfo/crypto/Block;-><init>()V

    .line 213
    .local v3, "inbuf":Lcom/samsung/musicplus/mediainfo/crypto/Block;
    new-instance v5, Lcom/samsung/musicplus/mediainfo/crypto/Block;

    invoke-direct {v5}, Lcom/samsung/musicplus/mediainfo/crypto/Block;-><init>()V

    .line 217
    .local v5, "outbuf":Lcom/samsung/musicplus/mediainfo/crypto/Block;
    new-instance v2, Lcom/samsung/musicplus/mediainfo/crypto/IceKey;

    invoke-direct {v2, v7}, Lcom/samsung/musicplus/mediainfo/crypto/IceKey;-><init>(I)V

    .line 218
    .local v2, "ik":Lcom/samsung/musicplus/mediainfo/crypto/IceKey;
    invoke-virtual {v2, p0}, Lcom/samsung/musicplus/mediainfo/crypto/IceKey;->set([B)V

    .line 220
    const/4 v0, 0x0

    .line 222
    .local v0, "block_loaded":Z
    :goto_0
    invoke-virtual {v3, p1}, Lcom/samsung/musicplus/mediainfo/crypto/Block;->read(Ljava/io/InputStream;)I

    move-result v4

    .local v4, "n":I
    const/16 v8, 0x8

    if-ne v4, v8, :cond_2

    .line 223
    if-eqz v0, :cond_1

    invoke-virtual {v5, p2}, Lcom/samsung/musicplus/mediainfo/crypto/Block;->write(Ljava/io/OutputStream;)Z

    move-result v8

    if-nez v8, :cond_1

    .line 244
    :cond_0
    :goto_1
    return v6

    .line 226
    :cond_1
    iget-object v8, v3, Lcom/samsung/musicplus/mediainfo/crypto/Block;->data:[B

    iget-object v9, v5, Lcom/samsung/musicplus/mediainfo/crypto/Block;->data:[B

    invoke-virtual {v2, v8, v9}, Lcom/samsung/musicplus/mediainfo/crypto/IceKey;->decrypt([B[B)V

    .line 227
    const/4 v0, 0x1

    goto :goto_0

    .line 230
    :cond_2
    if-eqz v4, :cond_3

    .line 231
    const-string v8, "RichInfo"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v10, Lcom/samsung/musicplus/mediainfo/crypto/IceCrypt;->CLASSTAG:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "Warning: truncated final block = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 234
    :cond_3
    invoke-virtual {v5, p2}, Lcom/samsung/musicplus/mediainfo/crypto/Block;->write(Ljava/io/OutputStream;)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 239
    :try_start_0
    invoke-virtual {p2}, Ljava/io/OutputStream;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move v6, v7

    .line 244
    goto :goto_1

    .line 240
    :catch_0
    move-exception v1

    .line 241
    .local v1, "e":Ljava/io/IOException;
    goto :goto_1
.end method

.method public static decrypt_string(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "cyperText"    # Ljava/lang/String;

    .prologue
    .line 149
    invoke-virtual {p0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/musicplus/mediainfo/crypto/IceCrypt;->decrypt_string([B)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static decrypt_string([B)Ljava/lang/String;
    .locals 7
    .param p0, "cyperText"    # [B

    .prologue
    .line 153
    const/4 v0, 0x0

    .line 154
    .local v0, "decryptedString":Ljava/lang/String;
    const/4 v2, 0x0

    .line 155
    .local v2, "inputStream":Ljava/io/ByteArrayInputStream;
    const/4 v4, 0x0

    .line 158
    .local v4, "outputStream":Ljava/io/ByteArrayOutputStream;
    :try_start_0
    new-instance v3, Ljava/io/ByteArrayInputStream;

    invoke-direct {v3, p0}, Ljava/io/ByteArrayInputStream;-><init>([B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 159
    .end local v2    # "inputStream":Ljava/io/ByteArrayInputStream;
    .local v3, "inputStream":Ljava/io/ByteArrayInputStream;
    :try_start_1
    new-instance v5, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v5}, Ljava/io/ByteArrayOutputStream;-><init>()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_7
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 161
    .end local v4    # "outputStream":Ljava/io/ByteArrayOutputStream;
    .local v5, "outputStream":Ljava/io/ByteArrayOutputStream;
    :try_start_2
    sget-object v6, Lcom/samsung/musicplus/mediainfo/crypto/IceCrypt;->KEY:[B

    invoke-static {v6, v3, v5}, Lcom/samsung/musicplus/mediainfo/crypto/IceCrypt;->decrypt_stream([BLjava/io/InputStream;Ljava/io/OutputStream;)Z

    .line 163
    invoke-virtual {v5}, Ljava/io/ByteArrayOutputStream;->toString()Ljava/lang/String;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_8
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result-object v0

    .line 184
    if-eqz v3, :cond_0

    .line 185
    :try_start_3
    invoke-virtual {v3}, Ljava/io/ByteArrayInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    .line 191
    :cond_0
    :goto_0
    if-eqz v5, :cond_1

    .line 192
    :try_start_4
    invoke-virtual {v5}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    :cond_1
    move-object v4, v5

    .end local v5    # "outputStream":Ljava/io/ByteArrayOutputStream;
    .restart local v4    # "outputStream":Ljava/io/ByteArrayOutputStream;
    move-object v2, v3

    .line 199
    .end local v3    # "inputStream":Ljava/io/ByteArrayInputStream;
    .restart local v2    # "inputStream":Ljava/io/ByteArrayInputStream;
    :cond_2
    :goto_1
    return-object v0

    .line 187
    .end local v2    # "inputStream":Ljava/io/ByteArrayInputStream;
    .end local v4    # "outputStream":Ljava/io/ByteArrayOutputStream;
    .restart local v3    # "inputStream":Ljava/io/ByteArrayInputStream;
    .restart local v5    # "outputStream":Ljava/io/ByteArrayOutputStream;
    :catch_0
    move-exception v1

    .line 188
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 194
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 195
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    move-object v4, v5

    .end local v5    # "outputStream":Ljava/io/ByteArrayOutputStream;
    .restart local v4    # "outputStream":Ljava/io/ByteArrayOutputStream;
    move-object v2, v3

    .line 197
    .end local v3    # "inputStream":Ljava/io/ByteArrayInputStream;
    .restart local v2    # "inputStream":Ljava/io/ByteArrayInputStream;
    goto :goto_1

    .line 180
    .end local v1    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v1

    .line 181
    .local v1, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_5
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 184
    if-eqz v2, :cond_3

    .line 185
    :try_start_6
    invoke-virtual {v2}, Ljava/io/ByteArrayInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    .line 191
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_3
    :goto_3
    if-eqz v4, :cond_2

    .line 192
    :try_start_7
    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    goto :goto_1

    .line 194
    :catch_3
    move-exception v1

    .line 195
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 187
    .local v1, "e":Ljava/lang/Exception;
    :catch_4
    move-exception v1

    .line 188
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 183
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v6

    .line 184
    :goto_4
    if-eqz v2, :cond_4

    .line 185
    :try_start_8
    invoke-virtual {v2}, Ljava/io/ByteArrayInputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    .line 191
    :cond_4
    :goto_5
    if-eqz v4, :cond_5

    .line 192
    :try_start_9
    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_6

    .line 196
    :cond_5
    :goto_6
    throw v6

    .line 187
    :catch_5
    move-exception v1

    .line 188
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 194
    .end local v1    # "e":Ljava/io/IOException;
    :catch_6
    move-exception v1

    .line 195
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 183
    .end local v1    # "e":Ljava/io/IOException;
    .end local v2    # "inputStream":Ljava/io/ByteArrayInputStream;
    .restart local v3    # "inputStream":Ljava/io/ByteArrayInputStream;
    :catchall_1
    move-exception v6

    move-object v2, v3

    .end local v3    # "inputStream":Ljava/io/ByteArrayInputStream;
    .restart local v2    # "inputStream":Ljava/io/ByteArrayInputStream;
    goto :goto_4

    .end local v2    # "inputStream":Ljava/io/ByteArrayInputStream;
    .end local v4    # "outputStream":Ljava/io/ByteArrayOutputStream;
    .restart local v3    # "inputStream":Ljava/io/ByteArrayInputStream;
    .restart local v5    # "outputStream":Ljava/io/ByteArrayOutputStream;
    :catchall_2
    move-exception v6

    move-object v4, v5

    .end local v5    # "outputStream":Ljava/io/ByteArrayOutputStream;
    .restart local v4    # "outputStream":Ljava/io/ByteArrayOutputStream;
    move-object v2, v3

    .end local v3    # "inputStream":Ljava/io/ByteArrayInputStream;
    .restart local v2    # "inputStream":Ljava/io/ByteArrayInputStream;
    goto :goto_4

    .line 180
    .end local v2    # "inputStream":Ljava/io/ByteArrayInputStream;
    .restart local v3    # "inputStream":Ljava/io/ByteArrayInputStream;
    :catch_7
    move-exception v1

    move-object v2, v3

    .end local v3    # "inputStream":Ljava/io/ByteArrayInputStream;
    .restart local v2    # "inputStream":Ljava/io/ByteArrayInputStream;
    goto :goto_2

    .end local v2    # "inputStream":Ljava/io/ByteArrayInputStream;
    .end local v4    # "outputStream":Ljava/io/ByteArrayOutputStream;
    .restart local v3    # "inputStream":Ljava/io/ByteArrayInputStream;
    .restart local v5    # "outputStream":Ljava/io/ByteArrayOutputStream;
    :catch_8
    move-exception v1

    move-object v4, v5

    .end local v5    # "outputStream":Ljava/io/ByteArrayOutputStream;
    .restart local v4    # "outputStream":Ljava/io/ByteArrayOutputStream;
    move-object v2, v3

    .end local v3    # "inputStream":Ljava/io/ByteArrayInputStream;
    .restart local v2    # "inputStream":Ljava/io/ByteArrayInputStream;
    goto :goto_2
.end method

.method public static encryptByteToHexString([B)Ljava/lang/String;
    .locals 2
    .param p0, "plainByte"    # [B

    .prologue
    .line 51
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "#0x"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p0}, Lcom/samsung/musicplus/mediainfo/crypto/IceCrypt;->encrypt_byte([B)[B

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoUtils;->toHexString([B)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static encrypt_byte(Ljava/io/InputStream;)[B
    .locals 7
    .param p0, "inputStream"    # Ljava/io/InputStream;

    .prologue
    .line 106
    const/4 v2, 0x0

    .line 108
    .local v2, "encryptedByte":[B
    new-instance v0, Lcom/samsung/musicplus/mediainfo/crypto/IceKey;

    const/4 v6, 0x1

    invoke-direct {v0, v6}, Lcom/samsung/musicplus/mediainfo/crypto/IceKey;-><init>(I)V

    .line 109
    .local v0, "crypto":Lcom/samsung/musicplus/mediainfo/crypto/IceKey;
    sget-object v6, Lcom/samsung/musicplus/mediainfo/crypto/IceCrypt;->KEY:[B

    invoke-virtual {v0, v6}, Lcom/samsung/musicplus/mediainfo/crypto/IceKey;->set([B)V

    .line 111
    new-instance v3, Lcom/samsung/musicplus/mediainfo/crypto/Block;

    invoke-direct {v3}, Lcom/samsung/musicplus/mediainfo/crypto/Block;-><init>()V

    .line 112
    .local v3, "iv":Lcom/samsung/musicplus/mediainfo/crypto/Block;
    invoke-static {v3}, Lcom/samsung/musicplus/mediainfo/crypto/IceCrypt;->build_iv(Lcom/samsung/musicplus/mediainfo/crypto/Block;)V

    .line 115
    const/4 v4, 0x0

    .line 119
    .local v4, "outputStream":Ljava/io/ByteArrayOutputStream;
    :try_start_0
    new-instance v5, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v5}, Ljava/io/ByteArrayOutputStream;-><init>()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 121
    .end local v4    # "outputStream":Ljava/io/ByteArrayOutputStream;
    .local v5, "outputStream":Ljava/io/ByteArrayOutputStream;
    :try_start_1
    invoke-static {v0, v3, p0, v5}, Lcom/samsung/musicplus/mediainfo/crypto/IceCrypt;->encrypt_stream(Lcom/samsung/musicplus/mediainfo/crypto/IceKey;Lcom/samsung/musicplus/mediainfo/crypto/Block;Ljava/io/InputStream;Ljava/io/OutputStream;)Z

    .line 122
    invoke-virtual {v5}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_7
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v2

    .line 135
    :try_start_2
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 140
    :goto_0
    :try_start_3
    invoke-virtual {v5}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1

    move-object v4, v5

    .line 145
    .end local v5    # "outputStream":Ljava/io/ByteArrayOutputStream;
    .restart local v4    # "outputStream":Ljava/io/ByteArrayOutputStream;
    :goto_1
    return-object v2

    .line 136
    .end local v4    # "outputStream":Ljava/io/ByteArrayOutputStream;
    .restart local v5    # "outputStream":Ljava/io/ByteArrayOutputStream;
    :catch_0
    move-exception v1

    .line 137
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 141
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 142
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    move-object v4, v5

    .line 144
    .end local v5    # "outputStream":Ljava/io/ByteArrayOutputStream;
    .restart local v4    # "outputStream":Ljava/io/ByteArrayOutputStream;
    goto :goto_1

    .line 131
    .end local v1    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v1

    .line 132
    .local v1, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_4
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 135
    :try_start_5
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    .line 140
    .end local v1    # "e":Ljava/lang/Exception;
    :goto_3
    :try_start_6
    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    goto :goto_1

    .line 141
    :catch_3
    move-exception v1

    .line 142
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 136
    .local v1, "e":Ljava/lang/Exception;
    :catch_4
    move-exception v1

    .line 137
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 134
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v6

    .line 135
    :goto_4
    :try_start_7
    invoke-virtual {p0}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_5

    .line 140
    :goto_5
    :try_start_8
    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_6

    .line 143
    :goto_6
    throw v6

    .line 136
    :catch_5
    move-exception v1

    .line 137
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 141
    .end local v1    # "e":Ljava/io/IOException;
    :catch_6
    move-exception v1

    .line 142
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 134
    .end local v1    # "e":Ljava/io/IOException;
    .end local v4    # "outputStream":Ljava/io/ByteArrayOutputStream;
    .restart local v5    # "outputStream":Ljava/io/ByteArrayOutputStream;
    :catchall_1
    move-exception v6

    move-object v4, v5

    .end local v5    # "outputStream":Ljava/io/ByteArrayOutputStream;
    .restart local v4    # "outputStream":Ljava/io/ByteArrayOutputStream;
    goto :goto_4

    .line 131
    .end local v4    # "outputStream":Ljava/io/ByteArrayOutputStream;
    .restart local v5    # "outputStream":Ljava/io/ByteArrayOutputStream;
    :catch_7
    move-exception v1

    move-object v4, v5

    .end local v5    # "outputStream":Ljava/io/ByteArrayOutputStream;
    .restart local v4    # "outputStream":Ljava/io/ByteArrayOutputStream;
    goto :goto_2
.end method

.method public static encrypt_byte([B)[B
    .locals 9
    .param p0, "plainByte"    # [B

    .prologue
    .line 59
    const/4 v2, 0x0

    .line 61
    .local v2, "encryptedByte":[B
    new-instance v0, Lcom/samsung/musicplus/mediainfo/crypto/IceKey;

    const/4 v8, 0x1

    invoke-direct {v0, v8}, Lcom/samsung/musicplus/mediainfo/crypto/IceKey;-><init>(I)V

    .line 62
    .local v0, "crypto":Lcom/samsung/musicplus/mediainfo/crypto/IceKey;
    sget-object v8, Lcom/samsung/musicplus/mediainfo/crypto/IceCrypt;->KEY:[B

    invoke-virtual {v0, v8}, Lcom/samsung/musicplus/mediainfo/crypto/IceKey;->set([B)V

    .line 64
    new-instance v5, Lcom/samsung/musicplus/mediainfo/crypto/Block;

    invoke-direct {v5}, Lcom/samsung/musicplus/mediainfo/crypto/Block;-><init>()V

    .line 65
    .local v5, "iv":Lcom/samsung/musicplus/mediainfo/crypto/Block;
    invoke-static {v5}, Lcom/samsung/musicplus/mediainfo/crypto/IceCrypt;->build_iv(Lcom/samsung/musicplus/mediainfo/crypto/Block;)V

    .line 67
    const/4 v3, 0x0

    .line 68
    .local v3, "inputStream":Ljava/io/ByteArrayInputStream;
    const/4 v6, 0x0

    .line 71
    .local v6, "outputStream":Ljava/io/ByteArrayOutputStream;
    if-eqz p0, :cond_0

    .line 72
    :try_start_0
    new-instance v4, Ljava/io/ByteArrayInputStream;

    invoke-direct {v4, p0}, Ljava/io/ByteArrayInputStream;-><init>([B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 73
    .end local v3    # "inputStream":Ljava/io/ByteArrayInputStream;
    .local v4, "inputStream":Ljava/io/ByteArrayInputStream;
    :try_start_1
    new-instance v7, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v7}, Ljava/io/ByteArrayOutputStream;-><init>()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_7
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 75
    .end local v6    # "outputStream":Ljava/io/ByteArrayOutputStream;
    .local v7, "outputStream":Ljava/io/ByteArrayOutputStream;
    :try_start_2
    invoke-static {v0, v5, v4, v7}, Lcom/samsung/musicplus/mediainfo/crypto/IceCrypt;->encrypt_stream(Lcom/samsung/musicplus/mediainfo/crypto/IceKey;Lcom/samsung/musicplus/mediainfo/crypto/Block;Ljava/io/InputStream;Ljava/io/OutputStream;)Z

    .line 76
    invoke-virtual {v7}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_8
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result-object v2

    move-object v6, v7

    .end local v7    # "outputStream":Ljava/io/ByteArrayOutputStream;
    .restart local v6    # "outputStream":Ljava/io/ByteArrayOutputStream;
    move-object v3, v4

    .line 88
    .end local v4    # "inputStream":Ljava/io/ByteArrayInputStream;
    .restart local v3    # "inputStream":Ljava/io/ByteArrayInputStream;
    :cond_0
    if-eqz v3, :cond_1

    .line 89
    :try_start_3
    invoke-virtual {v3}, Ljava/io/ByteArrayInputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    .line 95
    :cond_1
    :goto_0
    if-eqz v6, :cond_2

    .line 96
    :try_start_4
    invoke-virtual {v6}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    .line 102
    :cond_2
    :goto_1
    return-object v2

    .line 91
    :catch_0
    move-exception v1

    .line 92
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 98
    .end local v1    # "e":Ljava/io/IOException;
    :catch_1
    move-exception v1

    .line 99
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 84
    .end local v1    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v1

    .line 85
    .local v1, "e":Ljava/lang/Exception;
    :goto_2
    :try_start_5
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 88
    if-eqz v3, :cond_3

    .line 89
    :try_start_6
    invoke-virtual {v3}, Ljava/io/ByteArrayInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_4

    .line 95
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_3
    :goto_3
    if-eqz v6, :cond_2

    .line 96
    :try_start_7
    invoke-virtual {v6}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    goto :goto_1

    .line 98
    :catch_3
    move-exception v1

    .line 99
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_1

    .line 91
    .local v1, "e":Ljava/lang/Exception;
    :catch_4
    move-exception v1

    .line 92
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 87
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v8

    .line 88
    :goto_4
    if-eqz v3, :cond_4

    .line 89
    :try_start_8
    invoke-virtual {v3}, Ljava/io/ByteArrayInputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    .line 95
    :cond_4
    :goto_5
    if-eqz v6, :cond_5

    .line 96
    :try_start_9
    invoke-virtual {v6}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_6

    .line 100
    :cond_5
    :goto_6
    throw v8

    .line 91
    :catch_5
    move-exception v1

    .line 92
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5

    .line 98
    .end local v1    # "e":Ljava/io/IOException;
    :catch_6
    move-exception v1

    .line 99
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_6

    .line 87
    .end local v1    # "e":Ljava/io/IOException;
    .end local v3    # "inputStream":Ljava/io/ByteArrayInputStream;
    .restart local v4    # "inputStream":Ljava/io/ByteArrayInputStream;
    :catchall_1
    move-exception v8

    move-object v3, v4

    .end local v4    # "inputStream":Ljava/io/ByteArrayInputStream;
    .restart local v3    # "inputStream":Ljava/io/ByteArrayInputStream;
    goto :goto_4

    .end local v3    # "inputStream":Ljava/io/ByteArrayInputStream;
    .end local v6    # "outputStream":Ljava/io/ByteArrayOutputStream;
    .restart local v4    # "inputStream":Ljava/io/ByteArrayInputStream;
    .restart local v7    # "outputStream":Ljava/io/ByteArrayOutputStream;
    :catchall_2
    move-exception v8

    move-object v6, v7

    .end local v7    # "outputStream":Ljava/io/ByteArrayOutputStream;
    .restart local v6    # "outputStream":Ljava/io/ByteArrayOutputStream;
    move-object v3, v4

    .end local v4    # "inputStream":Ljava/io/ByteArrayInputStream;
    .restart local v3    # "inputStream":Ljava/io/ByteArrayInputStream;
    goto :goto_4

    .line 84
    .end local v3    # "inputStream":Ljava/io/ByteArrayInputStream;
    .restart local v4    # "inputStream":Ljava/io/ByteArrayInputStream;
    :catch_7
    move-exception v1

    move-object v3, v4

    .end local v4    # "inputStream":Ljava/io/ByteArrayInputStream;
    .restart local v3    # "inputStream":Ljava/io/ByteArrayInputStream;
    goto :goto_2

    .end local v3    # "inputStream":Ljava/io/ByteArrayInputStream;
    .end local v6    # "outputStream":Ljava/io/ByteArrayOutputStream;
    .restart local v4    # "inputStream":Ljava/io/ByteArrayInputStream;
    .restart local v7    # "outputStream":Ljava/io/ByteArrayOutputStream;
    :catch_8
    move-exception v1

    move-object v6, v7

    .end local v7    # "outputStream":Ljava/io/ByteArrayOutputStream;
    .restart local v6    # "outputStream":Ljava/io/ByteArrayOutputStream;
    move-object v3, v4

    .end local v4    # "inputStream":Ljava/io/ByteArrayInputStream;
    .restart local v3    # "inputStream":Ljava/io/ByteArrayInputStream;
    goto :goto_2
.end method

.method private static encrypt_stream(Lcom/samsung/musicplus/mediainfo/crypto/IceKey;Lcom/samsung/musicplus/mediainfo/crypto/Block;Ljava/io/InputStream;Ljava/io/OutputStream;)Z
    .locals 8
    .param p0, "ik"    # Lcom/samsung/musicplus/mediainfo/crypto/IceKey;
    .param p1, "iv"    # Lcom/samsung/musicplus/mediainfo/crypto/Block;
    .param p2, "plainText"    # Ljava/io/InputStream;
    .param p3, "cyperText"    # Ljava/io/OutputStream;

    .prologue
    const/4 v5, 0x0

    .line 251
    new-instance v0, Lcom/samsung/musicplus/mediainfo/crypto/Block;

    invoke-direct {v0}, Lcom/samsung/musicplus/mediainfo/crypto/Block;-><init>()V

    .line 252
    .local v0, "buf":Lcom/samsung/musicplus/mediainfo/crypto/Block;
    new-instance v2, Lcom/samsung/musicplus/mediainfo/crypto/Block;

    invoke-direct {v2}, Lcom/samsung/musicplus/mediainfo/crypto/Block;-><init>()V

    .line 253
    .local v2, "inbuf":Lcom/samsung/musicplus/mediainfo/crypto/Block;
    new-instance v4, Lcom/samsung/musicplus/mediainfo/crypto/Block;

    invoke-direct {v4}, Lcom/samsung/musicplus/mediainfo/crypto/Block;-><init>()V

    .line 255
    .local v4, "outbuf":Lcom/samsung/musicplus/mediainfo/crypto/Block;
    invoke-virtual {v4, p1}, Lcom/samsung/musicplus/mediainfo/crypto/Block;->copy(Lcom/samsung/musicplus/mediainfo/crypto/Block;)V

    .line 257
    :cond_0
    invoke-virtual {v2, p2}, Lcom/samsung/musicplus/mediainfo/crypto/Block;->read(Ljava/io/InputStream;)I

    move-result v3

    .local v3, "n":I
    const/16 v6, 0x8

    if-ne v3, v6, :cond_2

    .line 258
    iget-object v6, v2, Lcom/samsung/musicplus/mediainfo/crypto/Block;->data:[B

    iget-object v7, v4, Lcom/samsung/musicplus/mediainfo/crypto/Block;->data:[B

    invoke-virtual {p0, v6, v7}, Lcom/samsung/musicplus/mediainfo/crypto/IceKey;->encrypt([B[B)V

    .line 259
    invoke-virtual {v4, p3}, Lcom/samsung/musicplus/mediainfo/crypto/Block;->write(Ljava/io/OutputStream;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 278
    :cond_1
    :goto_0
    return v5

    .line 265
    :cond_2
    if-eqz v3, :cond_3

    .line 266
    invoke-virtual {v2, v3, v0}, Lcom/samsung/musicplus/mediainfo/crypto/Block;->pad(ILcom/samsung/musicplus/mediainfo/crypto/Block;)V

    .line 267
    iget-object v6, v2, Lcom/samsung/musicplus/mediainfo/crypto/Block;->data:[B

    iget-object v7, v4, Lcom/samsung/musicplus/mediainfo/crypto/Block;->data:[B

    invoke-virtual {p0, v6, v7}, Lcom/samsung/musicplus/mediainfo/crypto/IceKey;->encrypt([B[B)V

    .line 268
    invoke-virtual {v4, p3}, Lcom/samsung/musicplus/mediainfo/crypto/Block;->write(Ljava/io/OutputStream;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 273
    :cond_3
    :try_start_0
    invoke-virtual {p3}, Ljava/io/OutputStream;->flush()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 278
    const/4 v5, 0x1

    goto :goto_0

    .line 274
    :catch_0
    move-exception v1

    .line 275
    .local v1, "e":Ljava/io/IOException;
    goto :goto_0
.end method

.method public static encrypt_string(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "plainText"    # Ljava/lang/String;

    .prologue
    .line 28
    if-nez p0, :cond_0

    .line 29
    const-string v2, ""

    .line 47
    :goto_0
    return-object v2

    .line 40
    :cond_0
    const/4 v1, 0x0

    .line 42
    .local v1, "plainByte":[B
    :try_start_0
    const-string v2, "ASCII"

    invoke-virtual {p0, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 47
    :goto_1
    invoke-static {v1}, Lcom/samsung/musicplus/mediainfo/crypto/IceCrypt;->encryptByteToHexString([B)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 43
    :catch_0
    move-exception v0

    .line 44
    .local v0, "e1":Ljava/io/UnsupportedEncodingException;
    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_1
.end method
