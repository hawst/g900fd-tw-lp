.class public Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoUtils$AlbumArtLoader;
.super Landroid/os/AsyncTask;
.source "RichInfoUtils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoUtils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AlbumArtLoader"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/Void;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field private mView:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 417
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Object;)Landroid/graphics/Bitmap;
    .locals 5
    .param p1, "obj"    # [Ljava/lang/Object;

    .prologue
    .line 422
    invoke-virtual {p0}, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoUtils$AlbumArtLoader;->isCancelled()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 423
    const/4 v3, 0x0

    .line 432
    :goto_0
    return-object v3

    .line 426
    :cond_0
    const/4 v3, 0x0

    aget-object v3, p1, v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoUtils$AlbumArtLoader;->mView:Landroid/widget/ImageView;

    .line 428
    const/4 v3, 0x1

    aget-object v0, p1, v3

    check-cast v0, Ljava/lang/String;

    .line 429
    .local v0, "artwork":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "http://image.allmusic.com/02/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 431
    .local v1, "imageLink":Ljava/lang/String;
    const/4 v3, 0x2

    aget-object v2, p1, v3

    check-cast v2, Ljava/lang/Integer;

    .line 432
    .local v2, "size":Ljava/lang/Integer;
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v1, v3}, Lcom/samsung/musicplus/util/AlbumArtUtils;->getNetworkArtworkQuick(Ljava/lang/String;I)Landroid/graphics/Bitmap;

    move-result-object v3

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 417
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoUtils$AlbumArtLoader;->doInBackground([Ljava/lang/Object;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1, "bm"    # Landroid/graphics/Bitmap;

    .prologue
    .line 437
    if-nez p1, :cond_0

    .line 438
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoUtils$AlbumArtLoader;->mView:Landroid/widget/ImageView;

    const v1, 0x7f02003a

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 442
    :goto_0
    return-void

    .line 440
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoUtils$AlbumArtLoader;->mView:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 417
    check-cast p1, Landroid/graphics/Bitmap;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoUtils$AlbumArtLoader;->onPostExecute(Landroid/graphics/Bitmap;)V

    return-void
.end method
