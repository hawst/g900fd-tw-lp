.class public Lcom/samsung/musicplus/mediainfo/crypto/AesCrypt;
.super Ljava/lang/Object;
.source "AesCrypt.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/mediainfo/crypto/AesCrypt$KDFClass;
    }
.end annotation


# static fields
.field private static final ALGORITHM:Ljava/lang/String; = "AES"

.field private static final CLASSTAG:Ljava/lang/String;

.field static final ENCRYPTED_RICHINFO_DATA:Ljava/lang/String; = "/EncryptedData/EncryptionMethod/CipherData/CipherValue"

.field private static final MODE:Ljava/lang/String; = "/CBC"

.field private static final PADDING:Ljava/lang/String; = "/NoPadding"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lcom/samsung/musicplus/mediainfo/crypto/AesCrypt;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/mediainfo/crypto/AesCrypt;->CLASSTAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 182
    return-void
.end method

.method public static crypt(ILjava/io/File;)Ljava/io/InputStream;
    .locals 4
    .param p0, "mode"    # I
    .param p1, "source"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 46
    invoke-static {p1}, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoUtils;->toByteFromFile(Ljava/io/File;)[B

    move-result-object v0

    .line 49
    .local v0, "bytes":[B
    new-instance v1, Ljavax/crypto/CipherInputStream;

    new-instance v2, Ljava/io/ByteArrayInputStream;

    invoke-direct {v2, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-static {p0}, Lcom/samsung/musicplus/mediainfo/crypto/AesCrypt;->getCipher(I)Ljavax/crypto/Cipher;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljavax/crypto/CipherInputStream;-><init>(Ljava/io/InputStream;Ljavax/crypto/Cipher;)V

    return-object v1
.end method

.method public static crypt(ILjava/lang/String;)Ljava/io/InputStream;
    .locals 4
    .param p0, "mode"    # I
    .param p1, "source"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 57
    const/4 v1, 0x0

    invoke-static {p1, v1}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    .line 59
    .local v0, "bytes":[B
    new-instance v1, Ljavax/crypto/CipherInputStream;

    new-instance v2, Ljava/io/ByteArrayInputStream;

    invoke-direct {v2, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-static {p0}, Lcom/samsung/musicplus/mediainfo/crypto/AesCrypt;->getCipher(I)Ljavax/crypto/Cipher;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljavax/crypto/CipherInputStream;-><init>(Ljava/io/InputStream;Ljavax/crypto/Cipher;)V

    return-object v1
.end method

.method public static crypt(ILjava/io/File;Ljava/io/File;)V
    .locals 10
    .param p0, "mode"    # I
    .param p1, "source"    # Ljava/io/File;
    .param p2, "dest"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v9, 0x0

    .line 64
    invoke-static {p1}, Lcom/samsung/musicplus/mediainfo/crypto/AesCrypt;->getEncryptedParam(Ljava/io/File;)[B

    move-result-object v8

    invoke-static {v8, v9}, Landroid/util/Base64;->decode([BI)[B

    move-result-object v3

    .line 66
    .local v3, "decodedBytes":[B
    const/4 v1, 0x0

    .line 67
    .local v1, "cipherInput":Ljavax/crypto/CipherInputStream;
    const/4 v5, 0x0

    .line 70
    .local v5, "output":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v2, Ljavax/crypto/CipherInputStream;

    new-instance v8, Ljava/io/ByteArrayInputStream;

    invoke-direct {v8, v3}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-static {p0}, Lcom/samsung/musicplus/mediainfo/crypto/AesCrypt;->getCipher(I)Ljavax/crypto/Cipher;

    move-result-object v9

    invoke-direct {v2, v8, v9}, Ljavax/crypto/CipherInputStream;-><init>(Ljava/io/InputStream;Ljavax/crypto/Cipher;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 72
    .end local v1    # "cipherInput":Ljavax/crypto/CipherInputStream;
    .local v2, "cipherInput":Ljavax/crypto/CipherInputStream;
    :try_start_1
    new-instance v6, Ljava/io/FileOutputStream;

    invoke-direct {v6, p2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 75
    .end local v5    # "output":Ljava/io/FileOutputStream;
    .local v6, "output":Ljava/io/FileOutputStream;
    const/4 v7, 0x0

    .line 76
    .local v7, "read":I
    const/16 v8, 0x400

    :try_start_2
    new-array v0, v8, [B

    .line 77
    .local v0, "buffer":[B
    :goto_0
    invoke-virtual {v2, v0}, Ljavax/crypto/CipherInputStream;->read([B)I

    move-result v7

    const/4 v8, -0x1

    if-eq v7, v8, :cond_2

    .line 78
    const/4 v8, 0x0

    invoke-virtual {v6, v0, v8, v7}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    goto :goto_0

    .line 80
    .end local v0    # "buffer":[B
    :catch_0
    move-exception v4

    move-object v5, v6

    .end local v6    # "output":Ljava/io/FileOutputStream;
    .restart local v5    # "output":Ljava/io/FileOutputStream;
    move-object v1, v2

    .line 81
    .end local v2    # "cipherInput":Ljavax/crypto/CipherInputStream;
    .end local v7    # "read":I
    .restart local v1    # "cipherInput":Ljavax/crypto/CipherInputStream;
    .local v4, "e":Ljava/lang/Exception;
    :goto_1
    :try_start_3
    invoke-virtual {v4}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 83
    if-eqz v5, :cond_0

    .line 84
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V

    .line 86
    :cond_0
    if-eqz v1, :cond_1

    .line 87
    invoke-virtual {v1}, Ljavax/crypto/CipherInputStream;->close()V

    .line 90
    .end local v4    # "e":Ljava/lang/Exception;
    :cond_1
    :goto_2
    return-void

    .line 83
    .end local v1    # "cipherInput":Ljavax/crypto/CipherInputStream;
    .end local v5    # "output":Ljava/io/FileOutputStream;
    .restart local v0    # "buffer":[B
    .restart local v2    # "cipherInput":Ljavax/crypto/CipherInputStream;
    .restart local v6    # "output":Ljava/io/FileOutputStream;
    .restart local v7    # "read":I
    :cond_2
    if-eqz v6, :cond_3

    .line 84
    invoke-virtual {v6}, Ljava/io/FileOutputStream;->close()V

    .line 86
    :cond_3
    if-eqz v2, :cond_6

    .line 87
    invoke-virtual {v2}, Ljavax/crypto/CipherInputStream;->close()V

    move-object v5, v6

    .end local v6    # "output":Ljava/io/FileOutputStream;
    .restart local v5    # "output":Ljava/io/FileOutputStream;
    move-object v1, v2

    .end local v2    # "cipherInput":Ljavax/crypto/CipherInputStream;
    .restart local v1    # "cipherInput":Ljavax/crypto/CipherInputStream;
    goto :goto_2

    .line 83
    .end local v0    # "buffer":[B
    .end local v7    # "read":I
    :catchall_0
    move-exception v8

    :goto_3
    if-eqz v5, :cond_4

    .line 84
    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V

    .line 86
    :cond_4
    if-eqz v1, :cond_5

    .line 87
    invoke-virtual {v1}, Ljavax/crypto/CipherInputStream;->close()V

    :cond_5
    throw v8

    .line 83
    .end local v1    # "cipherInput":Ljavax/crypto/CipherInputStream;
    .restart local v2    # "cipherInput":Ljavax/crypto/CipherInputStream;
    :catchall_1
    move-exception v8

    move-object v1, v2

    .end local v2    # "cipherInput":Ljavax/crypto/CipherInputStream;
    .restart local v1    # "cipherInput":Ljavax/crypto/CipherInputStream;
    goto :goto_3

    .end local v1    # "cipherInput":Ljavax/crypto/CipherInputStream;
    .end local v5    # "output":Ljava/io/FileOutputStream;
    .restart local v2    # "cipherInput":Ljavax/crypto/CipherInputStream;
    .restart local v6    # "output":Ljava/io/FileOutputStream;
    .restart local v7    # "read":I
    :catchall_2
    move-exception v8

    move-object v5, v6

    .end local v6    # "output":Ljava/io/FileOutputStream;
    .restart local v5    # "output":Ljava/io/FileOutputStream;
    move-object v1, v2

    .end local v2    # "cipherInput":Ljavax/crypto/CipherInputStream;
    .restart local v1    # "cipherInput":Ljavax/crypto/CipherInputStream;
    goto :goto_3

    .line 80
    .end local v7    # "read":I
    :catch_1
    move-exception v4

    goto :goto_1

    .end local v1    # "cipherInput":Ljavax/crypto/CipherInputStream;
    .restart local v2    # "cipherInput":Ljavax/crypto/CipherInputStream;
    :catch_2
    move-exception v4

    move-object v1, v2

    .end local v2    # "cipherInput":Ljavax/crypto/CipherInputStream;
    .restart local v1    # "cipherInput":Ljavax/crypto/CipherInputStream;
    goto :goto_1

    .end local v1    # "cipherInput":Ljavax/crypto/CipherInputStream;
    .end local v5    # "output":Ljava/io/FileOutputStream;
    .restart local v0    # "buffer":[B
    .restart local v2    # "cipherInput":Ljavax/crypto/CipherInputStream;
    .restart local v6    # "output":Ljava/io/FileOutputStream;
    .restart local v7    # "read":I
    :cond_6
    move-object v5, v6

    .end local v6    # "output":Ljava/io/FileOutputStream;
    .restart local v5    # "output":Ljava/io/FileOutputStream;
    move-object v1, v2

    .end local v2    # "cipherInput":Ljavax/crypto/CipherInputStream;
    .restart local v1    # "cipherInput":Ljavax/crypto/CipherInputStream;
    goto :goto_2
.end method

.method private static getCipher(I)Ljavax/crypto/Cipher;
    .locals 6
    .param p0, "mode"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    .line 94
    const-string v4, "AES/CBC/NoPadding"

    invoke-static {v4}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v0

    .line 97
    .local v0, "cipher":Ljavax/crypto/Cipher;
    const-string v4, "00000000000000000000000000000000"

    const/16 v5, 0x10

    invoke-static {v4, v5}, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoUtils;->toBytes(Ljava/lang/String;I)[B

    move-result-object v1

    .line 98
    .local v1, "iv":[B
    new-instance v2, Ljavax/crypto/spec/IvParameterSpec;

    invoke-direct {v2, v1}, Ljavax/crypto/spec/IvParameterSpec;-><init>([B)V

    .line 101
    .local v2, "ivParameterSpec":Ljavax/crypto/spec/IvParameterSpec;
    new-instance v3, Ljavax/crypto/spec/SecretKeySpec;

    invoke-static {}, Lcom/samsung/musicplus/mediainfo/crypto/AesCrypt$KDFClass;->SecCryptoKDF()[B

    move-result-object v4

    const-string v5, "AES"

    invoke-direct {v3, v4, v5}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 102
    .local v3, "key":Ljavax/crypto/spec/SecretKeySpec;
    invoke-virtual {v0, p0, v3, v2}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;Ljava/security/spec/AlgorithmParameterSpec;)V

    .line 104
    return-object v0
.end method

.method private static getEncryptedParam(Ljava/io/File;)[B
    .locals 10
    .param p0, "source"    # Ljava/io/File;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Exception;
        }
    .end annotation

    .prologue
    const/4 v9, 0x1

    .line 134
    const/4 v2, 0x0

    .line 135
    .local v2, "encryptedData":[B
    new-instance v5, Ljava/io/InputStreamReader;

    new-instance v7, Ljava/io/FileInputStream;

    invoke-direct {v7, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-direct {v5, v7}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    .line 139
    .local v5, "r":Ljava/io/Reader;
    :try_start_0
    invoke-static {}, Lorg/xmlpull/v1/XmlPullParserFactory;->newInstance()Lorg/xmlpull/v1/XmlPullParserFactory;

    move-result-object v4

    .line 140
    .local v4, "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    const/4 v7, 0x1

    invoke-virtual {v4, v7}, Lorg/xmlpull/v1/XmlPullParserFactory;->setNamespaceAware(Z)V

    .line 141
    invoke-virtual {v4}, Lorg/xmlpull/v1/XmlPullParserFactory;->newPullParser()Lorg/xmlpull/v1/XmlPullParser;

    move-result-object v6

    .line 142
    .local v6, "xpp":Lorg/xmlpull/v1/XmlPullParser;
    invoke-interface {v6, v5}, Lorg/xmlpull/v1/XmlPullParser;->setInput(Ljava/io/Reader;)V

    .line 144
    new-instance v0, Ljava/lang/StringBuffer;

    const-string v7, ""

    invoke-direct {v0, v7}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 145
    .local v0, "current":Ljava/lang/StringBuffer;
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->getEventType()I

    move-result v3

    .line 146
    .local v3, "eventType":I
    :goto_0
    if-eq v3, v9, :cond_1

    .line 147
    packed-switch v3, :pswitch_data_0

    .line 168
    :cond_0
    :goto_1
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->next()I

    move-result v3

    goto :goto_0

    .line 149
    :pswitch_0
    const-string v7, "/"

    invoke-virtual {v0, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 150
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->getName()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;
    :try_end_0
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 170
    .end local v0    # "current":Ljava/lang/StringBuffer;
    .end local v3    # "eventType":I
    .end local v4    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v6    # "xpp":Lorg/xmlpull/v1/XmlPullParser;
    :catch_0
    move-exception v1

    .line 171
    .local v1, "e":Lorg/xmlpull/v1/XmlPullParserException;
    :try_start_1
    invoke-virtual {v1}, Lorg/xmlpull/v1/XmlPullParserException;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 177
    invoke-virtual {v5}, Ljava/io/Reader;->close()V

    .line 179
    .end local v1    # "e":Lorg/xmlpull/v1/XmlPullParserException;
    :goto_2
    return-object v2

    .line 153
    .restart local v0    # "current":Ljava/lang/StringBuffer;
    .restart local v3    # "eventType":I
    .restart local v4    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .restart local v6    # "xpp":Lorg/xmlpull/v1/XmlPullParser;
    :pswitch_1
    :try_start_2
    const-string v7, "/"

    invoke-virtual {v0, v7}, Ljava/lang/StringBuffer;->lastIndexOf(Ljava/lang/String;)I

    move-result v7

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->length()I

    move-result v8

    invoke-virtual {v0, v7, v8}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;
    :try_end_2
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 172
    .end local v0    # "current":Ljava/lang/StringBuffer;
    .end local v3    # "eventType":I
    .end local v4    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v6    # "xpp":Lorg/xmlpull/v1/XmlPullParser;
    :catch_1
    move-exception v1

    .line 173
    .local v1, "e":Ljava/io/IOException;
    :try_start_3
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 177
    invoke-virtual {v5}, Ljava/io/Reader;->close()V

    goto :goto_2

    .line 156
    .end local v1    # "e":Ljava/io/IOException;
    .restart local v0    # "current":Ljava/lang/StringBuffer;
    .restart local v3    # "eventType":I
    .restart local v4    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .restart local v6    # "xpp":Lorg/xmlpull/v1/XmlPullParser;
    :pswitch_2
    :try_start_4
    const-string v7, "/EncryptedData/EncryptionMethod/CipherData/CipherValue"

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 157
    invoke-interface {v6}, Lorg/xmlpull/v1/XmlPullParser;->getText()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/String;->getBytes()[B
    :try_end_4
    .catch Lorg/xmlpull/v1/XmlPullParserException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/NullPointerException; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v2

    goto :goto_1

    .line 177
    :cond_1
    invoke-virtual {v5}, Ljava/io/Reader;->close()V

    goto :goto_2

    .line 174
    .end local v0    # "current":Ljava/lang/StringBuffer;
    .end local v3    # "eventType":I
    .end local v4    # "factory":Lorg/xmlpull/v1/XmlPullParserFactory;
    .end local v6    # "xpp":Lorg/xmlpull/v1/XmlPullParser;
    :catch_2
    move-exception v1

    .line 175
    .local v1, "e":Ljava/lang/NullPointerException;
    :try_start_5
    invoke-virtual {v1}, Ljava/lang/NullPointerException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 177
    invoke-virtual {v5}, Ljava/io/Reader;->close()V

    goto :goto_2

    .end local v1    # "e":Ljava/lang/NullPointerException;
    :catchall_0
    move-exception v7

    invoke-virtual {v5}, Ljava/io/Reader;->close()V

    throw v7

    .line 147
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
