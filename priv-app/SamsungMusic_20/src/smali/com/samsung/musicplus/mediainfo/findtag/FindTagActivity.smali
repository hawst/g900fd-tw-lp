.class public Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;
.super Landroid/app/Activity;
.source "FindTagActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$RichInfoSaver;
    }
.end annotation


# static fields
.field private static final CLASSTAG:Ljava/lang/String;

.field private static final DISPLAY_FIND_TAG_RESULTS:I = 0x5

.field private static final DO_RECOGNITION:I = 0x2

.field public static final FAIL_TO_SAVE_FILE:I = 0x7

.field private static final INTERNAL_RECORDING:I = 0x6

.field public static final NETWORK_ERROR:I = 0x3

.field private static final REQUEST_CONFIG:I = 0x1

.field private static final REQUEST_ID_KEY:Ljava/lang/String; = "requestId"

.field private static final REQUEST_RESULT:I = 0x4

.field private static final SIGNATURE_DATA_KEY:Ljava/lang/String; = "sigdata"

.field private static TAG_MESSAGE_STEP_BY_STEP:Z


# instance fields
.field private mFilePath:Ljava/lang/String;

.field private mIsSavingResults:Z

.field private mIsShowResults:Z

.field private mNetworkData:Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;

.field private final mNetworkResponseHandler:Landroid/os/Handler;

.field private mPause:Z

.field private mProgressDialog:Landroid/app/ProgressDialog;

.field private final mResultListener:Lcom/samsung/musicplus/library/audio/FindTag$OnFindTagResultListener;

.field private final mRichInfoResponseHandler:Landroid/os/Handler;

.field private mServiceName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 61
    const-class v0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->CLASSTAG:Ljava/lang/String;

    .line 80
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->TAG_MESSAGE_STEP_BY_STEP:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 60
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 63
    iput-boolean v0, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->mIsShowResults:Z

    .line 65
    iput-boolean v0, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->mIsSavingResults:Z

    .line 75
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->mPause:Z

    .line 183
    new-instance v0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$3;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$3;-><init>(Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;)V

    iput-object v0, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->mRichInfoResponseHandler:Landroid/os/Handler;

    .line 240
    new-instance v0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$4;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$4;-><init>(Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;)V

    iput-object v0, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->mResultListener:Lcom/samsung/musicplus/library/audio/FindTag$OnFindTagResultListener;

    .line 433
    new-instance v0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$9;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$9;-><init>(Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;)V

    iput-object v0, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->mNetworkResponseHandler:Landroid/os/Handler;

    .line 523
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->cancelAllOperations()V

    return-void
.end method

.method static synthetic access$100()Z
    .locals 1

    .prologue
    .line 60
    sget-boolean v0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->TAG_MESSAGE_STEP_BY_STEP:Z

    return v0
.end method

.method static synthetic access$1000(Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;)Landroid/app/ProgressDialog;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;
    .param p1, "x1"    # Landroid/app/ProgressDialog;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->mRichInfoResponseHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;Z)Ljava/util/List;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 60
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->getBaseOrbitParameters(Z)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1300(Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;)Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->mNetworkData:Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;

    return-object v0
.end method

.method static synthetic access$1302(Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;)Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;
    .param p1, "x1"    # Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->mNetworkData:Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;

    return-object p1
.end method

.method static synthetic access$1402(Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->mServiceName:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$1502(Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 60
    iput-boolean p1, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->mIsSavingResults:Z

    return p1
.end method

.method static synthetic access$200(Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->requestConfig()V

    return-void
.end method

.method static synthetic access$300(Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;[B)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;
    .param p1, "x1"    # [B

    .prologue
    .line 60
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->doRecognition([B)V

    return-void
.end method

.method static synthetic access$400(Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->hideProgressDialog()V

    return-void
.end method

.method static synthetic access$500(Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 60
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->requestResults(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$600(Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->mFilePath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$700(Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;)Lcom/samsung/musicplus/library/audio/FindTag$OnFindTagResultListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->mResultListener:Lcom/samsung/musicplus/library/audio/FindTag$OnFindTagResultListener;

    return-object v0
.end method

.method static synthetic access$800()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    sget-object v0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->CLASSTAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$900(Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->displayFindTagResults()V

    return-void
.end method

.method private cancelAllOperations()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 154
    iget-object v1, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->mNetworkResponseHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 155
    iget-object v1, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->mRichInfoResponseHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 157
    invoke-static {}, Lcom/samsung/musicplus/library/audio/FindTag;->getInstance()Lcom/samsung/musicplus/library/audio/FindTag;

    move-result-object v0

    .line 158
    .local v0, "tag":Lcom/samsung/musicplus/library/audio/FindTag;
    invoke-virtual {v0}, Lcom/samsung/musicplus/library/audio/FindTag;->cancel()V

    .line 160
    iget-boolean v1, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->mIsShowResults:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->mIsSavingResults:Z

    if-eqz v1, :cond_1

    .line 161
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->finish()V

    .line 163
    :cond_1
    return-void
.end method

.method private displayFindTagResults()V
    .locals 3

    .prologue
    .line 497
    invoke-direct {p0}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->hideProgressDialog()V

    .line 498
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->mIsShowResults:Z

    .line 501
    iget-boolean v0, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->mPause:Z

    if-eqz v0, :cond_0

    .line 506
    :goto_0
    return-void

    .line 504
    :cond_0
    new-instance v0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagResultDialog;

    iget-object v1, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->mNetworkData:Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;

    iget-object v1, v1, Lcom/samsung/musicplus/mediainfo/findtag/NetworkData;->resultFindTags:Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagResultDialog;-><init>(Ljava/util/ArrayList;)V

    invoke-virtual {p0}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "tag_result"

    invoke-virtual {v0, v1, v2}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagResultDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private doRecognition([B)V
    .locals 2
    .param p1, "recordedSample"    # [B

    .prologue
    .line 329
    if-nez p1, :cond_0

    .line 367
    :goto_0
    return-void

    .line 333
    :cond_0
    iget-object v1, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->mNetworkResponseHandler:Landroid/os/Handler;

    invoke-static {v1}, Lcom/samsung/musicplus/util/HTTPRequestHelper;->getResponseHandlerInstance(Landroid/os/Handler;)Lorg/apache/http/client/ResponseHandler;

    move-result-object v0

    .line 338
    .local v0, "responseHandler":Lorg/apache/http/client/ResponseHandler;, "Lorg/apache/http/client/ResponseHandler<Ljava/lang/String;>;"
    new-instance v1, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$7;

    invoke-direct {v1, p0, p1, v0}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$7;-><init>(Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;[BLorg/apache/http/client/ResponseHandler;)V

    invoke-virtual {v1}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$7;->start()V

    goto :goto_0
.end method

.method private getBaseOrbitParameters(Z)Ljava/util/List;
    .locals 5
    .param p1, "requestConfig"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)",
            "Ljava/util/List",
            "<",
            "Lcom/samsung/musicplus/library/http/multipart/Part;",
            ">;"
        }
    .end annotation

    .prologue
    .line 402
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 404
    .local v0, "parts":Ljava/util/List;, "Ljava/util/List<Lcom/samsung/musicplus/library/http/multipart/Part;>;"
    if-nez p1, :cond_0

    iget-object v2, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->mServiceName:Ljava/lang/String;

    if-nez v2, :cond_1

    .line 405
    :cond_0
    new-instance v2, Lcom/samsung/musicplus/library/http/multipart/StringPart;

    const-string v3, "service"

    const-string v4, "cn=Config,cn=Samsung,cn=services"

    invoke-direct {v2, v3, v4}, Lcom/samsung/musicplus/library/http/multipart/StringPart;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 412
    :goto_0
    new-instance v2, Lcom/samsung/musicplus/library/http/multipart/StringPart;

    const-string v3, "language"

    invoke-virtual {p0}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v4

    iget-object v4, v4, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v4}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/samsung/musicplus/library/http/multipart/StringPart;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 414
    new-instance v2, Lcom/samsung/musicplus/library/http/multipart/StringPart;

    const-string v3, "cryptToken"

    const-string v4, "025B58C0"

    invoke-static {v4}, Lcom/samsung/musicplus/mediainfo/crypto/IceCrypt;->encrypt_string(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/samsung/musicplus/library/http/multipart/StringPart;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 418
    const-string v2, "phone"

    invoke-virtual {p0, v2}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    .line 420
    .local v1, "tm":Landroid/telephony/TelephonyManager;
    new-instance v2, Lcom/samsung/musicplus/library/http/multipart/StringPart;

    const-string v3, "deviceId"

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/musicplus/mediainfo/crypto/IceCrypt;->encrypt_string(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/samsung/musicplus/library/http/multipart/StringPart;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 422
    new-instance v2, Lcom/samsung/musicplus/library/http/multipart/StringPart;

    const-string v3, "deviceModel"

    sget-object v4, Lcom/samsung/musicplus/mediainfo/findtag/ShazamNetwork;->DEVICEMODEL:Ljava/lang/String;

    invoke-static {v4}, Lcom/samsung/musicplus/mediainfo/crypto/IceCrypt;->encrypt_string(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/samsung/musicplus/library/http/multipart/StringPart;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 424
    new-instance v2, Lcom/samsung/musicplus/library/http/multipart/StringPart;

    const-string v3, "applicationIdentifier"

    const-string v4, "Samsung_FT_Android_GA__3.0.0"

    invoke-static {v4}, Lcom/samsung/musicplus/mediainfo/crypto/IceCrypt;->encrypt_string(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/samsung/musicplus/library/http/multipart/StringPart;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 427
    return-object v0

    .line 408
    .end local v1    # "tm":Landroid/telephony/TelephonyManager;
    :cond_1
    new-instance v2, Lcom/samsung/musicplus/library/http/multipart/StringPart;

    const-string v3, "service"

    iget-object v4, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->mServiceName:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lcom/samsung/musicplus/library/http/multipart/StringPart;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private hideProgressDialog()V
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 148
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 149
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 151
    :cond_0
    return-void
.end method

.method private requestConfig()V
    .locals 2

    .prologue
    .line 313
    iget-object v1, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->mNetworkResponseHandler:Landroid/os/Handler;

    invoke-static {v1}, Lcom/samsung/musicplus/util/HTTPRequestHelper;->getResponseHandlerInstance(Landroid/os/Handler;)Lorg/apache/http/client/ResponseHandler;

    move-result-object v0

    .line 318
    .local v0, "responseHandler":Lorg/apache/http/client/ResponseHandler;, "Lorg/apache/http/client/ResponseHandler<Ljava/lang/String;>;"
    new-instance v1, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$6;

    invoke-direct {v1, p0, v0}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$6;-><init>(Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;Lorg/apache/http/client/ResponseHandler;)V

    invoke-virtual {v1}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$6;->start()V

    .line 326
    return-void
.end method

.method private requestResults(Ljava/lang/String;)V
    .locals 2
    .param p1, "requestId"    # Ljava/lang/String;

    .prologue
    .line 370
    iget-object v1, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->mNetworkResponseHandler:Landroid/os/Handler;

    invoke-static {v1}, Lcom/samsung/musicplus/util/HTTPRequestHelper;->getResponseHandlerInstance(Landroid/os/Handler;)Lorg/apache/http/client/ResponseHandler;

    move-result-object v0

    .line 375
    .local v0, "responseHandler":Lorg/apache/http/client/ResponseHandler;, "Lorg/apache/http/client/ResponseHandler<Ljava/lang/String;>;"
    new-instance v1, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$8;

    invoke-direct {v1, p0, p1, v0}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$8;-><init>(Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;Ljava/lang/String;Lorg/apache/http/client/ResponseHandler;)V

    invoke-virtual {v1}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$8;->start()V

    .line 391
    return-void
.end method

.method private requestUMSinfo()V
    .locals 2

    .prologue
    .line 259
    iget-object v1, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->mNetworkResponseHandler:Landroid/os/Handler;

    invoke-static {v1}, Lcom/samsung/musicplus/util/HTTPRequestHelper;->getResponseHandlerInstance(Landroid/os/Handler;)Lorg/apache/http/client/ResponseHandler;

    move-result-object v0

    .line 264
    .local v0, "responseHandler":Lorg/apache/http/client/ResponseHandler;, "Lorg/apache/http/client/ResponseHandler<Ljava/lang/String;>;"
    new-instance v1, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$5;

    invoke-direct {v1, p0, v0}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$5;-><init>(Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;Lorg/apache/http/client/ResponseHandler;)V

    invoke-virtual {v1}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$5;->start()V

    .line 310
    return-void
.end method

.method private showProgressDialog()V
    .locals 3

    .prologue
    .line 123
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 124
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 125
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    new-instance v1, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$1;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$1;-><init>(Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 131
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    new-instance v1, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$2;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$2;-><init>(Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;)V

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 137
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 138
    sget-boolean v0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->TAG_MESSAGE_STEP_BY_STEP:Z

    if-eqz v0, :cond_0

    .line 139
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f1000a1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 143
    :goto_0
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 144
    return-void

    .line 141
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f10013c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 84
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 86
    if-nez p1, :cond_0

    .line 89
    invoke-virtual {p0}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "FDTG"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/util/logging/FeatureLogger;->insertLog(Landroid/content/Context;Ljava/lang/String;)V

    .line 91
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "path"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->mFilePath:Ljava/lang/String;

    .line 93
    invoke-direct {p0}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->showProgressDialog()V

    .line 96
    sget-boolean v0, Lcom/samsung/musicplus/mediainfo/richinfo/RichInfoUtils;->sHasShop:Z

    if-nez v0, :cond_1

    .line 97
    invoke-direct {p0}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->requestUMSinfo()V

    .line 101
    :goto_0
    return-void

    .line 99
    :cond_1
    invoke-direct {p0}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->requestConfig()V

    goto :goto_0
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 111
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->mPause:Z

    .line 112
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 113
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 105
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 106
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->mPause:Z

    .line 107
    return-void
.end method

.method protected onStop()V
    .locals 0

    .prologue
    .line 117
    invoke-direct {p0}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->hideProgressDialog()V

    .line 118
    invoke-direct {p0}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;->cancelAllOperations()V

    .line 119
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 120
    return-void
.end method

.method public saveRichInfoData(Ljava/lang/String;)V
    .locals 3
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 514
    new-instance v0, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$RichInfoSaver;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$RichInfoSaver;-><init>(Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity;)V

    .line 515
    .local v0, "saver":Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$RichInfoSaver;
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/mediainfo/findtag/FindTagActivity$RichInfoSaver;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 516
    return-void
.end method
