.class public Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;
.super Lcom/samsung/musicplus/MusicBaseActivity;
.source "MediaInfoEditActivity.java"


# static fields
.field private static final CHAR_SET:[Ljava/lang/String;

.field private static final MAX_LENGTH:I = 0xff

.field private static final METADATA_ALBUM_FIELD:I = 0x2

.field private static final METADATA_ARTIST_FIELD:I = 0x1

.field private static final METADATA_TITLE_FIELD:I = 0x0

.field private static final TAG:Ljava/lang/String; = "MediaInfo"


# instance fields
.field private filterSpecialChar:Landroid/text/InputFilter;

.field private mAlbum:Landroid/widget/EditText;

.field private mArtist:Landroid/widget/EditText;

.field private mBaseUri:Ljava/lang/String;

.field private mCustomDone:Landroid/widget/TextView;

.field private mInputFilers:Landroid/text/InputFilter;

.field private mMetaData:Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;

.field private mMetaRetriever:Lcom/samsung/musicplus/library/metadata/SecMediaMetaDataRetriever;

.field private mOnItemSelectedListener:Landroid/widget/AdapterView$OnItemSelectedListener;

.field private mTextWatcher:Landroid/text/TextWatcher;

.field private mTitle:Landroid/widget/EditText;

.field private mToast:Landroid/widget/Toast;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 46
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, ""

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "EUC-KR"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "SHIFT-JIS"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "GBK"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "BIG5"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "UTF-8"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "UTF-16"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->CHAR_SET:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/samsung/musicplus/MusicBaseActivity;-><init>()V

    .line 78
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mToast:Landroid/widget/Toast;

    .line 287
    new-instance v0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity$3;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity$3;-><init>(Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;)V

    iput-object v0, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mOnItemSelectedListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    .line 391
    new-instance v0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity$4;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity$4;-><init>(Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;)V

    iput-object v0, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mTextWatcher:Landroid/text/TextWatcher;

    .line 418
    new-instance v0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity$5;

    const/16 v1, 0xff

    invoke-direct {v0, p0, v1}, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity$5;-><init>(Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;I)V

    iput-object v0, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mInputFilers:Landroid/text/InputFilter;

    .line 432
    new-instance v0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity$6;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity$6;-><init>(Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;)V

    iput-object v0, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->filterSpecialChar:Landroid/text/InputFilter;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mBaseUri:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->saveMetaData()V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;)Lcom/samsung/musicplus/library/metadata/SecMediaMetaDataRetriever;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mMetaRetriever:Lcom/samsung/musicplus/library/metadata/SecMediaMetaDataRetriever;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;)Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mMetaData:Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;II)Ljava/lang/CharSequence;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 42
    invoke-direct {p0, p1, p2}, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->getMetadatabyCharset(II)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mTitle:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$600(Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mCustomDone:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$700(Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;)Landroid/widget/Toast;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mToast:Landroid/widget/Toast;

    return-object v0
.end method

.method private getMetadatabyCharset(II)Ljava/lang/CharSequence;
    .locals 5
    .param p1, "type"    # I
    .param p2, "charset"    # I

    .prologue
    const/4 v4, 0x0

    .line 463
    const/4 v1, 0x0

    .line 464
    .local v1, "metadata":[B
    const/4 v2, 0x0

    .line 465
    .local v2, "result":Ljava/lang/CharSequence;
    packed-switch p1, :pswitch_data_0

    .line 476
    :goto_0
    if-nez v1, :cond_1

    .line 477
    const/4 v2, 0x0

    .line 487
    :goto_1
    if-eqz v2, :cond_0

    .line 488
    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 490
    .end local v2    # "result":Ljava/lang/CharSequence;
    :cond_0
    return-object v2

    .line 467
    .restart local v2    # "result":Ljava/lang/CharSequence;
    :pswitch_0
    const/4 v3, 0x7

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/metadata/MetaDataConverter;->getMetadata(II)[B

    move-result-object v1

    .line 468
    goto :goto_0

    .line 470
    :pswitch_1
    const/4 v3, 0x2

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/metadata/MetaDataConverter;->getMetadata(II)[B

    move-result-object v1

    .line 471
    goto :goto_0

    .line 473
    :pswitch_2
    const/4 v3, 0x1

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/metadata/MetaDataConverter;->getMetadata(II)[B

    move-result-object v1

    goto :goto_0

    .line 480
    :cond_1
    :try_start_0
    new-instance v2, Ljava/lang/String;

    .end local v2    # "result":Ljava/lang/CharSequence;
    sget-object v3, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->CHAR_SET:[Ljava/lang/String;

    aget-object v3, v3, p2

    invoke-direct {v2, v1, v3}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .restart local v2    # "result":Ljava/lang/CharSequence;
    goto :goto_1

    .line 481
    .end local v2    # "result":Ljava/lang/CharSequence;
    :catch_0
    move-exception v0

    .line 482
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    const/4 v2, 0x0

    .restart local v2    # "result":Ljava/lang/CharSequence;
    goto :goto_1

    .line 465
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private saveMetaData()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 495
    iget-object v5, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mMetaData:Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;

    if-nez v5, :cond_0

    .line 549
    :goto_0
    return-void

    .line 499
    :cond_0
    iget-object v5, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mTitle:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    .line 500
    .local v2, "title":Ljava/lang/String;
    iget-object v5, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mArtist:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    .line 501
    .local v1, "artist":Ljava/lang/String;
    iget-object v5, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mAlbum:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 504
    .local v0, "album":Ljava/lang/String;
    invoke-static {v2, v1, v0}, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils;->adjustMetaData(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 507
    iget-object v5, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mMetaData:Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;

    iget-object v5, v5, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;->data:Ljava/lang/String;

    const-string v6, "audio/mpeg"

    invoke-static {v5, v6}, Lcom/samsung/musicplus/library/metadata/MetaDataConverter;->openFile(Ljava/lang/String;Ljava/lang/String;)I

    .line 509
    if-eqz v2, :cond_1

    iget-object v5, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mTitle:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getVisibility()I

    move-result v5

    if-nez v5, :cond_1

    .line 510
    const/4 v5, 0x7

    invoke-static {v5, v2}, Lcom/samsung/musicplus/library/metadata/MetaDataConverter;->setMetadata(ILjava/lang/String;)I

    .line 512
    :cond_1
    if-eqz v1, :cond_2

    iget-object v5, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mArtist:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getVisibility()I

    move-result v5

    if-nez v5, :cond_2

    .line 513
    const/4 v5, 0x2

    invoke-static {v5, v1}, Lcom/samsung/musicplus/library/metadata/MetaDataConverter;->setMetadata(ILjava/lang/String;)I

    .line 515
    :cond_2
    if-eqz v0, :cond_3

    iget-object v5, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mAlbum:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getVisibility()I

    move-result v5

    if-nez v5, :cond_3

    .line 516
    const/4 v5, 0x1

    invoke-static {v5, v0}, Lcom/samsung/musicplus/library/metadata/MetaDataConverter;->setMetadata(ILjava/lang/String;)I

    .line 519
    :cond_3
    invoke-static {}, Lcom/samsung/musicplus/library/metadata/MetaDataConverter;->saveMetadata()I

    .line 520
    invoke-static {}, Lcom/samsung/musicplus/library/metadata/MetaDataConverter;->closeFile()I

    .line 523
    new-instance v4, Landroid/content/ContentValues;

    const/4 v5, 0x3

    invoke-direct {v4, v5}, Landroid/content/ContentValues;-><init>(I)V

    .line 524
    .local v4, "values":Landroid/content/ContentValues;
    if-eqz v2, :cond_4

    iget-object v5, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mTitle:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getVisibility()I

    move-result v5

    if-nez v5, :cond_4

    .line 525
    const-string v5, "title"

    invoke-virtual {v4, v5, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 527
    :cond_4
    if-eqz v1, :cond_5

    iget-object v5, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mArtist:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getVisibility()I

    move-result v5

    if-nez v5, :cond_5

    .line 528
    const-string v5, "artist"

    invoke-virtual {v4, v5, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 530
    :cond_5
    if-eqz v0, :cond_6

    iget-object v5, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mAlbum:Landroid/widget/EditText;

    invoke-virtual {v5}, Landroid/widget/EditText;->getVisibility()I

    move-result v5

    if-nez v5, :cond_6

    .line 531
    const-string v5, "album"

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 533
    :cond_6
    invoke-virtual {v4}, Landroid/content/ContentValues;->size()I

    move-result v5

    if-eqz v5, :cond_7

    .line 535
    invoke-virtual {p0}, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mBaseUri:Ljava/lang/String;

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v5, v6, v4, v8, v8}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 539
    :cond_7
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "file://"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mMetaData:Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;

    iget-object v6, v6, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;->data:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 540
    .local v3, "uri":Landroid/net/Uri;
    new-instance v5, Landroid/content/Intent;

    const-string v6, "android.intent.action.MEDIA_SCANNER_SCAN_FILE"

    invoke-direct {v5, v6, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p0, v5}, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 542
    iget-object v5, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mMetaRetriever:Lcom/samsung/musicplus/library/metadata/SecMediaMetaDataRetriever;

    if-eqz v5, :cond_8

    iget-object v5, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mMetaRetriever:Lcom/samsung/musicplus/library/metadata/SecMediaMetaDataRetriever;

    invoke-virtual {v5}, Lcom/samsung/musicplus/library/metadata/SecMediaMetaDataRetriever;->isUnicode()Z

    move-result v5

    if-eqz v5, :cond_8

    .line 543
    invoke-virtual {p0}, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f100071

    invoke-static {v5, v6, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    .line 546
    :cond_8
    invoke-virtual {p0}, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f100072

    invoke-static {v5, v6, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0
.end method

.method private setCustomCancelDone()V
    .locals 4

    .prologue
    .line 140
    invoke-virtual {p0}, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v1

    .line 142
    .local v1, "customView":Landroid/view/View;
    const v2, 0x7f0d0043

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mCustomDone:Landroid/widget/TextView;

    .line 143
    iget-object v2, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mCustomDone:Landroid/widget/TextView;

    const v3, 0x7f100147

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 144
    iget-object v2, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mCustomDone:Landroid/widget/TextView;

    new-instance v3, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity$1;

    invoke-direct {v3, p0}, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity$1;-><init>(Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 161
    const v2, 0x7f0d0042

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 162
    .local v0, "customCancel":Landroid/widget/TextView;
    new-instance v2, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity$2;

    invoke-direct {v2, p0}, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity$2;-><init>(Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;)V

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 169
    return-void
.end method

.method private setItemEditText(IILjava/lang/String;)Landroid/widget/EditText;
    .locals 1
    .param p1, "viewId"    # I
    .param p2, "str"    # I
    .param p3, "text"    # Ljava/lang/String;

    .prologue
    .line 373
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->setItemEditText(IILjava/lang/String;Z)Landroid/widget/EditText;

    move-result-object v0

    return-object v0
.end method

.method private setItemEditText(IILjava/lang/String;Z)Landroid/widget/EditText;
    .locals 5
    .param p1, "viewId"    # I
    .param p2, "str"    # I
    .param p3, "text"    # Ljava/lang/String;
    .param p4, "visible"    # Z

    .prologue
    const/16 v4, 0x8

    .line 377
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 378
    .local v1, "frame":Landroid/view/View;
    const v3, 0x7f0d00e1

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 379
    .local v2, "title":Landroid/widget/TextView;
    const v3, 0x7f0d00e2

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 381
    .local v0, "content":Landroid/widget/EditText;
    if-eqz p3, :cond_0

    if-nez p4, :cond_1

    .line 382
    :cond_0
    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 383
    invoke-virtual {v0, v4}, Landroid/widget/EditText;->setVisibility(I)V

    .line 388
    :goto_0
    return-object v0

    .line 385
    :cond_1
    invoke-virtual {v2, p2}, Landroid/widget/TextView;->setText(I)V

    .line 386
    invoke-virtual {v0, p3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private setItemText(IILjava/lang/String;)V
    .locals 1
    .param p1, "viewId"    # I
    .param p2, "str"    # I
    .param p3, "text"    # Ljava/lang/String;

    .prologue
    .line 347
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->setItemText(IILjava/lang/String;Ljava/lang/String;)V

    .line 348
    return-void
.end method

.method private setItemText(IILjava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "viewId"    # I
    .param p2, "str"    # I
    .param p3, "text"    # Ljava/lang/String;
    .param p4, "description"    # Ljava/lang/String;

    .prologue
    .line 351
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 352
    .local v1, "frame":Landroid/view/View;
    const v3, 0x7f0d00e1

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 353
    .local v2, "title":Landroid/widget/TextView;
    const v3, 0x7f0d00e2

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 355
    .local v0, "content":Landroid/widget/TextView;
    if-nez p3, :cond_1

    .line 356
    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 370
    :cond_0
    :goto_0
    return-void

    .line 358
    :cond_1
    invoke-virtual {v2, p2}, Landroid/widget/TextView;->setText(I)V

    .line 359
    invoke-virtual {v0, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 361
    if-eqz p4, :cond_2

    .line 362
    invoke-virtual {v0, p4}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 366
    :cond_2
    const v3, 0x7f0d00e0

    if-ne p1, v3, :cond_0

    .line 367
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setSelected(Z)V

    goto :goto_0
.end method

.method private setMetaDataInfo(Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;)V
    .locals 12
    .param p1, "metaData"    # Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;

    .prologue
    .line 172
    invoke-virtual {p0}, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    .line 175
    .local v3, "context":Landroid/content/Context;
    const v8, 0x7f0d00d4

    invoke-virtual {p0, v8}, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 176
    .local v2, "albumView":Landroid/widget/ImageView;
    iget-wide v8, p1, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;->albumId:J

    invoke-static {v3, v8, v9}, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils;->getAlbumArt(Landroid/content/Context;J)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 177
    .local v1, "albumArt":Landroid/graphics/Bitmap;
    if-nez v1, :cond_0

    .line 178
    const v8, 0x7f02003a

    invoke-virtual {v2, v8}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 183
    :goto_0
    const v8, 0x7f0d00e5

    invoke-virtual {p0, v8}, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/Spinner;

    .line 184
    .local v6, "spinner":Landroid/widget/Spinner;
    const v8, 0x7f090003

    const v9, 0x1090008

    invoke-static {v3, v8, v9}, Landroid/widget/ArrayAdapter;->createFromResource(Landroid/content/Context;II)Landroid/widget/ArrayAdapter;

    move-result-object v0

    .line 186
    .local v0, "adapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/CharSequence;>;"
    const v8, 0x1090009

    invoke-virtual {v0, v8}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    .line 187
    invoke-virtual {v6, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 188
    iget-object v8, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mOnItemSelectedListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    invoke-virtual {v6, v8}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 191
    iget-object v8, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mMetaRetriever:Lcom/samsung/musicplus/library/metadata/SecMediaMetaDataRetriever;

    if-eqz v8, :cond_1

    iget-object v8, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mMetaRetriever:Lcom/samsung/musicplus/library/metadata/SecMediaMetaDataRetriever;

    iget-object v9, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mMetaRetriever:Lcom/samsung/musicplus/library/metadata/SecMediaMetaDataRetriever;

    const/4 v9, 0x7

    invoke-virtual {v8, v9}, Lcom/samsung/musicplus/library/metadata/SecMediaMetaDataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v8

    if-nez v8, :cond_1

    .line 199
    const v8, 0x7f0d00d6

    const v9, 0x7f100178

    iget-object v10, p1, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;->title:Ljava/lang/String;

    const/4 v11, 0x0

    invoke-direct {p0, v8, v9, v10, v11}, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->setItemEditText(IILjava/lang/String;Z)Landroid/widget/EditText;

    move-result-object v8

    iput-object v8, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mTitle:Landroid/widget/EditText;

    .line 205
    :goto_1
    iget-object v8, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mMetaRetriever:Lcom/samsung/musicplus/library/metadata/SecMediaMetaDataRetriever;

    if-eqz v8, :cond_2

    iget-object v8, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mMetaRetriever:Lcom/samsung/musicplus/library/metadata/SecMediaMetaDataRetriever;

    iget-object v9, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mMetaRetriever:Lcom/samsung/musicplus/library/metadata/SecMediaMetaDataRetriever;

    const/4 v9, 0x2

    invoke-virtual {v8, v9}, Lcom/samsung/musicplus/library/metadata/SecMediaMetaDataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v8

    if-nez v8, :cond_2

    .line 213
    const v8, 0x7f0d00d5

    const v9, 0x7f100026

    iget-object v10, p1, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;->artist:Ljava/lang/String;

    const/4 v11, 0x0

    invoke-direct {p0, v8, v9, v10, v11}, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->setItemEditText(IILjava/lang/String;Z)Landroid/widget/EditText;

    move-result-object v8

    iput-object v8, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mArtist:Landroid/widget/EditText;

    .line 220
    :goto_2
    iget-object v8, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mMetaRetriever:Lcom/samsung/musicplus/library/metadata/SecMediaMetaDataRetriever;

    if-eqz v8, :cond_3

    iget-object v8, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mMetaRetriever:Lcom/samsung/musicplus/library/metadata/SecMediaMetaDataRetriever;

    iget-object v9, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mMetaRetriever:Lcom/samsung/musicplus/library/metadata/SecMediaMetaDataRetriever;

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Lcom/samsung/musicplus/library/metadata/SecMediaMetaDataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v8

    if-nez v8, :cond_3

    .line 228
    const v8, 0x7f0d00d7

    const v9, 0x7f10001e

    iget-object v10, p1, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;->album:Ljava/lang/String;

    const/4 v11, 0x0

    invoke-direct {p0, v8, v9, v10, v11}, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->setItemEditText(IILjava/lang/String;Z)Landroid/widget/EditText;

    move-result-object v8

    iput-object v8, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mAlbum:Landroid/widget/EditText;

    .line 237
    :goto_3
    iget-object v8, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mTitle:Landroid/widget/EditText;

    iget-object v9, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v8, v9}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 238
    iget-object v8, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mArtist:Landroid/widget/EditText;

    iget-object v9, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v8, v9}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 239
    iget-object v8, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mAlbum:Landroid/widget/EditText;

    iget-object v9, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mTextWatcher:Landroid/text/TextWatcher;

    invoke-virtual {v8, v9}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 241
    iget-object v8, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mTitle:Landroid/widget/EditText;

    const/4 v9, 0x2

    new-array v9, v9, [Landroid/text/InputFilter;

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->filterSpecialChar:Landroid/text/InputFilter;

    aput-object v11, v9, v10

    const/4 v10, 0x1

    iget-object v11, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mInputFilers:Landroid/text/InputFilter;

    aput-object v11, v9, v10

    invoke-virtual {v8, v9}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 243
    iget-object v8, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mArtist:Landroid/widget/EditText;

    const/4 v9, 0x2

    new-array v9, v9, [Landroid/text/InputFilter;

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->filterSpecialChar:Landroid/text/InputFilter;

    aput-object v11, v9, v10

    const/4 v10, 0x1

    iget-object v11, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mInputFilers:Landroid/text/InputFilter;

    aput-object v11, v9, v10

    invoke-virtual {v8, v9}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 245
    iget-object v8, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mAlbum:Landroid/widget/EditText;

    const/4 v9, 0x2

    new-array v9, v9, [Landroid/text/InputFilter;

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->filterSpecialChar:Landroid/text/InputFilter;

    aput-object v11, v9, v10

    const/4 v10, 0x1

    iget-object v11, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mInputFilers:Landroid/text/InputFilter;

    aput-object v11, v9, v10

    invoke-virtual {v8, v9}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 249
    iget-wide v8, p1, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;->duration:J

    invoke-static {v3, v8, v9}, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils;->getTrackLength(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v7

    .line 250
    .local v7, "trackLength":Ljava/lang/String;
    const v8, 0x7f0d00d8

    const v9, 0x7f10017a

    invoke-static {v3, v7}, Lcom/samsung/musicplus/util/UiUtils;->getDurationTalkback(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {p0, v8, v9, v7, v10}, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->setItemText(IILjava/lang/String;Ljava/lang/String;)V

    .line 253
    const v8, 0x7f0d00d9

    const v9, 0x7f100090

    iget-object v10, p1, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;->genre:Ljava/lang/String;

    invoke-direct {p0, v8, v9, v10}, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->setItemText(IILjava/lang/String;)V

    .line 255
    const v8, 0x7f0d00da

    const v9, 0x7f10012d

    iget-object v10, p1, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;->year:Ljava/lang/String;

    invoke-direct {p0, v8, v9, v10}, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->setItemText(IILjava/lang/String;)V

    .line 257
    const v8, 0x7f0d00db

    const v9, 0x7f10017c

    iget-object v10, p1, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;->track:Ljava/lang/String;

    invoke-direct {p0, v8, v9, v10}, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->setItemText(IILjava/lang/String;)V

    .line 259
    const v8, 0x7f0d00dc

    const v9, 0x7f10008f

    iget-object v10, p1, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;->mimeType:Ljava/lang/String;

    invoke-direct {p0, v8, v9, v10}, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->setItemText(IILjava/lang/String;)V

    .line 261
    const v8, 0x7f0d00dd

    const v9, 0x7f100036

    iget v10, p1, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;->bitDepth:I

    invoke-static {v3, v10}, Lcom/samsung/musicplus/util/UiUtils;->getBitDepth(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v10

    invoke-direct {p0, v8, v9, v10}, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->setItemText(IILjava/lang/String;)V

    .line 264
    const v8, 0x7f0d00de

    const v9, 0x7f100145

    iget v10, p1, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;->samplingRate:I

    invoke-static {v3, v10}, Lcom/samsung/musicplus/util/UiUtils;->getSamplingRate(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v10

    invoke-direct {p0, v8, v9, v10}, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->setItemText(IILjava/lang/String;)V

    .line 267
    iget-wide v8, p1, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;->size:J

    invoke-static {v3, v8, v9}, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils;->getFileSize(Landroid/content/Context;J)[Ljava/lang/String;

    move-result-object v5

    .line 268
    .local v5, "fileSize":[Ljava/lang/String;
    const v8, 0x7f0d00df

    const v9, 0x7f10015c

    const/4 v10, 0x0

    aget-object v10, v5, v10

    const/4 v11, 0x1

    aget-object v11, v5, v11

    invoke-direct {p0, v8, v9, v10, v11}, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->setItemText(IILjava/lang/String;Ljava/lang/String;)V

    .line 270
    const v8, 0x7f0d00df

    invoke-virtual {p0, v8}, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->findViewById(I)Landroid/view/View;

    move-result-object v8

    const v9, 0x7f0d00e2

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    const/4 v9, 0x1

    aget-object v9, v5, v9

    invoke-virtual {v8, v9}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 273
    const v8, 0x7f0d00e0

    const v9, 0x7f10011f

    iget-object v10, p1, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;->data:Ljava/lang/String;

    invoke-direct {p0, v8, v9, v10}, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->setItemText(IILjava/lang/String;)V

    .line 275
    const v8, 0x7f0d00e4

    invoke-virtual {p0, v8}, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 276
    .local v4, "descriptiopn":Landroid/widget/TextView;
    iget-object v8, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mMetaRetriever:Lcom/samsung/musicplus/library/metadata/SecMediaMetaDataRetriever;

    if-eqz v8, :cond_4

    iget-object v8, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mMetaRetriever:Lcom/samsung/musicplus/library/metadata/SecMediaMetaDataRetriever;

    invoke-virtual {v8}, Lcom/samsung/musicplus/library/metadata/SecMediaMetaDataRetriever;->isUnicode()Z

    move-result v8

    if-eqz v8, :cond_4

    .line 277
    const v8, 0x7f10006f

    invoke-virtual {p0, v8}, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 280
    const/4 v8, 0x5

    invoke-virtual {v6, v8}, Landroid/widget/Spinner;->setSelection(I)V

    .line 281
    const/4 v8, 0x0

    invoke-virtual {v6, v8}, Landroid/widget/Spinner;->setEnabled(Z)V

    .line 285
    :goto_4
    return-void

    .line 180
    .end local v0    # "adapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/CharSequence;>;"
    .end local v4    # "descriptiopn":Landroid/widget/TextView;
    .end local v5    # "fileSize":[Ljava/lang/String;
    .end local v6    # "spinner":Landroid/widget/Spinner;
    .end local v7    # "trackLength":Ljava/lang/String;
    :cond_0
    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto/16 :goto_0

    .line 201
    .restart local v0    # "adapter":Landroid/widget/ArrayAdapter;, "Landroid/widget/ArrayAdapter<Ljava/lang/CharSequence;>;"
    .restart local v6    # "spinner":Landroid/widget/Spinner;
    :cond_1
    const v8, 0x7f0d00d6

    const v9, 0x7f100178

    iget-object v10, p1, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;->title:Ljava/lang/String;

    invoke-direct {p0, v8, v9, v10}, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->setItemEditText(IILjava/lang/String;)Landroid/widget/EditText;

    move-result-object v8

    iput-object v8, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mTitle:Landroid/widget/EditText;

    goto/16 :goto_1

    .line 215
    :cond_2
    const v8, 0x7f0d00d5

    const v9, 0x7f100026

    iget-object v10, p1, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;->artist:Ljava/lang/String;

    invoke-direct {p0, v8, v9, v10}, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->setItemEditText(IILjava/lang/String;)Landroid/widget/EditText;

    move-result-object v8

    iput-object v8, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mArtist:Landroid/widget/EditText;

    .line 216
    iget-object v8, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mArtist:Landroid/widget/EditText;

    invoke-virtual {v8}, Landroid/widget/EditText;->requestFocus()Z

    goto/16 :goto_2

    .line 230
    :cond_3
    const v8, 0x7f0d00d7

    const v9, 0x7f10001e

    iget-object v10, p1, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;->album:Ljava/lang/String;

    invoke-direct {p0, v8, v9, v10}, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->setItemEditText(IILjava/lang/String;)Landroid/widget/EditText;

    move-result-object v8

    iput-object v8, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mAlbum:Landroid/widget/EditText;

    goto/16 :goto_3

    .line 283
    .restart local v4    # "descriptiopn":Landroid/widget/TextView;
    .restart local v5    # "fileSize":[Ljava/lang/String;
    .restart local v7    # "trackLength":Ljava/lang/String;
    :cond_4
    const v8, 0x7f100070

    invoke-virtual {p0, v8}, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_4
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 82
    invoke-super {p0, p1}, Lcom/samsung/musicplus/MusicBaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 83
    const v2, 0x7f040046

    invoke-virtual {p0, v2}, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->setContentView(I)V

    .line 84
    invoke-direct {p0}, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->setCustomCancelDone()V

    .line 87
    invoke-virtual {p0}, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 88
    .local v1, "lp":Landroid/view/WindowManager$LayoutParams;
    iget v2, v1, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    or-int/lit8 v2, v2, 0x1

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    .line 89
    invoke-virtual {p0}, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 91
    if-nez p1, :cond_2

    .line 92
    invoke-virtual {p0}, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 93
    .local v0, "intent":Landroid/content/Intent;
    if-eqz v0, :cond_0

    .line 94
    const-string v2, "uri"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mBaseUri:Ljava/lang/String;

    .line 100
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    :goto_0
    invoke-static {}, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils;->getMetaData()Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mMetaData:Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;

    .line 102
    iget-object v2, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mMetaData:Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;

    if-eqz v2, :cond_1

    .line 103
    new-instance v2, Lcom/samsung/musicplus/library/metadata/SecMediaMetaDataRetriever;

    iget-object v3, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mMetaData:Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;

    iget-object v3, v3, Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;->data:Ljava/lang/String;

    invoke-direct {v2, v3}, Lcom/samsung/musicplus/library/metadata/SecMediaMetaDataRetriever;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mMetaRetriever:Lcom/samsung/musicplus/library/metadata/SecMediaMetaDataRetriever;

    .line 104
    iget-object v2, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mMetaData:Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;

    invoke-direct {p0, v2}, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->setMetaDataInfo(Lcom/samsung/musicplus/mediainfo/MediaInfoUtils$MetaData;)V

    .line 107
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, ""

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mToast:Landroid/widget/Toast;

    .line 109
    return-void

    .line 97
    :cond_2
    const-string v2, "uri"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mBaseUri:Ljava/lang/String;

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mMetaRetriever:Lcom/samsung/musicplus/library/metadata/SecMediaMetaDataRetriever;

    if-eqz v0, :cond_0

    .line 120
    iget-object v0, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mMetaRetriever:Lcom/samsung/musicplus/library/metadata/SecMediaMetaDataRetriever;

    invoke-virtual {v0}, Lcom/samsung/musicplus/library/metadata/SecMediaMetaDataRetriever;->release()V

    .line 122
    :cond_0
    invoke-super {p0}, Lcom/samsung/musicplus/MusicBaseActivity;->onDestroy()V

    .line 123
    return-void
.end method

.method protected onReceiveMediaState(Landroid/content/Intent;)V
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 127
    invoke-super {p0, p1}, Lcom/samsung/musicplus/MusicBaseActivity;->onReceiveMediaState(Landroid/content/Intent;)V

    .line 128
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 129
    .local v0, "action":Ljava/lang/String;
    const-string v1, "MediaInfo"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mMediaReceiver "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 130
    const-string v1, "android.intent.action.MEDIA_MOUNTED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 131
    invoke-virtual {p0}, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->finish()V

    .line 132
    const-string v1, "MediaInfo"

    const-string v2, "MEDIA_MOUNTED"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    :cond_0
    :goto_0
    return-void

    .line 133
    :cond_1
    const-string v1, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 135
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->finish()V

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 113
    const-string v0, "uri"

    iget-object v1, p0, Lcom/samsung/musicplus/mediainfo/MediaInfoEditActivity;->mBaseUri:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    invoke-super {p0, p1}, Lcom/samsung/musicplus/MusicBaseActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 115
    return-void
.end method
