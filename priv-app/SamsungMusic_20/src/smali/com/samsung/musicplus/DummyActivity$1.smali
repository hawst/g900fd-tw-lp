.class Lcom/samsung/musicplus/DummyActivity$1;
.super Landroid/content/BroadcastReceiver;
.source "DummyActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/DummyActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/DummyActivity;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/DummyActivity;)V
    .locals 0

    .prologue
    .line 43
    iput-object p1, p0, Lcom/samsung/musicplus/DummyActivity$1;->this$0:Lcom/samsung/musicplus/DummyActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 46
    const-string v1, "coverOpen"

    const/4 v2, 0x0

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 47
    .local v0, "coverOpen":Z
    # getter for: Lcom/samsung/musicplus/DummyActivity;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/DummyActivity;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mBroadcastReceiver coverOpen : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    if-eqz v0, :cond_0

    .line 49
    iget-object v1, p0, Lcom/samsung/musicplus/DummyActivity$1;->this$0:Lcom/samsung/musicplus/DummyActivity;

    invoke-virtual {v1}, Lcom/samsung/musicplus/DummyActivity;->finish()V

    .line 51
    :cond_0
    return-void
.end method
