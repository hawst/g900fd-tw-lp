.class public Lcom/samsung/musicplus/glwidget/model/ShadowMatrixUpdater;
.super Lcom/samsung/musicplus/glwidget/model/ModelMatrixUpdater;
.source "ShadowMatrixUpdater.java"


# instance fields
.field private mShadow:Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;


# direct methods
.method public constructor <init>([Lcom/samsung/musicplus/glwidget/model/Model;Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;)V
    .locals 0
    .param p1, "models"    # [Lcom/samsung/musicplus/glwidget/model/Model;
    .param p2, "shadow"    # Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/glwidget/model/ModelMatrixUpdater;-><init>([Lcom/samsung/musicplus/glwidget/model/Model;)V

    .line 16
    iput-object p2, p0, Lcom/samsung/musicplus/glwidget/model/ShadowMatrixUpdater;->mShadow:Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;

    .line 17
    return-void
.end method


# virtual methods
.method public updateMatrix(I[FI)V
    .locals 2
    .param p1, "index"    # I
    .param p2, "matrix"    # [F
    .param p3, "offset"    # I

    .prologue
    .line 21
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/model/ShadowMatrixUpdater;->mModels:[Lcom/samsung/musicplus/glwidget/model/Model;

    aget-object v0, v0, p1

    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/model/ShadowMatrixUpdater;->mShadow:Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;

    invoke-static {v0, v1, p2, p3}, Lcom/samsung/musicplus/glwidget/model/ModelUtils;->updateShadowMatrix(Lcom/samsung/musicplus/glwidget/model/Model;Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;[FI)V

    .line 22
    return-void
.end method
