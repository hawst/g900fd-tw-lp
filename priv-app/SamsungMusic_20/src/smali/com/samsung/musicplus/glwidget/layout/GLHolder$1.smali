.class Lcom/samsung/musicplus/glwidget/layout/GLHolder$1;
.super Ljava/lang/Object;
.source "GLHolder.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/glwidget/layout/GLHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/glwidget/layout/GLHolder;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/glwidget/layout/GLHolder;)V
    .locals 0

    .prologue
    .line 73
    iput-object p1, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder$1;->this$0:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 76
    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder$1;->this$0:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    # getter for: Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mAnimationStarted:Z
    invoke-static {v1}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->access$000(Lcom/samsung/musicplus/glwidget/layout/GLHolder;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder$1;->this$0:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    # getter for: Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mOnAnimationListener:Lcom/samsung/musicplus/glwidget/GLGalleryView$OnAnimationListener;
    invoke-static {v1}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->access$100(Lcom/samsung/musicplus/glwidget/layout/GLHolder;)Lcom/samsung/musicplus/glwidget/GLGalleryView$OnAnimationListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 77
    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder$1;->this$0:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    # getter for: Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mLayout:Lcom/samsung/musicplus/glwidget/layout/GalleryLayout;
    invoke-static {v1}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->access$200(Lcom/samsung/musicplus/glwidget/layout/GLHolder;)Lcom/samsung/musicplus/glwidget/layout/GalleryLayout;

    move-result-object v1

    invoke-interface {v1}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayout;->getSelectedDX()F

    move-result v1

    neg-float v1, v1

    iget-object v2, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder$1;->this$0:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    # getter for: Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mLayout:Lcom/samsung/musicplus/glwidget/layout/GalleryLayout;
    invoke-static {v2}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->access$200(Lcom/samsung/musicplus/glwidget/layout/GLHolder;)Lcom/samsung/musicplus/glwidget/layout/GalleryLayout;

    move-result-object v2

    invoke-interface {v2}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayout;->getMaxSelectedDx()F

    move-result v2

    div-float v0, v1, v2

    .line 81
    .local v0, "fraction":F
    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder$1;->this$0:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    # getter for: Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mOnAnimationListener:Lcom/samsung/musicplus/glwidget/GLGalleryView$OnAnimationListener;
    invoke-static {v1}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->access$100(Lcom/samsung/musicplus/glwidget/layout/GLHolder;)Lcom/samsung/musicplus/glwidget/GLGalleryView$OnAnimationListener;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder$1;->this$0:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    # getter for: Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mParent:Lcom/samsung/musicplus/glwidget/GLGalleryView;
    invoke-static {v2}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->access$300(Lcom/samsung/musicplus/glwidget/layout/GLHolder;)Lcom/samsung/musicplus/glwidget/GLGalleryView;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Lcom/samsung/musicplus/glwidget/GLGalleryView$OnAnimationListener;->onAnimationUpdate(Lcom/samsung/musicplus/glwidget/GLGalleryView;F)V

    .line 82
    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder$1;->this$0:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    # getter for: Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->access$500(Lcom/samsung/musicplus/glwidget/layout/GLHolder;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder$1;->this$0:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    # getter for: Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mAnimationUpdate:Ljava/lang/Runnable;
    invoke-static {v2}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->access$400(Lcom/samsung/musicplus/glwidget/layout/GLHolder;)Ljava/lang/Runnable;

    move-result-object v2

    const-wide/16 v4, 0x64

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 84
    .end local v0    # "fraction":F
    :cond_0
    return-void
.end method
