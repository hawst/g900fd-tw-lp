.class public abstract Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;
.super Ljava/lang/Object;
.source "GalleryLayoutBase.java"

# interfaces
.implements Lcom/samsung/musicplus/glwidget/layout/GalleryLayout;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$PositionInfo;,
        Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;,
        Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$Renderer;
    }
.end annotation


# static fields
.field protected static final DEBUG:Z = true

.field protected static final DEBUG_SCROLL:Z

.field public static final OrtographicConverter:Lcom/samsung/musicplus/glwidget/utils/CoordConverter;


# instance fields
.field protected LOG_TAG:Ljava/lang/String;

.field protected volatile mAdapter:Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;

.field protected volatile mAligned:Z

.field private mAnimationRunnable:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;

.field private mAnimationStarted:Z

.field private mFramesCount:I

.field private mGestureDetector:Landroid/view/GestureDetector;

.field private mGestureListener:Landroid/view/GestureDetector$OnGestureListener;

.field private mGettingDataTime:I

.field protected mHeight:I

.field private mNeedReallign:Z

.field protected mParent:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

.field private volatile mPerformanceTestInProgress:Z

.field private mPerformanceTestLock:Ljava/lang/Object;

.field private mRenderTime:I

.field private mRenderer:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$Renderer;

.field private mSelectedAdapterIndex:I

.field private mSelectedViewIndex:I

.field private mStopped:Z

.field protected mSurfaceRecreated:Z

.field private mToLeftPositionInfo:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$PositionInfo;

.field private mToRightPositionInfo:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$PositionInfo;

.field protected mTouched:Z

.field private mUsedModelsCount:I

.field private mUsedModelsCountInited:Z

.field protected mWidth:I

.field protected mWrapAdapter:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 130
    new-instance v0, Lcom/samsung/musicplus/glwidget/utils/CoordConverter$OrtographicConverter;

    invoke-direct {v0}, Lcom/samsung/musicplus/glwidget/utils/CoordConverter$OrtographicConverter;-><init>()V

    sput-object v0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->OrtographicConverter:Lcom/samsung/musicplus/glwidget/utils/CoordConverter;

    return-void
.end method

.method public constructor <init>(Lcom/samsung/musicplus/glwidget/layout/GLHolder;)V
    .locals 3
    .param p1, "parent"    # Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    .prologue
    const/4 v1, 0x0

    .line 132
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    const-class v0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->LOG_TAG:Ljava/lang/String;

    .line 112
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mPerformanceTestLock:Ljava/lang/Object;

    .line 114
    iput-boolean v1, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAligned:Z

    .line 116
    iput-boolean v1, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAnimationStarted:Z

    .line 124
    new-instance v0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$PositionInfo;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$PositionInfo;-><init>(I)V

    iput-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mToLeftPositionInfo:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$PositionInfo;

    .line 126
    new-instance v0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$PositionInfo;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$PositionInfo;-><init>(I)V

    iput-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mToRightPositionInfo:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$PositionInfo;

    .line 1250
    new-instance v0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$4;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$4;-><init>(Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;)V

    iput-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mGestureListener:Landroid/view/GestureDetector$OnGestureListener;

    .line 133
    iput-object p1, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mParent:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    .line 134
    new-instance v0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$Renderer;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$Renderer;-><init>(Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$1;)V

    iput-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mRenderer:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$Renderer;

    .line 136
    new-instance v0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;-><init>(Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;)V

    iput-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAnimationRunnable:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;

    .line 138
    new-instance v0, Landroid/view/GestureDetector;

    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mParent:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v1}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mGestureListener:Landroid/view/GestureDetector$OnGestureListener;

    invoke-direct {v0, v1, v2}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mGestureDetector:Landroid/view/GestureDetector;

    .line 139
    return-void
.end method

.method static synthetic access$100(Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->doPerformanceTest()V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;

    .prologue
    .line 27
    iget-boolean v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mPerformanceTestInProgress:Z

    return v0
.end method

.method static synthetic access$314(Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;J)I
    .locals 3
    .param p0, "x0"    # Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;
    .param p1, "x1"    # J

    .prologue
    .line 27
    iget v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mRenderTime:I

    int-to-long v0, v0

    add-long/2addr v0, p1

    long-to-int v0, v0

    iput v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mRenderTime:I

    return v0
.end method

.method static synthetic access$408(Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;)I
    .locals 2
    .param p0, "x0"    # Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;

    .prologue
    .line 27
    iget v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mFramesCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mFramesCount:I

    return v0
.end method

.method static synthetic access$500(Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mPerformanceTestLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$600(Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;II)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->checkReallignView(II)V

    return-void
.end method

.method static synthetic access$700(Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;)Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAnimationRunnable:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;

    return-object v0
.end method

.method static synthetic access$800(Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;

    .prologue
    .line 27
    iget-boolean v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAnimationStarted:Z

    return v0
.end method

.method static synthetic access$802(Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;
    .param p1, "x1"    # Z

    .prologue
    .line 27
    iput-boolean p1, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAnimationStarted:Z

    return p1
.end method

.method static synthetic access$900(Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;

    .prologue
    .line 27
    iget-boolean v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mStopped:Z

    return v0
.end method

.method private checkFixOutCoord([FII)V
    .locals 4
    .param p1, "outCoord"    # [F
    .param p2, "idx"    # I
    .param p3, "max"    # I

    .prologue
    const/4 v3, 0x0

    .line 552
    aget v0, p1, p2

    cmpg-float v0, v0, v3

    if-gez v0, :cond_2

    .line 553
    aget v0, p1, p2

    const/high16 v1, -0x40800000    # -1.0f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 555
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "wrong outCoord["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    aget v2, p1, p2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 557
    :cond_0
    aput v3, p1, p2

    .line 565
    :cond_1
    :goto_0
    return-void

    .line 558
    :cond_2
    aget v0, p1, p2

    int-to-float v1, p3

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_1

    .line 559
    aget v0, p1, p2

    int-to-float v1, p3

    cmpl-float v0, v0, v1

    if-lez v0, :cond_3

    .line 561
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "wrong outCoord["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    aget v2, p1, p2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " max: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 563
    :cond_3
    add-int/lit8 v0, p3, -0x1

    int-to-float v0, v0

    aput v0, p1, p2

    goto :goto_0
.end method

.method private checkReallignView(II)V
    .locals 4
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 217
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mParent:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v0}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->checkUIThread()V

    .line 218
    iput p1, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mWidth:I

    .line 219
    iput p2, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mHeight:I

    .line 220
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAdapter:Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAdapter:Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;

    invoke-interface {v0}, Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    if-eqz p1, :cond_0

    if-nez p2, :cond_2

    .line 222
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "checkReallignView skipped mAdapter: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAdapter:Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " width: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " height: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 225
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAdapter:Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;

    if-eqz v0, :cond_1

    .line 226
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mAdapter.getCount(): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAdapter:Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;

    invoke-interface {v2}, Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 243
    :cond_1
    :goto_0
    return-void

    .line 230
    :cond_2
    iget-boolean v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mStopped:Z

    if-eqz v0, :cond_3

    .line 231
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->LOG_TAG:Ljava/lang/String;

    const-string v1, "checkReallignView during pause"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 232
    iput-boolean v3, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mNeedReallign:Z

    .line 236
    :cond_3
    iput-boolean v2, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mUsedModelsCountInited:Z

    .line 237
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->realignView()V

    .line 238
    iput-boolean v2, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mSurfaceRecreated:Z

    .line 239
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->updateViewData()V

    .line 240
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->applyScroll(F)Z

    .line 241
    iput-boolean v3, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAligned:Z

    .line 242
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mParent:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v0}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->requestRender()V

    goto :goto_0
.end method

.method private checkUpdateSelected(FFF)Z
    .locals 4
    .param p1, "dx"    # F
    .param p2, "maxDx"    # F
    .param p3, "origDx"    # F

    .prologue
    const/4 v3, 0x0

    .line 1114
    const/4 v1, 0x0

    .line 1115
    .local v1, "failed":Z
    cmpl-float v2, p1, v3

    if-lez v2, :cond_2

    const/4 v0, 0x1

    .line 1118
    .local v0, "dir":I
    :goto_0
    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->updateSelected(I)I

    move-result v2

    if-eq v2, v0, :cond_0

    cmpl-float v2, p1, v3

    if-eqz v2, :cond_0

    .line 1123
    iget-object v2, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->LOG_TAG:Ljava/lang/String;

    const-string v3, "unable to change selection during scroll, check limitScroll method"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1124
    invoke-direct {p0, p2, p3}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->debugScrollState(FF)V

    .line 1125
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAligned:Z

    .line 1126
    iget v2, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mWidth:I

    iget v3, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mHeight:I

    invoke-direct {p0, v2, v3}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->checkReallignView(II)V

    .line 1127
    const/4 v1, 0x1

    .line 1129
    :cond_0
    iget-boolean v2, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mPerformanceTestInProgress:Z

    if-nez v2, :cond_1

    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->isFlinging()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1130
    iget-object v2, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mParent:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v2}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->fireOnSelectionChange()V

    .line 1132
    :cond_1
    return v1

    .line 1115
    .end local v0    # "dir":I
    :cond_2
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private debugScrollState(FF)V
    .locals 0
    .param p1, "maxDx"    # F
    .param p2, "origDx"    # F

    .prologue
    .line 1145
    return-void
.end method

.method private doPerformanceTest()V
    .locals 19

    .prologue
    .line 818
    const-wide/16 v14, 0xbb8

    invoke-static {v14, v15}, Landroid/os/SystemClock;->sleep(J)V

    .line 819
    const/16 v12, 0x7530

    .line 820
    .local v12, "testTime":I
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v10

    .line 821
    .local v10, "startTime":J
    const/4 v14, 0x0

    move-object/from16 v0, p0

    iput v14, v0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mRenderTime:I

    .line 822
    const/4 v14, 0x0

    move-object/from16 v0, p0

    iput v14, v0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mFramesCount:I

    .line 823
    const/4 v14, 0x0

    move-object/from16 v0, p0

    iput v14, v0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mGettingDataTime:I

    .line 824
    const/4 v14, 0x1

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mPerformanceTestInProgress:Z

    .line 826
    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mWidth:I

    mul-int/lit8 v13, v14, 0x2

    .line 827
    .local v13, "testWidth":I
    const/4 v5, 0x2

    .line 828
    .local v5, "testSpeed":I
    const-wide/16 v6, 0x0

    .line 829
    .local v6, "scrollTime":J
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v14

    sub-long/2addr v14, v10

    int-to-long v0, v12

    move-wide/from16 v16, v0

    cmp-long v14, v14, v16

    if-gez v14, :cond_4

    .line 833
    const/4 v3, 0x0

    .line 834
    .local v3, "limited":Z
    const/4 v2, 0x0

    .line 835
    .local v2, "i":I
    :goto_0
    if-ge v2, v13, :cond_2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v14

    sub-long/2addr v14, v10

    int-to-long v0, v12

    move-wide/from16 v16, v0

    cmp-long v14, v14, v16

    if-gez v14, :cond_2

    if-nez v3, :cond_2

    .line 836
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mPerformanceTestLock:Ljava/lang/Object;

    monitor-enter v15

    .line 837
    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtimeNanos()J

    move-result-wide v8

    .line 838
    .local v8, "start":J
    neg-int v14, v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->handleScroll(I)Z

    move-result v3

    .line 839
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtimeNanos()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v16

    sub-long v16, v16, v8

    add-long v6, v6, v16

    .line 841
    :try_start_1
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mPerformanceTestInProgress:Z

    if-eqz v14, :cond_1

    .line 842
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mPerformanceTestLock:Ljava/lang/Object;

    invoke-virtual {v14}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 847
    :cond_1
    :goto_1
    :try_start_2
    monitor-exit v15

    .line 835
    add-int/2addr v2, v5

    goto :goto_0

    .line 847
    .end local v8    # "start":J
    :catchall_0
    move-exception v14

    monitor-exit v15
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v14

    .line 852
    :cond_2
    const/4 v3, 0x0

    .line 853
    const/4 v2, 0x0

    .line 854
    :goto_2
    if-ge v2, v13, :cond_0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v14

    sub-long/2addr v14, v10

    int-to-long v0, v12

    move-wide/from16 v16, v0

    cmp-long v14, v14, v16

    if-gez v14, :cond_0

    if-nez v3, :cond_0

    .line 855
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mPerformanceTestLock:Ljava/lang/Object;

    monitor-enter v15

    .line 856
    :try_start_3
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtimeNanos()J

    move-result-wide v8

    .line 857
    .restart local v8    # "start":J
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->handleScroll(I)Z

    .line 858
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtimeNanos()J
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-wide v16

    sub-long v16, v16, v8

    add-long v6, v6, v16

    .line 860
    :try_start_4
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mPerformanceTestInProgress:Z

    if-eqz v14, :cond_3

    .line 861
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mPerformanceTestLock:Ljava/lang/Object;

    invoke-virtual {v14}, Ljava/lang/Object;->wait()V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 866
    :cond_3
    :goto_3
    :try_start_5
    monitor-exit v15

    .line 854
    add-int/2addr v2, v5

    goto :goto_2

    .line 866
    .end local v8    # "start":J
    :catchall_1
    move-exception v14

    monitor-exit v15
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    throw v14

    .line 869
    .end local v2    # "i":I
    .end local v3    # "limited":Z
    :cond_4
    long-to-double v14, v6

    const-wide v16, 0x412e848000000000L    # 1000000.0

    div-double v14, v14, v16

    double-to-long v6, v14

    .line 870
    const-string v14, "Render time per frame: %.2f(ms)%nScroll time per frame: %.4f(ms)%nFrames: %d%nTest time: %d(ms)%nRender: %d(ms)%nAdapter: %d(ms)%nScroll time: %d"

    const/4 v15, 0x7

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mRenderTime:I

    move/from16 v17, v0

    move/from16 v0, v17

    int-to-float v0, v0

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mFramesCount:I

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    div-float v17, v17, v18

    invoke-static/range {v17 .. v17}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x1

    long-to-float v0, v6

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mFramesCount:I

    move/from16 v18, v0

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    div-float v17, v17, v18

    invoke-static/range {v17 .. v17}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x2

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mFramesCount:I

    move/from16 v17, v0

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x3

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x4

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mRenderTime:I

    move/from16 v17, v0

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x5

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mGettingDataTime:I

    move/from16 v17, v0

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-static {v14, v15}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 874
    .local v4, "res":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->LOG_TAG:Ljava/lang/String;

    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "performanceTest Finished - "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 875
    const-wide/16 v14, 0x3e8

    invoke-static {v14, v15}, Landroid/os/SystemClock;->sleep(J)V

    .line 876
    const/4 v14, 0x0

    move-object/from16 v0, p0

    iput-boolean v14, v0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mPerformanceTestInProgress:Z

    .line 877
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mParent:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    new-instance v15, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$3;

    move-object/from16 v0, p0

    invoke-direct {v15, v0, v4}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$3;-><init>(Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;Ljava/lang/String;)V

    invoke-virtual {v14, v15}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->post(Ljava/lang/Runnable;)Z

    .line 892
    return-void

    .line 863
    .end local v4    # "res":Ljava/lang/String;
    .restart local v2    # "i":I
    .restart local v3    # "limited":Z
    .restart local v8    # "start":J
    :catch_0
    move-exception v14

    goto/16 :goto_3

    .line 844
    :catch_1
    move-exception v14

    goto/16 :goto_1
.end method

.method private doPositionSelectedView(F)Z
    .locals 1
    .param p1, "dx"    # F

    .prologue
    .line 1188
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->positionSelectedView(F)Z

    move-result v0

    return v0
.end method

.method private doPositionView(Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$PositionInfo;F)Z
    .locals 6
    .param p1, "inf"    # Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$PositionInfo;
    .param p2, "dx"    # F

    .prologue
    .line 1165
    iget v0, p1, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$PositionInfo;->mDir:I

    if-lez v0, :cond_0

    iget v0, p1, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$PositionInfo;->mIndex:I

    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->getUsedModelsCount()I

    move-result v1

    if-ge v0, v1, :cond_1

    :cond_0
    iget v0, p1, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$PositionInfo;->mDir:I

    if-gez v0, :cond_2

    iget v0, p1, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$PositionInfo;->mPosition:I

    if-gez v0, :cond_2

    .line 1166
    :cond_1
    const/4 v0, 0x0

    .line 1181
    :goto_0
    return v0

    .line 1167
    :cond_2
    iget v0, p1, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$PositionInfo;->mIndex:I

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->relIndex(I)I

    move-result v4

    .line 1171
    .local v4, "relIndex":I
    iget v1, p1, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$PositionInfo;->mDir:I

    iget v2, p1, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$PositionInfo;->mPosition:I

    iget v3, p1, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$PositionInfo;->mPrevIndex:I

    move-object v0, p0

    move v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->positionView(IIIIF)V

    .line 1173
    iput v4, p1, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$PositionInfo;->mPrevIndex:I

    .line 1174
    iget v0, p1, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$PositionInfo;->mDir:I

    if-lez v0, :cond_3

    .line 1175
    iget v0, p1, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$PositionInfo;->mPosition:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p1, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$PositionInfo;->mPosition:I

    .line 1176
    iget v0, p1, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$PositionInfo;->mIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p1, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$PositionInfo;->mIndex:I

    .line 1181
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    .line 1178
    :cond_3
    iget v0, p1, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$PositionInfo;->mPosition:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p1, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$PositionInfo;->mPosition:I

    .line 1179
    iget v0, p1, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$PositionInfo;->mIndex:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p1, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$PositionInfo;->mIndex:I

    goto :goto_1
.end method

.method private doUpdateItemData(II)V
    .locals 6
    .param p1, "viewIdx"    # I
    .param p2, "adapterIdx"    # I

    .prologue
    .line 598
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 599
    .local v0, "startTime":J
    invoke-virtual {p0, p1, p2}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->updateItemData(II)V

    .line 600
    iget-boolean v2, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mPerformanceTestInProgress:Z

    if-eqz v2, :cond_0

    .line 601
    iget v2, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mGettingDataTime:I

    int-to-long v2, v2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    sub-long/2addr v4, v0

    add-long/2addr v2, v4

    long-to-int v2, v2

    iput v2, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mGettingDataTime:I

    .line 603
    :cond_0
    return-void
.end method

.method private isVisiblePosition(I)Z
    .locals 1
    .param p1, "adaptedIndex"    # I

    .prologue
    .line 651
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAdapter:Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;

    if-eqz v0, :cond_0

    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAdapter:Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;

    invoke-interface {v0}, Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;->getCount()I

    move-result v0

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setSelection(IZ)V
    .locals 3
    .param p1, "position"    # I
    .param p2, "force"    # Z

    .prologue
    .line 458
    if-nez p2, :cond_0

    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->getSelection()I

    move-result v0

    if-eq v0, p1, :cond_1

    .line 460
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setSelection: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 464
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAdapter:Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->getSelection()I

    move-result v0

    sub-int v0, p1, v0

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->getUsedModelsCount()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 465
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->animateToChild(I)V

    .line 473
    :goto_0
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->LOG_TAG:Ljava/lang/String;

    const-string v1, "setSelection end"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 476
    :cond_1
    return-void

    .line 468
    :cond_2
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->setSelectedAdapterIndex(I)V

    .line 469
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAligned:Z

    .line 470
    iget v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mWidth:I

    iget v1, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mHeight:I

    invoke-direct {p0, v0, v1}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->checkReallignView(II)V

    goto :goto_0
.end method

.method private updateSelectedAdapterDx(I)V
    .locals 2
    .param p1, "di"    # I

    .prologue
    .line 198
    iget v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mSelectedAdapterIndex:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mSelectedAdapterIndex:I

    .line 199
    iget-boolean v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mWrapAdapter:Z

    if-eqz v0, :cond_1

    .line 200
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAdapter:Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAdapter:Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;

    invoke-interface {v0}, Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 201
    iget v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mSelectedAdapterIndex:I

    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAdapter:Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;

    invoke-interface {v1}, Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;->getCount()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAdapter:Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;

    invoke-interface {v1}, Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;->getCount()I

    move-result v1

    rem-int/2addr v0, v1

    iput v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mSelectedAdapterIndex:I

    .line 211
    :cond_0
    :goto_0
    iget v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mSelectedViewIndex:I

    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->getUsedModelsCount()I

    move-result v1

    add-int/2addr v0, v1

    add-int/2addr v0, p1

    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->getUsedModelsCount()I

    move-result v1

    rem-int/2addr v0, v1

    iput v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mSelectedViewIndex:I

    .line 213
    return-void

    .line 205
    :cond_1
    iget v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mSelectedAdapterIndex:I

    if-gez v0, :cond_2

    .line 206
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mSelectedAdapterIndex:I

    goto :goto_0

    .line 207
    :cond_2
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAdapter:Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mSelectedAdapterIndex:I

    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAdapter:Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;

    invoke-interface {v1}, Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;->getCount()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 208
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAdapter:Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;

    invoke-interface {v0}, Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mSelectedAdapterIndex:I

    goto :goto_0
.end method

.method private updateViewData(I)V
    .locals 9
    .param p1, "di"    # I

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 612
    iget-object v7, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAdapter:Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAdapter:Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;

    invoke-interface {v7}, Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;->getCount()I

    move-result v7

    if-eqz v7, :cond_0

    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->getModelsCount()I

    move-result v7

    if-eqz v7, :cond_0

    if-nez p1, :cond_1

    .line 636
    :cond_0
    :goto_0
    return-void

    .line 616
    :cond_1
    if-lez p1, :cond_3

    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->getUsedModelsCount()I

    move-result v7

    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->getSelectedModelPosition()I

    move-result v8

    sub-int/2addr v7, v8

    if-le p1, v7, :cond_3

    move v1, v6

    .line 617
    .local v1, "full":Z
    :goto_1
    if-nez v1, :cond_2

    if-gez p1, :cond_4

    neg-int v7, p1

    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->getSelectedModelPosition()I

    move-result v8

    if-le v7, v8, :cond_4

    :cond_2
    move v1, v6

    .line 619
    :goto_2
    if-eqz v1, :cond_5

    .line 620
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->updateViewData()V

    goto :goto_0

    .end local v1    # "full":Z
    :cond_3
    move v1, v5

    .line 616
    goto :goto_1

    .restart local v1    # "full":Z
    :cond_4
    move v1, v5

    .line 617
    goto :goto_2

    .line 622
    :cond_5
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->getUsedModelsCount()I

    move-result v5

    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->getSelectedModelPosition()I

    move-result v6

    sub-int/2addr v5, v6

    add-int/lit8 v2, v5, -0x1

    .line 623
    .local v2, "relIndx":I
    if-gez p1, :cond_6

    .line 624
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->getSelectedModelPosition()I

    move-result v5

    neg-int v2, v5

    .line 625
    :cond_6
    invoke-virtual {p0, v2}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->relIndex(I)I

    move-result v3

    .line 626
    .local v3, "viewIdx":I
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->getSelection()I

    move-result v5

    add-int v0, v5, v2

    .line 627
    .local v0, "adapterIdx":I
    iget-boolean v5, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mWrapAdapter:Z

    if-eqz v5, :cond_7

    .line 628
    iget-object v5, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAdapter:Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;

    invoke-interface {v5}, Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;->getCount()I

    move-result v5

    add-int/2addr v5, v0

    iget-object v6, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAdapter:Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;

    invoke-interface {v6}, Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;->getCount()I

    move-result v6

    rem-int v0, v5, v6

    .line 630
    :cond_7
    invoke-direct {p0, v0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->isVisiblePosition(I)Z

    move-result v4

    .line 631
    .local v4, "visible":Z
    if-eqz v4, :cond_8

    .line 632
    invoke-direct {p0, v3, v0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->doUpdateItemData(II)V

    .line 634
    :cond_8
    invoke-virtual {p0, v3, v4}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->setModelVisible(IZ)V

    goto :goto_0
.end method


# virtual methods
.method protected animateScroll(II)V
    .locals 1
    .param p1, "delta"    # I
    .param p2, "time"    # I

    .prologue
    .line 716
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAnimationRunnable:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;->animateScroll(II)V

    .line 717
    return-void
.end method

.method protected abstract animateToChild(I)V
.end method

.method public animationFinished()Z
    .locals 1

    .prologue
    .line 436
    iget-boolean v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mTouched:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAnimationRunnable:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;

    invoke-virtual {v0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;->scrollFinished()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected declared-synchronized applyScroll(F)Z
    .locals 13
    .param p1, "dx"    # F

    .prologue
    const/4 v12, 0x0

    const/4 v7, 0x1

    const/4 v9, 0x0

    .line 995
    monitor-enter p0

    :try_start_0
    iget-boolean v10, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mStopped:Z

    if-eqz v10, :cond_0

    .line 996
    iget-object v10, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->LOG_TAG:Ljava/lang/String;

    const-string v11, "applyScroll ingored during pause"

    invoke-static {v10, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1101
    :goto_0
    monitor-exit p0

    return v9

    .line 999
    :cond_0
    const/high16 v3, 0x7f800000    # Float.POSITIVE_INFINITY

    .line 1001
    .local v3, "maxDx":F
    :try_start_1
    iget-object v10, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAdapter:Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;

    invoke-interface {v10}, Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;->getCount()I

    move-result v10

    if-ne v10, v7, :cond_4

    .line 1002
    const/4 v3, 0x0

    .line 1011
    :cond_1
    :goto_1
    move v5, p1

    .line 1013
    .local v5, "origDx":F
    const/4 v1, 0x0

    .line 1014
    .local v1, "limitDelta":F
    invoke-static {p1}, Ljava/lang/Math;->abs(F)F

    move-result v10

    cmpl-float v10, v10, v3

    if-lez v10, :cond_5

    move v2, v7

    .line 1015
    .local v2, "limited":Z
    :goto_2
    if-eqz v2, :cond_2

    .line 1016
    cmpg-float v10, p1, v12

    if-gez v10, :cond_6

    .line 1017
    add-float v1, p1, v3

    .line 1018
    neg-float p1, v3

    .line 1025
    :cond_2
    :goto_3
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->startPositioning(F)V

    .line 1027
    const/4 v8, 0x0

    .line 1037
    .local v8, "tries":I
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->getSelection()I

    move-result v6

    .line 1039
    .local v6, "originalSelection":I
    :goto_4
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->doPositionSelectedView(F)Z

    move-result v10

    if-nez v10, :cond_3

    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->getUsedModelsCount()I

    move-result v10

    if-gt v8, v10, :cond_3

    .line 1040
    invoke-direct {p0, p1, v3, v5}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->checkUpdateSelected(FFF)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 1041
    const/4 v2, 0x1

    .line 1047
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->getUsedModelsCount()I

    move-result v10

    if-le v8, v10, :cond_9

    .line 1050
    cmpl-float v9, p1, v12

    if-nez v9, :cond_8

    .line 1051
    invoke-direct {p0, v3, v5}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->debugScrollState(FF)V

    .line 1052
    new-instance v9, Ljava/lang/RuntimeException;

    const-string v10, "Unable to change selected view, check your setupSelectedView method!"

    invoke-direct {v9, v10}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 995
    .end local v1    # "limitDelta":F
    .end local v2    # "limited":Z
    .end local v3    # "maxDx":F
    .end local v5    # "origDx":F
    .end local v6    # "originalSelection":I
    .end local v8    # "tries":I
    :catchall_0
    move-exception v9

    monitor-exit p0

    throw v9

    .line 1003
    .restart local v3    # "maxDx":F
    :cond_4
    :try_start_2
    iget-boolean v10, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mWrapAdapter:Z

    if-nez v10, :cond_1

    .line 1005
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->limitScroll(F)F

    move-result v10

    invoke-static {v10}, Ljava/lang/Math;->abs(F)F

    move-result v3

    goto :goto_1

    .restart local v1    # "limitDelta":F
    .restart local v5    # "origDx":F
    :cond_5
    move v2, v9

    .line 1014
    goto :goto_2

    .line 1020
    .restart local v2    # "limited":Z
    :cond_6
    sub-float v1, p1, v3

    .line 1021
    move p1, v3

    goto :goto_3

    .line 1044
    .restart local v6    # "originalSelection":I
    .restart local v8    # "tries":I
    :cond_7
    add-int/lit8 v8, v8, 0x1

    goto :goto_4

    .line 1065
    :cond_8
    invoke-virtual {p0, v6}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->setSelectedAdapterIndex(I)V

    .line 1066
    iget-object v9, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->LOG_TAG:Ljava/lang/String;

    const-string v10, "unable to change selection during scroll"

    invoke-static {v9, v10}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1067
    invoke-direct {p0, v3, v5}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->debugScrollState(FF)V

    .line 1068
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAligned:Z

    .line 1069
    iget v9, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mWidth:I

    iget v10, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mHeight:I

    invoke-direct {p0, v9, v10}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->checkReallignView(II)V

    move v9, v7

    .line 1071
    goto/16 :goto_0

    .line 1075
    :cond_9
    const/4 v10, 0x0

    invoke-virtual {p0, v10}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->relIndex(I)I

    move-result v0

    .line 1076
    .local v0, "index":I
    iget-object v10, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mToRightPositionInfo:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$PositionInfo;

    iput v0, v10, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$PositionInfo;->mPrevIndex:I

    .line 1077
    iget-object v10, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mToRightPositionInfo:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$PositionInfo;

    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->getSelectedModelPosition()I

    move-result v11

    add-int/lit8 v11, v11, 0x1

    iput v11, v10, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$PositionInfo;->mPosition:I

    .line 1078
    iget-object v10, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mToRightPositionInfo:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$PositionInfo;

    const/4 v11, 0x1

    iput v11, v10, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$PositionInfo;->mIndex:I

    .line 1080
    iget-object v10, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mToLeftPositionInfo:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$PositionInfo;

    iput v0, v10, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$PositionInfo;->mPrevIndex:I

    .line 1081
    iget-object v10, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mToLeftPositionInfo:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$PositionInfo;

    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->getSelectedModelPosition()I

    move-result v11

    add-int/lit8 v11, v11, -0x1

    iput v11, v10, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$PositionInfo;->mPosition:I

    .line 1082
    iget-object v10, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mToLeftPositionInfo:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$PositionInfo;

    const/4 v11, -0x1

    iput v11, v10, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$PositionInfo;->mIndex:I

    .line 1084
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->getSelectedDX()F

    move-result v10

    cmpg-float v10, v10, v12

    if-gez v10, :cond_a

    .line 1085
    .local v7, "toRight":Z
    :goto_5
    const/4 v4, 0x1

    .local v4, "modelCount":I
    :goto_6
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->getUsedModelsCount()I

    move-result v9

    if-ge v4, v9, :cond_d

    .line 1086
    if-eqz v7, :cond_b

    iget-object v9, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mToRightPositionInfo:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$PositionInfo;

    invoke-direct {p0, v9, p1}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->doPositionView(Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$PositionInfo;F)Z

    move-result v9

    if-eqz v9, :cond_b

    .line 1087
    const/4 v7, 0x0

    .line 1085
    :goto_7
    add-int/lit8 v4, v4, 0x1

    goto :goto_6

    .end local v4    # "modelCount":I
    .end local v7    # "toRight":Z
    :cond_a
    move v7, v9

    .line 1084
    goto :goto_5

    .line 1088
    .restart local v4    # "modelCount":I
    .restart local v7    # "toRight":Z
    :cond_b
    iget-object v9, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mToLeftPositionInfo:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$PositionInfo;

    invoke-direct {p0, v9, p1}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->doPositionView(Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$PositionInfo;F)Z

    move-result v9

    if-eqz v9, :cond_c

    .line 1089
    const/4 v7, 0x1

    goto :goto_7

    .line 1091
    :cond_c
    iget-object v9, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mToRightPositionInfo:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$PositionInfo;

    invoke-direct {p0, v9, p1}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->doPositionView(Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$PositionInfo;F)Z

    goto :goto_7

    .line 1095
    :cond_d
    if-eqz v2, :cond_e

    .line 1096
    invoke-virtual {p0, p1, v1}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->handleOverScroll(FF)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_e
    move v9, v2

    .line 1101
    goto/16 :goto_0
.end method

.method protected convertDx(I)F
    .locals 1
    .param p1, "dx"    # I

    .prologue
    .line 397
    int-to-float v0, p1

    return v0
.end method

.method protected doGetAlbumArtForCoord([F[F)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "inCoord"    # [F
    .param p2, "outCoord"    # [F

    .prologue
    .line 415
    const/4 v0, 0x0

    return-object v0
.end method

.method protected doGetSelectedDx()F
    .locals 1

    .prologue
    .line 939
    const/4 v0, 0x0

    return v0
.end method

.method public getAlbumArtForCoord([F[F)Landroid/graphics/Bitmap;
    .locals 6
    .param p1, "inCoord"    # [F
    .param p2, "outCoord"    # [F

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 522
    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAdapter:Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAdapter:Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;

    invoke-interface {v1}, Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;->getCount()I

    move-result v1

    if-lez v1, :cond_0

    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->getModelsCount()I

    move-result v1

    if-lez v1, :cond_0

    iget-boolean v1, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAligned:Z

    if-nez v1, :cond_1

    .line 540
    :cond_0
    :goto_0
    return-object v0

    .line 525
    :cond_1
    if-eqz p1, :cond_2

    if-eqz p2, :cond_2

    aget v1, p1, v4

    cmpl-float v1, v1, v3

    if-ltz v1, :cond_2

    aget v1, p1, v4

    iget v2, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mWidth:I

    int-to-float v2, v2

    cmpg-float v1, v1, v2

    if-gez v1, :cond_2

    aget v1, p1, v5

    cmpl-float v1, v1, v3

    if-ltz v1, :cond_2

    aget v1, p1, v5

    iget v2, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mHeight:I

    int-to-float v2, v2

    cmpg-float v1, v1, v2

    if-gez v1, :cond_2

    .line 527
    invoke-virtual {p0, p1, p2}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->doGetAlbumArtForCoord([F[F)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 529
    .local v0, "res":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_0

    .line 530
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-direct {p0, p2, v4, v1}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->checkFixOutCoord([FII)V

    .line 531
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    invoke-direct {p0, p2, v5, v1}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->checkFixOutCoord([FII)V

    goto :goto_0

    .line 534
    .end local v0    # "res":Landroid/graphics/Bitmap;
    :cond_2
    if-eqz p1, :cond_3

    if-nez p2, :cond_4

    .line 535
    :cond_3
    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->LOG_TAG:Ljava/lang/String;

    const-string v2, "inCoord or outCoord is null"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 537
    :cond_4
    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "wrong inCoord in getAlbumArtForCoord x: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget v3, p1, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " y: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget v3, p1, v5

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected getConverter()Lcom/samsung/musicplus/glwidget/utils/CoordConverter;
    .locals 1

    .prologue
    .line 428
    sget-object v0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->OrtographicConverter:Lcom/samsung/musicplus/glwidget/utils/CoordConverter;

    return-object v0
.end method

.method protected getDebugState()Ljava/lang/String;
    .locals 1

    .prologue
    .line 573
    const/4 v0, 0x0

    return-object v0
.end method

.method protected abstract getMaxScrollWidth()F
.end method

.method protected abstract getModelsCount()I
.end method

.method public bridge synthetic getRenderer()Landroid/opengl/GLSurfaceView$Renderer;
    .locals 1

    .prologue
    .line 27
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->getRenderer()Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$Renderer;

    move-result-object v0

    return-object v0
.end method

.method public getRenderer()Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$Renderer;
    .locals 1

    .prologue
    .line 739
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mRenderer:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$Renderer;

    return-object v0
.end method

.method public getSelectedDX()F
    .locals 1

    .prologue
    .line 1137
    iget-boolean v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAligned:Z

    if-eqz v0, :cond_0

    .line 1138
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->doGetSelectedDx()F

    move-result v0

    .line 1140
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected abstract getSelectedModelPosition()I
.end method

.method public getSelection()I
    .locals 1

    .prologue
    .line 441
    iget v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mSelectedAdapterIndex:I

    return v0
.end method

.method protected getUsedModelsCount()I
    .locals 2

    .prologue
    .line 584
    iget-boolean v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mUsedModelsCountInited:Z

    if-nez v0, :cond_0

    .line 585
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mUsedModelsCountInited:Z

    .line 586
    iget-boolean v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mWrapAdapter:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAdapter:Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;

    if-eqz v0, :cond_1

    .line 587
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->getModelsCount()I

    move-result v0

    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAdapter:Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;

    invoke-interface {v1}, Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;->getCount()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mUsedModelsCount:I

    .line 592
    :goto_0
    iget v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mSelectedAdapterIndex:I

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->setSelectedAdapterIndex(I)V

    .line 594
    :cond_0
    iget v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mUsedModelsCount:I

    return v0

    .line 589
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->getModelsCount()I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mUsedModelsCount:I

    goto :goto_0
.end method

.method protected handleOverScroll(FF)V
    .locals 0
    .param p1, "dx"    # F
    .param p2, "limitDelta"    # F

    .prologue
    .line 365
    return-void
.end method

.method protected handleScroll(I)Z
    .locals 6
    .param p1, "dx"    # I

    .prologue
    .line 902
    if-eqz p1, :cond_0

    iget-boolean v5, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAligned:Z

    if-nez v5, :cond_1

    .line 906
    :cond_0
    const/4 v3, 0x0

    .line 935
    :goto_0
    return v3

    .line 908
    :cond_1
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->convertDx(I)F

    move-result v4

    .line 913
    .local v4, "pDx":F
    const/4 v3, 0x0

    .line 916
    .local v3, "limited":Z
    const/4 v2, 0x1

    .line 917
    .local v2, "dir":I
    if-gez p1, :cond_2

    .line 918
    const/4 v2, -0x1

    .line 922
    :cond_2
    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v0

    .line 925
    .local v0, "absDx":F
    :goto_1
    const/4 v5, 0x0

    cmpl-float v5, v0, v5

    if-lez v5, :cond_3

    if-nez v3, :cond_3

    .line 926
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->getMaxScrollWidth()F

    move-result v5

    invoke-static {v5, v0}, Ljava/lang/Math;->min(FF)F

    move-result v1

    .line 927
    .local v1, "d":F
    int-to-float v5, v2

    mul-float/2addr v5, v1

    invoke-virtual {p0, v5}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->applyScroll(F)Z

    move-result v3

    .line 928
    sub-float/2addr v0, v1

    .line 929
    goto :goto_1

    .line 933
    .end local v1    # "d":F
    :cond_3
    iget-object v5, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mParent:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v5}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->requestRender()V

    goto :goto_0
.end method

.method public isFlinging()Z
    .locals 1

    .prologue
    .line 780
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAnimationRunnable:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;

    invoke-virtual {v0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;->isFlinging()Z

    move-result v0

    return v0
.end method

.method protected isScrollFinished()Z
    .locals 1

    .prologue
    .line 706
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAnimationRunnable:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;

    invoke-virtual {v0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;->scrollFinished()Z

    move-result v0

    return v0
.end method

.method public isTouched()Z
    .locals 1

    .prologue
    .line 770
    iget-boolean v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mTouched:Z

    return v0
.end method

.method protected abstract limitScroll(F)F
.end method

.method protected abstract onDrawFrame(Ljavax/microedition/khronos/opengles/GL10;)V
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 721
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mStopped:Z

    .line 722
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 726
    iput-boolean v2, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mStopped:Z

    .line 727
    iget-boolean v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mNeedReallign:Z

    if-eqz v0, :cond_0

    .line 728
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->LOG_TAG:Ljava/lang/String;

    const-string v1, "checkReallignView reapplied onResume"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 729
    iget v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mWidth:I

    iget v1, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mHeight:I

    invoke-direct {p0, v0, v1}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->checkReallignView(II)V

    .line 731
    :cond_0
    iput-boolean v2, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mNeedReallign:Z

    .line 732
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAnimationRunnable:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;

    invoke-virtual {v0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;->scrollFinished()Z

    move-result v0

    if-nez v0, :cond_1

    .line 733
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mParent:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAnimationRunnable:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->postOnAnimation(Ljava/lang/Runnable;)V

    .line 735
    :cond_1
    return-void
.end method

.method protected onSingleTap(FF)Z
    .locals 1
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 323
    const/4 v0, 0x0

    return v0
.end method

.method protected abstract onSurfaceCreated(Ljavax/microedition/khronos/opengles/GL10;Ljavax/microedition/khronos/egl/EGLConfig;)V
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v0, 0x0

    .line 744
    iget-boolean v1, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAligned:Z

    if-nez v1, :cond_0

    .line 759
    :goto_0
    return v0

    .line 747
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 759
    :goto_1
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0

    .line 749
    :pswitch_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mTouched:Z

    goto :goto_1

    .line 753
    :pswitch_2
    iput-boolean v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mTouched:Z

    .line 754
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAnimationRunnable:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;

    invoke-virtual {v0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;->OnUp()V

    goto :goto_1

    .line 747
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected performanceTest()V
    .locals 2

    .prologue
    .line 784
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mParent:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    new-instance v1, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$1;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$1;-><init>(Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;)V

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->post(Ljava/lang/Runnable;)Z

    .line 798
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$2;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$2;-><init>(Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 814
    return-void
.end method

.method protected abstract positionSelectedView(F)Z
.end method

.method protected abstract positionView(IIIIF)V
.end method

.method public reFlashSelection()V
    .locals 2

    .prologue
    .line 454
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->getSelection()I

    move-result v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->setSelection(IZ)V

    .line 455
    return-void
.end method

.method protected abstract realignView()V
.end method

.method protected relIndex(I)I
    .locals 1
    .param p1, "di"    # I

    .prologue
    .line 643
    iget-boolean v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mUsedModelsCountInited:Z

    if-nez v0, :cond_0

    .line 645
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->getUsedModelsCount()I

    .line 647
    :cond_0
    iget v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mSelectedViewIndex:I

    invoke-virtual {p0, v0, p1}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->relIndex(II)I

    move-result v0

    return v0
.end method

.method protected relIndex(II)I
    .locals 2
    .param p1, "idx"    # I
    .param p2, "di"    # I

    .prologue
    .line 639
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->getUsedModelsCount()I

    move-result v0

    add-int/2addr v0, p1

    add-int/2addr v0, p2

    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->getUsedModelsCount()I

    move-result v1

    rem-int/2addr v0, v1

    return v0
.end method

.method public setAdapter(Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;)V
    .locals 3
    .param p1, "adapter"    # Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;

    .prologue
    .line 144
    if-eqz p1, :cond_2

    .line 145
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setAdapter: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "adapter.count: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p1}, Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    :goto_0
    iput-object p1, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAdapter:Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;

    .line 151
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAdapter:Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;

    if-eqz v0, :cond_1

    .line 153
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->getSelection()I

    move-result v0

    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAdapter:Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;

    invoke-interface {v1}, Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;->getCount()I

    move-result v1

    if-lt v0, v1, :cond_0

    .line 154
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAdapter:Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;

    invoke-interface {v0}, Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->setSelectedAdapterIndex(I)V

    .line 156
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->stopAnimation()V

    .line 157
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAligned:Z

    .line 158
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAdapter:Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;

    invoke-interface {v0}, Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    .line 159
    iget v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mWidth:I

    iget v1, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mHeight:I

    invoke-direct {p0, v0, v1}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->checkReallignView(II)V

    .line 163
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->LOG_TAG:Ljava/lang/String;

    const-string v1, "setAdapter end"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 165
    return-void

    .line 147
    :cond_2
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->LOG_TAG:Ljava/lang/String;

    const-string v1, "setAdapter: null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setAdapterWrap(Z)V
    .locals 0
    .param p1, "wrap"    # Z

    .prologue
    .line 517
    iput-boolean p1, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mWrapAdapter:Z

    .line 518
    return-void
.end method

.method protected abstract setModelVisible(IZ)V
.end method

.method protected setScrollingFriction(F)V
    .locals 1
    .param p1, "friction"    # F

    .prologue
    .line 422
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAnimationRunnable:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;

    if-eqz v0, :cond_0

    .line 423
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAnimationRunnable:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;->setFriction(F)V

    .line 425
    :cond_0
    return-void
.end method

.method protected setSelectedAdapterIndex(I)V
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 173
    iput p1, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mSelectedAdapterIndex:I

    .line 174
    iget-boolean v1, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mWrapAdapter:Z

    if-eqz v1, :cond_2

    .line 175
    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAdapter:Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAdapter:Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;

    invoke-interface {v1}, Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;->getCount()I

    move-result v1

    if-lez v1, :cond_0

    .line 176
    iget v1, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mSelectedAdapterIndex:I

    iget-object v2, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAdapter:Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;

    invoke-interface {v2}, Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;->getCount()I

    move-result v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAdapter:Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;

    invoke-interface {v2}, Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;->getCount()I

    move-result v2

    rem-int/2addr v1, v2

    iput v1, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mSelectedAdapterIndex:I

    .line 186
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->getUsedModelsCount()I

    move-result v0

    .line 187
    .local v0, "usedModels":I
    if-eqz v0, :cond_1

    .line 188
    iget v1, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mSelectedAdapterIndex:I

    rem-int/2addr v1, v0

    iput v1, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mSelectedViewIndex:I

    .line 190
    :cond_1
    return-void

    .line 180
    .end local v0    # "usedModels":I
    :cond_2
    iget v1, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mSelectedAdapterIndex:I

    if-gez v1, :cond_3

    .line 181
    const/4 v1, 0x0

    iput v1, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mSelectedAdapterIndex:I

    goto :goto_0

    .line 182
    :cond_3
    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAdapter:Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mSelectedAdapterIndex:I

    iget-object v2, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAdapter:Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;

    invoke-interface {v2}, Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;->getCount()I

    move-result v2

    if-lt v1, v2, :cond_0

    .line 183
    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAdapter:Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;

    invoke-interface {v1}, Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;->getCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mSelectedAdapterIndex:I

    goto :goto_0
.end method

.method public setSelection(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 450
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->setSelection(IZ)V

    .line 451
    return-void
.end method

.method protected startPositioning(F)V
    .locals 0
    .param p1, "dx"    # F

    .prologue
    .line 333
    return-void
.end method

.method public stopAnimation()V
    .locals 1

    .prologue
    .line 1193
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAnimationRunnable:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;

    invoke-virtual {v0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;->stopAnimation()V

    .line 1194
    return-void
.end method

.method protected abstract updateItemData(II)V
.end method

.method protected declared-synchronized updateSelected(I)I
    .locals 5
    .param p1, "di"    # I

    .prologue
    .line 665
    monitor-enter p0

    :try_start_0
    iget-object v3, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAdapter:Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;

    if-nez v3, :cond_0

    .line 667
    iget-object v3, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->LOG_TAG:Ljava/lang/String;

    const-string v4, "updateSelected skipped"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 669
    const/4 v3, 0x0

    .line 697
    :goto_0
    monitor-exit p0

    return v3

    .line 671
    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->getSelection()I

    move-result v3

    add-int v1, v3, p1

    .line 676
    .local v1, "newPosition":I
    iget-boolean v3, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mWrapAdapter:Z

    if-nez v3, :cond_1

    .line 677
    if-gez v1, :cond_3

    .line 678
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->getSelection()I

    move-result p1

    .line 684
    :cond_1
    :goto_1
    if-eqz p1, :cond_4

    .line 686
    const/4 v2, 0x1

    .line 687
    .local v2, "sign":I
    if-gez p1, :cond_2

    .line 688
    const/4 v2, -0x1

    .line 690
    :cond_2
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_2
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v3

    if-ge v0, v3, :cond_5

    .line 691
    invoke-direct {p0, v2}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->updateSelectedAdapterDx(I)V

    .line 692
    invoke-direct {p0, v2}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->updateViewData(I)V

    .line 690
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 679
    .end local v0    # "i":I
    .end local v2    # "sign":I
    :cond_3
    iget-object v3, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAdapter:Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;

    invoke-interface {v3}, Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;->getCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    if-le v1, v3, :cond_1

    .line 680
    iget-object v3, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAdapter:Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;

    invoke-interface {v3}, Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;->getCount()I

    move-result v3

    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->getSelection()I

    move-result v4

    sub-int/2addr v3, v4

    add-int/lit8 p1, v3, -0x1

    goto :goto_1

    .line 695
    :cond_4
    iget-object v3, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->LOG_TAG:Ljava/lang/String;

    const-string v4, "updateSelected di == 0"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_5
    move v3, p1

    .line 697
    goto :goto_0

    .line 665
    .end local v1    # "newPosition":I
    :catchall_0
    move-exception v3

    monitor-exit p0

    throw v3
.end method

.method protected declared-synchronized updateViewData()V
    .locals 8

    .prologue
    .line 482
    monitor-enter p0

    :try_start_0
    iget-object v5, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAdapter:Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAdapter:Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;

    invoke-interface {v5}, Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;->getCount()I

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->getUsedModelsCount()I

    move-result v5

    if-nez v5, :cond_2

    .line 484
    :cond_0
    iget-object v5, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->LOG_TAG:Ljava/lang/String;

    const-string v6, "updateViewData skipped"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 513
    :cond_1
    monitor-exit p0

    return-void

    .line 488
    :cond_2
    :try_start_1
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->getSelectedModelPosition()I

    move-result v5

    neg-int v2, v5

    .line 489
    .local v2, "relIdx":I
    invoke-virtual {p0, v2}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->relIndex(I)I

    move-result v3

    .line 490
    .local v3, "viewIdx":I
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->getSelection()I

    move-result v5

    add-int v0, v5, v2

    .line 491
    .local v0, "adapterIdx":I
    iget-boolean v5, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mWrapAdapter:Z

    if-eqz v5, :cond_3

    .line 492
    iget-object v5, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAdapter:Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;

    invoke-interface {v5}, Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;->getCount()I

    move-result v5

    add-int/2addr v5, v0

    iget-object v6, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAdapter:Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;

    invoke-interface {v6}, Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;->getCount()I

    move-result v6

    rem-int v0, v5, v6

    .line 495
    :cond_3
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->getUsedModelsCount()I

    move-result v5

    if-ge v1, v5, :cond_1

    .line 496
    invoke-direct {p0, v0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->isVisiblePosition(I)Z

    move-result v4

    .line 497
    .local v4, "visible":Z
    if-eqz v4, :cond_4

    .line 498
    invoke-direct {p0, v3, v0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->doUpdateItemData(II)V

    .line 500
    :cond_4
    if-ltz v3, :cond_6

    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->getUsedModelsCount()I

    move-result v5

    if-ge v3, v5, :cond_6

    .line 501
    invoke-virtual {p0, v3, v4}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->setModelVisible(IZ)V

    .line 507
    :goto_1
    const/4 v5, 0x1

    invoke-virtual {p0, v3, v5}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->relIndex(II)I

    move-result v3

    .line 508
    add-int/lit8 v0, v0, 0x1

    .line 509
    iget-boolean v5, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mWrapAdapter:Z

    if-eqz v5, :cond_5

    .line 510
    iget-object v5, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAdapter:Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;

    invoke-interface {v5}, Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;->getCount()I

    move-result v5

    add-int/2addr v5, v0

    iget-object v6, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAdapter:Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;

    invoke-interface {v6}, Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;->getCount()I

    move-result v6

    rem-int v0, v5, v6

    .line 495
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 503
    :cond_6
    iget-object v5, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->LOG_TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "wrong index in setModelVisible viewIdx: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " getUsedModelsCount: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->getUsedModelsCount()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " getSelectedModelPosition: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->getSelectedModelPosition()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/musicplus/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 482
    .end local v0    # "adapterIdx":I
    .end local v1    # "i":I
    .end local v2    # "relIdx":I
    .end local v3    # "viewIdx":I
    .end local v4    # "visible":Z
    :catchall_0
    move-exception v5

    monitor-exit p0

    throw v5
.end method
