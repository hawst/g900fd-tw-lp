.class public interface abstract Lcom/samsung/musicplus/glwidget/layout/GalleryLayout;
.super Ljava/lang/Object;
.source "GalleryLayout.java"

# interfaces
.implements Lcom/samsung/musicplus/glwidget/layout/IAlbumArtInfo;


# virtual methods
.method public abstract animationFinished()Z
.end method

.method public abstract getAlbumArtForCoord([F[F)Landroid/graphics/Bitmap;
.end method

.method public abstract getMaxSelectedDx()F
.end method

.method public abstract getRenderer()Landroid/opengl/GLSurfaceView$Renderer;
.end method

.method public abstract getSelectedDX()F
.end method

.method public abstract getSelection()I
.end method

.method public abstract isFlinging()Z
.end method

.method public abstract isTouched()Z
.end method

.method public abstract onPause()V
.end method

.method public abstract onResume()V
.end method

.method public abstract onTouchEvent(Landroid/view/MotionEvent;)Z
.end method

.method public abstract reFlashSelection()V
.end method

.method public abstract setAdapter(Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;)V
.end method

.method public abstract setAdapterWrap(Z)V
.end method

.method public abstract setSelection(I)V
.end method

.method public abstract stopAnimation()V
.end method
