.class Lcom/samsung/musicplus/glwidget/GLTextureViewBase$LogWriter;
.super Ljava/io/Writer;
.source "GLTextureViewBase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/glwidget/GLTextureViewBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "LogWriter"
.end annotation


# instance fields
.field private mBuilder:Ljava/lang/StringBuilder;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 505
    invoke-direct {p0}, Ljava/io/Writer;-><init>()V

    .line 536
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase$LogWriter;->mBuilder:Ljava/lang/StringBuilder;

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/musicplus/glwidget/GLTextureViewBase$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/musicplus/glwidget/GLTextureViewBase$1;

    .prologue
    .line 505
    invoke-direct {p0}, Lcom/samsung/musicplus/glwidget/GLTextureViewBase$LogWriter;-><init>()V

    return-void
.end method

.method private flushBuilder()V
    .locals 3

    .prologue
    .line 530
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase$LogWriter;->mBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-lez v0, :cond_0

    .line 531
    # getter for: Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->access$100()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase$LogWriter;->mBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 532
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase$LogWriter;->mBuilder:Ljava/lang/StringBuilder;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase$LogWriter;->mBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 534
    :cond_0
    return-void
.end method


# virtual methods
.method public close()V
    .locals 0

    .prologue
    .line 509
    invoke-direct {p0}, Lcom/samsung/musicplus/glwidget/GLTextureViewBase$LogWriter;->flushBuilder()V

    .line 510
    return-void
.end method

.method public flush()V
    .locals 0

    .prologue
    .line 514
    invoke-direct {p0}, Lcom/samsung/musicplus/glwidget/GLTextureViewBase$LogWriter;->flushBuilder()V

    .line 515
    return-void
.end method

.method public write([CII)V
    .locals 3
    .param p1, "buf"    # [C
    .param p2, "offset"    # I
    .param p3, "count"    # I

    .prologue
    .line 519
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, p3, :cond_1

    .line 520
    add-int v2, p2, v1

    aget-char v0, p1, v2

    .line 521
    .local v0, "c":C
    const/16 v2, 0xa

    if-ne v0, v2, :cond_0

    .line 522
    invoke-direct {p0}, Lcom/samsung/musicplus/glwidget/GLTextureViewBase$LogWriter;->flushBuilder()V

    .line 519
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 524
    :cond_0
    iget-object v2, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase$LogWriter;->mBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 527
    .end local v0    # "c":C
    :cond_1
    return-void
.end method
