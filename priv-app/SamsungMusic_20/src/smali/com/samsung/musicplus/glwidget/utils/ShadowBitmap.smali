.class public Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;
.super Ljava/lang/Object;
.source "ShadowBitmap.java"


# static fields
.field private static final LOG_TAG:Ljava/lang/String;

.field private static mRandom:Ljava/util/Random;


# instance fields
.field private mBitmap:Landroid/graphics/Bitmap;

.field private mCenterColor:I

.field private mRect:Landroid/graphics/Rect;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    const-class v0, Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;->LOG_TAG:Ljava/lang/String;

    .line 24
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    sput-object v0, Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;->mRandom:Ljava/util/Random;

    return-void
.end method

.method public constructor <init>(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;->mRect:Landroid/graphics/Rect;

    .line 29
    iput-object p1, p0, Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;->mBitmap:Landroid/graphics/Bitmap;

    .line 30
    const/16 v0, 0x64

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;->detectBounds(I)V

    .line 31
    return-void
.end method

.method private detectBounds(I)V
    .locals 16
    .param p1, "tries"    # I

    .prologue
    .line 34
    move/from16 v0, p1

    new-array v9, v0, [Landroid/graphics/Point;

    .line 35
    .local v9, "p":[Landroid/graphics/Point;
    move/from16 v0, p1

    new-array v1, v0, [I

    .line 36
    .local v1, "c":[I
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    move/from16 v0, p1

    if-ge v6, v0, :cond_0

    .line 37
    new-instance v13, Landroid/graphics/Point;

    invoke-direct {v13}, Landroid/graphics/Point;-><init>()V

    aput-object v13, v9, v6

    .line 38
    aget-object v13, v9, v6

    sget-object v14, Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;->mRandom:Ljava/util/Random;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v15}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v15

    invoke-virtual {v14, v15}, Ljava/util/Random;->nextInt(I)I

    move-result v14

    iput v14, v13, Landroid/graphics/Point;->x:I

    .line 39
    aget-object v13, v9, v6

    sget-object v14, Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;->mRandom:Ljava/util/Random;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v15}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v15

    invoke-virtual {v14, v15}, Ljava/util/Random;->nextInt(I)I

    move-result v14

    iput v14, v13, Landroid/graphics/Point;->y:I

    .line 40
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;->mBitmap:Landroid/graphics/Bitmap;

    aget-object v14, v9, v6

    iget v14, v14, Landroid/graphics/Point;->x:I

    aget-object v15, v9, v6

    iget v15, v15, Landroid/graphics/Point;->y:I

    invoke-virtual {v13, v14, v15}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v13

    aput v13, v1, v6

    .line 36
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 42
    :cond_0
    new-instance v5, Landroid/util/SparseIntArray;

    invoke-direct {v5}, Landroid/util/SparseIntArray;-><init>()V

    .line 43
    .local v5, "counter":Landroid/util/SparseIntArray;
    const/4 v7, 0x0

    .line 44
    .local v7, "maxCount":I
    const/4 v8, -0x1

    .line 45
    .local v8, "maxIndex":I
    const/4 v6, 0x0

    :goto_1
    move/from16 v0, p1

    if-ge v6, v0, :cond_2

    .line 46
    aget v3, v1, v6

    .line 47
    .local v3, "color":I
    invoke-virtual {v5, v3}, Landroid/util/SparseIntArray;->get(I)I

    move-result v13

    add-int/lit8 v4, v13, 0x1

    .line 48
    .local v4, "count":I
    invoke-virtual {v5, v3, v4}, Landroid/util/SparseIntArray;->put(II)V

    .line 49
    if-ge v7, v4, :cond_1

    .line 50
    move v7, v4

    .line 51
    move v8, v6

    .line 45
    :cond_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 55
    .end local v3    # "color":I
    .end local v4    # "count":I
    :cond_2
    aget-object v13, v9, v8

    iget v11, v13, Landroid/graphics/Point;->x:I

    .line 56
    .local v11, "x":I
    aget-object v13, v9, v8

    iget v12, v13, Landroid/graphics/Point;->y:I

    .line 58
    .local v12, "y":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v13, v11, v12}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v13

    move-object/from16 v0, p0

    iput v13, v0, Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;->mCenterColor:I

    .line 61
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;->mRect:Landroid/graphics/Rect;

    iput v11, v13, Landroid/graphics/Rect;->left:I

    .line 62
    :goto_2
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;->mRect:Landroid/graphics/Rect;

    iget v13, v13, Landroid/graphics/Rect;->left:I

    add-int/lit8 v13, v13, -0x1

    if-lez v13, :cond_3

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;->mBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;->mRect:Landroid/graphics/Rect;

    iget v14, v14, Landroid/graphics/Rect;->left:I

    add-int/lit8 v14, v14, -0x1

    invoke-virtual {v13, v14, v12}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v13

    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;->mCenterColor:I

    if-ne v13, v14, :cond_3

    .line 63
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;->mRect:Landroid/graphics/Rect;

    iget v14, v13, Landroid/graphics/Rect;->left:I

    add-int/lit8 v14, v14, -0x1

    iput v14, v13, Landroid/graphics/Rect;->left:I

    goto :goto_2

    .line 67
    :cond_3
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;->mRect:Landroid/graphics/Rect;

    iput v11, v13, Landroid/graphics/Rect;->right:I

    .line 68
    :goto_3
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;->mRect:Landroid/graphics/Rect;

    iget v13, v13, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v14}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v14

    if-ge v13, v14, :cond_4

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;->mBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;->mRect:Landroid/graphics/Rect;

    iget v14, v14, Landroid/graphics/Rect;->right:I

    invoke-virtual {v13, v14, v12}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v13

    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;->mCenterColor:I

    if-ne v13, v14, :cond_4

    .line 69
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;->mRect:Landroid/graphics/Rect;

    iget v14, v13, Landroid/graphics/Rect;->right:I

    add-int/lit8 v14, v14, 0x1

    iput v14, v13, Landroid/graphics/Rect;->right:I

    goto :goto_3

    .line 73
    :cond_4
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;->mRect:Landroid/graphics/Rect;

    iput v12, v13, Landroid/graphics/Rect;->top:I

    .line 74
    :goto_4
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;->mRect:Landroid/graphics/Rect;

    iget v13, v13, Landroid/graphics/Rect;->top:I

    add-int/lit8 v13, v13, -0x1

    if-lez v13, :cond_5

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;->mBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;->mRect:Landroid/graphics/Rect;

    iget v14, v14, Landroid/graphics/Rect;->top:I

    add-int/lit8 v14, v14, -0x1

    invoke-virtual {v13, v11, v14}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v13

    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;->mCenterColor:I

    if-ne v13, v14, :cond_5

    .line 75
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;->mRect:Landroid/graphics/Rect;

    iget v14, v13, Landroid/graphics/Rect;->top:I

    add-int/lit8 v14, v14, -0x1

    iput v14, v13, Landroid/graphics/Rect;->top:I

    goto :goto_4

    .line 79
    :cond_5
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;->mRect:Landroid/graphics/Rect;

    iput v12, v13, Landroid/graphics/Rect;->bottom:I

    .line 80
    :goto_5
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;->mRect:Landroid/graphics/Rect;

    iget v13, v13, Landroid/graphics/Rect;->bottom:I

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v14}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v14

    if-ge v13, v14, :cond_6

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;->mBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;->mRect:Landroid/graphics/Rect;

    iget v14, v14, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v13, v11, v14}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v13

    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;->mCenterColor:I

    if-ne v13, v14, :cond_6

    .line 81
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;->mRect:Landroid/graphics/Rect;

    iget v14, v13, Landroid/graphics/Rect;->bottom:I

    add-int/lit8 v14, v14, 0x1

    iput v14, v13, Landroid/graphics/Rect;->bottom:I

    goto :goto_5

    .line 84
    :cond_6
    sget-object v13, Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;->LOG_TAG:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Rect: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;->mRect:Landroid/graphics/Rect;

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    sget-object v13, Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;->LOG_TAG:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, "Bitmap width: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v15}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " height: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v15}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;->mBitmap:Landroid/graphics/Bitmap;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v13}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v13

    if-nez v13, :cond_7

    sget-object v13, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    :goto_6
    const/4 v15, 0x1

    invoke-virtual {v14, v13, v15}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object v13

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;->mBitmap:Landroid/graphics/Bitmap;

    .line 91
    new-instance v2, Landroid/graphics/Canvas;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;->mBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v2, v13}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 92
    .local v2, "canvas":Landroid/graphics/Canvas;
    new-instance v10, Landroid/graphics/Paint;

    invoke-direct {v10}, Landroid/graphics/Paint;-><init>()V

    .line 93
    .local v10, "paint":Landroid/graphics/Paint;
    const/4 v13, 0x0

    invoke-virtual {v10, v13}, Landroid/graphics/Paint;->setColor(I)V

    .line 94
    new-instance v13, Landroid/graphics/PorterDuffXfermode;

    sget-object v14, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v13, v14}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v10, v13}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 95
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;->mRect:Landroid/graphics/Rect;

    invoke-virtual {v2, v13, v10}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 96
    return-void

    .line 90
    .end local v2    # "canvas":Landroid/graphics/Canvas;
    .end local v10    # "paint":Landroid/graphics/Paint;
    :cond_7
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v13}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v13

    goto :goto_6
.end method


# virtual methods
.method public getBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;->mBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getBounds()Landroid/graphics/Rect;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;->mRect:Landroid/graphics/Rect;

    return-object v0
.end method
