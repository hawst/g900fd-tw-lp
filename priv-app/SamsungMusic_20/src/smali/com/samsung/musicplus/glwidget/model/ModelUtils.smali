.class public Lcom/samsung/musicplus/glwidget/model/ModelUtils;
.super Ljava/lang/Object;
.source "ModelUtils.java"


# static fields
.field public static final Z_FIGHT_CONST:F = 20.0f


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static cancelAllAnimations([Lcom/samsung/musicplus/glwidget/model/AnimationModel;)V
    .locals 2
    .param p0, "models"    # [Lcom/samsung/musicplus/glwidget/model/AnimationModel;

    .prologue
    .line 121
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p0

    if-ge v0, v1, :cond_0

    .line 122
    aget-object v1, p0, v0

    invoke-virtual {v1}, Lcom/samsung/musicplus/glwidget/model/AnimationModel;->cancelAnimation()V

    .line 121
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 124
    :cond_0
    return-void
.end method

.method public static getDebugState([Lcom/samsung/musicplus/glwidget/model/Model;)Ljava/lang/String;
    .locals 3
    .param p0, "models"    # [Lcom/samsung/musicplus/glwidget/model/Model;

    .prologue
    .line 149
    if-nez p0, :cond_0

    .line 150
    const-string v2, ""

    .line 162
    :goto_0
    return-object v2

    .line 152
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 153
    .local v1, "str":Ljava/lang/StringBuilder;
    const-string v2, "Count: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 154
    array-length v2, p0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 155
    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 156
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    array-length v2, p0

    if-ge v0, v2, :cond_1

    .line 157
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 158
    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 159
    aget-object v2, p0, v0

    invoke-virtual {v2}, Lcom/samsung/musicplus/glwidget/model/Model;->getDebugState()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 160
    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 156
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 162
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method

.method public static hasStartedAnimation([Lcom/samsung/musicplus/glwidget/model/AnimationModel;)Z
    .locals 3
    .param p0, "models"    # [Lcom/samsung/musicplus/glwidget/model/AnimationModel;

    .prologue
    const/4 v1, 0x0

    .line 132
    if-nez p0, :cond_1

    .line 140
    :cond_0
    :goto_0
    return v1

    .line 135
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    array-length v2, p0

    if-ge v0, v2, :cond_0

    .line 136
    aget-object v2, p0, v0

    invoke-virtual {v2}, Lcom/samsung/musicplus/glwidget/model/AnimationModel;->isAnimationStarted()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 137
    const/4 v1, 0x1

    goto :goto_0

    .line 135
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public static resetModified([Lcom/samsung/musicplus/glwidget/model/Model;)V
    .locals 2
    .param p0, "models"    # [Lcom/samsung/musicplus/glwidget/model/Model;

    .prologue
    .line 108
    if-nez p0, :cond_1

    .line 114
    :cond_0
    return-void

    .line 111
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p0

    if-ge v0, v1, :cond_0

    .line 112
    aget-object v1, p0, v0

    invoke-virtual {v1}, Lcom/samsung/musicplus/glwidget/model/Model;->resetModified()V

    .line 111
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static updateMarkerMatrix(FLcom/samsung/musicplus/glwidget/model/Model;Lcom/samsung/musicplus/glwidget/model/BitmapModel;[FI)V
    .locals 17
    .param p0, "originalModelHeight"    # F
    .param p1, "model"    # Lcom/samsung/musicplus/glwidget/model/Model;
    .param p2, "marker"    # Lcom/samsung/musicplus/glwidget/model/BitmapModel;
    .param p3, "matrix"    # [F
    .param p4, "offset"    # I

    .prologue
    .line 79
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/musicplus/glwidget/model/BitmapModel;->getHeight()F

    move-result v2

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/musicplus/glwidget/model/Model;->getHeight()F

    move-result v3

    mul-float/2addr v2, v3

    div-float v10, v2, p0

    .line 80
    .local v10, "height":F
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/musicplus/glwidget/model/BitmapModel;->getWidth()F

    move-result v2

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/musicplus/glwidget/model/Model;->getWidth()F

    move-result v3

    mul-float/2addr v2, v3

    div-float v13, v2, p0

    .line 81
    .local v13, "width":F
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/musicplus/glwidget/model/BitmapModel;->getWidth()F

    move-result v2

    div-float v11, v13, v2

    .line 82
    .local v11, "kDx":F
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/musicplus/glwidget/model/BitmapModel;->getHeight()F

    move-result v2

    div-float v12, v10, v2

    .line 83
    .local v12, "kDy":F
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/musicplus/glwidget/model/Model;->getZ()F

    move-result v16

    .line 85
    .local v16, "z":F
    const/high16 v2, 0x40000000    # 2.0f

    div-float v8, v10, v2

    .line 86
    .local v8, "halfHeight":F
    const/high16 v2, 0x40000000    # 2.0f

    div-float v9, v13, v2

    .line 87
    .local v9, "halfWidth":F
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/musicplus/glwidget/model/Model;->getAngle()F

    move-result v4

    .line 88
    .local v4, "angle":F
    const/4 v2, 0x0

    cmpl-float v2, v4, v2

    if-eqz v2, :cond_0

    .line 89
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/musicplus/glwidget/model/BitmapModel;->getX()F

    move-result v2

    mul-float v14, v11, v2

    .line 90
    .local v14, "x":F
    invoke-virtual/range {p2 .. p2}, Lcom/samsung/musicplus/glwidget/model/BitmapModel;->getY()F

    move-result v2

    mul-float v15, v12, v2

    .line 91
    .local v15, "y":F
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/musicplus/glwidget/model/Model;->getX()F

    move-result v2

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/musicplus/glwidget/model/Model;->getY()F

    move-result v3

    const/high16 v5, 0x41a00000    # 20.0f

    add-float v5, v5, v16

    move-object/from16 v0, p3

    move/from16 v1, p4

    invoke-static {v0, v1, v2, v3, v5}, Landroid/opengl/Matrix;->translateM([FIFFF)V

    .line 92
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/musicplus/glwidget/model/Model;->getRotX()F

    move-result v5

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/musicplus/glwidget/model/Model;->getRotY()F

    move-result v6

    invoke-virtual/range {p1 .. p1}, Lcom/samsung/musicplus/glwidget/model/Model;->getRotZ()F

    move-result v7

    move-object/from16 v2, p3

    move/from16 v3, p4

    invoke-static/range {v2 .. v7}, Landroid/opengl/Matrix;->rotateM([FIFFFF)V

    .line 93
    sub-float v2, v14, v9

    neg-float v3, v15

    sub-float/2addr v3, v8

    const/4 v5, 0x0

    move-object/from16 v0, p3

    move/from16 v1, p4

    invoke-static {v0, v1, v2, v3, v5}, Landroid/opengl/Matrix;->translateM([FIFFF)V

    .line 99
    :goto_0
    const/4 v2, 0x0

    move-object/from16 v0, p3

    move/from16 v1, p4

    invoke-static {v0, v1, v13, v10, v2}, Landroid/opengl/Matrix;->scaleM([FIFFF)V

    .line 100
    return-void

    .line 95
    .end local v14    # "x":F
    .end local v15    # "y":F
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/musicplus/glwidget/model/Model;->getX()F

    move-result v2

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/musicplus/glwidget/model/BitmapModel;->getX()F

    move-result v3

    mul-float/2addr v3, v11

    add-float v14, v2, v3

    .line 96
    .restart local v14    # "x":F
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/musicplus/glwidget/model/Model;->getY()F

    move-result v2

    invoke-virtual/range {p2 .. p2}, Lcom/samsung/musicplus/glwidget/model/BitmapModel;->getY()F

    move-result v3

    mul-float/2addr v3, v12

    sub-float v15, v2, v3

    .line 97
    .restart local v15    # "y":F
    sub-float v2, v14, v9

    sub-float v3, v15, v8

    const/high16 v5, 0x41a00000    # 20.0f

    add-float v5, v5, v16

    move-object/from16 v0, p3

    move/from16 v1, p4

    invoke-static {v0, v1, v2, v3, v5}, Landroid/opengl/Matrix;->translateM([FIFFF)V

    goto :goto_0
.end method

.method public static updateModelMatrix(Lcom/samsung/musicplus/glwidget/model/Model;[FI)V
    .locals 10
    .param p0, "m"    # Lcom/samsung/musicplus/glwidget/model/Model;
    .param p1, "matrix"    # [F
    .param p2, "offset"    # I

    .prologue
    const/high16 v1, 0x40000000    # 2.0f

    const/4 v9, 0x0

    .line 28
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/model/Model;->getHeight()F

    move-result v0

    div-float v7, v0, v1

    .line 29
    .local v7, "halfHeight":F
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/model/Model;->getWidth()F

    move-result v0

    div-float v8, v0, v1

    .line 30
    .local v8, "halfWidth":F
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/model/Model;->getAngle()F

    move-result v6

    .line 31
    .local v6, "angle":F
    cmpl-float v0, v6, v9

    if-eqz v0, :cond_0

    .line 32
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/model/Model;->getX()F

    move-result v0

    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/model/Model;->getY()F

    move-result v1

    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/model/Model;->getZ()F

    move-result v2

    invoke-static {p1, p2, v0, v1, v2}, Landroid/opengl/Matrix;->translateM([FIFFF)V

    .line 33
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/model/Model;->getAngle()F

    move-result v2

    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/model/Model;->getRotX()F

    move-result v3

    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/model/Model;->getRotY()F

    move-result v4

    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/model/Model;->getRotZ()F

    move-result v5

    move-object v0, p1

    move v1, p2

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->rotateM([FIFFFF)V

    .line 34
    neg-float v0, v8

    neg-float v1, v7

    invoke-static {p1, p2, v0, v1, v9}, Landroid/opengl/Matrix;->translateM([FIFFF)V

    .line 39
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/model/Model;->getWidth()F

    move-result v0

    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/model/Model;->getHeight()F

    move-result v1

    invoke-static {p1, p2, v0, v1, v9}, Landroid/opengl/Matrix;->scaleM([FIFFF)V

    .line 40
    return-void

    .line 36
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/model/Model;->getX()F

    move-result v0

    sub-float/2addr v0, v8

    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/model/Model;->getY()F

    move-result v1

    sub-float/2addr v1, v7

    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/model/Model;->getZ()F

    move-result v2

    invoke-static {p1, p2, v0, v1, v2}, Landroid/opengl/Matrix;->translateM([FIFFF)V

    goto :goto_0
.end method

.method public static updateShadowMatrix(Lcom/samsung/musicplus/glwidget/model/Model;Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;[FI)V
    .locals 22
    .param p0, "m"    # Lcom/samsung/musicplus/glwidget/model/Model;
    .param p1, "s"    # Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;
    .param p2, "matrix"    # [F
    .param p3, "offset"    # I

    .prologue
    .line 51
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v17

    .line 52
    .local v17, "sb":Landroid/graphics/Bitmap;
    invoke-virtual/range {p1 .. p1}, Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;->getBounds()Landroid/graphics/Rect;

    move-result-object v16

    .line 54
    .local v16, "r":Landroid/graphics/Rect;
    invoke-virtual/range {v17 .. v17}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/glwidget/model/Model;->getWidth()F

    move-result v5

    mul-float/2addr v4, v5

    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Rect;->width()I

    move-result v5

    int-to-float v5, v5

    div-float v18, v4, v5

    .line 55
    .local v18, "width":F
    invoke-virtual/range {v17 .. v17}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    int-to-float v4, v4

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/glwidget/model/Model;->getHeight()F

    move-result v5

    mul-float/2addr v4, v5

    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Rect;->height()I

    move-result v5

    int-to-float v5, v5

    div-float v13, v4, v5

    .line 56
    .local v13, "height":F
    invoke-virtual/range {v17 .. v17}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    int-to-float v4, v4

    div-float v14, v13, v4

    .line 57
    .local v14, "kHeight":F
    invoke-virtual/range {v17 .. v17}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    int-to-float v4, v4

    div-float v15, v18, v4

    .line 58
    .local v15, "kWidth":F
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/glwidget/model/Model;->getX()F

    move-result v4

    invoke-virtual/range {v17 .. v17}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    int-to-float v5, v5

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v5, v6

    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Rect;->exactCenterX()F

    move-result v6

    sub-float/2addr v5, v6

    mul-float/2addr v5, v15

    add-float v19, v4, v5

    .line 59
    .local v19, "x":F
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/glwidget/model/Model;->getY()F

    move-result v4

    invoke-virtual/range {v16 .. v16}, Landroid/graphics/Rect;->exactCenterY()F

    move-result v5

    invoke-virtual/range {v17 .. v17}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    int-to-float v6, v6

    const/high16 v7, 0x40000000    # 2.0f

    div-float/2addr v6, v7

    sub-float/2addr v5, v6

    mul-float/2addr v5, v14

    add-float v20, v4, v5

    .line 60
    .local v20, "y":F
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/glwidget/model/Model;->getZ()F

    move-result v21

    .line 61
    .local v21, "z":F
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/glwidget/model/Model;->getAngle()F

    move-result v10

    .line 63
    .local v10, "angle":F
    const/high16 v4, 0x40000000    # 2.0f

    div-float v11, v13, v4

    .line 64
    .local v11, "halfHeight":F
    const/high16 v4, 0x40000000    # 2.0f

    div-float v12, v18, v4

    .line 66
    .local v12, "halfWidth":F
    const/4 v4, 0x0

    cmpl-float v4, v10, v4

    if-eqz v4, :cond_0

    .line 68
    const/high16 v4, 0x41a00000    # 20.0f

    sub-float v4, v21, v4

    move-object/from16 v0, p2

    move/from16 v1, p3

    move/from16 v2, v19

    move/from16 v3, v20

    invoke-static {v0, v1, v2, v3, v4}, Landroid/opengl/Matrix;->translateM([FIFFF)V

    .line 69
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/glwidget/model/Model;->getAngle()F

    move-result v6

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/glwidget/model/Model;->getRotX()F

    move-result v7

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/glwidget/model/Model;->getRotY()F

    move-result v8

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/glwidget/model/Model;->getRotZ()F

    move-result v9

    move-object/from16 v4, p2

    move/from16 v5, p3

    invoke-static/range {v4 .. v9}, Landroid/opengl/Matrix;->rotateM([FIFFFF)V

    .line 70
    neg-float v4, v12

    neg-float v5, v11

    const/4 v6, 0x0

    move-object/from16 v0, p2

    move/from16 v1, p3

    invoke-static {v0, v1, v4, v5, v6}, Landroid/opengl/Matrix;->translateM([FIFFF)V

    .line 74
    :goto_0
    const/4 v4, 0x0

    move-object/from16 v0, p2

    move/from16 v1, p3

    move/from16 v2, v18

    invoke-static {v0, v1, v2, v13, v4}, Landroid/opengl/Matrix;->scaleM([FIFFF)V

    .line 75
    return-void

    .line 72
    :cond_0
    sub-float v4, v19, v12

    sub-float v5, v20, v11

    const/high16 v6, 0x41a00000    # 20.0f

    sub-float v6, v21, v6

    move-object/from16 v0, p2

    move/from16 v1, p3

    invoke-static {v0, v1, v4, v5, v6}, Landroid/opengl/Matrix;->translateM([FIFFF)V

    goto :goto_0
.end method
