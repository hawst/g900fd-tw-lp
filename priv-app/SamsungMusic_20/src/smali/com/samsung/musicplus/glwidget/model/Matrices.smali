.class public Lcom/samsung/musicplus/glwidget/model/Matrices;
.super Ljava/lang/Object;
.source "Matrices.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/glwidget/model/Matrices$MatrixUpdater;
    }
.end annotation


# instance fields
.field private final mArrayLength:I

.field private final mMatrices:[[F

.field private final mMatrixCount:I

.field private final mMaxTransformationCount:I

.field private final mTransformations:[Lcom/samsung/musicplus/glwidget/model/Matrices$MatrixUpdater;


# direct methods
.method public constructor <init>(II)V
    .locals 1
    .param p1, "maxTransformationCount"    # I
    .param p2, "matrixCount"    # I

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput p1, p0, Lcom/samsung/musicplus/glwidget/model/Matrices;->mMaxTransformationCount:I

    .line 57
    iput p2, p0, Lcom/samsung/musicplus/glwidget/model/Matrices;->mMatrixCount:I

    .line 58
    mul-int/lit8 v0, p2, 0x10

    iput v0, p0, Lcom/samsung/musicplus/glwidget/model/Matrices;->mArrayLength:I

    .line 59
    iget v0, p0, Lcom/samsung/musicplus/glwidget/model/Matrices;->mMaxTransformationCount:I

    new-array v0, v0, [[F

    iput-object v0, p0, Lcom/samsung/musicplus/glwidget/model/Matrices;->mMatrices:[[F

    .line 60
    iget v0, p0, Lcom/samsung/musicplus/glwidget/model/Matrices;->mMaxTransformationCount:I

    new-array v0, v0, [Lcom/samsung/musicplus/glwidget/model/Matrices$MatrixUpdater;

    iput-object v0, p0, Lcom/samsung/musicplus/glwidget/model/Matrices;->mTransformations:[Lcom/samsung/musicplus/glwidget/model/Matrices$MatrixUpdater;

    .line 61
    return-void
.end method

.method private checkTransformationIndex(ILjava/lang/String;)V
    .locals 3
    .param p1, "index"    # I
    .param p2, "message"    # Ljava/lang/String;

    .prologue
    .line 111
    if-ltz p1, :cond_0

    iget v0, p0, Lcom/samsung/musicplus/glwidget/model/Matrices;->mMaxTransformationCount:I

    if-le p1, v0, :cond_1

    .line 112
    :cond_0
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Wrong transformation index: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 114
    :cond_1
    return-void
.end method


# virtual methods
.method public getMatrices(I)[F
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 106
    const-string v0, "getMatrices"

    invoke-direct {p0, p1, v0}, Lcom/samsung/musicplus/glwidget/model/Matrices;->checkTransformationIndex(ILjava/lang/String;)V

    .line 107
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/model/Matrices;->mMatrices:[[F

    aget-object v0, v0, p1

    return-object v0
.end method

.method public setTransformation(ILcom/samsung/musicplus/glwidget/model/Matrices$MatrixUpdater;)V
    .locals 2
    .param p1, "index"    # I
    .param p2, "transformation"    # Lcom/samsung/musicplus/glwidget/model/Matrices$MatrixUpdater;

    .prologue
    .line 72
    const-string v0, "setTransformation"

    invoke-direct {p0, p1, v0}, Lcom/samsung/musicplus/glwidget/model/Matrices;->checkTransformationIndex(ILjava/lang/String;)V

    .line 73
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/model/Matrices;->mTransformations:[Lcom/samsung/musicplus/glwidget/model/Matrices$MatrixUpdater;

    aput-object p2, v0, p1

    .line 74
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/model/Matrices;->mMatrices:[[F

    aget-object v0, v0, p1

    if-nez v0, :cond_0

    .line 75
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/model/Matrices;->mMatrices:[[F

    iget v1, p0, Lcom/samsung/musicplus/glwidget/model/Matrices;->mArrayLength:I

    new-array v1, v1, [F

    aput-object v1, v0, p1

    .line 77
    :cond_0
    return-void
.end method

.method public updateMatrices()V
    .locals 6

    .prologue
    .line 84
    const/4 v4, 0x0

    .local v4, "ti":I
    :goto_0
    iget v5, p0, Lcom/samsung/musicplus/glwidget/model/Matrices;->mMaxTransformationCount:I

    if-ge v4, v5, :cond_2

    .line 85
    iget-object v5, p0, Lcom/samsung/musicplus/glwidget/model/Matrices;->mTransformations:[Lcom/samsung/musicplus/glwidget/model/Matrices$MatrixUpdater;

    aget-object v3, v5, v4

    .line 86
    .local v3, "t":Lcom/samsung/musicplus/glwidget/model/Matrices$MatrixUpdater;
    if-eqz v3, :cond_1

    .line 87
    const/4 v2, 0x0

    .line 88
    .local v2, "offset":I
    iget-object v5, p0, Lcom/samsung/musicplus/glwidget/model/Matrices;->mMatrices:[[F

    aget-object v0, v5, v4

    .line 89
    .local v0, "matrices":[F
    const/4 v1, 0x0

    .local v1, "mi":I
    :goto_1
    iget v5, p0, Lcom/samsung/musicplus/glwidget/model/Matrices;->mMatrixCount:I

    if-ge v1, v5, :cond_1

    .line 90
    invoke-interface {v3, v1}, Lcom/samsung/musicplus/glwidget/model/Matrices$MatrixUpdater;->needUpdateMatrix(I)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 91
    invoke-static {v0, v2}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 92
    invoke-interface {v3, v1, v0, v2}, Lcom/samsung/musicplus/glwidget/model/Matrices$MatrixUpdater;->updateMatrix(I[FI)V

    .line 94
    :cond_0
    add-int/lit8 v2, v2, 0x10

    .line 89
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 84
    .end local v0    # "matrices":[F
    .end local v1    # "mi":I
    .end local v2    # "offset":I
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 98
    .end local v3    # "t":Lcom/samsung/musicplus/glwidget/model/Matrices$MatrixUpdater;
    :cond_2
    return-void
.end method
