.class public abstract Lcom/samsung/musicplus/glwidget/utils/Camera;
.super Ljava/lang/Object;
.source "Camera.java"


# instance fields
.field protected mBottom:F

.field protected mFar:F

.field protected mLeft:F

.field protected mLookX:F

.field protected mLookY:F

.field protected mLookZ:F

.field private mMatrix:[F

.field protected mNear:F

.field protected mNeedUpdateMatrix:Z

.field protected mRight:F

.field protected mTop:F

.field protected mX:F

.field protected mY:F

.field protected mZ:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 12
    const/16 v0, 0x10

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/samsung/musicplus/glwidget/utils/Camera;->mMatrix:[F

    .line 14
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/glwidget/utils/Camera;->mNeedUpdateMatrix:Z

    return-void
.end method


# virtual methods
.method protected abstract doGetMatrix([F)V
.end method

.method public getMatrix()[F
    .locals 1

    .prologue
    .line 135
    iget-boolean v0, p0, Lcom/samsung/musicplus/glwidget/utils/Camera;->mNeedUpdateMatrix:Z

    if-eqz v0, :cond_0

    .line 136
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/utils/Camera;->mMatrix:[F

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/glwidget/utils/Camera;->doGetMatrix([F)V

    .line 138
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/utils/Camera;->mMatrix:[F

    return-object v0
.end method

.method public getPosX()F
    .locals 1

    .prologue
    .line 93
    iget v0, p0, Lcom/samsung/musicplus/glwidget/utils/Camera;->mX:F

    return v0
.end method

.method public getPosY()F
    .locals 1

    .prologue
    .line 100
    iget v0, p0, Lcom/samsung/musicplus/glwidget/utils/Camera;->mY:F

    return v0
.end method

.method public getPosZ()F
    .locals 1

    .prologue
    .line 107
    iget v0, p0, Lcom/samsung/musicplus/glwidget/utils/Camera;->mZ:F

    return v0
.end method

.method public setDistance(FF)V
    .locals 1
    .param p1, "near"    # F
    .param p2, "far"    # F

    .prologue
    .line 158
    iget v0, p0, Lcom/samsung/musicplus/glwidget/utils/Camera;->mNear:F

    cmpl-float v0, v0, p1

    if-nez v0, :cond_0

    iget v0, p0, Lcom/samsung/musicplus/glwidget/utils/Camera;->mFar:F

    cmpl-float v0, v0, p2

    if-nez v0, :cond_0

    .line 164
    :goto_0
    return-void

    .line 161
    :cond_0
    iput p1, p0, Lcom/samsung/musicplus/glwidget/utils/Camera;->mNear:F

    .line 162
    iput p2, p0, Lcom/samsung/musicplus/glwidget/utils/Camera;->mFar:F

    .line 163
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/glwidget/utils/Camera;->mNeedUpdateMatrix:Z

    goto :goto_0
.end method

.method public setLookAt(FFF)V
    .locals 1
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "z"    # F

    .prologue
    .line 118
    iget v0, p0, Lcom/samsung/musicplus/glwidget/utils/Camera;->mLookX:F

    cmpl-float v0, v0, p1

    if-nez v0, :cond_0

    iget v0, p0, Lcom/samsung/musicplus/glwidget/utils/Camera;->mLookY:F

    cmpl-float v0, v0, p2

    if-nez v0, :cond_0

    iget v0, p0, Lcom/samsung/musicplus/glwidget/utils/Camera;->mLookZ:F

    cmpl-float v0, v0, p3

    if-nez v0, :cond_0

    .line 125
    :goto_0
    return-void

    .line 121
    :cond_0
    iput p1, p0, Lcom/samsung/musicplus/glwidget/utils/Camera;->mLookX:F

    .line 122
    iput p2, p0, Lcom/samsung/musicplus/glwidget/utils/Camera;->mLookY:F

    .line 123
    iput p3, p0, Lcom/samsung/musicplus/glwidget/utils/Camera;->mLookZ:F

    .line 124
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/glwidget/utils/Camera;->mNeedUpdateMatrix:Z

    goto :goto_0
.end method

.method public setPosition(FFF)V
    .locals 1
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "z"    # F

    .prologue
    .line 80
    iget v0, p0, Lcom/samsung/musicplus/glwidget/utils/Camera;->mX:F

    cmpl-float v0, p1, v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/samsung/musicplus/glwidget/utils/Camera;->mY:F

    cmpl-float v0, p2, v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/samsung/musicplus/glwidget/utils/Camera;->mZ:F

    cmpl-float v0, p3, v0

    if-nez v0, :cond_0

    .line 87
    :goto_0
    return-void

    .line 83
    :cond_0
    iput p1, p0, Lcom/samsung/musicplus/glwidget/utils/Camera;->mX:F

    .line 84
    iput p2, p0, Lcom/samsung/musicplus/glwidget/utils/Camera;->mY:F

    .line 85
    iput p3, p0, Lcom/samsung/musicplus/glwidget/utils/Camera;->mZ:F

    .line 86
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/glwidget/utils/Camera;->mNeedUpdateMatrix:Z

    goto :goto_0
.end method

.method public setViewport(FF)V
    .locals 3
    .param p1, "width"    # F
    .param p2, "height"    # F

    .prologue
    const/high16 v2, 0x40000000    # 2.0f

    .line 47
    div-float v1, p1, v2

    .line 48
    .local v1, "halfWidth":F
    div-float v0, p2, v2

    .line 49
    .local v0, "halfHeight":F
    neg-float v2, v1

    iput v2, p0, Lcom/samsung/musicplus/glwidget/utils/Camera;->mLeft:F

    .line 50
    iput v1, p0, Lcom/samsung/musicplus/glwidget/utils/Camera;->mRight:F

    .line 51
    iput v0, p0, Lcom/samsung/musicplus/glwidget/utils/Camera;->mTop:F

    .line 52
    neg-float v2, v0

    iput v2, p0, Lcom/samsung/musicplus/glwidget/utils/Camera;->mBottom:F

    .line 53
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/musicplus/glwidget/utils/Camera;->mNeedUpdateMatrix:Z

    .line 54
    return-void
.end method

.method public setViewport(FFFF)V
    .locals 1
    .param p1, "left"    # F
    .param p2, "right"    # F
    .param p3, "bottom"    # F
    .param p4, "top"    # F

    .prologue
    .line 65
    iput p1, p0, Lcom/samsung/musicplus/glwidget/utils/Camera;->mLeft:F

    .line 66
    iput p2, p0, Lcom/samsung/musicplus/glwidget/utils/Camera;->mRight:F

    .line 67
    iput p3, p0, Lcom/samsung/musicplus/glwidget/utils/Camera;->mBottom:F

    .line 68
    iput p4, p0, Lcom/samsung/musicplus/glwidget/utils/Camera;->mTop:F

    .line 69
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/glwidget/utils/Camera;->mNeedUpdateMatrix:Z

    .line 70
    return-void
.end method
