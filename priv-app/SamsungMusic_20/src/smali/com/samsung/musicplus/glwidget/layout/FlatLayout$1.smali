.class Lcom/samsung/musicplus/glwidget/layout/FlatLayout$1;
.super Ljava/lang/Object;
.source "FlatLayout.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/glwidget/layout/FlatLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/glwidget/layout/FlatLayout;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/glwidget/layout/FlatLayout;)V
    .locals 0

    .prologue
    .line 463
    iput-object p1, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout$1;->this$0:Lcom/samsung/musicplus/glwidget/layout/FlatLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 466
    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout$1;->this$0:Lcom/samsung/musicplus/glwidget/layout/FlatLayout;

    # getter for: Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapModel;
    invoke-static {v1}, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->access$000(Lcom/samsung/musicplus/glwidget/layout/FlatLayout;)[Lcom/samsung/musicplus/glwidget/model/BitmapModel;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/musicplus/glwidget/model/ModelUtils;->hasStartedAnimation([Lcom/samsung/musicplus/glwidget/model/AnimationModel;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout$1;->this$0:Lcom/samsung/musicplus/glwidget/layout/FlatLayout;

    # getter for: Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mTexts:[Lcom/samsung/musicplus/glwidget/model/ViewModel;
    invoke-static {v1}, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->access$100(Lcom/samsung/musicplus/glwidget/layout/FlatLayout;)[Lcom/samsung/musicplus/glwidget/model/ViewModel;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/musicplus/glwidget/model/ModelUtils;->hasStartedAnimation([Lcom/samsung/musicplus/glwidget/model/AnimationModel;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    const/4 v0, 0x1

    .line 467
    .local v0, "more":Z
    :goto_0
    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout$1;->this$0:Lcom/samsung/musicplus/glwidget/layout/FlatLayout;

    iget-object v1, v1, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mParent:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v1}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->requestRender()V

    .line 468
    if-eqz v0, :cond_1

    .line 469
    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout$1;->this$0:Lcom/samsung/musicplus/glwidget/layout/FlatLayout;

    iget-object v1, v1, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mParent:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v1, p0}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->postOnAnimation(Ljava/lang/Runnable;)V

    .line 471
    :cond_1
    return-void

    .line 466
    .end local v0    # "more":Z
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
