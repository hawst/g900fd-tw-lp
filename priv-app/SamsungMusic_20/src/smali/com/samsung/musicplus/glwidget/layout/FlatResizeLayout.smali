.class public Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;
.super Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;
.source "FlatResizeLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout$AlbArtPublisher;,
        Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout$Config;
    }
.end annotation


# static fields
.field private static final ART_LAYER:I = 0x0

.field private static final NOT_SELECTED_Z:F = 2000.0f

.field private static final SELECTED_Z:F = 2200.0f

.field private static final TEXT_LAYER:I = 0x1

.field private static final TRANSFORMATIONS_COUNT:I = 0x2

.field private static final TRANSFORMATION_ART:I = 0x0

.field private static final TRANSFORMATION_SHADOW:I = 0x1


# instance fields
.field private mAdapterIndexMap:Landroid/util/SparseIntArray;

.field private mAlbOpK:F

.field private mAlbOpacity:F

.field private mAlbSize:I

.field private mAlbSpacing:I

.field private mAlbSpacingAdditional:I

.field private mAlbTextOpK:F

.field private mAlbY:I

.field private mAlbumSpacingK:F

.field private mAlbumTexts:[Lcom/samsung/musicplus/glwidget/render/BitmapView;

.field private mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapsModel;

.field private mAnimK:F

.field private mAnimLeft:I

.field private mAnimRight:I

.field private mCamera:Lcom/samsung/musicplus/glwidget/utils/Camera;

.field private mCenterX:I

.field private mCenterY:I

.field private mConfig:Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout$Config;

.field private mHasShadow:Z

.field private mLastPositionMap:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/samsung/musicplus/glwidget/layout/IAlbumArtInfo$PositionInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mMarkers:Lcom/samsung/musicplus/glwidget/utils/MarkerViews;

.field private mMatrices:Lcom/samsung/musicplus/glwidget/model/Matrices;

.field private mMaxScrollWidth:I

.field private mMaxSelectedDx:F

.field private mModelsVBO:Lcom/samsung/musicplus/glwidget/render/VBO;

.field private mSelHeight:I

.field private mSelWidth:I

.field private mSelectedModelIndex:I

.field private mShader:Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;

.field private mShadow:Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;


# direct methods
.method public constructor <init>(Lcom/samsung/musicplus/glwidget/layout/GLHolder;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "parent"    # Lcom/samsung/musicplus/glwidget/layout/GLHolder;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v0, 0x0

    .line 107
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;-><init>(Lcom/samsung/musicplus/glwidget/layout/GLHolder;)V

    .line 92
    new-instance v1, Lcom/samsung/musicplus/glwidget/utils/OrthoCamera;

    invoke-direct {v1}, Lcom/samsung/musicplus/glwidget/utils/OrthoCamera;-><init>()V

    iput-object v1, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mCamera:Lcom/samsung/musicplus/glwidget/utils/Camera;

    .line 199
    new-instance v1, Landroid/util/SparseIntArray;

    invoke-direct {v1}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v1, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAdapterIndexMap:Landroid/util/SparseIntArray;

    .line 201
    new-instance v1, Landroid/util/SparseArray;

    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    iput-object v1, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mLastPositionMap:Landroid/util/SparseArray;

    .line 109
    const-string v1, "com.samsung.musicplus.glwidget.layout.FRLayout"

    iput-object v1, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->LOG_TAG:Ljava/lang/String;

    .line 110
    new-instance v1, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout$Config;

    invoke-virtual {p1}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v2

    sget-object v3, Lcom/sec/android/app/music/R$styleable;->GLFlatResizeGalleryView:[I

    invoke-virtual {v2, p2, v3, v0, v0}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout$Config;-><init>(Landroid/content/res/TypedArray;)V

    iput-object v1, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mConfig:Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout$Config;

    .line 112
    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mConfig:Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout$Config;

    invoke-virtual {v1}, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout$Config;->getScrollingFriction()F

    move-result v1

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->setScrollingFriction(F)V

    .line 114
    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mConfig:Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout$Config;

    invoke-virtual {v1}, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout$Config;->getShadowBitmap()I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    iput-boolean v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mHasShadow:Z

    .line 115
    return-void
.end method

.method private fillAlbumsArray(I)V
    .locals 11
    .param p1, "albumsCount"    # I

    .prologue
    const/4 v10, 0x0

    const/4 v5, 0x2

    const/4 v9, 0x1

    .line 119
    const/4 v7, 0x2

    .line 120
    .local v7, "layersCount":I
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mConfig:Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout$Config;

    invoke-virtual {v0}, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout$Config;->getTextLayout()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 121
    const/4 v7, 0x1

    .line 124
    :cond_0
    div-int/lit8 v0, p1, 0x2

    iput v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mSelectedModelIndex:I

    .line 126
    if-le v7, v9, :cond_2

    .line 127
    new-array v0, p1, [Lcom/samsung/musicplus/glwidget/render/BitmapView;

    iput-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbumTexts:[Lcom/samsung/musicplus/glwidget/render/BitmapView;

    .line 132
    :goto_0
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mParent:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v0}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    .line 134
    .local v4, "inflater":Landroid/view/LayoutInflater;
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapsModel;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapsModel;

    array-length v0, v0

    if-eq v0, p1, :cond_3

    .line 135
    :cond_1
    new-array v0, p1, [Lcom/samsung/musicplus/glwidget/model/BitmapsModel;

    iput-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapsModel;

    .line 136
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_1
    if-ge v6, p1, :cond_3

    .line 137
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapsModel;

    new-instance v1, Lcom/samsung/musicplus/glwidget/model/BitmapsModel;

    invoke-direct {v1, v7}, Lcom/samsung/musicplus/glwidget/model/BitmapsModel;-><init>(I)V

    aput-object v1, v0, v6

    .line 136
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 129
    .end local v4    # "inflater":Landroid/view/LayoutInflater;
    .end local v6    # "i":I
    :cond_2
    iput-object v10, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbumTexts:[Lcom/samsung/musicplus/glwidget/render/BitmapView;

    goto :goto_0

    .line 141
    .restart local v4    # "inflater":Landroid/view/LayoutInflater;
    :cond_3
    new-instance v0, Lcom/samsung/musicplus/glwidget/utils/MarkerViews;

    iget v1, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mSelHeight:I

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->getConverter()Lcom/samsung/musicplus/glwidget/utils/CoordConverter;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapsModel;

    invoke-direct/range {v0 .. v5}, Lcom/samsung/musicplus/glwidget/utils/MarkerViews;-><init>(FLcom/samsung/musicplus/glwidget/utils/CoordConverter;[Lcom/samsung/musicplus/glwidget/model/AnimationModel;Landroid/view/LayoutInflater;I)V

    iput-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mMarkers:Lcom/samsung/musicplus/glwidget/utils/MarkerViews;

    .line 143
    if-le v7, v9, :cond_4

    .line 144
    iget v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mSelHeight:I

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    .line 146
    .local v8, "size":I
    const/4 v6, 0x0

    .restart local v6    # "i":I
    :goto_2
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapsModel;

    array-length v0, v0

    if-ge v6, v0, :cond_4

    .line 147
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbumTexts:[Lcom/samsung/musicplus/glwidget/render/BitmapView;

    new-instance v1, Lcom/samsung/musicplus/glwidget/render/BitmapView;

    iget-object v2, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mConfig:Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout$Config;

    invoke-virtual {v2}, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout$Config;->getTextLayout()I

    move-result v2

    invoke-virtual {v4, v2, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    invoke-direct {v1, v2, v8, v8}, Lcom/samsung/musicplus/glwidget/render/BitmapView;-><init>(Landroid/view/View;II)V

    aput-object v1, v0, v6

    .line 146
    add-int/lit8 v6, v6, 0x1

    goto :goto_2

    .line 152
    .end local v6    # "i":I
    .end local v8    # "size":I
    :cond_4
    new-instance v0, Lcom/samsung/musicplus/glwidget/model/Matrices;

    invoke-direct {v0, v5, p1}, Lcom/samsung/musicplus/glwidget/model/Matrices;-><init>(II)V

    iput-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mMatrices:Lcom/samsung/musicplus/glwidget/model/Matrices;

    .line 153
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mMatrices:Lcom/samsung/musicplus/glwidget/model/Matrices;

    const/4 v1, 0x0

    new-instance v2, Lcom/samsung/musicplus/glwidget/model/ModelMatrixUpdater;

    iget-object v3, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapsModel;

    invoke-direct {v2, v3}, Lcom/samsung/musicplus/glwidget/model/ModelMatrixUpdater;-><init>([Lcom/samsung/musicplus/glwidget/model/Model;)V

    invoke-virtual {v0, v1, v2}, Lcom/samsung/musicplus/glwidget/model/Matrices;->setTransformation(ILcom/samsung/musicplus/glwidget/model/Matrices$MatrixUpdater;)V

    .line 155
    iget-boolean v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mHasShadow:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mShadow:Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;

    if-eqz v0, :cond_5

    .line 156
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mMatrices:Lcom/samsung/musicplus/glwidget/model/Matrices;

    new-instance v1, Lcom/samsung/musicplus/glwidget/model/ShadowMatrixUpdater;

    iget-object v2, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapsModel;

    iget-object v3, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mShadow:Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;

    invoke-direct {v1, v2, v3}, Lcom/samsung/musicplus/glwidget/model/ShadowMatrixUpdater;-><init>([Lcom/samsung/musicplus/glwidget/model/Model;Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;)V

    invoke-virtual {v0, v9, v1}, Lcom/samsung/musicplus/glwidget/model/Matrices;->setTransformation(ILcom/samsung/musicplus/glwidget/model/Matrices$MatrixUpdater;)V

    .line 158
    :cond_5
    return-void
.end method

.method private resizeAnimated(IF)Z
    .locals 12
    .param p1, "idx"    # I
    .param p2, "dx"    # F

    .prologue
    .line 636
    iget-object v9, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapsModel;

    aget-object v3, v9, p1

    .line 637
    .local v3, "model":Lcom/samsung/musicplus/glwidget/model/BitmapsModel;
    invoke-virtual {v3}, Lcom/samsung/musicplus/glwidget/model/BitmapsModel;->getX()F

    move-result v9

    sub-float v8, v9, p2

    .line 638
    .local v8, "x":F
    invoke-virtual {v3}, Lcom/samsung/musicplus/glwidget/model/BitmapsModel;->getHeight()F

    move-result v2

    .line 639
    .local v2, "height":F
    iget v9, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAnimLeft:I

    int-to-float v9, v9

    cmpl-float v9, v8, v9

    if-ltz v9, :cond_3

    iget v9, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAnimRight:I

    int-to-float v9, v9

    cmpg-float v9, v8, v9

    if-gtz v9, :cond_3

    const/4 v4, 0x1

    .line 640
    .local v4, "needScale":Z
    :goto_0
    const/4 v5, 0x0

    .line 641
    .local v5, "positionAlphaValue":I
    iget-object v9, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mLastPositionMap:Landroid/util/SparseArray;

    iget-object v10, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAdapterIndexMap:Landroid/util/SparseIntArray;

    invoke-virtual {v10, p1}, Landroid/util/SparseIntArray;->get(I)I

    move-result v10

    invoke-virtual {v9, v10}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/musicplus/glwidget/layout/IAlbumArtInfo$PositionInfo;

    .line 642
    .local v6, "prevInfo":Lcom/samsung/musicplus/glwidget/layout/IAlbumArtInfo$PositionInfo;
    if-nez v6, :cond_0

    .line 643
    new-instance v6, Lcom/samsung/musicplus/glwidget/layout/IAlbumArtInfo$PositionInfo;

    .end local v6    # "prevInfo":Lcom/samsung/musicplus/glwidget/layout/IAlbumArtInfo$PositionInfo;
    invoke-direct {v6}, Lcom/samsung/musicplus/glwidget/layout/IAlbumArtInfo$PositionInfo;-><init>()V

    .line 644
    .restart local v6    # "prevInfo":Lcom/samsung/musicplus/glwidget/layout/IAlbumArtInfo$PositionInfo;
    const/high16 v9, 0x3f800000    # 1.0f

    iput v9, v6, Lcom/samsung/musicplus/glwidget/layout/IAlbumArtInfo$PositionInfo;->alpha:F

    .line 645
    const/4 v9, 0x0

    iput v9, v6, Lcom/samsung/musicplus/glwidget/layout/IAlbumArtInfo$PositionInfo;->basePosition:I

    .line 648
    :cond_0
    if-eqz v4, :cond_5

    .line 650
    iget v9, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mCenterX:I

    int-to-float v9, v9

    cmpg-float v9, v8, v9

    if-gez v9, :cond_4

    .line 651
    iget v9, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbSize:I

    int-to-float v9, v9

    iget v10, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAnimK:F

    iget v11, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAnimLeft:I

    int-to-float v11, v11

    sub-float v11, v8, v11

    mul-float/2addr v10, v11

    const/high16 v11, 0x40000000    # 2.0f

    mul-float/2addr v10, v11

    add-float v2, v9, v10

    .line 652
    iget v9, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAnimLeft:I

    int-to-float v9, v9

    sub-float v1, v8, v9

    .line 653
    .local v1, "dAlbOp":F
    const/4 v9, -0x1

    iput v9, v6, Lcom/samsung/musicplus/glwidget/layout/IAlbumArtInfo$PositionInfo;->basePosition:I

    .line 660
    :goto_1
    invoke-virtual {v3, v2, v2}, Lcom/samsung/musicplus/glwidget/model/BitmapsModel;->setSize(FF)V

    .line 661
    invoke-virtual {v3}, Lcom/samsung/musicplus/glwidget/model/BitmapsModel;->getAlphas()[F

    move-result-object v0

    .line 662
    .local v0, "alphas":[F
    const/4 v9, 0x0

    iget v10, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbOpK:F

    mul-float/2addr v10, v1

    iget v11, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbOpacity:F

    add-float/2addr v10, v11

    aput v10, v0, v9

    .line 663
    const/4 v9, 0x0

    aget v9, v0, v9

    iput v9, v6, Lcom/samsung/musicplus/glwidget/layout/IAlbumArtInfo$PositionInfo;->alpha:F

    .line 664
    iget-object v9, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mLastPositionMap:Landroid/util/SparseArray;

    iget-object v10, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAdapterIndexMap:Landroid/util/SparseIntArray;

    invoke-virtual {v10, p1}, Landroid/util/SparseIntArray;->get(I)I

    move-result v10

    invoke-virtual {v9, v10, v6}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 666
    invoke-virtual {v3}, Lcom/samsung/musicplus/glwidget/model/BitmapsModel;->getLayersCount()I

    move-result v9

    const/4 v10, 0x1

    if-le v9, v10, :cond_1

    .line 667
    const/4 v9, 0x1

    iget v10, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mCenterX:I

    int-to-float v10, v10

    sub-float/2addr v10, v8

    invoke-static {v10}, Ljava/lang/Math;->abs(F)F

    move-result v10

    iget v11, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbTextOpK:F

    mul-float/2addr v10, v11

    aput v10, v0, v9

    .line 678
    .end local v1    # "dAlbOp":F
    :cond_1
    :goto_2
    invoke-virtual {v3, v8}, Lcom/samsung/musicplus/glwidget/model/BitmapsModel;->setX(F)V

    .line 679
    const/high16 v9, 0x44fa0000    # 2000.0f

    invoke-virtual {v3, v9}, Lcom/samsung/musicplus/glwidget/model/BitmapsModel;->setZ(F)V

    .line 680
    iget v9, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mCenterX:I

    int-to-float v9, v9

    sub-float v9, v8, v9

    invoke-static {v9}, Ljava/lang/Math;->abs(F)F

    move-result v9

    const/high16 v10, 0x3f800000    # 1.0f

    sub-float/2addr v9, v10

    iget v10, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mMaxSelectedDx:F

    cmpg-float v9, v9, v10

    if-gtz v9, :cond_6

    const/4 v7, 0x1

    .line 681
    .local v7, "selected":Z
    :goto_3
    if-eqz v7, :cond_2

    .line 682
    const v9, 0x45098000    # 2200.0f

    invoke-virtual {v3, v9}, Lcom/samsung/musicplus/glwidget/model/BitmapsModel;->setZ(F)V

    .line 684
    :cond_2
    return v7

    .line 639
    .end local v0    # "alphas":[F
    .end local v4    # "needScale":Z
    .end local v5    # "positionAlphaValue":I
    .end local v6    # "prevInfo":Lcom/samsung/musicplus/glwidget/layout/IAlbumArtInfo$PositionInfo;
    .end local v7    # "selected":Z
    :cond_3
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 655
    .restart local v4    # "needScale":Z
    .restart local v5    # "positionAlphaValue":I
    .restart local v6    # "prevInfo":Lcom/samsung/musicplus/glwidget/layout/IAlbumArtInfo$PositionInfo;
    :cond_4
    iget v9, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbSize:I

    int-to-float v9, v9

    iget v10, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAnimK:F

    iget v11, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAnimRight:I

    int-to-float v11, v11

    sub-float v11, v8, v11

    mul-float/2addr v10, v11

    const/high16 v11, 0x40000000    # 2.0f

    mul-float/2addr v10, v11

    sub-float v2, v9, v10

    .line 656
    iget v9, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAnimRight:I

    int-to-float v9, v9

    sub-float v1, v9, v8

    .line 657
    .restart local v1    # "dAlbOp":F
    const/4 v9, 0x1

    iput v9, v6, Lcom/samsung/musicplus/glwidget/layout/IAlbumArtInfo$PositionInfo;->basePosition:I

    goto :goto_1

    .line 670
    .end local v1    # "dAlbOp":F
    :cond_5
    iget v9, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbSize:I

    int-to-float v9, v9

    iget v10, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbSize:I

    int-to-float v10, v10

    invoke-virtual {v3, v9, v10}, Lcom/samsung/musicplus/glwidget/model/BitmapsModel;->setSize(FF)V

    .line 671
    invoke-virtual {v3}, Lcom/samsung/musicplus/glwidget/model/BitmapsModel;->getAlphas()[F

    move-result-object v0

    .line 672
    .restart local v0    # "alphas":[F
    const/4 v9, 0x0

    iget v10, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbOpacity:F

    aput v10, v0, v9

    .line 673
    invoke-virtual {v3}, Lcom/samsung/musicplus/glwidget/model/BitmapsModel;->getLayersCount()I

    move-result v9

    const/4 v10, 0x1

    if-le v9, v10, :cond_1

    .line 674
    const/4 v9, 0x1

    const/high16 v10, 0x3f800000    # 1.0f

    aput v10, v0, v9

    goto :goto_2

    .line 680
    :cond_6
    const/4 v7, 0x0

    goto :goto_3
.end method

.method private updateMaxScrollWidth()V
    .locals 1

    .prologue
    .line 333
    iget v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbSize:I

    div-int/lit8 v0, v0, 0x4

    iput v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mMaxScrollWidth:I

    .line 334
    return-void
.end method


# virtual methods
.method protected animateToChild(I)V
    .locals 2
    .param p1, "adapterIndex"    # I

    .prologue
    .line 238
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->getSelection()I

    move-result v1

    sub-int v0, p1, v1

    .line 239
    .local v0, "di":I
    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->updateSelected(I)I

    .line 240
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->realignView()V

    .line 241
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->applyScroll(F)Z

    .line 242
    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mParent:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v1}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->requestRender()V

    .line 243
    return-void
.end method

.method public doGetAlbumArtForCoord([F[F)Landroid/graphics/Bitmap;
    .locals 13
    .param p1, "inCoord"    # [F
    .param p2, "outCoord"    # [F

    .prologue
    .line 456
    const/4 v7, 0x0

    aget v4, p1, v7

    .line 457
    .local v4, "x":F
    iget v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mHeight:I

    int-to-float v7, v7

    const/4 v8, 0x1

    aget v8, p1, v8

    sub-float v5, v7, v8

    .line 458
    .local v5, "y":F
    const v6, 0x44f9e000    # 1999.0f

    .line 459
    .local v6, "z":F
    const/4 v3, 0x0

    .line 462
    .local v3, "resAlbum":Lcom/samsung/musicplus/glwidget/model/BitmapsModel;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapsModel;

    array-length v7, v7

    if-ge v1, v7, :cond_1

    .line 463
    iget-object v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapsModel;

    aget-object v0, v7, v1

    .line 464
    .local v0, "cur":Lcom/samsung/musicplus/glwidget/model/BitmapsModel;
    invoke-virtual {v0}, Lcom/samsung/musicplus/glwidget/model/BitmapsModel;->getX()F

    move-result v7

    sub-float/2addr v7, v4

    invoke-static {v7}, Ljava/lang/Math;->abs(F)F

    move-result v7

    invoke-virtual {v0}, Lcom/samsung/musicplus/glwidget/model/BitmapsModel;->getWidth()F

    move-result v8

    const/high16 v9, 0x40000000    # 2.0f

    div-float/2addr v8, v9

    cmpg-float v7, v7, v8

    if-gez v7, :cond_0

    invoke-virtual {v0}, Lcom/samsung/musicplus/glwidget/model/BitmapsModel;->getY()F

    move-result v7

    sub-float/2addr v7, v5

    invoke-static {v7}, Ljava/lang/Math;->abs(F)F

    move-result v7

    invoke-virtual {v0}, Lcom/samsung/musicplus/glwidget/model/BitmapsModel;->getHeight()F

    move-result v8

    const/high16 v9, 0x40000000    # 2.0f

    div-float/2addr v8, v9

    cmpg-float v7, v7, v8

    if-gez v7, :cond_0

    invoke-virtual {v0}, Lcom/samsung/musicplus/glwidget/model/BitmapsModel;->getZ()F

    move-result v7

    cmpl-float v7, v7, v6

    if-lez v7, :cond_0

    invoke-virtual {v0}, Lcom/samsung/musicplus/glwidget/model/BitmapsModel;->getAlphas()[F

    move-result-object v7

    const/4 v8, 0x0

    aget v7, v7, v8

    float-to-double v8, v7

    const-wide v10, 0x3fb999999999999aL    # 0.1

    cmpl-double v7, v8, v10

    if-lez v7, :cond_0

    .line 467
    move-object v3, v0

    .line 468
    invoke-virtual {v0}, Lcom/samsung/musicplus/glwidget/model/BitmapsModel;->getZ()F

    move-result v6

    .line 462
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 471
    .end local v0    # "cur":Lcom/samsung/musicplus/glwidget/model/BitmapsModel;
    :cond_1
    if-eqz v3, :cond_3

    .line 473
    iget-object v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->LOG_TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "doGetAlbumArtForCoord cur x: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v3}, Lcom/samsung/musicplus/glwidget/model/BitmapsModel;->getX()F

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " y: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v3}, Lcom/samsung/musicplus/glwidget/model/BitmapsModel;->getY()F

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " size: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v3}, Lcom/samsung/musicplus/glwidget/model/BitmapsModel;->getHeight()F

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 475
    const/4 v7, 0x0

    const/4 v8, 0x0

    aget v8, p1, v8

    invoke-virtual {v3}, Lcom/samsung/musicplus/glwidget/model/BitmapsModel;->getX()F

    move-result v9

    invoke-virtual {v3}, Lcom/samsung/musicplus/glwidget/model/BitmapsModel;->getWidth()F

    move-result v10

    const/high16 v11, 0x40000000    # 2.0f

    div-float/2addr v10, v11

    sub-float/2addr v9, v10

    sub-float/2addr v8, v9

    aput v8, p2, v7

    .line 476
    const/4 v7, 0x1

    const/4 v8, 0x1

    aget v8, p1, v8

    iget v9, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mHeight:I

    int-to-float v9, v9

    invoke-virtual {v3}, Lcom/samsung/musicplus/glwidget/model/BitmapsModel;->getY()F

    move-result v10

    invoke-virtual {v3}, Lcom/samsung/musicplus/glwidget/model/BitmapsModel;->getHeight()F

    move-result v11

    const/high16 v12, 0x40000000    # 2.0f

    div-float/2addr v11, v12

    add-float/2addr v10, v11

    sub-float/2addr v9, v10

    sub-float/2addr v8, v9

    aput v8, p2, v7

    .line 478
    iget-object v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->LOG_TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "doGetAlbumArtForCoord outCoord before rescale x: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x0

    aget v9, p2, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " y: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x1

    aget v9, p2, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 480
    invoke-virtual {v3}, Lcom/samsung/musicplus/glwidget/model/BitmapsModel;->getRenderableBitmaps()[Landroid/graphics/Bitmap;

    move-result-object v7

    const/4 v8, 0x0

    aget-object v2, v7, v8

    .line 481
    .local v2, "res":Landroid/graphics/Bitmap;
    if-nez v2, :cond_2

    .line 482
    iget-object v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mParent:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v7}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->getDefaultAlbumArt()Landroid/graphics/Bitmap;

    move-result-object v2

    .line 485
    :cond_2
    const/4 v7, 0x0

    const/4 v8, 0x0

    aget v8, p2, v8

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v9

    int-to-float v9, v9

    mul-float/2addr v8, v9

    invoke-virtual {v3}, Lcom/samsung/musicplus/glwidget/model/BitmapsModel;->getWidth()F

    move-result v9

    div-float/2addr v8, v9

    float-to-int v8, v8

    int-to-float v8, v8

    aput v8, p2, v7

    .line 486
    const/4 v7, 0x1

    const/4 v8, 0x1

    aget v8, p2, v8

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    int-to-float v9, v9

    mul-float/2addr v8, v9

    invoke-virtual {v3}, Lcom/samsung/musicplus/glwidget/model/BitmapsModel;->getHeight()F

    move-result v9

    div-float/2addr v8, v9

    float-to-int v8, v8

    int-to-float v8, v8

    aput v8, p2, v7

    .line 488
    iget-object v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->LOG_TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "doGetAlbumArtForCoord outCoord x: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x0

    aget v9, p2, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " y: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x1

    aget v9, p2, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 492
    .end local v2    # "res":Landroid/graphics/Bitmap;
    :goto_1
    return-object v2

    :cond_3
    const/4 v2, 0x0

    goto :goto_1
.end method

.method protected doGetSelectedDx()F
    .locals 3

    .prologue
    .line 256
    const/4 v0, 0x0

    .line 257
    .local v0, "res":F
    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapsModel;

    if-eqz v1, :cond_0

    .line 258
    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapsModel;

    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->relIndex(I)I

    move-result v2

    aget-object v1, v1, v2

    invoke-virtual {v1}, Lcom/samsung/musicplus/glwidget/model/BitmapsModel;->getX()F

    move-result v1

    iget v2, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mCenterX:I

    int-to-float v2, v2

    sub-float v0, v1, v2

    .line 260
    :cond_0
    return v0
.end method

.method public getAlbumLastPosition(I)Lcom/samsung/musicplus/glwidget/layout/IAlbumArtInfo$PositionInfo;
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 689
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mLastPositionMap:Landroid/util/SparseArray;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/util/SparseArray;->get(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/glwidget/layout/IAlbumArtInfo$PositionInfo;

    return-object v0
.end method

.method protected getDebugState()Ljava/lang/String;
    .locals 4

    .prologue
    .line 450
    const-string v0, "SelectedAdapterIndex: %d mCenterX: %d mAlbSize: %d mSelHeight: %d ModelsCount: %d ModelsState: %s"

    const/4 v1, 0x6

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->getSelection()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget v3, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mCenterX:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget v3, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbSize:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget v3, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mSelHeight:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->getModelsCount()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x5

    iget-object v3, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapsModel;

    invoke-static {v3}, Lcom/samsung/musicplus/glwidget/model/ModelUtils;->getDebugState([Lcom/samsung/musicplus/glwidget/model/Model;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getMaxScrollWidth()F
    .locals 1

    .prologue
    .line 338
    iget v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mMaxScrollWidth:I

    int-to-float v0, v0

    return v0
.end method

.method public getMaxSelectedDx()F
    .locals 1

    .prologue
    .line 445
    iget v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mMaxSelectedDx:F

    return v0
.end method

.method protected getModelsCount()I
    .locals 1

    .prologue
    .line 265
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapsModel;

    if-eqz v0, :cond_0

    .line 266
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapsModel;

    array-length v0, v0

    .line 268
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected getSelectedModelPosition()I
    .locals 1

    .prologue
    .line 273
    iget v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mSelectedModelIndex:I

    return v0
.end method

.method protected limitScroll(F)F
    .locals 7
    .param p1, "dx"    # F

    .prologue
    const/high16 v6, 0x40000000    # 2.0f

    .line 306
    iget-object v3, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapsModel;

    const/4 v4, 0x0

    invoke-virtual {p0, v4}, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->relIndex(I)I

    move-result v4

    aget-object v1, v3, v4

    .line 307
    .local v1, "selected":Lcom/samsung/musicplus/glwidget/model/Model;
    invoke-virtual {v1}, Lcom/samsung/musicplus/glwidget/model/Model;->getX()F

    move-result v2

    .line 308
    .local v2, "selectedX":F
    iget v3, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mCenterX:I

    int-to-float v3, v3

    sub-float/2addr v3, v2

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    const/high16 v4, 0x3f800000    # 1.0f

    sub-float v0, v3, v4

    .line 310
    .local v0, "maxDx":F
    const/4 v3, 0x0

    cmpg-float v3, p1, v3

    if-gez v3, :cond_1

    .line 311
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->getSelection()I

    move-result v3

    if-lez v3, :cond_0

    .line 312
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->getSelection()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    iget v4, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbSize:I

    iget v5, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbSpacing:I

    add-int/2addr v4, v5

    mul-int/2addr v3, v4

    int-to-float v3, v3

    invoke-virtual {v1}, Lcom/samsung/musicplus/glwidget/model/Model;->getWidth()F

    move-result v4

    div-float/2addr v4, v6

    add-float/2addr v3, v4

    iget v4, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbSpacingAdditional:I

    int-to-float v4, v4

    add-float/2addr v3, v4

    add-float/2addr v0, v3

    .line 322
    :cond_0
    :goto_0
    return v0

    .line 316
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->getSelection()I

    move-result v3

    iget-object v4, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAdapter:Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;

    invoke-interface {v4}, Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;->getCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ge v3, v4, :cond_0

    .line 317
    iget-object v3, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAdapter:Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;

    invoke-interface {v3}, Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;->getCount()I

    move-result v3

    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->getSelection()I

    move-result v4

    sub-int/2addr v3, v4

    add-int/lit8 v3, v3, -0x2

    iget v4, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbSize:I

    iget v5, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbSpacing:I

    add-int/2addr v4, v5

    mul-int/2addr v3, v4

    int-to-float v3, v3

    invoke-virtual {v1}, Lcom/samsung/musicplus/glwidget/model/Model;->getWidth()F

    move-result v4

    div-float/2addr v4, v6

    add-float/2addr v3, v4

    iget v4, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbSpacingAdditional:I

    int-to-float v4, v4

    add-float/2addr v3, v4

    add-float/2addr v0, v3

    goto :goto_0
.end method

.method protected onDrawFrame(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 6
    .param p1, "notUsed"    # Ljavax/microedition/khronos/opengles/GL10;

    .prologue
    const/16 v5, 0xbe2

    .line 176
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapsModel;

    if-eqz v0, :cond_1

    .line 177
    const/16 v0, 0x4100

    invoke-static {v0}, Landroid/opengl/GLES20;->glClear(I)V

    .line 179
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mMatrices:Lcom/samsung/musicplus/glwidget/model/Matrices;

    invoke-virtual {v0}, Lcom/samsung/musicplus/glwidget/model/Matrices;->updateMatrices()V

    .line 181
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mModelsVBO:Lcom/samsung/musicplus/glwidget/render/VBO;

    invoke-virtual {v0}, Lcom/samsung/musicplus/glwidget/render/VBO;->bind()V

    .line 183
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mShader:Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;

    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mCamera:Lcom/samsung/musicplus/glwidget/utils/Camera;

    invoke-virtual {v1}, Lcom/samsung/musicplus/glwidget/utils/Camera;->getMatrix()[F

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mMatrices:Lcom/samsung/musicplus/glwidget/model/Matrices;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/musicplus/glwidget/model/Matrices;->getMatrices(I)[F

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapsModel;

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->renderBitmaps([F[F[Lcom/samsung/musicplus/glwidget/render/RenderableBitmaps;)V

    .line 185
    invoke-static {v5}, Landroid/opengl/GLES20;->glEnable(I)V

    .line 187
    iget-boolean v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mHasShadow:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mShadow:Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;

    if-eqz v0, :cond_0

    .line 188
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mShader:Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;

    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mCamera:Lcom/samsung/musicplus/glwidget/utils/Camera;

    invoke-virtual {v1}, Lcom/samsung/musicplus/glwidget/utils/Camera;->getMatrix()[F

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mMatrices:Lcom/samsung/musicplus/glwidget/model/Matrices;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/samsung/musicplus/glwidget/model/Matrices;->getMatrices(I)[F

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapsModel;

    iget-object v4, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mShadow:Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;

    invoke-virtual {v4}, Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->renderBitmap([F[F[Lcom/samsung/musicplus/glwidget/render/Renderable;Landroid/graphics/Bitmap;)V

    .line 191
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mMarkers:Lcom/samsung/musicplus/glwidget/utils/MarkerViews;

    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mCamera:Lcom/samsung/musicplus/glwidget/utils/Camera;

    iget-object v2, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mShader:Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/musicplus/glwidget/utils/MarkerViews;->render(Lcom/samsung/musicplus/glwidget/utils/Camera;Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;)V

    .line 193
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapsModel;

    invoke-static {v0}, Lcom/samsung/musicplus/glwidget/model/ModelUtils;->resetModified([Lcom/samsung/musicplus/glwidget/model/Model;)V

    .line 195
    invoke-static {v5}, Landroid/opengl/GLES20;->glDisable(I)V

    .line 197
    :cond_1
    return-void
.end method

.method protected onSingleTap(FF)Z
    .locals 3
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    .line 248
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapsModel;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAdapter:Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAdapter:Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;

    invoke-interface {v0}, Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 249
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mParent:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapsModel;

    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->relIndex(I)I

    move-result v2

    aget-object v1, v1, v2

    invoke-virtual {v1}, Lcom/samsung/musicplus/glwidget/model/BitmapsModel;->getAdapterIndex()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->fireOnItemClick(I)V

    .line 251
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method protected onSurfaceCreated(Ljavax/microedition/khronos/opengles/GL10;Ljavax/microedition/khronos/egl/EGLConfig;)V
    .locals 6
    .param p1, "notUsed"    # Ljavax/microedition/khronos/opengles/GL10;
    .param p2, "config"    # Ljavax/microedition/khronos/egl/EGLConfig;

    .prologue
    const/high16 v5, 0x437f0000    # 255.0f

    .line 162
    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mConfig:Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout$Config;

    invoke-virtual {v1}, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout$Config;->getBackgroundColor()I

    move-result v0

    .line 164
    .local v0, "color":I
    invoke-static {v0}, Landroid/graphics/Color;->red(I)I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v5

    invoke-static {v0}, Landroid/graphics/Color;->green(I)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v5

    invoke-static {v0}, Landroid/graphics/Color;->blue(I)I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v3, v5

    invoke-static {v0}, Landroid/graphics/Color;->alpha(I)I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v4, v5

    invoke-static {v1, v2, v3, v4}, Landroid/opengl/GLES20;->glClearColor(FFFF)V

    .line 166
    new-instance v1, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;

    iget-object v2, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mParent:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v2}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;-><init>(Landroid/content/Context;[Ljava/lang/String;)V

    iput-object v1, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mShader:Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;

    .line 167
    new-instance v1, Lcom/samsung/musicplus/glwidget/render/VBO;

    sget-object v2, Lcom/samsung/musicplus/glwidget/render/Meshes;->TexturedRectangle:[F

    invoke-direct {v1, v2}, Lcom/samsung/musicplus/glwidget/render/VBO;-><init>([F)V

    iput-object v1, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mModelsVBO:Lcom/samsung/musicplus/glwidget/render/VBO;

    .line 169
    const/16 v1, 0xb71

    invoke-static {v1}, Landroid/opengl/GLES20;->glEnable(I)V

    .line 170
    const/16 v1, 0x203

    invoke-static {v1}, Landroid/opengl/GLES20;->glDepthFunc(I)V

    .line 171
    const/16 v1, 0x302

    const/16 v2, 0x303

    invoke-static {v1, v2}, Landroid/opengl/GLES20;->glBlendFunc(II)V

    .line 172
    return-void
.end method

.method protected positionSelectedView(F)Z
    .locals 1
    .param p1, "dx"    # F

    .prologue
    .line 283
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->relIndex(I)I

    move-result v0

    invoke-direct {p0, v0, p1}, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->resizeAnimated(IF)Z

    move-result v0

    return v0
.end method

.method protected positionView(IIIIF)V
    .locals 10
    .param p1, "dir"    # I
    .param p2, "position"    # I
    .param p3, "prevIndex"    # I
    .param p4, "index"    # I
    .param p5, "dx"    # F

    .prologue
    .line 288
    iget-object v6, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapsModel;

    aget-object v1, v6, p3

    .line 289
    .local v1, "p":Lcom/samsung/musicplus/glwidget/model/Model;
    iget-object v6, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapsModel;

    aget-object v0, v6, p4

    .line 290
    .local v0, "c":Lcom/samsung/musicplus/glwidget/model/Model;
    invoke-virtual {v1}, Lcom/samsung/musicplus/glwidget/model/Model;->getX()F

    move-result v6

    int-to-float v7, p1

    invoke-virtual {v1}, Lcom/samsung/musicplus/glwidget/model/Model;->getWidth()F

    move-result v8

    invoke-virtual {v0}, Lcom/samsung/musicplus/glwidget/model/Model;->getWidth()F

    move-result v9

    add-float/2addr v8, v9

    const/high16 v9, 0x40000000    # 2.0f

    div-float/2addr v8, v9

    mul-float/2addr v7, v8

    add-float v5, v6, v7

    .line 291
    .local v5, "x":F
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->getSelectedModelPosition()I

    move-result v6

    sub-int v6, p2, v6

    invoke-static {v6}, Ljava/lang/Math;->abs(I)I

    move-result v6

    const/4 v7, 0x1

    if-gt v6, v7, :cond_0

    iget v6, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbSpacing:I

    iget v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbSpacingAdditional:I

    if-ne v6, v7, :cond_1

    .line 293
    :cond_0
    iget v6, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbSpacing:I

    mul-int/2addr v6, p1

    int-to-float v6, v6

    add-float/2addr v5, v6

    .line 300
    :goto_0
    invoke-virtual {v0, v5}, Lcom/samsung/musicplus/glwidget/model/Model;->setX(F)V

    .line 301
    const/4 v6, 0x0

    invoke-direct {p0, p4, v6}, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->resizeAnimated(IF)Z

    .line 302
    return-void

    .line 295
    :cond_1
    iget-object v6, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapsModel;

    const/4 v7, 0x0

    invoke-virtual {p0, v7}, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->relIndex(I)I

    move-result v7

    aget-object v2, v6, v7

    .line 296
    .local v2, "s":Lcom/samsung/musicplus/glwidget/model/Model;
    invoke-virtual {v2}, Lcom/samsung/musicplus/glwidget/model/Model;->getX()F

    move-result v6

    iget v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mCenterX:I

    int-to-float v7, v7

    sub-float/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v3

    .line 297
    .local v3, "selectedDx":F
    iget v6, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbSpacingAdditional:I

    int-to-float v6, v6

    iget v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbumSpacingK:F

    mul-float/2addr v7, v3

    add-float v4, v6, v7

    .line 298
    .local v4, "spacing":F
    int-to-float v6, p1

    mul-float/2addr v6, v4

    add-float/2addr v5, v6

    goto :goto_0
.end method

.method protected realignView()V
    .locals 12

    .prologue
    const/high16 v11, 0x3f800000    # 1.0f

    const/4 v10, 0x0

    .line 343
    iget-object v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mCamera:Lcom/samsung/musicplus/glwidget/utils/Camera;

    const v8, 0x3dcccccd    # 0.1f

    const v9, 0x459c4000    # 5000.0f

    invoke-virtual {v7, v8, v9}, Lcom/samsung/musicplus/glwidget/utils/Camera;->setDistance(FF)V

    .line 344
    iget-object v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mCamera:Lcom/samsung/musicplus/glwidget/utils/Camera;

    invoke-virtual {v7, v10, v10, v10}, Lcom/samsung/musicplus/glwidget/utils/Camera;->setLookAt(FFF)V

    .line 345
    iget-object v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mCamera:Lcom/samsung/musicplus/glwidget/utils/Camera;

    const v8, 0x458ca000    # 4500.0f

    invoke-virtual {v7, v10, v10, v8}, Lcom/samsung/musicplus/glwidget/utils/Camera;->setPosition(FFF)V

    .line 346
    iget-object v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mCamera:Lcom/samsung/musicplus/glwidget/utils/Camera;

    iget v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mWidth:I

    int-to-float v8, v8

    iget v9, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mHeight:I

    int-to-float v9, v9

    invoke-virtual {v7, v10, v8, v10, v9}, Lcom/samsung/musicplus/glwidget/utils/Camera;->setViewport(FFFF)V

    .line 348
    iget-object v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mConfig:Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout$Config;

    iget v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mHeight:I

    invoke-virtual {v7, v8}, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout$Config;->setHeight(I)V

    .line 350
    iget-object v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mConfig:Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout$Config;

    invoke-virtual {v7}, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout$Config;->getAlbSize()I

    move-result v7

    iput v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbSize:I

    .line 351
    iget-object v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mConfig:Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout$Config;

    invoke-virtual {v7}, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout$Config;->getAlbSpacing()I

    move-result v7

    iput v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbSpacing:I

    .line 352
    iget-object v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mConfig:Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout$Config;

    invoke-virtual {v7}, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout$Config;->getAlbSpacingAdditinal()I

    move-result v7

    iput v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbSpacingAdditional:I

    .line 353
    iget-object v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mConfig:Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout$Config;

    invoke-virtual {v7}, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout$Config;->getAlbOpacity()F

    move-result v7

    iput v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbOpacity:F

    .line 355
    iget-object v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mConfig:Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout$Config;

    invoke-virtual {v7}, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout$Config;->getSelAlbSize()I

    move-result v7

    iput v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mSelHeight:I

    .line 357
    iget v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mSelHeight:I

    iput v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mSelWidth:I

    .line 359
    iget v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mWidth:I

    iget v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mSelWidth:I

    sub-int/2addr v7, v8

    div-int/lit8 v4, v7, 0x2

    .line 360
    .local v4, "restWidth":I
    const/4 v6, 0x1

    .line 361
    .local v6, "visibleCount":I
    :goto_0
    iget v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbSpacing:I

    if-le v4, v7, :cond_0

    .line 362
    iget v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbSpacing:I

    iget v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbSize:I

    add-int/2addr v7, v8

    sub-int/2addr v4, v7

    .line 363
    add-int/lit8 v6, v6, 0x2

    goto :goto_0

    .line 366
    :cond_0
    add-int/lit8 v6, v6, 0x2

    .line 368
    const/4 v7, 0x5

    if-ge v6, v7, :cond_1

    .line 369
    const/4 v6, 0x5

    .line 372
    :cond_1
    iget-boolean v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mHasShadow:Z

    if-eqz v7, :cond_2

    iget-object v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapsModel;

    if-nez v7, :cond_2

    .line 374
    new-instance v7, Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;

    iget-object v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mParent:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v8}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    iget-object v9, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mConfig:Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout$Config;

    invoke-virtual {v9}, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout$Config;->getShadowBitmap()I

    move-result v9

    invoke-static {v8, v9}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mShadow:Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;

    .line 379
    :cond_2
    iget-object v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapsModel;

    if-eqz v7, :cond_3

    iget-object v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapsModel;

    array-length v7, v7

    if-eq v7, v6, :cond_4

    .line 380
    :cond_3
    invoke-direct {p0, v6}, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->fillAlbumsArray(I)V

    .line 383
    :cond_4
    iget v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mWidth:I

    div-int/lit8 v7, v7, 0x2

    iput v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mCenterX:I

    .line 384
    iget v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mHeight:I

    div-int/lit8 v7, v7, 0x2

    iput v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mCenterY:I

    .line 387
    iget v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mSelHeight:I

    iget v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbSize:I

    add-int/2addr v7, v8

    div-int/lit8 v7, v7, 0x2

    iget v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbSpacingAdditional:I

    add-int v1, v7, v8

    .line 388
    .local v1, "animWidth":I
    iget v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mCenterX:I

    sub-int/2addr v7, v1

    iput v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAnimLeft:I

    .line 389
    iget v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mCenterX:I

    add-int/2addr v7, v1

    iput v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAnimRight:I

    .line 391
    iget v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mSelHeight:I

    iget v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbSize:I

    sub-int/2addr v7, v8

    div-int/lit8 v7, v7, 0x2

    int-to-float v0, v7

    .line 392
    .local v0, "animDy":F
    int-to-float v7, v1

    div-float v7, v0, v7

    iput v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAnimK:F

    .line 393
    iget v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbOpacity:F

    sub-float v7, v11, v7

    iget v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mCenterX:I

    iget v9, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAnimLeft:I

    sub-int/2addr v8, v9

    int-to-float v8, v8

    div-float/2addr v7, v8

    iput v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbOpK:F

    .line 394
    iget v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mCenterX:I

    iget v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAnimLeft:I

    sub-int/2addr v7, v8

    int-to-float v7, v7

    div-float v7, v11, v7

    iput v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbTextOpK:F

    .line 395
    iget v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mHeight:I

    div-int/lit8 v7, v7, 0x2

    iput v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbY:I

    .line 423
    iget v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbSize:I

    div-int/lit8 v7, v7, 0x2

    int-to-float v7, v7

    add-float/2addr v7, v0

    iget v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbSpacing:I

    div-int/lit8 v8, v8, 0x2

    int-to-float v8, v8

    add-float/2addr v7, v8

    int-to-float v8, v1

    div-float v8, v0, v8

    add-float/2addr v8, v11

    div-float/2addr v7, v8

    iput v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mMaxSelectedDx:F

    .line 425
    iget-object v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->LOG_TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "MaxSelectedDx: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget v9, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mMaxSelectedDx:F

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 427
    iget v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbSpacing:I

    iget v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbSpacingAdditional:I

    sub-int/2addr v7, v8

    int-to-float v7, v7

    iget v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mMaxSelectedDx:F

    div-float/2addr v7, v8

    iput v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbumSpacingK:F

    .line 429
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    iget-object v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapsModel;

    array-length v7, v7

    if-ge v3, v7, :cond_5

    .line 430
    iget-object v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapsModel;

    aget-object v2, v7, v3

    .line 431
    .local v2, "cur":Lcom/samsung/musicplus/glwidget/model/Model;
    iget v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbY:I

    int-to-float v7, v7

    invoke-virtual {v2, v7}, Lcom/samsung/musicplus/glwidget/model/Model;->setY(F)V

    .line 432
    const/high16 v7, 0x44fa0000    # 2000.0f

    invoke-virtual {v2, v7}, Lcom/samsung/musicplus/glwidget/model/Model;->setZ(F)V

    .line 433
    iget v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbSize:I

    int-to-float v7, v7

    iget v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbSize:I

    int-to-float v8, v8

    invoke-virtual {v2, v7, v8}, Lcom/samsung/musicplus/glwidget/model/Model;->setSize(FF)V

    .line 429
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 436
    .end local v2    # "cur":Lcom/samsung/musicplus/glwidget/model/Model;
    :cond_5
    invoke-direct {p0}, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->updateMaxScrollWidth()V

    .line 438
    iget-object v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapsModel;

    const/4 v8, 0x0

    invoke-virtual {p0, v8}, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->relIndex(I)I

    move-result v8

    aget-object v5, v7, v8

    .line 439
    .local v5, "selected":Lcom/samsung/musicplus/glwidget/model/Model;
    iget v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mSelWidth:I

    int-to-float v7, v7

    iget v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mSelHeight:I

    int-to-float v8, v8

    invoke-virtual {v5, v7, v8}, Lcom/samsung/musicplus/glwidget/model/Model;->setSize(FF)V

    .line 440
    iget v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mCenterX:I

    int-to-float v7, v7

    iget v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mCenterY:I

    int-to-float v8, v8

    const v9, 0x45098000    # 2200.0f

    invoke-virtual {v5, v7, v8, v9}, Lcom/samsung/musicplus/glwidget/model/Model;->setPosition(FFF)V

    .line 441
    return-void
.end method

.method public setAdapterWrap(Z)V
    .locals 0
    .param p1, "wrap"    # Z

    .prologue
    .line 327
    invoke-super {p0, p1}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->setAdapterWrap(Z)V

    .line 328
    invoke-direct {p0}, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->updateMaxScrollWidth()V

    .line 329
    return-void
.end method

.method protected setModelVisible(IZ)V
    .locals 1
    .param p1, "viewIndex"    # I
    .param p2, "visible"    # Z

    .prologue
    .line 278
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapsModel;

    aget-object v0, v0, p1

    invoke-virtual {v0, p2}, Lcom/samsung/musicplus/glwidget/model/BitmapsModel;->setVisibility(Z)V

    .line 279
    return-void
.end method

.method protected updateItemData(II)V
    .locals 10
    .param p1, "viewIndex"    # I
    .param p2, "adapterIndex"    # I

    .prologue
    const/4 v9, 0x0

    .line 205
    iget-object v6, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAdapter:Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;

    iget-object v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mParent:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v7}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-interface {v6, v7, p2}, Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;->getAlbArtUri(Landroid/content/Context;I)Landroid/net/Uri;

    move-result-object v4

    .line 207
    .local v4, "uri":Landroid/net/Uri;
    iget-object v6, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAdapterIndexMap:Landroid/util/SparseIntArray;

    invoke-virtual {v6, p1, p2}, Landroid/util/SparseIntArray;->put(II)V

    .line 209
    iget-object v6, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAdapter:Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;

    iget-object v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mParent:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v7}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-interface {v6, v7, p2}, Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;->getMarkerViewCount(Landroid/content/Context;I)I

    move-result v2

    .line 210
    .local v2, "markersCount":I
    iget-object v6, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAdapter:Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;

    iget-object v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mParent:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v7}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->getContext()Landroid/content/Context;

    move-result-object v7

    iget-object v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mMarkers:Lcom/samsung/musicplus/glwidget/utils/MarkerViews;

    invoke-virtual {v8, v2}, Lcom/samsung/musicplus/glwidget/utils/MarkerViews;->getBindMarkers(I)[Lcom/samsung/musicplus/glwidget/GLGalleryView$MarkerViewInfo;

    move-result-object v8

    invoke-interface {v6, v7, p2, v8}, Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;->updateMarkerViews(Landroid/content/Context;I[Lcom/samsung/musicplus/glwidget/GLGalleryView$MarkerViewInfo;)V

    .line 211
    iget-object v6, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mMarkers:Lcom/samsung/musicplus/glwidget/utils/MarkerViews;

    invoke-virtual {v6, p1, v2}, Lcom/samsung/musicplus/glwidget/utils/MarkerViews;->updateMakers(II)V

    .line 213
    iget-object v6, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapsModel;

    aget-object v1, v6, p1

    .line 214
    .local v1, "cur":Lcom/samsung/musicplus/glwidget/model/BitmapsModel;
    iget-object v6, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbumTexts:[Lcom/samsung/musicplus/glwidget/render/BitmapView;

    if-eqz v6, :cond_0

    .line 215
    iget-object v6, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbumTexts:[Lcom/samsung/musicplus/glwidget/render/BitmapView;

    aget-object v6, v6, p1

    invoke-virtual {v6}, Lcom/samsung/musicplus/glwidget/render/BitmapView;->getView()Landroid/view/View;

    move-result-object v5

    .line 216
    .local v5, "view":Landroid/view/View;
    iget-object v6, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAdapter:Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;

    iget-object v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mParent:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v7}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->getContext()Landroid/content/Context;

    move-result-object v7

    invoke-interface {v6, v7, v5, p2}, Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;->bindView(Landroid/content/Context;Landroid/view/View;I)V

    .line 217
    invoke-virtual {v5}, Landroid/view/View;->invalidate()V

    .line 218
    iget-object v6, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mAlbumTexts:[Lcom/samsung/musicplus/glwidget/render/BitmapView;

    aget-object v6, v6, p1

    invoke-virtual {v6}, Lcom/samsung/musicplus/glwidget/render/BitmapView;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v6

    const/4 v7, 0x1

    invoke-virtual {v1, v6, v7}, Lcom/samsung/musicplus/glwidget/model/BitmapsModel;->sendBitmap(Landroid/graphics/Bitmap;I)V

    .line 220
    .end local v5    # "view":Landroid/view/View;
    :cond_0
    invoke-virtual {v1, p2}, Lcom/samsung/musicplus/glwidget/model/BitmapsModel;->setAdapterIndex(I)V

    .line 221
    const/4 v0, 0x0

    .line 222
    .local v0, "bm":Landroid/graphics/Bitmap;
    if-eqz v4, :cond_1

    .line 223
    invoke-static {}, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->getCache()Lcom/samsung/musicplus/bitmapcache/BitmapCache;

    move-result-object v6

    invoke-virtual {v6, v4}, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->getBitmap(Landroid/net/Uri;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 225
    :cond_1
    if-eqz v0, :cond_2

    .line 226
    const/4 v6, 0x0

    invoke-virtual {v1, v6}, Lcom/samsung/musicplus/glwidget/model/BitmapsModel;->setPublisher(Lcom/samsung/musicplus/bitmapcache/BitmapCache$Publisher;)V

    .line 227
    invoke-virtual {v1, v0, v9}, Lcom/samsung/musicplus/glwidget/model/BitmapsModel;->setBitmap(Landroid/graphics/Bitmap;I)V

    .line 234
    :goto_0
    return-void

    .line 229
    :cond_2
    iget-object v6, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mParent:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v6}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->getDefaultAlbumArt()Landroid/graphics/Bitmap;

    move-result-object v6

    invoke-virtual {v1, v6, v9}, Lcom/samsung/musicplus/glwidget/model/BitmapsModel;->setBitmap(Landroid/graphics/Bitmap;I)V

    .line 230
    new-instance v3, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout$AlbArtPublisher;

    invoke-direct {v3, p0, p2, v1}, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout$AlbArtPublisher;-><init>(Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;ILcom/samsung/musicplus/glwidget/model/BitmapsModel;)V

    .line 231
    .local v3, "publisher":Lcom/samsung/musicplus/bitmapcache/BitmapCache$Publisher;
    invoke-virtual {v1, v3}, Lcom/samsung/musicplus/glwidget/model/BitmapsModel;->setPublisher(Lcom/samsung/musicplus/bitmapcache/BitmapCache$Publisher;)V

    .line 232
    invoke-static {}, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->getCache()Lcom/samsung/musicplus/bitmapcache/BitmapCache;

    move-result-object v6

    invoke-virtual {v6, v4, v3}, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->loadBitmap(Landroid/net/Uri;Lcom/samsung/musicplus/bitmapcache/BitmapCache$Publisher;)V

    goto :goto_0
.end method
