.class public Lcom/samsung/musicplus/glwidget/render/ShadeShader;
.super Ljava/lang/Object;
.source "ShadeShader.java"


# instance fields
.field private mColorHandler:I

.field private mCoordHandler:I

.field private mHandler:I

.field private mModelMatrixHandler:I

.field private mPVMatrixHandler:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    const/4 v0, 0x0

    const v1, 0x7f080003

    const v2, 0x7f080002

    invoke-static {p1, v0, v1, v2}, Lcom/samsung/musicplus/glwidget/render/ShaderCompiler;->compile(Landroid/content/Context;[Ljava/lang/String;II)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/glwidget/render/ShadeShader;->mHandler:I

    .line 23
    invoke-direct {p0}, Lcom/samsung/musicplus/glwidget/render/ShadeShader;->use()V

    .line 24
    iget v0, p0, Lcom/samsung/musicplus/glwidget/render/ShadeShader;->mHandler:I

    const-string v1, "aCoord"

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetAttribLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/glwidget/render/ShadeShader;->mCoordHandler:I

    .line 25
    iget v0, p0, Lcom/samsung/musicplus/glwidget/render/ShadeShader;->mHandler:I

    const-string v1, "aColor"

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetAttribLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/glwidget/render/ShadeShader;->mColorHandler:I

    .line 26
    iget v0, p0, Lcom/samsung/musicplus/glwidget/render/ShadeShader;->mHandler:I

    const-string v1, "uModelMatrix"

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/glwidget/render/ShadeShader;->mModelMatrixHandler:I

    .line 27
    iget v0, p0, Lcom/samsung/musicplus/glwidget/render/ShadeShader;->mHandler:I

    const-string v1, "uPVMatrix"

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/glwidget/render/ShadeShader;->mPVMatrixHandler:I

    .line 28
    return-void
.end method

.method private draw()V
    .locals 3

    .prologue
    .line 68
    const/4 v0, 0x5

    const/4 v1, 0x0

    const/4 v2, 0x6

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glDrawArrays(III)V

    .line 69
    return-void
.end method

.method private setModelMatrix([FI)V
    .locals 3
    .param p1, "matrix"    # [F
    .param p2, "offset"    # I

    .prologue
    .line 35
    iget v0, p0, Lcom/samsung/musicplus/glwidget/render/ShadeShader;->mModelMatrixHandler:I

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2, p1, p2}, Landroid/opengl/GLES20;->glUniformMatrix4fv(IIZ[FI)V

    .line 36
    return-void
.end method

.method private setPVMatrix([F)V
    .locals 3
    .param p1, "pVMatrix"    # [F

    .prologue
    const/4 v2, 0x0

    .line 39
    iget v0, p0, Lcom/samsung/musicplus/glwidget/render/ShadeShader;->mPVMatrixHandler:I

    const/4 v1, 0x1

    invoke-static {v0, v1, v2, p1, v2}, Landroid/opengl/GLES20;->glUniformMatrix4fv(IIZ[FI)V

    .line 40
    return-void
.end method

.method private use()V
    .locals 1

    .prologue
    .line 31
    iget v0, p0, Lcom/samsung/musicplus/glwidget/render/ShadeShader;->mHandler:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glUseProgram(I)V

    .line 32
    return-void
.end method


# virtual methods
.method public render([F[F[Lcom/samsung/musicplus/glwidget/render/Renderable;Lcom/samsung/musicplus/glwidget/render/VBO;Lcom/samsung/musicplus/glwidget/render/VBO;)V
    .locals 8
    .param p1, "PVMatrix"    # [F
    .param p2, "matrices"    # [F
    .param p3, "models"    # [Lcom/samsung/musicplus/glwidget/render/Renderable;
    .param p4, "coords"    # Lcom/samsung/musicplus/glwidget/render/VBO;
    .param p5, "colors"    # Lcom/samsung/musicplus/glwidget/render/VBO;

    .prologue
    const/16 v2, 0x1406

    const/4 v3, 0x0

    .line 43
    if-eqz p3, :cond_0

    if-nez p2, :cond_1

    .line 65
    :cond_0
    return-void

    .line 47
    :cond_1
    invoke-direct {p0}, Lcom/samsung/musicplus/glwidget/render/ShadeShader;->use()V

    .line 48
    invoke-virtual {p4}, Lcom/samsung/musicplus/glwidget/render/VBO;->bind()V

    .line 49
    iget v0, p0, Lcom/samsung/musicplus/glwidget/render/ShadeShader;->mCoordHandler:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    .line 50
    iget v0, p0, Lcom/samsung/musicplus/glwidget/render/ShadeShader;->mCoordHandler:I

    const/4 v1, 0x3

    move v4, v3

    move v5, v3

    invoke-static/range {v0 .. v5}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZII)V

    .line 51
    invoke-virtual {p5}, Lcom/samsung/musicplus/glwidget/render/VBO;->bind()V

    .line 52
    iget v0, p0, Lcom/samsung/musicplus/glwidget/render/ShadeShader;->mColorHandler:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    .line 53
    iget v0, p0, Lcom/samsung/musicplus/glwidget/render/ShadeShader;->mColorHandler:I

    const/4 v1, 0x4

    move v4, v3

    move v5, v3

    invoke-static/range {v0 .. v5}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZII)V

    .line 55
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/glwidget/render/ShadeShader;->setPVMatrix([F)V

    .line 57
    const/4 v7, 0x0

    .line 58
    .local v7, "offset":I
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    array-length v0, p3

    if-ge v6, v0, :cond_0

    .line 59
    aget-object v0, p3, v6

    invoke-interface {v0}, Lcom/samsung/musicplus/glwidget/render/Renderable;->isRenderable()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 60
    invoke-direct {p0, p2, v7}, Lcom/samsung/musicplus/glwidget/render/ShadeShader;->setModelMatrix([FI)V

    .line 61
    invoke-direct {p0}, Lcom/samsung/musicplus/glwidget/render/ShadeShader;->draw()V

    .line 63
    :cond_2
    add-int/lit8 v7, v7, 0x10

    .line 58
    add-int/lit8 v6, v6, 0x1

    goto :goto_0
.end method
