.class public Lcom/samsung/musicplus/glwidget/utils/CoordConverter$PerspectiveConverter;
.super Ljava/lang/Object;
.source "CoordConverter.java"

# interfaces
.implements Lcom/samsung/musicplus/glwidget/utils/CoordConverter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/glwidget/utils/CoordConverter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PerspectiveConverter"
.end annotation


# instance fields
.field private mCameraDistance:F

.field private mDistanceToProjection:F

.field private mFov:F


# direct methods
.method public constructor <init>(FFF)V
    .locals 0
    .param p1, "cameraDistance"    # F
    .param p2, "distanceToProjection"    # F
    .param p3, "fov"    # F

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput p1, p0, Lcom/samsung/musicplus/glwidget/utils/CoordConverter$PerspectiveConverter;->mCameraDistance:F

    .line 30
    iput p2, p0, Lcom/samsung/musicplus/glwidget/utils/CoordConverter$PerspectiveConverter;->mDistanceToProjection:F

    .line 31
    iput p3, p0, Lcom/samsung/musicplus/glwidget/utils/CoordConverter$PerspectiveConverter;->mFov:F

    .line 32
    return-void
.end method


# virtual methods
.method public getPSize(I)F
    .locals 3
    .param p1, "screenSize"    # I

    .prologue
    .line 36
    iget v0, p0, Lcom/samsung/musicplus/glwidget/utils/CoordConverter$PerspectiveConverter;->mCameraDistance:F

    iget v1, p0, Lcom/samsung/musicplus/glwidget/utils/CoordConverter$PerspectiveConverter;->mDistanceToProjection:F

    iget v2, p0, Lcom/samsung/musicplus/glwidget/utils/CoordConverter$PerspectiveConverter;->mFov:F

    invoke-static {p1, v0, v1, v2}, Lcom/samsung/musicplus/glwidget/utils/FrustumCamera;->screenToPhysical(IFFF)F

    move-result v0

    return v0
.end method

.method public getSSize(F)I
    .locals 3
    .param p1, "pSize"    # F

    .prologue
    .line 42
    iget v0, p0, Lcom/samsung/musicplus/glwidget/utils/CoordConverter$PerspectiveConverter;->mCameraDistance:F

    iget v1, p0, Lcom/samsung/musicplus/glwidget/utils/CoordConverter$PerspectiveConverter;->mDistanceToProjection:F

    iget v2, p0, Lcom/samsung/musicplus/glwidget/utils/CoordConverter$PerspectiveConverter;->mFov:F

    invoke-static {p1, v0, v1, v2}, Lcom/samsung/musicplus/glwidget/utils/FrustumCamera;->physicalToScreen(FFFF)I

    move-result v0

    return v0
.end method
