.class Lcom/samsung/musicplus/glwidget/GLTextureViewBase;
.super Landroid/view/TextureView;
.source "GLTextureViewBase.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/glwidget/GLTextureViewBase$GLException;,
        Lcom/samsung/musicplus/glwidget/GLTextureViewBase$LogWriter;
    }
.end annotation


# static fields
.field public static final DEBUG_CHECK_GL_ERROR:I = 0x1

.field public static final DEBUG_LOG_GL_CALLS:I = 0x2

.field static final EGL_CONTEXT_CLIENT_VERSION:I = 0x3098

.field static final EGL_OPENGL_ES2_BIT:I = 0x4

.field private static LOG_TAG:Ljava/lang/String; = null

.field private static final MSG_REQUEST_RENDER:I = 0x3

.field private static final MSG_TEXTURE_AVAILABLE:I = 0x2

.field private static final MSG_TEXTURE_DESTROYED:I = 0x4

.field private static final MSG_TEXTURE_SIZE_CHANGED:I


# instance fields
.field private mDebugFlags:I

.field private volatile mEgl:Ljavax/microedition/khronos/egl/EGL10;

.field private volatile mEglConfig:Ljavax/microedition/khronos/egl/EGLConfig;

.field private volatile mEglContext:Ljavax/microedition/khronos/egl/EGLContext;

.field private volatile mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

.field private volatile mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

.field private mGLWrapper:Landroid/opengl/GLSurfaceView$GLWrapper;

.field private mGl:Ljavax/microedition/khronos/opengles/GL10;

.field private mHandler:Landroid/os/Handler;

.field private mHandlerCallback:Landroid/os/Handler$Callback;

.field private mHeight:I

.field private mListener:Landroid/view/TextureView$SurfaceTextureListener;

.field private mRenderer:Landroid/opengl/GLSurfaceView$Renderer;

.field private volatile mRenderingThread:Landroid/os/HandlerThread;

.field private mSurface:Landroid/graphics/SurfaceTexture;

.field private volatile mSurfaceCreated:Z

.field private volatile mSurfaceDestroyed:Z

.field private mThreadName:Ljava/lang/String;

.field private mWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 66
    const-class v0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "threadName"    # Ljava/lang/String;

    .prologue
    .line 75
    invoke-direct {p0, p1, p2}, Landroid/view/TextureView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 141
    new-instance v0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase$1;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/glwidget/GLTextureViewBase$1;-><init>(Lcom/samsung/musicplus/glwidget/GLTextureViewBase;)V

    iput-object v0, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mListener:Landroid/view/TextureView$SurfaceTextureListener;

    .line 211
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mSurfaceCreated:Z

    .line 487
    new-instance v0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase$2;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/glwidget/GLTextureViewBase$2;-><init>(Lcom/samsung/musicplus/glwidget/GLTextureViewBase;)V

    iput-object v0, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mHandlerCallback:Landroid/os/Handler$Callback;

    .line 76
    iput-object p3, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mThreadName:Ljava/lang/String;

    .line 77
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->startRenderingThread()V

    .line 78
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "threadName"    # Ljava/lang/String;

    .prologue
    .line 69
    invoke-direct {p0, p1}, Landroid/view/TextureView;-><init>(Landroid/content/Context;)V

    .line 141
    new-instance v0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase$1;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/glwidget/GLTextureViewBase$1;-><init>(Lcom/samsung/musicplus/glwidget/GLTextureViewBase;)V

    iput-object v0, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mListener:Landroid/view/TextureView$SurfaceTextureListener;

    .line 211
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mSurfaceCreated:Z

    .line 487
    new-instance v0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase$2;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/glwidget/GLTextureViewBase$2;-><init>(Lcom/samsung/musicplus/glwidget/GLTextureViewBase;)V

    iput-object v0, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mHandlerCallback:Landroid/os/Handler$Callback;

    .line 70
    iput-object p2, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mThreadName:Ljava/lang/String;

    .line 71
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->startRenderingThread()V

    .line 72
    return-void
.end method

.method static synthetic access$002(Lcom/samsung/musicplus/glwidget/GLTextureViewBase;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/glwidget/GLTextureViewBase;
    .param p1, "x1"    # Z

    .prologue
    .line 56
    iput-boolean p1, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mSurfaceDestroyed:Z

    return p1
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    sget-object v0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->LOG_TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/musicplus/glwidget/GLTextureViewBase;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/glwidget/GLTextureViewBase;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$302(Lcom/samsung/musicplus/glwidget/GLTextureViewBase;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/glwidget/GLTextureViewBase;
    .param p1, "x1"    # Z

    .prologue
    .line 56
    iput-boolean p1, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mSurfaceCreated:Z

    return p1
.end method

.method static synthetic access$500(Lcom/samsung/musicplus/glwidget/GLTextureViewBase;ZLandroid/graphics/SurfaceTexture;II)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/glwidget/GLTextureViewBase;
    .param p1, "x1"    # Z
    .param p2, "x2"    # Landroid/graphics/SurfaceTexture;
    .param p3, "x3"    # I
    .param p4, "x4"    # I

    .prologue
    .line 56
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->handleTextureAvailableSizeChanged(ZLandroid/graphics/SurfaceTexture;II)Z

    move-result v0

    return v0
.end method

.method static synthetic access$600(Lcom/samsung/musicplus/glwidget/GLTextureViewBase;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/glwidget/GLTextureViewBase;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->handleRequestRender()Z

    move-result v0

    return v0
.end method

.method static synthetic access$700(Lcom/samsung/musicplus/glwidget/GLTextureViewBase;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/glwidget/GLTextureViewBase;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->handleTextureDestroyed()Z

    move-result v0

    return v0
.end method

.method private checkGLCurrent()V
    .locals 5

    .prologue
    .line 371
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mEglContext:Ljavax/microedition/khronos/egl/EGLContext;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    if-nez v0, :cond_1

    .line 372
    :cond_0
    const-string v0, "invalid egl/eglContext/eglSurface"

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->failRuntimeException(Ljava/lang/String;)V

    .line 374
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mEglContext:Ljavax/microedition/khronos/egl/EGLContext;

    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    invoke-interface {v1}, Ljavax/microedition/khronos/egl/EGL10;->eglGetCurrentContext()Ljavax/microedition/khronos/egl/EGLContext;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    const/16 v2, 0x3059

    invoke-interface {v1, v2}, Ljavax/microedition/khronos/egl/EGL10;->eglGetCurrentSurface(I)Ljavax/microedition/khronos/egl/EGLSurface;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 376
    :cond_2
    sget-object v0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->LOG_TAG:Ljava/lang/String;

    const-string v1, "switching GLContext back to ours"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 377
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v2, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    iget-object v3, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    iget-object v4, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mEglContext:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-interface {v0, v1, v2, v3, v4}, Ljavax/microedition/khronos/egl/EGL10;->eglMakeCurrent(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLContext;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 378
    const-string v0, "eglMakeCurrent failed"

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->failRuntimeException(Ljava/lang/String;)V

    .line 383
    :cond_3
    :goto_0
    return-void

    .line 380
    :cond_4
    sget-object v0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->LOG_TAG:Ljava/lang/String;

    const-string v1, "switching GLContext back succeded"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private chooseEglConfig()Ljavax/microedition/khronos/egl/EGLConfig;
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v4, 0x1

    .line 328
    new-array v5, v4, [I

    .line 329
    .local v5, "configsCount":[I
    new-array v3, v4, [Ljavax/microedition/khronos/egl/EGLConfig;

    .line 330
    .local v3, "configs":[Ljavax/microedition/khronos/egl/EGLConfig;
    invoke-direct {p0}, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->getConfig()[I

    move-result-object v2

    .line 331
    .local v2, "configSpec":[I
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    invoke-interface/range {v0 .. v5}, Ljavax/microedition/khronos/egl/EGL10;->eglChooseConfig(Ljavax/microedition/khronos/egl/EGLDisplay;[I[Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 332
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "eglChooseConfig failed "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v4, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    invoke-interface {v4}, Ljavax/microedition/khronos/egl/EGL10;->eglGetError()I

    move-result v4

    invoke-static {v4}, Landroid/opengl/GLUtils;->getEGLErrorString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 334
    :cond_0
    aget v0, v5, v6

    if-lez v0, :cond_1

    .line 335
    aget-object v0, v3, v6

    .line 337
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private createContext(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;)Ljavax/microedition/khronos/egl/EGLContext;
    .locals 2
    .param p1, "egl"    # Ljavax/microedition/khronos/egl/EGL10;
    .param p2, "eglDisplay"    # Ljavax/microedition/khronos/egl/EGLDisplay;
    .param p3, "eglConfig"    # Ljavax/microedition/khronos/egl/EGLConfig;

    .prologue
    .line 321
    const/4 v1, 0x3

    new-array v0, v1, [I

    fill-array-data v0, :array_0

    .line 324
    .local v0, "attrib_list":[I
    sget-object v1, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_CONTEXT:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-interface {p1, p2, p3, v1, v0}, Ljavax/microedition/khronos/egl/EGL10;->eglCreateContext(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;Ljavax/microedition/khronos/egl/EGLContext;[I)Ljavax/microedition/khronos/egl/EGLContext;

    move-result-object v1

    return-object v1

    .line 321
    nop

    :array_0
    .array-data 4
        0x3098
        0x2
        0x3038
    .end array-data
.end method

.method private failRuntimeException(Ljava/lang/String;)V
    .locals 6
    .param p1, "error"    # Ljava/lang/String;

    .prologue
    .line 313
    const/4 v0, -0x1

    .line 314
    .local v0, "code":I
    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    if-eqz v1, :cond_0

    .line 315
    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    invoke-interface {v1}, Ljavax/microedition/khronos/egl/EGL10;->eglGetError()I

    move-result v0

    .line 317
    :cond_0
    new-instance v1, Lcom/samsung/musicplus/glwidget/GLTextureViewBase$GLException;

    const-string v2, "%s code: %d msg: %s"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    const/4 v4, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    invoke-static {v0}, Landroid/opengl/GLUtils;->getEGLErrorString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/samsung/musicplus/glwidget/GLTextureViewBase$GLException;-><init>(ILjava/lang/String;)V

    throw v1
.end method

.method private getConfig()[I
    .locals 1

    .prologue
    .line 341
    const/16 v0, 0xf

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    return-object v0

    :array_0
    .array-data 4
        0x3040
        0x4
        0x3024
        0x8
        0x3023
        0x8
        0x3022
        0x8
        0x3021
        0x8
        0x3025
        0x10
        0x3026
        0x0
        0x3038
    .end array-data
.end method

.method private handleRequestRender()Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 349
    iget-boolean v1, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mSurfaceDestroyed:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mSurfaceCreated:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mRenderer:Landroid/opengl/GLSurfaceView$Renderer;

    if-eqz v1, :cond_0

    .line 351
    :try_start_0
    invoke-direct {p0}, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->checkGLCurrent()V

    .line 352
    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mRenderer:Landroid/opengl/GLSurfaceView$Renderer;

    iget-object v2, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mGl:Ljavax/microedition/khronos/opengles/GL10;

    invoke-interface {v1, v2}, Landroid/opengl/GLSurfaceView$Renderer;->onDrawFrame(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 353
    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v2, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v3, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    invoke-interface {v1, v2, v3}, Ljavax/microedition/khronos/egl/EGL10;->eglSwapBuffers(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 355
    iget-boolean v1, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mSurfaceDestroyed:Z

    if-eqz v1, :cond_1

    .line 367
    :cond_0
    :goto_0
    return v4

    .line 358
    :cond_1
    const-string v1, "Cannot swap buffers"

    invoke-direct {p0, v1}, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->failRuntimeException(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/samsung/musicplus/glwidget/GLTextureViewBase$GLException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 360
    :catch_0
    move-exception v0

    .line 361
    .local v0, "e":Lcom/samsung/musicplus/glwidget/GLTextureViewBase$GLException;
    iget-boolean v1, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mSurfaceDestroyed:Z

    if-nez v1, :cond_0

    .line 364
    throw v0
.end method

.method private handleTextureAvailableSizeChanged(ZLandroid/graphics/SurfaceTexture;II)Z
    .locals 8
    .param p1, "sizeChangedEvent"    # Z
    .param p2, "texture"    # Landroid/graphics/SurfaceTexture;
    .param p3, "width"    # I
    .param p4, "height"    # I

    .prologue
    const/4 v2, 0x0

    const/4 v7, 0x0

    const/4 v3, 0x1

    .line 387
    sget-object v4, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->LOG_TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "handleTextureAvailableSizeChanged sizeChangedEvent: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " width: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " height: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 389
    iget-boolean v4, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mSurfaceDestroyed:Z

    if-eqz v4, :cond_1

    .line 390
    sget-object v2, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->LOG_TAG:Ljava/lang/String;

    const-string v4, "surface already destroyed 1"

    invoke-static {v2, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 457
    :cond_0
    :goto_0
    return v3

    .line 393
    :cond_1
    iget-object v4, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mSurface:Landroid/graphics/SurfaceTexture;

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mSurface:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v4, p2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 394
    sget-object v4, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->LOG_TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "wrong surface come to sizeChangedEvent: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " mSurface: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mSurface:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " texture: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/musicplus/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 396
    :cond_2
    if-nez p1, :cond_3

    .line 398
    :try_start_0
    invoke-direct {p0, p2}, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->initGL(Landroid/graphics/SurfaceTexture;)Ljavax/microedition/khronos/opengles/GL10;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mGl:Ljavax/microedition/khronos/opengles/GL10;
    :try_end_0
    .catch Lcom/samsung/musicplus/glwidget/GLTextureViewBase$GLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 409
    :cond_3
    iget-boolean v4, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mSurfaceDestroyed:Z

    if-eqz v4, :cond_5

    .line 410
    sget-object v2, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->LOG_TAG:Ljava/lang/String;

    const-string v4, "surface already destroyed 2"

    invoke-static {v2, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 399
    :catch_0
    move-exception v0

    .line 401
    .local v0, "e":Lcom/samsung/musicplus/glwidget/GLTextureViewBase$GLException;
    iget-boolean v2, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mSurfaceDestroyed:Z

    if-eqz v2, :cond_4

    .line 402
    sget-object v2, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->LOG_TAG:Ljava/lang/String;

    const-string v4, "surface already destroyed 5"

    invoke-static {v2, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 405
    :cond_4
    throw v0

    .line 413
    .end local v0    # "e":Lcom/samsung/musicplus/glwidget/GLTextureViewBase$GLException;
    :cond_5
    iget-object v4, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mGl:Ljavax/microedition/khronos/opengles/GL10;

    if-nez v4, :cond_6

    .line 414
    sget-object v2, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->LOG_TAG:Ljava/lang/String;

    const-string v4, "mGl == null"

    invoke-static {v2, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 417
    :cond_6
    invoke-direct {p0}, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->checkGLCurrent()V

    .line 419
    invoke-static {v2, v2, p3, p4}, Landroid/opengl/GLES20;->glViewport(IIII)V

    .line 420
    invoke-static {v7, v7, v7, v7}, Landroid/opengl/GLES20;->glClearColor(FFFF)V

    .line 421
    const/16 v4, 0x4000

    invoke-static {v4}, Landroid/opengl/GLES20;->glClear(I)V

    .line 422
    iget-object v4, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    if-eqz v4, :cond_c

    iget-object v4, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v5, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v6, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    invoke-interface {v4, v5, v6}, Ljavax/microedition/khronos/egl/EGL10;->eglSwapBuffers(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;)Z

    move-result v4

    if-nez v4, :cond_c

    .line 423
    iget-boolean v4, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mSurfaceDestroyed:Z

    if-eqz v4, :cond_7

    .line 424
    sget-object v2, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->LOG_TAG:Ljava/lang/String;

    const-string v4, "surface already destroyed 3"

    invoke-static {v2, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 428
    :cond_7
    :try_start_1
    const-string v4, "Cannot swap buffers"

    invoke-direct {p0, v4}, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->failRuntimeException(Ljava/lang/String;)V
    :try_end_1
    .catch Lcom/samsung/musicplus/glwidget/GLTextureViewBase$GLException; {:try_start_1 .. :try_end_1} :catch_1

    .line 444
    :cond_8
    const/4 v1, 0x0

    .line 445
    .local v1, "sizeChanged":Z
    iget-boolean v4, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mSurfaceCreated:Z

    if-nez v4, :cond_9

    .line 446
    iput-boolean v3, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mSurfaceCreated:Z

    .line 447
    iget-object v4, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mRenderer:Landroid/opengl/GLSurfaceView$Renderer;

    iget-object v5, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mGl:Ljavax/microedition/khronos/opengles/GL10;

    iget-object v6, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mEglConfig:Ljavax/microedition/khronos/egl/EGLConfig;

    invoke-interface {v4, v5, v6}, Landroid/opengl/GLSurfaceView$Renderer;->onSurfaceCreated(Ljavax/microedition/khronos/opengles/GL10;Ljavax/microedition/khronos/egl/EGLConfig;)V

    .line 449
    const/4 v1, 0x1

    .line 451
    :cond_9
    if-nez v1, :cond_a

    iget v4, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mWidth:I

    if-ne p3, v4, :cond_a

    iget v4, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mHeight:I

    if-eq p4, v4, :cond_d

    :cond_a
    move v1, v3

    .line 452
    :goto_1
    if-eqz v1, :cond_0

    .line 453
    iget-object v2, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mRenderer:Landroid/opengl/GLSurfaceView$Renderer;

    iget-object v4, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mGl:Ljavax/microedition/khronos/opengles/GL10;

    invoke-interface {v2, v4, p3, p4}, Landroid/opengl/GLSurfaceView$Renderer;->onSurfaceChanged(Ljavax/microedition/khronos/opengles/GL10;II)V

    .line 454
    iput p3, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mWidth:I

    .line 455
    iput p4, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mHeight:I

    goto/16 :goto_0

    .line 429
    .end local v1    # "sizeChanged":Z
    :catch_1
    move-exception v0

    .line 431
    .restart local v0    # "e":Lcom/samsung/musicplus/glwidget/GLTextureViewBase$GLException;
    iget v2, v0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase$GLException;->code:I

    const/16 v4, 0x300d

    if-ne v2, v4, :cond_b

    .line 432
    const-wide/16 v4, 0x32

    invoke-static {v4, v5}, Landroid/os/SystemClock;->sleep(J)V

    .line 433
    iget-boolean v2, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mSurfaceDestroyed:Z

    if-nez v2, :cond_0

    .line 437
    :cond_b
    throw v0

    .line 439
    .end local v0    # "e":Lcom/samsung/musicplus/glwidget/GLTextureViewBase$GLException;
    :cond_c
    iget-object v4, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    if-nez v4, :cond_8

    .line 440
    sget-object v2, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->LOG_TAG:Ljava/lang/String;

    const-string v4, "mEgl == null"

    invoke-static {v2, v4}, Lcom/samsung/musicplus/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .restart local v1    # "sizeChanged":Z
    :cond_d
    move v1, v2

    .line 451
    goto :goto_1
.end method

.method private declared-synchronized handleTextureDestroyed()Z
    .locals 6

    .prologue
    .line 461
    monitor-enter p0

    const/4 v1, 0x0

    :try_start_0
    iput-object v1, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mSurface:Landroid/graphics/SurfaceTexture;

    .line 462
    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 463
    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mEgl:Ljavax/microedition/khronos/egl/EGL10;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v1, :cond_2

    .line 465
    :try_start_1
    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v2, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    sget-object v3, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    sget-object v4, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    sget-object v5, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_CONTEXT:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-interface {v1, v2, v3, v4, v5}, Ljavax/microedition/khronos/egl/EGL10;->eglMakeCurrent(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLContext;)Z

    .line 468
    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mEglContext:Ljavax/microedition/khronos/egl/EGLContext;

    if-eqz v1, :cond_0

    .line 469
    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v2, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v3, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mEglContext:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-interface {v1, v2, v3}, Ljavax/microedition/khronos/egl/EGL10;->eglDestroyContext(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLContext;)Z

    .line 471
    :cond_0
    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    if-eqz v1, :cond_1

    .line 472
    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v2, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v3, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    invoke-interface {v1, v2, v3}, Ljavax/microedition/khronos/egl/EGL10;->eglDestroySurface(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;)Z

    .line 474
    :cond_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    .line 475
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mEglContext:Ljavax/microedition/khronos/egl/EGLContext;

    .line 476
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    .line 477
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mEglContext:Ljavax/microedition/khronos/egl/EGLContext;

    .line 478
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mEgl:Ljavax/microedition/khronos/egl/EGL10;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 483
    :cond_2
    :goto_0
    :try_start_2
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->stopRenderingThread()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 484
    const/4 v1, 0x1

    monitor-exit p0

    return v1

    .line 479
    :catch_0
    move-exception v0

    .line 480
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    :try_start_3
    sget-object v1, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->LOG_TAG:Ljava/lang/String;

    const-string v2, "handleTextureDestoyed"

    invoke-static {v1, v2, v0}, Lcom/samsung/musicplus/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 461
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method private declared-synchronized initGL(Landroid/graphics/SurfaceTexture;)Ljavax/microedition/khronos/opengles/GL10;
    .locals 9
    .param p1, "texture"    # Landroid/graphics/SurfaceTexture;

    .prologue
    const/4 v1, 0x0

    .line 256
    monitor-enter p0

    :try_start_0
    sget-object v4, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->LOG_TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "initGL texture: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 257
    iget-boolean v4, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mSurfaceDestroyed:Z

    if-eqz v4, :cond_0

    .line 258
    sget-object v4, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->LOG_TAG:Ljava/lang/String;

    const-string v5, "surface already destroyed 4"

    invoke-static {v4, v5}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 309
    :goto_0
    monitor-exit p0

    return-object v1

    .line 261
    :cond_0
    :try_start_1
    iput-object p1, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mSurface:Landroid/graphics/SurfaceTexture;

    .line 263
    invoke-static {}, Ljavax/microedition/khronos/egl/EGLContext;->getEGL()Ljavax/microedition/khronos/egl/EGL;

    move-result-object v4

    check-cast v4, Ljavax/microedition/khronos/egl/EGL10;

    iput-object v4, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    .line 265
    iget-object v4, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    sget-object v5, Ljavax/microedition/khronos/egl/EGL10;->EGL_DEFAULT_DISPLAY:Ljava/lang/Object;

    invoke-interface {v4, v5}, Ljavax/microedition/khronos/egl/EGL10;->eglGetDisplay(Ljava/lang/Object;)Ljavax/microedition/khronos/egl/EGLDisplay;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    .line 266
    iget-object v4, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    sget-object v5, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_DISPLAY:Ljavax/microedition/khronos/egl/EGLDisplay;

    if-ne v4, v5, :cond_1

    .line 267
    const-string v4, "eglGetDisplay failed"

    invoke-direct {p0, v4}, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->failRuntimeException(Ljava/lang/String;)V

    .line 270
    :cond_1
    const/4 v4, 0x2

    new-array v3, v4, [I

    .line 271
    .local v3, "version":[I
    iget-object v4, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v5, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    invoke-interface {v4, v5, v3}, Ljavax/microedition/khronos/egl/EGL10;->eglInitialize(Ljavax/microedition/khronos/egl/EGLDisplay;[I)Z

    move-result v4

    if-nez v4, :cond_2

    .line 272
    const-string v4, "eglInitialize failed"

    invoke-direct {p0, v4}, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->failRuntimeException(Ljava/lang/String;)V

    .line 275
    :cond_2
    invoke-direct {p0}, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->chooseEglConfig()Ljavax/microedition/khronos/egl/EGLConfig;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mEglConfig:Ljavax/microedition/khronos/egl/EGLConfig;

    .line 276
    iget-object v4, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mEglConfig:Ljavax/microedition/khronos/egl/EGLConfig;

    if-nez v4, :cond_3

    .line 277
    const-string v4, "eglConfig not initialized"

    invoke-direct {p0, v4}, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->failRuntimeException(Ljava/lang/String;)V

    .line 280
    :cond_3
    iget-object v4, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v5, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v6, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mEglConfig:Ljavax/microedition/khronos/egl/EGLConfig;

    invoke-direct {p0, v4, v5, v6}, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->createContext(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;)Ljavax/microedition/khronos/egl/EGLContext;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mEglContext:Ljavax/microedition/khronos/egl/EGLContext;

    .line 282
    iget-object v4, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v5, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v6, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mEglConfig:Ljavax/microedition/khronos/egl/EGLConfig;

    iget-object v7, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mSurface:Landroid/graphics/SurfaceTexture;

    const/4 v8, 0x0

    invoke-interface {v4, v5, v6, v7, v8}, Ljavax/microedition/khronos/egl/EGL10;->eglCreateWindowSurface(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;Ljava/lang/Object;[I)Ljavax/microedition/khronos/egl/EGLSurface;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    .line 284
    iget-object v4, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    sget-object v5, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    if-ne v4, v5, :cond_5

    .line 285
    :cond_4
    const-string v4, "createWindowSurface failed"

    invoke-direct {p0, v4}, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->failRuntimeException(Ljava/lang/String;)V

    .line 288
    :cond_5
    iget-object v4, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mEgl:Ljavax/microedition/khronos/egl/EGL10;

    iget-object v5, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mEglDisplay:Ljavax/microedition/khronos/egl/EGLDisplay;

    iget-object v6, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    iget-object v7, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mEglSurface:Ljavax/microedition/khronos/egl/EGLSurface;

    iget-object v8, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mEglContext:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-interface {v4, v5, v6, v7, v8}, Ljavax/microedition/khronos/egl/EGL10;->eglMakeCurrent(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLContext;)Z

    move-result v4

    if-nez v4, :cond_6

    .line 289
    const-string v4, "eglMakeCurrent failed"

    invoke-direct {p0, v4}, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->failRuntimeException(Ljava/lang/String;)V

    .line 292
    :cond_6
    iget-object v4, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mEglContext:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-virtual {v4}, Ljavax/microedition/khronos/egl/EGLContext;->getGL()Ljavax/microedition/khronos/opengles/GL;

    move-result-object v1

    .line 293
    .local v1, "gl":Ljavax/microedition/khronos/opengles/GL;
    iget-object v4, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mGLWrapper:Landroid/opengl/GLSurfaceView$GLWrapper;

    if-eqz v4, :cond_7

    .line 294
    iget-object v4, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mGLWrapper:Landroid/opengl/GLSurfaceView$GLWrapper;

    invoke-interface {v4, v1}, Landroid/opengl/GLSurfaceView$GLWrapper;->wrap(Ljavax/microedition/khronos/opengles/GL;)Ljavax/microedition/khronos/opengles/GL;

    move-result-object v1

    .line 297
    :cond_7
    iget v4, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mDebugFlags:I

    and-int/lit8 v4, v4, 0x3

    if-eqz v4, :cond_a

    .line 298
    const/4 v0, 0x0

    .line 299
    .local v0, "configFlags":I
    const/4 v2, 0x0

    .line 300
    .local v2, "log":Ljava/io/Writer;
    iget v4, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mDebugFlags:I

    and-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_8

    .line 301
    or-int/lit8 v0, v0, 0x1

    .line 303
    :cond_8
    iget v4, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mDebugFlags:I

    and-int/lit8 v4, v4, 0x2

    if-eqz v4, :cond_9

    .line 304
    new-instance v2, Lcom/samsung/musicplus/glwidget/GLTextureViewBase$LogWriter;

    .end local v2    # "log":Ljava/io/Writer;
    const/4 v4, 0x0

    invoke-direct {v2, v4}, Lcom/samsung/musicplus/glwidget/GLTextureViewBase$LogWriter;-><init>(Lcom/samsung/musicplus/glwidget/GLTextureViewBase$1;)V

    .line 306
    .restart local v2    # "log":Ljava/io/Writer;
    :cond_9
    invoke-static {v1, v0, v2}, Landroid/opengl/GLDebugHelper;->wrap(Ljavax/microedition/khronos/opengles/GL;ILjava/io/Writer;)Ljavax/microedition/khronos/opengles/GL;

    move-result-object v1

    .line 308
    .end local v0    # "configFlags":I
    .end local v2    # "log":Ljava/io/Writer;
    :cond_a
    sget-object v4, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->LOG_TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "initGL end texture: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 309
    check-cast v1, Ljavax/microedition/khronos/opengles/GL10;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    .line 256
    .end local v1    # "gl":Ljavax/microedition/khronos/opengles/GL;
    .end local v3    # "version":[I
    :catchall_0
    move-exception v4

    monitor-exit p0

    throw v4
.end method


# virtual methods
.method public getDebugFlags()I
    .locals 1

    .prologue
    .line 99
    iget v0, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mDebugFlags:I

    return v0
.end method

.method public requestRender()V
    .locals 2

    .prologue
    const/4 v1, 0x3

    .line 124
    iget-boolean v0, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mSurfaceDestroyed:Z

    if-eqz v0, :cond_0

    .line 125
    sget-object v0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->LOG_TAG:Ljava/lang/String;

    const-string v1, "requestRender surface already destroyed"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    :goto_0
    return-void

    .line 129
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 130
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method

.method public setDebugFlags(I)V
    .locals 0
    .param p1, "debugFlags"    # I

    .prologue
    .line 90
    iput p1, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mDebugFlags:I

    .line 91
    return-void
.end method

.method public setGLWrapper(Landroid/opengl/GLSurfaceView$GLWrapper;)V
    .locals 0
    .param p1, "glWrapper"    # Landroid/opengl/GLSurfaceView$GLWrapper;

    .prologue
    .line 116
    iput-object p1, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mGLWrapper:Landroid/opengl/GLSurfaceView$GLWrapper;

    .line 117
    return-void
.end method

.method public setRenderer(Landroid/opengl/GLSurfaceView$Renderer;)V
    .locals 0
    .param p1, "renderer"    # Landroid/opengl/GLSurfaceView$Renderer;

    .prologue
    .line 120
    iput-object p1, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mRenderer:Landroid/opengl/GLSurfaceView$Renderer;

    .line 121
    return-void
.end method

.method protected startRenderingThread()V
    .locals 3

    .prologue
    .line 185
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mListener:Landroid/view/TextureView$SurfaceTextureListener;

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    .line 186
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mRenderingThread:Landroid/os/HandlerThread;

    if-nez v0, :cond_0

    .line 187
    sget-object v0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->LOG_TAG:Ljava/lang/String;

    const-string v1, "startRenderingThread"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    new-instance v0, Landroid/os/HandlerThread;

    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mThreadName:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mRenderingThread:Landroid/os/HandlerThread;

    .line 189
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mRenderingThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 190
    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mRenderingThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mHandlerCallback:Landroid/os/Handler$Callback;

    invoke-direct {v0, v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mHandler:Landroid/os/Handler;

    .line 191
    sget-object v0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "startRenderingThread done "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mRenderingThread:Landroid/os/HandlerThread;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mRenderingThread:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    :cond_0
    return-void
.end method

.method protected stopRenderingThread()V
    .locals 4

    .prologue
    .line 196
    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mRenderingThread:Landroid/os/HandlerThread;

    if-eqz v1, :cond_0

    .line 197
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mRenderingThread:Landroid/os/HandlerThread;

    .line 198
    .local v0, "cur":Landroid/os/HandlerThread;
    sget-object v1, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "stopRenderingThread "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mRenderingThread:Landroid/os/HandlerThread;

    .line 200
    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    .line 201
    sget-object v1, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->LOG_TAG:Ljava/lang/String;

    const-string v2, "stopRenderingThread done"

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    .end local v0    # "cur":Landroid/os/HandlerThread;
    :cond_0
    return-void
.end method
