.class Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout$Config;
.super Ljava/lang/Object;
.source "FlatResizeLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Config"
.end annotation


# static fields
.field public static final DIMENSION_UNSPECIFIED:I = -0x1


# instance fields
.field private mAlbOpacity:F

.field private mAlbSize:I

.field private mAlbSpacing:I

.field private mAlbSpacingAdditional:I

.field private mBackgroundColor:I

.field private mHeight:I

.field private mScrollingFriction:F

.field private mSelAlbSize:I

.field private mSelBorder:I

.field private mShadowBitmap:I

.field private mTextLayout:I


# direct methods
.method public constructor <init>(Landroid/content/res/TypedArray;)V
    .locals 4
    .param p1, "a"    # Landroid/content/res/TypedArray;

    .prologue
    .line 566
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 568
    const/4 v0, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x1

    const/high16 v3, 0x3f800000    # 1.0f

    :try_start_0
    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/content/res/TypedArray;->getFraction(IIIF)F

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout$Config;->mAlbOpacity:F

    .line 570
    const/4 v0, 0x1

    const/16 v1, 0xb4

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout$Config;->mAlbSize:I

    .line 572
    const/4 v0, 0x2

    const/16 v1, 0x1a

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout$Config;->mAlbSpacing:I

    .line 574
    const/4 v0, 0x3

    iget v1, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout$Config;->mAlbSpacing:I

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout$Config;->mAlbSpacingAdditional:I

    .line 576
    const/4 v0, 0x4

    const/high16 v1, -0x1000000

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout$Config;->mBackgroundColor:I

    .line 578
    const/4 v0, 0x6

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout$Config;->mSelAlbSize:I

    .line 581
    const/4 v0, 0x5

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout$Config;->mSelBorder:I

    .line 584
    const/4 v0, 0x7

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout$Config;->mTextLayout:I

    .line 586
    const/16 v0, 0x8

    invoke-static {}, Landroid/view/ViewConfiguration;->getScrollFriction()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout$Config;->mScrollingFriction:F

    .line 589
    const/16 v0, 0x9

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout$Config;->mShadowBitmap:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 591
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    .line 593
    return-void

    .line 591
    :catchall_0
    move-exception v0

    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    throw v0
.end method


# virtual methods
.method public getAlbOpacity()F
    .locals 1

    .prologue
    .line 543
    iget v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout$Config;->mAlbOpacity:F

    return v0
.end method

.method public getAlbSize()I
    .locals 1

    .prologue
    .line 527
    iget v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout$Config;->mAlbSize:I

    return v0
.end method

.method public getAlbSpacing()I
    .locals 1

    .prologue
    .line 531
    iget v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout$Config;->mAlbSpacing:I

    return v0
.end method

.method public getAlbSpacingAdditinal()I
    .locals 1

    .prologue
    .line 535
    iget v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout$Config;->mAlbSpacingAdditional:I

    return v0
.end method

.method public getBackgroundColor()I
    .locals 1

    .prologue
    .line 539
    iget v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout$Config;->mBackgroundColor:I

    return v0
.end method

.method public getScrollingFriction()F
    .locals 1

    .prologue
    .line 559
    iget v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout$Config;->mScrollingFriction:F

    return v0
.end method

.method public getSelAlbSize()I
    .locals 2

    .prologue
    .line 547
    iget v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout$Config;->mSelBorder:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 548
    iget v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout$Config;->mSelAlbSize:I

    .line 550
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout$Config;->mHeight:I

    iget v1, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout$Config;->mSelBorder:I

    mul-int/lit8 v1, v1, 0x2

    sub-int/2addr v0, v1

    goto :goto_0
.end method

.method public getShadowBitmap()I
    .locals 1

    .prologue
    .line 563
    iget v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout$Config;->mShadowBitmap:I

    return v0
.end method

.method public getTextLayout()I
    .locals 1

    .prologue
    .line 555
    iget v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout$Config;->mTextLayout:I

    return v0
.end method

.method public setHeight(I)V
    .locals 0
    .param p1, "height"    # I

    .prologue
    .line 522
    iput p1, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout$Config;->mHeight:I

    .line 523
    return-void
.end method
