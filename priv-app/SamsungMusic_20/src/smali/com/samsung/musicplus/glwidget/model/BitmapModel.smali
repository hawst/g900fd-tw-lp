.class public Lcom/samsung/musicplus/glwidget/model/BitmapModel;
.super Lcom/samsung/musicplus/glwidget/model/AnimationModel;
.source "BitmapModel.java"

# interfaces
.implements Lcom/samsung/musicplus/glwidget/render/RenderableBitmap;


# instance fields
.field private mAlpha:F

.field private mBitmap:Landroid/graphics/Bitmap;

.field private mUpdated:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/samsung/musicplus/glwidget/model/AnimationModel;-><init>()V

    .line 19
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/samsung/musicplus/glwidget/model/BitmapModel;->mAlpha:F

    return-void
.end method


# virtual methods
.method public getAlpha()F
    .locals 1

    .prologue
    .line 75
    iget v0, p0, Lcom/samsung/musicplus/glwidget/model/BitmapModel;->mAlpha:F

    return v0
.end method

.method public getRenderableBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/model/BitmapModel;->mBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public isRenderable()Z
    .locals 1

    .prologue
    .line 67
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/model/BitmapModel;->isVisible()Z

    move-result v0

    return v0
.end method

.method public resetUpdated()V
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/glwidget/model/BitmapModel;->mUpdated:Z

    .line 52
    return-void
.end method

.method public setAlpha(F)V
    .locals 0
    .param p1, "alpha"    # F

    .prologue
    .line 83
    iput p1, p0, Lcom/samsung/musicplus/glwidget/model/BitmapModel;->mAlpha:F

    .line 84
    return-void
.end method

.method public setBitmap(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/model/BitmapModel;->mBitmap:Landroid/graphics/Bitmap;

    if-eq p1, v0, :cond_1

    .line 28
    iput-object p1, p0, Lcom/samsung/musicplus/glwidget/model/BitmapModel;->mBitmap:Landroid/graphics/Bitmap;

    .line 33
    iget-boolean v0, p0, Lcom/samsung/musicplus/glwidget/model/BitmapModel;->mUpdated:Z

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isMutable()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/samsung/musicplus/glwidget/model/BitmapModel;->mUpdated:Z

    .line 35
    :cond_1
    return-void

    .line 33
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public updated()Z
    .locals 1

    .prologue
    .line 43
    iget-boolean v0, p0, Lcom/samsung/musicplus/glwidget/model/BitmapModel;->mUpdated:Z

    return v0
.end method
