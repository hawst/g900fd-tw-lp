.class public Lcom/samsung/musicplus/glwidget/model/ModelMatrixUpdater;
.super Ljava/lang/Object;
.source "ModelMatrixUpdater.java"

# interfaces
.implements Lcom/samsung/musicplus/glwidget/model/Matrices$MatrixUpdater;


# instance fields
.field protected mModels:[Lcom/samsung/musicplus/glwidget/model/Model;


# direct methods
.method public constructor <init>([Lcom/samsung/musicplus/glwidget/model/Model;)V
    .locals 0
    .param p1, "models"    # [Lcom/samsung/musicplus/glwidget/model/Model;

    .prologue
    .line 12
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 13
    iput-object p1, p0, Lcom/samsung/musicplus/glwidget/model/ModelMatrixUpdater;->mModels:[Lcom/samsung/musicplus/glwidget/model/Model;

    .line 14
    return-void
.end method


# virtual methods
.method public needUpdateMatrix(I)Z
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 18
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/model/ModelMatrixUpdater;->mModels:[Lcom/samsung/musicplus/glwidget/model/Model;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lcom/samsung/musicplus/glwidget/model/Model;->isModified()Z

    move-result v0

    return v0
.end method

.method public updateMatrix(I[FI)V
    .locals 1
    .param p1, "index"    # I
    .param p2, "matrix"    # [F
    .param p3, "offset"    # I

    .prologue
    .line 23
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/model/ModelMatrixUpdater;->mModels:[Lcom/samsung/musicplus/glwidget/model/Model;

    aget-object v0, v0, p1

    invoke-static {v0, p2, p3}, Lcom/samsung/musicplus/glwidget/model/ModelUtils;->updateModelMatrix(Lcom/samsung/musicplus/glwidget/model/Model;[FI)V

    .line 24
    return-void
.end method
