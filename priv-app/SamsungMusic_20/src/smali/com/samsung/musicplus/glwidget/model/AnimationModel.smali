.class public Lcom/samsung/musicplus/glwidget/model/AnimationModel;
.super Lcom/samsung/musicplus/glwidget/model/Model;
.source "AnimationModel.java"


# static fields
.field private static final ANI_TYPE_SCALE:I


# instance fields
.field private mAdapterIndex:I

.field private mAniStartTime:J

.field private mAnimation:Landroid/animation/AnimatorSet;

.field private final mAnimationHandler:Landroid/os/Handler;

.field private mDuration:I

.field private mInterpolator:Landroid/view/animation/AccelerateDecelerateInterpolator;

.field mIsAnimationFinished:Z

.field private mPublisher:Lcom/samsung/musicplus/bitmapcache/BitmapCache$Publisher;

.field private mRestoreScaleF:F


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/samsung/musicplus/glwidget/model/Model;-><init>()V

    .line 29
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/musicplus/glwidget/model/AnimationModel;->mRestoreScaleF:F

    .line 31
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/musicplus/glwidget/model/AnimationModel;->mAniStartTime:J

    .line 33
    const/16 v0, 0x14a

    iput v0, p0, Lcom/samsung/musicplus/glwidget/model/AnimationModel;->mDuration:I

    .line 35
    new-instance v0, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v0}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/glwidget/model/AnimationModel;->mInterpolator:Landroid/view/animation/AccelerateDecelerateInterpolator;

    .line 37
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/glwidget/model/AnimationModel;->mIsAnimationFinished:Z

    .line 39
    new-instance v0, Lcom/samsung/musicplus/glwidget/model/AnimationModel$1;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/glwidget/model/AnimationModel$1;-><init>(Lcom/samsung/musicplus/glwidget/model/AnimationModel;)V

    iput-object v0, p0, Lcom/samsung/musicplus/glwidget/model/AnimationModel;->mAnimationHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/glwidget/model/AnimationModel;)J
    .locals 2
    .param p0, "x0"    # Lcom/samsung/musicplus/glwidget/model/AnimationModel;

    .prologue
    .line 19
    iget-wide v0, p0, Lcom/samsung/musicplus/glwidget/model/AnimationModel;->mAniStartTime:J

    return-wide v0
.end method

.method static synthetic access$100(Lcom/samsung/musicplus/glwidget/model/AnimationModel;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/glwidget/model/AnimationModel;

    .prologue
    .line 19
    iget v0, p0, Lcom/samsung/musicplus/glwidget/model/AnimationModel;->mDuration:I

    return v0
.end method

.method static synthetic access$200(Lcom/samsung/musicplus/glwidget/model/AnimationModel;)Landroid/view/animation/AccelerateDecelerateInterpolator;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/glwidget/model/AnimationModel;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/model/AnimationModel;->mInterpolator:Landroid/view/animation/AccelerateDecelerateInterpolator;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/musicplus/glwidget/model/AnimationModel;)F
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/glwidget/model/AnimationModel;

    .prologue
    .line 19
    iget v0, p0, Lcom/samsung/musicplus/glwidget/model/AnimationModel;->mRestoreScaleF:F

    return v0
.end method


# virtual methods
.method public cancelAnimation()V
    .locals 2

    .prologue
    .line 102
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/model/AnimationModel;->mAnimation:Landroid/animation/AnimatorSet;

    .line 103
    .local v0, "a":Landroid/animation/AnimatorSet;
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/musicplus/glwidget/model/AnimationModel;->mAnimation:Landroid/animation/AnimatorSet;

    .line 104
    if-eqz v0, :cond_0

    .line 105
    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->cancel()V

    .line 107
    :cond_0
    return-void
.end method

.method public getAdapterIndex()I
    .locals 1

    .prologue
    .line 132
    iget v0, p0, Lcom/samsung/musicplus/glwidget/model/AnimationModel;->mAdapterIndex:I

    return v0
.end method

.method public getPublisher()Lcom/samsung/musicplus/bitmapcache/BitmapCache$Publisher;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/model/AnimationModel;->mPublisher:Lcom/samsung/musicplus/bitmapcache/BitmapCache$Publisher;

    return-object v0
.end method

.method public isAnimationStarted()Z
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/model/AnimationModel;->mAnimation:Landroid/animation/AnimatorSet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/model/AnimationModel;->mAnimation:Landroid/animation/AnimatorSet;

    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->isStarted()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-boolean v0, p0, Lcom/samsung/musicplus/glwidget/model/AnimationModel;->mIsAnimationFinished:Z

    if-nez v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setAdapterIndex(I)V
    .locals 0
    .param p1, "adapterIndex"    # I

    .prologue
    .line 124
    iput p1, p0, Lcom/samsung/musicplus/glwidget/model/AnimationModel;->mAdapterIndex:I

    .line 125
    return-void
.end method

.method public setAnimation(Landroid/animation/AnimatorSet;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/AnimatorSet;

    .prologue
    .line 77
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/model/AnimationModel;->cancelAnimation()V

    .line 78
    iput-object p1, p0, Lcom/samsung/musicplus/glwidget/model/AnimationModel;->mAnimation:Landroid/animation/AnimatorSet;

    .line 79
    invoke-virtual {p1, p0}, Landroid/animation/AnimatorSet;->setTarget(Ljava/lang/Object;)V

    .line 80
    return-void
.end method

.method public setPublisher(Lcom/samsung/musicplus/bitmapcache/BitmapCache$Publisher;)V
    .locals 0
    .param p1, "publisher"    # Lcom/samsung/musicplus/bitmapcache/BitmapCache$Publisher;

    .prologue
    .line 91
    iput-object p1, p0, Lcom/samsung/musicplus/glwidget/model/AnimationModel;->mPublisher:Lcom/samsung/musicplus/bitmapcache/BitmapCache$Publisher;

    .line 92
    return-void
.end method

.method public startScaleRestoreAnimation(F)V
    .locals 3
    .param p1, "restoreValue"    # F

    .prologue
    const/4 v2, 0x0

    .line 83
    iget v0, p0, Lcom/samsung/musicplus/glwidget/model/AnimationModel;->mScaleF:F

    sub-float/2addr v0, p1

    iput v0, p0, Lcom/samsung/musicplus/glwidget/model/AnimationModel;->mRestoreScaleF:F

    .line 84
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/musicplus/glwidget/model/AnimationModel;->mAniStartTime:J

    .line 85
    iput-boolean v2, p0, Lcom/samsung/musicplus/glwidget/model/AnimationModel;->mIsAnimationFinished:Z

    .line 86
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/model/AnimationModel;->mAnimationHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 87
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/model/AnimationModel;->mAnimationHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 88
    return-void
.end method
