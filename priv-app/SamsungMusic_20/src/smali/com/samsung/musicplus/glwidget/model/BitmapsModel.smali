.class public Lcom/samsung/musicplus/glwidget/model/BitmapsModel;
.super Lcom/samsung/musicplus/glwidget/model/AnimationModel;
.source "BitmapsModel.java"

# interfaces
.implements Lcom/samsung/musicplus/glwidget/render/RenderableBitmaps;


# instance fields
.field private mAlpha:[F

.field private mBitmaps:[Landroid/graphics/Bitmap;

.field private mLayersCount:I

.field private mUpdated:[Z


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "layersCount"    # I

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/samsung/musicplus/glwidget/model/AnimationModel;-><init>()V

    .line 29
    iput p1, p0, Lcom/samsung/musicplus/glwidget/model/BitmapsModel;->mLayersCount:I

    .line 30
    iget v0, p0, Lcom/samsung/musicplus/glwidget/model/BitmapsModel;->mLayersCount:I

    new-array v0, v0, [Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/samsung/musicplus/glwidget/model/BitmapsModel;->mBitmaps:[Landroid/graphics/Bitmap;

    .line 31
    iget v0, p0, Lcom/samsung/musicplus/glwidget/model/BitmapsModel;->mLayersCount:I

    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/samsung/musicplus/glwidget/model/BitmapsModel;->mUpdated:[Z

    .line 32
    iget v0, p0, Lcom/samsung/musicplus/glwidget/model/BitmapsModel;->mLayersCount:I

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/samsung/musicplus/glwidget/model/BitmapsModel;->mAlpha:[F

    .line 33
    return-void
.end method


# virtual methods
.method public getAlpha()F
    .locals 2

    .prologue
    .line 106
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/model/BitmapsModel;->mAlpha:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    return v0
.end method

.method public getAlphas()[F
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/model/BitmapsModel;->mAlpha:[F

    return-object v0
.end method

.method public getLayersCount()I
    .locals 1

    .prologue
    .line 115
    iget v0, p0, Lcom/samsung/musicplus/glwidget/model/BitmapsModel;->mLayersCount:I

    return v0
.end method

.method public getRenderableBitmaps()[Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/model/BitmapsModel;->mBitmaps:[Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public isRenderable()Z
    .locals 1

    .prologue
    .line 90
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/model/BitmapsModel;->isVisible()Z

    move-result v0

    return v0
.end method

.method public sendBitmap(Landroid/graphics/Bitmap;I)V
    .locals 2
    .param p1, "bm"    # Landroid/graphics/Bitmap;
    .param p2, "layer"    # I

    .prologue
    .line 60
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/model/BitmapsModel;->mBitmaps:[Landroid/graphics/Bitmap;

    aput-object p1, v0, p2

    .line 65
    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/model/BitmapsModel;->mUpdated:[Z

    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/model/BitmapsModel;->mUpdated:[Z

    aget-boolean v0, v0, p2

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isMutable()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    aput-boolean v0, v1, p2

    .line 66
    return-void

    .line 65
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setBitmap(Landroid/graphics/Bitmap;I)V
    .locals 2
    .param p1, "bm"    # Landroid/graphics/Bitmap;
    .param p2, "layer"    # I

    .prologue
    .line 42
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/model/BitmapsModel;->mBitmaps:[Landroid/graphics/Bitmap;

    aget-object v0, v0, p2

    if-eq v0, p1, :cond_1

    .line 43
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/model/BitmapsModel;->mBitmaps:[Landroid/graphics/Bitmap;

    aput-object p1, v0, p2

    .line 48
    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/model/BitmapsModel;->mUpdated:[Z

    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/model/BitmapsModel;->mUpdated:[Z

    aget-boolean v0, v0, p2

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isMutable()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    aput-boolean v0, v1, p2

    .line 50
    :cond_1
    return-void

    .line 48
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public updates()[Z
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/model/BitmapsModel;->mUpdated:[Z

    return-object v0
.end method
