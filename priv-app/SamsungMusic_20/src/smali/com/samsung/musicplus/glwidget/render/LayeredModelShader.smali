.class public Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;
.super Ljava/lang/Object;
.source "LayeredModelShader.java"


# static fields
.field public static final DEF_FOG:Ljava/lang/String; = "Fog"

.field public static final DEF_GREYSCALE:Ljava/lang/String; = "Greyscale"

.field public static final DEF_QUAD_OPACITY_MASK:Ljava/lang/String; = "QuadOpacityMask"

.field public static final DEF_RENDERING_OPACITY:Ljava/lang/String; = "RenderingOpacity"

.field private static final INVALID_SHADER:I = -0x1

.field private static final LOG_TAG:Ljava/lang/String; = "TextureModelShader"

.field private static final maxLayersCount:I = 0x4


# instance fields
.field private mContext:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field

.field private mCoordHandler:I

.field private final mCurrentTextures:[I

.field private mDefines:[Ljava/lang/String;

.field private mFogColor:[F

.field private mFogColorHandler:I

.field private mFogFar:F

.field private mFogFarHandler:I

.field private mFogNear:F

.field private mFogNearHandler:I

.field private mHandler:[I

.field private mModelMatrixHandler:I

.field private mOpacityHandler:I

.field private mPVMatrixHandler:I

.field private mQuadOpacityMaskTexture:Lcom/samsung/musicplus/glwidget/render/Texture;

.field private mQuadOpacityMaskTextureHandler:I

.field private final mQuadTextureUnit:I

.field private mRenderingOpacity:F

.field private mRenderingOpacityHandler:I

.field private mTextures:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Landroid/graphics/Bitmap;",
            "Lcom/samsung/musicplus/glwidget/render/Texture;",
            ">;"
        }
    .end annotation
.end field

.field private mTexturesHandler:[I

.field private mViewportSize:[F

.field private mViewportSizeHandler:I


# direct methods
.method public constructor <init>(Landroid/content/Context;[Ljava/lang/String;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "defines"    # [Ljava/lang/String;

    .prologue
    const/4 v3, 0x2

    const/4 v2, 0x4

    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    new-array v1, v2, [I

    iput-object v1, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mHandler:[I

    .line 60
    new-array v1, v3, [I

    fill-array-data v1, :array_0

    iput-object v1, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mCurrentTextures:[I

    .line 64
    iput v2, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mQuadTextureUnit:I

    .line 75
    new-array v1, v2, [F

    iput-object v1, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mFogColor:[F

    .line 83
    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mRenderingOpacity:F

    .line 93
    new-array v1, v3, [F

    iput-object v1, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mViewportSize:[F

    .line 98
    iput-object p2, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mDefines:[Ljava/lang/String;

    .line 99
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mContext:Ljava/lang/ref/WeakReference;

    .line 100
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mHandler:[I

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 101
    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mHandler:[I

    const/4 v2, -0x1

    aput v2, v1, v0

    .line 100
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 104
    :cond_0
    new-instance v1, Ljava/util/WeakHashMap;

    invoke-direct {v1}, Ljava/util/WeakHashMap;-><init>()V

    iput-object v1, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mTextures:Ljava/util/Map;

    .line 105
    return-void

    .line 60
    nop

    :array_0
    .array-data 4
        0x0
        0x1
    .end array-data
.end method

.method private draw()V
    .locals 3

    .prologue
    .line 225
    const/4 v0, 0x5

    const/4 v1, 0x0

    const/4 v2, 0x4

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glDrawArrays(III)V

    .line 226
    return-void
.end method

.method private getTexture(Landroid/graphics/Bitmap;[ZI)Lcom/samsung/musicplus/glwidget/render/Texture;
    .locals 3
    .param p1, "bm"    # Landroid/graphics/Bitmap;
    .param p2, "updated"    # [Z
    .param p3, "i"    # I

    .prologue
    const/4 v2, 0x0

    .line 270
    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mTextures:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/glwidget/render/Texture;

    .line 271
    .local v0, "res":Lcom/samsung/musicplus/glwidget/render/Texture;
    if-eqz v0, :cond_0

    if-eqz p2, :cond_2

    aget-boolean v1, p2, p3

    if-eqz v1, :cond_2

    .line 272
    :cond_0
    if-nez v0, :cond_1

    .line 273
    new-instance v0, Lcom/samsung/musicplus/glwidget/render/Texture;

    .end local v0    # "res":Lcom/samsung/musicplus/glwidget/render/Texture;
    invoke-direct {v0, v2}, Lcom/samsung/musicplus/glwidget/render/Texture;-><init>(Z)V

    .line 274
    .restart local v0    # "res":Lcom/samsung/musicplus/glwidget/render/Texture;
    :cond_1
    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mTextures:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 275
    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/glwidget/render/Texture;->sendBitmap(Landroid/graphics/Bitmap;)V

    .line 276
    if-eqz p2, :cond_2

    .line 277
    aput-boolean v2, p2, p3

    .line 280
    :cond_2
    return-object v0
.end method

.method private setModelMatrix([FI)V
    .locals 3
    .param p1, "matrix"    # [F
    .param p2, "offset"    # I

    .prologue
    .line 284
    iget v0, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mModelMatrixHandler:I

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2, p1, p2}, Landroid/opengl/GLES20;->glUniformMatrix4fv(IIZ[FI)V

    .line 285
    return-void
.end method

.method private setOpacity(F)V
    .locals 1
    .param p1, "alpha"    # F

    .prologue
    .line 233
    iget v0, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mOpacityHandler:I

    invoke-static {v0, p1}, Landroid/opengl/GLES20;->glUniform1f(IF)V

    .line 234
    return-void
.end method

.method private setOpacity([F)V
    .locals 3
    .param p1, "opacity"    # [F

    .prologue
    .line 229
    iget v0, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mOpacityHandler:I

    array-length v1, p1

    const/4 v2, 0x0

    invoke-static {v0, v1, p1, v2}, Landroid/opengl/GLES20;->glUniform1fv(II[FI)V

    .line 230
    return-void
.end method

.method private setPVMatrix([F)V
    .locals 3
    .param p1, "pVMatrix"    # [F

    .prologue
    const/4 v2, 0x0

    .line 288
    iget v0, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mPVMatrixHandler:I

    const/4 v1, 0x1

    invoke-static {v0, v1, v2, p1, v2}, Landroid/opengl/GLES20;->glUniformMatrix4fv(IIZ[FI)V

    .line 289
    return-void
.end method

.method private setupRendering([FI)V
    .locals 8
    .param p1, "PVMatrix"    # [F
    .param p2, "layersCount"    # I

    .prologue
    const/4 v7, 0x4

    const/4 v3, 0x0

    .line 203
    invoke-direct {p0, p2}, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->use(I)V

    .line 204
    iget v0, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mCoordHandler:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    .line 205
    iget v0, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mCoordHandler:I

    const/4 v1, 0x3

    const/16 v2, 0x1406

    move v4, v3

    move v5, v3

    invoke-static/range {v0 .. v5}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZII)V

    .line 206
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    if-ge v6, p2, :cond_0

    .line 207
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mTexturesHandler:[I

    aget v0, v0, v6

    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mCurrentTextures:[I

    aget v1, v1, v6

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glUniform1i(II)V

    .line 206
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 209
    :cond_0
    iget v0, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mFogColorHandler:I

    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mFogColor:[F

    invoke-static {v0, v3, v1, v7}, Landroid/opengl/GLES20;->glUniform4fv(II[FI)V

    .line 210
    iget v0, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mFogNearHandler:I

    iget v1, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mFogNear:F

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glUniform1f(IF)V

    .line 211
    iget v0, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mFogFarHandler:I

    iget v1, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mFogFar:F

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glUniform1f(IF)V

    .line 212
    iget v0, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mRenderingOpacityHandler:I

    iget v1, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mRenderingOpacity:F

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glUniform1f(IF)V

    .line 213
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mQuadOpacityMaskTexture:Lcom/samsung/musicplus/glwidget/render/Texture;

    if-eqz v0, :cond_1

    .line 215
    iget v0, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mQuadOpacityMaskTextureHandler:I

    invoke-static {v0, v7}, Landroid/opengl/GLES20;->glUniform1i(II)V

    .line 216
    iget v0, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mViewportSizeHandler:I

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mViewportSize:[F

    invoke-static {v0, v1, v2, v3}, Landroid/opengl/GLES20;->glUniform2fv(II[FI)V

    .line 217
    const v0, 0x84c4

    invoke-static {v0}, Landroid/opengl/GLES20;->glActiveTexture(I)V

    .line 218
    const/16 v0, 0xde1

    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mQuadOpacityMaskTexture:Lcom/samsung/musicplus/glwidget/render/Texture;

    invoke-virtual {v1}, Lcom/samsung/musicplus/glwidget/render/Texture;->name()I

    move-result v1

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 221
    :cond_1
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->setPVMatrix([F)V

    .line 222
    return-void
.end method

.method private updateTexture(Lcom/samsung/musicplus/glwidget/render/RenderableBitmap;)V
    .locals 5
    .param p1, "c"    # Lcom/samsung/musicplus/glwidget/render/RenderableBitmap;

    .prologue
    .line 252
    invoke-interface {p1}, Lcom/samsung/musicplus/glwidget/render/RenderableBitmap;->getRenderableBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 253
    .local v0, "bm":Landroid/graphics/Bitmap;
    invoke-interface {p1}, Lcom/samsung/musicplus/glwidget/render/RenderableBitmap;->updated()Z

    move-result v2

    .line 254
    .local v2, "updated":Z
    if-eqz v0, :cond_3

    .line 255
    iget-object v3, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mTextures:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/musicplus/glwidget/render/Texture;

    .line 256
    .local v1, "t":Lcom/samsung/musicplus/glwidget/render/Texture;
    if-eqz v1, :cond_0

    if-eqz v2, :cond_2

    .line 257
    :cond_0
    if-nez v1, :cond_1

    .line 258
    new-instance v1, Lcom/samsung/musicplus/glwidget/render/Texture;

    .end local v1    # "t":Lcom/samsung/musicplus/glwidget/render/Texture;
    const/4 v3, 0x0

    invoke-direct {v1, v3}, Lcom/samsung/musicplus/glwidget/render/Texture;-><init>(Z)V

    .line 259
    .restart local v1    # "t":Lcom/samsung/musicplus/glwidget/render/Texture;
    iget-object v3, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mTextures:Ljava/util/Map;

    invoke-interface {v3, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 261
    :cond_1
    invoke-virtual {v1, v0}, Lcom/samsung/musicplus/glwidget/render/Texture;->sendBitmap(Landroid/graphics/Bitmap;)V

    .line 262
    invoke-interface {p1}, Lcom/samsung/musicplus/glwidget/render/RenderableBitmap;->resetUpdated()V

    .line 264
    :cond_2
    const v3, 0x84c0

    invoke-static {v3}, Landroid/opengl/GLES20;->glActiveTexture(I)V

    .line 265
    const/16 v3, 0xde1

    invoke-virtual {v1}, Lcom/samsung/musicplus/glwidget/render/Texture;->name()I

    move-result v4

    invoke-static {v3, v4}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 267
    .end local v1    # "t":Lcom/samsung/musicplus/glwidget/render/Texture;
    :cond_3
    return-void
.end method

.method private updateTextures(Lcom/samsung/musicplus/glwidget/render/RenderableBitmaps;)V
    .locals 6
    .param p1, "c"    # Lcom/samsung/musicplus/glwidget/render/RenderableBitmaps;

    .prologue
    .line 237
    invoke-interface {p1}, Lcom/samsung/musicplus/glwidget/render/RenderableBitmaps;->getRenderableBitmaps()[Landroid/graphics/Bitmap;

    move-result-object v0

    .line 238
    .local v0, "bm":[Landroid/graphics/Bitmap;
    invoke-interface {p1}, Lcom/samsung/musicplus/glwidget/render/RenderableBitmaps;->updates()[Z

    move-result-object v3

    .line 240
    .local v3, "updates":[Z
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v4, v0

    if-ge v1, v4, :cond_1

    .line 241
    const/4 v4, 0x0

    aget-object v4, v0, v4

    if-nez v4, :cond_0

    .line 240
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 244
    :cond_0
    aget-object v4, v0, v1

    invoke-direct {p0, v4, v3, v1}, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->getTexture(Landroid/graphics/Bitmap;[ZI)Lcom/samsung/musicplus/glwidget/render/Texture;

    move-result-object v2

    .line 246
    .local v2, "res":Lcom/samsung/musicplus/glwidget/render/Texture;
    const v4, 0x84c0

    add-int/2addr v4, v1

    invoke-static {v4}, Landroid/opengl/GLES20;->glActiveTexture(I)V

    .line 247
    const/16 v4, 0xde1

    invoke-virtual {v2}, Lcom/samsung/musicplus/glwidget/render/Texture;->name()I

    move-result v5

    invoke-static {v4, v5}, Landroid/opengl/GLES20;->glBindTexture(II)V

    goto :goto_1

    .line 249
    .end local v2    # "res":Lcom/samsung/musicplus/glwidget/render/Texture;
    :cond_1
    return-void
.end method

.method private use(I)V
    .locals 14
    .param p1, "layersCount"    # I

    .prologue
    const/4 v11, 0x2

    const/4 v13, 0x1

    const/4 v12, 0x0

    .line 292
    iget-object v7, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mHandler:[I

    aget v7, v7, p1

    const/4 v8, -0x1

    if-ne v7, v8, :cond_6

    .line 293
    iget-object v7, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mContext:Ljava/lang/ref/WeakReference;

    invoke-virtual {v7}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 294
    .local v0, "context":Landroid/content/Context;
    if-nez v0, :cond_0

    .line 295
    const-string v7, "TextureModelShader"

    const-string v8, "No context to compile shader!"

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 349
    .end local v0    # "context":Landroid/content/Context;
    :goto_0
    return-void

    .line 299
    .restart local v0    # "context":Landroid/content/Context;
    :cond_0
    const v7, 0x7f080001

    invoke-static {v0, v7}, Lcom/samsung/musicplus/glwidget/render/ShaderCompiler;->loadShader(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v6

    .line 301
    .local v6, "vertex":Ljava/lang/String;
    const/high16 v7, 0x7f080000

    invoke-static {v0, v7}, Lcom/samsung/musicplus/glwidget/render/ShaderCompiler;->loadShader(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v2

    .line 303
    .local v2, "fragmentTemplate":Ljava/lang/String;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 305
    .local v5, "textureDeclarationCode":Ljava/lang/StringBuilder;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    if-ge v3, p1, :cond_1

    .line 306
    sget-object v7, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v8, "uniform sampler2D uTextures%d;"

    new-array v9, v13, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v12

    invoke-static {v7, v8, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 305
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 310
    :cond_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 311
    .local v4, "layersHandlerCode":Ljava/lang/StringBuilder;
    const/4 v3, 0x0

    :goto_2
    if-ge v3, p1, :cond_2

    .line 312
    sget-object v7, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const-string v8, "color += texture2D(uTextures%d, vTextureCoord) * uOpacity[%d];"

    new-array v9, v11, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v12

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v13

    invoke-static {v7, v8, v9}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 311
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 315
    :cond_2
    sget-object v7, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v12

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v13

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v11

    invoke-static {v7, v2, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 318
    .local v1, "fragment":Ljava/lang/String;
    iget-object v7, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mHandler:[I

    iget-object v8, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mDefines:[Ljava/lang/String;

    invoke-static {v0, v8, v6, v1}, Lcom/samsung/musicplus/glwidget/render/ShaderCompiler;->compile(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v8

    aput v8, v7, p1

    .line 320
    iget-object v7, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mHandler:[I

    aget v7, v7, p1

    invoke-static {v7}, Landroid/opengl/GLES20;->glUseProgram(I)V

    .line 322
    iget-object v7, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mHandler:[I

    aget v7, v7, p1

    const-string v8, "aCoord"

    invoke-static {v7, v8}, Landroid/opengl/GLES20;->glGetAttribLocation(ILjava/lang/String;)I

    move-result v7

    iput v7, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mCoordHandler:I

    .line 323
    iget-object v7, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mHandler:[I

    aget v7, v7, p1

    const-string v8, "uModelMatrix"

    invoke-static {v7, v8}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v7

    iput v7, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mModelMatrixHandler:I

    .line 325
    iget-object v7, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mHandler:[I

    aget v7, v7, p1

    const-string v8, "uPVMatrix"

    invoke-static {v7, v8}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v7

    iput v7, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mPVMatrixHandler:I

    .line 327
    iget-object v7, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mTexturesHandler:[I

    if-eqz v7, :cond_3

    iget-object v7, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mTexturesHandler:[I

    array-length v7, v7

    if-eq v7, p1, :cond_4

    .line 328
    :cond_3
    new-array v7, p1, [I

    iput-object v7, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mTexturesHandler:[I

    .line 331
    :cond_4
    const/4 v3, 0x0

    :goto_3
    if-ge v3, p1, :cond_5

    .line 332
    iget-object v7, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mTexturesHandler:[I

    iget-object v8, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mHandler:[I

    aget v8, v8, p1

    const-string v9, "uTextures%d"

    new-array v10, v13, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v10, v12

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v8

    aput v8, v7, v3

    .line 331
    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    .line 335
    :cond_5
    iget-object v7, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mHandler:[I

    aget v7, v7, p1

    const-string v8, "uOpacity"

    invoke-static {v7, v8}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v7

    iput v7, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mOpacityHandler:I

    .line 337
    iget-object v7, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mHandler:[I

    aget v7, v7, p1

    const-string v8, "uFogColor"

    invoke-static {v7, v8}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v7

    iput v7, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mFogColorHandler:I

    .line 338
    iget-object v7, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mHandler:[I

    aget v7, v7, p1

    const-string v8, "uFogNear"

    invoke-static {v7, v8}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v7

    iput v7, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mFogNearHandler:I

    .line 339
    iget-object v7, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mHandler:[I

    aget v7, v7, p1

    const-string v8, "uFogFar"

    invoke-static {v7, v8}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v7

    iput v7, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mFogFarHandler:I

    .line 340
    iget-object v7, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mHandler:[I

    aget v7, v7, p1

    const-string v8, "uRenderingOpacity"

    invoke-static {v7, v8}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v7

    iput v7, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mRenderingOpacityHandler:I

    .line 342
    iget-object v7, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mHandler:[I

    aget v7, v7, p1

    const-string v8, "uViewportSize"

    invoke-static {v7, v8}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v7

    iput v7, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mViewportSizeHandler:I

    .line 344
    iget-object v7, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mHandler:[I

    aget v7, v7, p1

    const-string v8, "uQuadOpacityMaskTexture"

    invoke-static {v7, v8}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v7

    iput v7, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mQuadOpacityMaskTextureHandler:I

    .line 348
    .end local v0    # "context":Landroid/content/Context;
    .end local v1    # "fragment":Ljava/lang/String;
    .end local v2    # "fragmentTemplate":Ljava/lang/String;
    .end local v3    # "i":I
    .end local v4    # "layersHandlerCode":Ljava/lang/StringBuilder;
    .end local v5    # "textureDeclarationCode":Ljava/lang/StringBuilder;
    .end local v6    # "vertex":Ljava/lang/String;
    :cond_6
    iget-object v7, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mHandler:[I

    aget v7, v7, p1

    invoke-static {v7}, Landroid/opengl/GLES20;->glUseProgram(I)V

    goto/16 :goto_0
.end method


# virtual methods
.method public renderBitmap([F[F[Lcom/samsung/musicplus/glwidget/render/Renderable;Landroid/graphics/Bitmap;)V
    .locals 6
    .param p1, "PVMatrix"    # [F
    .param p2, "matrices"    # [F
    .param p3, "models"    # [Lcom/samsung/musicplus/glwidget/render/Renderable;
    .param p4, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 179
    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    .line 200
    :cond_0
    return-void

    .line 182
    :cond_1
    const/4 v4, 0x1

    invoke-direct {p0, p1, v4}, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->setupRendering([FI)V

    .line 184
    const/4 v2, 0x0

    .line 186
    .local v2, "offset":I
    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-direct {p0, p4, v4, v5}, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->getTexture(Landroid/graphics/Bitmap;[ZI)Lcom/samsung/musicplus/glwidget/render/Texture;

    move-result-object v3

    .line 188
    .local v3, "texture":Lcom/samsung/musicplus/glwidget/render/Texture;
    const v4, 0x84c0

    invoke-static {v4}, Landroid/opengl/GLES20;->glActiveTexture(I)V

    .line 189
    const/16 v4, 0xde1

    invoke-virtual {v3}, Lcom/samsung/musicplus/glwidget/render/Texture;->name()I

    move-result v5

    invoke-static {v4, v5}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 191
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v4, p3

    if-ge v1, v4, :cond_0

    .line 192
    aget-object v0, p3, v1

    .line 193
    .local v0, "c":Lcom/samsung/musicplus/glwidget/render/Renderable;
    if-eqz v0, :cond_2

    invoke-interface {v0}, Lcom/samsung/musicplus/glwidget/render/Renderable;->isRenderable()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 194
    invoke-direct {p0, p2, v2}, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->setModelMatrix([FI)V

    .line 195
    invoke-interface {v0}, Lcom/samsung/musicplus/glwidget/render/Renderable;->getAlpha()F

    move-result v4

    invoke-direct {p0, v4}, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->setOpacity(F)V

    .line 196
    invoke-direct {p0}, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->draw()V

    .line 198
    :cond_2
    add-int/lit8 v2, v2, 0x10

    .line 191
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public renderBitmaps([F[F[Lcom/samsung/musicplus/glwidget/render/RenderableBitmap;)V
    .locals 4
    .param p1, "PVMatrix"    # [F
    .param p2, "matrices"    # [F
    .param p3, "models"    # [Lcom/samsung/musicplus/glwidget/render/RenderableBitmap;

    .prologue
    .line 136
    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    .line 154
    :cond_0
    return-void

    .line 140
    :cond_1
    const/4 v3, 0x1

    invoke-direct {p0, p1, v3}, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->setupRendering([FI)V

    .line 142
    const/4 v2, 0x0

    .line 144
    .local v2, "offset":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v3, p3

    if-ge v1, v3, :cond_0

    .line 145
    aget-object v0, p3, v1

    .line 146
    .local v0, "c":Lcom/samsung/musicplus/glwidget/render/RenderableBitmap;
    if-eqz v0, :cond_2

    invoke-interface {v0}, Lcom/samsung/musicplus/glwidget/render/RenderableBitmap;->isRenderable()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 147
    invoke-direct {p0, p2, v2}, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->setModelMatrix([FI)V

    .line 148
    invoke-direct {p0, v0}, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->updateTexture(Lcom/samsung/musicplus/glwidget/render/RenderableBitmap;)V

    .line 149
    invoke-interface {v0}, Lcom/samsung/musicplus/glwidget/render/RenderableBitmap;->getAlpha()F

    move-result v3

    invoke-direct {p0, v3}, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->setOpacity(F)V

    .line 150
    invoke-direct {p0}, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->draw()V

    .line 152
    :cond_2
    add-int/lit8 v2, v2, 0x10

    .line 144
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public renderBitmaps([F[F[Lcom/samsung/musicplus/glwidget/render/RenderableBitmaps;)V
    .locals 5
    .param p1, "PVMatrix"    # [F
    .param p2, "matrices"    # [F
    .param p3, "models"    # [Lcom/samsung/musicplus/glwidget/render/RenderableBitmaps;

    .prologue
    .line 157
    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    .line 176
    :cond_0
    return-void

    .line 161
    :cond_1
    const/4 v4, 0x0

    aget-object v4, p3, v4

    invoke-interface {v4}, Lcom/samsung/musicplus/glwidget/render/RenderableBitmaps;->getRenderableBitmaps()[Landroid/graphics/Bitmap;

    move-result-object v4

    array-length v2, v4

    .line 162
    .local v2, "layersCount":I
    invoke-direct {p0, p1, v2}, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->setupRendering([FI)V

    .line 164
    const/4 v3, 0x0

    .line 166
    .local v3, "offset":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v4, p3

    if-ge v1, v4, :cond_0

    .line 167
    aget-object v0, p3, v1

    .line 168
    .local v0, "c":Lcom/samsung/musicplus/glwidget/render/RenderableBitmaps;
    if-eqz v0, :cond_2

    invoke-interface {v0}, Lcom/samsung/musicplus/glwidget/render/RenderableBitmaps;->isRenderable()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 169
    invoke-direct {p0, p2, v3}, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->setModelMatrix([FI)V

    .line 170
    invoke-direct {p0, v0}, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->updateTextures(Lcom/samsung/musicplus/glwidget/render/RenderableBitmaps;)V

    .line 171
    invoke-interface {v0}, Lcom/samsung/musicplus/glwidget/render/RenderableBitmaps;->getAlphas()[F

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->setOpacity([F)V

    .line 172
    invoke-direct {p0}, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->draw()V

    .line 174
    :cond_2
    add-int/lit8 v3, v3, 0x10

    .line 166
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public setFogColor(FFFF)V
    .locals 2
    .param p1, "r"    # F
    .param p2, "g"    # F
    .param p3, "b"    # F
    .param p4, "a"    # F

    .prologue
    .line 108
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mFogColor:[F

    const/4 v1, 0x0

    aput p1, v0, v1

    .line 109
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mFogColor:[F

    const/4 v1, 0x1

    aput p2, v0, v1

    .line 110
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mFogColor:[F

    const/4 v1, 0x2

    aput p3, v0, v1

    .line 111
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mFogColor:[F

    const/4 v1, 0x3

    aput p4, v0, v1

    .line 112
    return-void
.end method

.method public setFogDistance(FF)V
    .locals 0
    .param p1, "near"    # F
    .param p2, "far"    # F

    .prologue
    .line 115
    iput p1, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mFogNear:F

    .line 116
    iput p2, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mFogFar:F

    .line 117
    return-void
.end method

.method public setQuadOpacityMaskTexture(Landroid/graphics/Bitmap;)V
    .locals 2
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 129
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mQuadOpacityMaskTexture:Lcom/samsung/musicplus/glwidget/render/Texture;

    if-nez v0, :cond_0

    .line 130
    new-instance v0, Lcom/samsung/musicplus/glwidget/render/Texture;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/samsung/musicplus/glwidget/render/Texture;-><init>(Z)V

    iput-object v0, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mQuadOpacityMaskTexture:Lcom/samsung/musicplus/glwidget/render/Texture;

    .line 132
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mQuadOpacityMaskTexture:Lcom/samsung/musicplus/glwidget/render/Texture;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/glwidget/render/Texture;->sendBitmap(Landroid/graphics/Bitmap;)V

    .line 133
    return-void
.end method

.method public setRenderingOpacity(F)V
    .locals 0
    .param p1, "renderingOpacity"    # F

    .prologue
    .line 120
    iput p1, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mRenderingOpacity:F

    .line 121
    return-void
.end method

.method public setViewportSize(FF)V
    .locals 2
    .param p1, "width"    # F
    .param p2, "height"    # F

    .prologue
    .line 124
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mViewportSize:[F

    const/4 v1, 0x0

    aput p1, v0, v1

    .line 125
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->mViewportSize:[F

    const/4 v1, 0x1

    aput p2, v0, v1

    .line 126
    return-void
.end method
