.class Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;
.super Ljava/lang/Object;
.source "GalleryLayoutBase.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AnimationRunnable"
.end annotation


# instance fields
.field private mFlinging:Z

.field private mScroller:Landroid/widget/Scroller;

.field private volatile mX:I

.field final synthetic this$0:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;


# direct methods
.method public constructor <init>(Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;)V
    .locals 2

    .prologue
    .line 1300
    iput-object p1, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;->this$0:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1301
    new-instance v0, Landroid/widget/Scroller;

    iget-object v1, p1, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mParent:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v1}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;->mScroller:Landroid/widget/Scroller;

    .line 1302
    return-void
.end method


# virtual methods
.method public OnDown()V
    .locals 0

    .prologue
    .line 1325
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;->stopAnimation()V

    .line 1326
    return-void
.end method

.method public OnFling(I)V
    .locals 9
    .param p1, "velocity"    # I

    .prologue
    const/4 v1, 0x0

    .line 1337
    if-nez p1, :cond_0

    .line 1344
    :goto_0
    return-void

    .line 1340
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;->stopAnimation()V

    .line 1341
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;->mScroller:Landroid/widget/Scroller;

    const/high16 v5, -0x80000000

    const v6, 0x7fffffff

    move v2, v1

    move v3, p1

    move v4, v1

    move v7, v1

    move v8, v1

    invoke-virtual/range {v0 .. v8}, Landroid/widget/Scroller;->fling(IIIIIIII)V

    .line 1342
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;->mFlinging:Z

    .line 1343
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;->this$0:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;

    iget-object v0, v0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mParent:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v0, p0}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->postOnAnimation(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public OnScroll(I)V
    .locals 1
    .param p1, "dx"    # I

    .prologue
    .line 1329
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;->this$0:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;

    invoke-virtual {v0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->getModelsCount()I

    move-result v0

    if-nez v0, :cond_1

    .line 1334
    :cond_0
    :goto_0
    return-void

    .line 1332
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;->stopAnimation()V

    .line 1333
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;->this$0:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->handleScroll(I)Z

    goto :goto_0
.end method

.method public OnUp()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1370
    iput-boolean v1, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;->mFlinging:Z

    .line 1371
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;->this$0:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;

    invoke-virtual {v0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->getSelectedDX()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const/high16 v2, 0x3f800000    # 1.0f

    cmpl-float v0, v0, v2

    if-lez v0, :cond_1

    .line 1372
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;->stopAnimation()V

    .line 1373
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;->mScroller:Landroid/widget/Scroller;

    iget-object v2, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;->this$0:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;

    invoke-virtual {v2}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->getSelectedDX()F

    move-result v2

    neg-float v2, v2

    float-to-int v3, v2

    const/16 v5, 0x190

    move v2, v1

    move v4, v1

    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    .line 1374
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;->this$0:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;

    iget-object v0, v0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mParent:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v0, p0}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->postOnAnimation(Ljava/lang/Runnable;)V

    .line 1386
    :cond_0
    :goto_0
    return-void

    .line 1376
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;->this$0:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;

    # getter for: Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mPerformanceTestInProgress:Z
    invoke-static {v0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->access$200(Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1377
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;->this$0:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;

    iget-object v0, v0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mParent:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v0}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->fireOnSelectionChange()V

    .line 1379
    :cond_2
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;->this$0:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;

    # getter for: Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAnimationStarted:Z
    invoke-static {v0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->access$800(Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1380
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;->this$0:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;

    # setter for: Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAnimationStarted:Z
    invoke-static {v0, v1}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->access$802(Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;Z)Z

    .line 1381
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;->this$0:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;

    # getter for: Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mPerformanceTestInProgress:Z
    invoke-static {v0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->access$200(Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1382
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;->this$0:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;

    iget-object v0, v0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mParent:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v0}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->fireOnAnimationFinished()V

    goto :goto_0
.end method

.method public animateScroll(II)V
    .locals 6
    .param p1, "delta"    # I
    .param p2, "time"    # I

    .prologue
    const/4 v1, 0x0

    .line 1315
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;->stopAnimation()V

    .line 1316
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;->mScroller:Landroid/widget/Scroller;

    move v2, v1

    move v3, p1

    move v4, v1

    move v5, p2

    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    .line 1317
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;->this$0:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;

    iget-object v0, v0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mParent:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v0, p0}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->postOnAnimation(Ljava/lang/Runnable;)V

    .line 1318
    return-void
.end method

.method public isFlinging()Z
    .locals 1

    .prologue
    .line 1311
    iget-boolean v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;->mFlinging:Z

    return v0
.end method

.method public run()V
    .locals 5

    .prologue
    .line 1348
    iget-object v4, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;->this$0:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;

    # getter for: Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mStopped:Z
    invoke-static {v4}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->access$900(Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1367
    :goto_0
    return-void

    .line 1351
    :cond_0
    iget-object v4, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v4}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v2

    .line 1352
    .local v2, "more":Z
    iget-object v4, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v4}, Landroid/widget/Scroller;->getCurrX()I

    move-result v3

    .line 1353
    .local v3, "x":I
    iget v4, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;->mX:I

    sub-int v0, v4, v3

    .line 1354
    .local v0, "dx":I
    const/4 v1, 0x0

    .line 1355
    .local v1, "limited":Z
    if-eqz v0, :cond_1

    .line 1356
    iget-object v4, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;->this$0:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;

    invoke-virtual {v4, v0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->handleScroll(I)Z

    move-result v1

    .line 1357
    :cond_1
    if-eqz v1, :cond_2

    .line 1358
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;->stopAnimation()V

    .line 1360
    :cond_2
    iput v3, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;->mX:I

    .line 1361
    if-eqz v2, :cond_3

    if-nez v1, :cond_3

    .line 1362
    iget-object v4, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;->this$0:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;

    iget-object v4, v4, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mParent:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v4, p0}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 1363
    iget-object v4, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;->this$0:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;

    iget-object v4, v4, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mParent:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v4, p0}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->postOnAnimation(Ljava/lang/Runnable;)V

    goto :goto_0

    .line 1365
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;->OnUp()V

    goto :goto_0
.end method

.method public scrollFinished()Z
    .locals 1

    .prologue
    .line 1321
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v0}, Landroid/widget/Scroller;->isFinished()Z

    move-result v0

    return v0
.end method

.method public setFriction(F)V
    .locals 1
    .param p1, "friction"    # F

    .prologue
    .line 1305
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;->mScroller:Landroid/widget/Scroller;

    if-eqz v0, :cond_0

    .line 1306
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;->mScroller:Landroid/widget/Scroller;

    invoke-virtual {v0, p1}, Landroid/widget/Scroller;->setFriction(F)V

    .line 1308
    :cond_0
    return-void
.end method

.method public stopAnimation()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1389
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;->this$0:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;

    iget-object v0, v0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mParent:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v0, p0}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 1390
    iput v1, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;->mX:I

    .line 1391
    iput-boolean v1, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;->mFlinging:Z

    .line 1392
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;->mScroller:Landroid/widget/Scroller;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Scroller;->forceFinished(Z)V

    .line 1393
    return-void
.end method
