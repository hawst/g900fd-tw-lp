.class public Lcom/samsung/musicplus/glwidget/GLGalleryTextureView;
.super Lcom/samsung/musicplus/glwidget/GLTextureViewBase;
.source "GLGalleryTextureView.java"

# interfaces
.implements Lcom/samsung/musicplus/glwidget/GLGalleryView;


# static fields
.field private static final LOG_TAG:Ljava/lang/String;


# instance fields
.field private impl:Lcom/samsung/musicplus/glwidget/layout/GLHolder;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lcom/samsung/musicplus/glwidget/GLGalleryTextureView;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/glwidget/GLGalleryTextureView;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;Ljava/lang/Class;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/util/AttributeSet;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/samsung/musicplus/glwidget/layout/GalleryLayout;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 50
    .local p3, "layoutClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/samsung/musicplus/glwidget/layout/GalleryLayout;>;"
    invoke-virtual {p3}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Ljava/lang/String;)V

    .line 38
    new-instance v0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-direct {v0}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/glwidget/GLGalleryTextureView;->impl:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    .line 51
    invoke-direct {p0, p1, p3, p2}, Lcom/samsung/musicplus/glwidget/GLGalleryTextureView;->init(Landroid/content/Context;Ljava/lang/Class;Landroid/util/AttributeSet;)V

    .line 52
    return-void
.end method

.method private init(Landroid/content/Context;Ljava/lang/Class;Landroid/util/AttributeSet;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p3, "attrs"    # Landroid/util/AttributeSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/samsung/musicplus/glwidget/layout/GalleryLayout;",
            ">;",
            "Landroid/util/AttributeSet;",
            ")V"
        }
    .end annotation

    .prologue
    .line 56
    .local p2, "layoutClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/samsung/musicplus/glwidget/layout/GalleryLayout;>;"
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/glwidget/GLGalleryTextureView;->setFocusable(Z)V

    .line 57
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/GLGalleryTextureView;->impl:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    move-object v1, p0

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->init(Lcom/samsung/musicplus/glwidget/GLGalleryView;Landroid/view/View;Landroid/content/Context;Ljava/lang/Class;Landroid/util/AttributeSet;)V

    .line 58
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/GLGalleryTextureView;->impl:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v0}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->getRenderer()Landroid/opengl/GLSurfaceView$Renderer;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/glwidget/GLGalleryTextureView;->setRenderer(Landroid/opengl/GLSurfaceView$Renderer;)V

    .line 61
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/glwidget/GLGalleryTextureView;->setDebugFlags(I)V

    .line 62
    return-void
.end method


# virtual methods
.method public animationFinished()Z
    .locals 1

    .prologue
    .line 214
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/GLGalleryTextureView;->impl:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v0}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->animationFinished()Z

    move-result v0

    return v0
.end method

.method public asView()Landroid/view/View;
    .locals 0

    .prologue
    .line 249
    return-object p0
.end method

.method public getAlbumArtForCoord([F[F)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "inCoord"    # [F
    .param p2, "outCoord"    # [F

    .prologue
    .line 244
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/GLGalleryTextureView;->impl:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->getAlbumArtForCoord([F[F)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getAlbumLastPosition(I)Lcom/samsung/musicplus/glwidget/layout/IAlbumArtInfo$PositionInfo;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 254
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/GLGalleryTextureView;->impl:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->getAlbumLastPosition(I)Lcom/samsung/musicplus/glwidget/layout/IAlbumArtInfo$PositionInfo;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic getDebugFlags()I
    .locals 1

    .prologue
    .line 34
    invoke-super {p0}, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->getDebugFlags()I

    move-result v0

    return v0
.end method

.method public getDefaultAlbumArt()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/GLGalleryTextureView;->impl:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v0}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->getDefaultAlbumArt()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getGLHolder()Lcom/samsung/musicplus/glwidget/layout/GLHolder;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/GLGalleryTextureView;->impl:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    return-object v0
.end method

.method public getSelectedItemPosition()I
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/GLGalleryTextureView;->impl:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v0}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->getSelectedItemPosition()I

    move-result v0

    return v0
.end method

.method public isFlinging()Z
    .locals 1

    .prologue
    .line 229
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/GLGalleryTextureView;->impl:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v0}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->isFlinging()Z

    move-result v0

    return v0
.end method

.method public isTouched()Z
    .locals 1

    .prologue
    .line 224
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/GLGalleryTextureView;->impl:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v0}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->isTouched()Z

    move-result v0

    return v0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 200
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/GLGalleryTextureView;->impl:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v0, p0, p1, p2}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->onKeyDown(Landroid/view/View;ILandroid/view/KeyEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1, p2}, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 209
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/GLGalleryTextureView;->impl:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v0, p0, p1, p2}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->onKeyUp(Landroid/view/View;ILandroid/view/KeyEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1, p2}, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onPause()V
    .locals 3

    .prologue
    .line 160
    sget-object v0, Lcom/samsung/musicplus/glwidget/GLGalleryTextureView;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "onPause"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/GLGalleryTextureView;->impl:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v0}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->onPause()V

    .line 162
    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 3
    .param p1, "state"    # Landroid/os/Parcelable;

    .prologue
    .line 112
    sget-object v1, Lcom/samsung/musicplus/glwidget/GLGalleryTextureView;->LOG_TAG:Ljava/lang/String;

    const-string v2, "onRestoreInstanceState"

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    instance-of v1, p1, Landroid/os/Bundle;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 114
    check-cast v0, Landroid/os/Bundle;

    .line 115
    .local v0, "s":Landroid/os/Bundle;
    const-string v1, "parent"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    invoke-super {p0, v1}, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 116
    const-string v1, "position"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/glwidget/GLGalleryTextureView;->setSelection(I)V

    .line 120
    .end local v0    # "s":Landroid/os/Bundle;
    :goto_0
    return-void

    .line 118
    :cond_0
    invoke-super {p0, p1}, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    goto :goto_0
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 170
    sget-object v0, Lcom/samsung/musicplus/glwidget/GLGalleryTextureView;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "onResume"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/GLGalleryTextureView;->startRenderingThread()V

    .line 172
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/GLGalleryTextureView;->impl:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v0}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->onResume()V

    .line 173
    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .prologue
    .line 100
    sget-object v1, Lcom/samsung/musicplus/glwidget/GLGalleryTextureView;->LOG_TAG:Ljava/lang/String;

    const-string v2, "onSaveInstanceState"

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 102
    .local v0, "res":Landroid/os/Bundle;
    const-string v1, "parent"

    invoke-super {p0}, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 103
    const-string v1, "position"

    iget-object v2, p0, Lcom/samsung/musicplus/glwidget/GLGalleryTextureView;->impl:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v2}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->getSelectedItemPosition()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 104
    return-object v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 180
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/GLGalleryTextureView;->impl:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public bridge synthetic requestRender()V
    .locals 0

    .prologue
    .line 34
    invoke-super {p0}, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->requestRender()V

    return-void
.end method

.method public setAdapter(Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;)V
    .locals 1
    .param p1, "adapter"    # Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/GLGalleryTextureView;->impl:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->setAdapter(Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;)V

    .line 72
    return-void
.end method

.method public setAdapterWrap(Z)V
    .locals 1
    .param p1, "wrap"    # Z

    .prologue
    .line 239
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/GLGalleryTextureView;->impl:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->setAdapterWrap(Z)V

    .line 240
    return-void
.end method

.method public bridge synthetic setDebugFlags(I)V
    .locals 0
    .param p1, "x0"    # I

    .prologue
    .line 34
    invoke-super {p0, p1}, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->setDebugFlags(I)V

    return-void
.end method

.method public setDefaultAlbumArt(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/GLGalleryTextureView;->impl:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->setDefaultAlbumArt(Landroid/graphics/Bitmap;)V

    .line 142
    return-void
.end method

.method public bridge synthetic setGLWrapper(Landroid/opengl/GLSurfaceView$GLWrapper;)V
    .locals 0
    .param p1, "x0"    # Landroid/opengl/GLSurfaceView$GLWrapper;

    .prologue
    .line 34
    invoke-super {p0, p1}, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->setGLWrapper(Landroid/opengl/GLSurfaceView$GLWrapper;)V

    return-void
.end method

.method public setOnAnimationListener(Lcom/samsung/musicplus/glwidget/GLGalleryView$OnAnimationListener;)V
    .locals 1
    .param p1, "onAnimationFinished"    # Lcom/samsung/musicplus/glwidget/GLGalleryView$OnAnimationListener;

    .prologue
    .line 219
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/GLGalleryTextureView;->impl:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->setOnAnimationListener(Lcom/samsung/musicplus/glwidget/GLGalleryView$OnAnimationListener;)V

    .line 220
    return-void
.end method

.method public setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V
    .locals 1
    .param p1, "listener"    # Landroid/widget/AdapterView$OnItemClickListener;

    .prologue
    .line 191
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/GLGalleryTextureView;->impl:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 192
    return-void
.end method

.method public setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V
    .locals 1
    .param p1, "listener"    # Landroid/widget/AdapterView$OnItemSelectedListener;

    .prologue
    .line 130
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/GLGalleryTextureView;->impl:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 131
    return-void
.end method

.method public bridge synthetic setRenderer(Landroid/opengl/GLSurfaceView$Renderer;)V
    .locals 0
    .param p1, "x0"    # Landroid/opengl/GLSurfaceView$Renderer;

    .prologue
    .line 34
    invoke-super {p0, p1}, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->setRenderer(Landroid/opengl/GLSurfaceView$Renderer;)V

    return-void
.end method

.method public setSelection(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 82
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/GLGalleryTextureView;->impl:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->setSelection(I)V

    .line 83
    return-void
.end method
