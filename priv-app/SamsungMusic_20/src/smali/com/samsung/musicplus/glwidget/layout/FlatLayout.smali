.class public Lcom/samsung/musicplus/glwidget/layout/FlatLayout;
.super Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;
.source "FlatLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/glwidget/layout/FlatLayout$Config;,
        Lcom/samsung/musicplus/glwidget/layout/FlatLayout$AlbArtPublisher;
    }
.end annotation


# static fields
.field private static final TRANSFOMRATION_TEXT:I = 0x1

.field private static final TRANSFORMATIONS_COUNT:I = 0x3

.field private static final TRANSFORMATION_ART:I = 0x0

.field private static final TRANSFORMATION_SHADOW:I = 0x2


# instance fields
.field private mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapModel;

.field private mAnimationRepaint:Ljava/lang/Runnable;

.field private volatile mBackgroundOpacityMask:Landroid/graphics/Bitmap;

.field private mCamera:Lcom/samsung/musicplus/glwidget/utils/Camera;

.field private mCameraDistance:F

.field private mConfig:Lcom/samsung/musicplus/glwidget/layout/FlatLayout$Config;

.field private mConverter:Lcom/samsung/musicplus/glwidget/utils/CoordConverter;

.field private mDistanceToProjection:F

.field private mDxK:F

.field private mFov:I

.field private mHasShadow:Z

.field private mMarkers:Lcom/samsung/musicplus/glwidget/utils/MarkerViews;

.field private mMatrices:Lcom/samsung/musicplus/glwidget/model/Matrices;

.field private mMaxTiltedAngle:I

.field private mNeedRepositionSelected:Z

.field private mPAlbY:F

.field private mPAlbumSize:F

.field private mPAlbumSpacing:F

.field private mPHalfSizeSpacing:F

.field private mPSelectedX:F

.field private mPTextHeight:F

.field private mPTextY:F

.field private mPWidth:F

.field private mSAlbumSize:I

.field private mSAlbumSpacing:I

.field private mSTextHeight:I

.field private mScaleLimitEffect:Z

.field private mScrollingEnabled:Z

.field private mSelectedModelIndex:I

.field private mShader:Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;

.field private mShadow:Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;

.field private mTexts:[Lcom/samsung/musicplus/glwidget/model/ViewModel;

.field private mVBO:Lcom/samsung/musicplus/glwidget/render/VBO;


# direct methods
.method public constructor <init>(Lcom/samsung/musicplus/glwidget/layout/GLHolder;Landroid/util/AttributeSet;)V
    .locals 5
    .param p1, "parent"    # Lcom/samsung/musicplus/glwidget/layout/GLHolder;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 190
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;-><init>(Lcom/samsung/musicplus/glwidget/layout/GLHolder;)V

    .line 152
    new-instance v2, Lcom/samsung/musicplus/glwidget/utils/FrustumCamera;

    invoke-direct {v2}, Lcom/samsung/musicplus/glwidget/utils/FrustumCamera;-><init>()V

    iput-object v2, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mCamera:Lcom/samsung/musicplus/glwidget/utils/Camera;

    .line 170
    iput v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mSelectedModelIndex:I

    .line 187
    iput-boolean v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mScaleLimitEffect:Z

    .line 463
    new-instance v2, Lcom/samsung/musicplus/glwidget/layout/FlatLayout$1;

    invoke-direct {v2, p0}, Lcom/samsung/musicplus/glwidget/layout/FlatLayout$1;-><init>(Lcom/samsung/musicplus/glwidget/layout/FlatLayout;)V

    iput-object v2, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mAnimationRepaint:Ljava/lang/Runnable;

    .line 480
    iput-boolean v1, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mNeedRepositionSelected:Z

    .line 192
    const-string v2, "com.samsung.musicplus.glwidget.layout.FLayout"

    iput-object v2, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->LOG_TAG:Ljava/lang/String;

    .line 193
    new-instance v2, Lcom/samsung/musicplus/glwidget/layout/FlatLayout$Config;

    invoke-virtual {p1}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v3

    sget-object v4, Lcom/sec/android/app/music/R$styleable;->GLFlatGalleryView:[I

    invoke-virtual {v3, p2, v4, v1, v1}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/musicplus/glwidget/layout/FlatLayout$Config;-><init>(Landroid/content/res/TypedArray;)V

    iput-object v2, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mConfig:Lcom/samsung/musicplus/glwidget/layout/FlatLayout$Config;

    .line 195
    iget-object v2, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mConfig:Lcom/samsung/musicplus/glwidget/layout/FlatLayout$Config;

    invoke-virtual {v2}, Lcom/samsung/musicplus/glwidget/layout/FlatLayout$Config;->getScrollingFriction()F

    move-result v2

    invoke-virtual {p0, v2}, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->setScrollingFriction(F)V

    .line 197
    iget-object v2, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mConfig:Lcom/samsung/musicplus/glwidget/layout/FlatLayout$Config;

    invoke-virtual {v2}, Lcom/samsung/musicplus/glwidget/layout/FlatLayout$Config;->getShadowBitmap()I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mHasShadow:Z

    .line 198
    return-void

    :cond_0
    move v0, v1

    .line 197
    goto :goto_0
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/glwidget/layout/FlatLayout;)[Lcom/samsung/musicplus/glwidget/model/BitmapModel;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/glwidget/layout/FlatLayout;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapModel;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/musicplus/glwidget/layout/FlatLayout;)[Lcom/samsung/musicplus/glwidget/model/ViewModel;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/glwidget/layout/FlatLayout;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mTexts:[Lcom/samsung/musicplus/glwidget/model/ViewModel;

    return-object v0
.end method

.method private assignReturnRotation(Lcom/samsung/musicplus/glwidget/model/AnimationModel;)Landroid/animation/AnimatorSet;
    .locals 3
    .param p1, "model"    # Lcom/samsung/musicplus/glwidget/model/AnimationModel;

    .prologue
    .line 917
    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mParent:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v1}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mConfig:Lcom/samsung/musicplus/glwidget/layout/FlatLayout$Config;

    invoke-virtual {v2}, Lcom/samsung/musicplus/glwidget/layout/FlatLayout$Config;->getTiltedReturnAnimation()I

    move-result v2

    invoke-static {v1, v2}, Landroid/animation/AnimatorInflater;->loadAnimator(Landroid/content/Context;I)Landroid/animation/Animator;

    move-result-object v0

    check-cast v0, Landroid/animation/AnimatorSet;

    .line 919
    .local v0, "returnAnimation":Landroid/animation/AnimatorSet;
    invoke-virtual {p1, v0}, Lcom/samsung/musicplus/glwidget/model/AnimationModel;->setAnimation(Landroid/animation/AnimatorSet;)V

    .line 920
    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->setupStartValues()V

    .line 921
    invoke-direct {p0}, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->setAnimationRepaint()V

    .line 922
    return-object v0
.end method

.method private assignReturnRotation()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 887
    invoke-direct {p0}, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->findTiltingAngle()F

    move-result v0

    .line 888
    .local v0, "angle":F
    cmpl-float v4, v0, v5

    if-eqz v4, :cond_1

    .line 889
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v4, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapModel;

    array-length v4, v4

    if-ge v3, v4, :cond_1

    .line 890
    iget-object v4, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapModel;

    aget-object v2, v4, v3

    .line 891
    .local v2, "cur":Lcom/samsung/musicplus/glwidget/model/AnimationModel;
    invoke-virtual {v2}, Lcom/samsung/musicplus/glwidget/model/AnimationModel;->getAngle()F

    move-result v4

    cmpl-float v4, v4, v5

    if-nez v4, :cond_0

    .line 889
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 894
    :cond_0
    invoke-direct {p0, v2}, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->assignReturnRotation(Lcom/samsung/musicplus/glwidget/model/AnimationModel;)Landroid/animation/AnimatorSet;

    move-result-object v1

    .line 895
    .local v1, "animation":Landroid/animation/AnimatorSet;
    invoke-virtual {v1}, Landroid/animation/AnimatorSet;->start()V

    .line 896
    iget-object v4, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mTexts:[Lcom/samsung/musicplus/glwidget/model/ViewModel;

    aget-object v4, v4, v3

    invoke-direct {p0, v4}, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->assignReturnRotation(Lcom/samsung/musicplus/glwidget/model/AnimationModel;)Landroid/animation/AnimatorSet;

    move-result-object v1

    .line 897
    invoke-virtual {v1}, Landroid/animation/AnimatorSet;->start()V

    goto :goto_1

    .line 900
    .end local v1    # "animation":Landroid/animation/AnimatorSet;
    .end local v2    # "cur":Lcom/samsung/musicplus/glwidget/model/AnimationModel;
    .end local v3    # "i":I
    :cond_1
    return-void
.end method

.method private assignReturnScale()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 903
    invoke-direct {p0}, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->findEffectScale()F

    move-result v0

    .line 904
    .local v0, "angle":F
    cmpl-float v3, v0, v4

    if-eqz v3, :cond_1

    .line 905
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v3, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapModel;

    array-length v3, v3

    if-ge v2, v3, :cond_1

    .line 906
    iget-object v3, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapModel;

    aget-object v1, v3, v2

    .line 907
    .local v1, "cur":Lcom/samsung/musicplus/glwidget/model/AnimationModel;
    invoke-virtual {v1}, Lcom/samsung/musicplus/glwidget/model/AnimationModel;->getScaleF()F

    move-result v3

    cmpl-float v3, v3, v4

    if-nez v3, :cond_0

    .line 905
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 910
    :cond_0
    invoke-virtual {v1, v4}, Lcom/samsung/musicplus/glwidget/model/AnimationModel;->startScaleRestoreAnimation(F)V

    goto :goto_1

    .line 913
    .end local v1    # "cur":Lcom/samsung/musicplus/glwidget/model/AnimationModel;
    .end local v2    # "i":I
    :cond_1
    invoke-direct {p0}, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->setAnimationRepaint()V

    .line 914
    return-void
.end method

.method private createBackgroundOpacityMask()V
    .locals 10

    .prologue
    const/4 v8, 0x0

    .line 202
    iget-object v5, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->LOG_TAG:Ljava/lang/String;

    const-string v6, "createBackgroundOpacityMask"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 204
    iget v5, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mWidth:I

    iget v6, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mHeight:I

    sget-object v7, Landroid/graphics/Bitmap$Config;->ALPHA_8:Landroid/graphics/Bitmap$Config;

    invoke-static {v5, v6, v7}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 206
    .local v0, "bm":Landroid/graphics/Bitmap;
    new-instance v2, Landroid/graphics/drawable/GradientDrawable;

    sget-object v5, Landroid/graphics/drawable/GradientDrawable$Orientation;->LEFT_RIGHT:Landroid/graphics/drawable/GradientDrawable$Orientation;

    const/4 v6, 0x2

    new-array v6, v6, [I

    fill-array-data v6, :array_0

    invoke-direct {v2, v5, v6}, Landroid/graphics/drawable/GradientDrawable;-><init>(Landroid/graphics/drawable/GradientDrawable$Orientation;[I)V

    .line 210
    .local v2, "gradient":Landroid/graphics/drawable/GradientDrawable;
    invoke-virtual {v2, v8}, Landroid/graphics/drawable/GradientDrawable;->setGradientType(I)V

    .line 211
    iget v5, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mSAlbumSpacing:I

    div-int/lit8 v3, v5, 0x2

    .line 212
    .local v3, "gradientBound":I
    iget v5, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mHeight:I

    invoke-virtual {v2, v8, v8, v3, v5}, Landroid/graphics/drawable/GradientDrawable;->setBounds(IIII)V

    .line 213
    new-instance v1, Landroid/graphics/Canvas;

    invoke-direct {v1, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 214
    .local v1, "canvas":Landroid/graphics/Canvas;
    invoke-virtual {v2, v1}, Landroid/graphics/drawable/GradientDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 215
    new-instance v4, Landroid/graphics/Paint;

    invoke-direct {v4}, Landroid/graphics/Paint;-><init>()V

    .line 216
    .local v4, "paint":Landroid/graphics/Paint;
    const/high16 v5, -0x1000000

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 217
    new-instance v5, Landroid/graphics/RectF;

    int-to-float v6, v3

    const/4 v7, 0x0

    iget v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mWidth:I

    int-to-float v8, v8

    iget v9, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mHeight:I

    int-to-float v9, v9

    invoke-direct {v5, v6, v7, v8, v9}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-virtual {v1, v5, v4}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    .line 219
    iget-object v5, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mShader:Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;

    iget v6, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mWidth:I

    int-to-float v6, v6

    iget v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mHeight:I

    int-to-float v7, v7

    invoke-virtual {v5, v6, v7}, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->setViewportSize(FF)V

    .line 220
    iget-object v5, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mShader:Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;

    invoke-virtual {v5, v0}, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->setQuadOpacityMaskTexture(Landroid/graphics/Bitmap;)V

    .line 223
    iput-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mBackgroundOpacityMask:Landroid/graphics/Bitmap;

    .line 225
    return-void

    .line 206
    :array_0
    .array-data 4
        0x0
        -0x1000000
    .end array-data
.end method

.method private fillAlbumsArray(Landroid/view/LayoutInflater;I)V
    .locals 10
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "modelCount"    # I

    .prologue
    const/high16 v9, 0x40000000    # 2.0f

    const/4 v5, 0x2

    .line 260
    new-array v0, p2, [Lcom/samsung/musicplus/glwidget/model/BitmapModel;

    iput-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapModel;

    .line 261
    new-array v0, p2, [Lcom/samsung/musicplus/glwidget/model/ViewModel;

    iput-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mTexts:[Lcom/samsung/musicplus/glwidget/model/ViewModel;

    .line 262
    new-instance v0, Lcom/samsung/musicplus/glwidget/utils/MarkerViews;

    iget v1, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mPAlbumSize:F

    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->getConverter()Lcom/samsung/musicplus/glwidget/utils/CoordConverter;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapModel;

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Lcom/samsung/musicplus/glwidget/utils/MarkerViews;-><init>(FLcom/samsung/musicplus/glwidget/utils/CoordConverter;[Lcom/samsung/musicplus/glwidget/model/AnimationModel;Landroid/view/LayoutInflater;I)V

    iput-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mMarkers:Lcom/samsung/musicplus/glwidget/utils/MarkerViews;

    .line 264
    iget v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mSAlbumSize:I

    invoke-static {v0, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    .line 265
    .local v8, "width":I
    iget v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mSTextHeight:I

    invoke-static {v0, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v6

    .line 268
    .local v6, "height":I
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    if-ge v7, p2, :cond_0

    .line 269
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapModel;

    new-instance v1, Lcom/samsung/musicplus/glwidget/model/BitmapModel;

    invoke-direct {v1}, Lcom/samsung/musicplus/glwidget/model/BitmapModel;-><init>()V

    aput-object v1, v0, v7

    .line 270
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapModel;

    aget-object v0, v0, v7

    iget v1, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mPAlbumSize:F

    iget v2, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mPAlbumSize:F

    invoke-virtual {v0, v1, v2}, Lcom/samsung/musicplus/glwidget/model/BitmapModel;->setSize(FF)V

    .line 272
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mTexts:[Lcom/samsung/musicplus/glwidget/model/ViewModel;

    new-instance v1, Lcom/samsung/musicplus/glwidget/model/ViewModel;

    iget-object v2, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mConfig:Lcom/samsung/musicplus/glwidget/layout/FlatLayout$Config;

    invoke-virtual {v2}, Lcom/samsung/musicplus/glwidget/layout/FlatLayout$Config;->getTextLayout()I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    invoke-direct {v1, v2, v8, v6}, Lcom/samsung/musicplus/glwidget/model/ViewModel;-><init>(Landroid/view/View;II)V

    aput-object v1, v0, v7

    .line 273
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mTexts:[Lcom/samsung/musicplus/glwidget/model/ViewModel;

    aget-object v0, v0, v7

    iget v1, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mPAlbumSize:F

    iget v2, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mPTextHeight:F

    invoke-virtual {v0, v1, v2}, Lcom/samsung/musicplus/glwidget/model/ViewModel;->setSize(FF)V

    .line 268
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 276
    :cond_0
    new-instance v0, Lcom/samsung/musicplus/glwidget/model/Matrices;

    const/4 v1, 0x3

    invoke-direct {v0, v1, p2}, Lcom/samsung/musicplus/glwidget/model/Matrices;-><init>(II)V

    iput-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mMatrices:Lcom/samsung/musicplus/glwidget/model/Matrices;

    .line 278
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mMatrices:Lcom/samsung/musicplus/glwidget/model/Matrices;

    const/4 v1, 0x0

    new-instance v2, Lcom/samsung/musicplus/glwidget/model/ModelMatrixUpdater;

    iget-object v3, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapModel;

    invoke-direct {v2, v3}, Lcom/samsung/musicplus/glwidget/model/ModelMatrixUpdater;-><init>([Lcom/samsung/musicplus/glwidget/model/Model;)V

    invoke-virtual {v0, v1, v2}, Lcom/samsung/musicplus/glwidget/model/Matrices;->setTransformation(ILcom/samsung/musicplus/glwidget/model/Matrices$MatrixUpdater;)V

    .line 280
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mMatrices:Lcom/samsung/musicplus/glwidget/model/Matrices;

    const/4 v1, 0x1

    new-instance v2, Lcom/samsung/musicplus/glwidget/model/ModelMatrixUpdater;

    iget-object v3, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mTexts:[Lcom/samsung/musicplus/glwidget/model/ViewModel;

    invoke-direct {v2, v3}, Lcom/samsung/musicplus/glwidget/model/ModelMatrixUpdater;-><init>([Lcom/samsung/musicplus/glwidget/model/Model;)V

    invoke-virtual {v0, v1, v2}, Lcom/samsung/musicplus/glwidget/model/Matrices;->setTransformation(ILcom/samsung/musicplus/glwidget/model/Matrices$MatrixUpdater;)V

    .line 282
    iget-boolean v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mHasShadow:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mShadow:Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;

    if-eqz v0, :cond_1

    .line 283
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mMatrices:Lcom/samsung/musicplus/glwidget/model/Matrices;

    new-instance v1, Lcom/samsung/musicplus/glwidget/model/ShadowMatrixUpdater;

    iget-object v2, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapModel;

    iget-object v3, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mShadow:Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;

    invoke-direct {v1, v2, v3}, Lcom/samsung/musicplus/glwidget/model/ShadowMatrixUpdater;-><init>([Lcom/samsung/musicplus/glwidget/model/Model;Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;)V

    invoke-virtual {v0, v5, v1}, Lcom/samsung/musicplus/glwidget/model/Matrices;->setTransformation(ILcom/samsung/musicplus/glwidget/model/Matrices$MatrixUpdater;)V

    .line 285
    :cond_1
    return-void
.end method

.method private findEffectScale()F
    .locals 6

    .prologue
    .line 504
    const/4 v4, 0x0

    .line 505
    .local v4, "res":F
    const/4 v3, 0x0

    .line 506
    .local v3, "j":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v5, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapModel;

    array-length v5, v5

    if-ge v2, v5, :cond_1

    .line 507
    iget-object v5, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapModel;

    aget-object v0, v5, v2

    .line 508
    .local v0, "c":Lcom/samsung/musicplus/glwidget/model/BitmapModel;
    invoke-virtual {v0}, Lcom/samsung/musicplus/glwidget/model/BitmapModel;->getScaleF()F

    move-result v5

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v1

    .line 509
    .local v1, "ca":F
    cmpl-float v5, v1, v4

    if-lez v5, :cond_0

    .line 510
    move v4, v1

    .line 511
    move v3, v2

    .line 506
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 514
    .end local v0    # "c":Lcom/samsung/musicplus/glwidget/model/BitmapModel;
    .end local v1    # "ca":F
    :cond_1
    iget-object v5, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapModel;

    aget-object v5, v5, v3

    invoke-virtual {v5}, Lcom/samsung/musicplus/glwidget/model/BitmapModel;->getScaleF()F

    move-result v5

    return v5
.end method

.method private findTiltingAngle()F
    .locals 6

    .prologue
    .line 490
    const/4 v4, 0x0

    .line 491
    .local v4, "res":F
    const/4 v3, 0x0

    .line 492
    .local v3, "j":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v5, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapModel;

    array-length v5, v5

    if-ge v2, v5, :cond_1

    .line 493
    iget-object v5, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapModel;

    aget-object v0, v5, v2

    .line 494
    .local v0, "c":Lcom/samsung/musicplus/glwidget/model/BitmapModel;
    invoke-virtual {v0}, Lcom/samsung/musicplus/glwidget/model/BitmapModel;->getAngle()F

    move-result v5

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v1

    .line 495
    .local v1, "ca":F
    cmpl-float v5, v1, v4

    if-lez v5, :cond_0

    .line 496
    move v4, v1

    .line 497
    move v3, v2

    .line 492
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 500
    .end local v0    # "c":Lcom/samsung/musicplus/glwidget/model/BitmapModel;
    .end local v1    # "ca":F
    :cond_1
    iget-object v5, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapModel;

    aget-object v5, v5, v3

    invoke-virtual {v5}, Lcom/samsung/musicplus/glwidget/model/BitmapModel;->getAngle()F

    move-result v5

    return v5
.end method

.method private getPSize(I)F
    .locals 1
    .param p1, "screenSize"    # I

    .prologue
    .line 245
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mConverter:Lcom/samsung/musicplus/glwidget/utils/CoordConverter;

    invoke-interface {v0, p1}, Lcom/samsung/musicplus/glwidget/utils/CoordConverter;->getPSize(I)F

    move-result v0

    return v0
.end method

.method private getSSize(F)I
    .locals 1
    .param p1, "pSize"    # F

    .prologue
    .line 255
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mConverter:Lcom/samsung/musicplus/glwidget/utils/CoordConverter;

    invoke-interface {v0, p1}, Lcom/samsung/musicplus/glwidget/utils/CoordConverter;->getSSize(F)I

    move-result v0

    return v0
.end method

.method private positionView(IF)V
    .locals 3
    .param p1, "idx"    # I
    .param p2, "x"    # F

    .prologue
    const/4 v2, 0x0

    .line 288
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapModel;

    aget-object v0, v0, p1

    iget v1, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mPAlbY:F

    invoke-virtual {v0, p2, v1, v2}, Lcom/samsung/musicplus/glwidget/model/BitmapModel;->setPosition(FFF)V

    .line 289
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mTexts:[Lcom/samsung/musicplus/glwidget/model/ViewModel;

    aget-object v0, v0, p1

    iget v1, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mPTextY:F

    invoke-virtual {v0, p2, v1, v2}, Lcom/samsung/musicplus/glwidget/model/ViewModel;->setPosition(FFF)V

    .line 290
    return-void
.end method

.method private setAnimationRepaint()V
    .locals 2

    .prologue
    .line 485
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mParent:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mAnimationRepaint:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 486
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mParent:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mAnimationRepaint:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->postOnAnimation(Ljava/lang/Runnable;)V

    .line 487
    return-void
.end method


# virtual methods
.method protected animateToChild(I)V
    .locals 2
    .param p1, "adapterIndex"    # I

    .prologue
    .line 423
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->setSelectedAdapterIndex(I)V

    .line 424
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->realignView()V

    .line 426
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->relIndex(I)I

    move-result v0

    iget v1, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mPSelectedX:F

    invoke-direct {p0, v0, v1}, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->positionView(IF)V

    .line 427
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->updateViewData()V

    .line 428
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->applyScroll(F)Z

    .line 429
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mParent:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v0}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->requestRender()V

    .line 430
    return-void
.end method

.method protected convertDx(I)F
    .locals 2
    .param p1, "dx"    # I

    .prologue
    .line 819
    int-to-float v0, p1

    iget v1, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mDxK:F

    mul-float/2addr v0, v1

    return v0
.end method

.method public doGetAlbumArtForCoord([F[F)Landroid/graphics/Bitmap;
    .locals 13
    .param p1, "inCoord"    # [F
    .param p2, "outCoord"    # [F

    .prologue
    .line 856
    iget v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mHeight:I

    div-int/lit8 v7, v7, 0x2

    int-to-float v7, v7

    const/4 v8, 0x1

    aget v8, p1, v8

    sub-float/2addr v7, v8

    float-to-int v7, v7

    invoke-direct {p0, v7}, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->getPSize(I)F

    move-result v5

    .line 857
    .local v5, "pY":F
    const/4 v7, 0x0

    aget v7, p1, v7

    float-to-int v7, v7

    invoke-direct {p0, v7}, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->getPSize(I)F

    move-result v4

    .line 860
    .local v4, "pX":F
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapModel;

    array-length v7, v7

    if-ge v3, v7, :cond_2

    .line 861
    iget-object v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapModel;

    aget-object v0, v7, v3

    .line 862
    .local v0, "cur":Lcom/samsung/musicplus/glwidget/model/BitmapModel;
    invoke-virtual {v0}, Lcom/samsung/musicplus/glwidget/model/BitmapModel;->getWidth()F

    move-result v7

    const/high16 v8, 0x40000000    # 2.0f

    div-float v2, v7, v8

    .line 863
    .local v2, "halfWidth":F
    invoke-virtual {v0}, Lcom/samsung/musicplus/glwidget/model/BitmapModel;->getHeight()F

    move-result v7

    const/high16 v8, 0x40000000    # 2.0f

    div-float v1, v7, v8

    .line 864
    .local v1, "halfHeight":F
    invoke-virtual {v0}, Lcom/samsung/musicplus/glwidget/model/BitmapModel;->isVisible()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-virtual {v0}, Lcom/samsung/musicplus/glwidget/model/BitmapModel;->getX()F

    move-result v7

    sub-float/2addr v7, v4

    invoke-static {v7}, Ljava/lang/Math;->abs(F)F

    move-result v7

    cmpg-float v7, v7, v2

    if-gez v7, :cond_1

    invoke-virtual {v0}, Lcom/samsung/musicplus/glwidget/model/BitmapModel;->getY()F

    move-result v7

    sub-float/2addr v7, v5

    invoke-static {v7}, Ljava/lang/Math;->abs(F)F

    move-result v7

    cmpg-float v7, v7, v1

    if-gez v7, :cond_1

    .line 867
    const/4 v7, 0x0

    const/4 v8, 0x0

    aget v8, p1, v8

    invoke-virtual {v0}, Lcom/samsung/musicplus/glwidget/model/BitmapModel;->getX()F

    move-result v9

    iget v10, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mPAlbumSize:F

    const/high16 v11, 0x40000000    # 2.0f

    div-float/2addr v10, v11

    sub-float/2addr v9, v10

    invoke-direct {p0, v9}, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->getSSize(F)I

    move-result v9

    int-to-float v9, v9

    sub-float/2addr v8, v9

    aput v8, p2, v7

    .line 869
    const/4 v7, 0x1

    const/4 v8, 0x1

    aget v8, p1, v8

    iget v9, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mHeight:I

    div-int/lit8 v9, v9, 0x2

    invoke-virtual {v0}, Lcom/samsung/musicplus/glwidget/model/BitmapModel;->getY()F

    move-result v10

    iget v11, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mPAlbumSize:F

    const/high16 v12, 0x40000000    # 2.0f

    div-float/2addr v11, v12

    add-float/2addr v10, v11

    invoke-direct {p0, v10}, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->getSSize(F)I

    move-result v10

    sub-int/2addr v9, v10

    int-to-float v9, v9

    sub-float/2addr v8, v9

    aput v8, p2, v7

    .line 871
    invoke-virtual {v0}, Lcom/samsung/musicplus/glwidget/model/BitmapModel;->getRenderableBitmap()Landroid/graphics/Bitmap;

    move-result-object v6

    .line 873
    .local v6, "res":Landroid/graphics/Bitmap;
    if-nez v6, :cond_0

    .line 874
    iget-object v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mParent:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v7}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->getDefaultAlbumArt()Landroid/graphics/Bitmap;

    move-result-object v6

    .line 877
    :cond_0
    const/4 v7, 0x0

    const/4 v8, 0x0

    aget v8, p2, v8

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v9

    int-to-float v9, v9

    mul-float/2addr v8, v9

    iget v9, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mSAlbumSize:I

    int-to-float v9, v9

    div-float/2addr v8, v9

    float-to-int v8, v8

    int-to-float v8, v8

    aput v8, p2, v7

    .line 878
    const/4 v7, 0x1

    const/4 v8, 0x1

    aget v8, p2, v8

    invoke-virtual {v6}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    int-to-float v9, v9

    mul-float/2addr v8, v9

    iget v9, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mSAlbumSize:I

    int-to-float v9, v9

    div-float/2addr v8, v9

    float-to-int v8, v8

    int-to-float v8, v8

    aput v8, p2, v7

    .line 883
    .end local v0    # "cur":Lcom/samsung/musicplus/glwidget/model/BitmapModel;
    .end local v1    # "halfHeight":F
    .end local v2    # "halfWidth":F
    .end local v6    # "res":Landroid/graphics/Bitmap;
    :goto_1
    return-object v6

    .line 860
    .restart local v0    # "cur":Lcom/samsung/musicplus/glwidget/model/BitmapModel;
    .restart local v1    # "halfHeight":F
    .restart local v2    # "halfWidth":F
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    .line 883
    .end local v0    # "cur":Lcom/samsung/musicplus/glwidget/model/BitmapModel;
    .end local v1    # "halfHeight":F
    .end local v2    # "halfWidth":F
    :cond_2
    const/4 v6, 0x0

    goto :goto_1
.end method

.method public getAlbumLastPosition(I)Lcom/samsung/musicplus/glwidget/layout/IAlbumArtInfo$PositionInfo;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 1031
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getConverter()Lcom/samsung/musicplus/glwidget/utils/CoordConverter;
    .locals 1

    .prologue
    .line 229
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mConverter:Lcom/samsung/musicplus/glwidget/utils/CoordConverter;

    return-object v0
.end method

.method protected getDebugState()Ljava/lang/String;
    .locals 5

    .prologue
    .line 824
    const-string v0, "SelectedAdapterIndex: %d mPSelectedX: %.2f ModelsCount: %d HalfModelWidth: %.2f ModelsState: %s"

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->getSelection()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget v3, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mPSelectedX:F

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->getModelsCount()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget v3, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mPAlbumSize:F

    iget v4, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mPAlbumSpacing:F

    add-float/2addr v3, v4

    const/high16 v4, 0x40000000    # 2.0f

    div-float/2addr v3, v4

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    iget-object v3, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapModel;

    invoke-static {v3}, Lcom/samsung/musicplus/glwidget/model/ModelUtils;->getDebugState([Lcom/samsung/musicplus/glwidget/model/Model;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getMaxScrollWidth()F
    .locals 1

    .prologue
    .line 664
    iget v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mSAlbumSize:I

    int-to-float v0, v0

    return v0
.end method

.method public getMaxSelectedDx()F
    .locals 1

    .prologue
    .line 1026
    const/high16 v0, 0x3f800000    # 1.0f

    return v0
.end method

.method protected getModelsCount()I
    .locals 1

    .prologue
    .line 519
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapModel;

    if-eqz v0, :cond_0

    .line 520
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapModel;

    array-length v0, v0

    .line 522
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected getSelectedModelPosition()I
    .locals 1

    .prologue
    .line 527
    iget v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mSelectedModelIndex:I

    return v0
.end method

.method protected handleOverScroll(FF)V
    .locals 12
    .param p1, "dx"    # F
    .param p2, "limitDelta"    # F

    .prologue
    const/high16 v11, 0x3f800000    # 1.0f

    const/4 v10, 0x0

    .line 555
    iget-object v9, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mConfig:Lcom/samsung/musicplus/glwidget/layout/FlatLayout$Config;

    invoke-virtual {v9}, Lcom/samsung/musicplus/glwidget/layout/FlatLayout$Config;->getTiltedK()F

    move-result v4

    .line 557
    .local v4, "k":F
    iget-boolean v9, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mScaleLimitEffect:Z

    if-eqz v9, :cond_4

    .line 558
    invoke-direct {p0}, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->findEffectScale()F

    move-result v1

    .line 563
    .local v1, "angle":F
    :goto_0
    cmpl-float v9, p2, v10

    if-eqz v9, :cond_0

    .line 564
    div-float v9, p2, v4

    sub-float/2addr v1, v9

    .line 565
    iget v9, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mMaxTiltedAngle:I

    int-to-float v9, v9

    cmpl-float v9, v1, v9

    if-lez v9, :cond_5

    .line 566
    iget v9, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mMaxTiltedAngle:I

    int-to-float v1, v9

    .line 572
    :cond_0
    :goto_1
    cmpl-float v9, v1, v10

    if-eqz v9, :cond_3

    .line 574
    const/4 v7, -0x1

    .line 581
    .local v7, "tiltingIdx":I
    cmpl-float v9, p2, v10

    if-gtz v9, :cond_1

    cmpl-float v9, p1, v10

    if-lez v9, :cond_6

    :cond_1
    const/4 v5, 0x1

    .line 582
    .local v5, "max":Z
    :goto_2
    if-eqz v5, :cond_7

    .line 583
    const/high16 v8, -0x40800000    # -1.0f

    .line 587
    .local v8, "x":F
    :goto_3
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_4
    iget-object v9, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapModel;

    array-length v9, v9

    if-ge v3, v9, :cond_2

    .line 588
    iget-object v9, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapModel;

    aget-object v2, v9, v3

    .line 590
    .local v2, "cur":Lcom/samsung/musicplus/glwidget/model/BitmapModel;
    iget-boolean v9, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mScaleLimitEffect:Z

    if-eqz v9, :cond_8

    .line 591
    invoke-virtual {v2}, Lcom/samsung/musicplus/glwidget/model/BitmapModel;->getScaleF()F

    move-result v9

    cmpl-float v9, v9, v10

    if-eqz v9, :cond_9

    .line 592
    move v7, v3

    .line 611
    .end local v2    # "cur":Lcom/samsung/musicplus/glwidget/model/BitmapModel;
    :cond_2
    :goto_5
    const/4 v9, -0x1

    if-ne v7, v9, :cond_c

    .line 637
    .end local v3    # "i":I
    .end local v5    # "max":Z
    .end local v7    # "tiltingIdx":I
    .end local v8    # "x":F
    :cond_3
    :goto_6
    return-void

    .line 560
    .end local v1    # "angle":F
    :cond_4
    invoke-direct {p0}, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->findTiltingAngle()F

    move-result v1

    .restart local v1    # "angle":F
    goto :goto_0

    .line 567
    :cond_5
    iget v9, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mMaxTiltedAngle:I

    neg-int v9, v9

    int-to-float v9, v9

    cmpg-float v9, v1, v9

    if-gez v9, :cond_0

    .line 568
    iget v9, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mMaxTiltedAngle:I

    neg-int v9, v9

    int-to-float v1, v9

    goto :goto_1

    .line 581
    .restart local v7    # "tiltingIdx":I
    :cond_6
    const/4 v5, 0x0

    goto :goto_2

    .line 585
    .restart local v5    # "max":Z
    :cond_7
    iget v9, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mWidth:I

    add-int/lit8 v9, v9, 0x1

    int-to-float v8, v9

    .restart local v8    # "x":F
    goto :goto_3

    .line 596
    .restart local v2    # "cur":Lcom/samsung/musicplus/glwidget/model/BitmapModel;
    .restart local v3    # "i":I
    :cond_8
    invoke-virtual {v2}, Lcom/samsung/musicplus/glwidget/model/BitmapModel;->getAngle()F

    move-result v9

    cmpl-float v9, v9, v10

    if-eqz v9, :cond_9

    .line 597
    move v7, v3

    .line 598
    goto :goto_5

    .line 602
    :cond_9
    if-nez v5, :cond_b

    invoke-virtual {v2}, Lcom/samsung/musicplus/glwidget/model/BitmapModel;->isVisible()Z

    move-result v9

    if-eqz v9, :cond_b

    invoke-virtual {v2}, Lcom/samsung/musicplus/glwidget/model/BitmapModel;->getX()F

    move-result v9

    cmpg-float v9, v9, v8

    if-gez v9, :cond_b

    .line 603
    move v7, v3

    .line 604
    invoke-virtual {v2}, Lcom/samsung/musicplus/glwidget/model/BitmapModel;->getX()F

    move-result v8

    .line 587
    :cond_a
    :goto_7
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 605
    :cond_b
    if-eqz v5, :cond_a

    invoke-virtual {v2}, Lcom/samsung/musicplus/glwidget/model/BitmapModel;->isVisible()Z

    move-result v9

    if-eqz v9, :cond_a

    invoke-virtual {v2}, Lcom/samsung/musicplus/glwidget/model/BitmapModel;->getX()F

    move-result v9

    cmpl-float v9, v9, v8

    if-lez v9, :cond_a

    .line 606
    move v7, v3

    .line 607
    invoke-virtual {v2}, Lcom/samsung/musicplus/glwidget/model/BitmapModel;->getX()F

    move-result v8

    goto :goto_7

    .line 616
    .end local v2    # "cur":Lcom/samsung/musicplus/glwidget/model/BitmapModel;
    :cond_c
    iget-object v9, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapModel;

    aget-object v0, v9, v7

    .line 617
    .local v0, "album":Lcom/samsung/musicplus/glwidget/model/BitmapModel;
    iget-boolean v9, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mScaleLimitEffect:Z

    if-eqz v9, :cond_e

    .line 618
    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/glwidget/model/BitmapModel;->setScaleF(F)V

    .line 622
    :goto_8
    invoke-virtual {v0}, Lcom/samsung/musicplus/glwidget/model/BitmapModel;->cancelAnimation()V

    .line 623
    iget-boolean v9, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mScaleLimitEffect:Z

    if-nez v9, :cond_d

    .line 624
    iget-object v9, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mTexts:[Lcom/samsung/musicplus/glwidget/model/ViewModel;

    aget-object v6, v9, v7

    .line 625
    .local v6, "text":Lcom/samsung/musicplus/glwidget/model/ViewModel;
    invoke-virtual {v6, v1, v10, v11, v10}, Lcom/samsung/musicplus/glwidget/model/ViewModel;->setRotation(FFFF)V

    .line 626
    invoke-virtual {v6}, Lcom/samsung/musicplus/glwidget/model/ViewModel;->cancelAnimation()V

    .line 629
    .end local v6    # "text":Lcom/samsung/musicplus/glwidget/model/ViewModel;
    :cond_d
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->isTouched()Z

    move-result v9

    if-nez v9, :cond_3

    .line 630
    iget-boolean v9, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mScaleLimitEffect:Z

    if-eqz v9, :cond_f

    .line 631
    invoke-direct {p0}, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->assignReturnScale()V

    goto :goto_6

    .line 620
    :cond_e
    invoke-virtual {v0, v1, v10, v11, v10}, Lcom/samsung/musicplus/glwidget/model/BitmapModel;->setRotation(FFFF)V

    goto :goto_8

    .line 633
    :cond_f
    invoke-direct {p0}, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->assignReturnRotation()V

    goto/16 :goto_6
.end method

.method protected limitScroll(F)F
    .locals 6
    .param p1, "dx"    # F

    .prologue
    const/4 v2, 0x0

    .line 641
    iget-boolean v3, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mScrollingEnabled:Z

    if-nez v3, :cond_1

    move v0, v2

    .line 659
    :cond_0
    :goto_0
    return v0

    .line 646
    :cond_1
    iget-object v3, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapModel;

    const/4 v4, 0x0

    invoke-virtual {p0, v4}, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->relIndex(I)I

    move-result v4

    aget-object v3, v3, v4

    invoke-virtual {v3}, Lcom/samsung/musicplus/glwidget/model/BitmapModel;->getX()F

    move-result v1

    .line 648
    .local v1, "selectedX":F
    cmpg-float v3, p1, v2

    if-gez v3, :cond_2

    .line 649
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->getSelection()I

    move-result v3

    int-to-float v3, v3

    iget v4, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mPAlbumSize:F

    iget v5, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mPAlbumSpacing:F

    add-float/2addr v4, v5

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mPSelectedX:F

    sub-float/2addr v4, v1

    add-float v0, v3, v4

    .line 656
    .local v0, "maxPDx":F
    :goto_1
    cmpg-float v2, v0, v2

    if-gez v2, :cond_0

    .line 657
    const/4 v0, 0x0

    goto :goto_0

    .line 652
    .end local v0    # "maxPDx":F
    :cond_2
    iget-object v3, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mAdapter:Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;

    invoke-interface {v3}, Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;->getCount()I

    move-result v3

    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->getSelection()I

    move-result v4

    sub-int/2addr v3, v4

    add-int/lit8 v3, v3, -0x1

    int-to-float v3, v3

    iget v4, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mPAlbumSize:F

    iget v5, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mPAlbumSpacing:F

    add-float/2addr v4, v5

    mul-float/2addr v3, v4

    iget v4, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mPHalfSizeSpacing:F

    add-float/2addr v3, v4

    iget v4, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mPWidth:F

    sub-float/2addr v4, v1

    sub-float v0, v3, v4

    .restart local v0    # "maxPDx":F
    goto :goto_1
.end method

.method protected onDrawFrame(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 6
    .param p1, "notUsed"    # Ljavax/microedition/khronos/opengles/GL10;

    .prologue
    const/16 v5, 0xbe2

    .line 345
    const/16 v0, 0x4100

    invoke-static {v0}, Landroid/opengl/GLES20;->glClear(I)V

    .line 347
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mMatrices:Lcom/samsung/musicplus/glwidget/model/Matrices;

    invoke-virtual {v0}, Lcom/samsung/musicplus/glwidget/model/Matrices;->updateMatrices()V

    .line 349
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mVBO:Lcom/samsung/musicplus/glwidget/render/VBO;

    invoke-virtual {v0}, Lcom/samsung/musicplus/glwidget/render/VBO;->bind()V

    .line 351
    invoke-static {v5}, Landroid/opengl/GLES20;->glDisable(I)V

    .line 353
    iget-boolean v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mHasShadow:Z

    if-eqz v0, :cond_0

    .line 354
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mShader:Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;

    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mCamera:Lcom/samsung/musicplus/glwidget/utils/Camera;

    invoke-virtual {v1}, Lcom/samsung/musicplus/glwidget/utils/Camera;->getMatrix()[F

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mMatrices:Lcom/samsung/musicplus/glwidget/model/Matrices;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lcom/samsung/musicplus/glwidget/model/Matrices;->getMatrices(I)[F

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapModel;

    iget-object v4, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mShadow:Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;

    invoke-virtual {v4}, Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->renderBitmap([F[F[Lcom/samsung/musicplus/glwidget/render/Renderable;Landroid/graphics/Bitmap;)V

    .line 358
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mShader:Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;

    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mCamera:Lcom/samsung/musicplus/glwidget/utils/Camera;

    invoke-virtual {v1}, Lcom/samsung/musicplus/glwidget/utils/Camera;->getMatrix()[F

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mMatrices:Lcom/samsung/musicplus/glwidget/model/Matrices;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/samsung/musicplus/glwidget/model/Matrices;->getMatrices(I)[F

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapModel;

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->renderBitmaps([F[F[Lcom/samsung/musicplus/glwidget/render/RenderableBitmap;)V

    .line 360
    invoke-static {v5}, Landroid/opengl/GLES20;->glEnable(I)V

    .line 362
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mShader:Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;

    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mCamera:Lcom/samsung/musicplus/glwidget/utils/Camera;

    invoke-virtual {v1}, Lcom/samsung/musicplus/glwidget/utils/Camera;->getMatrix()[F

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mMatrices:Lcom/samsung/musicplus/glwidget/model/Matrices;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/samsung/musicplus/glwidget/model/Matrices;->getMatrices(I)[F

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mTexts:[Lcom/samsung/musicplus/glwidget/model/ViewModel;

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->renderBitmaps([F[F[Lcom/samsung/musicplus/glwidget/render/RenderableBitmap;)V

    .line 365
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mMarkers:Lcom/samsung/musicplus/glwidget/utils/MarkerViews;

    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mCamera:Lcom/samsung/musicplus/glwidget/utils/Camera;

    iget-object v2, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mShader:Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/musicplus/glwidget/utils/MarkerViews;->render(Lcom/samsung/musicplus/glwidget/utils/Camera;Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;)V

    .line 367
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapModel;

    invoke-static {v0}, Lcom/samsung/musicplus/glwidget/model/ModelUtils;->resetModified([Lcom/samsung/musicplus/glwidget/model/Model;)V

    .line 368
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mTexts:[Lcom/samsung/musicplus/glwidget/model/ViewModel;

    invoke-static {v0}, Lcom/samsung/musicplus/glwidget/model/ModelUtils;->resetModified([Lcom/samsung/musicplus/glwidget/model/Model;)V

    .line 375
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mBackgroundOpacityMask:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 376
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mBackgroundOpacityMask:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 377
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mBackgroundOpacityMask:Landroid/graphics/Bitmap;

    .line 379
    :cond_1
    return-void
.end method

.method protected onSingleTap(FF)Z
    .locals 12
    .param p1, "x"    # F
    .param p2, "y"    # F

    .prologue
    const/high16 v11, 0x40000000    # 2.0f

    const/4 v10, 0x0

    .line 435
    const/high16 v8, 0x3f000000    # 0.5f

    iget v9, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mHeight:I

    int-to-float v9, v9

    mul-float/2addr v8, v9

    sub-float/2addr v8, p2

    float-to-int v8, v8

    invoke-direct {p0, v8}, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->getPSize(I)F

    move-result v6

    .line 436
    .local v6, "pY":F
    float-to-int v8, p1

    invoke-direct {p0, v8}, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->getPSize(I)F

    move-result v5

    .line 439
    .local v5, "pX":F
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    iget-object v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapModel;

    array-length v8, v8

    if-ge v4, v8, :cond_0

    .line 440
    iget-object v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapModel;

    aget-object v1, v8, v4

    .line 441
    .local v1, "cur":Lcom/samsung/musicplus/glwidget/model/BitmapModel;
    invoke-virtual {v1}, Lcom/samsung/musicplus/glwidget/model/BitmapModel;->getWidth()F

    move-result v8

    div-float v3, v8, v11

    .line 442
    .local v3, "halfWidth":F
    invoke-virtual {v1}, Lcom/samsung/musicplus/glwidget/model/BitmapModel;->getHeight()F

    move-result v8

    iget v9, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mPTextHeight:F

    add-float/2addr v8, v9

    div-float v2, v8, v11

    .line 443
    .local v2, "halfHeight":F
    invoke-virtual {v1}, Lcom/samsung/musicplus/glwidget/model/BitmapModel;->isVisible()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-virtual {v1}, Lcom/samsung/musicplus/glwidget/model/BitmapModel;->getX()F

    move-result v8

    sub-float/2addr v8, v5

    invoke-static {v8}, Ljava/lang/Math;->abs(F)F

    move-result v8

    cmpg-float v8, v8, v3

    if-gez v8, :cond_1

    invoke-virtual {v1}, Lcom/samsung/musicplus/glwidget/model/BitmapModel;->getY()F

    move-result v8

    sub-float/2addr v8, v6

    invoke-static {v8}, Ljava/lang/Math;->abs(F)F

    move-result v8

    cmpg-float v8, v8, v2

    if-gez v8, :cond_1

    .line 445
    iget-object v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mParent:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v1}, Lcom/samsung/musicplus/glwidget/model/BitmapModel;->getAdapterIndex()I

    move-result v9

    invoke-virtual {v8, v9}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->fireOnItemClick(I)V

    .line 446
    iget-object v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mConfig:Lcom/samsung/musicplus/glwidget/layout/FlatLayout$Config;

    invoke-virtual {v8}, Lcom/samsung/musicplus/glwidget/layout/FlatLayout$Config;->getClickAnimation()I

    move-result v8

    const/4 v9, -0x1

    if-eq v8, v9, :cond_0

    .line 447
    iget-object v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mParent:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v8}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->getContext()Landroid/content/Context;

    move-result-object v8

    iget-object v9, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mConfig:Lcom/samsung/musicplus/glwidget/layout/FlatLayout$Config;

    invoke-virtual {v9}, Lcom/samsung/musicplus/glwidget/layout/FlatLayout$Config;->getClickAnimation()I

    move-result v9

    invoke-static {v8, v9}, Landroid/animation/AnimatorInflater;->loadAnimator(Landroid/content/Context;I)Landroid/animation/Animator;

    move-result-object v0

    check-cast v0, Landroid/animation/AnimatorSet;

    .line 449
    .local v0, "click":Landroid/animation/AnimatorSet;
    invoke-virtual {v1, v10}, Lcom/samsung/musicplus/glwidget/model/BitmapModel;->setAngle(F)V

    .line 450
    iget-object v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mTexts:[Lcom/samsung/musicplus/glwidget/model/ViewModel;

    aget-object v7, v8, v4

    .line 451
    .local v7, "text":Lcom/samsung/musicplus/glwidget/model/ViewModel;
    invoke-virtual {v7}, Lcom/samsung/musicplus/glwidget/model/ViewModel;->cancelAnimation()V

    .line 452
    invoke-virtual {v7, v10}, Lcom/samsung/musicplus/glwidget/model/ViewModel;->setAngle(F)V

    .line 453
    invoke-virtual {v1, v0}, Lcom/samsung/musicplus/glwidget/model/BitmapModel;->setAnimation(Landroid/animation/AnimatorSet;)V

    .line 454
    invoke-virtual {v0}, Landroid/animation/AnimatorSet;->start()V

    .line 455
    invoke-direct {p0}, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->setAnimationRepaint()V

    .line 460
    .end local v0    # "click":Landroid/animation/AnimatorSet;
    .end local v1    # "cur":Lcom/samsung/musicplus/glwidget/model/BitmapModel;
    .end local v2    # "halfHeight":F
    .end local v3    # "halfWidth":F
    .end local v7    # "text":Lcom/samsung/musicplus/glwidget/model/ViewModel;
    :cond_0
    const/4 v8, 0x1

    return v8

    .line 439
    .restart local v1    # "cur":Lcom/samsung/musicplus/glwidget/model/BitmapModel;
    .restart local v2    # "halfHeight":F
    .restart local v3    # "halfWidth":F
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method

.method protected onSurfaceCreated(Ljavax/microedition/khronos/opengles/GL10;Ljavax/microedition/khronos/egl/EGLConfig;)V
    .locals 7
    .param p1, "notUsed"    # Ljavax/microedition/khronos/opengles/GL10;
    .param p2, "config"    # Ljavax/microedition/khronos/egl/EGLConfig;

    .prologue
    const/4 v6, 0x1

    const/high16 v5, 0x437f0000    # 255.0f

    .line 325
    iput-boolean v6, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mSurfaceRecreated:Z

    .line 327
    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mConfig:Lcom/samsung/musicplus/glwidget/layout/FlatLayout$Config;

    invoke-virtual {v1}, Lcom/samsung/musicplus/glwidget/layout/FlatLayout$Config;->getBackgroundColor()I

    move-result v0

    .line 329
    .local v0, "color":I
    invoke-static {v0}, Landroid/graphics/Color;->red(I)I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v5

    invoke-static {v0}, Landroid/graphics/Color;->green(I)I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v5

    invoke-static {v0}, Landroid/graphics/Color;->blue(I)I

    move-result v3

    int-to-float v3, v3

    div-float/2addr v3, v5

    invoke-static {v0}, Landroid/graphics/Color;->alpha(I)I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v4, v5

    invoke-static {v1, v2, v3, v4}, Landroid/opengl/GLES20;->glClearColor(FFFF)V

    .line 332
    new-instance v1, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;

    iget-object v2, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mParent:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v2}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->getContext()Landroid/content/Context;

    move-result-object v2

    new-array v3, v6, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "QuadOpacityMask"

    aput-object v5, v3, v4

    invoke-direct {v1, v2, v3}, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;-><init>(Landroid/content/Context;[Ljava/lang/String;)V

    iput-object v1, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mShader:Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;

    .line 336
    const/16 v1, 0xb71

    invoke-static {v1}, Landroid/opengl/GLES20;->glEnable(I)V

    .line 337
    const/16 v1, 0x203

    invoke-static {v1}, Landroid/opengl/GLES20;->glDepthFunc(I)V

    .line 338
    const/16 v1, 0x302

    const/16 v2, 0x303

    invoke-static {v1, v2}, Landroid/opengl/GLES20;->glBlendFunc(II)V

    .line 340
    new-instance v1, Lcom/samsung/musicplus/glwidget/render/VBO;

    sget-object v2, Lcom/samsung/musicplus/glwidget/render/Meshes;->TexturedRectangle:[F

    invoke-direct {v1, v2}, Lcom/samsung/musicplus/glwidget/render/VBO;-><init>([F)V

    iput-object v1, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mVBO:Lcom/samsung/musicplus/glwidget/render/VBO;

    .line 341
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 538
    iget-boolean v1, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mAligned:Z

    if-nez v1, :cond_1

    .line 539
    const/4 v0, 0x0

    .line 549
    :cond_0
    :goto_0
    return v0

    .line 541
    :cond_1
    invoke-super {p0, p1}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 542
    .local v0, "res":Z
    iget-boolean v1, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mTouched:Z

    if-nez v1, :cond_0

    iget v1, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mMaxTiltedAngle:I

    if-lez v1, :cond_0

    .line 543
    iget-boolean v1, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mScaleLimitEffect:Z

    if-eqz v1, :cond_2

    .line 544
    invoke-direct {p0}, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->assignReturnScale()V

    goto :goto_0

    .line 546
    :cond_2
    invoke-direct {p0}, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->assignReturnRotation()V

    goto :goto_0
.end method

.method protected positionSelectedView(F)Z
    .locals 6
    .param p1, "dx"    # F

    .prologue
    const/4 v2, 0x0

    .line 669
    invoke-virtual {p0, v2}, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->relIndex(I)I

    move-result v0

    .line 670
    .local v0, "index":I
    iget-object v3, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapModel;

    aget-object v3, v3, v0

    invoke-virtual {v3}, Lcom/samsung/musicplus/glwidget/model/BitmapModel;->getX()F

    move-result v3

    sub-float v1, v3, p1

    .line 671
    .local v1, "x":F
    invoke-direct {p0, v0, v1}, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->positionView(IF)V

    .line 672
    iget v3, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mPSelectedX:F

    sub-float v3, v1, v3

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v3

    const/high16 v4, 0x3f800000    # 1.0f

    sub-float/2addr v3, v4

    iget v4, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mPAlbumSize:F

    iget v5, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mPAlbumSpacing:F

    add-float/2addr v4, v5

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v4, v5

    cmpg-float v3, v3, v4

    if-gez v3, :cond_0

    const/4 v2, 0x1

    :cond_0
    return v2
.end method

.method protected positionView(IIIIF)V
    .locals 7
    .param p1, "dir"    # I
    .param p2, "position"    # I
    .param p3, "prevIndex"    # I
    .param p4, "index"    # I
    .param p5, "dx"    # F

    .prologue
    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    .line 677
    iget-object v4, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapModel;

    aget-object v4, v4, p4

    invoke-virtual {v4}, Lcom/samsung/musicplus/glwidget/model/BitmapModel;->getAngle()F

    move-result v0

    .line 678
    .local v0, "angle":F
    cmpl-float v4, v0, v5

    if-eqz v4, :cond_1

    .line 679
    iget-object v4, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mConfig:Lcom/samsung/musicplus/glwidget/layout/FlatLayout$Config;

    invoke-virtual {v4}, Lcom/samsung/musicplus/glwidget/layout/FlatLayout$Config;->getTiltedK()F

    move-result v1

    .line 680
    .local v1, "k":F
    cmpl-float v4, v0, v5

    if-lez v4, :cond_2

    .line 681
    div-float v4, p5, v1

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    sub-float/2addr v0, v4

    .line 682
    cmpg-float v4, v0, v5

    if-gez v4, :cond_0

    .line 683
    const/4 v0, 0x0

    .line 691
    :cond_0
    :goto_0
    iget-object v4, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapModel;

    aget-object v4, v4, p4

    invoke-virtual {v4, v0, v5, v6, v5}, Lcom/samsung/musicplus/glwidget/model/BitmapModel;->setRotation(FFFF)V

    .line 692
    iget-object v4, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mTexts:[Lcom/samsung/musicplus/glwidget/model/ViewModel;

    aget-object v4, v4, p4

    invoke-virtual {v4, v0, v5, v6, v5}, Lcom/samsung/musicplus/glwidget/model/ViewModel;->setRotation(FFFF)V

    .line 695
    .end local v1    # "k":F
    :cond_1
    iget-object v4, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapModel;

    aget-object v4, v4, p3

    invoke-virtual {v4}, Lcom/samsung/musicplus/glwidget/model/BitmapModel;->getX()F

    move-result v2

    .line 696
    .local v2, "prevX":F
    int-to-float v4, p1

    iget v5, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mPAlbumSize:F

    iget v6, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mPAlbumSpacing:F

    add-float/2addr v5, v6

    mul-float/2addr v4, v5

    add-float v3, v2, v4

    .line 697
    .local v3, "x":F
    invoke-direct {p0, p4, v3}, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->positionView(IF)V

    .line 698
    return-void

    .line 685
    .end local v2    # "prevX":F
    .end local v3    # "x":F
    .restart local v1    # "k":F
    :cond_2
    cmpg-float v4, v0, v5

    if-gez v4, :cond_0

    .line 686
    div-float v4, p5, v1

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    add-float/2addr v0, v4

    .line 687
    cmpl-float v4, v0, v5

    if-lez v4, :cond_0

    .line 688
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected realignView()V
    .locals 13

    .prologue
    .line 703
    iget-object v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->LOG_TAG:Ljava/lang/String;

    const-string v9, "realignView"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 706
    iget-object v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapModel;

    if-nez v8, :cond_8

    const/4 v2, 0x1

    .line 708
    .local v2, "initialAllign":Z
    :goto_0
    const/16 v8, 0xf

    iput v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mFov:I

    .line 710
    iget v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mHeight:I

    iget v9, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mFov:I

    int-to-float v9, v9

    invoke-static {v8, v9}, Lcom/samsung/musicplus/glwidget/utils/FrustumCamera;->getCameraDistance(IF)F

    move-result v8

    iput v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mCameraDistance:F

    .line 713
    iget-object v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mConfig:Lcom/samsung/musicplus/glwidget/layout/FlatLayout$Config;

    invoke-virtual {v8}, Lcom/samsung/musicplus/glwidget/layout/FlatLayout$Config;->getAlbumSize()I

    move-result v8

    iput v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mSAlbumSize:I

    .line 715
    iget v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mSAlbumSize:I

    mul-int/lit8 v8, v8, 0x2

    int-to-float v8, v8

    iput v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mDistanceToProjection:F

    .line 717
    new-instance v8, Lcom/samsung/musicplus/glwidget/utils/CoordConverter$PerspectiveConverter;

    iget v9, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mCameraDistance:F

    iget v10, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mDistanceToProjection:F

    iget v11, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mFov:I

    int-to-float v11, v11

    invoke-direct {v8, v9, v10, v11}, Lcom/samsung/musicplus/glwidget/utils/CoordConverter$PerspectiveConverter;-><init>(FFF)V

    iput-object v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mConverter:Lcom/samsung/musicplus/glwidget/utils/CoordConverter;

    .line 719
    iget v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mSAlbumSize:I

    invoke-direct {p0, v8}, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->getPSize(I)F

    move-result v8

    iput v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mPAlbumSize:F

    .line 722
    iget-object v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mConfig:Lcom/samsung/musicplus/glwidget/layout/FlatLayout$Config;

    invoke-virtual {v8}, Lcom/samsung/musicplus/glwidget/layout/FlatLayout$Config;->getAlbumSpacing()I

    move-result v8

    iput v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mSAlbumSpacing:I

    .line 724
    iget v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mSAlbumSpacing:I

    invoke-direct {p0, v8}, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->getPSize(I)F

    move-result v8

    iput v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mPAlbumSpacing:F

    .line 726
    iget-object v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mConfig:Lcom/samsung/musicplus/glwidget/layout/FlatLayout$Config;

    invoke-virtual {v8}, Lcom/samsung/musicplus/glwidget/layout/FlatLayout$Config;->getMaxTiltedAngle()I

    move-result v8

    iput v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mMaxTiltedAngle:I

    .line 728
    iget-object v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mParent:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v8}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-static {v8}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 731
    .local v1, "inflater":Landroid/view/LayoutInflater;
    iget-object v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mConfig:Lcom/samsung/musicplus/glwidget/layout/FlatLayout$Config;

    invoke-virtual {v8}, Lcom/samsung/musicplus/glwidget/layout/FlatLayout$Config;->getTextLayout()I

    move-result v8

    const/4 v9, 0x0

    invoke-virtual {v1, v8, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/view/ViewGroup;

    .line 735
    .local v7, "view":Landroid/view/ViewGroup;
    new-instance v8, Landroid/view/ViewGroup$LayoutParams;

    const/4 v9, -0x1

    const/4 v10, -0x2

    invoke-direct {v8, v9, v10}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v7, v8}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 738
    iget v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mSAlbumSize:I

    const/high16 v9, 0x40000000    # 2.0f

    invoke-static {v8, v9}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-static {v9, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    invoke-virtual {v7, v8, v9}, Landroid/view/ViewGroup;->measure(II)V

    .line 742
    invoke-virtual {v7}, Landroid/view/ViewGroup;->getMeasuredHeight()I

    move-result v8

    iput v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mSTextHeight:I

    .line 744
    iget v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mSTextHeight:I

    invoke-direct {p0, v8}, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->getPSize(I)F

    move-result v8

    iput v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mPTextHeight:F

    .line 747
    iget v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mPAlbumSize:F

    iget v9, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mPTextHeight:F

    add-float v0, v8, v9

    .line 750
    .local v0, "fullModelHeight":F
    const/high16 v8, 0x40000000    # 2.0f

    div-float v6, v0, v8

    .line 752
    .local v6, "pModelTopY":F
    iget v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mPAlbumSize:F

    const/high16 v9, 0x40000000    # 2.0f

    div-float/2addr v8, v9

    sub-float v8, v6, v8

    iput v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mPAlbY:F

    .line 754
    iget v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mPAlbumSize:F

    sub-float v8, v6, v8

    iget v9, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mPTextHeight:F

    const/high16 v10, 0x40000000    # 2.0f

    div-float/2addr v9, v10

    sub-float/2addr v8, v9

    iput v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mPTextY:F

    .line 757
    iget v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mWidth:I

    iget v9, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mSAlbumSize:I

    iget v10, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mSAlbumSpacing:I

    add-int/2addr v9, v10

    div-int/2addr v8, v9

    add-int/lit8 v3, v8, 0x3

    .line 759
    .local v3, "modelCount":I
    iget-boolean v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mHasShadow:Z

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapModel;

    if-nez v8, :cond_0

    .line 761
    new-instance v8, Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;

    iget-object v9, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mParent:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v9}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->getContext()Landroid/content/Context;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    iget-object v10, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mConfig:Lcom/samsung/musicplus/glwidget/layout/FlatLayout$Config;

    invoke-virtual {v10}, Lcom/samsung/musicplus/glwidget/layout/FlatLayout$Config;->getShadowBitmap()I

    move-result v10

    invoke-static {v9, v10}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v9

    invoke-direct {v8, v9}, Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mShadow:Lcom/samsung/musicplus/glwidget/utils/ShadowBitmap;

    .line 764
    :cond_0
    const/4 v5, 0x0

    .line 765
    .local v5, "oldModelCount":I
    iget-object v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapModel;

    if-eqz v8, :cond_1

    .line 766
    iget-object v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapModel;

    array-length v5, v8

    .line 770
    :cond_1
    iget-object v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapModel;

    if-eqz v8, :cond_2

    iget-object v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapModel;

    array-length v8, v8

    if-eq v8, v3, :cond_3

    .line 771
    :cond_2
    invoke-direct {p0, v1, v3}, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->fillAlbumsArray(Landroid/view/LayoutInflater;I)V

    .line 774
    :cond_3
    iget-object v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mCamera:Lcom/samsung/musicplus/glwidget/utils/Camera;

    iget v9, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mWidth:I

    div-int/lit8 v9, v9, 0x2

    invoke-direct {p0, v9}, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->getPSize(I)F

    move-result v9

    const/4 v10, 0x0

    iget v11, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mCameraDistance:F

    iget v12, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mDistanceToProjection:F

    add-float/2addr v11, v12

    invoke-virtual {v8, v9, v10, v11}, Lcom/samsung/musicplus/glwidget/utils/Camera;->setPosition(FFF)V

    .line 775
    iget-object v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mCamera:Lcom/samsung/musicplus/glwidget/utils/Camera;

    iget v9, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mWidth:I

    div-int/lit8 v9, v9, 0x2

    invoke-direct {p0, v9}, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->getPSize(I)F

    move-result v9

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-virtual {v8, v9, v10, v11}, Lcom/samsung/musicplus/glwidget/utils/Camera;->setLookAt(FFF)V

    .line 776
    iget-object v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mCamera:Lcom/samsung/musicplus/glwidget/utils/Camera;

    iget v9, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mCameraDistance:F

    iget v10, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mCameraDistance:F

    const/high16 v11, 0x40000000    # 2.0f

    iget v12, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mDistanceToProjection:F

    mul-float/2addr v11, v12

    add-float/2addr v10, v11

    invoke-virtual {v8, v9, v10}, Lcom/samsung/musicplus/glwidget/utils/Camera;->setDistance(FF)V

    .line 777
    iget-object v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mCamera:Lcom/samsung/musicplus/glwidget/utils/Camera;

    iget v9, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mWidth:I

    int-to-float v9, v9

    iget v10, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mHeight:I

    int-to-float v10, v10

    invoke-virtual {v8, v9, v10}, Lcom/samsung/musicplus/glwidget/utils/Camera;->setViewport(FF)V

    .line 779
    iget v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mWidth:I

    invoke-direct {p0, v8}, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->getPSize(I)F

    move-result v8

    iput v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mPWidth:F

    .line 780
    iget v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mPAlbumSpacing:F

    iget v9, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mPAlbumSize:F

    const/high16 v10, 0x40000000    # 2.0f

    div-float/2addr v9, v10

    add-float/2addr v8, v9

    iput v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mPHalfSizeSpacing:F

    .line 783
    iget v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mPWidth:F

    iget v9, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mPHalfSizeSpacing:F

    cmpl-float v8, v8, v9

    if-lez v8, :cond_9

    .line 784
    iget v4, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mPHalfSizeSpacing:F

    .line 794
    .local v4, "newPSelectedX":F
    :goto_1
    if-ne v5, v3, :cond_4

    iget v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mPSelectedX:F

    cmpl-float v8, v4, v8

    if-nez v8, :cond_4

    iget-boolean v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mNeedRepositionSelected:Z

    if-eqz v8, :cond_5

    .line 796
    :cond_4
    const/4 v8, 0x0

    invoke-virtual {p0, v8}, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->relIndex(I)I

    move-result v8

    invoke-direct {p0, v8, v4}, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->positionView(IF)V

    .line 798
    :cond_5
    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mNeedRepositionSelected:Z

    .line 800
    iput v4, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mPSelectedX:F

    .line 803
    iget-object v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mAdapter:Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;

    invoke-interface {v8}, Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;->getCount()I

    move-result v8

    iget v9, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mWidth:I

    iget v10, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mSAlbumSize:I

    iget v11, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mSAlbumSpacing:I

    add-int/2addr v10, v11

    div-int/2addr v9, v10

    if-le v8, v9, :cond_a

    iget v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mWidth:I

    iget v9, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mSAlbumSize:I

    div-int/lit8 v9, v9, 0x2

    if-le v8, v9, :cond_a

    const/4 v8, 0x1

    :goto_2
    iput-boolean v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mScrollingEnabled:Z

    .line 805
    const/16 v8, 0xa

    invoke-direct {p0, v8}, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->getPSize(I)F

    move-result v8

    const/high16 v9, 0x41200000    # 10.0f

    div-float/2addr v8, v9

    iput v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mDxK:F

    .line 807
    if-nez v2, :cond_6

    iget-boolean v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mSurfaceRecreated:Z

    if-eqz v8, :cond_7

    .line 808
    :cond_6
    invoke-direct {p0}, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->createBackgroundOpacityMask()V

    .line 810
    :cond_7
    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mSurfaceRecreated:Z

    .line 813
    iget-object v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->LOG_TAG:Ljava/lang/String;

    const-string v9, "realignView end"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 815
    return-void

    .line 706
    .end local v0    # "fullModelHeight":F
    .end local v1    # "inflater":Landroid/view/LayoutInflater;
    .end local v2    # "initialAllign":Z
    .end local v3    # "modelCount":I
    .end local v4    # "newPSelectedX":F
    .end local v5    # "oldModelCount":I
    .end local v6    # "pModelTopY":F
    .end local v7    # "view":Landroid/view/ViewGroup;
    :cond_8
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 786
    .restart local v0    # "fullModelHeight":F
    .restart local v1    # "inflater":Landroid/view/LayoutInflater;
    .restart local v2    # "initialAllign":Z
    .restart local v3    # "modelCount":I
    .restart local v5    # "oldModelCount":I
    .restart local v6    # "pModelTopY":F
    .restart local v7    # "view":Landroid/view/ViewGroup;
    :cond_9
    iget v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mPHalfSizeSpacing:F

    iget v9, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mPWidth:F

    sub-float v4, v8, v9

    .restart local v4    # "newPSelectedX":F
    goto :goto_1

    .line 803
    :cond_a
    const/4 v8, 0x0

    goto :goto_2
.end method

.method public setAdapter(Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;)V
    .locals 4
    .param p1, "adapter"    # Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;

    .prologue
    .line 836
    iget-boolean v2, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mAligned:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mAdapter:Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;

    if-eqz v2, :cond_1

    if-eqz p1, :cond_1

    iget-object v2, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mAdapter:Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;

    invoke-interface {v2}, Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;->getCount()I

    move-result v2

    invoke-interface {p1}, Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;->getCount()I

    move-result v3

    if-le v2, v3, :cond_1

    .line 837
    iget v2, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mPAlbumSize:F

    iget v3, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mPAlbumSpacing:F

    add-float/2addr v2, v3

    iget v3, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mWidth:I

    invoke-direct {p0, v3}, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->getPSize(I)F

    move-result v3

    div-float/2addr v2, v3

    float-to-int v2, v2

    add-int/lit8 v1, v2, 0x1

    .line 841
    .local v1, "visibleViews":I
    invoke-interface {p1}, Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;->getCount()I

    move-result v2

    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->getSelection()I

    move-result v3

    sub-int/2addr v2, v3

    if-gt v2, v1, :cond_0

    .line 842
    invoke-interface {p1}, Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;->getCount()I

    move-result v2

    sub-int/2addr v2, v1

    add-int/lit8 v0, v2, 0x1

    .line 846
    .local v0, "newPosition":I
    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->setSelectedAdapterIndex(I)V

    .line 848
    .end local v0    # "newPosition":I
    :cond_0
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mNeedRepositionSelected:Z

    .line 850
    .end local v1    # "visibleViews":I
    :cond_1
    invoke-super {p0, p1}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->setAdapter(Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;)V

    .line 851
    return-void
.end method

.method protected setModelVisible(IZ)V
    .locals 1
    .param p1, "viewIndex"    # I
    .param p2, "visible"    # Z

    .prologue
    .line 532
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapModel;

    aget-object v0, v0, p1

    invoke-virtual {v0, p2}, Lcom/samsung/musicplus/glwidget/model/BitmapModel;->setVisibility(Z)V

    .line 533
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mTexts:[Lcom/samsung/musicplus/glwidget/model/ViewModel;

    aget-object v0, v0, p1

    invoke-virtual {v0, p2}, Lcom/samsung/musicplus/glwidget/model/ViewModel;->setVisibility(Z)V

    .line 534
    return-void
.end method

.method public setSelection(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 234
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mNeedRepositionSelected:Z

    .line 235
    invoke-super {p0, p1}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->setSelection(I)V

    .line 236
    return-void
.end method

.method protected updateItemData(II)V
    .locals 10
    .param p1, "viewIndex"    # I
    .param p2, "adapterIndex"    # I

    .prologue
    .line 383
    iget-object v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mAdapter:Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;

    iget-object v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mParent:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v8}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-interface {v7, v8, p2}, Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;->getAlbArtUri(Landroid/content/Context;I)Landroid/net/Uri;

    move-result-object v6

    .line 384
    .local v6, "uri":Landroid/net/Uri;
    iget-object v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mAlbums:[Lcom/samsung/musicplus/glwidget/model/BitmapModel;

    aget-object v0, v7, p1

    .line 385
    .local v0, "a":Lcom/samsung/musicplus/glwidget/model/BitmapModel;
    iget-object v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mTexts:[Lcom/samsung/musicplus/glwidget/model/ViewModel;

    aget-object v5, v7, p1

    .line 386
    .local v5, "t":Lcom/samsung/musicplus/glwidget/model/ViewModel;
    iget-object v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mAdapter:Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;

    iget-object v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mParent:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v8}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-virtual {v5}, Lcom/samsung/musicplus/glwidget/model/ViewModel;->getView()Landroid/view/View;

    move-result-object v9

    invoke-interface {v7, v8, v9, p2}, Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;->bindView(Landroid/content/Context;Landroid/view/View;I)V

    .line 388
    iget-object v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mAdapter:Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;

    iget-object v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mParent:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v8}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->getContext()Landroid/content/Context;

    move-result-object v8

    invoke-interface {v7, v8, p2}, Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;->getMarkerViewCount(Landroid/content/Context;I)I

    move-result v3

    .line 389
    .local v3, "markersCount":I
    iget-object v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mAdapter:Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;

    iget-object v8, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mParent:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v8}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->getContext()Landroid/content/Context;

    move-result-object v8

    iget-object v9, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mMarkers:Lcom/samsung/musicplus/glwidget/utils/MarkerViews;

    invoke-virtual {v9, v3}, Lcom/samsung/musicplus/glwidget/utils/MarkerViews;->getBindMarkers(I)[Lcom/samsung/musicplus/glwidget/GLGalleryView$MarkerViewInfo;

    move-result-object v9

    invoke-interface {v7, v8, p2, v9}, Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;->updateMarkerViews(Landroid/content/Context;I[Lcom/samsung/musicplus/glwidget/GLGalleryView$MarkerViewInfo;)V

    .line 390
    iget-object v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mMarkers:Lcom/samsung/musicplus/glwidget/utils/MarkerViews;

    invoke-virtual {v7, p1, v3}, Lcom/samsung/musicplus/glwidget/utils/MarkerViews;->updateMakers(II)V

    .line 392
    invoke-virtual {v5}, Lcom/samsung/musicplus/glwidget/model/ViewModel;->invalidate()V

    .line 393
    const/4 v1, 0x0

    .line 394
    .local v1, "bm":Landroid/graphics/Bitmap;
    if-eqz v6, :cond_0

    .line 395
    invoke-static {}, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->getCache()Lcom/samsung/musicplus/bitmapcache/BitmapCache;

    move-result-object v7

    invoke-virtual {v7, v6}, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->getBitmap(Landroid/net/Uri;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 397
    :cond_0
    invoke-virtual {v0, p2}, Lcom/samsung/musicplus/glwidget/model/BitmapModel;->setAdapterIndex(I)V

    .line 398
    if-eqz v1, :cond_2

    .line 399
    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/glwidget/model/BitmapModel;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 400
    const/4 v7, 0x0

    invoke-virtual {v0, v7}, Lcom/samsung/musicplus/glwidget/model/BitmapModel;->setPublisher(Lcom/samsung/musicplus/bitmapcache/BitmapCache$Publisher;)V

    .line 419
    :cond_1
    :goto_0
    return-void

    .line 403
    :cond_2
    if-eqz v6, :cond_1

    .line 404
    invoke-static {}, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->getCache()Lcom/samsung/musicplus/bitmapcache/BitmapCache;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->getLoader()Lcom/samsung/musicplus/bitmapcache/BitmapCache$Loader;

    move-result-object v7

    invoke-interface {v7, v6}, Lcom/samsung/musicplus/bitmapcache/BitmapCache$Loader;->isRemoteUri(Landroid/net/Uri;)Z

    move-result v2

    .line 406
    .local v2, "isRemoteUri":Z
    if-nez v2, :cond_3

    invoke-virtual {v0}, Lcom/samsung/musicplus/glwidget/model/BitmapModel;->getRenderableBitmap()Landroid/graphics/Bitmap;

    move-result-object v7

    if-nez v7, :cond_4

    .line 408
    :cond_3
    iget-object v7, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mParent:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v7}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->getDefaultAlbumArt()Landroid/graphics/Bitmap;

    move-result-object v7

    invoke-virtual {v0, v7}, Lcom/samsung/musicplus/glwidget/model/BitmapModel;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 412
    :cond_4
    new-instance v4, Lcom/samsung/musicplus/glwidget/layout/FlatLayout$AlbArtPublisher;

    invoke-direct {v4, p0, p2, v0}, Lcom/samsung/musicplus/glwidget/layout/FlatLayout$AlbArtPublisher;-><init>(Lcom/samsung/musicplus/glwidget/layout/FlatLayout;ILcom/samsung/musicplus/glwidget/model/BitmapModel;)V

    .line 414
    .local v4, "publisher":Lcom/samsung/musicplus/bitmapcache/BitmapCache$Publisher;
    invoke-virtual {v0, v4}, Lcom/samsung/musicplus/glwidget/model/BitmapModel;->setPublisher(Lcom/samsung/musicplus/bitmapcache/BitmapCache$Publisher;)V

    .line 416
    invoke-static {}, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->getCache()Lcom/samsung/musicplus/bitmapcache/BitmapCache;

    move-result-object v7

    invoke-virtual {v7, v6, v4}, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->loadBitmap(Landroid/net/Uri;Lcom/samsung/musicplus/bitmapcache/BitmapCache$Publisher;)V

    goto :goto_0
.end method
