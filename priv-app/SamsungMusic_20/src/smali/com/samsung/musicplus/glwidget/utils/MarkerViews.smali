.class public Lcom/samsung/musicplus/glwidget/utils/MarkerViews;
.super Ljava/lang/Object;
.source "MarkerViews.java"


# instance fields
.field private mBindMarkers:[Lcom/samsung/musicplus/glwidget/GLGalleryView$MarkerViewInfo;

.field private mBitmaps:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private mConverter:Lcom/samsung/musicplus/glwidget/utils/CoordConverter;

.field private mInflater:Landroid/view/LayoutInflater;

.field private mMarkers:[Lcom/samsung/musicplus/glwidget/model/BitmapModel;

.field private mMarkersCount:I

.field private mMatrices:Lcom/samsung/musicplus/glwidget/model/Matrices;

.field private mModels:[Lcom/samsung/musicplus/glwidget/model/AnimationModel;

.field private mModelsCount:I

.field private mOriginalModelHeight:F


# direct methods
.method public constructor <init>(FLcom/samsung/musicplus/glwidget/utils/CoordConverter;[Lcom/samsung/musicplus/glwidget/model/AnimationModel;Landroid/view/LayoutInflater;I)V
    .locals 4
    .param p1, "originalModelHeight"    # F
    .param p2, "converter"    # Lcom/samsung/musicplus/glwidget/utils/CoordConverter;
    .param p3, "models"    # [Lcom/samsung/musicplus/glwidget/model/AnimationModel;
    .param p4, "inflater"    # Landroid/view/LayoutInflater;
    .param p5, "markersCount"    # I

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    new-instance v1, Landroid/util/SparseArray;

    invoke-direct {v1}, Landroid/util/SparseArray;-><init>()V

    iput-object v1, p0, Lcom/samsung/musicplus/glwidget/utils/MarkerViews;->mBitmaps:Landroid/util/SparseArray;

    .line 43
    iput-object p2, p0, Lcom/samsung/musicplus/glwidget/utils/MarkerViews;->mConverter:Lcom/samsung/musicplus/glwidget/utils/CoordConverter;

    .line 44
    iput p1, p0, Lcom/samsung/musicplus/glwidget/utils/MarkerViews;->mOriginalModelHeight:F

    .line 45
    iput-object p3, p0, Lcom/samsung/musicplus/glwidget/utils/MarkerViews;->mModels:[Lcom/samsung/musicplus/glwidget/model/AnimationModel;

    .line 46
    iput-object p4, p0, Lcom/samsung/musicplus/glwidget/utils/MarkerViews;->mInflater:Landroid/view/LayoutInflater;

    .line 47
    iput p5, p0, Lcom/samsung/musicplus/glwidget/utils/MarkerViews;->mMarkersCount:I

    .line 48
    array-length v1, p3

    iput v1, p0, Lcom/samsung/musicplus/glwidget/utils/MarkerViews;->mModelsCount:I

    .line 49
    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/utils/MarkerViews;->mModels:[Lcom/samsung/musicplus/glwidget/model/AnimationModel;

    array-length v1, v1

    mul-int/2addr v1, p5

    new-array v1, v1, [Lcom/samsung/musicplus/glwidget/model/BitmapModel;

    iput-object v1, p0, Lcom/samsung/musicplus/glwidget/utils/MarkerViews;->mMarkers:[Lcom/samsung/musicplus/glwidget/model/BitmapModel;

    .line 50
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/utils/MarkerViews;->mMarkers:[Lcom/samsung/musicplus/glwidget/model/BitmapModel;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 51
    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/utils/MarkerViews;->mMarkers:[Lcom/samsung/musicplus/glwidget/model/BitmapModel;

    new-instance v2, Lcom/samsung/musicplus/glwidget/model/BitmapModel;

    invoke-direct {v2}, Lcom/samsung/musicplus/glwidget/model/BitmapModel;-><init>()V

    aput-object v2, v1, v0

    .line 50
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 53
    :cond_0
    new-instance v1, Lcom/samsung/musicplus/glwidget/model/Matrices;

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/samsung/musicplus/glwidget/utils/MarkerViews;->mModels:[Lcom/samsung/musicplus/glwidget/model/AnimationModel;

    array-length v3, v3

    mul-int/2addr v3, p5

    invoke-direct {v1, v2, v3}, Lcom/samsung/musicplus/glwidget/model/Matrices;-><init>(II)V

    iput-object v1, p0, Lcom/samsung/musicplus/glwidget/utils/MarkerViews;->mMatrices:Lcom/samsung/musicplus/glwidget/model/Matrices;

    .line 54
    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/utils/MarkerViews;->mMatrices:Lcom/samsung/musicplus/glwidget/model/Matrices;

    const/4 v2, 0x0

    new-instance v3, Lcom/samsung/musicplus/glwidget/utils/MarkerViews$1;

    invoke-direct {v3, p0}, Lcom/samsung/musicplus/glwidget/utils/MarkerViews$1;-><init>(Lcom/samsung/musicplus/glwidget/utils/MarkerViews;)V

    invoke-virtual {v1, v2, v3}, Lcom/samsung/musicplus/glwidget/model/Matrices;->setTransformation(ILcom/samsung/musicplus/glwidget/model/Matrices$MatrixUpdater;)V

    .line 68
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/glwidget/utils/MarkerViews;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/glwidget/utils/MarkerViews;

    .prologue
    .line 19
    iget v0, p0, Lcom/samsung/musicplus/glwidget/utils/MarkerViews;->mMarkersCount:I

    return v0
.end method

.method static synthetic access$100(Lcom/samsung/musicplus/glwidget/utils/MarkerViews;)F
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/glwidget/utils/MarkerViews;

    .prologue
    .line 19
    iget v0, p0, Lcom/samsung/musicplus/glwidget/utils/MarkerViews;->mOriginalModelHeight:F

    return v0
.end method

.method static synthetic access$200(Lcom/samsung/musicplus/glwidget/utils/MarkerViews;)[Lcom/samsung/musicplus/glwidget/model/AnimationModel;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/glwidget/utils/MarkerViews;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/utils/MarkerViews;->mModels:[Lcom/samsung/musicplus/glwidget/model/AnimationModel;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/musicplus/glwidget/utils/MarkerViews;)[Lcom/samsung/musicplus/glwidget/model/BitmapModel;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/glwidget/utils/MarkerViews;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/utils/MarkerViews;->mMarkers:[Lcom/samsung/musicplus/glwidget/model/BitmapModel;

    return-object v0
.end method

.method private ensureMarkersCount(I)V
    .locals 3
    .param p1, "markersCount"    # I

    .prologue
    .line 157
    iget v0, p0, Lcom/samsung/musicplus/glwidget/utils/MarkerViews;->mMarkersCount:I

    if-le p1, v0, :cond_0

    .line 158
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Too many markers. Update GLGalleryView.MAX_MARKERS_COUNT! count: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 162
    :cond_0
    return-void
.end method

.method private ensurePosition(I)V
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 151
    if-ltz p1, :cond_0

    iget v0, p0, Lcom/samsung/musicplus/glwidget/utils/MarkerViews;->mModelsCount:I

    if-le p1, v0, :cond_1

    .line 152
    :cond_0
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Wrong position: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 154
    :cond_1
    return-void
.end method

.method private getBitmapView(I)Landroid/graphics/Bitmap;
    .locals 6
    .param p1, "resourceId"    # I

    .prologue
    const/4 v3, 0x0

    const/4 v5, 0x0

    .line 137
    const/4 v4, -0x1

    if-ne p1, v4, :cond_1

    move-object v1, v3

    .line 147
    :cond_0
    :goto_0
    return-object v1

    .line 140
    :cond_1
    iget-object v4, p0, Lcom/samsung/musicplus/glwidget/utils/MarkerViews;->mBitmaps:Landroid/util/SparseArray;

    invoke-virtual {v4, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Bitmap;

    .line 141
    .local v1, "res":Landroid/graphics/Bitmap;
    if-nez v1, :cond_0

    .line 142
    invoke-static {v5, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 143
    .local v2, "width":I
    invoke-static {v5, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 144
    .local v0, "height":I
    new-instance v4, Lcom/samsung/musicplus/glwidget/render/BitmapView;

    iget-object v5, p0, Lcom/samsung/musicplus/glwidget/utils/MarkerViews;->mInflater:Landroid/view/LayoutInflater;

    invoke-virtual {v5, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    invoke-direct {v4, v3, v2, v0}, Lcom/samsung/musicplus/glwidget/render/BitmapView;-><init>(Landroid/view/View;II)V

    invoke-virtual {v4}, Lcom/samsung/musicplus/glwidget/render/BitmapView;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 145
    iget-object v3, p0, Lcom/samsung/musicplus/glwidget/utils/MarkerViews;->mBitmaps:Landroid/util/SparseArray;

    invoke-virtual {v3, p1, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_0
.end method

.method private setAlpha()V
    .locals 7

    .prologue
    .line 124
    const/4 v4, 0x0

    .line 125
    .local v4, "offset":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v6, p0, Lcom/samsung/musicplus/glwidget/utils/MarkerViews;->mModels:[Lcom/samsung/musicplus/glwidget/model/AnimationModel;

    array-length v6, v6

    if-ge v0, v6, :cond_2

    .line 126
    iget-object v6, p0, Lcom/samsung/musicplus/glwidget/utils/MarkerViews;->mModels:[Lcom/samsung/musicplus/glwidget/model/AnimationModel;

    aget-object v3, v6, v0

    .line 127
    .local v3, "m":Lcom/samsung/musicplus/glwidget/model/AnimationModel;
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_1
    iget v6, p0, Lcom/samsung/musicplus/glwidget/utils/MarkerViews;->mMarkersCount:I

    if-ge v1, v6, :cond_1

    .line 128
    iget-object v6, p0, Lcom/samsung/musicplus/glwidget/utils/MarkerViews;->mMarkers:[Lcom/samsung/musicplus/glwidget/model/BitmapModel;

    add-int/lit8 v5, v4, 0x1

    .end local v4    # "offset":I
    .local v5, "offset":I
    aget-object v2, v6, v4

    .line 129
    .local v2, "k":Lcom/samsung/musicplus/glwidget/model/BitmapModel;
    if-eqz v2, :cond_0

    if-eqz v3, :cond_0

    move-object v6, v3

    .line 130
    check-cast v6, Lcom/samsung/musicplus/glwidget/render/Renderable;

    invoke-interface {v6}, Lcom/samsung/musicplus/glwidget/render/Renderable;->getAlpha()F

    move-result v6

    invoke-virtual {v2, v6}, Lcom/samsung/musicplus/glwidget/model/BitmapModel;->setAlpha(F)V

    .line 127
    :cond_0
    add-int/lit8 v1, v1, 0x1

    move v4, v5

    .end local v5    # "offset":I
    .restart local v4    # "offset":I
    goto :goto_1

    .line 125
    .end local v2    # "k":Lcom/samsung/musicplus/glwidget/model/BitmapModel;
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 134
    .end local v1    # "j":I
    .end local v3    # "m":Lcom/samsung/musicplus/glwidget/model/AnimationModel;
    :cond_2
    return-void
.end method

.method private updateBitmap(Lcom/samsung/musicplus/glwidget/model/BitmapModel;I)Lcom/samsung/musicplus/glwidget/model/BitmapModel;
    .locals 1
    .param p1, "model"    # Lcom/samsung/musicplus/glwidget/model/BitmapModel;
    .param p2, "resourceId"    # I

    .prologue
    .line 106
    if-nez p1, :cond_0

    .line 107
    new-instance p1, Lcom/samsung/musicplus/glwidget/model/BitmapModel;

    .end local p1    # "model":Lcom/samsung/musicplus/glwidget/model/BitmapModel;
    invoke-direct {p1}, Lcom/samsung/musicplus/glwidget/model/BitmapModel;-><init>()V

    .line 109
    .restart local p1    # "model":Lcom/samsung/musicplus/glwidget/model/BitmapModel;
    :cond_0
    invoke-direct {p0, p2}, Lcom/samsung/musicplus/glwidget/utils/MarkerViews;->getBitmapView(I)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/samsung/musicplus/glwidget/model/BitmapModel;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 110
    return-object p1
.end method


# virtual methods
.method public getBindMarkers(I)[Lcom/samsung/musicplus/glwidget/GLGalleryView$MarkerViewInfo;
    .locals 3
    .param p1, "markersCount"    # I

    .prologue
    .line 71
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/glwidget/utils/MarkerViews;->ensureMarkersCount(I)V

    .line 72
    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/utils/MarkerViews;->mBindMarkers:[Lcom/samsung/musicplus/glwidget/GLGalleryView$MarkerViewInfo;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/utils/MarkerViews;->mBindMarkers:[Lcom/samsung/musicplus/glwidget/GLGalleryView$MarkerViewInfo;

    array-length v1, v1

    if-ge v1, p1, :cond_1

    .line 73
    :cond_0
    new-array v1, p1, [Lcom/samsung/musicplus/glwidget/GLGalleryView$MarkerViewInfo;

    iput-object v1, p0, Lcom/samsung/musicplus/glwidget/utils/MarkerViews;->mBindMarkers:[Lcom/samsung/musicplus/glwidget/GLGalleryView$MarkerViewInfo;

    .line 74
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p1, :cond_1

    .line 75
    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/utils/MarkerViews;->mBindMarkers:[Lcom/samsung/musicplus/glwidget/GLGalleryView$MarkerViewInfo;

    new-instance v2, Lcom/samsung/musicplus/glwidget/GLGalleryView$MarkerViewInfo;

    invoke-direct {v2}, Lcom/samsung/musicplus/glwidget/GLGalleryView$MarkerViewInfo;-><init>()V

    aput-object v2, v1, v0

    .line 74
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 78
    .end local v0    # "i":I
    :cond_1
    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/utils/MarkerViews;->mBindMarkers:[Lcom/samsung/musicplus/glwidget/GLGalleryView$MarkerViewInfo;

    return-object v1
.end method

.method public render(Lcom/samsung/musicplus/glwidget/utils/Camera;Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;)V
    .locals 3
    .param p1, "camera"    # Lcom/samsung/musicplus/glwidget/utils/Camera;
    .param p2, "shader"    # Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;

    .prologue
    .line 114
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/utils/MarkerViews;->mMatrices:Lcom/samsung/musicplus/glwidget/model/Matrices;

    invoke-virtual {v0}, Lcom/samsung/musicplus/glwidget/model/Matrices;->updateMatrices()V

    .line 115
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/utils/MarkerViews;->mMarkers:[Lcom/samsung/musicplus/glwidget/model/BitmapModel;

    invoke-static {v0}, Lcom/samsung/musicplus/glwidget/model/ModelUtils;->resetModified([Lcom/samsung/musicplus/glwidget/model/Model;)V

    .line 116
    invoke-direct {p0}, Lcom/samsung/musicplus/glwidget/utils/MarkerViews;->setAlpha()V

    .line 117
    invoke-virtual {p1}, Lcom/samsung/musicplus/glwidget/utils/Camera;->getMatrix()[F

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/utils/MarkerViews;->mMatrices:Lcom/samsung/musicplus/glwidget/model/Matrices;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/samsung/musicplus/glwidget/model/Matrices;->getMatrices(I)[F

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/glwidget/utils/MarkerViews;->mMarkers:[Lcom/samsung/musicplus/glwidget/model/BitmapModel;

    invoke-virtual {p2, v0, v1, v2}, Lcom/samsung/musicplus/glwidget/render/LayeredModelShader;->renderBitmaps([F[F[Lcom/samsung/musicplus/glwidget/render/RenderableBitmap;)V

    .line 118
    return-void
.end method

.method public updateMakers(II)V
    .locals 10
    .param p1, "position"    # I
    .param p2, "markersCount"    # I

    .prologue
    .line 82
    invoke-direct {p0, p2}, Lcom/samsung/musicplus/glwidget/utils/MarkerViews;->ensureMarkersCount(I)V

    .line 83
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/glwidget/utils/MarkerViews;->ensurePosition(I)V

    .line 84
    iget v7, p0, Lcom/samsung/musicplus/glwidget/utils/MarkerViews;->mMarkersCount:I

    mul-int v3, p1, v7

    .line 85
    .local v3, "offset":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, p2, :cond_1

    .line 86
    iget-object v7, p0, Lcom/samsung/musicplus/glwidget/utils/MarkerViews;->mBindMarkers:[Lcom/samsung/musicplus/glwidget/GLGalleryView$MarkerViewInfo;

    aget-object v7, v7, v2

    iget v7, v7, Lcom/samsung/musicplus/glwidget/GLGalleryView$MarkerViewInfo;->resourceId:I

    const/4 v8, -0x1

    if-eq v7, v8, :cond_0

    .line 87
    iget-object v7, p0, Lcom/samsung/musicplus/glwidget/utils/MarkerViews;->mMarkers:[Lcom/samsung/musicplus/glwidget/model/BitmapModel;

    iget-object v8, p0, Lcom/samsung/musicplus/glwidget/utils/MarkerViews;->mMarkers:[Lcom/samsung/musicplus/glwidget/model/BitmapModel;

    aget-object v8, v8, v3

    iget-object v9, p0, Lcom/samsung/musicplus/glwidget/utils/MarkerViews;->mBindMarkers:[Lcom/samsung/musicplus/glwidget/GLGalleryView$MarkerViewInfo;

    aget-object v9, v9, v2

    iget v9, v9, Lcom/samsung/musicplus/glwidget/GLGalleryView$MarkerViewInfo;->resourceId:I

    invoke-direct {p0, v8, v9}, Lcom/samsung/musicplus/glwidget/utils/MarkerViews;->updateBitmap(Lcom/samsung/musicplus/glwidget/model/BitmapModel;I)Lcom/samsung/musicplus/glwidget/model/BitmapModel;

    move-result-object v8

    aput-object v8, v7, v3

    .line 89
    iget-object v7, p0, Lcom/samsung/musicplus/glwidget/utils/MarkerViews;->mMarkers:[Lcom/samsung/musicplus/glwidget/model/BitmapModel;

    aget-object v7, v7, v3

    invoke-virtual {v7}, Lcom/samsung/musicplus/glwidget/model/BitmapModel;->getRenderableBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 90
    .local v0, "bm":Landroid/graphics/Bitmap;
    iget-object v7, p0, Lcom/samsung/musicplus/glwidget/utils/MarkerViews;->mBindMarkers:[Lcom/samsung/musicplus/glwidget/GLGalleryView$MarkerViewInfo;

    aget-object v7, v7, v2

    iget v5, v7, Lcom/samsung/musicplus/glwidget/GLGalleryView$MarkerViewInfo;->x:F

    .line 91
    .local v5, "x":F
    iget-object v7, p0, Lcom/samsung/musicplus/glwidget/utils/MarkerViews;->mBindMarkers:[Lcom/samsung/musicplus/glwidget/GLGalleryView$MarkerViewInfo;

    aget-object v7, v7, v2

    iget v6, v7, Lcom/samsung/musicplus/glwidget/GLGalleryView$MarkerViewInfo;->y:F

    .line 92
    .local v6, "y":F
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    .line 93
    .local v4, "w":I
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    .line 94
    .local v1, "h":I
    iget-object v7, p0, Lcom/samsung/musicplus/glwidget/utils/MarkerViews;->mMarkers:[Lcom/samsung/musicplus/glwidget/model/BitmapModel;

    aget-object v7, v7, v3

    invoke-virtual {v7, v5}, Lcom/samsung/musicplus/glwidget/model/BitmapModel;->setX(F)V

    .line 95
    iget-object v7, p0, Lcom/samsung/musicplus/glwidget/utils/MarkerViews;->mMarkers:[Lcom/samsung/musicplus/glwidget/model/BitmapModel;

    aget-object v7, v7, v3

    invoke-virtual {v7, v6}, Lcom/samsung/musicplus/glwidget/model/BitmapModel;->setY(F)V

    .line 96
    iget-object v7, p0, Lcom/samsung/musicplus/glwidget/utils/MarkerViews;->mMarkers:[Lcom/samsung/musicplus/glwidget/model/BitmapModel;

    aget-object v7, v7, v3

    iget-object v8, p0, Lcom/samsung/musicplus/glwidget/utils/MarkerViews;->mConverter:Lcom/samsung/musicplus/glwidget/utils/CoordConverter;

    invoke-interface {v8, v4}, Lcom/samsung/musicplus/glwidget/utils/CoordConverter;->getPSize(I)F

    move-result v8

    iget-object v9, p0, Lcom/samsung/musicplus/glwidget/utils/MarkerViews;->mConverter:Lcom/samsung/musicplus/glwidget/utils/CoordConverter;

    invoke-interface {v9, v1}, Lcom/samsung/musicplus/glwidget/utils/CoordConverter;->getPSize(I)F

    move-result v9

    invoke-virtual {v7, v8, v9}, Lcom/samsung/musicplus/glwidget/model/BitmapModel;->setSize(FF)V

    .line 97
    iget-object v7, p0, Lcom/samsung/musicplus/glwidget/utils/MarkerViews;->mMarkers:[Lcom/samsung/musicplus/glwidget/model/BitmapModel;

    aget-object v7, v7, v3

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Lcom/samsung/musicplus/glwidget/model/BitmapModel;->setVisibility(Z)V

    .line 101
    .end local v0    # "bm":Landroid/graphics/Bitmap;
    .end local v1    # "h":I
    .end local v4    # "w":I
    .end local v5    # "x":F
    .end local v6    # "y":F
    :goto_1
    add-int/lit8 v3, v3, 0x1

    .line 85
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 99
    :cond_0
    iget-object v7, p0, Lcom/samsung/musicplus/glwidget/utils/MarkerViews;->mMarkers:[Lcom/samsung/musicplus/glwidget/model/BitmapModel;

    aget-object v7, v7, v3

    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Lcom/samsung/musicplus/glwidget/model/BitmapModel;->setVisibility(Z)V

    goto :goto_1

    .line 103
    :cond_1
    return-void
.end method
