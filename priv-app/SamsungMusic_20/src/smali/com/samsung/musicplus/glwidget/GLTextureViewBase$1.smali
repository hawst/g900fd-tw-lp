.class Lcom/samsung/musicplus/glwidget/GLTextureViewBase$1;
.super Ljava/lang/Object;
.source "GLTextureViewBase.java"

# interfaces
.implements Landroid/view/TextureView$SurfaceTextureListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/glwidget/GLTextureViewBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/glwidget/GLTextureViewBase;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/glwidget/GLTextureViewBase;)V
    .locals 0

    .prologue
    .line 141
    iput-object p1, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase$1;->this$0:Lcom/samsung/musicplus/glwidget/GLTextureViewBase;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSurfaceTextureAvailable(Landroid/graphics/SurfaceTexture;II)V
    .locals 4
    .param p1, "texture"    # Landroid/graphics/SurfaceTexture;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 172
    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase$1;->this$0:Lcom/samsung/musicplus/glwidget/GLTextureViewBase;

    const/4 v2, 0x0

    # setter for: Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mSurfaceDestroyed:Z
    invoke-static {v1, v2}, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->access$002(Lcom/samsung/musicplus/glwidget/GLTextureViewBase;Z)Z

    .line 173
    # getter for: Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->access$100()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onSurfaceTextureAvailable width: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " height: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase$1;->this$0:Lcom/samsung/musicplus/glwidget/GLTextureViewBase;

    invoke-virtual {v1}, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->startRenderingThread()V

    .line 176
    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase$1;->this$0:Lcom/samsung/musicplus/glwidget/GLTextureViewBase;

    # getter for: Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->access$200(Lcom/samsung/musicplus/glwidget/GLTextureViewBase;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 177
    .local v0, "msg":Landroid/os/Message;
    iput p2, v0, Landroid/os/Message;->arg1:I

    .line 178
    iput p3, v0, Landroid/os/Message;->arg2:I

    .line 179
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 180
    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase$1;->this$0:Lcom/samsung/musicplus/glwidget/GLTextureViewBase;

    # getter for: Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->access$200(Lcom/samsung/musicplus/glwidget/GLTextureViewBase;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 181
    return-void
.end method

.method public onSurfaceTextureDestroyed(Landroid/graphics/SurfaceTexture;)Z
    .locals 4
    .param p1, "texture"    # Landroid/graphics/SurfaceTexture;

    .prologue
    const/4 v3, 0x1

    .line 161
    # getter for: Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->access$100()Ljava/lang/String;

    move-result-object v1

    const-string v2, "onSurfaceTextureDestroyed"

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase$1;->this$0:Lcom/samsung/musicplus/glwidget/GLTextureViewBase;

    const/4 v2, 0x0

    # setter for: Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mSurfaceCreated:Z
    invoke-static {v1, v2}, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->access$302(Lcom/samsung/musicplus/glwidget/GLTextureViewBase;Z)Z

    .line 163
    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase$1;->this$0:Lcom/samsung/musicplus/glwidget/GLTextureViewBase;

    # setter for: Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mSurfaceDestroyed:Z
    invoke-static {v1, v3}, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->access$002(Lcom/samsung/musicplus/glwidget/GLTextureViewBase;Z)Z

    .line 164
    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase$1;->this$0:Lcom/samsung/musicplus/glwidget/GLTextureViewBase;

    # getter for: Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->access$200(Lcom/samsung/musicplus/glwidget/GLTextureViewBase;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 165
    .local v0, "msg":Landroid/os/Message;
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 166
    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase$1;->this$0:Lcom/samsung/musicplus/glwidget/GLTextureViewBase;

    # getter for: Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->access$200(Lcom/samsung/musicplus/glwidget/GLTextureViewBase;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 167
    return v3
.end method

.method public onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V
    .locals 5
    .param p1, "texture"    # Landroid/graphics/SurfaceTexture;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    const/4 v4, 0x0

    .line 150
    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase$1;->this$0:Lcom/samsung/musicplus/glwidget/GLTextureViewBase;

    # setter for: Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mSurfaceDestroyed:Z
    invoke-static {v1, v4}, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->access$002(Lcom/samsung/musicplus/glwidget/GLTextureViewBase;Z)Z

    .line 151
    # getter for: Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->access$100()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onSurfaceTextureSizeChanged width: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " height: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase$1;->this$0:Lcom/samsung/musicplus/glwidget/GLTextureViewBase;

    # getter for: Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->access$200(Lcom/samsung/musicplus/glwidget/GLTextureViewBase;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 153
    .local v0, "msg":Landroid/os/Message;
    iput p2, v0, Landroid/os/Message;->arg1:I

    .line 154
    iput p3, v0, Landroid/os/Message;->arg2:I

    .line 155
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 156
    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase$1;->this$0:Lcom/samsung/musicplus/glwidget/GLTextureViewBase;

    # getter for: Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->access$200(Lcom/samsung/musicplus/glwidget/GLTextureViewBase;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 157
    return-void
.end method

.method public onSurfaceTextureUpdated(Landroid/graphics/SurfaceTexture;)V
    .locals 0
    .param p1, "texture"    # Landroid/graphics/SurfaceTexture;

    .prologue
    .line 146
    return-void
.end method
