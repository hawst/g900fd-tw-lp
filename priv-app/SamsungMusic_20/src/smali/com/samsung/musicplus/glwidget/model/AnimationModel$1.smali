.class Lcom/samsung/musicplus/glwidget/model/AnimationModel$1;
.super Landroid/os/Handler;
.source "AnimationModel.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/glwidget/model/AnimationModel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/glwidget/model/AnimationModel;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/glwidget/model/AnimationModel;)V
    .locals 0

    .prologue
    .line 39
    iput-object p1, p0, Lcom/samsung/musicplus/glwidget/model/AnimationModel$1;->this$0:Lcom/samsung/musicplus/glwidget/model/AnimationModel;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public dispatchMessage(Landroid/os/Message;)V
    .locals 10
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 43
    invoke-super {p0, p1}, Landroid/os/Handler;->dispatchMessage(Landroid/os/Message;)V

    .line 45
    iget-object v5, p0, Lcom/samsung/musicplus/glwidget/model/AnimationModel$1;->this$0:Lcom/samsung/musicplus/glwidget/model/AnimationModel;

    iput-boolean v1, v5, Lcom/samsung/musicplus/glwidget/model/AnimationModel;->mIsAnimationFinished:Z

    .line 46
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    iget-object v5, p0, Lcom/samsung/musicplus/glwidget/model/AnimationModel$1;->this$0:Lcom/samsung/musicplus/glwidget/model/AnimationModel;

    # getter for: Lcom/samsung/musicplus/glwidget/model/AnimationModel;->mAniStartTime:J
    invoke-static {v5}, Lcom/samsung/musicplus/glwidget/model/AnimationModel;->access$000(Lcom/samsung/musicplus/glwidget/model/AnimationModel;)J

    move-result-wide v8

    sub-long v2, v6, v8

    .line 47
    .local v2, "time":J
    const/high16 v0, 0x3f800000    # 1.0f

    .line 48
    .local v0, "delta":F
    iget-object v5, p0, Lcom/samsung/musicplus/glwidget/model/AnimationModel$1;->this$0:Lcom/samsung/musicplus/glwidget/model/AnimationModel;

    # getter for: Lcom/samsung/musicplus/glwidget/model/AnimationModel;->mDuration:I
    invoke-static {v5}, Lcom/samsung/musicplus/glwidget/model/AnimationModel;->access$100(Lcom/samsung/musicplus/glwidget/model/AnimationModel;)I

    move-result v5

    int-to-long v6, v5

    cmp-long v5, v2, v6

    if-gtz v5, :cond_0

    .line 49
    iget-object v5, p0, Lcom/samsung/musicplus/glwidget/model/AnimationModel$1;->this$0:Lcom/samsung/musicplus/glwidget/model/AnimationModel;

    # getter for: Lcom/samsung/musicplus/glwidget/model/AnimationModel;->mInterpolator:Landroid/view/animation/AccelerateDecelerateInterpolator;
    invoke-static {v5}, Lcom/samsung/musicplus/glwidget/model/AnimationModel;->access$200(Lcom/samsung/musicplus/glwidget/model/AnimationModel;)Landroid/view/animation/AccelerateDecelerateInterpolator;

    move-result-object v5

    long-to-float v6, v2

    iget-object v7, p0, Lcom/samsung/musicplus/glwidget/model/AnimationModel$1;->this$0:Lcom/samsung/musicplus/glwidget/model/AnimationModel;

    # getter for: Lcom/samsung/musicplus/glwidget/model/AnimationModel;->mDuration:I
    invoke-static {v7}, Lcom/samsung/musicplus/glwidget/model/AnimationModel;->access$100(Lcom/samsung/musicplus/glwidget/model/AnimationModel;)I

    move-result v7

    int-to-float v7, v7

    div-float/2addr v6, v7

    invoke-virtual {v5, v6}, Landroid/view/animation/AccelerateDecelerateInterpolator;->getInterpolation(F)F

    move-result v0

    .line 52
    :cond_0
    iget v5, p1, Landroid/os/Message;->what:I

    packed-switch v5, :pswitch_data_0

    .line 66
    :cond_1
    :goto_0
    return-void

    .line 54
    :pswitch_0
    iget-object v5, p0, Lcom/samsung/musicplus/glwidget/model/AnimationModel$1;->this$0:Lcom/samsung/musicplus/glwidget/model/AnimationModel;

    iget-object v6, p0, Lcom/samsung/musicplus/glwidget/model/AnimationModel$1;->this$0:Lcom/samsung/musicplus/glwidget/model/AnimationModel;

    # getter for: Lcom/samsung/musicplus/glwidget/model/AnimationModel;->mRestoreScaleF:F
    invoke-static {v6}, Lcom/samsung/musicplus/glwidget/model/AnimationModel;->access$300(Lcom/samsung/musicplus/glwidget/model/AnimationModel;)F

    move-result v6

    const/high16 v7, 0x3f800000    # 1.0f

    sub-float/2addr v7, v0

    mul-float/2addr v6, v7

    iput v6, v5, Lcom/samsung/musicplus/glwidget/model/AnimationModel;->mScaleF:F

    .line 55
    iget-object v5, p0, Lcom/samsung/musicplus/glwidget/model/AnimationModel$1;->this$0:Lcom/samsung/musicplus/glwidget/model/AnimationModel;

    iget-object v6, p0, Lcom/samsung/musicplus/glwidget/model/AnimationModel$1;->this$0:Lcom/samsung/musicplus/glwidget/model/AnimationModel;

    iget v6, v6, Lcom/samsung/musicplus/glwidget/model/AnimationModel;->mScaleF:F

    const/4 v7, 0x0

    cmpl-float v6, v6, v7

    if-eqz v6, :cond_2

    move v1, v4

    :cond_2
    iput-boolean v1, v5, Lcom/samsung/musicplus/glwidget/model/AnimationModel;->mIsAnimationFinished:Z

    .line 56
    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/model/AnimationModel$1;->this$0:Lcom/samsung/musicplus/glwidget/model/AnimationModel;

    invoke-virtual {v1}, Lcom/samsung/musicplus/glwidget/model/AnimationModel;->setModified()V

    .line 58
    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/model/AnimationModel$1;->this$0:Lcom/samsung/musicplus/glwidget/model/AnimationModel;

    iget-boolean v1, v1, Lcom/samsung/musicplus/glwidget/model/AnimationModel;->mIsAnimationFinished:Z

    if-nez v1, :cond_1

    .line 59
    invoke-virtual {p0, v4}, Lcom/samsung/musicplus/glwidget/model/AnimationModel$1;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 52
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
