.class Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$PositionInfo;
.super Ljava/lang/Object;
.source "GalleryLayoutBase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PositionInfo"
.end annotation


# instance fields
.field mDir:I

.field mIndex:I

.field mPosition:I

.field mPrevIndex:I


# direct methods
.method public constructor <init>(I)V
    .locals 0
    .param p1, "dir"    # I

    .prologue
    .line 1408
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1409
    iput p1, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$PositionInfo;->mDir:I

    .line 1410
    return-void
.end method


# virtual methods
.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 1414
    const-string v0, "dir: %d position: %d prevIndex: %d index: %d"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget v3, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$PositionInfo;->mDir:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget v3, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$PositionInfo;->mPosition:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget v3, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$PositionInfo;->mPrevIndex:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget v3, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$PositionInfo;->mIndex:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
