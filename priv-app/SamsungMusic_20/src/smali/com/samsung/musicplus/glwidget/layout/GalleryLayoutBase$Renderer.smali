.class Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$Renderer;
.super Ljava/lang/Object;
.source "GalleryLayoutBase.java"

# interfaces
.implements Landroid/opengl/GLSurfaceView$Renderer;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "Renderer"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;


# direct methods
.method private constructor <init>(Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;)V
    .locals 0

    .prologue
    .line 1196
    iput-object p1, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$Renderer;->this$0:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;
    .param p2, "x1"    # Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$1;

    .prologue
    .line 1196
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$Renderer;-><init>(Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;)V

    return-void
.end method


# virtual methods
.method public onDrawFrame(Ljavax/microedition/khronos/opengles/GL10;)V
    .locals 6
    .param p1, "notUsed"    # Ljavax/microedition/khronos/opengles/GL10;

    .prologue
    .line 1200
    iget-object v3, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$Renderer;->this$0:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;

    monitor-enter v3

    .line 1201
    :try_start_0
    iget-object v2, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$Renderer;->this$0:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;

    iget-object v2, v2, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAdapter:Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$Renderer;->this$0:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;

    iget-object v2, v2, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAdapter:Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;

    invoke-interface {v2}, Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;->getCount()I

    move-result v2

    if-lez v2, :cond_0

    .line 1202
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 1203
    .local v0, "startTime":J
    iget-object v2, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$Renderer;->this$0:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;

    invoke-virtual {v2, p1}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->onDrawFrame(Ljavax/microedition/khronos/opengles/GL10;)V

    .line 1204
    iget-object v2, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$Renderer;->this$0:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;

    # getter for: Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mPerformanceTestInProgress:Z
    invoke-static {v2}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->access$200(Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1205
    iget-object v2, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$Renderer;->this$0:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    sub-long/2addr v4, v0

    # += operator for: Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mRenderTime:I
    invoke-static {v2, v4, v5}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->access$314(Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;J)I

    .line 1206
    iget-object v2, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$Renderer;->this$0:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;

    # operator++ for: Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mFramesCount:I
    invoke-static {v2}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->access$408(Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;)I

    .line 1207
    iget-object v2, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$Renderer;->this$0:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;

    # getter for: Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mPerformanceTestLock:Ljava/lang/Object;
    invoke-static {v2}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->access$500(Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;)Ljava/lang/Object;

    move-result-object v4

    monitor-enter v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1209
    :try_start_1
    iget-object v2, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$Renderer;->this$0:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;

    # getter for: Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mPerformanceTestLock:Ljava/lang/Object;
    invoke-static {v2}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->access$500(Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->notify()V

    .line 1210
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1213
    .end local v0    # "startTime":J
    :cond_0
    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 1214
    return-void

    .line 1210
    .restart local v0    # "startTime":J
    :catchall_0
    move-exception v2

    :try_start_3
    monitor-exit v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :try_start_4
    throw v2

    .line 1213
    .end local v0    # "startTime":J
    :catchall_1
    move-exception v2

    monitor-exit v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v2
.end method

.method public onSurfaceChanged(Ljavax/microedition/khronos/opengles/GL10;II)V
    .locals 4
    .param p1, "notUsed"    # Ljavax/microedition/khronos/opengles/GL10;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    const/4 v3, 0x0

    .line 1219
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$Renderer;->this$0:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;

    iget-object v0, v0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onSurfaceChanged width: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " height: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1222
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$Renderer;->this$0:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;

    iput p2, v0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mWidth:I

    .line 1223
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$Renderer;->this$0:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;

    iput p3, v0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mHeight:I

    .line 1225
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$Renderer;->this$0:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;

    iget v0, v0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mWidth:I

    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$Renderer;->this$0:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;

    iget v1, v1, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mHeight:I

    invoke-static {v3, v3, v0, v1}, Landroid/opengl/GLES20;->glViewport(IIII)V

    .line 1227
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$Renderer;->this$0:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;

    iput-boolean v3, v0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAligned:Z

    .line 1228
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$Renderer;->this$0:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;

    iget-object v0, v0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mParent:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    new-instance v1, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$Renderer$1;

    invoke-direct {v1, p0, p2, p3}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$Renderer$1;-><init>(Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$Renderer;II)V

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->post(Ljava/lang/Runnable;)Z

    .line 1234
    return-void
.end method

.method public onSurfaceCreated(Ljavax/microedition/khronos/opengles/GL10;Ljavax/microedition/khronos/egl/EGLConfig;)V
    .locals 2
    .param p1, "notUsed"    # Ljavax/microedition/khronos/opengles/GL10;
    .param p2, "config"    # Ljavax/microedition/khronos/egl/EGLConfig;

    .prologue
    .line 1239
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$Renderer;->this$0:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;

    iget-object v0, v0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->LOG_TAG:Ljava/lang/String;

    const-string v1, "onSurfaceCreated"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1242
    invoke-static {p1, p2}, Lcom/samsung/musicplus/glwidget/utils/GLDebug;->debugSurfaceFormat(Ljavax/microedition/khronos/opengles/GL10;Ljavax/microedition/khronos/egl/EGLConfig;)V

    .line 1244
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$Renderer;->this$0:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAligned:Z

    .line 1245
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$Renderer;->this$0:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mSurfaceRecreated:Z

    .line 1246
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$Renderer;->this$0:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->onSurfaceCreated(Ljavax/microedition/khronos/opengles/GL10;Ljavax/microedition/khronos/egl/EGLConfig;)V

    .line 1247
    return-void
.end method
