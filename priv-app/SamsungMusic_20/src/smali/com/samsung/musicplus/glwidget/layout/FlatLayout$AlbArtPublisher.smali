.class Lcom/samsung/musicplus/glwidget/layout/FlatLayout$AlbArtPublisher;
.super Ljava/lang/Object;
.source "FlatLayout.java"

# interfaces
.implements Lcom/samsung/musicplus/bitmapcache/BitmapCache$Publisher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/glwidget/layout/FlatLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AlbArtPublisher"
.end annotation


# instance fields
.field private mAdapterIdx:I

.field private mAlbumModel:Lcom/samsung/musicplus/glwidget/model/BitmapModel;

.field final synthetic this$0:Lcom/samsung/musicplus/glwidget/layout/FlatLayout;


# direct methods
.method public constructor <init>(Lcom/samsung/musicplus/glwidget/layout/FlatLayout;ILcom/samsung/musicplus/glwidget/model/BitmapModel;)V
    .locals 0
    .param p2, "adapterIdx"    # I
    .param p3, "albumModel"    # Lcom/samsung/musicplus/glwidget/model/BitmapModel;

    .prologue
    .line 297
    iput-object p1, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout$AlbArtPublisher;->this$0:Lcom/samsung/musicplus/glwidget/layout/FlatLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 298
    iput p2, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout$AlbArtPublisher;->mAdapterIdx:I

    .line 299
    iput-object p3, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout$AlbArtPublisher;->mAlbumModel:Lcom/samsung/musicplus/glwidget/model/BitmapModel;

    .line 300
    return-void
.end method

.method private stillProperModel()Z
    .locals 2

    .prologue
    .line 303
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout$AlbArtPublisher;->mAlbumModel:Lcom/samsung/musicplus/glwidget/model/BitmapModel;

    invoke-virtual {v0}, Lcom/samsung/musicplus/glwidget/model/BitmapModel;->getPublisher()Lcom/samsung/musicplus/bitmapcache/BitmapCache$Publisher;

    move-result-object v0

    if-ne v0, p0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout$AlbArtPublisher;->mAlbumModel:Lcom/samsung/musicplus/glwidget/model/BitmapModel;

    invoke-virtual {v0}, Lcom/samsung/musicplus/glwidget/model/BitmapModel;->getAdapterIndex()I

    move-result v0

    iget v1, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout$AlbArtPublisher;->mAdapterIdx:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onFailPublishImage(Landroid/net/Uri;J)V
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "elapsedTime"    # J

    .prologue
    .line 317
    invoke-direct {p0}, Lcom/samsung/musicplus/glwidget/layout/FlatLayout$AlbArtPublisher;->stillProperModel()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 318
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout$AlbArtPublisher;->mAlbumModel:Lcom/samsung/musicplus/glwidget/model/BitmapModel;

    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout$AlbArtPublisher;->this$0:Lcom/samsung/musicplus/glwidget/layout/FlatLayout;

    iget-object v1, v1, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mParent:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v1}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->getDefaultAlbumArt()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/glwidget/model/BitmapModel;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 320
    :cond_0
    return-void
.end method

.method public onPublishImage(Landroid/net/Uri;Landroid/graphics/Bitmap;J)V
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;
    .param p3, "elapsedTime"    # J

    .prologue
    .line 309
    invoke-direct {p0}, Lcom/samsung/musicplus/glwidget/layout/FlatLayout$AlbArtPublisher;->stillProperModel()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 310
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout$AlbArtPublisher;->mAlbumModel:Lcom/samsung/musicplus/glwidget/model/BitmapModel;

    invoke-virtual {v0, p2}, Lcom/samsung/musicplus/glwidget/model/BitmapModel;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 311
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout$AlbArtPublisher;->this$0:Lcom/samsung/musicplus/glwidget/layout/FlatLayout;

    iget-object v0, v0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout;->mParent:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v0}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->requestRender()V

    .line 313
    :cond_0
    return-void
.end method
