.class public Lcom/samsung/musicplus/glwidget/model/Model;
.super Ljava/lang/Object;
.source "Model.java"


# instance fields
.field protected mAngle:F

.field protected mHeight:F

.field private mModified:Z

.field protected mRotX:F

.field protected mRotY:F

.field protected mRotZ:F

.field protected mScaleF:F

.field private mVisible:Z

.field protected mWidth:F

.field protected mX:F

.field protected mY:F

.field protected mZ:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/samsung/musicplus/glwidget/model/Model;->mRotY:F

    .line 34
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/musicplus/glwidget/model/Model;->mScaleF:F

    return-void
.end method

.method private makeScaleValue()F
    .locals 3

    .prologue
    const/high16 v2, 0x42c80000    # 100.0f

    .line 70
    iget v0, p0, Lcom/samsung/musicplus/glwidget/model/Model;->mScaleF:F

    const/high16 v1, 0x40400000    # 3.0f

    div-float/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    sub-float v0, v2, v0

    div-float/2addr v0, v2

    return v0
.end method


# virtual methods
.method public getAngle()F
    .locals 1

    .prologue
    .line 197
    iget v0, p0, Lcom/samsung/musicplus/glwidget/model/Model;->mAngle:F

    return v0
.end method

.method public getDebugState()Ljava/lang/String;
    .locals 4

    .prologue
    .line 326
    const-string v0, "{V<-%b X<-%.2f Y<-%.2f Z<-%.2f W<-%.2f H<-%.2f Angle<-%.2f}"

    const/4 v1, 0x7

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-boolean v3, p0, Lcom/samsung/musicplus/glwidget/model/Model;->mVisible:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget v3, p0, Lcom/samsung/musicplus/glwidget/model/Model;->mX:F

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget v3, p0, Lcom/samsung/musicplus/glwidget/model/Model;->mY:F

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget v3, p0, Lcom/samsung/musicplus/glwidget/model/Model;->mZ:F

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x4

    iget v3, p0, Lcom/samsung/musicplus/glwidget/model/Model;->mWidth:F

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x5

    iget v3, p0, Lcom/samsung/musicplus/glwidget/model/Model;->mHeight:F

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x6

    iget v3, p0, Lcom/samsung/musicplus/glwidget/model/Model;->mAngle:F

    invoke-static {v3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getHeight()F
    .locals 2

    .prologue
    .line 66
    iget v0, p0, Lcom/samsung/musicplus/glwidget/model/Model;->mHeight:F

    invoke-direct {p0}, Lcom/samsung/musicplus/glwidget/model/Model;->makeScaleValue()F

    move-result v1

    mul-float/2addr v0, v1

    return v0
.end method

.method public getRotX()F
    .locals 1

    .prologue
    .line 178
    iget v0, p0, Lcom/samsung/musicplus/glwidget/model/Model;->mRotX:F

    return v0
.end method

.method public getRotY()F
    .locals 1

    .prologue
    .line 159
    iget v0, p0, Lcom/samsung/musicplus/glwidget/model/Model;->mRotY:F

    return v0
.end method

.method public getRotZ()F
    .locals 1

    .prologue
    .line 140
    iget v0, p0, Lcom/samsung/musicplus/glwidget/model/Model;->mRotZ:F

    return v0
.end method

.method public getScaleF()F
    .locals 1

    .prologue
    .line 99
    iget v0, p0, Lcom/samsung/musicplus/glwidget/model/Model;->mScaleF:F

    return v0
.end method

.method public getWidth()F
    .locals 2

    .prologue
    .line 109
    iget v0, p0, Lcom/samsung/musicplus/glwidget/model/Model;->mWidth:F

    invoke-direct {p0}, Lcom/samsung/musicplus/glwidget/model/Model;->makeScaleValue()F

    move-result v1

    mul-float/2addr v0, v1

    return v0
.end method

.method public getX()F
    .locals 1

    .prologue
    .line 270
    iget v0, p0, Lcom/samsung/musicplus/glwidget/model/Model;->mX:F

    return v0
.end method

.method public getY()F
    .locals 1

    .prologue
    .line 251
    iget v0, p0, Lcom/samsung/musicplus/glwidget/model/Model;->mY:F

    return v0
.end method

.method public getZ()F
    .locals 1

    .prologue
    .line 232
    iget v0, p0, Lcom/samsung/musicplus/glwidget/model/Model;->mZ:F

    return v0
.end method

.method public isModified()Z
    .locals 1

    .prologue
    .line 43
    iget-boolean v0, p0, Lcom/samsung/musicplus/glwidget/model/Model;->mModified:Z

    return v0
.end method

.method public isVisible()Z
    .locals 1

    .prologue
    .line 315
    iget-boolean v0, p0, Lcom/samsung/musicplus/glwidget/model/Model;->mVisible:Z

    return v0
.end method

.method public resetModified()V
    .locals 1

    .prologue
    .line 50
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/glwidget/model/Model;->mModified:Z

    .line 51
    return-void
.end method

.method public setAngle(F)V
    .locals 0
    .param p1, "angle"    # F

    .prologue
    .line 206
    iput p1, p0, Lcom/samsung/musicplus/glwidget/model/Model;->mAngle:F

    .line 207
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/model/Model;->setModified()V

    .line 208
    return-void
.end method

.method public setHeight(F)V
    .locals 0
    .param p1, "height"    # F

    .prologue
    .line 79
    iput p1, p0, Lcom/samsung/musicplus/glwidget/model/Model;->mHeight:F

    .line 80
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/model/Model;->setModified()V

    .line 81
    return-void
.end method

.method protected setModified()V
    .locals 1

    .prologue
    .line 57
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/glwidget/model/Model;->mModified:Z

    .line 58
    return-void
.end method

.method public setPosition(FFF)V
    .locals 0
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "z"    # F

    .prologue
    .line 291
    iput p1, p0, Lcom/samsung/musicplus/glwidget/model/Model;->mX:F

    .line 292
    iput p2, p0, Lcom/samsung/musicplus/glwidget/model/Model;->mY:F

    .line 293
    iput p3, p0, Lcom/samsung/musicplus/glwidget/model/Model;->mZ:F

    .line 294
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/model/Model;->setModified()V

    .line 295
    return-void
.end method

.method public setRotX(F)V
    .locals 0
    .param p1, "rotX"    # F

    .prologue
    .line 187
    iput p1, p0, Lcom/samsung/musicplus/glwidget/model/Model;->mRotX:F

    .line 188
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/model/Model;->setModified()V

    .line 189
    return-void
.end method

.method public setRotY(F)V
    .locals 0
    .param p1, "rotY"    # F

    .prologue
    .line 168
    iput p1, p0, Lcom/samsung/musicplus/glwidget/model/Model;->mRotY:F

    .line 169
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/model/Model;->setModified()V

    .line 170
    return-void
.end method

.method public setRotZ(F)V
    .locals 0
    .param p1, "rotZ"    # F

    .prologue
    .line 149
    iput p1, p0, Lcom/samsung/musicplus/glwidget/model/Model;->mRotZ:F

    .line 150
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/model/Model;->setModified()V

    .line 151
    return-void
.end method

.method public setRotation(FFFF)V
    .locals 0
    .param p1, "angle"    # F
    .param p2, "rotX"    # F
    .param p3, "rotY"    # F
    .param p4, "rotZ"    # F

    .prologue
    .line 219
    iput p1, p0, Lcom/samsung/musicplus/glwidget/model/Model;->mAngle:F

    .line 220
    iput p2, p0, Lcom/samsung/musicplus/glwidget/model/Model;->mRotX:F

    .line 221
    iput p3, p0, Lcom/samsung/musicplus/glwidget/model/Model;->mRotY:F

    .line 222
    iput p4, p0, Lcom/samsung/musicplus/glwidget/model/Model;->mRotZ:F

    .line 223
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/model/Model;->setModified()V

    .line 224
    return-void
.end method

.method public setScaleF(F)V
    .locals 0
    .param p1, "scale"    # F

    .prologue
    .line 89
    iput p1, p0, Lcom/samsung/musicplus/glwidget/model/Model;->mScaleF:F

    .line 90
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/model/Model;->setModified()V

    .line 91
    return-void
.end method

.method public setSize(FF)V
    .locals 0
    .param p1, "width"    # F
    .param p2, "height"    # F

    .prologue
    .line 129
    iput p1, p0, Lcom/samsung/musicplus/glwidget/model/Model;->mWidth:F

    .line 130
    iput p2, p0, Lcom/samsung/musicplus/glwidget/model/Model;->mHeight:F

    .line 131
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/model/Model;->setModified()V

    .line 132
    return-void
.end method

.method public setVisibility(Z)V
    .locals 0
    .param p1, "visible"    # Z

    .prologue
    .line 306
    iput-boolean p1, p0, Lcom/samsung/musicplus/glwidget/model/Model;->mVisible:Z

    .line 307
    return-void
.end method

.method public setWidth(F)V
    .locals 0
    .param p1, "width"    # F

    .prologue
    .line 118
    iput p1, p0, Lcom/samsung/musicplus/glwidget/model/Model;->mWidth:F

    .line 119
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/model/Model;->setModified()V

    .line 120
    return-void
.end method

.method public setX(F)V
    .locals 0
    .param p1, "x"    # F

    .prologue
    .line 279
    iput p1, p0, Lcom/samsung/musicplus/glwidget/model/Model;->mX:F

    .line 280
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/model/Model;->setModified()V

    .line 281
    return-void
.end method

.method public setY(F)V
    .locals 0
    .param p1, "y"    # F

    .prologue
    .line 260
    iput p1, p0, Lcom/samsung/musicplus/glwidget/model/Model;->mY:F

    .line 261
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/model/Model;->setModified()V

    .line 262
    return-void
.end method

.method public setZ(F)V
    .locals 0
    .param p1, "z"    # F

    .prologue
    .line 241
    iput p1, p0, Lcom/samsung/musicplus/glwidget/model/Model;->mZ:F

    .line 242
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/model/Model;->setModified()V

    .line 243
    return-void
.end method
