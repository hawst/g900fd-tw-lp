.class Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$4;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "GalleryLayoutBase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;)V
    .locals 0

    .prologue
    .line 1250
    iput-object p1, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$4;->this$0:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 1253
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$4;->this$0:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;

    # getter for: Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAnimationRunnable:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;
    invoke-static {v0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->access$700(Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;)Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;->OnDown()V

    .line 1254
    const/4 v0, 0x1

    return v0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 2
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "velocityX"    # F
    .param p4, "velocityY"    # F

    .prologue
    .line 1272
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$4;->this$0:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;

    # getter for: Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAnimationRunnable:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;
    invoke-static {v0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->access$700(Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;)Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;

    move-result-object v0

    float-to-int v1, p3

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;->OnFling(I)V

    .line 1273
    const/4 v0, 0x1

    return v0
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 3
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "distanceX"    # F
    .param p4, "distanceY"    # F

    .prologue
    const/4 v2, 0x1

    .line 1259
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$4;->this$0:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;

    # getter for: Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAnimationStarted:Z
    invoke-static {v0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->access$800(Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1260
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$4;->this$0:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;

    # setter for: Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAnimationStarted:Z
    invoke-static {v0, v2}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->access$802(Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;Z)Z

    .line 1262
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$4;->this$0:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;

    # getter for: Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mPerformanceTestInProgress:Z
    invoke-static {v0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->access$200(Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1263
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$4;->this$0:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;

    iget-object v0, v0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mParent:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v0}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->fireOnAnimationStarted()V

    .line 1266
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$4;->this$0:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;

    # getter for: Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->mAnimationRunnable:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;
    invoke-static {v0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->access$700(Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;)Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;

    move-result-object v0

    float-to-int v1, p3

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$AnimationRunnable;->OnScroll(I)V

    .line 1267
    return v2
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 1278
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    .line 1279
    .local v0, "x":F
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    .line 1286
    .local v1, "y":F
    iget-object v2, p0, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase$4;->this$0:Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;

    invoke-virtual {v2, v0, v1}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayoutBase;->onSingleTap(FF)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-super {p0, p1}, Landroid/view/GestureDetector$SimpleOnGestureListener;->onSingleTapConfirmed(Landroid/view/MotionEvent;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method
