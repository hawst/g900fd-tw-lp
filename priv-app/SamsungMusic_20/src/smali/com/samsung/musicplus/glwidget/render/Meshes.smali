.class public Lcom/samsung/musicplus/glwidget/render/Meshes;
.super Ljava/lang/Object;
.source "Meshes.java"


# static fields
.field public static final FramedRectangle:[F

.field public static final TexturedRectangle:[F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 6
    const/16 v0, 0xc

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    sput-object v0, Lcom/samsung/musicplus/glwidget/render/Meshes;->TexturedRectangle:[F

    .line 13
    const/16 v0, 0x18

    new-array v0, v0, [F

    fill-array-data v0, :array_1

    sput-object v0, Lcom/samsung/musicplus/glwidget/render/Meshes;->FramedRectangle:[F

    return-void

    .line 6
    nop

    :array_0
    .array-data 4
        0x0
        0x0
        0x0
        0x3f800000    # 1.0f
        0x0
        0x0
        0x0
        0x3f800000    # 1.0f
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x0
    .end array-data

    .line 13
    :array_1
    .array-data 4
        0x0
        0x0
        0x0
        0x0
        0x3f800000    # 1.0f
        0x0
        0x0
        0x3f800000    # 1.0f
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x0
        0x3f800000    # 1.0f
        0x3f800000    # 1.0f
        0x0
        0x3f800000    # 1.0f
        0x0
        0x0
        0x3f800000    # 1.0f
        0x0
        0x0
        0x0
        0x0
        0x0
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final shadeColors(II)[F
    .locals 11
    .param p0, "c1"    # I
    .param p1, "c2"    # I

    .prologue
    const/high16 v10, 0x437f0000    # 255.0f

    .line 45
    invoke-static {p0}, Landroid/graphics/Color;->red(I)I

    move-result v9

    int-to-float v9, v9

    div-float v6, v9, v10

    .line 46
    .local v6, "r1":F
    invoke-static {p0}, Landroid/graphics/Color;->green(I)I

    move-result v9

    int-to-float v9, v9

    div-float v4, v9, v10

    .line 47
    .local v4, "g1":F
    invoke-static {p0}, Landroid/graphics/Color;->blue(I)I

    move-result v9

    int-to-float v9, v9

    div-float v2, v9, v10

    .line 48
    .local v2, "b1":F
    invoke-static {p0}, Landroid/graphics/Color;->alpha(I)I

    move-result v9

    int-to-float v9, v9

    div-float v0, v9, v10

    .line 50
    .local v0, "a1":F
    invoke-static {p1}, Landroid/graphics/Color;->red(I)I

    move-result v9

    int-to-float v9, v9

    div-float v7, v9, v10

    .line 51
    .local v7, "r2":F
    invoke-static {p1}, Landroid/graphics/Color;->green(I)I

    move-result v9

    int-to-float v9, v9

    div-float v5, v9, v10

    .line 52
    .local v5, "g2":F
    invoke-static {p1}, Landroid/graphics/Color;->blue(I)I

    move-result v9

    int-to-float v9, v9

    div-float v3, v9, v10

    .line 53
    .local v3, "b2":F
    invoke-static {p1}, Landroid/graphics/Color;->alpha(I)I

    move-result v9

    int-to-float v9, v9

    div-float v1, v9, v10

    .line 55
    .local v1, "a2":F
    const/16 v9, 0x18

    new-array v8, v9, [F

    const/4 v9, 0x0

    aput v7, v8, v9

    const/4 v9, 0x1

    aput v5, v8, v9

    const/4 v9, 0x2

    aput v3, v8, v9

    const/4 v9, 0x3

    aput v1, v8, v9

    const/4 v9, 0x4

    aput v6, v8, v9

    const/4 v9, 0x5

    aput v4, v8, v9

    const/4 v9, 0x6

    aput v2, v8, v9

    const/4 v9, 0x7

    aput v0, v8, v9

    const/16 v9, 0x8

    aput v7, v8, v9

    const/16 v9, 0x9

    aput v5, v8, v9

    const/16 v9, 0xa

    aput v3, v8, v9

    const/16 v9, 0xb

    aput v1, v8, v9

    const/16 v9, 0xc

    aput v6, v8, v9

    const/16 v9, 0xd

    aput v4, v8, v9

    const/16 v9, 0xe

    aput v2, v8, v9

    const/16 v9, 0xf

    aput v0, v8, v9

    const/16 v9, 0x10

    aput v7, v8, v9

    const/16 v9, 0x11

    aput v5, v8, v9

    const/16 v9, 0x12

    aput v3, v8, v9

    const/16 v9, 0x13

    aput v1, v8, v9

    const/16 v9, 0x14

    aput v6, v8, v9

    const/16 v9, 0x15

    aput v4, v8, v9

    const/16 v9, 0x16

    aput v2, v8, v9

    const/16 v9, 0x17

    aput v0, v8, v9

    .line 64
    .local v8, "res":[F
    return-object v8
.end method

.method public static final shadeCoords(F)[F
    .locals 6
    .param p0, "depth"    # F

    .prologue
    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v4, 0x0

    .line 28
    move v0, p0

    .line 29
    .local v0, "d":F
    const/16 v2, 0x12

    new-array v1, v2, [F

    const/4 v2, 0x0

    sub-float v3, v5, v0

    aput v3, v1, v2

    const/4 v2, 0x1

    sub-float v3, v4, v0

    aput v3, v1, v2

    const/4 v2, 0x2

    aput v4, v1, v2

    const/4 v2, 0x3

    aput v5, v1, v2

    const/4 v2, 0x4

    aput v4, v1, v2

    const/4 v2, 0x5

    aput v4, v1, v2

    const/4 v2, 0x6

    sub-float v3, v4, v0

    aput v3, v1, v2

    const/4 v2, 0x7

    sub-float v3, v4, v0

    aput v3, v1, v2

    const/16 v2, 0x8

    aput v4, v1, v2

    const/16 v2, 0x9

    aput v4, v1, v2

    const/16 v2, 0xa

    aput v4, v1, v2

    const/16 v2, 0xb

    aput v4, v1, v2

    const/16 v2, 0xc

    sub-float v3, v4, v0

    aput v3, v1, v2

    const/16 v2, 0xd

    sub-float v3, v5, v0

    aput v3, v1, v2

    const/16 v2, 0xe

    aput v4, v1, v2

    const/16 v2, 0xf

    aput v4, v1, v2

    const/16 v2, 0x10

    aput v5, v1, v2

    const/16 v2, 0x11

    aput v4, v1, v2

    .line 41
    .local v1, "res":[F
    return-object v1
.end method
