.class public interface abstract Lcom/samsung/musicplus/glwidget/GLGalleryView;
.super Ljava/lang/Object;
.source "GLGalleryView.java"

# interfaces
.implements Lcom/samsung/musicplus/glwidget/layout/IAlbumArtInfo;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/glwidget/GLGalleryView$MarkerViewInfo;,
        Lcom/samsung/musicplus/glwidget/GLGalleryView$OnAnimationListener;
    }
.end annotation


# static fields
.field public static final DEBUG_SURFACE_FORMAT:Z = true

.field public static final MAX_MARKERS_COUNT:I = 0x2

.field public static final PERFORMANCE_TEST:Z


# virtual methods
.method public abstract animationFinished()Z
.end method

.method public abstract asView()Landroid/view/View;
.end method

.method public abstract getAlbumArtForCoord([F[F)Landroid/graphics/Bitmap;
.end method

.method public abstract getDefaultAlbumArt()Landroid/graphics/Bitmap;
.end method

.method public abstract getGLHolder()Lcom/samsung/musicplus/glwidget/layout/GLHolder;
.end method

.method public abstract getHeight()I
.end method

.method public abstract getLeft()I
.end method

.method public abstract getSelectedItemPosition()I
.end method

.method public abstract getTop()I
.end method

.method public abstract getWidth()I
.end method

.method public abstract isFlinging()Z
.end method

.method public abstract isInEditMode()Z
.end method

.method public abstract isOpaque()Z
.end method

.method public abstract isTouched()Z
.end method

.method public abstract onPause()V
.end method

.method public abstract onResume()V
.end method

.method public abstract requestRender()V
.end method

.method public abstract setAdapter(Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;)V
.end method

.method public abstract setAdapterWrap(Z)V
.end method

.method public abstract setAlpha(F)V
.end method

.method public abstract setDefaultAlbumArt(Landroid/graphics/Bitmap;)V
.end method

.method public abstract setOnAnimationListener(Lcom/samsung/musicplus/glwidget/GLGalleryView$OnAnimationListener;)V
.end method

.method public abstract setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V
.end method

.method public abstract setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V
.end method

.method public abstract setOpaque(Z)V
.end method

.method public abstract setSelection(I)V
.end method

.method public abstract setVisibility(I)V
.end method
