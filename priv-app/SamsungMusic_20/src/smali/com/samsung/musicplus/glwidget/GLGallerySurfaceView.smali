.class public Lcom/samsung/musicplus/glwidget/GLGallerySurfaceView;
.super Landroid/opengl/GLSurfaceView;
.source "GLGallerySurfaceView.java"

# interfaces
.implements Lcom/samsung/musicplus/glwidget/GLGalleryView;


# static fields
.field private static final LOG_TAG:Ljava/lang/String;


# instance fields
.field private impl:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

.field private mOpaque:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const-class v0, Lcom/samsung/musicplus/glwidget/GLGallerySurfaceView;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/glwidget/GLGallerySurfaceView;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;Ljava/lang/Class;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/util/AttributeSet;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/samsung/musicplus/glwidget/layout/GalleryLayout;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 52
    .local p3, "layoutClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/samsung/musicplus/glwidget/layout/GalleryLayout;>;"
    invoke-direct {p0, p1, p2}, Landroid/opengl/GLSurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 38
    new-instance v0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-direct {v0}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/glwidget/GLGallerySurfaceView;->impl:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    .line 53
    invoke-direct {p0, p1, p3, p2}, Lcom/samsung/musicplus/glwidget/GLGallerySurfaceView;->init(Landroid/content/Context;Ljava/lang/Class;Landroid/util/AttributeSet;)V

    .line 54
    return-void
.end method

.method private init(Landroid/content/Context;Ljava/lang/Class;Landroid/util/AttributeSet;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p3, "attrs"    # Landroid/util/AttributeSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/samsung/musicplus/glwidget/layout/GalleryLayout;",
            ">;",
            "Landroid/util/AttributeSet;",
            ")V"
        }
    .end annotation

    .prologue
    .local p2, "layoutClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/samsung/musicplus/glwidget/layout/GalleryLayout;>;"
    const/4 v0, 0x1

    const/4 v6, 0x0

    const/16 v1, 0x8

    .line 234
    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/glwidget/GLGallerySurfaceView;->setFocusable(Z)V

    .line 235
    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/glwidget/GLGallerySurfaceView;->setOpaque(Z)V

    .line 238
    const/16 v5, 0x10

    move-object v0, p0

    move v2, v1

    move v3, v1

    move v4, v1

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/musicplus/glwidget/GLGallerySurfaceView;->setEGLConfigChooser(IIIIII)V

    .line 241
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/glwidget/GLGallerySurfaceView;->setEGLContextClientVersion(I)V

    .line 243
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/GLGallerySurfaceView;->impl:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    move-object v1, p0

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->init(Lcom/samsung/musicplus/glwidget/GLGalleryView;Landroid/view/View;Landroid/content/Context;Ljava/lang/Class;Landroid/util/AttributeSet;)V

    .line 245
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/GLGallerySurfaceView;->impl:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v0}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->getRenderer()Landroid/opengl/GLSurfaceView$Renderer;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/glwidget/GLGallerySurfaceView;->setRenderer(Landroid/opengl/GLSurfaceView$Renderer;)V

    .line 246
    invoke-virtual {p0, v6}, Lcom/samsung/musicplus/glwidget/GLGallerySurfaceView;->setRenderMode(I)V

    .line 247
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/glwidget/GLGallerySurfaceView;->setDebugFlags(I)V

    .line 248
    return-void
.end method


# virtual methods
.method public animationFinished()Z
    .locals 1

    .prologue
    .line 252
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/GLGallerySurfaceView;->impl:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v0}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->animationFinished()Z

    move-result v0

    return v0
.end method

.method public asView()Landroid/view/View;
    .locals 0

    .prologue
    .line 287
    return-object p0
.end method

.method public getAlbumArtForCoord([F[F)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "inCoord"    # [F
    .param p2, "outCoord"    # [F

    .prologue
    .line 282
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/GLGallerySurfaceView;->impl:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->getAlbumArtForCoord([F[F)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getAlbumLastPosition(I)Lcom/samsung/musicplus/glwidget/layout/IAlbumArtInfo$PositionInfo;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 292
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/GLGallerySurfaceView;->impl:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->getAlbumLastPosition(I)Lcom/samsung/musicplus/glwidget/layout/IAlbumArtInfo$PositionInfo;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultAlbumArt()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/GLGallerySurfaceView;->impl:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v0}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->getDefaultAlbumArt()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getGLHolder()Lcom/samsung/musicplus/glwidget/layout/GLHolder;
    .locals 1

    .prologue
    .line 272
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/GLGallerySurfaceView;->impl:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    return-object v0
.end method

.method public getSelectedItemPosition()I
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/GLGallerySurfaceView;->impl:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v0}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->getSelectedItemPosition()I

    move-result v0

    return v0
.end method

.method public isFlinging()Z
    .locals 1

    .prologue
    .line 267
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/GLGallerySurfaceView;->impl:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v0}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->isFlinging()Z

    move-result v0

    return v0
.end method

.method public isOpaque()Z
    .locals 1

    .prologue
    .line 229
    iget-boolean v0, p0, Lcom/samsung/musicplus/glwidget/GLGallerySurfaceView;->mOpaque:Z

    return v0
.end method

.method public isTouched()Z
    .locals 1

    .prologue
    .line 262
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/GLGallerySurfaceView;->impl:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v0}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->isTouched()Z

    move-result v0

    return v0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 191
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/GLGallerySurfaceView;->impl:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v0, p0, p1, p2}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->onKeyDown(Landroid/view/View;ILandroid/view/KeyEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1, p2}, Landroid/opengl/GLSurfaceView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 200
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/GLGallerySurfaceView;->impl:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v0, p0, p1, p2}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->onKeyUp(Landroid/view/View;ILandroid/view/KeyEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1, p2}, Landroid/opengl/GLSurfaceView;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/GLGallerySurfaceView;->impl:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v0}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->onPause()V

    .line 153
    invoke-super {p0}, Landroid/opengl/GLSurfaceView;->onPause()V

    .line 154
    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 3
    .param p1, "state"    # Landroid/os/Parcelable;

    .prologue
    .line 104
    sget-object v1, Lcom/samsung/musicplus/glwidget/GLGallerySurfaceView;->LOG_TAG:Ljava/lang/String;

    const-string v2, "onRestoreInstanceState"

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    instance-of v1, p1, Landroid/os/Bundle;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 106
    check-cast v0, Landroid/os/Bundle;

    .line 107
    .local v0, "s":Landroid/os/Bundle;
    const-string v1, "parent"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    invoke-super {p0, v1}, Landroid/opengl/GLSurfaceView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 108
    const-string v1, "position"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/glwidget/GLGallerySurfaceView;->setSelection(I)V

    .line 112
    .end local v0    # "s":Landroid/os/Bundle;
    :goto_0
    return-void

    .line 110
    :cond_0
    invoke-super {p0, p1}, Landroid/opengl/GLSurfaceView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    goto :goto_0
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/GLGallerySurfaceView;->impl:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v0}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->onResume()V

    .line 163
    invoke-super {p0}, Landroid/opengl/GLSurfaceView;->onResume()V

    .line 164
    return-void
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .prologue
    .line 92
    sget-object v1, Lcom/samsung/musicplus/glwidget/GLGallerySurfaceView;->LOG_TAG:Ljava/lang/String;

    const-string v2, "onSaveInstanceState"

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 93
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 94
    .local v0, "res":Landroid/os/Bundle;
    const-string v1, "parent"

    invoke-super {p0}, Landroid/opengl/GLSurfaceView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 95
    const-string v1, "position"

    iget-object v2, p0, Lcom/samsung/musicplus/glwidget/GLGallerySurfaceView;->impl:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v2}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->getSelectedItemPosition()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 96
    return-object v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 171
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/GLGallerySurfaceView;->impl:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Landroid/opengl/GLSurfaceView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setAdapter(Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;)V
    .locals 1
    .param p1, "adapter"    # Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;

    .prologue
    .line 63
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/GLGallerySurfaceView;->impl:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->setAdapter(Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;)V

    .line 64
    return-void
.end method

.method public setAdapterWrap(Z)V
    .locals 1
    .param p1, "wrap"    # Z

    .prologue
    .line 277
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/GLGallerySurfaceView;->impl:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->setAdapterWrap(Z)V

    .line 278
    return-void
.end method

.method public setDefaultAlbumArt(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 133
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/GLGallerySurfaceView;->impl:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->setDefaultAlbumArt(Landroid/graphics/Bitmap;)V

    .line 134
    return-void
.end method

.method public setOnAnimationListener(Lcom/samsung/musicplus/glwidget/GLGalleryView$OnAnimationListener;)V
    .locals 1
    .param p1, "onAnimationFinished"    # Lcom/samsung/musicplus/glwidget/GLGalleryView$OnAnimationListener;

    .prologue
    .line 257
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/GLGallerySurfaceView;->impl:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->setOnAnimationListener(Lcom/samsung/musicplus/glwidget/GLGalleryView$OnAnimationListener;)V

    .line 258
    return-void
.end method

.method public setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V
    .locals 1
    .param p1, "listener"    # Landroid/widget/AdapterView$OnItemClickListener;

    .prologue
    .line 182
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/GLGallerySurfaceView;->impl:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 183
    return-void
.end method

.method public setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V
    .locals 1
    .param p1, "listener"    # Landroid/widget/AdapterView$OnItemSelectedListener;

    .prologue
    .line 122
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/GLGallerySurfaceView;->impl:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 123
    return-void
.end method

.method public setOpaque(Z)V
    .locals 2
    .param p1, "opaque"    # Z

    .prologue
    .line 211
    iput-boolean p1, p0, Lcom/samsung/musicplus/glwidget/GLGallerySurfaceView;->mOpaque:Z

    .line 212
    iget-boolean v0, p0, Lcom/samsung/musicplus/glwidget/GLGallerySurfaceView;->mOpaque:Z

    if-eqz v0, :cond_0

    .line 213
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/glwidget/GLGallerySurfaceView;->setZOrderOnTop(Z)V

    .line 218
    :goto_0
    return-void

    .line 215
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/glwidget/GLGallerySurfaceView;->setZOrderOnTop(Z)V

    .line 216
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/GLGallerySurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    const/4 v1, -0x3

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->setFormat(I)V

    goto :goto_0
.end method

.method public setSelection(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 74
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/GLGallerySurfaceView;->impl:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->setSelection(I)V

    .line 75
    return-void
.end method
