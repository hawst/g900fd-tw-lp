.class Lcom/samsung/musicplus/glwidget/layout/GLHolder$2;
.super Ljava/lang/Object;
.source "GLHolder.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/glwidget/layout/GLHolder;->fireOnAnimationFinished()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/glwidget/layout/GLHolder;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/glwidget/layout/GLHolder;)V
    .locals 0

    .prologue
    .line 381
    iput-object p1, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder$2;->this$0:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 384
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder$2;->this$0:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mAnimationStarted:Z
    invoke-static {v0, v1}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->access$002(Lcom/samsung/musicplus/glwidget/layout/GLHolder;Z)Z

    .line 385
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder$2;->this$0:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    # getter for: Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->access$500(Lcom/samsung/musicplus/glwidget/layout/GLHolder;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder$2;->this$0:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    # getter for: Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mAnimationUpdate:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->access$400(Lcom/samsung/musicplus/glwidget/layout/GLHolder;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 386
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder$2;->this$0:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    # getter for: Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mOnAnimationListener:Lcom/samsung/musicplus/glwidget/GLGalleryView$OnAnimationListener;
    invoke-static {v0}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->access$100(Lcom/samsung/musicplus/glwidget/layout/GLHolder;)Lcom/samsung/musicplus/glwidget/GLGalleryView$OnAnimationListener;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder$2;->this$0:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    # getter for: Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mParent:Lcom/samsung/musicplus/glwidget/GLGalleryView;
    invoke-static {v1}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->access$300(Lcom/samsung/musicplus/glwidget/layout/GLHolder;)Lcom/samsung/musicplus/glwidget/GLGalleryView;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/samsung/musicplus/glwidget/GLGalleryView$OnAnimationListener;->onAnimationFinished(Lcom/samsung/musicplus/glwidget/GLGalleryView;)V

    .line 387
    return-void
.end method
