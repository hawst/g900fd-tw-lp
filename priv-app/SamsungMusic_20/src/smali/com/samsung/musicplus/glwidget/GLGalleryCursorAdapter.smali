.class public abstract Lcom/samsung/musicplus/glwidget/GLGalleryCursorAdapter;
.super Ljava/lang/Object;
.source "GLGalleryCursorAdapter.java"

# interfaces
.implements Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;


# instance fields
.field private mCursor:Landroid/database/Cursor;


# direct methods
.method public constructor <init>(Landroid/database/Cursor;)V
    .locals 0
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/samsung/musicplus/glwidget/GLGalleryCursorAdapter;->mCursor:Landroid/database/Cursor;

    .line 23
    return-void
.end method

.method private checkUpdateCursor(I)Z
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 95
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/GLGalleryCursorAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/GLGalleryCursorAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 96
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/GLGalleryCursorAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 97
    const/4 v0, 0x1

    .line 99
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public bindView(Landroid/content/Context;Landroid/view/View;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I

    .prologue
    .line 62
    invoke-direct {p0, p3}, Lcom/samsung/musicplus/glwidget/GLGalleryCursorAdapter;->checkUpdateCursor(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/GLGalleryCursorAdapter;->mCursor:Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2, v0}, Lcom/samsung/musicplus/glwidget/GLGalleryCursorAdapter;->bindView(Landroid/content/Context;Landroid/view/View;Landroid/database/Cursor;)V

    .line 65
    :cond_0
    return-void
.end method

.method protected abstract bindView(Landroid/content/Context;Landroid/view/View;Landroid/database/Cursor;)V
.end method

.method public getAlbArtUri(Landroid/content/Context;I)Landroid/net/Uri;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "position"    # I

    .prologue
    .line 69
    invoke-direct {p0, p2}, Lcom/samsung/musicplus/glwidget/GLGalleryCursorAdapter;->checkUpdateCursor(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 70
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/GLGalleryCursorAdapter;->mCursor:Landroid/database/Cursor;

    invoke-virtual {p0, p1, v0}, Lcom/samsung/musicplus/glwidget/GLGalleryCursorAdapter;->getAlbArtUri(Landroid/content/Context;Landroid/database/Cursor;)Landroid/net/Uri;

    move-result-object v0

    .line 72
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected abstract getAlbArtUri(Landroid/content/Context;Landroid/database/Cursor;)Landroid/net/Uri;
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/GLGalleryCursorAdapter;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 55
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/GLGalleryCursorAdapter;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    .line 57
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCursor()Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/GLGalleryCursorAdapter;->mCursor:Landroid/database/Cursor;

    return-object v0
.end method

.method public getMarkerViewCount(Landroid/content/Context;I)I
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "position"    # I

    .prologue
    .line 77
    invoke-direct {p0, p2}, Lcom/samsung/musicplus/glwidget/GLGalleryCursorAdapter;->checkUpdateCursor(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 78
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/GLGalleryCursorAdapter;->mCursor:Landroid/database/Cursor;

    invoke-virtual {p0, p1, v0}, Lcom/samsung/musicplus/glwidget/GLGalleryCursorAdapter;->getMarkerViewCount(Landroid/content/Context;Landroid/database/Cursor;)I

    move-result v0

    .line 80
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected abstract getMarkerViewCount(Landroid/content/Context;Landroid/database/Cursor;)I
.end method

.method public updateMarkerViews(Landroid/content/Context;I[Lcom/samsung/musicplus/glwidget/GLGalleryView$MarkerViewInfo;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "position"    # I
    .param p3, "markers"    # [Lcom/samsung/musicplus/glwidget/GLGalleryView$MarkerViewInfo;

    .prologue
    .line 85
    invoke-direct {p0, p2}, Lcom/samsung/musicplus/glwidget/GLGalleryCursorAdapter;->checkUpdateCursor(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/GLGalleryCursorAdapter;->mCursor:Landroid/database/Cursor;

    invoke-virtual {p0, p1, v0, p3}, Lcom/samsung/musicplus/glwidget/GLGalleryCursorAdapter;->updateMarkerViews(Landroid/content/Context;Landroid/database/Cursor;[Lcom/samsung/musicplus/glwidget/GLGalleryView$MarkerViewInfo;)V

    .line 88
    :cond_0
    return-void
.end method

.method protected abstract updateMarkerViews(Landroid/content/Context;Landroid/database/Cursor;[Lcom/samsung/musicplus/glwidget/GLGalleryView$MarkerViewInfo;)V
.end method
