.class public interface abstract Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;
.super Ljava/lang/Object;
.source "GLGalleryAdapter.java"


# virtual methods
.method public abstract bindView(Landroid/content/Context;Landroid/view/View;I)V
.end method

.method public abstract getAlbArtUri(Landroid/content/Context;I)Landroid/net/Uri;
.end method

.method public abstract getCount()I
.end method

.method public abstract getMarkerViewCount(Landroid/content/Context;I)I
.end method

.method public abstract updateMarkerViews(Landroid/content/Context;I[Lcom/samsung/musicplus/glwidget/GLGalleryView$MarkerViewInfo;)V
.end method
