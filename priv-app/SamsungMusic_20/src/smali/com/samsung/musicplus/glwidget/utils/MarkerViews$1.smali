.class Lcom/samsung/musicplus/glwidget/utils/MarkerViews$1;
.super Ljava/lang/Object;
.source "MarkerViews.java"

# interfaces
.implements Lcom/samsung/musicplus/glwidget/model/Matrices$MatrixUpdater;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/glwidget/utils/MarkerViews;-><init>(FLcom/samsung/musicplus/glwidget/utils/CoordConverter;[Lcom/samsung/musicplus/glwidget/model/AnimationModel;Landroid/view/LayoutInflater;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/glwidget/utils/MarkerViews;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/glwidget/utils/MarkerViews;)V
    .locals 0

    .prologue
    .line 54
    iput-object p1, p0, Lcom/samsung/musicplus/glwidget/utils/MarkerViews$1;->this$0:Lcom/samsung/musicplus/glwidget/utils/MarkerViews;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public needUpdateMatrix(I)Z
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 64
    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/utils/MarkerViews$1;->this$0:Lcom/samsung/musicplus/glwidget/utils/MarkerViews;

    # getter for: Lcom/samsung/musicplus/glwidget/utils/MarkerViews;->mMarkersCount:I
    invoke-static {v1}, Lcom/samsung/musicplus/glwidget/utils/MarkerViews;->access$000(Lcom/samsung/musicplus/glwidget/utils/MarkerViews;)I

    move-result v1

    div-int v0, p1, v1

    .line 65
    .local v0, "modelIndex":I
    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/utils/MarkerViews$1;->this$0:Lcom/samsung/musicplus/glwidget/utils/MarkerViews;

    # getter for: Lcom/samsung/musicplus/glwidget/utils/MarkerViews;->mModels:[Lcom/samsung/musicplus/glwidget/model/AnimationModel;
    invoke-static {v1}, Lcom/samsung/musicplus/glwidget/utils/MarkerViews;->access$200(Lcom/samsung/musicplus/glwidget/utils/MarkerViews;)[Lcom/samsung/musicplus/glwidget/model/AnimationModel;

    move-result-object v1

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/samsung/musicplus/glwidget/model/AnimationModel;->isModified()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/utils/MarkerViews$1;->this$0:Lcom/samsung/musicplus/glwidget/utils/MarkerViews;

    # getter for: Lcom/samsung/musicplus/glwidget/utils/MarkerViews;->mMarkers:[Lcom/samsung/musicplus/glwidget/model/BitmapModel;
    invoke-static {v1}, Lcom/samsung/musicplus/glwidget/utils/MarkerViews;->access$300(Lcom/samsung/musicplus/glwidget/utils/MarkerViews;)[Lcom/samsung/musicplus/glwidget/model/BitmapModel;

    move-result-object v1

    aget-object v1, v1, p1

    invoke-virtual {v1}, Lcom/samsung/musicplus/glwidget/model/BitmapModel;->isModified()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public updateMatrix(I[FI)V
    .locals 4
    .param p1, "index"    # I
    .param p2, "matrix"    # [F
    .param p3, "offset"    # I

    .prologue
    .line 57
    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/utils/MarkerViews$1;->this$0:Lcom/samsung/musicplus/glwidget/utils/MarkerViews;

    # getter for: Lcom/samsung/musicplus/glwidget/utils/MarkerViews;->mMarkersCount:I
    invoke-static {v1}, Lcom/samsung/musicplus/glwidget/utils/MarkerViews;->access$000(Lcom/samsung/musicplus/glwidget/utils/MarkerViews;)I

    move-result v1

    div-int v0, p1, v1

    .line 58
    .local v0, "modelIndex":I
    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/utils/MarkerViews$1;->this$0:Lcom/samsung/musicplus/glwidget/utils/MarkerViews;

    # getter for: Lcom/samsung/musicplus/glwidget/utils/MarkerViews;->mOriginalModelHeight:F
    invoke-static {v1}, Lcom/samsung/musicplus/glwidget/utils/MarkerViews;->access$100(Lcom/samsung/musicplus/glwidget/utils/MarkerViews;)F

    move-result v1

    iget-object v2, p0, Lcom/samsung/musicplus/glwidget/utils/MarkerViews$1;->this$0:Lcom/samsung/musicplus/glwidget/utils/MarkerViews;

    # getter for: Lcom/samsung/musicplus/glwidget/utils/MarkerViews;->mModels:[Lcom/samsung/musicplus/glwidget/model/AnimationModel;
    invoke-static {v2}, Lcom/samsung/musicplus/glwidget/utils/MarkerViews;->access$200(Lcom/samsung/musicplus/glwidget/utils/MarkerViews;)[Lcom/samsung/musicplus/glwidget/model/AnimationModel;

    move-result-object v2

    aget-object v2, v2, v0

    iget-object v3, p0, Lcom/samsung/musicplus/glwidget/utils/MarkerViews$1;->this$0:Lcom/samsung/musicplus/glwidget/utils/MarkerViews;

    # getter for: Lcom/samsung/musicplus/glwidget/utils/MarkerViews;->mMarkers:[Lcom/samsung/musicplus/glwidget/model/BitmapModel;
    invoke-static {v3}, Lcom/samsung/musicplus/glwidget/utils/MarkerViews;->access$300(Lcom/samsung/musicplus/glwidget/utils/MarkerViews;)[Lcom/samsung/musicplus/glwidget/model/BitmapModel;

    move-result-object v3

    aget-object v3, v3, p1

    invoke-static {v1, v2, v3, p2, p3}, Lcom/samsung/musicplus/glwidget/model/ModelUtils;->updateMarkerMatrix(FLcom/samsung/musicplus/glwidget/model/Model;Lcom/samsung/musicplus/glwidget/model/BitmapModel;[FI)V

    .line 60
    return-void
.end method
