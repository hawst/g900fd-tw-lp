.class public Lcom/samsung/musicplus/glwidget/layout/GLHolder;
.super Ljava/lang/Object;
.source "GLHolder.java"

# interfaces
.implements Lcom/samsung/musicplus/glwidget/GLGalleryView;


# static fields
.field private static final DEBUG:Z = false

.field private static LOG_TAG:Ljava/lang/String; = null

.field private static final mAnimationUpdateDelta:J = 0x64L


# instance fields
.field private mAdapter:Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;

.field private volatile mAnimationStarted:Z

.field private mAnimationUpdate:Ljava/lang/Runnable;

.field private mDefaultAlbArt:Landroid/graphics/Bitmap;

.field private mHandler:Landroid/os/Handler;

.field private mLayout:Lcom/samsung/musicplus/glwidget/layout/GalleryLayout;

.field private mOnAnimationListener:Lcom/samsung/musicplus/glwidget/GLGalleryView$OnAnimationListener;

.field private mOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

.field private mOnItemSelectedListener:Landroid/widget/AdapterView$OnItemSelectedListener;

.field private mParent:Lcom/samsung/musicplus/glwidget/GLGalleryView;

.field private mParentView:Landroid/view/View;

.field private mPaused:Z

.field private volatile mRenderRequested:Z

.field private mSelected:I

.field private final mUIThreadId:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    const-class v0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->LOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 73
    new-instance v0, Lcom/samsung/musicplus/glwidget/layout/GLHolder$1;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/glwidget/layout/GLHolder$1;-><init>(Lcom/samsung/musicplus/glwidget/layout/GLHolder;)V

    iput-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mAnimationUpdate:Ljava/lang/Runnable;

    .line 70
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->getId()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mUIThreadId:J

    .line 71
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/glwidget/layout/GLHolder;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    .prologue
    .line 43
    iget-boolean v0, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mAnimationStarted:Z

    return v0
.end method

.method static synthetic access$002(Lcom/samsung/musicplus/glwidget/layout/GLHolder;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/glwidget/layout/GLHolder;
    .param p1, "x1"    # Z

    .prologue
    .line 43
    iput-boolean p1, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mAnimationStarted:Z

    return p1
.end method

.method static synthetic access$100(Lcom/samsung/musicplus/glwidget/layout/GLHolder;)Lcom/samsung/musicplus/glwidget/GLGalleryView$OnAnimationListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mOnAnimationListener:Lcom/samsung/musicplus/glwidget/GLGalleryView$OnAnimationListener;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/musicplus/glwidget/layout/GLHolder;)Lcom/samsung/musicplus/glwidget/layout/GalleryLayout;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mLayout:Lcom/samsung/musicplus/glwidget/layout/GalleryLayout;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/musicplus/glwidget/layout/GLHolder;)Lcom/samsung/musicplus/glwidget/GLGalleryView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mParent:Lcom/samsung/musicplus/glwidget/GLGalleryView;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/musicplus/glwidget/layout/GLHolder;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mAnimationUpdate:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/musicplus/glwidget/layout/GLHolder;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private ensureUIThread()V
    .locals 4

    .prologue
    .line 533
    iget-wide v0, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mUIThreadId:J

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getId()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 534
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Only the original thread that created a view hierarchy can touch its views."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 536
    :cond_0
    return-void
.end method


# virtual methods
.method public animationFinished()Z
    .locals 1

    .prologue
    .line 393
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mLayout:Lcom/samsung/musicplus/glwidget/layout/GalleryLayout;

    invoke-interface {v0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayout;->animationFinished()Z

    move-result v0

    return v0
.end method

.method public asView()Landroid/view/View;
    .locals 1

    .prologue
    .line 521
    const/4 v0, 0x0

    return-object v0
.end method

.method public checkUIThread()V
    .locals 4

    .prologue
    .line 540
    iget-wide v0, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mUIThreadId:J

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getId()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 541
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Called from wrong thread!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 543
    :cond_0
    return-void
.end method

.method public fireOnAnimationFinished()V
    .locals 2

    .prologue
    .line 377
    invoke-direct {p0}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->ensureUIThread()V

    .line 379
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mOnAnimationListener:Lcom/samsung/musicplus/glwidget/GLGalleryView$OnAnimationListener;

    if-eqz v0, :cond_0

    .line 381
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mParentView:Landroid/view/View;

    new-instance v1, Lcom/samsung/musicplus/glwidget/layout/GLHolder$2;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/glwidget/layout/GLHolder$2;-><init>(Lcom/samsung/musicplus/glwidget/layout/GLHolder;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 390
    :cond_0
    return-void
.end method

.method public fireOnAnimationStarted()V
    .locals 4

    .prologue
    .line 363
    invoke-direct {p0}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->ensureUIThread()V

    .line 365
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mOnAnimationListener:Lcom/samsung/musicplus/glwidget/GLGalleryView$OnAnimationListener;

    if-eqz v0, :cond_1

    .line 366
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mHandler:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 367
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mHandler:Landroid/os/Handler;

    .line 369
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mOnAnimationListener:Lcom/samsung/musicplus/glwidget/GLGalleryView$OnAnimationListener;

    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mParent:Lcom/samsung/musicplus/glwidget/GLGalleryView;

    invoke-interface {v0, v1}, Lcom/samsung/musicplus/glwidget/GLGalleryView$OnAnimationListener;->onAnimationStarted(Lcom/samsung/musicplus/glwidget/GLGalleryView;)V

    .line 370
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mAnimationStarted:Z

    .line 372
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mAnimationUpdate:Ljava/lang/Runnable;

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 374
    :cond_1
    return-void
.end method

.method public fireOnItemClick(I)V
    .locals 6
    .param p1, "position"    # I

    .prologue
    const/4 v1, 0x0

    .line 339
    invoke-direct {p0}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->ensureUIThread()V

    .line 341
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    if-eqz v0, :cond_0

    .line 342
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mAdapter:Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mAdapter:Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;

    invoke-interface {v0}, Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;->getCount()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 346
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    const-wide/16 v4, 0x0

    move-object v2, v1

    move v3, p1

    invoke-interface/range {v0 .. v5}, Landroid/widget/AdapterView$OnItemClickListener;->onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    .line 349
    :cond_0
    return-void
.end method

.method public fireOnSelectionChange()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 301
    invoke-direct {p0}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->ensureUIThread()V

    .line 303
    iget v0, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mSelected:I

    iget-object v2, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mLayout:Lcom/samsung/musicplus/glwidget/layout/GalleryLayout;

    invoke-interface {v2}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayout;->getSelection()I

    move-result v2

    if-eq v0, v2, :cond_0

    .line 304
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mLayout:Lcom/samsung/musicplus/glwidget/layout/GalleryLayout;

    invoke-interface {v0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayout;->getSelection()I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mSelected:I

    .line 308
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mOnItemSelectedListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    if-eqz v0, :cond_0

    .line 309
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mOnItemSelectedListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    iget v3, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mSelected:I

    const-wide/16 v4, -0x1

    move-object v2, v1

    invoke-interface/range {v0 .. v5}, Landroid/widget/AdapterView$OnItemSelectedListener;->onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    .line 312
    :cond_0
    return-void
.end method

.method public getAlbumArtForCoord([F[F)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "inCoord"    # [F
    .param p2, "outCoord"    # [F

    .prologue
    .line 526
    invoke-direct {p0}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->ensureUIThread()V

    .line 528
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mLayout:Lcom/samsung/musicplus/glwidget/layout/GalleryLayout;

    invoke-interface {v0, p1, p2}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayout;->getAlbumArtForCoord([F[F)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getAlbumLastPosition(I)Lcom/samsung/musicplus/glwidget/layout/IAlbumArtInfo$PositionInfo;
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 547
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mLayout:Lcom/samsung/musicplus/glwidget/layout/GalleryLayout;

    invoke-interface {v0, p1}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayout;->getAlbumLastPosition(I)Lcom/samsung/musicplus/glwidget/layout/IAlbumArtInfo$PositionInfo;

    move-result-object v0

    return-object v0
.end method

.method public getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 466
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mParentView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultAlbumArt()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mDefaultAlbArt:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getGLHolder()Lcom/samsung/musicplus/glwidget/layout/GLHolder;
    .locals 1

    .prologue
    .line 474
    const/4 v0, 0x0

    return-object v0
.end method

.method public getHeight()I
    .locals 1

    .prologue
    .line 513
    const/4 v0, 0x0

    return v0
.end method

.method public getLeft()I
    .locals 1

    .prologue
    .line 489
    const/4 v0, 0x0

    return v0
.end method

.method public getRenderer()Landroid/opengl/GLSurfaceView$Renderer;
    .locals 1

    .prologue
    .line 352
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mLayout:Lcom/samsung/musicplus/glwidget/layout/GalleryLayout;

    invoke-interface {v0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayout;->getRenderer()Landroid/opengl/GLSurfaceView$Renderer;

    move-result-object v0

    return-object v0
.end method

.method public getSelectedItemPosition()I
    .locals 1

    .prologue
    .line 140
    iget v0, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mSelected:I

    return v0
.end method

.method public getTop()I
    .locals 1

    .prologue
    .line 497
    const/4 v0, 0x0

    return v0
.end method

.method public getWidth()I
    .locals 1

    .prologue
    .line 505
    const/4 v0, 0x0

    return v0
.end method

.method public init(Lcom/samsung/musicplus/glwidget/GLGalleryView;Landroid/view/View;Landroid/content/Context;Ljava/lang/Class;Landroid/util/AttributeSet;)V
    .locals 5
    .param p1, "parent"    # Lcom/samsung/musicplus/glwidget/GLGalleryView;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "context"    # Landroid/content/Context;
    .param p5, "attrs"    # Landroid/util/AttributeSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/musicplus/glwidget/GLGalleryView;",
            "Landroid/view/View;",
            "Landroid/content/Context;",
            "Ljava/lang/Class",
            "<+",
            "Lcom/samsung/musicplus/glwidget/layout/GalleryLayout;",
            ">;",
            "Landroid/util/AttributeSet;",
            ")V"
        }
    .end annotation

    .prologue
    .line 316
    .local p4, "layoutClass":Ljava/lang/Class;, "Ljava/lang/Class<+Lcom/samsung/musicplus/glwidget/layout/GalleryLayout;>;"
    invoke-direct {p0}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->ensureUIThread()V

    .line 318
    iput-object p2, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mParentView:Landroid/view/View;

    .line 320
    const/4 v1, 0x2

    :try_start_0
    new-array v1, v1, [Ljava/lang/Class;

    const/4 v2, 0x0

    const-class v3, Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-class v3, Landroid/util/AttributeSet;

    aput-object v3, v1, v2

    invoke-virtual {p4, v1}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-interface {p1}, Lcom/samsung/musicplus/glwidget/GLGalleryView;->getGLHolder()Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p5, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/musicplus/glwidget/layout/GalleryLayout;

    iput-object v1, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mLayout:Lcom/samsung/musicplus/glwidget/layout/GalleryLayout;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_4

    .line 335
    :goto_0
    iput-object p1, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mParent:Lcom/samsung/musicplus/glwidget/GLGalleryView;

    .line 336
    return-void

    .line 322
    :catch_0
    move-exception v0

    .line 323
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 324
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 325
    .local v0, "e":Ljava/lang/InstantiationException;
    invoke-virtual {v0}, Ljava/lang/InstantiationException;->printStackTrace()V

    goto :goto_0

    .line 326
    .end local v0    # "e":Ljava/lang/InstantiationException;
    :catch_2
    move-exception v0

    .line 327
    .local v0, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    .line 328
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    :catch_3
    move-exception v0

    .line 329
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    sget-object v1, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->LOG_TAG:Ljava/lang/String;

    const-string v2, "Exception in constructor, check details bellow"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 330
    .end local v0    # "e":Ljava/lang/reflect/InvocationTargetException;
    :catch_4
    move-exception v0

    .line 331
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    sget-object v1, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->LOG_TAG:Ljava/lang/String;

    const-string v2, "Provided layout should have a constructor(GLHolder parent, AttributeSet attrs)"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 333
    invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_0
.end method

.method public isFlinging()Z
    .locals 1

    .prologue
    .line 401
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mLayout:Lcom/samsung/musicplus/glwidget/layout/GalleryLayout;

    invoke-interface {v0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayout;->isFlinging()Z

    move-result v0

    return v0
.end method

.method public isInEditMode()Z
    .locals 1

    .prologue
    .line 406
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mParentView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isInEditMode()Z

    move-result v0

    return v0
.end method

.method public isOpaque()Z
    .locals 1

    .prologue
    .line 447
    invoke-direct {p0}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->ensureUIThread()V

    .line 449
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mParentView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isOpaque()Z

    move-result v0

    return v0
.end method

.method public isTouched()Z
    .locals 1

    .prologue
    .line 397
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mLayout:Lcom/samsung/musicplus/glwidget/layout/GalleryLayout;

    invoke-interface {v0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayout;->isTouched()Z

    move-result v0

    return v0
.end method

.method public onKeyDown(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "view"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v0, 0x1

    .line 236
    invoke-direct {p0}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->ensureUIThread()V

    .line 241
    sparse-switch p2, :sswitch_data_0

    .line 252
    const/4 v0, 0x0

    :goto_0
    :sswitch_0
    return v0

    .line 243
    :sswitch_1
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "previous"

    invoke-static {v1, v2, v0}, Lcom/samsung/musicplus/util/player/PlayUtils;->startCommand(Landroid/content/Context;Ljava/lang/String;Z)V

    goto :goto_0

    .line 246
    :sswitch_2
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "next"

    invoke-static {v1, v2, v0}, Lcom/samsung/musicplus/util/player/PlayUtils;->startCommand(Landroid/content/Context;Ljava/lang/String;Z)V

    goto :goto_0

    .line 241
    nop

    :sswitch_data_0
    .sparse-switch
        0x15 -> :sswitch_1
        0x16 -> :sswitch_2
        0x17 -> :sswitch_0
        0x42 -> :sswitch_0
    .end sparse-switch
.end method

.method public onKeyUp(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v0, 0x1

    .line 261
    invoke-direct {p0}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->ensureUIThread()V

    .line 266
    sparse-switch p2, :sswitch_data_0

    .line 282
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 268
    :sswitch_0
    invoke-virtual {p1, v0}, Landroid/view/View;->playSoundEffect(I)V

    .line 269
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->getSelectedItemPosition()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->setSelection(I)V

    .line 270
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->fireOnSelectionChange()V

    goto :goto_0

    .line 273
    :sswitch_1
    const/4 v1, 0x3

    invoke-virtual {p1, v1}, Landroid/view/View;->playSoundEffect(I)V

    .line 274
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->getSelectedItemPosition()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->setSelection(I)V

    .line 275
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->fireOnSelectionChange()V

    goto :goto_0

    .line 279
    :sswitch_2
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->getSelectedItemPosition()I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->fireOnItemClick(I)V

    goto :goto_0

    .line 266
    :sswitch_data_0
    .sparse-switch
        0x15 -> :sswitch_0
        0x16 -> :sswitch_1
        0x17 -> :sswitch_2
        0x42 -> :sswitch_2
    .end sparse-switch
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 185
    invoke-direct {p0}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->ensureUIThread()V

    .line 187
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mPaused:Z

    .line 188
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mLayout:Lcom/samsung/musicplus/glwidget/layout/GalleryLayout;

    invoke-interface {v0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayout;->onPause()V

    .line 189
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 197
    invoke-direct {p0}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->ensureUIThread()V

    .line 199
    iput-boolean v1, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mPaused:Z

    .line 200
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mLayout:Lcom/samsung/musicplus/glwidget/layout/GalleryLayout;

    invoke-interface {v0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayout;->onResume()V

    .line 201
    iget-boolean v0, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mRenderRequested:Z

    if-eqz v0, :cond_0

    .line 202
    iput-boolean v1, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mRenderRequested:Z

    .line 203
    sget-object v0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->LOG_TAG:Ljava/lang/String;

    const-string v1, "additional request render after resume"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 204
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mLayout:Lcom/samsung/musicplus/glwidget/layout/GalleryLayout;

    invoke-interface {v0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayout;->reFlashSelection()V

    .line 205
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->requestRender()V

    .line 207
    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 213
    invoke-direct {p0}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->ensureUIThread()V

    .line 215
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mLayout:Lcom/samsung/musicplus/glwidget/layout/GalleryLayout;

    invoke-interface {v0, p1}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public post(Ljava/lang/Runnable;)Z
    .locals 1
    .param p1, "action"    # Ljava/lang/Runnable;

    .prologue
    .line 420
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mParentView:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    move-result v0

    return v0
.end method

.method public postOnAnimation(Ljava/lang/Runnable;)V
    .locals 1
    .param p1, "action"    # Ljava/lang/Runnable;

    .prologue
    .line 424
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mParentView:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->postOnAnimation(Ljava/lang/Runnable;)V

    .line 425
    return-void
.end method

.method public removeCallbacks(Ljava/lang/Runnable;)Z
    .locals 1
    .param p1, "action"    # Ljava/lang/Runnable;

    .prologue
    .line 428
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mParentView:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    move-result v0

    return v0
.end method

.method public requestRender()V
    .locals 2

    .prologue
    .line 411
    iget-boolean v0, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mPaused:Z

    if-nez v0, :cond_0

    .line 412
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mParent:Lcom/samsung/musicplus/glwidget/GLGalleryView;

    invoke-interface {v0}, Lcom/samsung/musicplus/glwidget/GLGalleryView;->requestRender()V

    .line 417
    :goto_0
    return-void

    .line 414
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mRenderRequested:Z

    .line 415
    sget-object v0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->LOG_TAG:Ljava/lang/String;

    const-string v1, "requestRender ignored during pause"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setAdapter(Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;)V
    .locals 1
    .param p1, "adapter"    # Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;

    .prologue
    .line 94
    invoke-direct {p0}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->ensureUIThread()V

    .line 96
    iput-object p1, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mAdapter:Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;

    .line 97
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mLayout:Lcom/samsung/musicplus/glwidget/layout/GalleryLayout;

    invoke-interface {v0, p1}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayout;->setAdapter(Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;)V

    .line 98
    return-void
.end method

.method public setAdapterWrap(Z)V
    .locals 1
    .param p1, "wrap"    # Z

    .prologue
    .line 479
    invoke-direct {p0}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->ensureUIThread()V

    .line 481
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mLayout:Lcom/samsung/musicplus/glwidget/layout/GalleryLayout;

    invoke-interface {v0, p1}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayout;->setAdapterWrap(Z)V

    .line 482
    return-void
.end method

.method public setAlpha(F)V
    .locals 1
    .param p1, "alpha"    # F

    .prologue
    .line 440
    invoke-direct {p0}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->ensureUIThread()V

    .line 442
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mParentView:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setAlpha(F)V

    .line 443
    return-void
.end method

.method public setDefaultAlbumArt(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 164
    invoke-direct {p0}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->ensureUIThread()V

    .line 166
    iput-object p1, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mDefaultAlbArt:Landroid/graphics/Bitmap;

    .line 167
    return-void
.end method

.method public setOnAnimationListener(Lcom/samsung/musicplus/glwidget/GLGalleryView$OnAnimationListener;)V
    .locals 0
    .param p1, "onAnimationListener"    # Lcom/samsung/musicplus/glwidget/GLGalleryView$OnAnimationListener;

    .prologue
    .line 357
    invoke-direct {p0}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->ensureUIThread()V

    .line 359
    iput-object p1, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mOnAnimationListener:Lcom/samsung/musicplus/glwidget/GLGalleryView$OnAnimationListener;

    .line 360
    return-void
.end method

.method public setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V
    .locals 0
    .param p1, "listener"    # Landroid/widget/AdapterView$OnItemClickListener;

    .prologue
    .line 226
    invoke-direct {p0}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->ensureUIThread()V

    .line 228
    iput-object p1, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mOnItemClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 229
    return-void
.end method

.method public setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V
    .locals 0
    .param p1, "listener"    # Landroid/widget/AdapterView$OnItemSelectedListener;

    .prologue
    .line 151
    invoke-direct {p0}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->ensureUIThread()V

    .line 153
    iput-object p1, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mOnItemSelectedListener:Landroid/widget/AdapterView$OnItemSelectedListener;

    .line 154
    return-void
.end method

.method public setOpaque(Z)V
    .locals 2
    .param p1, "opaque"    # Z

    .prologue
    .line 454
    invoke-direct {p0}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->ensureUIThread()V

    .line 456
    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mParentView:Landroid/view/View;

    instance-of v1, v1, Lcom/samsung/musicplus/glwidget/GLGalleryTextureView;

    if-eqz v1, :cond_1

    .line 457
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mParentView:Landroid/view/View;

    check-cast v0, Lcom/samsung/musicplus/glwidget/GLGalleryTextureView;

    .line 458
    .local v0, "view":Lcom/samsung/musicplus/glwidget/GLGalleryTextureView;
    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/glwidget/GLGalleryTextureView;->setOpaque(Z)V

    .line 463
    .end local v0    # "view":Lcom/samsung/musicplus/glwidget/GLGalleryTextureView;
    :cond_0
    :goto_0
    return-void

    .line 459
    :cond_1
    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mParentView:Landroid/view/View;

    instance-of v1, v1, Lcom/samsung/musicplus/glwidget/GLGallerySurfaceView;

    if-eqz v1, :cond_0

    .line 460
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mParentView:Landroid/view/View;

    check-cast v0, Lcom/samsung/musicplus/glwidget/GLGallerySurfaceView;

    .line 461
    .local v0, "view":Lcom/samsung/musicplus/glwidget/GLGallerySurfaceView;
    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/glwidget/GLGallerySurfaceView;->setOpaque(Z)V

    goto :goto_0
.end method

.method public setSelection(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 108
    invoke-direct {p0}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->ensureUIThread()V

    .line 110
    iget v0, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mSelected:I

    if-ne v0, p1, :cond_0

    .line 131
    :goto_0
    return-void

    .line 113
    :cond_0
    if-gez p1, :cond_1

    .line 114
    const/4 p1, 0x0

    .line 116
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mAdapter:Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mAdapter:Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;

    invoke-interface {v0}, Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;->getCount()I

    move-result v0

    if-lt p1, v0, :cond_2

    .line 117
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mAdapter:Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;

    invoke-interface {v0}, Lcom/samsung/musicplus/glwidget/GLGalleryAdapter;->getCount()I

    move-result v0

    add-int/lit8 p1, v0, -0x1

    .line 119
    :cond_2
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mLayout:Lcom/samsung/musicplus/glwidget/layout/GalleryLayout;

    invoke-interface {v0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayout;->animationFinished()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 120
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mLayout:Lcom/samsung/musicplus/glwidget/layout/GalleryLayout;

    invoke-interface {v0}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayout;->stopAnimation()V

    .line 129
    :cond_3
    iput p1, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mSelected:I

    .line 130
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mLayout:Lcom/samsung/musicplus/glwidget/layout/GalleryLayout;

    invoke-interface {v0, p1}, Lcom/samsung/musicplus/glwidget/layout/GalleryLayout;->setSelection(I)V

    goto :goto_0
.end method

.method public setVisibility(I)V
    .locals 1
    .param p1, "visibility"    # I

    .prologue
    .line 433
    invoke-direct {p0}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->ensureUIThread()V

    .line 435
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->mParentView:Landroid/view/View;

    invoke-virtual {v0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 436
    return-void
.end method
