.class public Lcom/samsung/musicplus/glwidget/model/ViewModel;
.super Lcom/samsung/musicplus/glwidget/model/AnimationModel;
.source "ViewModel.java"

# interfaces
.implements Lcom/samsung/musicplus/glwidget/render/RenderableBitmap;


# instance fields
.field private mAlpha:F

.field private mBitmapView:Lcom/samsung/musicplus/glwidget/render/BitmapView;

.field private mUpdated:Z


# direct methods
.method public constructor <init>(Landroid/view/View;II)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/samsung/musicplus/glwidget/model/AnimationModel;-><init>()V

    .line 22
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/samsung/musicplus/glwidget/model/ViewModel;->mAlpha:F

    .line 25
    new-instance v0, Lcom/samsung/musicplus/glwidget/render/BitmapView;

    invoke-direct {v0, p1, p2, p3}, Lcom/samsung/musicplus/glwidget/render/BitmapView;-><init>(Landroid/view/View;II)V

    iput-object v0, p0, Lcom/samsung/musicplus/glwidget/model/ViewModel;->mBitmapView:Lcom/samsung/musicplus/glwidget/render/BitmapView;

    .line 26
    return-void
.end method


# virtual methods
.method public getAlpha()F
    .locals 1

    .prologue
    .line 59
    iget v0, p0, Lcom/samsung/musicplus/glwidget/model/ViewModel;->mAlpha:F

    return v0
.end method

.method public getRenderableBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/model/ViewModel;->mBitmapView:Lcom/samsung/musicplus/glwidget/render/BitmapView;

    invoke-virtual {v0}, Lcom/samsung/musicplus/glwidget/render/BitmapView;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getView()Landroid/view/View;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/model/ViewModel;->mBitmapView:Lcom/samsung/musicplus/glwidget/render/BitmapView;

    invoke-virtual {v0}, Lcom/samsung/musicplus/glwidget/render/BitmapView;->getView()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public invalidate()V
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/glwidget/model/ViewModel;->mUpdated:Z

    .line 34
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/model/ViewModel;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    .line 35
    return-void
.end method

.method public isRenderable()Z
    .locals 1

    .prologue
    .line 54
    invoke-virtual {p0}, Lcom/samsung/musicplus/glwidget/model/ViewModel;->isVisible()Z

    move-result v0

    return v0
.end method

.method public resetUpdated()V
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/glwidget/model/ViewModel;->mUpdated:Z

    .line 45
    return-void
.end method

.method public setAlpha(F)V
    .locals 0
    .param p1, "alpha"    # F

    .prologue
    .line 63
    iput p1, p0, Lcom/samsung/musicplus/glwidget/model/ViewModel;->mAlpha:F

    .line 64
    return-void
.end method

.method public updated()Z
    .locals 1

    .prologue
    .line 39
    iget-boolean v0, p0, Lcom/samsung/musicplus/glwidget/model/ViewModel;->mUpdated:Z

    return v0
.end method
