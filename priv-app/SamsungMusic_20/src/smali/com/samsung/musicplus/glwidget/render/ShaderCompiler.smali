.class public Lcom/samsung/musicplus/glwidget/render/ShaderCompiler;
.super Ljava/lang/Object;
.source "ShaderCompiler.java"


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "ShaderCompiler"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static addDefines([Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "defines"    # [Ljava/lang/String;
    .param p1, "source"    # Ljava/lang/String;

    .prologue
    .line 116
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 117
    .local v1, "res":Ljava/lang/StringBuilder;
    if-eqz p0, :cond_0

    .line 118
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v2, p0

    if-ge v0, v2, :cond_0

    .line 119
    const-string v2, "#define "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 120
    aget-object v2, p0, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 121
    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 118
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 124
    .end local v0    # "i":I
    :cond_0
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 125
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method public static compile(Landroid/content/Context;[Ljava/lang/String;II)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "defines"    # [Ljava/lang/String;
    .param p2, "vertexResource"    # I
    .param p3, "fragmentResource"    # I

    .prologue
    .line 31
    invoke-static {p0, p2}, Lcom/samsung/musicplus/glwidget/render/ShaderCompiler;->loadShader(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    .line 32
    .local v1, "vertex":Ljava/lang/String;
    invoke-static {p0, p3}, Lcom/samsung/musicplus/glwidget/render/ShaderCompiler;->loadShader(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    .line 33
    .local v0, "fragment":Ljava/lang/String;
    invoke-static {p0, p1, v1, v0}, Lcom/samsung/musicplus/glwidget/render/ShaderCompiler;->compile(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    return v2
.end method

.method public static compile(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "defines"    # [Ljava/lang/String;
    .param p2, "vertex"    # Ljava/lang/String;
    .param p3, "fragment"    # Ljava/lang/String;

    .prologue
    const v10, 0x8b81

    const/4 v9, 0x0

    .line 46
    const v6, 0x8b31

    invoke-static {v6}, Landroid/opengl/GLES20;->glCreateShader(I)I

    move-result v5

    .line 47
    .local v5, "vertexHandler":I
    invoke-static {p1, p2}, Lcom/samsung/musicplus/glwidget/render/ShaderCompiler;->addDefines([Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/opengl/GLES20;->glShaderSource(ILjava/lang/String;)V

    .line 48
    invoke-static {v5}, Landroid/opengl/GLES20;->glCompileShader(I)V

    .line 49
    const/4 v0, 0x0

    .line 50
    .local v0, "compilationFailed":Z
    const/4 v6, 0x1

    new-array v2, v6, [I

    .line 51
    .local v2, "compiled":[I
    invoke-static {v5, v10, v2, v9}, Landroid/opengl/GLES20;->glGetShaderiv(II[II)V

    .line 52
    aget v6, v2, v9

    if-nez v6, :cond_0

    .line 53
    invoke-static {v5}, Landroid/opengl/GLES20;->glGetShaderInfoLog(I)Ljava/lang/String;

    move-result-object v1

    .line 54
    .local v1, "compileLog":Ljava/lang/String;
    const-string v6, "ShaderCompiler"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "VertexShader compilation log: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    const/4 v0, 0x1

    .line 58
    .end local v1    # "compileLog":Ljava/lang/String;
    :cond_0
    const v6, 0x8b30

    invoke-static {v6}, Landroid/opengl/GLES20;->glCreateShader(I)I

    move-result v3

    .line 59
    .local v3, "fragmentHandler":I
    invoke-static {p1, p3}, Lcom/samsung/musicplus/glwidget/render/ShaderCompiler;->addDefines([Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/opengl/GLES20;->glShaderSource(ILjava/lang/String;)V

    .line 60
    invoke-static {v3}, Landroid/opengl/GLES20;->glCompileShader(I)V

    .line 62
    invoke-static {v3, v10, v2, v9}, Landroid/opengl/GLES20;->glGetShaderiv(II[II)V

    .line 63
    aget v6, v2, v9

    if-nez v6, :cond_1

    .line 64
    invoke-static {v3}, Landroid/opengl/GLES20;->glGetShaderInfoLog(I)Ljava/lang/String;

    move-result-object v1

    .line 65
    .restart local v1    # "compileLog":Ljava/lang/String;
    const-string v6, "ShaderCompiler"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "FragmentShader compilation log: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 66
    const/4 v0, 0x1

    .line 69
    .end local v1    # "compileLog":Ljava/lang/String;
    :cond_1
    if-eqz v0, :cond_2

    .line 70
    new-instance v6, Ljava/lang/RuntimeException;

    const-string v7, "Unable to compile shader, check log for detail"

    invoke-direct {v6, v7}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 73
    :cond_2
    invoke-static {}, Landroid/opengl/GLES20;->glCreateProgram()I

    move-result v4

    .line 74
    .local v4, "handler":I
    invoke-static {v4, v5}, Landroid/opengl/GLES20;->glAttachShader(II)V

    .line 75
    invoke-static {v4, v3}, Landroid/opengl/GLES20;->glAttachShader(II)V

    .line 76
    invoke-static {v4}, Landroid/opengl/GLES20;->glLinkProgram(I)V

    .line 77
    return v4
.end method

.method public static loadShader(Landroid/content/Context;I)Ljava/lang/String;
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "resourceId"    # I

    .prologue
    .line 91
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, p1}, Landroid/content/res/Resources;->openRawResource(I)Ljava/io/InputStream;

    move-result-object v2

    .line 93
    .local v2, "is":Ljava/io/InputStream;
    const/16 v5, 0x400

    :try_start_0
    new-array v0, v5, [B

    .line 94
    .local v0, "buffer":[B
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/io/InputStream;->available()I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 95
    .local v4, "str":Ljava/lang/StringBuilder;
    const/4 v5, 0x0

    invoke-virtual {v2}, Ljava/io/InputStream;->available()I

    move-result v6

    const/16 v7, 0x400

    invoke-static {v6, v7}, Ljava/lang/Math;->min(II)I

    move-result v6

    invoke-virtual {v2, v0, v5, v6}, Ljava/io/InputStream;->read([BII)I

    move-result v3

    .line 96
    .local v3, "read":I
    :goto_0
    if-lez v3, :cond_0

    .line 97
    new-instance v5, Ljava/lang/String;

    const/4 v6, 0x0

    const-string v7, "US-ASCII"

    invoke-static {v7}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v7

    invoke-direct {v5, v0, v6, v3, v7}, Ljava/lang/String;-><init>([BIILjava/nio/charset/Charset;)V

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 98
    const/4 v5, 0x0

    invoke-virtual {v2}, Ljava/io/InputStream;->available()I

    move-result v6

    const/16 v7, 0x400

    invoke-static {v6, v7}, Ljava/lang/Math;->min(II)I

    move-result v6

    invoke-virtual {v2, v0, v5, v6}, Ljava/io/InputStream;->read([BII)I

    move-result v3

    goto :goto_0

    .line 100
    :cond_0
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v5

    .line 104
    if-eqz v2, :cond_1

    .line 106
    :try_start_1
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 112
    .end local v0    # "buffer":[B
    .end local v3    # "read":I
    .end local v4    # "str":Ljava/lang/StringBuilder;
    :cond_1
    :goto_1
    return-object v5

    .line 101
    :catch_0
    move-exception v1

    .line 102
    .local v1, "e":Ljava/io/IOException;
    :try_start_2
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 104
    if-eqz v2, :cond_2

    .line 106
    :try_start_3
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 112
    :cond_2
    :goto_2
    const/4 v5, 0x0

    goto :goto_1

    .line 104
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v5

    if-eqz v2, :cond_3

    .line 106
    :try_start_4
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 109
    :cond_3
    :goto_3
    throw v5

    .line 107
    .restart local v0    # "buffer":[B
    .restart local v3    # "read":I
    .restart local v4    # "str":Ljava/lang/StringBuilder;
    :catch_1
    move-exception v6

    goto :goto_1

    .end local v0    # "buffer":[B
    .end local v3    # "read":I
    .end local v4    # "str":Ljava/lang/StringBuilder;
    .restart local v1    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v5

    goto :goto_2

    .end local v1    # "e":Ljava/io/IOException;
    :catch_3
    move-exception v6

    goto :goto_3
.end method
