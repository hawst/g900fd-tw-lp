.class Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout$AlbArtPublisher;
.super Ljava/lang/Object;
.source "FlatResizeLayout.java"

# interfaces
.implements Lcom/samsung/musicplus/bitmapcache/BitmapCache$Publisher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AlbArtPublisher"
.end annotation


# instance fields
.field private mAdapterIdx:I

.field private mAlbumModel:Lcom/samsung/musicplus/glwidget/model/BitmapsModel;

.field final synthetic this$0:Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;


# direct methods
.method public constructor <init>(Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;ILcom/samsung/musicplus/glwidget/model/BitmapsModel;)V
    .locals 0
    .param p2, "adapterIdx"    # I
    .param p3, "albumModel"    # Lcom/samsung/musicplus/glwidget/model/BitmapsModel;

    .prologue
    .line 601
    iput-object p1, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout$AlbArtPublisher;->this$0:Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 602
    iput p2, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout$AlbArtPublisher;->mAdapterIdx:I

    .line 603
    iput-object p3, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout$AlbArtPublisher;->mAlbumModel:Lcom/samsung/musicplus/glwidget/model/BitmapsModel;

    .line 604
    return-void
.end method

.method private stillProperModel()Z
    .locals 2

    .prologue
    .line 607
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout$AlbArtPublisher;->mAlbumModel:Lcom/samsung/musicplus/glwidget/model/BitmapsModel;

    invoke-virtual {v0}, Lcom/samsung/musicplus/glwidget/model/BitmapsModel;->getPublisher()Lcom/samsung/musicplus/bitmapcache/BitmapCache$Publisher;

    move-result-object v0

    if-ne v0, p0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout$AlbArtPublisher;->mAlbumModel:Lcom/samsung/musicplus/glwidget/model/BitmapsModel;

    invoke-virtual {v0}, Lcom/samsung/musicplus/glwidget/model/BitmapsModel;->getAdapterIndex()I

    move-result v0

    iget v1, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout$AlbArtPublisher;->mAdapterIdx:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onFailPublishImage(Landroid/net/Uri;J)V
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "elapsedTime"    # J

    .prologue
    .line 621
    invoke-direct {p0}, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout$AlbArtPublisher;->stillProperModel()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 622
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout$AlbArtPublisher;->mAlbumModel:Lcom/samsung/musicplus/glwidget/model/BitmapsModel;

    iget-object v1, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout$AlbArtPublisher;->this$0:Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;

    iget-object v1, v1, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mParent:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v1}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->getDefaultAlbumArt()Landroid/graphics/Bitmap;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/samsung/musicplus/glwidget/model/BitmapsModel;->setBitmap(Landroid/graphics/Bitmap;I)V

    .line 624
    :cond_0
    return-void
.end method

.method public onPublishImage(Landroid/net/Uri;Landroid/graphics/Bitmap;J)V
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;
    .param p3, "elapsedTime"    # J

    .prologue
    .line 613
    invoke-direct {p0}, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout$AlbArtPublisher;->stillProperModel()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 614
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout$AlbArtPublisher;->mAlbumModel:Lcom/samsung/musicplus/glwidget/model/BitmapsModel;

    const/4 v1, 0x0

    invoke-virtual {v0, p2, v1}, Lcom/samsung/musicplus/glwidget/model/BitmapsModel;->setBitmap(Landroid/graphics/Bitmap;I)V

    .line 615
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout$AlbArtPublisher;->this$0:Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;

    iget-object v0, v0, Lcom/samsung/musicplus/glwidget/layout/FlatResizeLayout;->mParent:Lcom/samsung/musicplus/glwidget/layout/GLHolder;

    invoke-virtual {v0}, Lcom/samsung/musicplus/glwidget/layout/GLHolder;->requestRender()V

    .line 617
    :cond_0
    return-void
.end method
