.class Lcom/samsung/musicplus/glwidget/layout/FlatLayout$Config;
.super Ljava/lang/Object;
.source "FlatLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/glwidget/layout/FlatLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Config"
.end annotation


# instance fields
.field private aAlbumSpacing:I

.field private mAlbumSize:I

.field private mBackgroundColor:I

.field private mClickAnimation:I

.field private mMaxTiltedAngle:I

.field private mScrollingFriction:F

.field private mShadowBitmap:I

.field private mTextLayout:I

.field private mTiltedK:F

.field private mTiltedReturnAnimation:I


# direct methods
.method public constructor <init>(Landroid/content/res/TypedArray;)V
    .locals 3
    .param p1, "a"    # Landroid/content/res/TypedArray;

    .prologue
    const/4 v2, -0x1

    .line 988
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 990
    const/4 v0, 0x4

    const/16 v1, 0xfa

    :try_start_0
    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout$Config;->mAlbumSize:I

    .line 991
    const/4 v0, 0x5

    const/16 v1, 0x14

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout$Config;->aAlbumSpacing:I

    .line 993
    const/4 v0, 0x6

    const/high16 v1, -0x1000000

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout$Config;->mBackgroundColor:I

    .line 995
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout$Config;->mMaxTiltedAngle:I

    .line 996
    iget v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout$Config;->mMaxTiltedAngle:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout$Config;->mMaxTiltedAngle:I

    const/16 v1, 0x5a

    if-le v0, v1, :cond_1

    .line 997
    :cond_0
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Max titled angle should be less then 90 degrees"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1019
    :catchall_0
    move-exception v0

    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    throw v0

    .line 999
    :cond_1
    const/4 v0, 0x7

    const/4 v1, -0x1

    :try_start_1
    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout$Config;->mTextLayout:I

    .line 1000
    iget v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout$Config;->mTextLayout:I

    if-ne v0, v2, :cond_2

    .line 1001
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Text layout is mandatory for GLGalleryView"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1003
    :cond_2
    const/4 v0, 0x2

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout$Config;->mClickAnimation:I

    .line 1005
    const/4 v0, 0x3

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout$Config;->mTiltedReturnAnimation:I

    .line 1007
    iget v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout$Config;->mTiltedReturnAnimation:I

    if-ne v0, v2, :cond_3

    .line 1008
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "tiltedReturnAnimation is mandatory for GLGalleryView"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1011
    :cond_3
    const/4 v0, 0x1

    const/high16 v1, 0x41f00000    # 30.0f

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout$Config;->mTiltedK:F

    .line 1013
    const/16 v0, 0x8

    invoke-static {}, Landroid/view/ViewConfiguration;->getScrollFriction()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout$Config;->mScrollingFriction:F

    .line 1016
    const/16 v0, 0x9

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout$Config;->mShadowBitmap:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1019
    invoke-virtual {p1}, Landroid/content/res/TypedArray;->recycle()V

    .line 1021
    return-void
.end method


# virtual methods
.method public getAlbumSize()I
    .locals 1

    .prologue
    .line 949
    iget v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout$Config;->mAlbumSize:I

    return v0
.end method

.method public getAlbumSpacing()I
    .locals 1

    .prologue
    .line 957
    iget v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout$Config;->aAlbumSpacing:I

    return v0
.end method

.method public getBackgroundColor()I
    .locals 1

    .prologue
    .line 961
    iget v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout$Config;->mBackgroundColor:I

    return v0
.end method

.method public getClickAnimation()I
    .locals 1

    .prologue
    .line 969
    iget v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout$Config;->mClickAnimation:I

    return v0
.end method

.method public getMaxTiltedAngle()I
    .locals 1

    .prologue
    .line 953
    iget v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout$Config;->mMaxTiltedAngle:I

    return v0
.end method

.method public getScrollingFriction()F
    .locals 1

    .prologue
    .line 981
    iget v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout$Config;->mScrollingFriction:F

    return v0
.end method

.method public getShadowBitmap()I
    .locals 1

    .prologue
    .line 985
    iget v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout$Config;->mShadowBitmap:I

    return v0
.end method

.method public getTextLayout()I
    .locals 1

    .prologue
    .line 965
    iget v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout$Config;->mTextLayout:I

    return v0
.end method

.method public getTiltedK()F
    .locals 1

    .prologue
    .line 977
    iget v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout$Config;->mTiltedK:F

    return v0
.end method

.method public getTiltedReturnAnimation()I
    .locals 1

    .prologue
    .line 973
    iget v0, p0, Lcom/samsung/musicplus/glwidget/layout/FlatLayout$Config;->mTiltedReturnAnimation:I

    return v0
.end method
