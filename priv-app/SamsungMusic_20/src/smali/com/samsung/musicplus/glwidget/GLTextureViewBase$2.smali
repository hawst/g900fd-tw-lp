.class Lcom/samsung/musicplus/glwidget/GLTextureViewBase$2;
.super Ljava/lang/Object;
.source "GLTextureViewBase.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/glwidget/GLTextureViewBase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/glwidget/GLTextureViewBase;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/glwidget/GLTextureViewBase;)V
    .locals 0

    .prologue
    .line 487
    iput-object p1, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase$2;->this$0:Lcom/samsung/musicplus/glwidget/GLTextureViewBase;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v0, 0x0

    .line 490
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 500
    :goto_0
    :pswitch_0
    return v0

    .line 493
    :pswitch_1
    iget-object v2, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase$2;->this$0:Lcom/samsung/musicplus/glwidget/GLTextureViewBase;

    iget v1, p1, Landroid/os/Message;->what:I

    if-nez v1, :cond_0

    const/4 v0, 0x1

    move v1, v0

    :goto_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/SurfaceTexture;

    iget v3, p1, Landroid/os/Message;->arg1:I

    iget v4, p1, Landroid/os/Message;->arg2:I

    # invokes: Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->handleTextureAvailableSizeChanged(ZLandroid/graphics/SurfaceTexture;II)Z
    invoke-static {v2, v1, v0, v3, v4}, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->access$500(Lcom/samsung/musicplus/glwidget/GLTextureViewBase;ZLandroid/graphics/SurfaceTexture;II)Z

    move-result v0

    goto :goto_0

    :cond_0
    move v1, v0

    goto :goto_1

    .line 496
    :pswitch_2
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase$2;->this$0:Lcom/samsung/musicplus/glwidget/GLTextureViewBase;

    # invokes: Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->handleRequestRender()Z
    invoke-static {v0}, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->access$600(Lcom/samsung/musicplus/glwidget/GLTextureViewBase;)Z

    move-result v0

    goto :goto_0

    .line 498
    :pswitch_3
    iget-object v0, p0, Lcom/samsung/musicplus/glwidget/GLTextureViewBase$2;->this$0:Lcom/samsung/musicplus/glwidget/GLTextureViewBase;

    # invokes: Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->handleTextureDestroyed()Z
    invoke-static {v0}, Lcom/samsung/musicplus/glwidget/GLTextureViewBase;->access$700(Lcom/samsung/musicplus/glwidget/GLTextureViewBase;)Z

    move-result v0

    goto :goto_0

    .line 490
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
