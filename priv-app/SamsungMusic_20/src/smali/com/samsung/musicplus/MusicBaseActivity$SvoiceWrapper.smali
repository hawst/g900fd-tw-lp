.class Lcom/samsung/musicplus/MusicBaseActivity$SvoiceWrapper;
.super Ljava/lang/Object;
.source "MusicBaseActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/MusicBaseActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SvoiceWrapper"
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final mSvoiceListener:Lcom/samsung/musicplus/library/audio/SvoiceManager$OnSvoiceListener;

.field private mSvoiceManager:Lcom/samsung/musicplus/library/audio/SvoiceManager;

.field final synthetic this$0:Lcom/samsung/musicplus/MusicBaseActivity;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/MusicBaseActivity;Landroid/content/Context;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 961
    iput-object p1, p0, Lcom/samsung/musicplus/MusicBaseActivity$SvoiceWrapper;->this$0:Lcom/samsung/musicplus/MusicBaseActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1001
    new-instance v0, Lcom/samsung/musicplus/MusicBaseActivity$SvoiceWrapper$1;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/MusicBaseActivity$SvoiceWrapper$1;-><init>(Lcom/samsung/musicplus/MusicBaseActivity$SvoiceWrapper;)V

    iput-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity$SvoiceWrapper;->mSvoiceListener:Lcom/samsung/musicplus/library/audio/SvoiceManager$OnSvoiceListener;

    .line 962
    sget-boolean v0, Lcom/samsung/musicplus/util/Version;->LIBRARY_API_1:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_VOICE_COMMAND_CONTROL:Z

    if-eqz v0, :cond_1

    .line 963
    :cond_0
    invoke-static {p2}, Lcom/samsung/musicplus/library/audio/SvoiceManager;->getInstance(Landroid/content/Context;)Lcom/samsung/musicplus/library/audio/SvoiceManager;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity$SvoiceWrapper;->mSvoiceManager:Lcom/samsung/musicplus/library/audio/SvoiceManager;

    .line 965
    :cond_1
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/MusicBaseActivity$SvoiceWrapper;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/MusicBaseActivity$SvoiceWrapper;

    .prologue
    .line 958
    invoke-direct {p0}, Lcom/samsung/musicplus/MusicBaseActivity$SvoiceWrapper;->start()V

    return-void
.end method

.method static synthetic access$100(Lcom/samsung/musicplus/MusicBaseActivity$SvoiceWrapper;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/MusicBaseActivity$SvoiceWrapper;

    .prologue
    .line 958
    invoke-direct {p0}, Lcom/samsung/musicplus/MusicBaseActivity$SvoiceWrapper;->stop()V

    return-void
.end method

.method static synthetic access$500(Lcom/samsung/musicplus/MusicBaseActivity$SvoiceWrapper;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/MusicBaseActivity$SvoiceWrapper;
    .param p1, "x1"    # I

    .prologue
    .line 958
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/MusicBaseActivity$SvoiceWrapper;->sendMusicCommand(I)V

    return-void
.end method

.method private getSvoiceNotificationInfo()Lcom/samsung/musicplus/library/audio/SvoiceManager$SvoiceNotiInfo;
    .locals 11

    .prologue
    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1054
    new-instance v2, Lcom/samsung/musicplus/library/audio/SvoiceManager$SvoiceNotiInfo;

    invoke-direct {v2}, Lcom/samsung/musicplus/library/audio/SvoiceManager$SvoiceNotiInfo;-><init>()V

    .line 1055
    .local v2, "info":Lcom/samsung/musicplus/library/audio/SvoiceManager$SvoiceNotiInfo;
    iget-object v3, p0, Lcom/samsung/musicplus/MusicBaseActivity$SvoiceWrapper;->this$0:Lcom/samsung/musicplus/MusicBaseActivity;

    const v4, 0x7f100023

    invoke-virtual {v3, v4}, Lcom/samsung/musicplus/MusicBaseActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/musicplus/library/audio/SvoiceManager$SvoiceNotiInfo;->appName:Ljava/lang/String;

    .line 1056
    const v3, 0x7f0200be

    iput v3, v2, Lcom/samsung/musicplus/library/audio/SvoiceManager$SvoiceNotiInfo;->iconId:I

    .line 1057
    iget-object v3, p0, Lcom/samsung/musicplus/MusicBaseActivity$SvoiceWrapper;->this$0:Lcom/samsung/musicplus/MusicBaseActivity;

    invoke-virtual {v3}, Lcom/samsung/musicplus/MusicBaseActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 1058
    .local v1, "context":Landroid/content/Context;
    iget-object v3, p0, Lcom/samsung/musicplus/MusicBaseActivity$SvoiceWrapper;->this$0:Lcom/samsung/musicplus/MusicBaseActivity;

    invoke-virtual {v3}, Lcom/samsung/musicplus/MusicBaseActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f090002

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    .line 1059
    .local v0, "commands":[Ljava/lang/String;
    const v3, 0x7f10002c

    const/4 v4, 0x6

    new-array v4, v4, [Ljava/lang/Object;

    aget-object v5, v0, v6

    aput-object v5, v4, v6

    aget-object v5, v0, v7

    aput-object v5, v4, v7

    aget-object v5, v0, v8

    aput-object v5, v4, v8

    aget-object v5, v0, v9

    aput-object v5, v4, v9

    aget-object v5, v0, v10

    aput-object v5, v4, v10

    const/4 v5, 0x5

    const/4 v6, 0x5

    aget-object v6, v0, v6

    aput-object v6, v4, v5

    invoke-virtual {v1, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/samsung/musicplus/library/audio/SvoiceManager$SvoiceNotiInfo;->notiString:Ljava/lang/String;

    .line 1061
    return-object v2
.end method

.method private sendMusicCommand(I)V
    .locals 2
    .param p1, "command"    # I

    .prologue
    const/4 v1, 0x0

    .line 1010
    packed-switch p1, :pswitch_data_0

    .line 1033
    :goto_0
    return-void

    .line 1012
    :pswitch_0
    const-string v0, "next"

    invoke-direct {p0, v0, v1}, Lcom/samsung/musicplus/MusicBaseActivity$SvoiceWrapper;->startService(Ljava/lang/String;Z)V

    goto :goto_0

    .line 1015
    :pswitch_1
    const-string v0, "pause"

    invoke-direct {p0, v0, v1}, Lcom/samsung/musicplus/MusicBaseActivity$SvoiceWrapper;->startService(Ljava/lang/String;Z)V

    goto :goto_0

    .line 1019
    :pswitch_2
    const-string v0, "previous"

    invoke-direct {p0, v0, v1}, Lcom/samsung/musicplus/MusicBaseActivity$SvoiceWrapper;->startService(Ljava/lang/String;Z)V

    goto :goto_0

    .line 1022
    :pswitch_3
    const-string v0, "play"

    invoke-direct {p0, v0, v1}, Lcom/samsung/musicplus/MusicBaseActivity$SvoiceWrapper;->startService(Ljava/lang/String;Z)V

    goto :goto_0

    .line 1025
    :pswitch_4
    const-string v0, "volume_down"

    invoke-direct {p0, v0, v1}, Lcom/samsung/musicplus/MusicBaseActivity$SvoiceWrapper;->startService(Ljava/lang/String;Z)V

    goto :goto_0

    .line 1028
    :pswitch_5
    const-string v0, "volume_up"

    invoke-direct {p0, v0, v1}, Lcom/samsung/musicplus/MusicBaseActivity$SvoiceWrapper;->startService(Ljava/lang/String;Z)V

    goto :goto_0

    .line 1010
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_5
        :pswitch_4
    .end packed-switch
.end method

.method private start()V
    .locals 3

    .prologue
    .line 968
    iget-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity$SvoiceWrapper;->mSvoiceManager:Lcom/samsung/musicplus/library/audio/SvoiceManager;

    if-eqz v0, :cond_1

    .line 969
    iget-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity$SvoiceWrapper;->this$0:Lcom/samsung/musicplus/MusicBaseActivity;

    # getter for: Lcom/samsung/musicplus/MusicBaseActivity;->mMiniPlayer:Lcom/samsung/musicplus/player/MiniPlayer;
    invoke-static {v0}, Lcom/samsung/musicplus/MusicBaseActivity;->access$400(Lcom/samsung/musicplus/MusicBaseActivity;)Lcom/samsung/musicplus/player/MiniPlayer;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity$SvoiceWrapper;->this$0:Lcom/samsung/musicplus/MusicBaseActivity;

    instance-of v0, v0, Lcom/samsung/musicplus/player/PlayerActivity;

    if-eqz v0, :cond_2

    .line 970
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity$SvoiceWrapper;->mSvoiceManager:Lcom/samsung/musicplus/library/audio/SvoiceManager;

    invoke-direct {p0}, Lcom/samsung/musicplus/MusicBaseActivity$SvoiceWrapper;->getSvoiceNotificationInfo()Lcom/samsung/musicplus/library/audio/SvoiceManager$SvoiceNotiInfo;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/MusicBaseActivity$SvoiceWrapper;->mSvoiceListener:Lcom/samsung/musicplus/library/audio/SvoiceManager$OnSvoiceListener;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/musicplus/library/audio/SvoiceManager;->startRecognition(Lcom/samsung/musicplus/library/audio/SvoiceManager$SvoiceNotiInfo;Lcom/samsung/musicplus/library/audio/SvoiceManager$OnSvoiceListener;)V

    .line 985
    :cond_1
    :goto_0
    return-void

    .line 975
    :cond_2
    iget-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity$SvoiceWrapper;->this$0:Lcom/samsung/musicplus/MusicBaseActivity;

    iget-object v0, v0, Lcom/samsung/musicplus/MusicBaseActivity;->mMiniPlayerRoot:Landroid/view/View;

    if-eqz v0, :cond_3

    .line 976
    iget-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity$SvoiceWrapper;->mSvoiceManager:Lcom/samsung/musicplus/library/audio/SvoiceManager;

    invoke-direct {p0}, Lcom/samsung/musicplus/MusicBaseActivity$SvoiceWrapper;->getSvoiceNotificationInfo()Lcom/samsung/musicplus/library/audio/SvoiceManager$SvoiceNotiInfo;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/MusicBaseActivity$SvoiceWrapper;->mSvoiceListener:Lcom/samsung/musicplus/library/audio/SvoiceManager$OnSvoiceListener;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/musicplus/library/audio/SvoiceManager;->startRecognition(Lcom/samsung/musicplus/library/audio/SvoiceManager$SvoiceNotiInfo;Lcom/samsung/musicplus/library/audio/SvoiceManager$OnSvoiceListener;)V

    goto :goto_0

    .line 981
    :cond_3
    iget-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity$SvoiceWrapper;->mSvoiceManager:Lcom/samsung/musicplus/library/audio/SvoiceManager;

    invoke-virtual {v0}, Lcom/samsung/musicplus/library/audio/SvoiceManager;->stopRecognition()V

    goto :goto_0
.end method

.method private startService(Ljava/lang/String;Z)V
    .locals 3
    .param p1, "command"    # Ljava/lang/String;
    .param p2, "force"    # Z

    .prologue
    .line 1045
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/samsung/musicplus/MusicBaseActivity$SvoiceWrapper;->this$0:Lcom/samsung/musicplus/MusicBaseActivity;

    const-class v2, Lcom/samsung/musicplus/service/PlayerService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1046
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "com.samsung.musicplus.action.SERVICE_COMMAND"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1047
    const-string v1, "command"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 1048
    const-string v1, "force"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1049
    const-string v1, "svoice_command"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 1050
    iget-object v1, p0, Lcom/samsung/musicplus/MusicBaseActivity$SvoiceWrapper;->this$0:Lcom/samsung/musicplus/MusicBaseActivity;

    invoke-virtual {v1, v0}, Lcom/samsung/musicplus/MusicBaseActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 1051
    return-void
.end method

.method private stop()V
    .locals 1

    .prologue
    .line 993
    iget-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity$SvoiceWrapper;->mSvoiceManager:Lcom/samsung/musicplus/library/audio/SvoiceManager;

    if-eqz v0, :cond_1

    .line 994
    iget-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity$SvoiceWrapper;->this$0:Lcom/samsung/musicplus/MusicBaseActivity;

    instance-of v0, v0, Lcom/samsung/musicplus/MusicMainActivity;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity$SvoiceWrapper;->this$0:Lcom/samsung/musicplus/MusicBaseActivity;

    invoke-virtual {v0}, Lcom/samsung/musicplus/MusicBaseActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/musicplus/util/UiUtils;->isMusicUiTop(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity$SvoiceWrapper;->this$0:Lcom/samsung/musicplus/MusicBaseActivity;

    invoke-virtual {v0}, Lcom/samsung/musicplus/MusicBaseActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 996
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity$SvoiceWrapper;->mSvoiceManager:Lcom/samsung/musicplus/library/audio/SvoiceManager;

    invoke-virtual {v0}, Lcom/samsung/musicplus/library/audio/SvoiceManager;->stopRecognition()V

    .line 999
    :cond_1
    return-void
.end method
