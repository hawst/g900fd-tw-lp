.class Lcom/samsung/musicplus/provider/MusicProvider$MoodUpdateHandler;
.super Landroid/os/Handler;
.source "MusicProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/provider/MusicProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "MoodUpdateHandler"
.end annotation


# static fields
.field private static final UPDATE_PER_COUNT:I = 0x1f4

.field private static sMoodUpdateHandler:Lcom/samsung/musicplus/provider/MusicProvider$MoodUpdateHandler;


# instance fields
.field private mRunningTask:Z


# direct methods
.method public constructor <init>(Landroid/os/Looper;)V
    .locals 3
    .param p1, "l"    # Landroid/os/Looper;

    .prologue
    const/4 v2, 0x0

    .line 2542
    invoke-direct {p0, p1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 2528
    iput-boolean v2, p0, Lcom/samsung/musicplus/provider/MusicProvider$MoodUpdateHandler;->mRunningTask:Z

    .line 2543
    # getter for: Lcom/samsung/musicplus/provider/MusicProvider;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/provider/MusicProvider;->access$300()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MoodUpdateHandler created"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2544
    iput-boolean v2, p0, Lcom/samsung/musicplus/provider/MusicProvider$MoodUpdateHandler;->mRunningTask:Z

    .line 2546
    return-void
.end method

.method static synthetic access$800(Lcom/samsung/musicplus/provider/MusicProvider$MoodUpdateHandler;Lcom/samsung/musicplus/provider/MusicProvider;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/provider/MusicProvider$MoodUpdateHandler;
    .param p1, "x1"    # Lcom/samsung/musicplus/provider/MusicProvider;

    .prologue
    .line 2524
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/provider/MusicProvider$MoodUpdateHandler;->updateMoodValueInBackground(Lcom/samsung/musicplus/provider/MusicProvider;)V

    return-void
.end method

.method public static getInstance()Lcom/samsung/musicplus/provider/MusicProvider$MoodUpdateHandler;
    .locals 3

    .prologue
    .line 2533
    sget-object v1, Lcom/samsung/musicplus/provider/MusicProvider$MoodUpdateHandler;->sMoodUpdateHandler:Lcom/samsung/musicplus/provider/MusicProvider$MoodUpdateHandler;

    if-nez v1, :cond_0

    .line 2534
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "MOOD_PARSER"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 2535
    .local v0, "t":Landroid/os/HandlerThread;
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 2536
    new-instance v1, Lcom/samsung/musicplus/provider/MusicProvider$MoodUpdateHandler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/musicplus/provider/MusicProvider$MoodUpdateHandler;-><init>(Landroid/os/Looper;)V

    sput-object v1, Lcom/samsung/musicplus/provider/MusicProvider$MoodUpdateHandler;->sMoodUpdateHandler:Lcom/samsung/musicplus/provider/MusicProvider$MoodUpdateHandler;

    .line 2538
    :cond_0
    sget-object v1, Lcom/samsung/musicplus/provider/MusicProvider$MoodUpdateHandler;->sMoodUpdateHandler:Lcom/samsung/musicplus/provider/MusicProvider$MoodUpdateHandler;

    return-object v1
.end method

.method private updateMoodValueInBackground(Lcom/samsung/musicplus/provider/MusicProvider;)V
    .locals 3
    .param p1, "p"    # Lcom/samsung/musicplus/provider/MusicProvider;

    .prologue
    const/4 v2, 0x0

    .line 2558
    invoke-virtual {p0, v2}, Lcom/samsung/musicplus/provider/MusicProvider$MoodUpdateHandler;->hasMessages(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2559
    invoke-virtual {p0}, Lcom/samsung/musicplus/provider/MusicProvider$MoodUpdateHandler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 2560
    .local v0, "msg":Landroid/os/Message;
    iput-object p1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 2561
    iput v2, v0, Landroid/os/Message;->what:I

    .line 2562
    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/provider/MusicProvider$MoodUpdateHandler;->sendMessage(Landroid/os/Message;)Z

    .line 2564
    .end local v0    # "msg":Landroid/os/Message;
    :cond_0
    return-void
.end method

.method private updateMoodValues(Lcom/samsung/musicplus/provider/MusicProvider;)V
    .locals 20
    .param p1, "p"    # Lcom/samsung/musicplus/provider/MusicProvider;

    .prologue
    .line 2573
    const/4 v11, 0x0

    .line 2574
    .local v11, "data":Landroid/database/Cursor;
    # getter for: Lcom/samsung/musicplus/provider/MusicProvider;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/provider/MusicProvider;->access$300()Ljava/lang/String;

    move-result-object v3

    const-string v4, "MoodUpdateHandler updateMoodValues() is called."

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2576
    const/4 v3, 0x1

    :try_start_0
    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/samsung/musicplus/provider/MusicProvider$MoodUpdateHandler;->mRunningTask:Z

    .line 2577
    new-instance v2, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v2}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 2578
    .local v2, "qb":Landroid/database/sqlite/SQLiteQueryBuilder;
    const-string v3, "audio"

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 2579
    # getter for: Lcom/samsung/musicplus/provider/MusicProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;
    invoke-static/range {p1 .. p1}, Lcom/samsung/musicplus/provider/MusicProvider;->access$400(Lcom/samsung/musicplus/provider/MusicProvider;)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "_id"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "_data"

    aput-object v6, v4, v5

    const-string v5, "is_analyzed IS NOT \'1\'"

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 2582
    if-nez v11, :cond_1

    .line 2584
    # getter for: Lcom/samsung/musicplus/provider/MusicProvider;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/provider/MusicProvider;->access$300()Ljava/lang/String;

    move-result-object v3

    const-string v4, "No music fiels to update for music square.(return data is null)"

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2586
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/samsung/musicplus/provider/MusicProvider$MoodUpdateHandler;->mRunningTask:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2670
    if-eqz v11, :cond_0

    .line 2671
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 2673
    :cond_0
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/samsung/musicplus/provider/MusicProvider$MoodUpdateHandler;->mRunningTask:Z

    .line 2676
    :goto_0
    return-void

    .line 2590
    :cond_1
    :try_start_1
    invoke-interface {v11}, Landroid/database/Cursor;->getCount()I

    move-result v17

    .line 2591
    .local v17, "size":I
    if-gtz v17, :cond_3

    .line 2593
    # getter for: Lcom/samsung/musicplus/provider/MusicProvider;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/provider/MusicProvider;->access$300()Ljava/lang/String;

    move-result-object v3

    const-string v4, "No music fiels to update for music square.(return data\'s size is <= 0)"

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2595
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 2596
    const/4 v11, 0x0

    .line 2597
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/samsung/musicplus/provider/MusicProvider$MoodUpdateHandler;->mRunningTask:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2670
    if-eqz v11, :cond_2

    .line 2671
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 2673
    :cond_2
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/samsung/musicplus/provider/MusicProvider$MoodUpdateHandler;->mRunningTask:Z

    goto :goto_0

    .line 2601
    :cond_3
    :try_start_2
    const-string v3, "_id"

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    .line 2602
    .local v10, "audioId":I
    const-string v3, "_data"

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v16

    .line 2605
    .local v16, "path":I
    new-instance v15, Lcom/samsung/musicplus/library/audio/MoodParser;

    invoke-direct {v15}, Lcom/samsung/musicplus/library/audio/MoodParser;-><init>()V

    .line 2606
    .local v15, "parser":Lcom/samsung/musicplus/library/audio/MoodParser;
    new-instance v19, Ljava/util/ArrayList;

    invoke-direct/range {v19 .. v19}, Ljava/util/ArrayList;-><init>()V

    .line 2607
    .local v19, "values":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentValues;>;"
    :cond_4
    :goto_1
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/musicplus/provider/MusicProvider$MoodUpdateHandler;->mRunningTask:Z

    if-eqz v3, :cond_a

    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_a

    .line 2608
    const/4 v14, 0x0

    .line 2609
    .local v14, "moodValue":[D
    const-string v3, "_data"

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v11, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 2611
    .local v12, "filePath":Ljava/lang/String;
    # getter for: Lcom/samsung/musicplus/provider/MusicProvider;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/provider/MusicProvider;->access$300()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "updateMoodValues filePath : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2613
    invoke-virtual {v12}, Ljava/lang/String;->getBytes()[B

    move-result-object v3

    array-length v3, v3

    const/16 v4, 0x200

    if-gt v3, v4, :cond_5

    .line 2614
    invoke-virtual {v15, v12}, Lcom/samsung/musicplus/library/audio/MoodParser;->getMood(Ljava/lang/String;)[D

    move-result-object v14

    .line 2616
    :cond_5
    if-nez v14, :cond_6

    .line 2618
    const/4 v3, 0x3

    new-array v14, v3, [D

    .end local v14    # "moodValue":[D
    fill-array-data v14, :array_0

    .line 2624
    .restart local v14    # "moodValue":[D
    :cond_6
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_2
    array-length v3, v14

    if-ge v13, v3, :cond_7

    .line 2625
    # getter for: Lcom/samsung/musicplus/provider/MusicProvider;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/provider/MusicProvider;->access$300()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onLoadFinished moodValue["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "] : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    aget-wide v6, v14, v13

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2624
    add-int/lit8 v13, v13, 0x1

    goto :goto_2

    .line 2628
    :cond_7
    new-instance v18, Landroid/content/ContentValues;

    invoke-direct/range {v18 .. v18}, Landroid/content/ContentValues;-><init>()V

    .line 2629
    .local v18, "value":Landroid/content/ContentValues;
    const-string v3, "_id"

    invoke-interface {v11, v10}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 2630
    const-string v3, "mood_cell"

    const/4 v4, -0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2631
    const-string v3, "year_cell"

    const/4 v4, -0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2632
    const-string v3, "_data"

    move/from16 v0, v16

    invoke-interface {v11, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2633
    const/4 v3, 0x0

    aget-wide v4, v14, v3

    const-wide/16 v6, 0x0

    cmpl-double v3, v4, v6

    if-nez v3, :cond_9

    const/4 v3, 0x1

    aget-wide v4, v14, v3

    const-wide/16 v6, 0x0

    cmpl-double v3, v4, v6

    if-nez v3, :cond_9

    const/4 v3, 0x2

    aget-wide v4, v14, v3

    const-wide/16 v6, 0x0

    cmpl-double v3, v4, v6

    if-nez v3, :cond_9

    .line 2637
    const-string v3, "mood_exciting"

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 2638
    const-string v3, "mood_cheerful"

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 2639
    const-string v3, "mood_violent"

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 2640
    const-string v3, "mood_brightness"

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 2648
    :goto_3
    const-string v3, "is_analyzed"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2649
    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2651
    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/16 v4, 0x1f4

    if-le v3, v4, :cond_4

    .line 2652
    # getter for: Lcom/samsung/musicplus/provider/MusicProvider;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/provider/MusicProvider;->access$300()Ljava/lang/String;

    move-result-object v3

    const-string v4, "run bulk update per 500 songs"

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2653
    const/16 v4, 0x44c

    const/4 v3, 0x0

    new-array v3, v3, [Landroid/content/ContentValues;

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Landroid/content/ContentValues;

    move-object/from16 v0, p1

    # invokes: Lcom/samsung/musicplus/provider/MusicProvider;->bulkUpdate(I[Landroid/content/ContentValues;)I
    invoke-static {v0, v4, v3}, Lcom/samsung/musicplus/provider/MusicProvider;->access$900(Lcom/samsung/musicplus/provider/MusicProvider;I[Landroid/content/ContentValues;)I

    .line 2654
    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->clear()V

    .line 2656
    # invokes: Lcom/samsung/musicplus/provider/MusicProvider;->updateMusicSquareGroupInfo()V
    invoke-static/range {p1 .. p1}, Lcom/samsung/musicplus/provider/MusicProvider;->access$700(Lcom/samsung/musicplus/provider/MusicProvider;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_1

    .line 2670
    .end local v2    # "qb":Landroid/database/sqlite/SQLiteQueryBuilder;
    .end local v10    # "audioId":I
    .end local v12    # "filePath":Ljava/lang/String;
    .end local v13    # "i":I
    .end local v14    # "moodValue":[D
    .end local v15    # "parser":Lcom/samsung/musicplus/library/audio/MoodParser;
    .end local v16    # "path":I
    .end local v17    # "size":I
    .end local v18    # "value":Landroid/content/ContentValues;
    .end local v19    # "values":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentValues;>;"
    :catchall_0
    move-exception v3

    if-eqz v11, :cond_8

    .line 2671
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 2673
    :cond_8
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/samsung/musicplus/provider/MusicProvider$MoodUpdateHandler;->mRunningTask:Z

    throw v3

    .line 2642
    .restart local v2    # "qb":Landroid/database/sqlite/SQLiteQueryBuilder;
    .restart local v10    # "audioId":I
    .restart local v12    # "filePath":Ljava/lang/String;
    .restart local v13    # "i":I
    .restart local v14    # "moodValue":[D
    .restart local v15    # "parser":Lcom/samsung/musicplus/library/audio/MoodParser;
    .restart local v16    # "path":I
    .restart local v17    # "size":I
    .restart local v18    # "value":Landroid/content/ContentValues;
    .restart local v19    # "values":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentValues;>;"
    :cond_9
    :try_start_3
    const-string v3, "mood_exciting"

    const/4 v4, 0x0

    aget-wide v4, v14, v4

    double-to-int v4, v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2643
    const-string v3, "mood_cheerful"

    const/4 v4, 0x1

    aget-wide v4, v14, v4

    double-to-int v4, v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2644
    const-string v3, "mood_violent"

    const/4 v4, 0x2

    aget-wide v4, v14, v4

    double-to-int v4, v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2645
    const-string v3, "mood_brightness"

    const/4 v4, 0x1

    aget-wide v4, v14, v4

    double-to-int v4, v4

    const/4 v5, 0x2

    aget-wide v6, v14, v5

    double-to-int v5, v6

    sub-int/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v18

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_3

    .line 2659
    .end local v12    # "filePath":Ljava/lang/String;
    .end local v13    # "i":I
    .end local v14    # "moodValue":[D
    .end local v18    # "value":Landroid/content/ContentValues;
    :cond_a
    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_b

    .line 2660
    # getter for: Lcom/samsung/musicplus/provider/MusicProvider;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/provider/MusicProvider;->access$300()Ljava/lang/String;

    move-result-object v3

    const-string v4, "parsing ended. run bulk update remained songs"

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2661
    const/16 v4, 0x44c

    const/4 v3, 0x0

    new-array v3, v3, [Landroid/content/ContentValues;

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Landroid/content/ContentValues;

    move-object/from16 v0, p1

    # invokes: Lcom/samsung/musicplus/provider/MusicProvider;->bulkUpdate(I[Landroid/content/ContentValues;)I
    invoke-static {v0, v4, v3}, Lcom/samsung/musicplus/provider/MusicProvider;->access$900(Lcom/samsung/musicplus/provider/MusicProvider;I[Landroid/content/ContentValues;)I

    .line 2662
    invoke-virtual/range {v19 .. v19}, Ljava/util/ArrayList;->clear()V

    .line 2666
    :cond_b
    # invokes: Lcom/samsung/musicplus/provider/MusicProvider;->updateMusicSquareGroupInfo()V
    invoke-static/range {p1 .. p1}, Lcom/samsung/musicplus/provider/MusicProvider;->access$700(Lcom/samsung/musicplus/provider/MusicProvider;)V

    .line 2668
    invoke-virtual {v15}, Lcom/samsung/musicplus/library/audio/MoodParser;->release()I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 2670
    if-eqz v11, :cond_c

    .line 2671
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 2673
    :cond_c
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/samsung/musicplus/provider/MusicProvider$MoodUpdateHandler;->mRunningTask:Z

    .line 2675
    # getter for: Lcom/samsung/musicplus/provider/MusicProvider;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/provider/MusicProvider;->access$300()Ljava/lang/String;

    move-result-object v3

    const-string v4, "MoodUpdateHandler updateMoodValues() ended."

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2618
    nop

    :array_0
    .array-data 8
        0x0
        0x0
        0x0
    .end array-data
.end method


# virtual methods
.method protected cancel()V
    .locals 2

    .prologue
    .line 2553
    # getter for: Lcom/samsung/musicplus/provider/MusicProvider;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/provider/MusicProvider;->access$300()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MoodUpdateHandler canceled"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2554
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/provider/MusicProvider$MoodUpdateHandler;->mRunningTask:Z

    .line 2555
    return-void
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 2568
    # getter for: Lcom/samsung/musicplus/provider/MusicProvider;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/provider/MusicProvider;->access$300()Ljava/lang/String;

    move-result-object v0

    const-string v1, "MoodUpdateHandler handleMessage()"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2569
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/samsung/musicplus/provider/MusicProvider;

    check-cast v0, Lcom/samsung/musicplus/provider/MusicProvider;

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/provider/MusicProvider$MoodUpdateHandler;->updateMoodValues(Lcom/samsung/musicplus/provider/MusicProvider;)V

    .line 2570
    return-void
.end method

.method protected isRunningMoodUpdate()Z
    .locals 1

    .prologue
    .line 2549
    iget-boolean v0, p0, Lcom/samsung/musicplus/provider/MusicProvider$MoodUpdateHandler;->mRunningTask:Z

    return v0
.end method
