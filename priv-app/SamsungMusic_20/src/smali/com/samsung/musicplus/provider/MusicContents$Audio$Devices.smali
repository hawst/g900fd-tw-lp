.class public final Lcom/samsung/musicplus/provider/MusicContents$Audio$Devices;
.super Ljava/lang/Object;
.source "MusicContents.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/provider/MusicContents$Audio;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Devices"
.end annotation


# static fields
.field public static final CONTENT_URI:Landroid/net/Uri;

.field public static final DEVICE_ORDER:Ljava/lang/String; = "CASE WHEN (network_mode != \'OFF\') THEN 1 ELSE 2 END, transport_type, network_mode DESC"

.field public static final DEVICE_TYPE:Ljava/lang/String; = "transport_type"

.field private static final SAMSUNG_LINK_API_SUPPORTED_VERSION:I = 0x3e8

.field private static final SAMSUNG_LINK_PACKAGE:Ljava/lang/String; = "com.samsung.android.sdk.samsunglink"

.field public static final SELECTION:Ljava/lang/String; = "transport_type == ? AND transport_type != ? AND physical_type != ?"

.field public static final SELECTION_ARGS:[Ljava/lang/String;

.field public static final _ID:Ljava/lang/String; = "_id"


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 1502
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device;->CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/samsung/musicplus/provider/MusicContents$Audio$Devices;->CONTENT_URI:Landroid/net/Uri;

    .line 1518
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->SLINK:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    invoke-virtual {v2}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->LOCAL:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    invoke-virtual {v2}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;->TV:Lcom/samsung/android/sdk/samsunglink/SlinkDevicePhysicalType;

    invoke-virtual {v2}, Ljava/lang/Enum;->name()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/musicplus/provider/MusicContents$Audio$Devices;->SELECTION_ARGS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1433
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getConnectState(Landroid/content/Context;Landroid/database/Cursor;)Ljava/lang/String;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 1627
    invoke-static {p1}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->getNetworkMode(Landroid/database/Cursor;)Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    move-result-object v1

    .line 1628
    .local v1, "mode":Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;
    sget-object v2, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->WIFI:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    if-ne v2, v1, :cond_1

    .line 1630
    sget-boolean v2, Lcom/samsung/musicplus/util/Features;->FLAG_SUPPORT_CHINA_WLAN:Z

    if-eqz v2, :cond_0

    const v0, 0x7f1001d7

    .line 1631
    .local v0, "messageId":I
    :goto_0
    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1633
    .end local v0    # "messageId":I
    :goto_1
    return-object v2

    .line 1630
    :cond_0
    const v0, 0x7f1001d6

    goto :goto_0

    .line 1633
    :cond_1
    invoke-virtual {v1}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_1
.end method

.method public static getContentsUri(J)Landroid/net/Uri;
    .locals 2
    .param p0, "deviceId"    # J

    .prologue
    .line 1537
    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Audio$Media;->getContentUriForDevice(J)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static getDeviceIcon(Landroid/content/Context;JZ)Landroid/graphics/Bitmap;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "id"    # J
    .param p3, "isOff"    # Z

    .prologue
    .line 1579
    :try_start_0
    sget-object v4, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconSize;->SMALL:Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconSize;

    sget-object v5, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconTheme;->DARK:Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconTheme;

    if-eqz p3, :cond_0

    const/4 v1, 0x0

    new-array v6, v1, [I

    :goto_0
    move-object v1, p0

    move-wide v2, p1

    invoke-static/range {v1 .. v6}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device;->getDeviceIcon(Landroid/content/Context;JLcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconSize;Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device$IconTheme;[I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 1584
    :goto_1
    return-object v1

    .line 1579
    :cond_0
    const/4 v1, 0x1

    new-array v6, v1, [I

    const/4 v1, 0x0

    const v2, 0x101009e

    aput v2, v6, v1
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1583
    :catch_0
    move-exception v0

    .line 1584
    .local v0, "e":Ljava/io/FileNotFoundException;
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public static getLocalDeviceId(Landroid/content/Context;)J
    .locals 10
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1547
    const-wide/16 v8, 0x1

    .line 1548
    .local v8, "deviceId":J
    const/4 v6, 0x0

    .line 1550
    .local v6, "data":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const-string v3, "transport_type = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    sget-object v7, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->LOCAL:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    invoke-virtual {v7}, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->name()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1557
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1559
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v8

    .line 1562
    :cond_0
    if-eqz v6, :cond_1

    .line 1563
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1566
    :cond_1
    return-wide v8

    .line 1562
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 1563
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method public static getName(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1
    .param p0, "c"    # Landroid/database/Cursor;

    .prologue
    .line 1616
    invoke-static {p0}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Device;->getDisplayName(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static hasSamsungAccount(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 1467
    invoke-static {p0}, Lcom/samsung/musicplus/provider/MusicContents$Audio$Devices;->isSupportSlink(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1468
    const-string v2, "MusicDevice"

    const-string v3, "Samsung link patform doesn\'t support, so return here."

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1474
    :goto_0
    return v1

    .line 1472
    :cond_0
    :try_start_0
    invoke-static {p0}, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->samsungAccountExists()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 1473
    :catch_0
    move-exception v0

    .line 1474
    .local v0, "e":Ljava/lang/Exception;
    goto :goto_0
.end method

.method public static isOff(Landroid/database/Cursor;)Z
    .locals 2
    .param p0, "c"    # Landroid/database/Cursor;

    .prologue
    .line 1643
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->OFF:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    invoke-static {p0}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->getNetworkMode(Landroid/database/Cursor;)Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkMode;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static isSingedIn(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 1455
    invoke-static {p0}, Lcom/samsung/musicplus/provider/MusicContents$Audio$Devices;->isSupportSlink(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1456
    const-string v2, "MusicDevice"

    const-string v3, "Samsung link patform doesn\'t support, so return here."

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1462
    :goto_0
    return v1

    .line 1460
    :cond_0
    :try_start_0
    invoke-static {p0}, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->isSignedIn()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    .line 1461
    :catch_0
    move-exception v0

    .line 1462
    .local v0, "e":Ljava/lang/Exception;
    goto :goto_0
.end method

.method public static isSupportSlink(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1440
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 1442
    .local v2, "pm":Landroid/content/pm/PackageManager;
    :try_start_0
    const-string v3, "com.samsung.android.sdk.samsunglink"

    const/16 v4, 0x4000

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 1444
    .local v1, "info":Landroid/content/pm/PackageInfo;
    iget-object v3, v1, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-boolean v3, v3, Landroid/content/pm/ApplicationInfo;->enabled:Z

    if-eqz v3, :cond_0

    iget v3, v1, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    const/16 v4, 0x3e8

    if-lt v3, v4, :cond_0

    .line 1446
    const/4 v3, 0x1

    .line 1451
    .end local v1    # "info":Landroid/content/pm/PackageInfo;
    :goto_0
    return v3

    .line 1448
    :catch_0
    move-exception v0

    .line 1449
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "MusicDevice"

    const-string v4, "Samsung link patform doesn\'t support"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1451
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public static isWebStorage(Landroid/database/Cursor;)Z
    .locals 2
    .param p0, "c"    # Landroid/database/Cursor;

    .prologue
    .line 1653
    invoke-static {p0}, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->getDeviceTransportType(Landroid/database/Cursor;)Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    move-result-object v0

    .line 1654
    .local v0, "type":Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;
    sget-object v1, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->WEB_STORAGE:Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/samsunglink/SlinkDeviceTransportType;->equals(Ljava/lang/Object;)Z

    move-result v1

    return v1
.end method

.method public static refreshDevices(Landroid/content/Context;[J)V
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "ids"    # [J

    .prologue
    .line 1595
    if-nez p1, :cond_1

    .line 1596
    const-string v6, "MusicUi"

    const-string v7, "Device id[] is null, so can\'t refresh devices"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1607
    :cond_0
    :goto_0
    return-void

    .line 1599
    :cond_1
    invoke-static {p0}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;

    move-result-object v5

    .line 1600
    .local v5, "sm":Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;
    if-nez v5, :cond_2

    .line 1601
    const-string v6, "MusicUi"

    const-string v7, "SlinkNetworkManager is null, so can\'t refresh devices"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1604
    :cond_2
    move-object v0, p1

    .local v0, "arr$":[J
    array-length v4, v0

    .local v4, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_1
    if-ge v1, v4, :cond_0

    aget-wide v2, v0, v1

    .line 1605
    .local v2, "id":J
    invoke-virtual {v5, v2, v3}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;->requestRefresh(J)V

    .line 1604
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public static startHowToUseActivity(Landroid/content/Context;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1490
    invoke-static {p0}, Lcom/samsung/android/sdk/samsunglink/SlinkViewerUtils;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkViewerUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sdk/samsunglink/SlinkViewerUtils;->createHowToUseViewIntent()Landroid/content/Intent;

    move-result-object v1

    .line 1492
    .local v1, "i":Landroid/content/Intent;
    :try_start_0
    invoke-virtual {p0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1498
    :goto_0
    return-void

    .line 1493
    :catch_0
    move-exception v0

    .line 1494
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v2, "MusicDevice"

    const-string v3, "Activity Not found!!!"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1495
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    .line 1496
    const-string v2, "Target Activity Not Found"

    const/4 v3, 0x1

    invoke-static {p0, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public static startLoginActivity(Landroid/content/Context;)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1479
    invoke-static {p0}, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/sdk/samsunglink/SlinkSignInUtils;->createSignInActivityIntent()Landroid/content/Intent;

    move-result-object v1

    .line 1481
    .local v1, "i":Landroid/content/Intent;
    :try_start_0
    invoke-virtual {p0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1487
    :goto_0
    return-void

    .line 1482
    :catch_0
    move-exception v0

    .line 1483
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v2, "MusicDevice"

    const-string v3, "Activity Not found!!!"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1484
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    .line 1485
    const-string v2, "Target Activity Not Found"

    const/4 v3, 0x1

    invoke-static {p0, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
