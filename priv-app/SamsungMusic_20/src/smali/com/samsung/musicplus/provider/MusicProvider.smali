.class public Lcom/samsung/musicplus/provider/MusicProvider;
.super Landroid/content/ContentProvider;
.source "MusicProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/provider/MusicProvider$MoodUpdateHandler;,
        Lcom/samsung/musicplus/provider/MusicProvider$MusicDBHelper;
    }
.end annotation


# static fields
.field private static final ALBUM_SEARCH_KEY:Ljava/lang/String;

.field private static final ARTIST_SEARCH_KEY:Ljava/lang/String;

.field private static final AUDIO_ALBUMART:I = 0x460

.field private static final AUDIO_ALBUMART_FILE_ID:I = 0x462

.field private static final AUDIO_ALBUMART_ID:I = 0x461

.field private static final AUDIO_ALBUMS:I = 0x45d

.field private static final AUDIO_ALBUMS_ID:I = 0x45e

.field private static final AUDIO_ARTISTS:I = 0x45b

.field private static final AUDIO_ARTISTS_ID:I = 0x45c

.field private static final AUDIO_ARTISTS_ID_ALBUMS:I = 0x45f

.field public static final AUDIO_MEDIA:I = 0x44c

.field public static final AUDIO_MEDIA_BASE:I = 0x3e8

.field public static final AUDIO_MEDIA_ID:I = 0x44d

.field public static final AUDIO_MEDIA_ID_PLAYLISTS:I = 0x450

.field public static final AUDIO_MEDIA_ID_PLAYLISTS_ID:I = 0x451

.field public static final AUDIO_MEDIA_SYNC_UPDATE:I = 0x473

.field private static final AUDIO_MUSIC_FOLDERS_VIEW:I = 0x470

.field private static final AUDIO_MUSIC_GENRES_VIEW:I = 0x471

.field public static final AUDIO_PLAYLISTS:I = 0x456

.field public static final AUDIO_PLAYLISTS_ID:I = 0x457

.field public static final AUDIO_PLAYLISTS_ID_MEMBERS:I = 0x458

.field private static final AUDIO_PLAYLISTS_ID_MEMBERS_CACHE:I = 0x463

.field public static final AUDIO_PLAYLISTS_ID_MEMBERS_FOR_CLOUD:I = 0x459

.field public static final AUDIO_PLAYLISTS_ID_MEMBERS_ID:I = 0x45a

.field private static final AUDIO_SEARCH_FANCY:I = 0x472

.field private static final AUDIO_TABLE_COLUMNS_DEFINITION:Ljava/lang/String; = "_id INTEGER PRIMARY KEY AUTOINCREMENT,source_id INTEGER NOT NULL,_data TEXT unique on conflict ignore,_display_name TEXT,_size INTEGER,date_added INTEGER,date_modified INTEGER,mime_type TEXT,title TEXT,title_key TEXT,duration INTEGER,composer TEXT,track INTEGER,year INTEGER,bucket_id TEXT,bucket_display_name TEXT,bucket_order INTEGER default 1,artist_id INTEGER,artist_group_id INTEGER,artist TEXT,artist_key TEXT,album_id INTEGER,album_group_id INTEGER,album TEXT,album_key TEXT,album_artist TEXT,genre_name TEXT default \'<unknown>\',year_name TEXT default \'<unknown>\',recently_played INTEGER default 0,most_played INTEGER default 0,recently_added_remove_flag INTEGER default 0,is_favorite INTEGER default 0,is_music INTEGER default 1,is_secretbox INTEGER default 0,sampling_rate INTEGER, bit_depth INTEGER, mood_exciting INTEGER, mood_cheerful INTEGER, mood_violent INTEGER, mood_brightness INTEGER, mood_cell INTEGER, year_cell INTEGER, is_analyzed INTEGER, provider_id TEXT, provider_name TEXT, extension TEXT, seed TEXT, media_type INTEGER"

.field private static final CLASSNAME:Ljava/lang/String;

.field static final DATABASE_VERSION:I = 0x2714

.field private static final DLNA:I = 0x4

.field private static final DLNA_ALBUMART:I = 0xb

.field private static final DLNA_ALBUM_ART:Ljava/lang/String; = "dlna_album_art"

.field private static final DLNA_ALL:I = 0xa

.field private static final DLNA_ALL_TABLE_NAME:Ljava/lang/String; = "dlna_all_table"

.field private static final DLNA_AVPLAYER:I = 0x7

.field private static final DLNA_AVPLAYER_TABLE_COLUMNS_DEFINITION:Ljava/lang/String; = "_id INTEGER PRIMARY KEY, avplayer_id TEXT, avplayer_name TEXT, album_art TEXT, nic_id TEXT, is_seekable_on_paused INTEGER,  is_wha INTEGER"

.field private static final DLNA_AVPLAYER_TABLE_COLUMNS_DEFINITION_V2:Ljava/lang/String; = "_id INTEGER PRIMARY KEY, avplayer_id TEXT, avplayer_name TEXT, album_art TEXT, nic_id TEXT, is_seekable_on_paused INTEGER"

.field private static final DLNA_AVPLAYER_TABLE_NAME:Ljava/lang/String; = "dlna_dmr_table"

.field private static final DLNA_ID:I = 0x5

.field private static final DLNA_MEDIA_TYPE:I = 0x40000

.field private static final DLNA_OPEN_INTENT_CONTENTS:I = 0x8

.field private static final DLNA_OPEN_INTENT_CONTENTS_ID:I = 0x9

.field private static final DLNA_OPEN_INTENT_CONTENTS_TABLE_NAME:Ljava/lang/String; = "dlna_open_intent_table"

.field private static final DLNA_PROVIDER:I = 0x6

.field private static final DLNA_PROVIDER_TABLE_COLUMNS_DEFINITION:Ljava/lang/String; = "_id INTEGER PRIMARY KEY, provider_id TEXT, provider_name TEXT, album_art TEXT, nic_id TEXT"

.field private static final DLNA_PROVIDER_TABLE_NAME:Ljava/lang/String; = "dlna_dms_table"

.field private static final DLNA_TABLE_COLUMNS_DEFINITION:Ljava/lang/String; = "_id INTEGER PRIMARY KEY, provider_id TEXT, provider_name TEXT, artist TEXT, album TEXT, album_id INTEGER, title TEXT, _data TEXT, album_art TEXT, mime_type TEXT, duration INTEGER, file_size LONG, extension TEXT, seed TEXT, genre_name TEXT"

.field private static final DLNA_TABLE_NAME:Ljava/lang/String; = "dlna_dms_contents_table"

.field private static final DUMMY_SOURCE_ID:I = 0x1

.field private static final EXTERNAL_STORAGE_SD_PATH:Ljava/lang/String; = "/storage/extSdCard"

.field private static final MUSIC_SERVICE:Ljava/lang/String; = "com.sec.android.app.music:service"

.field private static final MUSIC_SQUARE_CALM:I = 0x66

.field private static final MUSIC_SQUARE_CALM_GROUP:Ljava/lang/String; = "15,16,17,18,19,20,21,22,23,24"

.field private static final MUSIC_SQUARE_CALM_GROUP_MINI:Ljava/lang/String; = "6,7,8"

.field private static final MUSIC_SQUARE_CELL_UPDATE:Ljava/lang/String; = "mood_update"

.field private static final MUSIC_SQUARE_EXCITING:I = 0x65

.field private static final MUSIC_SQUARE_EXITING_GROUP:Ljava/lang/String; = "0,1,2,3,4,5,6,7,8,9"

.field private static final MUSIC_SQUARE_EXITING_GROUP_MINI:Ljava/lang/String; = "0,1,2"

.field private static final MUSIC_SQUARE_GET_CALM_SONG:Ljava/lang/String; = "mood_calm"

.field private static final MUSIC_SQUARE_GET_EXCITING_SONG:Ljava/lang/String; = "mood_exciting"

.field private static final MUSIC_SQUARE_GET_JOYFUL_SONG:Ljava/lang/String; = "mood_joyful"

.field private static final MUSIC_SQUARE_GET_PASSIONATE_SONG:Ljava/lang/String; = "mood_passionate"

.field private static final MUSIC_SQUARE_JOYFUL:I = 0x68

.field private static final MUSIC_SQUARE_JOYFUL_GROUP:Ljava/lang/String; = "3,4,8,9,13,14,18,19,23,24"

.field private static final MUSIC_SQUARE_JOYFUL_GROUP_MINI:Ljava/lang/String; = "2,5,8"

.field private static final MUSIC_SQUARE_MOOD_CELL_REORDER_QUERY:Ljava/lang/String; = "SELECT _id, mood_exciting, mood_brightness FROM (SELECT _id, mood_exciting, mood_brightness FROM audio_meta WHERE is_analyzed=1 AND mood_exciting IS NOT NULL AND mood_brightness IS NOT NULL ORDER BY mood_exciting DESC LIMIT ? OFFSET ?) ORDER BY mood_brightness,mood_exciting;"

.field private static final MUSIC_SQUARE_PASSIONATE:I = 0x67

.field private static final MUSIC_SQUARE_PASSIONATE_GROUP:Ljava/lang/String; = "0,1,5,6,10,11,15,16,20,21"

.field private static final MUSIC_SQUARE_PASSIONATE_GROUP_MINI:Ljava/lang/String; = "0,3,6"

.field private static final MUSIC_SQUARE_YEAR_CELL_REORDER_QUERY:Ljava/lang/String; = "SELECT _id, mood_exciting, year_name FROM (SELECT _id, mood_exciting, year_name FROM audio_meta WHERE is_analyzed=1 AND mood_exciting IS NOT NULL AND mood_brightness IS NOT NULL ORDER BY mood_exciting DESC LIMIT ? OFFSET ?) ORDER BY year_name,mood_exciting;"

.field private static final MUSIC_SQUAR_CELL:I = 0x3

.field private static final SEARCH_QUERY_FORMAT:Ljava/lang/String;

.field static final SEC_PLAYLIST_COLUMNS_ADDED:Ljava/lang/String; = ",mini_thumb_data"

.field private static final SYNC_TYPE_BASE:I = 0x64

.field private static final SYNC_TYPE_DELETE:I = 0x66

.field private static final SYNC_TYPE_INSERT:I = 0x65

.field private static final SYNC_TYPE_UPDATE:I = 0x67

.field private static final TITLE_PINYIN:Ljava/lang/String; = "title_pinyin"

.field private static final TITLE_SEARCH_KEY:Ljava/lang/String;

.field private static final URI_MATCHER:Landroid/content/UriMatcher;

.field protected static mCollator:Ljava/text/Collator;

.field public static final sAudioMapSyncTable:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field public static final sPlaylistsMapSyncTable:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mBackupFavoriteList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mDB:Landroid/database/sqlite/SQLiteDatabase;

.field private mLocaleChangeReceiver:Landroid/content/BroadcastReceiver;

.field private mMoodUpdateHandler:Lcom/samsung/musicplus/provider/MusicProvider$MoodUpdateHandler;

.field private mShutdownReceiver:Landroid/content/BroadcastReceiver;

.field private mSrcProviderObserver:Landroid/database/ContentObserver;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/16 v4, 0x472

    .line 90
    const-class v0, Lcom/samsung/musicplus/provider/MusicProvider;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/provider/MusicProvider;->CLASSNAME:Ljava/lang/String;

    .line 92
    sget-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_PINYIN:Z

    if-eqz v0, :cond_0

    const-string v0, "||\' \'||artist_search_key"

    :goto_0
    sput-object v0, Lcom/samsung/musicplus/provider/MusicProvider;->ARTIST_SEARCH_KEY:Ljava/lang/String;

    .line 95
    sget-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_PINYIN:Z

    if-eqz v0, :cond_1

    const-string v0, "||\' \'||album_search_key"

    :goto_1
    sput-object v0, Lcom/samsung/musicplus/provider/MusicProvider;->ALBUM_SEARCH_KEY:Ljava/lang/String;

    .line 98
    sget-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_PINYIN:Z

    if-eqz v0, :cond_2

    const-string v0, "||\' \'||title_search_key"

    :goto_2
    sput-object v0, Lcom/samsung/musicplus/provider/MusicProvider;->TITLE_SEARCH_KEY:Ljava/lang/String;

    .line 122
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SELECT artist_id as _id,\'artist\' AS mime_type,artist,media_type,download_type,download_status,download_id,artist_group_id,artist_key,NULL AS album,album_id,NULL AS album_group_id,NULL AS album_key,NULL AS title,artist AS text1,NULL AS text2,count(distinct album) AS data1,count(*) AS data2,artist_key"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/samsung/musicplus/provider/MusicProvider;->ARTIST_SEARCH_KEY:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AS match,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'content://media/external/audio/artists/\'||_id AS suggest_intent_data,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "1 AS grouporder, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "on_my_phone, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "auto_save "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "FROM audio WHERE (artist!=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "<unknown>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\') AND (%1$s) group by artist_group_id "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "UNION ALL "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "SELECT album_id as _id,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'album\' AS mime_type,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "artist,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "media_type,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "download_type,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "download_status,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "download_id,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "NULL AS artist_group_id,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "NULL AS artist_key,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "album,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "NULL AS album_id,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "album_group_id,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "album_key,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "NULL AS title,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "album AS text1,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "artist AS text2,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "NULL AS data1,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "NULL AS data2,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "artist_key||\' \'||album_key"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/samsung/musicplus/provider/MusicProvider;->ARTIST_SEARCH_KEY:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/samsung/musicplus/provider/MusicProvider;->ALBUM_SEARCH_KEY:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AS match,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'content://media/external/audio/albums/\'||_id AS suggest_intent_data,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "2 AS grouporder, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "on_my_phone, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "auto_save "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "FROM audio WHERE (album!=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "<unknown>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\')  AND (%2$s) group by album_group_id "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "UNION ALL "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "SELECT searchhelpertitle._id as _id,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "mime_type,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "artist,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "media_type,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "download_type,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "download_status,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "download_id,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "artist_group_id,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "artist_key,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "album,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "searchhelpertitle.album_id AS album_id,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "album_group_id,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "album_key,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "title,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "title AS text1,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "artist AS text2,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "NULL AS data1,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "NULL AS data2,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "artist_key||\' \'||album_key||\' \'||title_key"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/samsung/musicplus/provider/MusicProvider;->ARTIST_SEARCH_KEY:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/samsung/musicplus/provider/MusicProvider;->ALBUM_SEARCH_KEY:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    sget-object v1, Lcom/samsung/musicplus/provider/MusicProvider;->TITLE_SEARCH_KEY:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AS match,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'content://media/external/audio/media/\'||searchhelpertitle._id AS "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "suggest_intent_data,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "3 AS grouporder, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "on_my_phone, "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "auto_save "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "FROM searchhelpertitle WHERE (title != \'\') AND (%3$s)"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/provider/MusicProvider;->SEARCH_QUERY_FORMAT:Ljava/lang/String;

    .line 401
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    sput-object v0, Lcom/samsung/musicplus/provider/MusicProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    .line 469
    sget-object v0, Lcom/samsung/musicplus/provider/MusicProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.musicplus"

    const-string v2, "audio/media"

    const/16 v3, 0x44c

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 470
    sget-object v0, Lcom/samsung/musicplus/provider/MusicProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.musicplus"

    const-string v2, "audio/media/#"

    const/16 v3, 0x44d

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 471
    sget-object v0, Lcom/samsung/musicplus/provider/MusicProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.musicplus"

    const-string v2, "audio/media/sync_update"

    const/16 v3, 0x473

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 473
    sget-object v0, Lcom/samsung/musicplus/provider/MusicProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.musicplus"

    const-string v2, "audio/media/#/playlists"

    const/16 v3, 0x450

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 475
    sget-object v0, Lcom/samsung/musicplus/provider/MusicProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.musicplus"

    const-string v2, "audio/media/#/playlists/#"

    const/16 v3, 0x451

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 477
    sget-object v0, Lcom/samsung/musicplus/provider/MusicProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.musicplus"

    const-string v2, "audio/playlists"

    const/16 v3, 0x456

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 478
    sget-object v0, Lcom/samsung/musicplus/provider/MusicProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.musicplus"

    const-string v2, "audio/playlists/#"

    const/16 v3, 0x457

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 479
    sget-object v0, Lcom/samsung/musicplus/provider/MusicProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.musicplus"

    const-string v2, "audio/playlists/#/members"

    const/16 v3, 0x458

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 481
    sget-object v0, Lcom/samsung/musicplus/provider/MusicProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.musicplus"

    const-string v2, "audio/playlists/members/cache"

    const/16 v3, 0x463

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 485
    sget-object v0, Lcom/samsung/musicplus/provider/MusicProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.musicplus"

    const-string v2, "audio/playlists/#/members_cloud"

    const/16 v3, 0x459

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 487
    sget-object v0, Lcom/samsung/musicplus/provider/MusicProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.musicplus"

    const-string v2, "audio/playlists/#/members/#"

    const/16 v3, 0x45a

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 489
    sget-object v0, Lcom/samsung/musicplus/provider/MusicProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.musicplus"

    const-string v2, "audio/artists"

    const/16 v3, 0x45b

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 490
    sget-object v0, Lcom/samsung/musicplus/provider/MusicProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.musicplus"

    const-string v2, "audio/artists/#"

    const/16 v3, 0x45c

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 491
    sget-object v0, Lcom/samsung/musicplus/provider/MusicProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.musicplus"

    const-string v2, "audio/artists/#/albums"

    const/16 v3, 0x45f

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 493
    sget-object v0, Lcom/samsung/musicplus/provider/MusicProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.musicplus"

    const-string v2, "audio/albums"

    const/16 v3, 0x45d

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 494
    sget-object v0, Lcom/samsung/musicplus/provider/MusicProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.musicplus"

    const-string v2, "audio/albums/#"

    const/16 v3, 0x45e

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 495
    sget-object v0, Lcom/samsung/musicplus/provider/MusicProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.musicplus"

    const-string v2, "audio/albumart"

    const/16 v3, 0x460

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 496
    sget-object v0, Lcom/samsung/musicplus/provider/MusicProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.musicplus"

    const-string v2, "audio/albumart/#"

    const/16 v3, 0x461

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 497
    sget-object v0, Lcom/samsung/musicplus/provider/MusicProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.musicplus"

    const-string v2, "audio/media/#/albumart"

    const/16 v3, 0x462

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 500
    sget-object v0, Lcom/samsung/musicplus/provider/MusicProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.musicplus"

    const-string v2, "audio/music_folders"

    const/16 v3, 0x470

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 503
    sget-object v0, Lcom/samsung/musicplus/provider/MusicProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.musicplus"

    const-string v2, "audio/music_genres"

    const/16 v3, 0x471

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 506
    sget-object v0, Lcom/samsung/musicplus/provider/MusicProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.musicplus"

    const-string v2, "audio/search/fancy"

    invoke-virtual {v0, v1, v2, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 507
    sget-object v0, Lcom/samsung/musicplus/provider/MusicProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.musicplus"

    const-string v2, "audio/search/fancy/*"

    invoke-virtual {v0, v1, v2, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 509
    sget-object v0, Lcom/samsung/musicplus/provider/MusicProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.musicplus"

    const-string v2, "mood_update"

    const/4 v3, 0x3

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 510
    sget-object v0, Lcom/samsung/musicplus/provider/MusicProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.musicplus"

    const-string v2, "mood_exciting"

    const/16 v3, 0x65

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 512
    sget-object v0, Lcom/samsung/musicplus/provider/MusicProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.musicplus"

    const-string v2, "mood_calm"

    const/16 v3, 0x66

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 513
    sget-object v0, Lcom/samsung/musicplus/provider/MusicProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.musicplus"

    const-string v2, "mood_passionate"

    const/16 v3, 0x67

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 515
    sget-object v0, Lcom/samsung/musicplus/provider/MusicProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.musicplus"

    const-string v2, "mood_joyful"

    const/16 v3, 0x68

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 518
    sget-object v0, Lcom/samsung/musicplus/provider/MusicProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.musicplus"

    const-string v2, "dlna_dms_contents_table"

    const/4 v3, 0x4

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 519
    sget-object v0, Lcom/samsung/musicplus/provider/MusicProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.musicplus"

    const-string v2, "dlna_dms_contents_table/#"

    const/4 v3, 0x5

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 520
    sget-object v0, Lcom/samsung/musicplus/provider/MusicProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.musicplus"

    const-string v2, "dlna_album_art"

    const/16 v3, 0xb

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 521
    sget-object v0, Lcom/samsung/musicplus/provider/MusicProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.musicplus"

    const-string v2, "dlna_dms_table"

    const/4 v3, 0x6

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 522
    sget-object v0, Lcom/samsung/musicplus/provider/MusicProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.musicplus"

    const-string v2, "dlna_dmr_table"

    const/4 v3, 0x7

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 523
    sget-object v0, Lcom/samsung/musicplus/provider/MusicProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.musicplus"

    const-string v2, "dlna_open_intent_table"

    const/16 v3, 0x8

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 525
    sget-object v0, Lcom/samsung/musicplus/provider/MusicProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.musicplus"

    const-string v2, "dlna_open_intent_table/#"

    const/16 v3, 0x9

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 527
    sget-object v0, Lcom/samsung/musicplus/provider/MusicProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.musicplus"

    const-string v2, "dlna_all_table"

    const/16 v3, 0xa

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 2513
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/musicplus/provider/MusicProvider;->sPlaylistsMapSyncTable:Ljava/util/HashMap;

    .line 2515
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/musicplus/provider/MusicProvider;->sAudioMapSyncTable:Ljava/util/HashMap;

    return-void

    .line 92
    :cond_0
    const-string v0, ""

    goto/16 :goto_0

    .line 95
    :cond_1
    const-string v0, ""

    goto/16 :goto_1

    .line 98
    :cond_2
    const-string v0, ""

    goto/16 :goto_2
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 465
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 1073
    new-instance v0, Lcom/samsung/musicplus/provider/MusicProvider$1;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/samsung/musicplus/provider/MusicProvider$1;-><init>(Lcom/samsung/musicplus/provider/MusicProvider;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/samsung/musicplus/provider/MusicProvider;->mSrcProviderObserver:Landroid/database/ContentObserver;

    .line 1149
    new-instance v0, Lcom/samsung/musicplus/provider/MusicProvider$2;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/provider/MusicProvider$2;-><init>(Lcom/samsung/musicplus/provider/MusicProvider;)V

    iput-object v0, p0, Lcom/samsung/musicplus/provider/MusicProvider;->mLocaleChangeReceiver:Landroid/content/BroadcastReceiver;

    .line 1165
    new-instance v0, Lcom/samsung/musicplus/provider/MusicProvider$3;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/provider/MusicProvider$3;-><init>(Lcom/samsung/musicplus/provider/MusicProvider;)V

    iput-object v0, p0, Lcom/samsung/musicplus/provider/MusicProvider;->mShutdownReceiver:Landroid/content/BroadcastReceiver;

    .line 2406
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/provider/MusicProvider;->mBackupFavoriteList:Ljava/util/List;

    .line 466
    return-void
.end method

.method static synthetic access$000(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 0
    .param p0, "x0"    # Landroid/content/Context;
    .param p1, "x1"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "x2"    # I
    .param p3, "x3"    # I

    .prologue
    .line 88
    invoke-static {p0, p1, p2, p3}, Lcom/samsung/musicplus/provider/MusicProvider;->updateDatabase(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;II)V

    return-void
.end method

.method static synthetic access$100(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 0
    .param p0, "x0"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 88
    invoke-static {p0, p1, p2}, Lcom/samsung/musicplus/provider/MusicProvider;->updateAdditionalDatabase(Landroid/database/sqlite/SQLiteDatabase;II)V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/musicplus/provider/MusicProvider;Landroid/content/Context;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/provider/MusicProvider;
    .param p1, "x1"    # Landroid/content/Context;

    .prologue
    .line 88
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/provider/MusicProvider;->isActiveMusicService(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$300()Ljava/lang/String;
    .locals 1

    .prologue
    .line 88
    sget-object v0, Lcom/samsung/musicplus/provider/MusicProvider;->CLASSNAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/musicplus/provider/MusicProvider;)Landroid/database/sqlite/SQLiteDatabase;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/provider/MusicProvider;

    .prologue
    .line 88
    iget-object v0, p0, Lcom/samsung/musicplus/provider/MusicProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/musicplus/provider/MusicProvider;)Landroid/database/ContentObserver;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/provider/MusicProvider;

    .prologue
    .line 88
    iget-object v0, p0, Lcom/samsung/musicplus/provider/MusicProvider;->mSrcProviderObserver:Landroid/database/ContentObserver;

    return-object v0
.end method

.method static synthetic access$600(Lcom/samsung/musicplus/provider/MusicProvider;)Lcom/samsung/musicplus/provider/MusicProvider$MoodUpdateHandler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/provider/MusicProvider;

    .prologue
    .line 88
    iget-object v0, p0, Lcom/samsung/musicplus/provider/MusicProvider;->mMoodUpdateHandler:Lcom/samsung/musicplus/provider/MusicProvider$MoodUpdateHandler;

    return-object v0
.end method

.method static synthetic access$700(Lcom/samsung/musicplus/provider/MusicProvider;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/provider/MusicProvider;

    .prologue
    .line 88
    invoke-direct {p0}, Lcom/samsung/musicplus/provider/MusicProvider;->updateMusicSquareGroupInfo()V

    return-void
.end method

.method static synthetic access$900(Lcom/samsung/musicplus/provider/MusicProvider;I[Landroid/content/ContentValues;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/provider/MusicProvider;
    .param p1, "x1"    # I
    .param p2, "x2"    # [Landroid/content/ContentValues;

    .prologue
    .line 88
    invoke-direct {p0, p1, p2}, Lcom/samsung/musicplus/provider/MusicProvider;->bulkUpdate(I[Landroid/content/ContentValues;)I

    move-result v0

    return v0
.end method

.method private bulkInsertInternal(Landroid/net/Uri;[Landroid/content/ContentValues;I)I
    .locals 5
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # [Landroid/content/ContentValues;
    .param p3, "match"    # I

    .prologue
    .line 1542
    iget-object v3, p0, Lcom/samsung/musicplus/provider/MusicProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 1543
    const/4 v2, 0x0

    .line 1545
    .local v2, "numInserted":I
    :try_start_0
    array-length v1, p2

    .line 1546
    .local v1, "len":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 1547
    aget-object v3, p2, v0

    if-eqz v3, :cond_0

    .line 1548
    aget-object v3, p2, v0

    invoke-direct {p0, p1, p3, v3}, Lcom/samsung/musicplus/provider/MusicProvider;->insertInternal(Landroid/net/Uri;ILandroid/content/ContentValues;)Landroid/net/Uri;

    .line 1546
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1552
    :cond_1
    move v2, v1

    .line 1553
    iget-object v3, p0, Lcom/samsung/musicplus/provider/MusicProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1555
    iget-object v3, p0, Lcom/samsung/musicplus/provider/MusicProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1558
    invoke-direct {p0, p1, p3}, Lcom/samsung/musicplus/provider/MusicProvider;->notifyMultipleChanges(Landroid/net/Uri;I)V

    .line 1559
    return v2

    .line 1555
    .end local v0    # "i":I
    .end local v1    # "len":I
    :catchall_0
    move-exception v3

    iget-object v4, p0, Lcom/samsung/musicplus/provider/MusicProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v3
.end method

.method private bulkUpdate(I[Landroid/content/ContentValues;)I
    .locals 6
    .param p1, "match"    # I
    .param p2, "values"    # [Landroid/content/ContentValues;

    .prologue
    .line 2681
    sget-object v4, Lcom/samsung/musicplus/provider/MusicProvider;->CLASSNAME:Ljava/lang/String;

    const-string v5, "bulkUpdate() is called."

    invoke-static {v4, v5}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2683
    iget-object v4, p0, Lcom/samsung/musicplus/provider/MusicProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 2684
    const/4 v3, 0x0

    .line 2686
    .local v3, "numInserted":I
    :try_start_0
    array-length v2, p2

    .line 2687
    .local v2, "len":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 2688
    aget-object v4, p2, v1

    if-eqz v4, :cond_0

    .line 2689
    aget-object v4, p2, v1

    invoke-direct {p0, p1, v4}, Lcom/samsung/musicplus/provider/MusicProvider;->updateInternal(ILandroid/content/ContentValues;)I

    .line 2687
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2692
    :cond_1
    move v3, v2

    .line 2693
    iget-object v4, p0, Lcom/samsung/musicplus/provider/MusicProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2700
    iget-object v4, p0, Lcom/samsung/musicplus/provider/MusicProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 2702
    .end local v1    # "i":I
    .end local v2    # "len":I
    :goto_1
    return v3

    .line 2694
    :catch_0
    move-exception v0

    .line 2698
    .local v0, "e":Landroid/database/sqlite/SQLiteException;
    const/4 v3, 0x0

    .line 2700
    iget-object v4, p0, Lcom/samsung/musicplus/provider/MusicProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v4}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto :goto_1

    .end local v0    # "e":Landroid/database/sqlite/SQLiteException;
    :catchall_0
    move-exception v4

    iget-object v5, p0, Lcom/samsung/musicplus/provider/MusicProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v4
.end method

.method public static cleanHashTable()V
    .locals 1

    .prologue
    .line 2504
    sget-object v0, Lcom/samsung/musicplus/provider/MusicProvider;->sPlaylistsMapSyncTable:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 2505
    sget-object v0, Lcom/samsung/musicplus/provider/MusicProvider;->sAudioMapSyncTable:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 2506
    return-void
.end method

.method private combine(Ljava/util/List;[Ljava/lang/String;)[Ljava/lang/String;
    .locals 6
    .param p2, "userArgs"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;[",
            "Ljava/lang/String;",
            ")[",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .line 1468
    .local p1, "prepend":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v2

    .line 1469
    .local v2, "presize":I
    if-nez v2, :cond_0

    .line 1481
    .end local p2    # "userArgs":[Ljava/lang/String;
    :goto_0
    return-object p2

    .line 1473
    .restart local p2    # "userArgs":[Ljava/lang/String;
    :cond_0
    if-eqz p2, :cond_1

    array-length v3, p2

    .line 1474
    .local v3, "usersize":I
    :goto_1
    add-int v4, v2, v3

    new-array v0, v4, [Ljava/lang/String;

    .line 1475
    .local v0, "combined":[Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_2
    if-ge v1, v2, :cond_2

    .line 1476
    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    aput-object v4, v0, v1

    .line 1475
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1473
    .end local v0    # "combined":[Ljava/lang/String;
    .end local v1    # "i":I
    .end local v3    # "usersize":I
    :cond_1
    const/4 v3, 0x0

    goto :goto_1

    .line 1478
    .restart local v0    # "combined":[Ljava/lang/String;
    .restart local v1    # "i":I
    .restart local v3    # "usersize":I
    :cond_2
    const/4 v1, 0x0

    :goto_3
    if-ge v1, v3, :cond_3

    .line 1479
    add-int v4, v2, v1

    aget-object v5, p2, v1

    aput-object v5, v0, v4

    .line 1478
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_3
    move-object p2, v0

    .line 1481
    goto :goto_0
.end method

.method private static computeChineseSortKey(Ljava/lang/String;)Ljava/lang/String;
    .locals 15
    .param p0, "displayName"    # Ljava/lang/String;

    .prologue
    .line 2795
    const-string v13, "<unknown>"

    invoke-virtual {p0, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_1

    .line 2796
    const-string v10, "\u0001"

    .line 2858
    :cond_0
    :goto_0
    return-object v10

    .line 2798
    :cond_1
    const-string v13, "[\\[\\]\\(\\)\"\'.,?!]"

    const-string v14, ""

    invoke-virtual {p0, v13, v14}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object p0

    .line 2799
    invoke-static {}, Lcom/samsung/musicplus/util/HanziToPinyin;->getInstance()Lcom/samsung/musicplus/util/HanziToPinyin;

    move-result-object v13

    invoke-virtual {v13, p0}, Lcom/samsung/musicplus/util/HanziToPinyin;->get(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v12

    .line 2800
    .local v12, "tokens":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/musicplus/util/HanziToPinyin$Token;>;"
    const-string v6, ""

    .line 2802
    .local v6, "result":Ljava/lang/String;
    if-eqz v12, :cond_10

    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v13

    if-lez v13, :cond_10

    .line 2803
    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v5

    .line 2804
    .local v5, "n":I
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 2805
    .local v3, "keyBuilder":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v5, :cond_5

    .line 2806
    invoke-virtual {v12, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/samsung/musicplus/util/HanziToPinyin$Token;

    .line 2807
    .local v11, "token":Lcom/samsung/musicplus/util/HanziToPinyin$Token;
    iget v13, v11, Lcom/samsung/musicplus/util/HanziToPinyin$Token;->type:I

    const/4 v14, 0x2

    if-ne v13, v14, :cond_3

    .line 2808
    iget-object v13, v11, Lcom/samsung/musicplus/util/HanziToPinyin$Token;->source:Ljava/lang/String;

    invoke-virtual {v13}, Ljava/lang/String;->toCharArray()[C

    move-result-object v7

    .line 2809
    .local v7, "sources":[C
    iget-object v13, v11, Lcom/samsung/musicplus/util/HanziToPinyin$Token;->target:Ljava/lang/String;

    const-string v14, " "

    invoke-virtual {v13, v14}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    .line 2810
    .local v9, "targets":[Ljava/lang/String;
    array-length v13, v7

    array-length v14, v9

    if-ge v13, v14, :cond_2

    array-length v4, v7

    .line 2812
    .local v4, "m":I
    :goto_2
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_3
    if-ge v2, v4, :cond_4

    .line 2813
    aget-object v13, v9, v2

    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2812
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 2810
    .end local v2    # "j":I
    .end local v4    # "m":I
    :cond_2
    array-length v4, v9

    goto :goto_2

    .line 2819
    .end local v7    # "sources":[C
    .end local v9    # "targets":[Ljava/lang/String;
    :cond_3
    iget-object v13, v11, Lcom/samsung/musicplus/util/HanziToPinyin$Token;->target:Ljava/lang/String;

    const-string v14, " "

    invoke-virtual {v13, v14}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    .line 2820
    .restart local v9    # "targets":[Ljava/lang/String;
    const/4 v2, 0x0

    .restart local v2    # "j":I
    :goto_4
    array-length v13, v9

    if-ge v2, v13, :cond_4

    .line 2821
    aget-object v13, v9, v2

    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2822
    const/16 v13, 0x20

    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 2820
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 2805
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2827
    .end local v2    # "j":I
    .end local v9    # "targets":[Ljava/lang/String;
    .end local v11    # "token":Lcom/samsung/musicplus/util/HanziToPinyin$Token;
    :cond_5
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v10

    .line 2828
    .local v10, "tmp":Ljava/lang/String;
    const/4 v13, 0x0

    const/4 v14, 0x1

    invoke-virtual {v3, v13, v14}, Ljava/lang/StringBuilder;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 2829
    .local v0, "firstLetter":Ljava/lang/String;
    invoke-static {}, Ljava/text/Collator;->getInstance()Ljava/text/Collator;

    move-result-object v13

    sput-object v13, Lcom/samsung/musicplus/provider/MusicProvider;->mCollator:Ljava/text/Collator;

    .line 2830
    invoke-static {v0}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 2833
    .local v8, "symbol":Ljava/lang/String;
    sget-object v13, Lcom/samsung/musicplus/provider/MusicProvider;->mCollator:Ljava/text/Collator;

    const-string v14, "#"

    invoke-virtual {v13, v0, v14}, Ljava/text/Collator;->compare(Ljava/lang/String;Ljava/lang/String;)I

    move-result v13

    const/4 v14, -0x1

    if-ne v13, v14, :cond_6

    .line 2834
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "#"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_0

    .line 2835
    :cond_6
    const-string v13, "%7C"

    invoke-virtual {v8, v13}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v13

    if-nez v13, :cond_7

    .line 2836
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "#"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_0

    .line 2837
    :cond_7
    const-string v13, "%5B"

    invoke-virtual {v8, v13}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v13

    if-nez v13, :cond_8

    .line 2838
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "#"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_0

    .line 2839
    :cond_8
    const-string v13, "~"

    invoke-virtual {v8, v13}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v13

    if-nez v13, :cond_9

    .line 2840
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "#"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_0

    .line 2841
    :cond_9
    const-string v13, "_"

    invoke-virtual {v8, v13}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v13

    if-nez v13, :cond_a

    .line 2842
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "#"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_0

    .line 2843
    :cond_a
    const-string v13, "%C2%A2"

    invoke-virtual {v8, v13}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v13

    if-nez v13, :cond_b

    .line 2844
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "#"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_0

    .line 2845
    :cond_b
    const-string v13, "%C2%A3"

    invoke-virtual {v8, v13}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v13

    if-nez v13, :cond_c

    .line 2846
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "#"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_0

    .line 2847
    :cond_c
    const-string v13, "%C2%A5"

    invoke-virtual {v8, v13}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v13

    if-nez v13, :cond_d

    .line 2848
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "#"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_0

    .line 2849
    :cond_d
    const-string v13, "%C3%97"

    invoke-virtual {v8, v13}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v13

    if-nez v13, :cond_e

    .line 2850
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "#"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_0

    .line 2851
    :cond_e
    const-string v13, "%C3%B7"

    invoke-virtual {v8, v13}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v13

    if-nez v13, :cond_f

    .line 2852
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "#"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_0

    .line 2853
    :cond_f
    const-string v13, "%E2%82%AC"

    invoke-virtual {v8, v13}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v13

    if-nez v13, :cond_0

    .line 2854
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "#"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_0

    .end local v0    # "firstLetter":Ljava/lang/String;
    .end local v1    # "i":I
    .end local v3    # "keyBuilder":Ljava/lang/StringBuilder;
    .end local v5    # "n":I
    .end local v8    # "symbol":Ljava/lang/String;
    .end local v10    # "tmp":Ljava/lang/String;
    :cond_10
    move-object v10, v6

    .line 2858
    goto/16 :goto_0
.end method

.method private static createAlbumArtTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 809
    const-string v0, "DROP TABLE IF EXISTS album_art"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 810
    const-string v0, "CREATE TABLE IF NOT EXISTS album_art (album_id INTEGER PRIMARY KEY,album_art TEXT);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 814
    const-string v0, "DROP TABLE IF EXISTS dlna_album_art"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 815
    const-string v0, "CREATE TABLE IF NOT EXISTS dlna_album_art (album_id INTEGER PRIMARY KEY,album_art TEXT);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 818
    return-void
.end method

.method private static createAudioTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 821
    sget-object v0, Lcom/samsung/musicplus/provider/MusicProvider;->CLASSNAME:Ljava/lang/String;

    const-string v1, "createAudioTable"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 823
    const-string v0, "DROP TABLE IF EXISTS audio_meta"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 827
    const-string v0, "CREATE TABLE IF NOT EXISTS audio_meta (_id INTEGER PRIMARY KEY AUTOINCREMENT,source_id INTEGER NOT NULL,_data TEXT unique on conflict ignore,_display_name TEXT,_size INTEGER,date_added INTEGER,date_modified INTEGER,mime_type TEXT,title TEXT,title_key TEXT,duration INTEGER,composer TEXT,track INTEGER,year INTEGER,bucket_id TEXT,bucket_display_name TEXT,bucket_order INTEGER default 1,artist_id INTEGER,artist_group_id INTEGER,artist TEXT,artist_key TEXT,album_id INTEGER,album_group_id INTEGER,album TEXT,album_key TEXT,album_artist TEXT,genre_name TEXT default \'<unknown>\',year_name TEXT default \'<unknown>\',recently_played INTEGER default 0,most_played INTEGER default 0,recently_added_remove_flag INTEGER default 0,is_favorite INTEGER default 0,is_music INTEGER default 1,is_secretbox INTEGER default 0,sampling_rate INTEGER, bit_depth INTEGER, mood_exciting INTEGER, mood_cheerful INTEGER, mood_violent INTEGER, mood_brightness INTEGER, mood_cell INTEGER, year_cell INTEGER, is_analyzed INTEGER, provider_id TEXT, provider_name TEXT, extension TEXT, seed TEXT, media_type INTEGER);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 829
    sget-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_PINYIN:Z

    if-eqz v0, :cond_0

    .line 830
    const-string v0, "ALTER TABLE audio_meta ADD COLUMN title_pinyin TEXT;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 831
    const-string v0, "ALTER TABLE audio_meta ADD COLUMN artist_pinyin TEXT default \'<unknown>\';"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 832
    const-string v0, "ALTER TABLE audio_meta ADD COLUMN album_pinyin TEXT default \'<unknown>\';"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 833
    const-string v0, "ALTER TABLE audio_meta ADD COLUMN genre_name_pinyin TEXT default \'<unknown>\';"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 834
    const-string v0, "ALTER TABLE audio_meta ADD COLUMN composer_pinyin TEXT default \'<unknown>\';"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 835
    const-string v0, "ALTER TABLE audio_meta ADD COLUMN _display_name_pinyin TEXT;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 836
    const-string v0, "ALTER TABLE audio_meta ADD COLUMN artist_search_key TEXT;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 837
    const-string v0, "ALTER TABLE audio_meta ADD COLUMN album_search_key TEXT;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 838
    const-string v0, "ALTER TABLE audio_meta ADD COLUMN title_search_key TEXT;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 841
    :cond_0
    const-string v0, "CREATE INDEX albumkey_index on audio_meta(album_key);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 842
    const-string v0, "CREATE INDEX album_idx on audio_meta(album);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 843
    const-string v0, "CREATE INDEX album_id_idx ON audio_meta(album_id);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 844
    const-string v0, "CREATE INDEX artistkey_index on audio_meta(artist_key);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 845
    const-string v0, "CREATE INDEX artist_idx on audio_meta(artist);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 846
    const-string v0, "CREATE INDEX artist_id_idx ON audio_meta(artist_id);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 847
    const-string v0, "CREATE INDEX path_index ON audio_meta(_data);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 848
    const-string v0, "CREATE INDEX title_idx ON audio_meta(title);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 849
    const-string v0, "CREATE INDEX titlekey_index ON audio_meta(title_key);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 850
    const-string v0, "CREATE INDEX bucket_index on audio_meta(bucket_id);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 851
    const-string v0, "CREATE INDEX bucket_name on audio_meta(bucket_id, bucket_display_name);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 854
    const-string v0, "CREATE TABLE IF NOT EXISTS audio_playlists (_id INTEGER PRIMARY KEY,source_id TEXT,cloud_id TEXT, _data TEXT default \'\',name TEXT NOT NULL,date_added INTEGER,date_modified INTEGER,mini_thumb_data, UNIQUE(_data,name) ON CONFLICT IGNORE);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 871
    const-string v0, "CREATE TABLE IF NOT EXISTS audio_playlists_map (_id INTEGER PRIMARY KEY,audio_id INTEGER NOT NULL,playlist_id INTEGER NOT NULL,play_order INTEGER NOT NULL);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 876
    const-string v0, "CREATE TRIGGER IF NOT EXISTS audio_cleanup DELETE ON audio_meta BEGIN DELETE FROM audio_playlists_map WHERE audio_id = old._id;END"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 880
    const-string v0, "CREATE TRIGGER IF NOT EXISTS audio_playlist_map_cleanup DELETE ON audio_playlists  BEGIN  DELETE FROM audio_playlists_map WHERE playlist_id = old._id;END"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 883
    return-void
.end method

.method private static createAudioWithAlbumArtView(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 916
    const-string v0, "DROP VIEW IF EXISTS audio_with_albumart"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 917
    const-string v0, "DROP VIEW IF EXISTS audio"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 919
    const-string v0, "CREATE VIEW IF NOT EXISTS audio AS SELECT * , album_art.album_art AS album_art FROM audio_meta LEFT OUTER JOIN album_art ON audio_meta.album_id=album_art.album_id;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 924
    return-void
.end method

.method private static createPlaylistCacheTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 987
    const-string v0, "CREATE TABLE IF NOT EXISTS audio_playlists_map_cache (_id INTEGER PRIMARY KEY,audio_id INTEGER NOT NULL,playlist_id INTEGER NOT NULL,play_order INTEGER NOT NULL,_data TEXT);"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 991
    return-void
.end method

.method private static createSearchTable(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 927
    const-string v0, "CREATE VIEW IF NOT EXISTS searchhelpertitle AS SELECT * FROM audio ORDER BY title_key;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 977
    return-void
.end method

.method private dlnaContentsBulkInsert([Landroid/content/ContentValues;)I
    .locals 13
    .param p1, "values"    # [Landroid/content/ContentValues;

    .prologue
    .line 1563
    array-length v12, p1

    .line 1564
    .local v12, "length":I
    const/4 v11, 0x0

    .line 1565
    .local v11, "lastAlbumId":I
    const/4 v9, 0x0

    .line 1568
    .local v9, "c":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/samsung/musicplus/provider/MusicProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "dlna_album_art"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "album_id"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 1571
    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eqz v0, :cond_0

    .line 1572
    invoke-interface {v9}, Landroid/database/Cursor;->moveToLast()Z

    .line 1573
    const/4 v0, 0x0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v11

    .line 1576
    :cond_0
    if-eqz v9, :cond_1

    .line 1577
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 1581
    :cond_1
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_0
    if-ge v10, v12, :cond_4

    .line 1582
    add-int/lit8 v11, v11, 0x1

    .line 1584
    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    .line 1585
    .local v8, "albumArtValue":Landroid/content/ContentValues;
    const-string v0, "album_id"

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1587
    const-string v0, "album_art"

    aget-object v1, p1, v10

    const-string v2, "album_art"

    invoke-virtual {v1, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1589
    iget-object v0, p0, Lcom/samsung/musicplus/provider/MusicProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "dlna_album_art"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, v8}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 1593
    aget-object v0, p1, v10

    const-string v1, "media_type"

    const/high16 v2, 0x40000

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1595
    aget-object v0, p1, v10

    const-string v1, "_size"

    aget-object v2, p1, v10

    const-string v3, "file_size"

    invoke-virtual {v2, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1598
    aget-object v0, p1, v10

    const-string v1, "source_id"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1599
    aget-object v0, p1, v10

    const-string v1, "album_id"

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1601
    aget-object v0, p1, v10

    const-string v1, "album_art"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 1602
    aget-object v0, p1, v10

    const-string v1, "file_size"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 1604
    sget-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_PINYIN:Z

    if-eqz v0, :cond_2

    .line 1605
    aget-object v0, p1, v10

    const-string v1, "title"

    const-string v2, "title_pinyin"

    invoke-static {v0, v1, v2}, Lcom/samsung/musicplus/provider/MusicProvider;->subConvertOrgin2PinyinForSort(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 1609
    :cond_2
    iget-object v0, p0, Lcom/samsung/musicplus/provider/MusicProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "audio_meta"

    const/4 v2, 0x0

    aget-object v3, p1, v10

    invoke-virtual {v0, v1, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 1581
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_0

    .line 1576
    .end local v8    # "albumArtValue":Landroid/content/ContentValues;
    .end local v10    # "i":I
    :catchall_0
    move-exception v0

    if-eqz v9, :cond_3

    .line 1577
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 1612
    .restart local v10    # "i":I
    :cond_4
    return v12
.end method

.method private doAudioSearch(Landroid/database/sqlite/SQLiteDatabase;Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 16
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "qb"    # Landroid/database/sqlite/SQLiteQueryBuilder;
    .param p3, "uri"    # Landroid/net/Uri;
    .param p4, "projectionIn"    # [Ljava/lang/String;
    .param p5, "selection"    # Ljava/lang/String;
    .param p6, "selectionArgs"    # [Ljava/lang/String;
    .param p7, "sort"    # Ljava/lang/String;
    .param p8, "limit"    # Ljava/lang/String;

    .prologue
    .line 2264
    invoke-virtual/range {p3 .. p3}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v13

    const-string v14, "/"

    invoke-virtual {v13, v14}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v13

    if-eqz v13, :cond_2

    const-string v8, ""

    .line 2265
    .local v8, "search":Ljava/lang/String;
    :goto_0
    const-string v13, "  "

    const-string v14, " "

    invoke-virtual {v8, v13, v14}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v8

    .line 2267
    invoke-virtual {v8}, Ljava/lang/String;->length()I

    move-result v13

    if-lez v13, :cond_3

    const-string v13, " "

    invoke-virtual {v8, v13}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    .line 2268
    .local v9, "searchWords":[Ljava/lang/String;
    :goto_1
    array-length v13, v9

    new-array v12, v13, [Ljava/lang/String;

    .line 2269
    .local v12, "wildcardWords":[Ljava/lang/String;
    invoke-static {}, Ljava/text/Collator;->getInstance()Ljava/text/Collator;

    move-result-object v2

    .line 2270
    .local v2, "col":Ljava/text/Collator;
    const/4 v13, 0x0

    invoke-virtual {v2, v13}, Ljava/text/Collator;->setStrength(I)V

    .line 2271
    array-length v6, v9

    .line 2272
    .local v6, "len":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_2
    if-ge v4, v6, :cond_5

    .line 2280
    aget-object v13, v9, v4

    invoke-static {v13}, Lcom/samsung/musicplus/provider/SecDatabaseUtils;->keyFor(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 2282
    .local v5, "key":Ljava/lang/String;
    if-eqz v5, :cond_1

    .line 2283
    const-string v13, "\\"

    const-string v14, "\\\\"

    invoke-virtual {v5, v13, v14}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    .line 2284
    const-string v13, "%"

    const-string v14, "\\%"

    invoke-virtual {v5, v13, v14}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    .line 2285
    const-string v13, "_"

    const-string v14, "\\_"

    invoke-virtual {v5, v13, v14}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    .line 2286
    aget-object v13, v9, v4

    const-string v14, "a"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-nez v13, :cond_0

    aget-object v13, v9, v4

    const-string v14, "an"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-nez v13, :cond_0

    aget-object v13, v9, v4

    const-string v14, "the"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_4

    :cond_0
    const-string v13, "%"

    :goto_3
    aput-object v13, v12, v4

    .line 2272
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 2264
    .end local v2    # "col":Ljava/text/Collator;
    .end local v4    # "i":I
    .end local v5    # "key":Ljava/lang/String;
    .end local v6    # "len":I
    .end local v8    # "search":Ljava/lang/String;
    .end local v9    # "searchWords":[Ljava/lang/String;
    .end local v12    # "wildcardWords":[Ljava/lang/String;
    :cond_2
    invoke-virtual/range {p3 .. p3}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v8

    goto :goto_0

    .line 2267
    .restart local v8    # "search":Ljava/lang/String;
    :cond_3
    const/4 v13, 0x0

    new-array v9, v13, [Ljava/lang/String;

    goto :goto_1

    .line 2286
    .restart local v2    # "col":Ljava/text/Collator;
    .restart local v4    # "i":I
    .restart local v5    # "key":Ljava/lang/String;
    .restart local v6    # "len":I
    .restart local v9    # "searchWords":[Ljava/lang/String;
    .restart local v12    # "wildcardWords":[Ljava/lang/String;
    :cond_4
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "%"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, "%"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    goto :goto_3

    .line 2291
    .end local v5    # "key":Ljava/lang/String;
    :cond_5
    const-string v11, ""

    .line 2292
    .local v11, "where":Ljava/lang/String;
    const/4 v4, 0x0

    :goto_4
    array-length v13, v9

    if-ge v4, v13, :cond_7

    .line 2293
    if-nez v4, :cond_6

    .line 2294
    const-string v11, "match LIKE ? ESCAPE \'\\\'"

    .line 2292
    :goto_5
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    .line 2296
    :cond_6
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " AND match LIKE ? ESCAPE \'\\\'"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    goto :goto_5

    .line 2300
    :cond_7
    const-string v3, ""

    .line 2301
    .local v3, "combinedWhere":Ljava/lang/String;
    if-eqz p5, :cond_8

    .line 2302
    move-object/from16 v3, p5

    .line 2305
    :cond_8
    if-eqz v11, :cond_a

    invoke-virtual {v11}, Ljava/lang/String;->isEmpty()Z

    move-result v13

    if-nez v13, :cond_a

    .line 2306
    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v13

    if-nez v13, :cond_9

    .line 2307
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " AND "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2309
    :cond_9
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2312
    :cond_a
    sget-object v13, Lcom/samsung/musicplus/provider/MusicProvider;->SEARCH_QUERY_FORMAT:Ljava/lang/String;

    const/4 v14, 0x3

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    aput-object v3, v14, v15

    const/4 v15, 0x1

    aput-object v3, v14, v15

    const/4 v15, 0x2

    aput-object v3, v14, v15

    invoke-static {v13, v14}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    .line 2315
    .local v10, "sql":Ljava/lang/String;
    array-length v13, v12

    mul-int/lit8 v13, v13, 0x3

    new-array v7, v13, [Ljava/lang/String;

    .line 2316
    .local v7, "newWildcardWord":[Ljava/lang/String;
    const/4 v4, 0x0

    :goto_6
    array-length v13, v12

    mul-int/lit8 v13, v13, 0x3

    if-ge v4, v13, :cond_b

    .line 2317
    array-length v13, v12

    rem-int v13, v4, v13

    aget-object v13, v12, v13

    aput-object v13, v7, v4

    .line 2316
    add-int/lit8 v4, v4, 0x1

    goto :goto_6

    .line 2320
    :cond_b
    move-object/from16 v0, p1

    invoke-virtual {v0, v10, v7}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 2321
    .local v1, "c":Landroid/database/Cursor;
    if-eqz v1, :cond_c

    .line 2323
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/provider/MusicProvider;->getContext()Landroid/content/Context;

    move-result-object v13

    invoke-virtual {v13}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v13

    move-object/from16 v0, p3

    invoke-interface {v1, v13, v0}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    .line 2326
    :cond_c
    return-object v1
.end method

.method private getCellIds(I)Ljava/lang/String;
    .locals 5
    .param p1, "uriMatch"    # I

    .prologue
    const/4 v4, 0x5

    .line 1429
    invoke-virtual {p0}, Lcom/samsung/musicplus/provider/MusicProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0e0003

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    .line 1431
    .local v1, "column":I
    const-string v0, ""

    .line 1432
    .local v0, "cellIds":Ljava/lang/String;
    packed-switch p1, :pswitch_data_0

    .line 1464
    :goto_0
    return-object v0

    .line 1434
    :pswitch_0
    if-ne v1, v4, :cond_0

    .line 1435
    const-string v0, "0,1,2,3,4,5,6,7,8,9"

    goto :goto_0

    .line 1437
    :cond_0
    const-string v0, "0,1,2"

    .line 1439
    goto :goto_0

    .line 1441
    :pswitch_1
    if-ne v1, v4, :cond_1

    .line 1442
    const-string v0, "15,16,17,18,19,20,21,22,23,24"

    goto :goto_0

    .line 1444
    :cond_1
    const-string v0, "6,7,8"

    .line 1446
    goto :goto_0

    .line 1448
    :pswitch_2
    if-ne v1, v4, :cond_2

    .line 1449
    const-string v0, "0,1,5,6,10,11,15,16,20,21"

    goto :goto_0

    .line 1451
    :cond_2
    const-string v0, "0,3,6"

    .line 1453
    goto :goto_0

    .line 1455
    :pswitch_3
    if-ne v1, v4, :cond_3

    .line 1456
    const-string v0, "3,4,8,9,13,14,18,19,23,24"

    goto :goto_0

    .line 1458
    :cond_3
    const-string v0, "2,5,8"

    .line 1460
    goto :goto_0

    .line 1432
    :pswitch_data_0
    .packed-switch 0x65
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private insertInternal(Landroid/net/Uri;ILandroid/content/ContentValues;)Landroid/net/Uri;
    .locals 8
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "match"    # I
    .param p3, "initialValues"    # Landroid/content/ContentValues;

    .prologue
    const-wide/16 v6, 0x0

    const/4 v5, 0x0

    .line 2093
    const/4 v0, 0x0

    .line 2094
    .local v0, "newUri":Landroid/net/Uri;
    sparse-switch p2, :sswitch_data_0

    .line 2162
    new-instance v1, Ljava/lang/UnsupportedOperationException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Invalid URI "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v4}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2096
    :sswitch_0
    const-string v1, "media_type"

    const/high16 v4, 0x40000

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {p3, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2097
    const-string v1, "source_id"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {p3, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2098
    sget-boolean v1, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_PINYIN:Z

    if-eqz v1, :cond_0

    .line 2099
    const-string v1, "title"

    const-string v4, "title_pinyin"

    invoke-static {p3, v1, v4}, Lcom/samsung/musicplus/provider/MusicProvider;->subConvertOrgin2PinyinForSort(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 2102
    :cond_0
    iget-object v1, p0, Lcom/samsung/musicplus/provider/MusicProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string v4, "audio_meta"

    invoke-virtual {v1, v4, v5, p3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    .line 2103
    .local v2, "rowId":J
    cmp-long v1, v2, v6

    if-lez v1, :cond_1

    .line 2104
    sget-object v1, Lcom/samsung/musicplus/contents/dlna/DlnaStore$ServerContents;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v1, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 2164
    :cond_1
    :goto_0
    return-object v0

    .line 2109
    .end local v2    # "rowId":J
    :sswitch_1
    iget-object v1, p0, Lcom/samsung/musicplus/provider/MusicProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string v4, "dlna_dms_table"

    invoke-virtual {v1, v4, v5, p3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    .line 2110
    .restart local v2    # "rowId":J
    cmp-long v1, v2, v6

    if-lez v1, :cond_1

    .line 2111
    sget-object v1, Lcom/samsung/musicplus/contents/dlna/DlnaStore$Server;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v1, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    .line 2115
    .end local v2    # "rowId":J
    :sswitch_2
    iget-object v1, p0, Lcom/samsung/musicplus/provider/MusicProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string v4, "dlna_dmr_table"

    invoke-virtual {v1, v4, v5, p3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    .line 2116
    .restart local v2    # "rowId":J
    cmp-long v1, v2, v6

    if-lez v1, :cond_1

    .line 2117
    sget-object v1, Lcom/samsung/musicplus/contents/dlna/DlnaStore$Renderer;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v1, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    .line 2121
    .end local v2    # "rowId":J
    :sswitch_3
    iget-object v1, p0, Lcom/samsung/musicplus/provider/MusicProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string v4, "dlna_open_intent_table"

    invoke-virtual {v1, v4, v5, p3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    .line 2122
    .restart local v2    # "rowId":J
    cmp-long v1, v2, v6

    if-lez v1, :cond_1

    .line 2123
    sget-object v1, Lcom/samsung/musicplus/contents/dlna/DlnaStore$ServerContentsExtra;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v1, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    .line 2129
    .end local v2    # "rowId":J
    :sswitch_4
    iget-object v1, p0, Lcom/samsung/musicplus/provider/MusicProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string v4, "audio_meta"

    invoke-virtual {v1, v4, v5, p3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    .line 2130
    .restart local v2    # "rowId":J
    cmp-long v1, v2, v6

    if-lez v1, :cond_2

    .line 2131
    sget-object v1, Lcom/samsung/musicplus/provider/MusicContents$Audio$Media;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v1, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 2134
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "source_id"

    invoke-virtual {p3, v4}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {p2, v4, v5, v2, v3}, Lcom/samsung/musicplus/provider/MusicProvider;->makeHashTable(IJJ)V

    goto :goto_0

    .line 2138
    .end local v2    # "rowId":J
    :sswitch_5
    iget-object v1, p0, Lcom/samsung/musicplus/provider/MusicProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string v4, "audio_playlists"

    invoke-virtual {v1, v4, v5, p3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    .line 2139
    .restart local v2    # "rowId":J
    cmp-long v1, v2, v6

    if-lez v1, :cond_3

    .line 2140
    sget-object v1, Lcom/samsung/musicplus/provider/MusicContents$Audio$Playlists;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v1, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 2143
    :cond_3
    const-string v1, "source_id"

    invoke-virtual {p3, v1}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 2144
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, ""

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "source_id"

    invoke-virtual {p3, v4}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {p2, v4, v5, v2, v3}, Lcom/samsung/musicplus/provider/MusicProvider;->makeHashTable(IJJ)V

    goto/16 :goto_0

    .line 2148
    .end local v2    # "rowId":J
    :sswitch_6
    iget-object v1, p0, Lcom/samsung/musicplus/provider/MusicProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string v4, "album_art"

    invoke-virtual {v1, v4, v5, p3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    .line 2149
    .restart local v2    # "rowId":J
    cmp-long v1, v2, v6

    if-lez v1, :cond_1

    .line 2150
    sget-object v1, Lcom/samsung/musicplus/provider/MusicContents$Audio$AlbumArt;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v1, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    goto/16 :goto_0

    .line 2155
    .end local v2    # "rowId":J
    :sswitch_7
    iget-object v1, p0, Lcom/samsung/musicplus/provider/MusicProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string v4, "dlna_album_art"

    invoke-virtual {v1, v4, v5, p3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    .line 2156
    .restart local v2    # "rowId":J
    cmp-long v1, v2, v6

    if-lez v1, :cond_1

    .line 2157
    sget-object v1, Lcom/samsung/musicplus/provider/MusicContents$Audio$AlbumArt;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v1, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    goto/16 :goto_0

    .line 2094
    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x6 -> :sswitch_1
        0x7 -> :sswitch_2
        0x8 -> :sswitch_3
        0xb -> :sswitch_7
        0x44c -> :sswitch_4
        0x456 -> :sswitch_5
        0x460 -> :sswitch_6
    .end sparse-switch
.end method

.method private insertMoodCellGroupValue(III)V
    .locals 23
    .param p1, "row"    # I
    .param p2, "column"    # I
    .param p3, "allSongs"    # I

    .prologue
    .line 1726
    const/4 v6, 0x0

    .line 1728
    .local v6, "aRowSongs":I
    div-int v18, v6, p2

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->round(F)I

    move-result v5

    .line 1729
    .local v5, "aCellSongs":I
    if-nez v5, :cond_0

    .line 1731
    const/4 v5, 0x1

    .line 1734
    :cond_0
    move/from16 v15, p3

    .line 1735
    .local v15, "remainSongs":I
    const/4 v14, 0x0

    .line 1736
    .local v14, "offset":I
    const/4 v8, 0x0

    .line 1738
    .local v8, "cellId":I
    sget-object v18, Lcom/samsung/musicplus/provider/MusicProvider;->CLASSNAME:Ljava/lang/String;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "insertMoodCellGroupValue row : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " column : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " aRowSongs : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " aCellSongs : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1741
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_0
    move/from16 v0, p1

    if-ge v10, v0, :cond_1

    .line 1744
    sub-int v18, p1, v10

    div-int v18, v15, v18

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->round(F)I

    move-result v6

    .line 1745
    sub-int/2addr v15, v6

    .line 1746
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/provider/MusicProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    move-object/from16 v18, v0

    const-string v19, "SELECT _id, mood_exciting, mood_brightness FROM (SELECT _id, mood_exciting, mood_brightness FROM audio_meta WHERE is_analyzed=1 AND mood_exciting IS NOT NULL AND mood_brightness IS NOT NULL ORDER BY mood_exciting DESC LIMIT ? OFFSET ?) ORDER BY mood_brightness,mood_exciting;"

    const/16 v20, 0x2

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v22

    aput-object v22, v20, v21

    const/16 v21, 0x1

    invoke-static {v14}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v22

    aput-object v22, v20, v21

    invoke-virtual/range {v18 .. v20}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 1749
    .local v7, "c":Landroid/database/Cursor;
    if-nez v7, :cond_2

    .line 1781
    .end local v7    # "c":Landroid/database/Cursor;
    :cond_1
    return-void

    .line 1753
    .restart local v7    # "c":Landroid/database/Cursor;
    :cond_2
    add-int/2addr v14, v6

    .line 1755
    move/from16 v16, v6

    .line 1756
    .local v16, "remainSongsRow":I
    const/4 v12, 0x0

    .local v12, "j":I
    move v9, v8

    .end local v8    # "cellId":I
    .local v9, "cellId":I
    :goto_1
    move/from16 v0, p2

    if-ge v12, v0, :cond_6

    .line 1757
    sub-int v18, p2, v12

    div-int v18, v16, v18

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->round(F)I

    move-result v5

    .line 1758
    sub-int v16, v16, v5

    .line 1760
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    .line 1761
    .local v11, "ids":Ljava/lang/StringBuilder;
    const/4 v13, 0x0

    .local v13, "k":I
    :goto_2
    if-ge v13, v5, :cond_3

    .line 1762
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v18

    if-nez v18, :cond_4

    .line 1771
    :cond_3
    sget-object v18, Lcom/samsung/musicplus/provider/MusicProvider;->CLASSNAME:Ljava/lang/String;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "insertMoodCellGroupValue cellId : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " ids.toString() : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1774
    new-instance v17, Landroid/content/ContentValues;

    invoke-direct/range {v17 .. v17}, Landroid/content/ContentValues;-><init>()V

    .line 1775
    .local v17, "value":Landroid/content/ContentValues;
    const-string v18, "mood_cell"

    add-int/lit8 v8, v9, 0x1

    .end local v9    # "cellId":I
    .restart local v8    # "cellId":I
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    invoke-virtual/range {v17 .. v19}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1776
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/provider/MusicProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    move-object/from16 v18, v0

    const-string v19, "audio_meta"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "_id IN ("

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ")"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    const/16 v21, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move-object/from16 v2, v17

    move-object/from16 v3, v20

    move-object/from16 v4, v21

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1756
    add-int/lit8 v12, v12, 0x1

    move v9, v8

    .end local v8    # "cellId":I
    .restart local v9    # "cellId":I
    goto/16 :goto_1

    .line 1765
    .end local v17    # "value":Landroid/content/ContentValues;
    :cond_4
    if-eqz v13, :cond_5

    .line 1766
    const-string v18, ","

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1768
    :cond_5
    const-string v18, "_id"

    move-object/from16 v0, v18

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v18

    move/from16 v0, v18

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1761
    add-int/lit8 v13, v13, 0x1

    goto/16 :goto_2

    .line 1779
    .end local v11    # "ids":Ljava/lang/StringBuilder;
    .end local v13    # "k":I
    :cond_6
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 1741
    add-int/lit8 v10, v10, 0x1

    move v8, v9

    .end local v9    # "cellId":I
    .restart local v8    # "cellId":I
    goto/16 :goto_0
.end method

.method private insertYearCellGroupValue(III)V
    .locals 23
    .param p1, "row"    # I
    .param p2, "column"    # I
    .param p3, "allSongs"    # I

    .prologue
    .line 1784
    const/4 v6, 0x0

    .line 1786
    .local v6, "aRowSongs":I
    div-int v18, v6, p2

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->round(F)I

    move-result v5

    .line 1787
    .local v5, "aCellSongs":I
    if-nez v5, :cond_0

    .line 1789
    const/4 v5, 0x1

    .line 1792
    :cond_0
    move/from16 v15, p3

    .line 1793
    .local v15, "remainSongs":I
    const/4 v14, 0x0

    .line 1794
    .local v14, "offset":I
    const/4 v8, 0x0

    .line 1796
    .local v8, "cellId":I
    sget-object v18, Lcom/samsung/musicplus/provider/MusicProvider;->CLASSNAME:Ljava/lang/String;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "insertYearCellGroupValue row : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " column : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " aRowSongs : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " aCellSongs : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1799
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_0
    move/from16 v0, p1

    if-ge v10, v0, :cond_1

    .line 1802
    sub-int v18, p1, v10

    div-int v18, v15, v18

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->round(F)I

    move-result v6

    .line 1803
    sub-int/2addr v15, v6

    .line 1804
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/provider/MusicProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    move-object/from16 v18, v0

    const-string v19, "SELECT _id, mood_exciting, year_name FROM (SELECT _id, mood_exciting, year_name FROM audio_meta WHERE is_analyzed=1 AND mood_exciting IS NOT NULL AND mood_brightness IS NOT NULL ORDER BY mood_exciting DESC LIMIT ? OFFSET ?) ORDER BY year_name,mood_exciting;"

    const/16 v20, 0x2

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v22

    aput-object v22, v20, v21

    const/16 v21, 0x1

    invoke-static {v14}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v22

    aput-object v22, v20, v21

    invoke-virtual/range {v18 .. v20}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 1807
    .local v7, "c":Landroid/database/Cursor;
    if-nez v7, :cond_2

    .line 1839
    .end local v7    # "c":Landroid/database/Cursor;
    :cond_1
    return-void

    .line 1811
    .restart local v7    # "c":Landroid/database/Cursor;
    :cond_2
    add-int/2addr v14, v6

    .line 1813
    move/from16 v16, v6

    .line 1814
    .local v16, "remainSongsRow":I
    const/4 v12, 0x0

    .local v12, "j":I
    move v9, v8

    .end local v8    # "cellId":I
    .local v9, "cellId":I
    :goto_1
    move/from16 v0, p2

    if-ge v12, v0, :cond_6

    .line 1815
    sub-int v18, p2, v12

    div-int v18, v16, v18

    move/from16 v0, v18

    int-to-float v0, v0

    move/from16 v18, v0

    invoke-static/range {v18 .. v18}, Ljava/lang/Math;->round(F)I

    move-result v5

    .line 1816
    sub-int v16, v16, v5

    .line 1818
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    .line 1819
    .local v11, "ids":Ljava/lang/StringBuilder;
    const/4 v13, 0x0

    .local v13, "k":I
    :goto_2
    if-ge v13, v5, :cond_3

    .line 1820
    invoke-interface {v7}, Landroid/database/Cursor;->moveToNext()Z

    move-result v18

    if-nez v18, :cond_4

    .line 1829
    :cond_3
    sget-object v18, Lcom/samsung/musicplus/provider/MusicProvider;->CLASSNAME:Ljava/lang/String;

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "insertYearCellGroupValue cellId : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " ids.toString() : "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1832
    new-instance v17, Landroid/content/ContentValues;

    invoke-direct/range {v17 .. v17}, Landroid/content/ContentValues;-><init>()V

    .line 1833
    .local v17, "value":Landroid/content/ContentValues;
    const-string v18, "year_cell"

    add-int/lit8 v8, v9, 0x1

    .end local v9    # "cellId":I
    .restart local v8    # "cellId":I
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    invoke-virtual/range {v17 .. v19}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1834
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/provider/MusicProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    move-object/from16 v18, v0

    const-string v19, "audio_meta"

    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    const-string v21, "_id IN ("

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    const-string v21, ")"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v20

    const/16 v21, 0x0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    move-object/from16 v2, v17

    move-object/from16 v3, v20

    move-object/from16 v4, v21

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1814
    add-int/lit8 v12, v12, 0x1

    move v9, v8

    .end local v8    # "cellId":I
    .restart local v9    # "cellId":I
    goto/16 :goto_1

    .line 1823
    .end local v17    # "value":Landroid/content/ContentValues;
    :cond_4
    if-eqz v13, :cond_5

    .line 1824
    const-string v18, ","

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1826
    :cond_5
    const-string v18, "_id"

    move-object/from16 v0, v18

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v18

    move/from16 v0, v18

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1819
    add-int/lit8 v13, v13, 0x1

    goto/16 :goto_2

    .line 1837
    .end local v11    # "ids":Ljava/lang/StringBuilder;
    .end local v13    # "k":I
    :cond_6
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 1799
    add-int/lit8 v10, v10, 0x1

    move v8, v9

    .end local v9    # "cellId":I
    .restart local v8    # "cellId":I
    goto/16 :goto_0
.end method

.method private isActiveMusicService(Landroid/content/Context;)Z
    .locals 7
    .param p1, "c"    # Landroid/content/Context;

    .prologue
    const/4 v6, 0x0

    .line 1059
    const-string v5, "activity"

    invoke-virtual {p1, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 1062
    .local v0, "am":Landroid/app/ActivityManager;
    const/16 v5, 0x2710

    invoke-virtual {v0, v5}, Landroid/app/ActivityManager;->getRunningServices(I)Ljava/util/List;

    move-result-object v3

    .line 1063
    .local v3, "s":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningServiceInfo;>;"
    if-eqz v3, :cond_1

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    .line 1064
    .local v4, "size":I
    :goto_0
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v4, :cond_0

    .line 1065
    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/app/ActivityManager$RunningServiceInfo;

    iget-object v2, v5, Landroid/app/ActivityManager$RunningServiceInfo;->process:Ljava/lang/String;

    .line 1066
    .local v2, "name":Ljava/lang/String;
    const-string v5, "com.sec.android.app.music:service"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 1067
    const/4 v6, 0x1

    .line 1070
    .end local v2    # "name":Ljava/lang/String;
    :cond_0
    return v6

    .end local v1    # "i":I
    .end local v4    # "size":I
    :cond_1
    move v4, v6

    .line 1063
    goto :goto_0

    .line 1064
    .restart local v1    # "i":I
    .restart local v2    # "name":Ljava/lang/String;
    .restart local v4    # "size":I
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public static makeHashTable(IJJ)V
    .locals 3
    .param p0, "match"    # I
    .param p1, "key"    # J
    .param p3, "value"    # J

    .prologue
    .line 2483
    sparse-switch p0, :sswitch_data_0

    .line 2501
    :cond_0
    :goto_0
    return-void

    .line 2488
    :sswitch_0
    sget-object v0, Lcom/samsung/musicplus/provider/MusicProvider;->sAudioMapSyncTable:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2489
    sget-object v0, Lcom/samsung/musicplus/provider/MusicProvider;->sAudioMapSyncTable:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 2495
    :sswitch_1
    sget-object v0, Lcom/samsung/musicplus/provider/MusicProvider;->sPlaylistsMapSyncTable:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2496
    sget-object v0, Lcom/samsung/musicplus/provider/MusicProvider;->sPlaylistsMapSyncTable:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 2483
    :sswitch_data_0
    .sparse-switch
        0x44c -> :sswitch_0
        0x456 -> :sswitch_1
    .end sparse-switch
.end method

.method private static makePlaylistName(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "origin"    # Ljava/lang/String;

    .prologue
    .line 737
    const/4 v8, 0x2

    .line 739
    .local v8, "num":I
    const/4 v6, 0x0

    .line 740
    .local v6, "c":Landroid/database/Cursor;
    const/4 v7, 0x0

    .line 742
    .local v7, "count":I
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Audio$Playlists;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "count(*)"

    aput-object v4, v2, v3

    const-string v3, "name=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 749
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 750
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v7

    .line 754
    :cond_0
    if-eqz v6, :cond_1

    .line 755
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 756
    const/4 v6, 0x0

    .line 760
    :cond_1
    if-nez v7, :cond_3

    .line 791
    .end local p1    # "origin":Ljava/lang/String;
    :goto_0
    return-object p1

    .line 754
    .restart local p1    # "origin":Ljava/lang/String;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 755
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 756
    const/4 v6, 0x0

    :cond_2
    throw v0

    .line 764
    :cond_3
    move-object v10, p1

    .line 766
    .local v10, "suggestedname":Ljava/lang/String;
    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    add-int/lit8 v9, v8, 0x1

    .end local v8    # "num":I
    .local v9, "num":I
    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 768
    :try_start_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Audio$Playlists;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "count(*)"

    aput-object v4, v2, v3

    const-string v3, "name=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v10, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 775
    if-eqz v6, :cond_4

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 776
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v7

    .line 780
    :cond_4
    if-eqz v6, :cond_5

    .line 781
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 782
    const/4 v6, 0x0

    .line 786
    :cond_5
    if-nez v7, :cond_7

    move v8, v9

    .end local v9    # "num":I
    .restart local v8    # "num":I
    move-object p1, v10

    .line 791
    goto :goto_0

    .line 780
    .end local v8    # "num":I
    .restart local v9    # "num":I
    :catchall_1
    move-exception v0

    if-eqz v6, :cond_6

    .line 781
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 782
    const/4 v6, 0x0

    :cond_6
    throw v0

    :cond_7
    move v8, v9

    .end local v9    # "num":I
    .restart local v8    # "num":I
    goto :goto_1
.end method

.method private movePlaylistEntry(Landroid/database/sqlite/SQLiteDatabase;JII)I
    .locals 16
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "playlist"    # J
    .param p4, "from"    # I
    .param p5, "to"    # I

    .prologue
    .line 2039
    move/from16 v0, p4

    move/from16 v1, p5

    if-ne v0, v1, :cond_0

    .line 2040
    const/4 v13, 0x0

    .line 2087
    :goto_0
    return v13

    .line 2042
    :cond_0
    invoke-virtual/range {p1 .. p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 2044
    const/4 v13, 0x0

    .line 2045
    .local v13, "numlines":I
    :try_start_0
    const-string v3, "audio_playlists_map"

    const/4 v2, 0x1

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v5, "play_order"

    aput-object v5, v4, v2

    const-string v5, "playlist_id=?"

    const/4 v2, 0x1

    new-array v6, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-wide/from16 v0, p2

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v2

    const/4 v7, 0x0

    const/4 v8, 0x0

    const-string v9, "play_order"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move/from16 v0, p4

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v10, ",1"

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v10}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 2050
    .local v11, "c":Landroid/database/Cursor;
    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    .line 2051
    const/4 v2, 0x0

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    .line 2052
    .local v12, "from_play_order":I
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 2053
    const-string v3, "audio_playlists_map"

    const/4 v2, 0x1

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v5, "play_order"

    aput-object v5, v4, v2

    const-string v5, "playlist_id=?"

    const/4 v2, 0x1

    new-array v6, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-wide/from16 v0, p2

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v2

    const/4 v7, 0x0

    const/4 v8, 0x0

    const-string v9, "play_order"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move/from16 v0, p5

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v10, ",1"

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v10}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 2058
    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    .line 2059
    const/4 v2, 0x0

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    .line 2060
    .local v14, "to_play_order":I
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 2061
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "UPDATE audio_playlists_map SET play_order=-1 WHERE play_order="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AND playlist_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-wide/from16 v0, p2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2068
    move/from16 v0, p4

    move/from16 v1, p5

    if-ge v0, v1, :cond_1

    .line 2069
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "UPDATE audio_playlists_map SET play_order=play_order-1 WHERE play_order<="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AND play_order>"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AND playlist_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-wide/from16 v0, p2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2072
    sub-int v2, p5, p4

    add-int/lit8 v13, v2, 0x1

    .line 2079
    :goto_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "UPDATE audio_playlists_map SET play_order="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " WHERE play_order=-1 AND playlist_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-wide/from16 v0, p2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 2081
    invoke-virtual/range {p1 .. p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 2082
    sget-object v2, Lcom/samsung/musicplus/provider/MusicContents$Audio$Playlists;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-static/range {p2 .. p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v15

    .line 2084
    .local v15, "uri":Landroid/net/Uri;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/provider/MusicProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v15, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2087
    invoke-virtual/range {p1 .. p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    goto/16 :goto_0

    .line 2074
    .end local v15    # "uri":Landroid/net/Uri;
    :cond_1
    :try_start_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "UPDATE audio_playlists_map SET play_order=play_order+1 WHERE play_order>="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AND play_order<"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AND playlist_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-wide/from16 v0, p2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2077
    sub-int v2, p4, p5

    add-int/lit8 v13, v2, 0x1

    goto :goto_1

    .line 2087
    .end local v11    # "c":Landroid/database/Cursor;
    .end local v12    # "from_play_order":I
    .end local v14    # "to_play_order":I
    :catchall_0
    move-exception v2

    invoke-virtual/range {p1 .. p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v2
.end method

.method private notifyMultipleChanges(Landroid/net/Uri;I)V
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "match"    # I

    .prologue
    .line 1938
    const-string v1, "notifyChange"

    invoke-virtual {p1, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1939
    .local v0, "notifyChange":Ljava/lang/String;
    const-string v1, "disable"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1940
    const/16 v1, 0x3e8

    if-lt p2, v1, :cond_1

    .line 1957
    :cond_0
    :goto_0
    return-void

    .line 1954
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/musicplus/provider/MusicProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    goto :goto_0
.end method

.method private playlistBulkInsert(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;[Landroid/content/ContentValues;)I
    .locals 18
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "uri"    # Landroid/net/Uri;
    .param p3, "values"    # [Landroid/content/ContentValues;

    .prologue
    .line 1616
    const-string v13, "INSERT INTO audio_playlists_map (audio_id, playlist_id, play_order) VALUES (?,?,?)"

    .line 1617
    .local v13, "sql":Ljava/lang/String;
    move-object/from16 v0, p1

    invoke-virtual {v0, v13}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v14

    .line 1618
    .local v14, "stmt":Landroid/database/sqlite/SQLiteStatement;
    invoke-virtual/range {p2 .. p2}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v15

    const/16 v16, 0x2

    invoke-interface/range {v15 .. v16}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/String;

    invoke-static {v15}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v10

    .line 1620
    .local v10, "playlistId":J
    const/4 v8, 0x0

    .line 1621
    .local v8, "ordinal":Landroid/database/Cursor;
    const/4 v9, -0x1

    .line 1624
    .local v9, "playOrderBase":I
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "SELECT ifnull(max(play_order), 0)  FROM audio_playlists_map WHERE playlist_id = "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    const/16 v16, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v15, v1}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 1627
    if-eqz v8, :cond_0

    .line 1628
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1629
    const/4 v15, 0x0

    invoke-interface {v8, v15}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    .line 1630
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 1631
    const/4 v8, 0x0

    .line 1635
    :cond_0
    invoke-virtual/range {p1 .. p1}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 1636
    const/4 v6, 0x0

    .line 1638
    .local v6, "numInserted":I
    :try_start_0
    move-object/from16 v0, p3

    array-length v5, v0

    .line 1639
    .local v5, "len":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-ge v4, v5, :cond_3

    .line 1640
    aget-object v15, p3, v4

    const-string v16, "audio_id"

    invoke-virtual/range {v15 .. v16}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/lang/Number;

    invoke-virtual {v15}, Ljava/lang/Number;->longValue()J

    move-result-wide v2

    .line 1642
    .local v2, "audioid":J
    const/4 v15, 0x1

    invoke-virtual {v14, v15, v2, v3}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 1643
    const/4 v15, 0x2

    invoke-virtual {v14, v15, v10, v11}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 1646
    aget-object v15, p3, v4

    const-string v16, "play_order"

    invoke-virtual/range {v15 .. v16}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    .line 1647
    .local v7, "order":Ljava/lang/Object;
    const/4 v12, 0x0

    .line 1649
    .local v12, "playorder":I
    if-nez v7, :cond_2

    .line 1651
    add-int/lit8 v12, v9, 0x1

    .line 1652
    add-int/lit8 v9, v9, 0x1

    .line 1658
    .end local v7    # "order":Ljava/lang/Object;
    :cond_1
    :goto_1
    const/4 v15, 0x3

    int-to-long v0, v12

    move-wide/from16 v16, v0

    invoke-virtual/range {v14 .. v17}, Landroid/database/sqlite/SQLiteStatement;->bindLong(IJ)V

    .line 1660
    invoke-virtual {v14}, Landroid/database/sqlite/SQLiteStatement;->execute()V

    .line 1661
    invoke-virtual {v14}, Landroid/database/sqlite/SQLiteStatement;->clearBindings()V

    .line 1639
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1654
    .restart local v7    # "order":Ljava/lang/Object;
    :cond_2
    instance-of v15, v7, Ljava/lang/Number;

    if-eqz v15, :cond_1

    .line 1655
    check-cast v7, Ljava/lang/Number;

    .end local v7    # "order":Ljava/lang/Object;
    invoke-virtual {v7}, Ljava/lang/Number;->intValue()I

    move-result v12

    goto :goto_1

    .line 1663
    .end local v2    # "audioid":J
    .end local v12    # "playorder":I
    :cond_3
    move v6, v5

    .line 1664
    invoke-virtual/range {p1 .. p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1666
    invoke-virtual/range {p1 .. p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1667
    invoke-virtual {v14}, Landroid/database/sqlite/SQLiteStatement;->close()V

    .line 1669
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/provider/MusicProvider;->getContext()Landroid/content/Context;

    move-result-object v15

    invoke-virtual {v15}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v15

    const/16 v16, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-virtual {v15, v0, v1}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1671
    return v6

    .line 1666
    .end local v4    # "i":I
    .end local v5    # "len":I
    :catchall_0
    move-exception v15

    invoke-virtual/range {p1 .. p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 1667
    invoke-virtual {v14}, Landroid/database/sqlite/SQLiteStatement;->close()V

    throw v15
.end method

.method private static recreateAudioView(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 887
    const-string v0, "DROP VIEW IF EXISTS artist_info"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 888
    const-string v0, "DROP VIEW IF EXISTS album_info"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 889
    const-string v0, "DROP VIEW IF EXISTS music_folders_view;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 893
    const-string v0, "CREATE VIEW IF NOT EXISTS artist_info AS SELECT artist_id AS _id, artist, artist_key, album_id, COUNT(DISTINCT album) AS number_of_albums, COUNT(*) AS number_of_tracks FROM audio_meta GROUP BY artist_key;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 899
    const-string v0, "CREATE VIEW IF NOT EXISTS album_info AS SELECT audio_meta.album_id AS _id, album, album_key, artist, artist_id, artist_key, count(*) AS numsongs,album_art.album_art AS album_art FROM audio_meta LEFT OUTER JOIN album_art ON audio_meta.album_id=album_art.album_id GROUP BY album_key;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 909
    const-string v0, "CREATE VIEW IF NOT EXISTS music_folders_view AS SELECT _id, bucket_id, bucket_display_name, album_id, _data, count(_id) AS count, is_secretbox FROM (SELECT _id, bucket_id, bucket_display_name, album_id, _data, is_secretbox FROM audio_meta ORDER BY title_key DESC) GROUP BY bucket_id;"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 913
    return-void
.end method

.method private restoreFavoriteConfiguration()V
    .locals 9

    .prologue
    .line 2373
    sget-object v5, Lcom/samsung/musicplus/provider/MusicProvider;->CLASSNAME:Ljava/lang/String;

    const-string v6, "restoreFavoriteConfiguration started"

    invoke-static {v5, v6}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2374
    iget-object v5, p0, Lcom/samsung/musicplus/provider/MusicProvider;->mBackupFavoriteList:Ljava/util/List;

    if-nez v5, :cond_0

    .line 2404
    :goto_0
    return-void

    .line 2377
    :cond_0
    iget-object v5, p0, Lcom/samsung/musicplus/provider/MusicProvider;->mBackupFavoriteList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    .line 2378
    .local v0, "count":I
    if-nez v0, :cond_1

    .line 2379
    sget-object v5, Lcom/samsung/musicplus/provider/MusicProvider;->CLASSNAME:Ljava/lang/String;

    const-string v6, "restoreFavoriteConfiguration no restore list"

    invoke-static {v5, v6}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 2382
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/musicplus/provider/MusicProvider;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/musicplus/provider/VolumeStateCompat;->isMountedSDCard(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 2383
    sget-object v5, Lcom/samsung/musicplus/provider/MusicProvider;->CLASSNAME:Ljava/lang/String;

    const-string v6, "restoreFavoriteConfiguration"

    invoke-static {v5, v6}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2384
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 2385
    .local v3, "where":Ljava/lang/StringBuilder;
    new-array v4, v0, [Ljava/lang/String;

    .line 2386
    .local v4, "whereArgs":[Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v0, :cond_3

    .line 2387
    if-eqz v1, :cond_2

    .line 2388
    const-string v5, ","

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2390
    :cond_2
    const-string v5, "?"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2391
    iget-object v5, p0, Lcom/samsung/musicplus/provider/MusicProvider;->mBackupFavoriteList:Ljava/util/List;

    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    aput-object v5, v4, v1

    .line 2386
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2394
    :cond_3
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 2395
    .local v2, "value":Landroid/content/ContentValues;
    const-string v5, "is_favorite"

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 2396
    iget-object v5, p0, Lcom/samsung/musicplus/provider/MusicProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string v6, "audio_meta"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "_data IN ("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v2, v7, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 2399
    iget-object v5, p0, Lcom/samsung/musicplus/provider/MusicProvider;->mBackupFavoriteList:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->clear()V

    .line 2403
    .end local v1    # "i":I
    .end local v2    # "value":Landroid/content/ContentValues;
    .end local v3    # "where":Ljava/lang/StringBuilder;
    .end local v4    # "whereArgs":[Ljava/lang/String;
    :goto_2
    sget-object v5, Lcom/samsung/musicplus/provider/MusicProvider;->CLASSNAME:Ljava/lang/String;

    const-string v6, "restoreFavoriteConfiguration finished"

    invoke-static {v5, v6}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2401
    :cond_4
    sget-object v5, Lcom/samsung/musicplus/provider/MusicProvider;->CLASSNAME:Ljava/lang/String;

    const-string v6, "restoreFavoriteConfiguration extSdCard isn\'t mounted"

    invoke-static {v5, v6}, Lcom/samsung/musicplus/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method private restorePlaylistTableConfiguration()V
    .locals 6

    .prologue
    .line 2337
    sget-object v3, Lcom/samsung/musicplus/provider/MusicProvider;->CLASSNAME:Ljava/lang/String;

    const-string v4, "restorePlaylistTableConfiguration started"

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2339
    invoke-virtual {p0}, Lcom/samsung/musicplus/provider/MusicProvider;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/musicplus/provider/VolumeStateCompat;->isMountedSDCard(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2340
    sget-object v3, Lcom/samsung/musicplus/provider/MusicProvider;->CLASSNAME:Ljava/lang/String;

    const-string v4, "restorePlaylistTableConfiguration"

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2341
    const/4 v2, 0x0

    .line 2343
    .local v2, "ss1":Landroid/database/sqlite/SQLiteStatement;
    :try_start_0
    const-string v1, "INSERT INTO audio_playlists_map ( audio_id, playlist_id, play_order ) SELECT audio_meta._id, audio_playlists_map_cache.playlist_id,  audio_playlists_map_cache.play_order FROM audio_playlists_map_cache, audio_meta WHERE  audio_meta._data = audio_playlists_map_cache._data ;"

    .line 2351
    .local v1, "sqlstatement2":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/musicplus/provider/MusicProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v2

    .line 2353
    if-eqz v2, :cond_0

    .line 2354
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteStatement;->executeInsert()J

    .line 2355
    sget-object v3, Lcom/samsung/musicplus/provider/MusicProvider;->CLASSNAME:Ljava/lang/String;

    const-string v4, "delete from audio_playlists_map_cache"

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2356
    iget-object v3, p0, Lcom/samsung/musicplus/provider/MusicProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string v4, "DELETE FROM audio_playlists_map_cache"

    invoke-virtual {v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2363
    :cond_0
    if-eqz v2, :cond_1

    .line 2364
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteStatement;->close()V

    .line 2369
    .end local v1    # "sqlstatement2":Ljava/lang/String;
    .end local v2    # "ss1":Landroid/database/sqlite/SQLiteStatement;
    :cond_1
    :goto_0
    sget-object v3, Lcom/samsung/musicplus/provider/MusicProvider;->CLASSNAME:Ljava/lang/String;

    const-string v4, "restorePlaylistTableConfiguration finished"

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2370
    return-void

    .line 2359
    .restart local v2    # "ss1":Landroid/database/sqlite/SQLiteStatement;
    :catch_0
    move-exception v0

    .line 2360
    .local v0, "e":Landroid/database/SQLException;
    :try_start_1
    sget-object v3, Lcom/samsung/musicplus/provider/MusicProvider;->CLASSNAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " restorePlaylistTableConfiguration(): "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Landroid/database/SQLException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2363
    if-eqz v2, :cond_1

    .line 2364
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteStatement;->close()V

    goto :goto_0

    .line 2363
    .end local v0    # "e":Landroid/database/SQLException;
    :catchall_0
    move-exception v3

    if-eqz v2, :cond_2

    .line 2364
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteStatement;->close()V

    :cond_2
    throw v3

    .line 2367
    .end local v2    # "ss1":Landroid/database/sqlite/SQLiteStatement;
    :cond_3
    sget-object v3, Lcom/samsung/musicplus/provider/MusicProvider;->CLASSNAME:Ljava/lang/String;

    const-string v4, "restorePlaylistTableConfiguration extSdCard isn\'t mounted"

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private saveFavoriteConfiguration()V
    .locals 11

    .prologue
    .line 2409
    const/4 v8, 0x0

    .line 2410
    .local v8, "c":Landroid/database/Cursor;
    sget-object v0, Lcom/samsung/musicplus/provider/MusicProvider;->CLASSNAME:Ljava/lang/String;

    const-string v1, "saveFavoriteConfiguration started"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2412
    :try_start_0
    iget-object v0, p0, Lcom/samsung/musicplus/provider/MusicProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "audio_meta"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_data"

    aput-object v4, v2, v3

    const-string v3, "is_favorite = 1"

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 2415
    const/4 v9, 0x0

    .line 2416
    .local v9, "count":I
    if-eqz v8, :cond_0

    .line 2417
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v9

    .line 2419
    :cond_0
    if-eqz v9, :cond_3

    .line 2420
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_0
    if-ge v10, v9, :cond_1

    .line 2421
    invoke-interface {v8, v10}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 2422
    iget-object v0, p0, Lcom/samsung/musicplus/provider/MusicProvider;->mBackupFavoriteList:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2420
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 2424
    :cond_1
    sget-object v0, Lcom/samsung/musicplus/provider/MusicProvider;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "saveFavoriteConfiguration for saving count : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2429
    .end local v10    # "i":I
    :goto_1
    if-eqz v8, :cond_2

    .line 2430
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 2431
    const/4 v8, 0x0

    .line 2435
    :cond_2
    return-void

    .line 2426
    :cond_3
    :try_start_1
    sget-object v0, Lcom/samsung/musicplus/provider/MusicProvider;->CLASSNAME:Ljava/lang/String;

    const-string v1, "saveFavoriteConfiguration we have no favorite list for saving"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 2429
    .end local v9    # "count":I
    :catchall_0
    move-exception v0

    if-eqz v8, :cond_4

    .line 2430
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 2431
    const/4 v8, 0x0

    :cond_4
    throw v0
.end method

.method private savePlaylistTableConfiguration()V
    .locals 6

    .prologue
    .line 2443
    sget-object v3, Lcom/samsung/musicplus/provider/MusicProvider;->CLASSNAME:Ljava/lang/String;

    const-string v4, "savePlaylistTableConfiguration started"

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2444
    sget-object v3, Lcom/samsung/musicplus/provider/MusicProvider;->CLASSNAME:Ljava/lang/String;

    const-string v4, "savePlaylistTableConfiguration(for SD card)"

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2446
    const/4 v2, 0x0

    .line 2448
    .local v2, "ss1":Landroid/database/sqlite/SQLiteStatement;
    :try_start_0
    const-string v1, "INSERT INTO audio_playlists_map_cache ( _id, audio_id, playlist_id, play_order, _data ) SELECT audio_playlists_map._id, audio_playlists_map.audio_id, audio_playlists_map.playlist_id, audio_playlists_map.play_order, audio_meta._data FROM audio_playlists_map, audio_meta WHERE audio_meta._id = audio_playlists_map.audio_id AND audio_meta._data LIKE \'/storage/extSdCard/%\';"

    .line 2458
    .local v1, "sqlstatement1":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/musicplus/provider/MusicProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v2

    .line 2460
    if-eqz v2, :cond_0

    .line 2461
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteStatement;->executeInsert()J
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2467
    :cond_0
    if-eqz v2, :cond_1

    .line 2468
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteStatement;->close()V

    .line 2470
    .end local v1    # "sqlstatement1":Ljava/lang/String;
    :cond_1
    :goto_0
    sget-object v3, Lcom/samsung/musicplus/provider/MusicProvider;->CLASSNAME:Ljava/lang/String;

    const-string v4, "savePlaylistTableConfiguration finished"

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2472
    return-void

    .line 2464
    :catch_0
    move-exception v0

    .line 2465
    .local v0, "e":Landroid/database/SQLException;
    :try_start_1
    sget-object v3, Lcom/samsung/musicplus/provider/MusicProvider;->CLASSNAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " savePlaylistTableConfiguration(): "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Landroid/database/SQLException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2467
    if-eqz v2, :cond_1

    .line 2468
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteStatement;->close()V

    goto :goto_0

    .line 2467
    .end local v0    # "e":Landroid/database/SQLException;
    :catchall_0
    move-exception v3

    if-eqz v2, :cond_2

    .line 2468
    invoke-virtual {v2}, Landroid/database/sqlite/SQLiteStatement;->close()V

    :cond_2
    throw v3
.end method

.method private static subConvertOrgin2PinyinForSort(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "values"    # Landroid/content/ContentValues;
    .param p1, "orginKey"    # Ljava/lang/String;
    .param p2, "pinyinKey"    # Ljava/lang/String;

    .prologue
    .line 2772
    const/4 v1, 0x0

    .line 2773
    .local v1, "pinyin":Ljava/lang/String;
    invoke-virtual {p0, p1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2775
    .local v0, "orgin":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 2776
    const-string v2, "<unknown>"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 2777
    const-string v1, "\u0001"

    .line 2781
    :goto_0
    if-eqz v1, :cond_0

    .line 2782
    invoke-virtual {p0, p2}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 2783
    invoke-virtual {p0, p2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 2787
    :cond_0
    return-object v1

    .line 2779
    :cond_1
    invoke-static {v0}, Lcom/samsung/musicplus/provider/MusicProvider;->computeChineseSortKey(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private static syncFavoriteList(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 703
    const/4 v8, 0x0

    .line 706
    .local v8, "favorite":Landroid/database/Cursor;
    :try_start_0
    const-string v1, "audio_meta"

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "source_id"

    aput-object v3, v2, v0

    const-string v3, "is_favorite=1"

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v0, p1

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 710
    new-instance v10, Lcom/samsung/musicplus/provider/MediaInserter;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/16 v1, 0x1f4

    invoke-direct {v10, v0, v1}, Lcom/samsung/musicplus/provider/MediaInserter;-><init>(Landroid/content/ContentResolver;I)V

    .line 712
    .local v10, "mediaInserter":Lcom/samsung/musicplus/provider/MediaInserter;
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_0
    invoke-interface {v8}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-ge v9, v0, :cond_0

    .line 713
    invoke-interface {v8, v9}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 714
    new-instance v11, Landroid/content/ContentValues;

    invoke-direct {v11}, Landroid/content/ContentValues;-><init>()V

    .line 715
    .local v11, "value":Landroid/content/ContentValues;
    const-string v0, "audio_id"

    const/4 v1, 0x0

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v11, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 716
    const-string v0, "play_order"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v11, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 718
    const-string v0, "external"

    invoke-static {p0}, Lcom/samsung/musicplus/util/UiUtils;->getFavorietListId(Landroid/content/Context;)J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Landroid/provider/MediaStore$Audio$Playlists$Members;->getContentUri(Ljava/lang/String;J)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v10, v0, v11}, Lcom/samsung/musicplus/provider/MediaInserter;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)V

    .line 712
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 723
    .end local v11    # "value":Landroid/content/ContentValues;
    :cond_0
    invoke-virtual {v10}, Lcom/samsung/musicplus/provider/MediaInserter;->flushAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 727
    if-eqz v8, :cond_1

    .line 728
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 729
    const/4 v8, 0x0

    .line 733
    :cond_1
    return-void

    .line 727
    .end local v9    # "i":I
    .end local v10    # "mediaInserter":Lcom/samsung/musicplus/provider/MediaInserter;
    :catchall_0
    move-exception v0

    if-eqz v8, :cond_2

    .line 728
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 729
    const/4 v8, 0x0

    :cond_2
    throw v0
.end method

.method private static syncPlaylists(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 17
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 635
    const/4 v14, 0x0

    .line 638
    .local v14, "playlists":Landroid/database/Cursor;
    :try_start_0
    const-string v3, "audio_playlists"

    const/4 v2, 0x2

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v5, "_id"

    aput-object v5, v4, v2

    const/4 v2, 0x1

    const-string v5, "name"

    aput-object v5, v4, v2

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    .line 641
    invoke-interface {v14}, Landroid/database/Cursor;->getCount()I

    move-result v10

    .line 642
    .local v10, "count":I
    if-eqz v10, :cond_0

    .line 643
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_0
    if-ge v11, v10, :cond_0

    .line 645
    invoke-interface {v14, v11}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 646
    new-instance v16, Landroid/content/ContentValues;

    invoke-direct/range {v16 .. v16}, Landroid/content/ContentValues;-><init>()V

    .line 647
    .local v16, "value":Landroid/content/ContentValues;
    const-string v2, "name"

    const-string v3, "name"

    invoke-interface {v14, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v14, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-static {v0, v3}, Lcom/samsung/musicplus/provider/MusicProvider;->makePlaylistName(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 651
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Landroid/provider/MediaStore$Audio$Playlists;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v16

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v15

    .line 653
    .local v15, "uri":Landroid/net/Uri;
    invoke-virtual {v15}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v2

    const/4 v3, 0x3

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    .line 656
    .local v12, "newId":J
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v14, v12, v13}, Lcom/samsung/musicplus/provider/MusicProvider;->syncPlaylistsMap(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;Landroid/database/Cursor;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 643
    add-int/lit8 v11, v11, 0x1

    goto :goto_0

    .line 660
    .end local v11    # "i":I
    .end local v12    # "newId":J
    .end local v15    # "uri":Landroid/net/Uri;
    .end local v16    # "value":Landroid/content/ContentValues;
    :cond_0
    if-eqz v14, :cond_1

    .line 661
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    .line 662
    const/4 v14, 0x0

    .line 666
    :cond_1
    return-void

    .line 660
    .end local v10    # "count":I
    :catchall_0
    move-exception v2

    if-eqz v14, :cond_2

    .line 661
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    .line 662
    const/4 v14, 0x0

    :cond_2
    throw v2
.end method

.method private static syncPlaylistsMap(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;Landroid/database/Cursor;J)V
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "playlists"    # Landroid/database/Cursor;
    .param p3, "newId"    # J

    .prologue
    .line 670
    const/4 v2, 0x0

    .line 673
    .local v2, "playlistsMap":Landroid/database/Cursor;
    :try_start_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "SELECT source_id, play_order from audio_playlists_map, audio where (audio._id = audio_playlists_map.audio_id) AND playlist_id = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface {p2, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {p1, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 678
    new-instance v1, Lcom/samsung/musicplus/provider/MediaInserter;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const/16 v5, 0x1f4

    invoke-direct {v1, v4, v5}, Lcom/samsung/musicplus/provider/MediaInserter;-><init>(Landroid/content/ContentResolver;I)V

    .line 680
    .local v1, "mediaInserter":Lcom/samsung/musicplus/provider/MediaInserter;
    const/4 v0, 0x0

    .local v0, "j":I
    :goto_0
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-ge v0, v4, :cond_0

    .line 681
    invoke-interface {v2, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 682
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 683
    .local v3, "values":Landroid/content/ContentValues;
    const-string v4, "audio_id"

    const/4 v5, 0x0

    invoke-interface {v2, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 684
    const-string v4, "play_order"

    const/4 v5, 0x1

    invoke-interface {v2, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 686
    const-string v4, "external"

    invoke-static {v4, p3, p4}, Landroid/provider/MediaStore$Audio$Playlists$Members;->getContentUri(Ljava/lang/String;J)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v1, v4, v3}, Lcom/samsung/musicplus/provider/MediaInserter;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)V

    .line 680
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 691
    .end local v3    # "values":Landroid/content/ContentValues;
    :cond_0
    invoke-virtual {v1}, Lcom/samsung/musicplus/provider/MediaInserter;->flushAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 695
    if-eqz v2, :cond_1

    .line 696
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 697
    const/4 v2, 0x0

    .line 700
    :cond_1
    return-void

    .line 695
    .end local v0    # "j":I
    .end local v1    # "mediaInserter":Lcom/samsung/musicplus/provider/MediaInserter;
    :catchall_0
    move-exception v4

    if-eqz v2, :cond_2

    .line 696
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 697
    const/4 v2, 0x0

    :cond_2
    throw v4
.end method

.method private syncSourceProvider(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;II)V
    .locals 16
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "match"    # I
    .param p6, "syncType"    # I

    .prologue
    .line 2176
    const/4 v14, 0x0

    .line 2177
    .local v14, "srcWhereCol":Ljava/lang/String;
    const/4 v12, 0x0

    .line 2178
    .local v12, "srcUri":Landroid/net/Uri;
    new-instance v1, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v1}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 2179
    .local v1, "qb":Landroid/database/sqlite/SQLiteQueryBuilder;
    const/4 v3, 0x0

    .line 2182
    .local v3, "projection":[Ljava/lang/String;
    sparse-switch p5, :sswitch_data_0

    .line 2205
    new-instance v2, Ljava/lang/UnsupportedOperationException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Invalid URI "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 2185
    :sswitch_0
    const/4 v2, 0x1

    new-array v3, v2, [Ljava/lang/String;

    .end local v3    # "projection":[Ljava/lang/String;
    const/4 v2, 0x0

    const-string v4, "source_id"

    aput-object v4, v3, v2

    .line 2188
    .restart local v3    # "projection":[Ljava/lang/String;
    const-string v14, "_id"

    .line 2189
    sget-object v12, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 2190
    const-string v2, "audio_meta"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 2210
    :goto_0
    const/4 v9, 0x0

    .line 2211
    .local v9, "c":Landroid/database/Cursor;
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    .line 2213
    .local v13, "srcWhere":Ljava/lang/StringBuilder;
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/musicplus/provider/MusicProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    invoke-virtual/range {v1 .. v8}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v9

    .line 2215
    if-nez v9, :cond_1

    .line 2239
    if-eqz v9, :cond_0

    .line 2240
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 2241
    const/4 v9, 0x0

    .line 2258
    :cond_0
    :goto_1
    :pswitch_0
    return-void

    .line 2194
    .end local v9    # "c":Landroid/database/Cursor;
    .end local v13    # "srcWhere":Ljava/lang/StringBuilder;
    :sswitch_1
    const/4 v2, 0x1

    new-array v3, v2, [Ljava/lang/String;

    .end local v3    # "projection":[Ljava/lang/String;
    const/4 v2, 0x0

    const-string v4, "source_id"

    aput-object v4, v3, v2

    .line 2197
    .restart local v3    # "projection":[Ljava/lang/String;
    const-string v14, "_id"

    .line 2198
    sget-object v12, Landroid/provider/MediaStore$Audio$Playlists;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 2200
    const-string v15, "_data like \'%m3u\' or _data like \'%pla\' or _data like \'%wpl\'"

    .line 2201
    .local v15, "syncPlaylistFormat":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " AND ("

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, ")"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    .line 2202
    const-string v2, "audio_playlists"

    invoke-virtual {v1, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_0

    .line 2219
    .end local v15    # "syncPlaylistFormat":Ljava/lang/String;
    .restart local v9    # "c":Landroid/database/Cursor;
    .restart local v13    # "srcWhere":Ljava/lang/StringBuilder;
    :cond_1
    :try_start_1
    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    move-result v10

    .line 2220
    .local v10, "count":I
    if-nez v10, :cond_3

    .line 2221
    if-eqz v9, :cond_2

    .line 2222
    invoke-interface {v9}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2223
    const/4 v9, 0x0

    .line 2239
    :cond_2
    if-eqz v9, :cond_0

    .line 2240
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 2241
    const/4 v9, 0x0

    goto :goto_1

    .line 2228
    :cond_3
    :try_start_2
    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2229
    const-string v2, " IN ("

    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2230
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_2
    if-ge v11, v10, :cond_5

    .line 2231
    invoke-interface {v9, v11}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 2232
    const/4 v2, 0x0

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 2233
    add-int/lit8 v2, v10, -0x1

    if-ge v11, v2, :cond_4

    .line 2234
    const-string v2, ","

    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2230
    :cond_4
    add-int/lit8 v11, v11, 0x1

    goto :goto_2

    .line 2237
    :cond_5
    const-string v2, ")"

    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 2239
    if-eqz v9, :cond_6

    .line 2240
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 2241
    const/4 v9, 0x0

    .line 2246
    :cond_6
    packed-switch p6, :pswitch_data_0

    .line 2256
    new-instance v2, Ljava/lang/UnsupportedOperationException;

    const-string v4, "Invalid SYNC_TYPE"

    invoke-direct {v2, v4}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 2239
    .end local v10    # "count":I
    .end local v11    # "i":I
    :catchall_0
    move-exception v2

    if-eqz v9, :cond_7

    .line 2240
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 2241
    const/4 v9, 0x0

    :cond_7
    throw v2

    .line 2250
    .restart local v10    # "count":I
    .restart local v11    # "i":I
    :pswitch_1
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/provider/MusicProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v2, v12, v4, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    goto/16 :goto_1

    .line 2253
    :pswitch_2
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/provider/MusicProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v2, v12, v0, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto/16 :goto_1

    .line 2182
    nop

    :sswitch_data_0
    .sparse-switch
        0x44c -> :sswitch_0
        0x44d -> :sswitch_0
        0x456 -> :sswitch_1
        0x457 -> :sswitch_1
    .end sparse-switch

    .line 2246
    :pswitch_data_0
    .packed-switch 0x65
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private static updateAdditionalDatabase(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 7
    .param p0, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "fromVersion"    # I
    .param p2, "toVersion"    # I

    .prologue
    const/4 v6, 0x1

    .line 1003
    sget-object v0, Lcom/samsung/musicplus/provider/MusicProvider;->CLASSNAME:Ljava/lang/String;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "updateAdditionalDatabase fromVersion[%d],  toVersion[%d])"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1008
    if-le p1, p2, :cond_0

    .line 1009
    sget-object v0, Lcom/samsung/musicplus/provider/MusicProvider;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Illegal update request: can\'t downgrade from "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". Did you forget to wipe data?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1011
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 1014
    :cond_0
    if-ge p1, v6, :cond_1

    .line 1016
    invoke-static {p0}, Lcom/samsung/musicplus/provider/MusicProvider;->createSearchTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1018
    :cond_1
    return-void
.end method

.method private static updateDatabase(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "fromVersion"    # I
    .param p3, "toVersion"    # I

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    .line 580
    sget-object v0, Lcom/samsung/musicplus/provider/MusicProvider;->CLASSNAME:Ljava/lang/String;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "updateDatabase fromVersion[%d],  toVersion[%d])"

    new-array v3, v7, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 583
    if-le p2, p3, :cond_0

    .line 584
    sget-object v0, Lcom/samsung/musicplus/provider/MusicProvider;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Illegal update request: can\'t downgrade from "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". Did you forget to wipe data?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 586
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 590
    :cond_0
    if-lt p2, v6, :cond_1

    const/16 v0, 0x64

    if-ne p2, v0, :cond_2

    .line 592
    :cond_1
    invoke-static {p1}, Lcom/samsung/musicplus/provider/MusicProvider;->createAlbumArtTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 593
    invoke-static {p1}, Lcom/samsung/musicplus/provider/MusicProvider;->createAudioTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 594
    invoke-static {p1}, Lcom/samsung/musicplus/provider/MusicProvider;->recreateAudioView(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 595
    invoke-static {p1}, Lcom/samsung/musicplus/provider/MusicProvider;->createAudioWithAlbumArtView(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 596
    invoke-static {p1}, Lcom/samsung/musicplus/provider/MusicProvider;->createPlaylistCacheTable(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 599
    const-string v0, "DROP TABLE IF EXISTS dlna_dms_contents_table"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 600
    const-string v0, "CREATE TABLE dlna_dms_contents_table (_id INTEGER PRIMARY KEY, provider_id TEXT, provider_name TEXT, artist TEXT, album TEXT, album_id INTEGER, title TEXT, _data TEXT, album_art TEXT, mime_type TEXT, duration INTEGER, file_size LONG, extension TEXT, seed TEXT, genre_name TEXT);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 603
    const-string v0, "DROP TABLE IF EXISTS dlna_open_intent_table"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 604
    const-string v0, "CREATE TABLE dlna_open_intent_table (_id INTEGER PRIMARY KEY, provider_id TEXT, provider_name TEXT, artist TEXT, album TEXT, album_id INTEGER, title TEXT, _data TEXT, album_art TEXT, mime_type TEXT, duration INTEGER, file_size LONG, extension TEXT, seed TEXT, genre_name TEXT);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 607
    const-string v0, "DROP TABLE IF EXISTS dlna_dms_table"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 608
    const-string v0, "CREATE TABLE dlna_dms_table (_id INTEGER PRIMARY KEY, provider_id TEXT, provider_name TEXT, album_art TEXT, nic_id TEXT);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 611
    const-string v0, "DROP TABLE IF EXISTS dlna_dmr_table"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 612
    const-string v0, "CREATE TABLE dlna_dmr_table (_id INTEGER PRIMARY KEY, avplayer_id TEXT, avplayer_name TEXT, album_art TEXT, nic_id TEXT, is_seekable_on_paused INTEGER,  is_wha INTEGER);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 616
    :cond_2
    if-ge p2, v7, :cond_3

    .line 622
    invoke-static {p0, p1}, Lcom/samsung/musicplus/provider/MusicProvider;->syncPlaylists(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;)V

    .line 623
    invoke-static {p0, p1}, Lcom/samsung/musicplus/provider/MusicProvider;->syncFavoriteList(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;)V

    .line 626
    :cond_3
    const/16 v0, 0x2713

    if-ge p2, v0, :cond_4

    .line 628
    const-string v0, "DROP TABLE IF EXISTS dlna_dmr_table"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 629
    const-string v0, "CREATE TABLE dlna_dmr_table (_id INTEGER PRIMARY KEY, avplayer_id TEXT, avplayer_name TEXT, album_art TEXT, nic_id TEXT, is_seekable_on_paused INTEGER);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 632
    :cond_4
    return-void
.end method

.method private updateInternal(ILandroid/content/ContentValues;)I
    .locals 5
    .param p1, "match"    # I
    .param p2, "initialValues"    # Landroid/content/ContentValues;

    .prologue
    .line 2706
    const/4 v0, 0x0

    .line 2707
    .local v0, "count":I
    packed-switch p1, :pswitch_data_0

    .line 2715
    :goto_0
    return v0

    .line 2709
    :pswitch_0
    iget-object v1, p0, Lcom/samsung/musicplus/provider/MusicProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "audio_meta"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_id = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "_id"

    invoke-virtual {p2, v4}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v1, v2, p2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 2711
    goto :goto_0

    .line 2707
    :pswitch_data_0
    .packed-switch 0x44c
        :pswitch_0
    .end packed-switch
.end method

.method private updateMusicSquareGroupInfo()V
    .locals 15

    .prologue
    const/4 v14, 0x0

    const/4 v4, 0x0

    .line 1687
    sget-object v0, Lcom/samsung/musicplus/provider/MusicProvider;->CLASSNAME:Ljava/lang/String;

    const-string v1, "updateMusicSquareGroupInfo() is called."

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1688
    iget-object v0, p0, Lcom/samsung/musicplus/provider/MusicProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "audio_meta"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "count(*)"

    aput-object v3, v2, v14

    const-string v3, "is_analyzed=\'1\' AND mood_exciting IS NOT NULL AND mood_brightness IS NOT NULL"

    move-object v5, v4

    move-object v6, v4

    move-object v7, v4

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 1693
    .local v11, "c":Landroid/database/Cursor;
    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1694
    invoke-interface {v11, v14}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 1695
    .local v10, "allSong":I
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 1697
    if-nez v10, :cond_0

    .line 1723
    :goto_0
    return-void

    .line 1701
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/provider/MusicProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e0003

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v12

    .line 1703
    .local v12, "column":I
    move v13, v12

    .line 1705
    .local v13, "row":I
    div-int v0, v10, v13

    int-to-float v0, v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v9

    .line 1707
    .local v9, "aRowSongs":I
    div-int v0, v9, v12

    int-to-float v0, v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v8

    .line 1708
    .local v8, "aCellSongs":I
    if-nez v8, :cond_1

    .line 1710
    const/4 v8, 0x1

    .line 1713
    :cond_1
    sget-object v0, Lcom/samsung/musicplus/provider/MusicProvider;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "updateMusicSquareGroupInfoupdateMusicSquareGroupInfo row : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " column : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " aRowSongs : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " aCellSongs : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1718
    invoke-direct {p0, v13, v12, v10}, Lcom/samsung/musicplus/provider/MusicProvider;->insertMoodCellGroupValue(III)V

    .line 1719
    invoke-direct {p0, v13, v12, v10}, Lcom/samsung/musicplus/provider/MusicProvider;->insertYearCellGroupValue(III)V

    .line 1720
    invoke-virtual {p0}, Lcom/samsung/musicplus/provider/MusicProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {}, Lcom/samsung/musicplus/provider/MusicContents;->getMusicSquareCellUpdator()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1722
    sget-object v0, Lcom/samsung/musicplus/provider/MusicProvider;->CLASSNAME:Ljava/lang/String;

    const-string v1, "updateMusicSquareGroupInfo() is ended."

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I
    .locals 7
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # [Landroid/content/ContentValues;

    .prologue
    const/4 v6, 0x0

    .line 1505
    sget-object v3, Lcom/samsung/musicplus/provider/MusicProvider;->CLASSNAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "bulkInsert uri : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1507
    sget-object v3, Lcom/samsung/musicplus/provider/MusicProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    invoke-virtual {v3, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v1

    .line 1508
    .local v1, "match":I
    const/4 v0, 0x0

    .line 1509
    .local v0, "count":I
    sparse-switch v1, :sswitch_data_0

    .line 1535
    invoke-direct {p0, p1, p2, v1}, Lcom/samsung/musicplus/provider/MusicProvider;->bulkInsertInternal(Landroid/net/Uri;[Landroid/content/ContentValues;I)I

    move-result v2

    .line 1536
    .local v2, "numInserted":I
    sget-object v3, Lcom/samsung/musicplus/provider/MusicProvider;->CLASSNAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "bulkInsert : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " items are inserted"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1537
    .end local v2    # "numInserted":I
    :goto_0
    return v2

    .line 1513
    :sswitch_0
    iget-object v3, p0, Lcom/samsung/musicplus/provider/MusicProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    invoke-direct {p0, v3, p1, p2}, Lcom/samsung/musicplus/provider/MusicProvider;->playlistBulkInsert(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;[Landroid/content/ContentValues;)I

    move-result v2

    goto :goto_0

    .line 1516
    :sswitch_1
    invoke-direct {p0}, Lcom/samsung/musicplus/provider/MusicProvider;->savePlaylistTableConfiguration()V

    .line 1518
    const/4 v2, 0x1

    goto :goto_0

    .line 1522
    :sswitch_2
    const/16 v3, 0x44c

    invoke-direct {p0, v3, p2}, Lcom/samsung/musicplus/provider/MusicProvider;->bulkUpdate(I[Landroid/content/ContentValues;)I

    move-result v0

    .line 1523
    invoke-virtual {p0}, Lcom/samsung/musicplus/provider/MusicProvider;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/samsung/musicplus/provider/MusicContents$Audio$Media;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v3, v4, v6}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1525
    sget-object v3, Lcom/samsung/musicplus/provider/MusicProvider;->CLASSNAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "bulkInsert : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " items are updated"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v2, v0

    .line 1526
    goto :goto_0

    .line 1529
    :sswitch_3
    invoke-direct {p0, p2}, Lcom/samsung/musicplus/provider/MusicProvider;->dlnaContentsBulkInsert([Landroid/content/ContentValues;)I

    move-result v0

    .line 1530
    invoke-virtual {p0}, Lcom/samsung/musicplus/provider/MusicProvider;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/samsung/musicplus/provider/MusicContents$Audio$Media;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v3, v4, v6}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    move v2, v0

    .line 1532
    goto :goto_0

    .line 1509
    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_3
        0x457 -> :sswitch_0
        0x458 -> :sswitch_0
        0x459 -> :sswitch_0
        0x463 -> :sswitch_1
        0x473 -> :sswitch_2
    .end sparse-switch
.end method

.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 9
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    const/16 v6, 0x66

    const/4 v8, 0x2

    .line 1844
    sget-object v0, Lcom/samsung/musicplus/provider/MusicProvider;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "delete uri : "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " selection : "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1846
    const/4 v7, 0x0

    .line 1847
    .local v7, "count":I
    sget-object v0, Lcom/samsung/musicplus/provider/MusicProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v5

    .line 1848
    .local v5, "match":I
    sparse-switch v5, :sswitch_data_0

    .line 1927
    :goto_0
    invoke-direct {p0, p1, v5}, Lcom/samsung/musicplus/provider/MusicProvider;->notifyMultipleChanges(Landroid/net/Uri;I)V

    .line 1928
    return v7

    .line 1850
    :sswitch_0
    iget-object v0, p0, Lcom/samsung/musicplus/provider/MusicProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "audio_meta"

    invoke-virtual {v0, v1, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v7

    .line 1852
    goto :goto_0

    .line 1854
    :sswitch_1
    iget-object v0, p0, Lcom/samsung/musicplus/provider/MusicProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "dlna_dms_table"

    invoke-virtual {v0, v1, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v7

    .line 1855
    goto :goto_0

    .line 1857
    :sswitch_2
    iget-object v0, p0, Lcom/samsung/musicplus/provider/MusicProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "dlna_dmr_table"

    invoke-virtual {v0, v1, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v7

    .line 1858
    goto :goto_0

    .line 1860
    :sswitch_3
    iget-object v0, p0, Lcom/samsung/musicplus/provider/MusicProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "dlna_open_intent_table"

    invoke-virtual {v0, v1, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v7

    .line 1861
    goto :goto_0

    .line 1863
    :sswitch_4
    iget-object v0, p0, Lcom/samsung/musicplus/provider/MusicProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "audio_meta"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "media_type=262144 AND "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v7

    .line 1865
    iget-object v0, p0, Lcom/samsung/musicplus/provider/MusicProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "dlna_album_art"

    invoke-virtual {v0, v1, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    add-int/2addr v7, v0

    .line 1866
    iget-object v0, p0, Lcom/samsung/musicplus/provider/MusicProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "dlna_dms_table"

    invoke-virtual {v0, v1, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    add-int/2addr v7, v0

    .line 1867
    iget-object v0, p0, Lcom/samsung/musicplus/provider/MusicProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "dlna_dmr_table"

    invoke-virtual {v0, v1, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    add-int/2addr v7, v0

    .line 1868
    goto :goto_0

    .line 1879
    :sswitch_5
    iget-object v0, p0, Lcom/samsung/musicplus/provider/MusicProvider;->mMoodUpdateHandler:Lcom/samsung/musicplus/provider/MusicProvider$MoodUpdateHandler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/provider/MusicProvider;->mMoodUpdateHandler:Lcom/samsung/musicplus/provider/MusicProvider$MoodUpdateHandler;

    invoke-virtual {v0}, Lcom/samsung/musicplus/provider/MusicProvider$MoodUpdateHandler;->isRunningMoodUpdate()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1880
    iget-object v0, p0, Lcom/samsung/musicplus/provider/MusicProvider;->mMoodUpdateHandler:Lcom/samsung/musicplus/provider/MusicProvider$MoodUpdateHandler;

    invoke-virtual {v0}, Lcom/samsung/musicplus/provider/MusicProvider$MoodUpdateHandler;->cancel()V

    .line 1883
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/provider/MusicProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "audio_meta"

    invoke-virtual {v0, v1, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v7

    .line 1885
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/musicplus/provider/MusicProvider$4;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/provider/MusicProvider$4;-><init>(Lcom/samsung/musicplus/provider/MusicProvider;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    goto/16 :goto_0

    :sswitch_6
    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    .line 1893
    invoke-direct/range {v0 .. v6}, Lcom/samsung/musicplus/provider/MusicProvider;->syncSourceProvider(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;II)V

    .line 1894
    iget-object v0, p0, Lcom/samsung/musicplus/provider/MusicProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "audio_playlists"

    invoke-virtual {v0, v1, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v7

    .line 1895
    goto/16 :goto_0

    .line 1898
    :sswitch_7
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1899
    .local v3, "where":Ljava/lang/String;
    if-eqz p2, :cond_1

    .line 1900
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    :cond_1
    move-object v0, p0

    move-object v1, p1

    move-object v4, p3

    .line 1902
    invoke-direct/range {v0 .. v6}, Lcom/samsung/musicplus/provider/MusicProvider;->syncSourceProvider(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;II)V

    .line 1903
    iget-object v0, p0, Lcom/samsung/musicplus/provider/MusicProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "audio_playlists"

    invoke-virtual {v0, v1, v3, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v7

    .line 1904
    goto/16 :goto_0

    .line 1907
    .end local v3    # "where":Ljava/lang/String;
    :sswitch_8
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "playlist_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v8}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1908
    .restart local v3    # "where":Ljava/lang/String;
    if-eqz p2, :cond_2

    .line 1909
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1911
    :cond_2
    iget-object v0, p0, Lcom/samsung/musicplus/provider/MusicProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "audio_playlists_map"

    invoke-virtual {v0, v1, v3, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v7

    .line 1913
    goto/16 :goto_0

    .line 1915
    .end local v3    # "where":Ljava/lang/String;
    :sswitch_9
    iget-object v0, p0, Lcom/samsung/musicplus/provider/MusicProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "album_art"

    invoke-virtual {v0, v1, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v7

    .line 1916
    goto/16 :goto_0

    .line 1919
    :sswitch_a
    invoke-direct {p0}, Lcom/samsung/musicplus/provider/MusicProvider;->restorePlaylistTableConfiguration()V

    goto/16 :goto_0

    .line 1848
    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x6 -> :sswitch_1
        0x7 -> :sswitch_2
        0x8 -> :sswitch_3
        0xa -> :sswitch_4
        0x44c -> :sswitch_5
        0x456 -> :sswitch_6
        0x457 -> :sswitch_7
        0x458 -> :sswitch_8
        0x45a -> :sswitch_8
        0x460 -> :sswitch_9
        0x463 -> :sswitch_a
    .end sparse-switch
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 1486
    const-string v0, "vnd.android.cursor.dir/audio"

    return-object v0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 6
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 1492
    sget-object v3, Lcom/samsung/musicplus/provider/MusicProvider;->CLASSNAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "insert uri : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1493
    const/4 v1, 0x0

    .line 1494
    .local v1, "newUri":Landroid/net/Uri;
    sget-object v3, Lcom/samsung/musicplus/provider/MusicProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    invoke-virtual {v3, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v0

    .line 1495
    .local v0, "match":I
    invoke-direct {p0, p1, v0, p2}, Lcom/samsung/musicplus/provider/MusicProvider;->insertInternal(Landroid/net/Uri;ILandroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v1

    .line 1496
    const-string v3, "notifyChange"

    invoke-virtual {p1, v3}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1497
    .local v2, "notifyChange":Ljava/lang/String;
    if-nez v1, :cond_0

    const-string v3, "disable"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1498
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/provider/MusicProvider;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, p1, v4}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    .line 1500
    :cond_1
    return-object v1
.end method

.method public onCreate()Z
    .locals 7

    .prologue
    .line 1025
    sget-object v4, Lcom/samsung/musicplus/provider/MusicProvider;->CLASSNAME:Ljava/lang/String;

    const-string v5, "onCreate"

    invoke-static {v4, v5}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1032
    invoke-virtual {p0}, Lcom/samsung/musicplus/provider/MusicProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/musicplus/provider/MusicProvider$MusicDBHelper;->getInstance(Landroid/content/Context;)Lcom/samsung/musicplus/provider/MusicProvider$MusicDBHelper;

    move-result-object v0

    .line 1036
    .local v0, "helper":Lcom/samsung/musicplus/provider/MusicProvider$MusicDBHelper;
    monitor-enter v0

    .line 1037
    :try_start_0
    invoke-virtual {v0}, Lcom/samsung/musicplus/provider/MusicProvider$MusicDBHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/musicplus/provider/MusicProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    .line 1038
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1041
    const-string v4, "content://media/external/audio/media"

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 1042
    .local v3, "notifyUri":Landroid/net/Uri;
    invoke-virtual {p0}, Lcom/samsung/musicplus/provider/MusicProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/samsung/musicplus/provider/MusicProvider;->mSrcProviderObserver:Landroid/database/ContentObserver;

    invoke-virtual {v4, v3, v5, v6}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 1045
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 1046
    .local v1, "iLocaleChangeFilter":Landroid/content/IntentFilter;
    const-string v4, "android.intent.action.LOCALE_CHANGED"

    invoke-virtual {v1, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1047
    invoke-virtual {p0}, Lcom/samsung/musicplus/provider/MusicProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/musicplus/provider/MusicProvider;->mLocaleChangeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v4, v5, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1049
    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    .line 1050
    .local v2, "iShutdownFilter":Landroid/content/IntentFilter;
    const-string v4, "android.intent.action.ACTION_SHUTDOWN"

    invoke-virtual {v2, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 1051
    invoke-virtual {p0}, Lcom/samsung/musicplus/provider/MusicProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/musicplus/provider/MusicProvider;->mShutdownReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v4, v5, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1053
    const/4 v4, 0x1

    return v4

    .line 1038
    .end local v1    # "iLocaleChangeFilter":Landroid/content/IntentFilter;
    .end local v2    # "iShutdownFilter":Landroid/content/IntentFilter;
    .end local v3    # "notifyUri":Landroid/net/Uri;
    :catchall_0
    move-exception v4

    :try_start_1
    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 27
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 1184
    sget-object v4, Lcom/samsung/musicplus/provider/MusicProvider;->CLASSNAME:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "query uri : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1186
    new-instance v8, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v8}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 1187
    .local v8, "qb":Landroid/database/sqlite/SQLiteQueryBuilder;
    const-string v4, "limit"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 1189
    .local v14, "limit":Ljava/lang/String;
    const/16 v20, 0x0

    .line 1190
    .local v20, "groupBy":Ljava/lang/String;
    new-instance v25, Ljava/util/ArrayList;

    invoke-direct/range {v25 .. v25}, Ljava/util/ArrayList;-><init>()V

    .line 1192
    .local v25, "prependArgs":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    sget-object v4, Lcom/samsung/musicplus/provider/MusicProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v26

    .line 1193
    .local v26, "uriMatch":I
    sparse-switch v26, :sswitch_data_0

    .line 1418
    :goto_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/provider/MusicProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    move-object/from16 v2, p4

    invoke-direct {v0, v1, v2}, Lcom/samsung/musicplus/provider/MusicProvider;->combine(Ljava/util/List;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v19

    const/16 v21, 0x0

    move-object v15, v8

    move-object/from16 v17, p2

    move-object/from16 v18, p3

    move-object/from16 v22, p5

    move-object/from16 v23, v14

    invoke-virtual/range {v15 .. v23}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v24

    .line 1421
    .local v24, "c":Landroid/database/Cursor;
    if-eqz v24, :cond_0

    .line 1422
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/provider/MusicProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    move-object/from16 v0, v24

    move-object/from16 v1, p1

    invoke-interface {v0, v4, v1}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    .line 1425
    .end local v8    # "qb":Landroid/database/sqlite/SQLiteQueryBuilder;
    .end local v24    # "c":Landroid/database/Cursor;
    :cond_0
    :goto_1
    return-object v24

    .line 1195
    .restart local v8    # "qb":Landroid/database/sqlite/SQLiteQueryBuilder;
    :sswitch_0
    const-string v4, "audio"

    invoke-virtual {v8, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1196
    const-string v4, "media_type=262144"

    invoke-virtual {v8, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1199
    :sswitch_1
    const-string v4, "audio"

    invoke-virtual {v8, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1200
    const-string v4, "_id=? AND media_type=262144"

    invoke-virtual {v8, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1201
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v4

    const/4 v6, 0x1

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    move-object/from16 v0, v25

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1204
    :sswitch_2
    const-string v4, "dlna_album_art"

    invoke-virtual {v8, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_0

    .line 1207
    :sswitch_3
    const-string v4, "dlna_dms_table"

    invoke-virtual {v8, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_0

    .line 1210
    :sswitch_4
    const-string v4, "dlna_dmr_table"

    invoke-virtual {v8, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_0

    .line 1213
    :sswitch_5
    const-string v4, "dlna_open_intent_table"

    invoke-virtual {v8, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto :goto_0

    .line 1216
    :sswitch_6
    const-string v4, "dlna_open_intent_table"

    invoke-virtual {v8, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1217
    const-string v4, "_id=?"

    invoke-virtual {v8, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1218
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v4

    const/4 v6, 0x1

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    move-object/from16 v0, v25

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1225
    :sswitch_7
    const-string v4, "audio"

    invoke-virtual {v8, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1226
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mood_cell IN ("

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    move/from16 v1, v26

    invoke-direct {v0, v1}, Lcom/samsung/musicplus/provider/MusicProvider;->getCellIds(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, ")"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    .line 1228
    goto/16 :goto_0

    .line 1232
    :sswitch_8
    const-string v4, "audio"

    invoke-virtual {v8, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1237
    :sswitch_9
    const-string v4, "audio"

    invoke-virtual {v8, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1238
    const-string v4, "_id=?"

    invoke-virtual {v8, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1239
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v4

    const/4 v6, 0x2

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    move-object/from16 v0, v25

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1243
    :sswitch_a
    const-string v4, "audio_playlists"

    invoke-virtual {v8, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1244
    const-string v4, "_id IN (SELECT playlist_id FROM audio_playlists_map WHERE audio_id=?)"

    invoke-virtual {v8, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1246
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v4

    const/4 v6, 0x2

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    move-object/from16 v0, v25

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1250
    :sswitch_b
    const-string v4, "audio_playlists"

    invoke-virtual {v8, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1251
    const-string v4, "_id=?"

    invoke-virtual {v8, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1252
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v4

    const/4 v6, 0x4

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    move-object/from16 v0, v25

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1259
    :sswitch_c
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/provider/MusicProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Landroid/provider/MediaStore$Audio$Playlists;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v6, p2

    move-object/from16 v7, p3

    move-object/from16 v8, p4

    move-object/from16 v9, p5

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .end local v8    # "qb":Landroid/database/sqlite/SQLiteQueryBuilder;
    move-result-object v24

    .line 1262
    .restart local v24    # "c":Landroid/database/Cursor;
    goto/16 :goto_1

    .line 1272
    .end local v24    # "c":Landroid/database/Cursor;
    .restart local v8    # "qb":Landroid/database/sqlite/SQLiteQueryBuilder;
    :sswitch_d
    sget-object v4, Landroid/provider/MediaStore$Audio$Playlists;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v4}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v6

    const/4 v7, 0x2

    invoke-interface {v6, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v5

    .line 1275
    .local v5, "mediaUri":Landroid/net/Uri;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/provider/MusicProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    move-object/from16 v6, p2

    move-object/from16 v7, p3

    move-object/from16 v8, p4

    move-object/from16 v9, p5

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .end local v8    # "qb":Landroid/database/sqlite/SQLiteQueryBuilder;
    move-result-object v24

    .line 1277
    .restart local v24    # "c":Landroid/database/Cursor;
    goto/16 :goto_1

    .line 1311
    .end local v5    # "mediaUri":Landroid/net/Uri;
    .end local v24    # "c":Landroid/database/Cursor;
    .restart local v8    # "qb":Landroid/database/sqlite/SQLiteQueryBuilder;
    :sswitch_e
    const-string v6, "external"

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v4

    const/4 v7, 0x2

    invoke-interface {v4, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    invoke-static {v6, v10, v11}, Landroid/provider/MediaStore$Audio$Playlists$Members;->getContentUri(Ljava/lang/String;J)Landroid/net/Uri;

    move-result-object v5

    .line 1313
    .restart local v5    # "mediaUri":Landroid/net/Uri;
    const/16 v4, 0x45a

    move/from16 v0, v26

    if-ne v0, v4, :cond_1

    .line 1314
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "/"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v4

    const/4 v7, 0x4

    invoke-interface {v4, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 1317
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/provider/MusicProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    move-object/from16 v6, p2

    move-object/from16 v7, p3

    move-object/from16 v8, p4

    move-object/from16 v9, p5

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    .end local v8    # "qb":Landroid/database/sqlite/SQLiteQueryBuilder;
    move-result-object v24

    .line 1319
    .restart local v24    # "c":Landroid/database/Cursor;
    goto/16 :goto_1

    .line 1324
    .end local v5    # "mediaUri":Landroid/net/Uri;
    .end local v24    # "c":Landroid/database/Cursor;
    .restart local v8    # "qb":Landroid/database/sqlite/SQLiteQueryBuilder;
    :sswitch_f
    const-string v4, "audio_meta"

    invoke-virtual {v8, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1325
    const-string v20, "artist_group_id"

    .line 1326
    goto/16 :goto_0

    .line 1329
    :sswitch_10
    const-string v4, "audio_meta"

    invoke-virtual {v8, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1330
    const-string v4, "_id=?"

    invoke-virtual {v8, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1331
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v4

    const/4 v6, 0x2

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    move-object/from16 v0, v25

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1332
    const-string v20, "artist_group_id"

    .line 1333
    goto/16 :goto_0

    .line 1359
    :sswitch_11
    const-string v4, "audio_meta"

    invoke-virtual {v8, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1360
    const-string v20, "album_group_id"

    .line 1361
    goto/16 :goto_0

    .line 1364
    :sswitch_12
    const-string v4, "audio_meta"

    invoke-virtual {v8, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1365
    const-string v4, "_id=?"

    invoke-virtual {v8, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->appendWhere(Ljava/lang/CharSequence;)V

    .line 1366
    const-string v20, "album_group_id"

    .line 1367
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v4

    const/4 v6, 0x2

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    move-object/from16 v0, v25

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 1371
    :sswitch_13
    const-string v4, "album_art"

    invoke-virtual {v8, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1375
    :sswitch_14
    const-string v4, "audio_playlists_map_cache"

    invoke-virtual {v8, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 1379
    :sswitch_15
    const-string v4, "(SELECT _id, bucket_id,bucket_display_name,album_id,_data,is_secretbox,media_type,bucket_order FROM audio ORDER BY title_key DESC)"

    invoke-virtual {v8, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1384
    const-string v20, "bucket_id"

    .line 1385
    goto/16 :goto_0

    .line 1397
    :sswitch_16
    sget-boolean v4, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_PINYIN:Z

    if-eqz v4, :cond_2

    .line 1398
    const-string v4, "(SELECT _id, genre_name, genre_name_pinyin, album_id, media_type FROM audio_meta WHERE is_music=1 ORDER BY title_key ASC)"

    invoke-virtual {v8, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1401
    const-string v20, "genre_name_pinyin"

    goto/16 :goto_0

    .line 1403
    :cond_2
    const-string v4, "(SELECT _id, genre_name, album_id, media_type FROM audio_meta WHERE is_music=1 ORDER BY title_key ASC)"

    invoke-virtual {v8, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 1406
    const-string v20, "genre_name"

    .line 1408
    goto/16 :goto_0

    .line 1411
    :sswitch_17
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/musicplus/provider/MusicProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    move-object/from16 v0, p0

    move-object/from16 v1, v25

    move-object/from16 v2, p4

    invoke-direct {v0, v1, v2}, Lcom/samsung/musicplus/provider/MusicProvider;->combine(Ljava/util/List;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    move-object/from16 v6, p0

    move-object/from16 v9, p1

    move-object/from16 v10, p2

    move-object/from16 v11, p3

    move-object/from16 v13, p5

    invoke-direct/range {v6 .. v14}, Lcom/samsung/musicplus/provider/MusicProvider;->doAudioSearch(Landroid/database/sqlite/SQLiteDatabase;Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v24

    goto/16 :goto_1

    .line 1193
    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x5 -> :sswitch_1
        0x6 -> :sswitch_3
        0x7 -> :sswitch_4
        0x8 -> :sswitch_5
        0x9 -> :sswitch_6
        0xb -> :sswitch_2
        0x65 -> :sswitch_7
        0x66 -> :sswitch_7
        0x67 -> :sswitch_7
        0x68 -> :sswitch_7
        0x44c -> :sswitch_8
        0x44d -> :sswitch_9
        0x450 -> :sswitch_a
        0x451 -> :sswitch_b
        0x456 -> :sswitch_c
        0x457 -> :sswitch_d
        0x458 -> :sswitch_e
        0x45a -> :sswitch_e
        0x45b -> :sswitch_f
        0x45c -> :sswitch_10
        0x45d -> :sswitch_11
        0x45e -> :sswitch_12
        0x460 -> :sswitch_13
        0x463 -> :sswitch_14
        0x470 -> :sswitch_15
        0x471 -> :sswitch_16
        0x472 -> :sswitch_17
    .end sparse-switch
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 21
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 1962
    sget-object v4, Lcom/samsung/musicplus/provider/MusicProvider;->CLASSNAME:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "update uri : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " selection : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p3

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1964
    const/16 v16, 0x0

    .line 1966
    .local v16, "count":I
    sget-object v4, Lcom/samsung/musicplus/provider/MusicProvider;->URI_MATCHER:Landroid/content/UriMatcher;

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v9

    .line 1968
    .local v9, "match":I
    sparse-switch v9, :sswitch_data_0

    .line 2030
    :cond_0
    :goto_0
    const-string v4, "notifyChange"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 2031
    .local v19, "notifyChange":Ljava/lang/String;
    const-string v4, "disable"

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 2032
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/provider/MusicProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v4, v0, v5}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    :cond_1
    move/from16 v4, v16

    .line 2035
    .end local v19    # "notifyChange":Ljava/lang/String;
    :goto_1
    return v4

    .line 1970
    :sswitch_0
    invoke-static {}, Lcom/samsung/musicplus/provider/MusicProvider$MoodUpdateHandler;->getInstance()Lcom/samsung/musicplus/provider/MusicProvider$MoodUpdateHandler;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/samsung/musicplus/provider/MusicProvider;->mMoodUpdateHandler:Lcom/samsung/musicplus/provider/MusicProvider$MoodUpdateHandler;

    .line 1971
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/musicplus/provider/MusicProvider;->mMoodUpdateHandler:Lcom/samsung/musicplus/provider/MusicProvider$MoodUpdateHandler;

    move-object/from16 v0, p0

    # invokes: Lcom/samsung/musicplus/provider/MusicProvider$MoodUpdateHandler;->updateMoodValueInBackground(Lcom/samsung/musicplus/provider/MusicProvider;)V
    invoke-static {v4, v0}, Lcom/samsung/musicplus/provider/MusicProvider$MoodUpdateHandler;->access$800(Lcom/samsung/musicplus/provider/MusicProvider$MoodUpdateHandler;Lcom/samsung/musicplus/provider/MusicProvider;)V

    .line 1972
    const/4 v4, 0x1

    goto :goto_1

    .line 1974
    :sswitch_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/musicplus/provider/MusicProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string v5, "audio_meta"

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    move-object/from16 v2, p4

    invoke-virtual {v4, v5, v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v16

    .line 1976
    goto :goto_0

    .line 1978
    :sswitch_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/musicplus/provider/MusicProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string v5, "dlna_dms_table"

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    move-object/from16 v2, p4

    invoke-virtual {v4, v5, v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v16

    .line 1979
    goto :goto_0

    .line 1981
    :sswitch_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/musicplus/provider/MusicProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string v5, "dlna_dmr_table"

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    move-object/from16 v2, p4

    invoke-virtual {v4, v5, v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v16

    .line 1982
    goto :goto_0

    .line 1984
    :sswitch_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/musicplus/provider/MusicProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string v5, "dlna_open_intent_table"

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    move-object/from16 v2, p4

    invoke-virtual {v4, v5, v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v16

    .line 1986
    goto :goto_0

    .line 1988
    :sswitch_5
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/musicplus/provider/MusicProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string v5, "audio_meta"

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    move-object/from16 v2, p4

    invoke-virtual {v4, v5, v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v16

    .line 1989
    goto/16 :goto_0

    .line 1992
    :sswitch_6
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "_id="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v4

    const/4 v6, 0x2

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 1993
    .local v7, "where":Ljava/lang/String;
    if-eqz p3, :cond_2

    .line 1994
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " AND ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p3

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 1996
    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/musicplus/provider/MusicProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string v5, "audio_meta"

    move-object/from16 v0, p2

    move-object/from16 v1, p4

    invoke-virtual {v4, v5, v0, v7, v1}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v16

    .line 1997
    const/16 v10, 0x67

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    move-object/from16 v6, p2

    move-object/from16 v8, p4

    invoke-direct/range {v4 .. v10}, Lcom/samsung/musicplus/provider/MusicProvider;->syncSourceProvider(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;II)V

    goto/16 :goto_0

    .line 2000
    .end local v7    # "where":Ljava/lang/String;
    :sswitch_7
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/musicplus/provider/MusicProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string v5, "audio_playlists"

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    move-object/from16 v2, p4

    invoke-virtual {v4, v5, v0, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v16

    .line 2001
    goto/16 :goto_0

    .line 2004
    :sswitch_8
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "_id="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v4

    const/4 v6, 0x2

    invoke-interface {v4, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 2005
    .restart local v7    # "where":Ljava/lang/String;
    if-eqz p3, :cond_3

    .line 2006
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " AND ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p3

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ")"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 2008
    :cond_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/musicplus/provider/MusicProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    const-string v5, "audio_playlists"

    move-object/from16 v0, p2

    move-object/from16 v1, p4

    invoke-virtual {v4, v5, v0, v7, v1}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v16

    .line 2009
    goto/16 :goto_0

    .line 2012
    .end local v7    # "where":Ljava/lang/String;
    :sswitch_9
    const-string v4, "move"

    move-object/from16 v0, p1

    invoke-virtual {v0, v4}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 2013
    .local v18, "moveit":Ljava/lang/String;
    if-eqz v18, :cond_0

    .line 2014
    const-string v17, "play_order"

    .line 2015
    .local v17, "key":Ljava/lang/String;
    if-eqz p2, :cond_4

    move-object/from16 v0, p2

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 2016
    move-object/from16 v0, p2

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v15

    .line 2017
    .local v15, "newpos":I
    invoke-virtual/range {p1 .. p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v20

    .line 2018
    .local v20, "segments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v4, 0x2

    move-object/from16 v0, v20

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    .line 2019
    .local v12, "playlist":J
    const/4 v4, 0x4

    move-object/from16 v0, v20

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v14

    .line 2020
    .local v14, "oldpos":I
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/musicplus/provider/MusicProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;

    move-object/from16 v10, p0

    invoke-direct/range {v10 .. v15}, Lcom/samsung/musicplus/provider/MusicProvider;->movePlaylistEntry(Landroid/database/sqlite/SQLiteDatabase;JII)I

    move-result v4

    goto/16 :goto_1

    .line 2022
    .end local v12    # "playlist":J
    .end local v14    # "oldpos":I
    .end local v15    # "newpos":I
    .end local v20    # "segments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_4
    new-instance v4, Ljava/lang/IllegalArgumentException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Need to specify "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " when using \'move\' parameter"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 1968
    nop

    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_0
        0x4 -> :sswitch_1
        0x6 -> :sswitch_2
        0x7 -> :sswitch_3
        0x8 -> :sswitch_4
        0x44c -> :sswitch_5
        0x44d -> :sswitch_6
        0x456 -> :sswitch_7
        0x457 -> :sswitch_8
        0x45a -> :sswitch_9
    .end sparse-switch
.end method
