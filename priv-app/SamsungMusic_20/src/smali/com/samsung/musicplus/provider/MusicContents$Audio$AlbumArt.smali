.class public final Lcom/samsung/musicplus/provider/MusicContents$Audio$AlbumArt;
.super Ljava/lang/Object;
.source "MusicContents.java"

# interfaces
.implements Landroid/provider/BaseColumns;
.implements Lcom/samsung/musicplus/provider/MusicContents$Audio$AlbumArtColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/provider/MusicContents$Audio;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AlbumArt"
.end annotation


# static fields
.field public static final CONTENT_URI:Landroid/net/Uri;

.field public static final TABLE_NAME:Ljava/lang/String; = "album_art"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1298
    invoke-static {}, Lcom/samsung/musicplus/provider/MusicContents$Audio$AlbumArt;->getContentUri()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/provider/MusicContents$Audio$AlbumArt;->CONTENT_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1283
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getContentUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 1291
    const-string v0, "content://com.samsung.musicplus/audio/albumart"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method
