.class Lcom/samsung/musicplus/provider/MusicProvider$1;
.super Landroid/database/ContentObserver;
.source "MusicProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/provider/MusicProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/provider/MusicProvider;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/provider/MusicProvider;Landroid/os/Handler;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Handler;

    .prologue
    .line 1073
    iput-object p1, p0, Lcom/samsung/musicplus/provider/MusicProvider$1;->this$0:Lcom/samsung/musicplus/provider/MusicProvider;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method

.method private getMedaDBCount()I
    .locals 9

    .prologue
    .line 1104
    const/4 v6, 0x0

    .line 1105
    .local v6, "c":Landroid/database/Cursor;
    const/4 v7, 0x0

    .line 1107
    .local v7, "count":I
    :try_start_0
    iget-object v0, p0, Lcom/samsung/musicplus/provider/MusicProvider$1;->this$0:Lcom/samsung/musicplus/provider/MusicProvider;

    invoke-virtual {v0}, Lcom/samsung/musicplus/provider/MusicProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "count(*)"

    aput-object v4, v2, v3

    const-string v3, "is_music=1"

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1112
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1113
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v7

    .line 1118
    if-eqz v6, :cond_0

    .line 1119
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1120
    const/4 v6, 0x0

    .line 1123
    :cond_0
    :goto_0
    return v7

    .line 1114
    :catch_0
    move-exception v8

    .line 1115
    .local v8, "e":Ljava/lang/IllegalStateException;
    :try_start_1
    invoke-virtual {v8}, Ljava/lang/IllegalStateException;->printStackTrace()V

    .line 1116
    # getter for: Lcom/samsung/musicplus/provider/MusicProvider;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/provider/MusicProvider;->access$300()Ljava/lang/String;

    move-result-object v0

    const-string v1, "getMediaDBCount failed - just working as 0"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1118
    if-eqz v6, :cond_0

    .line 1119
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1120
    const/4 v6, 0x0

    goto :goto_0

    .line 1118
    .end local v8    # "e":Ljava/lang/IllegalStateException;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_1

    .line 1119
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1120
    const/4 v6, 0x0

    :cond_1
    throw v0
.end method

.method private getMusicDBCount()I
    .locals 10

    .prologue
    .line 1130
    const/4 v8, 0x0

    .line 1133
    .local v8, "c":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/samsung/musicplus/provider/MusicProvider$1;->this$0:Lcom/samsung/musicplus/provider/MusicProvider;

    # getter for: Lcom/samsung/musicplus/provider/MusicProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;
    invoke-static {v0}, Lcom/samsung/musicplus/provider/MusicProvider;->access$400(Lcom/samsung/musicplus/provider/MusicProvider;)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "audio_meta"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "count(*)"

    aput-object v4, v2, v3

    const-string v3, "is_music=1"

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 1137
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1138
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v9

    .line 1140
    .local v9, "count":I
    if-eqz v8, :cond_0

    .line 1141
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 1142
    const/4 v8, 0x0

    .line 1145
    :cond_0
    return v9

    .line 1140
    .end local v9    # "count":I
    :catchall_0
    move-exception v0

    if-eqz v8, :cond_1

    .line 1141
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 1142
    const/4 v8, 0x0

    :cond_1
    throw v0
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 5
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    .line 1076
    invoke-super {p0, p1, p2}, Landroid/database/ContentObserver;->onChange(ZLandroid/net/Uri;)V

    .line 1077
    const/4 v2, 0x0

    .line 1078
    .local v2, "isStartService":Z
    iget-object v3, p0, Lcom/samsung/musicplus/provider/MusicProvider$1;->this$0:Lcom/samsung/musicplus/provider/MusicProvider;

    invoke-virtual {v3}, Lcom/samsung/musicplus/provider/MusicProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 1079
    .local v1, "context":Landroid/content/Context;
    if-eqz v1, :cond_1

    iget-object v3, p0, Lcom/samsung/musicplus/provider/MusicProvider$1;->this$0:Lcom/samsung/musicplus/provider/MusicProvider;

    # invokes: Lcom/samsung/musicplus/provider/MusicProvider;->isActiveMusicService(Landroid/content/Context;)Z
    invoke-static {v3, v1}, Lcom/samsung/musicplus/provider/MusicProvider;->access$200(Lcom/samsung/musicplus/provider/MusicProvider;Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1082
    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, "/external/audio/"

    invoke-virtual {v3, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1083
    const/4 v2, 0x1

    .line 1091
    :cond_0
    :goto_0
    if-eqz v2, :cond_1

    .line 1092
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 1093
    .local v0, "args":Landroid/os/Bundle;
    const-string v3, "scan_type"

    const/4 v4, 0x1

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1094
    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/samsung/musicplus/provider/MusicSyncService;

    invoke-direct {v3, v1, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v3, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 1098
    .end local v0    # "args":Landroid/os/Bundle;
    :cond_1
    return-void

    .line 1087
    :cond_2
    invoke-direct {p0}, Lcom/samsung/musicplus/provider/MusicProvider$1;->getMusicDBCount()I

    move-result v3

    invoke-direct {p0}, Lcom/samsung/musicplus/provider/MusicProvider$1;->getMedaDBCount()I

    move-result v4

    if-eq v3, v4, :cond_0

    .line 1088
    const/4 v2, 0x1

    goto :goto_0
.end method
