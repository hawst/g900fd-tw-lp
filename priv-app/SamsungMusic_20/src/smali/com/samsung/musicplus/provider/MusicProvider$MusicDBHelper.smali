.class Lcom/samsung/musicplus/provider/MusicProvider$MusicDBHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "MusicProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/provider/MusicProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "MusicDBHelper"
.end annotation


# static fields
.field private static sInstance:Lcom/samsung/musicplus/provider/MusicProvider$MusicDBHelper;


# instance fields
.field final mContext:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 546
    const-string v0, "music_player.db"

    const/4 v1, 0x0

    const/16 v2, 0x2714

    invoke-direct {p0, p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 547
    iput-object p1, p0, Lcom/samsung/musicplus/provider/MusicProvider$MusicDBHelper;->mContext:Landroid/content/Context;

    .line 548
    const-string v0, "MusicDBHelper"

    const-string v1, "()"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 549
    return-void
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/samsung/musicplus/provider/MusicProvider$MusicDBHelper;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 539
    const-class v1, Lcom/samsung/musicplus/provider/MusicProvider$MusicDBHelper;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/musicplus/provider/MusicProvider$MusicDBHelper;->sInstance:Lcom/samsung/musicplus/provider/MusicProvider$MusicDBHelper;

    if-nez v0, :cond_0

    .line 540
    new-instance v0, Lcom/samsung/musicplus/provider/MusicProvider$MusicDBHelper;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/provider/MusicProvider$MusicDBHelper;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/musicplus/provider/MusicProvider$MusicDBHelper;->sInstance:Lcom/samsung/musicplus/provider/MusicProvider$MusicDBHelper;

    .line 542
    :cond_0
    sget-object v0, Lcom/samsung/musicplus/provider/MusicProvider$MusicDBHelper;->sInstance:Lcom/samsung/musicplus/provider/MusicProvider$MusicDBHelper;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 539
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 4
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    const/16 v3, 0x2714

    const/4 v2, 0x0

    .line 554
    const-string v0, "MusicDBHelper"

    const-string v1, "onCreate"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 556
    iget-object v0, p0, Lcom/samsung/musicplus/provider/MusicProvider$MusicDBHelper;->mContext:Landroid/content/Context;

    # invokes: Lcom/samsung/musicplus/provider/MusicProvider;->updateDatabase(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;II)V
    invoke-static {v0, p1, v2, v3}, Lcom/samsung/musicplus/provider/MusicProvider;->access$000(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;II)V

    .line 557
    # invokes: Lcom/samsung/musicplus/provider/MusicProvider;->updateAdditionalDatabase(Landroid/database/sqlite/SQLiteDatabase;II)V
    invoke-static {p1, v2, v3}, Lcom/samsung/musicplus/provider/MusicProvider;->access$100(Landroid/database/sqlite/SQLiteDatabase;II)V

    .line 558
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 563
    const-string v0, "MusicDBHelper"

    const-string v1, "onUpgrade"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 565
    iget-object v0, p0, Lcom/samsung/musicplus/provider/MusicProvider$MusicDBHelper;->mContext:Landroid/content/Context;

    # invokes: Lcom/samsung/musicplus/provider/MusicProvider;->updateDatabase(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;II)V
    invoke-static {v0, p1, p2, p3}, Lcom/samsung/musicplus/provider/MusicProvider;->access$000(Landroid/content/Context;Landroid/database/sqlite/SQLiteDatabase;II)V

    .line 566
    # invokes: Lcom/samsung/musicplus/provider/MusicProvider;->updateAdditionalDatabase(Landroid/database/sqlite/SQLiteDatabase;II)V
    invoke-static {p1, p2, p3}, Lcom/samsung/musicplus/provider/MusicProvider;->access$100(Landroid/database/sqlite/SQLiteDatabase;II)V

    .line 567
    return-void
.end method
