.class public interface abstract Lcom/samsung/musicplus/provider/MusicContents$Audio$AlbumColumns;
.super Ljava/lang/Object;
.source "MusicContents.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/provider/MusicContents$Audio;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "AlbumColumns"
.end annotation


# static fields
.field public static final ALBUM:Ljava/lang/String; = "album"

.field public static final ALBUM_ART:Ljava/lang/String; = "album_art"

.field public static final ALBUM_ID:Ljava/lang/String; = "album_id"

.field public static final ALBUM_KEY:Ljava/lang/String; = "album_key"

.field public static final ARTIST:Ljava/lang/String; = "artist"

.field public static final NUMBER_OF_SONGS:Ljava/lang/String; = "numsongs"

.field public static final NUMBER_OF_SONGS_FOR_ARTIST:Ljava/lang/String; = "numsongs_by_artist"
