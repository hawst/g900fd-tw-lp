.class public final Lcom/samsung/musicplus/provider/MusicContents$Audio$DeviceContents;
.super Ljava/lang/Object;
.source "MusicContents.java"

# interfaces
.implements Lcom/samsung/musicplus/account/SamsungAccountConstant;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/provider/MusicContents$Audio;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "DeviceContents"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/provider/MusicContents$Audio$DeviceContents$TRANSFER_TYPE;
    }
.end annotation


# static fields
.field public static ACTION_S_LINK_INITIALIZE_CHANGED:Ljava/lang/String; = null

.field public static ACTION_S_LINK_SIGNIN_CHANGED:Ljava/lang/String; = null

.field public static final ALBUM:Ljava/lang/String; = "album"

.field public static final ALBUM_ID:Ljava/lang/String; = "album_id"

.field public static final ARTIST:Ljava/lang/String; = "artist"

.field public static final CONTENT_URI:Landroid/net/Uri;

.field public static final CONTENT_URI_STRING:Ljava/lang/String;

.field public static final DATA:Ljava/lang/String; = "_data"

.field public static final DEVICE_ID:Ljava/lang/String; = "device_id"

.field public static final DUMMY_ALBUM_URI_STRING:Ljava/lang/String;

.field public static final DURATION:Ljava/lang/String; = "duration"

.field private static final HTTP:Ljava/lang/String; = "http"

.field public static final INDEX_CHAR:Ljava/lang/String; = "index_char"

.field public static final LOCAL_ID:Ljava/lang/String; = "local_source_media_id"

.field public static final MIME_TYPE:Ljava/lang/String; = "mime_type"

.field public static final REQ_CODE_SEND_TO_OTHER_DEVICE:I = 0x1

.field private static final SLHTTP:Ljava/lang/String; = "slhttp"

.field private static final SUPPORT_SAME_AP:Z = true

.field private static final SUPPORT_SCS:Z = true

.field public static final TITLE:Ljava/lang/String; = "title"

.field public static final _ID:Ljava/lang/String; = "_id"

.field private static sWakeLock:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 1685
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Audio$Media;->CONTENT_URI:Landroid/net/Uri;

    sput-object v0, Lcom/samsung/musicplus/provider/MusicContents$Audio$DeviceContents;->CONTENT_URI:Landroid/net/Uri;

    .line 1687
    sget-object v0, Lcom/samsung/musicplus/provider/MusicContents$Audio$DeviceContents;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/provider/MusicContents$Audio$DeviceContents;->CONTENT_URI_STRING:Ljava/lang/String;

    .line 1693
    sget-object v0, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Audio$Media;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/provider/MusicContents$Audio$DeviceContents;->DUMMY_ALBUM_URI_STRING:Ljava/lang/String;

    .line 2026
    const-string v0, "com.samsung.android.sdk.samsunglink.SlinkNetworkManager.BROADCAST_INITIALIZING_STATE_CHANGED"

    sput-object v0, Lcom/samsung/musicplus/provider/MusicContents$Audio$DeviceContents;->ACTION_S_LINK_INITIALIZE_CHANGED:Ljava/lang/String;

    .line 2028
    const-string v0, "com.samsung.android.sdk.samsunglink.SlinkSignInUtils.BROADCAST_SIGNIN_STATE_CHANGED"

    sput-object v0, Lcom/samsung/musicplus/provider/MusicContents$Audio$DeviceContents;->ACTION_S_LINK_SIGNIN_CHANGED:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1662
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1974
    return-void
.end method

.method public static acquireWakeLock(Landroid/content/Context;)V
    .locals 0
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2057
    return-void
.end method

.method public static delete(Landroid/content/Context;[J)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "ids"    # [J

    .prologue
    .line 1938
    move-object v0, p1

    .local v0, "arr$":[J
    array-length v4, v0

    .local v4, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v4, :cond_0

    aget-wide v2, v0, v1

    .line 1939
    .local v2, "id":J
    invoke-static {p0, v2, v3}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Files;->deleteFile(Landroid/content/Context;J)V

    .line 1938
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1941
    .end local v2    # "id":J
    :cond_0
    return-void
.end method

.method public static download(Landroid/content/Context;[J)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "ids"    # [J

    .prologue
    .line 1923
    invoke-static {p0}, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;

    move-result-object v1

    .line 1924
    .local v1, "transfer":Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;
    new-instance v0, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;

    invoke-direct {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;-><init>()V

    .line 1926
    .local v0, "options":Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;
    sget-object v2, Landroid/os/Environment;->DIRECTORY_MUSIC:Ljava/lang/String;

    invoke-static {v2}, Landroid/os/Environment;->getExternalStoragePublicDirectory(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    iput-object v2, v0, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;->targetDirectory:Ljava/io/File;

    .line 1928
    invoke-static {p1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->createFromSlinkMediaStoreIds([J)Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->downloadFiles(Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)V

    .line 1929
    return-void
.end method

.method public static getAlbumArt(Landroid/content/Context;JJI)Landroid/graphics/Bitmap;
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "albumId"    # J
    .param p3, "threadId"    # J
    .param p5, "size"    # I

    .prologue
    .line 1910
    new-instance v10, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v10}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 1911
    .local v10, "options":Landroid/graphics/BitmapFactory$Options;
    sget-object v0, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    iput-object v0, v10, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 1912
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-wide v2, p1

    move-wide v4, p3

    move/from16 v6, p5

    move/from16 v7, p5

    invoke-static/range {v1 .. v10}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Audio$AlbumArt;->getThumbnail(Landroid/content/ContentResolver;JJIIZZLandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public static final getContentUri(J)Landroid/net/Uri;
    .locals 2
    .param p0, "id"    # J

    .prologue
    .line 1697
    invoke-static {p0, p1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Audio$Media;->getEntryUri(J)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static getCursor(Landroid/content/Context;Landroid/content/Intent;)Landroid/database/Cursor;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "i"    # Landroid/content/Intent;

    .prologue
    .line 2000
    invoke-static {p0}, Lcom/samsung/android/sdk/samsunglink/SlinkViewerUtils;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkViewerUtils;

    move-result-object v0

    .line 2001
    .local v0, "viewer":Lcom/samsung/android/sdk/samsunglink/SlinkViewerUtils;
    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/samsunglink/SlinkViewerUtils;->getCursorFromViewIntent(Landroid/content/Intent;)Landroid/database/Cursor;

    move-result-object v1

    return-object v1
.end method

.method private static final getMatchingUri(Landroid/content/Context;JLcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;Z)Landroid/net/Uri;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "id"    # J
    .param p3, "info"    # Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;
    .param p4, "isDmr"    # Z

    .prologue
    .line 1756
    const-string v6, "MusicPlayer"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getMatchingUri id "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " isDmr: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1757
    const/4 v0, 0x0

    .line 1758
    .local v0, "bestConnection":Landroid/net/Uri;
    const/4 v4, 0x0

    .local v4, "retryCount":I
    :goto_0
    const/4 v6, 0x3

    if-ge v4, v6, :cond_6

    if-nez v0, :cond_6

    .line 1762
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    invoke-static {v6, p1, p2}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Audio$Media;->getAudioUriBatch(Landroid/content/ContentResolver;J)Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Audio$Media$AudioUriBatch;

    move-result-object v5

    .line 1764
    .local v5, "uris":Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Audio$Media$AudioUriBatch;
    if-nez v5, :cond_0

    .line 1765
    const-string v6, "MusicPlayer"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Failed to retrieve URIs for "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1766
    const/4 v6, 0x0

    .line 1817
    .end local v5    # "uris":Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Audio$Media$AudioUriBatch;
    :goto_1
    return-object v6

    .line 1769
    .restart local v5    # "uris":Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Audio$Media$AudioUriBatch;
    :cond_0
    invoke-virtual {v5}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Audio$Media$AudioUriBatch;->getLocalUri()Landroid/net/Uri;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 1770
    invoke-virtual {v5}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Audio$Media$AudioUriBatch;->getLocalUri()Landroid/net/Uri;

    move-result-object v0

    .line 1771
    const-string v6, "MusicPlayer"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, " via matched local file : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1758
    :goto_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 1772
    :cond_1
    invoke-virtual {v5}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Audio$Media$AudioUriBatch;->getSameAccessPointUri()Landroid/net/Uri;

    move-result-object v6

    if-eqz v6, :cond_2

    .line 1773
    invoke-virtual {v5}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Audio$Media$AudioUriBatch;->getSameAccessPointUri()Landroid/net/Uri;

    move-result-object v0

    .line 1774
    const-string v6, "MusicPlayer"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, " via Same AP direct connection : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 1775
    :cond_2
    if-eqz p3, :cond_4

    if-nez p4, :cond_4

    invoke-virtual {v5}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Audio$Media$AudioUriBatch;->getScsUri()Landroid/net/Uri;

    move-result-object v6

    if-eqz v6, :cond_4

    .line 1778
    invoke-virtual {v5}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Audio$Media$AudioUriBatch;->getScsUri()Landroid/net/Uri;

    move-result-object v0

    .line 1779
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1780
    .local v3, "path":Ljava/lang/String;
    invoke-static {p0}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;->getScsCoreConfig()Lcom/samsung/android/sdk/samsunglink/network/SlinkScsCoreConfig;

    move-result-object v1

    .line 1782
    .local v1, "config":Lcom/samsung/android/sdk/samsunglink/network/SlinkScsCoreConfig;
    if-nez v1, :cond_3

    .line 1783
    invoke-virtual {v5}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Audio$Media$AudioUriBatch;->getHttpProxyUri()Landroid/net/Uri;

    move-result-object v0

    .line 1784
    const-string v6, "MusicPlayer"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, " via Samsung Link proxy : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 1786
    :cond_3
    invoke-static {p0, v3, p3, v1}, Lcom/samsung/musicplus/provider/MusicContents$Audio$DeviceContents;->getScsConfigPath(Landroid/content/Context;Ljava/lang/String;Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;Lcom/samsung/android/sdk/samsunglink/network/SlinkScsCoreConfig;)Ljava/lang/String;

    move-result-object v3

    .line 1787
    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1788
    const-string v6, "MusicPlayer"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, " via SCS connection : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 1790
    .end local v1    # "config":Lcom/samsung/android/sdk/samsunglink/network/SlinkScsCoreConfig;
    .end local v3    # "path":Ljava/lang/String;
    :cond_4
    invoke-virtual {v5}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Audio$Media$AudioUriBatch;->getHttpProxyUri()Landroid/net/Uri;

    move-result-object v6

    if-eqz v6, :cond_5

    .line 1793
    invoke-virtual {v5}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Audio$Media$AudioUriBatch;->getHttpProxyUri()Landroid/net/Uri;

    move-result-object v0

    .line 1794
    const-string v6, "MusicPlayer"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, " via Samsung Link proxy : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 1796
    :cond_5
    const-string v6, "MusicPlayer"

    const-string v7, " there no bestConnection uri, try again 2 sec later "

    invoke-static {v6, v7}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1805
    const-wide/16 v6, 0x7d0

    :try_start_0
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_2

    .line 1806
    :catch_0
    move-exception v2

    .line 1807
    .local v2, "e":Ljava/lang/InterruptedException;
    const-string v6, "MusicPlayer"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, " InterruptedException during thread sleep : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v2}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 1812
    .end local v2    # "e":Ljava/lang/InterruptedException;
    .end local v5    # "uris":Lcom/samsung/android/sdk/samsunglink/SlinkMediaStore$Audio$Media$AudioUriBatch;
    :cond_6
    if-eqz v0, :cond_7

    if-nez p4, :cond_7

    const-string v6, "http"

    invoke-virtual {v0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_7

    .line 1815
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v6

    const-string v7, "slhttp"

    invoke-virtual {v6, v7}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    :cond_7
    move-object v6, v0

    .line 1817
    goto/16 :goto_1
.end method

.method private static getMediaSet(I[J)Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;
    .locals 1
    .param p0, "type"    # I
    .param p1, "items"    # [J

    .prologue
    .line 1981
    packed-switch p0, :pswitch_data_0

    .line 1987
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 1983
    :pswitch_0
    invoke-static {p1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->createFromMediaStoreIds([J)Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    move-result-object v0

    goto :goto_0

    .line 1985
    :pswitch_1
    invoke-static {p1}, Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;->createFromSlinkMediaStoreIds([J)Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    move-result-object v0

    goto :goto_0

    .line 1981
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static final getPlayingUri(Landroid/content/Context;JLcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;Z)Ljava/lang/String;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "id"    # J
    .param p3, "info"    # Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;
    .param p4, "isDmr"    # Z

    .prologue
    .line 1712
    invoke-static {p0, p1, p2, p3, p4}, Lcom/samsung/musicplus/provider/MusicContents$Audio$DeviceContents;->getMatchingUri(Landroid/content/Context;JLcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;Z)Landroid/net/Uri;

    move-result-object v0

    .line 1713
    .local v0, "uri":Landroid/net/Uri;
    if-eqz v0, :cond_0

    .line 1714
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1716
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getScsConfigPath(Landroid/content/Context;Ljava/lang/String;Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;)Ljava/lang/String;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "info"    # Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;

    .prologue
    .line 1830
    invoke-static {p0}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;->getScsCoreConfig()Lcom/samsung/android/sdk/samsunglink/network/SlinkScsCoreConfig;

    move-result-object v0

    .line 1832
    .local v0, "config":Lcom/samsung/android/sdk/samsunglink/network/SlinkScsCoreConfig;
    invoke-static {p0, p1, p2, v0}, Lcom/samsung/musicplus/provider/MusicContents$Audio$DeviceContents;->getScsConfigPath(Landroid/content/Context;Ljava/lang/String;Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;Lcom/samsung/android/sdk/samsunglink/network/SlinkScsCoreConfig;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private static getScsConfigPath(Landroid/content/Context;Ljava/lang/String;Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;Lcom/samsung/android/sdk/samsunglink/network/SlinkScsCoreConfig;)Ljava/lang/String;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "info"    # Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;
    .param p3, "config"    # Lcom/samsung/android/sdk/samsunglink/network/SlinkScsCoreConfig;

    .prologue
    const/4 v2, 0x0

    .line 1846
    if-nez p2, :cond_0

    .line 1847
    const-string v1, "MusicPlayer"

    const-string v2, "Account info is null"

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1897
    .end local p1    # "path":Ljava/lang/String;
    :goto_0
    return-object p1

    .line 1850
    .restart local p1    # "path":Ljava/lang/String;
    :cond_0
    if-nez p3, :cond_1

    .line 1851
    const-string v1, "MusicPlayer"

    const-string v2, "SlinkScsCoreConfig is null"

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1855
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 1857
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string v1, "?GroupId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1858
    invoke-virtual {p3}, Lcom/samsung/android/sdk/samsunglink/network/SlinkScsCoreConfig;->getGroupId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1859
    const-string v1, "?PeerId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1860
    invoke-virtual {p3}, Lcom/samsung/android/sdk/samsunglink/network/SlinkScsCoreConfig;->getPeerId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1861
    const-string v1, "?InstanceID="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1862
    invoke-virtual {p3}, Lcom/samsung/android/sdk/samsunglink/network/SlinkScsCoreConfig;->getInstanceId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1865
    const-string v1, "?AppId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1866
    const-string v1, "f05tf070ze"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1867
    const-string v1, "?AuthType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1868
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1869
    const-string v1, "?ServicePort="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1870
    const-string v1, "1007"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1871
    const-string v1, "?ProxyPortInstance="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1872
    const-string v1, "9050"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1873
    const-string v1, "?Plain_UDP="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1874
    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1876
    const-string v1, "?fwk_target="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1877
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1878
    const-string v1, "?server_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1879
    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1882
    const-string v1, "?token="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1883
    iget-object v1, p2, Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;->accessToken:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1884
    const-string v1, "?user_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1885
    iget-object v1, p2, Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;->userId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1886
    const-string v1, "?mcc="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1887
    iget-object v1, p2, Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;->mcc:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1888
    const-string v1, "?cc="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1889
    iget-object v1, p2, Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;->cc:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1893
    const-string v1, "?login_id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1894
    iget-object v1, p2, Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;->loginId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1895
    const-string v1, "?login_id_type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1896
    iget-object v1, p2, Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;->loginType:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1897
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto/16 :goto_0
.end method

.method private static isDataEnabled(Landroid/content/Context;)Z
    .locals 10
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2080
    const-string v7, "connectivity"

    invoke-virtual {p0, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 2082
    .local v0, "cm":Landroid/net/ConnectivityManager;
    invoke-virtual {v0, v6}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v4

    .line 2083
    .local v4, "wifi":Landroid/net/NetworkInfo;
    invoke-virtual {v0, v5}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v3

    .line 2085
    .local v3, "mobile":Landroid/net/NetworkInfo;
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 2086
    .local v2, "isWifiConnected":Ljava/lang/Boolean;
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 2087
    .local v1, "isMobileConnected":Ljava/lang/Boolean;
    if-eqz v4, :cond_0

    .line 2088
    const-string v7, "MusicPlayer"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "isDataEnabled  wifi : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2089
    invoke-virtual {v4}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v7

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 2091
    :cond_0
    if-eqz v3, :cond_1

    .line 2092
    const-string v7, "MusicPlayer"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "isDataEnabled  mobile : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2093
    invoke-virtual {v3}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v7

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 2095
    :cond_1
    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    if-nez v7, :cond_2

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v7

    if-eqz v7, :cond_3

    :cond_2
    move v5, v6

    :cond_3
    return v5
.end method

.method public static isNetworkInitialized(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2031
    invoke-static {p0}, Lcom/samsung/musicplus/provider/MusicContents$Audio$Devices;->isSupportSlink(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2032
    const-string v1, "MusicPlayer"

    const-string v2, "not support S link do not check networkInitialized"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2033
    const/4 v0, 0x0

    .line 2037
    :goto_0
    return v0

    .line 2035
    :cond_0
    invoke-static {p0}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;->isInitialized()Z

    move-result v0

    .line 2036
    .local v0, "isInitialized":Z
    const-string v1, "MusicDevice"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "is S link NetworkInitialized ? "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private static isSameApConnection(Ljava/lang/String;)Z
    .locals 1
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 1736
    if-eqz p0, :cond_1

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "SLHTTP://"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "slhttp://"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1738
    :cond_0
    const/4 v0, 0x1

    .line 1740
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isScsConnection(Ljava/lang/String;)Z
    .locals 1
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    .line 1728
    if-eqz p0, :cond_1

    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "NTCL://"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "ntcl://"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1730
    :cond_0
    const/4 v0, 0x1

    .line 1732
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isScsOrSameApConnection(Ljava/lang/String;)Z
    .locals 3
    .param p0, "path"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 1744
    if-nez p0, :cond_1

    .line 1745
    const-string v1, "MusicPlayer"

    const-string v2, "isScsOrSameApConnection path is null"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1751
    :cond_0
    :goto_0
    return v0

    .line 1748
    :cond_1
    invoke-static {p0}, Lcom/samsung/musicplus/provider/MusicContents$Audio$DeviceContents;->isScsConnection(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-static {p0}, Lcom/samsung/musicplus/provider/MusicContents$Audio$DeviceContents;->isSameApConnection(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1749
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static loadMusicContentsFrist(Landroid/content/Context;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 2010
    invoke-static {p0}, Lcom/samsung/musicplus/provider/MusicContents$Audio$Devices;->isSupportSlink(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2011
    const-string v1, "MusicDevice"

    const-string v2, "setMusicContentsFrist but not support S link, return here"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2022
    :goto_0
    return-void

    .line 2016
    :cond_0
    :try_start_0
    invoke-static {p0}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager;->setSyncMediaPriority(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2018
    :catch_0
    move-exception v0

    .line 2019
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "MusicDevice"

    const-string v2, "Samsung link patform make below error during set priority"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 2020
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public static releaseWakeLock()V
    .locals 2

    .prologue
    .line 2099
    sget-object v0, Lcom/samsung/musicplus/provider/MusicContents$Audio$DeviceContents;->sWakeLock:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/samsung/musicplus/provider/MusicContents$Audio$DeviceContents;->sWakeLock:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2100
    sget-object v0, Lcom/samsung/musicplus/provider/MusicContents$Audio$DeviceContents;->sWakeLock:Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/samsunglink/SlinkNetworkManager$SlinkWakeLock;->release()V

    .line 2101
    const-string v0, "MusicPlayer"

    const-string v1, "S link network lock released"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2103
    :cond_0
    return-void
.end method

.method public static startDeviceSelectActivityForResult(Landroid/app/Activity;I[JI)V
    .locals 6
    .param p0, "a"    # Landroid/app/Activity;
    .param p1, "type"    # I
    .param p2, "items"    # [J
    .param p3, "requestCode"    # I

    .prologue
    .line 1960
    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;

    move-result-object v2

    .line 1963
    .local v2, "transfer":Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;
    invoke-static {p1, p2}, Lcom/samsung/musicplus/provider/MusicContents$Audio$DeviceContents;->getMediaSet(I[J)Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;

    move-result-object v3

    new-instance v4, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;

    invoke-direct {v4}, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;-><init>()V

    invoke-virtual {v2, v3, v4}, Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils;->createSendToActivityIntent(Lcom/samsung/android/sdk/samsunglink/SlinkMediaSet;Lcom/samsung/android/sdk/samsunglink/SlinkFileTransferUtils$TransferOptions;)Landroid/content/Intent;

    move-result-object v1

    .line 1966
    .local v1, "i":Landroid/content/Intent;
    :try_start_0
    invoke-virtual {p0, v1, p3}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1972
    :goto_0
    return-void

    .line 1967
    :catch_0
    move-exception v0

    .line 1968
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v3, "MusicDevice"

    const-string v4, "Activity Not found!!!"

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1969
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    .line 1970
    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "Target Activity Not Found"

    const/4 v5, 0x1

    invoke-static {v3, v4, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
