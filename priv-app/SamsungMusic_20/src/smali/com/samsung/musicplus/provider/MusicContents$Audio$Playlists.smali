.class public final Lcom/samsung/musicplus/provider/MusicContents$Audio$Playlists;
.super Ljava/lang/Object;
.source "MusicContents.java"

# interfaces
.implements Landroid/provider/BaseColumns;
.implements Lcom/samsung/musicplus/provider/MusicContents$Audio$PlaylistsColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/provider/MusicContents$Audio;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Playlists"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/provider/MusicContents$Audio$Playlists$Members;
    }
.end annotation


# static fields
.field public static final CONTENT_TYPE:Ljava/lang/String; = "vnd.android.cursor.dir/playlist"

.field public static final CONTENT_URI:Landroid/net/Uri;

.field public static final DEFAULT_SORT_ORDER:Ljava/lang/String; = "name"

.field public static final ENTRY_CONTENT_TYPE:Ljava/lang/String; = "vnd.android.cursor.item/playlist"

.field public static final TABLE_NAME:Ljava/lang/String; = "audio_playlists"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 994
    invoke-static {}, Lcom/samsung/musicplus/provider/MusicContents$Audio$Playlists;->getContentUri()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/provider/MusicContents$Audio$Playlists;->CONTENT_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 979
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1019
    return-void
.end method

.method public static getContentUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 987
    const-string v0, "content://com.samsung.musicplus/audio/playlists"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method
