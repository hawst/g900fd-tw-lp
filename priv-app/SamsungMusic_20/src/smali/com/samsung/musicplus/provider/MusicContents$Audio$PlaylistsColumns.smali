.class public interface abstract Lcom/samsung/musicplus/provider/MusicContents$Audio$PlaylistsColumns;
.super Ljava/lang/Object;
.source "MusicContents.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/provider/MusicContents$Audio;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "PlaylistsColumns"
.end annotation


# static fields
.field public static final CLOUD_ID:Ljava/lang/String; = "cloud_id"

.field public static final DATA:Ljava/lang/String; = "_data"

.field public static final DATE_ADDED:Ljava/lang/String; = "date_added"

.field public static final DATE_MODIFIED:Ljava/lang/String; = "date_modified"

.field public static final NAME:Ljava/lang/String; = "name"

.field public static final SOURCE_ID:Ljava/lang/String; = "source_id"
