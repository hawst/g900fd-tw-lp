.class public Lcom/samsung/musicplus/provider/VolumeStateCompat;
.super Ljava/lang/Object;
.source "VolumeStateCompat.java"


# static fields
.field public static final EXTERNAL_STORAGE_SD_PATH:Ljava/lang/String; = "/storage/extSdCard"

.field private static final TAG:Ljava/lang/String; = "MusicSyncService"

.field private static final sGetVolumeStateMethod:Ljava/lang/reflect/Method;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    invoke-static {}, Lcom/samsung/musicplus/provider/VolumeStateCompat;->findGetVolumeStateMethod()Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/provider/VolumeStateCompat;->sGetVolumeStateMethod:Ljava/lang/reflect/Method;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static findGetVolumeStateMethod()Ljava/lang/reflect/Method;
    .locals 6

    .prologue
    .line 43
    :try_start_0
    const-class v0, Landroid/os/storage/StorageManager;

    .line 44
    .local v0, "cls":Ljava/lang/Class;, "Ljava/lang/Class<Landroid/os/storage/StorageManager;>;"
    const-string v2, "getVolumeState"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {v0, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 48
    :goto_0
    return-object v2

    .line 45
    :catch_0
    move-exception v1

    .line 46
    .local v1, "e":Ljava/lang/NoSuchMethodException;
    const-string v2, "MusicSyncService"

    const-string v3, "getVolumeState() Method not found."

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private static getSDCardVolumeState(Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 66
    const-string v1, "storage"

    invoke-virtual {p0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/storage/StorageManager;

    .line 67
    .local v0, "sm":Landroid/os/storage/StorageManager;
    sget-boolean v1, Lcom/samsung/musicplus/util/Version;->LIBRARY_OVER_API_1:Z

    if-eqz v1, :cond_0

    .line 68
    const-string v1, "/storage/extSdCard"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/storage/StorageManagerCompat;->getVolumeState(Landroid/os/storage/StorageManager;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 70
    :goto_0
    return-object v1

    :cond_0
    const-string v1, "/storage/extSdCard"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/provider/VolumeStateCompat;->getVolumeState(Landroid/os/storage/StorageManager;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private static getVolumeState(Landroid/os/storage/StorageManager;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "sm"    # Landroid/os/storage/StorageManager;
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 52
    sget-object v1, Lcom/samsung/musicplus/provider/VolumeStateCompat;->sGetVolumeStateMethod:Ljava/lang/reflect/Method;

    if-eqz v1, :cond_0

    .line 54
    :try_start_0
    sget-object v1, Lcom/samsung/musicplus/provider/VolumeStateCompat;->sGetVolumeStateMethod:Ljava/lang/reflect/Method;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v1, p0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    .line 62
    :goto_0
    return-object v1

    .line 55
    :catch_0
    move-exception v0

    .line 56
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    const-string v1, "MusicSyncService"

    const-string v2, "getVolumeState() InvocationTargetException() occured."

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    .end local v0    # "e":Ljava/lang/reflect/InvocationTargetException;
    :cond_0
    :goto_1
    const-string v1, "unmounted"

    goto :goto_0

    .line 57
    :catch_1
    move-exception v0

    .line 58
    .local v0, "e":Ljava/lang/IllegalAccessException;
    const-string v1, "MusicSyncService"

    const-string v2, "getVolumeState() IllegalAccessException() occured."

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static isMountedSDCard(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 81
    invoke-static {p0}, Lcom/samsung/musicplus/provider/VolumeStateCompat;->getSDCardVolumeState(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 82
    .local v0, "state":Ljava/lang/String;
    const-string v2, "MusicSyncService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SD Card Mounted State = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    const-string v2, "bad_removal"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "nofs"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "removed"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "unknown"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "unmounted"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 94
    :cond_0
    :goto_0
    return v1

    .line 89
    :cond_1
    const-string v2, "mounted"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "mounted_ro"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 91
    :cond_2
    const/4 v1, 0x1

    goto :goto_0
.end method
