.class public Lcom/samsung/musicplus/provider/MusicContents;
.super Ljava/lang/Object;
.source "MusicContents.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/provider/MusicContents$Audio;,
        Lcom/samsung/musicplus/provider/MusicContents$MediaColumns;,
        Lcom/samsung/musicplus/provider/MusicContents$MediaType;
    }
.end annotation


# static fields
.field public static final AUTHORITY:Ljava/lang/String; = "com.samsung.musicplus"

.field public static CLOUD_SONGS_SELECTION:Ljava/lang/String; = null

.field public static final CONTENT_AUTHORITY_SLASH:Ljava/lang/String; = "content://com.samsung.musicplus/"

.field public static DEFAULT_SONGS_SELECTION:Ljava/lang/String; = null

.field public static final DISABLE:Ljava/lang/String; = "disable"

.field public static DOWNLOADED_SONGS_SELECTION:Ljava/lang/String; = null

.field public static final MUSIC_CONTENTS_URI:Landroid/net/Uri;

.field public static final MUSIC_CONTENTS_URI_WITHOUT_NOTIFY:Landroid/net/Uri;

.field public static final MUSIC_SQUARE_CELL_UPDATE:Ljava/lang/String; = "mood_update"

.field public static final MUSIC_SQUARE_GET_CALM:Ljava/lang/String; = "mood_calm"

.field public static final MUSIC_SQUARE_GET_EXCITING:Ljava/lang/String; = "mood_exciting"

.field public static final MUSIC_SQUARE_GET_JOYFUL:Ljava/lang/String; = "mood_joyful"

.field public static final MUSIC_SQUARE_GET_PASSIONATE:Ljava/lang/String; = "mood_passionate"

.field public static final PARAM_NOTIFY_CHANGE:Ljava/lang/String; = "notifyChange"

.field public static SEARCH_CONTENT_URI:Landroid/net/Uri; = null

.field public static SLINK_SONGS_SELECTION:Ljava/lang/String; = null

.field public static STORE_SONGS_SELECTION:Ljava/lang/String; = null

.field public static final UNKNOWN_STRING:Ljava/lang/String; = "<unknown>"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 92
    invoke-static {}, Lcom/samsung/musicplus/provider/MusicContents;->getContentUri()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/provider/MusicContents;->MUSIC_CONTENTS_URI:Landroid/net/Uri;

    .line 122
    sget-object v0, Lcom/samsung/musicplus/provider/MusicContents;->MUSIC_CONTENTS_URI:Landroid/net/Uri;

    invoke-static {v0}, Lcom/samsung/musicplus/provider/MusicContents;->getNotifyDisabledUri(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/provider/MusicContents;->MUSIC_CONTENTS_URI_WITHOUT_NOTIFY:Landroid/net/Uri;

    .line 237
    invoke-static {}, Lcom/samsung/musicplus/provider/MusicContents;->getSearchContentUri()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/provider/MusicContents;->SEARCH_CONTENT_URI:Landroid/net/Uri;

    .line 251
    const-string v0, "media_type&1=1"

    sput-object v0, Lcom/samsung/musicplus/provider/MusicContents;->DEFAULT_SONGS_SELECTION:Ljava/lang/String;

    .line 258
    const-string v0, "media_type&2=2"

    sput-object v0, Lcom/samsung/musicplus/provider/MusicContents;->CLOUD_SONGS_SELECTION:Ljava/lang/String;

    .line 265
    const-string v0, "media_type&3=3"

    sput-object v0, Lcom/samsung/musicplus/provider/MusicContents;->DOWNLOADED_SONGS_SELECTION:Ljava/lang/String;

    .line 271
    const-string v0, "media_type&131074=131074"

    sput-object v0, Lcom/samsung/musicplus/provider/MusicContents;->STORE_SONGS_SELECTION:Ljava/lang/String;

    .line 277
    const-string v0, "media_type&524290=524290"

    sput-object v0, Lcom/samsung/musicplus/provider/MusicContents;->SLINK_SONGS_SELECTION:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 460
    return-void
.end method

.method private static final getContentUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 130
    sget-object v0, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 132
    .local v0, "uri":Landroid/net/Uri;
    return-object v0
.end method

.method public static final getMusicSquareCalmUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 171
    const-string v0, "content://com.samsung.musicplus/mood_calm"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static final getMusicSquareCellUpdator()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 145
    const-string v0, "content://com.samsung.musicplus/mood_update"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static final getMusicSquareExcitingUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 158
    const-string v0, "content://com.samsung.musicplus/mood_exciting"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static final getMusicSquareJoyfulUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 197
    const-string v0, "content://com.samsung.musicplus/mood_joyful"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static final getMusicSquarePassionateUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 184
    const-string v0, "content://com.samsung.musicplus/mood_passionate"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static getNotifyDisabledUri(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 3
    .param p0, "uri"    # Landroid/net/Uri;

    .prologue
    .line 112
    if-nez p0, :cond_0

    .line 113
    const/4 v0, 0x0

    .line 115
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "notifyChange"

    const-string v2, "disable"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public static final getPCloudUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 234
    const-string v0, "content://com.sec.pcw/player/music"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private static getSearchContentUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 243
    const-string v0, "content://media/external/audio/search/fancy"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method
