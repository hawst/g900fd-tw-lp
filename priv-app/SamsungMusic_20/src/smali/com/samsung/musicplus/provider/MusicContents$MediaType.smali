.class public Lcom/samsung/musicplus/provider/MusicContents$MediaType;
.super Ljava/lang/Object;
.source "MusicContents.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/provider/MusicContents;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MediaType"
.end annotation


# static fields
.field public static final MEDIA_LOCAL:I = 0x10001

.field public static final SLINK_CLOUD:I = 0x80002

.field public static final SLINK_DOWNLOADED:I = 0x80003

.field public static final STATUS_CLOUD:I = 0x2

.field public static final STATUS_DOWNLOADED:I = 0x3

.field public static final STATUS_LOCAL:I = 0x1

.field public static final STORE_CLOUD:I = 0x20002

.field public static final STORE_DOWNLOADED:I = 0x20003

.field public static final TYPE_DLNA:I = 0x40000

.field public static final TYPE_MEDIA:I = 0x10000

.field public static final TYPE_NONE:I = 0x0

.field public static final TYPE_SLINK:I = 0x80000

.field public static final TYPE_STORE:I = 0x20000


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 289
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
