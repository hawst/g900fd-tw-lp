.class public interface abstract Lcom/samsung/musicplus/provider/MusicContents$Audio$DlnaColumns;
.super Ljava/lang/Object;
.source "MusicContents.java"

# interfaces
.implements Lcom/samsung/musicplus/provider/MusicContents$Audio$AudioColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/provider/MusicContents$Audio;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "DlnaColumns"
.end annotation


# static fields
.field public static final EXTENSION:Ljava/lang/String; = "extension"

.field public static final PROVIDER_ID:Ljava/lang/String; = "provider_id"

.field public static final PROVIDER_NAME:Ljava/lang/String; = "provider_name"

.field public static final SEED:Ljava/lang/String; = "seed"
