.class public interface abstract Lcom/samsung/musicplus/provider/MusicContents$Audio$AudioColumns;
.super Ljava/lang/Object;
.source "MusicContents.java"

# interfaces
.implements Lcom/samsung/musicplus/provider/MusicContents$MediaColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/provider/MusicContents$Audio;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "AudioColumns"
.end annotation


# static fields
.field public static final ALBUM:Ljava/lang/String; = "album"

.field public static final ALBUM_ARTIST:Ljava/lang/String; = "album_artist"

.field public static final ALBUM_ARTIST_COUNT:Ljava/lang/String; = "artist_count"

.field public static final ALBUM_GROUP_ID:Ljava/lang/String; = "album_group_id"

.field public static final ALBUM_ID:Ljava/lang/String; = "album_id"

.field public static final ALBUM_KEY:Ljava/lang/String; = "album_key"

.field public static final ALBUM_PINYIN:Ljava/lang/String; = "album_pinyin"

.field public static final ALBUM_SEARCH_KEY:Ljava/lang/String; = "album_search_key"

.field public static final ARTIST:Ljava/lang/String; = "artist"

.field public static final ARTIST_GROUP_ID:Ljava/lang/String; = "artist_group_id"

.field public static final ARTIST_ID:Ljava/lang/String; = "artist_id"

.field public static final ARTIST_KEY:Ljava/lang/String; = "artist_key"

.field public static final ARTIST_PINYIN:Ljava/lang/String; = "artist_pinyin"

.field public static final ARTIST_SEARCH_KEY:Ljava/lang/String; = "artist_search_key"

.field public static final BIT_DEPTH:Ljava/lang/String; = "bit_depth"

.field public static final BOOKMARK:Ljava/lang/String; = "bookmark"

.field public static final BRIGHTNESS:Ljava/lang/String; = "mood_brightness"

.field public static final BUCKET_DISPLAY_NAME:Ljava/lang/String; = "bucket_display_name"

.field public static final BUCKET_ID:Ljava/lang/String; = "bucket_id"

.field public static final BUCKET_ORDER:Ljava/lang/String; = "bucket_order"

.field public static final CHEERFUL:Ljava/lang/String; = "mood_cheerful"

.field public static final COMPOSER:Ljava/lang/String; = "composer"

.field public static final COMPOSER_PINYIN:Ljava/lang/String; = "composer_pinyin"

.field public static final DISPLAY_NAME_PINYIN:Ljava/lang/String; = "_display_name_pinyin"

.field public static final DURATION:Ljava/lang/String; = "duration"

.field public static final EXCITING:Ljava/lang/String; = "mood_exciting"

.field public static final GENRE_NAME:Ljava/lang/String; = "genre_name"

.field public static final GENRE_NAME_PINYIN:Ljava/lang/String; = "genre_name_pinyin"

.field public static final IS_ANALYZED:Ljava/lang/String; = "is_analyzed"

.field public static final IS_FAVORITE:Ljava/lang/String; = "is_favorite"

.field public static final IS_SECRETBOX:Ljava/lang/String; = "is_secretbox"

.field public static final MOOD_CELL_GROUP:Ljava/lang/String; = "mood_cell"

.field public static final MOST_PLAYED:Ljava/lang/String; = "most_played"

.field public static final RECENTLY_ADDED_REMOVE_FLAG:Ljava/lang/String; = "recently_added_remove_flag"

.field public static final RECENTLY_PLAYED:Ljava/lang/String; = "recently_played"

.field public static final RECORDING_TYPE:Ljava/lang/String; = "recordingtype"

.field public static final SAMPLING_RATE:Ljava/lang/String; = "sampling_rate"

.field public static final SOURCE_ID:Ljava/lang/String; = "source_id"

.field public static final TITLE_KEY:Ljava/lang/String; = "title_key"

.field public static final TITLE_PINYIN:Ljava/lang/String; = "title_pinyin"

.field public static final TITLE_SEARCH_KEY:Ljava/lang/String; = "title_search_key"

.field public static final TRACK:Ljava/lang/String; = "track"

.field public static final VIOLENT:Ljava/lang/String; = "mood_violent"

.field public static final YEAR:Ljava/lang/String; = "year"

.field public static final YEAR_CELL_GROUP:Ljava/lang/String; = "year_cell"

.field public static final YEAR_NAME:Ljava/lang/String; = "year_name"
