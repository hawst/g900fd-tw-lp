.class public Lcom/samsung/musicplus/provider/MediaDeleter;
.super Ljava/lang/Object;
.source "MediaDeleter.java"


# instance fields
.field private mBufferSizePerUri:I

.field private mCr:Landroid/content/ContentResolver;

.field whereArgs:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field whereClause:Ljava/lang/StringBuilder;


# direct methods
.method public constructor <init>(Landroid/content/ContentResolver;I)V
    .locals 2
    .param p1, "cr"    # Landroid/content/ContentResolver;
    .param p2, "bufferSizePerUri"    # I

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/provider/MediaDeleter;->whereClause:Ljava/lang/StringBuilder;

    .line 32
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0x64

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/samsung/musicplus/provider/MediaDeleter;->whereArgs:Ljava/util/ArrayList;

    .line 39
    iput-object p1, p0, Lcom/samsung/musicplus/provider/MediaDeleter;->mCr:Landroid/content/ContentResolver;

    .line 40
    iput p2, p0, Lcom/samsung/musicplus/provider/MediaDeleter;->mBufferSizePerUri:I

    .line 41
    return-void
.end method


# virtual methods
.method public delete(Landroid/net/Uri;J)V
    .locals 4
    .param p1, "tableUri"    # Landroid/net/Uri;
    .param p2, "id"    # J

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/musicplus/provider/MediaDeleter;->whereClause:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lcom/samsung/musicplus/provider/MediaDeleter;->whereClause:Ljava/lang/StringBuilder;

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 47
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/provider/MediaDeleter;->whereClause:Ljava/lang/StringBuilder;

    const-string v1, "?"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 48
    iget-object v0, p0, Lcom/samsung/musicplus/provider/MediaDeleter;->whereArgs:Ljava/util/ArrayList;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 49
    iget-object v0, p0, Lcom/samsung/musicplus/provider/MediaDeleter;->whereArgs:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget v1, p0, Lcom/samsung/musicplus/provider/MediaDeleter;->mBufferSizePerUri:I

    if-le v0, v1, :cond_1

    .line 50
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/provider/MediaDeleter;->flush(Landroid/net/Uri;)V

    .line 52
    :cond_1
    return-void
.end method

.method public flush(Landroid/net/Uri;)V
    .locals 5
    .param p1, "tableUri"    # Landroid/net/Uri;

    .prologue
    .line 54
    iget-object v2, p0, Lcom/samsung/musicplus/provider/MediaDeleter;->whereArgs:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 55
    .local v1, "size":I
    if-lez v1, :cond_0

    .line 56
    new-array v0, v1, [Ljava/lang/String;

    .line 57
    .local v0, "foo":[Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/musicplus/provider/MediaDeleter;->whereArgs:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "foo":[Ljava/lang/String;
    check-cast v0, [Ljava/lang/String;

    .line 58
    .restart local v0    # "foo":[Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/musicplus/provider/MediaDeleter;->mCr:Landroid/content/ContentResolver;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "source_id IN ("

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/musicplus/provider/MediaDeleter;->whereClause:Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, p1, v3, v0}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 60
    iget-object v2, p0, Lcom/samsung/musicplus/provider/MediaDeleter;->whereClause:Ljava/lang/StringBuilder;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->setLength(I)V

    .line 61
    iget-object v2, p0, Lcom/samsung/musicplus/provider/MediaDeleter;->whereArgs:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 63
    .end local v0    # "foo":[Ljava/lang/String;
    :cond_0
    return-void
.end method
