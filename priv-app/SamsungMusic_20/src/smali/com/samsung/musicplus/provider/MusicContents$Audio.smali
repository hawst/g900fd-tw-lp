.class public final Lcom/samsung/musicplus/provider/MusicContents$Audio;
.super Ljava/lang/Object;
.source "MusicContents.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/provider/MusicContents;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Audio"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/provider/MusicContents$Audio$Recordings;,
        Lcom/samsung/musicplus/provider/MusicContents$Audio$DeviceContents;,
        Lcom/samsung/musicplus/provider/MusicContents$Audio$Devices;,
        Lcom/samsung/musicplus/provider/MusicContents$Audio$Genres;,
        Lcom/samsung/musicplus/provider/MusicContents$Audio$Folders;,
        Lcom/samsung/musicplus/provider/MusicContents$Audio$FolderColumns;,
        Lcom/samsung/musicplus/provider/MusicContents$Audio$Albums;,
        Lcom/samsung/musicplus/provider/MusicContents$Audio$AlbumArt;,
        Lcom/samsung/musicplus/provider/MusicContents$Audio$AlbumArtColumns;,
        Lcom/samsung/musicplus/provider/MusicContents$Audio$AlbumColumns;,
        Lcom/samsung/musicplus/provider/MusicContents$Audio$Artists;,
        Lcom/samsung/musicplus/provider/MusicContents$Audio$ArtistColumns;,
        Lcom/samsung/musicplus/provider/MusicContents$Audio$Playlists;,
        Lcom/samsung/musicplus/provider/MusicContents$Audio$PlaylistsColumns;,
        Lcom/samsung/musicplus/provider/MusicContents$Audio$Dlna;,
        Lcom/samsung/musicplus/provider/MusicContents$Audio$DlnaColumns;,
        Lcom/samsung/musicplus/provider/MusicContents$Audio$Media;,
        Lcom/samsung/musicplus/provider/MusicContents$Audio$AudioColumns;
    }
.end annotation


# static fields
.field public static final CONTENT_URI:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 470
    invoke-static {}, Lcom/samsung/musicplus/provider/MusicContents$Audio;->getContentUri()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/provider/MusicContents$Audio;->CONTENT_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 460
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2106
    return-void
.end method

.method public static getContentUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 466
    const-string v0, "content://media/external"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method
