.class Lcom/samsung/musicplus/provider/MusicProvider$2;
.super Landroid/content/BroadcastReceiver;
.source "MusicProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/provider/MusicProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/provider/MusicProvider;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/provider/MusicProvider;)V
    .locals 0

    .prologue
    .line 1149
    iput-object p1, p0, Lcom/samsung/musicplus/provider/MusicProvider$2;->this$0:Lcom/samsung/musicplus/provider/MusicProvider;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 1152
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 1154
    .local v0, "action":Ljava/lang/String;
    const-string v2, "android.intent.action.LOCALE_CHANGED"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1155
    invoke-static {}, Lcom/samsung/musicplus/provider/SecDatabaseUtils;->setChangedLocale()V

    .line 1157
    :try_start_0
    iget-object v2, p0, Lcom/samsung/musicplus/provider/MusicProvider$2;->this$0:Lcom/samsung/musicplus/provider/MusicProvider;

    # getter for: Lcom/samsung/musicplus/provider/MusicProvider;->mDB:Landroid/database/sqlite/SQLiteDatabase;
    invoke-static {v2}, Lcom/samsung/musicplus/provider/MusicProvider;->access$400(Lcom/samsung/musicplus/provider/MusicProvider;)Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->setLocale(Ljava/util/Locale;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1162
    :cond_0
    :goto_0
    return-void

    .line 1158
    :catch_0
    move-exception v1

    .line 1159
    .local v1, "e":Ljava/lang/RuntimeException;
    # getter for: Lcom/samsung/musicplus/provider/MusicProvider;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/provider/MusicProvider;->access$300()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Failed to setLocale()"

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
