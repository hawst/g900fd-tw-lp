.class public Lcom/samsung/musicplus/provider/MusicSyncService;
.super Landroid/app/Service;
.source "MusicSyncService.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/provider/MusicSyncService$1;,
        Lcom/samsung/musicplus/provider/MusicSyncService$ServiceHandler;
    }
.end annotation


# static fields
.field public static final ARGS_SCAN_TYPE:Ljava/lang/String; = "scan_type"

.field public static final EVENT_NONE:I = 0x0

.field public static final EVENT_SYNC_AUDIO_ALL_PLAYLISTS:I = 0x2

.field public static final EVENT_SYNC_AUDIO_PLAYLISTS_FILES:I = 0x1

.field public static final EVENT_SYNC_MEDIASCANNER_FINISHED:I = 0x5

.field public static final EVENT_SYNC_MOUNDTED_SDCARD:I = 0x4

.field public static final EVENT_SYNC_SHUTDOWN:I = 0x6

.field public static final EVENT_SYNC_UNMOUNDTED_SDCARD:I = 0x3

.field private static final EXTERNAL_STORAGE_PATH:Ljava/lang/String; = "/storage/extSdCard"

.field public static final QUICK_LIST_NAME:Ljava/lang/String; = "Quick list"

.field private static final SYNC_CANCELED:I = -0x1

.field private static final SYNC_COMPLETED:I = 0x0

.field private static final TAG:Ljava/lang/String; = "MusicSyncService"

.field private static sIsShutdown:Z


# instance fields
.field private final PER_UPDATE_MAX_COUNT:I

.field private SUPPORT_PLAYLIST_FORMAT:Ljava/lang/String;

.field private mAfterMountedEvent:Z

.field private mPendingSyncAllPlayList:Z

.field private volatile mServiceHandler:Lcom/samsung/musicplus/provider/MusicSyncService$ServiceHandler;

.field private volatile mServiceLooper:Landroid/os/Looper;

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 85
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/musicplus/provider/MusicSyncService;->sIsShutdown:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 41
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 51
    const/16 v0, 0x1f4

    iput v0, p0, Lcom/samsung/musicplus/provider/MusicSyncService;->PER_UPDATE_MAX_COUNT:I

    .line 53
    iput-boolean v1, p0, Lcom/samsung/musicplus/provider/MusicSyncService;->mPendingSyncAllPlayList:Z

    .line 81
    iput-boolean v1, p0, Lcom/samsung/musicplus/provider/MusicSyncService;->mAfterMountedEvent:Z

    .line 830
    const-string v0, "_data like \'%m3u\' or _data like \'%pla\' or _data like \'%wpl\'"

    iput-object v0, p0, Lcom/samsung/musicplus/provider/MusicSyncService;->SUPPORT_PLAYLIST_FORMAT:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$100(Lcom/samsung/musicplus/provider/MusicSyncService;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/provider/MusicSyncService;
    .param p1, "x1"    # I

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/provider/MusicSyncService;->sync(I)V

    return-void
.end method

.method private deleteOrUpdate(Lcom/samsung/musicplus/provider/MediaDeleter;Lcom/samsung/musicplus/provider/MediaInserter;IIJJII)V
    .locals 9
    .param p1, "deleter"    # Lcom/samsung/musicplus/provider/MediaDeleter;
    .param p2, "updater"    # Lcom/samsung/musicplus/provider/MediaInserter;
    .param p3, "num"    # I
    .param p4, "index"    # I
    .param p5, "srcId"    # J
    .param p7, "mediaId"    # J
    .param p9, "media_type"    # I
    .param p10, "event"    # I

    .prologue
    .line 477
    const/4 v7, 0x0

    .line 479
    .local v7, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/provider/MusicSyncService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 480
    .local v0, "cr":Landroid/content/ContentResolver;
    const/4 v2, 0x0

    .line 481
    .local v2, "columns":[Ljava/lang/String;
    sget-boolean v1, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_PINYIN:Z

    if-eqz v1, :cond_3

    .line 482
    sget-object v2, Lcom/samsung/musicplus/provider/MusicContents$Audio$Media;->MEDIA_COLUMNS_FOR_SYNC_PINYIN:[Ljava/lang/String;

    .line 486
    :goto_0
    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "_id="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p5, p6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/database/Cursor;

    move-result-object v7

    .line 492
    if-eqz v7, :cond_0

    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-nez v1, :cond_1

    .line 494
    :cond_0
    sget-object v1, Lcom/samsung/musicplus/provider/MusicContents$Audio$Media;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {p1, v1, p5, p6}, Lcom/samsung/musicplus/provider/MediaDeleter;->delete(Landroid/net/Uri;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 529
    :cond_1
    if-eqz v7, :cond_2

    .line 530
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 533
    :cond_2
    return-void

    .line 484
    :cond_3
    :try_start_1
    sget-object v2, Lcom/samsung/musicplus/provider/MusicContents$Audio$Media;->MEDIA_COLUMNS_FOR_SYNC:[Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 529
    .end local v0    # "cr":Landroid/content/ContentResolver;
    .end local v2    # "columns":[Ljava/lang/String;
    :catchall_0
    move-exception v1

    if-eqz v7, :cond_4

    .line 530
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v1
.end method

.method private processPlayLists(I)V
    .locals 3
    .param p1, "event"    # I

    .prologue
    const/4 v2, 0x0

    .line 166
    packed-switch p1, :pswitch_data_0

    .line 189
    :goto_0
    :pswitch_0
    return-void

    .line 168
    :pswitch_1
    const-string v0, "MusicSyncService"

    const-string v1, "sync all playlists and songs from Media Provider"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    invoke-direct {p0}, Lcom/samsung/musicplus/provider/MusicSyncService;->syncAllPlaylists()V

    goto :goto_0

    .line 179
    :pswitch_2
    iget-boolean v0, p0, Lcom/samsung/musicplus/provider/MusicSyncService;->mAfterMountedEvent:Z

    if-eqz v0, :cond_0

    .line 180
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/provider/MusicSyncService;->mAfterMountedEvent:Z

    .line 181
    invoke-virtual {p0}, Lcom/samsung/musicplus/provider/MusicSyncService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {}, Lcom/samsung/musicplus/provider/MusicContents$Audio$Playlists$Members;->getContentCacheUri()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v2, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 184
    :cond_0
    const-string v0, "MusicSyncService"

    const-string v1, "sync only playlist files from Media Provider"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    invoke-direct {p0}, Lcom/samsung/musicplus/provider/MusicSyncService;->syncPlaylistsFiles()V

    goto :goto_0

    .line 166
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method private sync(I)V
    .locals 9
    .param p1, "event"    # I

    .prologue
    .line 88
    iget-object v0, p0, Lcom/samsung/musicplus/provider/MusicSyncService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 89
    const/4 v8, 0x0

    .line 91
    .local v8, "result":I
    const/4 v0, 0x3

    if-ne p1, v0, :cond_2

    .line 92
    :try_start_0
    sget-boolean v0, Lcom/samsung/musicplus/provider/MusicSyncService;->sIsShutdown:Z

    if-eqz v0, :cond_1

    .line 93
    const-string v0, "MusicSyncService"

    const-string v1, "during shutdown, so in this case, ignore this event"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 155
    :cond_0
    :goto_0
    invoke-static {}, Lcom/samsung/musicplus/provider/MusicProvider;->cleanHashTable()V

    .line 160
    const-string v0, "MusicSyncService"

    const-string v1, "sync() ended"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    iget-object v0, p0, Lcom/samsung/musicplus/provider/MusicSyncService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 163
    return-void

    .line 95
    :cond_1
    :try_start_1
    const-string v0, "MusicSyncService"

    const-string v1, "unmount sd card, so delete all of songs at SD card."

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    invoke-virtual {p0}, Lcom/samsung/musicplus/provider/MusicSyncService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/musicplus/provider/MusicContents$Audio$Media;->CONTENT_URI:Landroid/net/Uri;

    const-string v2, "_data LIKE \'/storage/extSdCard/%\'"

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 155
    :catchall_0
    move-exception v0

    invoke-static {}, Lcom/samsung/musicplus/provider/MusicProvider;->cleanHashTable()V

    .line 160
    const-string v1, "MusicSyncService"

    const-string v2, "sync() ended"

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    iget-object v1, p0, Lcom/samsung/musicplus/provider/MusicSyncService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    throw v0

    .line 107
    :cond_2
    const/4 v6, 0x0

    .line 108
    .local v6, "c":Landroid/database/Cursor;
    const/4 v7, 0x0

    .line 110
    .local v7, "count":I
    :try_start_2
    invoke-virtual {p0}, Lcom/samsung/musicplus/provider/MusicSyncService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/musicplus/provider/MusicContents$Audio$Media;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "count(*)"

    aput-object v4, v2, v3

    sget-object v3, Lcom/samsung/musicplus/provider/MusicContents;->DEFAULT_SONGS_SELECTION:Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 114
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 115
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v7

    .line 117
    if-eqz v6, :cond_3

    .line 118
    :try_start_3
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 119
    const/4 v6, 0x0

    .line 124
    :cond_3
    if-eqz v7, :cond_5

    .line 125
    const-string v0, "MusicSyncService"

    const-string v1, "sync insert, delte, update from Media Provider"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 126
    invoke-direct {p0}, Lcom/samsung/musicplus/provider/MusicSyncService;->syncInsert()V

    .line 127
    invoke-direct {p0}, Lcom/samsung/musicplus/provider/MusicSyncService;->updateMusicSquareCell()V

    .line 128
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/provider/MusicSyncService;->syncDeleteOrUpdate(I)I

    move-result v8

    .line 135
    :goto_1
    if-nez v8, :cond_0

    .line 137
    invoke-direct {p0}, Lcom/samsung/musicplus/provider/MusicSyncService;->syncAlbumArtTable()V

    .line 140
    iget-boolean v0, p0, Lcom/samsung/musicplus/provider/MusicSyncService;->mPendingSyncAllPlayList:Z

    if-eqz v0, :cond_0

    .line 145
    iget-object v0, p0, Lcom/samsung/musicplus/provider/MusicSyncService;->mServiceHandler:Lcom/samsung/musicplus/provider/MusicSyncService$ServiceHandler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/provider/MusicSyncService$ServiceHandler;->removeMessages(I)V

    .line 146
    const/4 p1, 0x2

    .line 147
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/provider/MusicSyncService;->mPendingSyncAllPlayList:Z

    goto/16 :goto_0

    .line 117
    :catchall_1
    move-exception v0

    if-eqz v6, :cond_4

    .line 118
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 119
    const/4 v6, 0x0

    :cond_4
    throw v0

    .line 130
    :cond_5
    const-string v0, "MusicSyncService"

    const-string v1, "init sync from Media Provider"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    invoke-direct {p0}, Lcom/samsung/musicplus/provider/MusicSyncService;->syncAudio()V

    .line 132
    invoke-direct {p0}, Lcom/samsung/musicplus/provider/MusicSyncService;->updateMusicSquareCell()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method private syncAddedFiles(J)V
    .locals 3
    .param p1, "maxId"    # J

    .prologue
    .line 865
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/provider/MusicSyncService;->SUPPORT_PLAYLIST_FORMAT:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " AND "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "_id > "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 866
    .local v0, "where":Ljava/lang/String;
    invoke-direct {p0, v0}, Lcom/samsung/musicplus/provider/MusicSyncService;->syncPlaylists(Ljava/lang/String;)V

    .line 867
    return-void
.end method

.method private syncAlbumArtTable()V
    .locals 14

    .prologue
    const/4 v13, 0x1

    const/4 v12, 0x0

    const/4 v5, 0x0

    .line 296
    const-string v3, "MusicSyncService"

    const-string v4, "syncAlbumArtTable()"

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 298
    sget-object v1, Landroid/provider/MediaStore$Audio$Albums;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 299
    .local v1, "srcUri":Landroid/net/Uri;
    const/4 v3, 0x2

    new-array v2, v3, [Ljava/lang/String;

    const-string v3, "_id"

    aput-object v3, v2, v12

    const-string v3, "album_art"

    aput-object v3, v2, v13

    .line 302
    .local v2, "columns":[Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/musicplus/provider/MusicSyncService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 304
    .local v0, "cr":Landroid/content/ContentResolver;
    sget-object v3, Lcom/samsung/musicplus/provider/MusicContents$Audio$AlbumArt;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0, v3, v5, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 306
    const/4 v6, 0x0

    .line 307
    .local v6, "c":Landroid/database/Cursor;
    const/4 v9, 0x0

    .line 309
    .local v9, "srcCount":I
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    :try_start_0
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 310
    if-nez v6, :cond_1

    .line 311
    const-string v3, "MusicSyncService"

    const-string v4, "c == null so cancel sync Album Art Table"

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 335
    if-eqz v6, :cond_0

    .line 336
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 337
    const/4 v6, 0x0

    .line 341
    :cond_0
    :goto_0
    return-void

    .line 314
    :cond_1
    :try_start_1
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v9

    .line 316
    if-eqz v9, :cond_3

    .line 319
    new-instance v8, Lcom/samsung/musicplus/provider/MediaInserter;

    const/16 v3, 0x1f4

    invoke-direct {v8, v0, v3}, Lcom/samsung/musicplus/provider/MediaInserter;-><init>(Landroid/content/ContentResolver;I)V

    .line 321
    .local v8, "mediaInserter":Lcom/samsung/musicplus/provider/MediaInserter;
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_1
    if-ge v7, v9, :cond_2

    .line 322
    invoke-interface {v6, v7}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 323
    new-instance v11, Landroid/content/ContentValues;

    invoke-direct {v11}, Landroid/content/ContentValues;-><init>()V

    .line 324
    .local v11, "value":Landroid/content/ContentValues;
    const-string v3, "album_id"

    const/4 v4, 0x0

    invoke-interface {v6, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v11, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 325
    const-string v3, "album_art"

    const/4 v4, 0x1

    invoke-interface {v6, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v11, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 327
    sget-object v10, Lcom/samsung/musicplus/provider/MusicContents$Audio$AlbumArt;->CONTENT_URI:Landroid/net/Uri;

    .line 328
    .local v10, "uri":Landroid/net/Uri;
    invoke-virtual {v8, v10, v11}, Lcom/samsung/musicplus/provider/MediaInserter;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)V

    .line 321
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 331
    .end local v10    # "uri":Landroid/net/Uri;
    .end local v11    # "value":Landroid/content/ContentValues;
    :cond_2
    invoke-virtual {v8}, Lcom/samsung/musicplus/provider/MediaInserter;->flushAll()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 335
    .end local v7    # "i":I
    .end local v8    # "mediaInserter":Lcom/samsung/musicplus/provider/MediaInserter;
    :cond_3
    if-eqz v6, :cond_0

    .line 336
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 337
    const/4 v6, 0x0

    goto :goto_0

    .line 335
    :catchall_0
    move-exception v3

    if-eqz v6, :cond_4

    .line 336
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 337
    const/4 v6, 0x0

    :cond_4
    throw v3
.end method

.method private syncAllPlaylists()V
    .locals 1

    .prologue
    .line 627
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/provider/MusicSyncService;->syncPlaylists(Ljava/lang/String;)V

    .line 628
    return-void
.end method

.method private syncAudio()V
    .locals 12

    .prologue
    .line 344
    const-string v4, "MusicSyncService"

    const-string v5, "syncAudio()"

    invoke-static {v4, v5}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 347
    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    .line 348
    .local v1, "srcUri":Landroid/net/Uri;
    const/4 v2, 0x0

    .line 349
    .local v2, "columns":[Ljava/lang/String;
    sget-boolean v4, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_PINYIN:Z

    if-eqz v4, :cond_0

    .line 350
    sget-object v2, Lcom/samsung/musicplus/provider/MusicContents$Audio$Media;->MEDIA_COLUMNS_FOR_SYNC_PINYIN:[Ljava/lang/String;

    .line 354
    :goto_0
    const-string v3, "is_music = 1"

    .line 355
    .local v3, "selection":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/musicplus/provider/MusicSyncService;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 356
    .local v0, "cr":Landroid/content/ContentResolver;
    const/4 v6, 0x0

    .line 358
    .local v6, "c":Landroid/database/Cursor;
    const/4 v4, 0x0

    const/4 v5, 0x0

    :try_start_0
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 359
    if-eqz v6, :cond_2

    .line 360
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v9

    .line 362
    .local v9, "srcCount":I
    const-string v4, "MusicSyncService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "syncAudio(). count - "

    invoke-virtual {v5, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 364
    if-eqz v9, :cond_2

    .line 367
    new-instance v8, Lcom/samsung/musicplus/provider/MediaInserter;

    const/16 v4, 0x1f4

    invoke-direct {v8, v0, v4}, Lcom/samsung/musicplus/provider/MediaInserter;-><init>(Landroid/content/ContentResolver;I)V

    .line 369
    .local v8, "mediaInserter":Lcom/samsung/musicplus/provider/MediaInserter;
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_1
    if-ge v7, v9, :cond_1

    .line 370
    invoke-interface {v6, v7}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 371
    invoke-static {v6}, Lcom/samsung/musicplus/provider/MusicContents$Audio$Media;->makeContentValue(Landroid/database/Cursor;)Landroid/content/ContentValues;

    move-result-object v10

    .line 372
    .local v10, "values":Landroid/content/ContentValues;
    sget-object v4, Lcom/samsung/musicplus/provider/MusicContents$Audio$Media;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v8, v4, v10}, Lcom/samsung/musicplus/provider/MediaInserter;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 369
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 352
    .end local v0    # "cr":Landroid/content/ContentResolver;
    .end local v3    # "selection":Ljava/lang/String;
    .end local v6    # "c":Landroid/database/Cursor;
    .end local v7    # "i":I
    .end local v8    # "mediaInserter":Lcom/samsung/musicplus/provider/MediaInserter;
    .end local v9    # "srcCount":I
    .end local v10    # "values":Landroid/content/ContentValues;
    :cond_0
    sget-object v2, Lcom/samsung/musicplus/provider/MusicContents$Audio$Media;->MEDIA_COLUMNS_FOR_SYNC:[Ljava/lang/String;

    goto :goto_0

    .line 375
    .restart local v0    # "cr":Landroid/content/ContentResolver;
    .restart local v3    # "selection":Ljava/lang/String;
    .restart local v6    # "c":Landroid/database/Cursor;
    .restart local v7    # "i":I
    .restart local v8    # "mediaInserter":Lcom/samsung/musicplus/provider/MediaInserter;
    .restart local v9    # "srcCount":I
    :cond_1
    :try_start_1
    invoke-virtual {v8}, Lcom/samsung/musicplus/provider/MediaInserter;->flushAll()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 380
    .end local v7    # "i":I
    .end local v8    # "mediaInserter":Lcom/samsung/musicplus/provider/MediaInserter;
    .end local v9    # "srcCount":I
    :cond_2
    if-eqz v6, :cond_3

    .line 381
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 382
    const/4 v6, 0x0

    .line 386
    :cond_3
    return-void

    .line 380
    :catchall_0
    move-exception v4

    if-eqz v6, :cond_4

    .line 381
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 382
    const/4 v6, 0x0

    :cond_4
    throw v4
.end method

.method private syncDeleteOrUpdate(I)I
    .locals 21
    .param p1, "event"    # I

    .prologue
    .line 395
    const-string v2, "MusicSyncService"

    const-string v5, "syncDeleteOrUpdate()"

    invoke-static {v2, v5}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 396
    const/4 v6, 0x0

    .line 398
    .local v6, "selectionArgs":[Ljava/lang/String;
    const-wide/16 v12, 0x0

    .line 399
    .local v12, "lastId":J
    const-wide/16 v14, 0x0

    .line 400
    .local v14, "_id":J
    const/16 v16, 0x0

    .line 401
    .local v16, "mediaType":I
    sget-object v2, Lcom/samsung/musicplus/provider/MusicContents$Audio$Media;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v5, "limit"

    const-string v7, "1000"

    invoke-virtual {v2, v5, v7}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    .line 404
    .local v3, "limitUri":Landroid/net/Uri;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/provider/MusicSyncService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v19

    .line 405
    .local v19, "cr":Landroid/content/ContentResolver;
    new-instance v20, Lcom/samsung/musicplus/provider/MediaDeleter;

    const/16 v2, 0x1f4

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-direct {v0, v1, v2}, Lcom/samsung/musicplus/provider/MediaDeleter;-><init>(Landroid/content/ContentResolver;I)V

    .line 406
    .local v20, "deleter":Lcom/samsung/musicplus/provider/MediaDeleter;
    new-instance v9, Lcom/samsung/musicplus/provider/MediaInserter;

    const/16 v2, 0x1f4

    move-object/from16 v0, v19

    invoke-direct {v9, v0, v2}, Lcom/samsung/musicplus/provider/MediaInserter;-><init>(Landroid/content/ContentResolver;I)V

    .line 408
    .local v9, "updater":Lcom/samsung/musicplus/provider/MediaInserter;
    const/4 v2, 0x3

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v5, "source_id"

    aput-object v5, v4, v2

    const/4 v2, 0x1

    const-string v5, "_id"

    aput-object v5, v4, v2

    const/4 v2, 0x2

    const-string v5, "media_type"

    aput-object v5, v4, v2

    .line 413
    .local v4, "columns":[Ljava/lang/String;
    :cond_0
    :goto_0
    const/16 v18, 0x0

    .line 416
    .local v18, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/provider/MusicSyncService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v7, Lcom/samsung/musicplus/provider/MusicContents;->DEFAULT_SONGS_SELECTION:Ljava/lang/String;

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " AND "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, "source_id"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " > "

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v7, "source_id"

    const/4 v8, 0x0

    invoke-virtual/range {v2 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v18

    .line 423
    if-nez v18, :cond_3

    .line 452
    if-eqz v18, :cond_1

    .line 453
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    .line 458
    :cond_1
    :goto_1
    sget-object v2, Lcom/samsung/musicplus/provider/MusicContents$Audio$Media;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Lcom/samsung/musicplus/provider/MediaDeleter;->flush(Landroid/net/Uri;)V

    .line 459
    invoke-virtual {v9}, Lcom/samsung/musicplus/provider/MediaInserter;->flushAll()V

    .line 461
    const/4 v2, 0x0

    :cond_2
    :goto_2
    return v2

    .line 427
    :cond_3
    :try_start_1
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->getCount()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v10

    .line 428
    .local v10, "num":I
    if-nez v10, :cond_4

    .line 452
    if-eqz v18, :cond_1

    .line 453
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 431
    :cond_4
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_3
    if-ge v11, v10, :cond_8

    .line 432
    :try_start_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/musicplus/provider/MusicSyncService;->mServiceHandler:Lcom/samsung/musicplus/provider/MusicSyncService$ServiceHandler;

    const/4 v5, 0x1

    invoke-virtual {v2, v5}, Lcom/samsung/musicplus/provider/MusicSyncService$ServiceHandler;->hasMessages(I)Z

    move-result v2

    if-nez v2, :cond_5

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/musicplus/provider/MusicSyncService;->mServiceHandler:Lcom/samsung/musicplus/provider/MusicSyncService$ServiceHandler;

    const/4 v5, 0x3

    invoke-virtual {v2, v5}, Lcom/samsung/musicplus/provider/MusicSyncService$ServiceHandler;->hasMessages(I)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 436
    :cond_5
    const-string v2, "MusicSyncService"

    const-string v5, "pending queue have same sync event. so cancel current syncing task"

    invoke-static {v2, v5}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 438
    sget-object v2, Lcom/samsung/musicplus/provider/MusicContents$Audio$Media;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Lcom/samsung/musicplus/provider/MediaDeleter;->flush(Landroid/net/Uri;)V

    .line 439
    invoke-virtual {v9}, Lcom/samsung/musicplus/provider/MediaInserter;->flushAll()V

    .line 440
    if-eqz v18, :cond_6

    .line 441
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 443
    :cond_6
    const/4 v2, -0x1

    .line 452
    if-eqz v18, :cond_2

    .line 453
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    goto :goto_2

    .line 445
    :cond_7
    :try_start_3
    move-object/from16 v0, v18

    invoke-interface {v0, v11}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 446
    const-string v2, "source_id"

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    .line 447
    const-string v2, "_id"

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    .line 448
    const-string v2, "media_type"

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, v18

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v16

    move-object/from16 v7, p0

    move-object/from16 v8, v20

    move/from16 v17, p1

    .line 449
    invoke-direct/range {v7 .. v17}, Lcom/samsung/musicplus/provider/MusicSyncService;->deleteOrUpdate(Lcom/samsung/musicplus/provider/MediaDeleter;Lcom/samsung/musicplus/provider/MediaInserter;IIJJII)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 431
    add-int/lit8 v11, v11, 0x1

    goto :goto_3

    .line 452
    :cond_8
    if-eqz v18, :cond_0

    .line 453
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 452
    .end local v10    # "num":I
    .end local v11    # "i":I
    :catchall_0
    move-exception v2

    if-eqz v18, :cond_9

    .line 453
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    :cond_9
    throw v2
.end method

.method private syncFavoriteList(J)V
    .locals 19
    .param p1, "_id"    # J

    .prologue
    .line 776
    const/4 v3, 0x2

    new-array v4, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v5, "audio_id"

    aput-object v5, v4, v3

    const/4 v3, 0x1

    const-string v5, "is_music"

    aput-object v5, v4, v3

    .line 786
    .local v4, "Col":[Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/provider/MusicSyncService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 787
    .local v2, "cr":Landroid/content/ContentResolver;
    const/4 v9, 0x0

    .line 789
    .local v9, "c":Landroid/database/Cursor;
    :try_start_0
    const-string v3, "external"

    move-wide/from16 v0, p1

    invoke-static {v3, v0, v1}, Landroid/provider/MediaStore$Audio$Playlists$Members;->getContentUri(Ljava/lang/String;J)Landroid/net/Uri;

    move-result-object v3

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v2 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v9

    .line 791
    if-nez v9, :cond_1

    .line 823
    if-eqz v9, :cond_0

    .line 824
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 825
    const/4 v9, 0x0

    .line 828
    :cond_0
    :goto_0
    return-void

    .line 795
    :cond_1
    :try_start_1
    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v12

    .line 796
    .local v12, "count":I
    if-nez v12, :cond_2

    .line 823
    if-eqz v9, :cond_0

    .line 824
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 825
    const/4 v9, 0x0

    goto :goto_0

    .line 800
    :cond_2
    const-wide/16 v16, 0x0

    .line 802
    .local v16, "src_audioId":J
    :try_start_2
    const-string v3, "audio_id"

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    .line 804
    .local v14, "src_Idx":I
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    .line 805
    .local v18, "where":Ljava/lang/StringBuilder;
    const-string v3, "_id IN ("

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 806
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_1
    if-ge v13, v12, :cond_5

    .line 807
    invoke-interface {v9, v13}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 808
    invoke-interface {v9, v14}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v16

    .line 810
    sget-object v3, Lcom/samsung/musicplus/provider/MusicProvider;->sAudioMapSyncTable:Ljava/util/HashMap;

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    .line 811
    .local v10, "audioId":J
    move-object/from16 v0, v18

    invoke-virtual {v0, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 812
    add-int/lit8 v3, v12, -0x1

    if-eq v13, v3, :cond_3

    .line 813
    const-string v3, ","

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 806
    :goto_2
    add-int/lit8 v13, v13, 0x1

    goto :goto_1

    .line 815
    :cond_3
    const-string v3, ")"

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 823
    .end local v10    # "audioId":J
    .end local v12    # "count":I
    .end local v13    # "i":I
    .end local v14    # "src_Idx":I
    .end local v16    # "src_audioId":J
    .end local v18    # "where":Ljava/lang/StringBuilder;
    :catchall_0
    move-exception v3

    if-eqz v9, :cond_4

    .line 824
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 825
    const/4 v9, 0x0

    :cond_4
    throw v3

    .line 819
    .restart local v12    # "count":I
    .restart local v13    # "i":I
    .restart local v14    # "src_Idx":I
    .restart local v16    # "src_audioId":J
    .restart local v18    # "where":Ljava/lang/StringBuilder;
    :cond_5
    :try_start_3
    new-instance v15, Landroid/content/ContentValues;

    invoke-direct {v15}, Landroid/content/ContentValues;-><init>()V

    .line 820
    .local v15, "values":Landroid/content/ContentValues;
    const-string v3, "is_favorite"

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v15, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 821
    sget-object v3, Lcom/samsung/musicplus/provider/MusicContents$Audio$Media;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v2, v3, v15, v5, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 823
    if-eqz v9, :cond_0

    .line 824
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 825
    const/4 v9, 0x0

    goto :goto_0
.end method

.method private syncInsert()V
    .locals 15

    .prologue
    .line 542
    const-string v3, "MusicSyncService"

    const-string v4, "syncInsert()"

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 543
    const/4 v7, 0x0

    .line 545
    .local v7, "c":Landroid/database/Cursor;
    const-wide/16 v10, 0x0

    .line 548
    .local v10, "maxId":J
    sget-object v3, Lcom/samsung/musicplus/provider/MusicContents$Audio$Media;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "limit"

    const-string v5, "1"

    invoke-virtual {v3, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 551
    .local v1, "limitUri":Landroid/net/Uri;
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/provider/MusicSyncService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v3, 0x1

    new-array v2, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "source_id"

    aput-object v4, v2, v3

    sget-object v3, Lcom/samsung/musicplus/provider/MusicContents;->DEFAULT_SONGS_SELECTION:Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "source_id DESC"

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/database/Cursor;

    move-result-object v7

    .line 554
    if-eqz v7, :cond_0

    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v3

    if-eqz v3, :cond_0

    .line 555
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 556
    const/4 v3, 0x0

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v10

    .line 560
    :cond_0
    if-eqz v7, :cond_1

    .line 561
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 562
    const/4 v7, 0x0

    .line 566
    :cond_1
    sget-object v3, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "limit"

    const-string v5, "1000"

    invoke-virtual {v3, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 569
    invoke-virtual {p0}, Lcom/samsung/musicplus/provider/MusicSyncService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 570
    .local v0, "cr":Landroid/content/ContentResolver;
    new-instance v12, Lcom/samsung/musicplus/provider/MediaInserter;

    const/16 v3, 0x1f4

    invoke-direct {v12, v0, v3}, Lcom/samsung/musicplus/provider/MediaInserter;-><init>(Landroid/content/ContentResolver;I)V

    .line 572
    .local v12, "mediaInserter":Lcom/samsung/musicplus/provider/MediaInserter;
    const-string v3, "MusicSyncService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "syncInsert(). max source_id = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 576
    :cond_2
    :goto_0
    const/4 v2, 0x0

    .line 577
    .local v2, "columns":[Ljava/lang/String;
    sget-boolean v3, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_PINYIN:Z

    if-eqz v3, :cond_5

    .line 578
    sget-object v2, Lcom/samsung/musicplus/provider/MusicContents$Audio$Media;->MEDIA_COLUMNS_FOR_SYNC_PINYIN:[Ljava/lang/String;

    .line 583
    :goto_1
    :try_start_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "is_music = 1  AND _id>"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const-string v5, "_id"

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/database/Cursor;
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v7

    .line 585
    if-nez v7, :cond_6

    .line 612
    if-eqz v7, :cond_3

    .line 613
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 614
    const/4 v7, 0x0

    .line 621
    :cond_3
    :goto_2
    invoke-virtual {v12}, Lcom/samsung/musicplus/provider/MediaInserter;->flushAll()V

    .line 622
    const/4 v12, 0x0

    .line 624
    return-void

    .line 560
    .end local v0    # "cr":Landroid/content/ContentResolver;
    .end local v2    # "columns":[Ljava/lang/String;
    .end local v12    # "mediaInserter":Lcom/samsung/musicplus/provider/MediaInserter;
    :catchall_0
    move-exception v3

    if-eqz v7, :cond_4

    .line 561
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 562
    const/4 v7, 0x0

    :cond_4
    throw v3

    .line 580
    .restart local v0    # "cr":Landroid/content/ContentResolver;
    .restart local v2    # "columns":[Ljava/lang/String;
    .restart local v12    # "mediaInserter":Lcom/samsung/musicplus/provider/MediaInserter;
    :cond_5
    sget-object v2, Lcom/samsung/musicplus/provider/MusicContents$Audio$Media;->MEDIA_COLUMNS_FOR_SYNC:[Ljava/lang/String;

    goto :goto_1

    .line 589
    :cond_6
    :try_start_2
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v13

    .line 591
    .local v13, "num":I
    const-string v3, "MusicSyncService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "syncInsert(). num cnt of media provider  = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 593
    if-nez v13, :cond_8

    .line 594
    if-eqz v7, :cond_7

    .line 595
    invoke-interface {v7}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 596
    const/4 v7, 0x0

    .line 612
    :cond_7
    if-eqz v7, :cond_3

    .line 613
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 614
    const/4 v7, 0x0

    goto :goto_2

    .line 601
    :cond_8
    const/4 v9, 0x0

    .local v9, "i":I
    :goto_3
    if-ge v9, v13, :cond_9

    .line 602
    :try_start_3
    invoke-interface {v7, v9}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 603
    const-string v3, "_id"

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v7, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 604
    invoke-static {v7}, Lcom/samsung/musicplus/provider/MusicContents$Audio$Media;->makeContentValue(Landroid/database/Cursor;)Landroid/content/ContentValues;

    move-result-object v14

    .line 605
    .local v14, "values":Landroid/content/ContentValues;
    sget-object v3, Lcom/samsung/musicplus/provider/MusicContents$Audio$Media;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v12, v3, v14}, Lcom/samsung/musicplus/provider/MediaInserter;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)V
    :try_end_3
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 601
    add-int/lit8 v9, v9, 0x1

    goto :goto_3

    .line 612
    .end local v14    # "values":Landroid/content/ContentValues;
    :cond_9
    if-eqz v7, :cond_2

    .line 613
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 614
    const/4 v7, 0x0

    goto/16 :goto_0

    .line 607
    .end local v9    # "i":I
    .end local v13    # "num":I
    :catch_0
    move-exception v8

    .line 608
    .local v8, "e":Landroid/database/sqlite/SQLiteException;
    :try_start_4
    const-string v3, "MusicSyncService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, " sqlite works as abnormal during sync insert"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v8}, Landroid/database/sqlite/SQLiteException;->getMessage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 612
    if-eqz v7, :cond_3

    .line 613
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 614
    const/4 v7, 0x0

    goto/16 :goto_2

    .line 612
    .end local v8    # "e":Landroid/database/sqlite/SQLiteException;
    :catchall_1
    move-exception v3

    if-eqz v7, :cond_a

    .line 613
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 614
    const/4 v7, 0x0

    :cond_a
    throw v3
.end method

.method private syncModifiedFiles(J)V
    .locals 23
    .param p1, "maxId"    # J

    .prologue
    .line 870
    const/4 v14, 0x0

    .line 872
    .local v14, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/provider/MusicSyncService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Lcom/samsung/musicplus/provider/MusicContents$Audio$Playlists;->CONTENT_URI:Landroid/net/Uri;

    const/4 v7, 0x3

    new-array v6, v7, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "_id"

    aput-object v8, v6, v7

    const/4 v7, 0x1

    const-string v8, "source_id"

    aput-object v8, v6, v7

    const/4 v7, 0x2

    const-string v8, "date_modified"

    aput-object v8, v6, v7

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "("

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/musicplus/provider/MusicSyncService;->SUPPORT_PLAYLIST_FORMAT:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " AND "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "_id <= "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-wide/from16 v0, p1

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    const-string v9, "source_id DESC"

    const/4 v10, 0x0

    invoke-virtual/range {v4 .. v10}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/database/Cursor;

    move-result-object v14

    .line 881
    if-eqz v14, :cond_2

    invoke-interface {v14}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 882
    const/16 v18, 0x0

    .line 883
    .local v18, "src_C":Landroid/database/Cursor;
    const-wide/16 v20, 0x0

    .line 884
    .local v20, "src_id":J
    const-wide/16 v12, 0x0

    .line 885
    .local v12, "_id":J
    const/4 v15, 0x0

    .line 886
    .local v15, "date_modified":I
    const-string v4, "source_id"

    invoke-interface {v14, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v17

    .line 887
    .local v17, "srcId_idx":I
    const-string v4, "_id"

    invoke-interface {v14, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    .line 888
    .local v11, "_id_idx":I
    const-string v4, "date_modified"

    invoke-interface {v14, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v16

    .line 890
    .local v16, "date_modified_idx":I
    const/4 v4, 0x1

    new-array v6, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "date_modified"

    aput-object v5, v6, v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 896
    .local v6, "columns":[Ljava/lang/String;
    :cond_0
    :try_start_1
    move/from16 v0, v17

    invoke-interface {v14, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v20

    .line 897
    invoke-interface {v14, v11}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    .line 898
    move/from16 v0, v16

    invoke-interface {v14, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v15

    .line 899
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/provider/MusicSyncService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Landroid/provider/MediaStore$Audio$Playlists;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "_id = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-wide/from16 v0, v20

    invoke-virtual {v7, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-virtual/range {v4 .. v10}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/database/Cursor;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v18

    .line 903
    if-nez v18, :cond_4

    .line 929
    if-eqz v18, :cond_1

    .line 930
    :try_start_2
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    .line 933
    :cond_1
    :goto_0
    invoke-interface {v14}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v4

    if-nez v4, :cond_0

    .line 936
    .end local v6    # "columns":[Ljava/lang/String;
    .end local v11    # "_id_idx":I
    .end local v12    # "_id":J
    .end local v15    # "date_modified":I
    .end local v16    # "date_modified_idx":I
    .end local v17    # "srcId_idx":I
    .end local v18    # "src_C":Landroid/database/Cursor;
    .end local v20    # "src_id":J
    :cond_2
    if-eqz v14, :cond_3

    .line 937
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    .line 940
    :cond_3
    return-void

    .line 907
    .restart local v6    # "columns":[Ljava/lang/String;
    .restart local v11    # "_id_idx":I
    .restart local v12    # "_id":J
    .restart local v15    # "date_modified":I
    .restart local v16    # "date_modified_idx":I
    .restart local v17    # "srcId_idx":I
    .restart local v18    # "src_C":Landroid/database/Cursor;
    .restart local v20    # "src_id":J
    :cond_4
    :try_start_3
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-nez v4, :cond_7

    .line 912
    invoke-static {v12, v13}, Lcom/samsung/musicplus/provider/MusicContents$Audio$Playlists$Members;->getContentUri(J)Landroid/net/Uri;

    move-result-object v19

    .line 913
    .local v19, "uri":Landroid/net/Uri;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/provider/MusicSyncService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, v19

    invoke-virtual {v4, v0, v5, v7}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 914
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/provider/MusicSyncService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-static {}, Lcom/samsung/musicplus/provider/MusicContents$Audio$Playlists;->getContentUri()Landroid/net/Uri;

    move-result-object v5

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "_id= "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v4, v5, v7, v8}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 929
    .end local v19    # "uri":Landroid/net/Uri;
    :cond_5
    :goto_1
    if-eqz v18, :cond_1

    .line 930
    :try_start_4
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 936
    .end local v6    # "columns":[Ljava/lang/String;
    .end local v11    # "_id_idx":I
    .end local v12    # "_id":J
    .end local v15    # "date_modified":I
    .end local v16    # "date_modified_idx":I
    .end local v17    # "srcId_idx":I
    .end local v18    # "src_C":Landroid/database/Cursor;
    .end local v20    # "src_id":J
    :catchall_0
    move-exception v4

    if-eqz v14, :cond_6

    .line 937
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    :cond_6
    throw v4

    .line 918
    .restart local v6    # "columns":[Ljava/lang/String;
    .restart local v11    # "_id_idx":I
    .restart local v12    # "_id":J
    .restart local v15    # "date_modified":I
    .restart local v16    # "date_modified_idx":I
    .restart local v17    # "srcId_idx":I
    .restart local v18    # "src_C":Landroid/database/Cursor;
    .restart local v20    # "src_id":J
    :cond_7
    :try_start_5
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->moveToFirst()Z

    .line 919
    const/4 v4, 0x0

    move-object/from16 v0, v18

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    if-eq v4, v15, :cond_5

    .line 920
    invoke-static {v12, v13}, Lcom/samsung/musicplus/provider/MusicContents$Audio$Playlists$Members;->getContentUri(J)Landroid/net/Uri;

    move-result-object v19

    .line 921
    .restart local v19    # "uri":Landroid/net/Uri;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/provider/MusicSyncService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v7, 0x0

    move-object/from16 v0, v19

    invoke-virtual {v4, v0, v5, v7}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 922
    const/16 v4, 0x456

    move-wide/from16 v0, v20

    invoke-static {v4, v0, v1, v12, v13}, Lcom/samsung/musicplus/provider/MusicProvider;->makeHashTable(IJJ)V

    .line 924
    move-object/from16 v0, p0

    move-wide/from16 v1, v20

    invoke-direct {v0, v1, v2}, Lcom/samsung/musicplus/provider/MusicSyncService;->syncPlaylistMembers(J)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_1

    .line 929
    .end local v19    # "uri":Landroid/net/Uri;
    :catchall_1
    move-exception v4

    if-eqz v18, :cond_8

    .line 930
    :try_start_6
    invoke-interface/range {v18 .. v18}, Landroid/database/Cursor;->close()V

    :cond_8
    throw v4
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0
.end method

.method private syncPlaylistMembers(J)V
    .locals 23
    .param p1, "src_playlistId"    # J

    .prologue
    .line 708
    new-instance v22, Landroid/content/ContentValues;

    invoke-direct/range {v22 .. v22}, Landroid/content/ContentValues;-><init>()V

    .line 709
    .local v22, "values":Landroid/content/ContentValues;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/provider/MusicSyncService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 710
    .local v2, "cr":Landroid/content/ContentResolver;
    new-instance v14, Lcom/samsung/musicplus/provider/MediaInserter;

    const/16 v3, 0x1f4

    invoke-direct {v14, v2, v3}, Lcom/samsung/musicplus/provider/MediaInserter;-><init>(Landroid/content/ContentResolver;I)V

    .line 711
    .local v14, "inserter":Lcom/samsung/musicplus/provider/MediaInserter;
    const/4 v3, 0x3

    new-array v4, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v5, "audio_id"

    aput-object v5, v4, v3

    const/4 v3, 0x1

    const-string v5, "play_order"

    aput-object v5, v4, v3

    const/4 v3, 0x2

    const-string v5, "is_music"

    aput-object v5, v4, v3

    .line 722
    .local v4, "columns":[Ljava/lang/String;
    const/4 v9, 0x0

    .line 724
    .local v9, "c":Landroid/database/Cursor;
    :try_start_0
    const-string v3, "external"

    move-wide/from16 v0, p1

    invoke-static {v3, v0, v1}, Landroid/provider/MediaStore$Audio$Playlists$Members;->getContentUri(Ljava/lang/String;J)Landroid/net/Uri;

    move-result-object v3

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {v2 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v9

    .line 728
    if-nez v9, :cond_1

    .line 763
    if-eqz v9, :cond_0

    .line 764
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 765
    const/4 v9, 0x0

    .line 770
    :cond_0
    :goto_0
    return-void

    .line 732
    :cond_1
    :try_start_1
    sget-object v3, Lcom/samsung/musicplus/provider/MusicProvider;->sPlaylistsMapSyncTable:Ljava/util/HashMap;

    invoke-static/range {p1 .. p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    .line 733
    .local v16, "pId":J
    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    move-result v12

    .line 734
    .local v12, "count":I
    const-wide/16 v20, 0x0

    .line 735
    .local v20, "src_audioId":J
    const/4 v15, 0x0

    .line 736
    .local v15, "playOrder":I
    const-string v3, "audio_id"

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v18

    .line 737
    .local v18, "src_Idx":I
    const-string v3, "play_order"

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v19

    .line 739
    .local v19, "src_PlayOrder":I
    const-wide/16 v6, 0x0

    cmp-long v3, v16, v6

    if-gez v3, :cond_2

    .line 763
    if-eqz v9, :cond_0

    .line 764
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 765
    const/4 v9, 0x0

    goto :goto_0

    .line 747
    :cond_2
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_1
    if-ge v13, v12, :cond_4

    .line 748
    :try_start_2
    invoke-interface {v9, v13}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 749
    move/from16 v0, v18

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v20

    .line 750
    move/from16 v0, v19

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v15

    .line 752
    if-eqz v22, :cond_3

    .line 753
    invoke-virtual/range {v22 .. v22}, Landroid/content/ContentValues;->clear()V

    .line 756
    :cond_3
    const-string v3, "playlist_id"

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 757
    sget-object v3, Lcom/samsung/musicplus/provider/MusicProvider;->sAudioMapSyncTable:Ljava/util/HashMap;

    invoke-static/range {v20 .. v21}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    .line 758
    .local v10, "audioId":J
    const-string v3, "audio_id"

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 759
    const-string v3, "play_order"

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 760
    invoke-static/range {v16 .. v17}, Lcom/samsung/musicplus/provider/MusicContents$Audio$Playlists$Members;->getContentUri(J)Landroid/net/Uri;

    move-result-object v3

    move-object/from16 v0, v22

    invoke-virtual {v14, v3, v0}, Lcom/samsung/musicplus/provider/MediaInserter;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 747
    add-int/lit8 v13, v13, 0x1

    goto :goto_1

    .line 763
    .end local v10    # "audioId":J
    :cond_4
    if-eqz v9, :cond_5

    .line 764
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 765
    const/4 v9, 0x0

    .line 769
    :cond_5
    invoke-virtual {v14}, Lcom/samsung/musicplus/provider/MediaInserter;->flushAll()V

    goto/16 :goto_0

    .line 763
    .end local v12    # "count":I
    .end local v13    # "i":I
    .end local v15    # "playOrder":I
    .end local v16    # "pId":J
    .end local v18    # "src_Idx":I
    .end local v19    # "src_PlayOrder":I
    .end local v20    # "src_audioId":J
    :catchall_0
    move-exception v3

    if-eqz v9, :cond_6

    .line 764
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 765
    const/4 v9, 0x0

    :cond_6
    throw v3
.end method

.method private syncPlaylists(Ljava/lang/String;)V
    .locals 23
    .param p1, "where"    # Ljava/lang/String;

    .prologue
    .line 636
    const-string v3, "MusicSyncService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "syncPlaylists() : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 637
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/provider/MusicSyncService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 640
    .local v2, "cr":Landroid/content/ContentResolver;
    const/4 v14, 0x0

    .line 642
    .local v14, "c":Landroid/database/Cursor;
    :try_start_0
    sget-object v3, Landroid/provider/MediaStore$Audio$Playlists;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v4, 0x4

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "_id"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string v6, "name"

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string v6, "_data"

    aput-object v6, v4, v5

    const/4 v5, 0x3

    const-string v6, "date_modified"

    aput-object v6, v4, v5

    const/4 v6, 0x0

    const-string v7, "_id"

    const/4 v8, 0x0

    move-object/from16 v5, p1

    invoke-virtual/range {v2 .. v8}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v14

    .line 647
    if-nez v14, :cond_1

    .line 694
    if-eqz v14, :cond_0

    .line 695
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    .line 696
    const/4 v14, 0x0

    .line 700
    :cond_0
    :goto_0
    return-void

    .line 651
    :cond_1
    :try_start_1
    invoke-interface {v14}, Landroid/database/Cursor;->getCount()I

    move-result v20

    .line 652
    .local v20, "num":I
    move/from16 v0, v20

    new-array v0, v0, [J

    move-object/from16 v21, v0

    .line 653
    .local v21, "src_playlists":[J
    const-wide/16 v12, 0x0

    .line 654
    .local v12, "_id":J
    const/16 v18, 0x0

    .line 655
    .local v18, "name":Ljava/lang/String;
    const/4 v9, 0x0

    .line 656
    .local v9, "_data":Ljava/lang/String;
    const/4 v15, 0x0

    .line 657
    .local v15, "date_modified":I
    const-string v3, "_id"

    invoke-interface {v14, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v11

    .line 658
    .local v11, "_id_Idx":I
    const-string v3, "name"

    invoke-interface {v14, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v19

    .line 659
    .local v19, "name_Idx":I
    const-string v3, "_data"

    invoke-interface {v14, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v10

    .line 660
    .local v10, "_data_Idx":I
    const-string v3, "date_modified"

    invoke-interface {v14, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v16

    .line 662
    .local v16, "date_modified_Idx":I
    new-instance v22, Landroid/content/ContentValues;

    invoke-direct/range {v22 .. v22}, Landroid/content/ContentValues;-><init>()V

    .line 665
    .local v22, "values":Landroid/content/ContentValues;
    const/16 v17, 0x0

    .local v17, "i":I
    :goto_1
    move/from16 v0, v17

    move/from16 v1, v20

    if-ge v0, v1, :cond_5

    .line 666
    move/from16 v0, v17

    invoke-interface {v14, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 667
    invoke-interface {v14, v11}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    .line 668
    move/from16 v0, v19

    invoke-interface {v14, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 669
    invoke-interface {v14, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 670
    move/from16 v0, v16

    invoke-interface {v14, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v15

    .line 671
    aput-wide v12, v21, v17

    .line 673
    const-string v3, "Quick list"

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 674
    aput-wide v12, v21, v17

    .line 675
    if-eqz v22, :cond_2

    .line 676
    invoke-virtual/range {v22 .. v22}, Landroid/content/ContentValues;->clear()V

    .line 678
    :cond_2
    const-string v3, "source_id"

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 679
    const-string v3, "name"

    move-object/from16 v0, v22

    move-object/from16 v1, v18

    invoke-virtual {v0, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 680
    const-string v3, "_data"

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 681
    const-string v3, "date_modified"

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 682
    sget-object v3, Lcom/samsung/musicplus/provider/MusicContents$Audio$Playlists;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v3}, Lcom/samsung/musicplus/provider/MusicContents;->getNotifyDisabledUri(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v3

    move-object/from16 v0, v22

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 686
    move-object/from16 v0, p0

    invoke-direct {v0, v12, v13}, Lcom/samsung/musicplus/provider/MusicSyncService;->syncPlaylistMembers(J)V

    .line 665
    :goto_2
    add-int/lit8 v17, v17, 0x1

    goto :goto_1

    .line 689
    :cond_3
    move-object/from16 v0, p0

    invoke-direct {v0, v12, v13}, Lcom/samsung/musicplus/provider/MusicSyncService;->syncFavoriteList(J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 694
    .end local v9    # "_data":Ljava/lang/String;
    .end local v10    # "_data_Idx":I
    .end local v11    # "_id_Idx":I
    .end local v12    # "_id":J
    .end local v15    # "date_modified":I
    .end local v16    # "date_modified_Idx":I
    .end local v17    # "i":I
    .end local v18    # "name":Ljava/lang/String;
    .end local v19    # "name_Idx":I
    .end local v20    # "num":I
    .end local v21    # "src_playlists":[J
    .end local v22    # "values":Landroid/content/ContentValues;
    :catchall_0
    move-exception v3

    if-eqz v14, :cond_4

    .line 695
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    .line 696
    const/4 v14, 0x0

    :cond_4
    throw v3

    .line 694
    .restart local v9    # "_data":Ljava/lang/String;
    .restart local v10    # "_data_Idx":I
    .restart local v11    # "_id_Idx":I
    .restart local v12    # "_id":J
    .restart local v15    # "date_modified":I
    .restart local v16    # "date_modified_Idx":I
    .restart local v17    # "i":I
    .restart local v18    # "name":Ljava/lang/String;
    .restart local v19    # "name_Idx":I
    .restart local v20    # "num":I
    .restart local v21    # "src_playlists":[J
    .restart local v22    # "values":Landroid/content/ContentValues;
    :cond_5
    if-eqz v14, :cond_0

    .line 695
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    .line 696
    const/4 v14, 0x0

    goto/16 :goto_0
.end method

.method private syncPlaylistsFiles()V
    .locals 10

    .prologue
    .line 833
    const-string v0, "MusicSyncService"

    const-string v2, "syncPlaylistsFiles()"

    invoke-static {v0, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 834
    const/4 v7, 0x0

    .line 836
    .local v7, "c":Landroid/database/Cursor;
    const-wide/16 v8, 0x0

    .line 838
    .local v8, "maxId":J
    sget-object v0, Lcom/samsung/musicplus/provider/MusicContents$Audio$Playlists;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v2, "limit"

    const-string v3, "1"

    invoke-virtual {v0, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 841
    .local v1, "limitUri":Landroid/net/Uri;
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/provider/MusicSyncService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "source_id"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "source_id DESC"

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/database/Cursor;

    move-result-object v7

    .line 845
    if-eqz v7, :cond_0

    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eqz v0, :cond_0

    .line 846
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    .line 847
    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v8

    .line 851
    :cond_0
    if-eqz v7, :cond_1

    .line 852
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 853
    const/4 v7, 0x0

    .line 858
    :cond_1
    invoke-direct {p0, v8, v9}, Lcom/samsung/musicplus/provider/MusicSyncService;->syncAddedFiles(J)V

    .line 861
    invoke-direct {p0, v8, v9}, Lcom/samsung/musicplus/provider/MusicSyncService;->syncModifiedFiles(J)V

    .line 862
    return-void

    .line 851
    :catchall_0
    move-exception v0

    if-eqz v7, :cond_2

    .line 852
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 853
    const/4 v7, 0x0

    :cond_2
    throw v0
.end method

.method private updateMusicSquareCell()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 192
    invoke-virtual {p0}, Lcom/samsung/musicplus/provider/MusicSyncService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 193
    .local v0, "resolver":Landroid/content/ContentResolver;
    invoke-static {}, Lcom/samsung/musicplus/provider/MusicContents;->getMusicSquareCellUpdator()Landroid/net/Uri;

    move-result-object v1

    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    invoke-virtual {v0, v1, v2, v3, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 194
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 292
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 4

    .prologue
    .line 198
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 199
    const-string v2, "power"

    invoke-virtual {p0, v2}, Lcom/samsung/musicplus/provider/MusicSyncService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 200
    .local v0, "pm":Landroid/os/PowerManager;
    const/4 v2, 0x1

    const-string v3, "MusicSyncService"

    invoke-virtual {v0, v2, v3}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/musicplus/provider/MusicSyncService;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 206
    new-instance v1, Ljava/lang/Thread;

    const/4 v2, 0x0

    const-string v3, "MusicSyncService"

    invoke-direct {v1, v2, p0, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/ThreadGroup;Ljava/lang/Runnable;Ljava/lang/String;)V

    .line 207
    .local v1, "thr":Ljava/lang/Thread;
    invoke-virtual {v1}, Ljava/lang/Thread;->start()V

    .line 208
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 258
    :goto_0
    iget-object v0, p0, Lcom/samsung/musicplus/provider/MusicSyncService;->mServiceLooper:Landroid/os/Looper;

    if-nez v0, :cond_0

    .line 259
    monitor-enter p0

    .line 261
    const-wide/16 v0, 0x64

    :try_start_0
    invoke-virtual {p0, v0, v1}, Ljava/lang/Object;->wait(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 264
    :goto_1
    :try_start_1
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    .line 266
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/provider/MusicSyncService;->mServiceLooper:Landroid/os/Looper;

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 267
    return-void

    .line 262
    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 7
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    const/4 v2, 0x3

    const/4 v1, 0x2

    const/4 v6, 0x1

    .line 212
    :goto_0
    iget-object v3, p0, Lcom/samsung/musicplus/provider/MusicSyncService;->mServiceHandler:Lcom/samsung/musicplus/provider/MusicSyncService$ServiceHandler;

    if-nez v3, :cond_0

    .line 213
    monitor-enter p0

    .line 215
    const-wide/16 v4, 0x64

    :try_start_0
    invoke-virtual {p0, v4, v5}, Ljava/lang/Object;->wait(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 218
    :goto_1
    :try_start_1
    monitor-exit p0

    goto :goto_0

    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1

    .line 221
    :cond_0
    if-nez p1, :cond_1

    .line 222
    const-string v2, "MusicSyncService"

    const-string v3, "Intent is null in onStartCommand: "

    new-instance v4, Ljava/lang/NullPointerException;

    invoke-direct {v4}, Ljava/lang/NullPointerException;-><init>()V

    invoke-static {v2, v3, v4}, Lcom/samsung/musicplus/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 251
    :goto_2
    return v1

    .line 227
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "scan_type"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 229
    .local v0, "msg":I
    const-string v3, "MusicSyncService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onStartCommand() : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    iget-object v3, p0, Lcom/samsung/musicplus/provider/MusicSyncService;->mServiceHandler:Lcom/samsung/musicplus/provider/MusicSyncService$ServiceHandler;

    invoke-virtual {v3, v0}, Lcom/samsung/musicplus/provider/MusicSyncService$ServiceHandler;->hasMessages(I)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 231
    const-string v1, "MusicSyncService"

    const-string v3, "We receive a same request for syncing media. It will be ignored."

    invoke-static {v1, v3}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v2

    .line 232
    goto :goto_2

    .line 234
    :cond_2
    packed-switch v0, :pswitch_data_0

    .line 250
    :pswitch_0
    iget-object v1, p0, Lcom/samsung/musicplus/provider/MusicSyncService;->mServiceHandler:Lcom/samsung/musicplus/provider/MusicSyncService$ServiceHandler;

    invoke-virtual {v1, v0}, Lcom/samsung/musicplus/provider/MusicSyncService$ServiceHandler;->sendEmptyMessage(I)Z

    move v1, v2

    .line 251
    goto :goto_2

    .line 236
    :pswitch_1
    iput-boolean v6, p0, Lcom/samsung/musicplus/provider/MusicSyncService;->mAfterMountedEvent:Z

    goto :goto_2

    .line 239
    :pswitch_2
    iget-object v2, p0, Lcom/samsung/musicplus/provider/MusicSyncService;->mServiceHandler:Lcom/samsung/musicplus/provider/MusicSyncService$ServiceHandler;

    invoke-virtual {v2, v6}, Lcom/samsung/musicplus/provider/MusicSyncService$ServiceHandler;->removeMessages(I)V

    .line 240
    iget-object v2, p0, Lcom/samsung/musicplus/provider/MusicSyncService;->mServiceHandler:Lcom/samsung/musicplus/provider/MusicSyncService$ServiceHandler;

    invoke-virtual {v2, v0}, Lcom/samsung/musicplus/provider/MusicSyncService$ServiceHandler;->sendEmptyMessage(I)Z

    goto :goto_2

    .line 243
    :pswitch_3
    sput-boolean v6, Lcom/samsung/musicplus/provider/MusicSyncService;->sIsShutdown:Z

    goto :goto_2

    .line 246
    :pswitch_4
    iput-boolean v6, p0, Lcom/samsung/musicplus/provider/MusicSyncService;->mPendingSyncAllPlayList:Z

    .line 247
    iget-object v1, p0, Lcom/samsung/musicplus/provider/MusicSyncService;->mServiceHandler:Lcom/samsung/musicplus/provider/MusicSyncService$ServiceHandler;

    invoke-virtual {v1, v0}, Lcom/samsung/musicplus/provider/MusicSyncService$ServiceHandler;->sendEmptyMessage(I)Z

    move v1, v2

    .line 248
    goto :goto_2

    .line 216
    .end local v0    # "msg":I
    :catch_0
    move-exception v3

    goto :goto_1

    .line 234
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_4
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public run()V
    .locals 2

    .prologue
    .line 272
    const/16 v0, 0xb

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    .line 274
    invoke-static {}, Landroid/os/Looper;->prepare()V

    .line 276
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/provider/MusicSyncService;->mServiceLooper:Landroid/os/Looper;

    .line 277
    new-instance v0, Lcom/samsung/musicplus/provider/MusicSyncService$ServiceHandler;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/samsung/musicplus/provider/MusicSyncService$ServiceHandler;-><init>(Lcom/samsung/musicplus/provider/MusicSyncService;Lcom/samsung/musicplus/provider/MusicSyncService$1;)V

    iput-object v0, p0, Lcom/samsung/musicplus/provider/MusicSyncService;->mServiceHandler:Lcom/samsung/musicplus/provider/MusicSyncService$ServiceHandler;

    .line 279
    invoke-static {}, Landroid/os/Looper;->loop()V

    .line 280
    return-void
.end method
