.class Lcom/samsung/musicplus/provider/MusicProvider$3;
.super Landroid/content/BroadcastReceiver;
.source "MusicProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/provider/MusicProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/provider/MusicProvider;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/provider/MusicProvider;)V
    .locals 0

    .prologue
    .line 1165
    iput-object p1, p0, Lcom/samsung/musicplus/provider/MusicProvider$3;->this$0:Lcom/samsung/musicplus/provider/MusicProvider;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 1168
    # getter for: Lcom/samsung/musicplus/provider/MusicProvider;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/provider/MusicProvider;->access$300()Ljava/lang/String;

    move-result-object v0

    const-string v1, "mShutdownReceiver - for stopping Mood Parser"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1169
    const-string v0, "android.intent.action.ACTION_SHUTDOWN"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1170
    iget-object v0, p0, Lcom/samsung/musicplus/provider/MusicProvider$3;->this$0:Lcom/samsung/musicplus/provider/MusicProvider;

    invoke-virtual {v0}, Lcom/samsung/musicplus/provider/MusicProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/musicplus/provider/MusicProvider$3;->this$0:Lcom/samsung/musicplus/provider/MusicProvider;

    # getter for: Lcom/samsung/musicplus/provider/MusicProvider;->mSrcProviderObserver:Landroid/database/ContentObserver;
    invoke-static {v1}, Lcom/samsung/musicplus/provider/MusicProvider;->access$500(Lcom/samsung/musicplus/provider/MusicProvider;)Landroid/database/ContentObserver;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 1171
    iget-object v0, p0, Lcom/samsung/musicplus/provider/MusicProvider$3;->this$0:Lcom/samsung/musicplus/provider/MusicProvider;

    # getter for: Lcom/samsung/musicplus/provider/MusicProvider;->mMoodUpdateHandler:Lcom/samsung/musicplus/provider/MusicProvider$MoodUpdateHandler;
    invoke-static {v0}, Lcom/samsung/musicplus/provider/MusicProvider;->access$600(Lcom/samsung/musicplus/provider/MusicProvider;)Lcom/samsung/musicplus/provider/MusicProvider$MoodUpdateHandler;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/provider/MusicProvider$3;->this$0:Lcom/samsung/musicplus/provider/MusicProvider;

    # getter for: Lcom/samsung/musicplus/provider/MusicProvider;->mMoodUpdateHandler:Lcom/samsung/musicplus/provider/MusicProvider$MoodUpdateHandler;
    invoke-static {v0}, Lcom/samsung/musicplus/provider/MusicProvider;->access$600(Lcom/samsung/musicplus/provider/MusicProvider;)Lcom/samsung/musicplus/provider/MusicProvider$MoodUpdateHandler;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/musicplus/provider/MusicProvider$MoodUpdateHandler;->isRunningMoodUpdate()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1172
    # getter for: Lcom/samsung/musicplus/provider/MusicProvider;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/provider/MusicProvider;->access$300()Ljava/lang/String;

    move-result-object v0

    const-string v1, "mMoodUpdateHandler.cancel()"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1173
    iget-object v0, p0, Lcom/samsung/musicplus/provider/MusicProvider$3;->this$0:Lcom/samsung/musicplus/provider/MusicProvider;

    # getter for: Lcom/samsung/musicplus/provider/MusicProvider;->mMoodUpdateHandler:Lcom/samsung/musicplus/provider/MusicProvider$MoodUpdateHandler;
    invoke-static {v0}, Lcom/samsung/musicplus/provider/MusicProvider;->access$600(Lcom/samsung/musicplus/provider/MusicProvider;)Lcom/samsung/musicplus/provider/MusicProvider$MoodUpdateHandler;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/musicplus/provider/MusicProvider$MoodUpdateHandler;->cancel()V

    .line 1177
    :cond_0
    return-void
.end method
