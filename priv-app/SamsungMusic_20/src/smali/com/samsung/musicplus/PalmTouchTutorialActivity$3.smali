.class Lcom/samsung/musicplus/PalmTouchTutorialActivity$3;
.super Ljava/lang/Object;
.source "PalmTouchTutorialActivity.java"

# interfaces
.implements Landroid/media/AudioManager$OnAudioFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/PalmTouchTutorialActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/PalmTouchTutorialActivity;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/PalmTouchTutorialActivity;)V
    .locals 0

    .prologue
    .line 260
    iput-object p1, p0, Lcom/samsung/musicplus/PalmTouchTutorialActivity$3;->this$0:Lcom/samsung/musicplus/PalmTouchTutorialActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAudioFocusChange(I)V
    .locals 3
    .param p1, "focusChange"    # I

    .prologue
    .line 263
    packed-switch p1, :pswitch_data_0

    .line 272
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/musicplus/PalmTouchTutorialActivity$3;->this$0:Lcom/samsung/musicplus/PalmTouchTutorialActivity;

    iget-object v0, v0, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown audio focus change code,"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    :goto_0
    :pswitch_1
    return-void

    .line 267
    :pswitch_2
    iget-object v0, p0, Lcom/samsung/musicplus/PalmTouchTutorialActivity$3;->this$0:Lcom/samsung/musicplus/PalmTouchTutorialActivity;

    # invokes: Lcom/samsung/musicplus/PalmTouchTutorialActivity;->stopSound()V
    invoke-static {v0}, Lcom/samsung/musicplus/PalmTouchTutorialActivity;->access$100(Lcom/samsung/musicplus/PalmTouchTutorialActivity;)V

    goto :goto_0

    .line 263
    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
