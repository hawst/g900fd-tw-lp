.class Lcom/samsung/musicplus/contents/square/MusicSquareFragment$3;
.super Ljava/lang/Object;
.source "MusicSquareFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->ensureSquareView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;)V
    .locals 0

    .prologue
    .line 354
    iput-object p1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$3;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p2, "paramView"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 357
    .local p1, "paramAdapterView":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    if-ltz p3, :cond_1

    .line 358
    iget-object v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$3;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    iget-object v1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$3;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    # getter for: Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSquareGridView:Landroid/widget/GridView;
    invoke-static {v1}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->access$100(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;)Landroid/widget/GridView;

    move-result-object v1

    # invokes: Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->clearAllMusicSquareCell(Landroid/widget/GridView;)V
    invoke-static {v0, v1}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->access$200(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;Landroid/widget/GridView;)V

    .line 359
    iget-object v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$3;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    iget-object v1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$3;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    # getter for: Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSquareGridView:Landroid/widget/GridView;
    invoke-static {v1}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->access$100(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;)Landroid/widget/GridView;

    move-result-object v1

    # invokes: Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->selectMusicSquareCell(Landroid/widget/GridView;I)V
    invoke-static {v0, v1, p3}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->access$300(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;Landroid/widget/GridView;I)V

    .line 360
    iget-object v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$3;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    # getter for: Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mIsOnResumed:Z
    invoke-static {v0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->access$400(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 361
    iget-object v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$3;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    # invokes: Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->doPlaySelectedMusicSquareCell()Z
    invoke-static {v0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->access$500(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;)Z

    .line 363
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$3;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    # invokes: Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->saveSelectedCellPref()V
    invoke-static {v0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->access$600(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;)V

    .line 365
    :cond_1
    return-void
.end method
