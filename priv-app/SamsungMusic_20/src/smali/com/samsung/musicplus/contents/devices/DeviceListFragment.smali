.class public Lcom/samsung/musicplus/contents/devices/DeviceListFragment;
.super Lcom/samsung/musicplus/widget/fragment/CommonListFragment;
.source "DeviceListFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader;,
        Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;,
        Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DataPopup;
    }
.end annotation


# static fields
.field private static final CATEGORY_NEAR_BY_DEVICE:J = -0x12L

.field private static final CATEGORY_NO_ITEM:J = -0x14L

.field private static final CATEGORY_NO_ITEM_GO_TO_SLINK:J = -0x16L

.field private static final CATEGORY_NO_ITEM_HOW_TO_REGISTER:J = -0x17L

.field private static final CATEGORY_NO_ITEM_WITH_ICON:J = -0x15L

.field private static final CATEGORY_SLINK_DEVICE:J = -0x13L

.field public static final FEATURE_SLINK_REFRESH:Z = true

.field private static final FIRST_CATEGORY_ITEM:J = -0x12L

.field private static final LAST_CATEGORY_ITEM:J = -0x17L

.field public static final SUPPORT_DEVICE_TAB:Z = true

.field private static final SUPPORT_DLNA_DMS:Z

.field private static final SUPPORT_SLINK_DEVICE_ICON:Z = true

.field private static final TAG:Ljava/lang/String; = "MusicUiDeviceTab"

.field private static sEachDevicesPopupValue:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mCountLoader:Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader;

.field private mDataPopupType:I

.field private mDevice:Landroid/database/Cursor;

.field private mDeviceCount:I

.field private mDeviceIds:[J

.field private mDeviceStateReceiver:Landroid/content/BroadcastReceiver;

.field private mDms:Landroid/database/Cursor;

.field private mDmsCount:I

.field private mEnableSlink:Ljava/lang/Boolean;

.field private mHasSamsungAccount:Z

.field private mIsLogin:Z

.field private mLastLoadCompleteTime:J

.field private final mListUpdater:Landroid/os/Handler;

.field private mMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/view/View;",
            "Landroid/database/ContentObserver;",
            ">;"
        }
    .end annotation
.end field

.field private mUpdateNotifier:Ljava/lang/Runnable;

.field private mUpdateThrottle:I

.field private mWaiting:Z

.field private mWifiConnected:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 150
    sget-boolean v0, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_SUPPORT_DLNA_DMC_ONLY:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->SUPPORT_DLNA_DMS:Z

    .line 602
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->sEachDevicesPopupValue:Ljava/util/ArrayList;

    return-void

    .line 150
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 94
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;-><init>()V

    .line 110
    iput-boolean v2, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mWifiConnected:Z

    .line 133
    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mDataPopupType:I

    .line 165
    iput v2, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mDeviceCount:I

    .line 170
    iput v2, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mDmsCount:I

    .line 248
    new-instance v0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$1;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$1;-><init>(Lcom/samsung/musicplus/contents/devices/DeviceListFragment;)V

    iput-object v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mDeviceStateReceiver:Landroid/content/BroadcastReceiver;

    .line 405
    iput-boolean v2, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mIsLogin:Z

    .line 407
    iput-boolean v2, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mHasSamsungAccount:Z

    .line 1147
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mMap:Ljava/util/HashMap;

    .line 1155
    new-instance v0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$2;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$2;-><init>(Lcom/samsung/musicplus/contents/devices/DeviceListFragment;)V

    iput-object v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mListUpdater:Landroid/os/Handler;

    .line 1162
    const/16 v0, 0xfa0

    iput v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mUpdateThrottle:I

    .line 1164
    const-wide/16 v0, -0x2710

    iput-wide v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mLastLoadCompleteTime:J

    .line 1166
    iput-boolean v2, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mWaiting:Z

    .line 1168
    new-instance v0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$3;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$3;-><init>(Lcom/samsung/musicplus/contents/devices/DeviceListFragment;)V

    iput-object v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mUpdateNotifier:Ljava/lang/Runnable;

    .line 1211
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/contents/devices/DeviceListFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/devices/DeviceListFragment;

    .prologue
    .line 94
    iget-boolean v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mWifiConnected:Z

    return v0
.end method

.method static synthetic access$002(Lcom/samsung/musicplus/contents/devices/DeviceListFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/devices/DeviceListFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 94
    iput-boolean p1, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mWifiConnected:Z

    return p1
.end method

.method static synthetic access$100(Lcom/samsung/musicplus/contents/devices/DeviceListFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/devices/DeviceListFragment;

    .prologue
    .line 94
    iget-boolean v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mIsLogin:Z

    return v0
.end method

.method static synthetic access$102(Lcom/samsung/musicplus/contents/devices/DeviceListFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/devices/DeviceListFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 94
    iput-boolean p1, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mIsLogin:Z

    return p1
.end method

.method static synthetic access$1300(Lcom/samsung/musicplus/contents/devices/DeviceListFragment;)[J
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/devices/DeviceListFragment;

    .prologue
    .line 94
    iget-object v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mDeviceIds:[J

    return-object v0
.end method

.method static synthetic access$1600(Lcom/samsung/musicplus/contents/devices/DeviceListFragment;Landroid/widget/TextView;Landroid/content/Context;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/devices/DeviceListFragment;
    .param p1, "x1"    # Landroid/widget/TextView;
    .param p2, "x2"    # Landroid/content/Context;
    .param p3, "x3"    # I

    .prologue
    .line 94
    invoke-virtual {p0, p1, p2, p3}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->setNumberOfTextView(Landroid/widget/TextView;Landroid/content/Context;I)V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/musicplus/contents/devices/DeviceListFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/devices/DeviceListFragment;

    .prologue
    .line 94
    iget-boolean v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mHasSamsungAccount:Z

    return v0
.end method

.method static synthetic access$2000(Lcom/samsung/musicplus/contents/devices/DeviceListFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/devices/DeviceListFragment;

    .prologue
    .line 94
    iget-boolean v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mIsStop:Z

    return v0
.end method

.method static synthetic access$202(Lcom/samsung/musicplus/contents/devices/DeviceListFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/devices/DeviceListFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 94
    iput-boolean p1, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mHasSamsungAccount:Z

    return p1
.end method

.method static synthetic access$2100(Lcom/samsung/musicplus/contents/devices/DeviceListFragment;)Lcom/samsung/musicplus/util/ListUtils$QueryArgs;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/devices/DeviceListFragment;

    .prologue
    .line 94
    iget-object v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mQueryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/samsung/musicplus/contents/devices/DeviceListFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/devices/DeviceListFragment;

    .prologue
    .line 94
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->invalidateListViews()V

    return-void
.end method

.method static synthetic access$2300(Lcom/samsung/musicplus/contents/devices/DeviceListFragment;)J
    .locals 2
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/devices/DeviceListFragment;

    .prologue
    .line 94
    iget-wide v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mLastLoadCompleteTime:J

    return-wide v0
.end method

.method static synthetic access$2302(Lcom/samsung/musicplus/contents/devices/DeviceListFragment;J)J
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/devices/DeviceListFragment;
    .param p1, "x1"    # J

    .prologue
    .line 94
    iput-wide p1, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mLastLoadCompleteTime:J

    return-wide p1
.end method

.method static synthetic access$2400(Lcom/samsung/musicplus/contents/devices/DeviceListFragment;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/devices/DeviceListFragment;

    .prologue
    .line 94
    iget-object v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mListUpdater:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/samsung/musicplus/contents/devices/DeviceListFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/devices/DeviceListFragment;

    .prologue
    .line 94
    iget-boolean v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mWaiting:Z

    return v0
.end method

.method static synthetic access$2502(Lcom/samsung/musicplus/contents/devices/DeviceListFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/devices/DeviceListFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 94
    iput-boolean p1, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mWaiting:Z

    return p1
.end method

.method static synthetic access$2600(Lcom/samsung/musicplus/contents/devices/DeviceListFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/devices/DeviceListFragment;

    .prologue
    .line 94
    iget v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mUpdateThrottle:I

    return v0
.end method

.method static synthetic access$2700(Lcom/samsung/musicplus/contents/devices/DeviceListFragment;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/devices/DeviceListFragment;

    .prologue
    .line 94
    iget-object v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mUpdateNotifier:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/musicplus/contents/devices/DeviceListFragment;I)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/devices/DeviceListFragment;
    .param p1, "x1"    # I

    .prologue
    .line 94
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->isDmsItem(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/samsung/musicplus/contents/devices/DeviceListFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/devices/DeviceListFragment;

    .prologue
    .line 94
    iget v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mDmsCount:I

    return v0
.end method

.method static synthetic access$600(Lcom/samsung/musicplus/contents/devices/DeviceListFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/devices/DeviceListFragment;

    .prologue
    .line 94
    iget v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mDeviceCount:I

    return v0
.end method

.method static synthetic access$700(Lcom/samsung/musicplus/contents/devices/DeviceListFragment;Landroid/widget/TextView;Landroid/content/Context;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/devices/DeviceListFragment;
    .param p1, "x1"    # Landroid/widget/TextView;
    .param p2, "x2"    # Landroid/content/Context;
    .param p3, "x3"    # I

    .prologue
    .line 94
    invoke-virtual {p0, p1, p2, p3}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->setNumberOfTextView(Landroid/widget/TextView;Landroid/content/Context;I)V

    return-void
.end method

.method private addObserver(Landroid/content/Context;Landroid/widget/TextView;J)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "text"    # Landroid/widget/TextView;
    .param p3, "id"    # J

    .prologue
    .line 1181
    iget-object v3, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mMap:Ljava/util/HashMap;

    invoke-virtual {v3, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/database/ContentObserver;

    .line 1182
    .local v1, "oldObserver":Landroid/database/ContentObserver;
    if-eqz v1, :cond_0

    .line 1183
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 1186
    :cond_0
    new-instance v0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$4;

    new-instance v3, Landroid/os/Handler;

    invoke-direct {v3}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v3}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$4;-><init>(Lcom/samsung/musicplus/contents/devices/DeviceListFragment;Landroid/os/Handler;)V

    .line 1203
    .local v0, "observer":Landroid/database/ContentObserver;
    invoke-static {p3, p4}, Lcom/samsung/musicplus/provider/MusicContents$Audio$Devices;->getContentsUri(J)Landroid/net/Uri;

    move-result-object v2

    .line 1204
    .local v2, "uri":Landroid/net/Uri;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v2, v4, v0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 1205
    iget-object v3, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mMap:Ljava/util/HashMap;

    invoke-virtual {v3, p2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1206
    return-void
.end method

.method private disableAddToFavorite(Landroid/view/Menu;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 1473
    const v1, 0x7f0d01d2

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 1474
    .local v0, "addToFavorite":Landroid/view/MenuItem;
    if-eqz v0, :cond_0

    .line 1475
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1477
    :cond_0
    return-void
.end method

.method private disableAddToPlaylistOption(Landroid/view/Menu;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 1459
    const v1, 0x7f0d01cd

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 1460
    .local v0, "addToPlaylist":Landroid/view/MenuItem;
    if-eqz v0, :cond_0

    .line 1461
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1463
    :cond_0
    return-void
.end method

.method private disableAddToSecretBoxOption(Landroid/view/Menu;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 1452
    const v1, 0x7f0d01d6

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 1453
    .local v0, "addToSecretBox":Landroid/view/MenuItem;
    if-eqz v0, :cond_0

    .line 1454
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1456
    :cond_0
    return-void
.end method

.method private disableDeleteOption(Landroid/view/Menu;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 1466
    const v1, 0x7f0d01d0

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 1467
    .local v0, "delete":Landroid/view/MenuItem;
    if-eqz v0, :cond_0

    .line 1468
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1470
    :cond_0
    return-void
.end method

.method private disableRemoveFromSecretBoxOption(Landroid/view/Menu;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 1445
    const v1, 0x7f0d01d7

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 1446
    .local v0, "removeFromSecretBox":Landroid/view/MenuItem;
    if-eqz v0, :cond_0

    .line 1447
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1449
    :cond_0
    return-void
.end method

.method private disableRemoveOption(Landroid/view/Menu;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 1487
    const v1, 0x7f0d01cf

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 1488
    .local v0, "remove":Landroid/view/MenuItem;
    if-eqz v0, :cond_0

    .line 1489
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1491
    :cond_0
    return-void
.end method

.method private disableSetAsOption(Landroid/view/Menu;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 1480
    const v1, 0x7f0d01a3

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 1481
    .local v0, "setAs":Landroid/view/MenuItem;
    if-eqz v0, :cond_0

    .line 1482
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 1484
    :cond_0
    return-void
.end method

.method private getDeviceCount(Landroid/database/Cursor;)I
    .locals 1
    .param p1, "data"    # Landroid/database/Cursor;

    .prologue
    .line 483
    if-nez p1, :cond_0

    .line 484
    const/4 v0, 0x0

    .line 486
    :goto_0
    return v0

    :cond_0
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    goto :goto_0
.end method

.method private getDeviceCursor(Landroid/database/Cursor;IIJ)Landroid/database/Cursor;
    .locals 0
    .param p1, "data"    # Landroid/database/Cursor;
    .param p2, "count"    # I
    .param p3, "stringId"    # I
    .param p4, "category"    # J

    .prologue
    .line 475
    if-nez p2, :cond_0

    .line 476
    invoke-direct {p0, p3, p4, p5}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mergeNoItem(IJ)Landroid/database/Cursor;

    move-result-object p1

    .line 478
    .end local p1    # "data":Landroid/database/Cursor;
    :cond_0
    return-object p1
.end method

.method private getDeviceId(I)Ljava/lang/String;
    .locals 6
    .param p1, "position"    # I

    .prologue
    .line 491
    const-string v3, ""

    .line 492
    .local v3, "keyWord":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v5

    invoke-interface {v5, p1}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 493
    .local v0, "c":Landroid/database/Cursor;
    if-nez v0, :cond_0

    move-object v4, v3

    .line 508
    .end local v3    # "keyWord":Ljava/lang/String;
    .local v4, "keyWord":Ljava/lang/String;
    :goto_0
    return-object v4

    .line 498
    .end local v4    # "keyWord":Ljava/lang/String;
    .restart local v3    # "keyWord":Ljava/lang/String;
    :cond_0
    const-string v5, "_id"

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 499
    .local v2, "index":I
    const/4 v5, -0x1

    if-le v2, v5, :cond_1

    .line 500
    invoke-interface {v0}, Landroid/database/Cursor;->getColumnCount()I

    move-result v5

    if-ge v2, v5, :cond_1

    .line 502
    :try_start_0
    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    :cond_1
    :goto_1
    move-object v4, v3

    .line 508
    .end local v3    # "keyWord":Ljava/lang/String;
    .restart local v4    # "keyWord":Ljava/lang/String;
    goto :goto_0

    .line 503
    .end local v4    # "keyWord":Ljava/lang/String;
    .restart local v3    # "keyWord":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 504
    .local v1, "e":Ljava/lang/IndexOutOfBoundsException;
    invoke-virtual {v1}, Ljava/lang/IndexOutOfBoundsException;->printStackTrace()V

    goto :goto_1
.end method

.method private getDeviceIds(Landroid/database/Cursor;)[J
    .locals 6
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    const/4 v2, 0x0

    .line 512
    if-nez p1, :cond_1

    .line 525
    :cond_0
    return-object v2

    .line 516
    :cond_1
    const-string v4, "_id"

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 517
    .local v3, "index":I
    const/4 v4, -0x1

    if-le v3, v4, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->getColumnCount()I

    move-result v4

    if-ge v3, v4, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 518
    const/4 v0, 0x0

    .line 519
    .local v0, "i":I
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v4

    new-array v2, v4, [J

    .line 521
    .local v2, "ids":[J
    :goto_0
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "i":I
    .local v1, "i":I
    invoke-interface {p1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    aput-wide v4, v2, v0

    .line 522
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_0

    move v0, v1

    .end local v1    # "i":I
    .restart local v0    # "i":I
    goto :goto_0
.end method

.method private getDeviceString(I)Ljava/lang/String;
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 529
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    invoke-interface {v1, p1}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 530
    .local v0, "c":Landroid/database/Cursor;
    invoke-static {v0}, Lcom/samsung/musicplus/provider/MusicContents$Audio$Devices;->getName(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private getSlinkDeviceCursorLoader()Landroid/content/CursorLoader;
    .locals 7

    .prologue
    .line 311
    new-instance v0, Landroid/content/CursorLoader;

    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lcom/samsung/musicplus/provider/MusicContents$Audio$Devices;->CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x0

    const-string v4, "transport_type == ? AND transport_type != ? AND physical_type != ?"

    sget-object v5, Lcom/samsung/musicplus/provider/MusicContents$Audio$Devices;->SELECTION_ARGS:[Ljava/lang/String;

    const-string v6, "CASE WHEN (network_mode != \'OFF\') THEN 1 ELSE 2 END, transport_type, network_mode DESC"

    invoke-direct/range {v0 .. v6}, Landroid/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 315
    .local v0, "cl":Landroid/content/CursorLoader;
    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v2, v3}, Landroid/content/CursorLoader;->setUpdateThrottle(J)V

    .line 316
    return-object v0
.end method

.method private invalidateListViews()V
    .locals 1

    .prologue
    .line 208
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->getAbsListView()Landroid/widget/AbsListView;

    move-result-object v0

    .line 209
    .local v0, "lv":Landroid/widget/AbsListView;
    if-eqz v0, :cond_0

    .line 210
    invoke-virtual {v0}, Landroid/widget/AbsListView;->invalidateViews()V

    .line 212
    :cond_0
    return-void
.end method

.method private isDeviceDisabled(I)Z
    .locals 3
    .param p1, "position"    # I

    .prologue
    const/4 v1, 0x1

    .line 534
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v2

    invoke-interface {v2, p1}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 535
    .local v0, "c":Landroid/database/Cursor;
    if-eqz v0, :cond_0

    .line 536
    invoke-static {v0}, Lcom/samsung/musicplus/provider/MusicContents$Audio$Devices;->isWebStorage(Landroid/database/Cursor;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-static {v0}, Lcom/samsung/musicplus/provider/MusicContents$Audio$Devices;->isOff(Landroid/database/Cursor;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 538
    :cond_0
    :goto_0
    return v1

    .line 536
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private isDmsItem(I)Z
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 629
    iget v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mDeviceCount:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mDeviceCount:I

    add-int/lit8 v0, v0, 0x1

    if-lt p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isNeedDataPopup(Ljava/lang/String;)Z
    .locals 3
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 605
    iget v2, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mDataPopupType:I

    packed-switch v2, :pswitch_data_0

    move v0, v1

    .line 624
    :cond_0
    :goto_0
    :pswitch_0
    return v0

    .line 609
    :pswitch_1
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/musicplus/dialog/SlinkDataDialog;->isUserAcceptWarning(Landroid/app/Activity;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    .line 610
    goto :goto_0

    .line 615
    :pswitch_2
    sget-object v2, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->sEachDevicesPopupValue:Ljava/util/ArrayList;

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    move v0, v1

    .line 616
    goto :goto_0

    .line 618
    :cond_1
    sget-object v1, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->sEachDevicesPopupValue:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 605
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private isSupportSlink(Landroid/content/Context;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 141
    iget-object v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mEnableSlink:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 142
    iget-object v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mEnableSlink:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 144
    :goto_0
    return v0

    :cond_0
    invoke-static {p1}, Lcom/samsung/musicplus/provider/MusicContents$Audio$Devices;->isSupportSlink(Landroid/content/Context;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mEnableSlink:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0
.end method

.method public static launchTrackActivity(Landroid/app/Activity;Ljava/lang/String;ILjava/lang/String;)V
    .locals 2
    .param p0, "a"    # Landroid/app/Activity;
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "trackList"    # I
    .param p3, "keyWord"    # Ljava/lang/String;

    .prologue
    .line 633
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/samsung/musicplus/contents/TrackActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 635
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "list_type"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 636
    const-string v1, "keyword"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 637
    const-string v1, "track_title"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 638
    invoke-virtual {p0, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 639
    return-void
.end method

.method private mergeCursors(Landroid/database/Cursor;Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 3
    .param p1, "c1"    # Landroid/database/Cursor;
    .param p2, "c2"    # Landroid/database/Cursor;

    .prologue
    .line 327
    if-nez p1, :cond_1

    if-nez p2, :cond_1

    .line 328
    const/4 p2, 0x0

    .line 336
    .end local p2    # "c2":Landroid/database/Cursor;
    :cond_0
    :goto_0
    return-object p2

    .line 330
    .restart local p2    # "c2":Landroid/database/Cursor;
    :cond_1
    if-nez p1, :cond_2

    if-nez p2, :cond_0

    .line 333
    :cond_2
    if-eqz p1, :cond_3

    if-nez p2, :cond_3

    move-object p2, p1

    .line 334
    goto :goto_0

    .line 336
    :cond_3
    new-instance v0, Lcom/samsung/musicplus/contents/devices/DeviceMergeCursor;

    const/4 v1, 0x2

    new-array v1, v1, [Landroid/database/Cursor;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    aput-object p2, v1, v2

    invoke-direct {v0, v1}, Lcom/samsung/musicplus/contents/devices/DeviceMergeCursor;-><init>([Landroid/database/Cursor;)V

    move-object p2, v0

    goto :goto_0
.end method

.method private mergeNoItem(IJ)Landroid/database/Cursor;
    .locals 6
    .param p1, "stringId"    # I
    .param p2, "category"    # J

    .prologue
    .line 366
    const/4 v3, 0x3

    new-array v2, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string v4, "title"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string v4, "dummy_column"

    aput-object v4, v2, v3

    .line 370
    .local v2, "projection":[Ljava/lang/String;
    new-instance v1, Landroid/database/MatrixCursor;

    invoke-direct {v1, v2}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 371
    .local v1, "noItemCursor":Landroid/database/MatrixCursor;
    new-instance v0, Ljava/util/ArrayList;

    array-length v3, v2

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 372
    .local v0, "noItem":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 375
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 376
    const-string v3, "@"

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 377
    invoke-virtual {v1, v0}, Landroid/database/MatrixCursor;->addRow(Ljava/lang/Iterable;)V

    .line 378
    return-object v1
.end method

.method private mergeTitle(Landroid/database/Cursor;IJ)Landroid/database/Cursor;
    .locals 7
    .param p1, "c"    # Landroid/database/Cursor;
    .param p2, "stringId"    # I
    .param p3, "category"    # J

    .prologue
    const/4 v4, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 350
    const/4 v3, 0x3

    new-array v0, v3, [Ljava/lang/String;

    const-string v3, "_id"

    aput-object v3, v0, v5

    const-string v3, "title"

    aput-object v3, v0, v6

    const-string v3, "dummy_column"

    aput-object v3, v0, v4

    .line 353
    .local v0, "projection":[Ljava/lang/String;
    new-instance v2, Landroid/database/MatrixCursor;

    invoke-direct {v2, v0}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 354
    .local v2, "titleCursor":Landroid/database/MatrixCursor;
    new-instance v1, Ljava/util/ArrayList;

    array-length v3, v0

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 355
    .local v1, "title":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 356
    invoke-virtual {p0, p2}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 357
    const-string v3, "@"

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 358
    invoke-virtual {v2, v1}, Landroid/database/MatrixCursor;->addRow(Ljava/lang/Iterable;)V

    .line 360
    new-instance v3, Lcom/samsung/musicplus/contents/devices/DeviceMergeCursor;

    new-array v4, v4, [Landroid/database/Cursor;

    aput-object v2, v4, v5

    aput-object p1, v4, v6

    invoke-direct {v3, v4}, Lcom/samsung/musicplus/contents/devices/DeviceMergeCursor;-><init>([Landroid/database/Cursor;)V

    return-object v3
.end method

.method private showSlinkDataDialog(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "keyWord"    # Ljava/lang/String;
    .param p2, "title"    # Ljava/lang/String;

    .prologue
    .line 593
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "slink_data"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    if-nez v1, :cond_0

    .line 594
    invoke-static {p2, p1}, Lcom/samsung/musicplus/dialog/SlinkDataDialog;->getInstance(Ljava/lang/String;Ljava/lang/String;)Landroid/app/DialogFragment;

    move-result-object v0

    .line 595
    .local v0, "fg":Landroid/app/DialogFragment;
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "slink_data"

    invoke-virtual {v0, v1, v2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 597
    .end local v0    # "fg":Landroid/app/DialogFragment;
    :cond_0
    return-void
.end method


# virtual methods
.method protected createAdapter()Landroid/widget/CursorAdapter;
    .locals 6

    .prologue
    .line 179
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    .line 180
    .local v2, "a":Landroid/app/Activity;
    new-instance v0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;

    const v3, 0x7f040036

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;-><init>(Lcom/samsung/musicplus/contents/devices/DeviceListFragment;Landroid/content/Context;ILandroid/database/Cursor;I)V

    return-object v0
.end method

.method protected initIndexView(I)V
    .locals 0
    .param p1, "count"    # I

    .prologue
    .line 175
    return-void
.end method

.method protected initLoader()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 278
    sget-boolean v0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->SUPPORT_DLNA_DMS:Z

    if-eqz v0, :cond_0

    .line 280
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    const v1, 0x1000b

    invoke-virtual {v0, v1, v2, p0}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    .line 282
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->isSupportSlink(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 284
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    const v1, 0x1000d

    invoke-virtual {v0, v1, v2, p0}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    .line 286
    :cond_1
    return-void
.end method

.method protected launchTrackActivity(I)V
    .locals 7
    .param p1, "position"    # I

    .prologue
    .line 558
    const/4 v3, -0x1

    .line 559
    .local v3, "trackList":I
    const/4 v1, 0x0

    .line 560
    .local v1, "keyWord":Ljava/lang/String;
    const/4 v2, 0x0

    .line 561
    .local v2, "title":Ljava/lang/String;
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->isDmsItem(I)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 563
    const v3, 0x2000b

    .line 564
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->getKeyWord(I)Ljava/lang/String;

    move-result-object v1

    .line 565
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->getTitleString(I)Ljava/lang/String;

    move-result-object v2

    .line 567
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 568
    .local v0, "context":Landroid/content/Context;
    invoke-static {v0, v3}, Lcom/samsung/musicplus/dialog/DataCheckDialog;->isDataHelpChecked(Landroid/content/Context;I)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 569
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->getKeyWord(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->getTitleString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v4, v5, v6}, Lcom/samsung/musicplus/dialog/DataCheckDialog;->showDataCheckDialog(Landroid/content/Context;Landroid/app/FragmentManager;Ljava/lang/String;Ljava/lang/String;)V

    .line 589
    .end local v0    # "context":Landroid/content/Context;
    :cond_0
    :goto_0
    return-void

    .line 574
    :cond_1
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->isDeviceDisabled(I)Z

    move-result v4

    if-nez v4, :cond_0

    .line 579
    const v3, 0x2000d

    .line 580
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->getDeviceString(I)Ljava/lang/String;

    move-result-object v2

    .line 581
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->getDeviceId(I)Ljava/lang/String;

    move-result-object v1

    .line 583
    invoke-direct {p0, v1}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->isNeedDataPopup(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 584
    invoke-direct {p0, v1, v2}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->showSlinkDataDialog(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 588
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-static {v4, v2, v3, v1}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->launchTrackActivity(Landroid/app/Activity;Ljava/lang/String;ILjava/lang/String;)V

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 185
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 186
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 187
    .local v0, "context":Landroid/content/Context;
    new-instance v1, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader;

    invoke-direct {v1, v0}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mCountLoader:Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader;

    .line 189
    invoke-static {v0}, Lcom/samsung/musicplus/provider/MusicContents$Audio$Devices;->isSingedIn(Landroid/content/Context;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mIsLogin:Z

    .line 190
    invoke-static {v0}, Lcom/samsung/musicplus/provider/MusicContents$Audio$Devices;->hasSamsungAccount(Landroid/content/Context;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mHasSamsungAccount:Z

    .line 191
    invoke-static {v0}, Lcom/samsung/musicplus/provider/MusicContents$Audio$DeviceContents;->loadMusicContentsFrist(Landroid/content/Context;)V

    .line 193
    invoke-static {v0}, Lcom/samsung/musicplus/util/UiUtils;->isWifiConnected(Landroid/content/Context;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mWifiConnected:Z

    .line 194
    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/content/Loader;
    .locals 4
    .param p1, "id"    # I
    .param p2, "args"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 290
    packed-switch p1, :pswitch_data_0

    .line 300
    :pswitch_0
    const-string v1, "MusicUiDeviceTab"

    const-string v2, "You did something wrong.. "

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 301
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 292
    :pswitch_1
    const-string v1, "MusicUiDeviceTab"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "onCreateLoader p cloud"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 293
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->getSlinkDeviceCursorLoader()Landroid/content/CursorLoader;

    move-result-object v0

    .line 294
    .local v0, "cl":Landroid/content/CursorLoader;
    const-string v1, "MusicUiDeviceTab"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onCreateLoader : p cloud loader "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 298
    .end local v0    # "cl":Landroid/content/CursorLoader;
    :pswitch_2
    invoke-super {p0, p1, p2}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->onCreateLoader(ILandroid/os/Bundle;)Landroid/content/Loader;

    move-result-object v0

    goto :goto_0

    .line 290
    :pswitch_data_0
    .packed-switch 0x1000b
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onListItemClick(Landroid/widget/AbsListView;Landroid/view/View;IJ)V
    .locals 2
    .param p1, "l"    # Landroid/widget/AbsListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    .line 549
    const-wide/16 v0, -0x17

    cmp-long v0, v0, p4

    if-gtz v0, :cond_0

    const-wide/16 v0, -0x12

    cmp-long v0, p4, v0

    if-gtz v0, :cond_0

    .line 554
    :goto_0
    return-void

    .line 553
    :cond_0
    invoke-super/range {p0 .. p5}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->onListItemClick(Landroid/widget/AbsListView;Landroid/view/View;IJ)V

    goto :goto_0
.end method

.method public onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V
    .locals 9
    .param p2, "data"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 411
    .local p1, "loader":Landroid/content/Loader;, "Landroid/content/Loader<Landroid/database/Cursor;>;"
    const-string v1, "MusicUiDeviceTab"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "onLoadFinished loader.getId() :"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Loader;->getId()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " data - "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " size ? "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-nez p2, :cond_2

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 413
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->getContext()Landroid/content/Context;

    move-result-object v6

    .line 414
    .local v6, "context":Landroid/content/Context;
    invoke-virtual {p1}, Landroid/content/Loader;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 461
    :goto_1
    :pswitch_0
    const/4 v8, 0x0

    .line 462
    .local v8, "slink":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    invoke-direct {p0, v6}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->isSupportSlink(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 463
    iget-object v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mDevice:Landroid/database/Cursor;

    const v1, 0x7f100133

    const-wide/16 v4, -0x13

    invoke-direct {p0, v0, v1, v4, v5}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mergeTitle(Landroid/database/Cursor;IJ)Landroid/database/Cursor;

    move-result-object v8

    .line 465
    :cond_0
    const/4 v7, 0x0

    .line 466
    .local v7, "dms":Landroid/database/Cursor;
    sget-boolean v0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->SUPPORT_DLNA_DMS:Z

    if-eqz v0, :cond_1

    .line 467
    iget-object v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mDms:Landroid/database/Cursor;

    const v1, 0x7f1000e0

    const-wide/16 v4, -0x12

    invoke-direct {p0, v0, v1, v4, v5}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mergeTitle(Landroid/database/Cursor;IJ)Landroid/database/Cursor;

    move-result-object v7

    .line 469
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mAdapter:Landroid/widget/CursorAdapter;

    invoke-direct {p0, v8, v7}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mergeCursors(Landroid/database/Cursor;Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/CursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 470
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->setListShown(Z)V

    .line 471
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->updateNumberView()V

    .line 472
    return-void

    .line 411
    .end local v6    # "context":Landroid/content/Context;
    .end local v7    # "dms":Landroid/database/Cursor;
    .end local v8    # "slink":Landroid/database/Cursor;
    :cond_2
    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    goto :goto_0

    .line 416
    .restart local v6    # "context":Landroid/content/Context;
    :pswitch_1
    invoke-virtual {p0, v6}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->releaseObservers(Landroid/content/Context;)V

    .line 417
    iget-boolean v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mIsLogin:Z

    if-nez v0, :cond_3

    .line 418
    invoke-static {v6}, Lcom/samsung/musicplus/provider/MusicContents$Audio$Devices;->isSingedIn(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mIsLogin:Z

    .line 420
    :cond_3
    iget-boolean v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mHasSamsungAccount:Z

    if-nez v0, :cond_4

    .line 421
    invoke-static {v6}, Lcom/samsung/musicplus/provider/MusicContents$Audio$Devices;->hasSamsungAccount(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mHasSamsungAccount:Z

    .line 424
    :cond_4
    const-string v0, "MusicUiDeviceTab"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "isLogin ? "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mIsLogin:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mHasSamsungAccount ? "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mHasSamsungAccount:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 426
    invoke-direct {p0, p2}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->getDeviceIds(Landroid/database/Cursor;)[J

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mDeviceIds:[J

    .line 427
    iget-boolean v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mHasSamsungAccount:Z

    if-nez v0, :cond_5

    .line 429
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mDeviceCount:I

    .line 430
    const v0, 0x7f100103

    const-wide/16 v4, -0x16

    invoke-direct {p0, v0, v4, v5}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mergeNoItem(IJ)Landroid/database/Cursor;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mDevice:Landroid/database/Cursor;

    .line 432
    const-string v0, "SSLK"

    const-wide/16 v4, 0x0

    invoke-static {v6, v0, v4, v5}, Lcom/samsung/musicplus/util/logging/FeatureLogger;->insertLog(Landroid/content/Context;Ljava/lang/String;J)V

    goto/16 :goto_1

    .line 433
    :cond_5
    iget-boolean v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mIsLogin:Z

    if-nez v0, :cond_6

    .line 435
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mDeviceCount:I

    .line 436
    const v0, 0x7f10010e

    const-wide/16 v4, -0x16

    invoke-direct {p0, v0, v4, v5}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mergeNoItem(IJ)Landroid/database/Cursor;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mDevice:Landroid/database/Cursor;

    .line 437
    const-string v0, "SSLK"

    const-wide/16 v4, 0x0

    invoke-static {v6, v0, v4, v5}, Lcom/samsung/musicplus/util/logging/FeatureLogger;->insertLog(Landroid/content/Context;Ljava/lang/String;J)V

    goto/16 :goto_1

    .line 439
    :cond_6
    invoke-direct {p0, p2}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->getDeviceCount(Landroid/database/Cursor;)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mDeviceCount:I

    .line 440
    iget v2, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mDeviceCount:I

    const v3, 0x7f1000f8

    const-wide/16 v4, -0x17

    move-object v0, p0

    move-object v1, p2

    invoke-direct/range {v0 .. v5}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->getDeviceCursor(Landroid/database/Cursor;IIJ)Landroid/database/Cursor;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mDevice:Landroid/database/Cursor;

    .line 442
    const-string v0, "SSLK"

    const-wide/16 v4, 0x3e8

    invoke-static {v6, v0, v4, v5}, Lcom/samsung/musicplus/util/logging/FeatureLogger;->insertLog(Landroid/content/Context;Ljava/lang/String;J)V

    goto/16 :goto_1

    .line 446
    :pswitch_2
    invoke-direct {p0, p2}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->getDeviceCount(Landroid/database/Cursor;)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mDmsCount:I

    .line 447
    invoke-virtual {p0, p2}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->setColumnIndicesAndSetIndex(Landroid/database/Cursor;)V

    .line 448
    const/4 v3, -0x1

    .line 449
    .local v3, "noItemStringId":I
    iget-boolean v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mWifiConnected:Z

    if-eqz v0, :cond_8

    .line 450
    sget-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_DATA_CHECK_POPUP:Z

    if-eqz v0, :cond_7

    const v3, 0x7f1000e2

    .line 455
    :goto_2
    iget v2, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mDmsCount:I

    const-wide/16 v4, -0x14

    move-object v0, p0

    move-object v1, p2

    invoke-direct/range {v0 .. v5}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->getDeviceCursor(Landroid/database/Cursor;IIJ)Landroid/database/Cursor;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mDms:Landroid/database/Cursor;

    goto/16 :goto_1

    .line 450
    :cond_7
    const v3, 0x7f1000fb

    goto :goto_2

    .line 453
    :cond_8
    sget-boolean v0, Lcom/samsung/musicplus/util/Features;->FLAG_SUPPORT_CHINA_WLAN:Z

    if-eqz v0, :cond_9

    const v3, 0x7f1000fa

    :goto_3
    goto :goto_2

    :cond_9
    const v3, 0x7f1000f9

    goto :goto_3

    .line 414
    :pswitch_data_0
    .packed-switch 0x1000b
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Landroid/content/Loader;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 94
    check-cast p2, Landroid/database/Cursor;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 216
    invoke-static {}, Lcom/samsung/musicplus/provider/MusicContents$Audio$DeviceContents;->releaseWakeLock()V

    .line 218
    iget-object v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mCountLoader:Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader;->setExitTasksEarly(Z)V

    .line 219
    invoke-super {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->onPause()V

    .line 220
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 241
    const v1, 0x7f0d01cb

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 242
    .local v0, "search":Landroid/view/MenuItem;
    if-eqz v0, :cond_0

    .line 243
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 245
    :cond_0
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 246
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 199
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/musicplus/provider/MusicContents$Audio$DeviceContents;->acquireWakeLock(Landroid/content/Context;)V

    .line 201
    iget-object v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mCountLoader:Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader;->setExitTasksEarly(Z)V

    .line 202
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->invalidateListViews()V

    .line 203
    invoke-super {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->onResume()V

    .line 204
    return-void
.end method

.method public onStart()V
    .locals 3

    .prologue
    .line 225
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 226
    .local v0, "f":Landroid/content/IntentFilter;
    sget-object v1, Lcom/samsung/musicplus/provider/MusicContents$Audio$DeviceContents;->ACTION_S_LINK_SIGNIN_CHANGED:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 227
    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 228
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mDeviceStateReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 229
    invoke-super {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->onStart()V

    .line 230
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 234
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mDeviceStateReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 235
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->releaseObservers(Landroid/content/Context;)V

    .line 236
    invoke-super {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->onStop()V

    .line 237
    return-void
.end method

.method protected prepareContextMenu(Landroid/view/Menu;JI)V
    .locals 0
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "itemId"    # J
    .param p4, "position"    # I

    .prologue
    .line 651
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->disableAddToPlaylistOption(Landroid/view/Menu;)V

    .line 652
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->disableSetAsOption(Landroid/view/Menu;)V

    .line 653
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->disableDeleteOption(Landroid/view/Menu;)V

    .line 654
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->disableAddToFavorite(Landroid/view/Menu;)V

    .line 655
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->disableRemoveOption(Landroid/view/Menu;)V

    .line 656
    return-void
.end method

.method protected prepareOptionMenu(Landroid/view/Menu;)V
    .locals 0
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 643
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->disableDeleteOption(Landroid/view/Menu;)V

    .line 644
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->disableAddToSecretBoxOption(Landroid/view/Menu;)V

    .line 645
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->disableRemoveFromSecretBoxOption(Landroid/view/Menu;)V

    .line 647
    return-void
.end method

.method public releaseObservers(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1150
    iget-object v2, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mMap:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/database/ContentObserver;

    .line 1151
    .local v1, "observer":Landroid/database/ContentObserver;
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    goto :goto_0

    .line 1153
    .end local v1    # "observer":Landroid/database/ContentObserver;
    :cond_0
    return-void
.end method

.method protected setHeaderView()V
    .locals 0

    .prologue
    .line 545
    return-void
.end method
