.class public Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;
.super Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;
.source "MusicSquareListFragment.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;
.implements Lcom/samsung/musicplus/util/UiUtils$Defs;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/contents/square/MusicSquareListFragment$HoverListener;
    }
.end annotation


# static fields
.field private static final STATE_LIST_EXPANDED:Ljava/lang/String; = "STATE_LIST_EXPANDED"


# instance fields
.field private mAnimation:Z

.field private mFirstAccess:Z

.field private final mHoverListner:Lcom/samsung/musicplus/contents/square/MusicSquareListFragment$HoverListener;

.field private mIsPortrait:Z

.field private mIsTrackListOpened:Z

.field private mLaunchTrackIcon:Landroid/widget/ImageView;

.field private mLaunchTrackLayout:Landroid/view/View;

.field private mLaunchTrackText:Landroid/widget/TextView;

.field private mNoItemTextView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 42
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;-><init>()V

    .line 52
    iput-boolean v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mIsTrackListOpened:Z

    .line 54
    iput-boolean v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mAnimation:Z

    .line 56
    iput-boolean v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mIsPortrait:Z

    .line 58
    iput-boolean v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mFirstAccess:Z

    .line 245
    new-instance v0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment$HoverListener;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment$HoverListener;-><init>(Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;Lcom/samsung/musicplus/contents/square/MusicSquareListFragment$1;)V

    iput-object v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mHoverListner:Lcom/samsung/musicplus/contents/square/MusicSquareListFragment$HoverListener;

    .line 247
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;

    .prologue
    .line 42
    iget-boolean v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mIsTrackListOpened:Z

    return v0
.end method

.method static synthetic access$002(Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 42
    iput-boolean p1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mIsTrackListOpened:Z

    return p1
.end method

.method static synthetic access$102(Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 42
    iput-boolean p1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mFirstAccess:Z

    return p1
.end method

.method static synthetic access$200(Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->expandListView(Z)V

    return-void
.end method

.method static synthetic access$400(Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mLaunchTrackIcon:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;

    .prologue
    .line 42
    iget-boolean v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mAnimation:Z

    return v0
.end method

.method static synthetic access$600(Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->updateIconWithAnimation()V

    return-void
.end method

.method private expandListView(Z)V
    .locals 3
    .param p1, "expand"    # Z

    .prologue
    .line 399
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->getParentFragment()Landroid/app/Fragment;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Fragment;->getView()Landroid/view/View;

    move-result-object v1

    .line 400
    .local v1, "view":Landroid/view/View;
    if-eqz v1, :cond_0

    .line 401
    const v2, 0x7f0d0167

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 402
    .local v0, "squareGrid":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 403
    if-eqz p1, :cond_1

    .line 404
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 408
    :goto_0
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->markTrackText(Z)V

    .line 411
    .end local v0    # "squareGrid":Landroid/view/View;
    :cond_0
    return-void

    .line 406
    .restart local v0    # "squareGrid":Landroid/view/View;
    :cond_1
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public static getNewInstance(Ljava/lang/String;)Landroid/app/Fragment;
    .locals 4
    .param p0, "key"    # Ljava/lang/String;

    .prologue
    .line 61
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 62
    .local v0, "data":Landroid/os/Bundle;
    const-string v2, "list"

    const v3, 0x2000a

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 63
    const-string v2, "key"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    new-instance v1, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;

    invoke-direct {v1}, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;-><init>()V

    .line 65
    .local v1, "fg":Landroid/app/Fragment;
    invoke-virtual {v1, v0}, Landroid/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 66
    return-object v1
.end method

.method private markTrackText(Z)V
    .locals 5
    .param p1, "expand"    # Z

    .prologue
    .line 414
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 415
    .local v1, "context":Landroid/content/Context;
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0059

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 416
    .local v0, "color":I
    const-string v3, "sec-roboto-light"

    const/4 v4, 0x0

    invoke-static {v3, v4}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v2

    .line 417
    .local v2, "tf":Landroid/graphics/Typeface;
    if-eqz p1, :cond_0

    .line 418
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b005a

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 419
    const-string v3, "sec-roboto-light"

    const/4 v4, 0x1

    invoke-static {v3, v4}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v2

    .line 421
    :cond_0
    iget-object v3, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mLaunchTrackText:Landroid/widget/TextView;

    if-eqz v3, :cond_1

    .line 422
    iget-object v3, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mLaunchTrackText:Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 423
    iget-object v3, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mLaunchTrackText:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 425
    :cond_1
    return-void
.end method

.method private restartLoader(Ljava/lang/String;)V
    .locals 2
    .param p1, "keyWord"    # Ljava/lang/String;

    .prologue
    .line 357
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getListType()I

    move-result v0

    .line 359
    .local v0, "list":I
    const v1, 0x2000a

    if-ne v0, v1, :cond_3

    .line 360
    if-nez p1, :cond_0

    .line 361
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getKey()Ljava/lang/String;

    move-result-object p1

    .line 368
    :cond_0
    :goto_0
    if-eqz p1, :cond_1

    iget-object v1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mKey:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 369
    :cond_1
    iput-object p1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mKey:Ljava/lang/String;

    .line 370
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->updateListInfo()Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;

    .line 371
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->dispatchReCreateLoader()V

    .line 373
    :cond_2
    return-void

    .line 365
    :cond_3
    const/4 p1, 0x0

    goto :goto_0
.end method

.method private setMiniPlayerVisible()V
    .locals 2

    .prologue
    .line 293
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 294
    .local v0, "a":Landroid/app/Activity;
    instance-of v1, v0, Lcom/samsung/musicplus/MusicBaseActivity;

    if-eqz v1, :cond_0

    .line 295
    check-cast v0, Lcom/samsung/musicplus/MusicBaseActivity;

    .end local v0    # "a":Landroid/app/Activity;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/MusicBaseActivity;->setMiniPlayerVisible(Z)V

    .line 297
    :cond_0
    return-void
.end method

.method private setSquareMiniPlayer(Z)V
    .locals 1
    .param p1, "show"    # Z

    .prologue
    .line 150
    sget-boolean v0, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_MASS_PROJECT:Z

    if-nez v0, :cond_0

    .line 151
    :cond_0
    return-void
.end method

.method private setTransition()V
    .locals 4

    .prologue
    .line 376
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const v3, 0x7f0d0164

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 377
    .local v0, "container":Landroid/view/ViewGroup;
    new-instance v1, Landroid/animation/LayoutTransition;

    invoke-direct {v1}, Landroid/animation/LayoutTransition;-><init>()V

    .line 378
    .local v1, "trans":Landroid/animation/LayoutTransition;
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setLayoutTransition(Landroid/animation/LayoutTransition;)V

    .line 379
    const-wide/16 v2, 0x190

    invoke-virtual {v1, v2, v3}, Landroid/animation/LayoutTransition;->setDuration(J)V

    .line 380
    new-instance v2, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment$2;

    invoke-direct {v2, p0}, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment$2;-><init>(Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;)V

    invoke-virtual {v1, v2}, Landroid/animation/LayoutTransition;->addTransitionListener(Landroid/animation/LayoutTransition$TransitionListener;)V

    .line 393
    return-void
.end method

.method private updateHeaderView(I)V
    .locals 6
    .param p1, "count"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 271
    if-eqz p1, :cond_0

    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getListType()I

    move-result v1

    const v2, 0x2000a

    if-eq v1, v2, :cond_1

    .line 272
    :cond_0
    iget-object v1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mNoItemTextView:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 273
    iget-object v1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mLaunchTrackLayout:Landroid/view/View;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 290
    :goto_0
    return-void

    .line 278
    :cond_1
    invoke-direct {p0, v3}, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->setSquareMiniPlayer(Z)V

    .line 283
    iget-object v1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mLaunchTrackLayout:Landroid/view/View;

    invoke-virtual {v1, v5}, Landroid/view/View;->setVisibility(I)V

    .line 284
    iget-object v1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mNoItemTextView:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 285
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f000d

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, p1, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 287
    .local v0, "number":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mLaunchTrackText:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private updateIcon()V
    .locals 2

    .prologue
    .line 300
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->isAdded()Z

    move-result v0

    if-nez v0, :cond_0

    .line 312
    :goto_0
    return-void

    .line 303
    :cond_0
    iget-boolean v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mIsTrackListOpened:Z

    if-eqz v0, :cond_1

    .line 304
    iget-object v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mLaunchTrackIcon:Landroid/widget/ImageView;

    const v1, 0x7f020111

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 305
    iget-object v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mLaunchTrackIcon:Landroid/widget/ImageView;

    const v1, 0x7f100185

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 308
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mLaunchTrackIcon:Landroid/widget/ImageView;

    const v1, 0x7f02010f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 309
    iget-object v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mLaunchTrackIcon:Landroid/widget/ImageView;

    const v1, 0x7f100186

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private updateIconDirection()V
    .locals 2

    .prologue
    .line 315
    iget-boolean v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mIsTrackListOpened:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mLaunchTrackIcon:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getRotation()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 316
    iget-object v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mLaunchTrackIcon:Landroid/widget/ImageView;

    const/high16 v1, 0x43340000    # 180.0f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setRotation(F)V

    .line 318
    :cond_0
    return-void
.end method

.method private updateIconWithAnimation()V
    .locals 7

    .prologue
    const/high16 v6, 0x43340000    # 180.0f

    const/high16 v5, 0x40000000    # 2.0f

    const/4 v4, 0x0

    .line 330
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->isAdded()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-boolean v3, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mFirstAccess:Z

    if-eqz v3, :cond_1

    .line 350
    :cond_0
    :goto_0
    return-void

    .line 332
    :cond_1
    iget-object v3, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mLaunchTrackIcon:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getWidth()I

    move-result v3

    int-to-float v3, v3

    div-float v1, v3, v5

    .line 333
    .local v1, "x":F
    iget-object v3, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mLaunchTrackIcon:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getHeight()I

    move-result v3

    int-to-float v3, v3

    div-float v2, v3, v5

    .line 334
    .local v2, "y":F
    const/4 v0, 0x0

    .line 335
    .local v0, "mRotation":Landroid/view/animation/RotateAnimation;
    iget-boolean v3, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mIsTrackListOpened:Z

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mLaunchTrackIcon:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getRotation()F

    move-result v3

    cmpl-float v3, v3, v4

    if-nez v3, :cond_2

    .line 336
    new-instance v0, Landroid/view/animation/RotateAnimation;

    .end local v0    # "mRotation":Landroid/view/animation/RotateAnimation;
    invoke-direct {v0, v4, v6, v1, v2}, Landroid/view/animation/RotateAnimation;-><init>(FFFF)V

    .line 337
    .restart local v0    # "mRotation":Landroid/view/animation/RotateAnimation;
    iget-object v3, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mLaunchTrackIcon:Landroid/widget/ImageView;

    const v4, 0x7f100185

    invoke-virtual {p0, v4}, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 345
    :goto_1
    const-wide/16 v4, 0x2bc

    invoke-virtual {v0, v4, v5}, Landroid/view/animation/RotateAnimation;->setDuration(J)V

    .line 346
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/view/animation/RotateAnimation;->setFillAfter(Z)V

    .line 347
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/view/animation/RotateAnimation;->setRepeatCount(I)V

    .line 348
    invoke-virtual {v0, p0}, Landroid/view/animation/RotateAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 349
    iget-object v3, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mLaunchTrackIcon:Landroid/widget/ImageView;

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 340
    :cond_2
    iget-object v3, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mLaunchTrackIcon:Landroid/widget/ImageView;

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setRotation(F)V

    .line 341
    new-instance v0, Landroid/view/animation/RotateAnimation;

    .end local v0    # "mRotation":Landroid/view/animation/RotateAnimation;
    const/high16 v3, 0x43b40000    # 360.0f

    invoke-direct {v0, v6, v3, v1, v2}, Landroid/view/animation/RotateAnimation;-><init>(FFFF)V

    .line 342
    .restart local v0    # "mRotation":Landroid/view/animation/RotateAnimation;
    iget-object v3, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mLaunchTrackIcon:Landroid/widget/ImageView;

    const v4, 0x7f100186

    invoke-virtual {p0, v4}, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method


# virtual methods
.method protected createAdapter()Landroid/widget/CursorAdapter;
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 187
    new-instance v0, Lcom/samsung/musicplus/widget/list/TrackListAdapter;

    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const v2, 0x7f040033

    const/4 v3, 0x0

    move v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/samsung/musicplus/widget/list/TrackListAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;II)V

    return-object v0
.end method

.method protected handlePersonalModeMenu(Landroid/view/Menu;Z)V
    .locals 0
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "userPlaylist"    # Z

    .prologue
    .line 147
    return-void
.end method

.method public invalidateAllViews()V
    .locals 0

    .prologue
    .line 428
    invoke-super {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->invalidateAllViews()V

    .line 429
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x1

    .line 81
    iput-boolean v2, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mFirstAccess:Z

    .line 82
    if-eqz p1, :cond_0

    .line 83
    const-string v0, "STATE_LIST_EXPANDED"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    const-string v1, "action_mode"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    or-int/2addr v0, v1

    iput-boolean v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mIsTrackListOpened:Z

    .line 86
    :cond_0
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 87
    iget-boolean v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mIsTrackListOpened:Z

    if-eqz v0, :cond_1

    .line 88
    iget-boolean v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mIsTrackListOpened:Z

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->expandListView(Z)V

    .line 93
    :cond_1
    iget-boolean v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mIsPortrait:Z

    if-eqz v0, :cond_2

    .line 94
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->setTransition()V

    .line 95
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->getProgressContainer()Landroid/view/ViewGroup;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 97
    :cond_2
    invoke-virtual {p0, v2}, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->setHasOptionsMenu(Z)V

    .line 98
    return-void
.end method

.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 1
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    .line 440
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mAnimation:Z

    .line 441
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    .line 446
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 1
    .param p1, "arg0"    # Landroid/view/animation/Animation;

    .prologue
    .line 451
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mAnimation:Z

    .line 452
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 4
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 71
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 72
    .local v0, "context":Landroid/content/Context;
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    if-ne v3, v1, :cond_0

    :goto_0
    iput-boolean v1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mIsPortrait:Z

    .line 73
    iget-boolean v1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mIsPortrait:Z

    if-eqz v1, :cond_1

    .line 74
    const v1, 0x7f040024

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 76
    :goto_1
    return-object v1

    :cond_0
    move v1, v2

    .line 72
    goto :goto_0

    .line 76
    :cond_1
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v1

    goto :goto_1
.end method

.method public onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V
    .locals 4
    .param p2, "data"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "loader":Landroid/content/Loader;, "Landroid/content/Loader<Landroid/database/Cursor;>;"
    const/4 v2, 0x0

    .line 193
    invoke-super {p0, p1, p2}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V

    .line 195
    if-nez p2, :cond_4

    move v0, v2

    .line 196
    .local v0, "count":I
    :goto_0
    iget-boolean v3, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mIsPortrait:Z

    if-eqz v3, :cond_0

    .line 197
    invoke-direct {p0, v0}, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->updateHeaderView(I)V

    .line 198
    if-nez v0, :cond_0

    .line 199
    iput-boolean v2, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mIsTrackListOpened:Z

    .line 200
    iget-boolean v2, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mIsTrackListOpened:Z

    invoke-direct {p0, v2}, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->expandListView(Z)V

    .line 203
    :cond_0
    if-nez v0, :cond_1

    .line 204
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->setEmptyViewNoBackground()V

    .line 206
    :cond_1
    iget-boolean v2, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mIsPortrait:Z

    if-eqz v2, :cond_3

    iget-boolean v2, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mIsTrackListOpened:Z

    if-eqz v2, :cond_3

    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->isActionMode()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 207
    iget-object v2, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mLaunchTrackLayout:Landroid/view/View;

    if-nez v2, :cond_2

    .line 208
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->getView()Landroid/view/View;

    move-result-object v1

    .line 209
    .local v1, "rootView":Landroid/view/View;
    const v2, 0x7f0d008f

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mLaunchTrackLayout:Landroid/view/View;

    .line 211
    .end local v1    # "rootView":Landroid/view/View;
    :cond_2
    iget-object v2, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mLaunchTrackLayout:Landroid/view/View;

    if-eqz v2, :cond_3

    .line 212
    iget-object v2, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mLaunchTrackLayout:Landroid/view/View;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 215
    :cond_3
    return-void

    .line 195
    .end local v0    # "count":I
    :cond_4
    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    goto :goto_0
.end method

.method public bridge synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Landroid/content/Loader;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 42
    check-cast p2, Landroid/database/Cursor;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 6
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v5, 0x0

    .line 118
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 119
    const v4, 0x7f0d01e6

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 120
    .local v2, "setting":Landroid/view/MenuItem;
    if-eqz v2, :cond_0

    .line 121
    invoke-interface {v2, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 124
    :cond_0
    const v4, 0x7f0d01e5

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    .line 125
    .local v3, "square":Landroid/view/MenuItem;
    if-eqz v3, :cond_1

    .line 126
    invoke-interface {v3, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 128
    :cond_1
    iget-boolean v4, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mIsPortrait:Z

    if-eqz v4, :cond_3

    iget-boolean v4, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mIsTrackListOpened:Z

    if-nez v4, :cond_3

    .line 129
    const v4, 0x7f0d01dc

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 130
    .local v1, "select":Landroid/view/MenuItem;
    const v4, 0x7f0d01d0

    invoke-interface {p1, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 131
    .local v0, "delete":Landroid/view/MenuItem;
    if-eqz v1, :cond_2

    .line 132
    invoke-interface {v1, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 134
    :cond_2
    if-eqz v0, :cond_3

    .line 135
    invoke-interface {v0, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 138
    .end local v0    # "delete":Landroid/view/MenuItem;
    .end local v1    # "select":Landroid/view/MenuItem;
    :cond_3
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 324
    const-string v0, "STATE_LIST_EXPANDED"

    iget-boolean v1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mIsTrackListOpened:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 325
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 326
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 102
    iget-boolean v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mIsPortrait:Z

    if-nez v0, :cond_0

    .line 103
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->setSquareMiniPlayer(Z)V

    .line 105
    :cond_0
    invoke-super {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->onStart()V

    .line 106
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 110
    iget-boolean v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mIsPortrait:Z

    if-nez v0, :cond_0

    .line 111
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->setSquareMiniPlayer(Z)V

    .line 113
    :cond_0
    invoke-super {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->onStop()V

    .line 114
    return-void
.end method

.method protected setEmptyView(I)V
    .locals 5
    .param p1, "noItemTextId"    # I

    .prologue
    const/4 v4, 0x0

    .line 167
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/app/Activity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 168
    .local v0, "noItemText":Ljava/lang/CharSequence;
    iget-boolean v2, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mIsPortrait:Z

    if-eqz v2, :cond_1

    .line 169
    iput-boolean v4, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mIsTrackListOpened:Z

    .line 170
    iget-boolean v2, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mIsTrackListOpened:Z

    invoke-direct {p0, v2}, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->expandListView(Z)V

    .line 171
    iget-object v2, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mLaunchTrackLayout:Landroid/view/View;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 172
    iget-object v2, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mNoItemTextView:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 173
    iget-object v2, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mNoItemTextView:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 183
    :cond_0
    :goto_0
    return-void

    .line 175
    :cond_1
    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->setEmptyViewText(Ljava/lang/CharSequence;)V

    .line 176
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->getView()Landroid/view/View;

    move-result-object v2

    const v3, 0x7f0d0049

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 177
    .local v1, "noItemTextView":Landroid/widget/TextView;
    if-eqz v1, :cond_0

    .line 178
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b005b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 179
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c0123

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v4, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    goto :goto_0
.end method

.method protected setHeaderView()V
    .locals 4

    .prologue
    .line 219
    iget-boolean v2, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mIsPortrait:Z

    if-nez v2, :cond_0

    .line 243
    :goto_0
    return-void

    .line 222
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->getView()Landroid/view/View;

    move-result-object v1

    .line 223
    .local v1, "rootView":Landroid/view/View;
    const v2, 0x7f0d009a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 224
    .local v0, "header":Landroid/view/View;
    instance-of v2, v0, Landroid/view/ViewStub;

    if-eqz v2, :cond_1

    .line 225
    check-cast v0, Landroid/view/ViewStub;

    .end local v0    # "header":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 226
    const v2, 0x7f0d008f

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mLaunchTrackLayout:Landroid/view/View;

    .line 227
    iget-object v2, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mLaunchTrackLayout:Landroid/view/View;

    new-instance v3, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment$1;

    invoke-direct {v3, p0}, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment$1;-><init>(Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 236
    const v2, 0x7f0d00b0

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mLaunchTrackText:Landroid/widget/TextView;

    .line 237
    const v2, 0x7f0d00b1

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mLaunchTrackIcon:Landroid/widget/ImageView;

    .line 238
    iget-object v2, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mLaunchTrackIcon:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mHoverListner:Lcom/samsung/musicplus/contents/square/MusicSquareListFragment$HoverListener;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 239
    iget-object v2, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mLaunchTrackIcon:Landroid/widget/ImageView;

    const/16 v3, 0x5153

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/hardware/AirView;->setView(Landroid/view/View;I)V

    .line 240
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->updateIconDirection()V

    .line 242
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const v3, 0x7f0d016a

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mNoItemTextView:Landroid/widget/TextView;

    goto :goto_0
.end method

.method protected toggleHeaderAndShuffle(Z)V
    .locals 0
    .param p1, "visible"    # Z

    .prologue
    .line 435
    return-void
.end method

.method public updateListView(Ljava/lang/String;)V
    .locals 0
    .param p1, "keyWord"    # Ljava/lang/String;

    .prologue
    .line 353
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->restartLoader(Ljava/lang/String;)V

    .line 354
    return-void
.end method
