.class Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment$2;
.super Landroid/content/BroadcastReceiver;
.source "DlnaDmsListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;)V
    .locals 0

    .prologue
    .line 250
    iput-object p1, p0, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment$2;->this$0:Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 254
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 255
    .local v1, "action":Ljava/lang/String;
    const-string v5, "com.samsung.musicplus.dlna.connectivitychanged"

    invoke-virtual {v5, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 256
    const-string v5, "com.samsung.musicplus.dlna.connectivitychanged.extra.what"

    const/4 v6, -0x1

    invoke-virtual {p2, v5, v6}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 258
    .local v3, "errType":I
    const/4 v5, 0x1

    if-ne v3, v5, :cond_0

    .line 259
    const-string v5, "com.samsung.musicplus.dlna.extra.deviceId"

    invoke-virtual {p2, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 260
    .local v2, "deviceId":Ljava/lang/String;
    if-eqz v2, :cond_0

    iget-object v5, p0, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment$2;->this$0:Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;

    # getter for: Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->mKey:Ljava/lang/String;
    invoke-static {v5}, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->access$400(Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 266
    iget-object v5, p0, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment$2;->this$0:Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;

    invoke-virtual {v5}, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 267
    .local v0, "a":Landroid/app/Activity;
    if-eqz v0, :cond_0

    .line 268
    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/ActionBar;->getTitle()Ljava/lang/CharSequence;

    move-result-object v4

    .line 270
    .local v4, "string":Ljava/lang/CharSequence;
    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {p1, v5}, Lcom/samsung/musicplus/util/UiUtils;->launchDlnaErrorDialog(Landroid/content/Context;Ljava/lang/String;)V

    .line 271
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 276
    .end local v0    # "a":Landroid/app/Activity;
    .end local v2    # "deviceId":Ljava/lang/String;
    .end local v3    # "errType":I
    .end local v4    # "string":Ljava/lang/CharSequence;
    :cond_0
    return-void
.end method
