.class Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$2;
.super Ljava/lang/Object;
.source "DeviceListFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->getNearByDeviceCategoryView(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;

.field final synthetic val$button:Landroid/view/View;

.field final synthetic val$progress:Landroid/view/View;

.field final synthetic val$text:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;Landroid/view/View;Landroid/view/View;Landroid/widget/TextView;)V
    .locals 0

    .prologue
    .line 808
    iput-object p1, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$2;->this$1:Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;

    iput-object p2, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$2;->val$button:Landroid/view/View;

    iput-object p3, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$2;->val$progress:Landroid/view/View;

    iput-object p4, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$2;->val$text:Landroid/widget/TextView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x0

    .line 812
    iget-object v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$2;->this$1:Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;

    # getter for: Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->access$800(Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/musicplus/util/KnoxGateManager;->isKnoxModeOn(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 814
    iget-object v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$2;->this$1:Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;

    # getter for: Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->access$900(Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;)Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$2;->this$1:Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;

    # getter for: Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->access$1000(Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;->getKnoxFeatureDisabledString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 823
    :goto_0
    return-void

    .line 819
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$2;->this$1:Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;

    # getter for: Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->access$1100(Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;)Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f10005e

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 821
    iget-object v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$2;->this$1:Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;

    iget-object v1, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$2;->val$button:Landroid/view/View;

    iget-object v2, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$2;->val$progress:Landroid/view/View;

    iget-object v3, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$2;->val$text:Landroid/widget/TextView;

    const-wide/16 v4, -0x12

    # invokes: Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->showProgress(Landroid/view/View;Landroid/view/View;Landroid/widget/TextView;J)V
    invoke-static/range {v0 .. v5}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->access$1200(Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;Landroid/view/View;Landroid/view/View;Landroid/widget/TextView;J)V

    .line 822
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->refreshDlna()V

    goto :goto_0
.end method
