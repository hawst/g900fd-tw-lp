.class Lcom/samsung/musicplus/contents/normal/PlayListFragment$PlayListTrackAdapter;
.super Lcom/samsung/musicplus/contents/TrackListProgressAdapter;
.source "PlayListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/contents/normal/PlayListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PlayListTrackAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/contents/normal/PlayListFragment;


# direct methods
.method public constructor <init>(Lcom/samsung/musicplus/contents/normal/PlayListFragment;Landroid/content/Context;ILandroid/database/Cursor;I)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "layout"    # I
    .param p4, "c"    # Landroid/database/Cursor;
    .param p5, "flags"    # I

    .prologue
    .line 853
    iput-object p1, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment$PlayListTrackAdapter;->this$0:Lcom/samsung/musicplus/contents/normal/PlayListFragment;

    .line 854
    invoke-direct {p0, p2, p3, p4, p5}, Lcom/samsung/musicplus/contents/TrackListProgressAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;I)V

    .line 855
    return-void
.end method


# virtual methods
.method protected bindNowPlayingView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "c"    # Landroid/database/Cursor;

    .prologue
    .line 873
    iget-object v0, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment$PlayListTrackAdapter;->this$0:Lcom/samsung/musicplus/contents/normal/PlayListFragment;

    # getter for: Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mPlaylistId:J
    invoke-static {v0}, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->access$600(Lcom/samsung/musicplus/contents/normal/PlayListFragment;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/samsung/musicplus/util/ListUtils;->isUserMadePlayList(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 877
    :goto_0
    return-void

    .line 876
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/musicplus/contents/TrackListProgressAdapter;->bindNowPlayingView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    goto :goto_0
.end method

.method protected newAnimationView(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 863
    iget-object v0, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment$PlayListTrackAdapter;->this$0:Lcom/samsung/musicplus/contents/normal/PlayListFragment;

    # getter for: Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mPlaylistId:J
    invoke-static {v0}, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->access$600(Lcom/samsung/musicplus/contents/normal/PlayListFragment;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/samsung/musicplus/util/ListUtils;->isUserMadePlayList(J)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 867
    :goto_0
    return-void

    .line 866
    :cond_0
    invoke-super {p0, p1}, Lcom/samsung/musicplus/contents/TrackListProgressAdapter;->newAnimationView(Landroid/view/View;)V

    goto :goto_0
.end method
