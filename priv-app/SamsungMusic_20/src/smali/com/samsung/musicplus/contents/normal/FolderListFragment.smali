.class public Lcom/samsung/musicplus/contents/normal/FolderListFragment;
.super Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;
.source "FolderListFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/contents/normal/FolderListFragment$1;,
        Lcom/samsung/musicplus/contents/normal/FolderListFragment$FolderListAdapter;,
        Lcom/samsung/musicplus/contents/normal/FolderListFragment$FolderListTrackAdapter;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;-><init>()V

    .line 103
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/contents/normal/FolderListFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/normal/FolderListFragment;

    .prologue
    .line 38
    iget v0, p0, Lcom/samsung/musicplus/contents/normal/FolderListFragment;->mList:I

    return v0
.end method

.method static synthetic access$100(Lcom/samsung/musicplus/contents/normal/FolderListFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/normal/FolderListFragment;

    .prologue
    .line 38
    iget v0, p0, Lcom/samsung/musicplus/contents/normal/FolderListFragment;->mList:I

    return v0
.end method


# virtual methods
.method protected createAdapter()Landroid/widget/CursorAdapter;
    .locals 6

    .prologue
    const/4 v4, 0x0

    const v3, 0x7f040033

    const/4 v5, 0x0

    .line 56
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/normal/FolderListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    .line 57
    .local v2, "a":Landroid/app/Activity;
    iget v0, p0, Lcom/samsung/musicplus/contents/normal/FolderListFragment;->mList:I

    invoke-static {v0}, Lcom/samsung/musicplus/util/ListUtils;->isTab(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 58
    new-instance v0, Lcom/samsung/musicplus/contents/normal/FolderListFragment$FolderListAdapter;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/samsung/musicplus/contents/normal/FolderListFragment$FolderListAdapter;-><init>(Lcom/samsung/musicplus/contents/normal/FolderListFragment;Landroid/content/Context;ILandroid/database/Cursor;I)V

    .line 60
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/samsung/musicplus/contents/normal/FolderListFragment$FolderListTrackAdapter;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/samsung/musicplus/contents/normal/FolderListFragment$FolderListTrackAdapter;-><init>(Lcom/samsung/musicplus/contents/normal/FolderListFragment;Landroid/content/Context;ILandroid/database/Cursor;I)V

    goto :goto_0
.end method

.method public getListItemCount()I
    .locals 4

    .prologue
    .line 70
    const/4 v0, 0x0

    .line 71
    .local v0, "count":I
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/normal/FolderListFragment;->getAbsListView()Landroid/widget/AbsListView;

    move-result-object v1

    .line 72
    .local v1, "lv":Landroid/widget/AbsListView;
    if-eqz v1, :cond_0

    .line 73
    instance-of v2, v1, Lcom/samsung/musicplus/widget/list/MusicListView;

    if-eqz v2, :cond_1

    .line 74
    invoke-virtual {v1}, Landroid/widget/AbsListView;->getCount()I

    move-result v2

    check-cast v1, Lcom/samsung/musicplus/widget/list/MusicListView;

    .end local v1    # "lv":Landroid/widget/AbsListView;
    invoke-virtual {v1}, Lcom/samsung/musicplus/widget/list/MusicListView;->getHeaderViewsCount()I

    move-result v3

    sub-int v0, v2, v3

    .line 79
    :cond_0
    :goto_0
    return v0

    .line 76
    .restart local v1    # "lv":Landroid/widget/AbsListView;
    :cond_1
    invoke-virtual {v1}, Landroid/widget/AbsListView;->getCount()I

    move-result v0

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 46
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 47
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 42
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 43
    return-void
.end method

.method public onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V
    .locals 1
    .param p2, "data"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 84
    .local p1, "loader":Landroid/content/Loader;, "Landroid/content/Loader<Landroid/database/Cursor;>;"
    invoke-super {p0, p1, p2}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V

    .line 85
    iget v0, p0, Lcom/samsung/musicplus/contents/normal/FolderListFragment;->mList:I

    invoke-static {v0}, Lcom/samsung/musicplus/util/ListUtils;->isTrack(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 86
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/normal/FolderListFragment;->isSplitSubListFragment()Z

    move-result v0

    if-nez v0, :cond_0

    .line 87
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/normal/FolderListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 89
    :cond_0
    return-void
.end method

.method public bridge synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Landroid/content/Loader;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 38
    check-cast p2, Landroid/database/Cursor;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/samsung/musicplus/contents/normal/FolderListFragment;->onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 0
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 51
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 52
    return-void
.end method

.method protected setHeaderView()V
    .locals 0

    .prologue
    .line 65
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/normal/FolderListFragment;->setListHeaderView()V

    .line 66
    return-void
.end method
