.class public Lcom/samsung/musicplus/contents/extra/ReorderListActivity;
.super Lcom/samsung/musicplus/MusicBaseActivity;
.source "ReorderListActivity.java"


# static fields
.field private static final CLASSNAME:Ljava/lang/String;


# instance fields
.field private mKey:Ljava/lang/String;

.field private mList:I

.field private mMode:I

.field private mPlaylistId:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/samsung/musicplus/contents/extra/ReorderListActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/contents/extra/ReorderListActivity;->CLASSNAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/samsung/musicplus/MusicBaseActivity;-><init>()V

    return-void
.end method

.method private saveListOrder()V
    .locals 3

    .prologue
    .line 89
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/extra/ReorderListActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    iget v2, p0, Lcom/samsung/musicplus/contents/extra/ReorderListActivity;->mList:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 90
    .local v0, "fg":Landroid/app/Fragment;
    instance-of v1, v0, Lcom/samsung/musicplus/contents/extra/ReorderListFragment;

    if-eqz v1, :cond_0

    .line 91
    check-cast v0, Lcom/samsung/musicplus/contents/extra/ReorderListFragment;

    .end local v0    # "fg":Landroid/app/Fragment;
    invoke-virtual {v0}, Lcom/samsung/musicplus/contents/extra/ReorderListFragment;->saveListOrder()V

    .line 93
    :cond_0
    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/extra/ReorderListActivity;->saveListOrder()V

    .line 78
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 40
    const v1, 0x7f04006c

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/contents/extra/ReorderListActivity;->setContentView(I)V

    .line 41
    invoke-super {p0, p1}, Lcom/samsung/musicplus/MusicBaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 43
    if-eqz p1, :cond_1

    .line 44
    const-string v1, "list_type"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/samsung/musicplus/contents/extra/ReorderListActivity;->mList:I

    .line 45
    const-string v1, "list_mode"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/samsung/musicplus/contents/extra/ReorderListActivity;->mMode:I

    .line 46
    const-string v1, "keyword"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/musicplus/contents/extra/ReorderListActivity;->mKey:Ljava/lang/String;

    .line 47
    const-string v1, "playlist_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/samsung/musicplus/contents/extra/ReorderListActivity;->mPlaylistId:J

    .line 57
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/extra/ReorderListActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    iget v2, p0, Lcom/samsung/musicplus/contents/extra/ReorderListActivity;->mList:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v7

    .line 58
    .local v7, "fg":Landroid/app/Fragment;
    if-nez v7, :cond_0

    .line 59
    iget v1, p0, Lcom/samsung/musicplus/contents/extra/ReorderListActivity;->mList:I

    iget v2, p0, Lcom/samsung/musicplus/contents/extra/ReorderListActivity;->mMode:I

    iget-object v3, p0, Lcom/samsung/musicplus/contents/extra/ReorderListActivity;->mKey:Ljava/lang/String;

    iget-wide v4, p0, Lcom/samsung/musicplus/contents/extra/ReorderListActivity;->mPlaylistId:J

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Lcom/samsung/musicplus/contents/extra/ReorderListFragment;->getNewInstance(IILjava/lang/String;JZ)Landroid/app/Fragment;

    move-result-object v7

    .line 60
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/extra/ReorderListActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    const v2, 0x7f0d0135

    iget v3, p0, Lcom/samsung/musicplus/contents/extra/ReorderListActivity;->mList:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v7, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commit()I

    .line 63
    :cond_0
    const v1, 0x7f1000ab

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/contents/extra/ReorderListActivity;->setTitle(I)V

    .line 64
    return-void

    .line 49
    .end local v7    # "fg":Landroid/app/Fragment;
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/extra/ReorderListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 51
    .local v0, "data":Landroid/os/Bundle;
    const-string v1, "list_type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/samsung/musicplus/contents/extra/ReorderListActivity;->mList:I

    .line 52
    const-string v1, "list_mode"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/samsung/musicplus/contents/extra/ReorderListActivity;->mMode:I

    .line 53
    const-string v1, "keyword"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/musicplus/contents/extra/ReorderListActivity;->mKey:Ljava/lang/String;

    .line 54
    const-string v1, "playlist_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/samsung/musicplus/contents/extra/ReorderListActivity;->mPlaylistId:J

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 82
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 83
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/extra/ReorderListActivity;->saveListOrder()V

    .line 85
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 68
    const-string v0, "list_type"

    iget v1, p0, Lcom/samsung/musicplus/contents/extra/ReorderListActivity;->mList:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 69
    const-string v0, "list_mode"

    iget v1, p0, Lcom/samsung/musicplus/contents/extra/ReorderListActivity;->mMode:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 70
    const-string v0, "keyword"

    iget-object v1, p0, Lcom/samsung/musicplus/contents/extra/ReorderListActivity;->mKey:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    const-string v0, "playlist_id"

    iget-wide v2, p0, Lcom/samsung/musicplus/contents/extra/ReorderListActivity;->mPlaylistId:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 72
    invoke-super {p0, p1}, Lcom/samsung/musicplus/MusicBaseActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 73
    return-void
.end method
