.class public Lcom/samsung/musicplus/contents/search/QueryBrowserActivity;
.super Lcom/samsung/musicplus/MusicBaseActivity;
.source "QueryBrowserActivity.java"

# interfaces
.implements Landroid/widget/SearchView$OnQueryTextListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/contents/search/QueryBrowserActivity$QueryTextChangeListener;
    }
.end annotation


# instance fields
.field private mFragment:Landroid/app/Fragment;

.field private mSearchView:Landroid/widget/SearchView;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/samsung/musicplus/MusicBaseActivity;-><init>()V

    .line 228
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/contents/search/QueryBrowserActivity;)Lcom/samsung/musicplus/player/MiniPlayer;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/search/QueryBrowserActivity;

    .prologue
    .line 47
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/search/QueryBrowserActivity;->getMiniPlayer()Lcom/samsung/musicplus/player/MiniPlayer;

    move-result-object v0

    return-object v0
.end method

.method private receivePlayerState(Ljava/lang/String;)V
    .locals 3
    .param p1, "action"    # Ljava/lang/String;

    .prologue
    .line 158
    iget-object v0, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserActivity;->mFragment:Landroid/app/Fragment;

    if-eqz v0, :cond_0

    .line 159
    iget-object v0, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserActivity;->mFragment:Landroid/app/Fragment;

    check-cast v0, Lcom/samsung/musicplus/widget/fragment/OnMusicFragmentInterface;

    invoke-interface {v0, p1}, Lcom/samsung/musicplus/widget/fragment/OnMusicFragmentInterface;->receivePlayerState(Ljava/lang/String;)V

    .line 161
    :cond_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " updateAllViews(TAG_MUSIC_LIST) fragment :  "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserActivity;->mFragment:Landroid/app/Fragment;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " fragment.isResumed ? "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserActivity;->mFragment:Landroid/app/Fragment;

    if-nez v0, :cond_1

    const-string v0, "fragment is null"

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    return-void

    .line 161
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserActivity;->mFragment:Landroid/app/Fragment;

    invoke-virtual {v0}, Landroid/app/Fragment;->isResumed()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0
.end method

.method private setActionBar(Landroid/view/View;)V
    .locals 4
    .param p1, "customView"    # Landroid/view/View;

    .prologue
    const/4 v3, -0x1

    .line 181
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/search/QueryBrowserActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 182
    .local v0, "bar":Landroid/app/ActionBar;
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 183
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    .line 185
    new-instance v1, Landroid/app/ActionBar$LayoutParams;

    invoke-direct {v1, v3, v3}, Landroid/app/ActionBar$LayoutParams;-><init>(II)V

    .line 186
    .local v1, "lp":Landroid/app/ActionBar$LayoutParams;
    invoke-virtual {v0, p1, v1}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    .line 187
    return-void
.end method

.method private setKeyPadListener()V
    .locals 2

    .prologue
    .line 166
    const v1, 0x7f0d018f

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/contents/search/QueryBrowserActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/widget/DispatchKeyRelativeLayout;

    .line 167
    .local v0, "searchMain":Lcom/samsung/musicplus/widget/DispatchKeyRelativeLayout;
    if-eqz v0, :cond_0

    .line 168
    new-instance v1, Lcom/samsung/musicplus/contents/search/QueryBrowserActivity$1;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/contents/search/QueryBrowserActivity$1;-><init>(Lcom/samsung/musicplus/contents/search/QueryBrowserActivity;)V

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/widget/DispatchKeyRelativeLayout;->setListener(Lcom/samsung/musicplus/widget/DispatchKeyRelativeLayout$KeyPadDisplayListener;)V

    .line 178
    :cond_0
    return-void
.end method

.method private setSearchView()V
    .locals 2

    .prologue
    .line 190
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/search/QueryBrowserActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0d0193

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SearchView;

    iput-object v0, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserActivity;->mSearchView:Landroid/widget/SearchView;

    .line 191
    iget-object v0, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v0}, Landroid/widget/SearchView;->onActionViewExpanded()V

    .line 192
    iget-object v0, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v0, p0}, Landroid/widget/SearchView;->setOnQueryTextListener(Landroid/widget/SearchView$OnQueryTextListener;)V

    .line 193
    iget-object v0, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserActivity;->mSearchView:Landroid/widget/SearchView;

    new-instance v1, Lcom/samsung/musicplus/contents/search/QueryBrowserActivity$2;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/contents/search/QueryBrowserActivity$2;-><init>(Lcom/samsung/musicplus/contents/search/QueryBrowserActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/SearchView;->setOnCloseListener(Landroid/widget/SearchView$OnCloseListener;)V

    .line 199
    iget-object v0, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserActivity;->mFragment:Landroid/app/Fragment;

    instance-of v0, v0, Lcom/samsung/musicplus/contents/search/QueryBrowserActivity$QueryTextChangeListener;

    if-eqz v0, :cond_0

    .line 200
    iget-object v0, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserActivity;->mFragment:Landroid/app/Fragment;

    check-cast v0, Lcom/samsung/musicplus/contents/search/QueryBrowserActivity$QueryTextChangeListener;

    iget-object v1, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-interface {v0, v1}, Lcom/samsung/musicplus/contents/search/QueryBrowserActivity$QueryTextChangeListener;->setSearchView(Landroid/widget/SearchView;)V

    .line 202
    :cond_0
    return-void
.end method


# virtual methods
.method public finish()V
    .locals 2

    .prologue
    .line 117
    invoke-super {p0}, Lcom/samsung/musicplus/MusicBaseActivity;->finish()V

    .line 118
    const v0, 0x7f050012

    const v1, 0x7f050013

    invoke-virtual {p0, v0, v1}, Lcom/samsung/musicplus/contents/search/QueryBrowserActivity;->overridePendingTransition(II)V

    .line 119
    return-void
.end method

.method protected makeMiniPlayer()V
    .locals 0

    .prologue
    .line 147
    invoke-super {p0}, Lcom/samsung/musicplus/MusicBaseActivity;->makeMiniPlayer()V

    .line 148
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/search/QueryBrowserActivity;->setKeyPadListener()V

    .line 149
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 15
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 55
    invoke-super/range {p0 .. p1}, Lcom/samsung/musicplus/MusicBaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 57
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/search/QueryBrowserActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    .line 58
    .local v4, "i":Landroid/content/Intent;
    if-nez v4, :cond_2

    const/4 v0, 0x0

    .line 59
    .local v0, "action":Ljava/lang/String;
    :goto_0
    const/4 v6, 0x0

    .line 60
    .local v6, "keyWord":Ljava/lang/String;
    const v7, 0x20024

    .line 62
    .local v7, "listType":I
    if-eqz v4, :cond_5

    const-string v11, "android.intent.action.MEDIA_SEARCH"

    invoke-virtual {v11, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_5

    .line 63
    const-string v11, "android.intent.extra.focus"

    invoke-virtual {v4, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 64
    .local v3, "focus":Ljava/lang/String;
    const-string v11, "android.intent.extra.artist"

    invoke-virtual {v4, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 65
    .local v2, "artist":Ljava/lang/String;
    const-string v11, "android.intent.extra.album"

    invoke-virtual {v4, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 66
    .local v1, "album":Ljava/lang/String;
    const-string v11, "android.intent.extra.title"

    invoke-virtual {v4, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 67
    .local v9, "title":Ljava/lang/String;
    if-eqz v3, :cond_0

    .line 68
    const-string v11, "audio/"

    invoke-virtual {v3, v11}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_3

    if-eqz v9, :cond_3

    .line 69
    move-object v6, v9

    .line 91
    .end local v1    # "album":Ljava/lang/String;
    .end local v2    # "artist":Ljava/lang/String;
    .end local v3    # "focus":Ljava/lang/String;
    .end local v9    # "title":Ljava/lang/String;
    :cond_0
    :goto_1
    const v11, 0x7f040084

    invoke-virtual {p0, v11}, Lcom/samsung/musicplus/contents/search/QueryBrowserActivity;->setContentView(I)V

    .line 92
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/search/QueryBrowserActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v11

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v11

    iput-object v11, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserActivity;->mFragment:Landroid/app/Fragment;

    .line 94
    iget-object v11, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserActivity;->mFragment:Landroid/app/Fragment;

    if-nez v11, :cond_1

    .line 96
    invoke-static {v7, v6}, Lcom/samsung/musicplus/contents/ContentsFragmentFactory;->getNewInstance(ILjava/lang/String;)Landroid/app/Fragment;

    move-result-object v11

    iput-object v11, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserActivity;->mFragment:Landroid/app/Fragment;

    .line 97
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/search/QueryBrowserActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v11

    invoke-virtual {v11}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v11

    const v12, 0x7f0d0190

    iget-object v13, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserActivity;->mFragment:Landroid/app/Fragment;

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v11, v12, v13, v14}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v11

    invoke-virtual {v11}, Landroid/app/FragmentTransaction;->commit()I

    .line 101
    :cond_1
    const-string v11, "layout_inflater"

    invoke-virtual {p0, v11}, Lcom/samsung/musicplus/contents/search/QueryBrowserActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/LayoutInflater;

    .line 103
    .local v5, "inflator":Landroid/view/LayoutInflater;
    const v11, 0x7f040087

    const/4 v12, 0x0

    invoke-virtual {v5, v11, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v10

    .line 104
    .local v10, "v":Landroid/view/View;
    invoke-direct {p0, v10}, Lcom/samsung/musicplus/contents/search/QueryBrowserActivity;->setActionBar(Landroid/view/View;)V

    .line 105
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/search/QueryBrowserActivity;->setSearchView()V

    .line 107
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/search/QueryBrowserActivity;->getWindow()Landroid/view/Window;

    move-result-object v11

    invoke-virtual {v11}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v8

    .line 108
    .local v8, "lp":Landroid/view/WindowManager$LayoutParams;
    iget v11, v8, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    or-int/lit8 v11, v11, 0x1

    iput v11, v8, Landroid/view/WindowManager$LayoutParams;->samsungFlags:I

    .line 109
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/search/QueryBrowserActivity;->getWindow()Landroid/view/Window;

    move-result-object v11

    invoke-virtual {v11, v8}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 111
    const v11, 0x7f050012

    const v12, 0x7f050013

    invoke-virtual {p0, v11, v12}, Lcom/samsung/musicplus/contents/search/QueryBrowserActivity;->overridePendingTransition(II)V

    .line 112
    return-void

    .line 58
    .end local v0    # "action":Ljava/lang/String;
    .end local v5    # "inflator":Landroid/view/LayoutInflater;
    .end local v6    # "keyWord":Ljava/lang/String;
    .end local v7    # "listType":I
    .end local v8    # "lp":Landroid/view/WindowManager$LayoutParams;
    .end local v10    # "v":Landroid/view/View;
    :cond_2
    invoke-virtual {v4}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 70
    .restart local v0    # "action":Ljava/lang/String;
    .restart local v1    # "album":Ljava/lang/String;
    .restart local v2    # "artist":Ljava/lang/String;
    .restart local v3    # "focus":Ljava/lang/String;
    .restart local v6    # "keyWord":Ljava/lang/String;
    .restart local v7    # "listType":I
    .restart local v9    # "title":Ljava/lang/String;
    :cond_3
    const-string v11, "vnd.android.cursor.item/album"

    invoke-virtual {v3, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_4

    .line 71
    if-eqz v1, :cond_0

    .line 72
    move-object v6, v1

    .line 73
    if-eqz v2, :cond_0

    .line 74
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_1

    .line 77
    :cond_4
    const-string v11, "vnd.android.cursor.item/artist"

    invoke-virtual {v3, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    .line 78
    if-eqz v2, :cond_0

    .line 79
    move-object v6, v2

    goto/16 :goto_1

    .line 84
    .end local v1    # "album":Ljava/lang/String;
    .end local v2    # "artist":Ljava/lang/String;
    .end local v3    # "focus":Ljava/lang/String;
    .end local v9    # "title":Ljava/lang/String;
    :cond_5
    if-eqz v4, :cond_0

    .line 85
    const-string v11, "keyword"

    invoke-virtual {v4, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 86
    const-string v11, "list_type"

    const v12, 0x20024

    invoke-virtual {v4, v11, v12}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    goto/16 :goto_1
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 127
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 142
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Lcom/samsung/musicplus/MusicBaseActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    return v1

    .line 132
    :pswitch_0
    const-string v1, "input_method"

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/contents/search/QueryBrowserActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 133
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/search/QueryBrowserActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 134
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/search/QueryBrowserActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 135
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/search/QueryBrowserActivity;->getCurrentFocus()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    goto :goto_0

    .line 127
    nop

    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 123
    const/4 v0, 0x0

    return v0
.end method

.method public onQueryTextChange(Ljava/lang/String;)Z
    .locals 1
    .param p1, "newText"    # Ljava/lang/String;

    .prologue
    .line 222
    iget-object v0, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserActivity;->mFragment:Landroid/app/Fragment;

    instance-of v0, v0, Lcom/samsung/musicplus/contents/search/QueryBrowserActivity$QueryTextChangeListener;

    if-eqz v0, :cond_0

    .line 223
    iget-object v0, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserActivity;->mFragment:Landroid/app/Fragment;

    check-cast v0, Lcom/samsung/musicplus/contents/search/QueryBrowserActivity$QueryTextChangeListener;

    invoke-interface {v0, p1}, Lcom/samsung/musicplus/contents/search/QueryBrowserActivity$QueryTextChangeListener;->onQueryTextChanged(Ljava/lang/String;)V

    .line 225
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public onQueryTextSubmit(Ljava/lang/String;)Z
    .locals 3
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 213
    const-string v1, "input_method"

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/contents/search/QueryBrowserActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    .line 214
    .local v0, "imm":Landroid/view/inputmethod/InputMethodManager;
    iget-object v1, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v1}, Landroid/widget/SearchView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 215
    iget-object v1, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v1}, Landroid/widget/SearchView;->clearFocus()V

    .line 216
    return v2
.end method

.method protected onReceivePlayerState(Landroid/content/Intent;)V
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 153
    invoke-super {p0, p1}, Lcom/samsung/musicplus/MusicBaseActivity;->onReceivePlayerState(Landroid/content/Intent;)V

    .line 154
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/contents/search/QueryBrowserActivity;->receivePlayerState(Ljava/lang/String;)V

    .line 155
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1
    .param p1, "hasFocus"    # Z

    .prologue
    .line 206
    invoke-super {p0, p1}, Lcom/samsung/musicplus/MusicBaseActivity;->onWindowFocusChanged(Z)V

    .line 207
    if-eqz p1, :cond_0

    .line 208
    iget-object v0, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserActivity;->mSearchView:Landroid/widget/SearchView;

    invoke-virtual {v0}, Landroid/widget/SearchView;->requestFocus()Z

    .line 209
    :cond_0
    return-void
.end method
