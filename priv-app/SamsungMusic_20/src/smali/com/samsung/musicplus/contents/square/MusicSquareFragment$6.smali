.class Lcom/samsung/musicplus/contents/square/MusicSquareFragment$6;
.super Landroid/os/Handler;
.source "MusicSquareFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/contents/square/MusicSquareFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;)V
    .locals 0

    .prologue
    .line 639
    iput-object p1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$6;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v4, 0x0

    .line 643
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    .line 645
    .local v0, "position":Ljava/lang/Integer;
    # getter for: Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->access$1600()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mSquareVisualInteractionAnimatinHandler handleMessage msg.what : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Landroid/os/Message;->what:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  position : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 649
    iget-object v1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$6;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    # getter for: Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSquareGridView:Landroid/widget/GridView;
    invoke-static {v1}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->access$100(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;)Landroid/widget/GridView;

    move-result-object v1

    if-nez v1, :cond_0

    .line 650
    iget-object v1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$6;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    # invokes: Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->removeMessageAllVisualInteraction()V
    invoke-static {v1}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->access$1700(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;)V

    .line 651
    iget-object v1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$6;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    # invokes: Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->cleanAlbumArtBitmapVisualInteraction()V
    invoke-static {v1}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->access$1800(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;)V

    .line 652
    iget-object v1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$6;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    # setter for: Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mIsVisualInteraction:Z
    invoke-static {v1, v4}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->access$1902(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;Z)Z

    .line 678
    :goto_0
    return-void

    .line 655
    :cond_0
    iget v1, p1, Landroid/os/Message;->what:I

    sparse-switch v1, :sswitch_data_0

    goto :goto_0

    .line 666
    :sswitch_0
    iget-object v1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$6;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    iget-object v2, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$6;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    # getter for: Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSquareGridView:Landroid/widget/GridView;
    invoke-static {v2}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->access$100(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;)Landroid/widget/GridView;

    move-result-object v2

    # invokes: Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->clearAllMusicSquareCell(Landroid/widget/GridView;)V
    invoke-static {v1, v2}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->access$200(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;Landroid/widget/GridView;)V

    goto :goto_0

    .line 657
    :sswitch_1
    iget-object v1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$6;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    iget-object v2, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$6;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    # getter for: Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSquareGridView:Landroid/widget/GridView;
    invoke-static {v2}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->access$100(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;)Landroid/widget/GridView;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    # invokes: Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->selectMusicSquareCell(Landroid/widget/GridView;I)V
    invoke-static {v1, v2, v3}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->access$300(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;Landroid/widget/GridView;I)V

    goto :goto_0

    .line 660
    :sswitch_2
    iget-object v1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$6;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    iget-object v2, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$6;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    # getter for: Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSquareGridView:Landroid/widget/GridView;
    invoke-static {v2}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->access$100(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;)Landroid/widget/GridView;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    # invokes: Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->unselectMusicSquareCell(Landroid/widget/GridView;I)V
    invoke-static {v1, v2, v3}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->access$2000(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;Landroid/widget/GridView;I)V

    goto :goto_0

    .line 663
    :sswitch_3
    iget-object v1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$6;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    iget-object v2, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$6;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    # getter for: Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSquareGridView:Landroid/widget/GridView;
    invoke-static {v2}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->access$100(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;)Landroid/widget/GridView;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    # invokes: Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->setAlbumArtMusicSquareCell(Landroid/widget/GridView;I)V
    invoke-static {v1, v2, v3}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->access$2100(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;Landroid/widget/GridView;I)V

    goto :goto_0

    .line 669
    :sswitch_4
    iget-object v1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$6;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    # invokes: Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->cleanAlbumArtBitmapVisualInteraction()V
    invoke-static {v1}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->access$1800(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;)V

    .line 670
    iget-object v1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$6;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    invoke-virtual {v1}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->restoreMusicSquareCell()V

    .line 671
    iget-object v1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$6;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    # invokes: Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->invalidateAllViews()V
    invoke-static {v1}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->access$2200(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;)V

    .line 672
    iget-object v1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$6;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    # setter for: Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mIsVisualInteraction:Z
    invoke-static {v1, v4}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->access$1902(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;Z)Z

    goto :goto_0

    .line 655
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_3
        0x3 -> :sswitch_2
        0x64 -> :sswitch_4
    .end sparse-switch
.end method
