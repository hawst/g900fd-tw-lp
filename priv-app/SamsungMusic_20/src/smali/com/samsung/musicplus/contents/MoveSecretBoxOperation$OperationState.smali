.class public interface abstract Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$OperationState;
.super Ljava/lang/Object;
.source "MoveSecretBoxOperation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OperationState"
.end annotation


# static fields
.field public static final ABORT:I = 0x3

.field public static final CANCEL:I = 0x0

.field public static final FILE_ALREADY_EXIST:I = 0x4

.field public static final FINISH:I = 0x1

.field public static final UPDATE:I = 0x2
