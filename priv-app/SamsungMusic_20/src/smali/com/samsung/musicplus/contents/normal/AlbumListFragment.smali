.class public Lcom/samsung/musicplus/contents/normal/AlbumListFragment;
.super Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;
.source "AlbumListFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/contents/normal/AlbumListFragment$AlbumListTrackAdapter;,
        Lcom/samsung/musicplus/contents/normal/AlbumListFragment$AlbumTabAdapter;
    }
.end annotation


# static fields
.field private static final PREF_KEY_ALBUM_VIEW_TYPE:Ljava/lang/String; = "album_view_type"


# instance fields
.field private mCoverImage:Landroid/widget/ImageView;

.field private mCoverText1:Landroid/widget/TextView;

.field private mCoverText2:Landroid/widget/TextView;

.field private mCoverText3:Landroid/widget/TextView;

.field private mIsSingleArtist:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;-><init>()V

    .line 48
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/contents/normal/AlbumListFragment;->mIsSingleArtist:Z

    .line 231
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/contents/normal/AlbumListFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/normal/AlbumListFragment;

    .prologue
    .line 46
    iget v0, p0, Lcom/samsung/musicplus/contents/normal/AlbumListFragment;->mList:I

    return v0
.end method

.method static synthetic access$100(Lcom/samsung/musicplus/contents/normal/AlbumListFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/normal/AlbumListFragment;

    .prologue
    .line 46
    iget v0, p0, Lcom/samsung/musicplus/contents/normal/AlbumListFragment;->mList:I

    return v0
.end method

.method static synthetic access$200(Lcom/samsung/musicplus/contents/normal/AlbumListFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/normal/AlbumListFragment;

    .prologue
    .line 46
    iget-boolean v0, p0, Lcom/samsung/musicplus/contents/normal/AlbumListFragment;->mIsSingleArtist:Z

    return v0
.end method

.method static synthetic access$300(Lcom/samsung/musicplus/contents/normal/AlbumListFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/normal/AlbumListFragment;

    .prologue
    .line 46
    iget v0, p0, Lcom/samsung/musicplus/contents/normal/AlbumListFragment;->mList:I

    return v0
.end method

.method static synthetic access$400(Lcom/samsung/musicplus/contents/normal/AlbumListFragment;I)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/normal/AlbumListFragment;
    .param p1, "x1"    # I

    .prologue
    .line 46
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/contents/normal/AlbumListFragment;->isEnableListShuffle(I)Z

    move-result v0

    return v0
.end method

.method private initializeCoverView(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 143
    if-eqz p1, :cond_0

    .line 144
    const v1, 0x7f0d009e

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/samsung/musicplus/contents/normal/AlbumListFragment;->mCoverImage:Landroid/widget/ImageView;

    .line 145
    const v1, 0x7f0d009f

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/samsung/musicplus/contents/normal/AlbumListFragment;->mCoverText1:Landroid/widget/TextView;

    .line 146
    const v1, 0x7f0d00a0

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/samsung/musicplus/contents/normal/AlbumListFragment;->mCoverText2:Landroid/widget/TextView;

    .line 147
    const v1, 0x7f0d00a1

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/samsung/musicplus/contents/normal/AlbumListFragment;->mCoverText3:Landroid/widget/TextView;

    .line 148
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/normal/AlbumListFragment;->isSplitSubListFragment()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 149
    const v1, 0x7f0d009c

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 150
    .local v0, "coverView":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 151
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 155
    .end local v0    # "coverView":Landroid/view/View;
    :cond_0
    return-void
.end method

.method private updateCoverView(Landroid/database/Cursor;)V
    .locals 7
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 158
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/normal/AlbumListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    .line 159
    .local v3, "context":Landroid/content/Context;
    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0c0018

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 160
    .local v4, "size":I
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 161
    const-string v5, "album_id"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    int-to-long v0, v5

    .line 162
    .local v0, "albumId":J
    invoke-static {v3, v0, v1, v4, v4}, Lcom/samsung/musicplus/util/AlbumArtUtils;->getArtworkQuick(Landroid/content/Context;JII)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 163
    .local v2, "bm":Landroid/graphics/Bitmap;
    iget-object v5, p0, Lcom/samsung/musicplus/contents/normal/AlbumListFragment;->mCoverImage:Landroid/widget/ImageView;

    if-eqz v5, :cond_0

    if-eqz v2, :cond_0

    .line 164
    iget-object v5, p0, Lcom/samsung/musicplus/contents/normal/AlbumListFragment;->mCoverImage:Landroid/widget/ImageView;

    invoke-virtual {v5, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 167
    :cond_0
    iget-object v5, p0, Lcom/samsung/musicplus/contents/normal/AlbumListFragment;->mCoverText1:Landroid/widget/TextView;

    if-eqz v5, :cond_1

    .line 168
    iget-object v5, p0, Lcom/samsung/musicplus/contents/normal/AlbumListFragment;->mCoverText1:Landroid/widget/TextView;

    const-string v6, "album"

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 171
    :cond_1
    iget-object v5, p0, Lcom/samsung/musicplus/contents/normal/AlbumListFragment;->mCoverText2:Landroid/widget/TextView;

    if-eqz v5, :cond_2

    .line 172
    iget-boolean v5, p0, Lcom/samsung/musicplus/contents/normal/AlbumListFragment;->mIsSingleArtist:Z

    if-eqz v5, :cond_4

    .line 173
    iget-object v5, p0, Lcom/samsung/musicplus/contents/normal/AlbumListFragment;->mCoverText2:Landroid/widget/TextView;

    const-string v6, "artist"

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/samsung/musicplus/util/UiUtils;->transUnknownString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 179
    :cond_2
    :goto_0
    iget-object v5, p0, Lcom/samsung/musicplus/contents/normal/AlbumListFragment;->mCoverText3:Landroid/widget/TextView;

    if-eqz v5, :cond_3

    .line 180
    iget-object v5, p0, Lcom/samsung/musicplus/contents/normal/AlbumListFragment;->mCoverText3:Landroid/widget/TextView;

    const-string v6, "year_name"

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v6

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/samsung/musicplus/util/UiUtils;->transUnknownString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 184
    .end local v0    # "albumId":J
    .end local v2    # "bm":Landroid/graphics/Bitmap;
    :cond_3
    return-void

    .line 176
    .restart local v0    # "albumId":J
    .restart local v2    # "bm":Landroid/graphics/Bitmap;
    :cond_4
    iget-object v5, p0, Lcom/samsung/musicplus/contents/normal/AlbumListFragment;->mCoverText2:Landroid/widget/TextView;

    const v6, 0x7f1001be

    invoke-virtual {v3, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method protected createAdapter()Landroid/widget/CursorAdapter;
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 94
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/normal/AlbumListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    .line 95
    .local v2, "a":Landroid/app/Activity;
    iget v0, p0, Lcom/samsung/musicplus/contents/normal/AlbumListFragment;->mList:I

    invoke-static {v0}, Lcom/samsung/musicplus/util/ListUtils;->isGridType(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 96
    new-instance v0, Lcom/samsung/musicplus/contents/normal/AlbumListFragment$AlbumTabAdapter;

    const v3, 0x7f040029

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/samsung/musicplus/contents/normal/AlbumListFragment$AlbumTabAdapter;-><init>(Lcom/samsung/musicplus/contents/normal/AlbumListFragment;Landroid/content/Context;ILandroid/database/Cursor;I)V

    .line 101
    :goto_0
    return-object v0

    .line 98
    :cond_0
    iget v0, p0, Lcom/samsung/musicplus/contents/normal/AlbumListFragment;->mList:I

    invoke-static {v0}, Lcom/samsung/musicplus/util/ListUtils;->isTab(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 99
    new-instance v0, Lcom/samsung/musicplus/contents/normal/AlbumListFragment$AlbumTabAdapter;

    const v3, 0x7f040033

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/samsung/musicplus/contents/normal/AlbumListFragment$AlbumTabAdapter;-><init>(Lcom/samsung/musicplus/contents/normal/AlbumListFragment;Landroid/content/Context;ILandroid/database/Cursor;I)V

    goto :goto_0

    .line 101
    :cond_1
    new-instance v0, Lcom/samsung/musicplus/contents/normal/AlbumListFragment$AlbumListTrackAdapter;

    const v3, 0x7f040040

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/samsung/musicplus/contents/normal/AlbumListFragment$AlbumListTrackAdapter;-><init>(Lcom/samsung/musicplus/contents/normal/AlbumListFragment;Landroid/content/Context;ILandroid/database/Cursor;I)V

    goto :goto_0
.end method

.method protected getPrefKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 188
    const-string v0, "album_view_type"

    return-object v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 83
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 90
    return-void
.end method

.method public onChangeViewType(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 112
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;->onChangeViewType(I)V

    .line 113
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/normal/AlbumListFragment;->setHeaderView()V

    .line 114
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/normal/AlbumListFragment;->updateNumberView()V

    .line 115
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 65
    iget v0, p0, Lcom/samsung/musicplus/contents/normal/AlbumListFragment;->mList:I

    invoke-static {v0}, Lcom/samsung/musicplus/util/ListUtils;->isTrack(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 66
    const v0, 0x7f04001d

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 68
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V
    .locals 3
    .param p2, "data"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 119
    .local p1, "loader":Landroid/content/Loader;, "Landroid/content/Loader<Landroid/database/Cursor;>;"
    invoke-super {p0, p1, p2}, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;->onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V

    .line 120
    iget v1, p0, Lcom/samsung/musicplus/contents/normal/AlbumListFragment;->mList:I

    invoke-static {v1}, Lcom/samsung/musicplus/util/ListUtils;->isTrack(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 121
    invoke-static {p2}, Lcom/samsung/musicplus/util/UiUtils;->isSingleArtistAlbum(Landroid/database/Cursor;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/samsung/musicplus/contents/normal/AlbumListFragment;->mIsSingleArtist:Z

    .line 122
    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-nez v1, :cond_0

    .line 123
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/normal/AlbumListFragment;->isSplitSubListFragment()Z

    move-result v1

    if-nez v1, :cond_0

    .line 124
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/normal/AlbumListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    .line 127
    :cond_0
    invoke-direct {p0, p2}, Lcom/samsung/musicplus/contents/normal/AlbumListFragment;->updateCoverView(Landroid/database/Cursor;)V

    .line 130
    :cond_1
    iget-object v1, p0, Lcom/samsung/musicplus/contents/normal/AlbumListFragment;->mAdapter:Landroid/widget/CursorAdapter;

    iget-object v2, p0, Lcom/samsung/musicplus/contents/normal/AlbumListFragment;->mAdapter:Landroid/widget/CursorAdapter;

    invoke-virtual {v2}, Landroid/widget/CursorAdapter;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/CursorAdapter;->getItemId(I)J

    .line 131
    iget-object v1, p0, Lcom/samsung/musicplus/contents/normal/AlbumListFragment;->mAdapter:Landroid/widget/CursorAdapter;

    iget-object v2, p0, Lcom/samsung/musicplus/contents/normal/AlbumListFragment;->mAdapter:Landroid/widget/CursorAdapter;

    invoke-virtual {v2}, Landroid/widget/CursorAdapter;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/CursorAdapter;->getItemId(I)J

    .line 133
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/normal/AlbumListFragment;->getAbsListView()Landroid/widget/AbsListView;

    move-result-object v0

    .line 134
    .local v0, "lv":Landroid/widget/AbsListView;
    if-eqz v0, :cond_2

    instance-of v1, v0, Lcom/samsung/musicplus/widget/list/MusicListView;

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/normal/AlbumListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    instance-of v1, v1, Lcom/samsung/musicplus/contents/TrackActivity;

    if-eqz v1, :cond_2

    .line 137
    check-cast v0, Lcom/samsung/musicplus/widget/list/MusicListView;

    .end local v0    # "lv":Landroid/widget/AbsListView;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/widget/list/MusicListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 140
    :cond_2
    return-void
.end method

.method public bridge synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Landroid/content/Loader;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 46
    check-cast p2, Landroid/database/Cursor;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/samsung/musicplus/contents/normal/AlbumListFragment;->onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 106
    invoke-super {p0}, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;->onResume()V

    .line 107
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/normal/AlbumListFragment;->updateNumberView()V

    .line 108
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 73
    invoke-super {p0, p1, p2}, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 75
    iget v0, p0, Lcom/samsung/musicplus/contents/normal/AlbumListFragment;->mList:I

    invoke-static {v0}, Lcom/samsung/musicplus/util/ListUtils;->isTrack(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 76
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/contents/normal/AlbumListFragment;->initializeCoverView(Landroid/view/View;)V

    .line 79
    :cond_0
    return-void
.end method
