.class Lcom/samsung/musicplus/contents/square/MusicSquareFragment$AxisDialog$2;
.super Ljava/lang/Object;
.source "MusicSquareFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/contents/square/MusicSquareFragment$AxisDialog;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment$AxisDialog;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/contents/square/MusicSquareFragment$AxisDialog;)V
    .locals 0

    .prologue
    .line 1079
    iput-object p1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$AxisDialog$2;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment$AxisDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    .line 1082
    iget-object v1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$AxisDialog$2;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment$AxisDialog;

    invoke-virtual {v1}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$AxisDialog;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const v2, 0x1000a

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 1084
    .local v0, "fm":Landroid/app/Fragment;
    instance-of v1, v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    if-eqz v1, :cond_0

    .line 1085
    check-cast v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    .end local v0    # "fm":Landroid/app/Fragment;
    # invokes: Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->changeAxis(I)V
    invoke-static {v0, p2}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->access$2400(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;I)V

    .line 1088
    :cond_0
    # getter for: Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->sPreferences:Landroid/content/SharedPreferences;
    invoke-static {}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->access$2300()Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "square_theme"

    invoke-interface {v1, v2, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1089
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 1090
    return-void
.end method
