.class public Lcom/samsung/musicplus/contents/devices/DeviceMergeCursor;
.super Landroid/database/AbstractCursor;
.source "DeviceMergeCursor.java"


# instance fields
.field private mCursor:Landroid/database/Cursor;

.field private mCursors:[Landroid/database/Cursor;

.field private mObserver:Landroid/database/DataSetObserver;


# direct methods
.method public constructor <init>([Landroid/database/Cursor;)V
    .locals 2
    .param p1, "cursors"    # [Landroid/database/Cursor;

    .prologue
    .line 54
    invoke-direct {p0}, Landroid/database/AbstractCursor;-><init>()V

    .line 38
    new-instance v1, Lcom/samsung/musicplus/contents/devices/DeviceMergeCursor$1;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/contents/devices/DeviceMergeCursor$1;-><init>(Lcom/samsung/musicplus/contents/devices/DeviceMergeCursor;)V

    iput-object v1, p0, Lcom/samsung/musicplus/contents/devices/DeviceMergeCursor;->mObserver:Landroid/database/DataSetObserver;

    .line 55
    iput-object p1, p0, Lcom/samsung/musicplus/contents/devices/DeviceMergeCursor;->mCursors:[Landroid/database/Cursor;

    .line 56
    const/4 v1, 0x0

    aget-object v1, p1, v1

    iput-object v1, p0, Lcom/samsung/musicplus/contents/devices/DeviceMergeCursor;->mCursor:Landroid/database/Cursor;

    .line 58
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/samsung/musicplus/contents/devices/DeviceMergeCursor;->mCursors:[Landroid/database/Cursor;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 59
    iget-object v1, p0, Lcom/samsung/musicplus/contents/devices/DeviceMergeCursor;->mCursors:[Landroid/database/Cursor;

    aget-object v1, v1, v0

    if-nez v1, :cond_0

    .line 58
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 66
    :cond_1
    return-void
.end method

.method static synthetic access$002(Lcom/samsung/musicplus/contents/devices/DeviceMergeCursor;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/devices/DeviceMergeCursor;
    .param p1, "x1"    # I

    .prologue
    .line 33
    iput p1, p0, Lcom/samsung/musicplus/contents/devices/DeviceMergeCursor;->mPos:I

    return p1
.end method

.method static synthetic access$102(Lcom/samsung/musicplus/contents/devices/DeviceMergeCursor;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/devices/DeviceMergeCursor;
    .param p1, "x1"    # I

    .prologue
    .line 33
    iput p1, p0, Lcom/samsung/musicplus/contents/devices/DeviceMergeCursor;->mPos:I

    return p1
.end method


# virtual methods
.method public close()V
    .locals 3

    .prologue
    .line 189
    iget-object v2, p0, Lcom/samsung/musicplus/contents/devices/DeviceMergeCursor;->mCursors:[Landroid/database/Cursor;

    array-length v1, v2

    .line 190
    .local v1, "length":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_3

    .line 191
    iget-object v2, p0, Lcom/samsung/musicplus/contents/devices/DeviceMergeCursor;->mCursors:[Landroid/database/Cursor;

    aget-object v2, v2, v0

    if-nez v2, :cond_1

    .line 190
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 195
    :cond_1
    iget-object v2, p0, Lcom/samsung/musicplus/contents/devices/DeviceMergeCursor;->mCursors:[Landroid/database/Cursor;

    aget-object v2, v2, v0

    instance-of v2, v2, Landroid/database/MatrixCursor;

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/samsung/musicplus/contents/devices/DeviceMergeCursor;->mCursors:[Landroid/database/Cursor;

    aget-object v2, v2, v0

    instance-of v2, v2, Lcom/samsung/musicplus/contents/devices/DeviceMergeCursor;

    if-eqz v2, :cond_0

    .line 196
    :cond_2
    iget-object v2, p0, Lcom/samsung/musicplus/contents/devices/DeviceMergeCursor;->mCursors:[Landroid/database/Cursor;

    aget-object v2, v2, v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_1

    .line 199
    :cond_3
    invoke-super {p0}, Landroid/database/AbstractCursor;->close()V

    .line 200
    return-void
.end method

.method public deactivate()V
    .locals 3

    .prologue
    .line 175
    iget-object v2, p0, Lcom/samsung/musicplus/contents/devices/DeviceMergeCursor;->mCursors:[Landroid/database/Cursor;

    array-length v1, v2

    .line 176
    .local v1, "length":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 177
    iget-object v2, p0, Lcom/samsung/musicplus/contents/devices/DeviceMergeCursor;->mCursors:[Landroid/database/Cursor;

    aget-object v2, v2, v0

    if-eqz v2, :cond_0

    .line 178
    iget-object v2, p0, Lcom/samsung/musicplus/contents/devices/DeviceMergeCursor;->mCursors:[Landroid/database/Cursor;

    aget-object v2, v2, v0

    invoke-interface {v2}, Landroid/database/Cursor;->deactivate()V

    .line 176
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 181
    :cond_1
    invoke-super {p0}, Landroid/database/AbstractCursor;->deactivate()V

    .line 182
    return-void
.end method

.method public getBlob(I)[B
    .locals 1
    .param p1, "column"    # I

    .prologue
    .line 159
    iget-object v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceMergeCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    return-object v0
.end method

.method public getColumnNames()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceMergeCursor;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 166
    iget-object v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceMergeCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    move-result-object v0

    .line 168
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    goto :goto_0
.end method

.method public getCount()I
    .locals 4

    .prologue
    .line 71
    const/4 v0, 0x0

    .line 72
    .local v0, "count":I
    iget-object v3, p0, Lcom/samsung/musicplus/contents/devices/DeviceMergeCursor;->mCursors:[Landroid/database/Cursor;

    array-length v2, v3

    .line 73
    .local v2, "length":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 74
    iget-object v3, p0, Lcom/samsung/musicplus/contents/devices/DeviceMergeCursor;->mCursors:[Landroid/database/Cursor;

    aget-object v3, v3, v1

    if-eqz v3, :cond_0

    .line 75
    iget-object v3, p0, Lcom/samsung/musicplus/contents/devices/DeviceMergeCursor;->mCursors:[Landroid/database/Cursor;

    aget-object v3, v3, v1

    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I

    move-result v3

    add-int/2addr v0, v3

    .line 73
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 78
    :cond_1
    return v0
.end method

.method public getDouble(I)D
    .locals 2
    .param p1, "column"    # I

    .prologue
    .line 142
    iget-object v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceMergeCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v0

    return-wide v0
.end method

.method public getFloat(I)F
    .locals 1
    .param p1, "column"    # I

    .prologue
    .line 136
    iget-object v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceMergeCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getFloat(I)F

    move-result v0

    return v0
.end method

.method public getInt(I)I
    .locals 1
    .param p1, "column"    # I

    .prologue
    .line 124
    iget-object v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceMergeCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getLong(I)J
    .locals 2
    .param p1, "column"    # I

    .prologue
    .line 130
    iget-object v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceMergeCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public getShort(I)S
    .locals 1
    .param p1, "column"    # I

    .prologue
    .line 118
    iget-object v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceMergeCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getShort(I)S

    move-result v0

    return v0
.end method

.method public getString(I)Ljava/lang/String;
    .locals 1
    .param p1, "column"    # I

    .prologue
    .line 112
    iget-object v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceMergeCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getType(I)I
    .locals 1
    .param p1, "column"    # I

    .prologue
    .line 147
    iget-object v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceMergeCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getType(I)I

    move-result v0

    return v0
.end method

.method public isNull(I)Z
    .locals 1
    .param p1, "column"    # I

    .prologue
    .line 153
    iget-object v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceMergeCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    return v0
.end method

.method public onMove(II)Z
    .locals 6
    .param p1, "oldPosition"    # I
    .param p2, "newPosition"    # I

    .prologue
    .line 85
    const/4 v4, 0x0

    iput-object v4, p0, Lcom/samsung/musicplus/contents/devices/DeviceMergeCursor;->mCursor:Landroid/database/Cursor;

    .line 86
    const/4 v0, 0x0

    .line 87
    .local v0, "cursorStartPos":I
    iget-object v4, p0, Lcom/samsung/musicplus/contents/devices/DeviceMergeCursor;->mCursors:[Landroid/database/Cursor;

    array-length v2, v4

    .line 88
    .local v2, "length":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 89
    iget-object v4, p0, Lcom/samsung/musicplus/contents/devices/DeviceMergeCursor;->mCursors:[Landroid/database/Cursor;

    aget-object v4, v4, v1

    if-nez v4, :cond_0

    .line 88
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 93
    :cond_0
    iget-object v4, p0, Lcom/samsung/musicplus/contents/devices/DeviceMergeCursor;->mCursors:[Landroid/database/Cursor;

    aget-object v4, v4, v1

    invoke-interface {v4}, Landroid/database/Cursor;->getCount()I

    move-result v4

    add-int/2addr v4, v0

    if-ge p2, v4, :cond_2

    .line 94
    iget-object v4, p0, Lcom/samsung/musicplus/contents/devices/DeviceMergeCursor;->mCursors:[Landroid/database/Cursor;

    aget-object v4, v4, v1

    iput-object v4, p0, Lcom/samsung/musicplus/contents/devices/DeviceMergeCursor;->mCursor:Landroid/database/Cursor;

    .line 102
    :cond_1
    iget-object v4, p0, Lcom/samsung/musicplus/contents/devices/DeviceMergeCursor;->mCursor:Landroid/database/Cursor;

    if-eqz v4, :cond_3

    .line 103
    iget-object v4, p0, Lcom/samsung/musicplus/contents/devices/DeviceMergeCursor;->mCursor:Landroid/database/Cursor;

    sub-int v5, p2, v0

    invoke-interface {v4, v5}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v3

    .line 106
    :goto_2
    return v3

    .line 98
    :cond_2
    iget-object v4, p0, Lcom/samsung/musicplus/contents/devices/DeviceMergeCursor;->mCursors:[Landroid/database/Cursor;

    aget-object v4, v4, v1

    invoke-interface {v4}, Landroid/database/Cursor;->getCount()I

    move-result v4

    add-int/2addr v0, v4

    goto :goto_1

    .line 106
    :cond_3
    const/4 v3, 0x0

    goto :goto_2
.end method

.method public registerContentObserver(Landroid/database/ContentObserver;)V
    .locals 3
    .param p1, "observer"    # Landroid/database/ContentObserver;

    .prologue
    .line 205
    iget-object v2, p0, Lcom/samsung/musicplus/contents/devices/DeviceMergeCursor;->mCursors:[Landroid/database/Cursor;

    array-length v1, v2

    .line 206
    .local v1, "length":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 207
    iget-object v2, p0, Lcom/samsung/musicplus/contents/devices/DeviceMergeCursor;->mCursors:[Landroid/database/Cursor;

    aget-object v2, v2, v0

    if-eqz v2, :cond_0

    .line 208
    iget-object v2, p0, Lcom/samsung/musicplus/contents/devices/DeviceMergeCursor;->mCursors:[Landroid/database/Cursor;

    aget-object v2, v2, v0

    invoke-interface {v2, p1}, Landroid/database/Cursor;->registerContentObserver(Landroid/database/ContentObserver;)V

    .line 206
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 211
    :cond_1
    return-void
.end method

.method public registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 3
    .param p1, "observer"    # Landroid/database/DataSetObserver;

    .prologue
    .line 224
    iget-object v2, p0, Lcom/samsung/musicplus/contents/devices/DeviceMergeCursor;->mCursors:[Landroid/database/Cursor;

    array-length v1, v2

    .line 225
    .local v1, "length":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 226
    iget-object v2, p0, Lcom/samsung/musicplus/contents/devices/DeviceMergeCursor;->mCursors:[Landroid/database/Cursor;

    aget-object v2, v2, v0

    if-eqz v2, :cond_0

    .line 227
    iget-object v2, p0, Lcom/samsung/musicplus/contents/devices/DeviceMergeCursor;->mCursors:[Landroid/database/Cursor;

    aget-object v2, v2, v0

    invoke-interface {v2, p1}, Landroid/database/Cursor;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 225
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 230
    :cond_1
    return-void
.end method

.method public requery()Z
    .locals 3

    .prologue
    .line 245
    iget-object v2, p0, Lcom/samsung/musicplus/contents/devices/DeviceMergeCursor;->mCursors:[Landroid/database/Cursor;

    array-length v1, v2

    .line 246
    .local v1, "length":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_2

    .line 247
    iget-object v2, p0, Lcom/samsung/musicplus/contents/devices/DeviceMergeCursor;->mCursors:[Landroid/database/Cursor;

    aget-object v2, v2, v0

    if-nez v2, :cond_1

    .line 246
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 251
    :cond_1
    iget-object v2, p0, Lcom/samsung/musicplus/contents/devices/DeviceMergeCursor;->mCursors:[Landroid/database/Cursor;

    aget-object v2, v2, v0

    invoke-interface {v2}, Landroid/database/Cursor;->requery()Z

    move-result v2

    if-nez v2, :cond_0

    .line 252
    const/4 v2, 0x0

    .line 256
    :goto_1
    return v2

    :cond_2
    const/4 v2, 0x1

    goto :goto_1
.end method

.method public unregisterContentObserver(Landroid/database/ContentObserver;)V
    .locals 3
    .param p1, "observer"    # Landroid/database/ContentObserver;

    .prologue
    .line 214
    iget-object v2, p0, Lcom/samsung/musicplus/contents/devices/DeviceMergeCursor;->mCursors:[Landroid/database/Cursor;

    array-length v1, v2

    .line 215
    .local v1, "length":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 216
    iget-object v2, p0, Lcom/samsung/musicplus/contents/devices/DeviceMergeCursor;->mCursors:[Landroid/database/Cursor;

    aget-object v2, v2, v0

    if-eqz v2, :cond_0

    .line 217
    iget-object v2, p0, Lcom/samsung/musicplus/contents/devices/DeviceMergeCursor;->mCursors:[Landroid/database/Cursor;

    aget-object v2, v2, v0

    invoke-interface {v2, p1}, Landroid/database/Cursor;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 215
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 220
    :cond_1
    return-void
.end method

.method public unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 3
    .param p1, "observer"    # Landroid/database/DataSetObserver;

    .prologue
    .line 234
    iget-object v2, p0, Lcom/samsung/musicplus/contents/devices/DeviceMergeCursor;->mCursors:[Landroid/database/Cursor;

    array-length v1, v2

    .line 235
    .local v1, "length":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 236
    iget-object v2, p0, Lcom/samsung/musicplus/contents/devices/DeviceMergeCursor;->mCursors:[Landroid/database/Cursor;

    aget-object v2, v2, v0

    if-eqz v2, :cond_0

    .line 237
    iget-object v2, p0, Lcom/samsung/musicplus/contents/devices/DeviceMergeCursor;->mCursors:[Landroid/database/Cursor;

    aget-object v2, v2, v0

    invoke-interface {v2, p1}, Landroid/database/Cursor;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 235
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 240
    :cond_1
    return-void
.end method
