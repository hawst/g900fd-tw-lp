.class Lcom/samsung/musicplus/contents/devices/DeviceContentsListFragment$ContentsTrackAdapter;
.super Lcom/samsung/musicplus/widget/list/TrackListAdapter;
.source "DeviceContentsListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/contents/devices/DeviceContentsListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ContentsTrackAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/contents/devices/DeviceContentsListFragment;


# direct methods
.method public constructor <init>(Lcom/samsung/musicplus/contents/devices/DeviceContentsListFragment;Landroid/content/Context;ILandroid/database/Cursor;I)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "layout"    # I
    .param p4, "c"    # Landroid/database/Cursor;
    .param p5, "flags"    # I

    .prologue
    .line 268
    iput-object p1, p0, Lcom/samsung/musicplus/contents/devices/DeviceContentsListFragment$ContentsTrackAdapter;->this$0:Lcom/samsung/musicplus/contents/devices/DeviceContentsListFragment;

    .line 269
    invoke-direct {p0, p2, p3, p4, p5}, Lcom/samsung/musicplus/widget/list/TrackListAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;I)V

    .line 270
    return-void
.end method


# virtual methods
.method protected bindTextView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "c"    # Landroid/database/Cursor;

    .prologue
    .line 274
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;

    .line 275
    .local v2, "vh":Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;
    const-string v3, "title"

    invoke-interface {p3, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p3, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 276
    .local v0, "title":Ljava/lang/String;
    iget-object v1, v2, Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;->text1:Landroid/widget/TextView;

    check-cast v1, Lcom/samsung/musicplus/widget/MatchedTextView;

    .line 277
    .local v1, "tv1":Lcom/samsung/musicplus/widget/MatchedTextView;
    invoke-virtual {v1}, Lcom/samsung/musicplus/widget/MatchedTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/musicplus/contents/devices/DeviceContentsListFragment$ContentsTrackAdapter;->this$0:Lcom/samsung/musicplus/contents/devices/DeviceContentsListFragment;

    # getter for: Lcom/samsung/musicplus/contents/devices/DeviceContentsListFragment;->mQueryText:Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/musicplus/contents/devices/DeviceContentsListFragment;->access$000(Lcom/samsung/musicplus/contents/devices/DeviceContentsListFragment;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v3, v0, v4}, Lcom/samsung/musicplus/util/UiUtils;->setTextItem(Lcom/samsung/musicplus/widget/MatchedTextView;Landroid/text/TextPaint;Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    return-void
.end method

.method protected getArtKey(Landroid/database/Cursor;)Ljava/lang/Object;
    .locals 2
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 282
    const-string v0, "album_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method protected getArtworkInBackground(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/Object;I)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "iv"    # Landroid/widget/ImageView;
    .param p3, "artKey"    # Ljava/lang/Object;
    .param p4, "resId"    # I

    .prologue
    .line 288
    const v1, 0x7f02003b

    invoke-virtual {p2, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 290
    invoke-static {}, Lcom/samsung/musicplus/util/AlbumArtLoader;->getAlbumArtLoader()Lcom/samsung/musicplus/util/IAlbumArtLoader;

    move-result-object v0

    .line 291
    .local v0, "l":Lcom/samsung/musicplus/util/IAlbumArtLoader;
    sget-object v2, Lcom/samsung/musicplus/util/AlbumArtUtils;->S_LINK_DUMY_ARTWORK_URI_STRING:Ljava/lang/String;

    iget v5, p0, Lcom/samsung/musicplus/contents/devices/DeviceContentsListFragment$ContentsTrackAdapter;->mAlbumArtSize:I

    move-object v1, p1

    move-object v3, p3

    move-object v4, p2

    move v6, p4

    invoke-interface/range {v0 .. v6}, Lcom/samsung/musicplus/util/IAlbumArtLoader;->loadArtwork(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Object;Landroid/widget/ImageView;II)V

    .line 293
    return-void
.end method

.method protected getCachedArtwork(Ljava/lang/Object;)Landroid/graphics/drawable/Drawable;
    .locals 2
    .param p1, "artKey"    # Ljava/lang/Object;

    .prologue
    .line 297
    check-cast p1, Ljava/lang/Long;

    .end local p1    # "artKey":Ljava/lang/Object;
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/samsung/musicplus/util/AlbumArtUtils;->getCachedSlinkArtwork(J)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method
