.class public Lcom/samsung/musicplus/contents/normal/PlayListFragment;
.super Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;
.source "PlayListFragment.java"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;
.implements Lcom/samsung/musicplus/settings/PlayerSettingPreference;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/contents/normal/PlayListFragment$PlayListTrackAdapter;,
        Lcom/samsung/musicplus/contents/normal/PlayListFragment$PlayListTabAdapter;
    }
.end annotation


# static fields
.field public static final ACTION_REMOVE:Ljava/lang/String; = "com.samsung.musicplus.remove"

.field private static final MENU_ADD_TRACKS:I = 0x7f0d01e9

.field private static final PREF_KEY_PLAYLIST_VIEW_TYPE:Ljava/lang/String; = "playlist_view_type"


# instance fields
.field private mCountOfDefaultPlaylists:I

.field private mKeyListener:Landroid/content/BroadcastReceiver;

.field private mPlaylistId:J

.field private mPlaylistToBeRestored:[I

.field private mRemove:Landroid/view/MenuItem;

.field private mViewMode:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;-><init>()V

    .line 206
    new-instance v0, Lcom/samsung/musicplus/contents/normal/PlayListFragment$1;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/contents/normal/PlayListFragment$1;-><init>(Lcom/samsung/musicplus/contents/normal/PlayListFragment;)V

    iput-object v0, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mKeyListener:Landroid/content/BroadcastReceiver;

    .line 851
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/contents/normal/PlayListFragment;)Landroid/view/MenuItem;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/normal/PlayListFragment;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mRemove:Landroid/view/MenuItem;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/musicplus/contents/normal/PlayListFragment;)[I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/normal/PlayListFragment;

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->getSelectedPositions()[I

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/musicplus/contents/normal/PlayListFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/normal/PlayListFragment;

    .prologue
    .line 68
    iget v0, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mCountOfDefaultPlaylists:I

    return v0
.end method

.method static synthetic access$300(Lcom/samsung/musicplus/contents/normal/PlayListFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/normal/PlayListFragment;

    .prologue
    .line 68
    iget v0, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mCountOfData:I

    return v0
.end method

.method static synthetic access$400(Lcom/samsung/musicplus/contents/normal/PlayListFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/normal/PlayListFragment;

    .prologue
    .line 68
    iget v0, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mViewMode:I

    return v0
.end method

.method static synthetic access$500(Lcom/samsung/musicplus/contents/normal/PlayListFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/normal/PlayListFragment;

    .prologue
    .line 68
    iget v0, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mList:I

    return v0
.end method

.method static synthetic access$600(Lcom/samsung/musicplus/contents/normal/PlayListFragment;)J
    .locals 2
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/normal/PlayListFragment;

    .prologue
    .line 68
    iget-wide v0, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mPlaylistId:J

    return-wide v0
.end method

.method private addSongViaTabSelector(J)V
    .locals 5
    .param p1, "playlistId"    # J

    .prologue
    .line 330
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 331
    .local v0, "i":Landroid/content/Intent;
    const-wide/16 v2, -0xb

    cmp-long v1, p1, v2

    if-nez v1, :cond_0

    .line 332
    const-string v1, "list_mode"

    const/16 v2, 0x14

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 340
    :goto_0
    const-string v1, "header_mode"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 341
    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->startActivity(Landroid/content/Intent;)V

    .line 342
    return-void

    .line 334
    :cond_0
    const-string v1, "list_mode"

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 336
    const-string v1, "playlist_id"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    goto :goto_0
.end method

.method private getPlaylistOrder()[I
    .locals 8

    .prologue
    .line 465
    iget-object v5, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mPreferences:Landroid/content/SharedPreferences;

    const-string v6, "playlist_menu_list"

    const-string v7, "-11|-12|-13|-14"

    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 467
    .local v3, "playlistOrder":Ljava/lang/String;
    new-instance v4, Ljava/util/StringTokenizer;

    const-string v5, "|"

    invoke-direct {v4, v3, v5}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 468
    .local v4, "strToken":Ljava/util/StringTokenizer;
    invoke-virtual {v4}, Ljava/util/StringTokenizer;->countTokens()I

    move-result v0

    .line 469
    .local v0, "count":I
    new-array v2, v0, [I

    .line 471
    .local v2, "playlist":[I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 472
    invoke-virtual {v4}, Ljava/util/StringTokenizer;->nextElement()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    aput v5, v2, v1

    .line 471
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 474
    :cond_0
    return-object v2
.end method

.method private getSelectedPositions()[I
    .locals 10

    .prologue
    const/4 v6, 0x0

    .line 219
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->getAbsListView()Landroid/widget/AbsListView;

    move-result-object v4

    .line 220
    .local v4, "lv":Landroid/widget/AbsListView;
    if-nez v4, :cond_1

    .line 244
    :cond_0
    return-object v6

    .line 224
    :cond_1
    invoke-virtual {v4}, Landroid/widget/AbsListView;->getCheckedItemPositions()Landroid/util/SparseBooleanArray;

    move-result-object v8

    .line 225
    .local v8, "sp":Landroid/util/SparseBooleanArray;
    if-eqz v8, :cond_0

    invoke-virtual {v8}, Landroid/util/SparseBooleanArray;->size()I

    move-result v9

    if-eqz v9, :cond_0

    .line 230
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 231
    .local v5, "positionList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-virtual {v8}, Landroid/util/SparseBooleanArray;->size()I

    move-result v7

    .line 232
    .local v7, "size":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v7, :cond_3

    .line 233
    invoke-virtual {v8, v0}, Landroid/util/SparseBooleanArray;->valueAt(I)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 234
    invoke-virtual {v8, v0}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v5, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 232
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 239
    :cond_3
    const/4 v0, 0x0

    .line 240
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v9

    new-array v6, v9, [I

    .line 241
    .local v6, "positions":[I
    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v9

    if-eqz v9, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    .line 242
    .local v3, "in":Ljava/lang/Integer;
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "i":I
    .local v1, "i":I
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v9

    aput v9, v6, v0

    move v0, v1

    .line 243
    .end local v1    # "i":I
    .restart local v0    # "i":I
    goto :goto_1
.end method

.method private makeAddToPlaylistItem(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 7
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 377
    new-instance v3, Landroid/database/MatrixCursor;

    sget-object v4, Lcom/samsung/musicplus/util/ListUtils;->PLAY_LIST_PROJECTION:[Ljava/lang/String;

    invoke-direct {v3, v4}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 378
    .local v3, "playListCursor":Landroid/database/MatrixCursor;
    sget-object v4, Lcom/samsung/musicplus/util/ListUtils;->PLAY_LIST_PROJECTION:[Ljava/lang/String;

    array-length v2, v4

    .line 379
    .local v2, "length":I
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 381
    .local v1, "context":Landroid/content/Context;
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 382
    .local v0, "addtoPlaylist":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    const-wide/16 v4, -0x10

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 383
    const v4, 0x7f1000ac

    invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 384
    const-string v4, "@"

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 385
    invoke-virtual {v3, v0}, Landroid/database/MatrixCursor;->addRow(Ljava/lang/Iterable;)V

    .line 386
    new-instance v4, Landroid/database/MergeCursor;

    const/4 v5, 0x2

    new-array v5, v5, [Landroid/database/Cursor;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    const/4 v6, 0x1

    aput-object v3, v5, v6

    invoke-direct {v4, v5}, Landroid/database/MergeCursor;-><init>([Landroid/database/Cursor;)V

    return-object v4
.end method

.method private makePlayListDefaultItems(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 14
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 394
    new-instance v7, Landroid/database/MatrixCursor;

    sget-object v11, Lcom/samsung/musicplus/util/ListUtils;->PLAY_LIST_PROJECTION:[Ljava/lang/String;

    invoke-direct {v7, v11}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 395
    .local v7, "playListCursor":Landroid/database/MatrixCursor;
    sget-object v11, Lcom/samsung/musicplus/util/ListUtils;->PLAY_LIST_PROJECTION:[Ljava/lang/String;

    array-length v4, v11

    .line 396
    .local v4, "length":I
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v11

    invoke-virtual {v11}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 397
    .local v0, "context":Landroid/content/Context;
    iget v11, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mList:I

    const/high16 v12, 0x40000

    and-int/2addr v11, v12

    iput v11, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mViewMode:I

    .line 398
    iget v11, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mViewMode:I

    const/high16 v12, 0x40000

    if-eq v11, v12, :cond_0

    .line 400
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 401
    .local v2, "defaultPlaylists":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    const-wide/16 v12, -0xf

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {v2, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 402
    const v11, 0x7f100050

    invoke-virtual {v0, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v2, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 403
    const-string v11, "@"

    invoke-virtual {v2, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 404
    invoke-virtual {v7, v2}, Landroid/database/MatrixCursor;->addRow(Ljava/lang/Iterable;)V

    .line 407
    .end local v2    # "defaultPlaylists":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    :cond_0
    iget-object v11, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mPlaylistToBeRestored:[I

    array-length v1, v11

    .line 408
    .local v1, "count":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v1, :cond_1

    .line 409
    iget-object v11, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mPlaylistToBeRestored:[I

    aget v11, v11, v3

    packed-switch v11, :pswitch_data_0

    .line 408
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 411
    :pswitch_0
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 412
    .local v8, "quickList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    const-wide/16 v12, -0xb

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {v8, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 413
    const v11, 0x7f10008a

    invoke-virtual {v0, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v8, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 414
    const-string v11, "@"

    invoke-virtual {v8, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 415
    invoke-virtual {v7, v8}, Landroid/database/MatrixCursor;->addRow(Ljava/lang/Iterable;)V

    goto :goto_1

    .line 418
    .end local v8    # "quickList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    :pswitch_1
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 419
    .local v5, "mostPlayedkList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    const-wide/16 v12, -0xc

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {v5, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 420
    const v11, 0x7f1000cc

    invoke-virtual {v0, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v5, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 421
    const-string v11, "@"

    invoke-virtual {v5, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 422
    invoke-virtual {v7, v5}, Landroid/database/MatrixCursor;->addRow(Ljava/lang/Iterable;)V

    goto :goto_1

    .line 425
    .end local v5    # "mostPlayedkList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    :pswitch_2
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 426
    .local v10, "recentlyPlayedkList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    const-wide/16 v12, -0xd

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 427
    const v11, 0x7f10012c

    invoke-virtual {v0, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 428
    const-string v11, "@"

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 429
    invoke-virtual {v7, v10}, Landroid/database/MatrixCursor;->addRow(Ljava/lang/Iterable;)V

    goto :goto_1

    .line 432
    .end local v10    # "recentlyPlayedkList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    :pswitch_3
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 433
    .local v9, "recentlyAddedkList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    const-wide/16 v12, -0xe

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {v9, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 434
    const v11, 0x7f10012b

    invoke-virtual {v0, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 435
    const-string v11, "@"

    invoke-virtual {v9, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 436
    invoke-virtual {v7, v9}, Landroid/database/MatrixCursor;->addRow(Ljava/lang/Iterable;)V

    goto/16 :goto_1

    .line 443
    .end local v9    # "recentlyAddedkList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    :cond_1
    iget v11, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mViewMode:I

    const/high16 v12, 0x40000

    if-eq v11, v12, :cond_2

    .line 445
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6, v4}, Ljava/util/ArrayList;-><init>(I)V

    .line 446
    .local v6, "myPlaylists":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    const-wide/16 v12, -0xf

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {v6, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 447
    const v11, 0x7f1000db

    invoke-virtual {v0, v11}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v6, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 448
    const-string v11, "@"

    invoke-virtual {v6, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 449
    invoke-virtual {v7, v6}, Landroid/database/MatrixCursor;->addRow(Ljava/lang/Iterable;)V

    .line 452
    .end local v6    # "myPlaylists":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    :cond_2
    invoke-virtual {v7}, Landroid/database/MatrixCursor;->getCount()I

    move-result v11

    iput v11, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mCountOfDefaultPlaylists:I

    .line 453
    iget v11, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mCountOfData:I

    iget v12, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mCountOfDefaultPlaylists:I

    add-int/2addr v11, v12

    iput v11, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mCountOfData:I

    .line 454
    new-instance v11, Landroid/database/MergeCursor;

    const/4 v12, 0x2

    new-array v12, v12, [Landroid/database/Cursor;

    const/4 v13, 0x0

    aput-object v7, v12, v13

    const/4 v13, 0x1

    aput-object p1, v12, v13

    invoke-direct {v11, v12}, Landroid/database/MergeCursor;-><init>([Landroid/database/Cursor;)V

    return-object v11

    .line 409
    :pswitch_data_0
    .packed-switch -0xe
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private prepareTabOptionMenu(Landroid/view/Menu;)V
    .locals 6
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 308
    const v3, 0x7f0d01f8

    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 309
    .local v0, "createPlaylist":Landroid/view/MenuItem;
    const v3, 0x7f0d01d0

    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 310
    .local v1, "delete":Landroid/view/MenuItem;
    const v3, 0x7f0d01cf

    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 311
    .local v2, "remove":Landroid/view/MenuItem;
    if-eqz v0, :cond_0

    .line 312
    invoke-interface {v0, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 315
    :cond_0
    if-eqz v1, :cond_1

    .line 316
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->getNumberOfItmes()I

    move-result v3

    if-lez v3, :cond_3

    .line 317
    invoke-interface {v1, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 323
    :cond_1
    :goto_0
    if-eqz v2, :cond_2

    .line 324
    invoke-interface {v2, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 327
    :cond_2
    return-void

    .line 319
    :cond_3
    invoke-interface {v1, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method private prepareTrackOptionMenu(Landroid/view/Menu;J)V
    .locals 12
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "plid"    # J

    .prologue
    const v10, 0x7f0d01fa

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 247
    iget v5, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mList:I

    invoke-virtual {p0, v5}, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->getListSongCount(I)I

    move-result v2

    .line 250
    .local v2, "count":I
    const v5, 0x7f0d01d0

    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    .line 251
    .local v3, "delete":Landroid/view/MenuItem;
    if-eqz v3, :cond_0

    .line 252
    invoke-interface {v3, v9}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 257
    :cond_0
    const v5, 0x7f0d01cf

    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mRemove:Landroid/view/MenuItem;

    .line 258
    iget-wide v6, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mPlaylistId:J

    long-to-int v5, v6

    packed-switch v5, :pswitch_data_0

    .line 275
    :goto_0
    invoke-static {p2, p3}, Lcom/samsung/musicplus/util/ListUtils;->isUserMadePlayList(J)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 276
    const v5, 0x7f0d01ce

    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v4

    .line 278
    .local v4, "editItem":Landroid/view/MenuItem;
    if-eqz v4, :cond_1

    .line 279
    invoke-interface {v4, v8}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 281
    :cond_1
    if-lez v2, :cond_5

    .line 282
    iget-object v5, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mRemove:Landroid/view/MenuItem;

    invoke-interface {v5, v8}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 287
    :goto_1
    if-le v2, v8, :cond_2

    .line 288
    const v5, 0x7f0d01fb

    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 289
    .local v1, "changeOrder":Landroid/view/MenuItem;
    invoke-interface {v1, v8}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 301
    .end local v1    # "changeOrder":Landroid/view/MenuItem;
    .end local v4    # "editItem":Landroid/view/MenuItem;
    :cond_2
    :goto_2
    const v5, 0x7f0d01e9

    invoke-interface {p1, v5}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 302
    .local v0, "addItem":Landroid/view/MenuItem;
    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/musicplus/util/ListUtils;->hasNoSongs(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 303
    invoke-interface {v0, v9}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 305
    :cond_3
    return-void

    .line 260
    .end local v0    # "addItem":Landroid/view/MenuItem;
    :pswitch_0
    iget-object v5, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mRemove:Landroid/view/MenuItem;

    const v6, 0x7f1000b6

    invoke-interface {v5, v6}, Landroid/view/MenuItem;->setTitle(I)Landroid/view/MenuItem;

    .line 264
    :pswitch_1
    if-lez v2, :cond_4

    .line 265
    iget-object v5, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mRemove:Landroid/view/MenuItem;

    invoke-interface {v5, v8}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 267
    :cond_4
    iget-object v5, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mRemove:Landroid/view/MenuItem;

    invoke-interface {v5, v9}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 284
    .restart local v4    # "editItem":Landroid/view/MenuItem;
    :cond_5
    iget-object v5, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mRemove:Landroid/view/MenuItem;

    invoke-interface {v5, v9}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_1

    .line 293
    .end local v4    # "editItem":Landroid/view/MenuItem;
    :cond_6
    if-lez v2, :cond_7

    .line 294
    invoke-interface {p1, v10}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    invoke-interface {v5, v8}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_2

    .line 296
    :cond_7
    invoke-interface {p1, v10}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    invoke-interface {v5, v9}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_2

    .line 258
    :pswitch_data_0
    .packed-switch -0xe
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method protected addOtherView(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 3
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 346
    iget v1, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mList:I

    invoke-static {v1}, Lcom/samsung/musicplus/util/ListUtils;->isTrack(I)Z

    move-result v0

    .line 347
    .local v0, "isTrack":Z
    if-eqz v0, :cond_0

    .line 348
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;->addOtherView(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v1

    .line 356
    :goto_0
    return-object v1

    .line 350
    :cond_0
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->makeAddToPlaylistItem(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v1

    invoke-super {p0, v1}, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;->addOtherView(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object p1

    .line 351
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    iput v1, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mCountOfData:I

    .line 352
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->makePlayListDefaultItems(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object p1

    .line 353
    iget-object v1, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mIndex:Lcom/samsung/musicplus/widget/database/IndexView;

    if-eqz v1, :cond_1

    .line 354
    iget-object v1, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mIndex:Lcom/samsung/musicplus/widget/database/IndexView;

    iget v2, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mCountOfDefaultPlaylists:I

    invoke-virtual {v1, v2}, Lcom/samsung/musicplus/widget/database/IndexView;->setDefaultViewCount(I)V

    :cond_1
    move-object v1, p1

    .line 356
    goto :goto_0
.end method

.method protected createAdapter()Landroid/widget/CursorAdapter;
    .locals 6

    .prologue
    const v3, 0x7f040033

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 132
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    .line 133
    .local v2, "a":Landroid/app/Activity;
    iget v0, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mList:I

    invoke-static {v0}, Lcom/samsung/musicplus/util/ListUtils;->isGridType(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 134
    new-instance v0, Lcom/samsung/musicplus/contents/normal/PlayListFragment$PlayListTabAdapter;

    const v3, 0x7f040029

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/samsung/musicplus/contents/normal/PlayListFragment$PlayListTabAdapter;-><init>(Lcom/samsung/musicplus/contents/normal/PlayListFragment;Landroid/content/Context;ILandroid/database/Cursor;I)V

    .line 139
    :goto_0
    return-object v0

    .line 136
    :cond_0
    iget v0, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mList:I

    invoke-static {v0}, Lcom/samsung/musicplus/util/ListUtils;->isTab(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 137
    new-instance v0, Lcom/samsung/musicplus/contents/normal/PlayListFragment$PlayListTabAdapter;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/samsung/musicplus/contents/normal/PlayListFragment$PlayListTabAdapter;-><init>(Lcom/samsung/musicplus/contents/normal/PlayListFragment;Landroid/content/Context;ILandroid/database/Cursor;I)V

    goto :goto_0

    .line 139
    :cond_1
    new-instance v0, Lcom/samsung/musicplus/contents/normal/PlayListFragment$PlayListTrackAdapter;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/samsung/musicplus/contents/normal/PlayListFragment$PlayListTrackAdapter;-><init>(Lcom/samsung/musicplus/contents/normal/PlayListFragment;Landroid/content/Context;ILandroid/database/Cursor;I)V

    goto :goto_0
.end method

.method protected createMusicMenus()Lcom/samsung/musicplus/common/menu/AbstractMusicMenus;
    .locals 1

    .prologue
    .line 82
    new-instance v0, Lcom/samsung/musicplus/contents/menu/PlayListMenus;

    invoke-direct {v0}, Lcom/samsung/musicplus/contents/menu/PlayListMenus;-><init>()V

    return-object v0
.end method

.method protected getAudioIds([J[I)[J
    .locals 6
    .param p1, "selectedItemIds"    # [J
    .param p2, "positions"    # [I

    .prologue
    .line 563
    if-nez p2, :cond_1

    .line 564
    const/4 v3, 0x0

    new-array v0, v3, [J

    .line 572
    :cond_0
    return-object v0

    .line 567
    :cond_1
    array-length v2, p2

    .line 568
    .local v2, "length":I
    new-array v0, v2, [J

    .line 569
    .local v0, "audioIds":[J
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 570
    aget v3, p2, v1

    invoke-virtual {p0, v3}, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->getAudioId(I)J

    move-result-wide v4

    aput-wide v4, v0, v1

    .line 569
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method protected getNumberOfItmes()I
    .locals 2

    .prologue
    .line 509
    iget v0, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mCountOfData:I

    iget v1, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mCountOfDefaultPlaylists:I

    sub-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method protected getPrefKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 882
    const-string v0, "playlist_view_type"

    return-object v0
.end method

.method protected initializeList()V
    .locals 3

    .prologue
    .line 482
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->getView()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 483
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->getView()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f0d008f

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 484
    .local v0, "header":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 485
    iget v1, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mList:I

    invoke-static {v1}, Lcom/samsung/musicplus/util/ListUtils;->isGridType(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 486
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 491
    :cond_0
    :goto_0
    invoke-super {p0}, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;->initializeList()V

    .line 493
    .end local v0    # "header":Landroid/view/View;
    :cond_1
    return-void

    .line 488
    .restart local v0    # "header":Landroid/view/View;
    :cond_2
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method protected isShowNumberView(I)Z
    .locals 1
    .param p1, "count"    # I

    .prologue
    .line 518
    iget v0, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mList:I

    invoke-static {v0}, Lcom/samsung/musicplus/util/ListUtils;->isGridType(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 519
    const/4 v0, 0x1

    .line 521
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected needEmptyView(Landroid/database/Cursor;)Z
    .locals 1
    .param p1, "data"    # Landroid/database/Cursor;

    .prologue
    .line 200
    iget v0, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mList:I

    invoke-static {v0}, Lcom/samsung/musicplus/util/ListUtils;->isTab(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 201
    const/4 v0, 0x0

    .line 203
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;->needEmptyView(Landroid/database/Cursor;)Z

    move-result v0

    goto :goto_0
.end method

.method public onActionItemClicked(Landroid/view/MenuItem;[J[I)V
    .locals 8
    .param p1, "item"    # Landroid/view/MenuItem;
    .param p2, "selectedItemIds"    # [J
    .param p3, "selectedPositions"    # [I

    .prologue
    const/4 v6, 0x0

    .line 616
    iget-boolean v0, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mIsPause:Z

    if-eqz v0, :cond_0

    .line 650
    :goto_0
    return-void

    .line 620
    :cond_0
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 648
    :cond_1
    :pswitch_0
    invoke-virtual {p0, p2, p3}, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->getAudioIds([J[I)[J

    move-result-object v7

    .line 649
    .local v7, "audioIds":[J
    invoke-super {p0, p1, v7, p3}, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;->onActionItemClicked(Landroid/view/MenuItem;[J[I)V

    goto :goto_0

    .line 622
    .end local v7    # "audioIds":[J
    :pswitch_1
    new-instance v0, Lcom/samsung/musicplus/dialog/EditTitleDialog;

    iget v1, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mList:I

    const/16 v2, 0x8

    aget-wide v4, p2, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/musicplus/dialog/EditTitleDialog;-><init>(IILjava/lang/String;)V

    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "edit_title"

    invoke-virtual {v0, v1, v2}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 624
    invoke-static {}, Lcom/samsung/musicplus/widget/fragment/ActionModeRemoteHandler;->getInstance()Lcom/samsung/musicplus/widget/fragment/ActionModeRemoteHandler;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/samsung/musicplus/widget/fragment/ActionModeRemoteHandler;->registerActionModeHandle(Lcom/samsung/musicplus/widget/fragment/ActionModeRemoteHandler$ActionModeRemoteHandle;)V

    goto :goto_0

    .line 628
    :pswitch_2
    iget v0, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mList:I

    invoke-static {v0}, Lcom/samsung/musicplus/util/ListUtils;->isPlaylistTabContent(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 629
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;->onActionItemClicked(Landroid/view/MenuItem;[J[I)V

    goto :goto_0

    .line 635
    :cond_2
    :pswitch_3
    new-instance v0, Lcom/samsung/musicplus/util/FileOperationTask$RemoveTask;

    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mKey:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    move-object v4, p2

    move-object v5, p2

    invoke-direct/range {v0 .. v6}, Lcom/samsung/musicplus/util/FileOperationTask$RemoveTask;-><init>(Landroid/app/Activity;J[J[JZ)V

    new-array v1, v6, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/util/FileOperationTask$RemoveTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 637
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->finishActionMode()V

    goto :goto_0

    .line 640
    :pswitch_4
    iget v0, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mList:I

    invoke-static {v0}, Lcom/samsung/musicplus/util/ListUtils;->isPlaylistTabContent(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 641
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;->onActionItemClicked(Landroid/view/MenuItem;[J[I)V

    goto :goto_0

    .line 620
    nop

    :pswitch_data_0
    .packed-switch 0x7f0d01ca
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public onChangeViewType(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 183
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;->onChangeViewType(I)V

    .line 184
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->setHeaderView()V

    .line 185
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->updateNumberView()V

    .line 186
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 94
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 96
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->getPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mPreferences:Landroid/content/SharedPreferences;

    .line 97
    iget-object v0, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 98
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->getPlaylistOrder()[I

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mPlaylistToBeRestored:[I

    .line 102
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/musicplus/util/UiUtils;->getFavorietListId(Landroid/content/Context;)J

    .line 104
    iget-object v0, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mKey:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 105
    iget-object v0, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mKey:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mPlaylistId:J

    .line 107
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mKeyListener:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "com.samsung.musicplus.remove"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 108
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 4
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "inflater"    # Landroid/view/MenuInflater;

    .prologue
    .line 112
    iget-wide v0, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mPlaylistId:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    iget-wide v0, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mPlaylistId:J

    const-wide/16 v2, -0xb

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 113
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f120006

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 115
    :cond_1
    invoke-super {p0, p1, p2}, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 116
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 149
    iget-object v0, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 150
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mKeyListener:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 151
    invoke-super {p0}, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;->onDestroy()V

    .line 152
    return-void
.end method

.method public onListItemClick(Landroid/widget/AbsListView;Landroid/view/View;IJ)V
    .locals 4
    .param p1, "l"    # Landroid/widget/AbsListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    .line 156
    iget-boolean v0, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mIsPause:Z

    if-eqz v0, :cond_0

    .line 170
    :goto_0
    return-void

    .line 159
    :cond_0
    const-wide/16 v0, -0x10

    cmp-long v0, p4, v0

    if-nez v0, :cond_1

    .line 160
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mList:I

    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/musicplus/util/FileOperationTask$AddToPlaylistTask;->makePlaylist(Landroid/content/Context;ILandroid/app/FragmentManager;)V

    goto :goto_0

    .line 165
    :cond_1
    iget v0, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mList:I

    iget-object v1, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mKey:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/samsung/musicplus/util/ListUtils;->isUserPlaylistTrack(ILjava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 167
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->setPlayListByForce(Z)V

    .line 169
    :cond_2
    invoke-super/range {p0 .. p5}, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;->onListItemClick(Landroid/widget/AbsListView;Landroid/view/View;IJ)V

    goto :goto_0
.end method

.method public onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V
    .locals 0
    .param p2, "data"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 144
    .local p1, "loader":Landroid/content/Loader;, "Landroid/content/Loader<Landroid/database/Cursor;>;"
    invoke-super {p0, p1, p2}, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;->onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V

    .line 145
    return-void
.end method

.method public bridge synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Landroid/content/Loader;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 68
    check-cast p2, Landroid/database/Cursor;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 120
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 127
    :goto_0
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 122
    :pswitch_0
    iget-wide v0, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mPlaylistId:J

    invoke-direct {p0, v0, v1}, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->addSongViaTabSelector(J)V

    goto :goto_0

    .line 120
    :pswitch_data_0
    .packed-switch 0x7f0d01e9
        :pswitch_0
    .end packed-switch
.end method

.method public onPrepareActionMode(Landroid/view/Menu;)V
    .locals 13
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v12, 0x0

    .line 577
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->getAbsListView()Landroid/widget/AbsListView;

    move-result-object v5

    .line 578
    .local v5, "lv":Landroid/widget/AbsListView;
    if-nez v5, :cond_0

    .line 579
    const-string v7, "MusicUiList"

    const-string v8, "Called onPrepareActionMode() but listView is null yet."

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 612
    :goto_0
    return-void

    .line 583
    :cond_0
    invoke-virtual {p0, v5}, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->hasSelectedItem(Landroid/widget/AbsListView;)Z

    move-result v3

    .line 584
    .local v3, "hasSelectedItem":Z
    invoke-virtual {p0, v5}, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->isSelectedOneItem(Landroid/widget/AbsListView;)Z

    move-result v4

    .line 586
    .local v4, "isSelectedOne":Z
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->isDeleteMode()Z

    move-result v7

    if-nez v7, :cond_2

    .line 587
    iget v7, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mList:I

    invoke-static {v7}, Lcom/samsung/musicplus/util/ListUtils;->isTab(I)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 588
    const v7, 0x7f0d01cd

    invoke-interface {p1, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 589
    .local v0, "addToPlaylist":Landroid/view/MenuItem;
    if-eqz v0, :cond_1

    .line 590
    invoke-interface {v0, v12}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 592
    :cond_1
    const v7, 0x7f0d01ce

    invoke-interface {p1, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 593
    .local v2, "editItem":Landroid/view/MenuItem;
    if-eqz v2, :cond_2

    .line 594
    invoke-interface {v2, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 611
    .end local v0    # "addToPlaylist":Landroid/view/MenuItem;
    .end local v2    # "editItem":Landroid/view/MenuItem;
    :cond_2
    :goto_1
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;->onPrepareActionMode(Landroid/view/Menu;)V

    goto :goto_0

    .line 597
    :cond_3
    const v7, 0x7f0d01d0

    invoke-interface {p1, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 598
    .local v1, "delete":Landroid/view/MenuItem;
    const v7, 0x7f0d01cf

    invoke-interface {p1, v7}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v6

    .line 599
    .local v6, "remove":Landroid/view/MenuItem;
    if-eqz v1, :cond_4

    .line 600
    invoke-interface {v1, v12}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 602
    :cond_4
    if-eqz v6, :cond_2

    .line 603
    iget-wide v8, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mPlaylistId:J

    const-wide/16 v10, -0xb

    cmp-long v7, v8, v10

    if-nez v7, :cond_5

    .line 604
    invoke-interface {v6, v12}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_1

    .line 606
    :cond_5
    invoke-interface {v6, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_1
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 1
    .param p1, "sp"    # Landroid/content/SharedPreferences;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 361
    const-string v0, "playlist_menu_list"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 362
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->getPlaylistOrder()[I

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mPlaylistToBeRestored:[I

    .line 367
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->updateListInfo()Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;

    .line 368
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->dispatchReCreateLoader()V

    .line 370
    :cond_0
    return-void
.end method

.method protected prepareOptionMenu(Landroid/view/Menu;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 190
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;->prepareOptionMenu(Landroid/view/Menu;)V

    .line 191
    iget v0, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mList:I

    invoke-static {v0}, Lcom/samsung/musicplus/util/ListUtils;->isTrack(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 192
    iget-wide v0, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mPlaylistId:J

    invoke-direct {p0, p1, v0, v1}, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->prepareTrackOptionMenu(Landroid/view/Menu;J)V

    .line 196
    :goto_0
    return-void

    .line 194
    :cond_0
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->prepareTabOptionMenu(Landroid/view/Menu;)V

    goto :goto_0
.end method

.method public receivePlayerState(Ljava/lang/String;)V
    .locals 3
    .param p1, "action"    # Ljava/lang/String;

    .prologue
    .line 542
    const-string v1, "com.android.music.metachanged"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    iget v1, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mList:I

    invoke-static {v1}, Lcom/samsung/musicplus/util/ListUtils;->isTab(I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "com.samsung.musicplus.action.PREPARE_COMPLETED"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    :cond_0
    iget v1, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mList:I

    invoke-static {v1}, Lcom/samsung/musicplus/util/ListUtils;->isTrack(I)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "com.android.music.playstatechanged"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    :cond_1
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mList:I

    invoke-static {v1, v2}, Lcom/samsung/musicplus/util/ListUtils;->isSupportSplitSub(Landroid/content/Context;I)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, "com.android.music.playstatechanged"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 551
    :cond_2
    iget-object v1, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mAdapter:Landroid/widget/CursorAdapter;

    instance-of v1, v1, Lcom/samsung/musicplus/contents/normal/PlayListFragment$PlayListTabAdapter;

    if-eqz v1, :cond_3

    .line 552
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->getAbsListView()Landroid/widget/AbsListView;

    move-result-object v0

    .line 553
    .local v0, "mListView":Landroid/widget/AbsListView;
    if-eqz v0, :cond_3

    .line 554
    invoke-virtual {v0}, Landroid/widget/AbsListView;->invalidateViews()V

    .line 557
    .end local v0    # "mListView":Landroid/widget/AbsListView;
    :cond_3
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;->receivePlayerState(Ljava/lang/String;)V

    .line 559
    :cond_4
    return-void
.end method

.method protected setHeaderView()V
    .locals 2

    .prologue
    const/high16 v1, 0x40000

    .line 175
    iget v0, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mList:I

    and-int/2addr v0, v1

    iput v0, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mViewMode:I

    .line 176
    iget v0, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mViewMode:I

    if-ne v0, v1, :cond_0

    .line 177
    invoke-super {p0}, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;->setHeaderView()V

    .line 179
    :cond_0
    return-void
.end method

.method protected setNumberOfTextView(Landroid/widget/TextView;Landroid/content/Context;I)V
    .locals 5
    .param p1, "text"    # Landroid/widget/TextView;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "count"    # I

    .prologue
    const/4 v4, 0x0

    .line 526
    const/4 v1, 0x0

    .line 527
    .local v1, "padding":I
    if-lez p3, :cond_1

    .line 528
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;->setNumberOfTextView(Landroid/widget/TextView;Landroid/content/Context;I)V

    .line 529
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c0074

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 535
    :goto_0
    iget v2, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mList:I

    invoke-static {v2}, Lcom/samsung/musicplus/util/ListUtils;->isGridType(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 536
    invoke-virtual {p1, v4, v4, v1, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 538
    :cond_0
    return-void

    .line 531
    :cond_1
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f100100

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 532
    .local v0, "number":Ljava/lang/String;
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 533
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c0075

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    goto :goto_0
.end method

.method protected toggleHeaderAndShuffle(Z)V
    .locals 3
    .param p1, "visible"    # Z

    .prologue
    .line 497
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->getView()Landroid/view/View;

    move-result-object v1

    if-nez v1, :cond_1

    .line 505
    :cond_0
    :goto_0
    return-void

    .line 499
    :cond_1
    iget v1, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mList:I

    invoke-static {v1}, Lcom/samsung/musicplus/util/ListUtils;->isGridType(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 500
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->getView()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f0d008f

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 501
    .local v0, "view":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 502
    if-eqz p1, :cond_2

    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_2
    const/16 v1, 0x8

    goto :goto_1
.end method
