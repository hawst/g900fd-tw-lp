.class public interface abstract Lcom/samsung/musicplus/contents/menu/CommonListMenuHandler;
.super Ljava/lang/Object;
.source "CommonListMenuHandler.java"

# interfaces
.implements Lcom/samsung/musicplus/common/menu/MusicMenuHandler;


# virtual methods
.method public abstract getAudioId(I)J
.end method

.method public abstract getKey()Ljava/lang/String;
.end method

.method public abstract getKeyWord(I)Ljava/lang/String;
.end method

.method public abstract getList()I
.end method

.method public abstract getListAdapter()Landroid/widget/ListAdapter;
.end method

.method public abstract getListItemCount()I
.end method

.method public abstract getQueryArgs()Lcom/samsung/musicplus/util/ListUtils$QueryArgs;
.end method

.method public abstract getSongList(II)[J
.end method

.method public abstract getTitleString(I)Ljava/lang/String;
.end method

.method public abstract getViewType()I
.end method

.method public abstract handlePrepareContextMenu(Landroid/view/Menu;JI)V
.end method

.method public abstract handlePrepareOptionMenu(Landroid/view/Menu;)V
.end method
