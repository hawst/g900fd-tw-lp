.class Lcom/samsung/musicplus/contents/normal/GenreListFragment$GenreTabAdapter;
.super Lcom/samsung/musicplus/widget/list/CommonListAdapter;
.source "GenreListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/contents/normal/GenreListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GenreTabAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/contents/normal/GenreListFragment;


# direct methods
.method public constructor <init>(Lcom/samsung/musicplus/contents/normal/GenreListFragment;Landroid/content/Context;ILandroid/database/Cursor;I)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "layout"    # I
    .param p4, "c"    # Landroid/database/Cursor;
    .param p5, "flags"    # I

    .prologue
    .line 53
    iput-object p1, p0, Lcom/samsung/musicplus/contents/normal/GenreListFragment$GenreTabAdapter;->this$0:Lcom/samsung/musicplus/contents/normal/GenreListFragment;

    .line 54
    invoke-direct {p0, p2, p3, p4, p5}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;I)V

    .line 55
    return-void
.end method


# virtual methods
.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "c"    # Landroid/database/Cursor;

    .prologue
    .line 67
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    .line 68
    invoke-virtual {p0, p1, p2, p3}, Lcom/samsung/musicplus/contents/normal/GenreListFragment$GenreTabAdapter;->hideBottomDivider(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    .line 69
    return-void
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 60
    iget-object v0, p0, Lcom/samsung/musicplus/contents/normal/GenreListFragment$GenreTabAdapter;->this$0:Lcom/samsung/musicplus/contents/normal/GenreListFragment;

    # getter for: Lcom/samsung/musicplus/contents/normal/GenreListFragment;->mList:I
    invoke-static {v0}, Lcom/samsung/musicplus/contents/normal/GenreListFragment;->access$000(Lcom/samsung/musicplus/contents/normal/GenreListFragment;)I

    move-result v0

    invoke-static {v0}, Lcom/samsung/musicplus/util/ListUtils;->isGridType(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 61
    add-int/lit8 v0, p1, -0x1

    invoke-super {p0, v0}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    .line 63
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 72
    iget-object v0, p0, Lcom/samsung/musicplus/contents/normal/GenreListFragment$GenreTabAdapter;->this$0:Lcom/samsung/musicplus/contents/normal/GenreListFragment;

    # getter for: Lcom/samsung/musicplus/contents/normal/GenreListFragment;->mList:I
    invoke-static {v0}, Lcom/samsung/musicplus/contents/normal/GenreListFragment;->access$100(Lcom/samsung/musicplus/contents/normal/GenreListFragment;)I

    move-result v0

    invoke-static {v0}, Lcom/samsung/musicplus/util/ListUtils;->isGridType(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 73
    add-int/lit8 v0, p1, -0x1

    invoke-super {p0, v0}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->getItemId(I)J

    move-result-wide v0

    .line 75
    :goto_0
    return-wide v0

    :cond_0
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->getItemId(I)J

    move-result-wide v0

    goto :goto_0
.end method
