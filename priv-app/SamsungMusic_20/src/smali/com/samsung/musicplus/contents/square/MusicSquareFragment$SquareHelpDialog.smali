.class public Lcom/samsung/musicplus/contents/square/MusicSquareFragment$SquareHelpDialog;
.super Landroid/app/DialogFragment;
.source "MusicSquareFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/contents/square/MusicSquareFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SquareHelpDialog"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1777
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x0

    .line 1781
    invoke-virtual {p0, v2, v2}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$SquareHelpDialog;->setStyle(II)V

    .line 1783
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$SquareHelpDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 1784
    .local v0, "a":Landroid/app/Activity;
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v3, 0x7f1000d7

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f100114

    new-instance v4, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$SquareHelpDialog$1;

    invoke-direct {v4, p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$SquareHelpDialog$1;-><init>(Lcom/samsung/musicplus/contents/square/MusicSquareFragment$SquareHelpDialog;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    .line 1792
    .local v1, "builder":Landroid/app/AlertDialog$Builder;
    const v2, 0x7f1000d8

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 1793
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    return-object v2
.end method
