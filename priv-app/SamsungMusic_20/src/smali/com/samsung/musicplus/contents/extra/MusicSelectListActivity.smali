.class public Lcom/samsung/musicplus/contents/extra/MusicSelectListActivity;
.super Lcom/samsung/musicplus/MusicBaseActivity;
.source "MusicSelectListActivity.java"


# static fields
.field private static final CLASSNAME:Ljava/lang/String;


# instance fields
.field private mAdditionalKeyword:Ljava/lang/String;

.field private mHeaderMode:I

.field private mKey:Ljava/lang/String;

.field private mList:I

.field private mListMode:I

.field private mSelectedListId:[J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const-class v0, Lcom/samsung/musicplus/contents/extra/MusicSelectListActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/contents/extra/MusicSelectListActivity;->CLASSNAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/samsung/musicplus/MusicBaseActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 44
    invoke-super {p0, p1}, Lcom/samsung/musicplus/MusicBaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 45
    const v0, 0x7f04006c

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/contents/extra/MusicSelectListActivity;->setContentView(I)V

    .line 47
    if-eqz p1, :cond_1

    .line 48
    const-string v0, "list_type"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListActivity;->mList:I

    .line 49
    const-string v0, "list_mode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListActivity;->mListMode:I

    .line 50
    const-string v0, "header_mode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListActivity;->mHeaderMode:I

    .line 51
    const-string v0, "keyword"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListActivity;->mKey:Ljava/lang/String;

    .line 52
    const-string v0, "selected_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListActivity;->mSelectedListId:[J

    .line 53
    const-string v0, "addtional_keyword"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListActivity;->mAdditionalKeyword:Ljava/lang/String;

    .line 66
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/extra/MusicSelectListActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    iget v1, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListActivity;->mList:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v7

    .line 67
    .local v7, "fg":Landroid/app/Fragment;
    if-nez v7, :cond_0

    .line 70
    iget v0, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListActivity;->mList:I

    iget v1, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListActivity;->mListMode:I

    iget v2, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListActivity;->mHeaderMode:I

    iget-object v3, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListActivity;->mKey:Ljava/lang/String;

    iget-object v4, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListActivity;->mSelectedListId:[J

    iget-object v5, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListActivity;->mAdditionalKeyword:Ljava/lang/String;

    invoke-static/range {v0 .. v5}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->getNewInstance(IIILjava/lang/String;[JLjava/lang/String;)Landroid/app/Fragment;

    move-result-object v7

    .line 72
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/extra/MusicSelectListActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f0d0135

    iget v2, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListActivity;->mList:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v7, v2}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 75
    :cond_0
    return-void

    .line 56
    .end local v7    # "fg":Landroid/app/Fragment;
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/extra/MusicSelectListActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v6

    .line 58
    .local v6, "data":Landroid/os/Bundle;
    const-string v0, "list_type"

    const v1, 0x20001

    invoke-virtual {v6, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListActivity;->mList:I

    .line 59
    const-string v0, "list_mode"

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListActivity;->mListMode:I

    .line 60
    const-string v0, "header_mode"

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListActivity;->mHeaderMode:I

    .line 61
    const-string v0, "keyword"

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListActivity;->mKey:Ljava/lang/String;

    .line 62
    const-string v0, "selected_id"

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListActivity;->mSelectedListId:[J

    .line 63
    const-string v0, "addtional_keyword"

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListActivity;->mAdditionalKeyword:Ljava/lang/String;

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 79
    const-string v0, "list_type"

    iget v1, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListActivity;->mList:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 80
    const-string v0, "list_mode"

    iget v1, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListActivity;->mListMode:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 81
    const-string v0, "header_mode"

    iget v1, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListActivity;->mHeaderMode:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 82
    const-string v0, "keyword"

    iget-object v1, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListActivity;->mKey:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    const-string v0, "selected_id"

    iget-object v1, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListActivity;->mSelectedListId:[J

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putLongArray(Ljava/lang/String;[J)V

    .line 84
    const-string v0, "addtional_keyword"

    iget-object v1, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListActivity;->mAdditionalKeyword:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    invoke-super {p0, p1}, Lcom/samsung/musicplus/MusicBaseActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 86
    return-void
.end method
