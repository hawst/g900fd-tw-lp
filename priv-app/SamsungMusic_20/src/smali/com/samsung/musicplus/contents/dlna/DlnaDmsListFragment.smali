.class public Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;
.super Lcom/samsung/musicplus/widget/fragment/CommonListFragment;
.source "DlnaDmsListFragment.java"

# interfaces
.implements Lcom/samsung/musicplus/contents/search/QueryBrowserActivity$QueryTextChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment$DmsTrackAdapter;,
        Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment$DmsServerAdapter;
    }
.end annotation


# static fields
.field private static final SAVED_CONTENT_SEARCH_ERROR:Ljava/lang/String; = "content_search_error"


# instance fields
.field private mDlnaErrorReceiver:Landroid/content/BroadcastReceiver;

.field private mDlnaEventReceiver:Landroid/content/BroadcastReceiver;

.field private mIsContentsSearchError:Z

.field private mIsFlatSearchFinished:Z

.field private mQueryText:Ljava/lang/String;

.field private mSearchView:Landroid/widget/SearchView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 57
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;-><init>()V

    .line 64
    iput-boolean v0, p0, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->mIsFlatSearchFinished:Z

    .line 70
    iput-boolean v0, p0, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->mIsContentsSearchError:Z

    .line 74
    new-instance v0, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment$1;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment$1;-><init>(Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;)V

    iput-object v0, p0, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->mDlnaEventReceiver:Landroid/content/BroadcastReceiver;

    .line 250
    new-instance v0, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment$2;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment$2;-><init>(Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;)V

    iput-object v0, p0, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->mDlnaErrorReceiver:Landroid/content/BroadcastReceiver;

    .line 330
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;

    .prologue
    .line 57
    iget-boolean v0, p0, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->mIsContentsSearchError:Z

    return v0
.end method

.method static synthetic access$002(Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 57
    iput-boolean p1, p0, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->mIsContentsSearchError:Z

    return p1
.end method

.method static synthetic access$102(Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 57
    iput-boolean p1, p0, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->mIsFlatSearchFinished:Z

    return p1
.end method

.method static synthetic access$200(Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->mKey:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->mKey:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->mKey:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;)Landroid/widget/SearchView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->mSearchView:Landroid/widget/SearchView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;

    .prologue
    .line 57
    iget-object v0, p0, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->mQueryText:Ljava/lang/String;

    return-object v0
.end method

.method private disableAddToFavorite(Landroid/view/Menu;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 404
    const v1, 0x7f0d01d2

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 405
    .local v0, "addToFavorite":Landroid/view/MenuItem;
    if-eqz v0, :cond_0

    .line 406
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 408
    :cond_0
    return-void
.end method

.method private disableAddToPlaylistOption(Landroid/view/Menu;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 390
    const v1, 0x7f0d01cd

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 391
    .local v0, "addToPlaylist":Landroid/view/MenuItem;
    if-eqz v0, :cond_0

    .line 392
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 394
    :cond_0
    return-void
.end method

.method private disableAddToSecretBoxOption(Landroid/view/Menu;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 383
    const v1, 0x7f0d01d6

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 384
    .local v0, "addToSecretBox":Landroid/view/MenuItem;
    if-eqz v0, :cond_0

    .line 385
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 387
    :cond_0
    return-void
.end method

.method private disableDeleteOption(Landroid/view/Menu;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 397
    const v1, 0x7f0d01d0

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 398
    .local v0, "delete":Landroid/view/MenuItem;
    if-eqz v0, :cond_0

    .line 399
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 401
    :cond_0
    return-void
.end method

.method private disableRemoveFromSecretBoxOption(Landroid/view/Menu;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 376
    const v1, 0x7f0d01d7

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 377
    .local v0, "removeFromSecretBox":Landroid/view/MenuItem;
    if-eqz v0, :cond_0

    .line 378
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 380
    :cond_0
    return-void
.end method

.method private disableRemoveOption(Landroid/view/Menu;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 418
    const v1, 0x7f0d01cf

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 419
    .local v0, "remove":Landroid/view/MenuItem;
    if-eqz v0, :cond_0

    .line 420
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 422
    :cond_0
    return-void
.end method

.method private disableSetAsOption(Landroid/view/Menu;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 411
    const v1, 0x7f0d01a3

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 412
    .local v0, "setAs":Landroid/view/MenuItem;
    if-eqz v0, :cond_0

    .line 413
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 415
    :cond_0
    return-void
.end method

.method private getSelection(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "selection"    # Ljava/lang/String;
    .param p2, "text"    # Ljava/lang/String;

    .prologue
    .line 438
    if-eqz p2, :cond_0

    .line 439
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 440
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string v1, " AND ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "title"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " LIKE \'%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%\' OR "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "artist"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " LIKE \'%"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "%\')"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    .line 445
    .end local v0    # "sb":Ljava/lang/StringBuilder;
    :cond_0
    return-object p1
.end method

.method private registerDlnaEventReceiver()V
    .locals 3

    .prologue
    .line 234
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 235
    .local v0, "f":Landroid/content/IntentFilter;
    const-string v1, "com.samsung.musicplus.dlna.flat.searching.info"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 236
    const-string v1, "com.samsung.musicplus.dlna.flat.searching.error"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 237
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->mDlnaEventReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 238
    return-void
.end method

.method private registerDlnaWatcher()V
    .locals 3

    .prologue
    .line 241
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 242
    .local v0, "f":Landroid/content/IntentFilter;
    const-string v1, "com.samsung.musicplus.dlna.connectivitychanged"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 243
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->mDlnaErrorReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 244
    return-void
.end method

.method private unregisterDlanWatcher()V
    .locals 2

    .prologue
    .line 247
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->mDlnaErrorReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 248
    return-void
.end method


# virtual methods
.method protected createAdapter()Landroid/widget/CursorAdapter;
    .locals 6

    .prologue
    const/4 v4, 0x0

    const v3, 0x7f040033

    const/4 v5, 0x0

    .line 168
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    .line 169
    .local v2, "a":Landroid/app/Activity;
    iget v0, p0, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->mList:I

    invoke-static {v0}, Lcom/samsung/musicplus/util/ListUtils;->isTrack(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 170
    new-instance v0, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment$DmsTrackAdapter;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment$DmsTrackAdapter;-><init>(Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;Landroid/content/Context;ILandroid/database/Cursor;I)V

    .line 172
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment$DmsServerAdapter;

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment$DmsServerAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;I)V

    goto :goto_0
.end method

.method protected initLoader()V
    .locals 3

    .prologue
    .line 177
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    iget v1, p0, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->mList:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    .line 178
    return-void
.end method

.method protected isEnableListShuffle(I)Z
    .locals 1
    .param p1, "list"    # I

    .prologue
    .line 281
    iget-object v0, p0, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->mSearchView:Landroid/widget/SearchView;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 134
    iget v1, p0, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->mList:I

    invoke-static {v1}, Lcom/samsung/musicplus/util/ListUtils;->isTrack(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 135
    if-nez p1, :cond_3

    .line 136
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getListType()I

    move-result v1

    const v2, 0x2000b

    if-ne v1, v2, :cond_2

    .line 137
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getKey()Ljava/lang/String;

    move-result-object v0

    .line 138
    .local v0, "playingDms":Ljava/lang/String;
    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->mKey:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 140
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->mIsFlatSearchFinished:Z

    .line 153
    .end local v0    # "playingDms":Ljava/lang/String;
    :goto_0
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->registerDlnaWatcher()V

    .line 155
    :cond_0
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 156
    return-void

    .line 144
    .restart local v0    # "playingDms":Ljava/lang/String;
    :cond_1
    iget-object v1, p0, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->mKey:Ljava/lang/String;

    invoke-static {v1}, Lcom/samsung/musicplus/util/ServiceUtils;->selectDlnaDms(Ljava/lang/String;)V

    goto :goto_0

    .line 148
    .end local v0    # "playingDms":Ljava/lang/String;
    :cond_2
    iget-object v1, p0, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->mKey:Ljava/lang/String;

    invoke-static {v1}, Lcom/samsung/musicplus/util/ServiceUtils;->selectDlnaDms(Ljava/lang/String;)V

    goto :goto_0

    .line 151
    :cond_3
    const-string v1, "content_search_error"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->mIsContentsSearchError:Z

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 160
    iget v0, p0, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->mList:I

    invoke-static {v0}, Lcom/samsung/musicplus/util/ListUtils;->isTrack(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 161
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->unregisterDlanWatcher()V

    .line 163
    :cond_0
    invoke-super {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->onDestroy()V

    .line 164
    return-void
.end method

.method public onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V
    .locals 2
    .param p2, "data"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 198
    .local p1, "loader":Landroid/content/Loader;, "Landroid/content/Loader<Landroid/database/Cursor;>;"
    iget-object v0, p0, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->mSearchView:Landroid/widget/SearchView;

    if-eqz v0, :cond_0

    .line 199
    const v0, 0x7f100102

    iput v0, p0, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->mNoItemTextId:I

    .line 201
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V

    .line 202
    iget v0, p0, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->mList:I

    invoke-static {v0}, Lcom/samsung/musicplus/util/ListUtils;->isTrack(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 203
    if-eqz p2, :cond_1

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_2

    .line 204
    :cond_1
    iget-boolean v0, p0, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->mIsContentsSearchError:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->mIsFlatSearchFinished:Z

    if-nez v0, :cond_2

    .line 207
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->setListShownNoAnimation(Z)V

    .line 215
    :cond_2
    :goto_0
    return-void

    .line 211
    :cond_3
    iget-object v0, p0, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->mAdapter:Landroid/widget/CursorAdapter;

    instance-of v0, v0, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment$DmsServerAdapter;

    if-eqz v0, :cond_2

    .line 212
    iget-object v0, p0, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->mAdapter:Landroid/widget/CursorAdapter;

    check-cast v0, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment$DmsServerAdapter;

    iget-object v1, p0, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->mQueryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    iget-object v1, v1, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->albumArtCol:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment$DmsServerAdapter;->setAlbumColumn(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public bridge synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Landroid/content/Loader;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 57
    check-cast p2, Landroid/database/Cursor;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onQueryTextChanged(Ljava/lang/String;)V
    .locals 3
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 426
    iput-object p1, p0, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->mQueryText:Ljava/lang/String;

    .line 427
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->updateListInfo()Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;

    .line 428
    iget-object v0, p0, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->mQueryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    iget-object v1, p0, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->mQueryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    iget-object v1, v1, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->selection:Ljava/lang/String;

    iget-object v2, p0, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->mQueryText:Ljava/lang/String;

    invoke-direct {p0, v1, v2}, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->getSelection(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->selection:Ljava/lang/String;

    .line 429
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    iget v1, p0, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->mList:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    .line 430
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 182
    iget v0, p0, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->mList:I

    invoke-static {v0}, Lcom/samsung/musicplus/util/ListUtils;->isTrack(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 183
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->registerDlnaEventReceiver()V

    .line 185
    :cond_0
    invoke-super {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->onStart()V

    .line 186
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 190
    iget v0, p0, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->mList:I

    invoke-static {v0}, Lcom/samsung/musicplus/util/ListUtils;->isTrack(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 191
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->mDlnaEventReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 193
    :cond_0
    invoke-super {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->onStop()V

    .line 194
    return-void
.end method

.method protected prepareContextMenu(Landroid/view/Menu;JI)V
    .locals 0
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "itemId"    # J
    .param p4, "position"    # I

    .prologue
    .line 226
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->disableAddToPlaylistOption(Landroid/view/Menu;)V

    .line 227
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->disableSetAsOption(Landroid/view/Menu;)V

    .line 228
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->disableDeleteOption(Landroid/view/Menu;)V

    .line 229
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->disableAddToFavorite(Landroid/view/Menu;)V

    .line 230
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->disableRemoveOption(Landroid/view/Menu;)V

    .line 231
    return-void
.end method

.method protected prepareOptionMenu(Landroid/view/Menu;)V
    .locals 0
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 219
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->disableDeleteOption(Landroid/view/Menu;)V

    .line 220
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->disableAddToSecretBoxOption(Landroid/view/Menu;)V

    .line 221
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->disableRemoveFromSecretBoxOption(Landroid/view/Menu;)V

    .line 222
    return-void
.end method

.method public setSearchView(Landroid/widget/SearchView;)V
    .locals 0
    .param p1, "view"    # Landroid/widget/SearchView;

    .prologue
    .line 434
    iput-object p1, p0, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->mSearchView:Landroid/widget/SearchView;

    .line 435
    return-void
.end method
