.class Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$9$1;
.super Ljava/lang/Object;
.source "DeviceListFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$9;->onClick(Landroid/content/DialogInterface;I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$2:Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$9;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$9;)V
    .locals 0

    .prologue
    .line 1091
    iput-object p1, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$9$1;->this$2:Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$9;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 1093
    iget-object v1, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$9$1;->this$2:Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$9;

    iget-object v1, v1, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$9;->this$1:Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;

    # getter for: Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->access$1900(Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/android/sdk/samsunglink/SlinkFrameworkUserPortal;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkFrameworkUserPortal;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$9$1;->this$2:Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$9;

    iget-wide v2, v2, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$9;->val$deviceId:J

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/sdk/samsunglink/SlinkFrameworkUserPortal;->deregisterDevice(J)Z

    .line 1095
    const-wide/16 v2, 0x5dc

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1099
    :goto_0
    iget-object v1, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$9$1;->this$2:Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$9;

    iget-object v1, v1, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$9;->this$1:Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;

    iget-object v1, v1, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->this$0:Lcom/samsung/musicplus/contents/devices/DeviceListFragment;

    # getter for: Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mIsStop:Z
    invoke-static {v1}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->access$2000(Lcom/samsung/musicplus/contents/devices/DeviceListFragment;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$9$1;->this$2:Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$9;

    iget-object v1, v1, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$9;->this$1:Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;

    # getter for: Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->access$1800(Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1100
    iget-object v1, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$9$1;->this$2:Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$9;

    iget-object v1, v1, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$9;->this$1:Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;

    # getter for: Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->access$1800(Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 1102
    :cond_0
    return-void

    .line 1096
    :catch_0
    move-exception v0

    .line 1097
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method
