.class Lcom/samsung/musicplus/contents/normal/PlayListFragment$PlayListTabAdapter;
.super Lcom/samsung/musicplus/widget/list/CommonListAdapter;
.source "PlayListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/contents/normal/PlayListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PlayListTabAdapter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/contents/normal/PlayListFragment$PlayListTabAdapter$PlaylistTabHolder;
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/contents/normal/PlayListFragment;


# direct methods
.method public constructor <init>(Lcom/samsung/musicplus/contents/normal/PlayListFragment;Landroid/content/Context;ILandroid/database/Cursor;I)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "layout"    # I
    .param p4, "c"    # Landroid/database/Cursor;
    .param p5, "flags"    # I

    .prologue
    .line 653
    iput-object p1, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment$PlayListTabAdapter;->this$0:Lcom/samsung/musicplus/contents/normal/PlayListFragment;

    .line 654
    invoke-direct {p0, p2, p3, p4, p5}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;I)V

    .line 655
    return-void
.end method

.method private bindPlaylistItemView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 9
    .param p1, "v"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "c"    # Landroid/database/Cursor;

    .prologue
    .line 796
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/contents/normal/PlayListFragment$PlayListTabAdapter$PlaylistTabHolder;

    .line 797
    .local v0, "vh":Lcom/samsung/musicplus/contents/normal/PlayListFragment$PlayListTabAdapter$PlaylistTabHolder;
    iget-object v1, v0, Lcom/samsung/musicplus/contents/normal/PlayListFragment$PlayListTabAdapter$PlaylistTabHolder;->text1:Landroid/widget/TextView;

    const/4 v2, 0x1

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 798
    const-wide/16 v2, -0x10

    const/4 v1, 0x0

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-nez v1, :cond_2

    .line 799
    iget-object v1, v0, Lcom/samsung/musicplus/contents/normal/PlayListFragment$PlayListTabAdapter$PlaylistTabHolder;->text2:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 800
    iget-object v1, v0, Lcom/samsung/musicplus/contents/normal/PlayListFragment$PlayListTabAdapter$PlaylistTabHolder;->text3:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 801
    iget-object v1, v0, Lcom/samsung/musicplus/contents/normal/PlayListFragment$PlayListTabAdapter$PlaylistTabHolder;->text3:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 803
    :cond_0
    iget-object v1, v0, Lcom/samsung/musicplus/contents/normal/PlayListFragment$PlayListTabAdapter$PlaylistTabHolder;->duration:Landroid/widget/TextView;

    if-eqz v1, :cond_1

    .line 804
    iget-object v1, v0, Lcom/samsung/musicplus/contents/normal/PlayListFragment$PlayListTabAdapter$PlaylistTabHolder;->duration:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 816
    :cond_1
    :goto_0
    invoke-virtual {p0, p3}, Lcom/samsung/musicplus/contents/normal/PlayListFragment$PlayListTabAdapter;->getArtKey(Landroid/database/Cursor;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 817
    .local v6, "plid":J
    iget-object v1, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment$PlayListTabAdapter;->this$0:Lcom/samsung/musicplus/contents/normal/PlayListFragment;

    invoke-virtual {v1}, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->getViewType()I

    move-result v8

    .line 818
    .local v8, "viewType":I
    iget-object v1, v0, Lcom/samsung/musicplus/contents/normal/PlayListFragment$PlayListTabAdapter$PlaylistTabHolder;->text3:Landroid/widget/TextView;

    if-eqz v1, :cond_5

    .line 819
    invoke-static {}, Lcom/samsung/musicplus/widget/content/PlaylistItemProgress;->getPlaylistItemProgress()Lcom/samsung/musicplus/widget/content/PlaylistItemProgress;

    move-result-object v1

    iget-boolean v2, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment$PlayListTabAdapter;->mAlbumArtParsingEnabled:Z

    if-eqz v2, :cond_4

    iget-object v3, v0, Lcom/samsung/musicplus/contents/normal/PlayListFragment$PlayListTabAdapter$PlaylistTabHolder;->thumbnail:Landroid/widget/ImageView;

    :goto_1
    iget-object v4, v0, Lcom/samsung/musicplus/contents/normal/PlayListFragment$PlayListTabAdapter$PlaylistTabHolder;->text3:Landroid/widget/TextView;

    iget-object v5, v0, Lcom/samsung/musicplus/contents/normal/PlayListFragment$PlayListTabAdapter$PlaylistTabHolder;->duration:Landroid/widget/TextView;

    move-object v2, p2

    invoke-virtual/range {v1 .. v8}, Lcom/samsung/musicplus/widget/content/PlaylistItemProgress;->updateListView(Landroid/content/Context;Landroid/widget/ImageView;Landroid/widget/TextView;Landroid/widget/TextView;JI)V

    .line 826
    :goto_2
    return-void

    .line 807
    .end local v6    # "plid":J
    .end local v8    # "viewType":I
    :cond_2
    iget-object v1, v0, Lcom/samsung/musicplus/contents/normal/PlayListFragment$PlayListTabAdapter$PlaylistTabHolder;->text2:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 808
    iget-object v1, v0, Lcom/samsung/musicplus/contents/normal/PlayListFragment$PlayListTabAdapter$PlaylistTabHolder;->text3:Landroid/widget/TextView;

    if-eqz v1, :cond_3

    .line 809
    iget-object v1, v0, Lcom/samsung/musicplus/contents/normal/PlayListFragment$PlayListTabAdapter$PlaylistTabHolder;->text3:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 811
    :cond_3
    iget-object v1, v0, Lcom/samsung/musicplus/contents/normal/PlayListFragment$PlayListTabAdapter$PlaylistTabHolder;->duration:Landroid/widget/TextView;

    if-eqz v1, :cond_1

    .line 812
    iget-object v1, v0, Lcom/samsung/musicplus/contents/normal/PlayListFragment$PlayListTabAdapter$PlaylistTabHolder;->duration:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 819
    .restart local v6    # "plid":J
    .restart local v8    # "viewType":I
    :cond_4
    const/4 v3, 0x0

    goto :goto_1

    .line 823
    :cond_5
    invoke-static {}, Lcom/samsung/musicplus/widget/content/PlaylistItemProgress;->getPlaylistItemProgress()Lcom/samsung/musicplus/widget/content/PlaylistItemProgress;

    move-result-object v2

    iget-boolean v1, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment$PlayListTabAdapter;->mAlbumArtParsingEnabled:Z

    if-eqz v1, :cond_6

    iget-object v4, v0, Lcom/samsung/musicplus/contents/normal/PlayListFragment$PlayListTabAdapter$PlaylistTabHolder;->thumbnail:Landroid/widget/ImageView;

    :goto_3
    iget-object v5, v0, Lcom/samsung/musicplus/contents/normal/PlayListFragment$PlayListTabAdapter$PlaylistTabHolder;->text2:Landroid/widget/TextView;

    move-object v3, p2

    invoke-virtual/range {v2 .. v8}, Lcom/samsung/musicplus/widget/content/PlaylistItemProgress;->updateListView(Landroid/content/Context;Landroid/widget/ImageView;Landroid/widget/TextView;JI)V

    goto :goto_2

    :cond_6
    const/4 v4, 0x0

    goto :goto_3
.end method

.method private isAddToPlayListPosition(I)Z
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 681
    iget-object v0, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment$PlayListTabAdapter;->this$0:Lcom/samsung/musicplus/contents/normal/PlayListFragment;

    # getter for: Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mCountOfData:I
    invoke-static {v0}, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->access$300(Lcom/samsung/musicplus/contents/normal/PlayListFragment;)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isTitle(I)Z
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 677
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment$PlayListTabAdapter;->this$0:Lcom/samsung/musicplus/contents/normal/PlayListFragment;

    # getter for: Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mCountOfDefaultPlaylists:I
    invoke-static {v0}, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->access$200(Lcom/samsung/musicplus/contents/normal/PlayListFragment;)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setItemAlpha(JLandroid/view/View;)V
    .locals 5
    .param p1, "id"    # J
    .param p3, "v"    # Landroid/view/View;

    .prologue
    .line 836
    const v1, 0x7f0d00bb

    invoke-virtual {p3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 837
    .local v0, "divider":Landroid/view/View;
    iget-boolean v1, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment$PlayListTabAdapter;->mIsActionMode:Z

    if-eqz v1, :cond_1

    const-wide/16 v2, 0x0

    cmp-long v1, p1, v2

    if-gez v1, :cond_1

    .line 838
    const v1, 0x3e99999a    # 0.3f

    invoke-virtual {p3, v1}, Landroid/view/View;->setAlpha(F)V

    .line 839
    if-eqz v0, :cond_0

    .line 840
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 848
    :cond_0
    :goto_0
    return-void

    .line 843
    :cond_1
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {p3, v1}, Landroid/view/View;->setAlpha(F)V

    .line 844
    if-eqz v0, :cond_0

    .line 845
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 11
    .param p1, "v"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "c"    # Landroid/database/Cursor;

    .prologue
    .line 741
    const/4 v8, 0x0

    invoke-interface {p3, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 742
    .local v2, "id":J
    const-wide/16 v8, -0xf

    cmp-long v8, v8, v2

    if-nez v8, :cond_5

    .line 743
    invoke-interface {p3}, Landroid/database/Cursor;->isFirst()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 744
    const v8, 0x7f0d007f

    invoke-virtual {p1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 745
    .local v1, "divider":Landroid/view/View;
    if-eqz v1, :cond_0

    .line 746
    const/16 v8, 0x8

    invoke-virtual {v1, v8}, Landroid/view/View;->setVisibility(I)V

    .line 749
    .end local v1    # "divider":Landroid/view/View;
    :cond_0
    const v8, 0x7f0d0081

    invoke-virtual {p1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 751
    .local v6, "text":Landroid/widget/TextView;
    const/4 v8, 0x1

    invoke-interface {p3, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 752
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v9, 0x1

    invoke-interface {p3, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, ", "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v9, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment$PlayListTabAdapter;->this$0:Lcom/samsung/musicplus/contents/normal/PlayListFragment;

    invoke-virtual {v9}, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v9

    invoke-virtual {v9}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f10018b

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 753
    const v8, 0x7f0d0080

    invoke-virtual {p1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 754
    .local v7, "text2":Landroid/widget/TextView;
    iget-object v8, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment$PlayListTabAdapter;->this$0:Lcom/samsung/musicplus/contents/normal/PlayListFragment;

    # getter for: Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mList:I
    invoke-static {v8}, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->access$500(Lcom/samsung/musicplus/contents/normal/PlayListFragment;)I

    move-result v8

    invoke-static {v8}, Lcom/samsung/musicplus/util/ListUtils;->isGridType(I)Z

    move-result v8

    if-nez v8, :cond_1

    invoke-interface {p3}, Landroid/database/Cursor;->getPosition()I

    move-result v8

    if-nez v8, :cond_3

    .line 755
    :cond_1
    const/16 v8, 0x8

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 793
    .end local v6    # "text":Landroid/widget/TextView;
    .end local v7    # "text2":Landroid/widget/TextView;
    :cond_2
    :goto_0
    return-void

    .line 757
    .restart local v6    # "text":Landroid/widget/TextView;
    .restart local v7    # "text2":Landroid/widget/TextView;
    :cond_3
    const/4 v8, 0x0

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 758
    const v8, 0x7f0d007f

    invoke-virtual {p1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 759
    .restart local v1    # "divider":Landroid/view/View;
    if-eqz v1, :cond_4

    .line 760
    const/4 v8, 0x0

    invoke-virtual {v1, v8}, Landroid/view/View;->setVisibility(I)V

    .line 762
    :cond_4
    iget-object v8, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment$PlayListTabAdapter;->this$0:Lcom/samsung/musicplus/contents/normal/PlayListFragment;

    invoke-virtual {v8}, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->getNumberOfItmes()I

    move-result v0

    .line 763
    .local v0, "count":I
    iget-object v8, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment$PlayListTabAdapter;->this$0:Lcom/samsung/musicplus/contents/normal/PlayListFragment;

    invoke-virtual {v8, v7, p2, v0}, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->setNumberOfTextView(Landroid/widget/TextView;Landroid/content/Context;I)V

    goto :goto_0

    .line 767
    .end local v0    # "count":I
    .end local v1    # "divider":Landroid/view/View;
    .end local v6    # "text":Landroid/widget/TextView;
    .end local v7    # "text2":Landroid/widget/TextView;
    :cond_5
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/musicplus/contents/normal/PlayListFragment$PlayListTabAdapter;->bindPlaylistItemView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    .line 768
    invoke-virtual {p0, p1, p2, p3}, Lcom/samsung/musicplus/contents/normal/PlayListFragment$PlayListTabAdapter;->bindOtherView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    .line 769
    invoke-direct {p0, v2, v3, p1}, Lcom/samsung/musicplus/contents/normal/PlayListFragment$PlayListTabAdapter;->setItemAlpha(JLandroid/view/View;)V

    .line 771
    sget-boolean v8, Lcom/samsung/musicplus/util/Features;->FLAG_SUPPORT_SPLIT_LIST_VIEW:Z

    if-eqz v8, :cond_6

    .line 772
    invoke-virtual {p0, p1, p3}, Lcom/samsung/musicplus/contents/normal/PlayListFragment$PlayListTabAdapter;->setSelectedPositionBackground(Landroid/view/View;Landroid/database/Cursor;)V

    .line 774
    :cond_6
    const v8, 0x7f0d00bb

    invoke-virtual {p1, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 775
    .restart local v1    # "divider":Landroid/view/View;
    if-eqz v1, :cond_7

    .line 776
    iget-boolean v8, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment$PlayListTabAdapter;->mIsActionMode:Z

    if-eqz v8, :cond_8

    .line 777
    const v8, 0x7f020022

    invoke-virtual {v1, v8}, Landroid/view/View;->setBackgroundResource(I)V

    .line 782
    :cond_7
    :goto_1
    invoke-interface {p3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v8

    if-eqz v8, :cond_2

    .line 783
    const/4 v8, 0x0

    invoke-interface {p3, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 784
    .local v4, "idNext":J
    invoke-interface {p3}, Landroid/database/Cursor;->moveToPrevious()Z

    .line 785
    const-wide/16 v8, -0xf

    cmp-long v8, v8, v4

    if-nez v8, :cond_2

    if-eqz v1, :cond_2

    .line 786
    const/16 v8, 0x8

    invoke-virtual {v1, v8}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 779
    .end local v4    # "idNext":J
    :cond_8
    const v8, 0x7f020020

    invoke-virtual {v1, v8}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_1
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const-wide/16 v4, -0xf

    .line 700
    if-eqz p2, :cond_0

    .line 701
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/contents/normal/PlayListFragment$PlayListTabAdapter$PlaylistTabHolder;

    .line 702
    .local v0, "vh":Lcom/samsung/musicplus/contents/normal/PlayListFragment$PlayListTabAdapter$PlaylistTabHolder;
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/contents/normal/PlayListFragment$PlayListTabAdapter;->isTitle(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 703
    iget-wide v2, v0, Lcom/samsung/musicplus/contents/normal/PlayListFragment$PlayListTabAdapter$PlaylistTabHolder;->type:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 704
    const/4 p2, 0x0

    .line 712
    .end local v0    # "vh":Lcom/samsung/musicplus/contents/normal/PlayListFragment$PlayListTabAdapter$PlaylistTabHolder;
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    return-object v1

    .line 707
    .restart local v0    # "vh":Lcom/samsung/musicplus/contents/normal/PlayListFragment$PlayListTabAdapter$PlaylistTabHolder;
    :cond_1
    iget-wide v2, v0, Lcom/samsung/musicplus/contents/normal/PlayListFragment$PlayListTabAdapter$PlaylistTabHolder;->type:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 708
    const/4 p2, 0x0

    goto :goto_0
.end method

.method protected getViewHolder()Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;
    .locals 1

    .prologue
    .line 830
    new-instance v0, Lcom/samsung/musicplus/contents/normal/PlayListFragment$PlayListTabAdapter$PlaylistTabHolder;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/contents/normal/PlayListFragment$PlayListTabAdapter$PlaylistTabHolder;-><init>(Lcom/samsung/musicplus/contents/normal/PlayListFragment$PlayListTabAdapter;)V

    return-object v0
.end method

.method public isEnabled(I)Z
    .locals 3
    .param p1, "position"    # I

    .prologue
    const/4 v0, 0x0

    .line 686
    iget-object v1, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment$PlayListTabAdapter;->this$0:Lcom/samsung/musicplus/contents/normal/PlayListFragment;

    # getter for: Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mViewMode:I
    invoke-static {v1}, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->access$400(Lcom/samsung/musicplus/contents/normal/PlayListFragment;)I

    move-result v1

    const/high16 v2, 0x40000

    if-eq v1, v2, :cond_1

    .line 687
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/contents/normal/PlayListFragment$PlayListTabAdapter;->isTitle(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 695
    :cond_0
    :goto_0
    return v0

    .line 691
    :cond_1
    iget-boolean v1, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment$PlayListTabAdapter;->mIsActionMode:Z

    if-eqz v1, :cond_2

    .line 692
    iget-object v1, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment$PlayListTabAdapter;->this$0:Lcom/samsung/musicplus/contents/normal/PlayListFragment;

    # getter for: Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mCountOfDefaultPlaylists:I
    invoke-static {v1}, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->access$200(Lcom/samsung/musicplus/contents/normal/PlayListFragment;)I

    move-result v1

    if-lt p1, v1, :cond_0

    invoke-direct {p0, p1}, Lcom/samsung/musicplus/contents/normal/PlayListFragment$PlayListTabAdapter;->isAddToPlayListPosition(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 695
    :cond_2
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->isEnabled(I)Z

    move-result v0

    goto :goto_0
.end method

.method protected newTextView(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 734
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/contents/normal/PlayListFragment$PlayListTabAdapter$PlaylistTabHolder;

    .line 735
    .local v0, "vh":Lcom/samsung/musicplus/contents/normal/PlayListFragment$PlayListTabAdapter$PlaylistTabHolder;
    const v1, 0x7f0d00cb

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v0, Lcom/samsung/musicplus/contents/normal/PlayListFragment$PlayListTabAdapter$PlaylistTabHolder;->duration:Landroid/widget/TextView;

    .line 736
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->newTextView(Landroid/view/View;)V

    .line 737
    return-void
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "c"    # Landroid/database/Cursor;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const-wide/16 v10, -0xf

    const/4 v8, 0x0

    .line 717
    const-string v5, "MusicUiList"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment$PlayListTabAdapter;->this$0:Lcom/samsung/musicplus/contents/normal/PlayListFragment;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " newView"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/musicplus/library/iLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 718
    invoke-interface {p2, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 719
    .local v0, "id":J
    cmp-long v5, v10, v0

    if-nez v5, :cond_0

    .line 721
    const-string v5, "layout_inflater"

    invoke-virtual {p1, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 723
    .local v2, "li":Landroid/view/LayoutInflater;
    const v5, 0x7f04001a

    invoke-virtual {v2, v5, p3, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 724
    .local v3, "v":Landroid/view/View;
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/normal/PlayListFragment$PlayListTabAdapter;->getViewHolder()Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;

    move-result-object v4

    check-cast v4, Lcom/samsung/musicplus/contents/normal/PlayListFragment$PlayListTabAdapter$PlaylistTabHolder;

    .line 725
    .local v4, "vh":Lcom/samsung/musicplus/contents/normal/PlayListFragment$PlayListTabAdapter$PlaylistTabHolder;
    iput-wide v10, v4, Lcom/samsung/musicplus/contents/normal/PlayListFragment$PlayListTabAdapter$PlaylistTabHolder;->type:J

    .line 726
    invoke-virtual {v3, v4}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 730
    .end local v2    # "li":Landroid/view/LayoutInflater;
    .end local v3    # "v":Landroid/view/View;
    .end local v4    # "vh":Lcom/samsung/musicplus/contents/normal/PlayListFragment$PlayListTabAdapter$PlaylistTabHolder;
    :goto_0
    return-object v3

    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    goto :goto_0
.end method
