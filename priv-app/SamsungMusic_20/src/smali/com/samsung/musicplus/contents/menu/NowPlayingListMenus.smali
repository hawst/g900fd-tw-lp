.class public Lcom/samsung/musicplus/contents/menu/NowPlayingListMenus;
.super Lcom/samsung/musicplus/contents/menu/CommonListMenus;
.source "NowPlayingListMenus.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/menu/CommonListMenus;-><init>()V

    return-void
.end method


# virtual methods
.method public onContextItemSelected(Lcom/samsung/musicplus/common/menu/MusicMenuHandler;Landroid/view/MenuItem;J)Z
    .locals 9
    .param p1, "handler"    # Lcom/samsung/musicplus/common/menu/MusicMenuHandler;
    .param p2, "item"    # Landroid/view/MenuItem;
    .param p3, "selectedId"    # J

    .prologue
    const/4 v8, 0x0

    const/4 v3, 0x1

    .line 22
    invoke-interface {p2}, Landroid/view/MenuItem;->getMenuInfo()Landroid/view/ContextMenu$ContextMenuInfo;

    move-result-object v2

    check-cast v2, Landroid/widget/AdapterView$AdapterContextMenuInfo;

    .line 23
    .local v2, "mi":Landroid/widget/AdapterView$AdapterContextMenuInfo;
    invoke-interface {p1}, Lcom/samsung/musicplus/common/menu/MusicMenuHandler;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 25
    .local v0, "context":Landroid/content/Context;
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 36
    invoke-super {p0, p1, p2, p3, p4}, Lcom/samsung/musicplus/contents/menu/CommonListMenus;->onContextItemSelected(Lcom/samsung/musicplus/common/menu/MusicMenuHandler;Landroid/view/MenuItem;J)Z

    move-result v3

    :goto_0
    return v3

    .line 28
    :pswitch_0
    iget v4, v2, Landroid/widget/AdapterView$AdapterContextMenuInfo;->position:I

    iget v5, v2, Landroid/widget/AdapterView$AdapterContextMenuInfo;->position:I

    invoke-static {v4, v5}, Lcom/samsung/musicplus/util/ServiceUtils;->removeTracks(II)I

    .line 29
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0f000e

    new-array v6, v3, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-virtual {v4, v5, v3, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 31
    .local v1, "message":Ljava/lang/String;
    invoke-static {v0, v1, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 25
    :pswitch_data_0
    .packed-switch 0x7f0d01cf
        :pswitch_0
    .end packed-switch
.end method
