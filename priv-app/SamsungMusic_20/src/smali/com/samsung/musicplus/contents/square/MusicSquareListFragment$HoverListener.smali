.class Lcom/samsung/musicplus/contents/square/MusicSquareListFragment$HoverListener;
.super Ljava/lang/Object;
.source "MusicSquareListFragment.java"

# interfaces
.implements Landroid/view/View$OnHoverListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "HoverListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;


# direct methods
.method private constructor <init>(Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;)V
    .locals 0

    .prologue
    .line 247
    iput-object p1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment$HoverListener;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;Lcom/samsung/musicplus/contents/square/MusicSquareListFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;
    .param p2, "x1"    # Lcom/samsung/musicplus/contents/square/MusicSquareListFragment$1;

    .prologue
    .line 247
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment$HoverListener;-><init>(Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;)V

    return-void
.end method


# virtual methods
.method public onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 250
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/musicplus/library/hardware/AirView;->isEnablePenHoveringIconLabel(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/musicplus/library/hardware/AirView;->isEnablePenHovering(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 251
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/16 v1, 0x9

    if-ne v0, v1, :cond_2

    .line 252
    iget-object v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment$HoverListener;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;

    # getter for: Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mIsTrackListOpened:Z
    invoke-static {v0}, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->access$000(Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 253
    iget-object v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment$HoverListener;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;

    # getter for: Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mLaunchTrackIcon:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->access$400(Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment$HoverListener;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;

    const v2, 0x7f100175

    invoke-virtual {v1, v2}, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 265
    :cond_0
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 255
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment$HoverListener;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;

    # getter for: Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mLaunchTrackIcon:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->access$400(Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment$HoverListener;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;

    const v2, 0x7f100176

    invoke-virtual {v1, v2}, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 257
    :cond_2
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/16 v1, 0xa

    if-ne v0, v1, :cond_0

    .line 258
    iget-object v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment$HoverListener;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;

    # getter for: Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mIsTrackListOpened:Z
    invoke-static {v0}, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->access$000(Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 259
    iget-object v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment$HoverListener;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;

    # getter for: Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mLaunchTrackIcon:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->access$400(Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment$HoverListener;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;

    const v2, 0x7f100185

    invoke-virtual {v1, v2}, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 261
    :cond_3
    iget-object v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment$HoverListener;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;

    # getter for: Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->mLaunchTrackIcon:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->access$400(Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;)Landroid/widget/ImageView;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment$HoverListener;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;

    const v2, 0x7f100186

    invoke-virtual {v1, v2}, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
