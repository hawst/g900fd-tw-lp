.class Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment$1;
.super Landroid/content/BroadcastReceiver;
.source "DlnaDmsListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;)V
    .locals 0

    .prologue
    .line 74
    iput-object p1, p0, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment$1;->this$0:Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v4, -0x1

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 78
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 79
    .local v0, "action":Ljava/lang/String;
    const-string v3, "com.samsung.musicplus.dlna.flat.searching.info"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 80
    iget-object v3, p0, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment$1;->this$0:Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;

    # getter for: Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->mIsContentsSearchError:Z
    invoke-static {v3}, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->access$000(Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 81
    iget-object v3, p0, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment$1;->this$0:Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;

    # setter for: Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->mIsFlatSearchFinished:Z
    invoke-static {v3, v6}, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->access$102(Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;Z)Z

    .line 82
    iget-object v3, p0, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment$1;->this$0:Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;

    invoke-virtual {v3, v6}, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->setListShown(Z)V

    .line 129
    :cond_0
    :goto_0
    return-void

    .line 85
    :cond_1
    const-string v3, "com.samsung.musicplus.dlna.flat.searching.extra.what"

    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 86
    .local v2, "what":I
    const-string v3, "MusicUiList"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mDlnaEventReceiver action : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " what : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    packed-switch v2, :pswitch_data_0

    .line 103
    iget-object v3, p0, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment$1;->this$0:Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;

    # setter for: Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->mIsFlatSearchFinished:Z
    invoke-static {v3, v7}, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->access$102(Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;Z)Z

    goto :goto_0

    .line 89
    :pswitch_0
    iget-object v3, p0, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment$1;->this$0:Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;

    # setter for: Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->mIsFlatSearchFinished:Z
    invoke-static {v3, v7}, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->access$102(Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;Z)Z

    .line 90
    iget-object v3, p0, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment$1;->this$0:Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;

    invoke-virtual {v3, v7}, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->setListShown(Z)V

    goto :goto_0

    .line 93
    :pswitch_1
    iget-object v3, p0, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment$1;->this$0:Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;

    # setter for: Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->mIsFlatSearchFinished:Z
    invoke-static {v3, v7}, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->access$102(Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;Z)Z

    goto :goto_0

    .line 96
    :pswitch_2
    iget-object v3, p0, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment$1;->this$0:Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;

    # setter for: Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->mIsFlatSearchFinished:Z
    invoke-static {v3, v6}, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->access$102(Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;Z)Z

    .line 97
    iget-object v3, p0, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment$1;->this$0:Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;

    invoke-virtual {v3, v6}, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->setListShown(Z)V

    goto :goto_0

    .line 100
    :pswitch_3
    iget-object v3, p0, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment$1;->this$0:Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;

    # setter for: Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->mIsFlatSearchFinished:Z
    invoke-static {v3, v6}, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->access$102(Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;Z)Z

    goto :goto_0

    .line 106
    .end local v2    # "what":I
    :cond_2
    const-string v3, "com.samsung.musicplus.dlna.flat.searching.error"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 107
    const-string v3, "com.samsung.musicplus.dlna.flat.searching.extra.error"

    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 108
    .restart local v2    # "what":I
    const-string v3, "com.samsung.musicplus.dlna.extra.deviceId"

    invoke-virtual {p2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 109
    .local v1, "id":Ljava/lang/String;
    const-string v3, "MusicUiList"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mDlnaEventReceiver action : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " what : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    packed-switch v2, :pswitch_data_1

    .line 125
    iget-object v3, p0, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment$1;->this$0:Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;

    # setter for: Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->mIsFlatSearchFinished:Z
    invoke-static {v3, v7}, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->access$102(Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;Z)Z

    goto/16 :goto_0

    .line 117
    :pswitch_4
    iget-object v3, p0, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment$1;->this$0:Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;

    # getter for: Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->mKey:Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->access$200(Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment$1;->this$0:Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;

    # getter for: Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->mKey:Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->access$300(Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 119
    iget-object v3, p0, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment$1;->this$0:Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;

    # setter for: Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->mIsContentsSearchError:Z
    invoke-static {v3, v6}, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->access$002(Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;Z)Z

    .line 120
    iget-object v3, p0, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment$1;->this$0:Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;

    # setter for: Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->mIsFlatSearchFinished:Z
    invoke-static {v3, v6}, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->access$102(Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;Z)Z

    .line 121
    iget-object v3, p0, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment$1;->this$0:Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;

    invoke-virtual {v3, v6}, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->setListShown(Z)V

    goto/16 :goto_0

    .line 87
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 110
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method
