.class Lcom/samsung/musicplus/contents/extra/AddToPlaylistFragment$1;
.super Landroid/os/Handler;
.source "AddToPlaylistFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/contents/extra/AddToPlaylistFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/contents/extra/AddToPlaylistFragment;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/contents/extra/AddToPlaylistFragment;)V
    .locals 0

    .prologue
    .line 123
    iput-object p1, p0, Lcom/samsung/musicplus/contents/extra/AddToPlaylistFragment$1;->this$0:Lcom/samsung/musicplus/contents/extra/AddToPlaylistFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 127
    :try_start_0
    iget-object v1, p0, Lcom/samsung/musicplus/contents/extra/AddToPlaylistFragment$1;->this$0:Lcom/samsung/musicplus/contents/extra/AddToPlaylistFragment;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/samsung/musicplus/contents/extra/AddToPlaylistFragment;->setListShown(Z)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 133
    :goto_0
    return-void

    .line 128
    :catch_0
    move-exception v0

    .line 129
    .local v0, "e":Ljava/lang/IllegalStateException;
    const-string v1, "add_to_playlist"

    const-string v2, "Maybe this case is that the view is not prepared, so try again"

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    iget-object v1, p0, Lcom/samsung/musicplus/contents/extra/AddToPlaylistFragment$1;->this$0:Lcom/samsung/musicplus/contents/extra/AddToPlaylistFragment;

    # getter for: Lcom/samsung/musicplus/contents/extra/AddToPlaylistFragment;->mListUpdater:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/musicplus/contents/extra/AddToPlaylistFragment;->access$100(Lcom/samsung/musicplus/contents/extra/AddToPlaylistFragment;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x0

    const-wide/16 v4, 0x12c

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method
