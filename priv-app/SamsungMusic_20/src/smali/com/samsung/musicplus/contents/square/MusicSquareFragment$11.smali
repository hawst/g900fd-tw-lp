.class Lcom/samsung/musicplus/contents/square/MusicSquareFragment$11;
.super Lcom/samsung/musicplus/common/menu/AbstractMusicMenus;
.source "MusicSquareFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->createMusicMenus()Lcom/samsung/musicplus/common/menu/AbstractMusicMenus;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;)V
    .locals 0

    .prologue
    .line 1799
    iput-object p1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$11;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    invoke-direct {p0}, Lcom/samsung/musicplus/common/menu/AbstractMusicMenus;-><init>()V

    return-void
.end method


# virtual methods
.method public onContextItemSelected(Lcom/samsung/musicplus/common/menu/MusicMenuHandler;Landroid/view/MenuItem;J)Z
    .locals 1
    .param p1, "handler"    # Lcom/samsung/musicplus/common/menu/MusicMenuHandler;
    .param p2, "item"    # Landroid/view/MenuItem;
    .param p3, "selectedId"    # J

    .prologue
    .line 1828
    const/4 v0, 0x0

    return v0
.end method

.method public onCreateContextMenu(Lcom/samsung/musicplus/common/menu/MusicMenuHandler;Landroid/view/MenuInflater;Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;J)V
    .locals 0
    .param p1, "handler"    # Lcom/samsung/musicplus/common/menu/MusicMenuHandler;
    .param p2, "inflater"    # Landroid/view/MenuInflater;
    .param p3, "menu"    # Landroid/view/ContextMenu;
    .param p4, "v"    # Landroid/view/View;
    .param p5, "menuInfo"    # Landroid/view/ContextMenu$ContextMenuInfo;
    .param p6, "selectedId"    # J

    .prologue
    .line 1823
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/MenuInflater;Landroid/view/Menu;)V
    .locals 1
    .param p1, "inflater"    # Landroid/view/MenuInflater;
    .param p2, "menu"    # Landroid/view/Menu;

    .prologue
    .line 1806
    const v0, 0x7f120009

    invoke-virtual {p1, v0, p2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 1807
    return-void
.end method

.method public onOptionsItemSelected(Lcom/samsung/musicplus/common/menu/MusicMenuHandler;Landroid/view/MenuItem;)Z
    .locals 3
    .param p1, "handler"    # Lcom/samsung/musicplus/common/menu/MusicMenuHandler;
    .param p2, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 1811
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 1817
    const/4 v1, 0x0

    :goto_0
    return v1

    .line 1813
    :pswitch_0
    new-instance v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$AxisDialog;

    invoke-direct {v0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$AxisDialog;-><init>()V

    .line 1814
    .local v0, "dialog":Lcom/samsung/musicplus/contents/square/MusicSquareFragment$AxisDialog;
    iget-object v1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$11;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    invoke-virtual {v1}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "axis"

    invoke-virtual {v0, v1, v2}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$AxisDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 1815
    const/4 v1, 0x1

    goto :goto_0

    .line 1811
    nop

    :pswitch_data_0
    .packed-switch 0x7f0d01f5
        :pswitch_0
    .end packed-switch
.end method

.method public onPrepareOptionsMenu(Lcom/samsung/musicplus/common/menu/MusicMenuHandler;Landroid/view/Menu;)V
    .locals 0
    .param p1, "handler"    # Lcom/samsung/musicplus/common/menu/MusicMenuHandler;
    .param p2, "menu"    # Landroid/view/Menu;

    .prologue
    .line 1802
    return-void
.end method
