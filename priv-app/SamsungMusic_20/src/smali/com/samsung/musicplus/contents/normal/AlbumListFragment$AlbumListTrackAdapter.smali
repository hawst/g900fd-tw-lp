.class Lcom/samsung/musicplus/contents/normal/AlbumListFragment$AlbumListTrackAdapter;
.super Lcom/samsung/musicplus/contents/TrackListNumberAdapter;
.source "AlbumListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/contents/normal/AlbumListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AlbumListTrackAdapter"
.end annotation


# static fields
.field private static final NUMBER_DOT:Ljava/lang/String; = "."


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/contents/normal/AlbumListFragment;


# direct methods
.method public constructor <init>(Lcom/samsung/musicplus/contents/normal/AlbumListFragment;Landroid/content/Context;ILandroid/database/Cursor;I)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "layout"    # I
    .param p4, "c"    # Landroid/database/Cursor;
    .param p5, "flags"    # I

    .prologue
    .line 235
    iput-object p1, p0, Lcom/samsung/musicplus/contents/normal/AlbumListFragment$AlbumListTrackAdapter;->this$0:Lcom/samsung/musicplus/contents/normal/AlbumListFragment;

    .line 236
    invoke-direct {p0, p2, p3, p4, p5}, Lcom/samsung/musicplus/contents/TrackListNumberAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;I)V

    .line 237
    return-void
.end method


# virtual methods
.method protected bindTextView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "c"    # Landroid/database/Cursor;

    .prologue
    .line 265
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/musicplus/contents/TrackListNumberAdapter;->bindTextView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    .line 266
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/musicplus/contents/TrackListNumberAdapter$TrackNumberViewHolder;

    .line 267
    .local v1, "vh":Lcom/samsung/musicplus/contents/TrackListNumberAdapter$TrackNumberViewHolder;
    invoke-interface {p3}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    .line 268
    .local v0, "position":I
    iget-object v2, p0, Lcom/samsung/musicplus/contents/normal/AlbumListFragment$AlbumListTrackAdapter;->this$0:Lcom/samsung/musicplus/contents/normal/AlbumListFragment;

    iget-object v3, p0, Lcom/samsung/musicplus/contents/normal/AlbumListFragment$AlbumListTrackAdapter;->this$0:Lcom/samsung/musicplus/contents/normal/AlbumListFragment;

    # getter for: Lcom/samsung/musicplus/contents/normal/AlbumListFragment;->mList:I
    invoke-static {v3}, Lcom/samsung/musicplus/contents/normal/AlbumListFragment;->access$300(Lcom/samsung/musicplus/contents/normal/AlbumListFragment;)I

    move-result v3

    # invokes: Lcom/samsung/musicplus/contents/normal/AlbumListFragment;->isEnableListShuffle(I)Z
    invoke-static {v2, v3}, Lcom/samsung/musicplus/contents/normal/AlbumListFragment;->access$400(Lcom/samsung/musicplus/contents/normal/AlbumListFragment;I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 269
    add-int/lit8 v0, v0, 0x1

    .line 271
    :cond_0
    iget-object v2, v1, Lcom/samsung/musicplus/contents/TrackListNumberAdapter$TrackNumberViewHolder;->number:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 272
    return-void
.end method

.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "c"    # Landroid/database/Cursor;

    .prologue
    .line 255
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/musicplus/contents/TrackListNumberAdapter;->bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    .line 256
    const v1, 0x7f0d00bb

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 257
    .local v0, "divider":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 258
    const v1, 0x7f020022

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 260
    :cond_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/samsung/musicplus/contents/normal/AlbumListFragment$AlbumListTrackAdapter;->hideBottomDivider(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    .line 261
    return-void
.end method

.method protected newTextView(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 241
    invoke-super {p0, p1}, Lcom/samsung/musicplus/contents/TrackListNumberAdapter;->newTextView(Landroid/view/View;)V

    .line 242
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/contents/TrackListNumberAdapter$TrackNumberViewHolder;

    .line 243
    .local v0, "vh":Lcom/samsung/musicplus/contents/TrackListNumberAdapter$TrackNumberViewHolder;
    const v1, 0x7f0d009f

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v0, Lcom/samsung/musicplus/contents/TrackListNumberAdapter$TrackNumberViewHolder;->text1:Landroid/widget/TextView;

    .line 244
    const v1, 0x7f0d00a0

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v0, Lcom/samsung/musicplus/contents/TrackListNumberAdapter$TrackNumberViewHolder;->text2:Landroid/widget/TextView;

    .line 245
    iget-object v1, p0, Lcom/samsung/musicplus/contents/normal/AlbumListFragment$AlbumListTrackAdapter;->this$0:Lcom/samsung/musicplus/contents/normal/AlbumListFragment;

    # getter for: Lcom/samsung/musicplus/contents/normal/AlbumListFragment;->mIsSingleArtist:Z
    invoke-static {v1}, Lcom/samsung/musicplus/contents/normal/AlbumListFragment;->access$200(Lcom/samsung/musicplus/contents/normal/AlbumListFragment;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 246
    iget-object v1, v0, Lcom/samsung/musicplus/contents/TrackListNumberAdapter$TrackNumberViewHolder;->text2:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 250
    :goto_0
    invoke-virtual {p1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 251
    return-void

    .line 248
    :cond_0
    iget-object v1, v0, Lcom/samsung/musicplus/contents/TrackListNumberAdapter$TrackNumberViewHolder;->text2:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method protected setShuffleOfTextView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "c"    # Landroid/database/Cursor;

    .prologue
    const v4, 0x7f0c008a

    const/4 v3, 0x0

    .line 276
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/musicplus/contents/TrackListNumberAdapter;->setShuffleOfTextView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    .line 277
    const v1, 0x7f0d00ad

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 278
    .local v0, "layout":Landroid/widget/LinearLayout;
    iget-object v1, p0, Lcom/samsung/musicplus/contents/normal/AlbumListFragment$AlbumListTrackAdapter;->this$0:Lcom/samsung/musicplus/contents/normal/AlbumListFragment;

    invoke-virtual {v1}, Lcom/samsung/musicplus/contents/normal/AlbumListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/musicplus/contents/normal/AlbumListFragment$AlbumListTrackAdapter;->this$0:Lcom/samsung/musicplus/contents/normal/AlbumListFragment;

    invoke-virtual {v2}, Lcom/samsung/musicplus/contents/normal/AlbumListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v0, v1, v3, v2, v3}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    .line 280
    return-void
.end method
