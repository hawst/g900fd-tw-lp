.class public Lcom/samsung/musicplus/contents/dlna/DlnaStore$ServerContents;
.super Lcom/samsung/musicplus/library/dlna/DlnaStore$MediaServerContents;
.source "DlnaStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/contents/dlna/DlnaStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ServerContents"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/contents/dlna/DlnaStore$ServerContents$AlbumArt;
    }
.end annotation


# static fields
.field public static final CONTENT_URI:Landroid/net/Uri;

.field public static final DLNA_DMS_CONTENTS_TABLE_NAME:Ljava/lang/String; = "dlna_dms_contents_table"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 109
    invoke-static {}, Lcom/samsung/musicplus/contents/dlna/DlnaStore$ServerContents;->getContentUri()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/contents/dlna/DlnaStore$ServerContents;->CONTENT_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 100
    invoke-direct {p0}, Lcom/samsung/musicplus/library/dlna/DlnaStore$MediaServerContents;-><init>()V

    .line 128
    return-void
.end method

.method public static final getContentUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 120
    const-string v0, "content://com.samsung.musicplus/dlna_dms_contents_table"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method
