.class Lcom/samsung/musicplus/contents/normal/AlbumListFragment$AlbumTabAdapter;
.super Lcom/samsung/musicplus/widget/list/CommonListAdapter;
.source "AlbumListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/contents/normal/AlbumListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AlbumTabAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/contents/normal/AlbumListFragment;


# direct methods
.method public constructor <init>(Lcom/samsung/musicplus/contents/normal/AlbumListFragment;Landroid/content/Context;ILandroid/database/Cursor;I)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "layout"    # I
    .param p4, "c"    # Landroid/database/Cursor;
    .param p5, "flags"    # I

    .prologue
    .line 192
    iput-object p1, p0, Lcom/samsung/musicplus/contents/normal/AlbumListFragment$AlbumTabAdapter;->this$0:Lcom/samsung/musicplus/contents/normal/AlbumListFragment;

    .line 193
    invoke-direct {p0, p2, p3, p4, p5}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;I)V

    .line 194
    return-void
.end method


# virtual methods
.method protected bindTextView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "c"    # Landroid/database/Cursor;

    .prologue
    .line 218
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;

    .line 219
    .local v1, "vh":Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;
    iget-object v2, v1, Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;->text1:Landroid/widget/TextView;

    iget v3, p0, Lcom/samsung/musicplus/contents/normal/AlbumListFragment$AlbumTabAdapter;->mText1Index:I

    invoke-interface {p3, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 220
    const-string v2, "artist_count"

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 221
    .local v0, "countIdx":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 222
    invoke-interface {p3, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_1

    .line 223
    iget-object v2, v1, Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;->text2:Landroid/widget/TextView;

    const v3, 0x7f1001be

    invoke-virtual {p2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 228
    :cond_0
    :goto_0
    return-void

    .line 225
    :cond_1
    iget-object v2, v1, Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;->text2:Landroid/widget/TextView;

    iget v3, p0, Lcom/samsung/musicplus/contents/normal/AlbumListFragment$AlbumTabAdapter;->mText2Index:I

    invoke-interface {p3, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p2, v3}, Lcom/samsung/musicplus/util/UiUtils;->transUnknownString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "c"    # Landroid/database/Cursor;

    .prologue
    .line 213
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    .line 214
    invoke-virtual {p0, p1, p2, p3}, Lcom/samsung/musicplus/contents/normal/AlbumListFragment$AlbumTabAdapter;->hideBottomDivider(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    .line 215
    return-void
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 199
    iget-object v0, p0, Lcom/samsung/musicplus/contents/normal/AlbumListFragment$AlbumTabAdapter;->this$0:Lcom/samsung/musicplus/contents/normal/AlbumListFragment;

    # getter for: Lcom/samsung/musicplus/contents/normal/AlbumListFragment;->mList:I
    invoke-static {v0}, Lcom/samsung/musicplus/contents/normal/AlbumListFragment;->access$000(Lcom/samsung/musicplus/contents/normal/AlbumListFragment;)I

    move-result v0

    invoke-static {v0}, Lcom/samsung/musicplus/util/ListUtils;->isGridType(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 200
    add-int/lit8 v0, p1, -0x1

    invoke-super {p0, v0}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    .line 202
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 206
    iget-object v0, p0, Lcom/samsung/musicplus/contents/normal/AlbumListFragment$AlbumTabAdapter;->this$0:Lcom/samsung/musicplus/contents/normal/AlbumListFragment;

    # getter for: Lcom/samsung/musicplus/contents/normal/AlbumListFragment;->mList:I
    invoke-static {v0}, Lcom/samsung/musicplus/contents/normal/AlbumListFragment;->access$100(Lcom/samsung/musicplus/contents/normal/AlbumListFragment;)I

    move-result v0

    invoke-static {v0}, Lcom/samsung/musicplus/util/ListUtils;->isGridType(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 207
    add-int/lit8 v0, p1, -0x1

    invoke-super {p0, v0}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->getItemId(I)J

    move-result-wide v0

    .line 209
    :goto_0
    return-wide v0

    :cond_0
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->getItemId(I)J

    move-result-wide v0

    goto :goto_0
.end method
