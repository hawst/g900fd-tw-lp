.class public Lcom/samsung/musicplus/contents/PersonalFileOperationThread;
.super Ljava/lang/Thread;
.source "PersonalFileOperationThread.java"

# interfaces
.implements Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$OperationState;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/contents/PersonalFileOperationThread$FolderItem;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "PersonalFileOperation"


# instance fields
.field final MAX_PROGRESS_UPDATE:I

.field private mAllAppliedCheck:Z

.field private mContentResolver:Landroid/content/ContentResolver;

.field private mContext:Landroid/content/Context;

.field private mDoRename:Z

.field private mDoReplace:Z

.field private mHandler:Landroid/os/Handler;

.field private mIsFolder:Z

.field private mIsMoveCmd:Z

.field private mIsPrivateOn:Z

.field private mList:[J

.field private mMovedCnt:I

.field private mPartialWakeLock:Landroid/os/PowerManager$WakeLock;

.field private mPersonalItems:I

.field private mRemoveFromNowPlaying:Z

.field private mRenameCancelled:Z

.field private mSelectionDestPath:Ljava/lang/String;

.field private mStopped:Z

.field private mSuccessMovedCnt:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 106
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 65
    iput-boolean v0, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mStopped:Z

    .line 67
    iput v0, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mPersonalItems:I

    .line 69
    iput-boolean v0, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mAllAppliedCheck:Z

    .line 71
    iput-boolean v0, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mDoRename:Z

    .line 73
    iput-boolean v0, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mRenameCancelled:Z

    .line 75
    iput-boolean v0, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mDoReplace:Z

    .line 77
    iput-boolean v0, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mIsMoveCmd:Z

    .line 79
    iput-boolean v0, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mIsFolder:Z

    .line 86
    iput-boolean v0, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mRemoveFromNowPlaying:Z

    .line 168
    iput v0, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mSuccessMovedCnt:I

    .line 170
    iput v0, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mMovedCnt:I

    .line 361
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->MAX_PROGRESS_UPDATE:I

    .line 107
    iput-object p1, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mContext:Landroid/content/Context;

    .line 108
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mContentResolver:Landroid/content/ContentResolver;

    .line 109
    return-void
.end method

.method private addPostfix(Ljava/lang/String;I)Ljava/lang/String;
    .locals 7
    .param p1, "fullName"    # Ljava/lang/String;
    .param p2, "postfixNum"    # I

    .prologue
    const/16 v6, 0x29

    .line 465
    const/4 v3, 0x0

    .line 466
    .local v3, "resultStr":Ljava/lang/String;
    const/16 v5, 0x2e

    invoke-virtual {p1, v5}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    .line 467
    .local v1, "lastDot":I
    const/4 v5, -0x1

    if-eq v1, v5, :cond_0

    .line 468
    const/4 v5, 0x0

    invoke-virtual {p1, v5, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 469
    .local v2, "name":Ljava/lang/String;
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v5

    invoke-virtual {p1, v1, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 470
    .local v0, "ext":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4, v2}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 471
    .local v4, "sb":Ljava/lang/StringBuffer;
    const-string v5, " ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 472
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    .line 478
    .end local v0    # "ext":Ljava/lang/String;
    .end local v2    # "name":Ljava/lang/String;
    :goto_0
    return-object v3

    .line 474
    .end local v4    # "sb":Ljava/lang/StringBuffer;
    :cond_0
    new-instance v4, Ljava/lang/StringBuffer;

    invoke-direct {v4, p1}, Ljava/lang/StringBuffer;-><init>(Ljava/lang/String;)V

    .line 475
    .restart local v4    # "sb":Ljava/lang/StringBuffer;
    const-string v5, " ("

    invoke-virtual {v4, v5}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    move-result-object v5

    invoke-virtual {v5, v6}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 476
    invoke-virtual {v4}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method private checkFileNameExists(Ljava/lang/String;)Z
    .locals 2
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 450
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 451
    .local v0, "file":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    return v1
.end method

.method private checkInvalidFilePath(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "srcPath"    # Ljava/lang/String;

    .prologue
    .line 491
    iget-object v1, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mContext:Landroid/content/Context;

    invoke-direct {p0, v1}, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->getSecretBoxPath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 493
    .local v0, "secretBoxtPath":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->isMovingCmd()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->isMovingCmd()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p2, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    :cond_1
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private copyFile(JLjava/lang/String;Ljava/lang/String;)Z
    .locals 5
    .param p1, "id"    # J
    .param p3, "source"    # Ljava/lang/String;
    .param p4, "target"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 315
    iget-boolean v2, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mAllAppliedCheck:Z

    if-eqz v2, :cond_3

    .line 316
    iget-boolean v2, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mDoRename:Z

    if-eqz v2, :cond_2

    .line 317
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mContext:Landroid/content/Context;

    invoke-direct {p0, v3}, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->getDestRootPath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-direct {p0, p4}, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->startRename(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p4

    .line 318
    const-string v2, "PersonalFileOperation"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "copy file, applied all files and rename, path : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 343
    :cond_0
    :goto_0
    iget-boolean v2, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mRenameCancelled:Z

    if-eqz v2, :cond_5

    .line 358
    :cond_1
    :goto_1
    return v1

    .line 319
    :cond_2
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->isStopMoving()Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_1

    .line 323
    :cond_3
    invoke-direct {p0, p4}, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->checkFileNameExists(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 324
    iget-object v2, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mHandler:Landroid/os/Handler;

    const/4 v4, 0x4

    invoke-static {v3, v4, p4}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 327
    :try_start_0
    monitor-enter p0
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 328
    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V

    .line 329
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 334
    :goto_2
    iget-boolean v2, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mDoRename:Z

    if-eqz v2, :cond_4

    .line 335
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mContext:Landroid/content/Context;

    invoke-direct {p0, v3}, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->getDestRootPath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-direct {p0, p4}, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->startRename(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p4

    .line 337
    :cond_4
    const-string v2, "PersonalFileOperation"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "copy file, exists same file!!! target path : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", mAllAppliedCheck : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mAllAppliedCheck:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", mDoRename : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mDoRename:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", mRenameCancelled : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mRenameCancelled:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", mDoReplace : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mDoReplace:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 329
    :catchall_0
    move-exception v2

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    throw v2
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0

    .line 330
    :catch_0
    move-exception v0

    .line 331
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_2

    .line 351
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_5
    const-string v2, "PersonalFileOperation"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "copy file path - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 353
    invoke-direct {p0, p3, p4}, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->doRealFileCopy(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    .line 354
    .local v1, "success":Z
    iget-boolean v2, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mDoReplace:Z

    if-nez v2, :cond_1

    .line 355
    invoke-direct {p0, p1, p2, p4, v1}, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->updatePartialDBField(JLjava/lang/String;Z)V

    goto/16 :goto_1
.end method

.method private deleteListItemInDB(J)V
    .locals 5
    .param p1, "id"    # J

    .prologue
    .line 715
    iget-object v0, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/samsung/musicplus/provider/MusicContents;->MUSIC_CONTENTS_URI:Landroid/net/Uri;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 717
    return-void
.end method

.method private doFileMoveCommonOperation(JLjava/lang/String;Ljava/lang/String;)V
    .locals 7
    .param p1, "id"    # J
    .param p3, "srcPath"    # Ljava/lang/String;
    .param p4, "destPath"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x1

    .line 284
    iget v3, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mMovedCnt:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mMovedCnt:I

    const/4 v4, 0x0

    invoke-direct {p0, v3, v4}, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->updateProgress(II)V

    .line 286
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->isStopMoving()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 311
    :cond_0
    :goto_0
    return-void

    .line 290
    :cond_1
    if-eqz p3, :cond_0

    const-string v3, ""

    invoke-virtual {v3, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eq v3, v5, :cond_0

    if-eqz p4, :cond_0

    const-string v3, ""

    invoke-virtual {v3, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eq v3, v5, :cond_0

    .line 295
    invoke-direct {p0, p4}, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->getDestFreeSpace(Ljava/lang/String;)J

    move-result-wide v0

    .line 296
    .local v0, "destFreeSize":J
    const-wide/16 v4, 0x0

    cmp-long v3, v0, v4

    if-nez v3, :cond_2

    .line 297
    const-string v3, "PersonalFileOperation"

    const-string v4, "copyFile(), destFreeSize is 0"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 298
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->doStopMove()V

    .line 299
    iget-object v3, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mHandler:Landroid/os/Handler;

    const/4 v4, 0x3

    invoke-virtual {v3, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 303
    :cond_2
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->copyFile(JLjava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 304
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, p3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 305
    .local v2, "srcFile":Ljava/io/File;
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 306
    iget v3, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mSuccessMovedCnt:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mSuccessMovedCnt:I

    goto :goto_0
.end method

.method private doFileMoveOperation(Landroid/database/Cursor;Ljava/lang/String;)V
    .locals 7
    .param p1, "c"    # Landroid/database/Cursor;
    .param p2, "srcPath"    # Ljava/lang/String;

    .prologue
    .line 265
    const-string v3, "PersonalFileOperation"

    const-string v6, "copy file start!"

    invoke-static {v3, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 267
    const-string v3, "_id"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 268
    .local v4, "id":J
    const-string v3, "_display_name"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 269
    .local v2, "fileName":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mContext:Landroid/content/Context;

    invoke-direct {p0, v3}, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->getDestRootPath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 270
    .local v0, "destFolderPath":Ljava/lang/String;
    invoke-direct {p0, v0}, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->ensureFolder(Ljava/lang/String;)V

    .line 271
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 273
    .local v1, "destPath":Ljava/lang/String;
    invoke-direct {p0, v4, v5, p2, v1}, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->doFileMoveCommonOperation(JLjava/lang/String;Ljava/lang/String;)V

    .line 274
    return-void
.end method

.method private doMoveFolderOperation(Landroid/database/Cursor;)V
    .locals 11
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 241
    const-string v9, "bucket_id"

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 242
    .local v0, "bucketId":Ljava/lang/String;
    invoke-direct {p0, v0}, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->getFolderInfo(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    .line 243
    .local v3, "folderItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/musicplus/contents/PersonalFileOperationThread$FolderItem;>;"
    if-nez v3, :cond_1

    .line 244
    const-string v9, "PersonalFileOperation"

    const-string v10, "doMoveFolderOperation, folderItems is null!"

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 262
    :cond_0
    return-void

    .line 248
    :cond_1
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v10, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mContext:Landroid/content/Context;

    invoke-direct {p0, v10}, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->getDestRootPath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const/4 v9, 0x0

    invoke-virtual {v3, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/musicplus/contents/PersonalFileOperationThread$FolderItem;

    iget-object v9, v9, Lcom/samsung/musicplus/contents/PersonalFileOperationThread$FolderItem;->folderName:Ljava/lang/String;

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "/"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 249
    .local v1, "destFolderPath":Ljava/lang/String;
    invoke-direct {p0, v1}, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->ensureFolder(Ljava/lang/String;)V

    .line 250
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v5

    .line 251
    .local v5, "size":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-ge v4, v5, :cond_0

    .line 252
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->isStopMoving()Z

    move-result v9

    if-nez v9, :cond_0

    .line 256
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/musicplus/contents/PersonalFileOperationThread$FolderItem;

    iget-wide v6, v9, Lcom/samsung/musicplus/contents/PersonalFileOperationThread$FolderItem;->id:J

    .line 257
    .local v6, "id":J
    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/musicplus/contents/PersonalFileOperationThread$FolderItem;

    iget-object v8, v9, Lcom/samsung/musicplus/contents/PersonalFileOperationThread$FolderItem;->path:Ljava/lang/String;

    .line 258
    .local v8, "srcPath":Ljava/lang/String;
    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/musicplus/contents/PersonalFileOperationThread$FolderItem;

    iget-object v9, v9, Lcom/samsung/musicplus/contents/PersonalFileOperationThread$FolderItem;->fileName:Ljava/lang/String;

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 260
    .local v2, "destPath":Ljava/lang/String;
    invoke-direct {p0, v6, v7, v8, v2}, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->doFileMoveCommonOperation(JLjava/lang/String;Ljava/lang/String;)V

    .line 251
    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method

.method private doRealFileCopy(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 16
    .param p1, "source"    # Ljava/lang/String;
    .param p2, "target"    # Ljava/lang/String;

    .prologue
    .line 366
    const/4 v6, 0x0

    .line 367
    .local v6, "inputStream":Ljava/io/FileInputStream;
    const/4 v8, 0x0

    .line 370
    .local v8, "outputStream":Ljava/io/FileOutputStream;
    :try_start_0
    new-instance v7, Ljava/io/FileInputStream;

    move-object/from16 v0, p1

    invoke-direct {v7, v0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 371
    .end local v6    # "inputStream":Ljava/io/FileInputStream;
    .local v7, "inputStream":Ljava/io/FileInputStream;
    :try_start_1
    new-instance v9, Ljava/io/FileOutputStream;

    move-object/from16 v0, p2

    invoke-direct {v9, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_f

    .line 383
    .end local v8    # "outputStream":Ljava/io/FileOutputStream;
    .local v9, "outputStream":Ljava/io/FileOutputStream;
    invoke-virtual {v7}, Ljava/io/FileInputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v4

    .line 384
    .local v4, "fcin":Ljava/nio/channels/FileChannel;
    invoke-virtual {v9}, Ljava/io/FileOutputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v5

    .line 387
    .local v5, "fcout":Ljava/nio/channels/FileChannel;
    :try_start_2
    invoke-virtual {v4}, Ljava/nio/channels/FileChannel;->size()J

    move-result-wide v12

    .line 393
    .local v12, "srcSize":J
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mHandler:Landroid/os/Handler;

    const/4 v14, 0x2

    invoke-virtual {v11, v14}, Landroid/os/Handler;->removeMessages(I)V

    .line 394
    const/4 v11, -0x1

    long-to-int v14, v12

    move-object/from16 v0, p0

    invoke-direct {v0, v11, v14}, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->updateProgress(II)V

    .line 395
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-direct {v0, v1}, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->getDestFreeSpace(Ljava/lang/String;)J

    move-result-wide v14

    cmp-long v11, v12, v14

    if-lez v11, :cond_1

    .line 396
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->doStopMove()V

    .line 397
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mHandler:Landroid/os/Handler;

    const/4 v14, 0x3

    invoke-virtual {v11, v14}, Landroid/os/Handler;->sendEmptyMessage(I)Z
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_6
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 398
    const/4 v10, 0x0

    .line 413
    .local v10, "result":Z
    :goto_0
    :try_start_3
    invoke-virtual {v4}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 418
    :goto_1
    :try_start_4
    invoke-virtual {v5}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    .line 423
    :goto_2
    :try_start_5
    invoke-virtual {v9}, Ljava/io/FileOutputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    .line 428
    :goto_3
    :try_start_6
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_5

    .end local v12    # "srcSize":J
    :goto_4
    move-object v8, v9

    .end local v9    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v8    # "outputStream":Ljava/io/FileOutputStream;
    move-object v6, v7

    .line 434
    .end local v4    # "fcin":Ljava/nio/channels/FileChannel;
    .end local v5    # "fcout":Ljava/nio/channels/FileChannel;
    .end local v7    # "inputStream":Ljava/io/FileInputStream;
    .end local v10    # "result":Z
    .restart local v6    # "inputStream":Ljava/io/FileInputStream;
    :goto_5
    return v10

    .line 372
    :catch_0
    move-exception v2

    .line 373
    .local v2, "e":Ljava/io/FileNotFoundException;
    :goto_6
    if-eqz v6, :cond_0

    .line 375
    :try_start_7
    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1

    .line 380
    :cond_0
    :goto_7
    const/4 v10, 0x0

    goto :goto_5

    .line 376
    :catch_1
    move-exception v3

    .line 377
    .local v3, "e1":Ljava/io/IOException;
    const-string v11, "PersonalFileOperation"

    const-string v14, "copyFile : Fail to close inputStream"

    invoke-static {v11, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_7

    .line 404
    .end local v2    # "e":Ljava/io/FileNotFoundException;
    .end local v3    # "e1":Ljava/io/IOException;
    .end local v6    # "inputStream":Ljava/io/FileInputStream;
    .end local v8    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v4    # "fcin":Ljava/nio/channels/FileChannel;
    .restart local v5    # "fcout":Ljava/nio/channels/FileChannel;
    .restart local v7    # "inputStream":Ljava/io/FileInputStream;
    .restart local v9    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v12    # "srcSize":J
    :cond_1
    :try_start_8
    move-object/from16 v0, p0

    invoke-direct {v0, v7, v9}, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->doStreamCopy(Ljava/io/FileInputStream;Ljava/io/FileOutputStream;)V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_6
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 406
    const/4 v10, 0x1

    .restart local v10    # "result":Z
    goto :goto_0

    .line 414
    :catch_2
    move-exception v3

    .line 415
    .restart local v3    # "e1":Ljava/io/IOException;
    const-string v11, "PersonalFileOperation"

    const-string v14, "copyFile : Fail to close fcin"

    invoke-static {v11, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 419
    .end local v3    # "e1":Ljava/io/IOException;
    :catch_3
    move-exception v3

    .line 420
    .restart local v3    # "e1":Ljava/io/IOException;
    const-string v11, "PersonalFileOperation"

    const-string v14, "copyFile : Fail to close fcout"

    invoke-static {v11, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 424
    .end local v3    # "e1":Ljava/io/IOException;
    :catch_4
    move-exception v2

    .line 425
    .local v2, "e":Ljava/io/IOException;
    const-string v11, "PersonalFileOperation"

    const-string v14, "copyFile : Fail to close outputStream"

    invoke-static {v11, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    .line 429
    .end local v2    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v2

    .line 430
    .restart local v2    # "e":Ljava/io/IOException;
    const-string v11, "PersonalFileOperation"

    const-string v14, "copyFile : Fail to close inputStream"

    invoke-static {v11, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 408
    .end local v2    # "e":Ljava/io/IOException;
    .end local v10    # "result":Z
    .end local v12    # "srcSize":J
    :catch_6
    move-exception v2

    .line 409
    .restart local v2    # "e":Ljava/io/IOException;
    :try_start_9
    invoke-virtual {v2}, Ljava/io/IOException;->printStackTrace()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 410
    const/4 v10, 0x0

    .line 413
    .restart local v10    # "result":Z
    :try_start_a
    invoke-virtual {v4}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_8

    .line 418
    :goto_8
    :try_start_b
    invoke-virtual {v5}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_9

    .line 423
    :goto_9
    :try_start_c
    invoke-virtual {v9}, Ljava/io/FileOutputStream;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_a

    .line 428
    :goto_a
    :try_start_d
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_7

    goto :goto_4

    .line 429
    :catch_7
    move-exception v2

    .line 430
    const-string v11, "PersonalFileOperation"

    const-string v14, "copyFile : Fail to close inputStream"

    invoke-static {v11, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 414
    :catch_8
    move-exception v3

    .line 415
    .restart local v3    # "e1":Ljava/io/IOException;
    const-string v11, "PersonalFileOperation"

    const-string v14, "copyFile : Fail to close fcin"

    invoke-static {v11, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_8

    .line 419
    .end local v3    # "e1":Ljava/io/IOException;
    :catch_9
    move-exception v3

    .line 420
    .restart local v3    # "e1":Ljava/io/IOException;
    const-string v11, "PersonalFileOperation"

    const-string v14, "copyFile : Fail to close fcout"

    invoke-static {v11, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_9

    .line 424
    .end local v3    # "e1":Ljava/io/IOException;
    :catch_a
    move-exception v2

    .line 425
    const-string v11, "PersonalFileOperation"

    const-string v14, "copyFile : Fail to close outputStream"

    invoke-static {v11, v14}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_a

    .line 412
    .end local v2    # "e":Ljava/io/IOException;
    .end local v10    # "result":Z
    :catchall_0
    move-exception v11

    .line 413
    :try_start_e
    invoke-virtual {v4}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_b

    .line 418
    :goto_b
    :try_start_f
    invoke-virtual {v5}, Ljava/nio/channels/FileChannel;->close()V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_c

    .line 423
    :goto_c
    :try_start_10
    invoke-virtual {v9}, Ljava/io/FileOutputStream;->close()V
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_d

    .line 428
    :goto_d
    :try_start_11
    invoke-virtual {v7}, Ljava/io/FileInputStream;->close()V
    :try_end_11
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_e

    .line 431
    :goto_e
    throw v11

    .line 414
    :catch_b
    move-exception v3

    .line 415
    .restart local v3    # "e1":Ljava/io/IOException;
    const-string v14, "PersonalFileOperation"

    const-string v15, "copyFile : Fail to close fcin"

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_b

    .line 419
    .end local v3    # "e1":Ljava/io/IOException;
    :catch_c
    move-exception v3

    .line 420
    .restart local v3    # "e1":Ljava/io/IOException;
    const-string v14, "PersonalFileOperation"

    const-string v15, "copyFile : Fail to close fcout"

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_c

    .line 424
    .end local v3    # "e1":Ljava/io/IOException;
    :catch_d
    move-exception v2

    .line 425
    .restart local v2    # "e":Ljava/io/IOException;
    const-string v14, "PersonalFileOperation"

    const-string v15, "copyFile : Fail to close outputStream"

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_d

    .line 429
    .end local v2    # "e":Ljava/io/IOException;
    :catch_e
    move-exception v2

    .line 430
    .restart local v2    # "e":Ljava/io/IOException;
    const-string v14, "PersonalFileOperation"

    const-string v15, "copyFile : Fail to close inputStream"

    invoke-static {v14, v15}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_e

    .line 372
    .end local v2    # "e":Ljava/io/IOException;
    .end local v4    # "fcin":Ljava/nio/channels/FileChannel;
    .end local v5    # "fcout":Ljava/nio/channels/FileChannel;
    .end local v9    # "outputStream":Ljava/io/FileOutputStream;
    .restart local v8    # "outputStream":Ljava/io/FileOutputStream;
    :catch_f
    move-exception v2

    move-object v6, v7

    .end local v7    # "inputStream":Ljava/io/FileInputStream;
    .restart local v6    # "inputStream":Ljava/io/FileInputStream;
    goto/16 :goto_6
.end method

.method private doReset()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 503
    iput-boolean v0, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mAllAppliedCheck:Z

    .line 504
    iput-boolean v0, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mDoRename:Z

    .line 505
    iput-boolean v0, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mRenameCancelled:Z

    .line 506
    iput-boolean v0, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mDoReplace:Z

    .line 507
    iput-boolean v0, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mStopped:Z

    .line 508
    iput v0, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mPersonalItems:I

    .line 509
    iput v0, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mSuccessMovedCnt:I

    .line 510
    iput v0, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mMovedCnt:I

    .line 511
    return-void
.end method

.method private doStreamCopy(Ljava/io/FileInputStream;Ljava/io/FileOutputStream;)V
    .locals 5
    .param p1, "fis"    # Ljava/io/FileInputStream;
    .param p2, "fos"    # Ljava/io/FileOutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 438
    const/16 v0, 0x800

    .line 439
    .local v0, "BUFFER_SIZE":I
    const/16 v4, 0x800

    new-array v1, v4, [B

    .line 440
    .local v1, "buffer":[B
    const/4 v3, 0x0

    .line 441
    .local v3, "progress":I
    const/4 v2, 0x0

    .line 442
    .local v2, "copy_size":I
    :goto_0
    invoke-virtual {p1, v1}, Ljava/io/FileInputStream;->read([B)I

    move-result v2

    const/4 v4, -0x1

    if-eq v2, v4, :cond_0

    .line 443
    add-int/2addr v3, v2

    .line 444
    const/4 v4, 0x0

    invoke-virtual {p2, v1, v4, v2}, Ljava/io/FileOutputStream;->write([BII)V

    .line 445
    iget v4, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mMovedCnt:I

    invoke-direct {p0, v4, v3}, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->updateProgress(II)V

    goto :goto_0

    .line 447
    :cond_0
    return-void
.end method

.method private ensureFolder(Ljava/lang/String;)V
    .locals 2
    .param p1, "folderPath"    # Ljava/lang/String;

    .prologue
    .line 277
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 278
    .local v0, "folder":Ljava/io/File;
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 279
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 281
    :cond_0
    return-void
.end method

.method private getCursorForMovingItems(Landroid/content/Context;[J)Landroid/database/Cursor;
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "list"    # [J

    .prologue
    const/4 v4, 0x0

    .line 229
    const/4 v0, 0x4

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "_id"

    aput-object v1, v2, v0

    const/4 v0, 0x1

    const-string v1, "_data"

    aput-object v1, v2, v0

    const/4 v0, 0x2

    const-string v1, "_display_name"

    aput-object v1, v2, v0

    const/4 v0, 0x3

    const-string v1, "bucket_id"

    aput-object v1, v2, v0

    .line 234
    .local v2, "cols":[Ljava/lang/String;
    const-string v0, "_id"

    invoke-static {v0, p2}, Lcom/samsung/musicplus/util/UiUtils;->convertAudioIdsToSelection(Ljava/lang/String;[J)Ljava/lang/String;

    move-result-object v3

    .line 235
    .local v3, "where":Ljava/lang/String;
    iget-object v0, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/samsung/musicplus/provider/MusicContents;->MUSIC_CONTENTS_URI:Landroid/net/Uri;

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 237
    .local v6, "c":Landroid/database/Cursor;
    return-object v6
.end method

.method private getDestFreeSpace(Ljava/lang/String;)J
    .locals 4
    .param p1, "target"    # Ljava/lang/String;

    .prologue
    .line 588
    const-wide/16 v0, 0x0

    .line 590
    .local v0, "freeSize":J
    if-eqz p1, :cond_0

    const-string v2, ""

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 591
    :cond_0
    const-string v2, "PersonalFileOperation"

    const-string v3, "getDestFreeSpace(), target has no info"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 592
    const-wide/16 v2, 0x0

    .line 597
    :goto_0
    return-wide v2

    .line 595
    :cond_1
    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mContext:Landroid/content/Context;

    invoke-direct {p0, v3}, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->getDestRootPath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->getFreeSpace()J

    move-result-wide v0

    move-wide v2, v0

    .line 597
    goto :goto_0
.end method

.method private getDestRootPath(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 93
    const/4 v0, 0x0

    .line 94
    .local v0, "path":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->isMovingCmd()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 95
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->getSecretBoxPath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 103
    :goto_0
    return-object v0

    .line 97
    :cond_0
    iget-object v1, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mSelectionDestPath:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 98
    sget-object v0, Lcom/samsung/musicplus/library/storage/PrivateMode;->RESTORE_MUSIC_PATH:Ljava/lang/String;

    goto :goto_0

    .line 100
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mSelectionDestPath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private getFolderInfo(Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 14
    .param p1, "bucketId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/musicplus/contents/PersonalFileOperationThread$FolderItem;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 618
    const/4 v0, 0x4

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "_id"

    aput-object v1, v2, v0

    const/4 v0, 0x1

    const-string v1, "_data"

    aput-object v1, v2, v0

    const/4 v0, 0x2

    const-string v1, "_display_name"

    aput-object v1, v2, v0

    const/4 v0, 0x3

    const-string v1, "bucket_display_name"

    aput-object v1, v2, v0

    .line 622
    .local v2, "cols":[Ljava/lang/String;
    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    .line 623
    .local v13, "where":Ljava/lang/StringBuilder;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "bucket_id IN ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v13, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 624
    iget-object v0, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mContentResolver:Landroid/content/ContentResolver;

    sget-object v1, Lcom/samsung/musicplus/provider/MusicContents;->MUSIC_CONTENTS_URI:Landroid/net/Uri;

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v9

    .line 627
    .local v9, "c":Landroid/database/Cursor;
    if-nez v9, :cond_0

    .line 647
    :goto_0
    return-object v4

    .line 631
    :cond_0
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 633
    .local v11, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/musicplus/contents/PersonalFileOperationThread$FolderItem;>;"
    :try_start_0
    invoke-interface {v9}, Landroid/database/Cursor;->getCount()I

    move-result v12

    .line 634
    .local v12, "size":I
    const/4 v10, 0x0

    .local v10, "i":I
    :goto_1
    if-ge v10, v12, :cond_1

    .line 635
    invoke-interface {v9, v10}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 636
    new-instance v3, Lcom/samsung/musicplus/contents/PersonalFileOperationThread$FolderItem;

    const-string v0, "_id"

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    const-string v0, "_data"

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const-string v0, "_display_name"

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    const-string v0, "bucket_display_name"

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v9, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-direct/range {v3 .. v8}, Lcom/samsung/musicplus/contents/PersonalFileOperationThread$FolderItem;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v11, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 634
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    .line 642
    :cond_1
    if-eqz v9, :cond_2

    .line 643
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_2
    move-object v4, v11

    .line 647
    goto :goto_0

    .line 642
    .end local v10    # "i":I
    .end local v12    # "size":I
    :catchall_0
    move-exception v0

    if-eqz v9, :cond_3

    .line 643
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method private getSecretBoxPath(Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 89
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1}, Lcom/samsung/musicplus/library/storage/PrivateMode;->getRootDir(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getUriWithAudioId(J)Landroid/net/Uri;
    .locals 5
    .param p1, "audioId"    # J

    .prologue
    .line 703
    const/4 v0, 0x0

    .line 704
    .local v0, "uri":Landroid/net/Uri;
    const-wide/16 v2, -0x1

    cmp-long v1, p1, v2

    if-lez v1, :cond_0

    .line 705
    sget-object v1, Lcom/samsung/musicplus/provider/MusicContents;->MUSIC_CONTENTS_URI:Landroid/net/Uri;

    invoke-static {v1, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 707
    :cond_0
    return-object v0
.end method

.method private hasNowPlayingSong([J)Z
    .locals 6
    .param p1, "audioIds"    # [J

    .prologue
    .line 749
    array-length v3, p1

    .line 750
    .local v3, "length":I
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getCurrentAudioId()J

    move-result-wide v0

    .line 751
    .local v0, "audioId":J
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v3, :cond_1

    .line 752
    aget-wide v4, p1, v2

    cmp-long v4, v0, v4

    if-nez v4, :cond_0

    .line 753
    const/4 v4, 0x1

    .line 757
    :goto_1
    return v4

    .line 751
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 757
    :cond_1
    const/4 v4, 0x0

    goto :goto_1
.end method

.method private isPersonalModeOnAndMounted(Landroid/content/Context;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 498
    invoke-static {}, Lcom/samsung/musicplus/library/storage/PrivateMode;->isPrivateMode()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, Lcom/samsung/musicplus/library/storage/PrivateMode;->isPrivateDirMounted(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/musicplus/library/storage/PrivateMode;->isReadyToMove(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isStopMoving()Z
    .locals 3

    .prologue
    .line 571
    const-string v0, "PersonalFileOperation"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isStopMoving is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mStopped:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 572
    iget-boolean v0, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mStopped:Z

    return v0
.end method

.method private moveFiles([J)V
    .locals 5
    .param p1, "list"    # [J

    .prologue
    const/4 v4, 0x0

    .line 173
    const-string v2, "PersonalFileOperation"

    const-string v3, "moveFiles() start"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->isStopMoving()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 226
    :cond_0
    :goto_0
    return-void

    .line 178
    :cond_1
    iget-object v2, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mContext:Landroid/content/Context;

    invoke-direct {p0, v2, p1}, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->getCursorForMovingItems(Landroid/content/Context;[J)Landroid/database/Cursor;

    move-result-object v0

    .line 179
    .local v0, "c":Landroid/database/Cursor;
    if-nez v0, :cond_2

    .line 180
    const-string v2, "PersonalFileOperation"

    const-string v3, "moveFiles, cursor is null"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 184
    :cond_2
    iget v2, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mMovedCnt:I

    invoke-direct {p0, v2, v4}, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->updateProgress(II)V

    .line 190
    :try_start_0
    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    .line 191
    const-string v2, "PersonalFileOperation"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "c.getcount() : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 193
    :goto_1
    invoke-interface {v0}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v2

    if-nez v2, :cond_3

    .line 194
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->isStopMoving()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_4

    .line 222
    :cond_3
    if-eqz v0, :cond_0

    .line 223
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 197
    :cond_4
    :try_start_1
    iget-object v2, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mContext:Landroid/content/Context;

    invoke-direct {p0, v2}, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->isPersonalModeOnAndMounted(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 198
    const-string v2, "PersonalFileOperation"

    const-string v3, "At this time, Secret mode off or Secretdir unmounted"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 222
    if-eqz v0, :cond_0

    .line 223
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 202
    :cond_5
    :try_start_2
    const-string v2, "_data"

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 203
    .local v1, "srcPath":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mContext:Landroid/content/Context;

    invoke-direct {p0, v2, v1}, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->checkInvalidFilePath(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 204
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    .line 205
    iget v2, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mPersonalItems:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mPersonalItems:I

    .line 206
    iget v2, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mMovedCnt:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mMovedCnt:I

    const/4 v3, 0x0

    invoke-direct {p0, v2, v3}, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->updateProgress(II)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 222
    .end local v1    # "srcPath":Ljava/lang/String;
    :catchall_0
    move-exception v2

    if-eqz v0, :cond_6

    .line 223
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    :cond_6
    throw v2

    .line 212
    .restart local v1    # "srcPath":Ljava/lang/String;
    :cond_7
    const/4 v2, 0x0

    :try_start_3
    iput-boolean v2, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mRenameCancelled:Z

    .line 214
    iget-boolean v2, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mIsFolder:Z

    if-eqz v2, :cond_8

    .line 215
    invoke-direct {p0, v0}, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->doMoveFolderOperation(Landroid/database/Cursor;)V

    .line 219
    :goto_2
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_1

    .line 217
    :cond_8
    invoke-direct {p0, v0, v1}, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->doFileMoveOperation(Landroid/database/Cursor;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2
.end method

.method private removeFromCurrentPlaylist(Landroid/database/Cursor;)V
    .locals 8
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 720
    if-nez p1, :cond_1

    .line 746
    :cond_0
    return-void

    .line 723
    :cond_1
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 724
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 726
    .local v3, "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v5

    if-nez v5, :cond_5

    .line 727
    iget-boolean v5, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mIsFolder:Z

    if-eqz v5, :cond_4

    .line 728
    const-string v5, "bucket_id"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 729
    .local v0, "bucketId":Ljava/lang/String;
    invoke-direct {p0, v0}, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->getFolderInfo(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 730
    .local v1, "folderItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/musicplus/contents/PersonalFileOperationThread$FolderItem;>;"
    if-eqz v1, :cond_2

    .line 731
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 732
    .local v4, "size":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v4, :cond_3

    .line 733
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/musicplus/contents/PersonalFileOperationThread$FolderItem;

    iget-wide v6, v5, Lcom/samsung/musicplus/contents/PersonalFileOperationThread$FolderItem;->id:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 732
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 736
    .end local v2    # "i":I
    .end local v4    # "size":I
    :cond_2
    const-string v5, "PersonalFileOperation"

    const-string v6, "removeFromCurrentPlaylist, folderItems is null!"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 741
    .end local v0    # "bucketId":Ljava/lang/String;
    .end local v1    # "folderItems":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/musicplus/contents/PersonalFileOperationThread$FolderItem;>;"
    :cond_3
    :goto_2
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_0

    .line 739
    :cond_4
    const-string v5, "_id"

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 743
    :cond_5
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_3
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v2, v5, :cond_0

    .line 744
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-static {v6, v7}, Lcom/samsung/musicplus/util/ServiceUtils;->removeTrack(J)I

    .line 743
    add-int/lit8 v2, v2, 0x1

    goto :goto_3
.end method

.method private scanExternalStorage(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 486
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MEDIA_SCAN"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "file://"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 488
    return-void
.end method

.method private startRename(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 455
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 456
    .local v1, "renamedFile":Ljava/io/File;
    const/4 v0, 0x0

    .line 457
    .local v0, "i":I
    :goto_0
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 458
    new-instance v1, Ljava/io/File;

    .end local v1    # "renamedFile":Ljava/io/File;
    invoke-direct {p0, p1, v0}, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->addPostfix(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 459
    .restart local v1    # "renamedFile":Ljava/io/File;
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 461
    :cond_0
    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method private updatePartialDBField(JLjava/lang/String;Z)V
    .locals 9
    .param p1, "id"    # J
    .param p3, "target"    # Ljava/lang/String;
    .param p4, "success"    # Z

    .prologue
    const/4 v5, 0x2

    const/4 v7, 0x0

    .line 660
    if-eqz p4, :cond_0

    .line 661
    invoke-direct {p0, p1, p2}, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->getUriWithAudioId(J)Landroid/net/Uri;

    move-result-object v2

    .line 662
    .local v2, "uri":Landroid/net/Uri;
    iget-boolean v4, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mIsPrivateOn:Z

    if-eqz v4, :cond_2

    .line 664
    iget-object v4, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/samsung/android/privatemode/PrivateModeManager;->getPrivateStorageDir(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 665
    .local v1, "privatePath":Ljava/lang/String;
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3, v5}, Landroid/content/ContentValues;-><init>(I)V

    .line 666
    .local v3, "values":Landroid/content/ContentValues;
    const-string v4, "_data"

    invoke-virtual {v3, v4, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 667
    const-string v5, "is_secretbox"

    invoke-virtual {p3, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v4, 0x1

    :goto_0
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 669
    const-string v4, "PersonalFileOperation"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "updatePartialDBField() mode on , uri : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", target path : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 672
    :try_start_0
    iget-object v4, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mContentResolver:Landroid/content/ContentResolver;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v4, v2, v3, v5, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteConstraintException; {:try_start_0 .. :try_end_0} :catch_0

    .line 694
    .end local v1    # "privatePath":Ljava/lang/String;
    .end local v2    # "uri":Landroid/net/Uri;
    .end local v3    # "values":Landroid/content/ContentValues;
    :cond_0
    :goto_1
    return-void

    .line 667
    .restart local v1    # "privatePath":Ljava/lang/String;
    .restart local v2    # "uri":Landroid/net/Uri;
    .restart local v3    # "values":Landroid/content/ContentValues;
    :cond_1
    const/4 v4, 0x0

    goto :goto_0

    .line 673
    :catch_0
    move-exception v0

    .line 674
    .local v0, "e":Landroid/database/sqlite/SQLiteConstraintException;
    const-string v4, "PersonalFileOperation"

    const-string v5, "Update failed, just waiting from Media scan update"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 675
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteConstraintException;->printStackTrace()V

    goto :goto_1

    .line 679
    .end local v0    # "e":Landroid/database/sqlite/SQLiteConstraintException;
    .end local v1    # "privatePath":Ljava/lang/String;
    .end local v3    # "values":Landroid/content/ContentValues;
    :cond_2
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3, v5}, Landroid/content/ContentValues;-><init>(I)V

    .line 680
    .restart local v3    # "values":Landroid/content/ContentValues;
    const-string v4, "_id"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 681
    const-string v4, "_data"

    invoke-virtual {v3, v4, p3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 682
    const-string v4, "PersonalFileOperation"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "updatePartialDBField() mode off , id : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", target path : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 685
    :try_start_1
    iget-object v4, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mContentResolver:Landroid/content/ContentResolver;

    const-string v5, "content://media/external/playlists/personal/cache/save"

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v4, v5, v3}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    .line 691
    :goto_2
    iget-object v4, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mContentResolver:Landroid/content/ContentResolver;

    invoke-virtual {v4, v2, v7, v7}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_1

    .line 688
    :catch_1
    move-exception v0

    .line 689
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v4, "PersonalFileOperation"

    const-string v5, "Moving item caching failed."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method private updateProgress(II)V
    .locals 3
    .param p1, "count"    # I
    .param p2, "progress"    # I

    .prologue
    .line 482
    iget-object v0, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-static {v1, v2, p1, p2}, Landroid/os/Message;->obtain(Landroid/os/Handler;III)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 483
    return-void
.end method


# virtual methods
.method public doCancel(Z)V
    .locals 4
    .param p1, "allAppliedCheck"    # Z

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 544
    iput-boolean p1, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mAllAppliedCheck:Z

    .line 546
    iget-boolean v1, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mAllAppliedCheck:Z

    if-eqz v1, :cond_0

    .line 547
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->doStopMove()V

    .line 548
    iput-boolean v2, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mDoRename:Z

    .line 549
    iput-boolean v3, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mRenameCancelled:Z

    .line 550
    iput-boolean v2, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mDoReplace:Z

    .line 557
    :goto_0
    monitor-enter p0

    .line 559
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 563
    :goto_1
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 564
    return-void

    .line 552
    :cond_0
    iput-boolean v2, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mDoRename:Z

    .line 553
    iput-boolean v3, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mRenameCancelled:Z

    .line 554
    iput-boolean v2, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mDoReplace:Z

    goto :goto_0

    .line 560
    :catch_0
    move-exception v0

    .line 561
    .local v0, "e":Ljava/lang/Exception;
    :try_start_2
    const-string v1, "PersonalFileOperation"

    const-string v2, "doCancel() Exception!!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 563
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method public doRename(Z)V
    .locals 3
    .param p1, "allAppliedCheck"    # Z

    .prologue
    const/4 v2, 0x0

    .line 514
    iput-boolean p1, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mAllAppliedCheck:Z

    .line 515
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mDoRename:Z

    .line 516
    iput-boolean v2, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mRenameCancelled:Z

    .line 517
    iput-boolean v2, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mDoReplace:Z

    .line 519
    monitor-enter p0

    .line 521
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 525
    :goto_0
    :try_start_1
    monitor-exit p0

    .line 526
    return-void

    .line 522
    :catch_0
    move-exception v0

    .line 523
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "PersonalFileOperation"

    const-string v2, "doRename() Exception!!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 525
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public doReplace(Z)V
    .locals 3
    .param p1, "allAppliedCheck"    # Z

    .prologue
    const/4 v2, 0x0

    .line 529
    iput-boolean p1, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mAllAppliedCheck:Z

    .line 530
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mDoReplace:Z

    .line 531
    iput-boolean v2, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mDoRename:Z

    .line 532
    iput-boolean v2, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mRenameCancelled:Z

    .line 534
    monitor-enter p0

    .line 536
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 540
    :goto_0
    :try_start_1
    monitor-exit p0

    .line 541
    return-void

    .line 537
    :catch_0
    move-exception v0

    .line 538
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "PersonalFileOperation"

    const-string v2, "doReplace() Exception!!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 540
    .end local v0    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method

.method public doStopMove()V
    .locals 1

    .prologue
    .line 567
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mStopped:Z

    .line 568
    return-void
.end method

.method public getAlreadyExistPersonalCount()I
    .locals 1

    .prologue
    .line 576
    iget v0, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mPersonalItems:I

    return v0
.end method

.method public getSelectionDestPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 584
    iget-object v0, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mSelectionDestPath:Ljava/lang/String;

    return-object v0
.end method

.method public init(Landroid/os/Handler;[JZZLjava/lang/String;Z)V
    .locals 0
    .param p1, "handler"    # Landroid/os/Handler;
    .param p2, "list"    # [J
    .param p3, "isMoveCmd"    # Z
    .param p4, "isFolder"    # Z
    .param p5, "selectionDestPath"    # Ljava/lang/String;
    .param p6, "removeFromNowPlaying"    # Z

    .prologue
    .line 114
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->doReset()V

    .line 116
    iput-object p1, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mHandler:Landroid/os/Handler;

    .line 117
    iput-object p2, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mList:[J

    .line 118
    iput-boolean p3, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mIsMoveCmd:Z

    .line 119
    iput-boolean p4, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mIsFolder:Z

    .line 120
    iput-object p5, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mSelectionDestPath:Ljava/lang/String;

    .line 121
    iput-boolean p6, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mRemoveFromNowPlaying:Z

    .line 123
    return-void
.end method

.method public isMovingCmd()Z
    .locals 1

    .prologue
    .line 580
    iget-boolean v0, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mIsMoveCmd:Z

    return v0
.end method

.method public run()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 152
    :try_start_0
    iget-object v0, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mList:[J

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->moveFiles([J)V

    .line 153
    iget-boolean v0, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mRemoveFromNowPlaying:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mList:[J

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->hasNowPlayingSong([J)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 155
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mList:[J

    invoke-static {v0}, Lcom/samsung/musicplus/util/ServiceUtils;->removeTracks([J)I

    .line 157
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mContext:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->scanExternalStorage(Landroid/content/Context;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 159
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->isStopMoving()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 160
    iget-object v0, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mHandler:Landroid/os/Handler;

    iget v2, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mSuccessMovedCnt:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v4, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 166
    :goto_0
    return-void

    .line 162
    :cond_2
    iget-object v0, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mHandler:Landroid/os/Handler;

    iget v2, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mSuccessMovedCnt:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v5, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 159
    :catchall_0
    move-exception v0

    invoke-direct {p0}, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->isStopMoving()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 160
    iget-object v1, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mHandler:Landroid/os/Handler;

    iget v3, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mSuccessMovedCnt:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v4, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 162
    :goto_1
    throw v0

    :cond_3
    iget-object v1, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mHandler:Landroid/os/Handler;

    iget v3, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mSuccessMovedCnt:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v5, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_1
.end method

.method public setPrivateModeType(Z)V
    .locals 0
    .param p1, "privateOn"    # Z

    .prologue
    .line 699
    iput-boolean p1, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->mIsPrivateOn:Z

    .line 700
    return-void
.end method
