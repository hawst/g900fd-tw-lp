.class public Lcom/samsung/musicplus/contents/dlna/DlnaTabAsyncUpdater;
.super Lcom/samsung/musicplus/widget/content/ContentAsyncQueryHandler;
.source "DlnaTabAsyncUpdater.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/contents/dlna/DlnaTabAsyncUpdater$OnDlnaTabListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "MusicTab"


# instance fields
.field private mTabListener:Lcom/samsung/musicplus/contents/dlna/DlnaTabAsyncUpdater$OnDlnaTabListener;


# direct methods
.method public constructor <init>(Landroid/content/ContentResolver;Lcom/samsung/musicplus/contents/dlna/DlnaTabAsyncUpdater$OnDlnaTabListener;)V
    .locals 6
    .param p1, "res"    # Landroid/content/ContentResolver;
    .param p2, "listener"    # Lcom/samsung/musicplus/contents/dlna/DlnaTabAsyncUpdater$OnDlnaTabListener;

    .prologue
    const/4 v4, 0x0

    .line 37
    sget-object v2, Lcom/samsung/musicplus/contents/dlna/DlnaStore$Server;->CONTENT_URI:Landroid/net/Uri;

    const/4 v0, 0x1

    new-array v3, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "count(*)"

    aput-object v1, v3, v0

    move-object v0, p0

    move-object v1, p1

    move-object v5, v4

    invoke-direct/range {v0 .. v5}, Lcom/samsung/musicplus/widget/content/ContentAsyncQueryHandler;-><init>(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    .line 40
    iput-object p2, p0, Lcom/samsung/musicplus/contents/dlna/DlnaTabAsyncUpdater;->mTabListener:Lcom/samsung/musicplus/contents/dlna/DlnaTabAsyncUpdater$OnDlnaTabListener;

    .line 41
    return-void
.end method


# virtual methods
.method protected onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 4
    .param p1, "token"    # I
    .param p2, "cookie"    # Ljava/lang/Object;
    .param p3, "c"    # Landroid/database/Cursor;

    .prologue
    .line 45
    iget-object v1, p0, Lcom/samsung/musicplus/contents/dlna/DlnaTabAsyncUpdater;->mTabListener:Lcom/samsung/musicplus/contents/dlna/DlnaTabAsyncUpdater$OnDlnaTabListener;

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/samsung/musicplus/contents/dlna/DlnaTabAsyncUpdater;->mObserverRegistered:Z

    if-nez v1, :cond_2

    .line 47
    :cond_0
    if-eqz p3, :cond_1

    .line 48
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    .line 66
    :cond_1
    :goto_0
    return-void

    .line 53
    :cond_2
    const/4 v0, 0x0

    .line 54
    .local v0, "size":I
    if-eqz p3, :cond_3

    invoke-interface {p3}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 55
    const/4 v1, 0x0

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 57
    :cond_3
    const-string v1, "MusicTab"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DlnaTabAsyncUpdater onQueryComplete cursor : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " size : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    if-eqz p3, :cond_4

    .line 59
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    .line 61
    :cond_4
    if-lez v0, :cond_5

    .line 62
    iget-object v1, p0, Lcom/samsung/musicplus/contents/dlna/DlnaTabAsyncUpdater;->mTabListener:Lcom/samsung/musicplus/contents/dlna/DlnaTabAsyncUpdater$OnDlnaTabListener;

    invoke-interface {v1}, Lcom/samsung/musicplus/contents/dlna/DlnaTabAsyncUpdater$OnDlnaTabListener;->onAddDmsTab()V

    goto :goto_0

    .line 64
    :cond_5
    iget-object v1, p0, Lcom/samsung/musicplus/contents/dlna/DlnaTabAsyncUpdater;->mTabListener:Lcom/samsung/musicplus/contents/dlna/DlnaTabAsyncUpdater$OnDlnaTabListener;

    invoke-interface {v1}, Lcom/samsung/musicplus/contents/dlna/DlnaTabAsyncUpdater$OnDlnaTabListener;->onRemoveDmsTab()V

    goto :goto_0
.end method
