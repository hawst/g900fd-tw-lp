.class Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment$DmsTrackAdapter;
.super Lcom/samsung/musicplus/widget/list/TrackListAdapter;
.source "DlnaDmsListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DmsTrackAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;


# direct methods
.method public constructor <init>(Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;Landroid/content/Context;ILandroid/database/Cursor;I)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "layout"    # I
    .param p4, "c"    # Landroid/database/Cursor;
    .param p5, "flags"    # I

    .prologue
    .line 332
    iput-object p1, p0, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment$DmsTrackAdapter;->this$0:Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;

    .line 333
    invoke-direct {p0, p2, p3, p4, p5}, Lcom/samsung/musicplus/widget/list/TrackListAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;I)V

    .line 334
    return-void
.end method


# virtual methods
.method protected bindTextView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "c"    # Landroid/database/Cursor;

    .prologue
    .line 338
    iget-object v5, p0, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment$DmsTrackAdapter;->this$0:Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;

    # getter for: Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->mSearchView:Landroid/widget/SearchView;
    invoke-static {v5}, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->access$500(Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;)Landroid/widget/SearchView;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 339
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;

    .line 340
    .local v4, "vh":Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;
    iget-object v2, v4, Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;->text1:Landroid/widget/TextView;

    check-cast v2, Lcom/samsung/musicplus/widget/MatchedTextView;

    .line 341
    .local v2, "tv1":Lcom/samsung/musicplus/widget/MatchedTextView;
    iget-object v3, v4, Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;->text2:Landroid/widget/TextView;

    check-cast v3, Lcom/samsung/musicplus/widget/MatchedTextView;

    .line 342
    .local v3, "tv2":Lcom/samsung/musicplus/widget/MatchedTextView;
    iget v5, p0, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment$DmsTrackAdapter;->mText1Index:I

    invoke-interface {p3, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {p2, v5}, Lcom/samsung/musicplus/util/UiUtils;->transUnknownString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 343
    .local v0, "text1":Ljava/lang/String;
    invoke-virtual {v2}, Lcom/samsung/musicplus/widget/MatchedTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment$DmsTrackAdapter;->this$0:Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;

    # getter for: Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->mQueryText:Ljava/lang/String;
    invoke-static {v6}, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->access$600(Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v5, v0, v6}, Lcom/samsung/musicplus/util/UiUtils;->setTextItem(Lcom/samsung/musicplus/widget/MatchedTextView;Landroid/text/TextPaint;Ljava/lang/String;Ljava/lang/String;)V

    .line 344
    if-eqz v3, :cond_0

    iget v5, p0, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment$DmsTrackAdapter;->mText2Index:I

    if-ltz v5, :cond_0

    .line 345
    iget v5, p0, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment$DmsTrackAdapter;->mText2Index:I

    invoke-interface {p3, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {p2, v5}, Lcom/samsung/musicplus/util/UiUtils;->transUnknownString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 346
    .local v1, "text2":Ljava/lang/String;
    invoke-virtual {v3}, Lcom/samsung/musicplus/widget/MatchedTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment$DmsTrackAdapter;->this$0:Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;

    # getter for: Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->mQueryText:Ljava/lang/String;
    invoke-static {v6}, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;->access$600(Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v5, v1, v6}, Lcom/samsung/musicplus/util/UiUtils;->setTextItem(Lcom/samsung/musicplus/widget/MatchedTextView;Landroid/text/TextPaint;Ljava/lang/String;Ljava/lang/String;)V

    .line 351
    .end local v0    # "text1":Ljava/lang/String;
    .end local v1    # "text2":Ljava/lang/String;
    .end local v2    # "tv1":Lcom/samsung/musicplus/widget/MatchedTextView;
    .end local v3    # "tv2":Lcom/samsung/musicplus/widget/MatchedTextView;
    .end local v4    # "vh":Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;
    :cond_0
    :goto_0
    return-void

    .line 349
    :cond_1
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->bindTextView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    goto :goto_0
.end method

.method protected getArtworkInBackground(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/Object;I)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "iv"    # Landroid/widget/ImageView;
    .param p3, "artKey"    # Ljava/lang/Object;
    .param p4, "resId"    # I

    .prologue
    const v6, 0x7f02003b

    .line 361
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0138

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 366
    .local v5, "size":I
    invoke-virtual {p2, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 369
    invoke-static {}, Lcom/samsung/musicplus/util/AlbumArtLoader;->getAlbumArtLoader()Lcom/samsung/musicplus/util/IAlbumArtLoader;

    move-result-object v0

    sget-object v2, Lcom/samsung/musicplus/util/AlbumArtUtils;->DLNA_ARTWORK_URI_STRING:Ljava/lang/String;

    move-object v1, p1

    move-object v3, p3

    move-object v4, p2

    invoke-interface/range {v0 .. v6}, Lcom/samsung/musicplus/util/IAlbumArtLoader;->loadArtwork(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Object;Landroid/widget/ImageView;II)V

    .line 372
    return-void
.end method

.method protected getCachedArtwork(Ljava/lang/Object;)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p1, "artKey"    # Ljava/lang/Object;

    .prologue
    .line 355
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/musicplus/util/AlbumArtUtils;->getCachedNetworkAlbumArt(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method
