.class Lcom/samsung/musicplus/contents/devices/DeviceListFragment$1;
.super Landroid/content/BroadcastReceiver;
.source "DeviceListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/contents/devices/DeviceListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/contents/devices/DeviceListFragment;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/contents/devices/DeviceListFragment;)V
    .locals 0

    .prologue
    .line 248
    iput-object p1, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$1;->this$0:Lcom/samsung/musicplus/contents/devices/DeviceListFragment;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 252
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 253
    .local v0, "action":Ljava/lang/String;
    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 254
    invoke-static {p1}, Lcom/samsung/musicplus/util/UiUtils;->isWifiConnected(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 255
    const-string v1, "MusicUiDeviceTab"

    const-string v2, "Network connect success"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 256
    iget-object v1, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$1;->this$0:Lcom/samsung/musicplus/contents/devices/DeviceListFragment;

    # getter for: Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mWifiConnected:Z
    invoke-static {v1}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->access$000(Lcom/samsung/musicplus/contents/devices/DeviceListFragment;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 257
    iget-object v1, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$1;->this$0:Lcom/samsung/musicplus/contents/devices/DeviceListFragment;

    const/4 v2, 0x1

    # setter for: Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mWifiConnected:Z
    invoke-static {v1, v2}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->access$002(Lcom/samsung/musicplus/contents/devices/DeviceListFragment;Z)Z

    .line 258
    iget-object v1, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$1;->this$0:Lcom/samsung/musicplus/contents/devices/DeviceListFragment;

    invoke-virtual {v1}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->initLoader()V

    .line 273
    :cond_0
    :goto_0
    return-void

    .line 261
    :cond_1
    const-string v1, "MusicUiDeviceTab"

    const-string v2, "Network connect fail"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 262
    iget-object v1, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$1;->this$0:Lcom/samsung/musicplus/contents/devices/DeviceListFragment;

    # getter for: Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mWifiConnected:Z
    invoke-static {v1}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->access$000(Lcom/samsung/musicplus/contents/devices/DeviceListFragment;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 263
    iget-object v1, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$1;->this$0:Lcom/samsung/musicplus/contents/devices/DeviceListFragment;

    const/4 v2, 0x0

    # setter for: Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mWifiConnected:Z
    invoke-static {v1, v2}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->access$002(Lcom/samsung/musicplus/contents/devices/DeviceListFragment;Z)Z

    .line 264
    iget-object v1, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$1;->this$0:Lcom/samsung/musicplus/contents/devices/DeviceListFragment;

    invoke-virtual {v1}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->initLoader()V

    goto :goto_0

    .line 267
    :cond_2
    sget-object v1, Lcom/samsung/musicplus/provider/MusicContents$Audio$DeviceContents;->ACTION_S_LINK_SIGNIN_CHANGED:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 268
    iget-object v1, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$1;->this$0:Lcom/samsung/musicplus/contents/devices/DeviceListFragment;

    invoke-static {p1}, Lcom/samsung/musicplus/provider/MusicContents$Audio$Devices;->isSingedIn(Landroid/content/Context;)Z

    move-result v2

    # setter for: Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mIsLogin:Z
    invoke-static {v1, v2}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->access$102(Lcom/samsung/musicplus/contents/devices/DeviceListFragment;Z)Z

    .line 269
    iget-object v1, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$1;->this$0:Lcom/samsung/musicplus/contents/devices/DeviceListFragment;

    invoke-static {p1}, Lcom/samsung/musicplus/provider/MusicContents$Audio$Devices;->hasSamsungAccount(Landroid/content/Context;)Z

    move-result v2

    # setter for: Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mHasSamsungAccount:Z
    invoke-static {v1, v2}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->access$202(Lcom/samsung/musicplus/contents/devices/DeviceListFragment;Z)Z

    .line 270
    const-string v1, "MusicUiDeviceTab"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "S link signin state changed : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$1;->this$0:Lcom/samsung/musicplus/contents/devices/DeviceListFragment;

    # getter for: Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mIsLogin:Z
    invoke-static {v3}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->access$100(Lcom/samsung/musicplus/contents/devices/DeviceListFragment;)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " mHasSamsungAccount : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$1;->this$0:Lcom/samsung/musicplus/contents/devices/DeviceListFragment;

    # getter for: Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mHasSamsungAccount:Z
    invoke-static {v3}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->access$200(Lcom/samsung/musicplus/contents/devices/DeviceListFragment;)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
