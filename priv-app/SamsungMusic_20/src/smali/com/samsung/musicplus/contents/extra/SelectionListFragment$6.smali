.class Lcom/samsung/musicplus/contents/extra/SelectionListFragment$6;
.super Ljava/lang/Object;
.source "SelectionListFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/contents/extra/SelectionListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final MSG_TIMER_EXPIRED:I

.field private mIsHeaderButtonClicked:Z

.field private mTimerHandler:Landroid/os/Handler;

.field final synthetic this$0:Lcom/samsung/musicplus/contents/extra/SelectionListFragment;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/contents/extra/SelectionListFragment;)V
    .locals 1

    .prologue
    .line 597
    iput-object p1, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$6;->this$0:Lcom/samsung/musicplus/contents/extra/SelectionListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 598
    const/16 v0, 0x3e8

    iput v0, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$6;->MSG_TIMER_EXPIRED:I

    .line 600
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$6;->mIsHeaderButtonClicked:Z

    .line 639
    new-instance v0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$6$1;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$6$1;-><init>(Lcom/samsung/musicplus/contents/extra/SelectionListFragment$6;)V

    iput-object v0, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$6;->mTimerHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$802(Lcom/samsung/musicplus/contents/extra/SelectionListFragment$6;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/extra/SelectionListFragment$6;
    .param p1, "x1"    # Z

    .prologue
    .line 597
    iput-boolean p1, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$6;->mIsHeaderButtonClicked:Z

    return p1
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 604
    iget-object v3, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$6;->this$0:Lcom/samsung/musicplus/contents/extra/SelectionListFragment;

    # getter for: Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mIsPause:Z
    invoke-static {v3}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->access$400(Lcom/samsung/musicplus/contents/extra/SelectionListFragment;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 637
    :cond_0
    :goto_0
    return-void

    .line 607
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    .line 608
    .local v2, "id":I
    iget-object v3, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$6;->this$0:Lcom/samsung/musicplus/contents/extra/SelectionListFragment;

    invoke-virtual {v3}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 609
    .local v0, "a":Landroid/app/Activity;
    sparse-switch v2, :sswitch_data_0

    goto :goto_0

    .line 623
    :sswitch_0
    iget-boolean v3, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$6;->mIsHeaderButtonClicked:Z

    if-nez v3, :cond_0

    .line 624
    iput-boolean v6, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$6;->mIsHeaderButtonClicked:Z

    .line 625
    iget-object v3, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$6;->mTimerHandler:Landroid/os/Handler;

    const/16 v4, 0x3e8

    const-wide/16 v6, 0x12c

    invoke-virtual {v3, v4, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 626
    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$6;->this$0:Lcom/samsung/musicplus/contents/extra/SelectionListFragment;

    # getter for: Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mListType:I
    invoke-static {v4}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->access$600(Lcom/samsung/musicplus/contents/extra/SelectionListFragment;)I

    move-result v4

    iget-object v5, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$6;->this$0:Lcom/samsung/musicplus/contents/extra/SelectionListFragment;

    invoke-virtual {v5}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$6;->this$0:Lcom/samsung/musicplus/contents/extra/SelectionListFragment;

    # getter for: Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mSelectedListId:[J
    invoke-static {v6}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->access$700(Lcom/samsung/musicplus/contents/extra/SelectionListFragment;)[J

    move-result-object v6

    invoke-static {v3, v4, v5, v6}, Lcom/samsung/musicplus/util/FileOperationTask$AddToPlaylistTask;->makePlaylist(Landroid/content/Context;ILandroid/app/FragmentManager;[J)V

    goto :goto_0

    .line 612
    :sswitch_1
    const v3, 0x7f0d00ab

    invoke-virtual {v0, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    .line 613
    .local v1, "checkBox":Landroid/widget/CheckBox;
    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 614
    iget-object v3, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$6;->this$0:Lcom/samsung/musicplus/contents/extra/SelectionListFragment;

    invoke-virtual {v3, v7}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->setSelectAll(Z)V

    .line 619
    :goto_1
    iget-object v3, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$6;->this$0:Lcom/samsung/musicplus/contents/extra/SelectionListFragment;

    # invokes: Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->invalidateActionBarTitle()V
    invoke-static {v3}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->access$200(Lcom/samsung/musicplus/contents/extra/SelectionListFragment;)V

    .line 620
    const v3, 0x8000

    invoke-virtual {p1, v3}, Landroid/view/View;->sendAccessibilityEvent(I)V

    goto :goto_0

    .line 616
    :cond_2
    iget-object v3, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$6;->this$0:Lcom/samsung/musicplus/contents/extra/SelectionListFragment;

    invoke-virtual {v3, v6}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->setSelectAll(Z)V

    .line 617
    iget-object v3, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$6;->this$0:Lcom/samsung/musicplus/contents/extra/SelectionListFragment;

    # getter for: Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mDoneItem:Landroid/view/MenuItem;
    invoke-static {v3}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->access$500(Lcom/samsung/musicplus/contents/extra/SelectionListFragment;)Landroid/view/MenuItem;

    move-result-object v3

    invoke-interface {v3, v6}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    goto :goto_1

    .line 631
    .end local v1    # "checkBox":Landroid/widget/CheckBox;
    :sswitch_2
    new-instance v3, Lcom/samsung/musicplus/util/FileOperationTask$AddToNowPlayingTask;

    iget-object v4, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$6;->this$0:Lcom/samsung/musicplus/contents/extra/SelectionListFragment;

    invoke-virtual {v4}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$6;->this$0:Lcom/samsung/musicplus/contents/extra/SelectionListFragment;

    # getter for: Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mSelectedListId:[J
    invoke-static {v5}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->access$700(Lcom/samsung/musicplus/contents/extra/SelectionListFragment;)[J

    move-result-object v5

    invoke-direct {v3, v4, v5, v6}, Lcom/samsung/musicplus/util/FileOperationTask$AddToNowPlayingTask;-><init>(Landroid/app/Activity;[JZ)V

    new-array v4, v7, [Ljava/lang/Void;

    invoke-virtual {v3, v4}, Lcom/samsung/musicplus/util/FileOperationTask$AddToNowPlayingTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 632
    invoke-static {}, Lcom/samsung/musicplus/widget/fragment/ActionModeRemoteHandler;->getInstance()Lcom/samsung/musicplus/widget/fragment/ActionModeRemoteHandler;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/musicplus/widget/fragment/ActionModeRemoteHandler;->notifyActionModeFinishAction()V

    goto/16 :goto_0

    .line 609
    :sswitch_data_0
    .sparse-switch
        0x7f0d0093 -> :sswitch_0
        0x7f0d00a8 -> :sswitch_2
        0x7f0d00aa -> :sswitch_1
    .end sparse-switch
.end method
