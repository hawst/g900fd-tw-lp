.class public Lcom/samsung/musicplus/contents/normal/RecordingsListFragment;
.super Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;
.source "RecordingsListFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/contents/normal/RecordingsListFragment$RecordingsTrackAdapter;
    }
.end annotation


# static fields
.field private static final PREF_KEY_RECORDINGS_VIEW_TYPE:Ljava/lang/String; = "recordings_view_type"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;-><init>()V

    .line 70
    return-void
.end method


# virtual methods
.method protected addOtherView(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 0
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 67
    return-object p1
.end method

.method protected createAdapter()Landroid/widget/CursorAdapter;
    .locals 7

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 27
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/normal/RecordingsListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    .line 28
    .local v6, "a":Landroid/app/Activity;
    iget v0, p0, Lcom/samsung/musicplus/contents/normal/RecordingsListFragment;->mList:I

    invoke-static {v0}, Lcom/samsung/musicplus/util/ListUtils;->isGridType(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 29
    new-instance v0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;

    const v1, 0x7f040029

    invoke-direct {v0, v6, v1, v4, v5}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;I)V

    .line 35
    :goto_0
    return-object v0

    .line 31
    :cond_0
    iget v0, p0, Lcom/samsung/musicplus/contents/normal/RecordingsListFragment;->mList:I

    invoke-static {v0}, Lcom/samsung/musicplus/util/ListUtils;->isTrack(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 32
    new-instance v0, Lcom/samsung/musicplus/contents/normal/RecordingsListFragment$RecordingsTrackAdapter;

    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/normal/RecordingsListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const v3, 0x7f040033

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/samsung/musicplus/contents/normal/RecordingsListFragment$RecordingsTrackAdapter;-><init>(Lcom/samsung/musicplus/contents/normal/RecordingsListFragment;Landroid/content/Context;ILandroid/database/Cursor;I)V

    goto :goto_0

    .line 35
    :cond_1
    invoke-super {p0}, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;->createAdapter()Landroid/widget/CursorAdapter;

    move-result-object v0

    goto :goto_0
.end method

.method protected getPrefKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    const-string v0, "recordings_view_type"

    return-object v0
.end method

.method protected isEnableListShuffle(I)Z
    .locals 1
    .param p1, "list"    # I

    .prologue
    .line 62
    const/4 v0, 0x0

    return v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 46
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 47
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/normal/RecordingsListFragment;->getView()Landroid/view/View;

    move-result-object v1

    .line 48
    .local v1, "rootView":Landroid/view/View;
    const v2, 0x7f0d0084

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 49
    .local v0, "headerView":Landroid/view/View;
    instance-of v2, v0, Landroid/view/ViewStub;

    if-eqz v2, :cond_0

    .line 50
    check-cast v0, Landroid/view/ViewStub;

    .end local v0    # "headerView":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 52
    :cond_0
    const v2, 0x7f0d0090

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {p0, v2}, Lcom/samsung/musicplus/contents/normal/RecordingsListFragment;->setNumberView(Landroid/widget/TextView;)V

    .line 53
    return-void
.end method

.method public onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V
    .locals 0
    .param p2, "data"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 40
    .local p1, "loader":Landroid/content/Loader;, "Landroid/content/Loader<Landroid/database/Cursor;>;"
    invoke-super {p0, p1, p2}, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;->onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V

    .line 41
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/normal/RecordingsListFragment;->updateNumberView()V

    .line 42
    return-void
.end method

.method public bridge synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Landroid/content/Loader;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 21
    check-cast p2, Landroid/database/Cursor;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/samsung/musicplus/contents/normal/RecordingsListFragment;->onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method
