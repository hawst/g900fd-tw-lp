.class Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment$DmsServerAdapter;
.super Lcom/samsung/musicplus/widget/list/CommonListAdapter;
.source "DlnaDmsListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "DmsServerAdapter"
.end annotation


# instance fields
.field private mAlbumCol:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILandroid/database/Cursor;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "layout"    # I
    .param p3, "c"    # Landroid/database/Cursor;
    .param p4, "flags"    # I

    .prologue
    .line 289
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;I)V

    .line 290
    return-void
.end method


# virtual methods
.method protected getArtKey(Landroid/database/Cursor;)Ljava/lang/Object;
    .locals 2
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 306
    iget-object v1, p0, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment$DmsServerAdapter;->mAlbumCol:Ljava/lang/String;

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 307
    .local v0, "artWorkPath":Ljava/lang/String;
    return-object v0
.end method

.method protected getArtworkInBackground(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/Object;I)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "iv"    # Landroid/widget/ImageView;
    .param p3, "artKey"    # Ljava/lang/Object;
    .param p4, "resId"    # I

    .prologue
    const v6, 0x7f02003b

    .line 318
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0138

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 323
    .local v5, "size":I
    invoke-virtual {p2, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 324
    invoke-static {}, Lcom/samsung/musicplus/util/AlbumArtLoader;->getAlbumArtLoader()Lcom/samsung/musicplus/util/IAlbumArtLoader;

    move-result-object v0

    move-object v2, p3

    check-cast v2, Ljava/lang/String;

    move-object v1, p1

    move-object v3, p3

    move-object v4, p2

    invoke-interface/range {v0 .. v6}, Lcom/samsung/musicplus/util/IAlbumArtLoader;->loadArtwork(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Object;Landroid/widget/ImageView;II)V

    .line 326
    return-void
.end method

.method protected getCachedArtwork(Ljava/lang/Object;)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p1, "artKey"    # Ljava/lang/Object;

    .prologue
    .line 312
    check-cast p1, Ljava/lang/String;

    .end local p1    # "artKey":Ljava/lang/Object;
    invoke-static {p1}, Lcom/samsung/musicplus/util/AlbumArtUtils;->getCachedNetworkAlbumArt(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method setAlbumColumn(Ljava/lang/String;)V
    .locals 0
    .param p1, "column"    # Ljava/lang/String;

    .prologue
    .line 301
    iput-object p1, p0, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment$DmsServerAdapter;->mAlbumCol:Ljava/lang/String;

    .line 302
    return-void
.end method

.method protected setArtWorkView(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/Object;I)Z
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "iv"    # Landroid/widget/ImageView;
    .param p3, "artKey"    # Ljava/lang/Object;
    .param p4, "resId"    # I

    .prologue
    .line 294
    const/4 v1, 0x0

    invoke-virtual {p2, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 296
    const v1, 0x7f020123

    invoke-super {p0, p1, p2, p3, v1}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->setArtWorkView(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/Object;I)Z

    move-result v0

    .line 297
    .local v0, "isCached":Z
    return v0
.end method
