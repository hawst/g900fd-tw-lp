.class public Lcom/samsung/musicplus/contents/normal/ComposerListFragment;
.super Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;
.source "ComposerListFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/contents/normal/ComposerListFragment$ComposerTabAdapter;,
        Lcom/samsung/musicplus/contents/normal/ComposerListFragment$ComposerListTrackAdapter;
    }
.end annotation


# static fields
.field private static final PREF_KEY_COMPOSER_VIEW_TYPE:Ljava/lang/String; = "composer_view_type"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;-><init>()V

    .line 60
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/contents/normal/ComposerListFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/normal/ComposerListFragment;

    .prologue
    .line 17
    iget v0, p0, Lcom/samsung/musicplus/contents/normal/ComposerListFragment;->mList:I

    return v0
.end method

.method static synthetic access$100(Lcom/samsung/musicplus/contents/normal/ComposerListFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/normal/ComposerListFragment;

    .prologue
    .line 17
    iget v0, p0, Lcom/samsung/musicplus/contents/normal/ComposerListFragment;->mList:I

    return v0
.end method


# virtual methods
.method protected createAdapter()Landroid/widget/CursorAdapter;
    .locals 12

    .prologue
    const v3, 0x7f040033

    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 23
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/normal/ComposerListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    .line 24
    .local v2, "a":Landroid/app/Activity;
    iget v0, p0, Lcom/samsung/musicplus/contents/normal/ComposerListFragment;->mList:I

    invoke-static {v0}, Lcom/samsung/musicplus/util/ListUtils;->isGridType(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 25
    new-instance v0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;

    const v1, 0x7f040029

    invoke-direct {v0, v2, v1, v4, v5}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;I)V

    .line 34
    :goto_0
    return-object v0

    .line 27
    :cond_0
    iget v0, p0, Lcom/samsung/musicplus/contents/normal/ComposerListFragment;->mList:I

    invoke-static {v0}, Lcom/samsung/musicplus/util/ListUtils;->isTab(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 28
    new-instance v0, Lcom/samsung/musicplus/contents/normal/ComposerListFragment$ComposerTabAdapter;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/samsung/musicplus/contents/normal/ComposerListFragment$ComposerTabAdapter;-><init>(Lcom/samsung/musicplus/contents/normal/ComposerListFragment;Landroid/content/Context;ILandroid/database/Cursor;I)V

    goto :goto_0

    .line 30
    :cond_1
    iget v0, p0, Lcom/samsung/musicplus/contents/normal/ComposerListFragment;->mList:I

    invoke-static {v0}, Lcom/samsung/musicplus/util/ListUtils;->isTrack(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 31
    new-instance v6, Lcom/samsung/musicplus/contents/normal/ComposerListFragment$ComposerListTrackAdapter;

    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/normal/ComposerListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v8

    move-object v7, p0

    move v9, v3

    move-object v10, v4

    move v11, v5

    invoke-direct/range {v6 .. v11}, Lcom/samsung/musicplus/contents/normal/ComposerListFragment$ComposerListTrackAdapter;-><init>(Lcom/samsung/musicplus/contents/normal/ComposerListFragment;Landroid/content/Context;ILandroid/database/Cursor;I)V

    move-object v0, v6

    goto :goto_0

    .line 34
    :cond_2
    invoke-super {p0}, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;->createAdapter()Landroid/widget/CursorAdapter;

    move-result-object v0

    goto :goto_0
.end method

.method protected getPrefKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 98
    const-string v0, "composer_view_type"

    return-object v0
.end method

.method public onChangeViewType(I)V
    .locals 0
    .param p1, "type"    # I

    .prologue
    .line 44
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;->onChangeViewType(I)V

    .line 45
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/normal/ComposerListFragment;->setHeaderView()V

    .line 46
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/normal/ComposerListFragment;->updateNumberView()V

    .line 47
    return-void
.end method

.method public onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V
    .locals 1
    .param p2, "data"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 89
    .local p1, "loader":Landroid/content/Loader;, "Landroid/content/Loader<Landroid/database/Cursor;>;"
    invoke-super {p0, p1, p2}, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;->onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V

    .line 90
    iget v0, p0, Lcom/samsung/musicplus/contents/normal/ComposerListFragment;->mList:I

    invoke-static {v0}, Lcom/samsung/musicplus/util/ListUtils;->isTrack(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 91
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/normal/ComposerListFragment;->isSplitSubListFragment()Z

    move-result v0

    if-nez v0, :cond_0

    .line 92
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/normal/ComposerListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 94
    :cond_0
    return-void
.end method

.method public bridge synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Landroid/content/Loader;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 17
    check-cast p2, Landroid/database/Cursor;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/samsung/musicplus/contents/normal/ComposerListFragment;->onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 38
    invoke-super {p0}, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;->onResume()V

    .line 39
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/normal/ComposerListFragment;->updateNumberView()V

    .line 40
    return-void
.end method
