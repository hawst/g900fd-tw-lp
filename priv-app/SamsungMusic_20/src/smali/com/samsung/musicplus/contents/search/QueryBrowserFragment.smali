.class public Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;
.super Lcom/samsung/musicplus/widget/fragment/CommonListFragment;
.source "QueryBrowserFragment.java"

# interfaces
.implements Lcom/samsung/musicplus/contents/search/QueryBrowserActivity$QueryTextChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/contents/search/QueryBrowserFragment$ContentsChangeHandler;,
        Lcom/samsung/musicplus/contents/search/QueryBrowserFragment$QueryBrowserAdapter;
    }
.end annotation


# static fields
.field private static final MIME_TYPE_ALBUM:I = 0x2

.field private static final MIME_TYPE_ARTIST:I = 0x1

.field private static final MIME_TYPE_NOT_SUPPORT:I = -0x1

.field private static final MIME_TYPE_SONG:I = 0x3

.field protected static final RESTART_LOADER:I = 0xb

.field private static final TAG:Ljava/lang/String; = "MusicList"

.field private static sSavedMimeTypeIndex:Landroid/util/SparseIntArray;


# instance fields
.field private mId:J

.field private mIsContentChanged:Z

.field private mIsQueyTextChanged:Z

.field private mIsStart:Z

.field private mIsUnknown:Z

.field mLoaderHandler:Landroid/os/Handler;

.field private mObserver:Lcom/samsung/musicplus/widget/content/ContentAsyncQueryHandler$ContentChangeObserver;

.field private mQueryText:Ljava/lang/String;

.field private mSearchView:Landroid/widget/SearchView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 204
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    sput-object v0, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->sSavedMimeTypeIndex:Landroid/util/SparseIntArray;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 62
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;-><init>()V

    .line 82
    iput-boolean v0, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->mIsUnknown:Z

    .line 90
    iput-boolean v0, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->mIsContentChanged:Z

    .line 195
    new-instance v0, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment$1;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment$1;-><init>(Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;)V

    iput-object v0, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->mLoaderHandler:Landroid/os/Handler;

    .line 572
    return-void
.end method

.method static synthetic access$100(Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->restartLoader()V

    return-void
.end method

.method static synthetic access$1000(Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;Ljava/lang/String;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->getMimeTypeNum(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method static synthetic access$1100(Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->mIsStart:Z

    return v0
.end method

.method static synthetic access$1202(Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 62
    iput-boolean p1, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->mIsContentChanged:Z

    return p1
.end method

.method static synthetic access$300(Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;
    .param p1, "x1"    # Landroid/database/Cursor;

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->getMimeType(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;Landroid/content/Context;Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # Landroid/database/Cursor;

    .prologue
    .line 62
    invoke-direct {p0, p1, p2}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->getArtistName(Landroid/content/Context;Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->mIsUnknown:Z

    return v0
.end method

.method static synthetic access$600(Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;Landroid/content/Context;Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # Landroid/database/Cursor;

    .prologue
    .line 62
    invoke-direct {p0, p1, p2}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->getAlbumName(Landroid/content/Context;Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$700(Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->isTrackType(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$800(Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;Landroid/content/Context;Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # Landroid/database/Cursor;

    .prologue
    .line 62
    invoke-direct {p0, p1, p2}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->getSongName(Landroid/content/Context;Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$900()Landroid/util/SparseIntArray;
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->sSavedMimeTypeIndex:Landroid/util/SparseIntArray;

    return-object v0
.end method

.method private getAlbumName(Landroid/content/Context;Landroid/database/Cursor;)Ljava/lang/String;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "c"    # Landroid/database/Cursor;

    .prologue
    .line 413
    const-string v2, "album"

    invoke-interface {p2, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p2, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 414
    .local v1, "name":Ljava/lang/String;
    move-object v0, v1

    .line 415
    .local v0, "displayname":Ljava/lang/String;
    if-eqz v1, :cond_0

    const-string v2, "<unknown>"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 416
    :cond_0
    const v2, 0x7f1001bc

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 418
    :cond_1
    return-object v0
.end method

.method private getAllListAndPosition(J)Lcom/samsung/musicplus/util/ListUtils$SongList;
    .locals 9
    .param p1, "newAudioId"    # J

    .prologue
    const/4 v8, 0x0

    .line 330
    const v0, 0x20001

    invoke-static {v0, v8}, Lcom/samsung/musicplus/util/ListUtils;->getMusicListInfo(ILjava/lang/String;)Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;

    move-result-object v0

    iget-object v7, v0, Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;->queryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    .line 332
    .local v7, "queryArgs":Lcom/samsung/musicplus/util/ListUtils$QueryArgs;
    const/4 v6, 0x0

    .line 334
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, v7, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->uri:Landroid/net/Uri;

    iget-object v2, v7, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->projection:[Ljava/lang/String;

    iget-object v3, v7, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->selection:Ljava/lang/String;

    iget-object v4, v7, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->selectionArgs:[Ljava/lang/String;

    iget-object v5, v7, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->orderBy:Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v6

    .line 336
    if-nez v6, :cond_1

    .line 341
    if-eqz v6, :cond_0

    .line 342
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 345
    :cond_0
    :goto_0
    return-object v8

    .line 339
    :cond_1
    :try_start_1
    invoke-static {v6, p1, p2}, Lcom/samsung/musicplus/util/ListUtils;->getListIdForCursor(Landroid/database/Cursor;J)Lcom/samsung/musicplus/util/ListUtils$SongList;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v8

    .line 341
    .local v8, "song":Lcom/samsung/musicplus/util/ListUtils$SongList;
    if-eqz v6, :cond_0

    .line 342
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 341
    .end local v8    # "song":Lcom/samsung/musicplus/util/ListUtils$SongList;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 342
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method private getArtistName(Landroid/content/Context;Landroid/database/Cursor;)Ljava/lang/String;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "c"    # Landroid/database/Cursor;

    .prologue
    .line 402
    const-string v2, "artist"

    invoke-interface {p2, v2}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p2, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 403
    .local v1, "name":Ljava/lang/String;
    move-object v0, v1

    .line 404
    .local v0, "displayname":Ljava/lang/String;
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->mIsUnknown:Z

    .line 405
    if-eqz v1, :cond_0

    const-string v2, "<unknown>"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 406
    :cond_0
    const v2, 0x7f1001bd

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 407
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->mIsUnknown:Z

    .line 409
    :cond_1
    return-object v0
.end method

.method private getMimeType(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 2
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 376
    const-string v1, "mime_type"

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 377
    .local v0, "mimeType":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 378
    const-string v0, "audio/"

    .line 380
    :cond_0
    return-object v0
.end method

.method private getMimeTypeNum(Ljava/lang/String;)I
    .locals 1
    .param p1, "mimeType"    # Ljava/lang/String;

    .prologue
    .line 391
    const-string v0, "artist"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 392
    const/4 v0, 0x1

    .line 398
    :goto_0
    return v0

    .line 393
    :cond_0
    const-string v0, "album"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 394
    const/4 v0, 0x2

    goto :goto_0

    .line 395
    :cond_1
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->isTrackType(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 396
    const/4 v0, 0x3

    goto :goto_0

    .line 398
    :cond_2
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static getNewInstance(Ljava/lang/String;)Landroid/app/Fragment;
    .locals 4
    .param p0, "key"    # Ljava/lang/String;

    .prologue
    .line 93
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 94
    .local v0, "data":Landroid/os/Bundle;
    const-string v2, "list"

    const v3, 0x20024

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 95
    const-string v2, "key"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    new-instance v1, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;

    invoke-direct {v1}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;-><init>()V

    .line 97
    .local v1, "fg":Landroid/app/Fragment;
    invoke-virtual {v1, v0}, Landroid/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 98
    return-object v1
.end method

.method private getSongName(Landroid/content/Context;Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "c"    # Landroid/database/Cursor;

    .prologue
    .line 422
    const-string v0, "title"

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getTitleString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "c"    # Landroid/database/Cursor;
    .param p2, "mimeType"    # Ljava/lang/String;

    .prologue
    .line 360
    const-string v1, ""

    .line 361
    .local v1, "title":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 363
    .local v0, "context":Landroid/content/Context;
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 364
    const-string v2, "artist"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 365
    invoke-direct {p0, v0, p1}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->getArtistName(Landroid/content/Context;Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    .line 372
    :cond_0
    :goto_0
    return-object v1

    .line 366
    :cond_1
    const-string v2, "album"

    invoke-virtual {p2, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 367
    invoke-direct {p0, v0, p1}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->getAlbumName(Landroid/content/Context;Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 369
    :cond_2
    invoke-direct {p0, v0, p1}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->getSongName(Landroid/content/Context;Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private getTrackListType(Ljava/lang/String;)I
    .locals 1
    .param p1, "mimeType"    # Ljava/lang/String;

    .prologue
    .line 283
    const-string v0, "artist"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 284
    const v0, 0x20003

    .line 286
    :goto_0
    return v0

    :cond_0
    const v0, 0x20002

    goto :goto_0
.end method

.method private isTrackType(Ljava/lang/String;)Z
    .locals 1
    .param p1, "mimeType"    # Ljava/lang/String;

    .prologue
    .line 426
    const-string v0, "audio/"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "application/ogg"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "application/x-ogg"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private launchTrackActivity(Landroid/database/Cursor;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p1, "c"    # Landroid/database/Cursor;
    .param p2, "mimeType"    # Ljava/lang/String;
    .param p3, "keyWord"    # Ljava/lang/String;

    .prologue
    .line 290
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 291
    .local v0, "a":Landroid/app/Activity;
    new-instance v1, Landroid/content/Intent;

    const-class v4, Lcom/samsung/musicplus/contents/TrackActivity;

    invoke-direct {v1, v0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 292
    .local v1, "i":Landroid/content/Intent;
    invoke-direct {p0, p1, p2}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->getTitleString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 293
    .local v3, "title":Ljava/lang/String;
    invoke-direct {p0, p2}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->getTrackListType(Ljava/lang/String;)I

    move-result v2

    .line 294
    .local v2, "listType":I
    const-string v4, "list_type"

    invoke-virtual {v1, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 295
    const-string v4, "keyword"

    invoke-virtual {v1, v4, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 296
    const-string v4, "track_title"

    invoke-virtual {v1, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 297
    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->startActivity(Landroid/content/Intent;)V

    .line 298
    return-void
.end method

.method private restartLoader()V
    .locals 3

    .prologue
    .line 236
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->updateListInfo()Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;

    .line 237
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    iget v1, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->mList:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    .line 238
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->mIsContentChanged:Z

    .line 239
    return-void
.end method

.method private setEmptyTextColorForQuery()V
    .locals 4

    .prologue
    .line 176
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->getView()Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 177
    .local v1, "root":Landroid/view/ViewGroup;
    const v2, 0x7f0d0049

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 178
    .local v0, "noItemTextView":Landroid/widget/TextView;
    if-eqz v0, :cond_0

    .line 179
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0057

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 181
    :cond_0
    return-void
.end method

.method private setMimeTypeIndex(Landroid/database/Cursor;)V
    .locals 5
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 207
    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-nez v4, :cond_1

    .line 229
    :cond_0
    :goto_0
    return-void

    .line 210
    :cond_1
    sget-object v4, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->sSavedMimeTypeIndex:Landroid/util/SparseIntArray;

    invoke-virtual {v4}, Landroid/util/SparseIntArray;->clear()V

    .line 211
    const/4 v2, -0x1

    .line 212
    .local v2, "position":I
    const-string v0, ""

    .line 213
    .local v0, "mimeType":Ljava/lang/String;
    const/4 v1, -0x1

    .line 214
    .local v1, "mimeTypeNum":I
    const/4 v3, -0x1

    .line 215
    .local v3, "savedMimeType":I
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 217
    :cond_2
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->getMimeType(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    .line 218
    invoke-direct {p0, v0}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->getMimeTypeNum(Ljava/lang/String;)I

    move-result v1

    .line 219
    if-gez v1, :cond_4

    .line 227
    :cond_3
    :goto_1
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-nez v4, :cond_2

    goto :goto_0

    .line 222
    :cond_4
    if-eq v1, v3, :cond_3

    .line 223
    invoke-interface {p1}, Landroid/database/Cursor;->getPosition()I

    move-result v2

    .line 224
    sget-object v4, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->sSavedMimeTypeIndex:Landroid/util/SparseIntArray;

    invoke-virtual {v4, v2, v1}, Landroid/util/SparseIntArray;->put(II)V

    .line 225
    move v3, v1

    goto :goto_1
.end method

.method private setQuery(Landroid/widget/SearchView;Ljava/lang/String;)V
    .locals 1
    .param p1, "search"    # Landroid/widget/SearchView;
    .param p2, "query"    # Ljava/lang/String;

    .prologue
    .line 599
    if-nez p1, :cond_1

    .line 605
    :cond_0
    :goto_0
    return-void

    .line 602
    :cond_1
    if-eqz p2, :cond_0

    const-string v0, ""

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 603
    const/4 v0, 0x1

    invoke-virtual {p1, p2, v0}, Landroid/widget/SearchView;->setQuery(Ljava/lang/CharSequence;Z)V

    goto :goto_0
.end method

.method private startMusicActivity([JI)V
    .locals 8
    .param p1, "list"    # [J
    .param p2, "position"    # I

    .prologue
    .line 301
    if-nez p1, :cond_1

    .line 320
    :cond_0
    :goto_0
    return-void

    .line 304
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 305
    .local v0, "a":Landroid/app/Activity;
    new-instance v1, Landroid/content/Intent;

    const-class v5, Lcom/samsung/musicplus/player/PlayerActivity;

    invoke-direct {v1, v0, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 306
    .local v1, "i":Landroid/content/Intent;
    const/high16 v5, 0x4000000

    invoke-virtual {v1, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 307
    const/high16 v5, 0x10000000

    invoke-virtual {v1, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 309
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getCurrentAudioId()J

    move-result-wide v2

    .line 310
    .local v2, "currentId":J
    const v4, 0x20001

    .line 311
    .local v4, "listType":I
    iget-wide v6, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->mId:J

    cmp-long v5, v2, v6

    if-nez v5, :cond_2

    iget-wide v6, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->mId:J

    invoke-static {v4, v6, v7}, Lcom/samsung/musicplus/util/ServiceUtils;->isSameSong(IJ)Z

    move-result v5

    if-nez v5, :cond_3

    .line 312
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    const/4 v6, 0x0

    invoke-static {v5, v4, v6, p1, p2}, Lcom/samsung/musicplus/util/ServiceUtils;->openList(Landroid/content/Context;ILjava/lang/String;[JI)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 319
    :cond_3
    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method


# virtual methods
.method protected createAdapter()Landroid/widget/CursorAdapter;
    .locals 6

    .prologue
    .line 149
    new-instance v0, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment$QueryBrowserAdapter;

    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const v3, 0x7f04003b

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment$QueryBrowserAdapter;-><init>(Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;Landroid/content/Context;ILandroid/database/Cursor;I)V

    return-object v0
.end method

.method public getQueryText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 232
    iget-object v0, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->mQueryText:Ljava/lang/String;

    return-object v0
.end method

.method public getTitleString(I)Ljava/lang/String;
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 350
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v2

    invoke-interface {v2, p1}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 351
    .local v0, "c":Landroid/database/Cursor;
    const/4 v1, 0x0

    .line 352
    .local v1, "mimeType":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 353
    invoke-direct {p0, v0}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->getMimeType(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    .line 354
    invoke-direct {p0, v0, v1}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->getTitleString(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 356
    :goto_0
    return-object v2

    :cond_0
    const-string v2, ""

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x0

    .line 110
    iput-boolean v3, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->mIsQueyTextChanged:Z

    .line 111
    new-instance v0, Lcom/samsung/musicplus/widget/content/ContentAsyncQueryHandler$ContentChangeObserver;

    new-instance v1, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment$ContentsChangeHandler;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment$ContentsChangeHandler;-><init>(Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;Lcom/samsung/musicplus/contents/search/QueryBrowserFragment$1;)V

    invoke-direct {v0, v1}, Lcom/samsung/musicplus/widget/content/ContentAsyncQueryHandler$ContentChangeObserver;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->mObserver:Lcom/samsung/musicplus/widget/content/ContentAsyncQueryHandler$ContentChangeObserver;

    .line 112
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/musicplus/provider/MusicContents;->MUSIC_CONTENTS_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->mObserver:Lcom/samsung/musicplus/widget/content/ContentAsyncQueryHandler$ContentChangeObserver;

    invoke-virtual {v0, v1, v3, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 114
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 115
    iget-object v0, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->mSearchView:Landroid/widget/SearchView;

    if-eqz v0, :cond_0

    .line 116
    iget-object v0, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->mSearchView:Landroid/widget/SearchView;

    iget-object v1, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->mQueryText:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->setQuery(Landroid/widget/SearchView;Ljava/lang/String;)V

    .line 118
    :cond_0
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 3
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 185
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 186
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->getView()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 187
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->getView()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f0d0100

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 188
    .local v0, "backgroundView":Landroid/widget/ImageView;
    if-eqz v0, :cond_0

    .line 189
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 190
    const v1, 0x7f020119

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 193
    .end local v0    # "backgroundView":Landroid/widget/ImageView;
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 103
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 104
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->setHasOptionsMenu(Z)V

    .line 105
    iget-object v0, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->mKey:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->mQueryText:Ljava/lang/String;

    .line 106
    return-void
.end method

.method public onCreateContextMenu(Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 0
    .param p1, "menu"    # Landroid/view/ContextMenu;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "menuInfo"    # Landroid/view/ContextMenu$ContextMenuInfo;

    .prologue
    .line 280
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 137
    invoke-super {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->onDestroy()V

    .line 138
    iget-object v0, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->mObserver:Lcom/samsung/musicplus/widget/content/ContentAsyncQueryHandler$ContentChangeObserver;

    if-eqz v0, :cond_0

    .line 139
    iget-object v0, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->mObserver:Lcom/samsung/musicplus/widget/content/ContentAsyncQueryHandler$ContentChangeObserver;

    invoke-virtual {v0}, Lcom/samsung/musicplus/widget/content/ContentAsyncQueryHandler$ContentChangeObserver;->cancel()V

    .line 140
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->mObserver:Lcom/samsung/musicplus/widget/content/ContentAsyncQueryHandler$ContentChangeObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 142
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->mLoaderHandler:Landroid/os/Handler;

    if-eqz v0, :cond_1

    .line 143
    iget-object v0, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->mLoaderHandler:Landroid/os/Handler;

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 145
    :cond_1
    return-void
.end method

.method public onListItemClick(Landroid/widget/AbsListView;Landroid/view/View;IJ)V
    .locals 8
    .param p1, "l"    # Landroid/widget/AbsListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    .line 243
    iput-wide p4, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->mId:J

    .line 244
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v5

    invoke-interface {v5, p3}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 247
    .local v0, "c":Landroid/database/Cursor;
    if-eqz v0, :cond_0

    .line 248
    invoke-direct {p0, v0}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->getMimeType(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v3

    .line 249
    .local v3, "mimeType":Ljava/lang/String;
    const-string v5, "artist"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 253
    const-string v1, "_id"

    .line 255
    .local v1, "columnName":Ljava/lang/String;
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 256
    .local v2, "keyWord":Ljava/lang/String;
    invoke-direct {p0, v0, v3, v2}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->launchTrackActivity(Landroid/database/Cursor;Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    .end local v1    # "columnName":Ljava/lang/String;
    .end local v2    # "keyWord":Ljava/lang/String;
    .end local v3    # "mimeType":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 257
    .restart local v3    # "mimeType":Ljava/lang/String;
    :cond_1
    const-string v5, "album"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 261
    const-string v1, "_id"

    .line 263
    .restart local v1    # "columnName":Ljava/lang/String;
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 264
    .restart local v2    # "keyWord":Ljava/lang/String;
    invoke-direct {p0, v0, v3, v2}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->launchTrackActivity(Landroid/database/Cursor;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 266
    .end local v1    # "columnName":Ljava/lang/String;
    .end local v2    # "keyWord":Ljava/lang/String;
    :cond_2
    if-ltz p3, :cond_0

    const-wide/16 v6, 0x0

    cmp-long v5, p4, v6

    if-ltz v5, :cond_0

    .line 267
    invoke-direct {p0, p4, p5}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->getAllListAndPosition(J)Lcom/samsung/musicplus/util/ListUtils$SongList;

    move-result-object v4

    .line 268
    .local v4, "song":Lcom/samsung/musicplus/util/ListUtils$SongList;
    if-eqz v4, :cond_0

    .line 269
    iget-object v5, v4, Lcom/samsung/musicplus/util/ListUtils$SongList;->list:[J

    iget v6, v4, Lcom/samsung/musicplus/util/ListUtils$SongList;->position:I

    invoke-direct {p0, v5, v6}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->startMusicActivity([JI)V

    goto :goto_0
.end method

.method public onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V
    .locals 3
    .param p2, "data"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 155
    .local p1, "loader":Landroid/content/Loader;, "Landroid/content/Loader<Landroid/database/Cursor;>;"
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    .line 158
    .local v0, "lv":Landroid/widget/ListView;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 159
    iget v1, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->mList:I

    invoke-virtual {p1}, Landroid/content/Loader;->getId()I

    move-result v2

    if-ne v1, v2, :cond_1

    .line 160
    iget-object v1, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->mAdapter:Landroid/widget/CursorAdapter;

    invoke-virtual {v1, p2}, Landroid/widget/CursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 161
    invoke-direct {p0, p2}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->setMimeTypeIndex(Landroid/database/Cursor;)V

    .line 162
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->setListShown(Z)V

    .line 163
    iget-object v1, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->mAdapter:Landroid/widget/CursorAdapter;

    invoke-virtual {v1}, Landroid/widget/CursorAdapter;->getCount()I

    move-result v1

    if-nez v1, :cond_1

    .line 164
    iget-object v1, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->mQueryText:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->mQueryText:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    iget-boolean v1, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->mIsQueyTextChanged:Z

    if-nez v1, :cond_2

    .line 165
    const v1, 0x7f100106

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->setEmptyView(I)V

    .line 170
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->setEmptyViewNoBackground()V

    .line 173
    :cond_1
    return-void

    .line 167
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget v2, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->mNoItemTextId:I

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->setEmptyViewTextOnly(Ljava/lang/CharSequence;)V

    .line 168
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->setEmptyTextColorForQuery()V

    goto :goto_0
.end method

.method public bridge synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Landroid/content/Loader;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 62
    check-cast p2, Landroid/database/Cursor;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onQueryTextChanged(Ljava/lang/String;)V
    .locals 2
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 586
    iput-object p1, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->mQueryText:Ljava/lang/String;

    .line 587
    iget-object v0, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->mQueryText:Ljava/lang/String;

    iput-object v0, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->mKey:Ljava/lang/String;

    .line 588
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->mIsQueyTextChanged:Z

    .line 589
    iget-object v0, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->mLoaderHandler:Landroid/os/Handler;

    const/16 v1, 0xb

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 590
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 122
    invoke-super {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->onStart()V

    .line 123
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->mIsStart:Z

    .line 124
    iget-boolean v0, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->mIsContentChanged:Z

    if-eqz v0, :cond_0

    .line 125
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->restartLoader()V

    .line 127
    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 131
    invoke-super {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->onStop()V

    .line 132
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->mIsStart:Z

    .line 133
    return-void
.end method

.method public setSearchView(Landroid/widget/SearchView;)V
    .locals 1
    .param p1, "view"    # Landroid/widget/SearchView;

    .prologue
    .line 594
    iput-object p1, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->mSearchView:Landroid/widget/SearchView;

    .line 595
    iget-object v0, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->mQueryText:Ljava/lang/String;

    invoke-direct {p0, p1, v0}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->setQuery(Landroid/widget/SearchView;Ljava/lang/String;)V

    .line 596
    return-void
.end method
