.class public Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;
.super Ljava/lang/Object;
.source "MoveSecretBoxOperation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$OnMoveSecretBoxListener;,
        Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$OperationState;
    }
.end annotation


# static fields
.field private static final CLASSNAME:Ljava/lang/String; = "PersonalFileOperation"

.field private static sMoveSecretBoxOperation:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;


# instance fields
.field private mContext:Landroid/content/Context;

.field private final mHandler:Landroid/os/Handler;

.field private mMoveFilesThread:Lcom/samsung/musicplus/contents/PersonalFileOperationThread;

.field private mOnMoveSecretBoxListener:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$OnMoveSecretBoxListener;

.field mPmClient:Lcom/samsung/android/privatemode/IPrivateModeClient;

.field mPmMgr:Lcom/samsung/android/privatemode/PrivateModeManager;

.field mToken:Landroid/os/IBinder;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 127
    iput-object v0, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mToken:Landroid/os/IBinder;

    .line 129
    iput-object v0, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mPmMgr:Lcom/samsung/android/privatemode/PrivateModeManager;

    .line 131
    new-instance v0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$1;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$1;-><init>(Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;)V

    iput-object v0, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mPmClient:Lcom/samsung/android/privatemode/IPrivateModeClient;

    .line 172
    new-instance v0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$2;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$2;-><init>(Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;)V

    iput-object v0, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mHandler:Landroid/os/Handler;

    .line 89
    iput-object p1, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mContext:Landroid/content/Context;

    .line 90
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;)Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$OnMoveSecretBoxListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mOnMoveSecretBoxListener:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$OnMoveSecretBoxListener;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;)Lcom/samsung/musicplus/contents/PersonalFileOperationThread;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mMoveFilesThread:Lcom/samsung/musicplus/contents/PersonalFileOperationThread;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;

    .prologue
    .line 40
    iget-object v0, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 81
    sget-object v0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->sMoveSecretBoxOperation:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;

    if-nez v0, :cond_0

    .line 82
    new-instance v0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->sMoveSecretBoxOperation:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;

    .line 85
    :cond_0
    sget-object v0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->sMoveSecretBoxOperation:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;

    return-object v0
.end method


# virtual methods
.method public fileRename(Z)V
    .locals 1
    .param p1, "allAppliedCheck"    # Z

    .prologue
    .line 339
    iget-object v0, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mMoveFilesThread:Lcom/samsung/musicplus/contents/PersonalFileOperationThread;

    if-eqz v0, :cond_0

    .line 340
    iget-object v0, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mMoveFilesThread:Lcom/samsung/musicplus/contents/PersonalFileOperationThread;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->doRename(Z)V

    .line 342
    :cond_0
    return-void
.end method

.method public fileRenameCancel(Z)V
    .locals 1
    .param p1, "allAppliedCheck"    # Z

    .prologue
    .line 351
    iget-object v0, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mMoveFilesThread:Lcom/samsung/musicplus/contents/PersonalFileOperationThread;

    if-eqz v0, :cond_0

    .line 352
    iget-object v0, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mMoveFilesThread:Lcom/samsung/musicplus/contents/PersonalFileOperationThread;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->doCancel(Z)V

    .line 354
    :cond_0
    return-void
.end method

.method public fileReplace(Z)V
    .locals 1
    .param p1, "allAppliedCheck"    # Z

    .prologue
    .line 345
    iget-object v0, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mMoveFilesThread:Lcom/samsung/musicplus/contents/PersonalFileOperationThread;

    if-eqz v0, :cond_0

    .line 346
    iget-object v0, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mMoveFilesThread:Lcom/samsung/musicplus/contents/PersonalFileOperationThread;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->doReplace(Z)V

    .line 348
    :cond_0
    return-void
.end method

.method public getTotalCount([J)I
    .locals 16
    .param p1, "list"    # [J

    .prologue
    .line 302
    const/4 v14, 0x0

    .line 303
    .local v14, "cnt":I
    const/4 v1, 0x2

    new-array v3, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v3, v1

    const/4 v1, 0x1

    const-string v2, "bucket_id"

    aput-object v2, v3, v1

    .line 306
    .local v3, "cols":[Ljava/lang/String;
    const-string v1, "_id"

    move-object/from16 v0, p1

    invoke-static {v1, v0}, Lcom/samsung/musicplus/util/UiUtils;->convertAudioIdsToSelection(Ljava/lang/String;[J)Ljava/lang/String;

    move-result-object v4

    .line 307
    .local v4, "where":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/samsung/musicplus/provider/MusicContents;->MUSIC_CONTENTS_URI:Landroid/net/Uri;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v12

    .line 310
    .local v12, "c":Landroid/database/Cursor;
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    .line 311
    .local v15, "where2":Ljava/lang/StringBuilder;
    if-eqz v12, :cond_2

    .line 312
    invoke-interface {v12}, Landroid/database/Cursor;->moveToFirst()Z

    .line 313
    const-string v1, "bucket_id IN ("

    invoke-virtual {v15, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 314
    :goto_0
    invoke-interface {v12}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v1

    if-nez v1, :cond_1

    .line 315
    const-string v1, "bucket_id"

    invoke-interface {v12, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v12, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 316
    .local v11, "bucketId":Ljava/lang/String;
    invoke-virtual {v15, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 317
    invoke-interface {v12}, Landroid/database/Cursor;->isLast()Z

    move-result v1

    if-nez v1, :cond_0

    .line 318
    const-string v1, ","

    invoke-virtual {v15, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 320
    :cond_0
    invoke-interface {v12}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_0

    .line 322
    .end local v11    # "bucketId":Ljava/lang/String;
    :cond_1
    const-string v1, ")"

    invoke-virtual {v15, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 323
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 326
    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    sget-object v6, Lcom/samsung/musicplus/provider/MusicContents;->MUSIC_CONTENTS_URI:Landroid/net/Uri;

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object v7, v3

    invoke-virtual/range {v5 .. v10}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 328
    .local v13, "c2":Landroid/database/Cursor;
    if-eqz v13, :cond_3

    .line 329
    invoke-interface {v13}, Landroid/database/Cursor;->getCount()I

    move-result v14

    .line 330
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 333
    :cond_3
    const-string v1, "PersonalFileOperation"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "getTotalCount() count : "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 335
    return v14
.end method

.method public isMoveFilesThreadWorking()Z
    .locals 2

    .prologue
    .line 119
    iget-object v0, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mMoveFilesThread:Lcom/samsung/musicplus/contents/PersonalFileOperationThread;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mMoveFilesThread:Lcom/samsung/musicplus/contents/PersonalFileOperationThread;

    invoke-virtual {v0}, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->getState()Ljava/lang/Thread$State;

    move-result-object v0

    sget-object v1, Ljava/lang/Thread$State;->TERMINATED:Ljava/lang/Thread$State;

    if-eq v0, v1, :cond_0

    .line 120
    const-string v0, "PersonalFileOperation"

    const-string v1, "isThreadWorking() true"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    const/4 v0, 0x1

    .line 124
    :goto_0
    return v0

    .line 123
    :cond_0
    const-string v0, "PersonalFileOperation"

    const-string v1, "isThreadWorking() false"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setOnMoveSecretBoxListener(Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$OnMoveSecretBoxListener;)V
    .locals 0
    .param p1, "I"    # Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$OnMoveSecretBoxListener;

    .prologue
    .line 77
    iput-object p1, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mOnMoveSecretBoxListener:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$OnMoveSecretBoxListener;

    .line 78
    return-void
.end method

.method public startToMoveFiles([JZZLjava/lang/String;Z)Z
    .locals 9
    .param p1, "list"    # [J
    .param p2, "isMoveCmd"    # Z
    .param p3, "isFolder"    # Z
    .param p4, "selectionDestPath"    # Ljava/lang/String;
    .param p5, "removeFromNowPlaying"    # Z

    .prologue
    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 95
    const-string v0, "PersonalFileOperation"

    const-string v1, "startToMoveFiles()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    if-eqz p1, :cond_0

    array-length v0, p1

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->isMoveFilesThreadWorking()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v8

    .line 115
    :goto_0
    return v0

    .line 101
    :cond_1
    new-instance v0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;

    iget-object v1, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mMoveFilesThread:Lcom/samsung/musicplus/contents/PersonalFileOperationThread;

    .line 103
    iget-object v0, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mMoveFilesThread:Lcom/samsung/musicplus/contents/PersonalFileOperationThread;

    iget-object v1, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mHandler:Landroid/os/Handler;

    move-object v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    move v6, p5

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->init(Landroid/os/Handler;[JZZLjava/lang/String;Z)V

    .line 105
    invoke-static {}, Lcom/samsung/musicplus/library/storage/PrivateMode;->isPrivateMode()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 106
    const-string v0, "PersonalFileOperation"

    const-string v1, "startToMoveFiles() isSecretMode : true"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    iget-object v0, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mMoveFilesThread:Lcom/samsung/musicplus/contents/PersonalFileOperationThread;

    invoke-virtual {v0, v7}, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->setPrivateModeType(Z)V

    .line 108
    iget-object v0, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mMoveFilesThread:Lcom/samsung/musicplus/contents/PersonalFileOperationThread;

    invoke-virtual {v0}, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->start()V

    :goto_1
    move v0, v7

    .line 115
    goto :goto_0

    .line 110
    :cond_2
    const-string v0, "PersonalFileOperation"

    const-string v1, "startToMoveFiles() isSecretMode : false"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    iget-object v0, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mMoveFilesThread:Lcom/samsung/musicplus/contents/PersonalFileOperationThread;

    invoke-virtual {v0, v8}, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->setPrivateModeType(Z)V

    .line 112
    iget-object v0, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mPmClient:Lcom/samsung/android/privatemode/IPrivateModeClient;

    invoke-static {v0, v1}, Lcom/samsung/android/privatemode/PrivateModeManager;->getInstance(Landroid/content/Context;Lcom/samsung/android/privatemode/IPrivateModeClient;)Lcom/samsung/android/privatemode/PrivateModeManager;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mPmMgr:Lcom/samsung/android/privatemode/PrivateModeManager;

    goto :goto_1
.end method

.method public stopMove()V
    .locals 3

    .prologue
    .line 159
    iget-object v0, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mMoveFilesThread:Lcom/samsung/musicplus/contents/PersonalFileOperationThread;

    if-eqz v0, :cond_0

    .line 160
    iget-object v0, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mMoveFilesThread:Lcom/samsung/musicplus/contents/PersonalFileOperationThread;

    invoke-virtual {v0}, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->getState()Ljava/lang/Thread$State;

    move-result-object v0

    sget-object v1, Ljava/lang/Thread$State;->NEW:Ljava/lang/Thread$State;

    if-ne v0, v1, :cond_2

    .line 161
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mMoveFilesThread:Lcom/samsung/musicplus/contents/PersonalFileOperationThread;

    .line 166
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mPmMgr:Lcom/samsung/android/privatemode/PrivateModeManager;

    if-eqz v0, :cond_1

    .line 167
    const-string v0, "PersonalFileOperation"

    const-string v1, "unregister PrivateModeManager"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 168
    iget-object v0, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mPmMgr:Lcom/samsung/android/privatemode/PrivateModeManager;

    iget-object v1, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mToken:Landroid/os/IBinder;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/privatemode/PrivateModeManager;->unregisterClient(Landroid/os/IBinder;Z)Z

    .line 170
    :cond_1
    return-void

    .line 163
    :cond_2
    iget-object v0, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mMoveFilesThread:Lcom/samsung/musicplus/contents/PersonalFileOperationThread;

    invoke-virtual {v0}, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->doStopMove()V

    goto :goto_0
.end method
