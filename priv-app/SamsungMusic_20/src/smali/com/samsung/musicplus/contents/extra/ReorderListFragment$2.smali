.class Lcom/samsung/musicplus/contents/extra/ReorderListFragment$2;
.super Ljava/lang/Object;
.source "ReorderListFragment.java"

# interfaces
.implements Lcom/sec/android/touchwiz/animator/TwAbsDndAnimator$TwDndController;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/contents/extra/ReorderListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/contents/extra/ReorderListFragment;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/contents/extra/ReorderListFragment;)V
    .locals 0

    .prologue
    .line 177
    iput-object p1, p0, Lcom/samsung/musicplus/contents/extra/ReorderListFragment$2;->this$0:Lcom/samsung/musicplus/contents/extra/ReorderListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public allowDrag(I)Z
    .locals 1
    .param p1, "arg0"    # I

    .prologue
    .line 196
    const/4 v0, 0x1

    return v0
.end method

.method public allowDrop(II)Z
    .locals 1
    .param p1, "arg0"    # I
    .param p2, "arg1"    # I

    .prologue
    .line 191
    const/4 v0, 0x1

    return v0
.end method

.method public dropDone(II)V
    .locals 6
    .param p1, "from"    # I
    .param p2, "to"    # I

    .prologue
    const/4 v1, 0x1

    .line 180
    if-eq p1, p2, :cond_0

    .line 181
    iget-object v0, p0, Lcom/samsung/musicplus/contents/extra/ReorderListFragment$2;->this$0:Lcom/samsung/musicplus/contents/extra/ReorderListFragment;

    # setter for: Lcom/samsung/musicplus/contents/extra/ReorderListFragment;->mSkipDrawing:Z
    invoke-static {v0, v1}, Lcom/samsung/musicplus/contents/extra/ReorderListFragment;->access$002(Lcom/samsung/musicplus/contents/extra/ReorderListFragment;Z)Z

    .line 182
    iget-object v0, p0, Lcom/samsung/musicplus/contents/extra/ReorderListFragment$2;->this$0:Lcom/samsung/musicplus/contents/extra/ReorderListFragment;

    # setter for: Lcom/samsung/musicplus/contents/extra/ReorderListFragment;->mIsReorderChanged:Z
    invoke-static {v0, v1}, Lcom/samsung/musicplus/contents/extra/ReorderListFragment;->access$102(Lcom/samsung/musicplus/contents/extra/ReorderListFragment;Z)Z

    .line 184
    iget-object v0, p0, Lcom/samsung/musicplus/contents/extra/ReorderListFragment$2;->this$0:Lcom/samsung/musicplus/contents/extra/ReorderListFragment;

    invoke-virtual {v0}, Lcom/samsung/musicplus/contents/extra/ReorderListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/musicplus/contents/extra/ReorderListFragment$2;->this$0:Lcom/samsung/musicplus/contents/extra/ReorderListFragment;

    invoke-virtual {v1}, Lcom/samsung/musicplus/contents/extra/ReorderListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-wide v2, Lcom/samsung/musicplus/util/FileOperationTask;->sReorderPlayListId:J

    move v4, p1

    move v5, p2

    invoke-static/range {v0 .. v5}, Lcom/samsung/musicplus/util/FileOperationTask;->moveItem(Landroid/content/Context;Landroid/content/ContentResolver;JII)V

    .line 187
    :cond_0
    return-void
.end method
