.class Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTrackAdapter;
.super Lcom/samsung/musicplus/contents/TrackListNumberAdapter;
.source "ArtistListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/contents/normal/ArtistListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ArtistListTrackAdapter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTrackAdapter$ArtistTrackViewHolder;
    }
.end annotation


# static fields
.field private static final NUMBER_DOT:Ljava/lang/String; = "."


# direct methods
.method public constructor <init>(Landroid/content/Context;ILandroid/database/Cursor;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "layout"    # I
    .param p3, "c"    # Landroid/database/Cursor;
    .param p4, "flags"    # I

    .prologue
    .line 321
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/musicplus/contents/TrackListNumberAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;I)V

    .line 322
    return-void
.end method


# virtual methods
.method protected bindOtherView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "c"    # Landroid/database/Cursor;

    .prologue
    .line 383
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/musicplus/contents/TrackListNumberAdapter;->bindOtherView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    .line 384
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTrackAdapter$ArtistTrackViewHolder;

    .line 385
    .local v3, "vh":Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTrackAdapter$ArtistTrackViewHolder;
    iget-object v4, v3, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTrackAdapter$ArtistTrackViewHolder;->titleLayout:Landroid/view/View;

    if-nez v4, :cond_0

    .line 411
    :goto_0
    return-void

    .line 389
    :cond_0
    const/4 v2, 0x0

    .line 390
    .local v2, "savedAlbum":Ljava/lang/String;
    # getter for: Lcom/samsung/musicplus/contents/normal/ArtistListFragment;->sSavedAlbumIndex:Landroid/util/SparseArray;
    invoke-static {}, Lcom/samsung/musicplus/contents/normal/ArtistListFragment;->access$500()Landroid/util/SparseArray;

    move-result-object v4

    invoke-interface {p3}, Landroid/database/Cursor;->getPosition()I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    .line 391
    .local v1, "index":Ljava/lang/Object;
    instance-of v4, v1, Ljava/lang/String;

    if-eqz v4, :cond_1

    move-object v2, v1

    .line 393
    check-cast v2, Ljava/lang/String;

    .line 398
    :cond_1
    const-string v4, "album_id"

    invoke-interface {p3, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p3, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 402
    .local v0, "album":Ljava/lang/String;
    if-eqz v2, :cond_2

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 403
    iget-object v4, v3, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTrackAdapter$ArtistTrackViewHolder;->titleLayout:Landroid/view/View;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    .line 404
    iget-object v4, v3, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTrackAdapter$ArtistTrackViewHolder;->titleText:Landroid/widget/TextView;

    const-string v5, "album"

    invoke-interface {p3, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {p3, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 405
    iget-object v4, v3, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTrackAdapter$ArtistTrackViewHolder;->titleImage:Landroid/widget/ImageView;

    sget-object v5, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 406
    iget-object v4, v3, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTrackAdapter$ArtistTrackViewHolder;->titleImage:Landroid/widget/ImageView;

    invoke-virtual {p0, p3}, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTrackAdapter;->getArtKey(Landroid/database/Cursor;)Ljava/lang/Object;

    move-result-object v5

    const v6, 0x7f02003b

    invoke-virtual {p0, p2, v4, v5, v6}, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTrackAdapter;->setArtWorkView(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/Object;I)Z

    goto :goto_0

    .line 409
    :cond_2
    iget-object v4, v3, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTrackAdapter$ArtistTrackViewHolder;->titleLayout:Landroid/view/View;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method protected bindTextView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "c"    # Landroid/database/Cursor;

    .prologue
    .line 373
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/musicplus/contents/TrackListNumberAdapter;->bindTextView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    .line 374
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTrackAdapter$ArtistTrackViewHolder;

    .line 375
    .local v1, "vh":Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTrackAdapter$ArtistTrackViewHolder;
    iget-object v2, v1, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTrackAdapter$ArtistTrackViewHolder;->text1:Landroid/widget/TextView;

    iget v3, p0, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTrackAdapter;->mText1Index:I

    invoke-interface {p3, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 377
    # getter for: Lcom/samsung/musicplus/contents/normal/ArtistListFragment;->sSavedSongNumber:Landroid/util/SparseIntArray;
    invoke-static {}, Lcom/samsung/musicplus/contents/normal/ArtistListFragment;->access$400()Landroid/util/SparseIntArray;

    move-result-object v2

    invoke-interface {p3}, Landroid/database/Cursor;->getPosition()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/util/SparseIntArray;->get(I)I

    move-result v0

    .line 378
    .local v0, "count":I
    iget-object v2, v1, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTrackAdapter$ArtistTrackViewHolder;->number:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 379
    return-void
.end method

.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "c"    # Landroid/database/Cursor;

    .prologue
    .line 363
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/musicplus/contents/TrackListNumberAdapter;->bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    .line 364
    const v1, 0x7f0d00bb

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 365
    .local v0, "divider":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 366
    const v1, 0x7f020022

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 368
    :cond_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTrackAdapter;->hideBottomDivider(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    .line 369
    return-void
.end method

.method protected getArtworkInBackground(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/Object;I)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "iv"    # Landroid/widget/ImageView;
    .param p3, "artKey"    # Ljava/lang/Object;
    .param p4, "resId"    # I

    .prologue
    .line 416
    invoke-static {}, Lcom/samsung/musicplus/util/AlbumArtLoader;->getAlbumArtLoader()Lcom/samsung/musicplus/util/IAlbumArtLoader;

    move-result-object v0

    sget-object v2, Lcom/samsung/musicplus/util/AlbumArtUtils;->DEFAULT_ARTWORK_URI_STRING:Ljava/lang/String;

    iget v5, p0, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTrackAdapter;->mAlbumArtSize:I

    move-object v1, p1

    move-object v3, p3

    move-object v4, p2

    move v6, p4

    invoke-interface/range {v0 .. v6}, Lcom/samsung/musicplus/util/IAlbumArtLoader;->loadArtworkWithoutAnimation(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Object;Landroid/widget/ImageView;II)V

    .line 418
    return-void
.end method

.method protected getViewHolder()Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;
    .locals 2

    .prologue
    .line 358
    new-instance v0, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTrackAdapter$ArtistTrackViewHolder;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTrackAdapter$ArtistTrackViewHolder;-><init>(Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTrackAdapter;Lcom/samsung/musicplus/contents/normal/ArtistListFragment$1;)V

    return-object v0
.end method

.method protected newOtherView(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 335
    invoke-super {p0, p1}, Lcom/samsung/musicplus/contents/TrackListNumberAdapter;->newOtherView(Landroid/view/View;)V

    .line 336
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTrackAdapter$ArtistTrackViewHolder;

    .line 337
    .local v1, "vh":Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTrackAdapter$ArtistTrackViewHolder;
    const v2, 0x7f0d00c6

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, v1, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTrackAdapter$ArtistTrackViewHolder;->titleLayout:Landroid/view/View;

    .line 338
    iget-object v2, v1, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTrackAdapter$ArtistTrackViewHolder;->titleLayout:Landroid/view/View;

    new-instance v3, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTrackAdapter$1;

    invoke-direct {v3, p0}, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTrackAdapter$1;-><init>(Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTrackAdapter;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 345
    iget-object v2, v1, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTrackAdapter$ArtistTrackViewHolder;->titleLayout:Landroid/view/View;

    const v3, 0x7f0d00c8

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v1, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTrackAdapter$ArtistTrackViewHolder;->titleText:Landroid/widget/TextView;

    .line 347
    iget-object v2, v1, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTrackAdapter$ArtistTrackViewHolder;->titleLayout:Landroid/view/View;

    const v3, 0x7f0d00c7

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 348
    .local v0, "titleImage":Landroid/view/View;
    instance-of v2, v0, Landroid/view/ViewStub;

    if-eqz v2, :cond_0

    .line 349
    check-cast v0, Landroid/view/ViewStub;

    .end local v0    # "titleImage":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 352
    :cond_0
    iget-object v2, v1, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTrackAdapter$ArtistTrackViewHolder;->titleLayout:Landroid/view/View;

    const v3, 0x7f0d00c9

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, v1, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTrackAdapter$ArtistTrackViewHolder;->titleImage:Landroid/widget/ImageView;

    .line 353
    invoke-virtual {p1, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 354
    return-void
.end method
