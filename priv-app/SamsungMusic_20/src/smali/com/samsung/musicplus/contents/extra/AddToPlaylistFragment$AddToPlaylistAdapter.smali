.class Lcom/samsung/musicplus/contents/extra/AddToPlaylistFragment$AddToPlaylistAdapter;
.super Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter;
.source "AddToPlaylistFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/contents/extra/AddToPlaylistFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AddToPlaylistAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/contents/extra/AddToPlaylistFragment;


# direct methods
.method private constructor <init>(Lcom/samsung/musicplus/contents/extra/AddToPlaylistFragment;Landroid/content/Context;ILandroid/database/Cursor;I)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "layout"    # I
    .param p4, "c"    # Landroid/database/Cursor;
    .param p5, "flags"    # I

    .prologue
    .line 144
    iput-object p1, p0, Lcom/samsung/musicplus/contents/extra/AddToPlaylistFragment$AddToPlaylistAdapter;->this$0:Lcom/samsung/musicplus/contents/extra/AddToPlaylistFragment;

    .line 145
    invoke-direct/range {p0 .. p5}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter;-><init>(Lcom/samsung/musicplus/contents/extra/SelectionListFragment;Landroid/content/Context;ILandroid/database/Cursor;I)V

    .line 146
    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/musicplus/contents/extra/AddToPlaylistFragment;Landroid/content/Context;ILandroid/database/Cursor;ILcom/samsung/musicplus/contents/extra/AddToPlaylistFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/musicplus/contents/extra/AddToPlaylistFragment;
    .param p2, "x1"    # Landroid/content/Context;
    .param p3, "x2"    # I
    .param p4, "x3"    # Landroid/database/Cursor;
    .param p5, "x4"    # I
    .param p6, "x5"    # Lcom/samsung/musicplus/contents/extra/AddToPlaylistFragment$1;

    .prologue
    .line 142
    invoke-direct/range {p0 .. p5}, Lcom/samsung/musicplus/contents/extra/AddToPlaylistFragment$AddToPlaylistAdapter;-><init>(Lcom/samsung/musicplus/contents/extra/AddToPlaylistFragment;Landroid/content/Context;ILandroid/database/Cursor;I)V

    return-void
.end method


# virtual methods
.method protected bindPlaylistItemView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 8
    .param p1, "v"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "c"    # Landroid/database/Cursor;

    .prologue
    const/4 v5, 0x0

    .line 150
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter$SelectionViewHolder;

    .line 151
    .local v0, "vh":Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter$SelectionViewHolder;
    iget-object v1, v0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter$SelectionViewHolder;->text1:Landroid/widget/TextView;

    iget v2, p0, Lcom/samsung/musicplus/contents/extra/AddToPlaylistFragment$AddToPlaylistAdapter;->mText1Index:I

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 152
    iget-object v1, v0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter$SelectionViewHolder;->text2:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 153
    iget-object v1, v0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter$SelectionViewHolder;->text2:Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 154
    invoke-virtual {p0, p3}, Lcom/samsung/musicplus/contents/extra/AddToPlaylistFragment$AddToPlaylistAdapter;->getArtKey(Landroid/database/Cursor;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 155
    .local v6, "plid":J
    invoke-static {}, Lcom/samsung/musicplus/widget/content/PlaylistItemProgress;->getPlaylistItemProgress()Lcom/samsung/musicplus/widget/content/PlaylistItemProgress;

    move-result-object v1

    iget-object v3, v0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter$SelectionViewHolder;->thumbnail:Landroid/widget/ImageView;

    iget-object v4, v0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter$SelectionViewHolder;->text2:Landroid/widget/TextView;

    move-object v2, p2

    invoke-virtual/range {v1 .. v7}, Lcom/samsung/musicplus/widget/content/PlaylistItemProgress;->updateListView(Landroid/content/Context;Landroid/widget/ImageView;Landroid/widget/TextView;ZJ)V

    .line 158
    .end local v6    # "plid":J
    :cond_0
    return-void
.end method
