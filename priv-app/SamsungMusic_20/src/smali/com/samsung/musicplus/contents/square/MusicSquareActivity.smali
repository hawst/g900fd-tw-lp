.class public Lcom/samsung/musicplus/contents/square/MusicSquareActivity;
.super Lcom/samsung/musicplus/MusicBaseActivity;
.source "MusicSquareActivity.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/samsung/musicplus/MusicBaseActivity;-><init>()V

    return-void
.end method

.method private adjustSquareListArea(I)V
    .locals 6
    .param p1, "width"    # I

    .prologue
    .line 82
    const v4, 0x7f0d0166

    invoke-virtual {p0, v4}, Lcom/samsung/musicplus/contents/square/MusicSquareActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 83
    .local v2, "v":Landroid/view/View;
    if-eqz v2, :cond_1

    .line 84
    const/4 v3, 0x0

    .line 85
    .local v3, "visible":I
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0c00b9

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 86
    .local v0, "minWidth":I
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0c011f

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 87
    .local v1, "squareWidth":I
    sub-int v4, p1, v1

    if-le v0, v4, :cond_0

    .line 88
    const/4 v3, 0x4

    .line 90
    :cond_0
    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 92
    .end local v0    # "minWidth":I
    .end local v1    # "squareWidth":I
    .end local v3    # "visible":I
    :cond_1
    return-void
.end method

.method private getSquareFragment()Landroid/app/Fragment;
    .locals 2

    .prologue
    .line 52
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const v1, 0x1000a

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v3, 0x1000a

    .line 21
    invoke-super {p0, p1}, Lcom/samsung/musicplus/MusicBaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 23
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 24
    .local v0, "fg":Landroid/app/Fragment;
    if-nez v0, :cond_0

    .line 25
    const v1, 0x2000a

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->newInstance(Ljava/lang/String;)Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    move-result-object v0

    .line 26
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    const v2, 0x1020002

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v0, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commit()I

    .line 29
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    const v2, 0x7f1000d7

    invoke-virtual {v1, v2}, Landroid/app/ActionBar;->setTitle(I)V

    .line 30
    return-void
.end method

.method protected onGlobalWindowSizeChanged(II)V
    .locals 0
    .param p1, "height"    # I
    .param p2, "width"    # I

    .prologue
    .line 78
    invoke-direct {p0, p2}, Lcom/samsung/musicplus/contents/square/MusicSquareActivity;->adjustSquareListArea(I)V

    .line 79
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 66
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 73
    invoke-super {p0, p1}, Lcom/samsung/musicplus/MusicBaseActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    .line 68
    :pswitch_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareActivity;->launchQueryBrowser()V

    .line 69
    const/4 v0, 0x1

    goto :goto_0

    .line 66
    nop

    :pswitch_data_0
    .packed-switch 0x7f0d01cb
        :pswitch_0
    .end packed-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 57
    const v1, 0x7f0d01cb

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 58
    .local v0, "mi":Landroid/view/MenuItem;
    if-eqz v0, :cond_0

    .line 59
    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 61
    :cond_0
    invoke-super {p0, p1}, Lcom/samsung/musicplus/MusicBaseActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v1

    return v1
.end method

.method protected onReceivePlayerState(Landroid/content/Intent;)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 34
    invoke-super {p0, p1}, Lcom/samsung/musicplus/MusicBaseActivity;->onReceivePlayerState(Landroid/content/Intent;)V

    .line 35
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareActivity;->getSquareFragment()Landroid/app/Fragment;

    move-result-object v0

    .line 36
    .local v0, "fg":Landroid/app/Fragment;
    instance-of v1, v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    if-eqz v1, :cond_0

    .line 37
    check-cast v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    .end local v0    # "fg":Landroid/app/Fragment;
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->receivePlayerState(Ljava/lang/String;)V

    .line 40
    :cond_0
    return-void
.end method

.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 44
    invoke-super {p0, p1, p2}, Lcom/samsung/musicplus/MusicBaseActivity;->onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V

    .line 45
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareActivity;->getSquareFragment()Landroid/app/Fragment;

    move-result-object v0

    .line 46
    .local v0, "fg":Landroid/app/Fragment;
    instance-of v1, v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    if-eqz v1, :cond_0

    .line 47
    check-cast v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    .end local v0    # "fg":Landroid/app/Fragment;
    invoke-virtual {v0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->onServiceConnected()V

    .line 49
    :cond_0
    return-void
.end method
