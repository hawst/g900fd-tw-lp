.class public Lcom/samsung/musicplus/contents/dlna/DlnaStore$ServerContentsExtra;
.super Lcom/samsung/musicplus/library/dlna/DlnaStore$MediaServerContentsExtra;
.source "DlnaStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/contents/dlna/DlnaStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ServerContentsExtra"
.end annotation


# static fields
.field public static final CONTENT_URI:Landroid/net/Uri;

.field public static final DLNA_OPEN_INTENT_TABLE_NAME:Ljava/lang/String; = "dlna_open_intent_table"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 153
    invoke-static {}, Lcom/samsung/musicplus/contents/dlna/DlnaStore$ServerContentsExtra;->getContentUri()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/contents/dlna/DlnaStore$ServerContentsExtra;->CONTENT_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 144
    invoke-direct {p0}, Lcom/samsung/musicplus/library/dlna/DlnaStore$MediaServerContentsExtra;-><init>()V

    return-void
.end method

.method public static final getContentUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 166
    const-string v0, "content://com.samsung.musicplus/dlna_open_intent_table"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method
