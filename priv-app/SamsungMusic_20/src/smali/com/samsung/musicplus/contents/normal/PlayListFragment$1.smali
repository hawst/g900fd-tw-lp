.class Lcom/samsung/musicplus/contents/normal/PlayListFragment$1;
.super Landroid/content/BroadcastReceiver;
.source "PlayListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/contents/normal/PlayListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/contents/normal/PlayListFragment;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/contents/normal/PlayListFragment;)V
    .locals 0

    .prologue
    .line 206
    iput-object p1, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment$1;->this$0:Lcom/samsung/musicplus/contents/normal/PlayListFragment;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 209
    iget-object v3, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment$1;->this$0:Lcom/samsung/musicplus/contents/normal/PlayListFragment;

    invoke-virtual {v3}, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->getAbsListView()Landroid/widget/AbsListView;

    move-result-object v0

    .line 210
    .local v0, "lv":Landroid/widget/AbsListView;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/widget/AbsListView;->getCheckedItemCount()I

    move-result v3

    if-lez v3, :cond_0

    iget-object v3, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment$1;->this$0:Lcom/samsung/musicplus/contents/normal/PlayListFragment;

    # getter for: Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mRemove:Landroid/view/MenuItem;
    invoke-static {v3}, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->access$000(Lcom/samsung/musicplus/contents/normal/PlayListFragment;)Landroid/view/MenuItem;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 211
    invoke-virtual {v0}, Landroid/widget/AbsListView;->getCheckedItemIds()[J

    move-result-object v1

    .line 212
    .local v1, "selectedItemIds":[J
    iget-object v3, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment$1;->this$0:Lcom/samsung/musicplus/contents/normal/PlayListFragment;

    # invokes: Lcom/samsung/musicplus/contents/normal/PlayListFragment;->getSelectedPositions()[I
    invoke-static {v3}, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->access$100(Lcom/samsung/musicplus/contents/normal/PlayListFragment;)[I

    move-result-object v2

    .line 213
    .local v2, "selectedPositions":[I
    iget-object v3, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment$1;->this$0:Lcom/samsung/musicplus/contents/normal/PlayListFragment;

    iget-object v4, p0, Lcom/samsung/musicplus/contents/normal/PlayListFragment$1;->this$0:Lcom/samsung/musicplus/contents/normal/PlayListFragment;

    # getter for: Lcom/samsung/musicplus/contents/normal/PlayListFragment;->mRemove:Landroid/view/MenuItem;
    invoke-static {v4}, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->access$000(Lcom/samsung/musicplus/contents/normal/PlayListFragment;)Landroid/view/MenuItem;

    move-result-object v4

    invoke-virtual {v3, v4, v1, v2}, Lcom/samsung/musicplus/contents/normal/PlayListFragment;->onActionItemClicked(Landroid/view/MenuItem;[J[I)V

    .line 215
    .end local v1    # "selectedItemIds":[J
    .end local v2    # "selectedPositions":[I
    :cond_0
    return-void
.end method
