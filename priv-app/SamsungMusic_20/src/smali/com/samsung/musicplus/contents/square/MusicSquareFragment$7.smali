.class Lcom/samsung/musicplus/contents/square/MusicSquareFragment$7;
.super Landroid/os/Handler;
.source "MusicSquareFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/contents/square/MusicSquareFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;)V
    .locals 0

    .prologue
    .line 935
    iput-object p1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$7;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 938
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 958
    :cond_0
    :goto_0
    return-void

    .line 940
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$7;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mIsDown:Z
    invoke-static {v0, v1}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->access$902(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;Z)Z

    .line 941
    iget-object v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$7;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    const/16 v1, 0x3e8

    # invokes: Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->setReadyToClick(I)V
    invoke-static {v0, v1}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->access$1400(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;I)V

    .line 942
    iget-object v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$7;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    # invokes: Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->doPlaySelectedMusicSquareCell()Z
    invoke-static {v0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->access$500(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 943
    iget-object v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$7;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    # invokes: Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->saveSelectedCellPref()V
    invoke-static {v0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->access$600(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;)V

    goto :goto_0

    .line 947
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$7;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    # getter for: Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSquareGridView:Landroid/widget/GridView;
    invoke-static {v0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->access$100(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;)Landroid/widget/GridView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 948
    iget-object v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$7;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    # getter for: Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSquareGridView:Landroid/widget/GridView;
    invoke-static {v0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->access$100(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;)Landroid/widget/GridView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/GridView;->invalidateViews()V

    goto :goto_0

    .line 952
    :pswitch_2
    iget-object v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$7;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    const/4 v1, 0x1

    # setter for: Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mIsReadyToClick:Z
    invoke-static {v0, v1}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->access$702(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;Z)Z

    goto :goto_0

    .line 938
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
