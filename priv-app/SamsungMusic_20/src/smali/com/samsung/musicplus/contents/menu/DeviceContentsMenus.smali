.class public Lcom/samsung/musicplus/contents/menu/DeviceContentsMenus;
.super Lcom/samsung/musicplus/contents/menu/CommonListMenus;
.source "DeviceContentsMenus.java"


# static fields
.field private static final OPTION_DOWNLOAD_OPTION:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/menu/CommonListMenus;-><init>()V

    return-void
.end method

.method private disableAddToFavorite(Landroid/view/Menu;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 92
    const v1, 0x7f0d01d2

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 93
    .local v0, "addToFavorite":Landroid/view/MenuItem;
    if-eqz v0, :cond_0

    .line 94
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 96
    :cond_0
    return-void
.end method

.method private disableAddToSecretBoxOption(Landroid/view/Menu;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 78
    const v1, 0x7f0d01d6

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 79
    .local v0, "addToSecretBox":Landroid/view/MenuItem;
    if-eqz v0, :cond_0

    .line 80
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 82
    :cond_0
    return-void
.end method

.method private disableDeleteOption(Landroid/view/Menu;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 85
    const v1, 0x7f0d01d0

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 86
    .local v0, "delete":Landroid/view/MenuItem;
    if-eqz v0, :cond_0

    .line 87
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 89
    :cond_0
    return-void
.end method

.method private disableRemoveFromSecretBoxOption(Landroid/view/Menu;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 71
    const v1, 0x7f0d01d7

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 72
    .local v0, "removeFromSecretBox":Landroid/view/MenuItem;
    if-eqz v0, :cond_0

    .line 73
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 75
    :cond_0
    return-void
.end method

.method private disableSetAsOption(Landroid/view/Menu;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 99
    const v1, 0x7f0d01a3

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 100
    .local v0, "setAs":Landroid/view/MenuItem;
    if-eqz v0, :cond_0

    .line 101
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 103
    :cond_0
    return-void
.end method


# virtual methods
.method public onContextItemSelected(Lcom/samsung/musicplus/common/menu/MusicMenuHandler;Landroid/view/MenuItem;J)Z
    .locals 1
    .param p1, "handler"    # Lcom/samsung/musicplus/common/menu/MusicMenuHandler;
    .param p2, "item"    # Landroid/view/MenuItem;
    .param p3, "selectedId"    # J

    .prologue
    .line 67
    const/4 v0, 0x0

    return v0
.end method

.method public onCreateContextMenu(Lcom/samsung/musicplus/common/menu/MusicMenuHandler;Landroid/view/MenuInflater;Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;J)V
    .locals 0
    .param p1, "handler"    # Lcom/samsung/musicplus/common/menu/MusicMenuHandler;
    .param p2, "inflater"    # Landroid/view/MenuInflater;
    .param p3, "menu"    # Landroid/view/ContextMenu;
    .param p4, "v"    # Landroid/view/View;
    .param p5, "menuInfo"    # Landroid/view/ContextMenu$ContextMenuInfo;
    .param p6, "selectedId"    # J

    .prologue
    .line 63
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/MenuInflater;Landroid/view/Menu;)V
    .locals 0
    .param p1, "inflater"    # Landroid/view/MenuInflater;
    .param p2, "menu"    # Landroid/view/Menu;

    .prologue
    .line 30
    invoke-super {p0, p1, p2}, Lcom/samsung/musicplus/contents/menu/CommonListMenus;->onCreateOptionsMenu(Landroid/view/MenuInflater;Landroid/view/Menu;)V

    .line 35
    return-void
.end method

.method public onOptionsItemSelected(Lcom/samsung/musicplus/common/menu/MusicMenuHandler;Landroid/view/MenuItem;)Z
    .locals 7
    .param p1, "handler"    # Lcom/samsung/musicplus/common/menu/MusicMenuHandler;
    .param p2, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v4, 0x1

    .line 47
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v3, 0x7f0d01f3

    if-ne v0, v3, :cond_0

    move-object v6, p1

    .line 48
    check-cast v6, Lcom/samsung/musicplus/contents/menu/CommonListMenuHandler;

    .line 49
    .local v6, "commonMenuHanlder":Lcom/samsung/musicplus/contents/menu/CommonListMenuHandler;
    invoke-interface {v6}, Lcom/samsung/musicplus/contents/menu/CommonListMenuHandler;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 50
    .local v1, "context":Landroid/content/Context;
    invoke-interface {v6}, Lcom/samsung/musicplus/contents/menu/CommonListMenuHandler;->getList()I

    move-result v2

    .line 51
    .local v2, "list":I
    invoke-interface {v6}, Lcom/samsung/musicplus/contents/menu/CommonListMenuHandler;->getKey()Ljava/lang/String;

    move-result-object v5

    .line 52
    .local v5, "key":Ljava/lang/String;
    const/16 v3, 0xc

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/musicplus/contents/menu/DeviceContentsMenus;->launchSelector(Landroid/content/Context;IIILjava/lang/String;)V

    .line 57
    .end local v1    # "context":Landroid/content/Context;
    .end local v2    # "list":I
    .end local v5    # "key":Ljava/lang/String;
    .end local v6    # "commonMenuHanlder":Lcom/samsung/musicplus/contents/menu/CommonListMenuHandler;
    :goto_0
    return v4

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/samsung/musicplus/contents/menu/CommonListMenus;->onOptionsItemSelected(Lcom/samsung/musicplus/common/menu/MusicMenuHandler;Landroid/view/MenuItem;)Z

    move-result v4

    goto :goto_0
.end method

.method public onPrepareOptionsMenu(Lcom/samsung/musicplus/common/menu/MusicMenuHandler;Landroid/view/Menu;)V
    .locals 0
    .param p1, "handler"    # Lcom/samsung/musicplus/common/menu/MusicMenuHandler;
    .param p2, "menu"    # Landroid/view/Menu;

    .prologue
    .line 40
    invoke-direct {p0, p2}, Lcom/samsung/musicplus/contents/menu/DeviceContentsMenus;->disableDeleteOption(Landroid/view/Menu;)V

    .line 41
    invoke-direct {p0, p2}, Lcom/samsung/musicplus/contents/menu/DeviceContentsMenus;->disableAddToSecretBoxOption(Landroid/view/Menu;)V

    .line 42
    invoke-direct {p0, p2}, Lcom/samsung/musicplus/contents/menu/DeviceContentsMenus;->disableRemoveFromSecretBoxOption(Landroid/view/Menu;)V

    .line 43
    return-void
.end method
