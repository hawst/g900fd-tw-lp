.class Lcom/samsung/musicplus/contents/normal/FolderListFragment$FolderListAdapter;
.super Lcom/samsung/musicplus/widget/list/CommonListAdapter;
.source "FolderListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/contents/normal/FolderListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "FolderListAdapter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/contents/normal/FolderListFragment$FolderListAdapter$FolderTabViewHolder;
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/contents/normal/FolderListFragment;


# direct methods
.method public constructor <init>(Lcom/samsung/musicplus/contents/normal/FolderListFragment;Landroid/content/Context;ILandroid/database/Cursor;I)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "layout"    # I
    .param p4, "c"    # Landroid/database/Cursor;
    .param p5, "flags"    # I

    .prologue
    .line 109
    iput-object p1, p0, Lcom/samsung/musicplus/contents/normal/FolderListFragment$FolderListAdapter;->this$0:Lcom/samsung/musicplus/contents/normal/FolderListFragment;

    .line 110
    invoke-direct {p0, p2, p3, p4, p5}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;I)V

    .line 111
    return-void
.end method

.method private setPersonalIcon(Landroid/view/View;Landroid/widget/ImageView;I)V
    .locals 3
    .param p1, "extraViewGroup"    # Landroid/view/View;
    .param p2, "personalIcon"    # Landroid/widget/ImageView;
    .param p3, "isSecreoBoxFolder"    # I

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 187
    const/4 v0, 0x1

    if-ne p3, v0, :cond_1

    .line 188
    if-eqz p1, :cond_0

    .line 189
    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    .line 191
    :cond_0
    invoke-virtual {p2, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 198
    :goto_0
    return-void

    .line 193
    :cond_1
    if-eqz p1, :cond_2

    .line 194
    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 196
    :cond_2
    invoke-virtual {p2, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method protected bindOtherView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "c"    # Landroid/database/Cursor;

    .prologue
    .line 177
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/contents/normal/FolderListFragment$FolderListAdapter$FolderTabViewHolder;

    .line 178
    .local v0, "vh":Lcom/samsung/musicplus/contents/normal/FolderListFragment$FolderListAdapter$FolderTabViewHolder;
    iget-object v1, v0, Lcom/samsung/musicplus/contents/normal/FolderListFragment$FolderListAdapter$FolderTabViewHolder;->personalIcon:Landroid/widget/ImageView;

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/samsung/musicplus/contents/normal/FolderListFragment$FolderListAdapter;->mSecretBoxIndex:I

    if-ltz v1, :cond_0

    .line 179
    const v1, 0x7f0d00be

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-object v2, v0, Lcom/samsung/musicplus/contents/normal/FolderListFragment$FolderListAdapter$FolderTabViewHolder;->personalIcon:Landroid/widget/ImageView;

    iget v3, p0, Lcom/samsung/musicplus/contents/normal/FolderListFragment$FolderListAdapter;->mSecretBoxIndex:I

    invoke-interface {p3, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-direct {p0, v1, v2, v3}, Lcom/samsung/musicplus/contents/normal/FolderListFragment$FolderListAdapter;->setPersonalIcon(Landroid/view/View;Landroid/widget/ImageView;I)V

    .line 182
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->bindOtherView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    .line 183
    return-void
.end method

.method protected bindTextView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "c"    # Landroid/database/Cursor;

    .prologue
    const/4 v6, -0x1

    .line 159
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/musicplus/contents/normal/FolderListFragment$FolderListAdapter$FolderTabViewHolder;

    .line 160
    .local v3, "vh":Lcom/samsung/musicplus/contents/normal/FolderListFragment$FolderListAdapter$FolderTabViewHolder;
    iget-object v4, v3, Lcom/samsung/musicplus/contents/normal/FolderListFragment$FolderListAdapter$FolderTabViewHolder;->text1:Landroid/widget/TextView;

    iget v5, p0, Lcom/samsung/musicplus/contents/normal/FolderListFragment$FolderListAdapter;->mText1Index:I

    invoke-interface {p3, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 161
    iget-object v4, v3, Lcom/samsung/musicplus/contents/normal/FolderListFragment$FolderListAdapter$FolderTabViewHolder;->text2:Landroid/widget/TextView;

    if-eqz v4, :cond_1

    iget v4, p0, Lcom/samsung/musicplus/contents/normal/FolderListFragment$FolderListAdapter;->mText2Index:I

    if-ltz v4, :cond_1

    .line 162
    iget-object v4, v3, Lcom/samsung/musicplus/contents/normal/FolderListFragment$FolderListAdapter$FolderTabViewHolder;->text2:Landroid/widget/TextView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 163
    iget v4, p0, Lcom/samsung/musicplus/contents/normal/FolderListFragment$FolderListAdapter;->mText2Index:I

    invoke-interface {p3, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 164
    .local v2, "text2":Ljava/lang/String;
    const-string v4, ""

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 165
    const-string v4, "/"

    const/4 v5, 0x1

    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v1

    .line 166
    .local v1, "start":I
    const-string v4, "/"

    invoke-virtual {v2, v4}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    .line 167
    .local v0, "end":I
    if-eq v1, v6, :cond_0

    if-eq v0, v6, :cond_0

    .line 168
    invoke-virtual {v2, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 171
    .end local v0    # "end":I
    .end local v1    # "start":I
    :cond_0
    iget-object v4, v3, Lcom/samsung/musicplus/contents/normal/FolderListFragment$FolderListAdapter$FolderTabViewHolder;->text2:Landroid/widget/TextView;

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 173
    .end local v2    # "text2":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "c"    # Landroid/database/Cursor;

    .prologue
    .line 130
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    .line 131
    invoke-virtual {p0, p1, p2, p3}, Lcom/samsung/musicplus/contents/normal/FolderListFragment$FolderListAdapter;->hideBottomDivider(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    .line 132
    return-void
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 116
    iget-object v0, p0, Lcom/samsung/musicplus/contents/normal/FolderListFragment$FolderListAdapter;->this$0:Lcom/samsung/musicplus/contents/normal/FolderListFragment;

    # getter for: Lcom/samsung/musicplus/contents/normal/FolderListFragment;->mList:I
    invoke-static {v0}, Lcom/samsung/musicplus/contents/normal/FolderListFragment;->access$000(Lcom/samsung/musicplus/contents/normal/FolderListFragment;)I

    move-result v0

    invoke-static {v0}, Lcom/samsung/musicplus/util/ListUtils;->isGridType(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 117
    add-int/lit8 v0, p1, -0x1

    invoke-super {p0, v0}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    .line 119
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 123
    iget-object v0, p0, Lcom/samsung/musicplus/contents/normal/FolderListFragment$FolderListAdapter;->this$0:Lcom/samsung/musicplus/contents/normal/FolderListFragment;

    # getter for: Lcom/samsung/musicplus/contents/normal/FolderListFragment;->mList:I
    invoke-static {v0}, Lcom/samsung/musicplus/contents/normal/FolderListFragment;->access$100(Lcom/samsung/musicplus/contents/normal/FolderListFragment;)I

    move-result v0

    invoke-static {v0}, Lcom/samsung/musicplus/util/ListUtils;->isGridType(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 124
    add-int/lit8 v0, p1, -0x1

    invoke-super {p0, v0}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->getItemId(I)J

    move-result-wide v0

    .line 126
    :goto_0
    return-wide v0

    :cond_0
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->getItemId(I)J

    move-result-wide v0

    goto :goto_0
.end method

.method protected getViewHolder()Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;
    .locals 2

    .prologue
    .line 154
    new-instance v0, Lcom/samsung/musicplus/contents/normal/FolderListFragment$FolderListAdapter$FolderTabViewHolder;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/samsung/musicplus/contents/normal/FolderListFragment$FolderListAdapter$FolderTabViewHolder;-><init>(Lcom/samsung/musicplus/contents/normal/FolderListFragment$FolderListAdapter;Lcom/samsung/musicplus/contents/normal/FolderListFragment$1;)V

    return-object v0
.end method

.method protected newOtherView(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 135
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/musicplus/contents/normal/FolderListFragment$FolderListAdapter$FolderTabViewHolder;

    .line 137
    .local v2, "vh":Lcom/samsung/musicplus/contents/normal/FolderListFragment$FolderListAdapter$FolderTabViewHolder;
    const v3, 0x7f0d00bf

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 138
    .local v0, "personalStub":Landroid/view/View;
    instance-of v3, v0, Landroid/view/ViewStub;

    if-eqz v3, :cond_2

    .line 139
    check-cast v0, Landroid/view/ViewStub;

    .end local v0    # "personalStub":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 140
    const v3, 0x7f0d0105

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, v2, Lcom/samsung/musicplus/contents/normal/FolderListFragment$FolderListAdapter$FolderTabViewHolder;->personalIcon:Landroid/widget/ImageView;

    .line 145
    :cond_0
    :goto_0
    const v3, 0x7f0d00c1

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 146
    .local v1, "thumbnailIcon":Landroid/view/View;
    instance-of v3, v1, Landroid/view/ViewStub;

    if-eqz v3, :cond_1

    .line 147
    check-cast v1, Landroid/view/ViewStub;

    .end local v1    # "thumbnailIcon":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 149
    :cond_1
    invoke-virtual {p1, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 150
    return-void

    .line 141
    .restart local v0    # "personalStub":Landroid/view/View;
    :cond_2
    instance-of v3, v0, Landroid/widget/ImageView;

    if-eqz v3, :cond_0

    .line 142
    check-cast v0, Landroid/widget/ImageView;

    .end local v0    # "personalStub":Landroid/view/View;
    iput-object v0, v2, Lcom/samsung/musicplus/contents/normal/FolderListFragment$FolderListAdapter$FolderTabViewHolder;->personalIcon:Landroid/widget/ImageView;

    goto :goto_0
.end method
