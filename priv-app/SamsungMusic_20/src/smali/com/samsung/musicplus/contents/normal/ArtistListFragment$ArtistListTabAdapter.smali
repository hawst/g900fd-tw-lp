.class Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTabAdapter;
.super Lcom/samsung/musicplus/widget/list/CommonListAdapter;
.source "ArtistListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/contents/normal/ArtistListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ArtistListTabAdapter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTabAdapter$ArtistTabViewHolder;
    }
.end annotation


# instance fields
.field private mAblumCount:I

.field private mNumberOfAlbums:I

.field private mNumberOfTracks:I

.field final synthetic this$0:Lcom/samsung/musicplus/contents/normal/ArtistListFragment;


# direct methods
.method public constructor <init>(Lcom/samsung/musicplus/contents/normal/ArtistListFragment;Landroid/content/Context;ILandroid/database/Cursor;I)V
    .locals 2
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "layout"    # I
    .param p4, "c"    # Landroid/database/Cursor;
    .param p5, "flags"    # I

    .prologue
    const/4 v0, -0x1

    .line 160
    iput-object p1, p0, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTabAdapter;->this$0:Lcom/samsung/musicplus/contents/normal/ArtistListFragment;

    .line 161
    invoke-direct {p0, p2, p3, p4, p5}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;I)V

    .line 156
    iput v0, p0, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTabAdapter;->mNumberOfAlbums:I

    .line 158
    iput v0, p0, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTabAdapter;->mNumberOfTracks:I

    .line 162
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e0002

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTabAdapter;->mAblumCount:I

    .line 163
    return-void
.end method

.method private bindArtistThumbnailsView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "c"    # Landroid/database/Cursor;

    .prologue
    const/4 v5, 0x0

    .line 265
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTabAdapter$ArtistTabViewHolder;

    .line 266
    .local v2, "vh":Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTabAdapter$ArtistTabViewHolder;
    const v3, 0x7f0d00b3

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 267
    .local v1, "thumb":Landroid/view/View;
    iget v3, p0, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTabAdapter;->mNumberOfAlbums:I

    iget v4, p0, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTabAdapter;->mAblumCount:I

    if-le v3, v4, :cond_1

    .line 268
    iget-object v3, v2, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTabAdapter$ArtistTabViewHolder;->dummy:Landroid/widget/ImageView;

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 269
    if-eqz v1, :cond_0

    .line 270
    invoke-virtual {v1, v5, v5, v5, v5}, Landroid/view/View;->setPadding(IIII)V

    .line 279
    :cond_0
    :goto_0
    const-string v3, "_id"

    invoke-interface {p3, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p3, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, p2, v2, v3}, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTabAdapter;->setArtistAlbumView(Landroid/content/Context;Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTabAdapter$ArtistTabViewHolder;Ljava/lang/String;)V

    .line 281
    return-void

    .line 273
    :cond_1
    iget-object v3, v2, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTabAdapter$ArtistTabViewHolder;->dummy:Landroid/widget/ImageView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 274
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c0072

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 275
    .local v0, "padding":I
    if-eqz v1, :cond_0

    .line 276
    invoke-virtual {v1, v5, v5, v0, v5}, Landroid/view/View;->setPadding(IIII)V

    goto :goto_0
.end method

.method private initAlbumNumber(Landroid/database/Cursor;)V
    .locals 3
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 284
    const-string v2, "number_of_albums"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 285
    .local v0, "albumNumberIdx":I
    const-string v2, "number_of_tracks"

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 286
    .local v1, "trackNumberIdx":I
    if-lez v0, :cond_0

    if-lez v1, :cond_0

    .line 287
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, p0, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTabAdapter;->mNumberOfAlbums:I

    .line 288
    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, p0, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTabAdapter;->mNumberOfTracks:I

    .line 290
    :cond_0
    return-void
.end method

.method private prepareImageViews(Landroid/view/View;Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTabAdapter$ArtistTabViewHolder;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;
    .param p2, "vh"    # Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTabAdapter$ArtistTabViewHolder;

    .prologue
    .line 207
    iget v2, p0, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTabAdapter;->mAblumCount:I

    new-array v2, v2, [Landroid/widget/ImageView;

    iput-object v2, p2, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTabAdapter$ArtistTabViewHolder;->thumbnails:[Landroid/widget/ImageView;

    .line 210
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTabAdapter;->mAblumCount:I

    if-ge v0, v2, :cond_0

    .line 213
    packed-switch v0, :pswitch_data_0

    .line 240
    :cond_0
    return-void

    .line 215
    :pswitch_0
    const v2, 0x7f0d00a5

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 238
    .local v1, "iv":Landroid/widget/ImageView;
    :goto_1
    iget-object v2, p2, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTabAdapter$ArtistTabViewHolder;->thumbnails:[Landroid/widget/ImageView;

    aput-object v1, v2, v0

    .line 210
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 218
    .end local v1    # "iv":Landroid/widget/ImageView;
    :pswitch_1
    const v2, 0x7f0d00b7

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 219
    .restart local v1    # "iv":Landroid/widget/ImageView;
    goto :goto_1

    .line 221
    .end local v1    # "iv":Landroid/widget/ImageView;
    :pswitch_2
    const v2, 0x7f0d00b8

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 222
    .restart local v1    # "iv":Landroid/widget/ImageView;
    goto :goto_1

    .line 224
    .end local v1    # "iv":Landroid/widget/ImageView;
    :pswitch_3
    const v2, 0x7f0d00b9

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 225
    .restart local v1    # "iv":Landroid/widget/ImageView;
    goto :goto_1

    .line 227
    .end local v1    # "iv":Landroid/widget/ImageView;
    :pswitch_4
    const v2, 0x7f0d00ba

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 228
    .restart local v1    # "iv":Landroid/widget/ImageView;
    goto :goto_1

    .line 230
    .end local v1    # "iv":Landroid/widget/ImageView;
    :pswitch_5
    const v2, 0x7f0d00b4

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 231
    .restart local v1    # "iv":Landroid/widget/ImageView;
    goto :goto_1

    .line 213
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private setArtistAlbumView(Landroid/content/Context;Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTabAdapter$ArtistTabViewHolder;Ljava/lang/String;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "vh"    # Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTabAdapter$ArtistTabViewHolder;
    .param p3, "artistId"    # Ljava/lang/String;

    .prologue
    .line 293
    iget v0, p0, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTabAdapter;->mNumberOfAlbums:I

    new-array v4, v0, [Landroid/widget/ImageView;

    .line 294
    .local v4, "iv":[Landroid/widget/ImageView;
    invoke-static {p3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    .line 295
    .local v3, "artTag":Ljava/lang/Long;
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    iget v0, p0, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTabAdapter;->mAblumCount:I

    if-ge v7, v0, :cond_1

    .line 296
    iget v0, p0, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTabAdapter;->mNumberOfAlbums:I

    if-le v0, v7, :cond_0

    .line 297
    iget-object v0, p2, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTabAdapter$ArtistTabViewHolder;->thumbnails:[Landroid/widget/ImageView;

    aget-object v0, v0, v7

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 301
    iget-object v0, p2, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTabAdapter$ArtistTabViewHolder;->thumbnails:[Landroid/widget/ImageView;

    aget-object v0, v0, v7

    aput-object v0, v4, v7

    .line 305
    :goto_1
    iget-object v0, p2, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTabAdapter$ArtistTabViewHolder;->thumbnails:[Landroid/widget/ImageView;

    aget-object v0, v0, v7

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 295
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 303
    :cond_0
    iget-object v0, p2, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTabAdapter$ArtistTabViewHolder;->thumbnails:[Landroid/widget/ImageView;

    aget-object v0, v0, v7

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    .line 308
    :cond_1
    iget-boolean v0, p0, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTabAdapter;->mAlbumArtParsingEnabled:Z

    if-eqz v0, :cond_2

    .line 309
    invoke-static {}, Lcom/samsung/musicplus/util/AlbumArtLoader;->getAlbumArtLoader()Lcom/samsung/musicplus/util/IAlbumArtLoader;

    move-result-object v0

    sget-object v2, Lcom/samsung/musicplus/util/AlbumArtUtils;->DEFAULT_ARTWORK_URI_STRING:Ljava/lang/String;

    iget v5, p0, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTabAdapter;->mAlbumArtSize:I

    const v6, 0x7f020037

    move-object v1, p1

    invoke-interface/range {v0 .. v6}, Lcom/samsung/musicplus/util/IAlbumArtLoader;->loadArtistGroupArtwork(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Object;[Landroid/widget/ImageView;II)V

    .line 313
    :cond_2
    return-void
.end method


# virtual methods
.method protected bindTextView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "c"    # Landroid/database/Cursor;

    .prologue
    const/4 v4, 0x0

    .line 256
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTabAdapter$ArtistTabViewHolder;

    .line 257
    .local v1, "vh":Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTabAdapter$ArtistTabViewHolder;
    iget-object v2, v1, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTabAdapter$ArtistTabViewHolder;->text1:Landroid/widget/TextView;

    iget v3, p0, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTabAdapter;->mText1Index:I

    invoke-interface {p3, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p2, v3}, Lcom/samsung/musicplus/util/UiUtils;->transUnknownString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 258
    iget-object v2, v1, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTabAdapter$ArtistTabViewHolder;->text2:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 259
    iget v2, p0, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTabAdapter;->mNumberOfAlbums:I

    iget v3, p0, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTabAdapter;->mNumberOfTracks:I

    invoke-static {p2, v2, v3, v4}, Lcom/samsung/musicplus/util/UiUtils;->makeAlbumsSongsLabel(Landroid/content/Context;IIZ)Ljava/lang/String;

    move-result-object v0

    .line 261
    .local v0, "number":Ljava/lang/String;
    iget-object v2, v1, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTabAdapter$ArtistTabViewHolder;->text2:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 262
    return-void
.end method

.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "c"    # Landroid/database/Cursor;

    .prologue
    .line 244
    invoke-direct {p0, p3}, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTabAdapter;->initAlbumNumber(Landroid/database/Cursor;)V

    .line 245
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    .line 246
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTabAdapter;->bindArtistThumbnailsView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    .line 247
    const v1, 0x7f0d00bb

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 248
    .local v0, "divider":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 249
    const v1, 0x7f020022

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 251
    :cond_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTabAdapter;->hideBottomDivider(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    .line 252
    return-void
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 168
    iget-object v0, p0, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTabAdapter;->this$0:Lcom/samsung/musicplus/contents/normal/ArtistListFragment;

    # getter for: Lcom/samsung/musicplus/contents/normal/ArtistListFragment;->mList:I
    invoke-static {v0}, Lcom/samsung/musicplus/contents/normal/ArtistListFragment;->access$000(Lcom/samsung/musicplus/contents/normal/ArtistListFragment;)I

    move-result v0

    invoke-static {v0}, Lcom/samsung/musicplus/util/ListUtils;->isGridType(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 169
    add-int/lit8 v0, p1, -0x1

    invoke-super {p0, v0}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    .line 171
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 175
    iget-object v0, p0, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTabAdapter;->this$0:Lcom/samsung/musicplus/contents/normal/ArtistListFragment;

    # getter for: Lcom/samsung/musicplus/contents/normal/ArtistListFragment;->mList:I
    invoke-static {v0}, Lcom/samsung/musicplus/contents/normal/ArtistListFragment;->access$100(Lcom/samsung/musicplus/contents/normal/ArtistListFragment;)I

    move-result v0

    invoke-static {v0}, Lcom/samsung/musicplus/util/ListUtils;->isGridType(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 176
    add-int/lit8 v0, p1, -0x1

    invoke-super {p0, v0}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->getItemId(I)J

    move-result-wide v0

    .line 178
    :goto_0
    return-wide v0

    :cond_0
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->getItemId(I)J

    move-result-wide v0

    goto :goto_0
.end method

.method protected getViewHolder()Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;
    .locals 2

    .prologue
    .line 203
    new-instance v0, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTabAdapter$ArtistTabViewHolder;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTabAdapter$ArtistTabViewHolder;-><init>(Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTabAdapter;Lcom/samsung/musicplus/contents/normal/ArtistListFragment$1;)V

    return-object v0
.end method

.method protected newTextView(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 190
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTabAdapter$ArtistTabViewHolder;

    .line 191
    .local v0, "vh":Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTabAdapter$ArtistTabViewHolder;
    const v1, 0x7f0d009f

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v0, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTabAdapter$ArtistTabViewHolder;->text1:Landroid/widget/TextView;

    .line 192
    const v1, 0x7f0d00a0

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v0, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTabAdapter$ArtistTabViewHolder;->text2:Landroid/widget/TextView;

    .line 193
    iget v1, p0, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTabAdapter;->mText2Index:I

    if-ltz v1, :cond_0

    .line 194
    iget-object v1, v0, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTabAdapter$ArtistTabViewHolder;->text2:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 198
    :goto_0
    invoke-virtual {p1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 199
    return-void

    .line 196
    :cond_0
    iget-object v1, v0, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTabAdapter$ArtistTabViewHolder;->text2:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method protected newThumbnailView(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 182
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTabAdapter$ArtistTabViewHolder;

    .line 183
    .local v0, "vh":Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTabAdapter$ArtistTabViewHolder;
    const v1, 0x7f0d00b6

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, v0, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTabAdapter$ArtistTabViewHolder;->dummy:Landroid/widget/ImageView;

    .line 184
    invoke-direct {p0, p1, v0}, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTabAdapter;->prepareImageViews(Landroid/view/View;Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTabAdapter$ArtistTabViewHolder;)V

    .line 185
    invoke-virtual {p1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 186
    return-void
.end method
