.class public Lcom/samsung/musicplus/contents/devices/DeviceContentsListFragment;
.super Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;
.source "DeviceContentsListFragment.java"

# interfaces
.implements Lcom/samsung/musicplus/contents/search/QueryBrowserActivity$QueryTextChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/contents/devices/DeviceContentsListFragment$ContentsTrackAdapter;
    }
.end annotation


# static fields
.field private static final THROTTLE_TIME:I = 0x7d0


# instance fields
.field private mDeviceStateReceiver:Landroid/content/BroadcastReceiver;

.field private mQueryText:Ljava/lang/String;

.field private mSearchView:Landroid/widget/SearchView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;-><init>()V

    .line 123
    new-instance v0, Lcom/samsung/musicplus/contents/devices/DeviceContentsListFragment$1;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/contents/devices/DeviceContentsListFragment$1;-><init>(Lcom/samsung/musicplus/contents/devices/DeviceContentsListFragment;)V

    iput-object v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceContentsListFragment;->mDeviceStateReceiver:Landroid/content/BroadcastReceiver;

    .line 266
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/contents/devices/DeviceContentsListFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/devices/DeviceContentsListFragment;

    .prologue
    .line 67
    iget-object v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceContentsListFragment;->mQueryText:Ljava/lang/String;

    return-object v0
.end method

.method private deleteSlinkContents([J)V
    .locals 3
    .param p1, "selectedItemIds"    # [J

    .prologue
    .line 254
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/devices/DeviceContentsListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/devices/DeviceContentsListFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const/16 v2, 0x7d0

    invoke-static {v0, v1, p1, v2}, Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog;->show(Landroid/content/Context;Landroid/app/FragmentManager;[JI)V

    .line 256
    return-void
.end method

.method private downloadSlinkContents([J)V
    .locals 1
    .param p1, "selectedItemIds"    # [J

    .prologue
    .line 249
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/devices/DeviceContentsListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/samsung/musicplus/provider/MusicContents$Audio$DeviceContents;->download(Landroid/content/Context;[J)V

    .line 251
    return-void
.end method

.method private getDeviceContentsCursorLoader()Landroid/content/CursorLoader;
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 162
    sget-boolean v1, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_PINYIN:Z

    if-eqz v1, :cond_0

    const-string v7, "index_char, title"

    .line 165
    .local v7, "sortOrder":Ljava/lang/String;
    :goto_0
    new-instance v0, Landroid/content/CursorLoader;

    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/devices/DeviceContentsListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/contents/devices/DeviceContentsListFragment;->mKey:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/samsung/musicplus/provider/MusicContents$Audio$Devices;->getContentsUri(J)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {p0}, Lcom/samsung/musicplus/contents/devices/DeviceContentsListFragment;->getSelection()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " COLLATE LOCALIZED ASC"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object v5, v3

    invoke-direct/range {v0 .. v6}, Landroid/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    .local v0, "cl":Landroid/content/CursorLoader;
    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v2, v3}, Landroid/content/CursorLoader;->setUpdateThrottle(J)V

    .line 170
    return-object v0

    .line 162
    .end local v0    # "cl":Landroid/content/CursorLoader;
    .end local v7    # "sortOrder":Ljava/lang/String;
    :cond_0
    const-string v7, "title"

    goto :goto_0
.end method

.method private getSelection()Ljava/lang/String;
    .locals 4

    .prologue
    .line 174
    iget-object v2, p0, Lcom/samsung/musicplus/contents/devices/DeviceContentsListFragment;->mQueryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    iget-object v1, v2, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->selection:Ljava/lang/String;

    .line 175
    .local v1, "selection":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/musicplus/contents/devices/DeviceContentsListFragment;->mQueryText:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 176
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "title"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 177
    .local v0, "sb":Ljava/lang/StringBuilder;
    const-string v2, " Like \'%"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/musicplus/contents/devices/DeviceContentsListFragment;->mQueryText:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "%\'"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 179
    .end local v0    # "sb":Ljava/lang/StringBuilder;
    :cond_0
    return-object v1
.end method


# virtual methods
.method public actionSconnect()Z
    .locals 1

    .prologue
    .line 158
    const/4 v0, 0x0

    return v0
.end method

.method protected createAdapter()Landroid/widget/CursorAdapter;
    .locals 6

    .prologue
    .line 78
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/devices/DeviceContentsListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    .line 79
    .local v2, "a":Landroid/app/Activity;
    new-instance v0, Lcom/samsung/musicplus/contents/devices/DeviceContentsListFragment$ContentsTrackAdapter;

    const v3, 0x7f040033

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/samsung/musicplus/contents/devices/DeviceContentsListFragment$ContentsTrackAdapter;-><init>(Lcom/samsung/musicplus/contents/devices/DeviceContentsListFragment;Landroid/content/Context;ILandroid/database/Cursor;I)V

    return-object v0
.end method

.method protected createMusicMenus()Lcom/samsung/musicplus/common/menu/AbstractMusicMenus;
    .locals 1

    .prologue
    .line 245
    new-instance v0, Lcom/samsung/musicplus/contents/menu/DeviceContentsMenus;

    invoke-direct {v0}, Lcom/samsung/musicplus/contents/menu/DeviceContentsMenus;-><init>()V

    return-object v0
.end method

.method protected initLoader()V
    .locals 3

    .prologue
    .line 84
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/devices/DeviceContentsListFragment;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    const v1, 0x2000d

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    .line 85
    return-void
.end method

.method protected isEnableListShuffle(I)Z
    .locals 1
    .param p1, "list"    # I

    .prologue
    .line 260
    iget-object v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceContentsListFragment;->mSearchView:Landroid/widget/SearchView;

    if-eqz v0, :cond_0

    .line 261
    const/4 v0, 0x0

    .line 263
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->isEnableListShuffle(I)Z

    move-result v0

    goto :goto_0
.end method

.method public onActionItemClicked(Landroid/view/MenuItem;[J[I)V
    .locals 4
    .param p1, "item"    # Landroid/view/MenuItem;
    .param p2, "selectedItemIds"    # [J
    .param p3, "selectedPositions"    # [I

    .prologue
    .line 218
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 241
    :goto_0
    return-void

    .line 220
    :sswitch_0
    invoke-direct {p0, p2}, Lcom/samsung/musicplus/contents/devices/DeviceContentsListFragment;->downloadSlinkContents([J)V

    .line 221
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/devices/DeviceContentsListFragment;->finishActionMode()V

    goto :goto_0

    .line 224
    :sswitch_1
    invoke-direct {p0, p2}, Lcom/samsung/musicplus/contents/devices/DeviceContentsListFragment;->deleteSlinkContents([J)V

    .line 225
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/devices/DeviceContentsListFragment;->finishActionMode()V

    .line 226
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/devices/DeviceContentsListFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "SLOD"

    invoke-static {v1, v2}, Lcom/samsung/musicplus/util/logging/FeatureLogger;->insertLog(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 229
    :sswitch_2
    const/4 v1, 0x0

    aget-wide v2, p2, v1

    invoke-virtual {p0, v2, v3}, Lcom/samsung/musicplus/contents/devices/DeviceContentsListFragment;->launchDetails(J)V

    .line 230
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/devices/DeviceContentsListFragment;->finishActionMode()V

    goto :goto_0

    .line 233
    :sswitch_3
    const/4 v0, 0x2

    .line 234
    .local v0, "type":I
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/devices/DeviceContentsListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v1, v0, p2, v2}, Lcom/samsung/musicplus/provider/MusicContents$Audio$DeviceContents;->startDeviceSelectActivityForResult(Landroid/app/Activity;I[JI)V

    .line 237
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/devices/DeviceContentsListFragment;->finishActionMode()V

    goto :goto_0

    .line 218
    :sswitch_data_0
    .sparse-switch
        0x7f0d01d0 -> :sswitch_1
        0x7f0d01d8 -> :sswitch_2
        0x7f0d01ef -> :sswitch_3
        0x7f0d01f3 -> :sswitch_0
    .end sparse-switch
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 97
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 98
    .local v0, "f":Landroid/content/IntentFilter;
    sget-object v1, Lcom/samsung/musicplus/provider/MusicContents$Audio$DeviceContents;->ACTION_S_LINK_SIGNIN_CHANGED:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 99
    const-string v1, "android.intent.action.SAMSUNGACCOUNT_SIGNOUT_COMPLETED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 100
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/devices/DeviceContentsListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/contents/devices/DeviceContentsListFragment;->mDeviceStateReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 101
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 102
    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/content/Loader;
    .locals 1
    .param p1, "id"    # I
    .param p2, "args"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 151
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/devices/DeviceContentsListFragment;->getDeviceContentsCursorLoader()Landroid/content/CursorLoader;

    move-result-object v0

    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 119
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/devices/DeviceContentsListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/musicplus/contents/devices/DeviceContentsListFragment;->mDeviceStateReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 120
    invoke-super {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->onDestroy()V

    .line 121
    return-void
.end method

.method public onInflateOptionsMenu(Landroid/view/Menu;)V
    .locals 6
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x0

    .line 184
    const v1, 0x7f0d01f3

    const v2, 0x7f1000af

    invoke-interface {p1, v4, v1, v4, v2}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    .line 185
    .local v0, "m":Landroid/view/MenuItem;
    const v1, 0x7f0200f5

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 186
    invoke-interface {v0, v5}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 187
    const v1, 0x7f0d01d8

    const/4 v2, 0x1

    const v3, 0x7f1000ae

    invoke-interface {p1, v4, v1, v2, v3}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    .line 188
    invoke-interface {v0, v4}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 189
    const v1, 0x7f0d01d0

    const v2, 0x7f1000ad

    invoke-interface {p1, v4, v1, v4, v2}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    .line 190
    const v1, 0x7f0200fa

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setIcon(I)Landroid/view/MenuItem;

    .line 191
    invoke-interface {v0, v5}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 194
    const v1, 0x7f0d01ef

    const v2, 0x7f1000be

    invoke-interface {p1, v4, v1, v4, v2}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    .line 196
    invoke-interface {v0, v4}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 197
    return-void
.end method

.method public onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V
    .locals 1
    .param p2, "data"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 89
    .local p1, "loader":Landroid/content/Loader;, "Landroid/content/Loader<Landroid/database/Cursor;>;"
    iget-object v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceContentsListFragment;->mSearchView:Landroid/widget/SearchView;

    if-eqz v0, :cond_0

    .line 90
    const v0, 0x7f100102

    iput v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceContentsListFragment;->mNoItemTextId:I

    .line 92
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V

    .line 93
    return-void
.end method

.method public bridge synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Landroid/content/Loader;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 67
    check-cast p2, Landroid/database/Cursor;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/samsung/musicplus/contents/devices/DeviceContentsListFragment;->onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onPause()V
    .locals 0

    .prologue
    .line 113
    invoke-static {}, Lcom/samsung/musicplus/provider/MusicContents$Audio$DeviceContents;->releaseWakeLock()V

    .line 114
    invoke-super {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->onPause()V

    .line 115
    return-void
.end method

.method public onPrepareActionMode(Landroid/view/Menu;)V
    .locals 5
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 201
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/devices/DeviceContentsListFragment;->getAbsListView()Landroid/widget/AbsListView;

    move-result-object v2

    .line 202
    .local v2, "lv":Landroid/widget/AbsListView;
    if-nez v2, :cond_0

    .line 203
    const-string v3, "MusicUiList"

    const-string v4, "Called onPrepareActionMode() but listView is null yet."

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 214
    :goto_0
    return-void

    .line 207
    :cond_0
    invoke-virtual {p0, v2}, Lcom/samsung/musicplus/contents/devices/DeviceContentsListFragment;->hasSelectedItem(Landroid/widget/AbsListView;)Z

    move-result v0

    .line 208
    .local v0, "hasSelectedItem":Z
    invoke-virtual {p0, v2}, Lcom/samsung/musicplus/contents/devices/DeviceContentsListFragment;->isSelectedOneItem(Landroid/widget/AbsListView;)Z

    move-result v1

    .line 210
    .local v1, "isSelectedOne":Z
    const v3, 0x7f0d01d8

    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    invoke-interface {v3, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 211
    const v3, 0x7f0d01f3

    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 212
    const v3, 0x7f0d01d0

    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 213
    const v3, 0x7f0d01ef

    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v3

    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public onQueryTextChanged(Ljava/lang/String;)V
    .locals 3
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 303
    iput-object p1, p0, Lcom/samsung/musicplus/contents/devices/DeviceContentsListFragment;->mQueryText:Ljava/lang/String;

    .line 304
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/devices/DeviceContentsListFragment;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    iget v1, p0, Lcom/samsung/musicplus/contents/devices/DeviceContentsListFragment;->mList:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    .line 305
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 107
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/devices/DeviceContentsListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/musicplus/provider/MusicContents$Audio$DeviceContents;->acquireWakeLock(Landroid/content/Context;)V

    .line 108
    invoke-super {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->onResume()V

    .line 109
    return-void
.end method

.method public setSearchView(Landroid/widget/SearchView;)V
    .locals 0
    .param p1, "view"    # Landroid/widget/SearchView;

    .prologue
    .line 309
    iput-object p1, p0, Lcom/samsung/musicplus/contents/devices/DeviceContentsListFragment;->mSearchView:Landroid/widget/SearchView;

    .line 310
    return-void
.end method
