.class public Lcom/samsung/musicplus/contents/extra/SelectionListFragment;
.super Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;
.source "SelectionListFragment.java"

# interfaces
.implements Lcom/samsung/musicplus/util/UiUtils$Defs;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter;
    }
.end annotation


# static fields
.field private static final DESELECT_ALL:I = 0x7f0d01fe

.field private static final MENU_DONE:I = 0x7f0d01ca

.field private static final PLAYLIST_ID:Ljava/lang/String; = "playlist_id"

.field private static final SELECT_ALL:I = 0x7f0d01fd


# instance fields
.field private mAdditionalKeyWord:Ljava/lang/String;

.field private mCheckBox:Landroid/widget/CheckBox;

.field private mDoneItem:Landroid/view/MenuItem;

.field protected final mHeaderButtonClickListener:Landroid/view/View$OnClickListener;

.field private mHeaderMode:I

.field private mIsDoneEnabled:Z

.field private mIsRegistered:Z

.field private mKeyWord:Ljava/lang/String;

.field private mListMode:I

.field private mListType:I

.field private mObserver:Landroid/database/ContentObserver;

.field private mPlaylistId:J

.field private mPopupMenu:Lcom/samsung/musicplus/widget/AnchorSizePopupMenu;

.field private mSelectedListId:[J

.field mTwMultiSelectedListener:Landroid/widget/AdapterView$OnTwMultiSelectedListener;

.field private mUIHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 81
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;-><init>()V

    .line 103
    iput-boolean v0, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mIsDoneEnabled:Z

    .line 105
    iput-boolean v0, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mIsRegistered:Z

    .line 115
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mCheckBox:Landroid/widget/CheckBox;

    .line 191
    new-instance v0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$2;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$2;-><init>(Lcom/samsung/musicplus/contents/extra/SelectionListFragment;)V

    iput-object v0, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mUIHandler:Landroid/os/Handler;

    .line 597
    new-instance v0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$6;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$6;-><init>(Lcom/samsung/musicplus/contents/extra/SelectionListFragment;)V

    iput-object v0, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mHeaderButtonClickListener:Landroid/view/View$OnClickListener;

    .line 876
    new-instance v0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$7;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$7;-><init>(Lcom/samsung/musicplus/contents/extra/SelectionListFragment;)V

    iput-object v0, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mTwMultiSelectedListener:Landroid/widget/AdapterView$OnTwMultiSelectedListener;

    .line 893
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/contents/extra/SelectionListFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/extra/SelectionListFragment;

    .prologue
    .line 81
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->isEnableSelect()Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/samsung/musicplus/contents/extra/SelectionListFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/extra/SelectionListFragment;

    .prologue
    .line 81
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->refreshSelectedList()V

    return-void
.end method

.method static synthetic access$1000(Lcom/samsung/musicplus/contents/extra/SelectionListFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/extra/SelectionListFragment;

    .prologue
    .line 81
    iget v0, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mText3Idx:I

    return v0
.end method

.method static synthetic access$1100(Lcom/samsung/musicplus/contents/extra/SelectionListFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/extra/SelectionListFragment;

    .prologue
    .line 81
    iget v0, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mDurationIdx:I

    return v0
.end method

.method static synthetic access$1200(Lcom/samsung/musicplus/contents/extra/SelectionListFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/extra/SelectionListFragment;

    .prologue
    .line 81
    iget v0, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mHeaderMode:I

    return v0
.end method

.method static synthetic access$1300(Lcom/samsung/musicplus/contents/extra/SelectionListFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/extra/SelectionListFragment;

    .prologue
    .line 81
    iget v0, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mText2Idx:I

    return v0
.end method

.method static synthetic access$1400(Lcom/samsung/musicplus/contents/extra/SelectionListFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/extra/SelectionListFragment;

    .prologue
    .line 81
    iget v0, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mText2Idx:I

    return v0
.end method

.method static synthetic access$1500(Lcom/samsung/musicplus/contents/extra/SelectionListFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/extra/SelectionListFragment;

    .prologue
    .line 81
    iget v0, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mText3Idx:I

    return v0
.end method

.method static synthetic access$1600(Lcom/samsung/musicplus/contents/extra/SelectionListFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/extra/SelectionListFragment;

    .prologue
    .line 81
    iget v0, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mText3Idx:I

    return v0
.end method

.method static synthetic access$1700(Lcom/samsung/musicplus/contents/extra/SelectionListFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/extra/SelectionListFragment;

    .prologue
    .line 81
    iget v0, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mDurationIdx:I

    return v0
.end method

.method static synthetic access$1800(Lcom/samsung/musicplus/contents/extra/SelectionListFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/extra/SelectionListFragment;

    .prologue
    .line 81
    iget v0, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mDurationIdx:I

    return v0
.end method

.method static synthetic access$200(Lcom/samsung/musicplus/contents/extra/SelectionListFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/extra/SelectionListFragment;

    .prologue
    .line 81
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->invalidateActionBarTitle()V

    return-void
.end method

.method static synthetic access$300(Lcom/samsung/musicplus/contents/extra/SelectionListFragment;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/extra/SelectionListFragment;

    .prologue
    .line 81
    iget-object v0, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mCheckBox:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/musicplus/contents/extra/SelectionListFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/extra/SelectionListFragment;

    .prologue
    .line 81
    iget-boolean v0, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mIsPause:Z

    return v0
.end method

.method static synthetic access$500(Lcom/samsung/musicplus/contents/extra/SelectionListFragment;)Landroid/view/MenuItem;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/extra/SelectionListFragment;

    .prologue
    .line 81
    iget-object v0, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mDoneItem:Landroid/view/MenuItem;

    return-object v0
.end method

.method static synthetic access$600(Lcom/samsung/musicplus/contents/extra/SelectionListFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/extra/SelectionListFragment;

    .prologue
    .line 81
    iget v0, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mListType:I

    return v0
.end method

.method static synthetic access$700(Lcom/samsung/musicplus/contents/extra/SelectionListFragment;)[J
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/extra/SelectionListFragment;

    .prologue
    .line 81
    iget-object v0, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mSelectedListId:[J

    return-object v0
.end method

.method static synthetic access$900(Lcom/samsung/musicplus/contents/extra/SelectionListFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/extra/SelectionListFragment;

    .prologue
    .line 81
    iget v0, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mText2Idx:I

    return v0
.end method

.method private getAlignedSelectedItemIds()[J
    .locals 6

    .prologue
    .line 719
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    check-cast v4, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;

    invoke-virtual {v4}, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->getSelectedIDList()Ljava/util/ArrayList;

    move-result-object v2

    .line 720
    .local v2, "idStates":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 721
    .local v0, "count":I
    new-array v3, v0, [J

    .line 723
    .local v3, "ids":[J
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 724
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    aput-wide v4, v3, v1

    .line 723
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 726
    :cond_0
    return-object v3
.end method

.method public static getNewInstance(IIIJ)Landroid/app/Fragment;
    .locals 3
    .param p0, "list"    # I
    .param p1, "listMode"    # I
    .param p2, "headerMode"    # I
    .param p3, "playlistId"    # J

    .prologue
    .line 138
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 139
    .local v0, "data":Landroid/os/Bundle;
    const-string v1, "playlist_id"

    invoke-virtual {v0, v1, p3, p4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 140
    invoke-static {v0, p0, p1, p2}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->setFragmentData(Landroid/os/Bundle;III)Landroid/app/Fragment;

    move-result-object v1

    return-object v1
.end method

.method public static getNewInstance(IIILjava/lang/String;J)Landroid/app/Fragment;
    .locals 2
    .param p0, "list"    # I
    .param p1, "listMode"    # I
    .param p2, "headerMode"    # I
    .param p3, "key"    # Ljava/lang/String;
    .param p4, "playlistId"    # J

    .prologue
    .line 120
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 121
    .local v0, "data":Landroid/os/Bundle;
    const-string v1, "key"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    const-string v1, "playlist_id"

    invoke-virtual {v0, v1, p4, p5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 123
    invoke-static {v0, p0, p1, p2}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->setFragmentData(Landroid/os/Bundle;III)Landroid/app/Fragment;

    move-result-object v1

    return-object v1
.end method

.method public static getNewInstance(IIILjava/lang/String;[JLjava/lang/String;)Landroid/app/Fragment;
    .locals 2
    .param p0, "list"    # I
    .param p1, "listMode"    # I
    .param p2, "headerMode"    # I
    .param p3, "key"    # Ljava/lang/String;
    .param p4, "selectedListId"    # [J
    .param p5, "additonalKey"    # Ljava/lang/String;

    .prologue
    .line 129
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 130
    .local v0, "data":Landroid/os/Bundle;
    const-string v1, "key"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    const-string v1, "selected_id"

    invoke-virtual {v0, v1, p4}, Landroid/os/Bundle;->putLongArray(Ljava/lang/String;[J)V

    .line 132
    const-string v1, "addtional_keyword"

    invoke-virtual {v0, v1, p5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    invoke-static {v0, p0, p1, p2}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->setFragmentData(Landroid/os/Bundle;III)Landroid/app/Fragment;

    move-result-object v1

    return-object v1
.end method

.method private getRealCursorIndex(Landroid/content/ContentResolver;[J)[J
    .locals 12
    .param p1, "cr"    # Landroid/content/ContentResolver;
    .param p2, "list"    # [J

    .prologue
    const/4 v9, 0x0

    .line 1076
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    .line 1077
    .local v11, "where":Ljava/lang/StringBuilder;
    const-string v0, "_id IN ("

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1078
    array-length v10, p2

    .line 1079
    .local v10, "size":I
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_0
    if-ge v8, v10, :cond_1

    .line 1080
    aget-wide v0, p2, v8

    invoke-virtual {v11, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 1081
    add-int/lit8 v0, v10, -0x1

    if-ge v8, v0, :cond_0

    .line 1082
    const-string v0, ","

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1079
    :cond_0
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 1085
    :cond_1
    const-string v0, ")"

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1088
    const/4 v6, 0x0

    .line 1090
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    sget-object v1, Lcom/samsung/musicplus/provider/MusicContents;->MUSIC_CONTENTS_URI:Landroid/net/Uri;

    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v3, "_id"

    aput-object v3, v2, v0

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const-string v5, "_id"

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1094
    if-eqz v6, :cond_4

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1095
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v10

    .line 1096
    new-array v9, v10, [J

    .line 1098
    .local v9, "index":[J
    const-string v0, "_id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v7

    .line 1100
    .local v7, "colidx":I
    const/4 v8, 0x0

    :goto_1
    if-ge v8, v10, :cond_2

    .line 1101
    invoke-interface {v6, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    aput-wide v0, v9, v8

    .line 1102
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1100
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 1107
    :cond_2
    if-eqz v6, :cond_3

    .line 1108
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1111
    .end local v7    # "colidx":I
    .end local v9    # "index":[J
    :cond_3
    :goto_2
    return-object v9

    .line 1107
    :cond_4
    if-eqz v6, :cond_3

    .line 1108
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_2

    .line 1107
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_5

    .line 1108
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_5
    throw v0
.end method

.method private handleMenuOk()V
    .locals 17

    .prologue
    .line 760
    invoke-direct/range {p0 .. p0}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->getAlignedSelectedItemIds()[J

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mSelectedListId:[J

    .line 761
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    .line 762
    .local v3, "a":Landroid/app/Activity;
    invoke-virtual {v3}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v13

    .line 763
    .local v13, "context":Landroid/content/Context;
    const-string v2, "handleMenuOk()"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "mListMode : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mListMode:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v2, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 764
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mListMode:I

    packed-switch v2, :pswitch_data_0

    .line 824
    :cond_0
    :goto_0
    :pswitch_0
    return-void

    .line 766
    :pswitch_1
    new-instance v2, Lcom/samsung/musicplus/util/FileOperationTask$AddToPlaylistTask;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mSelectedListId:[J

    move-object/from16 v0, p0

    iget-wide v5, v0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mPlaylistId:J

    const/4 v7, 0x1

    invoke-direct/range {v2 .. v7}, Lcom/samsung/musicplus/util/FileOperationTask$AddToPlaylistTask;-><init>(Landroid/app/Activity;[JJZ)V

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Void;

    invoke-virtual {v2, v7}, Lcom/samsung/musicplus/util/FileOperationTask$AddToPlaylistTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 769
    :pswitch_2
    new-instance v14, Landroid/content/Intent;

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const-class v7, Lcom/samsung/musicplus/contents/extra/MusicSelectListActivity;

    invoke-direct {v14, v2, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 770
    .local v14, "i":Landroid/content/Intent;
    const-string v2, "list_type"

    const v7, 0x10004

    invoke-virtual {v14, v2, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 771
    const-string v2, "list_mode"

    const/4 v7, 0x2

    invoke-virtual {v14, v2, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 773
    const-string v2, "header_mode"

    const/4 v7, 0x2

    invoke-virtual {v14, v2, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 774
    const-string v2, "selected_id"

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mSelectedListId:[J

    invoke-virtual {v14, v2, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[J)Landroid/content/Intent;

    .line 775
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->startActivity(Landroid/content/Intent;)V

    .line 776
    invoke-virtual {v3}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 779
    .end local v14    # "i":Landroid/content/Intent;
    :pswitch_3
    new-instance v2, Lcom/samsung/musicplus/dialog/DeleteDialogFragment;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mSelectedListId:[J

    move-object/from16 v0, p0

    iget v8, v0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mListType:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->getListSongCount(I)I

    move-result v8

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mListType:I

    const/4 v11, 0x1

    invoke-direct {v2, v7, v8, v9, v11}, Lcom/samsung/musicplus/dialog/DeleteDialogFragment;-><init>([JIIZ)V

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v7

    const-string v8, "deleteDialog"

    invoke-virtual {v2, v7, v8}, Lcom/samsung/musicplus/dialog/DeleteDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 783
    :pswitch_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mKeyWord:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 784
    .local v4, "albumId":J
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->getAbsListView()Landroid/widget/AbsListView;

    move-result-object v16

    .line 785
    .local v16, "lv":Landroid/widget/AbsListView;
    if-nez v16, :cond_1

    .line 786
    const-string v2, "handleMenuOk()"

    const-string v7, "mListMode : LIST_MODE_REMOVE, lv is null."

    invoke-static {v2, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 789
    :cond_1
    invoke-virtual/range {v16 .. v16}, Landroid/widget/AbsListView;->getCheckedItemIds()[J

    move-result-object v6

    .line 790
    .local v6, "checkedItem":[J
    new-instance v2, Lcom/samsung/musicplus/util/FileOperationTask$RemoveTask;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mSelectedListId:[J

    const/4 v8, 0x1

    invoke-direct/range {v2 .. v8}, Lcom/samsung/musicplus/util/FileOperationTask$RemoveTask;-><init>(Landroid/app/Activity;J[J[JZ)V

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Void;

    invoke-virtual {v2, v7}, Lcom/samsung/musicplus/util/FileOperationTask$RemoveTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0

    .line 793
    .end local v4    # "albumId":J
    .end local v6    # "checkedItem":[J
    .end local v16    # "lv":Landroid/widget/AbsListView;
    :pswitch_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mSelectedListId:[J

    const/4 v7, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v7}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->showPersonalMoveDialogFragment([JZ)V

    goto/16 :goto_0

    .line 796
    :pswitch_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mSelectedListId:[J

    const/4 v7, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v7}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->showPersonalMoveDialogFragment([JZ)V

    goto/16 :goto_0

    .line 799
    :pswitch_7
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mSelectedListId:[J

    const v8, 0x7f0d01d4

    invoke-static {v2, v7, v8}, Lcom/samsung/musicplus/util/KnoxGateManager;->showKnoxDialog(Landroid/app/Activity;[JI)V

    goto/16 :goto_0

    .line 804
    :pswitch_8
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mSelectedListId:[J

    invoke-static {v2, v7}, Lcom/samsung/musicplus/util/UiUtils;->checkFavorites(Landroid/content/Context;[J)[J

    move-result-object v10

    .line 806
    .local v10, "favoriteSelectionIds":[J
    new-instance v7, Lcom/samsung/musicplus/util/FileOperationTask$SmartFavoriteTask;

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mSelectedListId:[J

    const/4 v11, 0x0

    const/4 v12, 0x1

    invoke-direct/range {v7 .. v12}, Lcom/samsung/musicplus/util/FileOperationTask$SmartFavoriteTask;-><init>(Landroid/app/Activity;[J[JZZ)V

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v7, v2}, Lcom/samsung/musicplus/util/FileOperationTask$SmartFavoriteTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0

    .line 810
    .end local v10    # "favoriteSelectionIds":[J
    :pswitch_9
    new-instance v2, Lcom/samsung/musicplus/util/FileOperationTask$AddToNowPlayingTask;

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mSelectedListId:[J

    const/4 v9, 0x1

    invoke-direct {v2, v7, v8, v9}, Lcom/samsung/musicplus/util/FileOperationTask$AddToNowPlayingTask;-><init>(Landroid/app/Activity;[JZ)V

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Void;

    invoke-virtual {v2, v7}, Lcom/samsung/musicplus/util/FileOperationTask$AddToNowPlayingTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0

    .line 813
    :pswitch_a
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mList:I

    const v7, 0x2000d

    if-ne v2, v7, :cond_0

    .line 814
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->getAbsListView()Landroid/widget/AbsListView;

    move-result-object v1

    .line 815
    .local v1, "av":Landroid/widget/AbsListView;
    if-eqz v1, :cond_2

    .line 816
    invoke-virtual {v1}, Landroid/widget/AbsListView;->getCheckedItemIds()[J

    move-result-object v15

    .line 817
    .local v15, "ids":[J
    invoke-static {v13, v15}, Lcom/samsung/musicplus/provider/MusicContents$Audio$DeviceContents;->download(Landroid/content/Context;[J)V

    .line 819
    .end local v15    # "ids":[J
    :cond_2
    invoke-virtual {v3}, Landroid/app/Activity;->finish()V

    goto/16 :goto_0

    .line 764
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_a
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method private invalidateActionBarTitle()V
    .locals 14

    .prologue
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 363
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 364
    .local v0, "a":Landroid/app/Activity;
    if-nez v0, :cond_1

    .line 413
    :cond_0
    :goto_0
    return-void

    .line 368
    :cond_1
    iget v8, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mListMode:I

    const/4 v11, 0x2

    if-eq v8, v11, :cond_2

    iget v8, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mListMode:I

    const/4 v11, 0x6

    if-ne v8, v11, :cond_3

    .line 370
    :cond_2
    const v8, 0x7f1000a9

    invoke-virtual {v0, v8}, Landroid/app/Activity;->setTitle(I)V

    goto :goto_0

    .line 374
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->getAbsListView()Landroid/widget/AbsListView;

    move-result-object v3

    .line 375
    .local v3, "lv":Landroid/widget/AbsListView;
    if-eqz v3, :cond_0

    .line 376
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->isEnableSelect()Z

    move-result v8

    if-eqz v8, :cond_7

    .line 377
    invoke-virtual {v3}, Landroid/widget/AbsListView;->getCheckedItemCount()I

    move-result v5

    .line 378
    .local v5, "selectedCount":I
    invoke-virtual {v3}, Landroid/widget/AbsListView;->getCount()I

    move-result v2

    .line 380
    .local v2, "count":I
    const/4 v4, 0x0

    .line 381
    .local v4, "selectedAll":Z
    if-lez v2, :cond_4

    if-gt v2, v5, :cond_4

    .line 382
    const/4 v4, 0x1

    .line 384
    :cond_4
    if-nez v5, :cond_8

    move v7, v9

    .line 386
    .local v7, "unselected":Z
    :goto_1
    const v8, 0x7f0d00ab

    invoke-virtual {v0, v8}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    .line 387
    .local v1, "checkBox":Landroid/widget/CheckBox;
    if-eqz v1, :cond_5

    .line 388
    invoke-virtual {v1, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 391
    :cond_5
    iget-object v8, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mPopupMenu:Lcom/samsung/musicplus/widget/AnchorSizePopupMenu;

    if-eqz v8, :cond_7

    .line 392
    iget-object v8, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mPopupMenu:Lcom/samsung/musicplus/widget/AnchorSizePopupMenu;

    invoke-virtual {v8}, Lcom/samsung/musicplus/widget/AnchorSizePopupMenu;->clearMenu()V

    .line 393
    if-nez v4, :cond_6

    .line 394
    iget-object v8, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mPopupMenu:Lcom/samsung/musicplus/widget/AnchorSizePopupMenu;

    const v11, 0x7f0d01fd

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f100151

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v8, v11, v12}, Lcom/samsung/musicplus/widget/AnchorSizePopupMenu;->addMenu(ILjava/lang/String;)V

    .line 397
    :cond_6
    if-nez v7, :cond_7

    .line 398
    iget-object v8, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mPopupMenu:Lcom/samsung/musicplus/widget/AnchorSizePopupMenu;

    const v11, 0x7f0d01fe

    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f100056

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v8, v11, v12}, Lcom/samsung/musicplus/widget/AnchorSizePopupMenu;->addMenu(ILjava/lang/String;)V

    .line 404
    .end local v1    # "checkBox":Landroid/widget/CheckBox;
    .end local v2    # "count":I
    .end local v4    # "selectedAll":Z
    .end local v5    # "selectedCount":I
    .end local v7    # "unselected":Z
    :cond_7
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v8

    check-cast v8, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;

    invoke-virtual {v8}, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->getSelectedItemsCount()I

    move-result v6

    .line 407
    .local v6, "totalSelectedCount":I
    invoke-direct {p0, v6}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->setSelectedText(I)V

    .line 409
    iget-object v8, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mDoneItem:Landroid/view/MenuItem;

    if-eqz v8, :cond_0

    .line 410
    iget-object v8, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mDoneItem:Landroid/view/MenuItem;

    if-lez v6, :cond_9

    :goto_2
    invoke-interface {v8, v9}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto/16 :goto_0

    .end local v6    # "totalSelectedCount":I
    .restart local v2    # "count":I
    .restart local v4    # "selectedAll":Z
    .restart local v5    # "selectedCount":I
    :cond_8
    move v7, v10

    .line 384
    goto :goto_1

    .end local v2    # "count":I
    .end local v4    # "selectedAll":Z
    .end local v5    # "selectedCount":I
    .restart local v6    # "totalSelectedCount":I
    :cond_9
    move v9, v10

    .line 410
    goto :goto_2
.end method

.method private isEnableSelect()Z
    .locals 3

    .prologue
    .line 847
    const/4 v0, 0x0

    .line 848
    .local v0, "result":Z
    iget v1, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mHeaderMode:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 849
    iget v1, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mListType:I

    invoke-static {v1}, Lcom/samsung/musicplus/util/ListUtils;->isTrack(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 850
    const/4 v0, 0x1

    .line 859
    :cond_0
    :goto_0
    return v0

    .line 852
    :cond_1
    iget v1, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mListType:I

    invoke-static {v1}, Lcom/samsung/musicplus/util/ListUtils;->isPlaylistTabContent(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 853
    const/4 v0, 0x1

    goto :goto_0

    .line 854
    :cond_2
    iget v1, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mListType:I

    const v2, 0x10007

    if-ne v1, v2, :cond_0

    invoke-direct {p0}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->isSecretBoxMode()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 855
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private isSecretBoxMode()Z
    .locals 2

    .prologue
    .line 863
    iget v0, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mListMode:I

    const/16 v1, 0x11

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mListMode:I

    const/16 v1, 0x12

    if-ne v0, v1, :cond_1

    .line 865
    :cond_0
    const/4 v0, 0x1

    .line 867
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private makeAddNowPlayinglistItem(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 7
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 435
    new-instance v3, Landroid/database/MatrixCursor;

    sget-object v4, Lcom/samsung/musicplus/util/ListUtils;->PLAY_LIST_PROJECTION:[Ljava/lang/String;

    invoke-direct {v3, v4}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 436
    .local v3, "playListCursor":Landroid/database/MatrixCursor;
    sget-object v4, Lcom/samsung/musicplus/util/ListUtils;->PLAY_LIST_PROJECTION:[Ljava/lang/String;

    array-length v2, v4

    .line 437
    .local v2, "length":I
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 443
    .local v1, "context":Landroid/content/Context;
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getListItemCount()I

    move-result v4

    if-eqz v4, :cond_0

    .line 444
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 445
    .local v0, "addtoPlaylist":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    const-wide/16 v4, -0x11

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 446
    const v4, 0x7f10000c

    invoke-virtual {v1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 447
    const-string v4, "@"

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 448
    invoke-virtual {v3, v0}, Landroid/database/MatrixCursor;->addRow(Ljava/lang/Iterable;)V

    .line 451
    .end local v0    # "addtoPlaylist":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    :cond_0
    new-instance v4, Landroid/database/MergeCursor;

    const/4 v5, 0x2

    new-array v5, v5, [Landroid/database/Cursor;

    const/4 v6, 0x0

    aput-object v3, v5, v6

    const/4 v6, 0x1

    aput-object p1, v5, v6

    invoke-direct {v4, v5}, Landroid/database/MergeCursor;-><init>([Landroid/database/Cursor;)V

    return-object v4
.end method

.method private refreshSelectedList()V
    .locals 8

    .prologue
    .line 1059
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 1060
    .local v0, "a":Landroid/app/Activity;
    if-eqz v0, :cond_0

    instance-of v5, v0, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;

    if-nez v5, :cond_1

    .line 1073
    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object v5, v0

    .line 1063
    check-cast v5, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;

    invoke-virtual {v5}, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->getSelectedIDList()Ljava/util/ArrayList;

    move-result-object v2

    .line 1064
    .local v2, "ids":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v5

    new-array v3, v5, [J

    .line 1065
    .local v3, "list":[J
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v1, v5, :cond_2

    .line 1066
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    aput-wide v6, v3, v1

    .line 1065
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1068
    :cond_2
    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    invoke-direct {p0, v5, v3}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->getRealCursorIndex(Landroid/content/ContentResolver;[J)[J

    move-result-object v4

    .line 1069
    .local v4, "realList":[J
    if-eqz v4, :cond_0

    .line 1072
    invoke-direct {p0, v3, v4}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->removeDeletedItemsInDb([J[J)V

    goto :goto_0
.end method

.method private registerContentObserver(Landroid/net/Uri;)V
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 1047
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 1048
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mIsRegistered:Z

    .line 1049
    return-void
.end method

.method private removeDeletedItemsInDb([J[J)V
    .locals 6
    .param p1, "prevList"    # [J
    .param p2, "realList"    # [J

    .prologue
    .line 1115
    array-length v4, p1

    add-int/lit8 v1, v4, -0x1

    .local v1, "i":I
    :goto_0
    if-ltz v1, :cond_1

    .line 1116
    aget-wide v2, p1, v1

    .line 1117
    .local v2, "trackid":J
    invoke-static {p2, v2, v3}, Ljava/util/Arrays;->binarySearch([JJ)I

    move-result v0

    .line 1118
    .local v0, "crsridx":I
    if-gez v0, :cond_0

    .line 1119
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    check-cast v4, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;

    const/4 v5, 0x0

    invoke-virtual {v4, v5, v2, v3}, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->updateSelectedIDs(ZJ)V

    .line 1115
    :cond_0
    add-int/lit8 v1, v1, -0x1

    goto :goto_0

    .line 1123
    .end local v0    # "crsridx":I
    .end local v2    # "trackid":J
    :cond_1
    return-void
.end method

.method private static setFragmentData(Landroid/os/Bundle;III)Landroid/app/Fragment;
    .locals 2
    .param p0, "data"    # Landroid/os/Bundle;
    .param p1, "list"    # I
    .param p2, "listMode"    # I
    .param p3, "headerMode"    # I

    .prologue
    .line 144
    const-string v1, "list"

    invoke-virtual {p0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 145
    const-string v1, "mode"

    invoke-virtual {p0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 146
    const-string v1, "header"

    invoke-virtual {p0, v1, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 148
    const v1, 0x2000d

    if-ne p1, v1, :cond_0

    .line 150
    new-instance v0, Lcom/samsung/musicplus/contents/devices/DeviceContentsSelectionListFragment;

    invoke-direct {v0}, Lcom/samsung/musicplus/contents/devices/DeviceContentsSelectionListFragment;-><init>()V

    .line 154
    .local v0, "fg":Landroid/app/Fragment;
    :goto_0
    invoke-virtual {v0, p0}, Landroid/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 155
    return-object v0

    .line 152
    .end local v0    # "fg":Landroid/app/Fragment;
    :cond_0
    new-instance v0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;

    invoke-direct {v0}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;-><init>()V

    .restart local v0    # "fg":Landroid/app/Fragment;
    goto :goto_0
.end method

.method private setListSelectable(Z)V
    .locals 2
    .param p1, "isEnableSelect"    # Z

    .prologue
    .line 352
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->getAbsListView()Landroid/widget/AbsListView;

    move-result-object v0

    .line 353
    .local v0, "lv":Landroid/widget/AbsListView;
    if-eqz v0, :cond_0

    .line 354
    if-eqz p1, :cond_1

    .line 355
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setChoiceMode(I)V

    .line 360
    :cond_0
    :goto_0
    return-void

    .line 357
    :cond_1
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setChoiceMode(I)V

    goto :goto_0
.end method

.method private setPenSelectionState()V
    .locals 2

    .prologue
    .line 307
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->getAbsListView()Landroid/widget/AbsListView;

    move-result-object v0

    .line 308
    .local v0, "alv":Landroid/widget/AbsListView;
    if-eqz v0, :cond_0

    .line 309
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->isEnableSelect()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 310
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setEnableDragBlock(Z)V

    .line 311
    iget-object v1, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mTwMultiSelectedListener:Landroid/widget/AdapterView$OnTwMultiSelectedListener;

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setTwMultiSelectedListener(Landroid/widget/AdapterView$OnTwMultiSelectedListener;)V

    .line 316
    :cond_0
    :goto_0
    return-void

    .line 313
    :cond_1
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setEnableDragBlock(Z)V

    goto :goto_0
.end method

.method private setSelectedText(I)V
    .locals 8
    .param p1, "count"    # I

    .prologue
    const v7, 0x7f1000de

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 416
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 417
    .local v0, "a":Landroid/app/Activity;
    new-array v3, v5, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v0, v7, v3}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 426
    .local v2, "selectedCount":Ljava/lang/String;
    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v3

    const v4, 0x7f0d00ac

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 428
    .local v1, "actionBarTitle":Landroid/widget/TextView;
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    new-array v4, v5, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v3, v7, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 432
    return-void
.end method

.method private setSelectionViewEnable()V
    .locals 0

    .prologue
    .line 304
    return-void
.end method

.method private showPersonalMoveDialogFragment([JZ)V
    .locals 7
    .param p1, "items"    # [J
    .param p2, "isMove"    # Z

    .prologue
    const/4 v6, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 828
    iget v4, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mListType:I

    const v5, 0x20007

    if-ne v4, v5, :cond_0

    move v1, v2

    .line 829
    .local v1, "removeFromNowPlaying":Z
    :goto_0
    iget v4, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mListType:I

    invoke-static {v4}, Lcom/samsung/musicplus/util/ListUtils;->isTab(I)Z

    move-result v4

    if-eqz v4, :cond_1

    iget v4, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mListType:I

    invoke-static {v4}, Lcom/samsung/musicplus/util/ListUtils;->getListContentType(I)I

    move-result v4

    const/4 v5, 0x7

    if-ne v4, v5, :cond_1

    .line 831
    invoke-static {p1, p2, v2, v6, v1}, Lcom/samsung/musicplus/dialog/PersonalMoveDialogFragment;->getInstance([JZZLjava/lang/String;Z)Landroid/app/DialogFragment;

    move-result-object v0

    .line 837
    .local v0, "moveDialog":Landroid/app/DialogFragment;
    :goto_1
    invoke-virtual {v0, v3}, Landroid/app/DialogFragment;->setCancelable(Z)V

    .line 838
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "moveDialog"

    invoke-virtual {v0, v2, v3}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 839
    return-void

    .end local v0    # "moveDialog":Landroid/app/DialogFragment;
    .end local v1    # "removeFromNowPlaying":Z
    :cond_0
    move v1, v3

    .line 828
    goto :goto_0

    .line 834
    .restart local v1    # "removeFromNowPlaying":Z
    :cond_1
    invoke-static {p1, p2, v3, v6, v1}, Lcom/samsung/musicplus/dialog/PersonalMoveDialogFragment;->getInstance([JZZLjava/lang/String;Z)Landroid/app/DialogFragment;

    move-result-object v0

    .restart local v0    # "moveDialog":Landroid/app/DialogFragment;
    goto :goto_1
.end method

.method private unregisterContentObserver()V
    .locals 2

    .prologue
    .line 1052
    iget-boolean v0, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mIsRegistered:Z

    if-eqz v0, :cond_0

    .line 1053
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 1054
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mIsRegistered:Z

    .line 1056
    :cond_0
    return-void
.end method


# virtual methods
.method protected addOtherView(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 1
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 327
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;->addOtherView(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method protected createAdapter()Landroid/widget/CursorAdapter;
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 220
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    .line 221
    .local v2, "a":Landroid/app/Activity;
    iget v0, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mListType:I

    invoke-static {v0}, Lcom/samsung/musicplus/util/ListUtils;->isGridType(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 222
    new-instance v0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter;

    const v3, 0x7f040029

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter;-><init>(Lcom/samsung/musicplus/contents/extra/SelectionListFragment;Landroid/content/Context;ILandroid/database/Cursor;I)V

    .line 224
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter;

    const v3, 0x7f040033

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter;-><init>(Lcom/samsung/musicplus/contents/extra/SelectionListFragment;Landroid/content/Context;ILandroid/database/Cursor;I)V

    goto :goto_0
.end method

.method protected initializeList()V
    .locals 1

    .prologue
    .line 229
    iget v0, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mList:I

    invoke-static {v0}, Lcom/samsung/musicplus/util/ListUtils;->isGridType(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->setViewType(I)V

    .line 230
    invoke-super {p0}, Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;->initializeList()V

    .line 231
    return-void

    .line 229
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected isEnableListShuffle(I)Z
    .locals 1
    .param p1, "list"    # I

    .prologue
    .line 873
    const/4 v0, 0x0

    return v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 166
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 167
    .local v0, "data":Landroid/os/Bundle;
    const-string v1, "list"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mListType:I

    .line 168
    const-string v1, "mode"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mListMode:I

    .line 169
    const-string v1, "key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mKeyWord:Ljava/lang/String;

    .line 170
    const-string v1, "header"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mHeaderMode:I

    .line 171
    const-string v1, "playlist_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mPlaylistId:J

    .line 172
    const-string v1, "selected_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mSelectedListId:[J

    .line 173
    const-string v1, "addtional_keyword"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mAdditionalKeyWord:Ljava/lang/String;

    .line 174
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 175
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->isEnableSelect()Z

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->setListSelectable(Z)V

    .line 176
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->setPenSelectionState()V

    .line 177
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->invalidateActionBarTitle()V

    .line 178
    new-instance v1, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$1;

    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    invoke-direct {v1, p0, v2}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$1;-><init>(Lcom/samsung/musicplus/contents/extra/SelectionListFragment;Landroid/os/Handler;)V

    iput-object v1, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mObserver:Landroid/database/ContentObserver;

    .line 188
    sget-object v1, Lcom/samsung/musicplus/provider/MusicContents;->MUSIC_CONTENTS_URI:Landroid/net/Uri;

    invoke-direct {p0, v1}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->registerContentObserver(Landroid/net/Uri;)V

    .line 189
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 160
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 161
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->setHasOptionsMenu(Z)V

    .line 162
    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/content/Loader;
    .locals 3
    .param p1, "id"    # I
    .param p2, "args"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 235
    iget v0, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mListMode:I

    packed-switch v0, :pswitch_data_0

    .line 260
    :goto_0
    iget-object v0, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mAdditionalKeyWord:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 261
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mQueryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    iget-object v2, v1, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->selection:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " AND "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mAdditionalKeyWord:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->selection:Ljava/lang/String;

    .line 263
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;->onCreateLoader(ILandroid/os/Bundle;)Landroid/content/Loader;

    move-result-object v0

    return-object v0

    .line 238
    :pswitch_0
    iget v0, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mListType:I

    invoke-static {v0}, Lcom/samsung/musicplus/util/ListUtils;->isTab(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 239
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mQueryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    iget-object v2, v1, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->selection:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " AND is_secretbox=0"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->selection:Ljava/lang/String;

    goto :goto_0

    .line 243
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mQueryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    iget-object v2, v1, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->selection:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " AND (is_secretbox=0 )"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->selection:Ljava/lang/String;

    goto :goto_0

    .line 248
    :pswitch_1
    iget v0, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mListType:I

    invoke-static {v0}, Lcom/samsung/musicplus/util/ListUtils;->isTab(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 249
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mQueryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    iget-object v2, v1, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->selection:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " AND is_secretbox=1"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->selection:Ljava/lang/String;

    goto/16 :goto_0

    .line 253
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mQueryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    iget-object v2, v1, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->selection:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " AND (is_secretbox=1 )"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->selection:Ljava/lang/String;

    goto/16 :goto_0

    .line 235
    nop

    :pswitch_data_0
    .packed-switch 0x11
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 4
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "inflater"    # Landroid/view/MenuInflater;

    .prologue
    const/4 v2, 0x1

    .line 731
    iget v1, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mHeaderMode:I

    if-ne v1, v2, :cond_0

    .line 732
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v1

    const/high16 v3, 0x7f120000

    invoke-virtual {v1, v3, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 733
    const v1, 0x7f0d01ca

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mDoneItem:Landroid/view/MenuItem;

    .line 734
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    check-cast v1, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;

    invoke-virtual {v1}, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->getSelectedItemsCount()I

    move-result v0

    .line 736
    .local v0, "totalSelectedCount":I
    iget-object v3, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mDoneItem:Landroid/view/MenuItem;

    if-lez v0, :cond_1

    move v1, v2

    :goto_0
    invoke-interface {v3, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 742
    .end local v0    # "totalSelectedCount":I
    :cond_0
    return-void

    .line 736
    .restart local v0    # "totalSelectedCount":I
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 214
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->unregisterContentObserver()V

    .line 215
    invoke-super {p0}, Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;->onDestroy()V

    .line 216
    return-void
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 208
    iget-object v0, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mUIHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 209
    invoke-super {p0}, Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;->onDestroyView()V

    .line 210
    return-void
.end method

.method public onListItemClick(Landroid/widget/AbsListView;Landroid/view/View;IJ)V
    .locals 16
    .param p1, "lv"    # Landroid/widget/AbsListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    .line 675
    if-eqz p1, :cond_0

    .line 676
    invoke-direct/range {p0 .. p0}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->isEnableSelect()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 677
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    check-cast v3, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;

    move-object/from16 v0, p1

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->isItemChecked(I)Z

    move-result v4

    move-object/from16 v0, p0

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->getAudioId(I)J

    move-result-wide v6

    invoke-virtual {v3, v4, v6, v7}, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->updateSelectedIDs(ZJ)V

    .line 679
    invoke-direct/range {p0 .. p0}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->invalidateActionBarTitle()V

    .line 711
    :cond_0
    :goto_0
    return-void

    .line 681
    :cond_1
    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/widget/AbsListView;->setChoiceMode(I)V

    .line 682
    move-object/from16 v0, p0

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->getKeyWord(I)Ljava/lang/String;

    move-result-object v5

    .line 683
    .local v5, "key":Ljava/lang/String;
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mListMode:I

    const/4 v4, 0x3

    if-eq v3, v4, :cond_2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mListMode:I

    const/16 v4, 0x14

    if-eq v3, v4, :cond_2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mListMode:I

    const/16 v4, 0x15

    if-ne v3, v4, :cond_3

    .line 686
    :cond_2
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mListType:I

    invoke-static {v3}, Lcom/samsung/musicplus/util/ListUtils;->getSubTrackList(I)I

    move-result v2

    .line 687
    .local v2, "subList":I
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->isResumed()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 688
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mListMode:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mHeaderMode:I

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mPlaylistId:J

    invoke-static/range {v2 .. v7}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->getNewInstance(IIILjava/lang/String;J)Landroid/app/Fragment;

    move-result-object v13

    .line 690
    .local v13, "fg":Landroid/app/Fragment;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v14

    .line 691
    .local v14, "ft":Landroid/app/FragmentTransaction;
    const v3, 0x1020011

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v14, v3, v13, v4}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 692
    const/16 v3, 0x1001

    invoke-virtual {v14, v3}, Landroid/app/FragmentTransaction;->setTransition(I)Landroid/app/FragmentTransaction;

    .line 693
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v14, v3}, Landroid/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 694
    invoke-virtual {v14}, Landroid/app/FragmentTransaction;->commit()I

    goto :goto_0

    .line 696
    .end local v2    # "subList":I
    .end local v13    # "fg":Landroid/app/Fragment;
    .end local v14    # "ft":Landroid/app/FragmentTransaction;
    :cond_3
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mListMode:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_0

    .line 697
    const-wide/16 v6, -0x11

    cmp-long v3, p4, v6

    if-nez v3, :cond_5

    .line 698
    new-instance v3, Lcom/samsung/musicplus/util/FileOperationTask$AddToNowPlayingTask;

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mSelectedListId:[J

    const/4 v7, 0x1

    invoke-direct {v3, v4, v6, v7}, Lcom/samsung/musicplus/util/FileOperationTask$AddToNowPlayingTask;-><init>(Landroid/app/Activity;[JZ)V

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Void;

    invoke-virtual {v3, v4}, Lcom/samsung/musicplus/util/FileOperationTask$AddToNowPlayingTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 707
    :cond_4
    :goto_1
    invoke-static {}, Lcom/samsung/musicplus/widget/fragment/ActionModeRemoteHandler;->getInstance()Lcom/samsung/musicplus/widget/fragment/ActionModeRemoteHandler;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/musicplus/widget/fragment/ActionModeRemoteHandler;->notifyActionModeFinishAction()V

    goto/16 :goto_0

    .line 700
    :cond_5
    invoke-static {v5}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v15

    .line 701
    .local v15, "value":Ljava/lang/Long;
    invoke-virtual {v15}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v3, v6, v8

    if-lez v3, :cond_4

    .line 702
    new-instance v7, Lcom/samsung/musicplus/util/FileOperationTask$AddToPlaylistTask;

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mSelectedListId:[J

    invoke-virtual {v15}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    const/4 v12, 0x1

    invoke-direct/range {v7 .. v12}, Lcom/samsung/musicplus/util/FileOperationTask$AddToPlaylistTask;-><init>(Landroid/app/Activity;[JJZ)V

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Void;

    invoke-virtual {v7, v3}, Lcom/samsung/musicplus/util/FileOperationTask$AddToPlaylistTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 704
    move-object/from16 v0, p0

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->launchTrackActivity(I)V

    goto :goto_1
.end method

.method public onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V
    .locals 3
    .param p2, "data"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 268
    .local p1, "loader":Landroid/content/Loader;, "Landroid/content/Loader<Landroid/database/Cursor;>;"
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->isEnableSelect()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 269
    iget-object v1, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mUIHandler:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 271
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;->onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V

    .line 278
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->setSelectionViewEnable()V

    .line 279
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->getAbsListView()Landroid/widget/AbsListView;

    move-result-object v0

    .line 280
    .local v0, "lv":Landroid/widget/AbsListView;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/samsung/musicplus/widget/list/MusicListView;

    if-eqz v1, :cond_1

    .line 287
    check-cast v0, Lcom/samsung/musicplus/widget/list/MusicListView;

    .end local v0    # "lv":Landroid/widget/AbsListView;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/widget/list/MusicListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 289
    :cond_1
    return-void
.end method

.method public bridge synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Landroid/content/Loader;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 81
    check-cast p2, Landroid/database/Cursor;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 746
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 756
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 748
    :pswitch_0
    invoke-static {}, Lcom/samsung/musicplus/widget/fragment/ActionModeRemoteHandler;->getInstance()Lcom/samsung/musicplus/widget/fragment/ActionModeRemoteHandler;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/musicplus/widget/fragment/ActionModeRemoteHandler;->notifyActionModeFinishAction()V

    .line 749
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->handleMenuOk()V

    goto :goto_0

    .line 746
    :pswitch_data_0
    .packed-switch 0x7f0d01ca
        :pswitch_0
    .end packed-switch
.end method

.method public resetSelectedId()V
    .locals 1

    .prologue
    .line 668
    iget-boolean v0, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mIsPause:Z

    if-nez v0, :cond_0

    .line 669
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->setSelectAll(Z)V

    .line 671
    :cond_0
    return-void
.end method

.method protected setHeaderView()V
    .locals 15

    .prologue
    const/4 v14, 0x0

    const v13, 0x3f19999a    # 0.6f

    .line 461
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 463
    .local v0, "a":Landroid/app/Activity;
    iget v10, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mHeaderMode:I

    const/4 v11, 0x1

    if-ne v10, v11, :cond_3

    .line 519
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v10

    invoke-static {v10}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v10

    const v11, 0x7f04002d

    const/4 v12, 0x0

    invoke-virtual {v10, v11, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 521
    .local v3, "actionModeView":Landroid/view/View;
    new-instance v10, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v11, -0x2

    const/4 v12, -0x1

    invoke-direct {v10, v11, v12}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v3, v10}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 523
    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    .line 524
    .local v1, "ab":Landroid/app/ActionBar;
    if-eqz v1, :cond_0

    .line 525
    const/16 v10, 0x10

    invoke-virtual {v1, v10}, Landroid/app/ActionBar;->setDisplayOptions(I)V

    .line 526
    invoke-virtual {v1, v3}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;)V

    .line 528
    :cond_0
    const v10, 0x7f0d00a9

    invoke-virtual {v3, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    .line 529
    .local v9, "selectAllLayout":Landroid/view/View;
    const v10, 0x7f0d00aa

    invoke-virtual {v3, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    .line 530
    .local v8, "selectAll":Landroid/view/View;
    const v10, 0x7f0d00ab

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/CheckBox;

    iput-object v10, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mCheckBox:Landroid/widget/CheckBox;

    .line 531
    invoke-virtual {v9, v14}, Landroid/view/View;->setVisibility(I)V

    .line 532
    iget v10, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mListType:I

    invoke-static {v10}, Lcom/samsung/musicplus/util/ListUtils;->isTab(I)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 533
    invoke-virtual {v9, v14}, Landroid/view/View;->setEnabled(Z)V

    .line 534
    iget-object v10, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v10, v13}, Landroid/widget/CheckBox;->setAlpha(F)V

    .line 535
    const v10, 0x7f0d00ac

    invoke-virtual {v3, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 536
    .local v2, "actionBarTitle":Landroid/widget/TextView;
    invoke-virtual {v2, v13}, Landroid/widget/TextView;->setAlpha(F)V

    .line 553
    .end local v2    # "actionBarTitle":Landroid/widget/TextView;
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v10

    invoke-static {v10, v8}, Lcom/samsung/musicplus/util/UiUtils;->setReferenceListBackgroundWithHover(Landroid/content/Context;Landroid/view/View;)V

    .line 595
    .end local v1    # "ab":Landroid/app/ActionBar;
    .end local v3    # "actionModeView":Landroid/view/View;
    .end local v8    # "selectAll":Landroid/view/View;
    .end local v9    # "selectAllLayout":Landroid/view/View;
    :cond_1
    :goto_1
    return-void

    .line 538
    .restart local v1    # "ab":Landroid/app/ActionBar;
    .restart local v3    # "actionModeView":Landroid/view/View;
    .restart local v8    # "selectAll":Landroid/view/View;
    .restart local v9    # "selectAllLayout":Landroid/view/View;
    :cond_2
    new-instance v10, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$5;

    invoke-direct {v10, p0}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$5;-><init>(Lcom/samsung/musicplus/contents/extra/SelectionListFragment;)V

    invoke-virtual {v8, v10}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    .line 570
    .end local v1    # "ab":Landroid/app/ActionBar;
    .end local v3    # "actionModeView":Landroid/view/View;
    .end local v8    # "selectAll":Landroid/view/View;
    .end local v9    # "selectAllLayout":Landroid/view/View;
    :cond_3
    iget v10, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mHeaderMode:I

    const/4 v11, 0x2

    if-ne v10, v11, :cond_1

    .line 571
    const v10, 0x7f0d0086

    invoke-virtual {v0, v10}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 572
    .local v5, "headerView":Landroid/view/View;
    instance-of v10, v5, Landroid/view/ViewStub;

    if-eqz v10, :cond_4

    move-object v10, v5

    .line 573
    check-cast v10, Landroid/view/ViewStub;

    invoke-virtual {v10}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 575
    :cond_4
    const v10, 0x7f0d0134

    invoke-virtual {v0, v10}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 576
    .local v6, "headerViewNowPlaying":Landroid/view/View;
    instance-of v10, v5, Landroid/view/ViewStub;

    if-eqz v10, :cond_5

    .line 577
    check-cast v6, Landroid/view/ViewStub;

    .end local v6    # "headerViewNowPlaying":Landroid/view/View;
    invoke-virtual {v6}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 579
    :cond_5
    const v10, 0x7f0d0093

    invoke-virtual {v0, v10}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 580
    .local v4, "create":Landroid/view/View;
    if-eqz v4, :cond_6

    .line 581
    iget-object v10, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mHeaderButtonClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v4, v10}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 583
    :cond_6
    const v10, 0x7f0d00a8

    invoke-virtual {v0, v10}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v7

    .line 584
    .local v7, "nowplaying":Landroid/view/View;
    if-eqz v7, :cond_7

    .line 585
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getListItemCount()I

    move-result v10

    if-eqz v10, :cond_8

    .line 586
    iget-object v10, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mHeaderButtonClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v7, v10}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 591
    :cond_7
    :goto_2
    if-eqz v4, :cond_1

    .line 592
    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v10

    invoke-static {v10, v4}, Lcom/samsung/musicplus/util/UiUtils;->setReferenceListBackgroundWithHover(Landroid/content/Context;Landroid/view/View;)V

    goto :goto_1

    .line 588
    :cond_8
    const/16 v10, 0x8

    invoke-virtual {v7, v10}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2
.end method

.method public setSelectAll(Z)V
    .locals 6
    .param p1, "isSeletAll"    # Z

    .prologue
    .line 655
    iget v2, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mCountOfData:I

    if-lez v2, :cond_0

    .line 656
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->getAbsListView()Landroid/widget/AbsListView;

    move-result-object v1

    .line 657
    .local v1, "list":Landroid/widget/AbsListView;
    if-eqz v1, :cond_0

    .line 658
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget v2, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mCountOfData:I

    if-ge v0, v2, :cond_0

    .line 659
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    check-cast v2, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->getAudioId(I)J

    move-result-wide v4

    invoke-virtual {v2, p1, v4, v5}, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->updateSelectedIDs(ZJ)V

    .line 661
    invoke-virtual {v1, v0, p1}, Landroid/widget/AbsListView;->setItemChecked(IZ)V

    .line 658
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 665
    .end local v0    # "i":I
    .end local v1    # "list":Landroid/widget/AbsListView;
    :cond_0
    return-void
.end method

.method public updateFragment()V
    .locals 2

    .prologue
    .line 334
    iget v0, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mHeaderMode:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 349
    :cond_0
    return-void
.end method
