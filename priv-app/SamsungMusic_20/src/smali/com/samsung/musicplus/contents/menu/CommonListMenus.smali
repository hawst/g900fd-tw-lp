.class public Lcom/samsung/musicplus/contents/menu/CommonListMenus;
.super Lcom/samsung/musicplus/common/menu/AbstractMusicMenus;
.source "CommonListMenus.java"

# interfaces
.implements Lcom/samsung/musicplus/util/UiUtils$Defs;


# static fields
.field public static final LOG_TAG:Ljava/lang/String; = "CommonListMenu"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/samsung/musicplus/common/menu/AbstractMusicMenus;-><init>()V

    return-void
.end method

.method private launchDeregisterDevices(Landroid/content/Context;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const v7, 0x7f100053

    .line 355
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 356
    .local v1, "dialogBuilder":Landroid/app/AlertDialog$Builder;
    new-instance v3, Landroid/app/ProgressDialog;

    invoke-direct {v3, p1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 357
    .local v3, "mProgressDialog":Landroid/app/ProgressDialog;
    invoke-static {p1}, Lcom/samsung/android/sdk/samsunglink/SlinkFrameworkUserPortal;->getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/samsunglink/SlinkFrameworkUserPortal;

    move-result-object v4

    .line 358
    .local v4, "mSLinkUtil":Lcom/samsung/android/sdk/samsunglink/SlinkFrameworkUserPortal;
    move-object v2, p1

    .line 359
    .local v2, "mContext":Landroid/content/Context;
    invoke-virtual {v1, v7}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 360
    const v5, 0x7f100054

    invoke-virtual {v1, v5}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 361
    const v5, 0x7f10003a

    invoke-virtual {p1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Lcom/samsung/musicplus/contents/menu/CommonListMenus$1;

    invoke-direct {v6, p0}, Lcom/samsung/musicplus/contents/menu/CommonListMenus$1;-><init>(Lcom/samsung/musicplus/contents/menu/CommonListMenus;)V

    invoke-virtual {v1, v5, v6}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 366
    invoke-virtual {p1, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-instance v6, Lcom/samsung/musicplus/contents/menu/CommonListMenus$2;

    invoke-direct {v6, p0, v3, v2, v4}, Lcom/samsung/musicplus/contents/menu/CommonListMenus$2;-><init>(Lcom/samsung/musicplus/contents/menu/CommonListMenus;Landroid/app/ProgressDialog;Landroid/content/Context;Lcom/samsung/android/sdk/samsunglink/SlinkFrameworkUserPortal;)V

    invoke-virtual {v1, v5, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 389
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 390
    .local v0, "dialog":Landroid/app/AlertDialog;
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 391
    return-void
.end method

.method private launchLgtRintone(Landroid/content/Context;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 400
    sget-boolean v8, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_CHECK_KOR_LGT:Z

    if-nez v8, :cond_0

    .line 431
    :goto_0
    return-void

    .line 404
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v7

    .line 405
    .local v7, "pm":Landroid/content/pm/PackageManager;
    const/16 v8, 0x2000

    invoke-virtual {v7, v8}, Landroid/content/pm/PackageManager;->getInstalledPackages(I)Ljava/util/List;

    move-result-object v0

    .line 407
    .local v0, "appinfo":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    const/4 v5, 0x0

    .line 408
    .local v5, "isinstalled":Z
    const/4 v2, 0x0

    .local v2, "count":I
    :goto_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v8

    if-ge v2, v8, :cond_1

    .line 409
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/content/pm/PackageInfo;

    .line 410
    .local v6, "pi":Landroid/content/pm/PackageInfo;
    iget-object v1, v6, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    .line 411
    .local v1, "appname":Ljava/lang/String;
    const-string v8, "com.lgu"

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 412
    const/4 v5, 0x1

    .line 416
    .end local v1    # "appname":Ljava/lang/String;
    .end local v6    # "pi":Landroid/content/pm/PackageInfo;
    :cond_1
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    .line 417
    .local v4, "intent":Landroid/content/Intent;
    if-eqz v5, :cond_3

    .line 418
    const-string v8, "com.lgu"

    const-string v9, "com.lgu.page.LoadingPage"

    invoke-virtual {v4, v8, v9}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 425
    :goto_2
    :try_start_0
    invoke-virtual {p1, v4}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 426
    :catch_0
    move-exception v3

    .line 427
    .local v3, "e":Landroid/content/ActivityNotFoundException;
    const-string v8, "CommonListMenu"

    const-string v9, "Activity Not found!!!"

    invoke-static {v8, v9}, Lcom/samsung/musicplus/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 428
    invoke-virtual {v3}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    .line 429
    const-string v8, "Target Activity Not Found"

    const/4 v9, 0x1

    invoke-static {p1, v8, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v8

    invoke-virtual {v8}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 408
    .end local v3    # "e":Landroid/content/ActivityNotFoundException;
    .end local v4    # "intent":Landroid/content/Intent;
    .restart local v1    # "appname":Ljava/lang/String;
    .restart local v6    # "pi":Landroid/content/pm/PackageInfo;
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 420
    .end local v1    # "appname":Ljava/lang/String;
    .end local v6    # "pi":Landroid/content/pm/PackageInfo;
    .restart local v4    # "intent":Landroid/content/Intent;
    :cond_3
    const-string v8, "com.lguplus.appstore"

    const-string v9, "com.lguplus.appstore.Store"

    invoke-virtual {v4, v8, v9}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 421
    const/high16 v8, 0x10000000

    invoke-virtual {v4, v8}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 422
    const-string v8, "payload"

    const-string v9, "PID=QA6010054881"

    invoke-virtual {v4, v8, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_2
.end method

.method private launchMusicSquare(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 393
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/samsung/musicplus/contents/square/MusicSquareActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 395
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "list_type"

    const v2, 0x2000a

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 396
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 397
    return-void
.end method

.method private launchSamsungMusic(Landroid/content/Context;)V
    .locals 13
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/high16 v12, 0x14000000

    .line 434
    const-string v1, "com.samsung.music"

    .line 435
    .local v1, "SAMSUNGMUSIC_PACKAGE_NAME":Ljava/lang/String;
    const-string v0, "com.samsung.music.ui.MainViewPagerActivity"

    .line 438
    .local v0, "SAMSUNGMUSIC_MAIN_ACTIVITY":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v9

    .line 439
    .local v9, "pm":Landroid/content/pm/PackageManager;
    const/16 v10, 0x2000

    invoke-virtual {v9, v10}, Landroid/content/pm/PackageManager;->getInstalledPackages(I)Ljava/util/List;

    move-result-object v2

    .line 441
    .local v2, "appinfo":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PackageInfo;>;"
    const/4 v7, 0x0

    .line 442
    .local v7, "isinstalled":Z
    const/4 v4, 0x0

    .local v4, "count":I
    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v10

    if-ge v4, v10, :cond_0

    .line 443
    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Landroid/content/pm/PackageInfo;

    .line 444
    .local v8, "pi":Landroid/content/pm/PackageInfo;
    iget-object v3, v8, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    .line 445
    .local v3, "appname":Ljava/lang/String;
    const-string v10, "com.samsung.music"

    invoke-virtual {v3, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 446
    const/4 v7, 0x1

    .line 451
    .end local v3    # "appname":Ljava/lang/String;
    .end local v8    # "pi":Landroid/content/pm/PackageInfo;
    :cond_0
    if-eqz v7, :cond_2

    .line 452
    new-instance v6, Landroid/content/Intent;

    const-string v10, "android.intent.action.MAIN"

    invoke-direct {v6, v10}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 453
    .local v6, "intent":Landroid/content/Intent;
    const-string v10, "android.intent.category.LAUNCHER"

    invoke-virtual {v6, v10}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 454
    const-string v10, "com.samsung.music"

    const-string v11, "com.samsung.music.ui.MainViewPagerActivity"

    invoke-virtual {v6, v10, v11}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 455
    invoke-virtual {v6, v12}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 462
    :goto_1
    :try_start_0
    invoke-virtual {p1, v6}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 468
    :goto_2
    return-void

    .line 442
    .end local v6    # "intent":Landroid/content/Intent;
    .restart local v3    # "appname":Ljava/lang/String;
    .restart local v8    # "pi":Landroid/content/pm/PackageInfo;
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 457
    .end local v3    # "appname":Ljava/lang/String;
    .end local v8    # "pi":Landroid/content/pm/PackageInfo;
    :cond_2
    new-instance v6, Landroid/content/Intent;

    invoke-direct {v6}, Landroid/content/Intent;-><init>()V

    .line 458
    .restart local v6    # "intent":Landroid/content/Intent;
    const-string v10, "samsungapps://ProductDetail/com.samsung.music"

    invoke-static {v10}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    invoke-virtual {v6, v10}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 459
    invoke-virtual {v6, v12}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    goto :goto_1

    .line 463
    :catch_0
    move-exception v5

    .line 464
    .local v5, "e":Landroid/content/ActivityNotFoundException;
    const-string v10, "CommonListMenu"

    const-string v11, "Activity Not found!!!"

    invoke-static {v10, v11}, Lcom/samsung/musicplus/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 465
    invoke-virtual {v5}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    .line 466
    const-string v10, "Target Activity Not Found"

    const/4 v11, 0x1

    invoke-static {p1, v10, v11}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v10

    invoke-virtual {v10}, Landroid/widget/Toast;->show()V

    goto :goto_2
.end method

.method private prepareFavoriteMenu(Lcom/samsung/musicplus/contents/menu/CommonListMenuHandler;Landroid/view/Menu;J)V
    .locals 5
    .param p1, "handler"    # Lcom/samsung/musicplus/contents/menu/CommonListMenuHandler;
    .param p2, "menu"    # Landroid/view/Menu;
    .param p3, "selectedId"    # J

    .prologue
    .line 123
    const v3, 0x7f0d01d2

    invoke-interface {p2, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 124
    .local v0, "addToFavorite":Landroid/view/MenuItem;
    const v3, 0x7f0d01d3

    invoke-interface {p2, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 126
    .local v2, "removeFromFavorite":Landroid/view/MenuItem;
    invoke-interface {p1}, Lcom/samsung/musicplus/contents/menu/CommonListMenuHandler;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, p3, p4}, Lcom/samsung/musicplus/util/UiUtils;->isFavorite(Landroid/content/Context;J)Z

    move-result v1

    .line 129
    .local v1, "isFavorite":Z
    if-eqz v1, :cond_0

    .line 130
    if-eqz v0, :cond_0

    if-eqz v2, :cond_0

    .line 131
    const/4 v3, 0x0

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 132
    const/4 v3, 0x1

    invoke-interface {v2, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 135
    :cond_0
    return-void
.end method

.method private toggleFavorites(Lcom/samsung/musicplus/contents/menu/CommonListMenuHandler;IJ)V
    .locals 7
    .param p1, "handler"    # Lcom/samsung/musicplus/contents/menu/CommonListMenuHandler;
    .param p2, "itemId"    # I
    .param p3, "selectedId"    # J

    .prologue
    const/4 v5, 0x1

    .line 471
    invoke-interface {p1}, Lcom/samsung/musicplus/contents/menu/CommonListMenuHandler;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 473
    .local v0, "context":Landroid/content/Context;
    invoke-static {v0, p3, p4}, Lcom/samsung/musicplus/util/ListUtils;->getLocalFilePath(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    .line 474
    .local v1, "filePath":Ljava/lang/String;
    const v2, 0x7f0d01d2

    if-eq p2, v2, :cond_0

    move v4, v5

    :goto_0
    move-wide v2, p3

    invoke-static/range {v0 .. v5}, Lcom/samsung/musicplus/util/FileOperationTask;->toggleFavorites(Landroid/content/Context;Ljava/lang/String;JZZ)Z

    .line 476
    return-void

    .line 474
    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onContextItemSelected(Lcom/samsung/musicplus/common/menu/MusicMenuHandler;Landroid/view/MenuItem;J)Z
    .locals 25
    .param p1, "handler"    # Lcom/samsung/musicplus/common/menu/MusicMenuHandler;
    .param p2, "item"    # Landroid/view/MenuItem;
    .param p3, "selectedId"    # J

    .prologue
    .line 140
    move-object/from16 v0, p1

    instance-of v0, v0, Lcom/samsung/musicplus/contents/menu/CommonListMenuHandler;

    move/from16 v18, v0

    if-nez v18, :cond_0

    .line 141
    const-string v18, "CommonListMenu"

    const-string v19, "onContextItemSelected : handler is not instance of CommonListMenuHandler"

    invoke-static/range {v18 .. v19}, Lcom/samsung/musicplus/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    const/16 v18, 0x0

    .line 240
    :goto_0
    return v18

    :cond_0
    move-object/from16 v4, p1

    .line 146
    check-cast v4, Lcom/samsung/musicplus/contents/menu/CommonListMenuHandler;

    .line 148
    .local v4, "commonHandler":Lcom/samsung/musicplus/contents/menu/CommonListMenuHandler;
    invoke-interface/range {p2 .. p2}, Landroid/view/MenuItem;->getMenuInfo()Landroid/view/ContextMenu$ContextMenuInfo;

    move-result-object v16

    check-cast v16, Landroid/widget/AdapterView$AdapterContextMenuInfo;

    .line 151
    .local v16, "mi":Landroid/widget/AdapterView$AdapterContextMenuInfo;
    invoke-interface {v4}, Lcom/samsung/musicplus/contents/menu/CommonListMenuHandler;->getList()I

    move-result v13

    .line 152
    .local v13, "list":I
    invoke-interface {v4}, Lcom/samsung/musicplus/contents/menu/CommonListMenuHandler;->getFragment()Landroid/app/Fragment;

    move-result-object v9

    .line 153
    .local v9, "fragment":Landroid/app/Fragment;
    invoke-interface {v4}, Lcom/samsung/musicplus/contents/menu/CommonListMenuHandler;->getContext()Landroid/content/Context;

    move-result-object v5

    .line 155
    .local v5, "context":Landroid/content/Context;
    invoke-static {v13}, Lcom/samsung/musicplus/util/ListUtils;->isTrack(I)Z

    move-result v12

    .line 156
    .local v12, "isTrack":Z
    invoke-interface/range {p2 .. p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v18

    sparse-switch v18, :sswitch_data_0

    .line 240
    :goto_1
    const/16 v18, 0x0

    goto :goto_0

    .line 158
    :sswitch_0
    if-eqz v12, :cond_1

    .line 159
    const/16 v18, 0x1

    move/from16 v0, v18

    new-array v11, v0, [J

    const/16 v18, 0x0

    aput-wide p3, v11, v18

    .line 165
    .local v11, "id":[J
    :goto_2
    new-instance v18, Lcom/samsung/musicplus/dialog/DeleteDialogFragment;

    array-length v0, v11

    move/from16 v19, v0

    const/16 v20, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v19

    move/from16 v2, v20

    invoke-direct {v0, v11, v1, v13, v2}, Lcom/samsung/musicplus/dialog/DeleteDialogFragment;-><init>([JIIZ)V

    invoke-virtual {v9}, Landroid/app/Fragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v19

    const-string v20, "deleteDialog"

    invoke-virtual/range {v18 .. v20}, Lcom/samsung/musicplus/dialog/DeleteDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 167
    const/16 v18, 0x1

    goto :goto_0

    .line 163
    .end local v11    # "id":[J
    :cond_1
    move-object/from16 v0, v16

    iget v0, v0, Landroid/widget/AdapterView$AdapterContextMenuInfo;->position:I

    move/from16 v18, v0

    move/from16 v0, v18

    invoke-interface {v4, v13, v0}, Lcom/samsung/musicplus/contents/menu/CommonListMenuHandler;->getSongList(II)[J

    move-result-object v11

    .restart local v11    # "id":[J
    goto :goto_2

    .line 169
    .end local v11    # "id":[J
    :sswitch_1
    invoke-static {v13}, Lcom/samsung/musicplus/util/ListUtils;->getPreDefinedList(I)I

    move-result v14

    .line 170
    .local v14, "listType":I
    const v18, 0x2000b

    move/from16 v0, v18

    if-eq v14, v0, :cond_2

    const v18, 0x2000d

    move/from16 v0, v18

    if-ne v14, v0, :cond_3

    .line 171
    :cond_2
    invoke-static {v14}, Lcom/samsung/musicplus/util/ListUtils;->convertToUri(I)Landroid/net/Uri;

    move-result-object v18

    move-object/from16 v0, v16

    iget-wide v0, v0, Landroid/widget/AdapterView$AdapterContextMenuInfo;->id:J

    move-wide/from16 v20, v0

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-static {v0, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static {}, Lcom/samsung/musicplus/util/UiUtils;->getTheme()I

    move-result v19

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-static {v0, v14, v1}, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog;->getInstance(Ljava/lang/String;II)Landroid/app/DialogFragment;

    move-result-object v8

    .line 174
    .local v8, "fg":Landroid/app/DialogFragment;
    invoke-virtual {v9}, Landroid/app/Fragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v18

    const-string v19, "dlnaDms_detail_info"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v8, v0, v1}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 186
    .end local v8    # "fg":Landroid/app/DialogFragment;
    :goto_3
    const/16 v18, 0x1

    goto/16 :goto_0

    .line 176
    :cond_3
    new-instance v10, Landroid/content/Intent;

    invoke-virtual {v5}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v18

    const-class v19, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-direct {v10, v0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 177
    .local v10, "i":Landroid/content/Intent;
    move-object/from16 v0, v16

    iget v0, v0, Landroid/widget/AdapterView$AdapterContextMenuInfo;->position:I

    move/from16 v18, v0

    move/from16 v0, v18

    invoke-interface {v4, v0}, Lcom/samsung/musicplus/contents/menu/CommonListMenuHandler;->getAudioId(I)J

    move-result-wide v18

    move-object/from16 v0, v16

    iget v0, v0, Landroid/widget/AdapterView$AdapterContextMenuInfo;->position:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    move-wide/from16 v1, v18

    move/from16 v3, v20

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/musicplus/contents/menu/CommonListMenus;->getUriFromPosition(JI)Landroid/net/Uri;

    move-result-object v17

    .line 178
    .local v17, "uri":Landroid/net/Uri;
    if-eqz v17, :cond_4

    const-string v18, ""

    invoke-virtual/range {v17 .. v17}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_5

    .line 179
    :cond_4
    const-string v18, "CommonListMenu"

    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "onContextItemSelected-MENU_Details : uri is "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 182
    :cond_5
    const-string v18, "uri"

    invoke-virtual/range {v17 .. v17}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v10, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 183
    const-string v18, "audio_id"

    move-object/from16 v0, v16

    iget v0, v0, Landroid/widget/AdapterView$AdapterContextMenuInfo;->position:I

    move/from16 v19, v0

    move/from16 v0, v19

    invoke-interface {v4, v0}, Lcom/samsung/musicplus/contents/menu/CommonListMenuHandler;->getAudioId(I)J

    move-result-wide v20

    move-object/from16 v0, v18

    move-wide/from16 v1, v20

    invoke-virtual {v10, v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 184
    invoke-virtual {v5, v10}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_3

    .line 189
    .end local v10    # "i":Landroid/content/Intent;
    .end local v14    # "listType":I
    .end local v17    # "uri":Landroid/net/Uri;
    :sswitch_2
    sget-boolean v18, Lcom/samsung/musicplus/util/Version;->LIBRARY_OVER_API_1:Z

    if-eqz v18, :cond_6

    .line 190
    new-instance v10, Landroid/content/Intent;

    const-class v18, Lcom/samsung/musicplus/player/SetAsActivity;

    move-object/from16 v0, v18

    invoke-direct {v10, v5, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 194
    .restart local v10    # "i":Landroid/content/Intent;
    :goto_4
    sget-object v18, Lcom/samsung/musicplus/provider/MusicContents;->MUSIC_CONTENTS_URI:Landroid/net/Uri;

    move-object/from16 v0, v18

    move-wide/from16 v1, p3

    invoke-static {v0, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v17

    .line 198
    .restart local v17    # "uri":Landroid/net/Uri;
    const-string v18, "extra_uri"

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v10, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 200
    :try_start_0
    invoke-virtual {v5, v10}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 206
    :goto_5
    const/16 v18, 0x1

    goto/16 :goto_0

    .line 192
    .end local v10    # "i":Landroid/content/Intent;
    .end local v17    # "uri":Landroid/net/Uri;
    :cond_6
    new-instance v10, Landroid/content/Intent;

    const-string v18, "com.sec.android.music.intent.action.LAUNCH_SET_AS"

    move-object/from16 v0, v18

    invoke-direct {v10, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .restart local v10    # "i":Landroid/content/Intent;
    goto :goto_4

    .line 201
    .restart local v17    # "uri":Landroid/net/Uri;
    :catch_0
    move-exception v7

    .line 202
    .local v7, "e":Landroid/content/ActivityNotFoundException;
    const-string v18, "CommonListMenu"

    const-string v19, "Activity Not found!!!"

    invoke-static/range {v18 .. v19}, Lcom/samsung/musicplus/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    invoke-virtual {v7}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    .line 204
    const-string v18, "Target Activity Not Found"

    const/16 v19, 0x1

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-static {v5, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/widget/Toast;->show()V

    goto :goto_5

    .line 208
    .end local v7    # "e":Landroid/content/ActivityNotFoundException;
    .end local v10    # "i":Landroid/content/Intent;
    .end local v17    # "uri":Landroid/net/Uri;
    :sswitch_3
    if-eqz v12, :cond_7

    .line 209
    const/16 v18, 0x1

    move/from16 v0, v18

    new-array v11, v0, [J

    const/16 v18, 0x0

    aput-wide p3, v11, v18

    .line 215
    .restart local v11    # "id":[J
    :goto_6
    const/16 v18, 0x2

    const/16 v19, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v5, v1, v2, v11}, Lcom/samsung/musicplus/contents/menu/CommonListMenus;->launchSelector(Landroid/content/Context;II[J)V

    .line 217
    const/16 v18, 0x1

    goto/16 :goto_0

    .line 213
    .end local v11    # "id":[J
    :cond_7
    move-object/from16 v0, v16

    iget v0, v0, Landroid/widget/AdapterView$AdapterContextMenuInfo;->position:I

    move/from16 v18, v0

    move/from16 v0, v18

    invoke-interface {v4, v13, v0}, Lcom/samsung/musicplus/contents/menu/CommonListMenuHandler;->getSongList(II)[J

    move-result-object v11

    .restart local v11    # "id":[J
    goto :goto_6

    .line 219
    .end local v11    # "id":[J
    :sswitch_4
    const v18, 0x7f0d01d2

    move-object/from16 v0, p0

    move/from16 v1, v18

    move-wide/from16 v2, p3

    invoke-direct {v0, v4, v1, v2, v3}, Lcom/samsung/musicplus/contents/menu/CommonListMenus;->toggleFavorites(Lcom/samsung/musicplus/contents/menu/CommonListMenuHandler;IJ)V

    .line 220
    const/16 v18, 0x1

    goto/16 :goto_0

    .line 222
    :sswitch_5
    const v18, 0x7f0d01d3

    move-object/from16 v0, p0

    move/from16 v1, v18

    move-wide/from16 v2, p3

    invoke-direct {v0, v4, v1, v2, v3}, Lcom/samsung/musicplus/contents/menu/CommonListMenus;->toggleFavorites(Lcom/samsung/musicplus/contents/menu/CommonListMenuHandler;IJ)V

    .line 223
    const/16 v18, 0x1

    goto/16 :goto_0

    .line 225
    :sswitch_6
    const/16 v18, 0x1

    move/from16 v0, v18

    new-array v11, v0, [J

    const/16 v18, 0x0

    aput-wide p3, v11, v18

    .line 228
    .restart local v11    # "id":[J
    new-instance v6, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;

    const-wide/16 v18, -0x1

    move-wide/from16 v0, v18

    invoke-direct {v6, v11, v0, v1}, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;-><init>([JJ)V

    .line 230
    .local v6, "dialog":Landroid/app/DialogFragment;
    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-virtual {v6, v0}, Landroid/app/DialogFragment;->setCancelable(Z)V

    .line 231
    invoke-virtual {v9}, Landroid/app/Fragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v18

    const-string v19, "knoxMoveDialog"

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v6, v0, v1}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 232
    const/16 v18, 0x1

    goto/16 :goto_0

    .line 234
    .end local v6    # "dialog":Landroid/app/DialogFragment;
    .end local v11    # "id":[J
    :sswitch_7
    move-object/from16 v0, v16

    iget-wide v0, v0, Landroid/widget/AdapterView$AdapterContextMenuInfo;->id:J

    move-wide/from16 v18, v0

    invoke-static/range {v18 .. v19}, Lcom/samsung/musicplus/util/ServiceUtils;->removeTrack(J)I

    .line 235
    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v18

    const v19, 0x7f0f000e

    const/16 v20, 0x1

    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const/16 v23, 0x1

    invoke-static/range {v23 .. v23}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    aput-object v23, v21, v22

    invoke-virtual/range {v18 .. v21}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    .line 237
    .local v15, "message":Ljava/lang/String;
    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-static {v5, v15, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/widget/Toast;->show()V

    goto/16 :goto_1

    .line 156
    :sswitch_data_0
    .sparse-switch
        0x7f0d01a3 -> :sswitch_2
        0x7f0d01cd -> :sswitch_3
        0x7f0d01cf -> :sswitch_7
        0x7f0d01d0 -> :sswitch_0
        0x7f0d01d2 -> :sswitch_4
        0x7f0d01d3 -> :sswitch_5
        0x7f0d01d4 -> :sswitch_6
        0x7f0d01d8 -> :sswitch_1
    .end sparse-switch
.end method

.method public onCreateContextMenu(Lcom/samsung/musicplus/common/menu/MusicMenuHandler;Landroid/view/MenuInflater;Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;J)V
    .locals 8
    .param p1, "handler"    # Lcom/samsung/musicplus/common/menu/MusicMenuHandler;
    .param p2, "inflater"    # Landroid/view/MenuInflater;
    .param p3, "menu"    # Landroid/view/ContextMenu;
    .param p4, "v"    # Landroid/view/View;
    .param p5, "menuInfo"    # Landroid/view/ContextMenu$ContextMenuInfo;
    .param p6, "selectedId"    # J

    .prologue
    .line 67
    invoke-interface {p1}, Lcom/samsung/musicplus/common/menu/MusicMenuHandler;->getFragment()Landroid/app/Fragment;

    move-result-object v0

    .line 68
    .local v0, "f":Landroid/app/Fragment;
    instance-of v1, v0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;

    if-eqz v1, :cond_0

    invoke-virtual {p0, p6, p7}, Lcom/samsung/musicplus/contents/menu/CommonListMenus;->checkValidContextItem(J)Z

    move-result v1

    if-nez v1, :cond_0

    .line 69
    const-string v1, "CommonListMenu"

    const-string v3, "onCreateContextMenu : selected id is negative."

    invoke-static {v1, v3}, Lcom/samsung/musicplus/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    .end local p5    # "menuInfo":Landroid/view/ContextMenu$ContextMenuInfo;
    :goto_0
    return-void

    .line 73
    .restart local p5    # "menuInfo":Landroid/view/ContextMenu$ContextMenuInfo;
    :cond_0
    move-wide v4, p6

    .line 75
    .local v4, "selectedItemId":J
    instance-of v1, p1, Lcom/samsung/musicplus/contents/menu/CommonListMenuHandler;

    if-nez v1, :cond_1

    .line 76
    const-string v1, "CommonListMenu"

    const-string v3, "onCreateContextMenu : handler is not instanceof CommListMenu"

    invoke-static {v1, v3}, Lcom/samsung/musicplus/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    move-object v2, p1

    .line 80
    check-cast v2, Lcom/samsung/musicplus/contents/menu/CommonListMenuHandler;

    .line 82
    .local v2, "commonMenuHanlder":Lcom/samsung/musicplus/contents/menu/CommonListMenuHandler;
    invoke-virtual {p0, p2, p3}, Lcom/samsung/musicplus/contents/menu/CommonListMenus;->onInflateContextMenu(Landroid/view/MenuInflater;Landroid/view/ContextMenu;)V

    .line 84
    check-cast p5, Landroid/widget/AdapterView$AdapterContextMenuInfo;

    .end local p5    # "menuInfo":Landroid/view/ContextMenu$ContextMenuInfo;
    iget v6, p5, Landroid/widget/AdapterView$AdapterContextMenuInfo;->position:I

    .line 85
    .local v6, "position":I
    invoke-interface {v2, v6}, Lcom/samsung/musicplus/contents/menu/CommonListMenuHandler;->getTitleString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {p3, v1}, Landroid/view/ContextMenu;->setHeaderTitle(Ljava/lang/CharSequence;)Landroid/view/ContextMenu;

    move-object v1, p0

    move-object v3, p3

    .line 86
    invoke-virtual/range {v1 .. v6}, Lcom/samsung/musicplus/contents/menu/CommonListMenus;->prepareContextMenu(Lcom/samsung/musicplus/contents/menu/CommonListMenuHandler;Landroid/view/Menu;JI)V

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/MenuInflater;Landroid/view/Menu;)V
    .locals 1
    .param p1, "inflater"    # Landroid/view/MenuInflater;
    .param p2, "menu"    # Landroid/view/Menu;

    .prologue
    .line 245
    const v0, 0x7f120004

    invoke-virtual {p1, v0, p2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 246
    return-void
.end method

.method protected onInflateContextMenu(Landroid/view/MenuInflater;Landroid/view/ContextMenu;)V
    .locals 1
    .param p1, "inflater"    # Landroid/view/MenuInflater;
    .param p2, "menu"    # Landroid/view/ContextMenu;

    .prologue
    .line 97
    const v0, 0x7f120003

    invoke-virtual {p1, v0, p2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 98
    return-void
.end method

.method public onOptionsItemSelected(Lcom/samsung/musicplus/common/menu/MusicMenuHandler;Landroid/view/MenuItem;)Z
    .locals 8
    .param p1, "handler"    # Lcom/samsung/musicplus/common/menu/MusicMenuHandler;
    .param p2, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v0, 0x0

    const/4 v4, 0x1

    .line 307
    instance-of v3, p1, Lcom/samsung/musicplus/contents/menu/CommonListMenuHandler;

    if-nez v3, :cond_1

    .line 308
    const-string v3, "CommonListMenu"

    const-string v4, "onOptionsItemSelected : handler is not instanceof CommListMenu"

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v4, v0

    .line 351
    :cond_0
    :goto_0
    return v4

    :cond_1
    move-object v6, p1

    .line 312
    check-cast v6, Lcom/samsung/musicplus/contents/menu/CommonListMenuHandler;

    .line 313
    .local v6, "commonMenuHanlder":Lcom/samsung/musicplus/contents/menu/CommonListMenuHandler;
    invoke-interface {v6}, Lcom/samsung/musicplus/contents/menu/CommonListMenuHandler;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 314
    .local v1, "context":Landroid/content/Context;
    invoke-interface {v6}, Lcom/samsung/musicplus/contents/menu/CommonListMenuHandler;->getList()I

    move-result v2

    .line 315
    .local v2, "list":I
    invoke-interface {v6}, Lcom/samsung/musicplus/contents/menu/CommonListMenuHandler;->getKey()Ljava/lang/String;

    move-result-object v5

    .line 317
    .local v5, "key":Ljava/lang/String;
    invoke-interface {p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    move v4, v0

    .line 351
    goto :goto_0

    .line 319
    :sswitch_0
    invoke-interface {p1}, Lcom/samsung/musicplus/common/menu/MusicMenuHandler;->getFragment()Landroid/app/Fragment;

    move-result-object v7

    .line 320
    .local v7, "f":Landroid/app/Fragment;
    instance-of v0, v7, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;

    if-eqz v0, :cond_0

    .line 321
    check-cast v7, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;

    .end local v7    # "f":Landroid/app/Fragment;
    invoke-virtual {v7}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->startActionMode()V

    goto :goto_0

    .line 325
    :sswitch_1
    const/16 v3, 0x11

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/musicplus/contents/menu/CommonListMenus;->launchSelector(Landroid/content/Context;IIILjava/lang/String;)V

    goto :goto_0

    .line 329
    :sswitch_2
    const/16 v3, 0x12

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/musicplus/contents/menu/CommonListMenus;->launchSelector(Landroid/content/Context;IIILjava/lang/String;)V

    goto :goto_0

    .line 334
    :sswitch_3
    const/16 v3, 0x13

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/musicplus/contents/menu/CommonListMenus;->launchSelector(Landroid/content/Context;IIILjava/lang/String;)V

    goto :goto_0

    .line 338
    :sswitch_4
    invoke-direct {p0, v1}, Lcom/samsung/musicplus/contents/menu/CommonListMenus;->launchLgtRintone(Landroid/content/Context;)V

    goto :goto_0

    .line 341
    :sswitch_5
    invoke-direct {p0, v1}, Lcom/samsung/musicplus/contents/menu/CommonListMenus;->launchSamsungMusic(Landroid/content/Context;)V

    goto :goto_0

    .line 344
    :sswitch_6
    invoke-direct {p0, v1}, Lcom/samsung/musicplus/contents/menu/CommonListMenus;->launchMusicSquare(Landroid/content/Context;)V

    goto :goto_0

    .line 317
    :sswitch_data_0
    .sparse-switch
        0x7f0d01d0 -> :sswitch_0
        0x7f0d01d4 -> :sswitch_3
        0x7f0d01d5 -> :sswitch_3
        0x7f0d01d6 -> :sswitch_1
        0x7f0d01d7 -> :sswitch_2
        0x7f0d01de -> :sswitch_4
        0x7f0d01df -> :sswitch_5
        0x7f0d01e5 -> :sswitch_6
    .end sparse-switch
.end method

.method public onPrepareOptionsMenu(Lcom/samsung/musicplus/common/menu/MusicMenuHandler;Landroid/view/Menu;)V
    .locals 13
    .param p1, "handler"    # Lcom/samsung/musicplus/common/menu/MusicMenuHandler;
    .param p2, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v12, 0x0

    const/4 v11, 0x1

    .line 250
    instance-of v9, p1, Lcom/samsung/musicplus/contents/menu/CommonListMenuHandler;

    if-eqz v9, :cond_5

    move-object v4, p1

    .line 251
    check-cast v4, Lcom/samsung/musicplus/contents/menu/CommonListMenuHandler;

    .line 252
    .local v4, "listOptionHandler":Lcom/samsung/musicplus/contents/menu/CommonListMenuHandler;
    invoke-interface {v4}, Lcom/samsung/musicplus/contents/menu/CommonListMenuHandler;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 253
    .local v1, "context":Landroid/content/Context;
    invoke-interface {v4}, Lcom/samsung/musicplus/contents/menu/CommonListMenuHandler;->getList()I

    move-result v3

    .line 255
    .local v3, "list":I
    invoke-static {v3}, Lcom/samsung/musicplus/util/ListUtils;->isTrack(I)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 256
    invoke-static {v3}, Lcom/samsung/musicplus/util/ListUtils;->getPreDefinedList(I)I

    move-result v9

    const v10, 0x20001

    if-ne v9, v10, :cond_1

    .line 257
    sget-boolean v9, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_CHECK_KOR_LGT:Z

    if-eqz v9, :cond_0

    invoke-static {v1}, Lcom/samsung/musicplus/util/KnoxGateManager;->isKnoxModeOn(Landroid/content/Context;)Z

    move-result v9

    if-nez v9, :cond_0

    .line 259
    const v9, 0x7f0d01de

    invoke-interface {p2, v9}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v5

    .line 260
    .local v5, "musicbelling":Landroid/view/MenuItem;
    if-eqz v5, :cond_0

    .line 261
    invoke-interface {v5, v11}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 263
    .end local v5    # "musicbelling":Landroid/view/MenuItem;
    :cond_0
    sget-boolean v9, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_SUPPORT_SAMSUNG_MUSIC:Z

    if-eqz v9, :cond_1

    invoke-static {v1}, Lcom/samsung/musicplus/util/KnoxGateManager;->isKnoxModeOn(Landroid/content/Context;)Z

    move-result v9

    if-nez v9, :cond_1

    .line 265
    const v9, 0x7f0d01df

    invoke-interface {p2, v9}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v7

    .line 266
    .local v7, "samsungMusic":Landroid/view/MenuItem;
    if-eqz v7, :cond_1

    .line 267
    invoke-interface {v7, v11}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 285
    .end local v7    # "samsungMusic":Landroid/view/MenuItem;
    :cond_1
    :goto_0
    const v9, 0x7f0d01d0

    invoke-interface {p2, v9}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 286
    .local v2, "delete":Landroid/view/MenuItem;
    if-eqz v2, :cond_2

    .line 287
    invoke-interface {v4}, Lcom/samsung/musicplus/contents/menu/CommonListMenuHandler;->getListItemCount()I

    move-result v9

    if-lez v9, :cond_8

    .line 288
    invoke-interface {v2, v11}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 294
    :cond_2
    :goto_1
    sget-boolean v9, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_MASS_PROJECT:Z

    if-nez v9, :cond_3

    .line 295
    :cond_3
    const v9, 0x7f0d01e5

    invoke-interface {p2, v9}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v8

    .line 296
    .local v8, "square":Landroid/view/MenuItem;
    if-eqz v8, :cond_4

    .line 297
    invoke-interface {v8, v11}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 300
    :cond_4
    invoke-interface {v4, p2}, Lcom/samsung/musicplus/contents/menu/CommonListMenuHandler;->handlePrepareOptionMenu(Landroid/view/Menu;)V

    .line 302
    .end local v1    # "context":Landroid/content/Context;
    .end local v2    # "delete":Landroid/view/MenuItem;
    .end local v3    # "list":I
    .end local v4    # "listOptionHandler":Lcom/samsung/musicplus/contents/menu/CommonListMenuHandler;
    .end local v8    # "square":Landroid/view/MenuItem;
    :cond_5
    return-void

    .line 272
    .restart local v1    # "context":Landroid/content/Context;
    .restart local v3    # "list":I
    .restart local v4    # "listOptionHandler":Lcom/samsung/musicplus/contents/menu/CommonListMenuHandler;
    :cond_6
    invoke-static {v3}, Lcom/samsung/musicplus/util/ListUtils;->getPreDefinedList(I)I

    move-result v9

    const v10, 0x10007

    if-eq v9, v10, :cond_1

    .line 273
    const v9, 0x7f0d01d6

    invoke-interface {p2, v9}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 274
    .local v0, "addToSecretBox":Landroid/view/MenuItem;
    const v9, 0x7f0d01d7

    invoke-interface {p2, v9}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v6

    .line 276
    .local v6, "removeFromSecretBox":Landroid/view/MenuItem;
    if-eqz v0, :cond_7

    .line 277
    invoke-interface {v0, v12}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 279
    :cond_7
    if-eqz v6, :cond_1

    .line 280
    invoke-interface {v6, v12}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 290
    .end local v0    # "addToSecretBox":Landroid/view/MenuItem;
    .end local v6    # "removeFromSecretBox":Landroid/view/MenuItem;
    .restart local v2    # "delete":Landroid/view/MenuItem;
    :cond_8
    invoke-interface {v2, v12}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_1
.end method

.method protected prepareContextMenu(Lcom/samsung/musicplus/contents/menu/CommonListMenuHandler;Landroid/view/Menu;JI)V
    .locals 5
    .param p1, "handler"    # Lcom/samsung/musicplus/contents/menu/CommonListMenuHandler;
    .param p2, "menu"    # Landroid/view/Menu;
    .param p3, "itemId"    # J
    .param p5, "position"    # I

    .prologue
    const/4 v4, 0x0

    .line 102
    const v3, 0x7f0d01d8

    invoke-interface {p2, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 103
    .local v1, "detail":Landroid/view/MenuItem;
    const v3, 0x7f0d01a3

    invoke-interface {p2, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 104
    .local v2, "setAs":Landroid/view/MenuItem;
    const v3, 0x7f0d01d2

    invoke-interface {p2, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 106
    .local v0, "addToFavorite":Landroid/view/MenuItem;
    invoke-interface {p1}, Lcom/samsung/musicplus/contents/menu/CommonListMenuHandler;->getList()I

    move-result v3

    invoke-static {v3}, Lcom/samsung/musicplus/util/ListUtils;->isTab(I)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 107
    if-eqz v1, :cond_0

    .line 108
    invoke-interface {v1, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 110
    :cond_0
    if-eqz v2, :cond_1

    .line 111
    invoke-interface {v2, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 113
    :cond_1
    if-eqz v0, :cond_2

    .line 114
    invoke-interface {v0, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 119
    :cond_2
    :goto_0
    invoke-interface {p1, p2, p3, p4, p5}, Lcom/samsung/musicplus/contents/menu/CommonListMenuHandler;->handlePrepareContextMenu(Landroid/view/Menu;JI)V

    .line 120
    return-void

    .line 117
    :cond_3
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/musicplus/contents/menu/CommonListMenus;->prepareFavoriteMenu(Lcom/samsung/musicplus/contents/menu/CommonListMenuHandler;Landroid/view/Menu;J)V

    goto :goto_0
.end method
