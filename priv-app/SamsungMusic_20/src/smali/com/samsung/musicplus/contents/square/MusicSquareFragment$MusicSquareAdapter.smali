.class public Lcom/samsung/musicplus/contents/square/MusicSquareFragment$MusicSquareAdapter;
.super Landroid/widget/BaseAdapter;
.source "MusicSquareFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/contents/square/MusicSquareFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "MusicSquareAdapter"
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field final synthetic this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;


# direct methods
.method public constructor <init>(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;Landroid/content/Context;)V
    .locals 0
    .param p2, "c"    # Landroid/content/Context;

    .prologue
    .line 1548
    iput-object p1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$MusicSquareAdapter;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 1549
    iput-object p2, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$MusicSquareAdapter;->mContext:Landroid/content/Context;

    .line 1550
    return-void
.end method

.method private setCellTts(Landroid/widget/ImageView;I)V
    .locals 4
    .param p1, "imageView"    # Landroid/widget/ImageView;
    .param p2, "position"    # I

    .prologue
    .line 1618
    iget-object v2, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$MusicSquareAdapter;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    invoke-virtual {v2}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 1619
    .local v0, "res":Landroid/content/res/Resources;
    const v2, 0x7f100192

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1620
    .local v1, "selectedTts":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/widget/ImageView;->isSelected()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$MusicSquareAdapter;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    # getter for: Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mPlayingCell:I
    invoke-static {v2}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->access$2700(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;)I

    move-result v2

    if-ne p2, v2, :cond_1

    .line 1621
    :cond_0
    const v2, 0x7f10019e

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1623
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const v3, 0x7f100182

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    add-int/lit8 v3, p2, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1625
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 2

    .prologue
    .line 1555
    sget v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->sMusicSquareColumn:I

    sget v1, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->sMusicSquareColumn:I

    mul-int/2addr v0, v1

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 1560
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 1565
    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 1577
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getHeight()I

    move-result v3

    sget v4, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->sMusicSquareColumn:I

    div-int v0, v3, v4

    .line 1581
    .local v0, "height":I
    new-instance v1, Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$MusicSquareAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v1, v3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 1582
    .local v1, "imageView":Landroid/widget/ImageView;
    new-instance v3, Landroid/widget/AbsListView$LayoutParams;

    const/4 v4, -0x1

    invoke-direct {v3, v4, v0}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 1584
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setAdjustViewBounds(Z)V

    .line 1585
    sget-object v3, Landroid/widget/ImageView$ScaleType;->FIT_XY:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    .line 1587
    iget-object v3, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$MusicSquareAdapter;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    invoke-virtual {v3}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c0116

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 1589
    .local v2, "pad":I
    iget-object v3, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$MusicSquareAdapter;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    # getter for: Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mPlayingCell:I
    invoke-static {v3}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->access$2700(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;)I

    move-result v3

    if-ne p1, v3, :cond_3

    iget-object v3, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$MusicSquareAdapter;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    # getter for: Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mAlbumBm:Landroid/graphics/Bitmap;
    invoke-static {v3}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->access$2800(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;)Landroid/graphics/Bitmap;

    move-result-object v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$MusicSquareAdapter;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    # getter for: Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mIsVisualInteraction:Z
    invoke-static {v3}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->access$1900(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 1590
    iget-object v3, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$MusicSquareAdapter;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    # getter for: Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mAlbumBm:Landroid/graphics/Bitmap;
    invoke-static {v3}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->access$2800(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v3

    if-nez v3, :cond_2

    .line 1591
    iget-object v3, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$MusicSquareAdapter;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    # getter for: Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mAlbumBm:Landroid/graphics/Bitmap;
    invoke-static {v3}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->access$2800(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 1592
    invoke-virtual {v1, v2, v2, v2, v2}, Landroid/widget/ImageView;->setPadding(IIII)V

    .line 1596
    :goto_0
    iget-object v3, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$MusicSquareAdapter;->mContext:Landroid/content/Context;

    const v4, 0x7f050016

    invoke-static {v3, v4}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1607
    :cond_0
    :goto_1
    invoke-direct {p0, v1, p1}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$MusicSquareAdapter;->setCellTts(Landroid/widget/ImageView;I)V

    .line 1609
    iget-object v3, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$MusicSquareAdapter;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    invoke-virtual {v3}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/musicplus/util/UiUtils;->checkHoverUI(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1610
    iget-object v3, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$MusicSquareAdapter;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    # invokes: Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->setCellAirView(Landroid/widget/ImageView;I)V
    invoke-static {v3, v1, p1}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->access$3000(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;Landroid/widget/ImageView;I)V

    .line 1613
    :cond_1
    return-object v1

    .line 1594
    :cond_2
    # getter for: Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->access$1600()Ljava/lang/String;

    move-result-object v3

    const-string v4, "mAlbumBm is recycled!!"

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1598
    :cond_3
    iget-object v3, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$MusicSquareAdapter;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    # getter for: Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSelectedCellList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->access$2900(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;)Ljava/util/ArrayList;

    move-result-object v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$MusicSquareAdapter;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    # getter for: Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSelectedCellList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->access$2900(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1600
    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 1601
    invoke-virtual {v1, v2, v2, v2, v2}, Landroid/widget/ImageView;->setPadding(IIII)V

    .line 1602
    const v3, 0x7f0b008f

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    .line 1603
    :cond_4
    iget-object v3, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$MusicSquareAdapter;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    # getter for: Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSelectedCellList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->access$2900(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;)Ljava/util/ArrayList;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$MusicSquareAdapter;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    # getter for: Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSelectedCellList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->access$2900(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1604
    invoke-virtual {v1, v2, v2, v2, v2}, Landroid/widget/ImageView;->setPadding(IIII)V

    .line 1605
    const v3, 0x7f0b0090

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1
.end method
