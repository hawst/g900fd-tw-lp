.class public Lcom/samsung/musicplus/contents/square/MusicSquareFragment;
.super Lcom/samsung/musicplus/MusicBaseFragment;
.source "MusicSquareFragment.java"

# interfaces
.implements Lcom/samsung/musicplus/settings/PlayerSettingPreference;
.implements Lcom/samsung/musicplus/util/UiUtils$Defs;
.implements Lcom/samsung/musicplus/widget/fragment/OnMusicFragmentInterface;
.implements Lcom/samsung/musicplus/widget/tab/OnTabListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/contents/square/MusicSquareFragment$SquareHelpDialog;,
        Lcom/samsung/musicplus/contents/square/MusicSquareFragment$MusicSquareAdapter;,
        Lcom/samsung/musicplus/contents/square/MusicSquareFragment$AxisDialog;
    }
.end annotation


# static fields
.field private static final CELL_SELECTED_COLORS:[I

.field private static final CHANGE_AXIS:I = 0x7f0d01f5

.field private static final CLASSNAME:Ljava/lang/String;

.field private static final CLEAN_ALBUMART_BITMAP:I = 0x64

.field private static final CLEAR_ALL_SQUARE_CELL:I = 0x0

.field private static final HOVER_ARTIST_RES:[I

.field private static final HOVER_CONTAINER_RES:[I

.field private static final HOVER_LIST_MAX:I = 0x4

.field private static final HOVER_TITLE_RES:[I

.field private static final LEFT_ALIGN:I = 0x2

.field private static final MUSIC_SQUARE_TEST_MODE:Z = false

.field private static final RIGHT_ALIGN:I = 0x1

.field private static final SAVED_INSTANCE_SELECTED_CELL:Ljava/lang/String; = "selected_cell"

.field private static final SELECT_SQUARE_CELL:I = 0x1

.field private static final SET_ALBUMART_SQUARE_CELL:I = 0x2

.field private static final SQUARE_DEBUG:Z = true

.field private static final SQUARE_GRIDVIEW_INVALIDATE:I = 0x1

.field private static final SQUARE_LONG_CLICK_CHCKER:I = 0x0

.field private static final SQUARE_LONG_CLICK_DELAY:I = 0x1f4

.field private static final SQUARE_READY_TO_CLICK_DELAY:I = 0x3e8

.field private static final SQUARE_SET_READY_TO_CLICK:I = 0x2

.field private static final SQUARE_SET_READY_TO_CLICK_DELAY:I = 0x12c

.field public static final TAG_SQUARE_LIST:Ljava/lang/String; = "square_list"

.field private static final UNSELECT_SQUARE_CELL:I = 0x3

.field private static sAdapter:Lcom/samsung/musicplus/contents/square/MusicSquareFragment$MusicSquareAdapter;

.field public static sMusicSquareColumn:I

.field private static sPreferences:Landroid/content/SharedPreferences;


# instance fields
.field private final MOVING_FILTER_PIXEL:I

.field private mAlbumBm:Landroid/graphics/Bitmap;

.field private mAxis:I

.field private mBaseXPoint:I

.field private mBaseYPoint:I

.field private mCellAirViewListener:Lcom/samsung/musicplus/library/hardware/AirView$OnAirViewMorePopupListener;

.field private mDoVisualInteraction:Z

.field private mHasNewCells:Z

.field private mIsDown:Z

.field private mIsOnResumed:Z

.field private mIsReadyToClick:Z

.field private mIsRegistered:Z

.field private mIsSelected:Z

.field private mIsVisualInteraction:Z

.field private mKeyWord:Ljava/lang/String;

.field private mLastSelectedCell:I

.field private mLeftText:Landroid/widget/TextView;

.field private mMusicSquareUpdateObserver:Landroid/database/ContentObserver;

.field private mPlayingCell:I

.field private final mQueueChangedListener:Landroid/content/BroadcastReceiver;

.field private mRightText:Landroid/widget/TextView;

.field private mSelectedCellList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mSquareGridView:Landroid/widget/GridView;

.field private final mSquareUpdateHandler:Landroid/os/Handler;

.field private final mSquareVisualInteractionAnimatinHandler:Landroid/os/Handler;

.field private final sAlbumArtBmVisualInteraction:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 115
    const-class v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->CLASSNAME:Ljava/lang/String;

    .line 124
    const/4 v0, 0x0

    sput v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->sMusicSquareColumn:I

    .line 153
    const/16 v0, 0x19

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->CELL_SELECTED_COLORS:[I

    .line 164
    new-array v0, v1, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->HOVER_TITLE_RES:[I

    .line 168
    new-array v0, v1, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->HOVER_ARTIST_RES:[I

    .line 172
    new-array v0, v1, [I

    fill-array-data v0, :array_3

    sput-object v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->HOVER_CONTAINER_RES:[I

    return-void

    .line 153
    nop

    :array_0
    .array-data 4
        0x7f0b0076
        0x7f0b0081
        0x7f0b0088
        0x7f0b0089
        0x7f0b008a
        0x7f0b008b
        0x7f0b008c
        0x7f0b008d
        0x7f0b008e
        0x7f0b0077
        0x7f0b0078
        0x7f0b0079
        0x7f0b007a
        0x7f0b007b
        0x7f0b007c
        0x7f0b007d
        0x7f0b007e
        0x7f0b007f
        0x7f0b0080
        0x7f0b0082
        0x7f0b0083
        0x7f0b0084
        0x7f0b0085
        0x7f0b0086
        0x7f0b0087
    .end array-data

    .line 164
    :array_1
    .array-data 4
        0x7f0d006f
        0x7f0d0072
        0x7f0d0075
        0x7f0d0078
    .end array-data

    .line 168
    :array_2
    .array-data 4
        0x7f0d0070
        0x7f0d0073
        0x7f0d0076
        0x7f0d0079
    .end array-data

    .line 172
    :array_3
    .array-data 4
        -0x1
        0x7f0d0071
        0x7f0d0074
        0x7f0d0077
    .end array-data
.end method

.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, -0x1

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 112
    invoke-direct {p0}, Lcom/samsung/musicplus/MusicBaseFragment;-><init>()V

    .line 132
    iput-boolean v2, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mIsDown:Z

    .line 142
    iput v4, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mLastSelectedCell:I

    .line 144
    iput-boolean v2, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mHasNewCells:Z

    .line 148
    iput-boolean v2, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mIsRegistered:Z

    .line 209
    new-instance v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$1;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$1;-><init>(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;)V

    iput-object v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mQueueChangedListener:Landroid/content/BroadcastReceiver;

    .line 229
    new-instance v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$2;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$2;-><init>(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mMusicSquareUpdateObserver:Landroid/database/ContentObserver;

    .line 260
    iput-boolean v2, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mIsOnResumed:Z

    .line 501
    iput v3, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->MOVING_FILTER_PIXEL:I

    .line 503
    iput v2, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mBaseXPoint:I

    .line 505
    iput v2, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mBaseYPoint:I

    .line 524
    iput-boolean v2, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mDoVisualInteraction:Z

    .line 536
    iput-boolean v2, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mIsVisualInteraction:Z

    .line 541
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->sAlbumArtBmVisualInteraction:Ljava/util/ArrayList;

    .line 639
    new-instance v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$6;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$6;-><init>(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;)V

    iput-object v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSquareVisualInteractionAnimatinHandler:Landroid/os/Handler;

    .line 933
    iput-boolean v3, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mIsReadyToClick:Z

    .line 935
    new-instance v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$7;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$7;-><init>(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;)V

    iput-object v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSquareUpdateHandler:Landroid/os/Handler;

    .line 1630
    iput v4, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mPlayingCell:I

    .line 1855
    iput-boolean v3, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mIsSelected:Z

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    .prologue
    .line 112
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->updateSubList()V

    return-void
.end method

.method static synthetic access$100(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;)Landroid/widget/GridView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    .prologue
    .line 112
    iget-object v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSquareGridView:Landroid/widget/GridView;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;II)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/square/MusicSquareFragment;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 112
    invoke-direct {p0, p1, p2}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->setBasePointForCheckMove(II)V

    return-void
.end method

.method static synthetic access$1100(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    .prologue
    .line 112
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->updateLongClickTimer()V

    return-void
.end method

.method static synthetic access$1200(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    .prologue
    .line 112
    iget v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mLastSelectedCell:I

    return v0
.end method

.method static synthetic access$1202(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/square/MusicSquareFragment;
    .param p1, "x1"    # I

    .prologue
    .line 112
    iput p1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mLastSelectedCell:I

    return p1
.end method

.method static synthetic access$1300(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    .prologue
    .line 112
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->cancelLongClick()V

    return-void
.end method

.method static synthetic access$1400(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/square/MusicSquareFragment;
    .param p1, "x1"    # I

    .prologue
    .line 112
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->setReadyToClick(I)V

    return-void
.end method

.method static synthetic access$1500(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;II)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/square/MusicSquareFragment;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 112
    invoke-direct {p0, p1, p2}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->checkMovePointUserIntented(II)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1600()Ljava/lang/String;
    .locals 1

    .prologue
    .line 112
    sget-object v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->CLASSNAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    .prologue
    .line 112
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->removeMessageAllVisualInteraction()V

    return-void
.end method

.method static synthetic access$1800(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    .prologue
    .line 112
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->cleanAlbumArtBitmapVisualInteraction()V

    return-void
.end method

.method static synthetic access$1900(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    .prologue
    .line 112
    iget-boolean v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mIsVisualInteraction:Z

    return v0
.end method

.method static synthetic access$1902(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/square/MusicSquareFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 112
    iput-boolean p1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mIsVisualInteraction:Z

    return p1
.end method

.method static synthetic access$200(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;Landroid/widget/GridView;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/square/MusicSquareFragment;
    .param p1, "x1"    # Landroid/widget/GridView;

    .prologue
    .line 112
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->clearAllMusicSquareCell(Landroid/widget/GridView;)V

    return-void
.end method

.method static synthetic access$2000(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;Landroid/widget/GridView;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/square/MusicSquareFragment;
    .param p1, "x1"    # Landroid/widget/GridView;
    .param p2, "x2"    # I

    .prologue
    .line 112
    invoke-direct {p0, p1, p2}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->unselectMusicSquareCell(Landroid/widget/GridView;I)V

    return-void
.end method

.method static synthetic access$2100(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;Landroid/widget/GridView;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/square/MusicSquareFragment;
    .param p1, "x1"    # Landroid/widget/GridView;
    .param p2, "x2"    # I

    .prologue
    .line 112
    invoke-direct {p0, p1, p2}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->setAlbumArtMusicSquareCell(Landroid/widget/GridView;I)V

    return-void
.end method

.method static synthetic access$2200(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    .prologue
    .line 112
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->invalidateAllViews()V

    return-void
.end method

.method static synthetic access$2300()Landroid/content/SharedPreferences;
    .locals 1

    .prologue
    .line 112
    sget-object v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->sPreferences:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/square/MusicSquareFragment;
    .param p1, "x1"    # I

    .prologue
    .line 112
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->changeAxis(I)V

    return-void
.end method

.method static synthetic access$2500(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    .prologue
    .line 112
    iget-object v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSquareUpdateHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;I)[Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/square/MusicSquareFragment;
    .param p1, "x1"    # I

    .prologue
    .line 112
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->getCellHoverView(I)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2700(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    .prologue
    .line 112
    iget v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mPlayingCell:I

    return v0
.end method

.method static synthetic access$2800(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    .prologue
    .line 112
    iget-object v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mAlbumBm:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$2900(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    .prologue
    .line 112
    iget-object v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSelectedCellList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;Landroid/widget/GridView;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/square/MusicSquareFragment;
    .param p1, "x1"    # Landroid/widget/GridView;
    .param p2, "x2"    # I

    .prologue
    .line 112
    invoke-direct {p0, p1, p2}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->selectMusicSquareCell(Landroid/widget/GridView;I)V

    return-void
.end method

.method static synthetic access$3000(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;Landroid/widget/ImageView;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/square/MusicSquareFragment;
    .param p1, "x1"    # Landroid/widget/ImageView;
    .param p2, "x2"    # I

    .prologue
    .line 112
    invoke-direct {p0, p1, p2}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->setCellAirView(Landroid/widget/ImageView;I)V

    return-void
.end method

.method static synthetic access$400(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    .prologue
    .line 112
    iget-boolean v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mIsOnResumed:Z

    return v0
.end method

.method static synthetic access$500(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    .prologue
    .line 112
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->doPlaySelectedMusicSquareCell()Z

    move-result v0

    return v0
.end method

.method static synthetic access$600(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    .prologue
    .line 112
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->saveSelectedCellPref()V

    return-void
.end method

.method static synthetic access$700(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    .prologue
    .line 112
    iget-boolean v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mIsReadyToClick:Z

    return v0
.end method

.method static synthetic access$702(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/square/MusicSquareFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 112
    iput-boolean p1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mIsReadyToClick:Z

    return p1
.end method

.method static synthetic access$800(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    .prologue
    .line 112
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->clearVisualInteraction()V

    return-void
.end method

.method static synthetic access$900(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    .prologue
    .line 112
    iget-boolean v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mIsDown:Z

    return v0
.end method

.method static synthetic access$902(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/square/MusicSquareFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 112
    iput-boolean p1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mIsDown:Z

    return p1
.end method

.method private cancelLongClick()V
    .locals 2

    .prologue
    .line 908
    iget-object v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSquareUpdateHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 909
    return-void
.end method

.method private changeAxis(I)V
    .locals 8
    .param p1, "axis"    # I

    .prologue
    const/4 v7, 0x0

    .line 975
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->getView()Landroid/view/View;

    move-result-object v5

    if-nez v5, :cond_1

    .line 976
    sget-object v5, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->CLASSNAME:Ljava/lang/String;

    const-string v6, "changeAxis() : getView() is null. so return"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1030
    :cond_0
    :goto_0
    return-void

    .line 980
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->getView()Landroid/view/View;

    move-result-object v5

    const v6, 0x7f0d0033

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 981
    .local v4, "topView":Landroid/widget/TextView;
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->getView()Landroid/view/View;

    move-result-object v5

    const v6, 0x7f0d0028

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 982
    .local v1, "bottomView":Landroid/widget/TextView;
    if-eqz v4, :cond_0

    if-eqz v1, :cond_0

    .line 986
    const v5, 0x7f1000c6

    invoke-virtual {p0, v5}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 987
    const v5, 0x7f1000c5

    invoke-virtual {p0, v5}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 991
    packed-switch p1, :pswitch_data_0

    .line 1001
    const v2, 0x7f1000ca

    .line 1002
    .local v2, "left":I
    const v3, 0x7f1000c7

    .line 1006
    .local v3, "right":I
    :goto_1
    iget-object v5, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mLeftText:Landroid/widget/TextView;

    invoke-virtual {p0, v2}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1007
    iget-object v5, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mLeftText:Landroid/widget/TextView;

    const/high16 v6, 0x43870000    # 270.0f

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setRotation(F)V

    .line 1009
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0c0120

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 1011
    .local v0, "bottomPadding":I
    iget-object v5, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mLeftText:Landroid/widget/TextView;

    invoke-virtual {v5, v7, v7, v7, v0}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 1012
    iget-object v5, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mLeftText:Landroid/widget/TextView;

    const/4 v6, 0x2

    invoke-direct {p0, v5, v6}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->matchAlign(Landroid/widget/TextView;I)V

    .line 1014
    iget-object v5, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mRightText:Landroid/widget/TextView;

    invoke-virtual {p0, v3}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1015
    iget-object v5, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mRightText:Landroid/widget/TextView;

    const/high16 v6, 0x42b40000    # 90.0f

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setRotation(F)V

    .line 1016
    iget-object v5, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mRightText:Landroid/widget/TextView;

    const/4 v6, 0x1

    invoke-direct {p0, v5, v6}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->matchAlign(Landroid/widget/TextView;I)V

    .line 1018
    iget v5, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mAxis:I

    if-eq v5, p1, :cond_0

    .line 1023
    iput p1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mAxis:I

    .line 1025
    iget-object v5, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSquareGridView:Landroid/widget/GridView;

    invoke-direct {p0, v5}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->clearAllMusicSquareCell(Landroid/widget/GridView;)V

    .line 1027
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->cleanAlbumBitmap()V

    .line 1028
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->saveSelectedCellPref()V

    .line 1029
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->invalidateAllViews()V

    goto/16 :goto_0

    .line 993
    .end local v0    # "bottomPadding":I
    .end local v2    # "left":I
    .end local v3    # "right":I
    :pswitch_0
    const v2, 0x7f1000ca

    .line 994
    .restart local v2    # "left":I
    const v3, 0x7f1000c7

    .line 995
    .restart local v3    # "right":I
    goto :goto_1

    .line 997
    .end local v2    # "left":I
    .end local v3    # "right":I
    :pswitch_1
    const v2, 0x7f1000c9

    .line 998
    .restart local v2    # "left":I
    const v3, 0x7f1000c8

    .line 999
    .restart local v3    # "right":I
    goto :goto_1

    .line 991
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private checkMovePointUserIntented(II)Z
    .locals 3
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    const/4 v2, 0x1

    .line 513
    const/4 v0, 0x1

    .line 515
    .local v0, "result":Z
    iget v1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mBaseXPoint:I

    sub-int/2addr v1, p1

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    if-ge v1, v2, :cond_1

    .line 516
    const/4 v0, 0x0

    .line 521
    :cond_0
    :goto_0
    return v0

    .line 517
    :cond_1
    iget v1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mBaseYPoint:I

    sub-int/2addr v1, p2

    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v1

    if-ge v1, v2, :cond_0

    .line 518
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private cleanAlbumArtBitmapVisualInteraction()V
    .locals 4

    .prologue
    .line 809
    iget-object v3, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->sAlbumArtBmVisualInteraction:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 810
    .local v2, "image":Ljava/lang/Object;
    instance-of v3, v2, Landroid/graphics/Bitmap;

    if-eqz v3, :cond_0

    move-object v0, v2

    .line 811
    check-cast v0, Landroid/graphics/Bitmap;

    .line 812
    .local v0, "bm":Landroid/graphics/Bitmap;
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v3

    if-nez v3, :cond_0

    .line 813
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0

    .line 818
    .end local v0    # "bm":Landroid/graphics/Bitmap;
    .end local v2    # "image":Ljava/lang/Object;
    :cond_1
    iget-object v3, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->sAlbumArtBmVisualInteraction:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 819
    return-void
.end method

.method private cleanAlbumBitmap()V
    .locals 2

    .prologue
    .line 1633
    sget-object v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->CLASSNAME:Ljava/lang/String;

    const-string v1, "cleanAlbumBitmap"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1634
    iget-object v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mAlbumBm:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 1635
    iget-object v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mAlbumBm:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1636
    iget-object v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mAlbumBm:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 1637
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mAlbumBm:Landroid/graphics/Bitmap;

    .line 1640
    :cond_0
    return-void
.end method

.method private clearAllMusicSquareCell(Landroid/widget/GridView;)V
    .locals 6
    .param p1, "v"    # Landroid/widget/GridView;

    .prologue
    .line 1256
    iget-object v4, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSelectedCellList:Ljava/util/ArrayList;

    if-eqz v4, :cond_0

    .line 1257
    iget-object v4, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSelectedCellList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->clear()V

    .line 1259
    :cond_0
    if-nez p1, :cond_2

    .line 1274
    :cond_1
    return-void

    .line 1262
    :cond_2
    invoke-virtual {p1}, Landroid/widget/GridView;->getChildCount()I

    move-result v3

    .line 1264
    .local v3, "size":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v3, :cond_1

    .line 1265
    invoke-virtual {p1, v1}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 1266
    .local v0, "child":Landroid/widget/ImageView;
    if-eqz v0, :cond_3

    .line 1267
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0c0116

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 1268
    .local v2, "pad":I
    invoke-virtual {v0, v2, v2, v2, v2}, Landroid/widget/ImageView;->setPadding(IIII)V

    .line 1269
    const v4, 0x7f0b0090

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1270
    const/16 v4, 0xff

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageAlpha(I)V

    .line 1271
    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 1264
    .end local v2    # "pad":I
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private clearSelectedCellPref()V
    .locals 4

    .prologue
    .line 890
    sget-object v1, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->sPreferences:Landroid/content/SharedPreferences;

    const-string v2, "savedSquare"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    .line 891
    .local v0, "values":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    if-eqz v0, :cond_0

    .line 892
    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 894
    :cond_0
    sget-object v1, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->sPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "savedSquare"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 895
    return-void
.end method

.method private clearVisualInteraction()V
    .locals 1

    .prologue
    .line 682
    iget-boolean v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mIsVisualInteraction:Z

    if-eqz v0, :cond_0

    .line 683
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mIsVisualInteraction:Z

    .line 684
    iget-object v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSquareGridView:Landroid/widget/GridView;

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->clearAllMusicSquareCell(Landroid/widget/GridView;)V

    .line 685
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->removeMessageAllVisualInteraction()V

    .line 686
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->restoreMusicSquareCell()V

    .line 687
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->cleanAlbumArtBitmapVisualInteraction()V

    .line 689
    :cond_0
    return-void
.end method

.method private detachChildFragmentManager()V
    .locals 4

    .prologue
    .line 303
    :try_start_0
    const-class v2, Landroid/app/Fragment;

    const-string v3, "mChildFragmentManager"

    invoke-virtual {v2, v3}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 304
    .local v0, "childfm":Ljava/lang/reflect/Field;
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 305
    const/4 v2, 0x0

    invoke-virtual {v0, p0, v2}, Ljava/lang/reflect/Field;->set(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    .line 314
    .end local v0    # "childfm":Ljava/lang/reflect/Field;
    :goto_0
    return-void

    .line 307
    :catch_0
    move-exception v1

    .line 308
    .local v1, "e":Ljava/lang/NoSuchFieldException;
    sget-object v2, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->CLASSNAME:Ljava/lang/String;

    const-string v3, "this is Work arround in KK, there are no logic that attach activity again in child fragment manager. If mChildFragmentManager field name was changed you should check the framework codes."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 310
    .end local v1    # "e":Ljava/lang/NoSuchFieldException;
    :catch_1
    move-exception v1

    .line 311
    .local v1, "e":Ljava/lang/IllegalAccessException;
    sget-object v2, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->CLASSNAME:Ljava/lang/String;

    const-string v3, "this is Work arround in KK, there are no logic that attach activity again in child fragment manager. If mChildFragmentManager field name was changed you should check the framework codes."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private doPlaySelectedMusicSquareCell()Z
    .locals 18

    .prologue
    .line 1149
    const/4 v12, 0x1

    .line 1150
    .local v12, "isSucess":Z
    invoke-direct/range {p0 .. p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->isEqualSelectMusicSquareCell()Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mHasNewCells:Z

    .line 1151
    sget-object v2, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->CLASSNAME:Ljava/lang/String;

    const-string v3, "doPlaySelectedMusicSquareCell"

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1152
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSelectedCellList:Ljava/util/ArrayList;

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Integer;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v14

    check-cast v14, [Ljava/lang/Integer;

    .line 1153
    .local v14, "selecteCells":[Ljava/lang/Integer;
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_1
    array-length v2, v14

    if-ge v11, v2, :cond_1

    .line 1154
    sget-object v2, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->CLASSNAME:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "selecteCells["

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "] : "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v5, v14, v11

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1153
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 1150
    .end local v11    # "i":I
    .end local v14    # "selecteCells":[Ljava/lang/Integer;
    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    .line 1156
    .restart local v11    # "i":I
    .restart local v14    # "selecteCells":[Ljava/lang/Integer;
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSelectedCellList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v16

    .line 1157
    .local v16, "size":I
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 1158
    .local v8, "cellIds":Ljava/lang/StringBuilder;
    const/4 v11, 0x0

    :goto_2
    move/from16 v0, v16

    if-ge v11, v0, :cond_3

    .line 1159
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSelectedCellList:Ljava/util/ArrayList;

    invoke-virtual {v2, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1160
    add-int/lit8 v2, v16, -0x1

    if-ge v11, v2, :cond_2

    .line 1161
    const-string v2, ","

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1158
    :cond_2
    add-int/lit8 v11, v11, 0x1

    goto :goto_2

    .line 1167
    :cond_3
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mAxis:I

    packed-switch v2, :pswitch_data_0

    .line 1175
    const-string v15, "mood_cell"

    .line 1179
    .local v15, "selectedAxis":Ljava/lang/String;
    :goto_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " IN ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1180
    .local v4, "selection":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    .line 1181
    .local v9, "context":Landroid/content/Context;
    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 1182
    .local v1, "resolver":Landroid/content/ContentResolver;
    sget-object v2, Lcom/samsung/musicplus/provider/MusicContents$Audio$Media;->CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "source_id AS _id"

    aput-object v6, v3, v5

    const/4 v5, 0x1

    const-string v6, "mood_cell"

    aput-object v6, v3, v5

    const/4 v5, 0x2

    const-string v6, "year_cell"

    aput-object v6, v3, v5

    const/4 v5, 0x0

    const-string v6, "title_key"

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 1187
    .local v10, "data":Landroid/database/Cursor;
    if-eqz v10, :cond_a

    .line 1188
    invoke-static {v10}, Lcom/samsung/musicplus/util/ListUtils;->getSongListForCursor(Landroid/database/Cursor;)[J

    move-result-object v7

    .line 1189
    .local v7, "audioids":[J
    const-string v2, "_id"

    invoke-static {v2, v7}, Lcom/samsung/musicplus/util/UiUtils;->convertAudioIdsToSelection(Ljava/lang/String;[J)Ljava/lang/String;

    move-result-object v13

    .line 1190
    .local v13, "key":Ljava/lang/String;
    if-nez v13, :cond_6

    .line 1191
    const/4 v12, 0x0

    .line 1197
    :goto_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mKeyWord:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->getAudioIds(Ljava/lang/String;)[J

    move-result-object v17

    .line 1198
    .local v17, "songList":[J
    move-object/from16 v0, v17

    array-length v2, v0

    if-eqz v2, :cond_4

    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    if-nez v2, :cond_7

    .line 1199
    :cond_4
    const/4 v12, 0x0

    .line 1200
    const v2, 0x7f100155

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v9, v2}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    .line 1219
    :goto_5
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 1224
    .end local v7    # "audioids":[J
    .end local v13    # "key":Ljava/lang/String;
    .end local v17    # "songList":[J
    :goto_6
    if-nez v12, :cond_5

    .line 1225
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mKeyWord:Ljava/lang/String;

    .line 1227
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSquareGridView:Landroid/widget/GridView;

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->clearAllMusicSquareCell(Landroid/widget/GridView;)V

    .line 1230
    :cond_5
    return v12

    .line 1169
    .end local v1    # "resolver":Landroid/content/ContentResolver;
    .end local v4    # "selection":Ljava/lang/String;
    .end local v9    # "context":Landroid/content/Context;
    .end local v10    # "data":Landroid/database/Cursor;
    .end local v15    # "selectedAxis":Ljava/lang/String;
    :pswitch_0
    const-string v15, "mood_cell"

    .line 1170
    .restart local v15    # "selectedAxis":Ljava/lang/String;
    goto/16 :goto_3

    .line 1172
    .end local v15    # "selectedAxis":Ljava/lang/String;
    :pswitch_1
    const-string v15, "year_cell"

    .line 1173
    .restart local v15    # "selectedAxis":Ljava/lang/String;
    goto/16 :goto_3

    .line 1193
    .restart local v1    # "resolver":Landroid/content/ContentResolver;
    .restart local v4    # "selection":Ljava/lang/String;
    .restart local v7    # "audioids":[J
    .restart local v9    # "context":Landroid/content/Context;
    .restart local v10    # "data":Landroid/database/Cursor;
    .restart local v13    # "key":Ljava/lang/String;
    :cond_6
    const/4 v12, 0x1

    .line 1194
    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mKeyWord:Ljava/lang/String;

    goto :goto_4

    .line 1202
    .restart local v17    # "songList":[J
    :cond_7
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mHasNewCells:Z

    if-nez v2, :cond_8

    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->isPlaying()Z

    move-result v2

    if-nez v2, :cond_9

    .line 1203
    :cond_8
    const v2, 0x2000a

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mKeyWord:Ljava/lang/String;

    const/4 v5, 0x0

    move-object/from16 v0, v17

    invoke-static {v9, v2, v3, v0, v5}, Lcom/samsung/musicplus/util/ServiceUtils;->openList(Landroid/content/Context;ILjava/lang/String;[JI)Z

    .line 1205
    const-string v2, "MSSQ"

    invoke-static {v9, v2}, Lcom/samsung/musicplus/util/logging/FeatureLogger;->insertLog(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_5

    .line 1207
    :cond_9
    new-instance v2, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$8;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$8;-><init>(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;)V

    invoke-virtual {v2}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$8;->start()V

    goto :goto_5

    .line 1221
    .end local v7    # "audioids":[J
    .end local v13    # "key":Ljava/lang/String;
    .end local v17    # "songList":[J
    :cond_a
    const/4 v12, 0x0

    goto :goto_6

    .line 1167
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private doVisualInteration()V
    .locals 20

    .prologue
    .line 593
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSquareGridView:Landroid/widget/GridView;

    move-object/from16 v16, v0

    if-eqz v16, :cond_3

    .line 594
    const/4 v3, 0x1

    .line 595
    .local v3, "INTERVAL":I
    const/16 v4, 0x7d0

    .line 596
    .local v4, "START_INTERVAL":I
    const/16 v2, 0x7d0

    .line 598
    .local v2, "END_INTERVAL":I
    const/4 v5, -0x1

    .line 599
    .local v5, "action":I
    const/16 v16, -0x1

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    .line 600
    .local v13, "position":Ljava/lang/Integer;
    const/4 v7, -0x1

    .line 601
    .local v7, "delay":I
    const/4 v12, -0x1

    .line 603
    .local v12, "maxdelay":I
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v16

    const v17, 0x7f090005

    invoke-virtual/range {v16 .. v17}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v15

    .line 605
    .local v15, "stream":[Ljava/lang/String;
    const/16 v16, 0x1

    move/from16 v0, v16

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mIsVisualInteraction:Z

    .line 607
    invoke-direct/range {p0 .. p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->cleanAlbumBitmap()V

    .line 608
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSquareGridView:Landroid/widget/GridView;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-direct {v0, v1}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->clearAllMusicSquareCell(Landroid/widget/GridView;)V

    .line 609
    invoke-direct/range {p0 .. p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->removeMessageAllVisualInteraction()V

    .line 610
    invoke-direct/range {p0 .. p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->cleanAlbumArtBitmapVisualInteraction()V

    .line 612
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSquareVisualInteractionAnimatinHandler:Landroid/os/Handler;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    invoke-virtual/range {v16 .. v17}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 613
    const/4 v10, 0x0

    .local v10, "j":I
    :goto_0
    sget v16, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->sMusicSquareColumn:I

    sget v17, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->sMusicSquareColumn:I

    mul-int v16, v16, v17

    move/from16 v0, v16

    if-ge v10, v0, :cond_0

    .line 614
    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->restoreFirstAlbumArtCellInfo(I)V

    .line 613
    add-int/lit8 v10, v10, 0x1

    goto :goto_0

    .line 616
    :cond_0
    move-object v6, v15

    .local v6, "arr$":[Ljava/lang/String;
    array-length v11, v6

    .local v11, "len$":I
    const/4 v9, 0x0

    .local v9, "i$":I
    :goto_1
    if-ge v9, v11, :cond_2

    aget-object v8, v6, v9

    .line 617
    .local v8, "element":Ljava/lang/String;
    const-string v16, ","

    move-object/from16 v0, v16

    invoke-virtual {v8, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v14

    .line 619
    .local v14, "str":[Ljava/lang/String;
    const/16 v16, 0x0

    aget-object v16, v14, v16

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 620
    const/16 v16, 0x1

    aget-object v16, v14, v16

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v16

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    .line 621
    const/16 v16, 0x2

    aget-object v16, v14, v16

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    .line 623
    if-le v7, v12, :cond_1

    .line 624
    move v12, v7

    .line 626
    :cond_1
    mul-int/lit8 v16, v7, 0x1

    move/from16 v0, v16

    add-int/lit16 v7, v0, 0x7d0

    .line 627
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSquareVisualInteractionAnimatinHandler:Landroid/os/Handler;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSquareVisualInteractionAnimatinHandler:Landroid/os/Handler;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v5, v13}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v17

    int-to-long v0, v7

    move-wide/from16 v18, v0

    invoke-virtual/range {v16 .. v19}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 616
    add-int/lit8 v9, v9, 0x1

    goto :goto_1

    .line 631
    .end local v8    # "element":Ljava/lang/String;
    .end local v14    # "str":[Ljava/lang/String;
    :cond_2
    mul-int/lit8 v16, v12, 0x1

    move/from16 v0, v16

    add-int/lit16 v0, v0, 0x7d0

    move/from16 v16, v0

    move/from16 v0, v16

    add-int/lit16 v12, v0, 0x7d0

    .line 632
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSquareVisualInteractionAnimatinHandler:Landroid/os/Handler;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    int-to-long v0, v12

    move-wide/from16 v18, v0

    invoke-virtual/range {v16 .. v19}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 634
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSquareVisualInteractionAnimatinHandler:Landroid/os/Handler;

    move-object/from16 v16, v0

    const/16 v17, 0x64

    int-to-long v0, v12

    move-wide/from16 v18, v0

    invoke-virtual/range {v16 .. v19}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 637
    .end local v2    # "END_INTERVAL":I
    .end local v3    # "INTERVAL":I
    .end local v4    # "START_INTERVAL":I
    .end local v5    # "action":I
    .end local v6    # "arr$":[Ljava/lang/String;
    .end local v7    # "delay":I
    .end local v9    # "i$":I
    .end local v10    # "j":I
    .end local v11    # "len$":I
    .end local v12    # "maxdelay":I
    .end local v13    # "position":Ljava/lang/Integer;
    .end local v15    # "stream":[Ljava/lang/String;
    :cond_3
    return-void
.end method

.method private doVisualInteration2()V
    .locals 22

    .prologue
    .line 545
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSquareGridView:Landroid/widget/GridView;

    move-object/from16 v17, v0

    if-eqz v17, :cond_2

    .line 546
    const/4 v5, 0x1

    .line 547
    .local v5, "INTERVAL":I
    const/16 v6, 0x3e8

    .line 548
    .local v6, "START_INTERVAL":I
    const/16 v4, 0xc8

    .line 550
    .local v4, "END_INTERVAL":I
    const/4 v7, -0x1

    .line 551
    .local v7, "action":I
    const/16 v17, -0x1

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    .line 552
    .local v14, "position":Ljava/lang/Integer;
    const/4 v9, -0x1

    .line 553
    .local v9, "delay":I
    const/4 v13, -0x1

    .line 555
    .local v13, "maxdelay":I
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v17

    const v18, 0x7f090006

    invoke-virtual/range {v17 .. v18}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v16

    .line 557
    .local v16, "stream":[Ljava/lang/String;
    const/16 v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mIsVisualInteraction:Z

    .line 559
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSquareGridView:Landroid/widget/GridView;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->clearAllMusicSquareCell(Landroid/widget/GridView;)V

    .line 560
    invoke-direct/range {p0 .. p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->cleanAlbumBitmap()V

    .line 561
    invoke-direct/range {p0 .. p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->removeMessageAllVisualInteraction()V

    .line 562
    invoke-direct/range {p0 .. p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->cleanAlbumArtBitmapVisualInteraction()V

    .line 564
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSquareVisualInteractionAnimatinHandler:Landroid/os/Handler;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-virtual/range {v17 .. v18}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 566
    move-object/from16 v8, v16

    .local v8, "arr$":[Ljava/lang/String;
    array-length v12, v8

    .local v12, "len$":I
    const/4 v11, 0x0

    .local v11, "i$":I
    :goto_0
    if-ge v11, v12, :cond_1

    aget-object v10, v8, v11

    .line 567
    .local v10, "element":Ljava/lang/String;
    const-string v17, ","

    move-object/from16 v0, v17

    invoke-virtual {v10, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v15

    .line 569
    .local v15, "str":[Ljava/lang/String;
    const/16 v17, 0x0

    aget-object v17, v15, v17

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    .line 570
    const/16 v17, 0x1

    aget-object v17, v15, v17

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v17

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v14

    .line 571
    const/16 v17, 0x2

    aget-object v17, v15, v17

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    .line 573
    if-le v9, v13, :cond_0

    .line 574
    move v13, v9

    .line 576
    :cond_0
    mul-int/lit8 v17, v9, 0x1

    move/from16 v0, v17

    add-int/lit16 v9, v0, 0x3e8

    .line 577
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSquareVisualInteractionAnimatinHandler:Landroid/os/Handler;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSquareVisualInteractionAnimatinHandler:Landroid/os/Handler;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v7, v14}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v18

    int-to-long v0, v9

    move-wide/from16 v20, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-wide/from16 v2, v20

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 566
    add-int/lit8 v11, v11, 0x1

    goto :goto_0

    .line 581
    .end local v10    # "element":Ljava/lang/String;
    .end local v15    # "str":[Ljava/lang/String;
    :cond_1
    mul-int/lit8 v17, v13, 0x1

    move/from16 v0, v17

    add-int/lit16 v0, v0, 0x3e8

    move/from16 v17, v0

    move/from16 v0, v17

    add-int/lit16 v13, v0, 0xc8

    .line 582
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSquareVisualInteractionAnimatinHandler:Landroid/os/Handler;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    int-to-long v0, v13

    move-wide/from16 v20, v0

    move-object/from16 v0, v17

    move/from16 v1, v18

    move-wide/from16 v2, v20

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 584
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSquareVisualInteractionAnimatinHandler:Landroid/os/Handler;

    move-object/from16 v17, v0

    const/16 v18, 0x64

    int-to-long v0, v13

    move-wide/from16 v20, v0

    move-object/from16 v0, v17

    move/from16 v1, v18

    move-wide/from16 v2, v20

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 587
    .end local v4    # "END_INTERVAL":I
    .end local v5    # "INTERVAL":I
    .end local v6    # "START_INTERVAL":I
    .end local v7    # "action":I
    .end local v8    # "arr$":[Ljava/lang/String;
    .end local v9    # "delay":I
    .end local v11    # "i$":I
    .end local v12    # "len$":I
    .end local v13    # "maxdelay":I
    .end local v14    # "position":Ljava/lang/Integer;
    .end local v16    # "stream":[Ljava/lang/String;
    :cond_2
    return-void
.end method

.method private ensureListView()V
    .locals 6

    .prologue
    .line 464
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 465
    .local v0, "a":Landroid/app/Activity;
    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v0}, Landroid/app/Activity;->isDestroyed()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->isAdded()Z

    move-result v3

    if-nez v3, :cond_2

    .line 467
    :cond_0
    sget-object v3, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->CLASSNAME:Ljava/lang/String;

    const-string v4, "This activity is already distroyed or finishing."

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 491
    :cond_1
    :goto_0
    return-void

    .line 470
    :cond_2
    iget-boolean v3, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mIsSelected:Z

    if-nez v3, :cond_3

    .line 471
    sget-object v3, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->CLASSNAME:Ljava/lang/String;

    const-string v4, "tab unselected, return!!"

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 474
    :cond_3
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->getSubList()Landroid/app/Fragment;

    move-result-object v2

    .line 475
    .local v2, "subList":Landroid/app/Fragment;
    if-nez v2, :cond_4

    .line 476
    iget-object v3, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mKeyWord:Ljava/lang/String;

    invoke-static {v3}, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->getNewInstance(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v2

    .line 484
    :cond_4
    :try_start_0
    invoke-virtual {v2}, Landroid/app/Fragment;->isAdded()Z

    move-result v3

    if-nez v3, :cond_1

    .line 485
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->getChildFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v3

    const v4, 0x7f0d0165

    const-string v5, "square_list"

    invoke-virtual {v3, v4, v2, v5}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 488
    :catch_0
    move-exception v1

    .line 489
    .local v1, "e":Ljava/lang/IllegalStateException;
    sget-object v3, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->CLASSNAME:Ljava/lang/String;

    const-string v4, "Prevent IllegalStateException"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private ensureSquareView()V
    .locals 8

    .prologue
    .line 318
    iget-object v3, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSquareGridView:Landroid/widget/GridView;

    if-eqz v3, :cond_1

    .line 461
    :cond_0
    :goto_0
    return-void

    .line 322
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->getView()Landroid/view/View;

    move-result-object v2

    .line 324
    .local v2, "mainView":Landroid/view/View;
    if-eqz v2, :cond_0

    .line 328
    sget v3, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->sMusicSquareColumn:I

    if-nez v3, :cond_2

    .line 331
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0e0003

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v3

    sput v3, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->sMusicSquareColumn:I

    .line 334
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    .line 335
    .local v1, "context":Landroid/content/Context;
    sget-boolean v3, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_MASS_PROJECT:Z

    if-nez v3, :cond_3

    .line 336
    :cond_3
    const v3, 0x7f0d016d

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/GridView;

    iput-object v3, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSquareGridView:Landroid/widget/GridView;

    .line 340
    const v3, 0x7f0d0031

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mLeftText:Landroid/widget/TextView;

    .line 341
    const v3, 0x7f0d0032

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mRightText:Landroid/widget/TextView;

    .line 342
    iget v3, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mAxis:I

    invoke-direct {p0, v3}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->changeAxis(I)V

    .line 344
    iget-object v3, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSquareGridView:Landroid/widget/GridView;

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/hardware/AirView;->setHoverScrollMode(Landroid/widget/AbsListView;Z)V

    .line 346
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    const v4, 0x7f050002

    invoke-static {v3, v4}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 347
    .local v0, "anim":Landroid/view/animation/Animation;
    const-wide/16 v4, 0x1f4

    invoke-virtual {v0, v4, v5}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 349
    iget-object v3, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSquareGridView:Landroid/widget/GridView;

    invoke-virtual {v3, v0}, Landroid/widget/GridView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 350
    iget-object v3, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSquareGridView:Landroid/widget/GridView;

    const/4 v4, 0x2

    invoke-virtual {v3, v4}, Landroid/widget/GridView;->setOverScrollMode(I)V

    .line 351
    new-instance v3, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$MusicSquareAdapter;

    invoke-direct {v3, p0, v1}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$MusicSquareAdapter;-><init>(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;Landroid/content/Context;)V

    sput-object v3, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->sAdapter:Lcom/samsung/musicplus/contents/square/MusicSquareFragment$MusicSquareAdapter;

    .line 352
    iget-object v3, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSquareGridView:Landroid/widget/GridView;

    sget-object v4, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->sAdapter:Lcom/samsung/musicplus/contents/square/MusicSquareFragment$MusicSquareAdapter;

    invoke-virtual {v3, v4}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 354
    iget-object v3, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSquareGridView:Landroid/widget/GridView;

    new-instance v4, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$3;

    invoke-direct {v4, p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$3;-><init>(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;)V

    invoke-virtual {v3, v4}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 368
    iget-object v3, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSquareGridView:Landroid/widget/GridView;

    new-instance v4, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$4;

    invoke-direct {v4, p0, v1}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$4;-><init>(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;Landroid/content/Context;)V

    invoke-virtual {v3, v4}, Landroid/widget/GridView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 449
    sget-boolean v3, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_SUPPORT_SQUARE_VISUAL_INTERACTION_ANIMATION:Z

    if-eqz v3, :cond_4

    sget v3, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->sMusicSquareColumn:I

    const/4 v4, 0x5

    if-ne v3, v4, :cond_4

    .line 451
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->restoreMusicSquareCell()V

    .line 460
    :goto_1
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mIsReadyToClick:Z

    goto/16 :goto_0

    .line 453
    :cond_4
    iget-object v3, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSquareGridView:Landroid/widget/GridView;

    new-instance v4, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$5;

    invoke-direct {v4, p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$5;-><init>(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;)V

    const-wide/16 v6, 0xc8

    invoke-virtual {v3, v4, v6, v7}, Landroid/widget/GridView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1
.end method

.method private getAudioIds(Ljava/lang/String;)[J
    .locals 8
    .param p1, "keyWord"    # Ljava/lang/String;

    .prologue
    .line 1234
    const v0, 0x2000a

    invoke-static {v0, p1}, Lcom/samsung/musicplus/util/ListUtils;->getMusicListInfo(ILjava/lang/String;)Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;

    move-result-object v0

    iget-object v6, v0, Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;->queryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    .line 1235
    .local v6, "args":Lcom/samsung/musicplus/util/ListUtils$QueryArgs;
    const/4 v7, 0x0

    .line 1237
    .local v7, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, v6, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->uri:Landroid/net/Uri;

    iget-object v2, v6, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->projection:[Ljava/lang/String;

    iget-object v3, v6, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->selection:Ljava/lang/String;

    iget-object v4, v6, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->selectionArgs:[Ljava/lang/String;

    iget-object v5, v6, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->orderBy:Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 1242
    invoke-static {v7}, Lcom/samsung/musicplus/util/ListUtils;->getSongListForCursor(Landroid/database/Cursor;)[J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1244
    if-eqz v7, :cond_0

    .line 1245
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_0
    return-object v0

    .line 1244
    :catchall_0
    move-exception v0

    if-eqz v7, :cond_1

    .line 1245
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v0
.end method

.method private getCellHoverView(I)[Ljava/lang/Object;
    .locals 16
    .param p1, "position"    # I

    .prologue
    .line 1430
    invoke-direct/range {p0 .. p1}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->makeCellInfo(I)Landroid/database/Cursor;

    move-result-object v2

    .line 1431
    .local v2, "c":Landroid/database/Cursor;
    const/4 v11, 0x2

    new-array v7, v11, [Ljava/lang/Object;

    .line 1432
    .local v7, "parameter":[Ljava/lang/Object;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->getActivity()Landroid/app/Activity;

    move-result-object v11

    invoke-virtual {v11}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v5

    .line 1433
    .local v5, "inflater":Landroid/view/LayoutInflater;
    if-nez v2, :cond_1

    .line 1434
    const v11, 0x7f040016

    const/4 v12, 0x0

    invoke-virtual {v5, v11, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v9

    .local v9, "preview":Landroid/view/View;
    move-object v11, v9

    .line 1435
    check-cast v11, Landroid/widget/TextView;

    const v12, 0x7f1000ff

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setText(I)V

    .line 1436
    const/4 v11, 0x0

    aput-object v9, v7, v11

    .line 1437
    const/4 v11, 0x1

    const/4 v12, 0x0

    invoke-static {v12}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v12

    aput-object v12, v7, v11

    .line 1478
    .end local v9    # "preview":Landroid/view/View;
    :cond_0
    :goto_0
    return-object v7

    .line 1441
    :cond_1
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v3

    .line 1442
    .local v3, "count":I
    if-nez v3, :cond_2

    .line 1443
    const v11, 0x7f040016

    const/4 v12, 0x0

    invoke-virtual {v5, v11, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v9

    .line 1444
    .restart local v9    # "preview":Landroid/view/View;
    move-object v0, v9

    check-cast v0, Landroid/widget/TextView;

    move-object v11, v0

    const v12, 0x7f1000ff

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setText(I)V

    .line 1445
    const/4 v11, 0x0

    aput-object v9, v7, v11

    .line 1446
    const/4 v11, 0x1

    const/4 v12, 0x0

    invoke-static {v12}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v12

    aput-object v12, v7, v11
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1477
    if-eqz v2, :cond_0

    .line 1478
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 1449
    .end local v9    # "preview":Landroid/view/View;
    :cond_2
    const v11, 0x7f040015

    const/4 v12, 0x0

    :try_start_1
    invoke-virtual {v5, v11, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v8

    .line 1450
    .local v8, "popupList":Landroid/view/View;
    const/4 v4, 0x0

    .line 1451
    .local v4, "i":I
    const-string v11, "title"

    invoke-interface {v2, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v10

    .line 1452
    .local v10, "titleIdx":I
    const-string v11, "artist"

    invoke-interface {v2, v11}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 1453
    .local v1, "artistIdx":I
    :cond_3
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v11

    if-eqz v11, :cond_5

    .line 1454
    sget-object v11, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->HOVER_TITLE_RES:[I

    aget v11, v11, v4

    invoke-virtual {v8, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->getContext()Landroid/content/Context;

    move-result-object v12

    invoke-interface {v2, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/samsung/musicplus/util/UiUtils;->transUnknownString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1456
    sget-object v11, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->HOVER_ARTIST_RES:[I

    aget v11, v11, v4

    invoke-virtual {v8, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/TextView;

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->getContext()Landroid/content/Context;

    move-result-object v12

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v13

    invoke-static {v12, v13}, Lcom/samsung/musicplus/util/UiUtils;->transUnknownString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1459
    if-lez v4, :cond_4

    .line 1460
    sget-object v11, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->HOVER_CONTAINER_RES:[I

    aget v11, v11, v4

    invoke-virtual {v8, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/LinearLayout;

    const/4 v12, 0x0

    invoke-virtual {v11, v12}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 1463
    :cond_4
    add-int/lit8 v4, v4, 0x1

    const/4 v11, 0x3

    if-le v4, v11, :cond_3

    .line 1467
    :cond_5
    const/4 v11, 0x0

    aput-object v8, v7, v11

    .line 1468
    const/4 v11, 0x1

    const/4 v12, 0x0

    invoke-static {v12}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v12

    aput-object v12, v7, v11

    .line 1469
    add-int/lit8 v11, v3, -0x4

    if-lez v11, :cond_6

    .line 1470
    const v11, 0x7f0d007a

    invoke-virtual {v8, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 1471
    .local v6, "more":Landroid/widget/TextView;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f100022

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    add-int/lit8 v15, v3, -0x4

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v13, v14

    invoke-virtual {v11, v12, v13}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v6, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1472
    const/4 v11, 0x0

    invoke-virtual {v6, v11}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1473
    const/4 v11, 0x1

    const/4 v12, 0x1

    invoke-static {v12}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v12

    aput-object v12, v7, v11
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1477
    .end local v6    # "more":Landroid/widget/TextView;
    :cond_6
    if-eqz v2, :cond_0

    .line 1478
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 1477
    .end local v1    # "artistIdx":I
    .end local v3    # "count":I
    .end local v4    # "i":I
    .end local v8    # "popupList":Landroid/view/View;
    .end local v10    # "titleIdx":I
    :catchall_0
    move-exception v11

    if-eqz v2, :cond_7

    .line 1478
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_7
    throw v11
.end method

.method private getGravity(I)I
    .locals 3
    .param p1, "position"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1494
    sget v1, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->sMusicSquareColumn:I

    const/4 v2, 0x5

    if-ne v1, v2, :cond_0

    .line 1495
    packed-switch p1, :pswitch_data_0

    .line 1535
    const/16 v0, 0x3031

    .line 1541
    .local v0, "gravity":I
    :goto_0
    return v0

    .line 1500
    .end local v0    # "gravity":I
    :pswitch_0
    const/16 v0, 0x5053

    .line 1501
    .restart local v0    # "gravity":I
    goto :goto_0

    .line 1508
    .end local v0    # "gravity":I
    :pswitch_1
    const/16 v0, 0x3033

    .line 1509
    .restart local v0    # "gravity":I
    goto :goto_0

    .line 1512
    .end local v0    # "gravity":I
    :pswitch_2
    const/16 v0, 0x5051

    .line 1513
    .restart local v0    # "gravity":I
    goto :goto_0

    .line 1517
    .end local v0    # "gravity":I
    :pswitch_3
    const/16 v0, 0x3031

    .line 1518
    .restart local v0    # "gravity":I
    goto :goto_0

    .line 1523
    .end local v0    # "gravity":I
    :pswitch_4
    const/16 v0, 0x5055

    .line 1524
    .restart local v0    # "gravity":I
    goto :goto_0

    .line 1531
    .end local v0    # "gravity":I
    :pswitch_5
    const/16 v0, 0x3035

    .line 1532
    .restart local v0    # "gravity":I
    goto :goto_0

    .line 1539
    .end local v0    # "gravity":I
    :cond_0
    const/16 v0, 0x3031

    .restart local v0    # "gravity":I
    goto :goto_0

    .line 1495
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_4
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_4
        :pswitch_4
        :pswitch_1
        :pswitch_1
        :pswitch_3
        :pswitch_5
        :pswitch_5
        :pswitch_1
        :pswitch_1
        :pswitch_3
        :pswitch_5
        :pswitch_5
        :pswitch_1
        :pswitch_1
        :pswitch_3
        :pswitch_5
        :pswitch_5
    .end packed-switch
.end method

.method private getSubList()Landroid/app/Fragment;
    .locals 2

    .prologue
    .line 1882
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->getChildFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    .line 1883
    .local v0, "fm":Landroid/app/FragmentManager;
    const-string v1, "square_list"

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    return-object v1
.end method

.method private initializeCellAirViewListener()V
    .locals 1

    .prologue
    .line 1407
    iget-object v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mCellAirViewListener:Lcom/samsung/musicplus/library/hardware/AirView$OnAirViewMorePopupListener;

    if-nez v0, :cond_0

    .line 1408
    new-instance v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$9;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$9;-><init>(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;)V

    iput-object v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mCellAirViewListener:Lcom/samsung/musicplus/library/hardware/AirView$OnAirViewMorePopupListener;

    .line 1417
    :cond_0
    return-void
.end method

.method private invalidateAllViews()V
    .locals 2

    .prologue
    .line 1664
    sget-object v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->CLASSNAME:Ljava/lang/String;

    const-string v1, "invalidateAllViews"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1666
    iget-boolean v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mIsVisualInteraction:Z

    if-eqz v0, :cond_0

    .line 1667
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->invalidateSubListViews()V

    .line 1676
    :goto_0
    return-void

    .line 1670
    :cond_0
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->clearVisualInteraction()V

    .line 1671
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->setPlayingCell()V

    .line 1672
    iget-object v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSquareGridView:Landroid/widget/GridView;

    if-eqz v0, :cond_1

    .line 1673
    iget-object v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSquareGridView:Landroid/widget/GridView;

    invoke-virtual {v0}, Landroid/widget/GridView;->invalidateViews()V

    .line 1675
    :cond_1
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->invalidateSubListViews()V

    goto :goto_0
.end method

.method private invalidateSubListViews()V
    .locals 1

    .prologue
    .line 1679
    iget-boolean v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mIsOnResumed:Z

    if-eqz v0, :cond_0

    .line 1680
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->updateSubList()V

    .line 1682
    :cond_0
    return-void
.end method

.method private isEqualSelectMusicSquareCell()Z
    .locals 10

    .prologue
    const/4 v6, 0x0

    .line 1321
    iget-object v7, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSelectedCellList:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 1322
    .local v4, "selectedListSize":I
    sget-object v7, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->sPreferences:Landroid/content/SharedPreferences;

    const-string v8, "savedSquare"

    const/4 v9, 0x0

    invoke-interface {v7, v8, v9}, Landroid/content/SharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v5

    .line 1323
    .local v5, "values":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    if-eqz v5, :cond_2

    invoke-interface {v5}, Ljava/util/Set;->size()I

    move-result v7

    if-ne v7, v4, :cond_2

    .line 1324
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1325
    .local v2, "index":Ljava/lang/String;
    const/4 v3, 0x0

    .line 1326
    .local v3, "isContains":Z
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v4, :cond_1

    .line 1327
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v7

    iget-object v8, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSelectedCellList:Ljava/util/ArrayList;

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 1328
    const/4 v3, 0x1

    .line 1332
    :cond_1
    if-nez v3, :cond_0

    .line 1340
    .end local v0    # "i":I
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "index":Ljava/lang/String;
    .end local v3    # "isContains":Z
    :cond_2
    :goto_1
    return v6

    .line 1326
    .restart local v0    # "i":I
    .restart local v1    # "i$":Ljava/util/Iterator;
    .restart local v2    # "index":Ljava/lang/String;
    .restart local v3    # "isContains":Z
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1340
    .end local v0    # "i":I
    .end local v2    # "index":Ljava/lang/String;
    .end local v3    # "isContains":Z
    :cond_4
    const/4 v6, 0x1

    goto :goto_1
.end method

.method private makeCellInfo(I)Landroid/database/Cursor;
    .locals 16
    .param p1, "position"    # I

    .prologue
    .line 1353
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mAxis:I

    packed-switch v2, :pswitch_data_0

    .line 1361
    const-string v15, "mood_cell"

    .line 1365
    .local v15, "selectedAxis":Ljava/lang/String;
    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 1367
    .local v1, "resolver":Landroid/content/ContentResolver;
    const/4 v13, 0x0

    .line 1368
    .local v13, "data":Landroid/database/Cursor;
    const/4 v14, 0x0

    .line 1370
    .local v14, "key":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1372
    .local v4, "selection":Ljava/lang/String;
    :try_start_0
    sget-object v2, Lcom/samsung/musicplus/provider/MusicContents$Audio$Media;->CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "source_id AS _id"

    aput-object v6, v3, v5

    const/4 v5, 0x1

    const-string v6, "mood_cell"

    aput-object v6, v3, v5

    const/4 v5, 0x2

    const-string v6, "year_cell"

    aput-object v6, v3, v5

    const/4 v5, 0x0

    const-string v6, "title_key"

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v13

    .line 1378
    invoke-static {v13}, Lcom/samsung/musicplus/util/ListUtils;->getSongListForCursor(Landroid/database/Cursor;)[J

    move-result-object v12

    .line 1379
    .local v12, "audioids":[J
    const-string v2, "_id"

    invoke-static {v2, v12}, Lcom/samsung/musicplus/util/UiUtils;->convertAudioIdsToSelection(Ljava/lang/String;[J)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v14

    .line 1381
    if-eqz v13, :cond_0

    .line 1382
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    .line 1386
    :cond_0
    const v2, 0x2000a

    invoke-static {v2, v14}, Lcom/samsung/musicplus/util/ListUtils;->getMusicListInfo(ILjava/lang/String;)Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;

    move-result-object v2

    iget-object v11, v2, Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;->queryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    .line 1387
    .local v11, "args":Lcom/samsung/musicplus/util/ListUtils$QueryArgs;
    iget-object v6, v11, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->uri:Landroid/net/Uri;

    iget-object v7, v11, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->projection:[Ljava/lang/String;

    iget-object v8, v11, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->selection:Ljava/lang/String;

    iget-object v9, v11, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->selectionArgs:[Ljava/lang/String;

    iget-object v10, v11, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->orderBy:Ljava/lang/String;

    move-object v5, v1

    invoke-virtual/range {v5 .. v10}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    return-object v2

    .line 1355
    .end local v1    # "resolver":Landroid/content/ContentResolver;
    .end local v4    # "selection":Ljava/lang/String;
    .end local v11    # "args":Lcom/samsung/musicplus/util/ListUtils$QueryArgs;
    .end local v12    # "audioids":[J
    .end local v13    # "data":Landroid/database/Cursor;
    .end local v14    # "key":Ljava/lang/String;
    .end local v15    # "selectedAxis":Ljava/lang/String;
    :pswitch_0
    const-string v15, "mood_cell"

    .line 1356
    .restart local v15    # "selectedAxis":Ljava/lang/String;
    goto :goto_0

    .line 1358
    .end local v15    # "selectedAxis":Ljava/lang/String;
    :pswitch_1
    const-string v15, "year_cell"

    .line 1359
    .restart local v15    # "selectedAxis":Ljava/lang/String;
    goto :goto_0

    .line 1381
    .restart local v1    # "resolver":Landroid/content/ContentResolver;
    .restart local v4    # "selection":Ljava/lang/String;
    .restart local v13    # "data":Landroid/database/Cursor;
    .restart local v14    # "key":Ljava/lang/String;
    :catchall_0
    move-exception v2

    if-eqz v13, :cond_1

    .line 1382
    invoke-interface {v13}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v2

    .line 1353
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private matchAlign(Landroid/widget/TextView;I)V
    .locals 9
    .param p1, "textView"    # Landroid/widget/TextView;
    .param p2, "align"    # I

    .prologue
    const/high16 v8, 0x40000000    # 2.0f

    .line 1043
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 1044
    .local v1, "rect":Landroid/graphics/Rect;
    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    invoke-interface {v5}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1045
    .local v3, "text":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v0

    .line 1046
    .local v0, "paint":Landroid/graphics/Paint;
    const/4 v5, 0x0

    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-interface {v6}, Ljava/lang/CharSequence;->length()I

    move-result v6

    invoke-virtual {v0, v3, v5, v6, v1}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 1047
    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v4

    .line 1048
    .local v4, "width":F
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0c0128

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 1049
    .local v2, "spacing":I
    packed-switch p2, :pswitch_data_0

    .line 1061
    :goto_0
    return-void

    .line 1051
    :pswitch_0
    iget v5, v1, Landroid/graphics/Rect;->top:I

    iget v6, v1, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    move-result v5

    int-to-float v5, v5

    sub-float v5, v4, v5

    div-float/2addr v5, v8

    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->getContext()Landroid/content/Context;

    move-result-object v6

    const/4 v7, 0x7

    invoke-static {v6, v7}, Lcom/samsung/musicplus/util/UiUtils;->dpToPx(Landroid/content/Context;I)I

    move-result v6

    int-to-float v6, v6

    sub-float/2addr v5, v6

    int-to-float v6, v2

    add-float/2addr v5, v6

    invoke-virtual {p1, v5}, Landroid/widget/TextView;->setTranslationX(F)V

    goto :goto_0

    .line 1055
    :pswitch_1
    neg-float v5, v4

    iget v6, v1, Landroid/graphics/Rect;->top:I

    iget v7, v1, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Math;->abs(I)I

    move-result v6

    int-to-float v6, v6

    add-float/2addr v5, v6

    div-float/2addr v5, v8

    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->getContext()Landroid/content/Context;

    move-result-object v6

    const/16 v7, 0x8

    invoke-static {v6, v7}, Lcom/samsung/musicplus/util/UiUtils;->dpToPx(Landroid/content/Context;I)I

    move-result v6

    int-to-float v6, v6

    add-float/2addr v5, v6

    int-to-float v6, v2

    sub-float/2addr v5, v6

    invoke-virtual {p1, v5}, Landroid/widget/TextView;->setTranslationX(F)V

    goto :goto_0

    .line 1049
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static newInstance(Ljava/lang/String;)Lcom/samsung/musicplus/contents/square/MusicSquareFragment;
    .locals 3
    .param p0, "tag"    # Ljava/lang/String;

    .prologue
    .line 177
    new-instance v1, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    invoke-direct {v1}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;-><init>()V

    .line 178
    .local v1, "frag":Lcom/samsung/musicplus/contents/square/MusicSquareFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 179
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "tag"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    invoke-virtual {v1, v0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->setArguments(Landroid/os/Bundle;)V

    .line 181
    return-object v1
.end method

.method private onTabChanged(Z)V
    .locals 6
    .param p1, "isSelected"    # Z

    .prologue
    .line 1858
    sget-object v3, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->CLASSNAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "onTabChanged selected: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1859
    iput-boolean p1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mIsSelected:Z

    .line 1860
    if-nez p1, :cond_1

    .line 1861
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->getSubList()Landroid/app/Fragment;

    move-result-object v2

    .line 1862
    .local v2, "subList":Landroid/app/Fragment;
    instance-of v3, v2, Lcom/samsung/musicplus/widget/tab/OnTabListener;

    if-eqz v3, :cond_0

    move-object v3, v2

    .line 1863
    check-cast v3, Lcom/samsung/musicplus/widget/tab/OnTabListener;

    invoke-interface {v3}, Lcom/samsung/musicplus/widget/tab/OnTabListener;->onTabUnselected()V

    .line 1869
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 1870
    .local v0, "a":Landroid/app/Activity;
    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v0}, Landroid/app/Activity;->isDestroyed()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1871
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->getChildFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    .line 1872
    .local v1, "fm":Landroid/app/FragmentManager;
    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 1877
    .end local v0    # "a":Landroid/app/Activity;
    .end local v1    # "fm":Landroid/app/FragmentManager;
    :cond_0
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->detachChildFragmentManager()V

    .line 1879
    .end local v2    # "subList":Landroid/app/Fragment;
    :cond_1
    return-void
.end method

.method private registerContentObserver(Landroid/net/Uri;)V
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 218
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mMusicSquareUpdateObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 219
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mIsRegistered:Z

    .line 220
    return-void
.end method

.method private removeMessageAllVisualInteraction()V
    .locals 2

    .prologue
    .line 692
    iget-object v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSquareVisualInteractionAnimatinHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 693
    iget-object v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSquareVisualInteractionAnimatinHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 694
    iget-object v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSquareVisualInteractionAnimatinHandler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 695
    iget-object v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSquareVisualInteractionAnimatinHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 696
    iget-object v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSquareVisualInteractionAnimatinHandler:Landroid/os/Handler;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 697
    return-void
.end method

.method private restoreFirstAlbumArtCellInfo(I)V
    .locals 29
    .param p1, "position"    # I

    .prologue
    .line 731
    new-instance v20, Ljava/lang/StringBuilder;

    invoke-direct/range {v20 .. v20}, Ljava/lang/StringBuilder;-><init>()V

    .line 732
    .local v20, "cellIds":Ljava/lang/StringBuilder;
    move-object/from16 v0, v20

    move/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 734
    sget-object v6, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->CLASSNAME:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "restoreFirstAlbumArtCellInfo position:"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move/from16 v0, p1

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 736
    move-object/from16 v0, p0

    iget v6, v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mAxis:I

    packed-switch v6, :pswitch_data_0

    .line 744
    const-string v27, "mood_cell"

    .line 748
    .local v27, "selectedAxis":Ljava/lang/String;
    :goto_0
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v27

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v8, "="

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-object/from16 v0, v20

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .line 749
    .local v7, "selection":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v22

    .line 750
    .local v22, "context":Landroid/content/Context;
    invoke-virtual/range {v22 .. v22}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    .line 751
    .local v4, "resolver":Landroid/content/ContentResolver;
    sget-object v6, Lcom/samsung/musicplus/provider/MusicContents$Audio$Media;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v6}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v6

    const-string v8, "limit"

    const-string v9, "1"

    invoke-virtual {v6, v8, v9}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v5

    .line 754
    .local v5, "uri":Landroid/net/Uri;
    const/16 v24, 0x0

    .line 755
    .local v24, "data":Landroid/database/Cursor;
    const/16 v18, 0x0

    .line 756
    .local v18, "bitmap":Landroid/graphics/Bitmap;
    const-wide/16 v14, -0x1

    .line 758
    .local v14, "albumId":J
    const/4 v6, 0x4

    :try_start_0
    new-array v6, v6, [Ljava/lang/String;

    const/4 v8, 0x0

    const-string v9, "_id"

    aput-object v9, v6, v8

    const/4 v8, 0x1

    const-string v9, "mood_cell"

    aput-object v9, v6, v8

    const/4 v8, 0x2

    const-string v9, "year_cell"

    aput-object v9, v6, v8

    const/4 v8, 0x3

    const-string v9, "_data"

    aput-object v9, v6, v8

    const/4 v8, 0x0

    const-string v9, "title_key"

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v24

    .line 763
    if-eqz v24, :cond_2

    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 764
    const-string v6, "_id"

    move-object/from16 v0, v24

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, v24

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v6

    int-to-long v0, v6

    move-wide/from16 v16, v0

    .line 765
    .local v16, "audioId":J
    const/16 v19, 0x0

    .line 767
    .local v19, "c":Landroid/database/Cursor;
    :try_start_1
    sget-object v9, Lcom/samsung/musicplus/provider/MusicContents;->MUSIC_CONTENTS_URI:Landroid/net/Uri;

    const/4 v6, 0x2

    new-array v10, v6, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string v8, "album_id"

    aput-object v8, v10, v6

    const/4 v6, 0x1

    const-string v8, "_data"

    aput-object v8, v10, v6

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "_id="

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    move-wide/from16 v0, v16

    long-to-int v8, v0

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    const-string v13, "title_key"

    move-object v8, v4

    invoke-virtual/range {v8 .. v13}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v19

    .line 771
    if-eqz v19, :cond_0

    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 772
    const-string v6, "album_id"

    move-object/from16 v0, v19

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v6

    move-object/from16 v0, v19

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v6

    int-to-long v14, v6

    .line 775
    :cond_0
    if-eqz v19, :cond_1

    .line 776
    :try_start_2
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    .line 779
    :cond_1
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSquareGridView:Landroid/widget/GridView;

    if-eqz v6, :cond_2

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSquareGridView:Landroid/widget/GridView;

    invoke-virtual {v6}, Landroid/widget/GridView;->getVisibility()I

    move-result v6

    if-nez v6, :cond_2

    .line 780
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSquareGridView:Landroid/widget/GridView;

    const/4 v8, 0x0

    invoke-virtual {v6, v8}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    move-result-object v21

    .line 781
    .local v21, "child":Landroid/view/View;
    if-eqz v21, :cond_2

    .line 782
    invoke-virtual/range {v21 .. v21}, Landroid/view/View;->getWidth()I

    move-result v28

    .line 783
    .local v28, "width":I
    invoke-virtual/range {v21 .. v21}, Landroid/view/View;->getHeight()I

    move-result v25

    .line 786
    .local v25, "height":I
    move-object/from16 v0, v22

    move/from16 v1, v28

    move/from16 v2, v25

    invoke-static {v0, v14, v15, v1, v2}, Lcom/samsung/musicplus/util/AlbumArtUtils;->getArtworkQuick(Landroid/content/Context;JII)Landroid/graphics/Bitmap;

    move-result-object v18

    .line 787
    sget-object v6, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->CLASSNAME:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "restoreFirstAlbumArtCellInfo "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, v21

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " width : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move/from16 v0, v28

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " height : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move/from16 v0, v25

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6, v8}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 793
    .end local v16    # "audioId":J
    .end local v19    # "c":Landroid/database/Cursor;
    .end local v21    # "child":Landroid/view/View;
    .end local v25    # "height":I
    .end local v28    # "width":I
    :cond_2
    if-eqz v24, :cond_3

    .line 794
    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->close()V

    .line 796
    :cond_3
    if-nez v18, :cond_6

    .line 797
    const/16 v23, 0x0

    .line 798
    .local v23, "d":Landroid/graphics/drawable/Drawable;
    invoke-virtual/range {v22 .. v22}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v26

    .line 799
    .local v26, "res":Landroid/content/res/Resources;
    const v6, 0x7f02003b

    move-object/from16 v0, v26

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v23

    .line 800
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->sAlbumArtBmVisualInteraction:Ljava/util/ArrayList;

    move-object/from16 v0, v23

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 805
    .end local v23    # "d":Landroid/graphics/drawable/Drawable;
    .end local v26    # "res":Landroid/content/res/Resources;
    :goto_1
    return-void

    .line 738
    .end local v4    # "resolver":Landroid/content/ContentResolver;
    .end local v5    # "uri":Landroid/net/Uri;
    .end local v7    # "selection":Ljava/lang/String;
    .end local v14    # "albumId":J
    .end local v18    # "bitmap":Landroid/graphics/Bitmap;
    .end local v22    # "context":Landroid/content/Context;
    .end local v24    # "data":Landroid/database/Cursor;
    .end local v27    # "selectedAxis":Ljava/lang/String;
    :pswitch_0
    const-string v27, "mood_cell"

    .line 739
    .restart local v27    # "selectedAxis":Ljava/lang/String;
    goto/16 :goto_0

    .line 741
    .end local v27    # "selectedAxis":Ljava/lang/String;
    :pswitch_1
    const-string v27, "year_cell"

    .line 742
    .restart local v27    # "selectedAxis":Ljava/lang/String;
    goto/16 :goto_0

    .line 775
    .restart local v4    # "resolver":Landroid/content/ContentResolver;
    .restart local v5    # "uri":Landroid/net/Uri;
    .restart local v7    # "selection":Ljava/lang/String;
    .restart local v14    # "albumId":J
    .restart local v16    # "audioId":J
    .restart local v18    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v19    # "c":Landroid/database/Cursor;
    .restart local v22    # "context":Landroid/content/Context;
    .restart local v24    # "data":Landroid/database/Cursor;
    :catchall_0
    move-exception v6

    if-eqz v19, :cond_4

    .line 776
    :try_start_3
    invoke-interface/range {v19 .. v19}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 793
    .end local v16    # "audioId":J
    .end local v19    # "c":Landroid/database/Cursor;
    :catchall_1
    move-exception v6

    if-eqz v24, :cond_5

    .line 794
    invoke-interface/range {v24 .. v24}, Landroid/database/Cursor;->close()V

    .line 796
    :cond_5
    if-nez v18, :cond_7

    .line 797
    const/16 v23, 0x0

    .line 798
    .restart local v23    # "d":Landroid/graphics/drawable/Drawable;
    invoke-virtual/range {v22 .. v22}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v26

    .line 799
    .restart local v26    # "res":Landroid/content/res/Resources;
    const v8, 0x7f02003b

    move-object/from16 v0, v26

    invoke-virtual {v0, v8}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v23

    .line 800
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->sAlbumArtBmVisualInteraction:Ljava/util/ArrayList;

    move-object/from16 v0, v23

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 802
    .end local v23    # "d":Landroid/graphics/drawable/Drawable;
    .end local v26    # "res":Landroid/content/res/Resources;
    :goto_2
    throw v6

    :cond_6
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->sAlbumArtBmVisualInteraction:Ljava/util/ArrayList;

    move-object/from16 v0, v18

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_7
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->sAlbumArtBmVisualInteraction:Ljava/util/ArrayList;

    move-object/from16 v0, v18

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 736
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private restoreInternalMusicSquareCell(Landroid/widget/GridView;)V
    .locals 9
    .param p1, "v"    # Landroid/widget/GridView;

    .prologue
    .line 1277
    iget-object v6, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSelectedCellList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1278
    sget-object v6, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->sPreferences:Landroid/content/SharedPreferences;

    const-string v7, "savedSquare"

    const/4 v8, 0x0

    invoke-interface {v6, v7, v8}, Landroid/content/SharedPreferences;->getStringSet(Ljava/lang/String;Ljava/util/Set;)Ljava/util/Set;

    move-result-object v5

    .line 1279
    .local v5, "values":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    if-eqz v5, :cond_0

    .line 1280
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1281
    .local v2, "index":Ljava/lang/String;
    iget-object v6, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSelectedCellList:Ljava/util/ArrayList;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1286
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "index":Ljava/lang/String;
    .end local v5    # "values":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    :cond_0
    iget-object v6, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSelectedCellList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 1287
    .local v4, "position":I
    invoke-virtual {p1, v4}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 1288
    .local v0, "child":Landroid/widget/ImageView;
    if-eqz v0, :cond_1

    .line 1289
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0c0116

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 1290
    .local v3, "pad":I
    const/4 v6, 0x1

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 1291
    invoke-virtual {v0, v3, v3, v3, v3}, Landroid/widget/ImageView;->setPadding(IIII)V

    .line 1292
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0b008f

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    goto :goto_1

    .line 1295
    .end local v0    # "child":Landroid/widget/ImageView;
    .end local v3    # "pad":I
    .end local v4    # "position":I
    :cond_2
    return-void
.end method

.method private restoreKeyWord()V
    .locals 18

    .prologue
    .line 1101
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSelectedCellList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v17

    .line 1102
    .local v17, "size":I
    if-gtz v17, :cond_1

    .line 1146
    :cond_0
    :goto_0
    return-void

    .line 1106
    :cond_1
    sget-object v2, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->CLASSNAME:Ljava/lang/String;

    const-string v3, "restoreKeyWord"

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1107
    const/4 v14, 0x0

    .line 1108
    .local v14, "k":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSelectedCellList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v12

    .local v12, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v13

    .line 1109
    .local v13, "inx":I
    sget-object v2, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->CLASSNAME:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "selected["

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    add-int/lit8 v15, v14, 0x1

    .end local v14    # "k":I
    .local v15, "k":I
    invoke-virtual {v3, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "] : "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v14, v15

    .line 1110
    .end local v15    # "k":I
    .restart local v14    # "k":I
    goto :goto_1

    .line 1112
    .end local v13    # "inx":I
    :cond_2
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 1113
    .local v8, "cellIds":Ljava/lang/StringBuilder;
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_2
    move/from16 v0, v17

    if-ge v11, v0, :cond_4

    .line 1114
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSelectedCellList:Ljava/util/ArrayList;

    invoke-virtual {v2, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 1115
    add-int/lit8 v2, v17, -0x1

    if-ge v11, v2, :cond_3

    .line 1116
    const-string v2, ","

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1113
    :cond_3
    add-int/lit8 v11, v11, 0x1

    goto :goto_2

    .line 1121
    :cond_4
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mAxis:I

    packed-switch v2, :pswitch_data_0

    .line 1129
    const-string v16, "mood_cell"

    .line 1133
    .local v16, "selectedAxis":Ljava/lang/String;
    :goto_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v16

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " IN ("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1134
    .local v4, "selection":Ljava/lang/String;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    .line 1135
    .local v9, "context":Landroid/content/Context;
    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 1136
    .local v1, "resolver":Landroid/content/ContentResolver;
    sget-object v2, Lcom/samsung/musicplus/provider/MusicContents$Audio$Media;->CONTENT_URI:Landroid/net/Uri;

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "source_id AS _id"

    aput-object v6, v3, v5

    const/4 v5, 0x1

    const-string v6, "mood_cell"

    aput-object v6, v3, v5

    const/4 v5, 0x2

    const-string v6, "year_cell"

    aput-object v6, v3, v5

    const/4 v5, 0x0

    const-string v6, "title_key"

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 1141
    .local v10, "data":Landroid/database/Cursor;
    if-eqz v10, :cond_0

    .line 1142
    invoke-static {v10}, Lcom/samsung/musicplus/util/ListUtils;->getSongListForCursor(Landroid/database/Cursor;)[J

    move-result-object v7

    .line 1143
    .local v7, "audioids":[J
    const-string v2, "_id"

    invoke-static {v2, v7}, Lcom/samsung/musicplus/util/UiUtils;->convertAudioIdsToSelection(Ljava/lang/String;[J)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mKeyWord:Ljava/lang/String;

    .line 1144
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 1123
    .end local v1    # "resolver":Landroid/content/ContentResolver;
    .end local v4    # "selection":Ljava/lang/String;
    .end local v7    # "audioids":[J
    .end local v9    # "context":Landroid/content/Context;
    .end local v10    # "data":Landroid/database/Cursor;
    .end local v16    # "selectedAxis":Ljava/lang/String;
    :pswitch_0
    const-string v16, "mood_cell"

    .line 1124
    .restart local v16    # "selectedAxis":Ljava/lang/String;
    goto :goto_3

    .line 1126
    .end local v16    # "selectedAxis":Ljava/lang/String;
    :pswitch_1
    const-string v16, "year_cell"

    .line 1127
    .restart local v16    # "selectedAxis":Ljava/lang/String;
    goto :goto_3

    .line 1121
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private saveSelectedCellPref()V
    .locals 5

    .prologue
    .line 882
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 883
    .local v2, "values":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    iget-object v3, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSelectedCellList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 884
    .local v1, "index":I
    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 886
    .end local v1    # "index":I
    :cond_0
    sget-object v3, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->sPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v4, "savedSquare"

    invoke-interface {v3, v4, v2}, Landroid/content/SharedPreferences$Editor;->putStringSet(Ljava/lang/String;Ljava/util/Set;)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 887
    return-void
.end method

.method private selectMusicSquareCell(Landroid/widget/GridView;I)V
    .locals 4
    .param p1, "v"    # Landroid/widget/GridView;
    .param p2, "position"    # I

    .prologue
    .line 1307
    iget-object v2, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSelectedCellList:Ljava/util/ArrayList;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1308
    invoke-virtual {p1, p2}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 1309
    .local v0, "child":Landroid/widget/ImageView;
    if-eqz v0, :cond_0

    .line 1310
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 1311
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c0116

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 1312
    .local v1, "pad":I
    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/widget/ImageView;->setPadding(IIII)V

    .line 1313
    const v2, 0x7f0b008f

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1314
    iget-object v2, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSelectedCellList:Ljava/util/ArrayList;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1317
    .end local v0    # "child":Landroid/widget/ImageView;
    .end local v1    # "pad":I
    :cond_0
    return-void
.end method

.method private setAlbumArtMusicSquareCell(Landroid/widget/GridView;I)V
    .locals 6
    .param p1, "v"    # Landroid/widget/GridView;
    .param p2, "position"    # I

    .prologue
    .line 701
    sget-object v3, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->CLASSNAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setAlbumArtMusicSquareCell position:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "sAlbumArtBmVisualInteraction.size():"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->sAlbumArtBmVisualInteraction:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 704
    iget-object v3, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->sAlbumArtBmVisualInteraction:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lt p2, v3, :cond_1

    .line 723
    :cond_0
    :goto_0
    return-void

    .line 707
    :cond_1
    invoke-virtual {p1, p2}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 708
    .local v0, "child":Landroid/widget/ImageView;
    if-eqz v0, :cond_0

    .line 709
    iget-object v3, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->sAlbumArtBmVisualInteraction:Ljava/util/ArrayList;

    invoke-virtual {v3, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    .line 710
    .local v1, "image":Ljava/lang/Object;
    instance-of v3, v1, Landroid/graphics/Bitmap;

    if-eqz v3, :cond_3

    .line 711
    if-eqz v1, :cond_2

    move-object v3, v1

    check-cast v3, Landroid/graphics/Bitmap;

    invoke-virtual {v3}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v3

    if-nez v3, :cond_2

    .line 712
    check-cast v1, Landroid/graphics/Bitmap;

    .end local v1    # "image":Ljava/lang/Object;
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 714
    .restart local v1    # "image":Ljava/lang/Object;
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 715
    .local v2, "res":Landroid/content/res/Resources;
    const v3, 0x7f02003b

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 717
    .end local v2    # "res":Landroid/content/res/Resources;
    :cond_3
    instance-of v3, v1, Landroid/graphics/drawable/Drawable;

    if-eqz v3, :cond_0

    .line 718
    check-cast v1, Landroid/graphics/drawable/Drawable;

    .end local v1    # "image":Ljava/lang/Object;
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method private setBasePointForCheckMove(II)V
    .locals 0
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 508
    iput p1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mBaseXPoint:I

    .line 509
    iput p2, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mBaseYPoint:I

    .line 510
    return-void
.end method

.method private setCellAirView(Landroid/widget/ImageView;I)V
    .locals 8
    .param p1, "iv"    # Landroid/widget/ImageView;
    .param p2, "position"    # I

    .prologue
    const/4 v6, 0x1

    const/4 v2, 0x0

    .line 1392
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 1393
    sget-boolean v0, Lcom/samsung/musicplus/util/Version;->LIBRARY_OVER_API_2:Z

    if-eqz v0, :cond_0

    .line 1394
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->initializeCellAirViewListener()V

    .line 1395
    iget-object v1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mCellAirViewListener:Lcom/samsung/musicplus/library/hardware/AirView$OnAirViewMorePopupListener;

    const/16 v3, 0x258

    move-object v0, p1

    move v4, v2

    move v5, v2

    move v7, v6

    invoke-static/range {v0 .. v7}, Lcom/samsung/musicplus/library/hardware/AirView;->setView(Landroid/view/View;Lcom/samsung/musicplus/library/hardware/AirView$OnAirViewMorePopupListener;IIZZZZ)V

    .line 1399
    :cond_0
    return-void
.end method

.method private setPlayingCell()V
    .locals 25

    .prologue
    .line 1685
    const/4 v4, -0x1

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mPlayingCell:I

    .line 1686
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getAlbumId()J

    move-result-wide v12

    .line 1687
    .local v12, "albumId":J
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getCurrentAudioId()J

    move-result-wide v14

    .line 1688
    .local v14, "audioId":J
    sget-object v4, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->CLASSNAME:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "invalidateAllViews albumId : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " audioId : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1689
    const-wide/16 v4, 0x0

    cmp-long v4, v14, v4

    if-gez v4, :cond_1

    .line 1690
    invoke-direct/range {p0 .. p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->cleanAlbumBitmap()V

    .line 1745
    :cond_0
    :goto_0
    return-void

    .line 1693
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->getActivity()Landroid/app/Activity;

    move-result-object v10

    .line 1694
    .local v10, "a":Landroid/app/Activity;
    if-eqz v10, :cond_0

    const-wide/16 v4, 0x0

    cmp-long v4, v12, v4

    if-lez v4, :cond_0

    .line 1695
    invoke-virtual {v10}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v19

    .line 1696
    .local v19, "context":Landroid/content/Context;
    const-string v20, "source_id"

    .line 1698
    .local v20, "idColumn":Ljava/lang/String;
    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Lcom/samsung/musicplus/provider/MusicContents$Audio$Media;->CONTENT_URI:Landroid/net/Uri;

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    const-string v8, "mood_cell"

    aput-object v8, v6, v7

    const/4 v7, 0x1

    const-string v8, "year_cell"

    aput-object v8, v6, v7

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v20

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "=?"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/String;

    const/4 v9, 0x0

    invoke-static {v14, v15}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v24

    aput-object v24, v8, v9

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v16

    .line 1706
    .local v16, "c":Landroid/database/Cursor;
    if-eqz v16, :cond_0

    .line 1707
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1709
    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mAxis:I

    packed-switch v4, :pswitch_data_0

    .line 1717
    const-string v17, "mood_cell"

    .line 1721
    .local v17, "column":Ljava/lang/String;
    :goto_1
    invoke-interface/range {v16 .. v17}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    move-object/from16 v0, v16

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mPlayingCell:I

    .line 1724
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSquareGridView:Landroid/widget/GridView;

    if-eqz v4, :cond_3

    move-object/from16 v0, p0

    iget v4, v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mPlayingCell:I

    const/4 v5, -0x1

    if-eq v4, v5, :cond_3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSquareGridView:Landroid/widget/GridView;

    invoke-virtual {v4}, Landroid/widget/GridView;->getVisibility()I

    move-result v4

    if-nez v4, :cond_3

    .line 1726
    invoke-direct/range {p0 .. p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->cleanAlbumBitmap()V

    .line 1727
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0c011b

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v23

    .line 1729
    .local v23, "sqareGridViewWidth":I
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0e0003

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v18

    .line 1731
    .local v18, "columnNumber":I
    div-int v22, v23, v18

    .line 1732
    .local v22, "size":I
    move-object/from16 v0, v19

    move/from16 v1, v22

    move/from16 v2, v22

    invoke-static {v0, v12, v13, v1, v2}, Lcom/samsung/musicplus/util/AlbumArtUtils;->getArtworkQuick(Landroid/content/Context;JII)Landroid/graphics/Bitmap;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mAlbumBm:Landroid/graphics/Bitmap;

    .line 1733
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mAlbumBm:Landroid/graphics/Bitmap;

    if-nez v4, :cond_2

    .line 1734
    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v21

    .line 1735
    .local v21, "res":Landroid/content/res/Resources;
    const v4, 0x7f02003b

    move-object/from16 v0, v21

    invoke-static {v0, v4}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v11

    .line 1737
    .local v11, "b":Landroid/graphics/Bitmap;
    move-object/from16 v0, p0

    iput-object v11, v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mAlbumBm:Landroid/graphics/Bitmap;

    .line 1739
    .end local v11    # "b":Landroid/graphics/Bitmap;
    .end local v21    # "res":Landroid/content/res/Resources;
    :cond_2
    sget-object v4, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->CLASSNAME:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setPlayingCell mAlbumBm is "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mAlbumBm:Landroid/graphics/Bitmap;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/musicplus/library/iLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 1742
    .end local v17    # "column":Ljava/lang/String;
    .end local v18    # "columnNumber":I
    .end local v22    # "size":I
    .end local v23    # "sqareGridViewWidth":I
    :cond_3
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 1711
    :pswitch_0
    const-string v17, "mood_cell"

    .line 1712
    .restart local v17    # "column":Ljava/lang/String;
    goto/16 :goto_1

    .line 1714
    .end local v17    # "column":Ljava/lang/String;
    :pswitch_1
    const-string v17, "year_cell"

    .line 1715
    .restart local v17    # "column":Ljava/lang/String;
    goto/16 :goto_1

    .line 1709
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private setReadyToClick(I)V
    .locals 4
    .param p1, "delay"    # I

    .prologue
    const/4 v1, 0x2

    .line 916
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mIsReadyToClick:Z

    .line 917
    iget-object v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSquareUpdateHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 918
    iget-object v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSquareUpdateHandler:Landroid/os/Handler;

    int-to-long v2, p1

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 919
    return-void
.end method

.method private setSquareHelpButton()V
    .locals 3

    .prologue
    .line 1752
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const v2, 0x7f0d0169

    invoke-virtual {v1, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 1754
    .local v0, "help":Landroid/widget/ImageView;
    if-eqz v0, :cond_0

    .line 1755
    const/16 v1, 0x5051

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/hardware/AirView;->setView(Landroid/view/View;I)V

    .line 1756
    new-instance v1, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$10;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$10;-><init>(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1775
    :cond_0
    return-void
.end method

.method private static showToast(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 1887
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1888
    return-void
.end method

.method private unregisterContentObserver()V
    .locals 2

    .prologue
    .line 223
    iget-boolean v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mIsRegistered:Z

    if-eqz v0, :cond_0

    .line 224
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mMusicSquareUpdateObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 225
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mIsRegistered:Z

    .line 227
    :cond_0
    return-void
.end method

.method private unselectMusicSquareCell(Landroid/widget/GridView;I)V
    .locals 2
    .param p1, "v"    # Landroid/widget/GridView;
    .param p2, "position"    # I

    .prologue
    .line 1345
    invoke-virtual {p1, p2}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 1346
    .local v0, "child":Landroid/widget/ImageView;
    if-eqz v0, :cond_0

    .line 1347
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setSelected(Z)V

    .line 1349
    :cond_0
    return-void
.end method

.method private updateLongClickTimer()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 902
    iget-object v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSquareUpdateHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 903
    iget-object v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSquareUpdateHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 905
    return-void
.end method

.method private updateSubList()V
    .locals 2

    .prologue
    .line 494
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->getSubList()Landroid/app/Fragment;

    move-result-object v0

    .line 495
    .local v0, "subList":Landroid/app/Fragment;
    instance-of v1, v0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;

    if-eqz v1, :cond_0

    .line 496
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->restoreKeyWord()V

    .line 497
    check-cast v0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;

    .end local v0    # "subList":Landroid/app/Fragment;
    iget-object v1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mKeyWord:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->updateListView(Ljava/lang/String;)V

    .line 499
    :cond_0
    return-void
.end method


# virtual methods
.method public cleanGridView()V
    .locals 2

    .prologue
    .line 1891
    iget-object v1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSquareGridView:Landroid/widget/GridView;

    invoke-direct {p0, v1}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->clearAllMusicSquareCell(Landroid/widget/GridView;)V

    .line 1892
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->cleanAlbumBitmap()V

    .line 1893
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 1894
    .local v0, "a":Landroid/app/Activity;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/musicplus/util/UiUtils;->isLockScreenOn(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1895
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->clearSelectedCellPref()V

    .line 1897
    :cond_0
    return-void
.end method

.method protected createMusicMenus()Lcom/samsung/musicplus/common/menu/AbstractMusicMenus;
    .locals 1

    .prologue
    .line 1799
    new-instance v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$11;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$11;-><init>(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;)V

    return-object v0
.end method

.method public getListType()I
    .locals 1

    .prologue
    .line 1906
    const/4 v0, 0x0

    return v0
.end method

.method public isEmptyList()Z
    .locals 1

    .prologue
    .line 1911
    const/4 v0, 0x0

    return v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 253
    invoke-super {p0, p1}, Lcom/samsung/musicplus/MusicBaseFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 254
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mDoVisualInteraction:Z

    .line 255
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->ensureSquareView()V

    .line 256
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->ensureListView()V

    .line 257
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->setSquareHelpButton()V

    .line 258
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x0

    .line 186
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->setHasOptionsMenu(Z)V

    .line 187
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "music_player_pref"

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->sPreferences:Landroid/content/SharedPreferences;

    .line 188
    sget-object v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->sPreferences:Landroid/content/SharedPreferences;

    const-string v1, "square_theme"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mAxis:I

    .line 189
    if-eqz p1, :cond_0

    .line 191
    const-string v0, "selected_cell"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getIntegerArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSelectedCellList:Ljava/util/ArrayList;

    .line 197
    :goto_0
    invoke-super {p0, p1}, Lcom/samsung/musicplus/MusicBaseFragment;->onCreate(Landroid/os/Bundle;)V

    .line 198
    return-void

    .line 194
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSelectedCellList:Ljava/util/ArrayList;

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 244
    sget-object v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCreateView savedInstanceState : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    sget-boolean v0, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_MASS_PROJECT:Z

    if-nez v0, :cond_0

    .line 246
    :cond_0
    const v0, 0x7f040076

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDestroyView()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 866
    sget-object v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->CLASSNAME:Ljava/lang/String;

    const-string v1, "onDestroyView"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 867
    iget-object v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSquareGridView:Landroid/widget/GridView;

    invoke-virtual {v0, v2}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 868
    iput-object v2, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSquareGridView:Landroid/widget/GridView;

    .line 869
    iget-object v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSquareUpdateHandler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 872
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mQueueChangedListener:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 878
    :goto_0
    invoke-super {p0}, Lcom/samsung/musicplus/MusicBaseFragment;->onDestroyView()V

    .line 879
    return-void

    .line 873
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public onDetach()V
    .locals 0

    .prologue
    .line 290
    invoke-super {p0}, Lcom/samsung/musicplus/MusicBaseFragment;->onDetach()V

    .line 292
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->detachChildFragmentManager()V

    .line 293
    return-void
.end method

.method public onFragmentWindowFocusChanged(Z)V
    .locals 0
    .param p1, "hasFocus"    # Z

    .prologue
    .line 1749
    return-void
.end method

.method public onMultiWindowModeChanged(Z)V
    .locals 0
    .param p1, "isMultiWindow"    # Z

    .prologue
    .line 1902
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 274
    iput-boolean v1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mIsOnResumed:Z

    .line 275
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->clearVisualInteraction()V

    .line 280
    iget-object v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSquareGridView:Landroid/widget/GridView;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setClickable(Z)V

    .line 285
    invoke-super {p0}, Lcom/samsung/musicplus/MusicBaseFragment;->onPause()V

    .line 286
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 264
    iput-boolean v1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mIsOnResumed:Z

    .line 265
    invoke-super {p0}, Lcom/samsung/musicplus/MusicBaseFragment;->onResume()V

    .line 267
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->setPlayingCell()V

    .line 268
    iget-object v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSquareGridView:Landroid/widget/GridView;

    invoke-virtual {v0}, Landroid/widget/GridView;->invalidateViews()V

    .line 269
    iget-object v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSquareGridView:Landroid/widget/GridView;

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setClickable(Z)V

    .line 270
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 965
    const-string v0, "selected_cell"

    iget-object v1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSelectedCellList:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putIntegerArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 966
    invoke-super {p0, p1}, Lcom/samsung/musicplus/MusicBaseFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 967
    return-void
.end method

.method public onServiceConnected()V
    .locals 2

    .prologue
    .line 1837
    sget-object v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->CLASSNAME:Ljava/lang/String;

    const-string v1, "onServiceConnected"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1839
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->isDetached()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1840
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mDoVisualInteraction:Z

    .line 1841
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->restoreMusicSquareCell()V

    .line 1843
    :cond_0
    return-void
.end method

.method public onStart()V
    .locals 3

    .prologue
    .line 202
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 203
    .local v0, "f":Landroid/content/IntentFilter;
    const-string v1, "com.samsung.musicplus.action.QUEUE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 204
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mQueueChangedListener:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 205
    invoke-static {}, Lcom/samsung/musicplus/provider/MusicContents;->getMusicSquareCellUpdator()Landroid/net/Uri;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->registerContentObserver(Landroid/net/Uri;)V

    .line 206
    invoke-super {p0}, Lcom/samsung/musicplus/MusicBaseFragment;->onStart()V

    .line 207
    return-void
.end method

.method public onStop()V
    .locals 0

    .prologue
    .line 860
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->unregisterContentObserver()V

    .line 861
    invoke-super {p0}, Lcom/samsung/musicplus/MusicBaseFragment;->onStop()V

    .line 862
    return-void
.end method

.method public onTabSelected()V
    .locals 1

    .prologue
    .line 1847
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->onTabChanged(Z)V

    .line 1848
    return-void
.end method

.method public onTabUnselected()V
    .locals 1

    .prologue
    .line 1852
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->onTabChanged(Z)V

    .line 1853
    return-void
.end method

.method public receivePlayerState(Ljava/lang/String;)V
    .locals 5
    .param p1, "action"    # Ljava/lang/String;

    .prologue
    .line 1644
    const-string v2, "com.android.music.metachanged"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "com.samsung.musicplus.action.QUEUE_CHANGED"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1647
    :cond_0
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getListType()I

    move-result v1

    .line 1648
    .local v1, "list":I
    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    const v2, 0x2000a

    if-eq v1, v2, :cond_1

    .line 1649
    sget-object v2, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->CLASSNAME:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "receivePlayerState = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1650
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->cleanGridView()V

    .line 1651
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mKeyWord:Ljava/lang/String;

    .line 1653
    :cond_1
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->invalidateAllViews()V

    .line 1660
    .end local v1    # "list":I
    :cond_2
    :goto_0
    return-void

    .line 1655
    :cond_3
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->getSubList()Landroid/app/Fragment;

    move-result-object v0

    .line 1656
    .local v0, "fg":Landroid/app/Fragment;
    instance-of v2, v0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;

    if-eqz v2, :cond_2

    .line 1657
    check-cast v0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;

    .end local v0    # "fg":Landroid/app/Fragment;
    invoke-virtual {v0}, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;->invalidateAllViews()V

    goto :goto_0
.end method

.method public restoreMusicSquareCell()V
    .locals 3

    .prologue
    .line 823
    sget-object v1, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->CLASSNAME:Ljava/lang/String;

    const-string v2, "restoreMusicSquareCell"

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 825
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getListType()I

    move-result v0

    .line 826
    .local v0, "list":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget-object v1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSquareGridView:Landroid/widget/GridView;

    if-nez v1, :cond_1

    .line 856
    :cond_0
    :goto_0
    return-void

    .line 829
    :cond_1
    const v1, 0x2000a

    if-ne v0, v1, :cond_3

    .line 831
    iget-object v1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSquareGridView:Landroid/widget/GridView;

    invoke-direct {p0, v1}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->restoreInternalMusicSquareCell(Landroid/widget/GridView;)V

    .line 832
    iget-object v1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mKeyWord:Ljava/lang/String;

    if-nez v1, :cond_2

    .line 833
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->restoreKeyWord()V

    .line 846
    :cond_2
    :goto_1
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->setPlayingCell()V

    .line 847
    iget-object v1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSquareGridView:Landroid/widget/GridView;

    invoke-virtual {v1}, Landroid/widget/GridView;->invalidateViews()V

    .line 849
    sget-boolean v1, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_SUPPORT_SQUARE_VISUAL_INTERACTION_ANIMATION:Z

    if-eqz v1, :cond_0

    sget v1, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->sMusicSquareColumn:I

    const/4 v2, 0x5

    if-ne v1, v2, :cond_0

    .line 851
    iget-boolean v1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mDoVisualInteraction:Z

    if-eqz v1, :cond_0

    .line 852
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mDoVisualInteraction:Z

    .line 853
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->doVisualInteration2()V

    goto :goto_0

    .line 836
    :cond_3
    iget-object v1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSelectedCellList:Ljava/util/ArrayList;

    if-eqz v1, :cond_2

    .line 837
    iget-object v1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSelectedCellList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 840
    if-eqz v0, :cond_2

    .line 841
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->clearSelectedCellPref()V

    goto :goto_1
.end method
