.class public Lcom/samsung/musicplus/contents/TrackListNumberAdapter;
.super Lcom/samsung/musicplus/widget/list/TrackListAdapter;
.source "TrackListNumberAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/contents/TrackListNumberAdapter$TrackNumberViewHolder;
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;ILandroid/database/Cursor;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "layout"    # I
    .param p3, "c"    # Landroid/database/Cursor;
    .param p4, "flags"    # I

    .prologue
    .line 21
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/musicplus/widget/list/TrackListAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;I)V

    .line 22
    return-void
.end method

.method private updateNowPlayingTextColor(Landroid/widget/TextView;Landroid/widget/TextView;J)V
    .locals 3
    .param p1, "number"    # Landroid/widget/TextView;
    .param p2, "duration"    # Landroid/widget/TextView;
    .param p3, "currentId"    # J

    .prologue
    .line 58
    iget-wide v0, p0, Lcom/samsung/musicplus/contents/TrackListNumberAdapter;->mPlayingId:J

    cmp-long v0, v0, p3

    if-nez v0, :cond_0

    .line 59
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/TrackListNumberAdapter;->getPlayingTextColorState()Landroid/content/res/ColorStateList;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 60
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/TrackListNumberAdapter;->getPlayingTextColorState()Landroid/content/res/ColorStateList;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 65
    :goto_0
    return-void

    .line 62
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/TrackListNumberAdapter;->getDefaultTextColor()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 63
    iget-object v0, p0, Lcom/samsung/musicplus/contents/TrackListNumberAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b002a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method


# virtual methods
.method protected bindNowPlayingView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "c"    # Landroid/database/Cursor;

    .prologue
    .line 39
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->bindNowPlayingView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    .line 41
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/contents/TrackListNumberAdapter$TrackNumberViewHolder;

    .line 42
    .local v0, "vh":Lcom/samsung/musicplus/contents/TrackListNumberAdapter$TrackNumberViewHolder;
    iget-object v1, v0, Lcom/samsung/musicplus/contents/TrackListNumberAdapter$TrackNumberViewHolder;->number:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/samsung/musicplus/contents/TrackListNumberAdapter$TrackNumberViewHolder;->duration:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 43
    iget-object v1, v0, Lcom/samsung/musicplus/contents/TrackListNumberAdapter$TrackNumberViewHolder;->number:Landroid/widget/TextView;

    iget-object v2, v0, Lcom/samsung/musicplus/contents/TrackListNumberAdapter$TrackNumberViewHolder;->duration:Landroid/widget/TextView;

    iget v3, p0, Lcom/samsung/musicplus/contents/TrackListNumberAdapter;->mAudioIndex:I

    invoke-interface {p3, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-direct {p0, v1, v2, v4, v5}, Lcom/samsung/musicplus/contents/TrackListNumberAdapter;->updateNowPlayingTextColor(Landroid/widget/TextView;Landroid/widget/TextView;J)V

    .line 44
    iget-object v1, v0, Lcom/samsung/musicplus/contents/TrackListNumberAdapter$TrackNumberViewHolder;->duration:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/musicplus/contents/TrackListNumberAdapter;->mContext:Landroid/content/Context;

    iget-object v3, v0, Lcom/samsung/musicplus/contents/TrackListNumberAdapter$TrackNumberViewHolder;->duration:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/musicplus/util/UiUtils;->getDurationTalkback(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 47
    :cond_0
    return-void
.end method

.method protected getViewHolder()Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;
    .locals 1

    .prologue
    .line 34
    new-instance v0, Lcom/samsung/musicplus/contents/TrackListNumberAdapter$TrackNumberViewHolder;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/contents/TrackListNumberAdapter$TrackNumberViewHolder;-><init>(Lcom/samsung/musicplus/contents/TrackListNumberAdapter;)V

    return-object v0
.end method

.method protected newTextView(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 26
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->newTextView(Landroid/view/View;)V

    .line 27
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/contents/TrackListNumberAdapter$TrackNumberViewHolder;

    .line 28
    .local v0, "vh":Lcom/samsung/musicplus/contents/TrackListNumberAdapter$TrackNumberViewHolder;
    const v1, 0x7f0d00ca

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v0, Lcom/samsung/musicplus/contents/TrackListNumberAdapter$TrackNumberViewHolder;->number:Landroid/widget/TextView;

    .line 29
    invoke-virtual {p1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 30
    return-void
.end method
