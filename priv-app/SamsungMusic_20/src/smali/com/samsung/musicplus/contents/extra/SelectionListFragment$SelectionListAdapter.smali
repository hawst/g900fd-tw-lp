.class public Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter;
.super Lcom/samsung/musicplus/widget/list/TrackListAdapter;
.source "SelectionListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/contents/extra/SelectionListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4
    name = "SelectionListAdapter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter$SelectionViewHolder;
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/contents/extra/SelectionListFragment;


# direct methods
.method public constructor <init>(Lcom/samsung/musicplus/contents/extra/SelectionListFragment;Landroid/content/Context;ILandroid/database/Cursor;I)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "layout"    # I
    .param p4, "c"    # Landroid/database/Cursor;
    .param p5, "flags"    # I

    .prologue
    .line 895
    iput-object p1, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter;->this$0:Lcom/samsung/musicplus/contents/extra/SelectionListFragment;

    .line 896
    invoke-direct {p0, p2, p3, p4, p5}, Lcom/samsung/musicplus/widget/list/TrackListAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;I)V

    .line 897
    return-void
.end method


# virtual methods
.method protected bindOtherView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "c"    # Landroid/database/Cursor;

    .prologue
    .line 1035
    iget-object v0, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter;->this$0:Lcom/samsung/musicplus/contents/extra/SelectionListFragment;

    # getter for: Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mListType:I
    invoke-static {v0}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->access$600(Lcom/samsung/musicplus/contents/extra/SelectionListFragment;)I

    move-result v0

    invoke-static {v0}, Lcom/samsung/musicplus/util/ListUtils;->isTrack(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1036
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->bindOtherView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    .line 1038
    :cond_0
    return-void
.end method

.method protected bindPlaylistItemView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 10
    .param p1, "v"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "c"    # Landroid/database/Cursor;

    .prologue
    const/16 v9, 0x8

    .line 982
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter$SelectionViewHolder;

    .line 983
    .local v8, "vh":Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter$SelectionViewHolder;
    invoke-virtual {p0, p3}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter;->getArtKey(Landroid/database/Cursor;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 984
    .local v4, "plid":J
    const-wide/16 v0, -0x11

    cmp-long v0, v4, v0

    if-nez v0, :cond_1

    .line 985
    iget-object v0, v8, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter$SelectionViewHolder;->thumbnail:Landroid/widget/ImageView;

    const v1, 0x7f020035

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 986
    iget-object v0, v8, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter$SelectionViewHolder;->text2:Landroid/widget/TextView;

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 991
    :goto_0
    iget-object v0, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter;->this$0:Lcom/samsung/musicplus/contents/extra/SelectionListFragment;

    # getter for: Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mHeaderMode:I
    invoke-static {v0}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->access$1200(Lcom/samsung/musicplus/contents/extra/SelectionListFragment;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 992
    iget-object v0, v8, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter$SelectionViewHolder;->text2:Landroid/widget/TextView;

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 993
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    .line 994
    .local v7, "lp":Landroid/view/ViewGroup$LayoutParams;
    iget-object v0, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter;->this$0:Lcom/samsung/musicplus/contents/extra/SelectionListFragment;

    invoke-virtual {v0}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c01bd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, v7, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 995
    invoke-virtual {p1, v7}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 997
    .end local v7    # "lp":Landroid/view/ViewGroup$LayoutParams;
    :cond_0
    return-void

    .line 988
    :cond_1
    invoke-static {}, Lcom/samsung/musicplus/widget/content/PlaylistItemProgress;->getPlaylistItemProgress()Lcom/samsung/musicplus/widget/content/PlaylistItemProgress;

    move-result-object v0

    iget-object v2, v8, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter$SelectionViewHolder;->thumbnail:Landroid/widget/ImageView;

    iget-object v3, v8, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter$SelectionViewHolder;->text2:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter;->this$0:Lcom/samsung/musicplus/contents/extra/SelectionListFragment;

    invoke-virtual {v1}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->getViewType()I

    move-result v6

    move-object v1, p2

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/musicplus/widget/content/PlaylistItemProgress;->updateListView(Landroid/content/Context;Landroid/widget/ImageView;Landroid/widget/TextView;JI)V

    goto :goto_0
.end method

.method protected bindTextView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 9
    .param p1, "v"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "c"    # Landroid/database/Cursor;

    .prologue
    const/4 v8, -0x1

    .line 1001
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter$SelectionViewHolder;

    .line 1002
    .local v5, "vh":Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter$SelectionViewHolder;
    iget-object v6, v5, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter$SelectionViewHolder;->text1:Landroid/widget/TextView;

    iget v7, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter;->mText1Index:I

    invoke-interface {p3, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-static {p2, v7}, Lcom/samsung/musicplus/util/UiUtils;->transUnknownString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1004
    iget-object v6, v5, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter$SelectionViewHolder;->text2:Landroid/widget/TextView;

    if-eqz v6, :cond_1

    .line 1005
    iget-object v6, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter;->this$0:Lcom/samsung/musicplus/contents/extra/SelectionListFragment;

    # getter for: Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mText2Idx:I
    invoke-static {v6}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->access$1300(Lcom/samsung/musicplus/contents/extra/SelectionListFragment;)I

    move-result v6

    if-ltz v6, :cond_1

    .line 1006
    iget-object v6, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter;->this$0:Lcom/samsung/musicplus/contents/extra/SelectionListFragment;

    # getter for: Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mText2Idx:I
    invoke-static {v6}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->access$1400(Lcom/samsung/musicplus/contents/extra/SelectionListFragment;)I

    move-result v6

    invoke-interface {p3, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {p2, v6}, Lcom/samsung/musicplus/util/UiUtils;->transUnknownString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1007
    .local v3, "text2":Ljava/lang/String;
    iget-object v6, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter;->this$0:Lcom/samsung/musicplus/contents/extra/SelectionListFragment;

    # getter for: Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mListType:I
    invoke-static {v6}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->access$600(Lcom/samsung/musicplus/contents/extra/SelectionListFragment;)I

    move-result v6

    const v7, 0x10007

    if-ne v6, v7, :cond_0

    .line 1008
    const-string v6, ""

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 1009
    const-string v6, "/"

    const/4 v7, 0x1

    invoke-virtual {v3, v6, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v2

    .line 1010
    .local v2, "start":I
    const-string v6, "/"

    invoke-virtual {v3, v6}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v1

    .line 1011
    .local v1, "end":I
    if-eq v2, v8, :cond_0

    if-eq v1, v8, :cond_0

    .line 1012
    invoke-virtual {v3, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 1016
    .end local v1    # "end":I
    .end local v2    # "start":I
    :cond_0
    iget-object v6, v5, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter$SelectionViewHolder;->text2:Landroid/widget/TextView;

    invoke-virtual {v6, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1019
    .end local v3    # "text2":Ljava/lang/String;
    :cond_1
    iget-object v6, v5, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter$SelectionViewHolder;->text3:Landroid/widget/TextView;

    if-eqz v6, :cond_2

    .line 1020
    iget-object v6, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter;->this$0:Lcom/samsung/musicplus/contents/extra/SelectionListFragment;

    # getter for: Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mText3Idx:I
    invoke-static {v6}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->access$1500(Lcom/samsung/musicplus/contents/extra/SelectionListFragment;)I

    move-result v6

    if-ltz v6, :cond_2

    .line 1021
    iget-object v6, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter;->this$0:Lcom/samsung/musicplus/contents/extra/SelectionListFragment;

    # getter for: Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mText3Idx:I
    invoke-static {v6}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->access$1600(Lcom/samsung/musicplus/contents/extra/SelectionListFragment;)I

    move-result v6

    invoke-interface {p3, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {p2, v6}, Lcom/samsung/musicplus/util/UiUtils;->transUnknownString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1022
    .local v4, "text3":Ljava/lang/String;
    iget-object v6, v5, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter$SelectionViewHolder;->text3:Landroid/widget/TextView;

    invoke-virtual {v6, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1025
    .end local v4    # "text3":Ljava/lang/String;
    :cond_2
    iget-object v6, v5, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter$SelectionViewHolder;->duration:Landroid/widget/TextView;

    if-eqz v6, :cond_3

    .line 1026
    iget-object v6, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter;->this$0:Lcom/samsung/musicplus/contents/extra/SelectionListFragment;

    # getter for: Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mDurationIdx:I
    invoke-static {v6}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->access$1700(Lcom/samsung/musicplus/contents/extra/SelectionListFragment;)I

    move-result v6

    if-ltz v6, :cond_3

    .line 1027
    iget-object v6, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter;->this$0:Lcom/samsung/musicplus/contents/extra/SelectionListFragment;

    # getter for: Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mDurationIdx:I
    invoke-static {v6}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->access$1800(Lcom/samsung/musicplus/contents/extra/SelectionListFragment;)I

    move-result v6

    invoke-interface {p3, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 1028
    .local v0, "duration":I
    iget-object v6, v5, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter$SelectionViewHolder;->duration:Landroid/widget/TextView;

    invoke-virtual {p0, p2, v6, v0}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter;->setDuration(Landroid/content/Context;Landroid/widget/TextView;I)V

    .line 1031
    .end local v0    # "duration":I
    :cond_3
    return-void
.end method

.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "c"    # Landroid/database/Cursor;

    .prologue
    .line 958
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter$SelectionViewHolder;

    .line 959
    .local v1, "vh":Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter$SelectionViewHolder;
    iget-object v2, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter;->this$0:Lcom/samsung/musicplus/contents/extra/SelectionListFragment;

    # getter for: Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mListType:I
    invoke-static {v2}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->access$600(Lcom/samsung/musicplus/contents/extra/SelectionListFragment;)I

    move-result v2

    invoke-static {v2}, Lcom/samsung/musicplus/util/ListUtils;->isPlaylistTabContent(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 960
    invoke-virtual {p0, p1, p2, p3}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter;->bindPlaylistItemView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    .line 966
    :cond_0
    :goto_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter;->bindTextView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    .line 967
    invoke-virtual {p0, p1, p2, p3}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter;->bindOtherView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    .line 968
    iget-object v2, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter;->this$0:Lcom/samsung/musicplus/contents/extra/SelectionListFragment;

    # getter for: Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mListType:I
    invoke-static {v2}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->access$600(Lcom/samsung/musicplus/contents/extra/SelectionListFragment;)I

    move-result v2

    invoke-static {v2}, Lcom/samsung/musicplus/util/ListUtils;->isTrack(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 969
    const v2, 0x7f0d00bb

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 970
    .local v0, "divider":Landroid/view/View;
    if-eqz v0, :cond_1

    .line 971
    const v2, 0x7f020022

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 974
    .end local v0    # "divider":Landroid/view/View;
    :cond_1
    return-void

    .line 962
    :cond_2
    iget-object v2, v1, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter$SelectionViewHolder;->thumbnail:Landroid/widget/ImageView;

    if-eqz v2, :cond_0

    .line 963
    invoke-virtual {p0, p1, p2, p3}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter;->bindThumbnailView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    goto :goto_0
.end method

.method protected getViewHolder()Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;
    .locals 1

    .prologue
    .line 1042
    new-instance v0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter$SelectionViewHolder;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter$SelectionViewHolder;-><init>(Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter;)V

    return-object v0
.end method

.method protected newAnimationView(Landroid/view/View;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 979
    return-void
.end method

.method protected newOtherView(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 936
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter$SelectionViewHolder;

    .line 937
    .local v2, "vh":Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter$SelectionViewHolder;
    iget-object v3, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter;->this$0:Lcom/samsung/musicplus/contents/extra/SelectionListFragment;

    # getter for: Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mListType:I
    invoke-static {v3}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->access$600(Lcom/samsung/musicplus/contents/extra/SelectionListFragment;)I

    move-result v3

    const v4, 0x10007

    if-ne v3, v4, :cond_0

    .line 938
    const v3, 0x7f0d00c1

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 939
    .local v1, "thumbnailIcon":Landroid/view/View;
    instance-of v3, v1, Landroid/view/ViewStub;

    if-eqz v3, :cond_0

    .line 940
    check-cast v1, Landroid/view/ViewStub;

    .end local v1    # "thumbnailIcon":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 943
    :cond_0
    iget-object v3, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter;->this$0:Lcom/samsung/musicplus/contents/extra/SelectionListFragment;

    # invokes: Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->isEnableSelect()Z
    invoke-static {v3}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->access$000(Lcom/samsung/musicplus/contents/extra/SelectionListFragment;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 944
    const v3, 0x7f0d00a6

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 945
    .local v0, "checkBox":Landroid/view/View;
    instance-of v3, v0, Landroid/view/ViewStub;

    if-eqz v3, :cond_1

    .line 946
    check-cast v0, Landroid/view/ViewStub;

    .end local v0    # "checkBox":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 948
    :cond_1
    const v3, 0x7f0d004c

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    iput-object v3, v2, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter$SelectionViewHolder;->checkbox:Landroid/widget/CheckBox;

    .line 950
    :cond_2
    invoke-virtual {p1, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 951
    iget-object v3, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter;->this$0:Lcom/samsung/musicplus/contents/extra/SelectionListFragment;

    # getter for: Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mListType:I
    invoke-static {v3}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->access$600(Lcom/samsung/musicplus/contents/extra/SelectionListFragment;)I

    move-result v3

    invoke-static {v3}, Lcom/samsung/musicplus/util/ListUtils;->isTrack(I)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 952
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->newOtherView(Landroid/view/View;)V

    .line 954
    :cond_3
    return-void
.end method

.method protected newTextView(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 905
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter$SelectionViewHolder;

    .line 906
    .local v0, "vh":Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter$SelectionViewHolder;
    const v1, 0x7f0d009f

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter$SelectionViewHolder;->text1:Landroid/widget/TextView;

    .line 907
    const v1, 0x7f0d00a0

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter$SelectionViewHolder;->text2:Landroid/widget/TextView;

    .line 908
    const v1, 0x7f0d00a1

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter$SelectionViewHolder;->text3:Landroid/widget/TextView;

    .line 909
    const v1, 0x7f0d00cb

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter$SelectionViewHolder;->duration:Landroid/widget/TextView;

    .line 910
    iget-object v1, v0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter$SelectionViewHolder;->text2:Landroid/widget/TextView;

    if-eqz v1, :cond_1

    .line 911
    iget-object v1, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter;->this$0:Lcom/samsung/musicplus/contents/extra/SelectionListFragment;

    # getter for: Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mText2Idx:I
    invoke-static {v1}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->access$900(Lcom/samsung/musicplus/contents/extra/SelectionListFragment;)I

    move-result v1

    if-gez v1, :cond_0

    iget-object v1, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter;->this$0:Lcom/samsung/musicplus/contents/extra/SelectionListFragment;

    # getter for: Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mListType:I
    invoke-static {v1}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->access$600(Lcom/samsung/musicplus/contents/extra/SelectionListFragment;)I

    move-result v1

    invoke-static {v1}, Lcom/samsung/musicplus/util/ListUtils;->isPlaylistTabContent(I)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 912
    :cond_0
    iget-object v1, v0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter$SelectionViewHolder;->text2:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 917
    :cond_1
    :goto_0
    iget-object v1, v0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter$SelectionViewHolder;->text3:Landroid/widget/TextView;

    if-eqz v1, :cond_2

    .line 918
    iget-object v1, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter;->this$0:Lcom/samsung/musicplus/contents/extra/SelectionListFragment;

    # getter for: Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mText3Idx:I
    invoke-static {v1}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->access$1000(Lcom/samsung/musicplus/contents/extra/SelectionListFragment;)I

    move-result v1

    if-ltz v1, :cond_5

    .line 919
    iget-object v1, v0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter$SelectionViewHolder;->text3:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 924
    :cond_2
    :goto_1
    iget-object v1, v0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter$SelectionViewHolder;->duration:Landroid/widget/TextView;

    if-eqz v1, :cond_3

    .line 925
    iget-object v1, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter;->this$0:Lcom/samsung/musicplus/contents/extra/SelectionListFragment;

    # getter for: Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mDurationIdx:I
    invoke-static {v1}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->access$1100(Lcom/samsung/musicplus/contents/extra/SelectionListFragment;)I

    move-result v1

    if-ltz v1, :cond_6

    .line 926
    iget-object v1, v0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter$SelectionViewHolder;->duration:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 931
    :cond_3
    :goto_2
    invoke-virtual {p1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 932
    return-void

    .line 914
    :cond_4
    iget-object v1, v0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter$SelectionViewHolder;->text2:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 921
    :cond_5
    iget-object v1, v0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter$SelectionViewHolder;->text3:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 928
    :cond_6
    iget-object v1, v0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter$SelectionViewHolder;->duration:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2
.end method
