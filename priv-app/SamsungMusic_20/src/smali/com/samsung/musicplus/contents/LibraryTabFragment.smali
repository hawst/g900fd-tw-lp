.class public Lcom/samsung/musicplus/contents/LibraryTabFragment;
.super Lcom/samsung/musicplus/MusicBaseFragment;
.source "LibraryTabFragment.java"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;
.implements Lcom/samsung/musicplus/base/list/IActionMode;
.implements Lcom/samsung/musicplus/contents/dlna/DlnaTabAsyncUpdater$OnDlnaTabListener;
.implements Lcom/samsung/musicplus/settings/PlayerSettingPreference;
.implements Lcom/samsung/musicplus/widget/ListSplitViewResizer$IResizerControlListener;
.implements Lcom/samsung/musicplus/widget/fragment/OnMusicFragmentInterface;
.implements Lcom/samsung/musicplus/widget/fragment/OnSconnectCallback;


# static fields
.field private static final PREF_KEY_CURRENT_TAB:Ljava/lang/String; = "music_current_tab"

.field private static final SAVED_ADDED_DMS:Ljava/lang/String; = "added_dms"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static final SAVED_TAB_ID:Ljava/lang/String; = "tab_id"

.field private static final TAG:Ljava/lang/String; = "MusicList"


# instance fields
.field private final DEVICES_TAB_ID:Ljava/lang/Integer;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private mDlnaTabUpdater:Lcom/samsung/musicplus/contents/dlna/DlnaTabAsyncUpdater;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private mIsDmsTabAdded:Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private mNeedUpdateTab:Z

.field private mPreferences:Landroid/content/SharedPreferences;

.field private mTabController:Lcom/samsung/musicplus/widget/tab/ITabController;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 51
    invoke-direct {p0}, Lcom/samsung/musicplus/MusicBaseFragment;-><init>()V

    .line 64
    const v0, 0x1000b

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/contents/LibraryTabFragment;->DEVICES_TAB_ID:Ljava/lang/Integer;

    .line 74
    iput-boolean v1, p0, Lcom/samsung/musicplus/contents/LibraryTabFragment;->mIsDmsTabAdded:Z

    .line 77
    iput-boolean v1, p0, Lcom/samsung/musicplus/contents/LibraryTabFragment;->mNeedUpdateTab:Z

    return-void
.end method

.method private addDmsTab()V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 238
    iget-boolean v0, p0, Lcom/samsung/musicplus/contents/LibraryTabFragment;->mIsDmsTabAdded:Z

    if-nez v0, :cond_0

    .line 239
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/contents/LibraryTabFragment;->mIsDmsTabAdded:Z

    .line 240
    iget-object v0, p0, Lcom/samsung/musicplus/contents/LibraryTabFragment;->mTabController:Lcom/samsung/musicplus/widget/tab/ITabController;

    iget-object v1, p0, Lcom/samsung/musicplus/contents/LibraryTabFragment;->DEVICES_TAB_ID:Ljava/lang/Integer;

    invoke-interface {v0, v1}, Lcom/samsung/musicplus/widget/tab/ITabController;->addTab(Ljava/lang/Integer;)V

    .line 242
    :cond_0
    return-void
.end method

.method private buildTabIds()Ljava/util/ArrayList;
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 182
    iget-object v5, p0, Lcom/samsung/musicplus/contents/LibraryTabFragment;->mPreferences:Landroid/content/SharedPreferences;

    const-string v6, "tab_menu_list"

    invoke-static {}, Lcom/samsung/musicplus/util/ListUtils;->getDefaultTab()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 184
    .local v2, "listOrder":Ljava/lang/String;
    new-instance v3, Ljava/util/StringTokenizer;

    const-string v5, "|"

    invoke-direct {v3, v2, v5}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    .local v3, "strToken":Ljava/util/StringTokenizer;
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 187
    .local v4, "tabIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->countTokens()I

    move-result v0

    .line 188
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 189
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->nextElement()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 188
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 192
    :cond_0
    iget-boolean v5, p0, Lcom/samsung/musicplus/contents/LibraryTabFragment;->mIsDmsTabAdded:Z

    if-eqz v5, :cond_1

    .line 195
    iget-object v5, p0, Lcom/samsung/musicplus/contents/LibraryTabFragment;->DEVICES_TAB_ID:Ljava/lang/Integer;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 197
    :cond_1
    return-object v4
.end method

.method private initializeTabs(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, -0x1

    .line 154
    if-eqz p1, :cond_0

    .line 155
    const-string v1, "tab_id"

    invoke-virtual {p1, v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 156
    .local v0, "currentTabId":I
    const-string v1, "added_dms"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/samsung/musicplus/contents/LibraryTabFragment;->mIsDmsTabAdded:Z

    .line 162
    :goto_0
    new-instance v1, Lcom/samsung/musicplus/widget/tab/TabHostTab;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/widget/tab/TabHostTab;-><init>(Landroid/app/Fragment;)V

    iput-object v1, p0, Lcom/samsung/musicplus/contents/LibraryTabFragment;->mTabController:Lcom/samsung/musicplus/widget/tab/ITabController;

    .line 166
    iget-object v1, p0, Lcom/samsung/musicplus/contents/LibraryTabFragment;->mTabController:Lcom/samsung/musicplus/widget/tab/ITabController;

    invoke-direct {p0}, Lcom/samsung/musicplus/contents/LibraryTabFragment;->buildTabIds()Ljava/util/ArrayList;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/samsung/musicplus/widget/tab/ITabController;->initializeTabs(Ljava/util/ArrayList;)V

    .line 167
    iget-object v1, p0, Lcom/samsung/musicplus/contents/LibraryTabFragment;->mTabController:Lcom/samsung/musicplus/widget/tab/ITabController;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/samsung/musicplus/widget/tab/ITabController;->setTabSelected(Ljava/lang/Integer;)V

    .line 168
    return-void

    .line 158
    .end local v0    # "currentTabId":I
    :cond_0
    iget-object v1, p0, Lcom/samsung/musicplus/contents/LibraryTabFragment;->mPreferences:Landroid/content/SharedPreferences;

    const-string v2, "music_current_tab"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .restart local v0    # "currentTabId":I
    goto :goto_0
.end method

.method private reinitializeTabs()V
    .locals 3

    .prologue
    .line 171
    iget-object v1, p0, Lcom/samsung/musicplus/contents/LibraryTabFragment;->mTabController:Lcom/samsung/musicplus/widget/tab/ITabController;

    invoke-interface {v1}, Lcom/samsung/musicplus/widget/tab/ITabController;->getCurrentTabId()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 172
    .local v0, "currentTabId":I
    iget-object v1, p0, Lcom/samsung/musicplus/contents/LibraryTabFragment;->mTabController:Lcom/samsung/musicplus/widget/tab/ITabController;

    invoke-direct {p0}, Lcom/samsung/musicplus/contents/LibraryTabFragment;->buildTabIds()Ljava/util/ArrayList;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/samsung/musicplus/widget/tab/ITabController;->initializeTabs(Ljava/util/ArrayList;)V

    .line 173
    iget-object v1, p0, Lcom/samsung/musicplus/contents/LibraryTabFragment;->mTabController:Lcom/samsung/musicplus/widget/tab/ITabController;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/samsung/musicplus/widget/tab/ITabController;->setTabSelected(Ljava/lang/Integer;)V

    .line 174
    return-void
.end method

.method private removeDmsTab()V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 249
    iget-boolean v0, p0, Lcom/samsung/musicplus/contents/LibraryTabFragment;->mIsDmsTabAdded:Z

    if-eqz v0, :cond_0

    .line 254
    iget-object v0, p0, Lcom/samsung/musicplus/contents/LibraryTabFragment;->mTabController:Lcom/samsung/musicplus/widget/tab/ITabController;

    iget-object v1, p0, Lcom/samsung/musicplus/contents/LibraryTabFragment;->mTabController:Lcom/samsung/musicplus/widget/tab/ITabController;

    invoke-interface {v1}, Lcom/samsung/musicplus/widget/tab/ITabController;->getTabCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Lcom/samsung/musicplus/widget/tab/ITabController;->removeTab(I)V

    .line 255
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/contents/LibraryTabFragment;->mIsDmsTabAdded:Z

    .line 257
    :cond_0
    return-void
.end method


# virtual methods
.method public actionSconnect()Z
    .locals 2

    .prologue
    .line 271
    iget-object v1, p0, Lcom/samsung/musicplus/contents/LibraryTabFragment;->mTabController:Lcom/samsung/musicplus/widget/tab/ITabController;

    invoke-interface {v1}, Lcom/samsung/musicplus/widget/tab/ITabController;->getCurrentContent()Ljava/lang/Object;

    move-result-object v0

    .line 272
    .local v0, "o":Ljava/lang/Object;
    instance-of v1, v0, Lcom/samsung/musicplus/widget/fragment/OnSconnectCallback;

    if-eqz v1, :cond_0

    .line 273
    check-cast v0, Lcom/samsung/musicplus/widget/fragment/OnSconnectCallback;

    .end local v0    # "o":Ljava/lang/Object;
    invoke-interface {v0}, Lcom/samsung/musicplus/widget/fragment/OnSconnectCallback;->actionSconnect()Z

    move-result v1

    .line 275
    :goto_0
    return v1

    .restart local v0    # "o":Ljava/lang/Object;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected createMusicMenus()Lcom/samsung/musicplus/common/menu/AbstractMusicMenus;
    .locals 1

    .prologue
    .line 204
    const/4 v0, 0x0

    return-object v0
.end method

.method public getListType()I
    .locals 1

    .prologue
    .line 306
    const/4 v0, 0x0

    return v0
.end method

.method public isEmptyList()Z
    .locals 1

    .prologue
    .line 311
    const/4 v0, 0x0

    return v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 99
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/LibraryTabFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "music_player_pref"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/contents/LibraryTabFragment;->mPreferences:Landroid/content/SharedPreferences;

    .line 100
    iget-object v0, p0, Lcom/samsung/musicplus/contents/LibraryTabFragment;->mPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 101
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/contents/LibraryTabFragment;->initializeTabs(Landroid/os/Bundle;)V

    .line 106
    invoke-super {p0, p1}, Lcom/samsung/musicplus/MusicBaseFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 107
    return-void
.end method

.method public onAddDmsTab()V
    .locals 0

    .prologue
    .line 261
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/LibraryTabFragment;->addDmsTab()V

    .line 262
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 88
    const v0, 0x7f04005a

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDestroy()V
    .locals 6

    .prologue
    .line 132
    iget-object v2, p0, Lcom/samsung/musicplus/contents/LibraryTabFragment;->mPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 133
    .local v1, "editor":Landroid/content/SharedPreferences$Editor;
    iget-object v2, p0, Lcom/samsung/musicplus/contents/LibraryTabFragment;->mTabController:Lcom/samsung/musicplus/widget/tab/ITabController;

    invoke-interface {v2}, Lcom/samsung/musicplus/widget/tab/ITabController;->getCurrentTabId()Ljava/lang/Integer;

    move-result-object v0

    .line 134
    .local v0, "currentTabID":Ljava/lang/Integer;
    const-string v3, "music_current_tab"

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    :goto_0
    invoke-interface {v1, v3, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 137
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 138
    iget-object v2, p0, Lcom/samsung/musicplus/contents/LibraryTabFragment;->mPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v2, p0}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 139
    invoke-super {p0}, Lcom/samsung/musicplus/MusicBaseFragment;->onDestroy()V

    .line 140
    return-void

    .line 134
    :cond_0
    iget-object v2, p0, Lcom/samsung/musicplus/contents/LibraryTabFragment;->mPreferences:Landroid/content/SharedPreferences;

    const-string v4, "music_current_tab"

    const/4 v5, -0x1

    invoke-interface {v2, v4, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    goto :goto_0
.end method

.method public onFragmentWindowFocusChanged(Z)V
    .locals 0
    .param p1, "hasFocus"    # Z

    .prologue
    .line 219
    return-void
.end method

.method public onMultiWindowModeChanged(Z)V
    .locals 1
    .param p1, "isMultiWindow"    # Z

    .prologue
    .line 301
    iget-object v0, p0, Lcom/samsung/musicplus/contents/LibraryTabFragment;->mTabController:Lcom/samsung/musicplus/widget/tab/ITabController;

    invoke-interface {v0, p1}, Lcom/samsung/musicplus/widget/tab/ITabController;->onMultiWindowStateChanged(Z)V

    .line 302
    return-void
.end method

.method public onRemoveDmsTab()V
    .locals 0

    .prologue
    .line 266
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/LibraryTabFragment;->removeDmsTab()V

    .line 267
    return-void
.end method

.method public onResizerMoveEnd()V
    .locals 0

    .prologue
    .line 297
    return-void
.end method

.method public onResizerMoveStart()V
    .locals 0

    .prologue
    .line 290
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 144
    iget-object v1, p0, Lcom/samsung/musicplus/contents/LibraryTabFragment;->mTabController:Lcom/samsung/musicplus/widget/tab/ITabController;

    invoke-interface {v1}, Lcom/samsung/musicplus/widget/tab/ITabController;->getCurrentTabId()Ljava/lang/Integer;

    move-result-object v0

    .line 145
    .local v0, "currentTabID":Ljava/lang/Integer;
    const-string v2, "tab_id"

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    :goto_0
    invoke-virtual {p1, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 148
    const-string v1, "added_dms"

    iget-boolean v2, p0, Lcom/samsung/musicplus/contents/LibraryTabFragment;->mIsDmsTabAdded:Z

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 149
    invoke-super {p0, p1}, Lcom/samsung/musicplus/MusicBaseFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 150
    return-void

    .line 145
    :cond_0
    iget-object v1, p0, Lcom/samsung/musicplus/contents/LibraryTabFragment;->mPreferences:Landroid/content/SharedPreferences;

    const-string v3, "music_current_tab"

    const/4 v4, -0x1

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    goto :goto_0
.end method

.method public onServiceConnected()V
    .locals 1

    .prologue
    .line 223
    iget-object v0, p0, Lcom/samsung/musicplus/contents/LibraryTabFragment;->mTabController:Lcom/samsung/musicplus/widget/tab/ITabController;

    invoke-interface {v0}, Lcom/samsung/musicplus/widget/tab/ITabController;->onServiceConnected()V

    .line 224
    return-void
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 1
    .param p1, "sp"    # Landroid/content/SharedPreferences;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 228
    const-string v0, "tab_menu_list"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 229
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/contents/LibraryTabFragment;->mNeedUpdateTab:Z

    .line 231
    :cond_0
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 111
    invoke-super {p0}, Lcom/samsung/musicplus/MusicBaseFragment;->onStart()V

    .line 112
    iget-boolean v0, p0, Lcom/samsung/musicplus/contents/LibraryTabFragment;->mNeedUpdateTab:Z

    if-eqz v0, :cond_0

    .line 113
    const-string v0, "MusicList"

    const-string v1, "Tab pref changed, reinitialize tabs!"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/LibraryTabFragment;->reinitializeTabs()V

    .line 115
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/contents/LibraryTabFragment;->mNeedUpdateTab:Z

    .line 117
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/contents/LibraryTabFragment;->mDlnaTabUpdater:Lcom/samsung/musicplus/contents/dlna/DlnaTabAsyncUpdater;

    if-eqz v0, :cond_1

    .line 118
    iget-object v0, p0, Lcom/samsung/musicplus/contents/LibraryTabFragment;->mDlnaTabUpdater:Lcom/samsung/musicplus/contents/dlna/DlnaTabAsyncUpdater;

    invoke-virtual {v0}, Lcom/samsung/musicplus/contents/dlna/DlnaTabAsyncUpdater;->startObserving()V

    .line 120
    :cond_1
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/samsung/musicplus/contents/LibraryTabFragment;->mDlnaTabUpdater:Lcom/samsung/musicplus/contents/dlna/DlnaTabAsyncUpdater;

    if-eqz v0, :cond_0

    .line 125
    iget-object v0, p0, Lcom/samsung/musicplus/contents/LibraryTabFragment;->mDlnaTabUpdater:Lcom/samsung/musicplus/contents/dlna/DlnaTabAsyncUpdater;

    invoke-virtual {v0}, Lcom/samsung/musicplus/contents/dlna/DlnaTabAsyncUpdater;->stopObserving()V

    .line 127
    :cond_0
    invoke-super {p0}, Lcom/samsung/musicplus/MusicBaseFragment;->onStop()V

    .line 128
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 94
    invoke-super {p0, p1, p2}, Lcom/samsung/musicplus/MusicBaseFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 95
    return-void
.end method

.method public receivePlayerState(Ljava/lang/String;)V
    .locals 1
    .param p1, "action"    # Ljava/lang/String;

    .prologue
    .line 210
    const-string v0, "com.android.music.settingchanged"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 214
    :goto_0
    return-void

    .line 213
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/contents/LibraryTabFragment;->mTabController:Lcom/samsung/musicplus/widget/tab/ITabController;

    invoke-interface {v0, p1}, Lcom/samsung/musicplus/widget/tab/ITabController;->receivePlayerState(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setActionModeEnabled(Z)V
    .locals 0
    .param p1, "enabled"    # Z

    .prologue
    .line 283
    return-void
.end method
