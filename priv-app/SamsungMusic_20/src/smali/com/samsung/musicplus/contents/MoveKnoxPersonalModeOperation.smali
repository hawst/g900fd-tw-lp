.class public Lcom/samsung/musicplus/contents/MoveKnoxPersonalModeOperation;
.super Ljava/lang/Object;
.source "MoveKnoxPersonalModeOperation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/contents/MoveKnoxPersonalModeOperation$1;,
        Lcom/samsung/musicplus/contents/MoveKnoxPersonalModeOperation$KnoxFileOperationTask;,
        Lcom/samsung/musicplus/contents/MoveKnoxPersonalModeOperation$OnSetResultListener;
    }
.end annotation


# static fields
.field private static final FAIL_ERROR_CODE_CONTAINER_STATE_PROBLEM:I = 0x1

.field private static final FAIL_ERROR_CODE_NOT_ERROR:I = -0x1

.field private static final FAIL_ERROR_CODE_STORAGE_FULL:I = 0x7

.field public static final PERSONAL_CONTAINER_ID:I = 0x0

.field public static final RELAY_SUCCESS:I = 0x0

.field private static final TAG:Ljava/lang/String; = "KnoxModeManager"

.field public static final UNDEFINED_CONTAINER_ID:I = -0x1


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mErrCode:I

.field private mFilePath:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mIsCancled:Z

.field private mKnoxManager:Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;

.field private mListener:Lcom/samsung/musicplus/contents/MoveKnoxPersonalModeOperation$OnSetResultListener;

.field private mMoveCount:I

.field private mTask:Lcom/samsung/musicplus/contents/MoveKnoxPersonalModeOperation$KnoxFileOperationTask;

.field private mThreadId:J


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/samsung/musicplus/contents/MoveKnoxPersonalModeOperation$OnSetResultListener;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "l"    # Lcom/samsung/musicplus/contents/MoveKnoxPersonalModeOperation$OnSetResultListener;

    .prologue
    const/4 v1, 0x0

    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-boolean v1, p0, Lcom/samsung/musicplus/contents/MoveKnoxPersonalModeOperation;->mIsCancled:Z

    .line 39
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/musicplus/contents/MoveKnoxPersonalModeOperation;->mErrCode:I

    .line 41
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/musicplus/contents/MoveKnoxPersonalModeOperation;->mFilePath:Ljava/util/ArrayList;

    .line 172
    iput v1, p0, Lcom/samsung/musicplus/contents/MoveKnoxPersonalModeOperation;->mMoveCount:I

    .line 174
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/samsung/musicplus/contents/MoveKnoxPersonalModeOperation;->mThreadId:J

    .line 67
    iput-object p1, p0, Lcom/samsung/musicplus/contents/MoveKnoxPersonalModeOperation;->mContext:Landroid/content/Context;

    .line 68
    invoke-static {p1}, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;->getInstance(Landroid/content/Context;)Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/contents/MoveKnoxPersonalModeOperation;->mKnoxManager:Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;

    .line 69
    iput-object p2, p0, Lcom/samsung/musicplus/contents/MoveKnoxPersonalModeOperation;->mListener:Lcom/samsung/musicplus/contents/MoveKnoxPersonalModeOperation$OnSetResultListener;

    .line 70
    return-void
.end method

.method private makeMoveFilePathList([J)V
    .locals 8
    .param p1, "list"    # [J

    .prologue
    .line 87
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/contents/MoveKnoxPersonalModeOperation;->mFilePath:Ljava/util/ArrayList;

    .line 88
    const/4 v0, 0x4

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "_id"

    aput-object v1, v2, v0

    const/4 v0, 0x1

    const-string v1, "_data"

    aput-object v1, v2, v0

    const/4 v0, 0x2

    const-string v1, "_display_name"

    aput-object v1, v2, v0

    const/4 v0, 0x3

    const-string v1, "bucket_id"

    aput-object v1, v2, v0

    .line 92
    .local v2, "cols":[Ljava/lang/String;
    const-string v0, "_id"

    invoke-static {v0, p1}, Lcom/samsung/musicplus/util/UiUtils;->convertAudioIdsToSelection(Ljava/lang/String;[J)Ljava/lang/String;

    move-result-object v3

    .line 94
    .local v3, "where":Ljava/lang/String;
    const/4 v6, 0x0

    .line 96
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/samsung/musicplus/contents/MoveKnoxPersonalModeOperation;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/musicplus/provider/MusicContents;->MUSIC_CONTENTS_URI:Landroid/net/Uri;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 98
    if-nez v6, :cond_1

    .line 99
    const-string v0, "KnoxModeManager"

    const-string v1, "requestMoveToKnox fail"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 113
    if-eqz v6, :cond_0

    .line 114
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 117
    :cond_0
    :goto_0
    return-void

    .line 103
    :cond_1
    :try_start_1
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 104
    :goto_1
    invoke-interface {v6}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_4

    .line 105
    const-string v0, "_data"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 107
    .local v7, "srcPath":Ljava/lang/String;
    if-eqz v7, :cond_2

    const-string v0, ""

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 108
    iget-object v0, p0, Lcom/samsung/musicplus/contents/MoveKnoxPersonalModeOperation;->mFilePath:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 110
    :cond_2
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 113
    .end local v7    # "srcPath":Ljava/lang/String;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    .line 114
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0

    .line 113
    :cond_4
    if-eqz v6, :cond_0

    .line 114
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method


# virtual methods
.method public deleteListItemInDB(J)V
    .locals 5
    .param p1, "id"    # J

    .prologue
    .line 132
    iget-object v0, p0, Lcom/samsung/musicplus/contents/MoveKnoxPersonalModeOperation;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/musicplus/provider/MusicContents;->MUSIC_CONTENTS_URI:Landroid/net/Uri;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "_id="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 134
    return-void
.end method

.method public endFileOperation()V
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/samsung/musicplus/contents/MoveKnoxPersonalModeOperation;->mKnoxManager:Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;

    invoke-virtual {v0}, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;->releaseSession()V

    .line 125
    return-void
.end method

.method public getErrorMsg()I
    .locals 2

    .prologue
    .line 153
    const/4 v0, 0x0

    .line 154
    .local v0, "msg":I
    iget v1, p0, Lcom/samsung/musicplus/contents/MoveKnoxPersonalModeOperation;->mErrCode:I

    sparse-switch v1, :sswitch_data_0

    .line 162
    const v0, 0x7f10007d

    .line 165
    :goto_0
    return v0

    .line 156
    :sswitch_0
    const v0, 0x7f10010b

    .line 157
    goto :goto_0

    .line 159
    :sswitch_1
    const v0, 0x7f1000cd

    .line 160
    goto :goto_0

    .line 154
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x7 -> :sswitch_0
    .end sparse-switch
.end method

.method public getErrorMsg(I)Ljava/lang/String;
    .locals 1
    .param p1, "result"    # I

    .prologue
    .line 169
    const/4 v0, 0x0

    return-object v0
.end method

.method public getErrorMsgKnoxV2()Ljava/lang/String;
    .locals 1

    .prologue
    .line 149
    const/4 v0, 0x0

    return-object v0
.end method

.method public isFailed()Z
    .locals 1

    .prologue
    .line 141
    iget v0, p0, Lcom/samsung/musicplus/contents/MoveKnoxPersonalModeOperation;->mErrCode:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isOperationCanceled()Z
    .locals 1

    .prologue
    .line 137
    iget-boolean v0, p0, Lcom/samsung/musicplus/contents/MoveKnoxPersonalModeOperation;->mIsCancled:Z

    return v0
.end method

.method public requestCancelMoveFile()V
    .locals 0

    .prologue
    .line 121
    return-void
.end method

.method public requestMoveToKnox([JJ)V
    .locals 4
    .param p1, "list"    # [J
    .param p2, "destinationId"    # J

    .prologue
    .line 73
    const-string v0, "KnoxModeManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "requestMoveToKnox() - container Id : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 75
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/contents/MoveKnoxPersonalModeOperation;->makeMoveFilePathList([J)V

    .line 77
    sget-boolean v0, Lcom/samsung/musicplus/util/Version;->LIBRARY_OVER_API_1:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/contents/MoveKnoxPersonalModeOperation;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;->isOverVersion2(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 78
    new-instance v0, Lcom/samsung/musicplus/contents/MoveKnoxPersonalModeOperation$KnoxFileOperationTask;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/samsung/musicplus/contents/MoveKnoxPersonalModeOperation$KnoxFileOperationTask;-><init>(Lcom/samsung/musicplus/contents/MoveKnoxPersonalModeOperation;Lcom/samsung/musicplus/contents/MoveKnoxPersonalModeOperation$1;)V

    iput-object v0, p0, Lcom/samsung/musicplus/contents/MoveKnoxPersonalModeOperation;->mTask:Lcom/samsung/musicplus/contents/MoveKnoxPersonalModeOperation$KnoxFileOperationTask;

    .line 79
    iget-object v0, p0, Lcom/samsung/musicplus/contents/MoveKnoxPersonalModeOperation;->mTask:Lcom/samsung/musicplus/contents/MoveKnoxPersonalModeOperation$KnoxFileOperationTask;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Integer;

    const/4 v2, 0x0

    long-to-int v3, p2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/contents/MoveKnoxPersonalModeOperation$KnoxFileOperationTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 84
    :goto_0
    return-void

    .line 81
    :cond_0
    const-string v0, "KnoxModeManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Request file paths - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/contents/MoveKnoxPersonalModeOperation;->mFilePath:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    iget-object v0, p0, Lcom/samsung/musicplus/contents/MoveKnoxPersonalModeOperation;->mKnoxManager:Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;

    iget-object v1, p0, Lcom/samsung/musicplus/contents/MoveKnoxPersonalModeOperation;->mFilePath:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/samsung/musicplus/contents/MoveKnoxPersonalModeOperation;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;->requestMoveFile(Ljava/util/ArrayList;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setErrorCode(I)V
    .locals 0
    .param p1, "err"    # I

    .prologue
    .line 145
    iput p1, p0, Lcom/samsung/musicplus/contents/MoveKnoxPersonalModeOperation;->mErrCode:I

    .line 146
    return-void
.end method
