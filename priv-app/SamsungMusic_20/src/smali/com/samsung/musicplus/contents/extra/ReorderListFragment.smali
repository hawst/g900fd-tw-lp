.class public Lcom/samsung/musicplus/contents/extra/ReorderListFragment;
.super Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;
.source "ReorderListFragment.java"


# static fields
.field private static final PLAYLIST_ID:Ljava/lang/String; = "playlist_id"

.field private static final SAVED_INSTANCE_REORDER_CHANGED:Ljava/lang/String; = "saved_reorder_changed"


# instance fields
.field private final mDndController:Lcom/sec/android/touchwiz/animator/TwAbsDndAnimator$TwDndController;

.field private mIsReorderChanged:Z

.field private mPlaylistId:J

.field private mReorderKey:Ljava/lang/String;

.field private mReorderList:I

.field private mSkipDrawing:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 29
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;-><init>()V

    .line 40
    iput-boolean v0, p0, Lcom/samsung/musicplus/contents/extra/ReorderListFragment;->mIsReorderChanged:Z

    .line 42
    iput-boolean v0, p0, Lcom/samsung/musicplus/contents/extra/ReorderListFragment;->mSkipDrawing:Z

    .line 177
    new-instance v0, Lcom/samsung/musicplus/contents/extra/ReorderListFragment$2;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/contents/extra/ReorderListFragment$2;-><init>(Lcom/samsung/musicplus/contents/extra/ReorderListFragment;)V

    iput-object v0, p0, Lcom/samsung/musicplus/contents/extra/ReorderListFragment;->mDndController:Lcom/sec/android/touchwiz/animator/TwAbsDndAnimator$TwDndController;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/contents/extra/ReorderListFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/extra/ReorderListFragment;

    .prologue
    .line 29
    iget-boolean v0, p0, Lcom/samsung/musicplus/contents/extra/ReorderListFragment;->mSkipDrawing:Z

    return v0
.end method

.method static synthetic access$002(Lcom/samsung/musicplus/contents/extra/ReorderListFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/extra/ReorderListFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 29
    iput-boolean p1, p0, Lcom/samsung/musicplus/contents/extra/ReorderListFragment;->mSkipDrawing:Z

    return p1
.end method

.method static synthetic access$102(Lcom/samsung/musicplus/contents/extra/ReorderListFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/extra/ReorderListFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 29
    iput-boolean p1, p0, Lcom/samsung/musicplus/contents/extra/ReorderListFragment;->mIsReorderChanged:Z

    return p1
.end method

.method public static getNewInstance(IILjava/lang/String;JZ)Landroid/app/Fragment;
    .locals 3
    .param p0, "list"    # I
    .param p1, "mode"    # I
    .param p2, "key"    # Ljava/lang/String;
    .param p3, "playlistId"    # J
    .param p5, "isReorderChanged"    # Z

    .prologue
    .line 47
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 48
    .local v0, "data":Landroid/os/Bundle;
    const-string v2, "list"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 49
    const-string v2, "key"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    const-string v2, "playlist_id"

    invoke-virtual {v0, v2, p3, p4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 51
    const-string v2, "saved_reorder_changed"

    invoke-virtual {v0, v2, p5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 52
    new-instance v1, Lcom/samsung/musicplus/contents/extra/ReorderListFragment;

    invoke-direct {v1}, Lcom/samsung/musicplus/contents/extra/ReorderListFragment;-><init>()V

    .line 53
    .local v1, "fg":Landroid/app/Fragment;
    invoke-virtual {v1, v0}, Landroid/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 54
    return-object v1
.end method

.method private initializeSkipDrawing()V
    .locals 1

    .prologue
    .line 163
    iget-boolean v0, p0, Lcom/samsung/musicplus/contents/extra/ReorderListFragment;->mSkipDrawing:Z

    if-eqz v0, :cond_0

    .line 164
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/contents/extra/ReorderListFragment;->mSkipDrawing:Z

    .line 166
    :cond_0
    return-void
.end method

.method private setReorderList()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 134
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/extra/ReorderListFragment;->getAbsListView()Lcom/sec/android/touchwiz/widget/TwAbsListView;

    move-result-object v1

    check-cast v1, Lcom/sec/android/touchwiz/widget/TwListView;

    .line 135
    .local v1, "twListView":Lcom/sec/android/touchwiz/widget/TwListView;
    new-instance v0, Lcom/sec/android/touchwiz/animator/TwDndListAnimator;

    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/extra/ReorderListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v0, v2, v1}, Lcom/sec/android/touchwiz/animator/TwDndListAnimator;-><init>(Landroid/content/Context;Lcom/sec/android/touchwiz/widget/TwListView;)V

    .line 136
    .local v0, "dndListAnimator":Lcom/sec/android/touchwiz/animator/TwDndListAnimator;
    iget-object v2, p0, Lcom/samsung/musicplus/contents/extra/ReorderListFragment;->mDndController:Lcom/sec/android/touchwiz/animator/TwAbsDndAnimator$TwDndController;

    invoke-virtual {v0, v2}, Lcom/sec/android/touchwiz/animator/TwDndListAnimator;->setDndController(Lcom/sec/android/touchwiz/animator/TwAbsDndAnimator$TwDndController;)V

    .line 137
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/touchwiz/widget/TwListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 138
    const v2, 0x7f020116

    invoke-virtual {v0, v2}, Lcom/sec/android/touchwiz/animator/TwDndListAnimator;->setDragGrabHandleDrawable(I)V

    .line 139
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/extra/ReorderListFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c0157

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v0, v2, v4, v4, v4}, Lcom/sec/android/touchwiz/animator/TwDndListAnimator;->setDragGrabHandlePadding(IIII)V

    .line 144
    const/4 v2, 0x3

    invoke-virtual {v0, v2}, Lcom/sec/android/touchwiz/animator/TwDndListAnimator;->setDragGrabHandlePositionGravity(I)V

    .line 145
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/sec/android/touchwiz/animator/TwDndListAnimator;->setDndMode(Z)V

    .line 153
    invoke-virtual {v1}, Lcom/sec/android/touchwiz/widget/TwListView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v2

    new-instance v3, Lcom/samsung/musicplus/contents/extra/ReorderListFragment$1;

    invoke-direct {v3, p0}, Lcom/samsung/musicplus/contents/extra/ReorderListFragment$1;-><init>(Lcom/samsung/musicplus/contents/extra/ReorderListFragment;)V

    invoke-virtual {v2, v3}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 160
    return-void
.end method


# virtual methods
.method protected addOtherView(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 0
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 120
    return-object p1
.end method

.method protected createAdapter()Landroid/widget/CursorAdapter;
    .locals 5

    .prologue
    .line 99
    new-instance v0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;

    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/extra/ReorderListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const v2, 0x7f04003f

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;I)V

    return-object v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 76
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 79
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/extra/ReorderListFragment;->getAbsListView()Lcom/sec/android/touchwiz/widget/TwAbsListView;

    move-result-object v1

    .line 80
    .local v1, "lv":Lcom/sec/android/touchwiz/widget/TwAbsListView;
    if-eqz v1, :cond_0

    .line 81
    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/contents/extra/ReorderListFragment;->unregisterForContextMenu(Landroid/view/View;)V

    .line 84
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/extra/ReorderListFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 85
    .local v0, "data":Landroid/os/Bundle;
    const-string v2, "list"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/samsung/musicplus/contents/extra/ReorderListFragment;->mReorderList:I

    .line 86
    const-string v2, "key"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/musicplus/contents/extra/ReorderListFragment;->mReorderKey:Ljava/lang/String;

    .line 87
    const-string v2, "playlist_id"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/samsung/musicplus/contents/extra/ReorderListFragment;->mPlaylistId:J

    .line 88
    const-string v2, "saved_reorder_changed"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, p0, Lcom/samsung/musicplus/contents/extra/ReorderListFragment;->mIsReorderChanged:Z

    .line 90
    if-eqz p1, :cond_1

    .line 91
    const-string v2, "saved_reorder_changed"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/samsung/musicplus/contents/extra/ReorderListFragment;->mIsReorderChanged:Z

    .line 94
    :cond_1
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/extra/ReorderListFragment;->setReorderList()V

    .line 95
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 65
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 66
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/contents/extra/ReorderListFragment;->setHasOptionsMenu(Z)V

    .line 67
    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/content/Loader;
    .locals 4
    .param p1, "id"    # I
    .param p2, "args"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 104
    invoke-super {p0, p1, p2}, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->onCreateLoader(ILandroid/os/Bundle;)Landroid/content/Loader;

    move-result-object v0

    check-cast v0, Landroid/content/CursorLoader;

    .line 105
    .local v0, "cl":Landroid/content/CursorLoader;
    if-eqz v0, :cond_0

    .line 106
    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/CursorLoader;->setUpdateThrottle(J)V

    .line 108
    :cond_0
    return-object v0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 0
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "inflater"    # Landroid/view/MenuInflater;

    .prologue
    .line 126
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 71
    const v0, 0x7f040022

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onListItemClick(Lcom/sec/android/touchwiz/widget/TwAbsListView;Landroid/view/View;IJ)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/touchwiz/widget/TwAbsListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    .line 130
    return-void
.end method

.method public onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V
    .locals 0
    .param p2, "data"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 113
    .local p1, "loader":Landroid/content/Loader;, "Landroid/content/Loader<Landroid/database/Cursor;>;"
    invoke-super {p0, p1, p2}, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V

    .line 114
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/extra/ReorderListFragment;->initializeSkipDrawing()V

    .line 115
    return-void
.end method

.method public bridge synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Landroid/content/Loader;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 29
    check-cast p2, Landroid/database/Cursor;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/samsung/musicplus/contents/extra/ReorderListFragment;->onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 59
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 60
    const-string v0, "saved_reorder_changed"

    iget-boolean v1, p0, Lcom/samsung/musicplus/contents/extra/ReorderListFragment;->mIsReorderChanged:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 61
    return-void
.end method

.method public saveListOrder()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 169
    iget-boolean v0, p0, Lcom/samsung/musicplus/contents/extra/ReorderListFragment;->mIsReorderChanged:Z

    if-eqz v0, :cond_0

    .line 170
    new-instance v1, Lcom/samsung/musicplus/util/FileOperationTask$ReorderTask;

    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/extra/ReorderListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const/4 v3, 0x1

    iget-wide v4, p0, Lcom/samsung/musicplus/contents/extra/ReorderListFragment;->mPlaylistId:J

    iget v6, p0, Lcom/samsung/musicplus/contents/extra/ReorderListFragment;->mReorderList:I

    iget-object v7, p0, Lcom/samsung/musicplus/contents/extra/ReorderListFragment;->mReorderKey:Ljava/lang/String;

    invoke-direct/range {v1 .. v8}, Lcom/samsung/musicplus/util/FileOperationTask$ReorderTask;-><init>(Landroid/app/Activity;ZJILjava/lang/String;Z)V

    new-array v0, v8, [Ljava/lang/Void;

    invoke-virtual {v1, v0}, Lcom/samsung/musicplus/util/FileOperationTask$ReorderTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 175
    :goto_0
    return-void

    .line 173
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/extra/ReorderListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method
