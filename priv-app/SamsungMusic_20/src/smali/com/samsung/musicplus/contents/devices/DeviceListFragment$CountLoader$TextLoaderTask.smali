.class Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader$TextLoaderTask;
.super Landroid/os/AsyncTask;
.source "DeviceListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TextLoaderTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        ">;"
    }
.end annotation


# instance fields
.field private data:Ljava/lang/Object;

.field private final mContext:Landroid/content/Context;

.field private final textViewReference:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader;


# direct methods
.method public constructor <init>(Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader;Landroid/content/Context;Landroid/widget/TextView;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "textView"    # Landroid/widget/TextView;

    .prologue
    .line 1272
    iput-object p1, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader$TextLoaderTask;->this$0:Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 1273
    iput-object p2, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader$TextLoaderTask;->mContext:Landroid/content/Context;

    .line 1274
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p3}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader$TextLoaderTask;->textViewReference:Ljava/lang/ref/WeakReference;

    .line 1275
    return-void
.end method

.method static synthetic access$2800(Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader$TextLoaderTask;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader$TextLoaderTask;

    .prologue
    .line 1265
    iget-object v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader$TextLoaderTask;->data:Ljava/lang/Object;

    return-object v0
.end method

.method private getAttachedTextView()Landroid/widget/TextView;
    .locals 5

    .prologue
    .line 1283
    iget-object v2, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader$TextLoaderTask;->textViewReference:Ljava/lang/ref/WeakReference;

    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 1284
    .local v1, "textView":Landroid/widget/TextView;
    # invokes: Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader;->getCounterWorkerTask(Landroid/widget/TextView;)Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader$TextLoaderTask;
    invoke-static {v1}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader;->access$2900(Landroid/widget/TextView;)Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader$TextLoaderTask;

    move-result-object v0

    .line 1286
    .local v0, "task":Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader$TextLoaderTask;
    const-string v2, "MusicUiDeviceTab"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getAttachedTextView this "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " task :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1287
    if-ne p0, v0, :cond_0

    .line 1292
    .end local v1    # "textView":Landroid/widget/TextView;
    :goto_0
    return-object v1

    .restart local v1    # "textView":Landroid/widget/TextView;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getCount(Landroid/net/Uri;)I
    .locals 8
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 1324
    const/4 v7, 0x0

    .line 1325
    .local v7, "count":I
    const/4 v6, 0x0

    .line 1327
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader$TextLoaderTask;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v3, "count(*)"

    aput-object v3, v2, v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1330
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1331
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v7

    .line 1334
    :cond_0
    if-eqz v6, :cond_1

    .line 1335
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1338
    :cond_1
    return v7

    .line 1334
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 1335
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Object;)Ljava/lang/Integer;
    .locals 6
    .param p1, "params"    # [Ljava/lang/Object;

    .prologue
    .line 1297
    const-string v2, "MusicUiDeviceTab"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "doInBackground "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1298
    const/4 v2, 0x0

    aget-object v2, p1, v2

    iput-object v2, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader$TextLoaderTask;->data:Ljava/lang/Object;

    .line 1301
    iget-object v2, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader$TextLoaderTask;->this$0:Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader;

    # getter for: Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader;->mPauseWorkLock:Ljava/lang/Object;
    invoke-static {v2}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader;->access$3000(Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader;)Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3

    .line 1302
    :goto_0
    :try_start_0
    iget-object v2, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader$TextLoaderTask;->this$0:Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader;

    # getter for: Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader;->mPauseWork:Z
    invoke-static {v2}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader;->access$3100(Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader$TextLoaderTask;->isCancelled()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_0

    .line 1304
    :try_start_1
    iget-object v2, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader$TextLoaderTask;->this$0:Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader;

    # getter for: Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader;->mPauseWorkLock:Ljava/lang/Object;
    invoke-static {v2}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader;->access$3000(Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->wait()V

    .line 1305
    const-string v2, "MusicUiDeviceTab"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "doInBackground wait "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1306
    :catch_0
    move-exception v2

    goto :goto_0

    .line 1309
    :cond_0
    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1310
    const/4 v0, 0x0

    .line 1315
    .local v0, "count":I
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader$TextLoaderTask;->isCancelled()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-direct {p0}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader$TextLoaderTask;->getAttachedTextView()Landroid/widget/TextView;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader$TextLoaderTask;->this$0:Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader;

    # getter for: Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader;->mExitTasksEarly:Z
    invoke-static {v2}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader;->access$3200(Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1316
    iget-object v2, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader$TextLoaderTask;->data:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/samsung/musicplus/provider/MusicContents$Audio$Devices;->getContentsUri(J)Landroid/net/Uri;

    move-result-object v1

    .line 1317
    .local v1, "uri":Landroid/net/Uri;
    invoke-direct {p0, v1}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader$TextLoaderTask;->getCount(Landroid/net/Uri;)I

    move-result v0

    .line 1319
    .end local v1    # "uri":Landroid/net/Uri;
    :cond_1
    const-string v2, "MusicUiDeviceTab"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "doInBackground count "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " this class "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1320
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    return-object v2

    .line 1309
    .end local v0    # "count":I
    :catchall_0
    move-exception v2

    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v2
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 1265
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader$TextLoaderTask;->doInBackground([Ljava/lang/Object;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected onCancelled(Ljava/lang/Integer;)V
    .locals 3
    .param p1, "result"    # Ljava/lang/Integer;

    .prologue
    .line 1360
    const-string v0, "MusicUiDeviceTab"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCancelled "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1361
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onCancelled(Ljava/lang/Object;)V

    .line 1362
    iget-object v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader$TextLoaderTask;->this$0:Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader;

    # getter for: Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader;->mPauseWorkLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader;->access$3000(Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 1363
    :try_start_0
    iget-object v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader$TextLoaderTask;->this$0:Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader;

    # getter for: Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader;->mPauseWorkLock:Ljava/lang/Object;
    invoke-static {v0}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader;->access$3000(Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 1364
    monitor-exit v1

    .line 1365
    return-void

    .line 1364
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected bridge synthetic onCancelled(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 1265
    check-cast p1, Ljava/lang/Integer;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader$TextLoaderTask;->onCancelled(Ljava/lang/Integer;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Integer;)V
    .locals 6
    .param p1, "result"    # Ljava/lang/Integer;

    .prologue
    .line 1343
    const-string v1, "MusicUiDeviceTab"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onPostExecute "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1346
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader$TextLoaderTask;->isCancelled()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader$TextLoaderTask;->this$0:Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader;

    # getter for: Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader;->mExitTasksEarly:Z
    invoke-static {v1}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader;->access$3200(Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1347
    :cond_0
    const-string v1, "MusicUiDeviceTab"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onPostExecute "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " but canceled"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1348
    const/4 p1, 0x0

    .line 1351
    :cond_1
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader$TextLoaderTask;->getAttachedTextView()Landroid/widget/TextView;

    move-result-object v0

    .line 1352
    .local v0, "text":Landroid/widget/TextView;
    if-eqz p1, :cond_2

    if-eqz v0, :cond_2

    .line 1353
    iget-object v1, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader$TextLoaderTask;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f000a

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1356
    :cond_2
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 1265
    check-cast p1, Ljava/lang/Integer;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader$TextLoaderTask;->onPostExecute(Ljava/lang/Integer;)V

    return-void
.end method
