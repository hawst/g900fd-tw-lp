.class Lcom/samsung/musicplus/contents/search/QueryBrowserFragment$ContentsChangeHandler;
.super Landroid/os/Handler;
.source "QueryBrowserFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ContentsChangeHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;


# direct methods
.method private constructor <init>(Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;)V
    .locals 0

    .prologue
    .line 572
    iput-object p1, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment$ContentsChangeHandler;->this$0:Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;Lcom/samsung/musicplus/contents/search/QueryBrowserFragment$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;
    .param p2, "x1"    # Lcom/samsung/musicplus/contents/search/QueryBrowserFragment$1;

    .prologue
    .line 572
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment$ContentsChangeHandler;-><init>(Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 576
    iget-object v0, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment$ContentsChangeHandler;->this$0:Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;

    # getter for: Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->mIsStart:Z
    invoke-static {v0}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->access$1100(Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 577
    iget-object v0, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment$ContentsChangeHandler;->this$0:Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;

    # invokes: Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->restartLoader()V
    invoke-static {v0}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->access$100(Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;)V

    .line 581
    :goto_0
    return-void

    .line 579
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment$ContentsChangeHandler;->this$0:Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;

    const/4 v1, 0x1

    # setter for: Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->mIsContentChanged:Z
    invoke-static {v0, v1}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->access$1202(Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;Z)Z

    goto :goto_0
.end method
