.class Lcom/samsung/musicplus/contents/extra/SelectionListFragment$5;
.super Ljava/lang/Object;
.source "SelectionListFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->setHeaderView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/contents/extra/SelectionListFragment;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/contents/extra/SelectionListFragment;)V
    .locals 0

    .prologue
    .line 538
    iput-object p1, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$5;->this$0:Lcom/samsung/musicplus/contents/extra/SelectionListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 541
    iget-object v0, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$5;->this$0:Lcom/samsung/musicplus/contents/extra/SelectionListFragment;

    # getter for: Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mCheckBox:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->access$300(Lcom/samsung/musicplus/contents/extra/SelectionListFragment;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 542
    iget-object v0, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$5;->this$0:Lcom/samsung/musicplus/contents/extra/SelectionListFragment;

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->setSelectAll(Z)V

    .line 543
    iget-object v0, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$5;->this$0:Lcom/samsung/musicplus/contents/extra/SelectionListFragment;

    # getter for: Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mCheckBox:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->access$300(Lcom/samsung/musicplus/contents/extra/SelectionListFragment;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 548
    :goto_0
    iget-object v0, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$5;->this$0:Lcom/samsung/musicplus/contents/extra/SelectionListFragment;

    # invokes: Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->invalidateActionBarTitle()V
    invoke-static {v0}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->access$200(Lcom/samsung/musicplus/contents/extra/SelectionListFragment;)V

    .line 549
    const v0, 0x8000

    invoke-virtual {p1, v0}, Landroid/view/View;->sendAccessibilityEvent(I)V

    .line 550
    return-void

    .line 545
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$5;->this$0:Lcom/samsung/musicplus/contents/extra/SelectionListFragment;

    invoke-virtual {v0, v2}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->setSelectAll(Z)V

    .line 546
    iget-object v0, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$5;->this$0:Lcom/samsung/musicplus/contents/extra/SelectionListFragment;

    # getter for: Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->mCheckBox:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->access$300(Lcom/samsung/musicplus/contents/extra/SelectionListFragment;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0
.end method
