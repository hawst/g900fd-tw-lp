.class Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$9;
.super Ljava/lang/Object;
.source "DeviceListFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->showPopupDeregistered(JLjava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;

.field final synthetic val$deviceId:J


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;J)V
    .locals 0

    .prologue
    .line 1083
    iput-object p1, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$9;->this$1:Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;

    iput-wide p2, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$9;->val$deviceId:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 1086
    iget-object v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$9;->this$1:Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;

    # getter for: Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->access$1800(Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;)Landroid/app/ProgressDialog;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$9;->this$1:Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;

    # getter for: Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->access$1700(Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f100055

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 1088
    iget-object v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$9;->this$1:Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;

    # getter for: Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->access$1800(Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;)Landroid/app/ProgressDialog;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 1089
    iget-object v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$9;->this$1:Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;

    # getter for: Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->access$1800(Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;)Landroid/app/ProgressDialog;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 1090
    iget-object v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$9;->this$1:Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;

    # getter for: Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->access$1800(Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 1091
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$9$1;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$9$1;-><init>(Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$9;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 1105
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 1106
    return-void
.end method
