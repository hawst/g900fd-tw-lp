.class public Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;
.super Lcom/samsung/musicplus/MusicBaseActivity;
.source "MusicSelectListTabActivity.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;
.implements Landroid/widget/TabHost$OnTabChangeListener;


# static fields
.field private static final SAVED_TAB_INDEX:Ljava/lang/String; = "index"

.field private static final TABS:[I


# instance fields
.field private isRestoredInstanceState:Z

.field private mHeaderMode:I

.field private mLastTab:Lcom/samsung/musicplus/widget/tab/MusicTabManager$TabInfo;

.field private mListMode:I

.field private mOrientation:I

.field private mPlaylistId:J

.field private mPrevTab:Landroid/view/View;

.field private mSelectedIDList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field

.field private mTabHost:Landroid/widget/TabHost;

.field private final mTabs:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/musicplus/widget/tab/MusicTabManager$TabInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 70
    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->TABS:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x20001
        0x10002
        0x10003
        0x10007
    .end array-data
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 49
    invoke-direct {p0}, Lcom/samsung/musicplus/MusicBaseActivity;-><init>()V

    .line 62
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->mTabs:Ljava/util/HashMap;

    .line 66
    iput v1, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->mOrientation:I

    .line 68
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->mSelectedIDList:Ljava/util/ArrayList;

    .line 74
    iput-boolean v1, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->isRestoredInstanceState:Z

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->scrollSelector()V

    return-void
.end method

.method static synthetic access$100(Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;)Landroid/widget/TabHost;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->mTabHost:Landroid/widget/TabHost;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->changeFocus(Z)V

    return-void
.end method

.method private addTab(Landroid/widget/TabHost;III)V
    .locals 11
    .param p1, "tabHost"    # Landroid/widget/TabHost;
    .param p2, "list"    # I
    .param p3, "position"    # I
    .param p4, "total"    # I

    .prologue
    const/4 v10, 0x0

    .line 208
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v7

    const v8, 0x7f040090

    invoke-virtual {v7, v8, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 209
    .local v3, "tab":Landroid/view/View;
    const v7, 0x7f0d01bc

    invoke-virtual {v3, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 210
    .local v2, "label":Landroid/widget/TextView;
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v7}, Landroid/widget/TabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v7

    invoke-virtual {v7, v3}, Landroid/widget/TabHost$TabSpec;->setIndicator(Landroid/view/View;)Landroid/widget/TabHost$TabSpec;

    move-result-object v7

    new-instance v8, Lcom/samsung/musicplus/widget/tab/MusicTabManager$TabInfo$DummyTabFactory;

    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v9

    invoke-direct {v8, v9}, Lcom/samsung/musicplus/widget/tab/MusicTabManager$TabInfo$DummyTabFactory;-><init>(Landroid/content/Context;)V

    invoke-virtual {v7, v8}, Landroid/widget/TabHost$TabSpec;->setContent(Landroid/widget/TabHost$TabContentFactory;)Landroid/widget/TabHost$TabSpec;

    move-result-object v5

    .line 212
    .local v5, "tabSpec":Landroid/widget/TabHost$TabSpec;
    invoke-static {p2}, Lcom/samsung/musicplus/util/ListUtils;->getDefaultTabName(I)I

    move-result v7

    invoke-virtual {p0, v7}, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    .line 214
    .local v4, "tabName":Ljava/lang/CharSequence;
    invoke-direct {p0, v2, v4, p3, p4}, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->setTabView(Landroid/widget/TextView;Ljava/lang/CharSequence;II)V

    .line 216
    invoke-virtual {v5}, Landroid/widget/TabHost$TabSpec;->getTag()Ljava/lang/String;

    move-result-object v6

    .line 217
    .local v6, "tag":Ljava/lang/String;
    new-instance v1, Lcom/samsung/musicplus/widget/tab/MusicTabManager$TabInfo;

    const-class v7, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;

    invoke-direct {v1, v6, v7, v10}, Lcom/samsung/musicplus/widget/tab/MusicTabManager$TabInfo;-><init>(Ljava/lang/String;Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 222
    .local v1, "info":Lcom/samsung/musicplus/widget/tab/MusicTabManager$TabInfo;
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v7

    invoke-virtual {v7, v6}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v7

    iput-object v7, v1, Lcom/samsung/musicplus/widget/tab/MusicTabManager$TabInfo;->fragment:Landroid/app/Fragment;

    .line 223
    iget-object v7, v1, Lcom/samsung/musicplus/widget/tab/MusicTabManager$TabInfo;->fragment:Landroid/app/Fragment;

    if-eqz v7, :cond_0

    iget-object v7, v1, Lcom/samsung/musicplus/widget/tab/MusicTabManager$TabInfo;->fragment:Landroid/app/Fragment;

    invoke-virtual {v7}, Landroid/app/Fragment;->isDetached()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 224
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 225
    .local v0, "ft":Landroid/app/FragmentTransaction;
    iget-object v7, v1, Lcom/samsung/musicplus/widget/tab/MusicTabManager$TabInfo;->fragment:Landroid/app/Fragment;

    invoke-virtual {v0, v7}, Landroid/app/FragmentTransaction;->detach(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 226
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 228
    .end local v0    # "ft":Landroid/app/FragmentTransaction;
    :cond_0
    iget-object v7, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->mTabs:Ljava/util/HashMap;

    invoke-virtual {v7, v6, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 229
    invoke-virtual {p1, v5}, Landroid/widget/TabHost;->addTab(Landroid/widget/TabHost$TabSpec;)V

    .line 230
    return-void
.end method

.method private addTabs([II)V
    .locals 6
    .param p1, "tabs"    # [I
    .param p2, "index"    # I

    .prologue
    const v3, 0x7fffffff

    .line 107
    if-ne p2, v3, :cond_0

    .line 108
    const/4 p2, 0x0

    .line 110
    :cond_0
    array-length v2, p1

    .line 116
    .local v2, "length":I
    invoke-direct {p0, v3}, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->resetCurrnetTab(I)Z

    move-result v0

    .line 117
    .local v0, "doTrick":Z
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_2

    .line 118
    iget-object v3, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->mTabHost:Landroid/widget/TabHost;

    aget v4, p1, v1

    add-int/lit8 v5, v1, 0x1

    invoke-direct {p0, v3, v4, v5, v2}, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->addTab(Landroid/widget/TabHost;III)V

    .line 119
    iget-object v3, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v3}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, p0}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 120
    if-eqz v0, :cond_1

    if-ne p2, v1, :cond_1

    .line 123
    const/4 v3, -0x1

    invoke-direct {p0, v3}, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->resetCurrnetTab(I)Z

    .line 124
    invoke-direct {p0, p2}, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->setCurrentTab(I)V

    .line 117
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 127
    :cond_2
    if-nez v0, :cond_3

    .line 129
    invoke-direct {p0, p2}, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->setCurrentTab(I)V

    .line 131
    :cond_3
    iget-object v3, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->mTabHost:Landroid/widget/TabHost;

    new-instance v4, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity$1;

    invoke-direct {v4, p0}, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity$1;-><init>(Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;)V

    invoke-virtual {v3, v4}, Landroid/widget/TabHost;->post(Ljava/lang/Runnable;)Z

    .line 138
    return-void
.end method

.method private changeFocus(Z)V
    .locals 2
    .param p1, "focusable"    # Z

    .prologue
    .line 343
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v1, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->TABS:[I

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 344
    iget-object v1, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v1}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/view/View;->setFocusable(Z)V

    .line 343
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 346
    :cond_0
    return-void
.end method

.method private resetCurrnetTab(I)Z
    .locals 6
    .param p1, "value"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 150
    :try_start_0
    iget-object v4, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    const-string v5, "mCurrentTab"

    invoke-virtual {v4, v5}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 151
    .local v1, "f":Ljava/lang/reflect/Field;
    const/4 v4, 0x1

    invoke-virtual {v1, v4}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 152
    iget-object v4, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v1, v4, p1}, Ljava/lang/reflect/Field;->setInt(Ljava/lang/Object;I)V
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2

    .line 163
    .end local v1    # "f":Ljava/lang/reflect/Field;
    :goto_0
    return v2

    .line 153
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/lang/NoSuchFieldException;
    move v2, v3

    .line 155
    goto :goto_0

    .line 156
    .end local v0    # "e":Ljava/lang/NoSuchFieldException;
    :catch_1
    move-exception v0

    .local v0, "e":Ljava/lang/IllegalArgumentException;
    move v2, v3

    .line 158
    goto :goto_0

    .line 159
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v0

    .local v0, "e":Ljava/lang/IllegalAccessException;
    move v2, v3

    .line 161
    goto :goto_0
.end method

.method private scrollSelector()V
    .locals 9

    .prologue
    .line 323
    const v7, 0x7f0d0103

    invoke-virtual {p0, v7}, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/HorizontalScrollView;

    .line 324
    .local v2, "scrollView":Landroid/widget/HorizontalScrollView;
    invoke-virtual {v2}, Landroid/widget/HorizontalScrollView;->getWidth()I

    move-result v5

    .line 325
    .local v5, "w":I
    invoke-virtual {v2}, Landroid/widget/HorizontalScrollView;->getScrollX()I

    move-result v3

    .line 326
    .local v3, "scrollX":I
    iget-object v7, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v7}, Landroid/widget/TabHost;->getCurrentTabView()Landroid/view/View;

    move-result-object v4

    .line 327
    .local v4, "v":Landroid/view/View;
    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    move-result v7

    sub-int/2addr v7, v3

    if-ltz v7, :cond_0

    invoke-virtual {v4}, Landroid/view/View;->getRight()I

    move-result v7

    sub-int/2addr v7, v3

    if-le v7, v5, :cond_1

    .line 329
    :cond_0
    div-int/lit8 v1, v5, 0x2

    .line 330
    .local v1, "containerCenter":I
    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    move-result v7

    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v8

    div-int/lit8 v8, v8, 0x2

    add-int v0, v7, v8

    .line 331
    .local v0, "buttonCenter":I
    sub-int v6, v0, v1

    .line 332
    .local v6, "x":I
    invoke-virtual {v2}, Landroid/widget/HorizontalScrollView;->getScrollY()I

    move-result v7

    invoke-virtual {v2, v6, v7}, Landroid/widget/HorizontalScrollView;->scrollTo(II)V

    .line 334
    .end local v0    # "buttonCenter":I
    .end local v1    # "containerCenter":I
    .end local v6    # "x":I
    :cond_1
    return-void
.end method

.method private setCurrentTab(I)V
    .locals 4
    .param p1, "index"    # I

    .prologue
    .line 167
    iget-object v0, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v0, p1}, Landroid/widget/TabHost;->setCurrentTab(I)V

    .line 170
    iget-object v0, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->mTabHost:Landroid/widget/TabHost;

    new-instance v1, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity$2;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity$2;-><init>(Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;)V

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/TabHost;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 178
    return-void
.end method

.method private setTabView(Landroid/widget/TextView;Ljava/lang/CharSequence;II)V
    .locals 5
    .param p1, "label"    # Landroid/widget/TextView;
    .param p2, "tabName"    # Ljava/lang/CharSequence;
    .param p3, "position"    # I
    .param p4, "total"    # I

    .prologue
    const/4 v4, 0x0

    .line 241
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 242
    const v0, 0x7f1001a7

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    const/4 v2, 0x1

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 243
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1, p2, v4}, Lcom/samsung/musicplus/util/UiUtils;->setTabTts(Landroid/content/Context;Landroid/view/View;Ljava/lang/CharSequence;Z)V

    .line 244
    return-void
.end method


# virtual methods
.method public getSelectedIDList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    .line 379
    iget-object v0, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->mSelectedIDList:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getSelectedItemsCount()I
    .locals 1

    .prologue
    .line 364
    iget-object v0, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->mSelectedIDList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 365
    const/4 v0, 0x0

    .line 367
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->mSelectedIDList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public isItemSelected(Ljava/lang/String;)Z
    .locals 2
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 371
    iget-object v0, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->mSelectedIDList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->mSelectedIDList:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    if-gez v0, :cond_1

    .line 372
    :cond_0
    const/4 v0, 0x0

    .line 375
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 182
    invoke-super {p0, p1}, Lcom/samsung/musicplus/MusicBaseActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 184
    iget v0, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->mOrientation:I

    iget v1, p1, Landroid/content/res/Configuration;->orientation:I

    if-eq v0, v1, :cond_0

    .line 185
    iget-object v0, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->mLastTab:Lcom/samsung/musicplus/widget/tab/MusicTabManager$TabInfo;

    iget-object v0, v0, Lcom/samsung/musicplus/widget/tab/MusicTabManager$TabInfo;->fragment:Landroid/app/Fragment;

    instance-of v0, v0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;

    if-eqz v0, :cond_0

    .line 186
    iget-object v0, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->mLastTab:Lcom/samsung/musicplus/widget/tab/MusicTabManager$TabInfo;

    iget-object v0, v0, Lcom/samsung/musicplus/widget/tab/MusicTabManager$TabInfo;->fragment:Landroid/app/Fragment;

    check-cast v0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;

    invoke-virtual {v0}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->updateFragment()V

    .line 189
    :cond_0
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity$3;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity$3;-><init>(Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;)V

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 197
    iget v0, p1, Landroid/content/res/Configuration;->orientation:I

    iput v0, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->mOrientation:I

    .line 198
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x0

    .line 78
    invoke-super {p0, p1}, Lcom/samsung/musicplus/MusicBaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 79
    const v2, 0x7f04006d

    invoke-virtual {p0, v2}, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->setContentView(I)V

    .line 80
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 82
    .local v0, "data":Landroid/os/Bundle;
    const-string v2, "playlist_id"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->mPlaylistId:J

    .line 83
    const-string v2, "list_mode"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->mListMode:I

    .line 84
    const-string v2, "header_mode"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->mHeaderMode:I

    .line 86
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->isRestoredInstanceState:Z

    .line 88
    const v2, 0x1020012

    invoke-virtual {p0, v2}, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TabHost;

    iput-object v2, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->mTabHost:Landroid/widget/TabHost;

    .line 89
    iget-object v2, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v2}, Landroid/widget/TabHost;->setup()V

    .line 90
    iget-object v2, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v2, p0}, Landroid/widget/TabHost;->setOnTabChangedListener(Landroid/widget/TabHost$OnTabChangeListener;)V

    .line 92
    if-eqz p1, :cond_0

    .line 93
    const-string v2, "index"

    invoke-virtual {p1, v2, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 94
    .local v1, "index":I
    sget-object v2, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->TABS:[I

    invoke-direct {p0, v2, v1}, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->addTabs([II)V

    .line 98
    .end local v1    # "index":I
    :goto_0
    return-void

    .line 96
    :cond_0
    sget-object v2, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->TABS:[I

    invoke-direct {p0, v2, v4}, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->addTabs([II)V

    goto :goto_0
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "arg1"    # Z

    .prologue
    .line 340
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 102
    invoke-super {p0}, Lcom/samsung/musicplus/MusicBaseActivity;->onResume()V

    .line 103
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->isRestoredInstanceState:Z

    .line 104
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 202
    const-string v0, "index"

    iget-object v1, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v1}, Landroid/widget/TabHost;->getCurrentTab()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 203
    invoke-super {p0, p1}, Lcom/samsung/musicplus/MusicBaseActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 204
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->isRestoredInstanceState:Z

    .line 205
    return-void
.end method

.method public onTabChanged(Ljava/lang/String;)V
    .locals 12
    .param p1, "tabId"    # Ljava/lang/String;

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x1

    const/4 v7, 0x0

    .line 252
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->isDestroyed()Z

    move-result v5

    if-nez v5, :cond_0

    iget-boolean v5, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->isRestoredInstanceState:Z

    if-nez v5, :cond_1

    .line 317
    :cond_0
    :goto_0
    return-void

    .line 256
    :cond_1
    iget-object v5, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->mTabs:Ljava/util/HashMap;

    invoke-virtual {v5, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/musicplus/widget/tab/MusicTabManager$TabInfo;

    .line 257
    .local v3, "newTab":Lcom/samsung/musicplus/widget/tab/MusicTabManager$TabInfo;
    iget-object v5, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->mLastTab:Lcom/samsung/musicplus/widget/tab/MusicTabManager$TabInfo;

    if-eq v5, v3, :cond_6

    .line 258
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 259
    .local v1, "ft":Landroid/app/FragmentTransaction;
    iget-object v5, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->mLastTab:Lcom/samsung/musicplus/widget/tab/MusicTabManager$TabInfo;

    if-eqz v5, :cond_4

    .line 260
    iget-object v5, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->mLastTab:Lcom/samsung/musicplus/widget/tab/MusicTabManager$TabInfo;

    iget-object v5, v5, Lcom/samsung/musicplus/widget/tab/MusicTabManager$TabInfo;->tag:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 261
    .local v2, "list":I
    iget-object v5, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->mLastTab:Lcom/samsung/musicplus/widget/tab/MusicTabManager$TabInfo;

    iget-object v5, v5, Lcom/samsung/musicplus/widget/tab/MusicTabManager$TabInfo;->fragment:Landroid/app/Fragment;

    if-eqz v5, :cond_2

    .line 262
    iget-object v5, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->mLastTab:Lcom/samsung/musicplus/widget/tab/MusicTabManager$TabInfo;

    iget-object v5, v5, Lcom/samsung/musicplus/widget/tab/MusicTabManager$TabInfo;->fragment:Landroid/app/Fragment;

    invoke-virtual {v1, v5}, Landroid/app/FragmentTransaction;->detach(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 264
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->mPrevTab:Landroid/view/View;

    invoke-static {v5, v6, v11, v7}, Lcom/samsung/musicplus/util/UiUtils;->setTabTts(Landroid/content/Context;Landroid/view/View;Ljava/lang/CharSequence;Z)V

    .line 267
    :cond_2
    const v5, 0x20001

    if-eq v2, v5, :cond_3

    .line 268
    invoke-static {v2}, Lcom/samsung/musicplus/util/ListUtils;->getSubTrackList(I)I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    .line 269
    .local v4, "subList":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 270
    .local v0, "fg":Landroid/app/Fragment;
    instance-of v5, v0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;

    if-eqz v5, :cond_3

    .line 273
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v5

    const/4 v6, 0x1

    invoke-virtual {v5, v4, v6}, Landroid/app/FragmentManager;->popBackStackImmediate(Ljava/lang/String;I)Z
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 287
    .end local v0    # "fg":Landroid/app/Fragment;
    .end local v4    # "subList":Ljava/lang/String;
    :cond_3
    :goto_1
    invoke-direct {p0, v7}, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->changeFocus(Z)V

    .line 290
    .end local v2    # "list":I
    :cond_4
    if-eqz v3, :cond_5

    .line 291
    iget-object v5, v3, Lcom/samsung/musicplus/widget/tab/MusicTabManager$TabInfo;->tag:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 292
    .restart local v2    # "list":I
    iget-object v5, v3, Lcom/samsung/musicplus/widget/tab/MusicTabManager$TabInfo;->fragment:Landroid/app/Fragment;

    if-nez v5, :cond_7

    .line 293
    iget v5, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->mListMode:I

    iget v6, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->mHeaderMode:I

    iget-wide v8, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->mPlaylistId:J

    invoke-static {v2, v5, v6, v8, v9}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->getNewInstance(IIIJ)Landroid/app/Fragment;

    move-result-object v5

    iput-object v5, v3, Lcom/samsung/musicplus/widget/tab/MusicTabManager$TabInfo;->fragment:Landroid/app/Fragment;

    .line 295
    const v5, 0x1020011

    iget-object v6, v3, Lcom/samsung/musicplus/widget/tab/MusicTabManager$TabInfo;->fragment:Landroid/app/Fragment;

    iget-object v7, v3, Lcom/samsung/musicplus/widget/tab/MusicTabManager$TabInfo;->tag:Ljava/lang/String;

    invoke-virtual {v1, v5, v6, v7}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 299
    :goto_2
    iget-object v5, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v5}, Landroid/widget/TabHost;->getCurrentTabView()Landroid/view/View;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->mPrevTab:Landroid/view/View;

    .line 302
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v6}, Landroid/widget/TabHost;->getCurrentTabView()Landroid/view/View;

    move-result-object v6

    invoke-static {v5, v6, v11, v10}, Lcom/samsung/musicplus/util/UiUtils;->setTabTts(Landroid/content/Context;Landroid/view/View;Ljava/lang/CharSequence;Z)V

    .line 305
    .end local v2    # "list":I
    :cond_5
    iput-object v3, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->mLastTab:Lcom/samsung/musicplus/widget/tab/MusicTabManager$TabInfo;

    .line 306
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commit()I

    .line 308
    .end local v1    # "ft":Landroid/app/FragmentTransaction;
    :cond_6
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->scrollSelector()V

    .line 309
    new-instance v5, Landroid/os/Handler;

    invoke-direct {v5}, Landroid/os/Handler;-><init>()V

    new-instance v6, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity$4;

    invoke-direct {v6, p0}, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity$4;-><init>(Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;)V

    const-wide/16 v8, 0x12c

    invoke-virtual {v5, v6, v8, v9}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 297
    .restart local v1    # "ft":Landroid/app/FragmentTransaction;
    .restart local v2    # "list":I
    :cond_7
    iget-object v5, v3, Lcom/samsung/musicplus/widget/tab/MusicTabManager$TabInfo;->fragment:Landroid/app/Fragment;

    invoke-virtual {v1, v5}, Landroid/app/FragmentTransaction;->attach(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    goto :goto_2

    .line 275
    .restart local v0    # "fg":Landroid/app/Fragment;
    .restart local v4    # "subList":Ljava/lang/String;
    :catch_0
    move-exception v5

    goto :goto_1
.end method

.method public updateSelectedIDs(ZJ)V
    .locals 2
    .param p1, "checked"    # Z
    .param p2, "value"    # J

    .prologue
    .line 349
    if-eqz p1, :cond_2

    .line 350
    iget-object v0, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->mSelectedIDList:Ljava/util/ArrayList;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    if-ltz v0, :cond_1

    .line 361
    :cond_0
    :goto_0
    return-void

    .line 353
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->mSelectedIDList:Ljava/util/ArrayList;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 355
    :cond_2
    iget-object v0, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->mSelectedIDList:Ljava/util/ArrayList;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    if-ltz v0, :cond_0

    .line 358
    iget-object v0, p0, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->mSelectedIDList:Ljava/util/ArrayList;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method
