.class Lcom/samsung/musicplus/contents/normal/RecordingsListFragment$RecordingsTrackAdapter;
.super Lcom/samsung/musicplus/contents/TrackListNumberAdapter;
.source "RecordingsListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/contents/normal/RecordingsListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "RecordingsTrackAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/contents/normal/RecordingsListFragment;


# direct methods
.method public constructor <init>(Lcom/samsung/musicplus/contents/normal/RecordingsListFragment;Landroid/content/Context;ILandroid/database/Cursor;I)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "layout"    # I
    .param p4, "c"    # Landroid/database/Cursor;
    .param p5, "flags"    # I

    .prologue
    .line 72
    iput-object p1, p0, Lcom/samsung/musicplus/contents/normal/RecordingsListFragment$RecordingsTrackAdapter;->this$0:Lcom/samsung/musicplus/contents/normal/RecordingsListFragment;

    .line 73
    invoke-direct {p0, p2, p3, p4, p5}, Lcom/samsung/musicplus/contents/TrackListNumberAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;I)V

    .line 74
    return-void
.end method


# virtual methods
.method protected bindTextView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "c"    # Landroid/database/Cursor;

    .prologue
    .line 87
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/contents/TrackListNumberAdapter$TrackNumberViewHolder;

    .line 88
    .local v0, "vh":Lcom/samsung/musicplus/contents/TrackListNumberAdapter$TrackNumberViewHolder;
    iget-object v1, v0, Lcom/samsung/musicplus/contents/TrackListNumberAdapter$TrackNumberViewHolder;->text1:Landroid/widget/TextView;

    iget v2, p0, Lcom/samsung/musicplus/contents/normal/RecordingsListFragment$RecordingsTrackAdapter;->mText1Index:I

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {p2, v2}, Lcom/samsung/musicplus/util/UiUtils;->transUnknownString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 89
    iget v1, p0, Lcom/samsung/musicplus/contents/normal/RecordingsListFragment$RecordingsTrackAdapter;->mText2Index:I

    if-lez v1, :cond_0

    iget v1, p0, Lcom/samsung/musicplus/contents/normal/RecordingsListFragment$RecordingsTrackAdapter;->mDurationIndex:I

    if-lez v1, :cond_0

    .line 90
    iget-object v1, v0, Lcom/samsung/musicplus/contents/TrackListNumberAdapter$TrackNumberViewHolder;->text2:Landroid/widget/TextView;

    iget v2, p0, Lcom/samsung/musicplus/contents/normal/RecordingsListFragment$RecordingsTrackAdapter;->mText2Index:I

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iget v3, p0, Lcom/samsung/musicplus/contents/normal/RecordingsListFragment$RecordingsTrackAdapter;->mDurationIndex:I

    invoke-interface {p3, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    div-int/lit16 v3, v3, 0x3e8

    invoke-static {p2, v2, v3}, Lcom/samsung/musicplus/util/UiUtils;->recordingInfoLabel(Landroid/content/Context;II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 92
    :cond_0
    return-void
.end method

.method protected newTextView(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 78
    invoke-super {p0, p1}, Lcom/samsung/musicplus/contents/TrackListNumberAdapter;->newTextView(Landroid/view/View;)V

    .line 79
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/contents/TrackListNumberAdapter$TrackNumberViewHolder;

    .line 80
    .local v0, "vh":Lcom/samsung/musicplus/contents/TrackListNumberAdapter$TrackNumberViewHolder;
    const v1, 0x7f0d009f

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v0, Lcom/samsung/musicplus/contents/TrackListNumberAdapter$TrackNumberViewHolder;->text1:Landroid/widget/TextView;

    .line 81
    const v1, 0x7f0d00a0

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v0, Lcom/samsung/musicplus/contents/TrackListNumberAdapter$TrackNumberViewHolder;->text2:Landroid/widget/TextView;

    .line 82
    invoke-virtual {p1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 83
    return-void
.end method
