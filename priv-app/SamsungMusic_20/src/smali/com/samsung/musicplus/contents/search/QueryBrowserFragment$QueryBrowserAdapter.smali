.class Lcom/samsung/musicplus/contents/search/QueryBrowserFragment$QueryBrowserAdapter;
.super Lcom/samsung/musicplus/widget/list/CommonListAdapter;
.source "QueryBrowserFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "QueryBrowserAdapter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/contents/search/QueryBrowserFragment$QueryBrowserAdapter$QueryBrowserViewHolder;
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;


# direct methods
.method public constructor <init>(Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;Landroid/content/Context;ILandroid/database/Cursor;I)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "layout"    # I
    .param p4, "c"    # Landroid/database/Cursor;
    .param p5, "flags"    # I

    .prologue
    .line 440
    iput-object p1, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment$QueryBrowserAdapter;->this$0:Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;

    .line 441
    invoke-direct {p0, p2, p3, p4, p5}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;I)V

    .line 442
    return-void
.end method


# virtual methods
.method protected bindOtherView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 8
    .param p1, "v"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "c"    # Landroid/database/Cursor;

    .prologue
    const/16 v7, 0x8

    .line 512
    iget-object v5, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment$QueryBrowserAdapter;->this$0:Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;

    # invokes: Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->getMimeType(Landroid/database/Cursor;)Ljava/lang/String;
    invoke-static {v5, p3}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->access$300(Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    .line 515
    .local v1, "mimetype":Ljava/lang/String;
    # getter for: Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->sSavedMimeTypeIndex:Landroid/util/SparseIntArray;
    invoke-static {}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->access$900()Landroid/util/SparseIntArray;

    move-result-object v5

    invoke-interface {p3}, Landroid/database/Cursor;->getPosition()I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/util/SparseIntArray;->get(I)I

    move-result v2

    .line 517
    .local v2, "savedMimeTypeNum":I
    iget-object v5, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment$QueryBrowserAdapter;->this$0:Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;

    # invokes: Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->getMimeTypeNum(Ljava/lang/String;)I
    invoke-static {v5, v1}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->access$1000(Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;Ljava/lang/String;)I

    move-result v0

    .line 519
    .local v0, "mimeTypeNum":I
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment$QueryBrowserAdapter$QueryBrowserViewHolder;

    .line 520
    .local v3, "vh":Lcom/samsung/musicplus/contents/search/QueryBrowserFragment$QueryBrowserAdapter$QueryBrowserViewHolder;
    if-ne v2, v0, :cond_1

    .line 521
    iget-object v5, v3, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment$QueryBrowserAdapter$QueryBrowserViewHolder;->titleLayout:Landroid/view/View;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/view/View;->setVisibility(I)V

    .line 522
    packed-switch v0, :pswitch_data_0

    .line 542
    :goto_0
    :pswitch_0
    const/4 v4, 0x0

    .line 543
    .local v4, "visibility":I
    invoke-interface {p3}, Landroid/database/Cursor;->getPosition()I

    move-result v5

    if-nez v5, :cond_0

    .line 544
    const/16 v4, 0x8

    .line 546
    :cond_0
    iget-object v5, v3, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment$QueryBrowserAdapter$QueryBrowserViewHolder;->titleDivider:Landroid/view/View;

    invoke-virtual {v5, v4}, Landroid/view/View;->setVisibility(I)V

    .line 547
    return-void

    .line 524
    .end local v4    # "visibility":I
    :pswitch_1
    iget-object v5, v3, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment$QueryBrowserAdapter$QueryBrowserViewHolder;->titleText:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment$QueryBrowserAdapter;->this$0:Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;

    const v7, 0x7f100027

    invoke-virtual {v6, v7}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 527
    :pswitch_2
    iget-object v5, v3, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment$QueryBrowserAdapter$QueryBrowserViewHolder;->titleText:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment$QueryBrowserAdapter;->this$0:Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;

    const v7, 0x7f10001f

    invoke-virtual {v6, v7}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 530
    :pswitch_3
    iget-object v5, v3, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment$QueryBrowserAdapter$QueryBrowserViewHolder;->titleText:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment$QueryBrowserAdapter;->this$0:Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;

    const v7, 0x7f10017d

    invoke-virtual {v6, v7}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 533
    :pswitch_4
    iget-object v5, v3, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment$QueryBrowserAdapter$QueryBrowserViewHolder;->titleLayout:Landroid/view/View;

    invoke-virtual {v5, v7}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 539
    :cond_1
    iget-object v5, v3, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment$QueryBrowserAdapter$QueryBrowserViewHolder;->titleLayout:Landroid/view/View;

    invoke-virtual {v5, v7}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 522
    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_4
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected bindTextView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 15
    .param p1, "v"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "c"    # Landroid/database/Cursor;

    .prologue
    .line 469
    iget-object v12, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment$QueryBrowserAdapter;->this$0:Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;

    invoke-virtual {v12}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->getQueryText()Ljava/lang/String;

    move-result-object v7

    .line 470
    .local v7, "queryText":Ljava/lang/String;
    iget-object v12, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment$QueryBrowserAdapter;->this$0:Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;

    move-object/from16 v0, p3

    # invokes: Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->getMimeType(Landroid/database/Cursor;)Ljava/lang/String;
    invoke-static {v12, v0}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->access$300(Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v3

    .line 472
    .local v3, "mimetype":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment$QueryBrowserAdapter$QueryBrowserViewHolder;

    .line 473
    .local v11, "vh":Lcom/samsung/musicplus/contents/search/QueryBrowserFragment$QueryBrowserAdapter$QueryBrowserViewHolder;
    iget-object v9, v11, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment$QueryBrowserAdapter$QueryBrowserViewHolder;->text1:Landroid/widget/TextView;

    check-cast v9, Lcom/samsung/musicplus/widget/MatchedTextView;

    .line 474
    .local v9, "tv1":Lcom/samsung/musicplus/widget/MatchedTextView;
    iget-object v10, v11, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment$QueryBrowserAdapter$QueryBrowserViewHolder;->text2:Landroid/widget/TextView;

    check-cast v10, Lcom/samsung/musicplus/widget/MatchedTextView;

    .line 476
    .local v10, "tv2":Lcom/samsung/musicplus/widget/MatchedTextView;
    const-string v12, "artist"

    invoke-virtual {v12, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_1

    .line 477
    iget-object v12, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment$QueryBrowserAdapter;->this$0:Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    # invokes: Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->getArtistName(Landroid/content/Context;Landroid/database/Cursor;)Ljava/lang/String;
    invoke-static {v12, v0, v1}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->access$400(Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;Landroid/content/Context;Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v2

    .line 478
    .local v2, "displayname":Ljava/lang/String;
    invoke-virtual {v9}, Lcom/samsung/musicplus/widget/MatchedTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v12

    invoke-static {v9, v12, v2, v7}, Lcom/samsung/musicplus/util/UiUtils;->setTextItem(Lcom/samsung/musicplus/widget/MatchedTextView;Landroid/text/TextPaint;Ljava/lang/String;Ljava/lang/String;)V

    .line 480
    const-string v12, "data1"

    move-object/from16 v0, p3

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v12

    move-object/from16 v0, p3

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    .line 481
    .local v5, "numalbums":I
    const-string v12, "data2"

    move-object/from16 v0, p3

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v12

    move-object/from16 v0, p3

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    .line 482
    .local v6, "numsongs":I
    iget-object v12, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment$QueryBrowserAdapter;->this$0:Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;

    # getter for: Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->mIsUnknown:Z
    invoke-static {v12}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->access$500(Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;)Z

    move-result v12

    move-object/from16 v0, p2

    invoke-static {v0, v5, v6, v12}, Lcom/samsung/musicplus/util/UiUtils;->makeAlbumsSongsLabel(Landroid/content/Context;IIZ)Ljava/lang/String;

    move-result-object v8

    .line 484
    .local v8, "songs_albums":Ljava/lang/String;
    invoke-virtual {v10, v8}, Lcom/samsung/musicplus/widget/MatchedTextView;->setText(Ljava/lang/CharSequence;)V

    .line 501
    .end local v2    # "displayname":Ljava/lang/String;
    .end local v5    # "numalbums":I
    .end local v6    # "numsongs":I
    .end local v8    # "songs_albums":Ljava/lang/String;
    :cond_0
    :goto_0
    return-void

    .line 486
    :cond_1
    const-string v12, "album"

    invoke-virtual {v12, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_2

    .line 487
    iget-object v12, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment$QueryBrowserAdapter;->this$0:Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    # invokes: Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->getAlbumName(Landroid/content/Context;Landroid/database/Cursor;)Ljava/lang/String;
    invoke-static {v12, v0, v1}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->access$600(Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;Landroid/content/Context;Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v2

    .line 488
    .restart local v2    # "displayname":Ljava/lang/String;
    invoke-virtual {v9}, Lcom/samsung/musicplus/widget/MatchedTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v12

    invoke-static {v9, v12, v2, v7}, Lcom/samsung/musicplus/util/UiUtils;->setTextItem(Lcom/samsung/musicplus/widget/MatchedTextView;Landroid/text/TextPaint;Ljava/lang/String;Ljava/lang/String;)V

    .line 490
    iget-object v12, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment$QueryBrowserAdapter;->this$0:Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    # invokes: Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->getArtistName(Landroid/content/Context;Landroid/database/Cursor;)Ljava/lang/String;
    invoke-static {v12, v0, v1}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->access$400(Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;Landroid/content/Context;Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v2

    .line 491
    invoke-virtual {v10}, Lcom/samsung/musicplus/widget/MatchedTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v12

    invoke-static {v10, v12, v2, v7}, Lcom/samsung/musicplus/util/UiUtils;->setTextItem(Lcom/samsung/musicplus/widget/MatchedTextView;Landroid/text/TextPaint;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 493
    .end local v2    # "displayname":Ljava/lang/String;
    :cond_2
    iget-object v12, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment$QueryBrowserAdapter;->this$0:Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;

    # invokes: Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->isTrackType(Ljava/lang/String;)Z
    invoke-static {v12, v3}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->access$700(Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;Ljava/lang/String;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 494
    iget-object v12, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment$QueryBrowserAdapter;->this$0:Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    # invokes: Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->getSongName(Landroid/content/Context;Landroid/database/Cursor;)Ljava/lang/String;
    invoke-static {v12, v0, v1}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->access$800(Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;Landroid/content/Context;Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v4

    .line 495
    .local v4, "name":Ljava/lang/String;
    invoke-virtual {v9}, Lcom/samsung/musicplus/widget/MatchedTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v12

    invoke-static {v9, v12, v4, v7}, Lcom/samsung/musicplus/util/UiUtils;->setTextItem(Lcom/samsung/musicplus/widget/MatchedTextView;Landroid/text/TextPaint;Ljava/lang/String;Ljava/lang/String;)V

    .line 497
    iget-object v12, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment$QueryBrowserAdapter;->this$0:Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    # invokes: Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->getArtistName(Landroid/content/Context;Landroid/database/Cursor;)Ljava/lang/String;
    invoke-static {v12, v0, v1}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->access$400(Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;Landroid/content/Context;Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v2

    .line 498
    .restart local v2    # "displayname":Ljava/lang/String;
    iget-object v12, p0, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment$QueryBrowserAdapter;->this$0:Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    # invokes: Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->getAlbumName(Landroid/content/Context;Landroid/database/Cursor;)Ljava/lang/String;
    invoke-static {v12, v0, v1}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;->access$600(Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;Landroid/content/Context;Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v4

    .line 499
    invoke-virtual {v10}, Lcom/samsung/musicplus/widget/MatchedTextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v12

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    const-string v14, " - "

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-static {v10, v12, v13, v7}, Lcom/samsung/musicplus/util/UiUtils;->setTextItem(Lcom/samsung/musicplus/widget/MatchedTextView;Landroid/text/TextPaint;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected bindThumbnailView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "c"    # Landroid/database/Cursor;

    .prologue
    .line 505
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment$QueryBrowserAdapter$QueryBrowserViewHolder;

    .line 506
    .local v1, "vh":Lcom/samsung/musicplus/contents/search/QueryBrowserFragment$QueryBrowserAdapter$QueryBrowserViewHolder;
    iget-object v0, v1, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment$QueryBrowserAdapter$QueryBrowserViewHolder;->thumbnail:Landroid/widget/ImageView;

    .line 507
    .local v0, "iv":Landroid/widget/ImageView;
    invoke-virtual {p0, p3}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment$QueryBrowserAdapter;->getArtKey(Landroid/database/Cursor;)Ljava/lang/Object;

    move-result-object v2

    const v3, 0x7f02003b

    invoke-virtual {p0, p2, v0, v2, v3}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment$QueryBrowserAdapter;->setArtWorkView(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/Object;I)Z

    .line 508
    return-void
.end method

.method protected getArtKey(Landroid/database/Cursor;)Ljava/lang/Object;
    .locals 4
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 551
    const-string v3, "mime_type"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 553
    .local v2, "mimetype":Ljava/lang/String;
    const-wide/16 v0, -0x1

    .line 554
    .local v0, "artKey":J
    const-string v3, "album"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 555
    const-string v3, "_id"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 559
    :goto_0
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    return-object v3

    .line 557
    :cond_0
    const-string v3, "album_id"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    goto :goto_0
.end method

.method protected getViewHolder()Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;
    .locals 2

    .prologue
    .line 464
    new-instance v0, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment$QueryBrowserAdapter$QueryBrowserViewHolder;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment$QueryBrowserAdapter$QueryBrowserViewHolder;-><init>(Lcom/samsung/musicplus/contents/search/QueryBrowserFragment$QueryBrowserAdapter;Lcom/samsung/musicplus/contents/search/QueryBrowserFragment$1;)V

    return-object v0
.end method

.method protected newOtherView(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 446
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment$QueryBrowserAdapter$QueryBrowserViewHolder;

    .line 447
    .local v1, "vh":Lcom/samsung/musicplus/contents/search/QueryBrowserFragment$QueryBrowserAdapter$QueryBrowserViewHolder;
    const v2, 0x7f0d007e

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, v1, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment$QueryBrowserAdapter$QueryBrowserViewHolder;->titleLayout:Landroid/view/View;

    .line 448
    iget-object v2, v1, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment$QueryBrowserAdapter$QueryBrowserViewHolder;->titleLayout:Landroid/view/View;

    new-instance v3, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment$QueryBrowserAdapter$1;

    invoke-direct {v3, p0}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment$QueryBrowserAdapter$1;-><init>(Lcom/samsung/musicplus/contents/search/QueryBrowserFragment$QueryBrowserAdapter;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 455
    const v2, 0x7f0d007f

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, v1, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment$QueryBrowserAdapter$QueryBrowserViewHolder;->titleDivider:Landroid/view/View;

    .line 456
    iget-object v2, v1, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment$QueryBrowserAdapter$QueryBrowserViewHolder;->titleLayout:Landroid/view/View;

    const v3, 0x7f0d0081

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, v1, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment$QueryBrowserAdapter$QueryBrowserViewHolder;->titleText:Landroid/widget/TextView;

    .line 457
    const v2, 0x7f0d00bb

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 458
    .local v0, "insetDivider":Landroid/view/View;
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 459
    invoke-virtual {p1, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 460
    return-void
.end method
