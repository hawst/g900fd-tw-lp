.class Lcom/samsung/musicplus/contents/normal/ComposerListFragment$ComposerTabAdapter;
.super Lcom/samsung/musicplus/widget/list/TrackListAdapter;
.source "ComposerListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/contents/normal/ComposerListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ComposerTabAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/contents/normal/ComposerListFragment;


# direct methods
.method public constructor <init>(Lcom/samsung/musicplus/contents/normal/ComposerListFragment;Landroid/content/Context;ILandroid/database/Cursor;I)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "layout"    # I
    .param p4, "c"    # Landroid/database/Cursor;
    .param p5, "flags"    # I

    .prologue
    .line 62
    iput-object p1, p0, Lcom/samsung/musicplus/contents/normal/ComposerListFragment$ComposerTabAdapter;->this$0:Lcom/samsung/musicplus/contents/normal/ComposerListFragment;

    .line 63
    invoke-direct {p0, p2, p3, p4, p5}, Lcom/samsung/musicplus/widget/list/TrackListAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;I)V

    .line 64
    return-void
.end method


# virtual methods
.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "c"    # Landroid/database/Cursor;

    .prologue
    .line 76
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    .line 77
    invoke-virtual {p0, p1, p2, p3}, Lcom/samsung/musicplus/contents/normal/ComposerListFragment$ComposerTabAdapter;->hideBottomDivider(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    .line 78
    return-void
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 69
    iget-object v0, p0, Lcom/samsung/musicplus/contents/normal/ComposerListFragment$ComposerTabAdapter;->this$0:Lcom/samsung/musicplus/contents/normal/ComposerListFragment;

    # getter for: Lcom/samsung/musicplus/contents/normal/ComposerListFragment;->mList:I
    invoke-static {v0}, Lcom/samsung/musicplus/contents/normal/ComposerListFragment;->access$000(Lcom/samsung/musicplus/contents/normal/ComposerListFragment;)I

    move-result v0

    invoke-static {v0}, Lcom/samsung/musicplus/util/ListUtils;->isGridType(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 70
    add-int/lit8 v0, p1, -0x1

    invoke-super {p0, v0}, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    .line 72
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 81
    iget-object v0, p0, Lcom/samsung/musicplus/contents/normal/ComposerListFragment$ComposerTabAdapter;->this$0:Lcom/samsung/musicplus/contents/normal/ComposerListFragment;

    # getter for: Lcom/samsung/musicplus/contents/normal/ComposerListFragment;->mList:I
    invoke-static {v0}, Lcom/samsung/musicplus/contents/normal/ComposerListFragment;->access$100(Lcom/samsung/musicplus/contents/normal/ComposerListFragment;)I

    move-result v0

    invoke-static {v0}, Lcom/samsung/musicplus/util/ListUtils;->isGridType(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 82
    add-int/lit8 v0, p1, -0x1

    invoke-super {p0, v0}, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->getItemId(I)J

    move-result-wide v0

    .line 84
    :goto_0
    return-wide v0

    :cond_0
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->getItemId(I)J

    move-result-wide v0

    goto :goto_0
.end method
