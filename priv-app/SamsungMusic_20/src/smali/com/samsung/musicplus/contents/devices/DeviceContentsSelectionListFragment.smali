.class public Lcom/samsung/musicplus/contents/devices/DeviceContentsSelectionListFragment;
.super Lcom/samsung/musicplus/contents/extra/SelectionListFragment;
.source "DeviceContentsSelectionListFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/contents/devices/DeviceContentsSelectionListFragment$DeviceContentsSelectListAdapter;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;-><init>()V

    .line 53
    return-void
.end method

.method private getDeviceContentsCursorLoader()Landroid/content/CursorLoader;
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 39
    new-instance v0, Landroid/content/CursorLoader;

    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/devices/DeviceContentsSelectionListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/contents/devices/DeviceContentsSelectionListFragment;->mKey:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Lcom/samsung/musicplus/provider/MusicContents$Audio$Devices;->getContentsUri(J)Landroid/net/Uri;

    move-result-object v2

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    invoke-direct/range {v0 .. v6}, Landroid/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    .local v0, "cl":Landroid/content/CursorLoader;
    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v2, v3}, Landroid/content/CursorLoader;->setUpdateThrottle(J)V

    .line 43
    return-object v0
.end method


# virtual methods
.method protected createAdapter()Landroid/widget/CursorAdapter;
    .locals 6

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/devices/DeviceContentsSelectionListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    .line 49
    .local v2, "a":Landroid/app/Activity;
    new-instance v0, Lcom/samsung/musicplus/contents/devices/DeviceContentsSelectionListFragment$DeviceContentsSelectListAdapter;

    const v3, 0x7f040033

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/samsung/musicplus/contents/devices/DeviceContentsSelectionListFragment$DeviceContentsSelectListAdapter;-><init>(Lcom/samsung/musicplus/contents/devices/DeviceContentsSelectionListFragment;Landroid/content/Context;ILandroid/database/Cursor;I)V

    return-object v0
.end method

.method protected initLoader()V
    .locals 3

    .prologue
    .line 30
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/devices/DeviceContentsSelectionListFragment;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    const v1, 0x2000d

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    .line 31
    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/content/Loader;
    .locals 1
    .param p1, "id"    # I
    .param p2, "args"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/devices/DeviceContentsSelectionListFragment;->getDeviceContentsCursorLoader()Landroid/content/CursorLoader;

    move-result-object v0

    return-object v0
.end method
