.class public Lcom/samsung/musicplus/contents/TrackListProgressAdapter;
.super Lcom/samsung/musicplus/widget/list/TrackListAdapter;
.source "TrackListProgressAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/contents/TrackListProgressAdapter$TrackProgressViewHolder;
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;ILandroid/database/Cursor;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "layout"    # I
    .param p3, "c"    # Landroid/database/Cursor;
    .param p4, "flags"    # I

    .prologue
    .line 22
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/musicplus/widget/list/TrackListAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;I)V

    .line 23
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0147

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/contents/TrackListProgressAdapter;->setPaddingRight(I)V

    .line 25
    return-void
.end method


# virtual methods
.method protected bindNowPlayingView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "c"    # Landroid/database/Cursor;

    .prologue
    .line 46
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->bindNowPlayingView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    .line 47
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/contents/TrackListProgressAdapter$TrackProgressViewHolder;

    .line 48
    .local v0, "vh":Lcom/samsung/musicplus/contents/TrackListProgressAdapter$TrackProgressViewHolder;
    iget-object v1, v0, Lcom/samsung/musicplus/contents/TrackListProgressAdapter$TrackProgressViewHolder;->progress:Landroid/widget/ProgressBar;

    if-eqz v1, :cond_0

    .line 49
    iget-object v1, v0, Lcom/samsung/musicplus/contents/TrackListProgressAdapter$TrackProgressViewHolder;->progress:Landroid/widget/ProgressBar;

    iget-object v2, v0, Lcom/samsung/musicplus/contents/TrackListProgressAdapter$TrackProgressViewHolder;->animation:Landroid/widget/ImageView;

    iget v3, p0, Lcom/samsung/musicplus/contents/TrackListProgressAdapter;->mAudioIndex:I

    invoke-interface {p3, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-virtual {p0, v1, v2, v4, v5}, Lcom/samsung/musicplus/contents/TrackListProgressAdapter;->updateProgressView(Landroid/widget/ProgressBar;Landroid/widget/ImageView;J)V

    .line 51
    :cond_0
    return-void
.end method

.method protected getViewHolder()Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;
    .locals 1

    .prologue
    .line 41
    new-instance v0, Lcom/samsung/musicplus/contents/TrackListProgressAdapter$TrackProgressViewHolder;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/contents/TrackListProgressAdapter$TrackProgressViewHolder;-><init>(Lcom/samsung/musicplus/contents/TrackListProgressAdapter;)V

    return-object v0
.end method

.method protected newOtherView(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 29
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->newOtherView(Landroid/view/View;)V

    .line 30
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/musicplus/contents/TrackListProgressAdapter$TrackProgressViewHolder;

    .line 31
    .local v1, "vh":Lcom/samsung/musicplus/contents/TrackListProgressAdapter$TrackProgressViewHolder;
    const v2, 0x7f0d00c3

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 32
    .local v0, "stub":Landroid/view/View;
    instance-of v2, v0, Landroid/view/ViewStub;

    if-eqz v2, :cond_0

    .line 33
    check-cast v0, Landroid/view/ViewStub;

    .end local v0    # "stub":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 35
    :cond_0
    const v2, 0x7f0d0133

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ProgressBar;

    iput-object v2, v1, Lcom/samsung/musicplus/contents/TrackListProgressAdapter$TrackProgressViewHolder;->progress:Landroid/widget/ProgressBar;

    .line 36
    invoke-virtual {p1, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 37
    return-void
.end method

.method protected updateProgressView(Landroid/widget/ProgressBar;Landroid/widget/ImageView;J)V
    .locals 3
    .param p1, "progress"    # Landroid/widget/ProgressBar;
    .param p2, "animation"    # Landroid/widget/ImageView;
    .param p3, "currentId"    # J

    .prologue
    const/16 v2, 0x8

    .line 61
    iget-wide v0, p0, Lcom/samsung/musicplus/contents/TrackListProgressAdapter;->mPlayingId:J

    cmp-long v0, v0, p3

    if-nez v0, :cond_3

    .line 62
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 63
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->isPreparing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 64
    if-eqz p2, :cond_0

    .line 65
    invoke-virtual {p2, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 67
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 77
    :goto_0
    return-void

    .line 69
    :cond_1
    invoke-virtual {p1, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0

    .line 72
    :cond_2
    invoke-virtual {p1, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0

    .line 75
    :cond_3
    invoke-virtual {p1, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0
.end method
