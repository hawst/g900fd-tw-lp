.class public Lcom/samsung/musicplus/contents/menu/PlayListMenus;
.super Lcom/samsung/musicplus/contents/menu/CommonListMenus;
.source "PlayListMenus.java"


# static fields
.field public static final LOG_TAG:Ljava/lang/String; = "PlayListMenu"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/menu/CommonListMenus;-><init>()V

    return-void
.end method


# virtual methods
.method public onContextItemSelected(Lcom/samsung/musicplus/common/menu/MusicMenuHandler;Landroid/view/MenuItem;J)Z
    .locals 15
    .param p1, "handler"    # Lcom/samsung/musicplus/common/menu/MusicMenuHandler;
    .param p2, "item"    # Landroid/view/MenuItem;
    .param p3, "selectedId"    # J

    .prologue
    .line 106
    move-object/from16 v0, p1

    instance-of v2, v0, Lcom/samsung/musicplus/contents/menu/CommonListMenuHandler;

    if-nez v2, :cond_0

    .line 107
    const-string v2, "PlayListMenu"

    const-string v4, "onContextItemSelected : handler is not instance of CommonListMenuHandler"

    invoke-static {v2, v4}, Lcom/samsung/musicplus/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    const/4 v2, 0x0

    .line 132
    :goto_0
    return v2

    :cond_0
    move-object/from16 v11, p1

    .line 112
    check-cast v11, Lcom/samsung/musicplus/contents/menu/CommonListMenuHandler;

    .line 114
    .local v11, "commonHandler":Lcom/samsung/musicplus/contents/menu/CommonListMenuHandler;
    invoke-interface/range {p2 .. p2}, Landroid/view/MenuItem;->getMenuInfo()Landroid/view/ContextMenu$ContextMenuInfo;

    move-result-object v13

    check-cast v13, Landroid/widget/AdapterView$AdapterContextMenuInfo;

    .line 115
    .local v13, "mi":Landroid/widget/AdapterView$AdapterContextMenuInfo;
    invoke-interface {v11}, Lcom/samsung/musicplus/contents/menu/CommonListMenuHandler;->getFragment()Landroid/app/Fragment;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    .line 116
    .local v3, "a":Landroid/app/Activity;
    iget v14, v13, Landroid/widget/AdapterView$AdapterContextMenuInfo;->position:I

    .line 117
    .local v14, "position":I
    invoke-interface {v11, v14}, Lcom/samsung/musicplus/contents/menu/CommonListMenuHandler;->getKeyWord(I)Ljava/lang/String;

    move-result-object v12

    .line 118
    .local v12, "key":Ljava/lang/String;
    invoke-interface/range {p2 .. p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 132
    invoke-super/range {p0 .. p4}, Lcom/samsung/musicplus/contents/menu/CommonListMenus;->onContextItemSelected(Lcom/samsung/musicplus/common/menu/MusicMenuHandler;Landroid/view/MenuItem;J)Z

    move-result v2

    goto :goto_0

    .line 120
    :pswitch_0
    new-instance v2, Lcom/samsung/musicplus/util/FileOperationTask$DeletePlaylistTask;

    const/4 v4, 0x0

    move-wide/from16 v0, p3

    invoke-direct {v2, v3, v0, v1, v4}, Lcom/samsung/musicplus/util/FileOperationTask$DeletePlaylistTask;-><init>(Landroid/app/Activity;JZ)V

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Void;

    invoke-virtual {v2, v4}, Lcom/samsung/musicplus/util/FileOperationTask$DeletePlaylistTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 121
    const/4 v2, 0x1

    goto :goto_0

    .line 123
    :pswitch_1
    new-instance v2, Lcom/samsung/musicplus/util/FileOperationTask$RemoveTask;

    invoke-interface {v11}, Lcom/samsung/musicplus/contents/menu/CommonListMenuHandler;->getKey()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iget-wide v6, v13, Landroid/widget/AdapterView$AdapterContextMenuInfo;->id:J

    int-to-long v8, v14

    const/4 v10, 0x0

    invoke-direct/range {v2 .. v10}, Lcom/samsung/musicplus/util/FileOperationTask$RemoveTask;-><init>(Landroid/app/Activity;JJJZ)V

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Void;

    invoke-virtual {v2, v4}, Lcom/samsung/musicplus/util/FileOperationTask$RemoveTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 125
    const/4 v2, 0x1

    goto :goto_0

    .line 127
    :pswitch_2
    new-instance v2, Lcom/samsung/musicplus/dialog/EditTitleDialog;

    invoke-interface {v11}, Lcom/samsung/musicplus/contents/menu/CommonListMenuHandler;->getList()I

    move-result v4

    const/16 v5, 0x9

    invoke-direct {v2, v4, v5, v12}, Lcom/samsung/musicplus/dialog/EditTitleDialog;-><init>(IILjava/lang/String;)V

    invoke-interface {v11}, Lcom/samsung/musicplus/contents/menu/CommonListMenuHandler;->getFragment()Landroid/app/Fragment;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Fragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    const-string v5, "edit_title"

    invoke-virtual {v2, v4, v5}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 129
    const/4 v2, 0x1

    goto :goto_0

    .line 118
    :pswitch_data_0
    .packed-switch 0x7f0d01ce
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public onCreateOptionsMenu(Landroid/view/MenuInflater;Landroid/view/Menu;)V
    .locals 1
    .param p1, "inflater"    # Landroid/view/MenuInflater;
    .param p2, "menu"    # Landroid/view/Menu;

    .prologue
    .line 34
    const v0, 0x7f12000a

    invoke-virtual {p1, v0, p2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 35
    invoke-super {p0, p1, p2}, Lcom/samsung/musicplus/contents/menu/CommonListMenus;->onCreateOptionsMenu(Landroid/view/MenuInflater;Landroid/view/Menu;)V

    .line 36
    return-void
.end method

.method protected onInflateContextMenu(Landroid/view/MenuInflater;Landroid/view/ContextMenu;)V
    .locals 1
    .param p1, "inflater"    # Landroid/view/MenuInflater;
    .param p2, "menu"    # Landroid/view/ContextMenu;

    .prologue
    .line 100
    invoke-super {p0, p1, p2}, Lcom/samsung/musicplus/contents/menu/CommonListMenus;->onInflateContextMenu(Landroid/view/MenuInflater;Landroid/view/ContextMenu;)V

    .line 101
    const v0, 0x7f120005

    invoke-virtual {p1, v0, p2}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 102
    return-void
.end method

.method public onOptionsItemSelected(Lcom/samsung/musicplus/common/menu/MusicMenuHandler;Landroid/view/MenuItem;)Z
    .locals 18
    .param p1, "handler"    # Lcom/samsung/musicplus/common/menu/MusicMenuHandler;
    .param p2, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 41
    move-object/from16 v0, p1

    instance-of v2, v0, Lcom/samsung/musicplus/contents/menu/CommonListMenuHandler;

    if-nez v2, :cond_0

    .line 42
    const-string v2, "PlayListMenu"

    const-string v3, "onOptionsItemSelected : handler is not instance of common list menu"

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    const/4 v2, 0x0

    .line 95
    :goto_0
    return v2

    :cond_0
    move-object/from16 v12, p1

    .line 46
    check-cast v12, Lcom/samsung/musicplus/contents/menu/CommonListMenuHandler;

    .line 47
    .local v12, "commonMenuHanlder":Lcom/samsung/musicplus/contents/menu/CommonListMenuHandler;
    invoke-interface {v12}, Lcom/samsung/musicplus/contents/menu/CommonListMenuHandler;->getContext()Landroid/content/Context;

    move-result-object v13

    .line 48
    .local v13, "context":Landroid/content/Context;
    invoke-interface {v12}, Lcom/samsung/musicplus/contents/menu/CommonListMenuHandler;->getList()I

    move-result v8

    .line 49
    .local v8, "list":I
    invoke-interface {v12}, Lcom/samsung/musicplus/contents/menu/CommonListMenuHandler;->getKey()Ljava/lang/String;

    move-result-object v9

    .line 50
    .local v9, "key":Ljava/lang/String;
    invoke-interface {v12}, Lcom/samsung/musicplus/contents/menu/CommonListMenuHandler;->getFragment()Landroid/app/Fragment;

    move-result-object v15

    .line 52
    .local v15, "fragment":Landroid/app/Fragment;
    invoke-interface/range {p2 .. p2}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 95
    invoke-super/range {p0 .. p2}, Lcom/samsung/musicplus/contents/menu/CommonListMenus;->onOptionsItemSelected(Lcom/samsung/musicplus/common/menu/MusicMenuHandler;Landroid/view/MenuItem;)Z

    move-result v2

    goto :goto_0

    .line 54
    :sswitch_0
    const-string v2, "SLOD"

    invoke-static {v13, v2}, Lcom/samsung/musicplus/util/logging/FeatureLogger;->insertLog(Landroid/content/Context;Ljava/lang/String;)V

    .line 56
    :sswitch_1
    invoke-interface/range {p1 .. p1}, Lcom/samsung/musicplus/common/menu/MusicMenuHandler;->getFragment()Landroid/app/Fragment;

    move-result-object v14

    .line 57
    .local v14, "f":Landroid/app/Fragment;
    instance-of v2, v14, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;

    if-eqz v2, :cond_1

    .line 58
    check-cast v14, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;

    .end local v14    # "f":Landroid/app/Fragment;
    invoke-virtual {v14}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->startActionMode()V

    .line 60
    :cond_1
    const/4 v2, 0x1

    goto :goto_0

    .line 62
    :sswitch_2
    invoke-virtual {v15}, Landroid/app/Fragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-static {v13, v8, v2}, Lcom/samsung/musicplus/util/FileOperationTask$AddToPlaylistTask;->makePlaylist(Landroid/content/Context;ILandroid/app/FragmentManager;)V

    .line 63
    const/4 v2, 0x1

    goto :goto_0

    .line 65
    :sswitch_3
    invoke-interface {v12}, Lcom/samsung/musicplus/contents/menu/CommonListMenuHandler;->getQueryArgs()Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    move-result-object v16

    .line 67
    .local v16, "queryArgs":Lcom/samsung/musicplus/util/ListUtils$QueryArgs;
    const/4 v11, 0x0

    .line 69
    .local v11, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {v15}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    move-object/from16 v0, v16

    iget-object v3, v0, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->uri:Landroid/net/Uri;

    move-object/from16 v0, v16

    iget-object v4, v0, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->projection:[Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v5, v0, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->selection:Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v6, v0, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->selectionArgs:[Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v7, v0, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->orderBy:Ljava/lang/String;

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 74
    invoke-static {v11}, Lcom/samsung/musicplus/util/ListUtils;->getSongListForCursor(Landroid/database/Cursor;)[J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v17

    .line 76
    .local v17, "songList":[J
    if-eqz v11, :cond_2

    .line 77
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 80
    :cond_2
    new-instance v2, Lcom/samsung/musicplus/dialog/EditTitleDialog;

    const/4 v3, 0x7

    move-object/from16 v0, v17

    invoke-direct {v2, v8, v3, v0}, Lcom/samsung/musicplus/dialog/EditTitleDialog;-><init>(II[J)V

    invoke-virtual {v15}, Landroid/app/Fragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    const-string v5, "edit_title"

    invoke-virtual {v2, v3, v5}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 82
    const/4 v2, 0x1

    goto :goto_0

    .line 76
    .end local v17    # "songList":[J
    :catchall_0
    move-exception v2

    if-eqz v11, :cond_3

    .line 77
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v2

    .line 84
    .end local v11    # "c":Landroid/database/Cursor;
    .end local v16    # "queryArgs":Lcom/samsung/musicplus/util/ListUtils$QueryArgs;
    :sswitch_4
    new-instance v2, Lcom/samsung/musicplus/dialog/EditTitleDialog;

    const/16 v3, 0x8

    invoke-direct {v2, v8, v3, v9}, Lcom/samsung/musicplus/dialog/EditTitleDialog;-><init>(IILjava/lang/String;)V

    invoke-virtual {v15}, Landroid/app/Fragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    const-string v5, "edit_title"

    invoke-virtual {v2, v3, v5}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 86
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 88
    :sswitch_5
    invoke-virtual {v15}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    .line 89
    .local v4, "a":Landroid/app/Activity;
    invoke-static {v9}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 90
    .local v6, "plid":J
    new-instance v3, Lcom/samsung/musicplus/util/FileOperationTask$ReorderTask;

    const/4 v5, 0x0

    const/4 v10, 0x1

    invoke-direct/range {v3 .. v10}, Lcom/samsung/musicplus/util/FileOperationTask$ReorderTask;-><init>(Landroid/app/Activity;ZJILjava/lang/String;Z)V

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v3, v2}, Lcom/samsung/musicplus/util/FileOperationTask$ReorderTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 91
    const/4 v2, 0x1

    goto/16 :goto_0

    .line 52
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0d01ce -> :sswitch_4
        0x7f0d01cf -> :sswitch_1
        0x7f0d01d0 -> :sswitch_0
        0x7f0d01f8 -> :sswitch_2
        0x7f0d01fa -> :sswitch_3
        0x7f0d01fb -> :sswitch_5
    .end sparse-switch
.end method
