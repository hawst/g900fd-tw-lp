.class Lcom/samsung/musicplus/contents/square/MusicSquareFragment$4;
.super Ljava/lang/Object;
.source "MusicSquareFragment.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->ensureSquareView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 368
    iput-object p1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$4;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    iput-object p2, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$4;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 5
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 372
    iget-object v3, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$4;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    # getter for: Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSquareGridView:Landroid/widget/GridView;
    invoke-static {v3}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->access$100(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;)Landroid/widget/GridView;

    move-result-object v3

    if-nez v3, :cond_0

    move v1, v2

    .line 445
    .end local p1    # "v":Landroid/view/View;
    :goto_0
    return v1

    .line 376
    .restart local p1    # "v":Landroid/view/View;
    :cond_0
    iget-object v3, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$4;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    # getter for: Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mIsReadyToClick:Z
    invoke-static {v3}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->access$700(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$4;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    # getter for: Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSquareGridView:Landroid/widget/GridView;
    invoke-static {v3}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->access$100(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;)Landroid/widget/GridView;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/GridView;->isClickable()Z

    move-result v3

    if-nez v3, :cond_2

    :cond_1
    move v1, v2

    .line 377
    goto :goto_0

    .line 379
    :cond_2
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    goto :goto_0

    .line 381
    :pswitch_0
    instance-of v1, p1, Landroid/widget/GridView;

    if-eqz v1, :cond_3

    .line 382
    iget-object v1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$4;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    # invokes: Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->clearVisualInteraction()V
    invoke-static {v1}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->access$800(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;)V

    .line 383
    iget-object v1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$4;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    # setter for: Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mIsDown:Z
    invoke-static {v1, v2}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->access$902(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;Z)Z

    move-object v1, p1

    .line 384
    check-cast v1, Landroid/widget/GridView;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {v1, v3, v4}, Landroid/widget/GridView;->pointToPosition(II)I

    move-result v0

    .line 387
    .local v0, "position":I
    iget-object v1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$4;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v4, v4

    # invokes: Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->setBasePointForCheckMove(II)V
    invoke-static {v1, v3, v4}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->access$1000(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;II)V

    .line 389
    iget-object v1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$4;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    # invokes: Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->updateLongClickTimer()V
    invoke-static {v1}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->access$1100(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;)V

    .line 391
    if-ltz v0, :cond_3

    iget-object v1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$4;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    # getter for: Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mLastSelectedCell:I
    invoke-static {v1}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->access$1200(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;)I

    move-result v1

    if-eq v0, v1, :cond_3

    .line 392
    iget-object v3, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$4;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    move-object v1, p1

    check-cast v1, Landroid/widget/GridView;

    # invokes: Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->clearAllMusicSquareCell(Landroid/widget/GridView;)V
    invoke-static {v3, v1}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->access$200(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;Landroid/widget/GridView;)V

    .line 393
    iget-object v1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$4;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    check-cast p1, Landroid/widget/GridView;

    .end local p1    # "v":Landroid/view/View;
    # invokes: Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->selectMusicSquareCell(Landroid/widget/GridView;I)V
    invoke-static {v1, p1, v0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->access$300(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;Landroid/widget/GridView;I)V

    .line 394
    iget-object v1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$4;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    # setter for: Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mLastSelectedCell:I
    invoke-static {v1, v0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->access$1202(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;I)I

    .end local v0    # "position":I
    :cond_3
    move v1, v2

    .line 402
    goto :goto_0

    .line 404
    .restart local p1    # "v":Landroid/view/View;
    :pswitch_1
    iget-object v3, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$4;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    # getter for: Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mIsDown:Z
    invoke-static {v3}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->access$900(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 405
    iget-object v3, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$4;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    # setter for: Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mIsDown:Z
    invoke-static {v3, v1}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->access$902(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;Z)Z

    .line 406
    iget-object v3, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$4;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    # getter for: Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mIsOnResumed:Z
    invoke-static {v3}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->access$400(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 407
    iget-object v3, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$4;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    # invokes: Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->doPlaySelectedMusicSquareCell()Z
    invoke-static {v3}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->access$500(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;)Z

    .line 410
    :cond_4
    iget-object v3, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$4;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    const/4 v4, -0x1

    # setter for: Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mLastSelectedCell:I
    invoke-static {v3, v4}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->access$1202(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;I)I

    .line 411
    iget-object v3, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$4;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    # invokes: Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->saveSelectedCellPref()V
    invoke-static {v3}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->access$600(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;)V

    .line 412
    iget-object v3, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$4;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    # invokes: Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->cancelLongClick()V
    invoke-static {v3}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->access$1300(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;)V

    .line 413
    iget-object v3, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$4;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    const/16 v4, 0x12c

    # invokes: Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->setReadyToClick(I)V
    invoke-static {v3, v4}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->access$1400(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;I)V

    .line 416
    iget-object v3, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$4;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    # getter for: Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mSquareGridView:Landroid/widget/GridView;
    invoke-static {v3}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->access$100(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;)Landroid/widget/GridView;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/widget/GridView;->playSoundEffect(I)V

    move v1, v2

    .line 418
    goto/16 :goto_0

    .line 420
    :pswitch_2
    iget-object v1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$4;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    # getter for: Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mIsDown:Z
    invoke-static {v1}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->access$900(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;)Z

    move-result v1

    if-eqz v1, :cond_6

    instance-of v1, p1, Landroid/widget/GridView;

    if-eqz v1, :cond_6

    move-object v1, p1

    .line 421
    check-cast v1, Landroid/widget/GridView;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v4, v4

    invoke-virtual {v1, v3, v4}, Landroid/widget/GridView;->pointToPosition(II)I

    move-result v0

    .line 423
    .restart local v0    # "position":I
    if-ltz v0, :cond_6

    .line 425
    iget-object v1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$4;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v4, v4

    # invokes: Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->checkMovePointUserIntented(II)Z
    invoke-static {v1, v3, v4}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->access$1500(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;II)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 427
    iget-object v1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$4;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    # invokes: Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->updateLongClickTimer()V
    invoke-static {v1}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->access$1100(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;)V

    .line 428
    iget-object v1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$4;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    float-to-int v4, v4

    # invokes: Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->setBasePointForCheckMove(II)V
    invoke-static {v1, v3, v4}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->access$1000(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;II)V

    .line 430
    :cond_5
    iget-object v1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$4;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    # getter for: Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mLastSelectedCell:I
    invoke-static {v1}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->access$1200(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;)I

    move-result v1

    if-eq v0, v1, :cond_6

    .line 431
    iget-object v1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$4;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    check-cast p1, Landroid/widget/GridView;

    .end local p1    # "v":Landroid/view/View;
    # invokes: Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->selectMusicSquareCell(Landroid/widget/GridView;I)V
    invoke-static {v1, p1, v0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->access$300(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;Landroid/widget/GridView;I)V

    .line 432
    iget-object v1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$4;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    # setter for: Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->mLastSelectedCell:I
    invoke-static {v1, v0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->access$1202(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;I)I

    .end local v0    # "position":I
    :cond_6
    move v1, v2

    .line 441
    goto/16 :goto_0

    .line 379
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
