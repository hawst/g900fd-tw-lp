.class Lcom/samsung/musicplus/contents/extra/SelectionListFragment$2;
.super Landroid/os/Handler;
.source "SelectionListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/contents/extra/SelectionListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/contents/extra/SelectionListFragment;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/contents/extra/SelectionListFragment;)V
    .locals 0

    .prologue
    .line 191
    iput-object p1, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$2;->this$0:Lcom/samsung/musicplus/contents/extra/SelectionListFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 193
    iget-object v2, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$2;->this$0:Lcom/samsung/musicplus/contents/extra/SelectionListFragment;

    invoke-virtual {v2}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->getAbsListView()Landroid/widget/AbsListView;

    move-result-object v1

    .line 194
    .local v1, "lv":Landroid/widget/AbsListView;
    if-eqz v1, :cond_1

    .line 195
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {v1}, Landroid/widget/AbsListView;->getCount()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 196
    iget-object v2, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$2;->this$0:Lcom/samsung/musicplus/contents/extra/SelectionListFragment;

    invoke-virtual {v2}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    check-cast v2, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;

    iget-object v3, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$2;->this$0:Lcom/samsung/musicplus/contents/extra/SelectionListFragment;

    invoke-virtual {v3, v0}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->getAudioId(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/samsung/musicplus/contents/extra/MusicSelectListTabActivity;->isItemSelected(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 198
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/widget/AbsListView;->setItemChecked(IZ)V

    .line 195
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 200
    :cond_0
    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/widget/AbsListView;->setItemChecked(IZ)V

    goto :goto_1

    .line 203
    .end local v0    # "i":I
    :cond_1
    iget-object v2, p0, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$2;->this$0:Lcom/samsung/musicplus/contents/extra/SelectionListFragment;

    # invokes: Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->invalidateActionBarTitle()V
    invoke-static {v2}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->access$200(Lcom/samsung/musicplus/contents/extra/SelectionListFragment;)V

    .line 204
    return-void
.end method
