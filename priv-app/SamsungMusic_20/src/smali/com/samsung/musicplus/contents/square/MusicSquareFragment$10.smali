.class Lcom/samsung/musicplus/contents/square/MusicSquareFragment$10;
.super Ljava/lang/Object;
.source "MusicSquareFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->setSquareHelpButton()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/contents/square/MusicSquareFragment;)V
    .locals 0

    .prologue
    .line 1756
    iput-object p1, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$10;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1760
    iget-object v4, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$10;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    invoke-virtual {v4}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 1761
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Activity;->isResumed()Z

    move-result v4

    if-nez v4, :cond_1

    .line 1772
    :cond_0
    :goto_0
    return-void

    .line 1764
    :cond_1
    iget-object v4, p0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$10;->this$0:Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    invoke-virtual {v4}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->getChildFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    .line 1765
    .local v2, "fm":Landroid/app/FragmentManager;
    const-string v4, "square_help"

    invoke-virtual {v2, v4}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    .line 1766
    .local v1, "fg":Landroid/app/Fragment;
    if-eqz v1, :cond_2

    .line 1767
    invoke-virtual {v2}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v3

    .line 1768
    .local v3, "ft":Landroid/app/FragmentTransaction;
    invoke-virtual {v3, v1}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 1769
    invoke-virtual {v3}, Landroid/app/FragmentTransaction;->commit()I

    .line 1771
    .end local v3    # "ft":Landroid/app/FragmentTransaction;
    :cond_2
    new-instance v4, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$SquareHelpDialog;

    invoke-direct {v4}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$SquareHelpDialog;-><init>()V

    const-string v5, "square_help"

    invoke-virtual {v4, v2, v5}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$SquareHelpDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method
