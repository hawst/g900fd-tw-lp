.class Lcom/samsung/musicplus/contents/PersonalFileOperationThread$FolderItem;
.super Ljava/lang/Object;
.source "PersonalFileOperationThread.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/contents/PersonalFileOperationThread;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "FolderItem"
.end annotation


# instance fields
.field fileName:Ljava/lang/String;

.field folderName:Ljava/lang/String;

.field id:J

.field path:Ljava/lang/String;


# direct methods
.method public constructor <init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "idParam"    # J
    .param p3, "pathParam"    # Ljava/lang/String;
    .param p4, "nameParam"    # Ljava/lang/String;
    .param p5, "folderParam"    # Ljava/lang/String;

    .prologue
    .line 609
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 610
    iput-wide p1, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread$FolderItem;->id:J

    .line 611
    iput-object p3, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread$FolderItem;->path:Ljava/lang/String;

    .line 612
    iput-object p4, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread$FolderItem;->fileName:Ljava/lang/String;

    .line 613
    iput-object p5, p0, Lcom/samsung/musicplus/contents/PersonalFileOperationThread$FolderItem;->folderName:Ljava/lang/String;

    .line 614
    return-void
.end method
