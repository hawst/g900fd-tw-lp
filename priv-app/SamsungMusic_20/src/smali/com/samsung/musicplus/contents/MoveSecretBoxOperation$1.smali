.class Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$1;
.super Lcom/samsung/android/privatemode/IPrivateModeClient$Stub;
.source "MoveSecretBoxOperation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;)V
    .locals 0

    .prologue
    .line 131
    iput-object p1, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$1;->this$0:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;

    invoke-direct {p0}, Lcom/samsung/android/privatemode/IPrivateModeClient$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onStateChange(II)V
    .locals 3
    .param p1, "state"    # I
    .param p2, "extInfo"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 135
    const-string v0, "PersonalFileOperation"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onStateChagne state:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 136
    if-nez p1, :cond_0

    .line 137
    iget-object v0, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$1;->this$0:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;

    iget-object v1, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$1;->this$0:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;

    iget-object v1, v1, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mPmMgr:Lcom/samsung/android/privatemode/PrivateModeManager;

    invoke-virtual {v1, p0}, Lcom/samsung/android/privatemode/PrivateModeManager;->registerClient(Lcom/samsung/android/privatemode/IPrivateModeClient;)Landroid/os/IBinder;

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mToken:Landroid/os/IBinder;

    .line 138
    iget-object v0, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$1;->this$0:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;

    iget-object v0, v0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mToken:Landroid/os/IBinder;

    if-nez v0, :cond_0

    .line 139
    const-string v0, "PersonalFileOperation"

    const-string v1, "state fail : registerClient returned null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    iget-object v0, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$1;->this$0:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;

    invoke-virtual {v0}, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->stopMove()V

    .line 141
    iget-object v0, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$1;->this$0:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;

    # getter for: Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mOnMoveSecretBoxListener:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$OnMoveSecretBoxListener;
    invoke-static {v0}, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->access$000(Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;)Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$OnMoveSecretBoxListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 142
    iget-object v0, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$1;->this$0:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;

    # getter for: Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mOnMoveSecretBoxListener:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$OnMoveSecretBoxListener;
    invoke-static {v0}, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->access$000(Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;)Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$OnMoveSecretBoxListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$OnMoveSecretBoxListener;->onMoveFinished()V

    .line 146
    :cond_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_1

    .line 147
    iget-object v0, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$1;->this$0:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;

    # getter for: Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mMoveFilesThread:Lcom/samsung/musicplus/contents/PersonalFileOperationThread;
    invoke-static {v0}, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->access$100(Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;)Lcom/samsung/musicplus/contents/PersonalFileOperationThread;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->start()V

    .line 149
    :cond_1
    const/4 v0, 0x3

    if-ne p1, v0, :cond_2

    .line 150
    iget-object v0, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$1;->this$0:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;

    invoke-virtual {v0}, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->stopMove()V

    .line 151
    iget-object v0, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$1;->this$0:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;

    # getter for: Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mOnMoveSecretBoxListener:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$OnMoveSecretBoxListener;
    invoke-static {v0}, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->access$000(Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;)Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$OnMoveSecretBoxListener;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 152
    iget-object v0, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$1;->this$0:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;

    # getter for: Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mOnMoveSecretBoxListener:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$OnMoveSecretBoxListener;
    invoke-static {v0}, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->access$000(Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;)Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$OnMoveSecretBoxListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$OnMoveSecretBoxListener;->onMoveFinished()V

    .line 155
    :cond_2
    return-void
.end method
