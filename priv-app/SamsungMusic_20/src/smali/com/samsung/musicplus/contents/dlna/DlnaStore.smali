.class public Lcom/samsung/musicplus/contents/dlna/DlnaStore;
.super Ljava/lang/Object;
.source "DlnaStore.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/contents/dlna/DlnaStore$ServerContentsExtra;,
        Lcom/samsung/musicplus/contents/dlna/DlnaStore$ServerContents;,
        Lcom/samsung/musicplus/contents/dlna/DlnaStore$Server;,
        Lcom/samsung/musicplus/contents/dlna/DlnaStore$Renderer;
    }
.end annotation


# static fields
.field private static final AUTHORITY:Ljava/lang/String; = "com.samsung.musicplus"

.field private static final CONTENT_AUTHORITY_SLASH:Ljava/lang/String; = "content://com.samsung.musicplus/"

.field public static final DLNA_ALL_DELETE_URI:Landroid/net/Uri;

.field public static final DLNA_ALL_TABLE_NAME:Ljava/lang/String; = "dlna_all_table"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    invoke-static {}, Lcom/samsung/musicplus/contents/dlna/DlnaStore;->getDlnaAllUri()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/contents/dlna/DlnaStore;->DLNA_ALL_DELETE_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 144
    return-void
.end method

.method private static final getDlnaAllUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 49
    const-string v0, "content://com.samsung.musicplus/dlna_all_table"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method
