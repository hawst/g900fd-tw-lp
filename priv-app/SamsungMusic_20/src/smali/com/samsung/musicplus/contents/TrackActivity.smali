.class public Lcom/samsung/musicplus/contents/TrackActivity;
.super Lcom/samsung/musicplus/MusicBaseActivity;
.source "TrackActivity.java"


# static fields
.field private static final CLASSNAME:Ljava/lang/String;

.field private static final MENU_SEARCH:I = 0x7f0d01cb


# instance fields
.field private isListEmpty:Z

.field private mKey:Ljava/lang/String;

.field private mList:I

.field private mTitle:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-class v0, Lcom/samsung/musicplus/contents/TrackActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/contents/TrackActivity;->CLASSNAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/samsung/musicplus/MusicBaseActivity;-><init>()V

    .line 36
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/contents/TrackActivity;->isListEmpty:Z

    return-void
.end method

.method private receivePlayerState(Ljava/lang/String;)V
    .locals 4
    .param p1, "action"    # Ljava/lang/String;

    .prologue
    .line 50
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/TrackActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    iget v2, p0, Lcom/samsung/musicplus/contents/TrackActivity;->mList:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 51
    .local v0, "fg":Landroid/app/Fragment;
    if-eqz v0, :cond_0

    move-object v1, v0

    .line 52
    check-cast v1, Lcom/samsung/musicplus/widget/fragment/OnMusicFragmentInterface;

    invoke-interface {v1, p1}, Lcom/samsung/musicplus/widget/fragment/OnMusicFragmentInterface;->receivePlayerState(Ljava/lang/String;)V

    .line 54
    :cond_0
    sget-object v2, Lcom/samsung/musicplus/contents/TrackActivity;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " updateAllViews(TAG_MUSIC_LIST) fragment :  "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " fragment.isResumed ? "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-nez v0, :cond_1

    const-string v1, "fragment is null"

    :goto_0
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    return-void

    .line 54
    :cond_1
    invoke-virtual {v0}, Landroid/app/Fragment;->isResumed()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto :goto_0
.end method

.method private setActionBar(ILjava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1, "list"    # I
    .param p2, "key"    # Ljava/lang/String;
    .param p3, "title"    # Ljava/lang/String;

    .prologue
    .line 151
    if-eqz p3, :cond_0

    const-string v3, "<unknown>"

    invoke-virtual {v3, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, ""

    invoke-virtual {v3, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 152
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "<"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/TrackActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f1001bb

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ">"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    .line 156
    :cond_1
    const v3, 0x20004

    if-ne p1, v3, :cond_2

    .line 157
    invoke-static {p2}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 158
    .local v0, "plid":J
    invoke-static {v0, v1}, Lcom/samsung/musicplus/util/ListUtils;->isUserMadePlayList(J)Z

    move-result v3

    if-nez v3, :cond_2

    .line 159
    invoke-static {v0, v1}, Lcom/samsung/musicplus/util/ListUtils;->getDefaultPlaylistName(J)I

    move-result v2

    .line 160
    .local v2, "resId":I
    if-eqz v2, :cond_2

    .line 161
    invoke-virtual {p0, v2}, Lcom/samsung/musicplus/contents/TrackActivity;->getString(I)Ljava/lang/String;

    move-result-object p3

    .line 165
    .end local v0    # "plid":J
    .end local v2    # "resId":I
    :cond_2
    invoke-virtual {p0, p3}, Lcom/samsung/musicplus/contents/TrackActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 166
    return-void
.end method


# virtual methods
.method public actionSconnect()Z
    .locals 3

    .prologue
    .line 96
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/TrackActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    iget v2, p0, Lcom/samsung/musicplus/contents/TrackActivity;->mList:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 97
    .local v0, "fg":Landroid/app/Fragment;
    instance-of v1, v0, Lcom/samsung/musicplus/widget/fragment/OnSconnectCallback;

    if-eqz v1, :cond_0

    .line 98
    check-cast v0, Lcom/samsung/musicplus/widget/fragment/OnSconnectCallback;

    .end local v0    # "fg":Landroid/app/Fragment;
    invoke-interface {v0}, Lcom/samsung/musicplus/widget/fragment/OnSconnectCallback;->actionSconnect()Z

    move-result v1

    .line 100
    :goto_0
    return v1

    .restart local v0    # "fg":Landroid/app/Fragment;
    :cond_0
    invoke-super {p0}, Lcom/samsung/musicplus/MusicBaseActivity;->actionSconnect()Z

    move-result v1

    goto :goto_0
.end method

.method protected launchQueryBrowser()V
    .locals 3

    .prologue
    .line 170
    iget v1, p0, Lcom/samsung/musicplus/contents/TrackActivity;->mList:I

    const v2, 0x2000d

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/samsung/musicplus/contents/TrackActivity;->mList:I

    const v2, 0x2000b

    if-ne v1, v2, :cond_1

    .line 171
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/samsung/musicplus/contents/search/QueryBrowserActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 172
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "keyword"

    iget-object v2, p0, Lcom/samsung/musicplus/contents/TrackActivity;->mKey:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 173
    const-string v1, "list_type"

    iget v2, p0, Lcom/samsung/musicplus/contents/TrackActivity;->mList:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 174
    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/contents/TrackActivity;->startActivity(Landroid/content/Intent;)V

    .line 178
    .end local v0    # "intent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 176
    :cond_1
    invoke-super {p0}, Lcom/samsung/musicplus/MusicBaseActivity;->launchQueryBrowser()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 66
    invoke-super {p0, p1}, Lcom/samsung/musicplus/MusicBaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 67
    const v2, 0x7f040091

    invoke-virtual {p0, v2}, Lcom/samsung/musicplus/contents/TrackActivity;->setContentView(I)V

    .line 69
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/TrackActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 70
    .local v0, "data":Landroid/os/Bundle;
    if-eqz p1, :cond_1

    .line 71
    const-string v2, "list_type"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/samsung/musicplus/contents/TrackActivity;->mList:I

    .line 72
    const-string v2, "keyword"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/musicplus/contents/TrackActivity;->mKey:Ljava/lang/String;

    .line 73
    const-string v2, "track_title"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/musicplus/contents/TrackActivity;->mTitle:Ljava/lang/String;

    .line 81
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/TrackActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/16 v3, 0x20

    invoke-virtual {v2, v3}, Landroid/view/Window;->setSoftInputMode(I)V

    .line 83
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/TrackActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    iget v3, p0, Lcom/samsung/musicplus/contents/TrackActivity;->mList:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    .line 84
    .local v1, "fg":Landroid/app/Fragment;
    if-nez v1, :cond_0

    .line 86
    iget v2, p0, Lcom/samsung/musicplus/contents/TrackActivity;->mList:I

    iget-object v3, p0, Lcom/samsung/musicplus/contents/TrackActivity;->mKey:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/samsung/musicplus/contents/ContentsFragmentFactory;->getNewInstance(ILjava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    .line 87
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/TrackActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v2

    const v3, 0x7f0d0190

    iget v4, p0, Lcom/samsung/musicplus/contents/TrackActivity;->mList:I

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v1, v4}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentTransaction;->commit()I

    .line 90
    :cond_0
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/Fragment;->setHasOptionsMenu(Z)V

    .line 91
    iget v2, p0, Lcom/samsung/musicplus/contents/TrackActivity;->mList:I

    iget-object v3, p0, Lcom/samsung/musicplus/contents/TrackActivity;->mKey:Ljava/lang/String;

    iget-object v4, p0, Lcom/samsung/musicplus/contents/TrackActivity;->mTitle:Ljava/lang/String;

    invoke-direct {p0, v2, v3, v4}, Lcom/samsung/musicplus/contents/TrackActivity;->setActionBar(ILjava/lang/String;Ljava/lang/String;)V

    .line 92
    return-void

    .line 76
    .end local v1    # "fg":Landroid/app/Fragment;
    :cond_1
    const-string v2, "list_type"

    const v3, 0x20001

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/samsung/musicplus/contents/TrackActivity;->mList:I

    .line 77
    const-string v2, "keyword"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/musicplus/contents/TrackActivity;->mKey:Ljava/lang/String;

    .line 78
    const-string v2, "track_title"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/musicplus/contents/TrackActivity;->mTitle:Ljava/lang/String;

    goto :goto_0
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 105
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/TrackActivity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f120001

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 106
    invoke-super {p0, p1}, Lcom/samsung/musicplus/MusicBaseActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 123
    sparse-switch p1, :sswitch_data_0

    .line 138
    invoke-super {p0, p1, p2}, Lcom/samsung/musicplus/MusicBaseActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    :cond_0
    :goto_0
    return v1

    .line 125
    :sswitch_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/TrackActivity;->launchQueryBrowser()V

    goto :goto_0

    .line 128
    :sswitch_1
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCtrlPressed()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 129
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/TrackActivity;->openOptionsMenu()V

    move v1, v2

    .line 130
    goto :goto_0

    .line 134
    :sswitch_2
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.musicplus.remove"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 135
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/contents/TrackActivity;->sendBroadcast(Landroid/content/Intent;)V

    move v1, v2

    .line 136
    goto :goto_0

    .line 123
    nop

    :sswitch_data_0
    .sparse-switch
        0x29 -> :sswitch_1
        0x54 -> :sswitch_0
        0x70 -> :sswitch_2
    .end sparse-switch
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 111
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 118
    :goto_0
    invoke-super {p0, p1}, Lcom/samsung/musicplus/MusicBaseActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 113
    :pswitch_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/TrackActivity;->launchQueryBrowser()V

    goto :goto_0

    .line 111
    :pswitch_data_0
    .packed-switch 0x7f0d01cb
        :pswitch_0
    .end packed-switch
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 60
    const v0, 0x7f0d01cb

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    iget-boolean v0, p0, Lcom/samsung/musicplus/contents/TrackActivity;->isListEmpty:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 61
    invoke-super {p0, p1}, Lcom/samsung/musicplus/MusicBaseActivity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0

    .line 60
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onReceiveMediaState(Landroid/content/Intent;)V
    .locals 0
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 40
    invoke-super {p0, p1}, Lcom/samsung/musicplus/MusicBaseActivity;->onReceiveMediaState(Landroid/content/Intent;)V

    .line 41
    return-void
.end method

.method protected onReceivePlayerState(Landroid/content/Intent;)V
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 45
    invoke-super {p0, p1}, Lcom/samsung/musicplus/MusicBaseActivity;->onReceivePlayerState(Landroid/content/Intent;)V

    .line 46
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/contents/TrackActivity;->receivePlayerState(Ljava/lang/String;)V

    .line 47
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 144
    const-string v0, "list_type"

    iget v1, p0, Lcom/samsung/musicplus/contents/TrackActivity;->mList:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 145
    const-string v0, "keyword"

    iget-object v1, p0, Lcom/samsung/musicplus/contents/TrackActivity;->mKey:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    const-string v0, "track_title"

    iget-object v1, p0, Lcom/samsung/musicplus/contents/TrackActivity;->mTitle:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    invoke-super {p0, p1}, Lcom/samsung/musicplus/MusicBaseActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 148
    return-void
.end method

.method public setListEmpty(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 181
    iput-boolean p1, p0, Lcom/samsung/musicplus/contents/TrackActivity;->isListEmpty:Z

    .line 182
    return-void
.end method
