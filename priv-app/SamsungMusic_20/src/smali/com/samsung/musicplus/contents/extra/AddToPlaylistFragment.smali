.class public Lcom/samsung/musicplus/contents/extra/AddToPlaylistFragment;
.super Lcom/samsung/musicplus/contents/extra/SelectionListFragment;
.source "AddToPlaylistFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/contents/extra/AddToPlaylistFragment$AddToPlaylistAdapter;
    }
.end annotation


# static fields
.field public static final FRAGMENT_TAG:Ljava/lang/String; = "add_to_playlist"

.field private static final LIST_ANIMATION_DURATION:I = 0xc8


# instance fields
.field private mListUpdater:Landroid/os/Handler;

.field private mSelectedListId:[J


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;-><init>()V

    .line 123
    new-instance v0, Lcom/samsung/musicplus/contents/extra/AddToPlaylistFragment$1;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/contents/extra/AddToPlaylistFragment$1;-><init>(Lcom/samsung/musicplus/contents/extra/AddToPlaylistFragment;)V

    iput-object v0, p0, Lcom/samsung/musicplus/contents/extra/AddToPlaylistFragment;->mListUpdater:Landroid/os/Handler;

    .line 142
    return-void
.end method

.method static synthetic access$100(Lcom/samsung/musicplus/contents/extra/AddToPlaylistFragment;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/extra/AddToPlaylistFragment;

    .prologue
    .line 41
    iget-object v0, p0, Lcom/samsung/musicplus/contents/extra/AddToPlaylistFragment;->mListUpdater:Landroid/os/Handler;

    return-object v0
.end method

.method public static getNewInstance()Landroid/app/Fragment;
    .locals 8

    .prologue
    .line 74
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 75
    .local v0, "data":Landroid/os/Bundle;
    const-string v2, "key"

    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    const-string v2, "selected_id"

    const/4 v3, 0x1

    new-array v3, v3, [J

    const/4 v4, 0x0

    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getCurrentAudioId()J

    move-result-wide v6

    aput-wide v6, v3, v4

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putLongArray(Ljava/lang/String;[J)V

    .line 79
    const-string v2, "list"

    const v3, 0x10004

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 80
    const-string v2, "mode"

    const/4 v3, 0x6

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 81
    const-string v2, "header"

    const/4 v3, 0x2

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 82
    new-instance v1, Lcom/samsung/musicplus/contents/extra/AddToPlaylistFragment;

    invoke-direct {v1}, Lcom/samsung/musicplus/contents/extra/AddToPlaylistFragment;-><init>()V

    .line 83
    .local v1, "fg":Landroid/app/Fragment;
    invoke-virtual {v1, v0}, Landroid/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 84
    return-object v1
.end method


# virtual methods
.method protected createAdapter()Landroid/widget/CursorAdapter;
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 103
    new-instance v0, Lcom/samsung/musicplus/contents/extra/AddToPlaylistFragment$AddToPlaylistAdapter;

    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/extra/AddToPlaylistFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const v3, 0x7f040039

    const/4 v5, 0x0

    move-object v1, p0

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Lcom/samsung/musicplus/contents/extra/AddToPlaylistFragment$AddToPlaylistAdapter;-><init>(Lcom/samsung/musicplus/contents/extra/AddToPlaylistFragment;Landroid/content/Context;ILandroid/database/Cursor;ILcom/samsung/musicplus/contents/extra/AddToPlaylistFragment$1;)V

    return-object v0
.end method

.method protected initIndexView(I)V
    .locals 0
    .param p1, "count"    # I

    .prologue
    .line 90
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 68
    invoke-super {p0, p1}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 69
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/extra/AddToPlaylistFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 70
    .local v0, "data":Landroid/os/Bundle;
    const-string v1, "selected_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/musicplus/contents/extra/AddToPlaylistFragment;->mSelectedListId:[J

    .line 71
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 57
    const v0, 0x7f040021

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 119
    iget-object v0, p0, Lcom/samsung/musicplus/contents/extra/AddToPlaylistFragment;->mListUpdater:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 120
    invoke-super {p0}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->onDestroyView()V

    .line 121
    return-void
.end method

.method public onListItemClick(Landroid/widget/AbsListView;Landroid/view/View;IJ)V
    .locals 6
    .param p1, "lv"    # Landroid/widget/AbsListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    .line 138
    invoke-virtual {p0, p3}, Lcom/samsung/musicplus/contents/extra/AddToPlaylistFragment;->getKeyWord(I)Ljava/lang/String;

    move-result-object v0

    .line 139
    .local v0, "key":Ljava/lang/String;
    new-instance v1, Lcom/samsung/musicplus/util/FileOperationTask$AddToThisPlaylistTask;

    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/extra/AddToPlaylistFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/musicplus/contents/extra/AddToPlaylistFragment;->mSelectedListId:[J

    invoke-static {v0}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/samsung/musicplus/util/FileOperationTask$AddToThisPlaylistTask;-><init>(Landroid/app/Activity;[JJ)V

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v1, v2}, Lcom/samsung/musicplus/util/FileOperationTask$AddToThisPlaylistTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 140
    return-void
.end method

.method public onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V
    .locals 4
    .param p2, "data"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 109
    .local p1, "loader":Landroid/content/Loader;, "Landroid/content/Loader<Landroid/database/Cursor;>;"
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/extra/AddToPlaylistFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 110
    invoke-virtual {p0, p2}, Lcom/samsung/musicplus/contents/extra/AddToPlaylistFragment;->setColumnIndicesAndSetIndex(Landroid/database/Cursor;)V

    .line 111
    iget-object v0, p0, Lcom/samsung/musicplus/contents/extra/AddToPlaylistFragment;->mAdapter:Landroid/widget/CursorAdapter;

    invoke-virtual {p0, p2}, Lcom/samsung/musicplus/contents/extra/AddToPlaylistFragment;->addOtherView(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/CursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 114
    iget-object v0, p0, Lcom/samsung/musicplus/contents/extra/AddToPlaylistFragment;->mListUpdater:Landroid/os/Handler;

    const/4 v1, 0x0

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 115
    return-void
.end method

.method public bridge synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Landroid/content/Loader;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 41
    check-cast p2, Landroid/database/Cursor;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/samsung/musicplus/contents/extra/AddToPlaylistFragment;->onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 62
    invoke-super {p0, p1, p2}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 63
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/extra/AddToPlaylistFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    const v1, 0x7f02009e

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelector(I)V

    .line 64
    return-void
.end method

.method protected setHeaderView()V
    .locals 3

    .prologue
    .line 94
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/extra/AddToPlaylistFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 95
    .local v0, "a":Landroid/app/Activity;
    const v2, 0x7f0d0093

    invoke-virtual {v0, v2}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 96
    .local v1, "create":Landroid/view/View;
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 97
    iget-object v2, p0, Lcom/samsung/musicplus/contents/extra/AddToPlaylistFragment;->mHeaderButtonClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 98
    const v2, 0x7f02009e

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundResource(I)V

    .line 99
    return-void
.end method
