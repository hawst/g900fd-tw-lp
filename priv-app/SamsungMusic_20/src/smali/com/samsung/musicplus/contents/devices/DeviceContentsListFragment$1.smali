.class Lcom/samsung/musicplus/contents/devices/DeviceContentsListFragment$1;
.super Landroid/content/BroadcastReceiver;
.source "DeviceContentsListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/contents/devices/DeviceContentsListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/contents/devices/DeviceContentsListFragment;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/contents/devices/DeviceContentsListFragment;)V
    .locals 0

    .prologue
    .line 123
    iput-object p1, p0, Lcom/samsung/musicplus/contents/devices/DeviceContentsListFragment$1;->this$0:Lcom/samsung/musicplus/contents/devices/DeviceContentsListFragment;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 127
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 128
    .local v1, "action":Ljava/lang/String;
    const-string v4, "MusicUiList"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " mDeviceStateReceiver action "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    sget-object v4, Lcom/samsung/musicplus/provider/MusicContents$Audio$DeviceContents;->ACTION_S_LINK_SIGNIN_CHANGED:Ljava/lang/String;

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 130
    invoke-static {p1}, Lcom/samsung/musicplus/provider/MusicContents$Audio$Devices;->isSingedIn(Landroid/content/Context;)Z

    move-result v3

    .line 131
    .local v3, "signIn":Z
    invoke-static {p1}, Lcom/samsung/musicplus/provider/MusicContents$Audio$Devices;->hasSamsungAccount(Landroid/content/Context;)Z

    move-result v2

    .line 132
    .local v2, "hasAccount":Z
    const-string v4, "MusicUiList"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "S link signin state changed : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " hasAccount : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    if-eqz v3, :cond_0

    if-nez v2, :cond_1

    .line 135
    :cond_0
    iget-object v4, p0, Lcom/samsung/musicplus/contents/devices/DeviceContentsListFragment$1;->this$0:Lcom/samsung/musicplus/contents/devices/DeviceContentsListFragment;

    invoke-virtual {v4}, Lcom/samsung/musicplus/contents/devices/DeviceContentsListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 136
    .local v0, "a":Landroid/app/Activity;
    if-eqz v0, :cond_1

    .line 137
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 146
    .end local v0    # "a":Landroid/app/Activity;
    .end local v2    # "hasAccount":Z
    .end local v3    # "signIn":Z
    :cond_1
    :goto_0
    return-void

    .line 140
    :cond_2
    const-string v4, "android.intent.action.SAMSUNGACCOUNT_SIGNOUT_COMPLETED"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 141
    iget-object v4, p0, Lcom/samsung/musicplus/contents/devices/DeviceContentsListFragment$1;->this$0:Lcom/samsung/musicplus/contents/devices/DeviceContentsListFragment;

    invoke-virtual {v4}, Lcom/samsung/musicplus/contents/devices/DeviceContentsListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 142
    .restart local v0    # "a":Landroid/app/Activity;
    if-eqz v0, :cond_1

    .line 143
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0
.end method
