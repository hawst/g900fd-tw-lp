.class public Lcom/samsung/musicplus/contents/normal/ArtistListFragment;
.super Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;
.source "ArtistListFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/contents/normal/ArtistListFragment$1;,
        Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTrackAdapter;,
        Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTabAdapter;
    }
.end annotation


# static fields
.field private static sSavedAlbumIndex:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static sSavedSongNumber:Landroid/util/SparseIntArray;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 109
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Lcom/samsung/musicplus/contents/normal/ArtistListFragment;->sSavedAlbumIndex:Landroid/util/SparseArray;

    .line 111
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    sput-object v0, Lcom/samsung/musicplus/contents/normal/ArtistListFragment;->sSavedSongNumber:Landroid/util/SparseIntArray;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;-><init>()V

    .line 316
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/contents/normal/ArtistListFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/normal/ArtistListFragment;

    .prologue
    .line 47
    iget v0, p0, Lcom/samsung/musicplus/contents/normal/ArtistListFragment;->mList:I

    return v0
.end method

.method static synthetic access$100(Lcom/samsung/musicplus/contents/normal/ArtistListFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/normal/ArtistListFragment;

    .prologue
    .line 47
    iget v0, p0, Lcom/samsung/musicplus/contents/normal/ArtistListFragment;->mList:I

    return v0
.end method

.method static synthetic access$400()Landroid/util/SparseIntArray;
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/samsung/musicplus/contents/normal/ArtistListFragment;->sSavedSongNumber:Landroid/util/SparseIntArray;

    return-object v0
.end method

.method static synthetic access$500()Landroid/util/SparseArray;
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/samsung/musicplus/contents/normal/ArtistListFragment;->sSavedAlbumIndex:Landroid/util/SparseArray;

    return-object v0
.end method

.method private saveAlbumIndex(Landroid/database/Cursor;)V
    .locals 5
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 120
    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-nez v4, :cond_1

    .line 145
    :cond_0
    :goto_0
    return-void

    .line 123
    :cond_1
    sget-object v4, Lcom/samsung/musicplus/contents/normal/ArtistListFragment;->sSavedAlbumIndex:Landroid/util/SparseArray;

    invoke-virtual {v4}, Landroid/util/SparseArray;->clear()V

    .line 124
    sget-object v4, Lcom/samsung/musicplus/contents/normal/ArtistListFragment;->sSavedSongNumber:Landroid/util/SparseIntArray;

    invoke-virtual {v4}, Landroid/util/SparseIntArray;->clear()V

    .line 125
    const/4 v2, -0x1

    .line 126
    .local v2, "position":I
    const/4 v0, 0x0

    .line 127
    .local v0, "album":Ljava/lang/String;
    const/4 v3, 0x0

    .line 128
    .local v3, "savedAlbum":Ljava/lang/String;
    const/4 v1, 0x1

    .line 129
    .local v1, "count":I
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 133
    :cond_2
    const-string v4, "album_id"

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v4

    invoke-interface {p1, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 134
    invoke-interface {p1}, Landroid/database/Cursor;->getPosition()I

    move-result v2

    .line 135
    if-eqz v0, :cond_3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 136
    add-int/lit8 v1, v1, 0x1

    .line 142
    :goto_1
    sget-object v4, Lcom/samsung/musicplus/contents/normal/ArtistListFragment;->sSavedSongNumber:Landroid/util/SparseIntArray;

    invoke-virtual {v4, v2, v1}, Landroid/util/SparseIntArray;->put(II)V

    .line 143
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-nez v4, :cond_2

    goto :goto_0

    .line 138
    :cond_3
    sget-object v4, Lcom/samsung/musicplus/contents/normal/ArtistListFragment;->sSavedAlbumIndex:Landroid/util/SparseArray;

    invoke-virtual {v4, v2, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 139
    move-object v3, v0

    .line 140
    const/4 v1, 0x1

    goto :goto_1
.end method


# virtual methods
.method protected createAdapter()Landroid/widget/CursorAdapter;
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 79
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/normal/ArtistListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    .line 80
    .local v2, "a":Landroid/app/Activity;
    iget v0, p0, Lcom/samsung/musicplus/contents/normal/ArtistListFragment;->mList:I

    invoke-static {v0}, Lcom/samsung/musicplus/util/ListUtils;->isTab(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 81
    new-instance v0, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTabAdapter;

    const v3, 0x7f040031

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTabAdapter;-><init>(Lcom/samsung/musicplus/contents/normal/ArtistListFragment;Landroid/content/Context;ILandroid/database/Cursor;I)V

    .line 83
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTrackAdapter;

    const v1, 0x7f04003d

    invoke-direct {v0, v2, v1, v4, v5}, Lcom/samsung/musicplus/contents/normal/ArtistListFragment$ArtistListTrackAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;I)V

    goto :goto_0
.end method

.method public getListItemCount()I
    .locals 4

    .prologue
    .line 93
    const/4 v0, 0x0

    .line 94
    .local v0, "count":I
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/normal/ArtistListFragment;->getAbsListView()Landroid/widget/AbsListView;

    move-result-object v1

    .line 95
    .local v1, "lv":Landroid/widget/AbsListView;
    if-eqz v1, :cond_0

    .line 96
    instance-of v2, v1, Lcom/samsung/musicplus/widget/list/MusicListView;

    if-eqz v2, :cond_1

    .line 97
    invoke-virtual {v1}, Landroid/widget/AbsListView;->getCount()I

    move-result v2

    check-cast v1, Lcom/samsung/musicplus/widget/list/MusicListView;

    .end local v1    # "lv":Landroid/widget/AbsListView;
    invoke-virtual {v1}, Lcom/samsung/musicplus/widget/list/MusicListView;->getHeaderViewsCount()I

    move-result v3

    sub-int v0, v2, v3

    .line 102
    :cond_0
    :goto_0
    return v0

    .line 99
    .restart local v1    # "lv":Landroid/widget/AbsListView;
    :cond_1
    invoke-virtual {v1}, Landroid/widget/AbsListView;->getCount()I

    move-result v0

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 56
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 57
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 51
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 52
    return-void
.end method

.method public onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V
    .locals 2
    .param p2, "data"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 61
    .local p1, "loader":Landroid/content/Loader;, "Landroid/content/Loader<Landroid/database/Cursor;>;"
    invoke-super {p0, p1, p2}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V

    .line 62
    iget v1, p0, Lcom/samsung/musicplus/contents/normal/ArtistListFragment;->mList:I

    invoke-static {v1}, Lcom/samsung/musicplus/util/ListUtils;->isTrack(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 63
    iget-object v1, p0, Lcom/samsung/musicplus/contents/normal/ArtistListFragment;->mAdapter:Landroid/widget/CursorAdapter;

    invoke-virtual {v1}, Landroid/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/samsung/musicplus/contents/normal/ArtistListFragment;->saveAlbumIndex(Landroid/database/Cursor;)V

    .line 64
    if-eqz p2, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-nez v1, :cond_1

    .line 65
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/normal/ArtistListFragment;->isSplitSubListFragment()Z

    move-result v1

    if-nez v1, :cond_1

    .line 66
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/normal/ArtistListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    .line 69
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/normal/ArtistListFragment;->getAbsListView()Landroid/widget/AbsListView;

    move-result-object v0

    .line 70
    .local v0, "lv":Landroid/widget/AbsListView;
    if-eqz v0, :cond_2

    instance-of v1, v0, Lcom/samsung/musicplus/widget/list/MusicListView;

    if-eqz v1, :cond_2

    .line 73
    check-cast v0, Lcom/samsung/musicplus/widget/list/MusicListView;

    .end local v0    # "lv":Landroid/widget/AbsListView;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/widget/list/MusicListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 75
    :cond_2
    return-void
.end method

.method public bridge synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Landroid/content/Loader;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 47
    check-cast p2, Landroid/database/Cursor;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/samsung/musicplus/contents/normal/ArtistListFragment;->onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method protected setHeaderView()V
    .locals 0

    .prologue
    .line 88
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/normal/ArtistListFragment;->setListHeaderView()V

    .line 89
    return-void
.end method
