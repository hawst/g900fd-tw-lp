.class Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$2;
.super Landroid/os/Handler;
.source "MoveSecretBoxOperation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;)V
    .locals 0

    .prologue
    .line 172
    iput-object p1, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$2;->this$0:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method private showToastMessage(I)V
    .locals 8
    .param p1, "cnt"    # I

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 231
    iget-object v2, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$2;->this$0:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;

    # getter for: Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mMoveFilesThread:Lcom/samsung/musicplus/contents/PersonalFileOperationThread;
    invoke-static {v2}, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->access$100(Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;)Lcom/samsung/musicplus/contents/PersonalFileOperationThread;

    move-result-object v2

    if-eqz v2, :cond_9

    .line 232
    iget-object v2, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$2;->this$0:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;

    # getter for: Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mMoveFilesThread:Lcom/samsung/musicplus/contents/PersonalFileOperationThread;
    invoke-static {v2}, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->access$100(Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;)Lcom/samsung/musicplus/contents/PersonalFileOperationThread;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->isMovingCmd()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 233
    iget-object v2, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$2;->this$0:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;

    # getter for: Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mMoveFilesThread:Lcom/samsung/musicplus/contents/PersonalFileOperationThread;
    invoke-static {v2}, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->access$100(Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;)Lcom/samsung/musicplus/contents/PersonalFileOperationThread;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->getAlreadyExistPersonalCount()I

    move-result v0

    .line 234
    .local v0, "alreadyPersonal":I
    if-lez v0, :cond_3

    .line 235
    if-ne v0, v6, :cond_0

    if-ne p1, v6, :cond_0

    .line 236
    iget-object v2, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$2;->this$0:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;

    # getter for: Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->access$200(Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;)Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f100008

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 294
    .end local v0    # "alreadyPersonal":I
    .local v1, "message":Ljava/lang/String;
    :goto_0
    iget-object v2, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$2;->this$0:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;

    # getter for: Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->access$200(Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v1, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 295
    return-void

    .line 237
    .end local v1    # "message":Ljava/lang/String;
    .restart local v0    # "alreadyPersonal":I
    :cond_0
    if-ne v0, v6, :cond_1

    if-le p1, v6, :cond_1

    .line 238
    iget-object v2, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$2;->this$0:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;

    # getter for: Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->access$200(Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;)Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f10000b

    new-array v4, v6, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .restart local v1    # "message":Ljava/lang/String;
    goto :goto_0

    .line 239
    .end local v1    # "message":Ljava/lang/String;
    :cond_1
    if-le v0, v6, :cond_2

    if-ne p1, v6, :cond_2

    .line 240
    iget-object v2, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$2;->this$0:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;

    # getter for: Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->access$200(Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;)Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f100009

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .restart local v1    # "message":Ljava/lang/String;
    goto :goto_0

    .line 242
    .end local v1    # "message":Ljava/lang/String;
    :cond_2
    iget-object v2, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$2;->this$0:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;

    # getter for: Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->access$200(Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;)Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f10000a

    new-array v4, v6, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .restart local v1    # "message":Ljava/lang/String;
    goto :goto_0

    .line 245
    .end local v1    # "message":Ljava/lang/String;
    :cond_3
    if-ne p1, v6, :cond_4

    .line 246
    iget-object v2, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$2;->this$0:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;

    # getter for: Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->access$200(Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;)Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f10000d

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .restart local v1    # "message":Ljava/lang/String;
    goto :goto_0

    .line 248
    .end local v1    # "message":Ljava/lang/String;
    :cond_4
    iget-object v2, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$2;->this$0:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;

    # getter for: Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->access$200(Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;)Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f10000e

    new-array v4, v6, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .restart local v1    # "message":Ljava/lang/String;
    goto :goto_0

    .line 258
    .end local v0    # "alreadyPersonal":I
    .end local v1    # "message":Ljava/lang/String;
    :cond_5
    iget-object v2, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$2;->this$0:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;

    # getter for: Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mMoveFilesThread:Lcom/samsung/musicplus/contents/PersonalFileOperationThread;
    invoke-static {v2}, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->access$100(Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;)Lcom/samsung/musicplus/contents/PersonalFileOperationThread;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/musicplus/contents/PersonalFileOperationThread;->getSelectionDestPath()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_7

    .line 259
    if-ne p1, v6, :cond_6

    .line 260
    iget-object v2, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$2;->this$0:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;

    # getter for: Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->access$200(Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;)Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f100136

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .restart local v1    # "message":Ljava/lang/String;
    goto/16 :goto_0

    .line 263
    .end local v1    # "message":Ljava/lang/String;
    :cond_6
    iget-object v2, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$2;->this$0:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;

    # getter for: Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->access$200(Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;)Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f100138

    new-array v4, v6, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .restart local v1    # "message":Ljava/lang/String;
    goto/16 :goto_0

    .line 268
    .end local v1    # "message":Ljava/lang/String;
    :cond_7
    if-ne p1, v6, :cond_8

    .line 269
    iget-object v2, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$2;->this$0:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;

    # getter for: Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->access$200(Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;)Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f100135

    new-array v4, v6, [Ljava/lang/Object;

    sget-object v5, Lcom/samsung/musicplus/library/storage/PrivateMode;->RESTORE_MUSIC_PATH:Ljava/lang/String;

    aput-object v5, v4, v7

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .restart local v1    # "message":Ljava/lang/String;
    goto/16 :goto_0

    .line 273
    .end local v1    # "message":Ljava/lang/String;
    :cond_8
    iget-object v2, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$2;->this$0:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;

    # getter for: Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->access$200(Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;)Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f100137

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    sget-object v5, Lcom/samsung/musicplus/library/storage/PrivateMode;->RESTORE_MUSIC_PATH:Ljava/lang/String;

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .restart local v1    # "message":Ljava/lang/String;
    goto/16 :goto_0

    .line 291
    .end local v1    # "message":Ljava/lang/String;
    :cond_9
    iget-object v2, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$2;->this$0:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;

    # getter for: Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->access$200(Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;)Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f100060

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .restart local v1    # "message":Ljava/lang/String;
    goto/16 :goto_0
.end method


# virtual methods
.method public declared-synchronized handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 174
    monitor-enter p0

    :try_start_0
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 221
    iget-object v1, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$2;->this$0:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;

    iget-object v1, v1, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mPmMgr:Lcom/samsung/android/privatemode/PrivateModeManager;

    if-eqz v1, :cond_0

    .line 222
    const-string v1, "PersonalFileOperation"

    const-string v2, "unregister PrivateModeManager"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 223
    iget-object v1, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$2;->this$0:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;

    iget-object v1, v1, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mPmMgr:Lcom/samsung/android/privatemode/PrivateModeManager;

    iget-object v2, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$2;->this$0:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;

    iget-object v2, v2, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mToken:Landroid/os/IBinder;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/privatemode/PrivateModeManager;->unregisterClient(Landroid/os/IBinder;Z)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 227
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 176
    :pswitch_0
    :try_start_1
    const-string v1, "PersonalFileOperation"

    const-string v2, "mHandler - CANCEL_MOVE_PROGRESS"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$2;->showToastMessage(I)V

    .line 178
    iget-object v1, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$2;->this$0:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;

    # getter for: Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mOnMoveSecretBoxListener:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$OnMoveSecretBoxListener;
    invoke-static {v1}, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->access$000(Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;)Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$OnMoveSecretBoxListener;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 179
    iget-object v1, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$2;->this$0:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;

    # getter for: Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mOnMoveSecretBoxListener:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$OnMoveSecretBoxListener;
    invoke-static {v1}, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->access$000(Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;)Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$OnMoveSecretBoxListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$OnMoveSecretBoxListener;->onMoveFinished()V

    .line 181
    :cond_1
    iget-object v1, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$2;->this$0:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;

    iget-object v1, v1, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mPmMgr:Lcom/samsung/android/privatemode/PrivateModeManager;

    if-eqz v1, :cond_0

    .line 182
    const-string v1, "PersonalFileOperation"

    const-string v2, "unregister PrivateModeManager"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    iget-object v1, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$2;->this$0:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;

    iget-object v1, v1, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mPmMgr:Lcom/samsung/android/privatemode/PrivateModeManager;

    iget-object v2, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$2;->this$0:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;

    iget-object v2, v2, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mToken:Landroid/os/IBinder;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/privatemode/PrivateModeManager;->unregisterClient(Landroid/os/IBinder;Z)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 174
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1

    .line 187
    :pswitch_1
    :try_start_2
    const-string v1, "PersonalFileOperation"

    const-string v2, "mHandler - FINISH_MOVE_PROGRESS"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 188
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$2;->showToastMessage(I)V

    .line 189
    iget-object v1, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$2;->this$0:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;

    # getter for: Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mOnMoveSecretBoxListener:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$OnMoveSecretBoxListener;
    invoke-static {v1}, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->access$000(Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;)Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$OnMoveSecretBoxListener;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 190
    iget-object v1, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$2;->this$0:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;

    # getter for: Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mOnMoveSecretBoxListener:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$OnMoveSecretBoxListener;
    invoke-static {v1}, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->access$000(Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;)Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$OnMoveSecretBoxListener;

    move-result-object v1

    invoke-interface {v1}, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$OnMoveSecretBoxListener;->onMoveFinished()V

    .line 192
    :cond_2
    iget-object v1, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$2;->this$0:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;

    iget-object v1, v1, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mPmMgr:Lcom/samsung/android/privatemode/PrivateModeManager;

    if-eqz v1, :cond_0

    .line 193
    const-string v1, "PersonalFileOperation"

    const-string v2, "unregister PrivateModeManager"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 194
    iget-object v1, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$2;->this$0:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;

    iget-object v1, v1, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mPmMgr:Lcom/samsung/android/privatemode/PrivateModeManager;

    iget-object v2, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$2;->this$0:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;

    iget-object v2, v2, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mToken:Landroid/os/IBinder;

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/privatemode/PrivateModeManager;->unregisterClient(Landroid/os/IBinder;Z)Z

    goto :goto_0

    .line 198
    :pswitch_2
    iget-object v1, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$2;->this$0:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;

    # getter for: Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mOnMoveSecretBoxListener:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$OnMoveSecretBoxListener;
    invoke-static {v1}, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->access$000(Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;)Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$OnMoveSecretBoxListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 199
    iget-object v1, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$2;->this$0:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;

    # getter for: Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mOnMoveSecretBoxListener:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$OnMoveSecretBoxListener;
    invoke-static {v1}, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->access$000(Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;)Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$OnMoveSecretBoxListener;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->arg1:I

    iget v3, p1, Landroid/os/Message;->arg2:I

    invoke-interface {v1, v2, v3}, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$OnMoveSecretBoxListener;->onUpdateProgress(II)V

    goto/16 :goto_0

    .line 203
    :pswitch_3
    const-string v1, "PersonalFileOperation"

    const-string v2, "mHandler - ABORT_MOVE_PROGRESS"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 204
    iget-object v1, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$2;->this$0:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;

    # getter for: Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->access$200(Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f10010b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 206
    .local v0, "error":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$2;->this$0:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;

    # getter for: Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mOnMoveSecretBoxListener:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$OnMoveSecretBoxListener;
    invoke-static {v1}, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->access$000(Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;)Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$OnMoveSecretBoxListener;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 207
    iget-object v1, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$2;->this$0:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;

    # getter for: Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mOnMoveSecretBoxListener:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$OnMoveSecretBoxListener;
    invoke-static {v1}, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->access$000(Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;)Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$OnMoveSecretBoxListener;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$OnMoveSecretBoxListener;->onAbortMove(Ljava/lang/String;)V

    .line 209
    :cond_3
    iget-object v1, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$2;->this$0:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;

    iget-object v1, v1, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mPmMgr:Lcom/samsung/android/privatemode/PrivateModeManager;

    if-eqz v1, :cond_0

    .line 210
    const-string v1, "PersonalFileOperation"

    const-string v2, "unregister PrivateModeManager"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 211
    iget-object v1, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$2;->this$0:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;

    iget-object v1, v1, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mPmMgr:Lcom/samsung/android/privatemode/PrivateModeManager;

    iget-object v2, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$2;->this$0:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;

    iget-object v2, v2, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mToken:Landroid/os/IBinder;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/privatemode/PrivateModeManager;->unregisterClient(Landroid/os/IBinder;Z)Z

    goto/16 :goto_0

    .line 215
    .end local v0    # "error":Ljava/lang/String;
    :pswitch_4
    const-string v1, "PersonalFileOperation"

    const-string v2, "mHandler - FILE_ALREADY_EXIST"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 216
    iget-object v1, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$2;->this$0:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;

    # getter for: Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mOnMoveSecretBoxListener:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$OnMoveSecretBoxListener;
    invoke-static {v1}, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->access$000(Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;)Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$OnMoveSecretBoxListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 217
    iget-object v1, p0, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$2;->this$0:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;

    # getter for: Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->mOnMoveSecretBoxListener:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$OnMoveSecretBoxListener;
    invoke-static {v1}, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->access$000(Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;)Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$OnMoveSecretBoxListener;

    move-result-object v2

    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-interface {v2, v1}, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$OnMoveSecretBoxListener;->onFileNameExist(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 174
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
