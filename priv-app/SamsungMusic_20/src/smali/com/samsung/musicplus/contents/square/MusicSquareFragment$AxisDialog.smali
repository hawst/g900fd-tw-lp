.class public Lcom/samsung/musicplus/contents/square/MusicSquareFragment$AxisDialog;
.super Landroid/app/DialogFragment;
.source "MusicSquareFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/contents/square/MusicSquareFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AxisDialog"
.end annotation


# static fields
.field public static final MOODS:I = 0x0

.field public static final PREF_KEY_SQUARE_THEME:Ljava/lang/String; = "square_theme"

.field public static final YEARS:I = 0x1


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1063
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x0

    .line 1072
    invoke-virtual {p0, v4, v4}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$AxisDialog;->setStyle(II)V

    .line 1075
    # getter for: Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->sPreferences:Landroid/content/SharedPreferences;
    invoke-static {}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;->access$2300()Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "square_theme"

    invoke-interface {v1, v2, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 1076
    .local v0, "axis":I
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$AxisDialog;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f10003b

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const v3, 0x7f1000cb

    invoke-virtual {p0, v3}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$AxisDialog;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    const/4 v3, 0x1

    const v4, 0x7f1001d8

    invoke-virtual {p0, v4}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$AxisDialog;->getString(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    new-instance v3, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$AxisDialog$2;

    invoke-direct {v3, p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$AxisDialog$2;-><init>(Lcom/samsung/musicplus/contents/square/MusicSquareFragment$AxisDialog;)V

    invoke-virtual {v1, v2, v0, v3}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f10003a

    new-instance v3, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$AxisDialog$1;

    invoke-direct {v3, p0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment$AxisDialog$1;-><init>(Lcom/samsung/musicplus/contents/square/MusicSquareFragment$AxisDialog;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    return-object v1
.end method
