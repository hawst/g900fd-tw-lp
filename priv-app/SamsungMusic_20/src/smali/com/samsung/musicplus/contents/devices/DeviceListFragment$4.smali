.class Lcom/samsung/musicplus/contents/devices/DeviceListFragment$4;
.super Landroid/database/ContentObserver;
.source "DeviceListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->addObserver(Landroid/content/Context;Landroid/widget/TextView;J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/contents/devices/DeviceListFragment;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/contents/devices/DeviceListFragment;Landroid/os/Handler;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Handler;

    .prologue
    .line 1186
    iput-object p1, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$4;->this$0:Lcom/samsung/musicplus/contents/devices/DeviceListFragment;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 8
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    .line 1190
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 1191
    .local v0, "now":J
    iget-object v2, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$4;->this$0:Lcom/samsung/musicplus/contents/devices/DeviceListFragment;

    # getter for: Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mLastLoadCompleteTime:J
    invoke-static {v2}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->access$2300(Lcom/samsung/musicplus/contents/devices/DeviceListFragment;)J

    move-result-wide v2

    iget-object v4, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$4;->this$0:Lcom/samsung/musicplus/contents/devices/DeviceListFragment;

    # getter for: Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mUpdateThrottle:I
    invoke-static {v4}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->access$2600(Lcom/samsung/musicplus/contents/devices/DeviceListFragment;)I

    move-result v4

    int-to-long v4, v4

    add-long/2addr v2, v4

    cmp-long v2, v0, v2

    if-gez v2, :cond_1

    .line 1192
    iget-object v2, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$4;->this$0:Lcom/samsung/musicplus/contents/devices/DeviceListFragment;

    # getter for: Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mWaiting:Z
    invoke-static {v2}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->access$2500(Lcom/samsung/musicplus/contents/devices/DeviceListFragment;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1193
    iget-object v2, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$4;->this$0:Lcom/samsung/musicplus/contents/devices/DeviceListFragment;

    # getter for: Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mListUpdater:Landroid/os/Handler;
    invoke-static {v2}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->access$2400(Lcom/samsung/musicplus/contents/devices/DeviceListFragment;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$4;->this$0:Lcom/samsung/musicplus/contents/devices/DeviceListFragment;

    # getter for: Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mUpdateNotifier:Ljava/lang/Runnable;
    invoke-static {v3}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->access$2700(Lcom/samsung/musicplus/contents/devices/DeviceListFragment;)Ljava/lang/Runnable;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$4;->this$0:Lcom/samsung/musicplus/contents/devices/DeviceListFragment;

    # getter for: Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mLastLoadCompleteTime:J
    invoke-static {v4}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->access$2300(Lcom/samsung/musicplus/contents/devices/DeviceListFragment;)J

    move-result-wide v4

    iget-object v6, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$4;->this$0:Lcom/samsung/musicplus/contents/devices/DeviceListFragment;

    # getter for: Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mUpdateThrottle:I
    invoke-static {v6}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->access$2600(Lcom/samsung/musicplus/contents/devices/DeviceListFragment;)I

    move-result v6

    int-to-long v6, v6

    add-long/2addr v4, v6

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postAtTime(Ljava/lang/Runnable;J)Z

    .line 1195
    iget-object v2, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$4;->this$0:Lcom/samsung/musicplus/contents/devices/DeviceListFragment;

    const/4 v3, 0x1

    # setter for: Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mWaiting:Z
    invoke-static {v2, v3}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->access$2502(Lcom/samsung/musicplus/contents/devices/DeviceListFragment;Z)Z

    .line 1200
    :cond_0
    :goto_0
    return-void

    .line 1199
    :cond_1
    iget-object v2, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$4;->this$0:Lcom/samsung/musicplus/contents/devices/DeviceListFragment;

    # getter for: Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mListUpdater:Landroid/os/Handler;
    invoke-static {v2}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->access$2400(Lcom/samsung/musicplus/contents/devices/DeviceListFragment;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$4;->this$0:Lcom/samsung/musicplus/contents/devices/DeviceListFragment;

    # getter for: Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mUpdateNotifier:Ljava/lang/Runnable;
    invoke-static {v3}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->access$2700(Lcom/samsung/musicplus/contents/devices/DeviceListFragment;)Ljava/lang/Runnable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method
