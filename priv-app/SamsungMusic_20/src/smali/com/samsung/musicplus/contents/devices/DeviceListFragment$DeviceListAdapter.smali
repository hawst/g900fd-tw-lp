.class Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;
.super Lcom/samsung/musicplus/widget/list/CommonListAdapter;
.source "DeviceListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/contents/devices/DeviceListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DeviceListAdapter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$DeviceListViewHolder;
    }
.end annotation


# instance fields
.field private mDeviceOffTextColor:I

.field private mDeviceOnTextColor:I

.field private mDialog:Landroid/app/AlertDialog;

.field private mProgressDialog:Landroid/app/ProgressDialog;

.field final synthetic this$0:Lcom/samsung/musicplus/contents/devices/DeviceListFragment;


# direct methods
.method public constructor <init>(Lcom/samsung/musicplus/contents/devices/DeviceListFragment;Landroid/content/Context;ILandroid/database/Cursor;I)V
    .locals 2
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "layout"    # I
    .param p4, "c"    # Landroid/database/Cursor;
    .param p5, "flags"    # I

    .prologue
    const/4 v0, 0x0

    .line 672
    iput-object p1, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->this$0:Lcom/samsung/musicplus/contents/devices/DeviceListFragment;

    .line 673
    invoke-direct {p0, p2, p3, p4, p5}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;I)V

    .line 664
    iput-object v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->mDialog:Landroid/app/AlertDialog;

    .line 670
    iput-object v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 674
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b002b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->mDeviceOnTextColor:I

    .line 675
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0029

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->mDeviceOffTextColor:I

    .line 676
    new-instance v0, Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 677
    return-void
.end method

.method static synthetic access$1000(Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;

    .prologue
    .line 658
    iget-object v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;

    .prologue
    .line 658
    iget-object v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;Landroid/view/View;Landroid/view/View;Landroid/widget/TextView;J)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;
    .param p1, "x1"    # Landroid/view/View;
    .param p2, "x2"    # Landroid/view/View;
    .param p3, "x3"    # Landroid/widget/TextView;
    .param p4, "x4"    # J

    .prologue
    .line 658
    invoke-direct/range {p0 .. p5}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->showProgress(Landroid/view/View;Landroid/view/View;Landroid/widget/TextView;J)V

    return-void
.end method

.method static synthetic access$1400(Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;

    .prologue
    .line 658
    iget-object v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;

    .prologue
    .line 658
    iget-object v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;

    .prologue
    .line 658
    iget-object v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;)Landroid/app/ProgressDialog;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;

    .prologue
    .line 658
    iget-object v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;

    .prologue
    .line 658
    iget-object v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;JLandroid/widget/TextView;)V
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;
    .param p1, "x1"    # J
    .param p3, "x2"    # Landroid/widget/TextView;

    .prologue
    .line 658
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->updateText(JLandroid/widget/TextView;)V

    return-void
.end method

.method static synthetic access$800(Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;

    .prologue
    .line 658
    iget-object v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$900(Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;

    .prologue
    .line 658
    iget-object v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private bindFixedView(Landroid/content/Context;Landroid/view/View;Landroid/database/Cursor;J)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "c"    # Landroid/database/Cursor;
    .param p4, "id"    # J

    .prologue
    const/4 v5, 0x1

    .line 1026
    invoke-direct {p0, p4, p5}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->isCategory(J)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1027
    invoke-interface {p3}, Landroid/database/Cursor;->isFirst()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1028
    const v4, 0x7f0d007f

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 1029
    .local v1, "divider":Landroid/view/View;
    if-eqz v1, :cond_0

    .line 1030
    const/16 v4, 0x8

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1033
    .end local v1    # "divider":Landroid/view/View;
    :cond_0
    const v4, 0x7f0d0081

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 1035
    .local v2, "text":Landroid/widget/TextView;
    invoke-interface {p3, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1036
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p3, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->this$0:Lcom/samsung/musicplus/contents/devices/DeviceListFragment;

    invoke-virtual {v5}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f10018b

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1037
    const v4, 0x7f0d0080

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 1038
    .local v3, "text2":Landroid/widget/TextView;
    const/4 v0, 0x0

    .line 1039
    .local v0, "count":I
    const-wide/16 v4, -0x12

    cmp-long v4, v4, p4

    if-nez v4, :cond_3

    .line 1040
    iget-object v4, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->this$0:Lcom/samsung/musicplus/contents/devices/DeviceListFragment;

    # getter for: Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mDmsCount:I
    invoke-static {v4}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->access$500(Lcom/samsung/musicplus/contents/devices/DeviceListFragment;)I

    move-result v0

    .line 1044
    :cond_1
    :goto_0
    iget-object v4, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->this$0:Lcom/samsung/musicplus/contents/devices/DeviceListFragment;

    # invokes: Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->setNumberOfTextView(Landroid/widget/TextView;Landroid/content/Context;I)V
    invoke-static {v4, v3, p1, v0}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->access$1600(Lcom/samsung/musicplus/contents/devices/DeviceListFragment;Landroid/widget/TextView;Landroid/content/Context;I)V

    .line 1054
    .end local v0    # "count":I
    .end local v2    # "text":Landroid/widget/TextView;
    .end local v3    # "text2":Landroid/widget/TextView;
    :cond_2
    :goto_1
    return-void

    .line 1041
    .restart local v0    # "count":I
    .restart local v2    # "text":Landroid/widget/TextView;
    .restart local v3    # "text2":Landroid/widget/TextView;
    :cond_3
    const-wide/16 v4, -0x13

    cmp-long v4, v4, p4

    if-nez v4, :cond_1

    .line 1042
    iget-object v4, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->this$0:Lcom/samsung/musicplus/contents/devices/DeviceListFragment;

    # getter for: Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mDeviceCount:I
    invoke-static {v4}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->access$600(Lcom/samsung/musicplus/contents/devices/DeviceListFragment;)I

    move-result v0

    goto :goto_0

    .line 1047
    .end local v0    # "count":I
    .end local v2    # "text":Landroid/widget/TextView;
    .end local v3    # "text2":Landroid/widget/TextView;
    :cond_4
    invoke-direct {p0, p4, p5}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->isNoitem(J)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1048
    const v4, 0x7f0d0049

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 1049
    .restart local v2    # "text":Landroid/widget/TextView;
    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1051
    invoke-interface {p3, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method private bindSlinkDeviceView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;J)V
    .locals 18
    .param p1, "v"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "c"    # Landroid/database/Cursor;
    .param p4, "id"    # J

    .prologue
    .line 950
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$DeviceListViewHolder;

    .line 951
    .local v12, "vh":Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$DeviceListViewHolder;
    move-wide/from16 v6, p4

    .line 952
    .local v6, "deviceId":J
    invoke-static/range {p3 .. p3}, Lcom/samsung/musicplus/provider/MusicContents$Audio$Devices;->getName(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v8

    .line 953
    .local v8, "deviceName":Ljava/lang/String;
    invoke-static/range {p3 .. p3}, Lcom/samsung/musicplus/provider/MusicContents$Audio$Devices;->isOff(Landroid/database/Cursor;)Z

    move-result v9

    .line 955
    .local v9, "isOff":Z
    move-object/from16 v0, p2

    move-wide/from16 v1, p4

    invoke-static {v0, v1, v2, v9}, Lcom/samsung/musicplus/provider/MusicContents$Audio$Devices;->getDeviceIcon(Landroid/content/Context;JZ)Landroid/graphics/Bitmap;

    move-result-object v4

    .line 956
    .local v4, "bm":Landroid/graphics/Bitmap;
    iget-object v13, v12, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$DeviceListViewHolder;->thumbnail:Landroid/widget/ImageView;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 957
    iget-object v13, v12, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$DeviceListViewHolder;->thumbnail:Landroid/widget/ImageView;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 958
    iget-object v13, v12, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$DeviceListViewHolder;->thumbnail:Landroid/widget/ImageView;

    invoke-virtual {v13, v4}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 963
    invoke-static/range {p3 .. p3}, Lcom/samsung/musicplus/provider/MusicContents$Audio$Devices;->isWebStorage(Landroid/database/Cursor;)Z

    move-result v13

    if-eqz v13, :cond_1

    .line 964
    iget-object v13, v12, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$DeviceListViewHolder;->text1:Landroid/widget/TextView;

    invoke-virtual {v13, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 965
    iget-object v13, v12, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$DeviceListViewHolder;->text1:Landroid/widget/TextView;

    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v14

    const v15, 0x7f0b002b

    invoke-virtual {v14, v15}, Landroid/content/res/Resources;->getColor(I)I

    move-result v14

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setTextColor(I)V

    .line 966
    iget-object v13, v12, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$DeviceListViewHolder;->text2:Landroid/widget/TextView;

    const/16 v14, 0x8

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setVisibility(I)V

    .line 967
    iget-object v13, v12, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$DeviceListViewHolder;->text3:Landroid/widget/TextView;

    const/16 v14, 0x8

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setVisibility(I)V

    .line 996
    :goto_0
    iget-object v13, v12, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$DeviceListViewHolder;->unregisterBtn:Landroid/widget/ImageView;

    if-eqz v13, :cond_0

    .line 997
    iget-object v13, v12, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$DeviceListViewHolder;->unregisterBtn:Landroid/widget/ImageView;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 998
    iget-object v13, v12, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$DeviceListViewHolder;->unregisterBtn:Landroid/widget/ImageView;

    new-instance v14, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$7;

    move-object/from16 v0, p0

    invoke-direct {v14, v0, v6, v7, v8}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$7;-><init>(Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;JLjava/lang/String;)V

    invoke-virtual {v13, v14}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1005
    :cond_0
    return-void

    .line 969
    :cond_1
    iget-object v13, v12, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$DeviceListViewHolder;->text1:Landroid/widget/TextView;

    invoke-virtual {v13, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 970
    iget-object v13, v12, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$DeviceListViewHolder;->text2:Landroid/widget/TextView;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setVisibility(I)V

    .line 971
    iget-object v13, v12, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$DeviceListViewHolder;->text3:Landroid/widget/TextView;

    const/4 v14, 0x0

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setVisibility(I)V

    .line 972
    if-eqz v9, :cond_2

    .line 973
    iget-object v13, v12, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$DeviceListViewHolder;->text1:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->mDeviceOffTextColor:I

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setTextColor(I)V

    .line 974
    iget-object v13, v12, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$DeviceListViewHolder;->text2:Landroid/widget/TextView;

    const v14, 0x7f10010a

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setText(I)V

    .line 975
    iget-object v13, v12, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$DeviceListViewHolder;->text2:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->mDeviceOffTextColor:I

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setTextColor(I)V

    .line 976
    iget-object v13, v12, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$DeviceListViewHolder;->text3:Landroid/widget/TextView;

    const-string v14, ""

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 977
    iget-object v13, v12, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$DeviceListViewHolder;->text3:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->mDeviceOffTextColor:I

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    .line 979
    :cond_2
    iget-object v13, v12, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$DeviceListViewHolder;->text1:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->mDeviceOnTextColor:I

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setTextColor(I)V

    .line 980
    iget-object v13, v12, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$DeviceListViewHolder;->text3:Landroid/widget/TextView;

    invoke-static/range {p2 .. p3}, Lcom/samsung/musicplus/provider/MusicContents$Audio$Devices;->getConnectState(Landroid/content/Context;Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 981
    iget-object v13, v12, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$DeviceListViewHolder;->text3:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget v14, v0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->mDeviceOnTextColor:I

    invoke-virtual {v13, v14}, Landroid/widget/TextView;->setTextColor(I)V

    .line 983
    invoke-static/range {p4 .. p5}, Lcom/samsung/musicplus/provider/MusicContents$Audio$Devices;->getContentsUri(J)Landroid/net/Uri;

    move-result-object v11

    .line 984
    .local v11, "uri":Landroid/net/Uri;
    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->getCount(Landroid/net/Uri;)I

    move-result v5

    .line 985
    .local v5, "count":I
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v13}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v13

    const v14, 0x7f0f000a

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-virtual {v13, v14, v5, v15}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    .line 987
    .local v10, "string":Ljava/lang/String;
    iget-object v13, v12, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$DeviceListViewHolder;->text2:Landroid/widget/TextView;

    invoke-virtual {v13, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0
.end method

.method private getCount(Landroid/net/Uri;)I
    .locals 8
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 1008
    const/4 v7, 0x0

    .line 1009
    .local v7, "count":I
    const/4 v6, 0x0

    .line 1011
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    iget-object v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v3, "count(*)"

    aput-object v3, v2, v1

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1014
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1015
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v7

    .line 1018
    :cond_0
    if-eqz v6, :cond_1

    .line 1019
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1022
    :cond_1
    return v7

    .line 1018
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 1019
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method private getFixedView(Landroid/content/Context;Landroid/view/ViewGroup;J)Landroid/view/View;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "parent"    # Landroid/view/ViewGroup;
    .param p3, "id"    # J

    .prologue
    .line 781
    const-wide/16 v0, -0x13

    cmp-long v0, v0, p3

    if-nez v0, :cond_0

    .line 782
    invoke-direct {p0, p1, p2}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->getSlinkDeviceCategoryView(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 792
    :goto_0
    return-object v0

    .line 784
    :cond_0
    const-wide/16 v0, -0x12

    cmp-long v0, v0, p3

    if-nez v0, :cond_1

    .line 786
    invoke-direct {p0, p1, p2}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->getNearByDeviceCategoryView(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 788
    :cond_1
    invoke-direct {p0, p3, p4}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->isNoitem(J)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 790
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->getNoitemView(Landroid/content/Context;Landroid/view/ViewGroup;J)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 792
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getNearByDeviceCategoryView(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v9, 0x0

    .line 796
    const-string v8, "layout_inflater"

    invoke-virtual {p1, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/LayoutInflater;

    .line 798
    .local v4, "li":Landroid/view/LayoutInflater;
    const v8, 0x7f04001b

    invoke-virtual {v4, v8, p2, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v7

    .line 799
    .local v7, "v":Landroid/view/View;
    const v8, 0x7f0d0083

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .line 800
    .local v5, "progress":Landroid/view/View;
    const v8, 0x7f0d0082

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 801
    .local v0, "button":Landroid/view/View;
    const v8, 0x7f0d0080

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 802
    .local v6, "text":Landroid/widget/TextView;
    const v8, 0x7f0d007f

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 803
    .local v1, "divider":Landroid/view/View;
    if-eqz v1, :cond_0

    .line 804
    invoke-virtual {v1, v9}, Landroid/view/View;->setVisibility(I)V

    .line 806
    :cond_0
    const-wide/16 v2, -0x12

    .line 807
    .local v2, "id":J
    iget-object v8, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->this$0:Lcom/samsung/musicplus/contents/devices/DeviceListFragment;

    invoke-virtual {v8}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v8

    invoke-virtual {v8}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f10005d

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 808
    new-instance v8, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$2;

    invoke-direct {v8, p0, v0, v5, v6}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$2;-><init>(Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;Landroid/view/View;Landroid/view/View;Landroid/widget/TextView;)V

    invoke-virtual {v0, v8}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 825
    return-object v7
.end method

.method private getNoitemView(Landroid/content/Context;Landroid/view/ViewGroup;J)Landroid/view/View;
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "parent"    # Landroid/view/ViewGroup;
    .param p3, "id"    # J

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x0

    .line 861
    const-string v4, "layout_inflater"

    invoke-virtual {p1, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 863
    .local v2, "li":Landroid/view/LayoutInflater;
    const v4, 0x7f040004

    invoke-virtual {v2, v4, p2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 865
    .local v3, "root":Landroid/view/View;
    const v4, 0x7f0d0048

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 866
    .local v1, "icon":Landroid/view/View;
    invoke-virtual {v1, v7}, Landroid/view/View;->setVisibility(I)V

    .line 873
    const v4, 0x7f0d004a

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 874
    .local v0, "button":Landroid/view/View;
    const-wide/16 v4, -0x16

    cmp-long v4, v4, p3

    if-nez v4, :cond_1

    .line 875
    iget-object v4, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->this$0:Lcom/samsung/musicplus/contents/devices/DeviceListFragment;

    # getter for: Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mHasSamsungAccount:Z
    invoke-static {v4}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->access$200(Lcom/samsung/musicplus/contents/devices/DeviceListFragment;)Z

    move-result v4

    if-eqz v4, :cond_0

    move-object v4, v0

    .line 876
    check-cast v4, Landroid/widget/Button;

    const v5, 0x7f100130

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setText(I)V

    .line 880
    :goto_0
    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 881
    new-instance v4, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$4;

    invoke-direct {v4, p0}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$4;-><init>(Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;)V

    invoke-virtual {v0, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 888
    new-instance v4, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$5;

    invoke-direct {v4, p0, v0}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$5;-><init>(Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;Landroid/view/View;)V

    invoke-virtual {v3, v4}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 908
    :goto_1
    return-object v3

    :cond_0
    move-object v4, v0

    .line 878
    check-cast v4, Landroid/widget/Button;

    const v5, 0x7f100159

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setText(I)V

    goto :goto_0

    .line 895
    :cond_1
    const-wide/16 v4, -0x17

    cmp-long v4, v4, p3

    if-nez v4, :cond_2

    move-object v4, v0

    .line 896
    check-cast v4, Landroid/widget/Button;

    const v5, 0x7f100134

    invoke-virtual {v4, v5}, Landroid/widget/Button;->setText(I)V

    .line 897
    invoke-virtual {v0, v6}, Landroid/view/View;->setVisibility(I)V

    .line 898
    new-instance v4, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$6;

    invoke-direct {v4, p0}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$6;-><init>(Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;)V

    invoke-virtual {v0, v4}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    .line 906
    :cond_2
    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method private getSlinkDeviceCategoryView(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 831
    const-string v7, "layout_inflater"

    invoke-virtual {p1, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 833
    .local v1, "li":Landroid/view/LayoutInflater;
    const v7, 0x7f04001b

    const/4 v8, 0x0

    invoke-virtual {v1, v7, p2, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v6

    .line 834
    .local v6, "v":Landroid/view/View;
    const v7, 0x7f0d0083

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 835
    .local v4, "progress":Landroid/view/View;
    const v7, 0x7f0d0082

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 836
    .local v0, "button":Landroid/view/View;
    const v7, 0x7f0d0080

    invoke-virtual {v6, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 837
    .local v5, "text":Landroid/widget/TextView;
    const-wide/16 v2, -0x13

    .line 838
    .local v2, "id":J
    iget-object v7, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->this$0:Lcom/samsung/musicplus/contents/devices/DeviceListFragment;

    invoke-virtual {v7}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f100144

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 839
    new-instance v7, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$3;

    invoke-direct {v7, p0, v0, v4, v5}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$3;-><init>(Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;Landroid/view/View;Landroid/view/View;Landroid/widget/TextView;)V

    invoke-virtual {v0, v7}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 852
    return-object v6
.end method

.method private isCategory(J)Z
    .locals 3
    .param p1, "id"    # J

    .prologue
    .line 684
    const-wide/16 v0, -0x13

    cmp-long v0, v0, p1

    if-eqz v0, :cond_0

    const-wide/16 v0, -0x12

    cmp-long v0, v0, p1

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isFixedView(J)Z
    .locals 1
    .param p1, "id"    # J

    .prologue
    .line 680
    invoke-direct {p0, p1, p2}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->isCategory(J)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p1, p2}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->isNoitem(J)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isNoitem(J)Z
    .locals 3
    .param p1, "id"    # J

    .prologue
    .line 688
    const-wide/16 v0, -0x14

    cmp-long v0, v0, p1

    if-eqz v0, :cond_0

    const-wide/16 v0, -0x15

    cmp-long v0, v0, p1

    if-eqz v0, :cond_0

    const-wide/16 v0, -0x16

    cmp-long v0, v0, p1

    if-eqz v0, :cond_0

    const-wide/16 v0, -0x17

    cmp-long v0, v0, p1

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isSlinkDeviceOff(I)Z
    .locals 3
    .param p1, "position"    # I

    .prologue
    const/4 v1, 0x0

    .line 728
    iget-object v2, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->this$0:Lcom/samsung/musicplus/contents/devices/DeviceListFragment;

    # invokes: Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->isDmsItem(I)Z
    invoke-static {v2, p1}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->access$300(Lcom/samsung/musicplus/contents/devices/DeviceListFragment;I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 735
    :cond_0
    :goto_0
    return v1

    .line 731
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 732
    .local v0, "c":Landroid/database/Cursor;
    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 733
    invoke-static {v0}, Lcom/samsung/musicplus/provider/MusicContents$Audio$Devices;->isOff(Landroid/database/Cursor;)Z

    move-result v1

    goto :goto_0
.end method

.method private showProgress(Landroid/view/View;Landroid/view/View;Landroid/widget/TextView;J)V
    .locals 8
    .param p1, "button"    # Landroid/view/View;
    .param p2, "progress"    # Landroid/view/View;
    .param p3, "text"    # Landroid/widget/TextView;
    .param p4, "id"    # J

    .prologue
    .line 750
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 751
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 752
    const v0, 0x7f10014d

    invoke-virtual {p3, v0}, Landroid/widget/TextView;->setText(I)V

    .line 753
    new-instance v0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$1;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p4

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$1;-><init>(Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;Landroid/view/View;Landroid/view/View;JLandroid/widget/TextView;)V

    const-wide/16 v2, 0x1388

    invoke-virtual {p1, v0, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 762
    return-void
.end method

.method private updateText(JLandroid/widget/TextView;)V
    .locals 5
    .param p1, "id"    # J
    .param p3, "text"    # Landroid/widget/TextView;

    .prologue
    .line 765
    const/4 v1, 0x0

    .line 766
    .local v1, "count":I
    const-wide/16 v2, -0x12

    cmp-long v2, v2, p1

    if-nez v2, :cond_2

    .line 767
    iget-object v2, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->this$0:Lcom/samsung/musicplus/contents/devices/DeviceListFragment;

    # getter for: Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mDmsCount:I
    invoke-static {v2}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->access$500(Lcom/samsung/musicplus/contents/devices/DeviceListFragment;)I

    move-result v1

    .line 774
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->this$0:Lcom/samsung/musicplus/contents/devices/DeviceListFragment;

    invoke-virtual {v2}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 775
    .local v0, "context":Landroid/content/Context;
    if-eqz v0, :cond_1

    .line 776
    iget-object v2, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->this$0:Lcom/samsung/musicplus/contents/devices/DeviceListFragment;

    # invokes: Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->setNumberOfTextView(Landroid/widget/TextView;Landroid/content/Context;I)V
    invoke-static {v2, p3, v0, v1}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->access$700(Lcom/samsung/musicplus/contents/devices/DeviceListFragment;Landroid/widget/TextView;Landroid/content/Context;I)V

    .line 778
    :cond_1
    return-void

    .line 768
    .end local v0    # "context":Landroid/content/Context;
    :cond_2
    const-wide/16 v2, -0x13

    cmp-long v2, v2, p1

    if-nez v2, :cond_0

    .line 769
    iget-object v2, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->this$0:Lcom/samsung/musicplus/contents/devices/DeviceListFragment;

    # getter for: Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mDeviceCount:I
    invoke-static {v2}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->access$600(Lcom/samsung/musicplus/contents/devices/DeviceListFragment;)I

    move-result v1

    goto :goto_0
.end method


# virtual methods
.method protected bindOtherView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "c"    # Landroid/database/Cursor;

    .prologue
    .line 1065
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$DeviceListViewHolder;

    .line 1067
    .local v0, "vh":Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$DeviceListViewHolder;
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->bindOtherView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    .line 1068
    return-void
.end method

.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 9
    .param p1, "v"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "c"    # Landroid/database/Cursor;

    .prologue
    const/4 v8, 0x0

    const/16 v3, 0x8

    .line 913
    invoke-interface {p3, v8}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 914
    .local v4, "id":J
    invoke-direct {p0, v4, v5}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->isFixedView(J)Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v0, p0

    move-object v1, p2

    move-object v2, p1

    move-object v3, p3

    .line 915
    invoke-direct/range {v0 .. v5}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->bindFixedView(Landroid/content/Context;Landroid/view/View;Landroid/database/Cursor;J)V

    .line 947
    :cond_0
    :goto_0
    return-void

    .line 919
    :cond_1
    const-string v0, "MusicUiDeviceTab"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "bindDefaultView c.getPosition() : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p3}, Landroid/database/Cursor;->getPosition()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mDeviceCount : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->this$0:Lcom/samsung/musicplus/contents/devices/DeviceListFragment;

    # getter for: Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mDeviceCount:I
    invoke-static {v2}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->access$600(Lcom/samsung/musicplus/contents/devices/DeviceListFragment;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 921
    iget-object v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->this$0:Lcom/samsung/musicplus/contents/devices/DeviceListFragment;

    invoke-interface {p3}, Landroid/database/Cursor;->getPosition()I

    move-result v1

    # invokes: Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->isDmsItem(I)Z
    invoke-static {v0, v1}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->access$300(Lcom/samsung/musicplus/contents/devices/DeviceListFragment;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 922
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$DeviceListViewHolder;

    .line 923
    .local v7, "vh":Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$DeviceListViewHolder;
    iget-object v0, v7, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$DeviceListViewHolder;->text1:Landroid/widget/TextView;

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b002b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 924
    iget-object v0, v7, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$DeviceListViewHolder;->thumbnail:Landroid/widget/ImageView;

    invoke-virtual {v0, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 925
    iget-object v0, v7, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$DeviceListViewHolder;->text2:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 926
    iget-object v0, v7, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$DeviceListViewHolder;->text3:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 928
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    .line 933
    iget-object v0, v7, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$DeviceListViewHolder;->unregisterBtn:Landroid/widget/ImageView;

    if-eqz v0, :cond_2

    .line 934
    iget-object v0, v7, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$DeviceListViewHolder;->unregisterBtn:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 939
    .end local v7    # "vh":Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$DeviceListViewHolder;
    :cond_2
    :goto_1
    const v0, 0x7f0d00bb

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 940
    .local v6, "divider":Landroid/view/View;
    if-eqz v6, :cond_0

    .line 941
    iget-boolean v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->mIsActionMode:Z

    if-eqz v0, :cond_4

    .line 942
    const v0, 0x7f020022

    invoke-virtual {v6, v0}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_0

    .end local v6    # "divider":Landroid/view/View;
    :cond_3
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    .line 937
    invoke-direct/range {v0 .. v5}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->bindSlinkDeviceView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;J)V

    goto :goto_1

    .line 944
    .restart local v6    # "divider":Landroid/view/View;
    :cond_4
    const v0, 0x7f020021

    invoke-virtual {v6, v0}, Landroid/view/View;->setBackgroundResource(I)V

    goto/16 :goto_0
.end method

.method protected getArtKey(Landroid/database/Cursor;)Ljava/lang/Object;
    .locals 2
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 1124
    iget-object v1, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->this$0:Lcom/samsung/musicplus/contents/devices/DeviceListFragment;

    # getter for: Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mQueryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;
    invoke-static {v1}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->access$2100(Lcom/samsung/musicplus/contents/devices/DeviceListFragment;)Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    move-result-object v1

    iget-object v1, v1, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->albumArtCol:Ljava/lang/String;

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1125
    .local v0, "artWorkPath":Ljava/lang/String;
    return-object v0
.end method

.method protected getArtworkInBackground(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/Object;I)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "iv"    # Landroid/widget/ImageView;
    .param p3, "artKey"    # Ljava/lang/Object;
    .param p4, "resId"    # I

    .prologue
    .line 1136
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0138

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 1141
    .local v5, "size":I
    invoke-virtual {p2, p4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1142
    invoke-static {}, Lcom/samsung/musicplus/util/AlbumArtLoader;->getAlbumArtLoader()Lcom/samsung/musicplus/util/IAlbumArtLoader;

    move-result-object v0

    move-object v2, p3

    check-cast v2, Ljava/lang/String;

    move-object v1, p1

    move-object v3, p3

    move-object v4, p2

    move v6, p4

    invoke-interface/range {v0 .. v6}, Lcom/samsung/musicplus/util/IAlbumArtLoader;->loadArtwork(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Object;Landroid/widget/ImageView;II)V

    .line 1144
    return-void
.end method

.method protected getCachedArtwork(Ljava/lang/Object;)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p1, "artKey"    # Ljava/lang/Object;

    .prologue
    .line 1130
    check-cast p1, Ljava/lang/String;

    .end local p1    # "artKey":Ljava/lang/Object;
    invoke-static {p1}, Lcom/samsung/musicplus/util/AlbumArtUtils;->getCachedNetworkAlbumArt(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 699
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 700
    .local v0, "c":Landroid/database/Cursor;
    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 701
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 709
    .local v2, "id":J
    const/4 p2, 0x0

    .line 710
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    return-object v1
.end method

.method protected getViewHolder()Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;
    .locals 1

    .prologue
    .line 694
    new-instance v0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$DeviceListViewHolder;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$DeviceListViewHolder;-><init>(Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;)V

    return-object v0
.end method

.method public isEnabled(I)Z
    .locals 6
    .param p1, "position"    # I

    .prologue
    const/4 v1, 0x0

    .line 715
    invoke-virtual {p0}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    .line 716
    .local v0, "c":Landroid/database/Cursor;
    if-eqz v0, :cond_1

    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 717
    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 718
    .local v2, "id":J
    invoke-direct {p0, v2, v3}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->isFixedView(J)Z

    move-result v4

    if-eqz v4, :cond_0

    const-wide/16 v4, -0x16

    cmp-long v4, v4, v2

    if-eqz v4, :cond_0

    .line 724
    .end local v2    # "id":J
    :goto_0
    return v1

    .line 720
    .restart local v2    # "id":J
    :cond_0
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->isSlinkDeviceOff(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 721
    const/4 v1, 0x1

    goto :goto_0

    .line 724
    .end local v2    # "id":J
    :cond_1
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->isEnabled(I)Z

    move-result v1

    goto :goto_0
.end method

.method protected newOtherView(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1058
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$DeviceListViewHolder;

    .line 1059
    .local v0, "vh":Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$DeviceListViewHolder;
    const v1, 0x7f0d00c5

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, v0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$DeviceListViewHolder;->unregisterBtn:Landroid/widget/ImageView;

    .line 1060
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->newOtherView(Landroid/view/View;)V

    .line 1061
    return-void
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "c"    # Landroid/database/Cursor;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 741
    const-string v2, "MusicUiDeviceTab"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->this$0:Lcom/samsung/musicplus/contents/devices/DeviceListFragment;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " newView"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 742
    const/4 v2, 0x0

    invoke-interface {p2, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 743
    .local v0, "id":J
    invoke-direct {p0, v0, v1}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->isFixedView(J)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 744
    invoke-direct {p0, p1, p3, v0, v1}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->getFixedView(Landroid/content/Context;Landroid/view/ViewGroup;J)Landroid/view/View;

    move-result-object v2

    .line 746
    :goto_0
    return-object v2

    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    goto :goto_0
.end method

.method protected setArtWorkView(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/Object;I)Z
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "iv"    # Landroid/widget/ImageView;
    .param p3, "artKey"    # Ljava/lang/Object;
    .param p4, "resId"    # I

    .prologue
    .line 1116
    const/4 v1, 0x0

    invoke-virtual {p2, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1118
    const v1, 0x7f020123

    invoke-super {p0, p1, p2, p3, v1}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->setArtWorkView(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/Object;I)Z

    move-result v0

    .line 1119
    .local v0, "isCached":Z
    return v0
.end method

.method public showPopupDeregistered(JLjava/lang/String;)V
    .locals 3
    .param p1, "deviceId"    # J
    .param p3, "deviceName"    # Ljava/lang/String;

    .prologue
    .line 1072
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 1074
    .local v0, "dialogBuilder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0, p3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 1076
    const v1, 0x7f100054

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 1077
    iget-object v1, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->mContext:Landroid/content/Context;

    const v2, 0x7f10003a

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$8;

    invoke-direct {v2, p0}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$8;-><init>(Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1083
    iget-object v1, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->mContext:Landroid/content/Context;

    const v2, 0x7f100053

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$9;

    invoke-direct {v2, p0, p1, p2}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$9;-><init>(Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;J)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 1109
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->mDialog:Landroid/app/AlertDialog;

    .line 1110
    iget-object v1, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 1112
    return-void
.end method
