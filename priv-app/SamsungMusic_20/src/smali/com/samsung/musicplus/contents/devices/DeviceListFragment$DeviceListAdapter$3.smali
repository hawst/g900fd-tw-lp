.class Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$3;
.super Ljava/lang/Object;
.source "DeviceListFragment.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->getSlinkDeviceCategoryView(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;

.field final synthetic val$button:Landroid/view/View;

.field final synthetic val$progress:Landroid/view/View;

.field final synthetic val$text:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;Landroid/view/View;Landroid/view/View;Landroid/widget/TextView;)V
    .locals 0

    .prologue
    .line 839
    iput-object p1, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$3;->this$1:Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;

    iput-object p2, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$3;->val$button:Landroid/view/View;

    iput-object p3, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$3;->val$progress:Landroid/view/View;

    iput-object p4, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$3;->val$text:Landroid/widget/TextView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 8
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 843
    iget-object v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$3;->this$1:Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;

    iget-object v1, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$3;->val$button:Landroid/view/View;

    iget-object v2, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$3;->val$progress:Landroid/view/View;

    iget-object v3, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$3;->val$text:Landroid/widget/TextView;

    const-wide/16 v4, -0x13

    # invokes: Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->showProgress(Landroid/view/View;Landroid/view/View;Landroid/widget/TextView;J)V
    invoke-static/range {v0 .. v5}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->access$1200(Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;Landroid/view/View;Landroid/view/View;Landroid/widget/TextView;J)V

    .line 844
    iget-object v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$3;->this$1:Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;

    iget-object v0, v0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->this$0:Lcom/samsung/musicplus/contents/devices/DeviceListFragment;

    invoke-virtual {v0}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    .line 845
    .local v6, "a":Landroid/app/Activity;
    invoke-virtual {v6}, Landroid/app/Activity;->isChangingConfigurations()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v6}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v6}, Landroid/app/Activity;->isDestroyed()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 850
    :cond_0
    :goto_0
    return-void

    .line 848
    :cond_1
    invoke-virtual {v6}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    .line 849
    .local v7, "context":Landroid/content/Context;
    iget-object v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter$3;->this$1:Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;

    iget-object v0, v0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$DeviceListAdapter;->this$0:Lcom/samsung/musicplus/contents/devices/DeviceListFragment;

    # getter for: Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->mDeviceIds:[J
    invoke-static {v0}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->access$1300(Lcom/samsung/musicplus/contents/devices/DeviceListFragment;)[J

    move-result-object v0

    invoke-static {v7, v0}, Lcom/samsung/musicplus/provider/MusicContents$Audio$Devices;->refreshDevices(Landroid/content/Context;[J)V

    goto :goto_0
.end method
