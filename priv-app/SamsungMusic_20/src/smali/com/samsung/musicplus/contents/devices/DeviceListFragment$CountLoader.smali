.class Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader;
.super Ljava/lang/Object;
.source "DeviceListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/contents/devices/DeviceListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "CountLoader"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader$AsyncTextTag;,
        Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader$TextLoaderTask;
    }
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mExitTasksEarly:Z

.field private mPauseWork:Z

.field private final mPauseWorkLock:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 1217
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1213
    iput-boolean v1, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader;->mExitTasksEarly:Z

    .line 1368
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader;->mPauseWorkLock:Ljava/lang/Object;

    .line 1370
    iput-boolean v1, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader;->mPauseWork:Z

    .line 1218
    iput-object p1, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader;->mContext:Landroid/content/Context;

    .line 1219
    return-void
.end method

.method static synthetic access$2900(Landroid/widget/TextView;)Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader$TextLoaderTask;
    .locals 1
    .param p0, "x0"    # Landroid/widget/TextView;

    .prologue
    .line 1211
    invoke-static {p0}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader;->getCounterWorkerTask(Landroid/widget/TextView;)Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader$TextLoaderTask;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$3000(Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader;

    .prologue
    .line 1211
    iget-object v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader;->mPauseWorkLock:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$3100(Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader;

    .prologue
    .line 1211
    iget-boolean v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader;->mPauseWork:Z

    return v0
.end method

.method static synthetic access$3200(Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader;

    .prologue
    .line 1211
    iget-boolean v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader;->mExitTasksEarly:Z

    return v0
.end method

.method public static cancelPotentialWork(Ljava/lang/Object;Landroid/widget/TextView;)Z
    .locals 6
    .param p0, "data"    # Ljava/lang/Object;
    .param p1, "textView"    # Landroid/widget/TextView;

    .prologue
    const/4 v2, 0x1

    .line 1246
    invoke-static {p1}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader;->getCounterWorkerTask(Landroid/widget/TextView;)Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader$TextLoaderTask;

    move-result-object v1

    .line 1248
    .local v1, "textWorkerTask":Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader$TextLoaderTask;
    if-eqz v1, :cond_1

    .line 1249
    # getter for: Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader$TextLoaderTask;->data:Ljava/lang/Object;
    invoke-static {v1}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader$TextLoaderTask;->access$2800(Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader$TextLoaderTask;)Ljava/lang/Object;

    move-result-object v0

    .line 1250
    .local v0, "textData":Ljava/lang/Object;
    if-eqz v0, :cond_0

    invoke-virtual {v0, p0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 1251
    :cond_0
    invoke-virtual {v1, v2}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader$TextLoaderTask;->cancel(Z)Z

    .line 1252
    const-string v3, "MusicUiDeviceTab"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "cancelPotentialWork - cancelled work for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1258
    .end local v0    # "textData":Ljava/lang/Object;
    :cond_1
    :goto_0
    return v2

    .line 1255
    .restart local v0    # "textData":Ljava/lang/Object;
    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private static getCounterWorkerTask(Landroid/widget/TextView;)Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader$TextLoaderTask;
    .locals 5
    .param p0, "textView"    # Landroid/widget/TextView;

    .prologue
    .line 1418
    const-string v2, "MusicUiDeviceTab"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getCounterWorkerTask "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1419
    if-eqz p0, :cond_0

    .line 1420
    invoke-virtual {p0}, Landroid/widget/TextView;->getTag()Ljava/lang/Object;

    move-result-object v1

    .line 1421
    .local v1, "text":Ljava/lang/Object;
    const-string v2, "MusicUiDeviceTab"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getCounterWorkerTask "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1422
    instance-of v2, v1, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader$AsyncTextTag;

    if-eqz v2, :cond_0

    move-object v0, v1

    .line 1423
    check-cast v0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader$AsyncTextTag;

    .line 1424
    .local v0, "asyncText":Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader$AsyncTextTag;
    const-string v2, "MusicUiDeviceTab"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getCounterWorkerTask getTextWorkerTask "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader$AsyncTextTag;->getTextWorkerTask()Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader$TextLoaderTask;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1427
    invoke-virtual {v0}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader$AsyncTextTag;->getTextWorkerTask()Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader$TextLoaderTask;

    move-result-object v2

    .line 1430
    .end local v0    # "asyncText":Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader$AsyncTextTag;
    .end local v1    # "text":Ljava/lang/Object;
    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method


# virtual methods
.method public load(Landroid/widget/TextView;J)V
    .locals 6
    .param p1, "text"    # Landroid/widget/TextView;
    .param p2, "id"    # J

    .prologue
    .line 1222
    const-wide/16 v2, 0x0

    cmp-long v1, p2, v2

    if-gez v1, :cond_1

    .line 1232
    :cond_0
    :goto_0
    return-void

    .line 1226
    :cond_1
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader;->cancelPotentialWork(Ljava/lang/Object;Landroid/widget/TextView;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1227
    new-instance v0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader$TextLoaderTask;

    iget-object v1, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader;->mContext:Landroid/content/Context;

    invoke-direct {v0, p0, v1, p1}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader$TextLoaderTask;-><init>(Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader;Landroid/content/Context;Landroid/widget/TextView;)V

    .line 1228
    .local v0, "task":Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader$TextLoaderTask;
    new-instance v1, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader$AsyncTextTag;

    invoke-direct {v1, p0, v0}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader$AsyncTextTag;-><init>(Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader;Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader$TextLoaderTask;)V

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 1229
    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader$TextLoaderTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 1230
    const-string v1, "MusicUiDeviceTab"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "load  executeOnExecutor text "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " task "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setExitTasksEarly(Z)V
    .locals 1
    .param p1, "exitTasksEarly"    # Z

    .prologue
    .line 1235
    iput-boolean p1, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader;->mExitTasksEarly:Z

    .line 1236
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader;->setPauseWork(Z)V

    .line 1237
    return-void
.end method

.method public setPauseWork(Z)V
    .locals 2
    .param p1, "pauseWork"    # Z

    .prologue
    .line 1385
    iget-object v1, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader;->mPauseWorkLock:Ljava/lang/Object;

    monitor-enter v1

    .line 1386
    :try_start_0
    iput-boolean p1, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader;->mPauseWork:Z

    .line 1387
    iget-boolean v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader;->mPauseWork:Z

    if-nez v0, :cond_0

    .line 1388
    iget-object v0, p0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment$CountLoader;->mPauseWorkLock:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 1390
    :cond_0
    monitor-exit v1

    .line 1391
    return-void

    .line 1390
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
