.class Lcom/samsung/musicplus/contents/devices/DeviceContentsSelectionListFragment$DeviceContentsSelectListAdapter;
.super Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter;
.source "DeviceContentsSelectionListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/contents/devices/DeviceContentsSelectionListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "DeviceContentsSelectListAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/contents/devices/DeviceContentsSelectionListFragment;


# direct methods
.method public constructor <init>(Lcom/samsung/musicplus/contents/devices/DeviceContentsSelectionListFragment;Landroid/content/Context;ILandroid/database/Cursor;I)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "layout"    # I
    .param p4, "c"    # Landroid/database/Cursor;
    .param p5, "flags"    # I

    .prologue
    .line 55
    iput-object p1, p0, Lcom/samsung/musicplus/contents/devices/DeviceContentsSelectionListFragment$DeviceContentsSelectListAdapter;->this$0:Lcom/samsung/musicplus/contents/devices/DeviceContentsSelectionListFragment;

    .line 56
    invoke-direct/range {p0 .. p5}, Lcom/samsung/musicplus/contents/extra/SelectionListFragment$SelectionListAdapter;-><init>(Lcom/samsung/musicplus/contents/extra/SelectionListFragment;Landroid/content/Context;ILandroid/database/Cursor;I)V

    .line 57
    return-void
.end method


# virtual methods
.method protected bindTextView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "c"    # Landroid/database/Cursor;

    .prologue
    .line 61
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;

    .line 62
    .local v1, "vh":Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;
    const-string v2, "title"

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 63
    .local v0, "title":Ljava/lang/String;
    iget-object v2, v1, Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;->text1:Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 64
    return-void
.end method

.method protected getArtKey(Landroid/database/Cursor;)Ljava/lang/Object;
    .locals 2
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 68
    const-string v0, "album_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method protected getArtworkInBackground(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/Object;I)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "iv"    # Landroid/widget/ImageView;
    .param p3, "artKey"    # Ljava/lang/Object;
    .param p4, "resId"    # I

    .prologue
    .line 74
    const v1, 0x7f02003b

    invoke-virtual {p2, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 76
    invoke-static {}, Lcom/samsung/musicplus/util/AlbumArtLoader;->getAlbumArtLoader()Lcom/samsung/musicplus/util/IAlbumArtLoader;

    move-result-object v0

    .line 77
    .local v0, "l":Lcom/samsung/musicplus/util/IAlbumArtLoader;
    sget-object v2, Lcom/samsung/musicplus/util/AlbumArtUtils;->S_LINK_DUMY_ARTWORK_URI_STRING:Ljava/lang/String;

    iget v5, p0, Lcom/samsung/musicplus/contents/devices/DeviceContentsSelectionListFragment$DeviceContentsSelectListAdapter;->mAlbumArtSize:I

    move-object v1, p1

    move-object v3, p3

    move-object v4, p2

    move v6, p4

    invoke-interface/range {v0 .. v6}, Lcom/samsung/musicplus/util/IAlbumArtLoader;->loadArtwork(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Object;Landroid/widget/ImageView;II)V

    .line 79
    return-void
.end method

.method protected getCachedArtwork(Ljava/lang/Object;)Landroid/graphics/drawable/Drawable;
    .locals 2
    .param p1, "artKey"    # Ljava/lang/Object;

    .prologue
    .line 83
    check-cast p1, Ljava/lang/Long;

    .end local p1    # "artKey":Ljava/lang/Object;
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/samsung/musicplus/util/AlbumArtUtils;->getCachedSlinkArtwork(J)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method
