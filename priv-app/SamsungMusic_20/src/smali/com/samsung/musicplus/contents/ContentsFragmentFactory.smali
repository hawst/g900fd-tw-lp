.class public Lcom/samsung/musicplus/contents/ContentsFragmentFactory;
.super Ljava/lang/Object;
.source "ContentsFragmentFactory.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static getContentsFragment(I)Landroid/app/Fragment;
    .locals 1
    .param p0, "list"    # I

    .prologue
    .line 65
    invoke-static {p0}, Lcom/samsung/musicplus/util/ListUtils;->isEasyMode(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 67
    invoke-static {p0}, Lcom/samsung/musicplus/contents/ContentsFragmentFactory;->getEasyModeFragment(I)Landroid/app/Fragment;

    move-result-object v0

    .line 69
    :goto_0
    return-object v0

    :cond_0
    invoke-static {p0}, Lcom/samsung/musicplus/contents/ContentsFragmentFactory;->getNormalContentsFragment(I)Landroid/app/Fragment;

    move-result-object v0

    goto :goto_0
.end method

.method private static getEasyModeFragment(I)Landroid/app/Fragment;
    .locals 1
    .param p0, "list"    # I

    .prologue
    .line 80
    const/4 v0, 0x0

    .line 81
    .local v0, "fg":Landroid/app/Fragment;
    sparse-switch p0, :sswitch_data_0

    .line 101
    :goto_0
    return-object v0

    .line 83
    :sswitch_0
    new-instance v0, Lcom/samsung/musicplus/contents/normal/AllListFragment;

    .end local v0    # "fg":Landroid/app/Fragment;
    invoke-direct {v0}, Lcom/samsung/musicplus/contents/normal/AllListFragment;-><init>()V

    .line 84
    .restart local v0    # "fg":Landroid/app/Fragment;
    goto :goto_0

    .line 87
    :sswitch_1
    new-instance v0, Lcom/samsung/musicplus/contents/normal/PlayListFragment;

    .end local v0    # "fg":Landroid/app/Fragment;
    invoke-direct {v0}, Lcom/samsung/musicplus/contents/normal/PlayListFragment;-><init>()V

    .line 88
    .restart local v0    # "fg":Landroid/app/Fragment;
    goto :goto_0

    .line 91
    :sswitch_2
    new-instance v0, Lcom/samsung/musicplus/contents/normal/AlbumListFragment;

    .end local v0    # "fg":Landroid/app/Fragment;
    invoke-direct {v0}, Lcom/samsung/musicplus/contents/normal/AlbumListFragment;-><init>()V

    .line 92
    .restart local v0    # "fg":Landroid/app/Fragment;
    goto :goto_0

    .line 95
    :sswitch_3
    new-instance v0, Lcom/samsung/musicplus/contents/normal/ArtistListFragment;

    .end local v0    # "fg":Landroid/app/Fragment;
    invoke-direct {v0}, Lcom/samsung/musicplus/contents/normal/ArtistListFragment;-><init>()V

    .line 96
    .restart local v0    # "fg":Landroid/app/Fragment;
    goto :goto_0

    .line 81
    nop

    :sswitch_data_0
    .sparse-switch
        0x10002 -> :sswitch_2
        0x10003 -> :sswitch_3
        0x10004 -> :sswitch_1
        0x20001 -> :sswitch_0
        0x20002 -> :sswitch_2
        0x20003 -> :sswitch_3
        0x20004 -> :sswitch_1
    .end sparse-switch
.end method

.method public static getNewInstance(ILjava/lang/String;)Landroid/app/Fragment;
    .locals 2
    .param p0, "list"    # I
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 50
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 51
    .local v0, "data":Landroid/os/Bundle;
    invoke-static {v0, p0, p1}, Lcom/samsung/musicplus/contents/ContentsFragmentFactory;->setFragmentData(Landroid/os/Bundle;ILjava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    return-object v1
.end method

.method private static getNormalContentsFragment(I)Landroid/app/Fragment;
    .locals 1
    .param p0, "list"    # I

    .prologue
    .line 111
    const/4 v0, 0x0

    .line 112
    .local v0, "fg":Landroid/app/Fragment;
    sparse-switch p0, :sswitch_data_0

    .line 173
    :goto_0
    return-object v0

    .line 114
    :sswitch_0
    new-instance v0, Lcom/samsung/musicplus/contents/normal/AllListFragment;

    .end local v0    # "fg":Landroid/app/Fragment;
    invoke-direct {v0}, Lcom/samsung/musicplus/contents/normal/AllListFragment;-><init>()V

    .line 115
    .restart local v0    # "fg":Landroid/app/Fragment;
    goto :goto_0

    .line 118
    :sswitch_1
    new-instance v0, Lcom/samsung/musicplus/contents/normal/AlbumListFragment;

    .end local v0    # "fg":Landroid/app/Fragment;
    invoke-direct {v0}, Lcom/samsung/musicplus/contents/normal/AlbumListFragment;-><init>()V

    .line 119
    .restart local v0    # "fg":Landroid/app/Fragment;
    goto :goto_0

    .line 122
    :sswitch_2
    new-instance v0, Lcom/samsung/musicplus/contents/normal/ArtistListFragment;

    .end local v0    # "fg":Landroid/app/Fragment;
    invoke-direct {v0}, Lcom/samsung/musicplus/contents/normal/ArtistListFragment;-><init>()V

    .line 123
    .restart local v0    # "fg":Landroid/app/Fragment;
    goto :goto_0

    .line 126
    :sswitch_3
    new-instance v0, Lcom/samsung/musicplus/contents/normal/FolderListFragment;

    .end local v0    # "fg":Landroid/app/Fragment;
    invoke-direct {v0}, Lcom/samsung/musicplus/contents/normal/FolderListFragment;-><init>()V

    .line 127
    .restart local v0    # "fg":Landroid/app/Fragment;
    goto :goto_0

    .line 130
    :sswitch_4
    new-instance v0, Lcom/samsung/musicplus/contents/normal/PlayListFragment;

    .end local v0    # "fg":Landroid/app/Fragment;
    invoke-direct {v0}, Lcom/samsung/musicplus/contents/normal/PlayListFragment;-><init>()V

    .line 131
    .restart local v0    # "fg":Landroid/app/Fragment;
    goto :goto_0

    .line 134
    :sswitch_5
    new-instance v0, Lcom/samsung/musicplus/bigpond/BigPondFragment;

    .end local v0    # "fg":Landroid/app/Fragment;
    invoke-direct {v0}, Lcom/samsung/musicplus/bigpond/BigPondFragment;-><init>()V

    .line 135
    .restart local v0    # "fg":Landroid/app/Fragment;
    goto :goto_0

    .line 139
    :sswitch_6
    new-instance v0, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;

    .end local v0    # "fg":Landroid/app/Fragment;
    invoke-direct {v0}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;-><init>()V

    .line 143
    .restart local v0    # "fg":Landroid/app/Fragment;
    goto :goto_0

    .line 145
    :sswitch_7
    new-instance v0, Lcom/samsung/musicplus/contents/devices/DeviceContentsListFragment;

    .end local v0    # "fg":Landroid/app/Fragment;
    invoke-direct {v0}, Lcom/samsung/musicplus/contents/devices/DeviceContentsListFragment;-><init>()V

    .line 146
    .restart local v0    # "fg":Landroid/app/Fragment;
    goto :goto_0

    .line 148
    :sswitch_8
    new-instance v0, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;

    .end local v0    # "fg":Landroid/app/Fragment;
    invoke-direct {v0}, Lcom/samsung/musicplus/contents/dlna/DlnaDmsListFragment;-><init>()V

    .line 149
    .restart local v0    # "fg":Landroid/app/Fragment;
    goto :goto_0

    .line 151
    :sswitch_9
    new-instance v0, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;

    .end local v0    # "fg":Landroid/app/Fragment;
    invoke-direct {v0}, Lcom/samsung/musicplus/contents/square/MusicSquareFragment;-><init>()V

    .line 152
    .restart local v0    # "fg":Landroid/app/Fragment;
    goto :goto_0

    .line 154
    :sswitch_a
    new-instance v0, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;

    .end local v0    # "fg":Landroid/app/Fragment;
    invoke-direct {v0}, Lcom/samsung/musicplus/contents/square/MusicSquareListFragment;-><init>()V

    .line 155
    .restart local v0    # "fg":Landroid/app/Fragment;
    goto :goto_0

    .line 158
    :sswitch_b
    new-instance v0, Lcom/samsung/musicplus/contents/normal/GenreListFragment;

    .end local v0    # "fg":Landroid/app/Fragment;
    invoke-direct {v0}, Lcom/samsung/musicplus/contents/normal/GenreListFragment;-><init>()V

    .line 159
    .restart local v0    # "fg":Landroid/app/Fragment;
    goto :goto_0

    .line 162
    :sswitch_c
    new-instance v0, Lcom/samsung/musicplus/contents/normal/ComposerListFragment;

    .end local v0    # "fg":Landroid/app/Fragment;
    invoke-direct {v0}, Lcom/samsung/musicplus/contents/normal/ComposerListFragment;-><init>()V

    .line 163
    .restart local v0    # "fg":Landroid/app/Fragment;
    goto :goto_0

    .line 165
    :sswitch_d
    new-instance v0, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;

    .end local v0    # "fg":Landroid/app/Fragment;
    invoke-direct {v0}, Lcom/samsung/musicplus/contents/search/QueryBrowserFragment;-><init>()V

    .line 166
    .restart local v0    # "fg":Landroid/app/Fragment;
    goto :goto_0

    .line 168
    :sswitch_e
    new-instance v0, Lcom/samsung/musicplus/contents/normal/RecordingsListFragment;

    .end local v0    # "fg":Landroid/app/Fragment;
    invoke-direct {v0}, Lcom/samsung/musicplus/contents/normal/RecordingsListFragment;-><init>()V

    .line 169
    .restart local v0    # "fg":Landroid/app/Fragment;
    goto :goto_0

    .line 112
    nop

    :sswitch_data_0
    .sparse-switch
        0x10002 -> :sswitch_1
        0x10003 -> :sswitch_2
        0x10004 -> :sswitch_4
        0x10006 -> :sswitch_b
        0x10007 -> :sswitch_3
        0x10008 -> :sswitch_c
        0x1000a -> :sswitch_9
        0x1000b -> :sswitch_6
        0x1000c -> :sswitch_5
        0x20001 -> :sswitch_0
        0x20002 -> :sswitch_1
        0x20003 -> :sswitch_2
        0x20004 -> :sswitch_4
        0x20006 -> :sswitch_b
        0x20007 -> :sswitch_3
        0x20008 -> :sswitch_c
        0x2000a -> :sswitch_a
        0x2000b -> :sswitch_8
        0x2000d -> :sswitch_7
        0x20011 -> :sswitch_e
        0x20024 -> :sswitch_d
    .end sparse-switch
.end method

.method private static setFragmentData(Landroid/os/Bundle;ILjava/lang/String;)Landroid/app/Fragment;
    .locals 2
    .param p0, "data"    # Landroid/os/Bundle;
    .param p1, "list"    # I
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 55
    const-string v1, "list"

    invoke-virtual {p0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 56
    const-string v1, "key"

    invoke-virtual {p0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    invoke-static {p1}, Lcom/samsung/musicplus/contents/ContentsFragmentFactory;->getContentsFragment(I)Landroid/app/Fragment;

    move-result-object v0

    .line 58
    .local v0, "fg":Landroid/app/Fragment;
    if-eqz v0, :cond_0

    .line 59
    invoke-virtual {v0, p0}, Landroid/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 61
    :cond_0
    return-object v0
.end method
