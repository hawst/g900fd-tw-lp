.class public Lcom/samsung/musicplus/bitmapcache/BitmapCache;
.super Ljava/lang/Object;
.source "BitmapCache.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/bitmapcache/BitmapCache$DiscardOldestPolicy;,
        Lcom/samsung/musicplus/bitmapcache/BitmapCache$LifoBlockingQueue;,
        Lcom/samsung/musicplus/bitmapcache/BitmapCache$LoaderAsyncTask;,
        Lcom/samsung/musicplus/bitmapcache/BitmapCache$Publisher;,
        Lcom/samsung/musicplus/bitmapcache/BitmapCache$Loader;
    }
.end annotation


# static fields
.field private static DEBUG:Z = false

.field private static final LOG_TAG:Ljava/lang/String;

.field public static final MAX_CACHE_MEM_SIZE:I = 0x989680

.field public static final UNSPECIFIED_HEIGHT:I = -0x1

.field private static volatile mInstance:Lcom/samsung/musicplus/bitmapcache/BitmapCache;

.field private static final sRemoteLoadingQueue:Ljava/util/concurrent/BlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/BlockingQueue",
            "<",
            "Ljava/lang/Runnable;",
            ">;"
        }
    .end annotation
.end field

.field private static sRemoteThreadPoolExecutor:Ljava/util/concurrent/Executor;

.field private static final sThreadFactory:Ljava/util/concurrent/ThreadFactory;

.field private static sWrfContext:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/content/Context;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mCache:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Landroid/net/Uri;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mLoader:Lcom/samsung/musicplus/bitmapcache/BitmapCache$Loader;

.field private mOptions:Ljava/lang/ThreadLocal;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ThreadLocal",
            "<",
            "Landroid/graphics/BitmapFactory$Options;",
            ">;"
        }
    .end annotation
.end field

.field private mPublishListeners:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/samsung/musicplus/bitmapcache/BitmapCache$Publisher;",
            ">;"
        }
    .end annotation
.end field

.field private volatile mTasks:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/net/Uri;",
            "Lcom/samsung/musicplus/bitmapcache/BitmapCache$LoaderAsyncTask;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const/4 v2, 0x1

    .line 148
    const-class v0, Lcom/samsung/musicplus/bitmapcache/BitmapCache;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->LOG_TAG:Ljava/lang/String;

    .line 150
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->DEBUG:Z

    .line 174
    new-instance v0, Lcom/samsung/musicplus/bitmapcache/BitmapCache$2;

    invoke-direct {v0}, Lcom/samsung/musicplus/bitmapcache/BitmapCache$2;-><init>()V

    sput-object v0, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->sThreadFactory:Ljava/util/concurrent/ThreadFactory;

    .line 185
    new-instance v0, Lcom/samsung/musicplus/bitmapcache/BitmapCache$LifoBlockingQueue;

    const/16 v1, 0xf

    invoke-direct {v0, v1}, Lcom/samsung/musicplus/bitmapcache/BitmapCache$LifoBlockingQueue;-><init>(I)V

    sput-object v0, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->sRemoteLoadingQueue:Ljava/util/concurrent/BlockingQueue;

    .line 198
    new-instance v1, Ljava/util/concurrent/ThreadPoolExecutor;

    const-wide/16 v4, 0x5

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    sget-object v7, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->sRemoteLoadingQueue:Ljava/util/concurrent/BlockingQueue;

    sget-object v8, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->sThreadFactory:Ljava/util/concurrent/ThreadFactory;

    new-instance v9, Lcom/samsung/musicplus/bitmapcache/BitmapCache$DiscardOldestPolicy;

    invoke-direct {v9}, Lcom/samsung/musicplus/bitmapcache/BitmapCache$DiscardOldestPolicy;-><init>()V

    move v3, v2

    invoke-direct/range {v1 .. v9}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;Ljava/util/concurrent/RejectedExecutionHandler;)V

    sput-object v1, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->sRemoteThreadPoolExecutor:Ljava/util/concurrent/Executor;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/samsung/musicplus/bitmapcache/BitmapCache$Loader;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "loader"    # Lcom/samsung/musicplus/bitmapcache/BitmapCache$Loader;
    .param p3, "maxMemSize"    # I

    .prologue
    .line 207
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 160
    new-instance v0, Lcom/samsung/musicplus/bitmapcache/BitmapCache$1;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/bitmapcache/BitmapCache$1;-><init>(Lcom/samsung/musicplus/bitmapcache/BitmapCache;)V

    iput-object v0, p0, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->mOptions:Ljava/lang/ThreadLocal;

    .line 172
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->mPublishListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 208
    iput-object p1, p0, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->mContext:Landroid/content/Context;

    .line 209
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->mTasks:Ljava/util/HashMap;

    .line 210
    iput-object p2, p0, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->mLoader:Lcom/samsung/musicplus/bitmapcache/BitmapCache$Loader;

    .line 211
    sget-object v0, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->LOG_TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Allocated bitmap cache: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 213
    new-instance v0, Lcom/samsung/musicplus/bitmapcache/BitmapCache$3;

    invoke-direct {v0, p0, p3}, Lcom/samsung/musicplus/bitmapcache/BitmapCache$3;-><init>(Lcom/samsung/musicplus/bitmapcache/BitmapCache;I)V

    iput-object v0, p0, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->mCache:Landroid/util/LruCache;

    .line 219
    return-void
.end method

.method static synthetic access$000()Z
    .locals 1

    .prologue
    .line 36
    sget-boolean v0, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->DEBUG:Z

    return v0
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->LOG_TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/musicplus/bitmapcache/BitmapCache;)Ljava/lang/ThreadLocal;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/bitmapcache/BitmapCache;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->mOptions:Ljava/lang/ThreadLocal;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/musicplus/bitmapcache/BitmapCache;)Lcom/samsung/musicplus/bitmapcache/BitmapCache$Loader;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/bitmapcache/BitmapCache;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->mLoader:Lcom/samsung/musicplus/bitmapcache/BitmapCache$Loader;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/musicplus/bitmapcache/BitmapCache;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/bitmapcache/BitmapCache;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->mTasks:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/musicplus/bitmapcache/BitmapCache;)Landroid/util/LruCache;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/bitmapcache/BitmapCache;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->mCache:Landroid/util/LruCache;

    return-object v0
.end method

.method static synthetic access$600(Lcom/samsung/musicplus/bitmapcache/BitmapCache;Landroid/net/Uri;Landroid/graphics/Bitmap;J)V
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/bitmapcache/BitmapCache;
    .param p1, "x1"    # Landroid/net/Uri;
    .param p2, "x2"    # Landroid/graphics/Bitmap;
    .param p3, "x3"    # J

    .prologue
    .line 36
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->fireListeners(Landroid/net/Uri;Landroid/graphics/Bitmap;J)V

    return-void
.end method

.method public static clearCaches()V
    .locals 1

    .prologue
    .line 141
    sget-object v0, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->mInstance:Lcom/samsung/musicplus/bitmapcache/BitmapCache;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->mInstance:Lcom/samsung/musicplus/bitmapcache/BitmapCache;

    iget-object v0, v0, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->mCache:Landroid/util/LruCache;

    if-eqz v0, :cond_0

    .line 142
    sget-object v0, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->mInstance:Lcom/samsung/musicplus/bitmapcache/BitmapCache;

    iget-object v0, v0, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->mCache:Landroid/util/LruCache;

    invoke-virtual {v0}, Landroid/util/LruCache;->evictAll()V

    .line 144
    :cond_0
    return-void
.end method

.method private fireListeners(Landroid/net/Uri;Landroid/graphics/Bitmap;J)V
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;
    .param p3, "elapsedTime"    # J

    .prologue
    .line 341
    if-eqz p2, :cond_0

    .line 342
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->mPublishListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 343
    iget-object v1, p0, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->mPublishListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/musicplus/bitmapcache/BitmapCache$Publisher;

    invoke-interface {v1, p1, p2, p3, p4}, Lcom/samsung/musicplus/bitmapcache/BitmapCache$Publisher;->onPublishImage(Landroid/net/Uri;Landroid/graphics/Bitmap;J)V

    .line 342
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 346
    .end local v0    # "i":I
    :cond_0
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_1
    iget-object v1, p0, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->mPublishListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 347
    iget-object v1, p0, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->mPublishListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/musicplus/bitmapcache/BitmapCache$Publisher;

    invoke-interface {v1, p1, p3, p4}, Lcom/samsung/musicplus/bitmapcache/BitmapCache$Publisher;->onFailPublishImage(Landroid/net/Uri;J)V

    .line 346
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 350
    :cond_1
    return-void
.end method

.method public static getCache()Lcom/samsung/musicplus/bitmapcache/BitmapCache;
    .locals 6

    .prologue
    .line 120
    sget-object v3, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->mInstance:Lcom/samsung/musicplus/bitmapcache/BitmapCache;

    if-nez v3, :cond_0

    .line 121
    sget-object v3, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->sWrfContext:Ljava/lang/ref/WeakReference;

    invoke-virtual {v3}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    .line 122
    .local v0, "context":Landroid/content/Context;
    if-eqz v0, :cond_1

    .line 127
    const-string v3, "activity"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/ActivityManager;

    invoke-virtual {v3}, Landroid/app/ActivityManager;->getMemoryClass()I

    move-result v1

    .line 129
    .local v1, "memClass":I
    const/high16 v3, 0x100000

    mul-int/2addr v3, v1

    div-int/lit8 v2, v3, 0xa

    .line 131
    .local v2, "memSize":I
    sget-object v3, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->LOG_TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "create bitmap cache maxMemSize: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    new-instance v3, Lcom/samsung/musicplus/bitmapcache/BitmapCache;

    new-instance v4, Lcom/samsung/musicplus/bitmapcache/AlbArtUtilsLoader;

    invoke-direct {v4}, Lcom/samsung/musicplus/bitmapcache/AlbArtUtilsLoader;-><init>()V

    invoke-direct {v3, v0, v4, v2}, Lcom/samsung/musicplus/bitmapcache/BitmapCache;-><init>(Landroid/content/Context;Lcom/samsung/musicplus/bitmapcache/BitmapCache$Loader;I)V

    sput-object v3, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->mInstance:Lcom/samsung/musicplus/bitmapcache/BitmapCache;

    .line 137
    :cond_0
    sget-object v3, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->mInstance:Lcom/samsung/musicplus/bitmapcache/BitmapCache;

    return-object v3

    .line 134
    .end local v1    # "memClass":I
    .end local v2    # "memSize":I
    :cond_1
    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "Application context already destroyed"

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method public static initCache(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 108
    instance-of v0, p0, Landroid/app/Application;

    if-nez v0, :cond_0

    .line 109
    sget-object v0, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->LOG_TAG:Ljava/lang/String;

    const-string v1, "Bitmap cache is global and context should be application context! Possible memory leak detected!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    :cond_0
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    sput-object v0, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->sWrfContext:Ljava/lang/ref/WeakReference;

    .line 113
    return-void
.end method


# virtual methods
.method public addOnPublishImageListener(Lcom/samsung/musicplus/bitmapcache/BitmapCache$Publisher;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/musicplus/bitmapcache/BitmapCache$Publisher;

    .prologue
    .line 231
    iget-object v0, p0, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->mPublishListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 232
    return-void
.end method

.method public getBitmap(Landroid/net/Uri;)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 317
    const/4 v0, -0x1

    invoke-virtual {p0, p1, v0}, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->getBitmap(Landroid/net/Uri;I)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getBitmap(Landroid/net/Uri;I)Landroid/graphics/Bitmap;
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "height"    # I

    .prologue
    .line 329
    iget-object v0, p0, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->mCache:Landroid/util/LruCache;

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getLoader()Lcom/samsung/musicplus/bitmapcache/BitmapCache$Loader;
    .locals 1

    .prologue
    .line 306
    iget-object v0, p0, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->mLoader:Lcom/samsung/musicplus/bitmapcache/BitmapCache$Loader;

    return-object v0
.end method

.method public loadBitmap(Landroid/net/Uri;ILcom/samsung/musicplus/bitmapcache/BitmapCache$Publisher;)V
    .locals 8
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "height"    # I
    .param p3, "publish"    # Lcom/samsung/musicplus/bitmapcache/BitmapCache$Publisher;

    .prologue
    .line 254
    if-nez p1, :cond_0

    .line 255
    sget-object v1, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->LOG_TAG:Ljava/lang/String;

    const-string v2, "uri is null!"

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 288
    :goto_0
    return-void

    .line 258
    :cond_0
    iget-object v1, p0, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->mCache:Landroid/util/LruCache;

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/graphics/Bitmap;

    .line 259
    .local v6, "res":Landroid/graphics/Bitmap;
    if-nez v6, :cond_6

    .line 260
    iget-object v7, p0, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->mTasks:Ljava/util/HashMap;

    monitor-enter v7

    .line 261
    :try_start_0
    iget-object v1, p0, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->mTasks:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/bitmapcache/BitmapCache$LoaderAsyncTask;

    .line 262
    .local v0, "task":Lcom/samsung/musicplus/bitmapcache/BitmapCache$LoaderAsyncTask;
    if-eqz v0, :cond_3

    .line 263
    sget-boolean v1, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->DEBUG:Z

    if-eqz v1, :cond_1

    .line 264
    sget-object v1, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "addPubliser to existing task: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 266
    :cond_1
    invoke-virtual {v0, p3}, Lcom/samsung/musicplus/bitmapcache/BitmapCache$LoaderAsyncTask;->addPublisher(Lcom/samsung/musicplus/bitmapcache/BitmapCache$Publisher;)V

    .line 284
    :cond_2
    :goto_1
    monitor-exit v7

    goto :goto_0

    .end local v0    # "task":Lcom/samsung/musicplus/bitmapcache/BitmapCache$LoaderAsyncTask;
    :catchall_0
    move-exception v1

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1

    .line 268
    .restart local v0    # "task":Lcom/samsung/musicplus/bitmapcache/BitmapCache$LoaderAsyncTask;
    :cond_3
    :try_start_1
    sget-boolean v1, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->DEBUG:Z

    if-eqz v1, :cond_4

    .line 269
    sget-object v1, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "loadBitmap: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 271
    :cond_4
    new-instance v0, Lcom/samsung/musicplus/bitmapcache/BitmapCache$LoaderAsyncTask;

    .end local v0    # "task":Lcom/samsung/musicplus/bitmapcache/BitmapCache$LoaderAsyncTask;
    iget-object v2, p0, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->mContext:Landroid/content/Context;

    move-object v1, p0

    move-object v3, p1

    move v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/samsung/musicplus/bitmapcache/BitmapCache$LoaderAsyncTask;-><init>(Lcom/samsung/musicplus/bitmapcache/BitmapCache;Landroid/content/Context;Landroid/net/Uri;ILcom/samsung/musicplus/bitmapcache/BitmapCache$Publisher;)V

    .line 272
    .restart local v0    # "task":Lcom/samsung/musicplus/bitmapcache/BitmapCache$LoaderAsyncTask;
    iget-object v1, p0, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->mTasks:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 273
    iget-object v1, p0, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->mLoader:Lcom/samsung/musicplus/bitmapcache/BitmapCache$Loader;

    invoke-interface {v1, p1}, Lcom/samsung/musicplus/bitmapcache/BitmapCache$Loader;->isRemoteUri(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 274
    sget-object v1, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->sRemoteThreadPoolExecutor:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/musicplus/bitmapcache/BitmapCache$LoaderAsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 275
    const-string v1, "executeOnExecutor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Remote: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->sRemoteThreadPoolExecutor:Ljava/util/concurrent/Executor;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 280
    :goto_2
    sget-boolean v1, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->DEBUG:Z

    if-eqz v1, :cond_2

    .line 281
    sget-object v1, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Async task count: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->mTasks:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 277
    :cond_5
    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/musicplus/bitmapcache/BitmapCache$LoaderAsyncTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 278
    const-string v1, "executeOnExecutor"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Local: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget-object v3, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 286
    .end local v0    # "task":Lcom/samsung/musicplus/bitmapcache/BitmapCache$LoaderAsyncTask;
    :cond_6
    const-wide/16 v2, 0x0

    invoke-interface {p3, p1, v6, v2, v3}, Lcom/samsung/musicplus/bitmapcache/BitmapCache$Publisher;->onPublishImage(Landroid/net/Uri;Landroid/graphics/Bitmap;J)V

    goto/16 :goto_0
.end method

.method public loadBitmap(Landroid/net/Uri;Lcom/samsung/musicplus/bitmapcache/BitmapCache$Publisher;)V
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "publish"    # Lcom/samsung/musicplus/bitmapcache/BitmapCache$Publisher;

    .prologue
    .line 297
    const/4 v0, -0x1

    invoke-virtual {p0, p1, v0, p2}, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->loadBitmap(Landroid/net/Uri;ILcom/samsung/musicplus/bitmapcache/BitmapCache$Publisher;)V

    .line 298
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 337
    iget-object v0, p0, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->mCache:Landroid/util/LruCache;

    iget-object v1, p0, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->mCache:Landroid/util/LruCache;

    invoke-virtual {v1}, Landroid/util/LruCache;->maxSize()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    invoke-virtual {v0, v1}, Landroid/util/LruCache;->trimToSize(I)V

    .line 338
    return-void
.end method

.method public removeOnPublishImageListener(Lcom/samsung/musicplus/bitmapcache/BitmapCache$Publisher;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/musicplus/bitmapcache/BitmapCache$Publisher;

    .prologue
    .line 241
    iget-object v0, p0, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->mPublishListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 242
    return-void
.end method
