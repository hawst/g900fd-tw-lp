.class public interface abstract Lcom/samsung/musicplus/bitmapcache/BitmapCache$Publisher;
.super Ljava/lang/Object;
.source "BitmapCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/bitmapcache/BitmapCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Publisher"
.end annotation


# virtual methods
.method public abstract onFailPublishImage(Landroid/net/Uri;J)V
.end method

.method public abstract onPublishImage(Landroid/net/Uri;Landroid/graphics/Bitmap;J)V
.end method
