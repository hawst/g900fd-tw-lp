.class Lcom/samsung/musicplus/bitmapcache/BitmapCache$1;
.super Ljava/lang/ThreadLocal;
.source "BitmapCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/bitmapcache/BitmapCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/ThreadLocal",
        "<",
        "Landroid/graphics/BitmapFactory$Options;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/bitmapcache/BitmapCache;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/bitmapcache/BitmapCache;)V
    .locals 0

    .prologue
    .line 160
    iput-object p1, p0, Lcom/samsung/musicplus/bitmapcache/BitmapCache$1;->this$0:Lcom/samsung/musicplus/bitmapcache/BitmapCache;

    invoke-direct {p0}, Ljava/lang/ThreadLocal;-><init>()V

    return-void
.end method


# virtual methods
.method protected initialValue()Landroid/graphics/BitmapFactory$Options;
    .locals 2

    .prologue
    .line 163
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 165
    .local v0, "res":Landroid/graphics/BitmapFactory$Options;
    const/16 v1, 0x4000

    new-array v1, v1, [B

    iput-object v1, v0, Landroid/graphics/BitmapFactory$Options;->inTempStorage:[B

    .line 166
    const/4 v1, 0x0

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inScaled:Z

    .line 167
    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v1, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    .line 168
    return-object v0
.end method

.method protected bridge synthetic initialValue()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 160
    invoke-virtual {p0}, Lcom/samsung/musicplus/bitmapcache/BitmapCache$1;->initialValue()Landroid/graphics/BitmapFactory$Options;

    move-result-object v0

    return-object v0
.end method
