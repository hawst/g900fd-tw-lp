.class Lcom/samsung/musicplus/bitmapcache/BitmapCache$LoaderAsyncTask;
.super Landroid/os/AsyncTask;
.source "BitmapCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/bitmapcache/BitmapCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoaderAsyncTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Integer;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private mHeight:I

.field private mPublish:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/musicplus/bitmapcache/BitmapCache$Publisher;",
            ">;"
        }
    .end annotation
.end field

.field private mStartTime:J

.field private mUri:Landroid/net/Uri;

.field final synthetic this$0:Lcom/samsung/musicplus/bitmapcache/BitmapCache;


# direct methods
.method public constructor <init>(Lcom/samsung/musicplus/bitmapcache/BitmapCache;Landroid/content/Context;Landroid/net/Uri;ILcom/samsung/musicplus/bitmapcache/BitmapCache$Publisher;)V
    .locals 2
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "uri"    # Landroid/net/Uri;
    .param p4, "height"    # I
    .param p5, "publish"    # Lcom/samsung/musicplus/bitmapcache/BitmapCache$Publisher;

    .prologue
    .line 364
    iput-object p1, p0, Lcom/samsung/musicplus/bitmapcache/BitmapCache$LoaderAsyncTask;->this$0:Lcom/samsung/musicplus/bitmapcache/BitmapCache;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 365
    iput-object p2, p0, Lcom/samsung/musicplus/bitmapcache/BitmapCache$LoaderAsyncTask;->mContext:Landroid/content/Context;

    .line 366
    iput-object p3, p0, Lcom/samsung/musicplus/bitmapcache/BitmapCache$LoaderAsyncTask;->mUri:Landroid/net/Uri;

    .line 367
    iput p4, p0, Lcom/samsung/musicplus/bitmapcache/BitmapCache$LoaderAsyncTask;->mHeight:I

    .line 368
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/musicplus/bitmapcache/BitmapCache$LoaderAsyncTask;->mStartTime:J

    .line 369
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/bitmapcache/BitmapCache$LoaderAsyncTask;->mPublish:Ljava/util/ArrayList;

    .line 370
    iget-object v0, p0, Lcom/samsung/musicplus/bitmapcache/BitmapCache$LoaderAsyncTask;->mPublish:Ljava/util/ArrayList;

    invoke-virtual {v0, p5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 371
    return-void
.end method


# virtual methods
.method public addPublisher(Lcom/samsung/musicplus/bitmapcache/BitmapCache$Publisher;)V
    .locals 1
    .param p1, "publish"    # Lcom/samsung/musicplus/bitmapcache/BitmapCache$Publisher;

    .prologue
    .line 374
    iget-object v0, p0, Lcom/samsung/musicplus/bitmapcache/BitmapCache$LoaderAsyncTask;->mPublish:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 375
    return-void
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Landroid/graphics/Bitmap;
    .locals 5
    .param p1, "params"    # [Ljava/lang/Void;

    .prologue
    .line 379
    # getter for: Lcom/samsung/musicplus/bitmapcache/BitmapCache;->DEBUG:Z
    invoke-static {}, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->access$000()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 380
    # getter for: Lcom/samsung/musicplus/bitmapcache/BitmapCache;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->access$100()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "doInBackground: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/bitmapcache/BitmapCache$LoaderAsyncTask;->mUri:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 382
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/bitmapcache/BitmapCache$LoaderAsyncTask;->this$0:Lcom/samsung/musicplus/bitmapcache/BitmapCache;

    # getter for: Lcom/samsung/musicplus/bitmapcache/BitmapCache;->mLoader:Lcom/samsung/musicplus/bitmapcache/BitmapCache$Loader;
    invoke-static {v0}, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->access$300(Lcom/samsung/musicplus/bitmapcache/BitmapCache;)Lcom/samsung/musicplus/bitmapcache/BitmapCache$Loader;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/bitmapcache/BitmapCache$LoaderAsyncTask;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/samsung/musicplus/bitmapcache/BitmapCache$LoaderAsyncTask;->mUri:Landroid/net/Uri;

    iget v4, p0, Lcom/samsung/musicplus/bitmapcache/BitmapCache$LoaderAsyncTask;->mHeight:I

    iget-object v0, p0, Lcom/samsung/musicplus/bitmapcache/BitmapCache$LoaderAsyncTask;->this$0:Lcom/samsung/musicplus/bitmapcache/BitmapCache;

    # getter for: Lcom/samsung/musicplus/bitmapcache/BitmapCache;->mOptions:Ljava/lang/ThreadLocal;
    invoke-static {v0}, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->access$200(Lcom/samsung/musicplus/bitmapcache/BitmapCache;)Ljava/lang/ThreadLocal;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/BitmapFactory$Options;

    invoke-interface {v1, v2, v3, v4, v0}, Lcom/samsung/musicplus/bitmapcache/BitmapCache$Loader;->onLoadImage(Landroid/content/Context;Landroid/net/Uri;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 352
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/bitmapcache/BitmapCache$LoaderAsyncTask;->doInBackground([Ljava/lang/Void;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected onCancelled()V
    .locals 4

    .prologue
    .line 387
    iget-object v0, p0, Lcom/samsung/musicplus/bitmapcache/BitmapCache$LoaderAsyncTask;->this$0:Lcom/samsung/musicplus/bitmapcache/BitmapCache;

    # getter for: Lcom/samsung/musicplus/bitmapcache/BitmapCache;->mTasks:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->access$400(Lcom/samsung/musicplus/bitmapcache/BitmapCache;)Ljava/util/HashMap;

    move-result-object v1

    monitor-enter v1

    .line 388
    :try_start_0
    # getter for: Lcom/samsung/musicplus/bitmapcache/BitmapCache;->DEBUG:Z
    invoke-static {}, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->access$000()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 389
    # getter for: Lcom/samsung/musicplus/bitmapcache/BitmapCache;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->access$100()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "remove because of cancel from taskslist: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/musicplus/bitmapcache/BitmapCache$LoaderAsyncTask;->mUri:Landroid/net/Uri;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 391
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/bitmapcache/BitmapCache$LoaderAsyncTask;->this$0:Lcom/samsung/musicplus/bitmapcache/BitmapCache;

    # getter for: Lcom/samsung/musicplus/bitmapcache/BitmapCache;->mTasks:Ljava/util/HashMap;
    invoke-static {v0}, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->access$400(Lcom/samsung/musicplus/bitmapcache/BitmapCache;)Ljava/util/HashMap;

    move-result-object v0

    iget-object v2, p0, Lcom/samsung/musicplus/bitmapcache/BitmapCache$LoaderAsyncTask;->mUri:Landroid/net/Uri;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 392
    monitor-exit v1

    .line 393
    return-void

    .line 392
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected onPostExecute(Landroid/graphics/Bitmap;)V
    .locals 8
    .param p1, "result"    # Landroid/graphics/Bitmap;

    .prologue
    .line 397
    iget-object v4, p0, Lcom/samsung/musicplus/bitmapcache/BitmapCache$LoaderAsyncTask;->this$0:Lcom/samsung/musicplus/bitmapcache/BitmapCache;

    # getter for: Lcom/samsung/musicplus/bitmapcache/BitmapCache;->mTasks:Ljava/util/HashMap;
    invoke-static {v4}, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->access$400(Lcom/samsung/musicplus/bitmapcache/BitmapCache;)Ljava/util/HashMap;

    move-result-object v5

    monitor-enter v5

    .line 398
    :try_start_0
    # getter for: Lcom/samsung/musicplus/bitmapcache/BitmapCache;->DEBUG:Z
    invoke-static {}, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->access$000()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 399
    # getter for: Lcom/samsung/musicplus/bitmapcache/BitmapCache;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->access$100()Ljava/lang/String;

    move-result-object v4

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "remove from taskslist: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/musicplus/bitmapcache/BitmapCache$LoaderAsyncTask;->mUri:Landroid/net/Uri;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 401
    :cond_0
    iget-object v4, p0, Lcom/samsung/musicplus/bitmapcache/BitmapCache$LoaderAsyncTask;->this$0:Lcom/samsung/musicplus/bitmapcache/BitmapCache;

    # getter for: Lcom/samsung/musicplus/bitmapcache/BitmapCache;->mTasks:Ljava/util/HashMap;
    invoke-static {v4}, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->access$400(Lcom/samsung/musicplus/bitmapcache/BitmapCache;)Ljava/util/HashMap;

    move-result-object v4

    iget-object v6, p0, Lcom/samsung/musicplus/bitmapcache/BitmapCache$LoaderAsyncTask;->mUri:Landroid/net/Uri;

    invoke-virtual {v4, v6}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 402
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 403
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/samsung/musicplus/bitmapcache/BitmapCache$LoaderAsyncTask;->mStartTime:J

    sub-long v2, v4, v6

    .line 404
    .local v2, "elapsedTime":J
    if-eqz p1, :cond_2

    .line 405
    iget-object v4, p0, Lcom/samsung/musicplus/bitmapcache/BitmapCache$LoaderAsyncTask;->this$0:Lcom/samsung/musicplus/bitmapcache/BitmapCache;

    # getter for: Lcom/samsung/musicplus/bitmapcache/BitmapCache;->mCache:Landroid/util/LruCache;
    invoke-static {v4}, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->access$500(Lcom/samsung/musicplus/bitmapcache/BitmapCache;)Landroid/util/LruCache;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/musicplus/bitmapcache/BitmapCache$LoaderAsyncTask;->mUri:Landroid/net/Uri;

    iget v6, p0, Lcom/samsung/musicplus/bitmapcache/BitmapCache$LoaderAsyncTask;->mHeight:I

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v4, v5, p1}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 406
    # getter for: Lcom/samsung/musicplus/bitmapcache/BitmapCache;->DEBUG:Z
    invoke-static {}, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->access$000()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 407
    # getter for: Lcom/samsung/musicplus/bitmapcache/BitmapCache;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->access$100()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/samsung/musicplus/bitmapcache/BitmapCache$LoaderAsyncTask;->mUri:Landroid/net/Uri;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " putToCache: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 409
    :cond_1
    iget-object v4, p0, Lcom/samsung/musicplus/bitmapcache/BitmapCache$LoaderAsyncTask;->mPublish:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/bitmapcache/BitmapCache$Publisher;

    .line 410
    .local v0, "cur":Lcom/samsung/musicplus/bitmapcache/BitmapCache$Publisher;
    iget-object v4, p0, Lcom/samsung/musicplus/bitmapcache/BitmapCache$LoaderAsyncTask;->mUri:Landroid/net/Uri;

    invoke-interface {v0, v4, p1, v2, v3}, Lcom/samsung/musicplus/bitmapcache/BitmapCache$Publisher;->onPublishImage(Landroid/net/Uri;Landroid/graphics/Bitmap;J)V

    goto :goto_0

    .line 402
    .end local v0    # "cur":Lcom/samsung/musicplus/bitmapcache/BitmapCache$Publisher;
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "elapsedTime":J
    :catchall_0
    move-exception v4

    :try_start_1
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v4

    .line 413
    .restart local v2    # "elapsedTime":J
    :cond_2
    iget-object v4, p0, Lcom/samsung/musicplus/bitmapcache/BitmapCache$LoaderAsyncTask;->mPublish:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .restart local v1    # "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/bitmapcache/BitmapCache$Publisher;

    .line 414
    .restart local v0    # "cur":Lcom/samsung/musicplus/bitmapcache/BitmapCache$Publisher;
    # getter for: Lcom/samsung/musicplus/bitmapcache/BitmapCache;->DEBUG:Z
    invoke-static {}, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->access$000()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 415
    # getter for: Lcom/samsung/musicplus/bitmapcache/BitmapCache;->LOG_TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->access$100()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v6, p0, Lcom/samsung/musicplus/bitmapcache/BitmapCache$LoaderAsyncTask;->mUri:Landroid/net/Uri;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " unable to load elapsedTime: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 417
    :cond_3
    iget-object v4, p0, Lcom/samsung/musicplus/bitmapcache/BitmapCache$LoaderAsyncTask;->mUri:Landroid/net/Uri;

    invoke-interface {v0, v4, v2, v3}, Lcom/samsung/musicplus/bitmapcache/BitmapCache$Publisher;->onFailPublishImage(Landroid/net/Uri;J)V

    goto :goto_1

    .line 420
    .end local v0    # "cur":Lcom/samsung/musicplus/bitmapcache/BitmapCache$Publisher;
    :cond_4
    iget-object v4, p0, Lcom/samsung/musicplus/bitmapcache/BitmapCache$LoaderAsyncTask;->this$0:Lcom/samsung/musicplus/bitmapcache/BitmapCache;

    iget-object v5, p0, Lcom/samsung/musicplus/bitmapcache/BitmapCache$LoaderAsyncTask;->mUri:Landroid/net/Uri;

    # invokes: Lcom/samsung/musicplus/bitmapcache/BitmapCache;->fireListeners(Landroid/net/Uri;Landroid/graphics/Bitmap;J)V
    invoke-static {v4, v5, p1, v2, v3}, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->access$600(Lcom/samsung/musicplus/bitmapcache/BitmapCache;Landroid/net/Uri;Landroid/graphics/Bitmap;J)V

    .line 421
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 352
    check-cast p1, Landroid/graphics/Bitmap;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/bitmapcache/BitmapCache$LoaderAsyncTask;->onPostExecute(Landroid/graphics/Bitmap;)V

    return-void
.end method
