.class public Lcom/samsung/musicplus/bitmapcache/AlbArtUtilsLoader;
.super Ljava/lang/Object;
.source "AlbArtUtilsLoader.java"

# interfaces
.implements Lcom/samsung/musicplus/bitmapcache/BitmapCache$Loader;


# static fields
.field private static final BIG_ALBUM_ART_SIZE:I = 0x320

.field private static final REMOTE_URI:I = 0x1

.field private static final sRemoteUriMather:Landroid/content/UriMatcher;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 61
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    sput-object v0, Lcom/samsung/musicplus/bitmapcache/AlbArtUtilsLoader;->sRemoteUriMather:Landroid/content/UriMatcher;

    .line 66
    sget-object v0, Lcom/samsung/musicplus/bitmapcache/AlbArtUtilsLoader;->sRemoteUriMather:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.musicplus"

    const-string v2, "/dlna_album_art/*"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 67
    sget-object v0, Lcom/samsung/musicplus/bitmapcache/AlbArtUtilsLoader;->sRemoteUriMather:Landroid/content/UriMatcher;

    const-string v1, "com.samsung.android.sdk.samsunglink.provider.SLinkMedia"

    const-string v2, "/audio/*"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 69
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public isRemoteUri(Landroid/net/Uri;)Z
    .locals 5
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v0, 0x1

    .line 54
    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    .line 55
    .local v1, "scheme":Ljava/lang/String;
    const-string v2, "content"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "content"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    sget-object v2, Lcom/samsung/musicplus/bitmapcache/AlbArtUtilsLoader;->sRemoteUriMather:Landroid/content/UriMatcher;

    invoke-virtual {v2, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v2

    if-ne v2, v0, :cond_1

    .line 57
    .local v0, "res":Z
    :cond_0
    :goto_0
    const-string v2, "AlbArtUtilsLoader"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isRemoteUri: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " res: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    return v0

    .line 55
    .end local v0    # "res":Z
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onLoadImage(Landroid/content/Context;Landroid/net/Uri;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "uri"    # Landroid/net/Uri;
    .param p3, "height"    # I
    .param p4, "options"    # Landroid/graphics/BitmapFactory$Options;

    .prologue
    const/16 v10, 0x320

    .line 28
    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v6

    .line 30
    .local v6, "u":Ljava/lang/String;
    :try_start_0
    invoke-virtual {p2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    .line 31
    .local v2, "id":Ljava/lang/String;
    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 32
    .local v4, "longId":J
    const/4 v7, 0x0

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v8

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v9

    sub-int/2addr v8, v9

    invoke-virtual {v6, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v6

    .line 33
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    const/4 v8, -0x1

    invoke-static {p1, v6, v7, v8}, Lcom/samsung/musicplus/util/AlbumArtUtils;->getArtworkBitmap(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Object;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 34
    .local v0, "bm":Landroid/graphics/Bitmap;
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    if-le v7, v10, :cond_1

    .line 35
    const-string v7, "AlbArtUtilsLoader"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "size is too big!!, height : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 36
    const/16 v7, 0x320

    const/16 v8, 0x320

    const/4 v9, 0x1

    invoke-static {v0, v7, v8, v9}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 38
    .local v3, "resizeBm":Landroid/graphics/Bitmap;
    invoke-virtual {v3, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 39
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 41
    :cond_0
    move-object v0, v3

    .line 48
    .end local v0    # "bm":Landroid/graphics/Bitmap;
    .end local v2    # "id":Ljava/lang/String;
    .end local v3    # "resizeBm":Landroid/graphics/Bitmap;
    .end local v4    # "longId":J
    :cond_1
    :goto_0
    return-object v0

    .line 44
    :catch_0
    move-exception v1

    .line 48
    .local v1, "e":Ljava/lang/NumberFormatException;
    const/4 v7, 0x0

    invoke-static {p1, v6, v7, p3}, Lcom/samsung/musicplus/util/AlbumArtUtils;->getArtworkBitmap(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Object;I)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method
