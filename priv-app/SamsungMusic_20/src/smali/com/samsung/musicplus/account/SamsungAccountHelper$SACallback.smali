.class Lcom/samsung/musicplus/account/SamsungAccountHelper$SACallback;
.super Lcom/msc/sa/aidl/ISACallback$Stub;
.source "SamsungAccountHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/account/SamsungAccountHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SACallback"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/account/SamsungAccountHelper;


# direct methods
.method private constructor <init>(Lcom/samsung/musicplus/account/SamsungAccountHelper;)V
    .locals 0

    .prologue
    .line 222
    iput-object p1, p0, Lcom/samsung/musicplus/account/SamsungAccountHelper$SACallback;->this$0:Lcom/samsung/musicplus/account/SamsungAccountHelper;

    invoke-direct {p0}, Lcom/msc/sa/aidl/ISACallback$Stub;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/musicplus/account/SamsungAccountHelper;Lcom/samsung/musicplus/account/SamsungAccountHelper$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/musicplus/account/SamsungAccountHelper;
    .param p2, "x1"    # Lcom/samsung/musicplus/account/SamsungAccountHelper$1;

    .prologue
    .line 222
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/account/SamsungAccountHelper$SACallback;-><init>(Lcom/samsung/musicplus/account/SamsungAccountHelper;)V

    return-void
.end method

.method private handleFail(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "resultData"    # Landroid/os/Bundle;

    .prologue
    .line 245
    const-string v2, "error_code"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 246
    .local v0, "error":Ljava/lang/String;
    const-string v2, "MusicAccount"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onReceiveAccessToken error : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    const-string v2, "error_message"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 248
    .local v1, "errorMsg":Ljava/lang/String;
    const-string v2, "MusicAccount"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onReceiveAccessToken errorMsg : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 251
    iget-object v2, p0, Lcom/samsung/musicplus/account/SamsungAccountHelper$SACallback;->this$0:Lcom/samsung/musicplus/account/SamsungAccountHelper;

    const/4 v3, 0x0

    # setter for: Lcom/samsung/musicplus/account/SamsungAccountHelper;->mInfo:Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;
    invoke-static {v2, v3}, Lcom/samsung/musicplus/account/SamsungAccountHelper;->access$202(Lcom/samsung/musicplus/account/SamsungAccountHelper;Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;)Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;

    .line 252
    return-void
.end method

.method private handleSuccess(Landroid/os/Bundle;)V
    .locals 12
    .param p1, "resultData"    # Landroid/os/Bundle;

    .prologue
    .line 255
    const-string v9, "access_token"

    invoke-virtual {p1, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 256
    .local v0, "accessToken":Ljava/lang/String;
    const-string v9, "MusicAccount"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "onReceiveAccessToken accessToken : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    const-string v9, "user_id"

    invoke-virtual {p1, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 259
    .local v8, "user_id":Ljava/lang/String;
    const-string v9, "MusicAccount"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "onReceiveAccessToken user_id : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 261
    const-string v9, "login_id"

    invoke-virtual {p1, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 262
    .local v5, "login_id":Ljava/lang/String;
    const-string v9, "MusicAccount"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "onReceiveAccessToken login_id : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    const-string v9, "login_id_type"

    invoke-virtual {p1, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 265
    .local v6, "login_id_type":Ljava/lang/String;
    const-string v9, "MusicAccount"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "onReceiveAccessToken login_id_type : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 267
    const-string v9, "mcc"

    invoke-virtual {p1, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 268
    .local v7, "mcc":Ljava/lang/String;
    const-string v9, "MusicAccount"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "onReceiveAccessToken mcc : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    const-string v9, "cc"

    invoke-virtual {p1, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 271
    .local v3, "cc":Ljava/lang/String;
    const-string v9, "MusicAccount"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "onReceiveAccessToken cc : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 273
    const-string v9, "api_server_url"

    invoke-virtual {p1, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 274
    .local v1, "api_server_url":Ljava/lang/String;
    const-string v9, "MusicAccount"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "onReceiveAccessToken api_server_url : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    const-string v9, "auth_server_url"

    invoke-virtual {p1, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 277
    .local v2, "auth_server_url":Ljava/lang/String;
    const-string v9, "MusicAccount"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "onReceiveAccessToken auth_server_url : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 279
    const-string v9, "device_physical_address_text"

    invoke-virtual {p1, v9}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 281
    .local v4, "device_physical_address_text":Ljava/lang/String;
    const-string v9, "MusicAccount"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "onReceiveAccessToken device_physical_address_text : "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 284
    iget-object v9, p0, Lcom/samsung/musicplus/account/SamsungAccountHelper$SACallback;->this$0:Lcom/samsung/musicplus/account/SamsungAccountHelper;

    new-instance v10, Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;

    invoke-direct {v10}, Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;-><init>()V

    # setter for: Lcom/samsung/musicplus/account/SamsungAccountHelper;->mInfo:Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;
    invoke-static {v9, v10}, Lcom/samsung/musicplus/account/SamsungAccountHelper;->access$202(Lcom/samsung/musicplus/account/SamsungAccountHelper;Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;)Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;

    .line 285
    iget-object v9, p0, Lcom/samsung/musicplus/account/SamsungAccountHelper$SACallback;->this$0:Lcom/samsung/musicplus/account/SamsungAccountHelper;

    # getter for: Lcom/samsung/musicplus/account/SamsungAccountHelper;->mInfo:Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;
    invoke-static {v9}, Lcom/samsung/musicplus/account/SamsungAccountHelper;->access$200(Lcom/samsung/musicplus/account/SamsungAccountHelper;)Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;

    move-result-object v9

    iput-object v0, v9, Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;->accessToken:Ljava/lang/String;

    .line 286
    iget-object v9, p0, Lcom/samsung/musicplus/account/SamsungAccountHelper$SACallback;->this$0:Lcom/samsung/musicplus/account/SamsungAccountHelper;

    # getter for: Lcom/samsung/musicplus/account/SamsungAccountHelper;->mInfo:Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;
    invoke-static {v9}, Lcom/samsung/musicplus/account/SamsungAccountHelper;->access$200(Lcom/samsung/musicplus/account/SamsungAccountHelper;)Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;

    move-result-object v9

    iput-object v8, v9, Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;->userId:Ljava/lang/String;

    .line 287
    iget-object v9, p0, Lcom/samsung/musicplus/account/SamsungAccountHelper$SACallback;->this$0:Lcom/samsung/musicplus/account/SamsungAccountHelper;

    # getter for: Lcom/samsung/musicplus/account/SamsungAccountHelper;->mInfo:Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;
    invoke-static {v9}, Lcom/samsung/musicplus/account/SamsungAccountHelper;->access$200(Lcom/samsung/musicplus/account/SamsungAccountHelper;)Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;

    move-result-object v9

    iput-object v7, v9, Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;->mcc:Ljava/lang/String;

    .line 288
    iget-object v9, p0, Lcom/samsung/musicplus/account/SamsungAccountHelper$SACallback;->this$0:Lcom/samsung/musicplus/account/SamsungAccountHelper;

    # getter for: Lcom/samsung/musicplus/account/SamsungAccountHelper;->mInfo:Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;
    invoke-static {v9}, Lcom/samsung/musicplus/account/SamsungAccountHelper;->access$200(Lcom/samsung/musicplus/account/SamsungAccountHelper;)Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;

    move-result-object v9

    iput-object v3, v9, Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;->cc:Ljava/lang/String;

    .line 289
    iget-object v9, p0, Lcom/samsung/musicplus/account/SamsungAccountHelper$SACallback;->this$0:Lcom/samsung/musicplus/account/SamsungAccountHelper;

    # getter for: Lcom/samsung/musicplus/account/SamsungAccountHelper;->mInfo:Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;
    invoke-static {v9}, Lcom/samsung/musicplus/account/SamsungAccountHelper;->access$200(Lcom/samsung/musicplus/account/SamsungAccountHelper;)Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;

    move-result-object v9

    iput-object v5, v9, Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;->loginId:Ljava/lang/String;

    .line 290
    iget-object v9, p0, Lcom/samsung/musicplus/account/SamsungAccountHelper$SACallback;->this$0:Lcom/samsung/musicplus/account/SamsungAccountHelper;

    # getter for: Lcom/samsung/musicplus/account/SamsungAccountHelper;->mInfo:Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;
    invoke-static {v9}, Lcom/samsung/musicplus/account/SamsungAccountHelper;->access$200(Lcom/samsung/musicplus/account/SamsungAccountHelper;)Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;

    move-result-object v9

    iput-object v6, v9, Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;->loginType:Ljava/lang/String;

    .line 291
    return-void
.end method


# virtual methods
.method public onReceiveAccessToken(IZLandroid/os/Bundle;)V
    .locals 3
    .param p1, "requestID"    # I
    .param p2, "isSuccess"    # Z
    .param p3, "resultData"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 227
    const-string v0, "MusicAccount"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onReceiveAccessToken requestID "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " success : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " data : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 229
    if-eqz p3, :cond_0

    .line 230
    if-eqz p2, :cond_2

    .line 231
    invoke-direct {p0, p3}, Lcom/samsung/musicplus/account/SamsungAccountHelper$SACallback;->handleSuccess(Landroid/os/Bundle;)V

    .line 237
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/samsung/musicplus/account/SamsungAccountHelper$SACallback;->this$0:Lcom/samsung/musicplus/account/SamsungAccountHelper;

    # invokes: Lcom/samsung/musicplus/account/SamsungAccountHelper;->releaseService()V
    invoke-static {v0}, Lcom/samsung/musicplus/account/SamsungAccountHelper;->access$300(Lcom/samsung/musicplus/account/SamsungAccountHelper;)V

    .line 239
    iget-object v0, p0, Lcom/samsung/musicplus/account/SamsungAccountHelper$SACallback;->this$0:Lcom/samsung/musicplus/account/SamsungAccountHelper;

    # getter for: Lcom/samsung/musicplus/account/SamsungAccountHelper;->mInfoListener:Lcom/samsung/musicplus/account/SamsungAccountHelper$OnAccountInfoListener;
    invoke-static {v0}, Lcom/samsung/musicplus/account/SamsungAccountHelper;->access$400(Lcom/samsung/musicplus/account/SamsungAccountHelper;)Lcom/samsung/musicplus/account/SamsungAccountHelper$OnAccountInfoListener;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 240
    iget-object v0, p0, Lcom/samsung/musicplus/account/SamsungAccountHelper$SACallback;->this$0:Lcom/samsung/musicplus/account/SamsungAccountHelper;

    # getter for: Lcom/samsung/musicplus/account/SamsungAccountHelper;->mInfoListener:Lcom/samsung/musicplus/account/SamsungAccountHelper$OnAccountInfoListener;
    invoke-static {v0}, Lcom/samsung/musicplus/account/SamsungAccountHelper;->access$400(Lcom/samsung/musicplus/account/SamsungAccountHelper;)Lcom/samsung/musicplus/account/SamsungAccountHelper$OnAccountInfoListener;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/musicplus/account/SamsungAccountHelper$SACallback;->this$0:Lcom/samsung/musicplus/account/SamsungAccountHelper;

    # getter for: Lcom/samsung/musicplus/account/SamsungAccountHelper;->mInfo:Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;
    invoke-static {v1}, Lcom/samsung/musicplus/account/SamsungAccountHelper;->access$200(Lcom/samsung/musicplus/account/SamsungAccountHelper;)Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/samsung/musicplus/account/SamsungAccountHelper$OnAccountInfoListener;->onAccountInfo(Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;)V

    .line 242
    :cond_1
    return-void

    .line 233
    :cond_2
    invoke-direct {p0, p3}, Lcom/samsung/musicplus/account/SamsungAccountHelper$SACallback;->handleFail(Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public onReceiveAuthCode(IZLandroid/os/Bundle;)V
    .locals 3
    .param p1, "requestID"    # I
    .param p2, "isSuccess"    # Z
    .param p3, "resultData"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 310
    const-string v0, "MusicAccount"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onReceiveAuthCode requestID "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " success : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " data : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 312
    return-void
.end method

.method public onReceiveChecklistValidation(IZLandroid/os/Bundle;)V
    .locals 3
    .param p1, "requestID"    # I
    .param p2, "isSuccess"    # Z
    .param p3, "resultData"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 296
    const-string v0, "MusicAccount"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onReceiveChecklistValidation requestID "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " success : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " data : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 298
    return-void
.end method

.method public onReceiveDisclaimerAgreement(IZLandroid/os/Bundle;)V
    .locals 3
    .param p1, "requestID"    # I
    .param p2, "isSuccess"    # Z
    .param p3, "resultData"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 303
    const-string v0, "MusicAccount"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onReceiveDisclaimerAgreement requestID "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " success : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " data : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 305
    return-void
.end method

.method public onReceivePasswordConfirmation(IZLandroid/os/Bundle;)V
    .locals 3
    .param p1, "requestID"    # I
    .param p2, "isSuccess"    # Z
    .param p3, "resultData"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 324
    const-string v0, "MusicAccount"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onReceivePasswordConfirmation requestID "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " success : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " data : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 326
    return-void
.end method

.method public onReceiveSCloudAccessToken(IZLandroid/os/Bundle;)V
    .locals 3
    .param p1, "requestID"    # I
    .param p2, "isSuccess"    # Z
    .param p3, "resultData"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 317
    const-string v0, "MusicAccount"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onReceiveSCloudAccessToken requestID "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " success : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " data : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 319
    return-void
.end method
