.class public interface abstract Lcom/samsung/musicplus/account/SamsungAccountConstant;
.super Ljava/lang/Object;
.source "SamsungAccountConstant.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/account/SamsungAccountConstant$SERVER_TYPE;,
        Lcom/samsung/musicplus/account/SamsungAccountConstant$AUTH_TYPE;
    }
.end annotation


# static fields
.field public static final CLIENT_ID:Ljava/lang/String; = "f05tf070ze"

.field public static final CLIENT_SECRET:Ljava/lang/String; = "031161ED1956233064FA49E6B7B06256"

.field public static final FWK_TARGET:I = 0x0

.field public static final INTENT_ACTION_SIGN_IN:Ljava/lang/String; = "android.intent.action.SAMSUNGACCOUNT_SIGNIN_COMPLETED"

.field public static final INTENT_ACTION_SIGN_OUT:Ljava/lang/String; = "android.intent.action.SAMSUNGACCOUNT_SIGNOUT_COMPLETED"

.field public static final PLAIN_UDP:I = 0x5

.field public static final PROXY_PORT_INSTANCE:Ljava/lang/String; = "9050"

.field public static final REQUEST_SERVICE_ID:I = 0x29ebf

.field public static final SAMSUNG_ACCOUNT_TYPE:Ljava/lang/String; = "com.osp.app.signin"

.field public static final SERVICE_PACKAGE_NAME:Ljava/lang/String; = "com.osp.app.signin"

.field public static final SERVICE_PORT:Ljava/lang/String; = "1007"

.field public static final START_SERVICE:Ljava/lang/String; = "com.msc.action.samsungaccount.REQUEST_SERVICE"
