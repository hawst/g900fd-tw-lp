.class Lcom/samsung/musicplus/account/SlinkReadyService$1;
.super Ljava/lang/Object;
.source "SlinkReadyService.java"

# interfaces
.implements Lcom/samsung/musicplus/account/SamsungAccountHelper$OnAccountInfoListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/account/SlinkReadyService;->onCreate()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/account/SlinkReadyService;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/account/SlinkReadyService;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 53
    iput-object p1, p0, Lcom/samsung/musicplus/account/SlinkReadyService$1;->this$0:Lcom/samsung/musicplus/account/SlinkReadyService;

    iput-object p2, p0, Lcom/samsung/musicplus/account/SlinkReadyService$1;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAccountInfo(Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;)V
    .locals 3
    .param p1, "info"    # Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;

    .prologue
    .line 57
    const-string v0, "MusicSlink"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onAccountInfo info "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    if-eqz p1, :cond_0

    .line 59
    iget-object v0, p0, Lcom/samsung/musicplus/account/SlinkReadyService$1;->this$0:Lcom/samsung/musicplus/account/SlinkReadyService;

    iget-object v1, p0, Lcom/samsung/musicplus/account/SlinkReadyService$1;->val$context:Landroid/content/Context;

    # invokes: Lcom/samsung/musicplus/account/SlinkReadyService;->initializeScsConnection(Landroid/content/Context;Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;)V
    invoke-static {v0, v1, p1}, Lcom/samsung/musicplus/account/SlinkReadyService;->access$000(Lcom/samsung/musicplus/account/SlinkReadyService;Landroid/content/Context;Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;)V

    .line 60
    iget-object v0, p0, Lcom/samsung/musicplus/account/SlinkReadyService$1;->this$0:Lcom/samsung/musicplus/account/SlinkReadyService;

    const/4 v1, 0x1

    # setter for: Lcom/samsung/musicplus/account/SlinkReadyService;->mInitialized:Z
    invoke-static {v0, v1}, Lcom/samsung/musicplus/account/SlinkReadyService;->access$102(Lcom/samsung/musicplus/account/SlinkReadyService;Z)Z

    .line 63
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/account/SlinkReadyService$1;->this$0:Lcom/samsung/musicplus/account/SlinkReadyService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/account/SlinkReadyService;->stopSelf()V

    .line 64
    return-void
.end method
