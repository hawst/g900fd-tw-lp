.class Lcom/samsung/musicplus/account/SamsungAccountHelper$2;
.super Landroid/content/BroadcastReceiver;
.source "SamsungAccountHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/account/SamsungAccountHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/account/SamsungAccountHelper;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/account/SamsungAccountHelper;)V
    .locals 0

    .prologue
    .line 133
    iput-object p1, p0, Lcom/samsung/musicplus/account/SamsungAccountHelper$2;->this$0:Lcom/samsung/musicplus/account/SamsungAccountHelper;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 139
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 140
    .local v0, "action":Ljava/lang/String;
    const-string v1, "MusicAccount"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mSignInReceiver action : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    const-string v1, "android.intent.action.SAMSUNGACCOUNT_SIGNIN_COMPLETED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 142
    iget-object v1, p0, Lcom/samsung/musicplus/account/SamsungAccountHelper$2;->this$0:Lcom/samsung/musicplus/account/SamsungAccountHelper;

    invoke-virtual {v1}, Lcom/samsung/musicplus/account/SamsungAccountHelper;->requestInfo()Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;

    .line 146
    :cond_0
    :goto_0
    return-void

    .line 143
    :cond_1
    const-string v1, "android.intent.action.SAMSUNGACCOUNT_SIGNOUT_COMPLETED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 144
    iget-object v1, p0, Lcom/samsung/musicplus/account/SamsungAccountHelper$2;->this$0:Lcom/samsung/musicplus/account/SamsungAccountHelper;

    const/4 v2, 0x0

    # setter for: Lcom/samsung/musicplus/account/SamsungAccountHelper;->mInfo:Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;
    invoke-static {v1, v2}, Lcom/samsung/musicplus/account/SamsungAccountHelper;->access$202(Lcom/samsung/musicplus/account/SamsungAccountHelper;Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;)Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;

    goto :goto_0
.end method
