.class public Lcom/samsung/musicplus/account/SlinkReadyService;
.super Landroid/app/Service;
.source "SlinkReadyService.java"


# static fields
.field public static final ACTION_SCS_INITIALIZE:Ljava/lang/String; = "com.samsung.music.action.SCS_INITIALIZE"

.field private static final TAG:Ljava/lang/String; = "MusicSlink"


# instance fields
.field private mInitialized:Z

.field private mSA:Lcom/samsung/musicplus/account/SamsungAccountHelper;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 32
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/account/SlinkReadyService;->mInitialized:Z

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/account/SlinkReadyService;Landroid/content/Context;Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/account/SlinkReadyService;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Lcom/samsung/musicplus/account/SlinkReadyService;->initializeScsConnection(Landroid/content/Context;Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;)V

    return-void
.end method

.method static synthetic access$102(Lcom/samsung/musicplus/account/SlinkReadyService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/account/SlinkReadyService;
    .param p1, "x1"    # Z

    .prologue
    .line 22
    iput-boolean p1, p0, Lcom/samsung/musicplus/account/SlinkReadyService;->mInitialized:Z

    return p1
.end method

.method private initializeScsConnection(Landroid/content/Context;Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "info"    # Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;

    .prologue
    .line 83
    const-string v3, "ntcl://Null?NTSCoreStart?"

    invoke-static {p1, v3, p2}, Lcom/samsung/musicplus/provider/MusicContents$Audio$DeviceContents;->getScsConfigPath(Landroid/content/Context;Ljava/lang/String;Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;)Ljava/lang/String;

    move-result-object v1

    .line 84
    .local v1, "path":Ljava/lang/String;
    const-string v3, "MusicSlink"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "initializeScsConnection path : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    new-instance v2, Landroid/media/MediaPlayer;

    invoke-direct {v2}, Landroid/media/MediaPlayer;-><init>()V

    .line 87
    .local v2, "player":Landroid/media/MediaPlayer;
    :try_start_0
    invoke-virtual {v2, v1}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 98
    invoke-virtual {v2}, Landroid/media/MediaPlayer;->release()V

    .line 99
    const/4 v2, 0x0

    .line 101
    :goto_0
    return-void

    .line 88
    :catch_0
    move-exception v0

    .line 89
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    :try_start_1
    const-string v3, "MusicSlink"

    const-string v4, "initializeScsConnection IllegalArgumentException"

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 98
    invoke-virtual {v2}, Landroid/media/MediaPlayer;->release()V

    .line 99
    const/4 v2, 0x0

    .line 100
    goto :goto_0

    .line 90
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 91
    .local v0, "e":Ljava/lang/SecurityException;
    :try_start_2
    const-string v3, "MusicSlink"

    const-string v4, "initializeScsConnection SecurityException"

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 98
    invoke-virtual {v2}, Landroid/media/MediaPlayer;->release()V

    .line 99
    const/4 v2, 0x0

    .line 100
    goto :goto_0

    .line 92
    .end local v0    # "e":Ljava/lang/SecurityException;
    :catch_2
    move-exception v0

    .line 93
    .local v0, "e":Ljava/lang/IllegalStateException;
    :try_start_3
    const-string v3, "MusicSlink"

    const-string v4, "initializeScsConnection IllegalStateException"

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 98
    invoke-virtual {v2}, Landroid/media/MediaPlayer;->release()V

    .line 99
    const/4 v2, 0x0

    .line 100
    goto :goto_0

    .line 94
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_3
    move-exception v0

    .line 95
    .local v0, "e":Ljava/io/IOException;
    :try_start_4
    const-string v3, "MusicSlink"

    const-string v4, "initializeScsConnection IOException"

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 98
    invoke-virtual {v2}, Landroid/media/MediaPlayer;->release()V

    .line 99
    const/4 v2, 0x0

    .line 100
    goto :goto_0

    .line 98
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v3

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->release()V

    .line 99
    const/4 v2, 0x0

    throw v3
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 78
    const-string v0, "MusicSlink"

    const-string v1, "SCS Initialize service onBind"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 49
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 50
    const-string v1, "MusicSlink"

    const-string v2, "SCS Initialize service onCreate"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    invoke-virtual {p0}, Lcom/samsung/musicplus/account/SlinkReadyService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 52
    .local v0, "context":Landroid/content/Context;
    new-instance v1, Lcom/samsung/musicplus/account/SamsungAccountHelper;

    invoke-direct {v1, v0}, Lcom/samsung/musicplus/account/SamsungAccountHelper;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/musicplus/account/SlinkReadyService;->mSA:Lcom/samsung/musicplus/account/SamsungAccountHelper;

    .line 53
    iget-object v1, p0, Lcom/samsung/musicplus/account/SlinkReadyService;->mSA:Lcom/samsung/musicplus/account/SamsungAccountHelper;

    new-instance v2, Lcom/samsung/musicplus/account/SlinkReadyService$1;

    invoke-direct {v2, p0, v0}, Lcom/samsung/musicplus/account/SlinkReadyService$1;-><init>(Lcom/samsung/musicplus/account/SlinkReadyService;Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Lcom/samsung/musicplus/account/SamsungAccountHelper;->setOnAccountInfoListener(Lcom/samsung/musicplus/account/SamsungAccountHelper$OnAccountInfoListener;)V

    .line 66
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 70
    const-string v0, "MusicSlink"

    const-string v1, "SCS Initialize service onDestroy"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    iget-object v0, p0, Lcom/samsung/musicplus/account/SlinkReadyService;->mSA:Lcom/samsung/musicplus/account/SamsungAccountHelper;

    invoke-virtual {v0}, Lcom/samsung/musicplus/account/SamsungAccountHelper;->release()V

    .line 72
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 73
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 36
    if-eqz p1, :cond_0

    .line 37
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 38
    .local v0, "action":Ljava/lang/String;
    const-string v1, "com.samsung.music.action.SCS_INITIALIZE"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 39
    iget-boolean v1, p0, Lcom/samsung/musicplus/account/SlinkReadyService;->mInitialized:Z

    if-nez v1, :cond_0

    .line 40
    iget-object v1, p0, Lcom/samsung/musicplus/account/SlinkReadyService;->mSA:Lcom/samsung/musicplus/account/SamsungAccountHelper;

    invoke-virtual {v1}, Lcom/samsung/musicplus/account/SamsungAccountHelper;->requestInfo()Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;

    .line 44
    .end local v0    # "action":Ljava/lang/String;
    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    move-result v1

    return v1
.end method
