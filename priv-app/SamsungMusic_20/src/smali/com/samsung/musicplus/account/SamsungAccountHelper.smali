.class public Lcom/samsung/musicplus/account/SamsungAccountHelper;
.super Ljava/lang/Object;
.source "SamsungAccountHelper.java"

# interfaces
.implements Landroid/content/ServiceConnection;
.implements Lcom/samsung/musicplus/account/SamsungAccountConstant;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/account/SamsungAccountHelper$SACallback;,
        Lcom/samsung/musicplus/account/SamsungAccountHelper$OnAccountInfoListener;,
        Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "MusicAccount"


# instance fields
.field private mCallback:Lcom/samsung/musicplus/account/SamsungAccountHelper$SACallback;

.field private mContext:Landroid/content/Context;

.field private mISaService:Lcom/msc/sa/aidl/ISAService;

.field private mInfo:Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;

.field private mInfoListener:Lcom/samsung/musicplus/account/SamsungAccountHelper$OnAccountInfoListener;

.field private mIsBind:Z

.field private mRegistrationCode:Ljava/lang/String;

.field private final mSignInReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 133
    new-instance v1, Lcom/samsung/musicplus/account/SamsungAccountHelper$2;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/account/SamsungAccountHelper$2;-><init>(Lcom/samsung/musicplus/account/SamsungAccountHelper;)V

    iput-object v1, p0, Lcom/samsung/musicplus/account/SamsungAccountHelper;->mSignInReceiver:Landroid/content/BroadcastReceiver;

    .line 187
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/musicplus/account/SamsungAccountHelper;->mIsBind:Z

    .line 126
    iput-object p1, p0, Lcom/samsung/musicplus/account/SamsungAccountHelper;->mContext:Landroid/content/Context;

    .line 127
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 128
    .local v0, "f":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.SAMSUNGACCOUNT_SIGNIN_COMPLETED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 129
    const-string v1, "android.intent.action.SAMSUNGACCOUNT_SIGNOUT_COMPLETED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 130
    iget-object v1, p0, Lcom/samsung/musicplus/account/SamsungAccountHelper;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/musicplus/account/SamsungAccountHelper;->mSignInReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 131
    return-void
.end method

.method static synthetic access$100(Lcom/samsung/musicplus/account/SamsungAccountHelper;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/account/SamsungAccountHelper;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/samsung/musicplus/account/SamsungAccountHelper;->requestInformation()V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/musicplus/account/SamsungAccountHelper;)Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/account/SamsungAccountHelper;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/samsung/musicplus/account/SamsungAccountHelper;->mInfo:Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;

    return-object v0
.end method

.method static synthetic access$202(Lcom/samsung/musicplus/account/SamsungAccountHelper;Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;)Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/account/SamsungAccountHelper;
    .param p1, "x1"    # Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;

    .prologue
    .line 27
    iput-object p1, p0, Lcom/samsung/musicplus/account/SamsungAccountHelper;->mInfo:Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;

    return-object p1
.end method

.method static synthetic access$300(Lcom/samsung/musicplus/account/SamsungAccountHelper;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/account/SamsungAccountHelper;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/samsung/musicplus/account/SamsungAccountHelper;->releaseService()V

    return-void
.end method

.method static synthetic access$400(Lcom/samsung/musicplus/account/SamsungAccountHelper;)Lcom/samsung/musicplus/account/SamsungAccountHelper$OnAccountInfoListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/account/SamsungAccountHelper;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/samsung/musicplus/account/SamsungAccountHelper;->mInfoListener:Lcom/samsung/musicplus/account/SamsungAccountHelper$OnAccountInfoListener;

    return-object v0
.end method

.method private bindService()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 190
    const-string v1, "MusicAccount"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "bindService mIsBind : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/samsung/musicplus/account/SamsungAccountHelper;->mIsBind:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    iget-boolean v1, p0, Lcom/samsung/musicplus/account/SamsungAccountHelper;->mIsBind:Z

    if-nez v1, :cond_0

    .line 192
    iput-boolean v4, p0, Lcom/samsung/musicplus/account/SamsungAccountHelper;->mIsBind:Z

    .line 193
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.msc.action.samsungaccount.REQUEST_SERVICE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 194
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "com.osp.app.signin"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 195
    iget-object v1, p0, Lcom/samsung/musicplus/account/SamsungAccountHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0, p0, v4}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 198
    .end local v0    # "i":Landroid/content/Intent;
    :cond_0
    return-void
.end method

.method private isSignedIn(Landroid/content/Context;)Z
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 209
    const/4 v2, 0x0

    .line 210
    .local v2, "signed":Z
    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 211
    .local v1, "manager":Landroid/accounts/AccountManager;
    const-string v3, "com.osp.app.signin"

    invoke-virtual {v1, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 212
    .local v0, "accountArr":[Landroid/accounts/Account;
    array-length v3, v0

    if-lez v3, :cond_0

    .line 214
    const/4 v2, 0x1

    .line 218
    :cond_0
    const-string v3, "MusicAccount"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "isSignedIn "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    return v2
.end method

.method private releaseService()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 173
    iget-object v1, p0, Lcom/samsung/musicplus/account/SamsungAccountHelper;->mISaService:Lcom/msc/sa/aidl/ISAService;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/musicplus/account/SamsungAccountHelper;->mRegistrationCode:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 175
    :try_start_0
    iget-object v1, p0, Lcom/samsung/musicplus/account/SamsungAccountHelper;->mISaService:Lcom/msc/sa/aidl/ISAService;

    iget-object v2, p0, Lcom/samsung/musicplus/account/SamsungAccountHelper;->mRegistrationCode:Ljava/lang/String;

    invoke-interface {v1, v2}, Lcom/msc/sa/aidl/ISAService;->unregisterCallback(Ljava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 180
    :cond_0
    :goto_0
    iget-boolean v1, p0, Lcom/samsung/musicplus/account/SamsungAccountHelper;->mIsBind:Z

    if-eqz v1, :cond_1

    .line 181
    invoke-direct {p0}, Lcom/samsung/musicplus/account/SamsungAccountHelper;->unbindService()V

    .line 183
    :cond_1
    iput-object v3, p0, Lcom/samsung/musicplus/account/SamsungAccountHelper;->mISaService:Lcom/msc/sa/aidl/ISAService;

    .line 184
    iput-object v3, p0, Lcom/samsung/musicplus/account/SamsungAccountHelper;->mCallback:Lcom/samsung/musicplus/account/SamsungAccountHelper$SACallback;

    .line 185
    return-void

    .line 176
    :catch_0
    move-exception v0

    .line 177
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method private requestInformation()V
    .locals 7

    .prologue
    .line 93
    iget-object v2, p0, Lcom/samsung/musicplus/account/SamsungAccountHelper;->mISaService:Lcom/msc/sa/aidl/ISAService;

    if-nez v2, :cond_0

    .line 94
    const-string v2, "MusicAccount"

    const-string v3, "requestInformation mISaService is already null. Ignore this request "

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    :goto_0
    return-void

    .line 97
    :cond_0
    iget-object v2, p0, Lcom/samsung/musicplus/account/SamsungAccountHelper;->mContext:Landroid/content/Context;

    invoke-direct {p0, v2}, Lcom/samsung/musicplus/account/SamsungAccountHelper;->isSignedIn(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 98
    const-string v2, "MusicAccount"

    const-string v3, "requestInformation but it is not signed in state"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 105
    :cond_1
    :try_start_0
    iget-object v2, p0, Lcom/samsung/musicplus/account/SamsungAccountHelper;->mISaService:Lcom/msc/sa/aidl/ISAService;

    if-eqz v2, :cond_2

    .line 106
    iget-object v2, p0, Lcom/samsung/musicplus/account/SamsungAccountHelper;->mISaService:Lcom/msc/sa/aidl/ISAService;

    const-string v3, "f05tf070ze"

    const-string v4, "031161ED1956233064FA49E6B7B06256"

    const-string v5, "com.sec.android.app.music"

    iget-object v6, p0, Lcom/samsung/musicplus/account/SamsungAccountHelper;->mCallback:Lcom/samsung/musicplus/account/SamsungAccountHelper$SACallback;

    invoke-interface {v2, v3, v4, v5, v6}, Lcom/msc/sa/aidl/ISAService;->registerCallback(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/msc/sa/aidl/ISACallback;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/musicplus/account/SamsungAccountHelper;->mRegistrationCode:Ljava/lang/String;

    .line 108
    const-string v2, "MusicAccount"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onServiceConnected mRegistrationCode "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/musicplus/account/SamsungAccountHelper;->mRegistrationCode:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 111
    .local v0, "data":Landroid/os/Bundle;
    const-string v2, "additional"

    const/16 v3, 0x8

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "user_id"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "login_id"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "login_id_type"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    const-string v5, "mcc"

    aput-object v5, v3, v4

    const/4 v4, 0x4

    const-string v5, "cc"

    aput-object v5, v3, v4

    const/4 v4, 0x5

    const-string v5, "api_server_url"

    aput-object v5, v3, v4

    const/4 v4, 0x6

    const-string v5, "auth_server_url"

    aput-object v5, v3, v4

    const/4 v4, 0x7

    const-string v5, "device_physical_address_text"

    aput-object v5, v3, v4

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 116
    iget-object v2, p0, Lcom/samsung/musicplus/account/SamsungAccountHelper;->mISaService:Lcom/msc/sa/aidl/ISAService;

    const v3, 0x29ebf

    iget-object v4, p0, Lcom/samsung/musicplus/account/SamsungAccountHelper;->mRegistrationCode:Ljava/lang/String;

    invoke-interface {v2, v3, v4, v0}, Lcom/msc/sa/aidl/ISAService;->requestAccessToken(ILjava/lang/String;Landroid/os/Bundle;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 120
    .end local v0    # "data":Landroid/os/Bundle;
    :catch_0
    move-exception v1

    .line 121
    .local v1, "e":Landroid/os/RemoteException;
    const-string v2, "MusicAccount"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "requestInformation RemoteException "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 118
    .end local v1    # "e":Landroid/os/RemoteException;
    :cond_2
    :try_start_1
    const-string v2, "MusicAccount"

    const-string v3, "requestInformation mISaService is already null. Ignore this request "

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method private unbindService()V
    .locals 3

    .prologue
    .line 201
    const-string v0, "MusicAccount"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "unbindService mIsBind : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/samsung/musicplus/account/SamsungAccountHelper;->mIsBind:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    iget-boolean v0, p0, Lcom/samsung/musicplus/account/SamsungAccountHelper;->mIsBind:Z

    if-eqz v0, :cond_0

    .line 203
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/account/SamsungAccountHelper;->mIsBind:Z

    .line 204
    iget-object v0, p0, Lcom/samsung/musicplus/account/SamsungAccountHelper;->mContext:Landroid/content/Context;

    invoke-virtual {v0, p0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 206
    :cond_0
    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 72
    const-string v0, "MusicAccount"

    const-string v1, "onServiceConnected"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    invoke-static {p2}, Lcom/msc/sa/aidl/ISAService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/msc/sa/aidl/ISAService;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/account/SamsungAccountHelper;->mISaService:Lcom/msc/sa/aidl/ISAService;

    .line 74
    new-instance v0, Lcom/samsung/musicplus/account/SamsungAccountHelper$SACallback;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/samsung/musicplus/account/SamsungAccountHelper$SACallback;-><init>(Lcom/samsung/musicplus/account/SamsungAccountHelper;Lcom/samsung/musicplus/account/SamsungAccountHelper$1;)V

    iput-object v0, p0, Lcom/samsung/musicplus/account/SamsungAccountHelper;->mCallback:Lcom/samsung/musicplus/account/SamsungAccountHelper$SACallback;

    .line 75
    new-instance v0, Lcom/samsung/musicplus/account/SamsungAccountHelper$1;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/account/SamsungAccountHelper$1;-><init>(Lcom/samsung/musicplus/account/SamsungAccountHelper;)V

    invoke-virtual {v0}, Lcom/samsung/musicplus/account/SamsungAccountHelper$1;->start()V

    .line 83
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 3
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    const/4 v2, 0x0

    .line 87
    const-string v0, "MusicAccount"

    const-string v1, "onServiceDisconnected"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    iput-object v2, p0, Lcom/samsung/musicplus/account/SamsungAccountHelper;->mISaService:Lcom/msc/sa/aidl/ISAService;

    .line 89
    iput-object v2, p0, Lcom/samsung/musicplus/account/SamsungAccountHelper;->mCallback:Lcom/samsung/musicplus/account/SamsungAccountHelper$SACallback;

    .line 90
    return-void
.end method

.method public release()V
    .locals 2

    .prologue
    .line 163
    iget-object v0, p0, Lcom/samsung/musicplus/account/SamsungAccountHelper;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/musicplus/account/SamsungAccountHelper;->mSignInReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 164
    invoke-direct {p0}, Lcom/samsung/musicplus/account/SamsungAccountHelper;->releaseService()V

    .line 165
    return-void
.end method

.method public requestInfo()Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/samsung/musicplus/account/SamsungAccountHelper;->mInfo:Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;

    if-eqz v0, :cond_0

    .line 155
    iget-object v0, p0, Lcom/samsung/musicplus/account/SamsungAccountHelper;->mInfo:Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;

    .line 158
    :goto_0
    return-object v0

    .line 157
    :cond_0
    invoke-direct {p0}, Lcom/samsung/musicplus/account/SamsungAccountHelper;->bindService()V

    .line 158
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setOnAccountInfoListener(Lcom/samsung/musicplus/account/SamsungAccountHelper$OnAccountInfoListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/musicplus/account/SamsungAccountHelper$OnAccountInfoListener;

    .prologue
    .line 150
    iput-object p1, p0, Lcom/samsung/musicplus/account/SamsungAccountHelper;->mInfoListener:Lcom/samsung/musicplus/account/SamsungAccountHelper$OnAccountInfoListener;

    .line 151
    return-void
.end method
