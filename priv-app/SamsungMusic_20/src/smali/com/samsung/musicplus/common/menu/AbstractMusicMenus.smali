.class public abstract Lcom/samsung/musicplus/common/menu/AbstractMusicMenus;
.super Ljava/lang/Object;
.source "AbstractMusicMenus.java"

# interfaces
.implements Lcom/samsung/musicplus/common/menu/MusicMenuDispatcher;
.implements Lcom/samsung/musicplus/common/menu/MusicMenus;


# static fields
.field public static final LOG_TAG:Ljava/lang/String; = "AbstractMusicMenus"


# instance fields
.field private mMusicMenu:Lcom/samsung/musicplus/common/menu/MusicMenus;

.field private mSelecteContextItemdId:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method protected checkValidContextItem(J)Z
    .locals 3
    .param p1, "selectedItemId"    # J

    .prologue
    .line 68
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    .line 69
    const/4 v0, 0x0

    .line 71
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public dispatchContextItemSelected(Lcom/samsung/musicplus/common/menu/MusicMenuHandler;Landroid/view/MenuItem;)Z
    .locals 4
    .param p1, "handler"    # Lcom/samsung/musicplus/common/menu/MusicMenuHandler;
    .param p2, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 92
    const/4 v0, 0x0

    .line 93
    .local v0, "handled":Z
    iget-object v1, p0, Lcom/samsung/musicplus/common/menu/AbstractMusicMenus;->mMusicMenu:Lcom/samsung/musicplus/common/menu/MusicMenus;

    if-eqz v1, :cond_0

    .line 94
    iget-object v1, p0, Lcom/samsung/musicplus/common/menu/AbstractMusicMenus;->mMusicMenu:Lcom/samsung/musicplus/common/menu/MusicMenus;

    iget-wide v2, p0, Lcom/samsung/musicplus/common/menu/AbstractMusicMenus;->mSelecteContextItemdId:J

    invoke-interface {v1, p1, p2, v2, v3}, Lcom/samsung/musicplus/common/menu/MusicMenus;->onContextItemSelected(Lcom/samsung/musicplus/common/menu/MusicMenuHandler;Landroid/view/MenuItem;J)Z

    move-result v0

    .line 97
    :cond_0
    if-nez v0, :cond_1

    .line 98
    iget-wide v2, p0, Lcom/samsung/musicplus/common/menu/AbstractMusicMenus;->mSelecteContextItemdId:J

    invoke-virtual {p0, p1, p2, v2, v3}, Lcom/samsung/musicplus/common/menu/AbstractMusicMenus;->onContextItemSelected(Lcom/samsung/musicplus/common/menu/MusicMenuHandler;Landroid/view/MenuItem;J)Z

    move-result v0

    .line 101
    :cond_1
    return v0
.end method

.method public dispatchCreateContextMenu(Lcom/samsung/musicplus/common/menu/MusicMenuHandler;Landroid/view/MenuInflater;Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
    .locals 8
    .param p1, "handler"    # Lcom/samsung/musicplus/common/menu/MusicMenuHandler;
    .param p2, "inflater"    # Landroid/view/MenuInflater;
    .param p3, "menu"    # Landroid/view/ContextMenu;
    .param p4, "v"    # Landroid/view/View;
    .param p5, "menuInfo"    # Landroid/view/ContextMenu$ContextMenuInfo;

    .prologue
    .line 78
    if-nez p1, :cond_0

    .line 79
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "MusicMenuHandler do not have to be null!!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 82
    :cond_0
    invoke-interface {p1, p5}, Lcom/samsung/musicplus/common/menu/MusicMenuHandler;->getContextSelectedId(Landroid/view/ContextMenu$ContextMenuInfo;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/musicplus/common/menu/AbstractMusicMenus;->mSelecteContextItemdId:J

    .line 84
    iget-wide v6, p0, Lcom/samsung/musicplus/common/menu/AbstractMusicMenus;->mSelecteContextItemdId:J

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v7}, Lcom/samsung/musicplus/common/menu/AbstractMusicMenus;->onCreateContextMenu(Lcom/samsung/musicplus/common/menu/MusicMenuHandler;Landroid/view/MenuInflater;Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;J)V

    .line 85
    iget-object v0, p0, Lcom/samsung/musicplus/common/menu/AbstractMusicMenus;->mMusicMenu:Lcom/samsung/musicplus/common/menu/MusicMenus;

    if-eqz v0, :cond_1

    .line 86
    iget-object v0, p0, Lcom/samsung/musicplus/common/menu/AbstractMusicMenus;->mMusicMenu:Lcom/samsung/musicplus/common/menu/MusicMenus;

    iget-wide v6, p0, Lcom/samsung/musicplus/common/menu/AbstractMusicMenus;->mSelecteContextItemdId:J

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-interface/range {v0 .. v7}, Lcom/samsung/musicplus/common/menu/MusicMenus;->onCreateContextMenu(Lcom/samsung/musicplus/common/menu/MusicMenuHandler;Landroid/view/MenuInflater;Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;J)V

    .line 88
    :cond_1
    return-void
.end method

.method public dispatchCreateOptionsMenu(Landroid/view/MenuInflater;Landroid/view/Menu;)V
    .locals 1
    .param p1, "inflater"    # Landroid/view/MenuInflater;
    .param p2, "menu"    # Landroid/view/Menu;

    .prologue
    .line 106
    invoke-virtual {p0, p1, p2}, Lcom/samsung/musicplus/common/menu/AbstractMusicMenus;->onCreateOptionsMenu(Landroid/view/MenuInflater;Landroid/view/Menu;)V

    .line 107
    iget-object v0, p0, Lcom/samsung/musicplus/common/menu/AbstractMusicMenus;->mMusicMenu:Lcom/samsung/musicplus/common/menu/MusicMenus;

    if-eqz v0, :cond_0

    .line 108
    iget-object v0, p0, Lcom/samsung/musicplus/common/menu/AbstractMusicMenus;->mMusicMenu:Lcom/samsung/musicplus/common/menu/MusicMenus;

    invoke-interface {v0, p1, p2}, Lcom/samsung/musicplus/common/menu/MusicMenus;->onCreateOptionsMenu(Landroid/view/MenuInflater;Landroid/view/Menu;)V

    .line 110
    :cond_0
    return-void
.end method

.method public dispatchOptionItemSelected(Lcom/samsung/musicplus/common/menu/MusicMenuHandler;Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "handler"    # Lcom/samsung/musicplus/common/menu/MusicMenuHandler;
    .param p2, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 122
    const/4 v0, 0x0

    .line 123
    .local v0, "handled":Z
    iget-object v1, p0, Lcom/samsung/musicplus/common/menu/AbstractMusicMenus;->mMusicMenu:Lcom/samsung/musicplus/common/menu/MusicMenus;

    if-eqz v1, :cond_0

    .line 124
    iget-object v1, p0, Lcom/samsung/musicplus/common/menu/AbstractMusicMenus;->mMusicMenu:Lcom/samsung/musicplus/common/menu/MusicMenus;

    invoke-interface {v1, p1, p2}, Lcom/samsung/musicplus/common/menu/MusicMenus;->onOptionsItemSelected(Lcom/samsung/musicplus/common/menu/MusicMenuHandler;Landroid/view/MenuItem;)Z

    move-result v0

    .line 127
    :cond_0
    if-nez v0, :cond_1

    .line 128
    invoke-virtual {p0, p1, p2}, Lcom/samsung/musicplus/common/menu/AbstractMusicMenus;->onOptionsItemSelected(Lcom/samsung/musicplus/common/menu/MusicMenuHandler;Landroid/view/MenuItem;)Z

    move-result v0

    .line 131
    :cond_1
    return v0
.end method

.method public dispatchPrepareOptionsMenu(Lcom/samsung/musicplus/common/menu/MusicMenuHandler;Landroid/view/Menu;)V
    .locals 1
    .param p1, "handler"    # Lcom/samsung/musicplus/common/menu/MusicMenuHandler;
    .param p2, "menu"    # Landroid/view/Menu;

    .prologue
    .line 114
    invoke-virtual {p0, p1, p2}, Lcom/samsung/musicplus/common/menu/AbstractMusicMenus;->onPrepareOptionsMenu(Lcom/samsung/musicplus/common/menu/MusicMenuHandler;Landroid/view/Menu;)V

    .line 115
    iget-object v0, p0, Lcom/samsung/musicplus/common/menu/AbstractMusicMenus;->mMusicMenu:Lcom/samsung/musicplus/common/menu/MusicMenus;

    if-eqz v0, :cond_0

    .line 116
    iget-object v0, p0, Lcom/samsung/musicplus/common/menu/AbstractMusicMenus;->mMusicMenu:Lcom/samsung/musicplus/common/menu/MusicMenus;

    invoke-interface {v0, p1, p2}, Lcom/samsung/musicplus/common/menu/MusicMenus;->onPrepareOptionsMenu(Lcom/samsung/musicplus/common/menu/MusicMenuHandler;Landroid/view/Menu;)V

    .line 118
    :cond_0
    return-void
.end method

.method protected getUriFromPosition(JI)Landroid/net/Uri;
    .locals 5
    .param p1, "audioId"    # J
    .param p3, "position"    # I

    .prologue
    .line 189
    const/4 v0, 0x0

    .line 190
    .local v0, "uri":Landroid/net/Uri;
    const-wide/16 v2, -0x1

    cmp-long v1, p1, v2

    if-lez v1, :cond_0

    .line 191
    sget-object v1, Lcom/samsung/musicplus/provider/MusicContents;->MUSIC_CONTENTS_URI:Landroid/net/Uri;

    invoke-static {v1, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 193
    :cond_0
    return-object v0
.end method

.method protected launchSelector(Landroid/content/Context;IIILjava/lang/String;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "list"    # I
    .param p3, "listMode"    # I
    .param p4, "headerMode"    # I
    .param p5, "key"    # Ljava/lang/String;

    .prologue
    .line 144
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/samsung/musicplus/contents/extra/MusicSelectListActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 145
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "list_type"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 146
    const-string v1, "list_mode"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 147
    const-string v1, "header_mode"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 148
    const-string v1, "keyword"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 149
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 150
    return-void
.end method

.method protected launchSelector(Landroid/content/Context;IIILjava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "list"    # I
    .param p3, "listMode"    # I
    .param p4, "headerMode"    # I
    .param p5, "key"    # Ljava/lang/String;
    .param p6, "additional_key"    # Ljava/lang/String;

    .prologue
    .line 162
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/samsung/musicplus/contents/extra/MusicSelectListActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 163
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "list_type"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 164
    const-string v1, "list_mode"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 165
    const-string v1, "header_mode"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 166
    const-string v1, "keyword"

    invoke-virtual {v0, v1, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 167
    const-string v1, "addtional_keyword"

    invoke-virtual {v0, v1, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 168
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 169
    return-void
.end method

.method protected launchSelector(Landroid/content/Context;II[J)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listMode"    # I
    .param p3, "headerMode"    # I
    .param p4, "id"    # [J

    .prologue
    .line 180
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/samsung/musicplus/contents/extra/MusicSelectListActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 181
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "list_type"

    const v2, 0x10004

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 182
    const-string v1, "list_mode"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 183
    const-string v1, "header_mode"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 184
    const-string v1, "selected_id"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[J)Landroid/content/Intent;

    .line 185
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 186
    return-void
.end method

.method public setOptionalMenu(Lcom/samsung/musicplus/common/menu/MusicMenus;)V
    .locals 0
    .param p1, "menu"    # Lcom/samsung/musicplus/common/menu/MusicMenus;

    .prologue
    .line 59
    iput-object p1, p0, Lcom/samsung/musicplus/common/menu/AbstractMusicMenus;->mMusicMenu:Lcom/samsung/musicplus/common/menu/MusicMenus;

    .line 60
    return-void
.end method

.method protected showToast(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "text"    # Ljava/lang/String;

    .prologue
    .line 203
    const/4 v0, 0x0

    invoke-static {p1, p2, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 204
    return-void
.end method
