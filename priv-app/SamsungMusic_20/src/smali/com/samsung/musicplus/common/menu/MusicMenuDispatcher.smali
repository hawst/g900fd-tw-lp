.class public interface abstract Lcom/samsung/musicplus/common/menu/MusicMenuDispatcher;
.super Ljava/lang/Object;
.source "MusicMenuDispatcher.java"


# virtual methods
.method public abstract dispatchContextItemSelected(Lcom/samsung/musicplus/common/menu/MusicMenuHandler;Landroid/view/MenuItem;)Z
.end method

.method public abstract dispatchCreateContextMenu(Lcom/samsung/musicplus/common/menu/MusicMenuHandler;Landroid/view/MenuInflater;Landroid/view/ContextMenu;Landroid/view/View;Landroid/view/ContextMenu$ContextMenuInfo;)V
.end method

.method public abstract dispatchCreateOptionsMenu(Landroid/view/MenuInflater;Landroid/view/Menu;)V
.end method

.method public abstract dispatchOptionItemSelected(Lcom/samsung/musicplus/common/menu/MusicMenuHandler;Landroid/view/MenuItem;)Z
.end method

.method public abstract dispatchPrepareOptionsMenu(Lcom/samsung/musicplus/common/menu/MusicMenuHandler;Landroid/view/Menu;)V
.end method
