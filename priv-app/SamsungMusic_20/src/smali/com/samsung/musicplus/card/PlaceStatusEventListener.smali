.class public Lcom/samsung/musicplus/card/PlaceStatusEventListener;
.super Landroid/content/BroadcastReceiver;
.source "PlaceStatusEventListener.java"


# static fields
.field private static final ACTION_STATUS_PLACE:Ljava/lang/String; = "com.samsung.android.providers.context.ACTION_STATUS_PLACE"

.field private static final EXTRA_PLACE_CATEGORY:Ljava/lang/String; = "category"

.field private static final EXTRA_PLACE_TYPE:Ljava/lang/String; = "type"

.field private static final PLACE_CATEGORY_CAR:I = 0x3

.field private static final PLACE_CATEGORY_HOME:I = 0x1

.field private static final PLACE_CATEGORY_OTHERS:I = 0x4

.field private static final PLACE_CATEGORY_UNKNOWN:I = 0x0

.field private static final PLACE_CATEGORY_WORK:I = 0x2

.field private static final TAG:Ljava/lang/String; = "PlaceStatus"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 38
    const-string v2, "com.samsung.android.providers.context.ACTION_STATUS_PLACE"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 39
    const-string v2, "PlaceStatus"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onReceive action : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    const-string v2, "category"

    const/4 v3, 0x0

    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 41
    .local v0, "category":I
    const-string v2, "type"

    const/4 v3, -0x1

    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 43
    .local v1, "type":I
    const/4 v2, 0x3

    if-ne v0, v2, :cond_0

    .line 45
    if-nez v1, :cond_1

    .line 58
    .end local v0    # "category":I
    .end local v1    # "type":I
    :cond_0
    :goto_0
    return-void

    .line 49
    .restart local v0    # "category":I
    .restart local v1    # "type":I
    :cond_1
    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    goto :goto_0
.end method
