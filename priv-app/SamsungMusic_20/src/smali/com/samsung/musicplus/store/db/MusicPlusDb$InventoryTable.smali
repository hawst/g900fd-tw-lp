.class public Lcom/samsung/musicplus/store/db/MusicPlusDb$InventoryTable;
.super Ljava/lang/Object;
.source "MusicPlusDb.java"

# interfaces
.implements Lcom/samsung/musicplus/store/db/MusicPlusDb$InventoryColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/store/db/MusicPlusDb;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "InventoryTable"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/store/db/MusicPlusDb$InventoryTable$SongQuality;
    }
.end annotation


# static fields
.field public static final CONSTRAINT:Ljava/lang/String; = "CONSTRAINT unique_song UNIQUE (song_id) ON CONFLICT REPLACE"

.field public static final CONTENT_URI:Landroid/net/Uri;

.field public static final CREATE_COLUMN:Ljava/lang/String; = " (_id integer primary key autoincrement, song_id text, on_my_phone default 1 ,path text, quality integer, size integer, auto_save integer default 0,media_scanner_id integer default -1,CONSTRAINT unique_song UNIQUE (song_id) ON CONFLICT REPLACE);"

.field public static final CREATE_SINGLE_INVENTORY_VIEW:Ljava/lang/String; = "CREATE VIEW inventory_single_view AS SELECT * from (SELECT * from inventory order by quality ASC) group by song_id;"

.field public static final CREATE_VIEW:Ljava/lang/String; = "CREATE VIEW inventory_view AS SELECT a.*, b.state FROM inventory AS a LEFT OUTER JOIN remote_songs AS b ON a.song_id=b.song_id;"

.field public static final SINGLE_INVENTORY_VIEW:Ljava/lang/String; = "inventory_single_view"

.field public static final TABLE_NAME:Ljava/lang/String; = "inventory"

.field public static final VIEW_NAME:Ljava/lang/String; = "inventory_view"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 709
    const-string v0, "content://com.samsung.musicplus.store/inventory"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/store/db/MusicPlusDb$InventoryTable;->CONTENT_URI:Landroid/net/Uri;

    .line 745
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 707
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
