.class public interface abstract Lcom/samsung/musicplus/store/db/MusicPlusDb$InventoryColumns;
.super Ljava/lang/Object;
.source "MusicPlusDb.java"

# interfaces
.implements Lcom/samsung/musicplus/store/db/MusicPlusDb$BaseSongsColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/store/db/MusicPlusDb;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "InventoryColumns"
.end annotation


# static fields
.field public static final ALTERNATIVE_QAULITY:Ljava/lang/String; = "alt_quality"

.field public static final AUTO_SAVE:Ljava/lang/String; = "auto_save"

.field public static final MEDIA_SCANNER_ID:Ljava/lang/String; = "media_scanner_id"

.field public static final ON_MY_PHONE:Ljava/lang/String; = "on_my_phone"

.field public static final PATH:Ljava/lang/String; = "path"

.field public static final QUALITY:Ljava/lang/String; = "quality"

.field public static final SIZE:Ljava/lang/String; = "size"
