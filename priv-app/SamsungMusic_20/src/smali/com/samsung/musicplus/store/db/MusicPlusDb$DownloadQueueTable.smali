.class public Lcom/samsung/musicplus/store/db/MusicPlusDb$DownloadQueueTable;
.super Ljava/lang/Object;
.source "MusicPlusDb.java"

# interfaces
.implements Lcom/samsung/musicplus/store/db/MusicPlusDb$DownloadQueueColumn;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/store/db/MusicPlusDb;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DownloadQueueTable"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/store/db/MusicPlusDb$DownloadQueueTable$Status;,
        Lcom/samsung/musicplus/store/db/MusicPlusDb$DownloadQueueTable$Type;
    }
.end annotation


# static fields
.field public static final CONTENT_URI:Landroid/net/Uri;

.field public static final CREATE_COLUMN:Ljava/lang/String; = " (_id integer primary key autoincrement, song_id text, download_priority integer, download_status integer default 1, download_type integer default 0,partial_path text,retry integer default 0,time_stamp integer, download_options integer default 0);"

.field public static final TABLE_NAME:Ljava/lang/String; = "download_queue"

.field public static final VIEW_NAME:Ljava/lang/String; = "download_queue_view"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 119
    const-string v0, "content://com.samsung.musicplus.store/download_queue"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/store/db/MusicPlusDb$DownloadQueueTable;->CONTENT_URI:Landroid/net/Uri;

    .line 131
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
