.class public Lcom/samsung/musicplus/store/db/MusicPlusDb;
.super Ljava/lang/Object;
.source "MusicPlusDb.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/store/db/MusicPlusDb$BaseSongsColumns;,
        Lcom/samsung/musicplus/store/db/MusicPlusDb$DownloadQueueColumn;,
        Lcom/samsung/musicplus/store/db/MusicPlusDb$DownloadQueueTable;,
        Lcom/samsung/musicplus/store/db/MusicPlusDb$InventoryColumns;,
        Lcom/samsung/musicplus/store/db/MusicPlusDb$InventoryTable;,
        Lcom/samsung/musicplus/store/db/MusicPlusDb$PurchasedSongsColumns;,
        Lcom/samsung/musicplus/store/db/MusicPlusDb$PurchasedTable;,
        Lcom/samsung/musicplus/store/db/MusicPlusDb$RemoteSongsColumns;,
        Lcom/samsung/musicplus/store/db/MusicPlusDb$RemoteSongsTable;,
        Lcom/samsung/musicplus/store/db/MusicPlusDb$TriggerCheckColumns;
    }
.end annotation


# static fields
.field public static final PRIMARY_DATABASE_NAME:Ljava/lang/String; = "musicplus.db"

.field public static final VERSION:I = 0x1


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
