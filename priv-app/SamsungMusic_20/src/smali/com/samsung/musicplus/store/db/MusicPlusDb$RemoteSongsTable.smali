.class public Lcom/samsung/musicplus/store/db/MusicPlusDb$RemoteSongsTable;
.super Ljava/lang/Object;
.source "MusicPlusDb.java"

# interfaces
.implements Lcom/samsung/musicplus/store/db/MusicPlusDb$RemoteSongsColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/store/db/MusicPlusDb;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "RemoteSongsTable"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/store/db/MusicPlusDb$RemoteSongsTable$PlayType;
    }
.end annotation


# static fields
.field public static final CONSTRAINT:Ljava/lang/String; = "CONSTRAINT uk_song UNIQUE (song_id) ON CONFLICT IGNORE"

.field public static final CONTENT_URI:Landroid/net/Uri;

.field public static final CONTENT_URI_NO_TRIGGER:Landroid/net/Uri;

.field public static final CREATE_COLUMN:Ljava/lang/String; = " (_id integer primary key autoincrement, song_id text, is_cloud integer, disc text, cut integer, preview_mp3_url text, coverart_url text, title text, title_key text, song_artist text, album_id integer, album text, album_key text, artist text, artist_id integer, artist_key text, bucket_id integer, bucket_display_name text,track integer, duration integer, release_date integer,genre_name text, state integer,features integer, sync_order integer, date_added integer, is_favorite integer default 0, most_played integer default 0, recently_played integer default 0, _data text, mime_type text default \'audio/mp4\' , trigger integer default 0, price_string text, play_type integer default 0, play_req_order integer default 0, CONSTRAINT uk_song UNIQUE (song_id) ON CONFLICT IGNORE);"

.field public static final CREATE_VIEW:Ljava/lang/String; = "CREATE VIEW remote_songs_view AS SELECT a.*, b.*, c.download_status, c.download_type, c.download_options, c._id AS download_id FROM remote_songs AS a  LEFT OUTER JOIN inventory AS b  ON a.song_id=b.song_id LEFT OUTER JOIN download_queue AS c  ON a.song_id=c.song_id AND c.download_type=1"

.field public static final HARD_CODED_BUCKET_DISPLAY_NAME:Ljava/lang/String; = "Available offline tracks"

.field public static final HARD_CODED_BUCKET_ID:I = 0x0

.field public static final NO_TRIGGER:Ljava/lang/String; = "remote_songs_no_trigger"

.field public static final TABLE_NAME:Ljava/lang/String; = "remote_songs"

.field public static final VIEW_NAME:Ljava/lang/String; = "remote_songs_view"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 484
    const-string v0, "content://com.samsung.musicplus.store/remote_songs"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/store/db/MusicPlusDb$RemoteSongsTable;->CONTENT_URI:Landroid/net/Uri;

    .line 489
    const-string v0, "content://com.samsung.musicplus.store/remote_songs_no_trigger"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/store/db/MusicPlusDb$RemoteSongsTable;->CONTENT_URI_NO_TRIGGER:Landroid/net/Uri;

    .line 564
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 482
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
