.class public interface abstract Lcom/samsung/musicplus/store/db/MusicPlusDb$RemoteSongsColumns;
.super Ljava/lang/Object;
.source "MusicPlusDb.java"

# interfaces
.implements Lcom/samsung/musicplus/store/db/MusicPlusDb$BaseSongsColumns;
.implements Lcom/samsung/musicplus/store/db/MusicPlusDb$TriggerCheckColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/store/db/MusicPlusDb;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "RemoteSongsColumns"
.end annotation


# static fields
.field public static final ALBUM_ARTIST:Ljava/lang/String; = "artist"

.field public static final ALBUM_ID:Ljava/lang/String; = "album_id"

.field public static final ALBUM_KEY:Ljava/lang/String; = "album_key"

.field public static final ALBUM_TITLE:Ljava/lang/String; = "album"

.field public static final ARTIST_ID:Ljava/lang/String; = "artist_id"

.field public static final ARTIST_KEY:Ljava/lang/String; = "artist_key"

.field public static final BUCKET_DISPLAY_NAME:Ljava/lang/String; = "bucket_display_name"

.field public static final BUCKET_ID:Ljava/lang/String; = "bucket_id"

.field public static final COVER_ART_URL:Ljava/lang/String; = "coverart_url"

.field public static final CUT:Ljava/lang/String; = "cut"

.field public static final DATA:Ljava/lang/String; = "_data"

.field public static final DATE_ADDED:Ljava/lang/String; = "date_added"

.field public static final DISC_ID:Ljava/lang/String; = "disc"

.field public static final DURATION:Ljava/lang/String; = "duration"

.field public static final FEATURES:Ljava/lang/String; = "features"

.field public static final GENRE:Ljava/lang/String; = "genre_name"

.field public static final IS_CLOUD_SONG:Ljava/lang/String; = "is_cloud"

.field public static final IS_FAVORITE:Ljava/lang/String; = "is_favorite"

.field public static final MIME_TYPE:Ljava/lang/String; = "mime_type"

.field public static final MOST_PLAYED:Ljava/lang/String; = "most_played"

.field public static final PLAY_REQ_ORDER:Ljava/lang/String; = "play_req_order"

.field public static final PLAY_TYPE:Ljava/lang/String; = "play_type"

.field public static final PREVIEW_URL:Ljava/lang/String; = "preview_mp3_url"

.field public static final PRICE_STRING:Ljava/lang/String; = "price_string"

.field public static final RECENTLY_PLAYED:Ljava/lang/String; = "recently_played"

.field public static final RELEASE_DATE:Ljava/lang/String; = "release_date"

.field public static final SONG_ARTIST:Ljava/lang/String; = "song_artist"

.field public static final SONG_TITLE:Ljava/lang/String; = "title"

.field public static final STATE:Ljava/lang/String; = "state"

.field public static final SYNC_ORDER:Ljava/lang/String; = "sync_order"

.field public static final TITLE_KEY:Ljava/lang/String; = "title_key"

.field public static final TRACK:Ljava/lang/String; = "track"
