.class public Lcom/samsung/musicplus/store/provider/StoreProvider;
.super Landroid/content/ContentProvider;
.source "StoreProvider.java"

# interfaces
.implements Ljava/lang/reflect/InvocationHandler;


# static fields
.field public static final AUTHORITY:Ljava/lang/String; = "com.samsung.musicplus.store"

.field public static final LOG_TAG:Ljava/lang/String;

.field private static MUSICPLUS_PROVIDER_INTERFACE:Ljava/lang/Class; = null
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private static final MUSICPLUS_PROVIDER_INTERFACE_NAME:Ljava/lang/String; = "com.samsung.musicplus.provider.IMusicPlusContentProvider"


# instance fields
.field protected mDatabase:Landroid/database/sqlite/SQLiteDatabase;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 25
    const-class v1, Lcom/samsung/musicplus/store/provider/StoreProvider;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/samsung/musicplus/store/provider/StoreProvider;->LOG_TAG:Ljava/lang/String;

    .line 115
    :try_start_0
    const-string v1, "com.samsung.musicplus.provider.IMusicPlusContentProvider"

    invoke-static {v1}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v1

    sput-object v1, Lcom/samsung/musicplus/store/provider/StoreProvider;->MUSICPLUS_PROVIDER_INTERFACE:Ljava/lang/Class;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 119
    .local v0, "e":Ljava/lang/ClassNotFoundException;
    :goto_0
    return-void

    .line 116
    .end local v0    # "e":Ljava/lang/ClassNotFoundException;
    :catch_0
    move-exception v0

    .line 117
    .restart local v0    # "e":Ljava/lang/ClassNotFoundException;
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 32
    return-void
.end method

.method private static check(Ljava/lang/String;Ljava/lang/reflect/Method;I[Ljava/lang/Object;)Z
    .locals 4
    .param p0, "method"    # Ljava/lang/String;
    .param p1, "invoked_method"    # Ljava/lang/reflect/Method;
    .param p2, "args_size"    # I
    .param p3, "invoked_args"    # [Ljava/lang/Object;

    .prologue
    .line 173
    if-eqz p3, :cond_0

    array-length v0, p3

    .line 174
    .local v0, "invoked_args_size":I
    :goto_0
    sget-object v1, Lcom/samsung/musicplus/store/provider/StoreProvider;->LOG_TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "check. invoked_args - "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", expected_args_size - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    invoke-virtual {p1}, Ljava/lang/reflect/Method;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    return v1

    .line 173
    .end local v0    # "invoked_args_size":I
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 98
    const/4 v0, 0x0

    return v0
.end method

.method public getDatabase()Landroid/database/sqlite/SQLiteDatabase;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/samsung/musicplus/store/provider/StoreProvider;->mDatabase:Landroid/database/sqlite/SQLiteDatabase;

    return-object v0
.end method

.method public getProvider()Landroid/content/ContentProvider;
    .locals 0

    .prologue
    .line 122
    return-object p0
.end method

.method public getProxy()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 130
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Class;

    const/4 v2, 0x0

    sget-object v3, Lcom/samsung/musicplus/store/provider/StoreProvider;->MUSICPLUS_PROVIDER_INTERFACE:Ljava/lang/Class;

    aput-object v3, v1, v2

    invoke-static {v0, v1, p0}, Ljava/lang/reflect/Proxy;->newProxyInstance(Ljava/lang/ClassLoader;[Ljava/lang/Class;Ljava/lang/reflect/InvocationHandler;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 86
    const/4 v0, 0x0

    return-object v0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 92
    const/4 v0, 0x0

    return-object v0
.end method

.method public invoke(Ljava/lang/Object;Ljava/lang/reflect/Method;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 7
    .param p1, "proxy"    # Ljava/lang/Object;
    .param p2, "method"    # Ljava/lang/reflect/Method;
    .param p3, "args"    # [Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/Throwable;
        }
    .end annotation

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 136
    sget-object v0, Lcom/samsung/musicplus/store/provider/StoreProvider;->LOG_TAG:Ljava/lang/String;

    const-string v1, "invoke. interface method is invoked"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 138
    const-string v0, "matchUri"

    invoke-static {v0, p2, v4, p3}, Lcom/samsung/musicplus/store/provider/StoreProvider;->check(Ljava/lang/String;Ljava/lang/reflect/Method;I[Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 139
    aget-object v0, p3, v3

    check-cast v0, Landroid/net/Uri;

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/store/provider/StoreProvider;->matchUri(Landroid/net/Uri;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 161
    :goto_0
    return-object v0

    .line 140
    :cond_0
    const-string v0, "onDatabaseCreated"

    invoke-static {v0, p2, v4, p3}, Lcom/samsung/musicplus/store/provider/StoreProvider;->check(Ljava/lang/String;Ljava/lang/reflect/Method;I[Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 141
    aget-object v0, p3, v3

    check-cast v0, Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/store/provider/StoreProvider;->onDatabaseCreated(Landroid/database/sqlite/SQLiteDatabase;)V

    move-object v0, v2

    .line 142
    goto :goto_0

    .line 143
    :cond_1
    const-string v0, "onCreateDatabase"

    invoke-static {v0, p2, v5, p3}, Lcom/samsung/musicplus/store/provider/StoreProvider;->check(Ljava/lang/String;Ljava/lang/reflect/Method;I[Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 144
    aget-object v0, p3, v3

    check-cast v0, Landroid/database/sqlite/SQLiteDatabase;

    aget-object v1, p3, v4

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/samsung/musicplus/store/provider/StoreProvider;->onCreateDatabase(Landroid/database/sqlite/SQLiteDatabase;I)V

    move-object v0, v2

    .line 145
    goto :goto_0

    .line 146
    :cond_2
    const-string v0, "onPostCreateDatabase"

    invoke-static {v0, p2, v5, p3}, Lcom/samsung/musicplus/store/provider/StoreProvider;->check(Ljava/lang/String;Ljava/lang/reflect/Method;I[Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 147
    aget-object v0, p3, v3

    check-cast v0, Landroid/database/sqlite/SQLiteDatabase;

    aget-object v1, p3, v4

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/samsung/musicplus/store/provider/StoreProvider;->onPostCreateDatabase(Landroid/database/sqlite/SQLiteDatabase;I)V

    move-object v0, v2

    .line 148
    goto :goto_0

    .line 149
    :cond_3
    const-string v0, "onUpgradeDatabase"

    invoke-static {v0, p2, v6, p3}, Lcom/samsung/musicplus/store/provider/StoreProvider;->check(Ljava/lang/String;Ljava/lang/reflect/Method;I[Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 150
    aget-object v0, p3, v3

    check-cast v0, Landroid/database/sqlite/SQLiteDatabase;

    aget-object v1, p3, v4

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    aget-object v1, p3, v5

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v0, v3, v1}, Lcom/samsung/musicplus/store/provider/StoreProvider;->onUpgradeDatabase(Landroid/database/sqlite/SQLiteDatabase;II)V

    move-object v0, v2

    .line 151
    goto :goto_0

    .line 152
    :cond_4
    const-string v0, "onUpgradeDatabase"

    invoke-static {v0, p2, v6, p3}, Lcom/samsung/musicplus/store/provider/StoreProvider;->check(Ljava/lang/String;Ljava/lang/reflect/Method;I[Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 153
    aget-object v0, p3, v3

    check-cast v0, Landroid/database/sqlite/SQLiteDatabase;

    aget-object v1, p3, v4

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v3

    aget-object v1, p3, v5

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v0, v3, v1}, Lcom/samsung/musicplus/store/provider/StoreProvider;->onPostUpgradeDatabase(Landroid/database/sqlite/SQLiteDatabase;II)V

    move-object v0, v2

    .line 154
    goto/16 :goto_0

    .line 155
    :cond_5
    const-string v0, "getDatabase"

    invoke-static {v0, p2, v3, p3}, Lcom/samsung/musicplus/store/provider/StoreProvider;->check(Ljava/lang/String;Ljava/lang/reflect/Method;I[Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 156
    invoke-virtual {p0}, Lcom/samsung/musicplus/store/provider/StoreProvider;->getDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    goto/16 :goto_0

    .line 157
    :cond_6
    const-string v0, "getProvider"

    invoke-static {v0, p2, v3, p3}, Lcom/samsung/musicplus/store/provider/StoreProvider;->check(Ljava/lang/String;Ljava/lang/reflect/Method;I[Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 158
    invoke-virtual {p0}, Lcom/samsung/musicplus/store/provider/StoreProvider;->getProvider()Landroid/content/ContentProvider;

    move-result-object v0

    goto/16 :goto_0

    .line 160
    :cond_7
    sget-object v0, Lcom/samsung/musicplus/store/provider/StoreProvider;->LOG_TAG:Ljava/lang/String;

    const-string v1, "invoke. interface method is not matched"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v2

    .line 161
    goto/16 :goto_0
.end method

.method public matchUri(Landroid/net/Uri;)I
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 35
    const/4 v0, -0x1

    return v0
.end method

.method public onCreate()Z
    .locals 1

    .prologue
    .line 73
    const/4 v0, 0x0

    return v0
.end method

.method public onCreateDatabase(Landroid/database/sqlite/SQLiteDatabase;I)V
    .locals 1
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "databaseVersion"    # I

    .prologue
    .line 45
    const-string v0, "CREATE TABLE IF NOT EXISTS download_queue (_id integer primary key autoincrement, song_id text, download_priority integer, download_status integer default 1, download_type integer default 0,partial_path text,retry integer default 0,time_stamp integer, download_options integer default 0);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 47
    const-string v0, "CREATE TABLE IF NOT EXISTS inventory (_id integer primary key autoincrement, song_id text, on_my_phone default 1 ,path text, quality integer, size integer, auto_save integer default 0,media_scanner_id integer default -1,CONSTRAINT unique_song UNIQUE (song_id) ON CONFLICT REPLACE);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 49
    const-string v0, "CREATE TABLE IF NOT EXISTS remote_songs (_id integer primary key autoincrement, song_id text, is_cloud integer, disc text, cut integer, preview_mp3_url text, coverart_url text, title text, title_key text, song_artist text, album_id integer, album text, album_key text, artist text, artist_id integer, artist_key text, bucket_id integer, bucket_display_name text,track integer, duration integer, release_date integer,genre_name text, state integer,features integer, sync_order integer, date_added integer, is_favorite integer default 0, most_played integer default 0, recently_played integer default 0, _data text, mime_type text default \'audio/mp4\' , trigger integer default 0, price_string text, play_type integer default 0, play_req_order integer default 0, CONSTRAINT uk_song UNIQUE (song_id) ON CONFLICT IGNORE);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 52
    const-string v0, "CREATE VIEW remote_songs_view AS SELECT a.*, b.*, c.download_status, c.download_type, c.download_options, c._id AS download_id FROM remote_songs AS a  LEFT OUTER JOIN inventory AS b  ON a.song_id=b.song_id LEFT OUTER JOIN download_queue AS c  ON a.song_id=c.song_id AND c.download_type=1"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 53
    return-void
.end method

.method public onDatabaseCreated(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 41
    return-void
.end method

.method public onPostCreateDatabase(Landroid/database/sqlite/SQLiteDatabase;I)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "databaseVersion"    # I

    .prologue
    .line 57
    return-void
.end method

.method public onPostUpgradeDatabase(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 68
    return-void
.end method

.method public onUpgradeDatabase(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 63
    return-void
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 80
    const/4 v0, 0x0

    return-object v0
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 104
    const/4 v0, 0x0

    return v0
.end method
