.class public Lcom/samsung/musicplus/store/db/MusicPlusDb$PurchasedTable;
.super Ljava/lang/Object;
.source "MusicPlusDb.java"

# interfaces
.implements Lcom/samsung/musicplus/store/db/MusicPlusDb$PurchasedSongsColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/store/db/MusicPlusDb;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PurchasedTable"
.end annotation


# static fields
.field public static final CONSTRAINT:Ljava/lang/String; = "CONSTRAINT unique_song UNIQUE (song_id) ON CONFLICT IGNORE"

.field public static final CONTENT_URI:Landroid/net/Uri;

.field public static final CREATE_COLUMN:Ljava/lang/String; = " (_id integer primary key autoincrement, song_id text, timestamp integer, CONSTRAINT unique_song UNIQUE (song_id) ON CONFLICT IGNORE);"

.field public static final TABLE_NAME:Ljava/lang/String; = "purchased"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 620
    const-string v0, "content://com.samsung.musicplus.store/purchased"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/store/db/MusicPlusDb$PurchasedTable;->CONTENT_URI:Landroid/net/Uri;

    .line 630
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 618
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
