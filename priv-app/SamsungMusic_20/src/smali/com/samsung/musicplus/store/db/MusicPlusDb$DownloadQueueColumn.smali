.class public interface abstract Lcom/samsung/musicplus/store/db/MusicPlusDb$DownloadQueueColumn;
.super Ljava/lang/Object;
.source "MusicPlusDb.java"

# interfaces
.implements Lcom/samsung/musicplus/store/db/MusicPlusDb$BaseSongsColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/store/db/MusicPlusDb;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "DownloadQueueColumn"
.end annotation


# static fields
.field public static final DOWNLOAD_ID:Ljava/lang/String; = "download_id"

.field public static final DOWNLOAD_TYPE:Ljava/lang/String; = "download_type"

.field public static final OPTIONS:Ljava/lang/String; = "download_options"

.field public static final PARTIAL_PATH:Ljava/lang/String; = "partial_path"

.field public static final PRIORITY:Ljava/lang/String; = "download_priority"

.field public static final RETRY:Ljava/lang/String; = "retry"

.field public static final STATUS:Ljava/lang/String; = "download_status"

.field public static final TIME_STAMP:Ljava/lang/String; = "time_stamp"
