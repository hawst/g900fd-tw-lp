.class public Lcom/samsung/musicplus/MusicBaseActivity;
.super Landroid/app/Activity;
.source "MusicBaseActivity.java"

# interfaces
.implements Landroid/content/ServiceConnection;
.implements Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity$StateChangeListener;
.implements Lcom/samsung/musicplus/util/UiUtils$Defs;
.implements Lcom/samsung/musicplus/widget/fragment/OnSconnectCallback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/MusicBaseActivity$SvoiceWrapper;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = true

.field protected static final FINISH_WITH_BACK_KEY:I = 0x64

.field private static final KEY_IS_PROGRESS:Ljava/lang/String; = "is_progress"

.field public static final NEED_FINISH_ACTION:I = 0x65

.field private static final TAG:Ljava/lang/String; = "MusicUi"


# instance fields
.field private mBezelItemListener:Lcom/samsung/musicplus/library/sdk/BezelManager$OnBezelItemListener;

.field private mBezelManager:Lcom/samsung/musicplus/library/sdk/BezelManager;

.field private mIsMultiWindow:Z

.field private mLoadingProgress:Landroid/app/ProgressDialog;

.field private mMiniPlayer:Lcom/samsung/musicplus/player/MiniPlayer;

.field protected mMiniPlayerRoot:Landroid/view/View;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private mMultiWindowObserver:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

.field protected mPause:Z

.field private mPlayerCancelListener:Landroid/content/DialogInterface$OnCancelListener;

.field private final mPlayerStatusListener:Landroid/content/BroadcastReceiver;

.field private mReservedAction:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mScanListener:Landroid/content/BroadcastReceiver;

.field private mService:Lcom/samsung/musicplus/service/IPlayerService;

.field private mServiceToken:Lcom/samsung/musicplus/util/ServiceUtils$ServiceToken;

.field protected mStop:Z

.field private mSvoice:Lcom/samsung/musicplus/MusicBaseActivity$SvoiceWrapper;

.field private mViewTreeObserver:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 99
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 122
    iput-boolean v0, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mStop:Z

    .line 124
    iput-boolean v0, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mPause:Z

    .line 553
    new-instance v0, Lcom/samsung/musicplus/MusicBaseActivity$4;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/MusicBaseActivity$4;-><init>(Lcom/samsung/musicplus/MusicBaseActivity;)V

    iput-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mPlayerCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    .line 593
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mReservedAction:Ljava/util/ArrayList;

    .line 605
    new-instance v0, Lcom/samsung/musicplus/MusicBaseActivity$5;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/MusicBaseActivity$5;-><init>(Lcom/samsung/musicplus/MusicBaseActivity;)V

    iput-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mPlayerStatusListener:Landroid/content/BroadcastReceiver;

    .line 653
    new-instance v0, Lcom/samsung/musicplus/MusicBaseActivity$6;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/MusicBaseActivity$6;-><init>(Lcom/samsung/musicplus/MusicBaseActivity;)V

    iput-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mScanListener:Landroid/content/BroadcastReceiver;

    .line 957
    return-void
.end method

.method static synthetic access$200(Lcom/samsung/musicplus/MusicBaseActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/MusicBaseActivity;

    .prologue
    .line 99
    invoke-direct {p0}, Lcom/samsung/musicplus/MusicBaseActivity;->isStartReservation()Z

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/samsung/musicplus/MusicBaseActivity;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/MusicBaseActivity;

    .prologue
    .line 99
    iget-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mReservedAction:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/musicplus/MusicBaseActivity;)Lcom/samsung/musicplus/player/MiniPlayer;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/MusicBaseActivity;

    .prologue
    .line 99
    iget-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mMiniPlayer:Lcom/samsung/musicplus/player/MiniPlayer;

    return-object v0
.end method

.method private closePrevDrmPopup()V
    .locals 6

    .prologue
    .line 573
    invoke-virtual {p0}, Lcom/samsung/musicplus/MusicBaseActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    .line 574
    .local v1, "fm":Landroid/app/FragmentManager;
    const-string v4, "drm"

    invoke-virtual {v1, v4}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v3

    .line 575
    .local v3, "prev":Landroid/app/Fragment;
    if-eqz v3, :cond_0

    .line 576
    invoke-virtual {v3}, Landroid/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    const-string v5, "path"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 577
    .local v2, "path":Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getCurrentPath()Ljava/lang/String;

    move-result-object v0

    .line 578
    .local v0, "currentPath":Ljava/lang/String;
    if-eqz v2, :cond_0

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 580
    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/FragmentTransaction;->commit()I

    .line 583
    .end local v0    # "currentPath":Ljava/lang/String;
    .end local v2    # "path":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method private hideProgressDialog()V
    .locals 2

    .prologue
    .line 565
    const-string v0, "MusicUi"

    const-string v1, "hideProgressDialog()"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 566
    iget-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mLoadingProgress:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mLoadingProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 567
    iget-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mLoadingProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 568
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mLoadingProgress:Landroid/app/ProgressDialog;

    .line 570
    :cond_0
    return-void
.end method

.method private initMultiWindowObserver()V
    .locals 1

    .prologue
    .line 270
    new-instance v0, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mMultiWindowObserver:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    .line 271
    iget-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mMultiWindowObserver:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual {v0, p0}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->setStateChangeListener(Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity$StateChangeListener;)Z

    .line 272
    iget-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mMultiWindowObserver:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isMultiWindow()Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mIsMultiWindow:Z

    .line 273
    return-void
.end method

.method private initWindowSizeObserver()V
    .locals 2

    .prologue
    .line 245
    iget-object v1, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mViewTreeObserver:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    if-nez v1, :cond_0

    .line 246
    new-instance v1, Lcom/samsung/musicplus/MusicBaseActivity$2;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/MusicBaseActivity$2;-><init>(Lcom/samsung/musicplus/MusicBaseActivity;)V

    iput-object v1, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mViewTreeObserver:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    .line 256
    invoke-virtual {p0}, Lcom/samsung/musicplus/MusicBaseActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 257
    .local v0, "vto":Landroid/view/ViewTreeObserver;
    if-eqz v0, :cond_0

    .line 258
    iget-object v1, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mViewTreeObserver:Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 261
    .end local v0    # "vto":Landroid/view/ViewTreeObserver;
    :cond_0
    return-void
.end method

.method private isStartReservation()Z
    .locals 2

    .prologue
    .line 600
    iget-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mReservedAction:Ljava/util/ArrayList;

    const-string v1, "com.android.music.metachanged"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private isSupportChangeAudioPath()Z
    .locals 1

    .prologue
    .line 837
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->isDmrPlaying()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/musicplus/MusicBaseActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/musicplus/util/KnoxGateManager;->isKnoxModeOn(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private playMusicByAlbum(Landroid/content/Intent;)V
    .locals 11
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const v10, 0x20002

    .line 888
    const-string v0, "album"

    const-wide/16 v2, -0x1

    invoke-virtual {p1, v0, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    .line 890
    .local v6, "albumId":Ljava/lang/String;
    invoke-static {v10, v6}, Lcom/samsung/musicplus/util/ListUtils;->getMusicListInfo(ILjava/lang/String;)Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;

    move-result-object v9

    .line 891
    .local v9, "trackinfo":Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;
    invoke-virtual {p0}, Lcom/samsung/musicplus/MusicBaseActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, v9, Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;->queryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    iget-object v1, v1, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->uri:Landroid/net/Uri;

    iget-object v2, v9, Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;->queryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    iget-object v2, v2, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->projection:[Ljava/lang/String;

    iget-object v3, v9, Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;->queryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    iget-object v3, v3, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->selection:Ljava/lang/String;

    iget-object v4, v9, Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;->queryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    iget-object v4, v4, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->selectionArgs:[Ljava/lang/String;

    iget-object v5, v9, Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;->queryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    iget-object v5, v5, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->orderBy:Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 894
    .local v7, "cursor":Landroid/database/Cursor;
    if-eqz v7, :cond_0

    .line 895
    new-instance v8, Landroid/content/Intent;

    const-class v0, Lcom/samsung/musicplus/player/PlayerActivity;

    invoke-direct {v8, p0, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 896
    .local v8, "playerIntent":Landroid/content/Intent;
    const/high16 v0, 0x4000000

    invoke-virtual {v8, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 897
    const/high16 v0, 0x10000000

    invoke-virtual {v8, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 898
    const-string v0, "list"

    invoke-static {v7}, Lcom/samsung/musicplus/util/ListUtils;->getSongListForCursor(Landroid/database/Cursor;)[J

    move-result-object v1

    invoke-virtual {v8, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[J)Landroid/content/Intent;

    .line 900
    const-string v0, "list_position"

    const/4 v1, 0x0

    invoke-virtual {v8, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 901
    const-string v0, "list_type"

    invoke-virtual {v8, v0, v10}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 902
    const-string v0, "keyword"

    invoke-virtual {v8, v0, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 903
    invoke-virtual {p0, v8}, Lcom/samsung/musicplus/MusicBaseActivity;->startActivity(Landroid/content/Intent;)V

    .line 904
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 906
    .end local v8    # "playerIntent":Landroid/content/Intent;
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/MusicBaseActivity;->finish()V

    .line 907
    return-void
.end method

.method private prepareViaBtOption(Landroid/view/Menu;)V
    .locals 6
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 813
    const v3, 0x7f0d01e1

    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 814
    .local v1, "bt":Landroid/view/MenuItem;
    const v3, 0x7f0d01e2

    invoke-interface {p1, v3}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 815
    .local v2, "phone":Landroid/view/MenuItem;
    if-eqz v1, :cond_0

    if-eqz v2, :cond_0

    .line 816
    invoke-virtual {p0}, Lcom/samsung/musicplus/MusicBaseActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->getInstance(Landroid/content/Context;)Lcom/samsung/musicplus/library/audio/SecAudioManager;

    move-result-object v0

    .line 817
    .local v0, "audio":Lcom/samsung/musicplus/library/audio/SecAudioManager;
    invoke-direct {p0}, Lcom/samsung/musicplus/MusicBaseActivity;->isSupportChangeAudioPath()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 818
    invoke-interface {v1, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 819
    invoke-interface {v2, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 831
    .end local v0    # "audio":Lcom/samsung/musicplus/library/audio/SecAudioManager;
    :cond_0
    :goto_0
    return-void

    .line 820
    .restart local v0    # "audio":Lcom/samsung/musicplus/library/audio/SecAudioManager;
    :cond_1
    invoke-virtual {v0}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->isWiredHeadsetOn()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 821
    invoke-interface {v1, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 822
    invoke-interface {v2, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 823
    :cond_2
    invoke-virtual {v0}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->isAudioPathBT()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 824
    invoke-interface {v1, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 825
    invoke-interface {v2, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 827
    :cond_3
    invoke-interface {v1, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 828
    invoke-interface {v2, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method private showBuffering()V
    .locals 2

    .prologue
    .line 765
    const v0, 0x7f1000a1

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/MusicBaseActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mPlayerCancelListener:Landroid/content/DialogInterface$OnCancelListener;

    invoke-direct {p0, v0, v1}, Lcom/samsung/musicplus/MusicBaseActivity;->showProgressDialog(Ljava/lang/String;Landroid/content/DialogInterface$OnCancelListener;)V

    .line 766
    return-void
.end method

.method private showProgressDialog(Ljava/lang/String;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 525
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/samsung/musicplus/MusicBaseActivity;->showProgressDialog(Ljava/lang/String;Landroid/content/DialogInterface$OnCancelListener;)V

    .line 526
    return-void
.end method

.method private showProgressDialog(Ljava/lang/String;Landroid/content/DialogInterface$OnCancelListener;)V
    .locals 4
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "listener"    # Landroid/content/DialogInterface$OnCancelListener;

    .prologue
    const/4 v3, 0x0

    .line 529
    invoke-direct {p0}, Lcom/samsung/musicplus/MusicBaseActivity;->hideProgressDialog()V

    .line 530
    const-string v0, "MusicUi"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "showProgressDialog() "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 536
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mLoadingProgress:Landroid/app/ProgressDialog;

    .line 537
    iget-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mLoadingProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 538
    iget-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mLoadingProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v0, p1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 539
    if-nez p2, :cond_1

    .line 540
    iget-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mLoadingProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v3}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 546
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/MusicBaseActivity;->isDestroyed()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/musicplus/MusicBaseActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 551
    :cond_0
    :goto_1
    return-void

    .line 542
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mLoadingProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v0, p2}, Landroid/app/ProgressDialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 543
    iget-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mLoadingProgress:Landroid/app/ProgressDialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    goto :goto_0

    .line 550
    :cond_2
    iget-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mLoadingProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    goto :goto_1
.end method

.method private updateMiniPlayerInfo(Ljava/lang/String;)V
    .locals 1
    .param p1, "action"    # Ljava/lang/String;

    .prologue
    .line 769
    iget-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mMiniPlayer:Lcom/samsung/musicplus/player/MiniPlayer;

    if-eqz v0, :cond_0

    .line 770
    const-string v0, "com.android.music.metachanged"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 771
    iget-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mMiniPlayer:Lcom/samsung/musicplus/player/MiniPlayer;

    invoke-virtual {v0}, Lcom/samsung/musicplus/player/MiniPlayer;->updateTrackAllInfo()V

    .line 785
    :cond_0
    :goto_0
    return-void

    .line 772
    :cond_1
    const-string v0, "com.android.music.playstatechanged"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 773
    iget-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mMiniPlayer:Lcom/samsung/musicplus/player/MiniPlayer;

    invoke-virtual {v0}, Lcom/samsung/musicplus/player/MiniPlayer;->updatePlayState()V

    goto :goto_0

    .line 774
    :cond_2
    const-string v0, "com.samsung.musicplus.action.PREPARE_COMPLETED"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 775
    iget-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mMiniPlayer:Lcom/samsung/musicplus/player/MiniPlayer;

    invoke-virtual {v0}, Lcom/samsung/musicplus/player/MiniPlayer;->updateTotalTime()Z

    goto :goto_0

    .line 776
    :cond_3
    const-string v0, "com.samsung.musicplus.action.META_EDITED"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 777
    iget-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mMiniPlayer:Lcom/samsung/musicplus/player/MiniPlayer;

    invoke-virtual {v0}, Lcom/samsung/musicplus/player/MiniPlayer;->updateSongInfo()V

    goto :goto_0

    .line 778
    :cond_4
    const-string v0, "com.android.music.settingchanged"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 779
    iget-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mMiniPlayer:Lcom/samsung/musicplus/player/MiniPlayer;

    invoke-virtual {v0}, Lcom/samsung/musicplus/player/MiniPlayer;->updateRepeatIcon()V

    .line 780
    iget-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mMiniPlayer:Lcom/samsung/musicplus/player/MiniPlayer;

    invoke-virtual {v0}, Lcom/samsung/musicplus/player/MiniPlayer;->updateShuffleIcon()V

    goto :goto_0

    .line 781
    :cond_5
    const-string v0, "com.samsung.musicplus.action.QUEUE_COMPLETED"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 782
    iget-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mMiniPlayer:Lcom/samsung/musicplus/player/MiniPlayer;

    invoke-virtual {v0}, Lcom/samsung/musicplus/player/MiniPlayer;->cancelFfRew()V

    goto :goto_0
.end method

.method private updateMiniPlayerLayout(Z)V
    .locals 4
    .param p1, "immediately"    # Z

    .prologue
    .line 935
    iget-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mMiniPlayer:Lcom/samsung/musicplus/player/MiniPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mMiniPlayerRoot:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 936
    if-eqz p1, :cond_1

    .line 937
    iget-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mMiniPlayer:Lcom/samsung/musicplus/player/MiniPlayer;

    invoke-virtual {v0}, Lcom/samsung/musicplus/player/MiniPlayer;->adjustMiniPlayerLayout()V

    .line 950
    :cond_0
    :goto_0
    return-void

    .line 939
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mMiniPlayerRoot:Landroid/view/View;

    new-instance v1, Lcom/samsung/musicplus/MusicBaseActivity$7;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/MusicBaseActivity$7;-><init>(Lcom/samsung/musicplus/MusicBaseActivity;)V

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method


# virtual methods
.method public actionSconnect()Z
    .locals 1

    .prologue
    .line 280
    const/4 v0, 0x0

    return v0
.end method

.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 1077
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    .line 1078
    .local v0, "action":I
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    .line 1079
    .local v1, "keyCode":I
    const/4 v2, 0x4

    if-ne v1, v2, :cond_1

    .line 1080
    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    .line 1081
    invoke-virtual {p1}, Landroid/view/KeyEvent;->isLongPress()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->isCanceled()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1082
    :cond_0
    const/4 v2, 0x0

    .line 1086
    :goto_0
    return v2

    :cond_1
    invoke-super {p0, p1}, Landroid/app/Activity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v2

    goto :goto_0
.end method

.method protected getMiniPlayer()Lcom/samsung/musicplus/player/MiniPlayer;
    .locals 1

    .prologue
    .line 489
    iget-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mMiniPlayer:Lcom/samsung/musicplus/player/MiniPlayer;

    return-object v0
.end method

.method protected final getService()Lcom/samsung/musicplus/service/IPlayerService;
    .locals 4

    .prologue
    .line 788
    iget-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mService:Lcom/samsung/musicplus/service/IPlayerService;

    if-nez v0, :cond_0

    .line 789
    sget-object v0, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    iput-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mService:Lcom/samsung/musicplus/service/IPlayerService;

    .line 791
    :cond_0
    const/4 v0, 0x1

    const-string v1, "MusicUi"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " getService "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(ZLjava/lang/String;Ljava/lang/String;)V

    .line 792
    iget-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mService:Lcom/samsung/musicplus/service/IPlayerService;

    return-object v0
.end method

.method public isMultiWindow()Z
    .locals 1

    .prologue
    .line 1072
    iget-boolean v0, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mIsMultiWindow:Z

    return v0
.end method

.method protected final isServiceConnected()Z
    .locals 1

    .prologue
    .line 796
    invoke-virtual {p0}, Lcom/samsung/musicplus/MusicBaseActivity;->getService()Lcom/samsung/musicplus/service/IPlayerService;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected launchQueryBrowser()V
    .locals 3

    .prologue
    .line 910
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/samsung/musicplus/contents/search/QueryBrowserActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 911
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "keyword"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 912
    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/MusicBaseActivity;->startActivity(Landroid/content/Intent;)V

    .line 913
    return-void
.end method

.method protected makeMiniPlayer()V
    .locals 3

    .prologue
    .line 475
    iget-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mMiniPlayer:Lcom/samsung/musicplus/player/MiniPlayer;

    if-nez v0, :cond_0

    .line 476
    iget-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mMiniPlayerRoot:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 477
    new-instance v0, Lcom/samsung/musicplus/player/MiniPlayer;

    invoke-virtual {p0}, Lcom/samsung/musicplus/MusicBaseActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mMiniPlayerRoot:Landroid/view/View;

    invoke-direct {v0, v1, v2}, Lcom/samsung/musicplus/player/MiniPlayer;-><init>(Landroid/content/Context;Landroid/view/View;)V

    iput-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mMiniPlayer:Lcom/samsung/musicplus/player/MiniPlayer;

    .line 478
    iget-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mMiniPlayer:Lcom/samsung/musicplus/player/MiniPlayer;

    new-instance v1, Lcom/samsung/musicplus/MusicBaseActivity$3;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/MusicBaseActivity$3;-><init>(Lcom/samsung/musicplus/MusicBaseActivity;)V

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/player/MiniPlayer;->setAlbumArtClickListener(Landroid/view/View$OnClickListener;)V

    .line 486
    :cond_0
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 1067
    const/16 v0, 0x64

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/MusicBaseActivity;->setResult(I)V

    .line 1068
    invoke-super {p0}, Landroid/app/Activity;->onBackPressed()V

    .line 1069
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 175
    const-string v4, "MusicUi"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " onCreate "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v8, v4, v5}, Lcom/samsung/musicplus/library/iLog;->d(ZLjava/lang/String;Ljava/lang/String;)V

    .line 177
    invoke-static {}, Lcom/samsung/musicplus/util/UiUtils;->isLowMemory()Z

    move-result v2

    .line 178
    .local v2, "lowMem":Z
    if-eqz v2, :cond_0

    .line 179
    invoke-virtual {p0}, Lcom/samsung/musicplus/MusicBaseActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f10010d

    invoke-static {v4, v5, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    .line 181
    invoke-virtual {p0}, Lcom/samsung/musicplus/MusicBaseActivity;->finish()V

    .line 184
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 188
    invoke-static {p0}, Lcom/samsung/musicplus/library/view/WindowManagerCompat;->setWindowStatusBarFlag(Landroid/app/Activity;)V

    .line 189
    const/4 v4, 0x3

    invoke-virtual {p0, v4}, Lcom/samsung/musicplus/MusicBaseActivity;->setVolumeControlStream(I)V

    .line 191
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 192
    .local v0, "f":Landroid/content/IntentFilter;
    const-string v4, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 193
    const-string v4, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v0, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 194
    const-string v4, "android.intent.action.MEDIA_SCANNER_FINISHED"

    invoke-virtual {v0, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 195
    const-string v4, "file"

    invoke-virtual {v0, v4}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 196
    iget-object v4, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mScanListener:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v4, v0}, Lcom/samsung/musicplus/MusicBaseActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 198
    sget-boolean v4, Lcom/samsung/musicplus/util/Version;->LIBRARY_OVER_API_1:Z

    if-eqz v4, :cond_1

    .line 201
    new-instance v4, Lcom/samsung/musicplus/MusicBaseActivity$1;

    invoke-direct {v4, p0}, Lcom/samsung/musicplus/MusicBaseActivity$1;-><init>(Lcom/samsung/musicplus/MusicBaseActivity;)V

    iput-object v4, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mBezelItemListener:Lcom/samsung/musicplus/library/sdk/BezelManager$OnBezelItemListener;

    .line 216
    :cond_1
    invoke-static {p0, p0}, Lcom/samsung/musicplus/util/ServiceUtils;->bindToService(Landroid/content/Context;Landroid/content/ServiceConnection;)Lcom/samsung/musicplus/util/ServiceUtils$ServiceToken;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mServiceToken:Lcom/samsung/musicplus/util/ServiceUtils$ServiceToken;

    .line 218
    sget-boolean v4, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_CHECK_KOR_LGT:Z

    if-eqz v4, :cond_2

    .line 219
    invoke-virtual {p0}, Lcom/samsung/musicplus/MusicBaseActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 220
    .local v1, "intent":Landroid/content/Intent;
    if-eqz v1, :cond_2

    const-string v4, "android.intent.action.VIEW"

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const-string v4, "vnd.android.cursor.dir/album"

    invoke-virtual {v1}, Landroid/content/Intent;->getType()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 222
    invoke-direct {p0, v1}, Lcom/samsung/musicplus/MusicBaseActivity;->playMusicByAlbum(Landroid/content/Intent;)V

    .line 226
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_2
    if-eqz p1, :cond_3

    .line 227
    const-string v4, "is_progress"

    invoke-virtual {p1, v4, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    .line 228
    .local v3, "showProgress":Z
    if-eqz v3, :cond_3

    .line 229
    invoke-direct {p0}, Lcom/samsung/musicplus/MusicBaseActivity;->showBuffering()V

    .line 236
    .end local v3    # "showProgress":Z
    :cond_3
    const-string v4, "MusicUi"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " onCreate End"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v8, v4, v5}, Lcom/samsung/musicplus/library/iLog;->d(ZLjava/lang/String;Ljava/lang/String;)V

    .line 239
    new-instance v4, Lcom/samsung/musicplus/MusicBaseActivity$SvoiceWrapper;

    invoke-virtual {p0}, Lcom/samsung/musicplus/MusicBaseActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, p0, v5}, Lcom/samsung/musicplus/MusicBaseActivity$SvoiceWrapper;-><init>(Lcom/samsung/musicplus/MusicBaseActivity;Landroid/content/Context;)V

    iput-object v4, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mSvoice:Lcom/samsung/musicplus/MusicBaseActivity$SvoiceWrapper;

    .line 241
    invoke-direct {p0}, Lcom/samsung/musicplus/MusicBaseActivity;->initMultiWindowObserver()V

    .line 242
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 4
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 801
    const/4 v0, 0x1

    const-string v1, "MusicUi"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " onCreateOptionsMenu "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(ZLjava/lang/String;Ljava/lang/String;)V

    .line 802
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method protected onDestroy()V
    .locals 4

    .prologue
    .line 406
    const/4 v0, 0x1

    const-string v1, "MusicUi"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " onDestroy"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(ZLjava/lang/String;Ljava/lang/String;)V

    .line 408
    invoke-virtual {p0}, Lcom/samsung/musicplus/MusicBaseActivity;->releaseMiniPlayer()V

    .line 410
    iget-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mScanListener:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/MusicBaseActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 411
    iget-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mServiceToken:Lcom/samsung/musicplus/util/ServiceUtils$ServiceToken;

    invoke-static {v0}, Lcom/samsung/musicplus/util/ServiceUtils;->unbindFromService(Lcom/samsung/musicplus/util/ServiceUtils$ServiceToken;)V

    .line 412
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 413
    return-void
.end method

.method protected onGlobalWindowSizeChanged(II)V
    .locals 0
    .param p1, "height"    # I
    .param p2, "width"    # I

    .prologue
    .line 932
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 5
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v1, 0x1

    .line 439
    const-string v2, "MusicUi"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onKeyDown keyCode : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 440
    sparse-switch p1, :sswitch_data_0

    .line 460
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    :cond_0
    :goto_1
    return v1

    .line 442
    :sswitch_0
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCtrlPressed()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 443
    invoke-virtual {p0}, Lcom/samsung/musicplus/MusicBaseActivity;->finish()V

    goto :goto_1

    .line 448
    :sswitch_1
    :try_start_0
    iget-object v2, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mService:Lcom/samsung/musicplus/service/IPlayerService;

    if-eqz v2, :cond_0

    .line 449
    iget-object v2, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v2}, Lcom/samsung/musicplus/service/IPlayerService;->isPlaying()Z

    move-result v2

    if-nez v2, :cond_1

    .line 450
    iget-object v2, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v2}, Lcom/samsung/musicplus/service/IPlayerService;->play()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 456
    :catch_0
    move-exception v0

    .line 457
    .local v0, "e":Landroid/os/RemoteException;
    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    .line 452
    .end local v0    # "e":Landroid/os/RemoteException;
    :cond_1
    :try_start_1
    iget-object v2, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-interface {v2}, Lcom/samsung/musicplus/service/IPlayerService;->pause()V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 440
    :sswitch_data_0
    .sparse-switch
        0x2d -> :sswitch_0
        0x3e -> :sswitch_1
    .end sparse-switch
.end method

.method public onModeChanged(Z)V
    .locals 1
    .param p1, "isMultiWindow"    # Z

    .prologue
    .line 917
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/MusicBaseActivity;->updateMiniPlayerLayout(Z)V

    .line 918
    iput-boolean p1, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mIsMultiWindow:Z

    .line 919
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 842
    const-string v0, "MusicUi"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " onOptionsItemSelected item : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 843
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 861
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 845
    :sswitch_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/MusicBaseActivity;->finish()V

    goto :goto_0

    .line 848
    :sswitch_1
    invoke-virtual {p0}, Lcom/samsung/musicplus/MusicBaseActivity;->viaBluetooth()V

    goto :goto_0

    .line 851
    :sswitch_2
    invoke-virtual {p0}, Lcom/samsung/musicplus/MusicBaseActivity;->viaDevice()V

    goto :goto_0

    .line 854
    :sswitch_3
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/samsung/musicplus/settings/PlayerSettingActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/MusicBaseActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 857
    :sswitch_4
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/MusicBaseActivity;->moveTaskToBack(Z)Z

    .line 858
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->killMusicProcess()V

    goto :goto_0

    .line 843
    nop

    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_0
        0x7f0d01e1 -> :sswitch_1
        0x7f0d01e2 -> :sswitch_2
        0x7f0d01e6 -> :sswitch_3
        0x7f0d01e8 -> :sswitch_4
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 368
    iput-boolean v3, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mPause:Z

    .line 369
    const-string v0, "MusicUi"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " onPause"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(ZLjava/lang/String;Ljava/lang/String;)V

    .line 372
    iget-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mSvoice:Lcom/samsung/musicplus/MusicBaseActivity$SvoiceWrapper;

    # invokes: Lcom/samsung/musicplus/MusicBaseActivity$SvoiceWrapper;->stop()V
    invoke-static {v0}, Lcom/samsung/musicplus/MusicBaseActivity$SvoiceWrapper;->access$100(Lcom/samsung/musicplus/MusicBaseActivity$SvoiceWrapper;)V

    .line 374
    iget-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mMiniPlayer:Lcom/samsung/musicplus/player/MiniPlayer;

    if-eqz v0, :cond_0

    .line 375
    iget-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mMiniPlayer:Lcom/samsung/musicplus/player/MiniPlayer;

    invoke-virtual {v0}, Lcom/samsung/musicplus/player/MiniPlayer;->onPause()V

    .line 378
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 379
    return-void
.end method

.method protected onPostResume()V
    .locals 0

    .prologue
    .line 265
    invoke-super {p0}, Landroid/app/Activity;->onPostResume()V

    .line 266
    invoke-direct {p0}, Lcom/samsung/musicplus/MusicBaseActivity;->initWindowSizeObserver()V

    .line 267
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)Z
    .locals 4
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 807
    const/4 v0, 0x1

    const-string v1, "MusicUi"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " onPrepareOptionsMenu "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(ZLjava/lang/String;Ljava/lang/String;)V

    .line 808
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/MusicBaseActivity;->prepareViaBtOption(Landroid/view/Menu;)V

    .line 809
    invoke-super {p0, p1}, Landroid/app/Activity;->onPrepareOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method protected onReceiveMediaState(Landroid/content/Intent;)V
    .locals 6
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 670
    const/4 v2, 0x1

    const-string v3, "MusicUi"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " mScanListener intent "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/samsung/musicplus/library/iLog;->d(ZLjava/lang/String;Ljava/lang/String;)V

    .line 671
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 673
    .local v0, "action":Ljava/lang/String;
    const-string v2, "android.intent.action.MEDIA_UNMOUNTED"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "android.intent.action.MEDIA_EJECT"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 685
    :cond_0
    invoke-static {}, Lcom/samsung/musicplus/util/AlbumArtUtils;->clearAlbumArtCache()V

    .line 687
    :cond_1
    const-string v2, "android.intent.action.MEDIA_SCANNER_FINISHED"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 688
    invoke-static {}, Lcom/samsung/musicplus/util/UiUtils;->isLowMemory()Z

    move-result v1

    .line 689
    .local v1, "lowMem":Z
    if-eqz v1, :cond_2

    .line 690
    invoke-virtual {p0}, Lcom/samsung/musicplus/MusicBaseActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f10010c

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 694
    .end local v1    # "lowMem":Z
    :cond_2
    return-void
.end method

.method protected onReceivePlayerState(Landroid/content/Intent;)V
    .locals 9
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 708
    const/4 v5, 0x1

    const-string v6, "MusicUi"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " onReceivePlayerState intent "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v6, v7}, Lcom/samsung/musicplus/library/iLog;->d(ZLjava/lang/String;Ljava/lang/String;)V

    .line 709
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 710
    .local v0, "action":Ljava/lang/String;
    const-string v5, "com.samsung.musicplus.action.DRM_REQUEST"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 711
    iget-boolean v5, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mPause:Z

    if-eqz v5, :cond_0

    .line 762
    :goto_0
    return-void

    .line 716
    :cond_0
    invoke-direct {p0}, Lcom/samsung/musicplus/MusicBaseActivity;->closePrevDrmPopup()V

    .line 718
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    .line 719
    .local v3, "data":Landroid/os/Bundle;
    const-string v5, "command"

    invoke-virtual {v3, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 721
    .local v2, "command":Ljava/lang/String;
    const-string v5, "startRights"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 722
    const v5, 0x7f100062

    invoke-virtual {p0, v5}, Lcom/samsung/musicplus/MusicBaseActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/samsung/musicplus/MusicBaseActivity;->showProgressDialog(Ljava/lang/String;)V

    .line 760
    .end local v2    # "command":Ljava/lang/String;
    .end local v3    # "data":Landroid/os/Bundle;
    :cond_1
    :goto_1
    invoke-direct {p0, v0}, Lcom/samsung/musicplus/MusicBaseActivity;->updateMiniPlayerInfo(Ljava/lang/String;)V

    goto :goto_0

    .line 723
    .restart local v2    # "command":Ljava/lang/String;
    .restart local v3    # "data":Landroid/os/Bundle;
    :cond_2
    const-string v5, "successRights"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 724
    invoke-direct {p0}, Lcom/samsung/musicplus/MusicBaseActivity;->hideProgressDialog()V

    goto :goto_1

    .line 728
    :cond_3
    invoke-direct {p0}, Lcom/samsung/musicplus/MusicBaseActivity;->hideProgressDialog()V

    .line 729
    invoke-static {v3}, Lcom/samsung/musicplus/dialog/DrmPopupFragment;->newInstance(Landroid/os/Bundle;)Lcom/samsung/musicplus/dialog/DrmPopupFragment;

    move-result-object v4

    .line 730
    .local v4, "dialog":Landroid/app/DialogFragment;
    invoke-virtual {p0}, Lcom/samsung/musicplus/MusicBaseActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v5

    const-string v6, "drm"

    invoke-virtual {v4, v5, v6}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_1

    .line 732
    .end local v2    # "command":Ljava/lang/String;
    .end local v3    # "data":Landroid/os/Bundle;
    .end local v4    # "dialog":Landroid/app/DialogFragment;
    :cond_4
    const-string v5, "com.android.music.metachanged"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 733
    invoke-direct {p0}, Lcom/samsung/musicplus/MusicBaseActivity;->closePrevDrmPopup()V

    .line 734
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->isConnectingWfd()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 735
    invoke-direct {p0}, Lcom/samsung/musicplus/MusicBaseActivity;->showBuffering()V

    goto :goto_1

    .line 736
    :cond_5
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->isDmrPlaying()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 738
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->isPreparing()Z

    move-result v5

    if-eqz v5, :cond_6

    .line 739
    invoke-direct {p0}, Lcom/samsung/musicplus/MusicBaseActivity;->showBuffering()V

    goto :goto_1

    .line 741
    :cond_6
    invoke-direct {p0}, Lcom/samsung/musicplus/MusicBaseActivity;->hideProgressDialog()V

    goto :goto_1

    .line 745
    :cond_7
    invoke-direct {p0}, Lcom/samsung/musicplus/MusicBaseActivity;->hideProgressDialog()V

    goto :goto_1

    .line 747
    :cond_8
    const-string v5, "com.samsung.musicplus.action.PLAYER_CHANGED"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 752
    const-string v5, "com.samsung.musicplus.action.PLAYER_BUFFERING"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 753
    const-string v5, "is_buffering"

    const/4 v6, 0x0

    invoke-virtual {p1, v5, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 754
    .local v1, "buffering":Z
    if-eqz v1, :cond_9

    .line 755
    invoke-direct {p0}, Lcom/samsung/musicplus/MusicBaseActivity;->showBuffering()V

    goto :goto_1

    .line 757
    :cond_9
    invoke-direct {p0}, Lcom/samsung/musicplus/MusicBaseActivity;->hideProgressDialog()V

    goto :goto_1
.end method

.method protected onResume()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 339
    iput-boolean v4, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mPause:Z

    .line 344
    const-string v1, "MusicUi"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " onResume"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v5, v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(ZLjava/lang/String;Ljava/lang/String;)V

    .line 345
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 352
    invoke-static {}, Lcom/samsung/musicplus/util/UiUtils;->isLowMemory()Z

    move-result v0

    .line 353
    .local v0, "lowMem":Z
    if-eqz v0, :cond_0

    .line 354
    invoke-virtual {p0}, Lcom/samsung/musicplus/MusicBaseActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f10010c

    invoke-static {v1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 357
    :cond_0
    iget-object v1, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mSvoice:Lcom/samsung/musicplus/MusicBaseActivity$SvoiceWrapper;

    # invokes: Lcom/samsung/musicplus/MusicBaseActivity$SvoiceWrapper;->start()V
    invoke-static {v1}, Lcom/samsung/musicplus/MusicBaseActivity$SvoiceWrapper;->access$000(Lcom/samsung/musicplus/MusicBaseActivity$SvoiceWrapper;)V

    .line 363
    invoke-direct {p0, v5}, Lcom/samsung/musicplus/MusicBaseActivity;->updateMiniPlayerLayout(Z)V

    .line 364
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 417
    const-string v1, "is_progress"

    iget-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mLoadingProgress:Landroid/app/ProgressDialog;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 419
    const-string v0, "MusicUi"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " onSaveInstanceState outState :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 420
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 421
    return-void

    .line 417
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mLoadingProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    goto :goto_0
.end method

.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 7
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 152
    invoke-static {p2}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/samsung/musicplus/service/IPlayerService;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mService:Lcom/samsung/musicplus/service/IPlayerService;

    .line 153
    const/4 v3, 0x1

    const-string v4, "MusicUi"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " onServiceConnected mService = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/musicplus/library/iLog;->d(ZLjava/lang/String;Ljava/lang/String;)V

    .line 154
    invoke-virtual {p0}, Lcom/samsung/musicplus/MusicBaseActivity;->makeMiniPlayer()V

    .line 155
    invoke-virtual {p0}, Lcom/samsung/musicplus/MusicBaseActivity;->updateMiniPlayer()V

    .line 156
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 157
    .local v1, "i":Landroid/content/Intent;
    iget-object v3, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mReservedAction:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 158
    .local v0, "action":Ljava/lang/String;
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/samsung/musicplus/MusicBaseActivity;->onReceivePlayerState(Landroid/content/Intent;)V

    goto :goto_0

    .line 160
    .end local v0    # "action":Ljava/lang/String;
    :cond_0
    iget-object v3, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mReservedAction:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 161
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 4
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 165
    const/4 v0, 0x1

    const-string v1, "MusicUi"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " onServiceDisconnected "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(ZLjava/lang/String;Ljava/lang/String;)V

    .line 166
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mService:Lcom/samsung/musicplus/service/IPlayerService;

    .line 167
    invoke-virtual {p0}, Lcom/samsung/musicplus/MusicBaseActivity;->finish()V

    .line 168
    return-void
.end method

.method public onSizeChanged(Landroid/graphics/Rect;)V
    .locals 1
    .param p1, "rect"    # Landroid/graphics/Rect;

    .prologue
    .line 923
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/MusicBaseActivity;->updateMiniPlayerLayout(Z)V

    .line 924
    return-void
.end method

.method protected onStart()V
    .locals 5

    .prologue
    .line 285
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mStop:Z

    .line 289
    const/4 v1, 0x1

    const-string v2, "MusicUi"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " onStart"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v2, v3}, Lcom/samsung/musicplus/library/iLog;->d(ZLjava/lang/String;Ljava/lang/String;)V

    .line 291
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 292
    .local v0, "f":Landroid/content/IntentFilter;
    const-string v1, "com.android.music.metachanged"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 293
    const-string v1, "com.samsung.musicplus.action.PREPARE_COMPLETED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 294
    const-string v1, "com.android.music.playstatechanged"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 295
    const-string v1, "com.samsung.musicplus.action.DRM_REQUEST"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 296
    const-string v1, "com.samsung.musicplus.action.PLAYER_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 297
    const-string v1, "com.samsung.musicplus.action.QUEUE_COMPLETED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 298
    const-string v1, "com.samsung.musicplus.action.PLAYER_BUFFERING"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 299
    const-string v1, "com.samsung.musicplus.action.META_EDITED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 300
    const-string v1, "com.samsung.musicplus.favouritechanged"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 301
    const-string v1, "com.android.music.settingchanged"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 302
    sget-boolean v1, Lcom/samsung/musicplus/util/Version;->LIBRARY_API_1:Z

    if-eqz v1, :cond_0

    .line 307
    const-string v1, "com.samsung.cover.OPEN"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 309
    :cond_0
    iget-object v1, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mPlayerStatusListener:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/samsung/musicplus/MusicBaseActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 311
    const v1, 0x7f0d00f2

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/MusicBaseActivity;->setMiniPlayerLayout(I)V

    .line 313
    sget-boolean v1, Lcom/samsung/musicplus/util/Version;->LIBRARY_OVER_API_1:Z

    if-eqz v1, :cond_1

    .line 316
    const-string v1, "Bezel"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onCreate bezel create "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 317
    new-instance v1, Lcom/samsung/musicplus/library/sdk/BezelManager;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/library/sdk/BezelManager;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mBezelManager:Lcom/samsung/musicplus/library/sdk/BezelManager;

    .line 320
    :cond_1
    const-string v1, "Bezel"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onStart register listener "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 321
    iget-object v1, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mBezelManager:Lcom/samsung/musicplus/library/sdk/BezelManager;

    if-eqz v1, :cond_2

    .line 322
    iget-object v1, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mBezelManager:Lcom/samsung/musicplus/library/sdk/BezelManager;

    iget-object v2, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mBezelItemListener:Lcom/samsung/musicplus/library/sdk/BezelManager$OnBezelItemListener;

    invoke-virtual {v1, v2}, Lcom/samsung/musicplus/library/sdk/BezelManager;->register(Lcom/samsung/musicplus/library/sdk/BezelManager$OnBezelItemListener;)V

    .line 324
    :cond_2
    const-string v1, "Bezel"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onStart register listener end "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 326
    iget-object v1, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mMiniPlayer:Lcom/samsung/musicplus/player/MiniPlayer;

    if-eqz v1, :cond_3

    .line 327
    iget-object v1, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mMiniPlayer:Lcom/samsung/musicplus/player/MiniPlayer;

    invoke-virtual {v1}, Lcom/samsung/musicplus/player/MiniPlayer;->onStart()V

    .line 329
    :cond_3
    invoke-super {p0}, Landroid/app/Activity;->onStart()V

    .line 330
    const-string v1, "MusicUi"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onStart end "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 335
    return-void
.end method

.method protected onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 383
    iput-boolean v3, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mStop:Z

    .line 384
    iget-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mReservedAction:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 385
    const-string v0, "MusicUi"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " onStop"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(ZLjava/lang/String;Ljava/lang/String;)V

    .line 387
    :try_start_0
    iget-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mPlayerStatusListener:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/MusicBaseActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 393
    :goto_0
    const-string v0, "Bezel"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onStop unregister listener "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 394
    iget-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mBezelManager:Lcom/samsung/musicplus/library/sdk/BezelManager;

    if-eqz v0, :cond_0

    .line 395
    iget-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mBezelManager:Lcom/samsung/musicplus/library/sdk/BezelManager;

    invoke-virtual {v0}, Lcom/samsung/musicplus/library/sdk/BezelManager;->unregister()V

    .line 398
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mMiniPlayer:Lcom/samsung/musicplus/player/MiniPlayer;

    if-eqz v0, :cond_1

    .line 399
    iget-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mMiniPlayer:Lcom/samsung/musicplus/player/MiniPlayer;

    invoke-virtual {v0}, Lcom/samsung/musicplus/player/MiniPlayer;->onStop()V

    .line 401
    :cond_1
    invoke-super {p0}, Landroid/app/Activity;->onStop()V

    .line 402
    return-void

    .line 388
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public onTrimMemory(I)V
    .locals 4
    .param p1, "level"    # I

    .prologue
    .line 425
    const/4 v0, 0x1

    const-string v1, "MusicUi"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " onTrimMemory level :"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(ZLjava/lang/String;Ljava/lang/String;)V

    .line 426
    const/16 v0, 0x28

    if-lt p1, v0, :cond_0

    .line 431
    invoke-static {}, Lcom/samsung/musicplus/util/AlbumArtUtils;->clearCaches()V

    .line 432
    invoke-static {}, Lcom/samsung/musicplus/bitmapcache/BitmapCache;->clearCaches()V

    .line 434
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onTrimMemory(I)V

    .line 435
    return-void
.end method

.method public onZoneChanged(I)V
    .locals 1
    .param p1, "type"    # I

    .prologue
    .line 928
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/MusicBaseActivity;->updateMiniPlayerLayout(Z)V

    .line 929
    return-void
.end method

.method protected releaseMiniPlayer()V
    .locals 1

    .prologue
    .line 497
    iget-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mMiniPlayer:Lcom/samsung/musicplus/player/MiniPlayer;

    if-eqz v0, :cond_0

    .line 498
    iget-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mMiniPlayer:Lcom/samsung/musicplus/player/MiniPlayer;

    invoke-virtual {v0}, Lcom/samsung/musicplus/player/MiniPlayer;->onStop()V

    .line 499
    iget-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mMiniPlayer:Lcom/samsung/musicplus/player/MiniPlayer;

    invoke-virtual {v0}, Lcom/samsung/musicplus/player/MiniPlayer;->onDestroy()V

    .line 500
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mMiniPlayer:Lcom/samsung/musicplus/player/MiniPlayer;

    .line 502
    :cond_0
    return-void
.end method

.method protected setMiniPlayerLayout(I)V
    .locals 1
    .param p1, "resId"    # I

    .prologue
    .line 468
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/MusicBaseActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mMiniPlayerRoot:Landroid/view/View;

    .line 469
    return-void
.end method

.method public setMiniPlayerVisible(Z)V
    .locals 2
    .param p1, "visible"    # Z

    .prologue
    .line 515
    if-eqz p1, :cond_0

    .line 516
    iget-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mMiniPlayerRoot:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 518
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mMiniPlayer:Lcom/samsung/musicplus/player/MiniPlayer;

    if-eqz v0, :cond_1

    .line 519
    iget-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mMiniPlayer:Lcom/samsung/musicplus/player/MiniPlayer;

    invoke-virtual {v0}, Lcom/samsung/musicplus/player/MiniPlayer;->updateTrackAllInfo()V

    .line 521
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/musicplus/MusicBaseActivity;->updateMiniPlayer()V

    .line 522
    return-void
.end method

.method protected updateMiniPlayer()V
    .locals 1

    .prologue
    .line 505
    iget-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mMiniPlayer:Lcom/samsung/musicplus/player/MiniPlayer;

    if-eqz v0, :cond_0

    .line 506
    iget-boolean v0, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mStop:Z

    if-eqz v0, :cond_1

    .line 507
    iget-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mMiniPlayer:Lcom/samsung/musicplus/player/MiniPlayer;

    invoke-virtual {v0}, Lcom/samsung/musicplus/player/MiniPlayer;->onStop()V

    .line 512
    :cond_0
    :goto_0
    return-void

    .line 509
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/MusicBaseActivity;->mMiniPlayer:Lcom/samsung/musicplus/player/MiniPlayer;

    invoke-virtual {v0}, Lcom/samsung/musicplus/player/MiniPlayer;->onStart()V

    goto :goto_0
.end method

.method protected viaBluetooth()V
    .locals 3

    .prologue
    .line 877
    invoke-virtual {p0}, Lcom/samsung/musicplus/MusicBaseActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 878
    .local v1, "context":Landroid/content/Context;
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->isBtConnected()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 879
    invoke-static {v1}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->getInstance(Landroid/content/Context;)Lcom/samsung/musicplus/library/audio/SecAudioManager;

    move-result-object v0

    .line 880
    .local v0, "audioManager":Lcom/samsung/musicplus/library/audio/SecAudioManager;
    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->setSoundPathToBT(Landroid/content/Context;)V

    .line 884
    .end local v0    # "audioManager":Lcom/samsung/musicplus/library/audio/SecAudioManager;
    :goto_0
    return-void

    .line 882
    :cond_0
    invoke-static {p0}, Lcom/samsung/musicplus/util/UiUtils;->startBluetoothDevicePicker(Landroid/app/Activity;)V

    goto :goto_0
.end method

.method protected viaDevice()V
    .locals 2

    .prologue
    .line 868
    invoke-virtual {p0}, Lcom/samsung/musicplus/MusicBaseActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 869
    .local v1, "context":Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->getInstance(Landroid/content/Context;)Lcom/samsung/musicplus/library/audio/SecAudioManager;

    move-result-object v0

    .line 870
    .local v0, "audioManager":Lcom/samsung/musicplus/library/audio/SecAudioManager;
    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->setSoundPathToDevice(Landroid/content/Context;)V

    .line 871
    return-void
.end method
