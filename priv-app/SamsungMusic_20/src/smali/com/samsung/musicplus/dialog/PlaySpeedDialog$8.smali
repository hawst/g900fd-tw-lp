.class Lcom/samsung/musicplus/dialog/PlaySpeedDialog$8;
.super Landroid/os/Handler;
.source "PlaySpeedDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/dialog/PlaySpeedDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/dialog/PlaySpeedDialog;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/dialog/PlaySpeedDialog;)V
    .locals 0

    .prologue
    .line 231
    iput-object p1, p0, Lcom/samsung/musicplus/dialog/PlaySpeedDialog$8;->this$0:Lcom/samsung/musicplus/dialog/PlaySpeedDialog;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const-wide/16 v2, 0x1f4

    .line 234
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 246
    :goto_0
    return-void

    .line 236
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/PlaySpeedDialog$8;->this$0:Lcom/samsung/musicplus/dialog/PlaySpeedDialog;

    # invokes: Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->upPlaySpeed()V
    invoke-static {v0}, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->access$700(Lcom/samsung/musicplus/dialog/PlaySpeedDialog;)V

    .line 237
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/PlaySpeedDialog$8;->this$0:Lcom/samsung/musicplus/dialog/PlaySpeedDialog;

    # getter for: Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->mPlaySpeedEventHandelr:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->access$000(Lcom/samsung/musicplus/dialog/PlaySpeedDialog;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 241
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/PlaySpeedDialog$8;->this$0:Lcom/samsung/musicplus/dialog/PlaySpeedDialog;

    # invokes: Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->downPlaySpeed()V
    invoke-static {v0}, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->access$800(Lcom/samsung/musicplus/dialog/PlaySpeedDialog;)V

    .line 242
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/PlaySpeedDialog$8;->this$0:Lcom/samsung/musicplus/dialog/PlaySpeedDialog;

    # getter for: Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->mPlaySpeedEventHandelr:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->access$000(Lcom/samsung/musicplus/dialog/PlaySpeedDialog;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 234
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
