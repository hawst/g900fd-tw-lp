.class Lcom/samsung/musicplus/dialog/PlaySpeedDialog$6;
.super Ljava/lang/Object;
.source "PlaySpeedDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/dialog/PlaySpeedDialog;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/dialog/PlaySpeedDialog;)V
    .locals 0

    .prologue
    .line 166
    iput-object p1, p0, Lcom/samsung/musicplus/dialog/PlaySpeedDialog$6;->this$0:Lcom/samsung/musicplus/dialog/PlaySpeedDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 169
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/PlaySpeedDialog$6;->this$0:Lcom/samsung/musicplus/dialog/PlaySpeedDialog;

    # getter for: Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->mPause:Z
    invoke-static {v0}, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->access$300(Lcom/samsung/musicplus/dialog/PlaySpeedDialog;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 178
    :goto_0
    return-void

    .line 173
    :cond_0
    const-string v0, "play_speed"

    iget-object v1, p0, Lcom/samsung/musicplus/dialog/PlaySpeedDialog$6;->this$0:Lcom/samsung/musicplus/dialog/PlaySpeedDialog;

    iget-object v2, p0, Lcom/samsung/musicplus/dialog/PlaySpeedDialog$6;->this$0:Lcom/samsung/musicplus/dialog/PlaySpeedDialog;

    # getter for: Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->mProgress:I
    invoke-static {v2}, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->access$100(Lcom/samsung/musicplus/dialog/PlaySpeedDialog;)I

    move-result v2

    # invokes: Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->getPlaySpeedValueFromIndex(I)F
    invoke-static {v1, v2}, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->access$500(Lcom/samsung/musicplus/dialog/PlaySpeedDialog;I)F

    move-result v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/util/ServiceUtils;->savePreferencesFloat(Ljava/lang/String;F)V

    .line 175
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/PlaySpeedDialog$6;->this$0:Lcom/samsung/musicplus/dialog/PlaySpeedDialog;

    invoke-virtual {v0}, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, -0x1

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method
