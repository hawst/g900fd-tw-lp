.class Lcom/samsung/musicplus/dialog/AdaptSoundDialog$2;
.super Ljava/lang/Object;
.source "AdaptSoundDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/dialog/AdaptSoundDialog;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/dialog/AdaptSoundDialog;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/dialog/AdaptSoundDialog;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 70
    iput-object p1, p0, Lcom/samsung/musicplus/dialog/AdaptSoundDialog$2;->this$0:Lcom/samsung/musicplus/dialog/AdaptSoundDialog;

    iput-object p2, p0, Lcom/samsung/musicplus/dialog/AdaptSoundDialog$2;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 74
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 75
    .local v0, "currentValue":I
    if-nez v0, :cond_3

    .line 76
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/AdaptSoundDialog$2;->this$0:Lcom/samsung/musicplus/dialog/AdaptSoundDialog;

    # invokes: Lcom/samsung/musicplus/dialog/AdaptSoundDialog;->isEnableAdaptSoundPath()Z
    invoke-static {v2}, Lcom/samsung/musicplus/dialog/AdaptSoundDialog;->access$200(Lcom/samsung/musicplus/dialog/AdaptSoundDialog;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 77
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/AdaptSoundDialog$2;->val$context:Landroid/content/Context;

    invoke-static {v2}, Lcom/samsung/musicplus/library/audio/AdaptSound;->isAlreadyDiagnosed(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 78
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.sec.hearingadjust.launch"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 79
    .local v1, "intent":Landroid/content/Intent;
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/AdaptSoundDialog$2;->val$context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 80
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/AdaptSoundDialog$2;->this$0:Lcom/samsung/musicplus/dialog/AdaptSoundDialog;

    invoke-virtual {v2, v1}, Lcom/samsung/musicplus/dialog/AdaptSoundDialog;->startActivity(Landroid/content/Intent;)V

    .line 81
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/AdaptSoundDialog$2;->this$0:Lcom/samsung/musicplus/dialog/AdaptSoundDialog;

    # getter for: Lcom/samsung/musicplus/dialog/AdaptSoundDialog;->mAlertDialog:Landroid/app/AlertDialog;
    invoke-static {v2}, Lcom/samsung/musicplus/dialog/AdaptSoundDialog;->access$300(Lcom/samsung/musicplus/dialog/AdaptSoundDialog;)Landroid/app/AlertDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog;->dismiss()V

    .line 97
    .end local v1    # "intent":Landroid/content/Intent;
    .end local p1    # "dialog":Landroid/content/DialogInterface;
    :goto_0
    return-void

    .line 83
    .restart local v1    # "intent":Landroid/content/Intent;
    .restart local p1    # "dialog":Landroid/content/DialogInterface;
    :cond_0
    # getter for: Lcom/samsung/musicplus/dialog/AdaptSoundDialog;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/dialog/AdaptSoundDialog;->access$000()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Not found hearingadjust application"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 84
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/AdaptSoundDialog$2;->this$0:Lcom/samsung/musicplus/dialog/AdaptSoundDialog;

    # invokes: Lcom/samsung/musicplus/dialog/AdaptSoundDialog;->handlingActivityNotFound()V
    invoke-static {v2}, Lcom/samsung/musicplus/dialog/AdaptSoundDialog;->access$100(Lcom/samsung/musicplus/dialog/AdaptSoundDialog;)V

    goto :goto_0

    .line 87
    .end local v1    # "intent":Landroid/content/Intent;
    :cond_1
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/AdaptSoundDialog$2;->this$0:Lcom/samsung/musicplus/dialog/AdaptSoundDialog;

    # invokes: Lcom/samsung/musicplus/dialog/AdaptSoundDialog;->setAdaptSound(Z)V
    invoke-static {v2, v3}, Lcom/samsung/musicplus/dialog/AdaptSoundDialog;->access$400(Lcom/samsung/musicplus/dialog/AdaptSoundDialog;Z)V

    goto :goto_0

    .line 90
    :cond_2
    check-cast p1, Landroid/app/AlertDialog;

    .end local p1    # "dialog":Landroid/content/DialogInterface;
    invoke-virtual {p1}, Landroid/app/AlertDialog;->getListView()Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2, v3, v3}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 91
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/AdaptSoundDialog$2;->this$0:Lcom/samsung/musicplus/dialog/AdaptSoundDialog;

    # invokes: Lcom/samsung/musicplus/dialog/AdaptSoundDialog;->setAdaptSound(Z)V
    invoke-static {v2, v4}, Lcom/samsung/musicplus/dialog/AdaptSoundDialog;->access$400(Lcom/samsung/musicplus/dialog/AdaptSoundDialog;Z)V

    .line 92
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/AdaptSoundDialog$2;->this$0:Lcom/samsung/musicplus/dialog/AdaptSoundDialog;

    iget-object v3, p0, Lcom/samsung/musicplus/dialog/AdaptSoundDialog$2;->val$context:Landroid/content/Context;

    # invokes: Lcom/samsung/musicplus/dialog/AdaptSoundDialog;->warningAdaptSound(Landroid/content/Context;)V
    invoke-static {v2, v3}, Lcom/samsung/musicplus/dialog/AdaptSoundDialog;->access$500(Lcom/samsung/musicplus/dialog/AdaptSoundDialog;Landroid/content/Context;)V

    goto :goto_0

    .line 95
    .restart local p1    # "dialog":Landroid/content/DialogInterface;
    :cond_3
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/AdaptSoundDialog$2;->this$0:Lcom/samsung/musicplus/dialog/AdaptSoundDialog;

    # invokes: Lcom/samsung/musicplus/dialog/AdaptSoundDialog;->setAdaptSound(Z)V
    invoke-static {v2, v4}, Lcom/samsung/musicplus/dialog/AdaptSoundDialog;->access$400(Lcom/samsung/musicplus/dialog/AdaptSoundDialog;Z)V

    goto :goto_0
.end method
