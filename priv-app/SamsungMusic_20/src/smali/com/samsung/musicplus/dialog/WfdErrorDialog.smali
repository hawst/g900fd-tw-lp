.class public Lcom/samsung/musicplus/dialog/WfdErrorDialog;
.super Landroid/app/DialogFragment;
.source "WfdErrorDialog.java"

# interfaces
.implements Lcom/samsung/musicplus/settings/PlayerSettingPreference;


# static fields
.field private static final CLASSNAME:Ljava/lang/String;


# instance fields
.field private mCheckBox:Landroid/widget/CheckBox;

.field private mExceptionalCase:I

.field private mIsChecked:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/samsung/musicplus/dialog/WfdErrorDialog;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/dialog/WfdErrorDialog;->CLASSNAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 34
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/dialog/WfdErrorDialog;->mIsChecked:Z

    .line 38
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/musicplus/dialog/WfdErrorDialog;->mExceptionalCase:I

    .line 41
    return-void
.end method

.method public constructor <init>(I)V
    .locals 1
    .param p1, "exceptionalCase"    # I

    .prologue
    .line 43
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 34
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/dialog/WfdErrorDialog;->mIsChecked:Z

    .line 38
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/musicplus/dialog/WfdErrorDialog;->mExceptionalCase:I

    .line 44
    iput p1, p0, Lcom/samsung/musicplus/dialog/WfdErrorDialog;->mExceptionalCase:I

    .line 45
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/dialog/WfdErrorDialog;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/WfdErrorDialog;

    .prologue
    .line 30
    iget-boolean v0, p0, Lcom/samsung/musicplus/dialog/WfdErrorDialog;->mIsChecked:Z

    return v0
.end method

.method static synthetic access$002(Lcom/samsung/musicplus/dialog/WfdErrorDialog;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/WfdErrorDialog;
    .param p1, "x1"    # Z

    .prologue
    .line 30
    iput-boolean p1, p0, Lcom/samsung/musicplus/dialog/WfdErrorDialog;->mIsChecked:Z

    return p1
.end method

.method static synthetic access$100(Lcom/samsung/musicplus/dialog/WfdErrorDialog;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/WfdErrorDialog;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/samsung/musicplus/dialog/WfdErrorDialog;->disableAllTogether()Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/samsung/musicplus/dialog/WfdErrorDialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/WfdErrorDialog;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/samsung/musicplus/dialog/WfdErrorDialog;->showChangeDeviceDialog()V

    return-void
.end method

.method static synthetic access$300(Lcom/samsung/musicplus/dialog/WfdErrorDialog;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/WfdErrorDialog;

    .prologue
    .line 30
    iget v0, p0, Lcom/samsung/musicplus/dialog/WfdErrorDialog;->mExceptionalCase:I

    return v0
.end method

.method private disableAllTogether()Z
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 157
    const/16 v0, 0x1f4

    .line 158
    .local v0, "WIFI_TURN_ON_DELAY":I
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/WfdErrorDialog;->getActivity()Landroid/app/Activity;

    move-result-object v5

    const-string v6, "wifi"

    invoke-virtual {v5, v6}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/wifi/WifiManager;

    .line 159
    .local v2, "wm":Landroid/net/wifi/WifiManager;
    if-eqz v2, :cond_0

    .line 160
    invoke-virtual {v2, v4}, Landroid/net/wifi/WifiManager;->setWifiIBSSEnabled(Z)Z

    .line 162
    const-wide/16 v6, 0x1f4

    :try_start_0
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 167
    invoke-virtual {v2, v3}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    .line 171
    :goto_0
    return v3

    .line 163
    :catch_0
    move-exception v1

    .line 164
    .local v1, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V

    move v3, v4

    .line 165
    goto :goto_0

    .end local v1    # "e":Ljava/lang/InterruptedException;
    :cond_0
    move v3, v4

    .line 171
    goto :goto_0
.end method

.method public static getWfdExceptionalCaseKey(I)Ljava/lang/String;
    .locals 4
    .param p0, "exceptionalCase"    # I

    .prologue
    .line 218
    const/4 v0, 0x0

    .line 219
    .local v0, "key":Ljava/lang/String;
    packed-switch p0, :pswitch_data_0

    .line 242
    :pswitch_0
    sget-object v1, Lcom/samsung/musicplus/dialog/WfdErrorDialog;->CLASSNAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "WFD dialog get key, unsupport exceptional case - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 246
    :goto_0
    return-object v0

    .line 221
    :pswitch_1
    const-string v0, "wfd_wifi_hotspot"

    .line 222
    goto :goto_0

    .line 224
    :pswitch_2
    const-string v0, "wfd_wifi_direct"

    .line 225
    goto :goto_0

    .line 227
    :pswitch_3
    const-string v0, "wfd_side_sync"

    .line 228
    goto :goto_0

    .line 230
    :pswitch_4
    const-string v0, "wfd_power_saving"

    .line 231
    goto :goto_0

    .line 233
    :pswitch_5
    const-string v0, "wfd_limited_play"

    .line 234
    goto :goto_0

    .line 236
    :pswitch_6
    const-string v0, "wfd_group_play"

    .line 237
    goto :goto_0

    .line 239
    :pswitch_7
    const-string v0, "wfd_all_together"

    .line 240
    goto :goto_0

    .line 219
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method private showChangeDeviceDialog()V
    .locals 5

    .prologue
    .line 192
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/WfdErrorDialog;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 193
    .local v1, "ft":Landroid/app/FragmentTransaction;
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/WfdErrorDialog;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    const-string v4, "device_dialog"

    invoke-virtual {v3, v4}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v2

    .line 194
    .local v2, "prev":Landroid/app/Fragment;
    if-eqz v2, :cond_0

    .line 195
    invoke-virtual {v1, v2}, Landroid/app/FragmentTransaction;->remove(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 200
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/WfdErrorDialog;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->isResumed()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 201
    sget-boolean v3, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_SUPPORT_WIFIDISPLAY_OLD:Z

    if-eqz v3, :cond_2

    .line 202
    new-instance v0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;

    invoke-direct {v0}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;-><init>()V

    .line 203
    .local v0, "deviceDialog":Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;
    const-string v3, "device_dialog"

    invoke-virtual {v0, v1, v3}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->show(Landroid/app/FragmentTransaction;Ljava/lang/String;)I

    .line 209
    .end local v0    # "deviceDialog":Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;
    :cond_1
    :goto_0
    return-void

    .line 205
    :cond_2
    new-instance v0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;

    invoke-direct {v0}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;-><init>()V

    .line 206
    .local v0, "deviceDialog":Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;
    const-string v3, "device_dialog"

    invoke-virtual {v0, v1, v3}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->show(Landroid/app/FragmentTransaction;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 14
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v13, 0x0

    const v12, 0x7f100114

    const/4 v11, 0x7

    const/4 v10, 0x0

    const/16 v9, 0x8

    .line 49
    sget-object v7, Lcom/samsung/musicplus/dialog/WfdErrorDialog;->CLASSNAME:Ljava/lang/String;

    const-string v8, "onCreateDialog()"

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    if-eqz p1, :cond_0

    .line 52
    const-string v7, "is_checked"

    const/4 v8, 0x1

    invoke-virtual {p1, v7, v8}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    iput-boolean v7, p0, Lcom/samsung/musicplus/dialog/WfdErrorDialog;->mIsChecked:Z

    .line 53
    const-string v7, "exceptional_case"

    invoke-virtual {p1, v7, v10}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v7

    iput v7, p0, Lcom/samsung/musicplus/dialog/WfdErrorDialog;->mExceptionalCase:I

    .line 55
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/WfdErrorDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 57
    .local v0, "a":Landroid/app/Activity;
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 58
    .local v1, "builder":Landroid/app/AlertDialog$Builder;
    iget v7, p0, Lcom/samsung/musicplus/dialog/WfdErrorDialog;->mExceptionalCase:I

    if-ne v7, v11, :cond_3

    .line 59
    const v7, 0x7f1001d5

    invoke-virtual {v1, v7}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 65
    :goto_0
    const-string v7, "layout_inflater"

    invoke-virtual {v0, v7}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 67
    .local v2, "inflater":Landroid/view/LayoutInflater;
    const v7, 0x7f040005

    invoke-virtual {v2, v7, v13}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 68
    .local v3, "mainView":Landroid/view/View;
    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 70
    const v7, 0x7f0d004b

    invoke-virtual {v3, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 71
    .local v6, "text":Landroid/widget/TextView;
    iget v7, p0, Lcom/samsung/musicplus/dialog/WfdErrorDialog;->mExceptionalCase:I

    packed-switch v7, :pswitch_data_0

    .line 103
    :goto_1
    :pswitch_0
    const v7, 0x7f0d004c

    invoke-virtual {v3, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/CheckBox;

    iput-object v7, p0, Lcom/samsung/musicplus/dialog/WfdErrorDialog;->mCheckBox:Landroid/widget/CheckBox;

    .line 104
    iget-object v7, p0, Lcom/samsung/musicplus/dialog/WfdErrorDialog;->mCheckBox:Landroid/widget/CheckBox;

    iget-boolean v8, p0, Lcom/samsung/musicplus/dialog/WfdErrorDialog;->mIsChecked:Z

    invoke-virtual {v7, v8}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 105
    iget-object v7, p0, Lcom/samsung/musicplus/dialog/WfdErrorDialog;->mCheckBox:Landroid/widget/CheckBox;

    new-instance v8, Lcom/samsung/musicplus/dialog/WfdErrorDialog$1;

    invoke-direct {v8, p0}, Lcom/samsung/musicplus/dialog/WfdErrorDialog$1;-><init>(Lcom/samsung/musicplus/dialog/WfdErrorDialog;)V

    invoke-virtual {v7, v8}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 111
    iget-object v7, p0, Lcom/samsung/musicplus/dialog/WfdErrorDialog;->mCheckBox:Landroid/widget/CheckBox;

    new-instance v8, Lcom/samsung/musicplus/dialog/WfdErrorDialog$2;

    invoke-direct {v8, p0}, Lcom/samsung/musicplus/dialog/WfdErrorDialog$2;-><init>(Lcom/samsung/musicplus/dialog/WfdErrorDialog;)V

    invoke-virtual {v7, v8}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 118
    iget v7, p0, Lcom/samsung/musicplus/dialog/WfdErrorDialog;->mExceptionalCase:I

    if-eq v7, v11, :cond_1

    iget v7, p0, Lcom/samsung/musicplus/dialog/WfdErrorDialog;->mExceptionalCase:I

    if-ne v7, v9, :cond_2

    .line 119
    :cond_1
    iget-object v7, p0, Lcom/samsung/musicplus/dialog/WfdErrorDialog;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v7, v9}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 120
    invoke-virtual {v6}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 121
    .local v5, "params":Landroid/view/ViewGroup$MarginLayoutParams;
    iput v10, v5, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    .line 122
    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 125
    .end local v5    # "params":Landroid/view/ViewGroup$MarginLayoutParams;
    :cond_2
    iget v7, p0, Lcom/samsung/musicplus/dialog/WfdErrorDialog;->mExceptionalCase:I

    if-ne v7, v9, :cond_6

    .line 126
    new-instance v7, Lcom/samsung/musicplus/dialog/WfdErrorDialog$3;

    invoke-direct {v7, p0}, Lcom/samsung/musicplus/dialog/WfdErrorDialog$3;-><init>(Lcom/samsung/musicplus/dialog/WfdErrorDialog;)V

    invoke-virtual {v1, v12, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 134
    const v7, 0x7f10003a

    invoke-virtual {v1, v7, v13}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 152
    :goto_2
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v7

    return-object v7

    .line 61
    .end local v2    # "inflater":Landroid/view/LayoutInflater;
    .end local v3    # "mainView":Landroid/view/View;
    .end local v6    # "text":Landroid/widget/TextView;
    :cond_3
    const v7, 0x7f1001c9

    invoke-virtual {v1, v7}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    goto :goto_0

    .line 73
    .restart local v2    # "inflater":Landroid/view/LayoutInflater;
    .restart local v3    # "mainView":Landroid/view/View;
    .restart local v6    # "text":Landroid/widget/TextView;
    :pswitch_1
    sget-boolean v7, Lcom/samsung/musicplus/util/Features;->FLAG_SUPPORT_CHINA_WLAN:Z

    if-eqz v7, :cond_4

    const v4, 0x7f1001d4

    .line 75
    .local v4, "messageId":I
    :goto_3
    invoke-virtual {v6, v4}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    .line 73
    .end local v4    # "messageId":I
    :cond_4
    const v4, 0x7f1001d3

    goto :goto_3

    .line 78
    :pswitch_2
    sget-boolean v7, Lcom/samsung/musicplus/util/Features;->FLAG_SUPPORT_CHINA_WLAN:Z

    if-eqz v7, :cond_5

    const v4, 0x7f1001d2

    .line 80
    .restart local v4    # "messageId":I
    :goto_4
    invoke-virtual {v6, v4}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    .line 78
    .end local v4    # "messageId":I
    :cond_5
    const v4, 0x7f1001d1

    goto :goto_4

    .line 83
    :pswitch_3
    const v7, 0x7f1001d0

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(I)V

    goto :goto_1

    .line 86
    :pswitch_4
    const v7, 0x7f1001cd

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_1

    .line 89
    :pswitch_5
    const v7, 0x7f1001cc

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_1

    .line 92
    :pswitch_6
    const v7, 0x7f1001cf

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_1

    .line 95
    :pswitch_7
    const v7, 0x7f1001ce

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_1

    .line 136
    :cond_6
    new-instance v7, Lcom/samsung/musicplus/dialog/WfdErrorDialog$4;

    invoke-direct {v7, p0}, Lcom/samsung/musicplus/dialog/WfdErrorDialog$4;-><init>(Lcom/samsung/musicplus/dialog/WfdErrorDialog;)V

    invoke-virtual {v1, v12, v7}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_2

    .line 71
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 176
    invoke-super {p0}, Landroid/app/DialogFragment;->onResume()V

    .line 177
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/WfdErrorDialog;->mCheckBox:Landroid/widget/CheckBox;

    if-eqz v0, :cond_0

    .line 178
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/WfdErrorDialog;->mCheckBox:Landroid/widget/CheckBox;

    const v1, 0x7f10005f

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setText(I)V

    .line 180
    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 184
    const-string v0, "is_checked"

    iget-boolean v1, p0, Lcom/samsung/musicplus/dialog/WfdErrorDialog;->mIsChecked:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 185
    const-string v0, "exceptional_case"

    iget v1, p0, Lcom/samsung/musicplus/dialog/WfdErrorDialog;->mExceptionalCase:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 186
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 187
    return-void
.end method
