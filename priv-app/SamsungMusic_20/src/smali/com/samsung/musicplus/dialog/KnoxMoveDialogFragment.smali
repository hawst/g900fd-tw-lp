.class public Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;
.super Landroid/app/DialogFragment;
.source "KnoxMoveDialogFragment.java"

# interfaces
.implements Lcom/samsung/musicplus/contents/MoveKnoxPersonalModeOperation$OnSetResultListener;


# static fields
.field private static final TAG:Ljava/lang/String; = "KnoxModeManager"


# instance fields
.field private mContainerId:J

.field private mContainerName:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mKnoxMode:Z

.field private mKnoxMoveOperation:Lcom/samsung/musicplus/contents/MoveKnoxPersonalModeOperation;

.field private final mKnoxReceiver:Landroid/content/BroadcastReceiver;

.field private mListItems:[J

.field private mProgressDialog:Landroid/app/ProgressDialog;

.field private mSuccessCount:I

.field private mToast:Landroid/widget/Toast;

.field private mTrigger:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 66
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 64
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mTrigger:Z

    .line 191
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mSuccessCount:I

    .line 193
    new-instance v0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment$2;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment$2;-><init>(Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;)V

    iput-object v0, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mKnoxReceiver:Landroid/content/BroadcastReceiver;

    .line 67
    return-void
.end method

.method public constructor <init>([JJ)V
    .locals 2
    .param p1, "items"    # [J
    .param p2, "containerId"    # J

    .prologue
    .line 69
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 64
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mTrigger:Z

    .line 191
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mSuccessCount:I

    .line 193
    new-instance v0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment$2;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment$2;-><init>(Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;)V

    iput-object v0, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mKnoxReceiver:Landroid/content/BroadcastReceiver;

    .line 70
    iput-object p1, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mListItems:[J

    .line 71
    iput-wide p2, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mContainerId:J

    .line 72
    return-void
.end method

.method public constructor <init>([JJLjava/lang/String;)V
    .locals 2
    .param p1, "items"    # [J
    .param p2, "containerId"    # J
    .param p4, "name"    # Ljava/lang/String;

    .prologue
    .line 74
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 64
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mTrigger:Z

    .line 191
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mSuccessCount:I

    .line 193
    new-instance v0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment$2;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment$2;-><init>(Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;)V

    iput-object v0, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mKnoxReceiver:Landroid/content/BroadcastReceiver;

    .line 75
    iput-object p1, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mListItems:[J

    .line 76
    iput-wide p2, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mContainerId:J

    .line 77
    iput-object p4, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mContainerName:Ljava/lang/String;

    .line 78
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;)Lcom/samsung/musicplus/contents/MoveKnoxPersonalModeOperation;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mKnoxMoveOperation:Lcom/samsung/musicplus/contents/MoveKnoxPersonalModeOperation;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->endOperation()V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->showToast(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$300(Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;)[J
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mListItems:[J

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;

    .prologue
    .line 46
    iget v0, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mSuccessCount:I

    return v0
.end method

.method static synthetic access$404(Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;

    .prologue
    .line 46
    iget v0, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mSuccessCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mSuccessCount:I

    return v0
.end method

.method static synthetic access$500(Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;
    .param p1, "x1"    # I

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->refreshProgressBar(I)V

    return-void
.end method

.method static synthetic access$600(Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$700(Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;

    .prologue
    .line 46
    iget-boolean v0, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mKnoxMode:Z

    return v0
.end method

.method static synthetic access$800(Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mContainerName:Ljava/lang/String;

    return-object v0
.end method

.method private endOperation()V
    .locals 2

    .prologue
    .line 180
    iget-object v1, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 181
    iget-object v1, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    .line 183
    :cond_0
    iget-object v1, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mKnoxMoveOperation:Lcom/samsung/musicplus/contents/MoveKnoxPersonalModeOperation;

    invoke-virtual {v1}, Lcom/samsung/musicplus/contents/MoveKnoxPersonalModeOperation;->endFileOperation()V

    .line 185
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 186
    .local v0, "a":Landroid/app/Activity;
    instance-of v1, v0, Lcom/samsung/musicplus/contents/extra/MusicSelectListActivity;

    if-eqz v1, :cond_1

    .line 187
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 189
    :cond_1
    return-void
.end method

.method private refreshProgressBar(I)V
    .locals 1
    .param p1, "i"    # I

    .prologue
    .line 174
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->getProgress()I

    move-result v0

    if-ge v0, p1, :cond_0

    .line 175
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, p1}, Landroid/app/ProgressDialog;->setProgress(I)V

    .line 177
    :cond_0
    return-void
.end method

.method private registerKnoxIntentReceiver()V
    .locals 3

    .prologue
    .line 157
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 158
    .local v0, "f":Landroid/content/IntentFilter;
    const-string v1, "com.sec.knox.container.FileRelayDone"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 159
    const-string v1, "com.sec.knox.container.FileRelayFail"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 160
    const-string v1, "com.sec.knox.container.FileRelayComplete"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 161
    const-string v1, "com.sec.knox.container.FileRelayProgress"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 163
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mKnoxReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 164
    return-void
.end method

.method private showToast(Ljava/lang/String;)V
    .locals 2
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 327
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mToast:Landroid/widget/Toast;

    if-nez v0, :cond_0

    .line 328
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mContext:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mToast:Landroid/widget/Toast;

    .line 332
    :goto_0
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 333
    return-void

    .line 330
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0, p1}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method


# virtual methods
.method public onCanceled()V
    .locals 4

    .prologue
    .line 311
    const-string v1, "KnoxModeManager"

    const-string v2, "onCanceled()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 313
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 314
    .local v0, "handler":Landroid/os/Handler;
    new-instance v1, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment$5;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment$5;-><init>(Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;)V

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 322
    return-void
.end method

.method public onCompleted()V
    .locals 4

    .prologue
    .line 253
    const-string v1, "KnoxModeManager"

    const-string v2, "onCompleted()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 255
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 256
    .local v0, "handler":Landroid/os/Handler;
    new-instance v1, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment$3;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment$3;-><init>(Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 287
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 82
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 83
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->setRetainInstance(Z)V

    .line 84
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v4, 0x7f10009f

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 96
    const-string v2, "KnoxModeManager"

    const-string v3, "KnoxMoveDialog create!"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mListItems:[J

    if-nez v2, :cond_0

    .line 99
    const-string v2, "KnoxModeManager"

    const-string v3, "mListItems is null"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    const/4 v2, 0x0

    .line 146
    :goto_0
    return-object v2

    .line 102
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 103
    .local v0, "a":Landroid/app/Activity;
    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mContext:Landroid/content/Context;

    .line 104
    new-instance v2, Lcom/samsung/musicplus/contents/MoveKnoxPersonalModeOperation;

    iget-object v3, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3, p0}, Lcom/samsung/musicplus/contents/MoveKnoxPersonalModeOperation;-><init>(Landroid/content/Context;Lcom/samsung/musicplus/contents/MoveKnoxPersonalModeOperation$OnSetResultListener;)V

    iput-object v2, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mKnoxMoveOperation:Lcom/samsung/musicplus/contents/MoveKnoxPersonalModeOperation;

    .line 106
    new-instance v2, Landroid/app/ProgressDialog;

    invoke-direct {v2, v0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 107
    sget-boolean v2, Lcom/samsung/musicplus/util/Version;->LIBRARY_OVER_API_1:Z

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;->availableKnoxPersonalMode(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 109
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/samsung/musicplus/util/KnoxGateManager;->isKnoxModeOn(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 110
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    const v3, 0x7f1000a0

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setTitle(I)V

    .line 111
    iput-boolean v6, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mKnoxMode:Z

    .line 121
    :goto_1
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f100129

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 122
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v2, v6}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    .line 124
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mListItems:[J

    array-length v1, v2

    .line 125
    .local v1, "length":I
    if-ne v1, v6, :cond_4

    .line 126
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    const-string v3, "(1/1)"

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setProgressNumberFormat(Ljava/lang/String;)V

    .line 130
    :goto_2
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v2, v1}, Landroid/app/ProgressDialog;->setMax(I)V

    .line 131
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v2, v5}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 132
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v2, v5}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 133
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    const/4 v3, -0x2

    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f10003a

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment$1;

    invoke-direct {v5, p0}, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment$1;-><init>(Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;)V

    invoke-virtual {v2, v3, v4, v5}, Landroid/app/ProgressDialog;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 142
    iget-boolean v2, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mTrigger:Z

    if-eqz v2, :cond_1

    .line 143
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mKnoxMoveOperation:Lcom/samsung/musicplus/contents/MoveKnoxPersonalModeOperation;

    iget-object v3, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mListItems:[J

    iget-wide v4, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mContainerId:J

    invoke-virtual {v2, v3, v4, v5}, Lcom/samsung/musicplus/contents/MoveKnoxPersonalModeOperation;->requestMoveToKnox([JJ)V

    .line 146
    :cond_1
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    goto/16 :goto_0

    .line 113
    .end local v1    # "length":I
    :cond_2
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v2, v4}, Landroid/app/ProgressDialog;->setTitle(I)V

    .line 114
    iput-boolean v5, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mKnoxMode:Z

    goto :goto_1

    .line 117
    :cond_3
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v2, v4}, Landroid/app/ProgressDialog;->setTitle(I)V

    .line 118
    iput-boolean v5, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mKnoxMode:Z

    goto :goto_1

    .line 128
    .restart local v1    # "length":I
    :cond_4
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    const-string v3, "(%1d/%2d)"

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setProgressNumberFormat(Ljava/lang/String;)V

    goto :goto_2
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 87
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->getRetainInstance()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 88
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setDismissMessage(Landroid/os/Message;)V

    .line 89
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mTrigger:Z

    .line 91
    :cond_0
    invoke-super {p0}, Landroid/app/DialogFragment;->onDestroyView()V

    .line 92
    return-void
.end method

.method public onFailed()V
    .locals 4

    .prologue
    .line 291
    const-string v1, "KnoxModeManager"

    const-string v2, "onFailed()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 293
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 294
    .local v0, "handler":Landroid/os/Handler;
    new-instance v1, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment$4;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment$4;-><init>(Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;)V

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 307
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 168
    invoke-super {p0}, Landroid/app/DialogFragment;->onPause()V

    .line 170
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mKnoxReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 171
    return-void
.end method

.method public onRelayResult(II)V
    .locals 4
    .param p1, "result"    # I
    .param p2, "progress"    # I

    .prologue
    .line 240
    if-nez p1, :cond_0

    .line 241
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mKnoxMoveOperation:Lcom/samsung/musicplus/contents/MoveKnoxPersonalModeOperation;

    iget-object v1, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mListItems:[J

    iget v2, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mSuccessCount:I

    aget-wide v2, v1, v2

    invoke-virtual {v0, v2, v3}, Lcom/samsung/musicplus/contents/MoveKnoxPersonalModeOperation;->deleteListItemInDB(J)V

    .line 242
    iget v0, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mSuccessCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mSuccessCount:I

    .line 243
    const-string v0, "KnoxModeManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onRelayResult(), success count : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mSuccessCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 248
    :goto_0
    invoke-direct {p0, p2}, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->refreshProgressBar(I)V

    .line 249
    return-void

    .line 245
    :cond_0
    const-string v0, "KnoxModeManager"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onRelayResult(), failed because of : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 246
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->onFailed()V

    goto :goto_0
.end method

.method public onResume()V
    .locals 0

    .prologue
    .line 151
    invoke-super {p0}, Landroid/app/DialogFragment;->onResume()V

    .line 153
    invoke-direct {p0}, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->registerKnoxIntentReceiver()V

    .line 154
    return-void
.end method
