.class public Lcom/samsung/musicplus/dialog/PlaySpeedDialog;
.super Landroid/app/DialogFragment;
.source "PlaySpeedDialog.java"

# interfaces
.implements Lcom/samsung/musicplus/settings/PlayerSettingPreference;


# static fields
.field private static final CLASSNAME:Ljava/lang/String;

.field private static final INCREMENT:F

.field private static final MAX_PLAY_SPEED_INDEX:I

.field private static final MAX_VALUE:F = 2.0f

.field private static final MINUS_BUTTON_LONG_PRESS:I = 0x1

.field private static final MIN_VALUE:F = 0.5f

.field private static final PLUS_BUTTON_LONG_PRESS:I = 0x0

.field private static final SAVED_INSTANCE_PROGRESS:Ljava/lang/String; = "saved_progress"

.field private static final UPDATE_DELAY:I = 0x1f4


# instance fields
.field private mMinusImage:Landroid/widget/ImageButton;

.field private mPause:Z

.field private final mPlaySpeedEventHandelr:Landroid/os/Handler;

.field private mPlaySpeedText:Landroid/widget/TextView;

.field private mPlusImage:Landroid/widget/ImageButton;

.field private mPressedButton:Z

.field private mProgress:I

.field private mSeekBar:Landroid/widget/SeekBar;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 30
    const-class v0, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->CLASSNAME:Ljava/lang/String;

    .line 32
    sget-boolean v0, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_SUPPROT_ENHANCED_PLAY_SPEED:Z

    if-eqz v0, :cond_0

    const/16 v0, 0xf

    :goto_0
    sput v0, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->MAX_PLAY_SPEED_INDEX:I

    .line 39
    const/high16 v0, 0x3fc00000    # 1.5f

    sget v1, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->MAX_PLAY_SPEED_INDEX:I

    int-to-float v1, v1

    div-float/2addr v0, v1

    sput v0, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->INCREMENT:F

    return-void

    .line 32
    :cond_0
    const/4 v0, 0x3

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 29
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 59
    iput-boolean v0, p0, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->mPause:Z

    .line 61
    iput-boolean v0, p0, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->mPressedButton:Z

    .line 231
    new-instance v0, Lcom/samsung/musicplus/dialog/PlaySpeedDialog$8;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/dialog/PlaySpeedDialog$8;-><init>(Lcom/samsung/musicplus/dialog/PlaySpeedDialog;)V

    iput-object v0, p0, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->mPlaySpeedEventHandelr:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/dialog/PlaySpeedDialog;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/PlaySpeedDialog;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->mPlaySpeedEventHandelr:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/musicplus/dialog/PlaySpeedDialog;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/PlaySpeedDialog;

    .prologue
    .line 29
    iget v0, p0, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->mProgress:I

    return v0
.end method

.method static synthetic access$102(Lcom/samsung/musicplus/dialog/PlaySpeedDialog;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/PlaySpeedDialog;
    .param p1, "x1"    # I

    .prologue
    .line 29
    iput p1, p0, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->mProgress:I

    return p1
.end method

.method static synthetic access$200(Lcom/samsung/musicplus/dialog/PlaySpeedDialog;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/PlaySpeedDialog;
    .param p1, "x1"    # I

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->setProgress(I)V

    return-void
.end method

.method static synthetic access$300(Lcom/samsung/musicplus/dialog/PlaySpeedDialog;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/PlaySpeedDialog;

    .prologue
    .line 29
    iget-boolean v0, p0, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->mPause:Z

    return v0
.end method

.method static synthetic access$400(Lcom/samsung/musicplus/dialog/PlaySpeedDialog;II)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/PlaySpeedDialog;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->handlePlaySpeed(II)Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/samsung/musicplus/dialog/PlaySpeedDialog;I)F
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/PlaySpeedDialog;
    .param p1, "x1"    # I

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->getPlaySpeedValueFromIndex(I)F

    move-result v0

    return v0
.end method

.method static synthetic access$602(Lcom/samsung/musicplus/dialog/PlaySpeedDialog;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/PlaySpeedDialog;
    .param p1, "x1"    # Z

    .prologue
    .line 29
    iput-boolean p1, p0, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->mPressedButton:Z

    return p1
.end method

.method static synthetic access$700(Lcom/samsung/musicplus/dialog/PlaySpeedDialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/PlaySpeedDialog;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->upPlaySpeed()V

    return-void
.end method

.method static synthetic access$800(Lcom/samsung/musicplus/dialog/PlaySpeedDialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/PlaySpeedDialog;

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->downPlaySpeed()V

    return-void
.end method

.method private downPlaySpeed()V
    .locals 1

    .prologue
    .line 275
    iget v0, p0, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->mProgress:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->mProgress:I

    .line 276
    iget v0, p0, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->mProgress:I

    if-gez v0, :cond_0

    .line 277
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->mProgress:I

    .line 281
    :goto_0
    return-void

    .line 280
    :cond_0
    iget v0, p0, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->mProgress:I

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->setProgress(I)V

    goto :goto_0
.end method

.method private getPlaySpeedSeekBarIndex(F)I
    .locals 4
    .param p1, "playSpeed"    # F

    .prologue
    .line 251
    const/4 v0, 0x0

    .local v0, "index":I
    :goto_0
    sget v1, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->MAX_PLAY_SPEED_INDEX:I

    if-gt v0, v1, :cond_0

    .line 252
    invoke-direct {p0, v0}, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->getPlaySpeedValueFromIndex(I)F

    move-result v1

    cmpl-float v1, p1, v1

    if-nez v1, :cond_1

    .line 253
    sget-object v1, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->CLASSNAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getPlaySpeedSeekBarIndex() - index: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " playSpeed: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    :cond_0
    return v0

    .line 251
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private getPlaySpeedValueFromIndex(I)F
    .locals 3
    .param p1, "index"    # I

    .prologue
    const/high16 v2, 0x41200000    # 10.0f

    .line 262
    int-to-float v0, p1

    sget v1, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->INCREMENT:F

    mul-float/2addr v0, v1

    const/high16 v1, 0x3f000000    # 0.5f

    add-float/2addr v0, v1

    mul-float/2addr v0, v2

    float-to-int v0, v0

    int-to-float v0, v0

    div-float/2addr v0, v2

    return v0
.end method

.method private handlePlaySpeed(II)Z
    .locals 5
    .param p1, "action"    # I
    .param p2, "speedEvent"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 211
    sget-object v2, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->CLASSNAME:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "handlePlaySpeed - action : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " speed event : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    packed-switch p1, :pswitch_data_0

    move v0, v1

    .line 227
    :cond_0
    :goto_0
    return v0

    .line 215
    :pswitch_0
    iget-object v1, p0, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->mPlaySpeedEventHandelr:Landroid/os/Handler;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v1, p2, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    .line 218
    :pswitch_1
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->mPlusImage:Landroid/widget/ImageButton;

    invoke-virtual {v2, v1}, Landroid/widget/ImageButton;->playSoundEffect(I)V

    .line 219
    iget-object v1, p0, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->mPlaySpeedEventHandelr:Landroid/os/Handler;

    invoke-virtual {v1, p2}, Landroid/os/Handler;->removeMessages(I)V

    .line 220
    if-nez p2, :cond_1

    .line 221
    invoke-direct {p0}, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->upPlaySpeed()V

    goto :goto_0

    .line 222
    :cond_1
    if-ne p2, v0, :cond_0

    .line 223
    invoke-direct {p0}, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->downPlaySpeed()V

    goto :goto_0

    .line 213
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private setProgress(I)V
    .locals 7
    .param p1, "progress"    # I

    .prologue
    .line 284
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->isAdded()Z

    move-result v1

    if-nez v1, :cond_0

    .line 295
    :goto_0
    return-void

    .line 287
    :cond_0
    sget-object v1, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->CLASSNAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setProgress - progress: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 289
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->getPlaySpeedValueFromIndex(I)F

    move-result v0

    .line 290
    .local v0, "playSpeed":F
    invoke-static {v0}, Lcom/samsung/musicplus/util/ServiceUtils;->setPlaySpeed(F)V

    .line 291
    iget-object v1, p0, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->mSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v1, p1}, Landroid/widget/SeekBar;->setProgress(I)V

    .line 292
    iget-object v1, p0, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->mPlaySpeedText:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "%.1f"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "x"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 293
    iget-object v1, p0, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->mSeekBar:Landroid/widget/SeekBar;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f100123

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->mPlaySpeedText:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\n"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private upPlaySpeed()V
    .locals 2

    .prologue
    .line 266
    iget v0, p0, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->mProgress:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->mProgress:I

    .line 267
    iget v0, p0, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->mProgress:I

    sget v1, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->MAX_PLAY_SPEED_INDEX:I

    if-le v0, v1, :cond_0

    .line 268
    sget v0, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->MAX_PLAY_SPEED_INDEX:I

    iput v0, p0, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->mProgress:I

    .line 272
    :goto_0
    return-void

    .line 271
    :cond_0
    iget v0, p0, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->mProgress:I

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->setProgress(I)V

    goto :goto_0
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 339
    const-string v1, "play_speed"

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-static {v1, v2}, Lcom/samsung/musicplus/util/ServiceUtils;->getPreferencesFloat(Ljava/lang/String;F)F

    move-result v0

    .line 340
    .local v0, "savedSpeed":F
    invoke-direct {p0, v0}, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->getPlaySpeedSeekBarIndex(F)I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->setProgress(I)V

    .line 341
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    .line 342
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 346
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->mPlaySpeedEventHandelr:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 347
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->mPlaySpeedEventHandelr:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 348
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 349
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 65
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 67
    .local v0, "activity":Landroid/app/Activity;
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 68
    .local v1, "builder":Landroid/app/AlertDialog$Builder;
    const v4, 0x7f100123

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 71
    const-string v4, "layout_inflater"

    invoke-virtual {v0, v4}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 73
    .local v2, "inflater":Landroid/view/LayoutInflater;
    const v4, 0x7f040071

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 74
    .local v3, "mainView":Landroid/view/View;
    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 76
    const v4, 0x7f0d014a

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->mPlaySpeedText:Landroid/widget/TextView;

    .line 78
    const v4, 0x7f0d014c

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/SeekBar;

    iput-object v4, p0, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->mSeekBar:Landroid/widget/SeekBar;

    .line 79
    iget-object v4, p0, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->mSeekBar:Landroid/widget/SeekBar;

    sget v5, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->MAX_PLAY_SPEED_INDEX:I

    invoke-virtual {v4, v5}, Landroid/widget/SeekBar;->setMax(I)V

    .line 80
    iget-object v4, p0, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->mSeekBar:Landroid/widget/SeekBar;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/widget/SeekBar;->incrementProgressBy(I)V

    .line 82
    if-eqz p1, :cond_0

    .line 83
    const-string v4, "saved_progress"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    iput v4, p0, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->mProgress:I

    .line 87
    :goto_0
    iget v4, p0, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->mProgress:I

    invoke-direct {p0, v4}, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->setProgress(I)V

    .line 90
    iget-object v4, p0, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->mSeekBar:Landroid/widget/SeekBar;

    new-instance v5, Lcom/samsung/musicplus/dialog/PlaySpeedDialog$1;

    invoke-direct {v5, p0}, Lcom/samsung/musicplus/dialog/PlaySpeedDialog$1;-><init>(Lcom/samsung/musicplus/dialog/PlaySpeedDialog;)V

    invoke-virtual {v4, v5}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 114
    const v4, 0x7f0d014d

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageButton;

    iput-object v4, p0, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->mPlusImage:Landroid/widget/ImageButton;

    .line 115
    iget-object v4, p0, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->mPlusImage:Landroid/widget/ImageButton;

    new-instance v5, Lcom/samsung/musicplus/dialog/PlaySpeedDialog$2;

    invoke-direct {v5, p0}, Lcom/samsung/musicplus/dialog/PlaySpeedDialog$2;-><init>(Lcom/samsung/musicplus/dialog/PlaySpeedDialog;)V

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 126
    iget-object v4, p0, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->mPlusImage:Landroid/widget/ImageButton;

    new-instance v5, Lcom/samsung/musicplus/dialog/PlaySpeedDialog$3;

    invoke-direct {v5, p0}, Lcom/samsung/musicplus/dialog/PlaySpeedDialog$3;-><init>(Lcom/samsung/musicplus/dialog/PlaySpeedDialog;)V

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 140
    const v4, 0x7f0d014b

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageButton;

    iput-object v4, p0, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->mMinusImage:Landroid/widget/ImageButton;

    .line 141
    iget-object v4, p0, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->mMinusImage:Landroid/widget/ImageButton;

    new-instance v5, Lcom/samsung/musicplus/dialog/PlaySpeedDialog$4;

    invoke-direct {v5, p0}, Lcom/samsung/musicplus/dialog/PlaySpeedDialog$4;-><init>(Lcom/samsung/musicplus/dialog/PlaySpeedDialog;)V

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 152
    iget-object v4, p0, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->mMinusImage:Landroid/widget/ImageButton;

    new-instance v5, Lcom/samsung/musicplus/dialog/PlaySpeedDialog$5;

    invoke-direct {v5, p0}, Lcom/samsung/musicplus/dialog/PlaySpeedDialog$5;-><init>(Lcom/samsung/musicplus/dialog/PlaySpeedDialog;)V

    invoke-virtual {v4, v5}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 166
    const v4, 0x7f100114

    new-instance v5, Lcom/samsung/musicplus/dialog/PlaySpeedDialog$6;

    invoke-direct {v5, p0}, Lcom/samsung/musicplus/dialog/PlaySpeedDialog$6;-><init>(Lcom/samsung/musicplus/dialog/PlaySpeedDialog;)V

    invoke-virtual {v1, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 181
    const v4, 0x7f10003a

    new-instance v5, Lcom/samsung/musicplus/dialog/PlaySpeedDialog$7;

    invoke-direct {v5, p0}, Lcom/samsung/musicplus/dialog/PlaySpeedDialog$7;-><init>(Lcom/samsung/musicplus/dialog/PlaySpeedDialog;)V

    invoke-virtual {v1, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 192
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v4

    return-object v4

    .line 85
    :cond_0
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getPlaySpeed()F

    move-result v4

    invoke-direct {p0, v4}, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->getPlaySpeedSeekBarIndex(F)I

    move-result v4

    iput v4, p0, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->mProgress:I

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 317
    iget-boolean v0, p0, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->mPressedButton:Z

    if-nez v0, :cond_0

    .line 322
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->onCancel(Landroid/content/DialogInterface;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 327
    :cond_0
    :goto_0
    invoke-super {p0}, Landroid/app/DialogFragment;->onDestroy()V

    .line 328
    return-void

    .line 323
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 0
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 333
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 334
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 311
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->mPause:Z

    .line 312
    invoke-super {p0}, Landroid/app/DialogFragment;->onPause()V

    .line 313
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 305
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->mPause:Z

    .line 306
    invoke-super {p0}, Landroid/app/DialogFragment;->onResume()V

    .line 307
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 299
    const-string v0, "saved_progress"

    iget v1, p0, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->mProgress:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 300
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 301
    return-void
.end method
