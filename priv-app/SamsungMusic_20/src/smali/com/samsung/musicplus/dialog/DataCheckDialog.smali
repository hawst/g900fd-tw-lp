.class public Lcom/samsung/musicplus/dialog/DataCheckDialog;
.super Landroid/app/DialogFragment;
.source "DataCheckDialog.java"


# static fields
.field private static final CLASSNAME:Ljava/lang/String;

.field private static final DATA_CONNECTED_MOBILE:I = 0x2

.field private static final DATA_CONNECTED_WIFI:I = 0x1

.field private static final DATA_DISCONNECTED:I = 0x0

.field private static final PREF_DATA_CHECK_HELP:Ljava/lang/String; = "data_check_help"

.field private static final PREF_KEY_DATA_CHECK_HELP_DLNA:Ljava/lang/String; = "data_check_help_dlna"

.field private static final PREF_KEY_DATA_CHECK_HELP_NEARBY_ITEM:Ljava/lang/String; = "data_check_help_nearby_item"

.field private static bShowDataCheckHelpDLNA:Z

.field private static bShowDataCheckHelpNearBy:Z


# instance fields
.field private mCheckBox:Landroid/widget/CheckBox;

.field private mIsChecked:Z

.field private mKeyWord:Ljava/lang/String;

.field private mNetworkStatus:I

.field private mPrefKey:Ljava/lang/String;

.field private mTitle:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 36
    const-class v0, Lcom/samsung/musicplus/dialog/DataCheckDialog;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/dialog/DataCheckDialog;->CLASSNAME:Ljava/lang/String;

    .line 44
    sput-boolean v1, Lcom/samsung/musicplus/dialog/DataCheckDialog;->bShowDataCheckHelpNearBy:Z

    .line 46
    sput-boolean v1, Lcom/samsung/musicplus/dialog/DataCheckDialog;->bShowDataCheckHelpDLNA:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 132
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 56
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/dialog/DataCheckDialog;->mIsChecked:Z

    .line 136
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;)V
    .locals 3
    .param p1, "networkStatus"    # I
    .param p2, "prefKey"    # Ljava/lang/String;

    .prologue
    .line 138
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 56
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/dialog/DataCheckDialog;->mIsChecked:Z

    .line 139
    sget-object v0, Lcom/samsung/musicplus/dialog/DataCheckDialog;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DataCheckDialog() : networkStatus="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", prefKey="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 141
    iput-object p2, p0, Lcom/samsung/musicplus/dialog/DataCheckDialog;->mPrefKey:Ljava/lang/String;

    .line 142
    const-string v0, ""

    iput-object v0, p0, Lcom/samsung/musicplus/dialog/DataCheckDialog;->mKeyWord:Ljava/lang/String;

    .line 143
    iput p1, p0, Lcom/samsung/musicplus/dialog/DataCheckDialog;->mNetworkStatus:I

    .line 144
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "networkStatus"    # I
    .param p2, "keyWord"    # Ljava/lang/String;
    .param p3, "prefKey"    # Ljava/lang/String;

    .prologue
    .line 146
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 56
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/dialog/DataCheckDialog;->mIsChecked:Z

    .line 147
    sget-object v0, Lcom/samsung/musicplus/dialog/DataCheckDialog;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DataCheckDialog() : networkStatus="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", keyWord="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", prefKey="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    iput-object p3, p0, Lcom/samsung/musicplus/dialog/DataCheckDialog;->mPrefKey:Ljava/lang/String;

    .line 150
    iput-object p2, p0, Lcom/samsung/musicplus/dialog/DataCheckDialog;->mKeyWord:Ljava/lang/String;

    .line 151
    iput p1, p0, Lcom/samsung/musicplus/dialog/DataCheckDialog;->mNetworkStatus:I

    .line 152
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "networkStatus"    # I
    .param p2, "keyWord"    # Ljava/lang/String;
    .param p3, "prefKey"    # Ljava/lang/String;
    .param p4, "title"    # Ljava/lang/String;

    .prologue
    .line 154
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 56
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/dialog/DataCheckDialog;->mIsChecked:Z

    .line 155
    sget-object v0, Lcom/samsung/musicplus/dialog/DataCheckDialog;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "DataCheckDialog() : networkStatus="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", keyWord="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", prefKey="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", title="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    iput-object p3, p0, Lcom/samsung/musicplus/dialog/DataCheckDialog;->mPrefKey:Ljava/lang/String;

    .line 158
    iput-object p2, p0, Lcom/samsung/musicplus/dialog/DataCheckDialog;->mKeyWord:Ljava/lang/String;

    .line 159
    iput p1, p0, Lcom/samsung/musicplus/dialog/DataCheckDialog;->mNetworkStatus:I

    .line 160
    iput-object p4, p0, Lcom/samsung/musicplus/dialog/DataCheckDialog;->mTitle:Ljava/lang/String;

    .line 161
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    sget-object v0, Lcom/samsung/musicplus/dialog/DataCheckDialog;->CLASSNAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/musicplus/dialog/DataCheckDialog;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/DataCheckDialog;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/DataCheckDialog;->mPrefKey:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/musicplus/dialog/DataCheckDialog;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/DataCheckDialog;

    .prologue
    .line 34
    iget-boolean v0, p0, Lcom/samsung/musicplus/dialog/DataCheckDialog;->mIsChecked:Z

    return v0
.end method

.method static synthetic access$202(Lcom/samsung/musicplus/dialog/DataCheckDialog;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/DataCheckDialog;
    .param p1, "x1"    # Z

    .prologue
    .line 34
    iput-boolean p1, p0, Lcom/samsung/musicplus/dialog/DataCheckDialog;->mIsChecked:Z

    return p1
.end method

.method static synthetic access$300(Lcom/samsung/musicplus/dialog/DataCheckDialog;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/DataCheckDialog;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/DataCheckDialog;->mKeyWord:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/musicplus/dialog/DataCheckDialog;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/DataCheckDialog;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/DataCheckDialog;->mTitle:Ljava/lang/String;

    return-object v0
.end method

.method private static checkNetworkStatus(Landroid/content/Context;)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x2

    .line 67
    invoke-static {p0}, Lcom/samsung/musicplus/util/DlnaUtils;->isAvailableScanNearbyDevices(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 68
    const/4 v1, 0x1

    .line 78
    :cond_0
    :goto_0
    return v1

    .line 71
    :cond_1
    const-string v2, "phone"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 73
    .local v0, "telephonyManager":Landroid/telephony/TelephonyManager;
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getDataState()I

    move-result v2

    if-eq v2, v1, :cond_0

    .line 78
    :cond_2
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isDataHelpChecked(Landroid/content/Context;I)Z
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "list"    # I

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 82
    sget-boolean v4, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_DATA_CHECK_POPUP:Z

    if-eqz v4, :cond_0

    .line 83
    invoke-static {p1}, Lcom/samsung/musicplus/util/ListUtils;->getPreDefinedList(I)I

    move-result v4

    const v5, 0x2000b

    if-ne v4, v5, :cond_0

    .line 84
    sget-boolean v4, Lcom/samsung/musicplus/dialog/DataCheckDialog;->bShowDataCheckHelpNearBy:Z

    if-eqz v4, :cond_0

    .line 85
    const-string v4, "data_check_help"

    invoke-virtual {p0, v4, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 88
    .local v1, "sp":Landroid/content/SharedPreferences;
    invoke-static {p0}, Lcom/samsung/musicplus/dialog/DataCheckDialog;->checkNetworkStatus(Landroid/content/Context;)I

    move-result v0

    .line 89
    .local v0, "networkStatus":I
    if-ne v0, v2, :cond_0

    .line 90
    const-string v4, "data_check_help_nearby_item"

    invoke-interface {v1, v4, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    sput-boolean v4, Lcom/samsung/musicplus/dialog/DataCheckDialog;->bShowDataCheckHelpNearBy:Z

    if-eqz v4, :cond_0

    .line 98
    .end local v0    # "networkStatus":I
    .end local v1    # "sp":Landroid/content/SharedPreferences;
    :goto_0
    return v2

    :cond_0
    move v2, v3

    goto :goto_0
.end method

.method public static isDataHelpDlna(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 110
    sget-boolean v4, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_DATA_CHECK_POPUP:Z

    if-eqz v4, :cond_0

    .line 111
    sget-boolean v4, Lcom/samsung/musicplus/dialog/DataCheckDialog;->bShowDataCheckHelpDLNA:Z

    if-eqz v4, :cond_0

    .line 112
    const-string v4, "data_check_help"

    invoke-virtual {p0, v4, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 114
    .local v1, "sp":Landroid/content/SharedPreferences;
    invoke-static {p0}, Lcom/samsung/musicplus/dialog/DataCheckDialog;->checkNetworkStatus(Landroid/content/Context;)I

    move-result v0

    .line 115
    .local v0, "networkStatus":I
    if-ne v0, v2, :cond_0

    .line 116
    const-string v4, "data_check_help_dlna"

    invoke-interface {v1, v4, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    sput-boolean v4, Lcom/samsung/musicplus/dialog/DataCheckDialog;->bShowDataCheckHelpDLNA:Z

    if-eqz v4, :cond_0

    .line 123
    .end local v0    # "networkStatus":I
    .end local v1    # "sp":Landroid/content/SharedPreferences;
    :goto_0
    return v2

    :cond_0
    move v2, v3

    goto :goto_0
.end method

.method public static showDataCheckDialog(Landroid/content/Context;Landroid/app/FragmentManager;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "fm"    # Landroid/app/FragmentManager;
    .param p2, "keyWord"    # Ljava/lang/String;
    .param p3, "title"    # Ljava/lang/String;

    .prologue
    .line 103
    invoke-static {p0}, Lcom/samsung/musicplus/dialog/DataCheckDialog;->checkNetworkStatus(Landroid/content/Context;)I

    move-result v0

    .line 104
    .local v0, "networkStatus":I
    new-instance v1, Lcom/samsung/musicplus/dialog/DataCheckDialog;

    const-string v2, "data_check_help_nearby_item"

    invoke-direct {v1, v0, p2, v2, p3}, Lcom/samsung/musicplus/dialog/DataCheckDialog;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "data_check_help_dialog"

    invoke-virtual {v1, p1, v2}, Lcom/samsung/musicplus/dialog/DataCheckDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 107
    return-void
.end method

.method public static showDataHelpDlnaDialog(Landroid/content/Context;Landroid/app/FragmentManager;)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "fm"    # Landroid/app/FragmentManager;

    .prologue
    .line 127
    invoke-static {p0}, Lcom/samsung/musicplus/dialog/DataCheckDialog;->checkNetworkStatus(Landroid/content/Context;)I

    move-result v0

    .line 128
    .local v0, "networkStatus":I
    new-instance v1, Lcom/samsung/musicplus/dialog/DataCheckDialog;

    const-string v2, "data_check_help_dlna"

    invoke-direct {v1, v0, v2}, Lcom/samsung/musicplus/dialog/DataCheckDialog;-><init>(ILjava/lang/String;)V

    const-string v2, "data_check_help_dialog"

    invoke-virtual {v1, p1, v2}, Lcom/samsung/musicplus/dialog/DataCheckDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 130
    return-void
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 12
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v7, 0x0

    const/4 v11, 0x1

    const/4 v8, 0x0

    .line 173
    const/4 v5, -0x1

    .line 174
    .local v5, "stringBodyId":I
    const/4 v6, -0x1

    .line 176
    .local v6, "stringTitleId":I
    invoke-virtual {p0, v8, v8}, Lcom/samsung/musicplus/dialog/DataCheckDialog;->setStyle(II)V

    .line 178
    if-eqz p1, :cond_0

    .line 179
    const-string v8, "prefKey"

    invoke-virtual {p1, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/samsung/musicplus/dialog/DataCheckDialog;->mPrefKey:Ljava/lang/String;

    .line 180
    const-string v8, "keyWord"

    invoke-virtual {p1, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/samsung/musicplus/dialog/DataCheckDialog;->mKeyWord:Ljava/lang/String;

    .line 181
    const-string v8, "networkStatus"

    invoke-virtual {p1, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v8

    iput v8, p0, Lcom/samsung/musicplus/dialog/DataCheckDialog;->mNetworkStatus:I

    .line 184
    :cond_0
    iget v8, p0, Lcom/samsung/musicplus/dialog/DataCheckDialog;->mNetworkStatus:I

    if-ne v8, v11, :cond_2

    .line 185
    const v5, 0x7f10004d

    .line 186
    const v6, 0x7f10004e

    .line 194
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/DataCheckDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 195
    .local v0, "a":Landroid/app/Activity;
    new-instance v8, Landroid/app/AlertDialog$Builder;

    invoke-direct {v8, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v8, v6}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v8

    const v9, 0x7f10003a

    new-instance v10, Lcom/samsung/musicplus/dialog/DataCheckDialog$2;

    invoke-direct {v10, p0}, Lcom/samsung/musicplus/dialog/DataCheckDialog$2;-><init>(Lcom/samsung/musicplus/dialog/DataCheckDialog;)V

    invoke-virtual {v8, v9, v10}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v8

    const v9, 0x7f100114

    new-instance v10, Lcom/samsung/musicplus/dialog/DataCheckDialog$1;

    invoke-direct {v10, p0}, Lcom/samsung/musicplus/dialog/DataCheckDialog$1;-><init>(Lcom/samsung/musicplus/dialog/DataCheckDialog;)V

    invoke-virtual {v8, v9, v10}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 232
    .local v2, "builder":Landroid/app/AlertDialog$Builder;
    const-string v8, "layout_inflater"

    invoke-virtual {v0, v8}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/LayoutInflater;

    .line 234
    .local v3, "inflater":Landroid/view/LayoutInflater;
    const v8, 0x7f040005

    invoke-virtual {v3, v8, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 235
    .local v4, "mainView":Landroid/view/View;
    invoke-virtual {v2, v4}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 237
    const v7, 0x7f0d004b

    invoke-virtual {v4, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 238
    .local v1, "bodyText":Landroid/widget/TextView;
    invoke-virtual {p0, v5}, Lcom/samsung/musicplus/dialog/DataCheckDialog;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 240
    const v7, 0x7f0d004c

    invoke-virtual {v4, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/CheckBox;

    iput-object v7, p0, Lcom/samsung/musicplus/dialog/DataCheckDialog;->mCheckBox:Landroid/widget/CheckBox;

    .line 241
    iget-object v7, p0, Lcom/samsung/musicplus/dialog/DataCheckDialog;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v7, v11}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 242
    iget-object v7, p0, Lcom/samsung/musicplus/dialog/DataCheckDialog;->mCheckBox:Landroid/widget/CheckBox;

    new-instance v8, Lcom/samsung/musicplus/dialog/DataCheckDialog$3;

    invoke-direct {v8, p0}, Lcom/samsung/musicplus/dialog/DataCheckDialog$3;-><init>(Lcom/samsung/musicplus/dialog/DataCheckDialog;)V

    invoke-virtual {v7, v8}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 248
    iget-object v7, p0, Lcom/samsung/musicplus/dialog/DataCheckDialog;->mCheckBox:Landroid/widget/CheckBox;

    new-instance v8, Lcom/samsung/musicplus/dialog/DataCheckDialog$4;

    invoke-direct {v8, p0}, Lcom/samsung/musicplus/dialog/DataCheckDialog$4;-><init>(Lcom/samsung/musicplus/dialog/DataCheckDialog;)V

    invoke-virtual {v7, v8}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 254
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v7

    .end local v0    # "a":Landroid/app/Activity;
    .end local v1    # "bodyText":Landroid/widget/TextView;
    .end local v2    # "builder":Landroid/app/AlertDialog$Builder;
    .end local v3    # "inflater":Landroid/view/LayoutInflater;
    .end local v4    # "mainView":Landroid/view/View;
    :cond_1
    return-object v7

    .line 187
    :cond_2
    iget v8, p0, Lcom/samsung/musicplus/dialog/DataCheckDialog;->mNetworkStatus:I

    const/4 v9, 0x2

    if-ne v8, v9, :cond_1

    .line 188
    const v5, 0x7f10004b

    .line 189
    const v6, 0x7f10004c

    goto :goto_0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 165
    const-string v0, "prefKey"

    iget-object v1, p0, Lcom/samsung/musicplus/dialog/DataCheckDialog;->mPrefKey:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    const-string v0, "keyWord"

    iget-object v1, p0, Lcom/samsung/musicplus/dialog/DataCheckDialog;->mKeyWord:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    const-string v0, "networkStatus"

    iget v1, p0, Lcom/samsung/musicplus/dialog/DataCheckDialog;->mNetworkStatus:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 168
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 169
    return-void
.end method
