.class public Lcom/samsung/musicplus/dialog/LowBatteryPopup;
.super Landroid/app/Activity;
.source "LowBatteryPopup.java"


# static fields
.field private static final CLASSNAME:Ljava/lang/String;


# instance fields
.field private mBatteryMessageBox:Landroid/app/AlertDialog;

.field private final mSystemReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/samsung/musicplus/dialog/LowBatteryPopup;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/dialog/LowBatteryPopup;->CLASSNAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 41
    new-instance v0, Lcom/samsung/musicplus/dialog/LowBatteryPopup$1;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/dialog/LowBatteryPopup$1;-><init>(Lcom/samsung/musicplus/dialog/LowBatteryPopup;)V

    iput-object v0, p0, Lcom/samsung/musicplus/dialog/LowBatteryPopup;->mSystemReceiver:Landroid/content/BroadcastReceiver;

    .line 62
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/musicplus/dialog/LowBatteryPopup;->mBatteryMessageBox:Landroid/app/AlertDialog;

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/samsung/musicplus/dialog/LowBatteryPopup;->CLASSNAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/musicplus/dialog/LowBatteryPopup;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/LowBatteryPopup;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/LowBatteryPopup;->mBatteryMessageBox:Landroid/app/AlertDialog;

    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 26
    invoke-virtual {p0, p0}, Lcom/samsung/musicplus/dialog/LowBatteryPopup;->showLowBatteryMsg(Landroid/content/Context;)V

    .line 27
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 28
    const/4 v1, 0x3

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/dialog/LowBatteryPopup;->setVolumeControlStream(I)V

    .line 30
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 31
    .local v0, "f":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 32
    iget-object v1, p0, Lcom/samsung/musicplus/dialog/LowBatteryPopup;->mSystemReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/samsung/musicplus/dialog/LowBatteryPopup;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 33
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/LowBatteryPopup;->mSystemReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/dialog/LowBatteryPopup;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 38
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 39
    return-void
.end method

.method public showLowBatteryMsg(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 65
    sget-object v1, Lcom/samsung/musicplus/dialog/LowBatteryPopup;->CLASSNAME:Ljava/lang/String;

    const-string v2, "showLowBatteryMsg() is called"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 66
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 67
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    new-instance v1, Lcom/samsung/musicplus/dialog/LowBatteryPopup$2;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/dialog/LowBatteryPopup$2;-><init>(Lcom/samsung/musicplus/dialog/LowBatteryPopup;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 77
    new-instance v1, Lcom/samsung/musicplus/dialog/LowBatteryPopup$3;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/dialog/LowBatteryPopup$3;-><init>(Lcom/samsung/musicplus/dialog/LowBatteryPopup;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 85
    const v1, 0x7f1001c8

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 86
    const v1, 0x7f1000a2

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f100114

    new-instance v3, Lcom/samsung/musicplus/dialog/LowBatteryPopup$4;

    invoke-direct {v3, p0}, Lcom/samsung/musicplus/dialog/LowBatteryPopup$4;-><init>(Lcom/samsung/musicplus/dialog/LowBatteryPopup;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 94
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/musicplus/dialog/LowBatteryPopup;->mBatteryMessageBox:Landroid/app/AlertDialog;

    .line 95
    iget-object v1, p0, Lcom/samsung/musicplus/dialog/LowBatteryPopup;->mBatteryMessageBox:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 96
    return-void
.end method
