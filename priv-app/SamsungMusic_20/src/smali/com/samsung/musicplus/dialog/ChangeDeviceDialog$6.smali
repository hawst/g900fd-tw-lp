.class Lcom/samsung/musicplus/dialog/ChangeDeviceDialog$6;
.super Landroid/content/BroadcastReceiver;
.source "ChangeDeviceDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;)V
    .locals 0

    .prologue
    .line 667
    iput-object p1, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog$6;->this$0:Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 670
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 671
    .local v0, "action":Ljava/lang/String;
    const-string v2, "android.intent.action.HDMI_PLUGGED"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 672
    const-string v2, "state"

    const/4 v3, 0x0

    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 673
    .local v1, "isHdmiConnected":Z
    # getter for: Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->access$000()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mHdmiReceiver() - mIsHdmiConnected: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 674
    if-eqz v1, :cond_0

    .line 675
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog$6;->this$0:Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;

    # getter for: Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mAlertDialog:Landroid/app/AlertDialog;
    invoke-static {v2}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->access$400(Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;)Landroid/app/AlertDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog;->dismiss()V

    .line 677
    .end local v1    # "isHdmiConnected":Z
    :cond_0
    return-void
.end method
