.class Lcom/samsung/musicplus/dialog/EditTitleDialog$8;
.super Ljava/lang/Object;
.source "EditTitleDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnShowListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/dialog/EditTitleDialog;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/dialog/EditTitleDialog;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/dialog/EditTitleDialog;)V
    .locals 0

    .prologue
    .line 345
    iput-object p1, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog$8;->this$0:Lcom/samsung/musicplus/dialog/EditTitleDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onShow(Landroid/content/DialogInterface;)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 348
    iget-object v1, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog$8;->this$0:Lcom/samsung/musicplus/dialog/EditTitleDialog;

    # getter for: Lcom/samsung/musicplus/dialog/EditTitleDialog;->mDialog:Landroid/app/AlertDialog;
    invoke-static {v1}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->access$400(Lcom/samsung/musicplus/dialog/EditTitleDialog;)Landroid/app/AlertDialog;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    .line 349
    .local v0, "b":Landroid/widget/Button;
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog$8;->this$0:Lcom/samsung/musicplus/dialog/EditTitleDialog;

    # getter for: Lcom/samsung/musicplus/dialog/EditTitleDialog;->mDefaultName:Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->access$300(Lcom/samsung/musicplus/dialog/EditTitleDialog;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 350
    iget-object v1, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog$8;->this$0:Lcom/samsung/musicplus/dialog/EditTitleDialog;

    # getter for: Lcom/samsung/musicplus/dialog/EditTitleDialog;->mEditText:Landroid/widget/EditText;
    invoke-static {v1}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->access$600(Lcom/samsung/musicplus/dialog/EditTitleDialog;)Landroid/widget/EditText;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog$8;->this$0:Lcom/samsung/musicplus/dialog/EditTitleDialog;

    # getter for: Lcom/samsung/musicplus/dialog/EditTitleDialog;->mDefaultName:Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->access$300(Lcom/samsung/musicplus/dialog/EditTitleDialog;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog$8;->this$0:Lcom/samsung/musicplus/dialog/EditTitleDialog;

    # getter for: Lcom/samsung/musicplus/dialog/EditTitleDialog;->mEditText:Landroid/widget/EditText;
    invoke-static {v2}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->access$600(Lcom/samsung/musicplus/dialog/EditTitleDialog;)Landroid/widget/EditText;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 351
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 354
    :cond_0
    return-void
.end method
