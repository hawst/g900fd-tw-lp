.class public Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog;
.super Landroid/app/DialogFragment;
.source "DlnaDmsDetailInfoDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog$SimpleAdapterTheme;
    }
.end annotation


# static fields
.field private static final DETAIL_INFO:Ljava/lang/String; = "detail_info"

.field private static final DETAIL_TYPE:Ljava/lang/String; = "detail_type"

.field private static final KEY_DURATION:Ljava/lang/String; = "duration"

.field private static final KEY_LIST_TYPE:Ljava/lang/String; = "list"

.field private static final KEY_THEME:Ljava/lang/String; = "theme"

.field private static final KEY_URI:Ljava/lang/String; = "uri"


# instance fields
.field private mAdapter:Landroid/widget/SimpleAdapter;

.field private mDuration:Ljava/lang/String;

.field private mListType:I

.field private mUri:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 51
    return-void
.end method

.method private addItem(Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p2, "detailType"    # Ljava/lang/String;
    .param p3, "detailInfo"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 295
    .local p1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;>;"
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 296
    .local v0, "temp":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;"
    const-string v1, ""

    invoke-virtual {v1, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 297
    const v1, 0x7f1001bb

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog;->getString(I)Ljava/lang/String;

    move-result-object p3

    .line 299
    :cond_0
    const-string v1, "detail_type"

    invoke-virtual {v0, v1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 300
    const-string v1, "detail_info"

    invoke-virtual {v0, v1, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 301
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 302
    return-void
.end method

.method private getAllShareDetalInfo()Ljava/util/ArrayList;
    .locals 9
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;>;"
        }
    .end annotation

    .prologue
    const v8, 0x7f100178

    const v7, 0x7f100177

    const v6, 0x7f10008f

    const v5, 0x7f10008c

    .line 183
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 185
    .local v0, "context":Landroid/content/Context;
    iget v3, p0, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog;->mListType:I

    iget-object v4, p0, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog;->mUri:Ljava/lang/String;

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-static {v0, v3, v4}, Lcom/samsung/musicplus/util/ListUtils;->getMediaInfo(Landroid/content/Context;ILandroid/net/Uri;)Lcom/samsung/musicplus/util/ListUtils$MediaInfo;

    move-result-object v1

    .line 187
    .local v1, "info":Lcom/samsung/musicplus/util/ListUtils$MediaInfo;
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 188
    .local v2, "simpleInfo":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/String;>;>;"
    if-nez v1, :cond_1

    .line 189
    const-string v3, "MusicSimpleInfo"

    const-string v4, "There are no information about current song."

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 215
    :cond_0
    :goto_0
    return-object v2

    .line 193
    :cond_1
    invoke-direct {p0, v0, v1}, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog;->setDurationInfo(Landroid/content/Context;Lcom/samsung/musicplus/util/ListUtils$MediaInfo;)V

    .line 195
    iget v3, p0, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog;->mListType:I

    const v4, 0x2000e

    if-eq v3, v4, :cond_2

    iget v3, p0, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog;->mListType:I

    const v4, 0x2000f

    if-ne v3, v4, :cond_3

    .line 197
    :cond_2
    const v3, 0x7f100026

    invoke-virtual {p0, v3}, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v0, v1}, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog;->setArtistInfo(Landroid/content/Context;Lcom/samsung/musicplus/util/ListUtils$MediaInfo;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v2, v3, v4}, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog;->addItem(Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    invoke-virtual {p0, v8}, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v0, v1}, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog;->setTitleInfo(Landroid/content/Context;Lcom/samsung/musicplus/util/ListUtils$MediaInfo;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v2, v3, v4}, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog;->addItem(Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    const v3, 0x7f10001e

    invoke-virtual {p0, v3}, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v0, v1}, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog;->setAlbumInfo(Landroid/content/Context;Lcom/samsung/musicplus/util/ListUtils$MediaInfo;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v2, v3, v4}, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog;->addItem(Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)V

    .line 200
    invoke-virtual {p0, v7}, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog;->mDuration:Ljava/lang/String;

    invoke-direct {p0, v2, v3, v4}, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog;->addItem(Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    const v3, 0x7f100090

    invoke-virtual {p0, v3}, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v0, v1}, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog;->setGenreInfo(Landroid/content/Context;Lcom/samsung/musicplus/util/ListUtils$MediaInfo;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v2, v3, v4}, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog;->addItem(Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    invoke-virtual {p0, v6}, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v0, v1}, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog;->setMimeTypeInfo(Landroid/content/Context;Lcom/samsung/musicplus/util/ListUtils$MediaInfo;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v2, v3, v4}, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog;->addItem(Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    invoke-virtual {p0, v5}, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v0, v1}, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog;->setFileSizeInfo(Landroid/content/Context;Lcom/samsung/musicplus/util/ListUtils$MediaInfo;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v2, v3, v4}, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog;->addItem(Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 206
    :cond_3
    invoke-virtual {p0, v8}, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v0, v1}, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog;->setTitleInfo(Landroid/content/Context;Lcom/samsung/musicplus/util/ListUtils$MediaInfo;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v2, v3, v4}, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog;->addItem(Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    invoke-virtual {p0, v7}, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog;->mDuration:Ljava/lang/String;

    invoke-direct {p0, v2, v3, v4}, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog;->addItem(Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    invoke-virtual {p0, v6}, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v0, v1}, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog;->setMimeTypeInfo(Landroid/content/Context;Lcom/samsung/musicplus/util/ListUtils$MediaInfo;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v2, v3, v4}, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog;->addItem(Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    iget v3, p0, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog;->mListType:I

    const v4, 0x2000b

    if-ne v3, v4, :cond_0

    .line 211
    invoke-virtual {p0, v5}, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v0, v1}, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog;->setFileSizeInfo(Landroid/content/Context;Lcom/samsung/musicplus/util/ListUtils$MediaInfo;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v2, v3, v4}, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog;->addItem(Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    const v3, 0x7f10011f

    invoke-virtual {p0, v3}, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v0, v1}, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog;->setDmsDeviceInfo(Landroid/content/Context;Lcom/samsung/musicplus/util/ListUtils$MediaInfo;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v2, v3, v4}, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog;->addItem(Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public static getInstance(Ljava/lang/String;I)Landroid/app/DialogFragment;
    .locals 1
    .param p0, "uri"    # Ljava/lang/String;
    .param p1, "listType"    # I

    .prologue
    .line 61
    invoke-static {}, Lcom/samsung/musicplus/util/UiUtils;->getTheme()I

    move-result v0

    invoke-static {p0, p1, v0}, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog;->getInstance(Ljava/lang/String;II)Landroid/app/DialogFragment;

    move-result-object v0

    return-object v0
.end method

.method public static getInstance(Ljava/lang/String;II)Landroid/app/DialogFragment;
    .locals 3
    .param p0, "uri"    # Ljava/lang/String;
    .param p1, "listType"    # I
    .param p2, "theme"    # I

    .prologue
    .line 65
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 66
    .local v0, "data":Landroid/os/Bundle;
    const-string v2, "uri"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 67
    const-string v2, "list"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 68
    const-string v2, "theme"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 69
    new-instance v1, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog;

    invoke-direct {v1}, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog;-><init>()V

    .line 70
    .local v1, "fg":Landroid/app/DialogFragment;
    invoke-virtual {v1, v0}, Landroid/app/DialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 71
    return-object v1
.end method

.method private setAlbumInfo(Landroid/content/Context;Lcom/samsung/musicplus/util/ListUtils$MediaInfo;)Ljava/lang/String;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "info"    # Lcom/samsung/musicplus/util/ListUtils$MediaInfo;

    .prologue
    .line 280
    iget-object v0, p2, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->album:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 281
    iget-object v0, p2, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->album:Ljava/lang/String;

    .line 283
    :goto_0
    return-object v0

    :cond_0
    const v0, 0x7f1001bb

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private setArtistInfo(Landroid/content/Context;Lcom/samsung/musicplus/util/ListUtils$MediaInfo;)Ljava/lang/String;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "info"    # Lcom/samsung/musicplus/util/ListUtils$MediaInfo;

    .prologue
    .line 287
    iget-object v0, p2, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->artist:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 288
    iget-object v0, p2, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->artist:Ljava/lang/String;

    .line 290
    :goto_0
    return-object v0

    :cond_0
    const v0, 0x7f1001bb

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private setDmsDeviceInfo(Landroid/content/Context;Lcom/samsung/musicplus/util/ListUtils$MediaInfo;)Ljava/lang/String;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "info"    # Lcom/samsung/musicplus/util/ListUtils$MediaInfo;

    .prologue
    .line 219
    iget-object v0, p2, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->dmsName:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 220
    iget-object v0, p2, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->dmsName:Ljava/lang/String;

    .line 222
    :goto_0
    return-object v0

    :cond_0
    const v0, 0x7f1001bb

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private setDurationInfo(Landroid/content/Context;Lcom/samsung/musicplus/util/ListUtils$MediaInfo;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "info"    # Lcom/samsung/musicplus/util/ListUtils$MediaInfo;

    .prologue
    const-wide/16 v4, 0x3e8

    const-wide/16 v2, 0x0

    .line 255
    iget-wide v0, p2, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->duration:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 256
    iget-wide v0, p2, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->duration:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    div-long/2addr v0, v4

    invoke-static {p1, v0, v1}, Lcom/samsung/musicplus/util/UiUtils;->makeTimeString(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog;->mDuration:Ljava/lang/String;

    .line 270
    :cond_0
    :goto_0
    return-void

    .line 259
    :cond_1
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getDuration()J

    move-result-wide v0

    iput-wide v0, p2, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->duration:J

    .line 260
    iget-wide v0, p2, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->duration:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    .line 261
    iget-wide v0, p2, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->duration:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    div-long/2addr v0, v4

    invoke-static {p1, v0, v1}, Lcom/samsung/musicplus/util/UiUtils;->makeTimeString(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog;->mDuration:Ljava/lang/String;

    goto :goto_0

    .line 265
    :cond_2
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog;->mDuration:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 266
    const v0, 0x7f1001bb

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog;->mDuration:Ljava/lang/String;

    goto :goto_0
.end method

.method private setFileSizeInfo(Landroid/content/Context;Lcom/samsung/musicplus/util/ListUtils$MediaInfo;)Ljava/lang/String;
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "info"    # Lcom/samsung/musicplus/util/ListUtils$MediaInfo;

    .prologue
    const-wide/high16 v8, 0x3ff0000000000000L    # 1.0

    .line 226
    iget-wide v4, p2, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->fileSize:J

    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-lez v1, :cond_2

    .line 227
    new-instance v0, Ljava/text/DecimalFormat;

    const-string v1, "0.##"

    invoke-direct {v0, v1}, Ljava/text/DecimalFormat;-><init>(Ljava/lang/String;)V

    .line 228
    .local v0, "df":Ljava/text/DecimalFormat;
    iget-wide v4, p2, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->fileSize:J

    long-to-double v2, v4

    .line 229
    .local v2, "result":D
    iget-wide v4, p2, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->fileSize:J

    long-to-double v4, v4

    const-wide/high16 v6, 0x4130000000000000L    # 1048576.0

    div-double v2, v4, v6

    cmpl-double v1, v2, v8

    if-ltz v1, :cond_0

    .line 230
    invoke-virtual {v0, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    const-string v4, "MB"

    invoke-virtual {v1, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 237
    .end local v0    # "df":Ljava/text/DecimalFormat;
    .end local v2    # "result":D
    :goto_0
    return-object v1

    .line 231
    .restart local v0    # "df":Ljava/text/DecimalFormat;
    .restart local v2    # "result":D
    :cond_0
    iget-wide v4, p2, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->fileSize:J

    long-to-double v4, v4

    const-wide/high16 v6, 0x4090000000000000L    # 1024.0

    div-double v2, v4, v6

    cmpl-double v1, v2, v8

    if-ltz v1, :cond_1

    .line 232
    invoke-virtual {v0, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    const-string v4, "KB"

    invoke-virtual {v1, v4}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 234
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, "Bytes"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 237
    .end local v0    # "df":Ljava/text/DecimalFormat;
    .end local v2    # "result":D
    :cond_2
    const v1, 0x7f1001bb

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private setGenreInfo(Landroid/content/Context;Lcom/samsung/musicplus/util/ListUtils$MediaInfo;)Ljava/lang/String;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "info"    # Lcom/samsung/musicplus/util/ListUtils$MediaInfo;

    .prologue
    .line 248
    iget-object v0, p2, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->genre:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 249
    iget-object v0, p2, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->genre:Ljava/lang/String;

    .line 251
    :goto_0
    return-object v0

    :cond_0
    const v0, 0x7f1001bb

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private setMimeTypeInfo(Landroid/content/Context;Lcom/samsung/musicplus/util/ListUtils$MediaInfo;)Ljava/lang/String;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "info"    # Lcom/samsung/musicplus/util/ListUtils$MediaInfo;

    .prologue
    .line 241
    iget-object v0, p2, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->mimeType:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 242
    iget-object v0, p2, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->mimeType:Ljava/lang/String;

    .line 244
    :goto_0
    return-object v0

    :cond_0
    const v0, 0x7f1001bb

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private setTitleInfo(Landroid/content/Context;Lcom/samsung/musicplus/util/ListUtils$MediaInfo;)Ljava/lang/String;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "info"    # Lcom/samsung/musicplus/util/ListUtils$MediaInfo;

    .prologue
    .line 273
    iget-object v0, p2, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->title:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 274
    iget-object v0, p2, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->title:Ljava/lang/String;

    .line 276
    :goto_0
    return-object v0

    :cond_0
    const v0, 0x7f1001bb

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 76
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 77
    .local v0, "data":Landroid/os/Bundle;
    const-string v1, "list"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog;->mListType:I

    .line 78
    const-string v1, "uri"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog;->mUri:Ljava/lang/String;

    .line 79
    if-eqz p1, :cond_0

    .line 80
    const-string v1, "duration"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog;->mDuration:Ljava/lang/String;

    .line 82
    :cond_0
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 83
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 10
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v9, 0x2

    .line 93
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v7

    .line 94
    .local v7, "data":Landroid/os/Bundle;
    const-string v0, "theme"

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    .line 95
    .local v6, "theme":I
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog;->getActivity()Landroid/app/Activity;

    move-result-object v1

    .line 96
    .local v1, "a":Landroid/app/Activity;
    new-instance v0, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog$SimpleAdapterTheme;

    invoke-direct {p0}, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog;->getAllShareDetalInfo()Ljava/util/ArrayList;

    move-result-object v2

    const v3, 0x7f04005e

    new-array v4, v9, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v8, "detail_type"

    aput-object v8, v4, v5

    const/4 v5, 0x1

    const-string v8, "detail_info"

    aput-object v8, v4, v5

    new-array v5, v9, [I

    fill-array-data v5, :array_0

    invoke-direct/range {v0 .. v6}, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog$SimpleAdapterTheme;-><init>(Landroid/content/Context;Ljava/util/List;I[Ljava/lang/String;[II)V

    iput-object v0, p0, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog;->mAdapter:Landroid/widget/SimpleAdapter;

    .line 102
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f1000ae

    invoke-virtual {p0, v2}, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget-object v2, p0, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog;->mAdapter:Landroid/widget/SimpleAdapter;

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setAdapter(Landroid/widget/ListAdapter;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x7f100114

    invoke-virtual {p0, v2}, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    new-instance v3, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog$1;

    invoke-direct {v3, p0}, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog$1;-><init>(Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0

    .line 96
    :array_0
    .array-data 4
        0x7f0d0114
        0x7f0d0115
    .end array-data
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 87
    const-string v0, "duration"

    iget-object v1, p0, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog;->mDuration:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 88
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 89
    return-void
.end method
