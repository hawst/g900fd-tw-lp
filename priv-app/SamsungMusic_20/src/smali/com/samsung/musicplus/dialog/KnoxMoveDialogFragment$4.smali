.class Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment$4;
.super Ljava/lang/Object;
.source "KnoxMoveDialogFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->onFailed()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;)V
    .locals 0

    .prologue
    .line 294
    iput-object p1, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment$4;->this$0:Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 298
    iget-object v1, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment$4;->this$0:Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;

    # getter for: Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mKnoxMoveOperation:Lcom/samsung/musicplus/contents/MoveKnoxPersonalModeOperation;
    invoke-static {v1}, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->access$000(Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;)Lcom/samsung/musicplus/contents/MoveKnoxPersonalModeOperation;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/musicplus/contents/MoveKnoxPersonalModeOperation;->getErrorMsgKnoxV2()Ljava/lang/String;

    move-result-object v0

    .line 299
    .local v0, "error":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v1, ""

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 300
    iget-object v1, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment$4;->this$0:Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;

    # invokes: Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->showToast(Ljava/lang/String;)V
    invoke-static {v1, v0}, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->access$200(Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;Ljava/lang/String;)V

    .line 304
    :goto_0
    iget-object v1, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment$4;->this$0:Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;

    # invokes: Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->endOperation()V
    invoke-static {v1}, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->access$100(Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;)V

    .line 305
    return-void

    .line 302
    :cond_0
    iget-object v1, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment$4;->this$0:Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;

    iget-object v2, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment$4;->this$0:Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;

    # getter for: Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->access$600(Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;)Landroid/content/Context;

    move-result-object v2

    const v3, 0x7f100089

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->showToast(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->access$200(Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;Ljava/lang/String;)V

    goto :goto_0
.end method
