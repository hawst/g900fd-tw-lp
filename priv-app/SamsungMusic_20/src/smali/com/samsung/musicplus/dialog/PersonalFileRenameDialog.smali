.class public Lcom/samsung/musicplus/dialog/PersonalFileRenameDialog;
.super Landroid/app/DialogFragment;
.source "PersonalFileRenameDialog.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "PersonalFileOperation"

.field private static final TARGET_PATH:Ljava/lang/String; = "target_path"


# instance fields
.field private mAllAppliedCheck:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 43
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/dialog/PersonalFileRenameDialog;->mAllAppliedCheck:Z

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/dialog/PersonalFileRenameDialog;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/PersonalFileRenameDialog;

    .prologue
    .line 37
    iget-boolean v0, p0, Lcom/samsung/musicplus/dialog/PersonalFileRenameDialog;->mAllAppliedCheck:Z

    return v0
.end method

.method static synthetic access$002(Lcom/samsung/musicplus/dialog/PersonalFileRenameDialog;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/PersonalFileRenameDialog;
    .param p1, "x1"    # Z

    .prologue
    .line 37
    iput-boolean p1, p0, Lcom/samsung/musicplus/dialog/PersonalFileRenameDialog;->mAllAppliedCheck:Z

    return p1
.end method

.method private getFileName(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "targetPath"    # Ljava/lang/String;

    .prologue
    .line 132
    if-nez p1, :cond_1

    .line 133
    const/4 v0, 0x0

    .line 146
    :cond_0
    :goto_0
    return-object v0

    .line 136
    :cond_1
    move-object v0, p1

    .line 138
    .local v0, "fileName":Ljava/lang/String;
    const/16 v2, 0x2f

    invoke-virtual {p1, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    .line 139
    .local v1, "pos":I
    if-ltz v1, :cond_2

    .line 140
    add-int/lit8 v2, v1, 0x1

    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    .line 142
    :cond_2
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_0

    .line 143
    move-object v0, p1

    goto :goto_0
.end method

.method public static getInstance(Ljava/lang/String;)Landroid/app/DialogFragment;
    .locals 3
    .param p0, "targetPath"    # Ljava/lang/String;

    .prologue
    .line 46
    new-instance v1, Lcom/samsung/musicplus/dialog/PersonalFileRenameDialog;

    invoke-direct {v1}, Lcom/samsung/musicplus/dialog/PersonalFileRenameDialog;-><init>()V

    .line 47
    .local v1, "renameDialog":Lcom/samsung/musicplus/dialog/PersonalFileRenameDialog;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 49
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "target_path"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    invoke-virtual {v1, v0}, Lcom/samsung/musicplus/dialog/PersonalFileRenameDialog;->setArguments(Landroid/os/Bundle;)V

    .line 52
    return-object v1
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 15
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 57
    const-string v12, "PersonalFileOperation"

    const-string v13, "PersonalFileRenameDialog, onCreate()"

    invoke-static {v12, v13}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 59
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/PersonalFileRenameDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v4

    .line 60
    .local v4, "data":Landroid/os/Bundle;
    const-string v12, "target_path"

    invoke-virtual {v4, v12}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 62
    .local v10, "targetPath":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/PersonalFileRenameDialog;->getActivity()Landroid/app/Activity;

    move-result-object v12

    invoke-virtual {v12}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 63
    .local v2, "context":Landroid/content/Context;
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/PersonalFileRenameDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    .line 64
    .local v9, "res":Landroid/content/res/Resources;
    invoke-static {v2}, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->getInstance(Landroid/content/Context;)Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;

    move-result-object v7

    .line 67
    .local v7, "mPersonalOperation":Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/PersonalFileRenameDialog;->getActivity()Landroid/app/Activity;

    move-result-object v12

    invoke-direct {v0, v12}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 68
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v6

    .line 69
    .local v6, "inflater":Landroid/view/LayoutInflater;
    const v12, 0x7f040005

    const/4 v13, 0x0

    invoke-virtual {v6, v12, v13}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 70
    .local v3, "customDialog":Landroid/view/View;
    const v12, 0x7f10008b

    invoke-virtual {v9, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 71
    .local v11, "title":Ljava/lang/String;
    invoke-direct {p0, v10}, Lcom/samsung/musicplus/dialog/PersonalFileRenameDialog;->getFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 72
    .local v5, "fileName":Ljava/lang/String;
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 73
    invoke-virtual {v0, v11}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 75
    const v12, 0x7f0d004c

    invoke-virtual {v3, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    .line 76
    .local v1, "checkBox":Landroid/widget/CheckBox;
    new-instance v12, Lcom/samsung/musicplus/dialog/PersonalFileRenameDialog$1;

    invoke-direct {v12, p0, v1}, Lcom/samsung/musicplus/dialog/PersonalFileRenameDialog$1;-><init>(Lcom/samsung/musicplus/dialog/PersonalFileRenameDialog;Landroid/widget/CheckBox;)V

    invoke-virtual {v1, v12}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 86
    const v12, 0x7f100025

    invoke-virtual {v1, v12}, Landroid/widget/CheckBox;->setText(I)V

    .line 88
    const v12, 0x7f0d004b

    invoke-virtual {v3, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/TextView;

    .line 89
    .local v8, "mainText":Landroid/widget/TextView;
    const v12, 0x7f100020

    invoke-virtual {v9, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x1

    new-array v13, v13, [Ljava/lang/Object;

    const/4 v14, 0x0

    aput-object v5, v13, v14

    invoke-static {v12, v13}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v8, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 91
    const v12, 0x7f100139

    invoke-virtual {v9, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    new-instance v13, Lcom/samsung/musicplus/dialog/PersonalFileRenameDialog$2;

    invoke-direct {v13, p0, v7}, Lcom/samsung/musicplus/dialog/PersonalFileRenameDialog$2;-><init>(Lcom/samsung/musicplus/dialog/PersonalFileRenameDialog;Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;)V

    invoke-virtual {v0, v12, v13}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 103
    const v12, 0x7f10013a

    invoke-virtual {v9, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    new-instance v13, Lcom/samsung/musicplus/dialog/PersonalFileRenameDialog$3;

    invoke-direct {v13, p0, v7}, Lcom/samsung/musicplus/dialog/PersonalFileRenameDialog$3;-><init>(Lcom/samsung/musicplus/dialog/PersonalFileRenameDialog;Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;)V

    invoke-virtual {v0, v12, v13}, Landroid/app/AlertDialog$Builder;->setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 115
    const v12, 0x7f10003a

    invoke-virtual {v9, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    new-instance v13, Lcom/samsung/musicplus/dialog/PersonalFileRenameDialog$4;

    invoke-direct {v13, p0, v7}, Lcom/samsung/musicplus/dialog/PersonalFileRenameDialog$4;-><init>(Lcom/samsung/musicplus/dialog/PersonalFileRenameDialog;Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;)V

    invoke-virtual {v0, v12, v13}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 128
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v12

    return-object v12
.end method
