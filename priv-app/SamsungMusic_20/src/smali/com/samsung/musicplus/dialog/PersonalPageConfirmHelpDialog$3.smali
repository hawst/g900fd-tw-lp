.class Lcom/samsung/musicplus/dialog/PersonalPageConfirmHelpDialog$3;
.super Ljava/lang/Object;
.source "PersonalPageConfirmHelpDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/dialog/PersonalPageConfirmHelpDialog;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/dialog/PersonalPageConfirmHelpDialog;

.field final synthetic val$a:Landroid/app/Activity;

.field final synthetic val$checkBox:Landroid/widget/CheckBox;

.field final synthetic val$isMoving:Z

.field final synthetic val$listItems:[J

.field final synthetic val$removeFromNowPlaying:Z


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/dialog/PersonalPageConfirmHelpDialog;Landroid/widget/CheckBox;ZLandroid/app/Activity;[JZ)V
    .locals 0

    .prologue
    .line 96
    iput-object p1, p0, Lcom/samsung/musicplus/dialog/PersonalPageConfirmHelpDialog$3;->this$0:Lcom/samsung/musicplus/dialog/PersonalPageConfirmHelpDialog;

    iput-object p2, p0, Lcom/samsung/musicplus/dialog/PersonalPageConfirmHelpDialog$3;->val$checkBox:Landroid/widget/CheckBox;

    iput-boolean p3, p0, Lcom/samsung/musicplus/dialog/PersonalPageConfirmHelpDialog$3;->val$isMoving:Z

    iput-object p4, p0, Lcom/samsung/musicplus/dialog/PersonalPageConfirmHelpDialog$3;->val$a:Landroid/app/Activity;

    iput-object p5, p0, Lcom/samsung/musicplus/dialog/PersonalPageConfirmHelpDialog$3;->val$listItems:[J

    iput-boolean p6, p0, Lcom/samsung/musicplus/dialog/PersonalPageConfirmHelpDialog$3;->val$removeFromNowPlaying:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 7
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 99
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/PersonalPageConfirmHelpDialog$3;->val$checkBox:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 100
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/PersonalPageConfirmHelpDialog$3;->this$0:Lcom/samsung/musicplus/dialog/PersonalPageConfirmHelpDialog;

    invoke-virtual {v0}, Lcom/samsung/musicplus/dialog/PersonalPageConfirmHelpDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "music_player_pref"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v6

    .line 102
    .local v6, "sp":Landroid/content/SharedPreferences;
    invoke-interface {v6}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "personal_mode_help"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 105
    .end local v6    # "sp":Landroid/content/SharedPreferences;
    :cond_0
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 106
    iget-boolean v0, p0, Lcom/samsung/musicplus/dialog/PersonalPageConfirmHelpDialog$3;->val$isMoving:Z

    if-nez v0, :cond_1

    .line 107
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/PersonalPageConfirmHelpDialog$3;->val$a:Landroid/app/Activity;

    iget-object v1, p0, Lcom/samsung/musicplus/dialog/PersonalPageConfirmHelpDialog$3;->this$0:Lcom/samsung/musicplus/dialog/PersonalPageConfirmHelpDialog;

    # getter for: Lcom/samsung/musicplus/dialog/PersonalPageConfirmHelpDialog;->mListType:I
    invoke-static {v1}, Lcom/samsung/musicplus/dialog/PersonalPageConfirmHelpDialog;->access$000(Lcom/samsung/musicplus/dialog/PersonalPageConfirmHelpDialog;)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/musicplus/dialog/PersonalPageConfirmHelpDialog$3;->val$listItems:[J

    iget-boolean v3, p0, Lcom/samsung/musicplus/dialog/PersonalPageConfirmHelpDialog$3;->val$isMoving:Z

    iget-boolean v4, p0, Lcom/samsung/musicplus/dialog/PersonalPageConfirmHelpDialog$3;->val$removeFromNowPlaying:Z

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/musicplus/dialog/PersonalFolderSelectionDialog;->showPersonalFolderSelectionDialog(Landroid/app/Activity;I[JZZ)V

    .line 113
    :goto_0
    return-void

    .line 110
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/PersonalPageConfirmHelpDialog$3;->val$a:Landroid/app/Activity;

    iget-object v1, p0, Lcom/samsung/musicplus/dialog/PersonalPageConfirmHelpDialog$3;->this$0:Lcom/samsung/musicplus/dialog/PersonalPageConfirmHelpDialog;

    # getter for: Lcom/samsung/musicplus/dialog/PersonalPageConfirmHelpDialog;->mListType:I
    invoke-static {v1}, Lcom/samsung/musicplus/dialog/PersonalPageConfirmHelpDialog;->access$000(Lcom/samsung/musicplus/dialog/PersonalPageConfirmHelpDialog;)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/musicplus/dialog/PersonalPageConfirmHelpDialog$3;->val$listItems:[J

    iget-boolean v3, p0, Lcom/samsung/musicplus/dialog/PersonalPageConfirmHelpDialog$3;->val$isMoving:Z

    const/4 v4, 0x0

    iget-boolean v5, p0, Lcom/samsung/musicplus/dialog/PersonalPageConfirmHelpDialog$3;->val$removeFromNowPlaying:Z

    invoke-static/range {v0 .. v5}, Lcom/samsung/musicplus/dialog/PersonalMoveDialogFragment;->showPersonalMoveDialogFragment(Landroid/app/Activity;I[JZLjava/lang/String;Z)V

    goto :goto_0
.end method
