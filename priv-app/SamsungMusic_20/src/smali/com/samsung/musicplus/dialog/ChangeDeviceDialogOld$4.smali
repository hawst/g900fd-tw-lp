.class Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld$4;
.super Landroid/content/BroadcastReceiver;
.source "ChangeDeviceDialogOld.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;)V
    .locals 0

    .prologue
    .line 158
    iput-object p1, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld$4;->this$0:Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 161
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 162
    .local v0, "action":Ljava/lang/String;
    const-string v1, "android.intent.action.HDMI_PLUGGED"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 163
    iget-object v1, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld$4;->this$0:Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;

    const-string v2, "state"

    const/4 v3, 0x0

    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    # setter for: Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mIsHdmiConnected:Z
    invoke-static {v1, v2}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->access$702(Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;Z)Z

    .line 164
    # getter for: Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mHdmiReceiver() - mIsHdmiConnected: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld$4;->this$0:Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;

    # getter for: Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mIsHdmiConnected:Z
    invoke-static {v3}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->access$700(Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 165
    iget-object v1, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld$4;->this$0:Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;

    # getter for: Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mIsHdmiConnected:Z
    invoke-static {v1}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->access$700(Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 166
    # getter for: Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->sWifiP2pDeviceList:Ljava/util/List;
    invoke-static {}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->access$600()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_0

    # getter for: Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->sWifiP2pDeviceList:Ljava/util/List;
    invoke-static {}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->access$600()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 169
    # getter for: Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->sWifiP2pDeviceList:Ljava/util/List;
    invoke-static {}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->access$600()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->clear()V

    .line 170
    iget-object v1, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld$4;->this$0:Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;

    iget-object v2, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld$4;->this$0:Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;

    # getter for: Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mListView:Landroid/widget/ListView;
    invoke-static {v2}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->access$100(Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;)Landroid/widget/ListView;

    move-result-object v2

    # invokes: Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->setAdapter(Landroid/widget/ListView;)V
    invoke-static {v1, v2}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->access$200(Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;Landroid/widget/ListView;)V

    .line 176
    :cond_0
    :goto_0
    return-void

    .line 173
    :cond_1
    iget-object v1, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld$4;->this$0:Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;

    # invokes: Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->scanWifiDevices()V
    invoke-static {v1}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->access$800(Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;)V

    goto :goto_0
.end method
