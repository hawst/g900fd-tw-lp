.class Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog$ContainerListAdapater;
.super Landroid/widget/BaseAdapter;
.source "KnoxContainerSelectDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ContainerListAdapater"
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field final synthetic this$0:Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog;


# direct methods
.method public constructor <init>(Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog;Landroid/content/Context;Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager$PersonaInfoListCompat;)V
    .locals 4
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "list"    # Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager$PersonaInfoListCompat;

    .prologue
    .line 115
    iput-object p1, p0, Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog$ContainerListAdapater;->this$0:Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 116
    iput-object p2, p0, Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog$ContainerListAdapater;->mContext:Landroid/content/Context;

    .line 117
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    # setter for: Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog;->mArray:Ljava/util/ArrayList;
    invoke-static {p1, v3}, Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog;->access$002(Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 118
    invoke-static {p2}, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;->getKnoxContainerInfo(Landroid/content/Context;)Ljava/util/HashMap;

    move-result-object v2

    .line 120
    .local v2, "knoxIdNamePair":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/lang/String;>;"
    if-eqz v2, :cond_0

    .line 121
    invoke-virtual {v2}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 122
    .local v0, "idSet":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 123
    new-instance v1, Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog$ContainerInfo;

    invoke-direct {v1}, Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog$ContainerInfo;-><init>()V

    .line 124
    .local v1, "info":Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog$ContainerInfo;
    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iput v3, v1, Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog$ContainerInfo;->id:I

    .line 125
    iget v3, v1, Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog$ContainerInfo;->id:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    iput-object v3, v1, Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog$ContainerInfo;->name:Ljava/lang/String;

    .line 126
    # getter for: Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog;->mArray:Ljava/util/ArrayList;
    invoke-static {p1}, Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog;->access$000(Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 129
    .end local v0    # "idSet":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/lang/Integer;>;"
    .end local v1    # "info":Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog$ContainerInfo;
    :cond_0
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 161
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog$ContainerListAdapater;->this$0:Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog;

    # getter for: Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog;->mArray:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog;->access$000(Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 166
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog$ContainerListAdapater;->this$0:Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog;

    # getter for: Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog;->mArray:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog;->access$000(Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 171
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog$ContainerListAdapater;->this$0:Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog;

    # getter for: Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog;->mArray:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog;->access$000(Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog$ContainerInfo;

    iget v0, v0, Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog$ContainerInfo;->id:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 133
    if-nez p2, :cond_0

    .line 134
    iget-object v4, p0, Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog$ContainerListAdapater;->mContext:Landroid/content/Context;

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 135
    .local v2, "inflater":Landroid/view/LayoutInflater;
    const v4, 0x7f040086

    const/4 v5, 0x0

    invoke-virtual {v2, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 138
    .end local v2    # "inflater":Landroid/view/LayoutInflater;
    :cond_0
    const v4, 0x7f0d00bc

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 139
    .local v1, "icon":Landroid/widget/ImageView;
    const v4, 0x7f0d0192

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 140
    .local v3, "text":Landroid/widget/TextView;
    const/4 v0, 0x0

    .line 142
    .local v0, "containerName":Ljava/lang/String;
    if-eqz v3, :cond_1

    .line 143
    iget-object v4, p0, Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog$ContainerListAdapater;->this$0:Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog;

    # getter for: Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog;->mArray:Ljava/util/ArrayList;
    invoke-static {v4}, Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog;->access$000(Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog$ContainerInfo;

    iget-object v0, v4, Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog$ContainerInfo;->name:Ljava/lang/String;

    .line 144
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 147
    :cond_1
    if-eqz v1, :cond_2

    .line 148
    const-string v4, "KNOX"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 149
    # getter for: Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog;->mContainerIcon:[I
    invoke-static {}, Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog;->access$100()[I

    move-result-object v4

    const/4 v5, 0x0

    aget v4, v4, v5

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 156
    :cond_2
    :goto_0
    return-object p2

    .line 150
    :cond_3
    const-string v4, "KNOX II"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 151
    # getter for: Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog;->mContainerIcon:[I
    invoke-static {}, Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog;->access$100()[I

    move-result-object v4

    const/4 v5, 0x1

    aget v4, v4, v5

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 153
    :cond_4
    # getter for: Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog;->mContainerIcon:[I
    invoke-static {}, Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog;->access$100()[I

    move-result-object v4

    rem-int/lit8 v5, p1, 0x2

    aget v4, v4, v5

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method
