.class public Lcom/samsung/musicplus/dialog/PersonalFolderSelectionDialog;
.super Landroid/app/DialogFragment;
.source "PersonalFolderSelectionDialog.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/dialog/PersonalFolderSelectionDialog$FolderSelectionListAdapter;
    }
.end annotation


# static fields
.field private static final IS_MOVING:Ljava/lang/String; = "is_moving"

.field private static final LIST_ITEMS:Ljava/lang/String; = "list_items"

.field private static final LIST_TYPE:Ljava/lang/String; = "list_type"

.field private static final REMOVE_FROM_NOW_PLAYING:Ljava/lang/String; = "remove_from_now_playing"

.field private static final TAG:Ljava/lang/String; = "PersonalFileOperation"


# instance fields
.field private mAadapter:Lcom/samsung/musicplus/dialog/PersonalFolderSelectionDialog$FolderSelectionListAdapter;

.field private mAlertDialog:Landroid/app/AlertDialog;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 182
    return-void
.end method

.method private getFolderSubPath(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 153
    const-string v1, ""

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 154
    const-string v1, "/"

    invoke-virtual {p1, v1}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    .line 155
    .local v0, "end":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 156
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p1

    .line 159
    .end local v0    # "end":I
    :cond_0
    return-object p1
.end method

.method public static getInstance(I[JZZ)Landroid/app/DialogFragment;
    .locals 3
    .param p0, "listType"    # I
    .param p1, "list"    # [J
    .param p2, "moving"    # Z
    .param p3, "removeFromNowPlaying"    # Z

    .prologue
    .line 65
    new-instance v1, Lcom/samsung/musicplus/dialog/PersonalFolderSelectionDialog;

    invoke-direct {v1}, Lcom/samsung/musicplus/dialog/PersonalFolderSelectionDialog;-><init>()V

    .line 66
    .local v1, "selectionDialog":Lcom/samsung/musicplus/dialog/PersonalFolderSelectionDialog;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 68
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "list_items"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putLongArray(Ljava/lang/String;[J)V

    .line 69
    const-string v2, "is_moving"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 70
    const-string v2, "list_type"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 71
    const-string v2, "remove_from_now_playing"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 72
    invoke-virtual {v1, v0}, Lcom/samsung/musicplus/dialog/PersonalFolderSelectionDialog;->setArguments(Landroid/os/Bundle;)V

    .line 73
    return-object v1
.end method

.method private static getNormalContentsCount(Landroid/content/Context;)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 163
    const v1, 0x20001

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/samsung/musicplus/util/ListUtils;->getMusicListInfo(ILjava/lang/String;)Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;

    move-result-object v0

    .line 164
    .local v0, "info":Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;
    iget-object v1, v0, Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;->queryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    const-string v2, "is_music=1 AND is_secretbox=0"

    iput-object v2, v1, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->selection:Ljava/lang/String;

    .line 166
    iget-object v1, v0, Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;->queryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    invoke-static {p0, v1}, Lcom/samsung/musicplus/util/ListUtils;->getSongCountInList(Landroid/content/Context;Lcom/samsung/musicplus/util/ListUtils$QueryArgs;)I

    move-result v1

    return v1
.end method

.method private setAdapter(Landroid/app/Activity;Landroid/widget/ListView;)V
    .locals 11
    .param p1, "a"    # Landroid/app/Activity;
    .param p2, "lv"    # Landroid/widget/ListView;

    .prologue
    .line 96
    const v0, 0x10007

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/samsung/musicplus/util/ListUtils;->getMusicListInfo(ILjava/lang/String;)Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;

    move-result-object v10

    .line 97
    .local v10, "info":Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "_data NOT LIKE \'%"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {p1}, Lcom/samsung/musicplus/library/storage/PrivateMode;->getRootDir(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "%\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 99
    .local v3, "selection":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, v10, Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;->queryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    iget-object v1, v1, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->uri:Landroid/net/Uri;

    iget-object v2, v10, Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;->queryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    iget-object v2, v2, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->projection:[Ljava/lang/String;

    iget-object v4, v10, Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;->queryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    iget-object v4, v4, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->selectionArgs:[Ljava/lang/String;

    iget-object v5, v10, Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;->queryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    iget-object v5, v5, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->orderBy:Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 102
    .local v7, "c":Landroid/database/Cursor;
    new-instance v4, Lcom/samsung/musicplus/dialog/PersonalFolderSelectionDialog$FolderSelectionListAdapter;

    invoke-virtual {p1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f040082

    iget-object v8, v10, Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;->queryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    const/4 v9, 0x0

    invoke-direct/range {v4 .. v9}, Lcom/samsung/musicplus/dialog/PersonalFolderSelectionDialog$FolderSelectionListAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;Lcom/samsung/musicplus/util/ListUtils$QueryArgs;I)V

    iput-object v4, p0, Lcom/samsung/musicplus/dialog/PersonalFolderSelectionDialog;->mAadapter:Lcom/samsung/musicplus/dialog/PersonalFolderSelectionDialog$FolderSelectionListAdapter;

    .line 105
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/PersonalFolderSelectionDialog;->mAadapter:Lcom/samsung/musicplus/dialog/PersonalFolderSelectionDialog$FolderSelectionListAdapter;

    invoke-virtual {p2, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 106
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/PersonalFolderSelectionDialog;->mAadapter:Lcom/samsung/musicplus/dialog/PersonalFolderSelectionDialog$FolderSelectionListAdapter;

    invoke-virtual {v0}, Lcom/samsung/musicplus/dialog/PersonalFolderSelectionDialog$FolderSelectionListAdapter;->notifyDataSetChanged()V

    .line 107
    return-void
.end method

.method public static showPersonalFolderSelectionDialog(Landroid/app/Activity;I[JZZ)V
    .locals 7
    .param p0, "a"    # Landroid/app/Activity;
    .param p1, "listType"    # I
    .param p2, "items"    # [J
    .param p3, "isMoving"    # Z
    .param p4, "removeFromNowPlaying"    # Z

    .prologue
    .line 171
    invoke-virtual {p0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/musicplus/dialog/PersonalFolderSelectionDialog;->getNormalContentsCount(Landroid/content/Context;)I

    move-result v0

    const/4 v1, 0x1

    if-lt v0, v1, :cond_0

    .line 172
    invoke-static {p1, p2, p3, p4}, Lcom/samsung/musicplus/dialog/PersonalFolderSelectionDialog;->getInstance(I[JZZ)Landroid/app/DialogFragment;

    move-result-object v6

    .line 174
    .local v6, "selectionDialog":Landroid/app/DialogFragment;
    invoke-virtual {p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v1, "personalFolderSelectionDialog"

    invoke-virtual {v6, v0, v1}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 180
    .end local v6    # "selectionDialog":Landroid/app/DialogFragment;
    :goto_0
    return-void

    .line 176
    :cond_0
    const/4 v4, 0x0

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move v3, p3

    move v5, p4

    invoke-static/range {v0 .. v5}, Lcom/samsung/musicplus/dialog/PersonalMoveDialogFragment;->showPersonalMoveDialogFragment(Landroid/app/Activity;I[JZLjava/lang/String;Z)V

    goto :goto_0
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v7, 0x0

    .line 78
    const-string v5, "PersonalFileOperation"

    const-string v6, "PersonalFolderSelectionDialog, onCreate()"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/PersonalFolderSelectionDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 81
    .local v0, "a":Landroid/app/Activity;
    const-string v5, "layout_inflater"

    invoke-virtual {v0, v5}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 83
    .local v2, "inflater":Landroid/view/LayoutInflater;
    const v5, 0x7f040006

    invoke-virtual {v2, v5, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    .line 84
    .local v4, "v":Landroid/view/View;
    const v5, 0x102000a

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ListView;

    .line 85
    .local v3, "listView":Landroid/widget/ListView;
    invoke-virtual {v3, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 86
    invoke-direct {p0, v0, v3}, Lcom/samsung/musicplus/dialog/PersonalFolderSelectionDialog;->setAdapter(Landroid/app/Activity;Landroid/widget/ListView;)V

    .line 88
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 89
    .local v1, "builder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    const v6, 0x7f1000b7

    invoke-virtual {v5, v6}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    const v6, 0x7f10003a

    invoke-virtual {v5, v6, v7}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/musicplus/dialog/PersonalFolderSelectionDialog;->mAlertDialog:Landroid/app/AlertDialog;

    .line 92
    iget-object v5, p0, Lcom/samsung/musicplus/dialog/PersonalFolderSelectionDialog;->mAlertDialog:Landroid/app/AlertDialog;

    return-object v5
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 146
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/PersonalFolderSelectionDialog;->mAadapter:Lcom/samsung/musicplus/dialog/PersonalFolderSelectionDialog$FolderSelectionListAdapter;

    if-eqz v0, :cond_0

    .line 147
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/PersonalFolderSelectionDialog;->mAadapter:Lcom/samsung/musicplus/dialog/PersonalFolderSelectionDialog$FolderSelectionListAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/dialog/PersonalFolderSelectionDialog$FolderSelectionListAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 149
    :cond_0
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onDismiss(Landroid/content/DialogInterface;)V

    .line 150
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 11
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 111
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/PersonalFolderSelectionDialog;->mAadapter:Lcom/samsung/musicplus/dialog/PersonalFolderSelectionDialog$FolderSelectionListAdapter;

    invoke-virtual {v0}, Lcom/samsung/musicplus/dialog/PersonalFolderSelectionDialog$FolderSelectionListAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v6

    .line 112
    .local v6, "c":Landroid/database/Cursor;
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/PersonalFolderSelectionDialog;->mAadapter:Lcom/samsung/musicplus/dialog/PersonalFolderSelectionDialog$FolderSelectionListAdapter;

    invoke-virtual {v0}, Lcom/samsung/musicplus/dialog/PersonalFolderSelectionDialog$FolderSelectionListAdapter;->getQueryInfo()Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    move-result-object v8

    .line 113
    .local v8, "queryArgs":Lcom/samsung/musicplus/util/ListUtils$QueryArgs;
    const/4 v4, 0x0

    .line 115
    .local v4, "selectionDestPath":Ljava/lang/String;
    if-eqz v6, :cond_1

    if-eqz v8, :cond_1

    .line 117
    :try_start_0
    invoke-interface {v6, p3}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 118
    iget-object v0, v8, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->text2Col:Ljava/lang/String;

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/dialog/PersonalFolderSelectionDialog;->getFolderSubPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 120
    const-string v0, "PersonalFileOperation"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "PersonalFolderSelectionDialog, onItemClick - dest path is "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v0, v9}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/PersonalFolderSelectionDialog;->mAlertDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/dialog/PersonalFolderSelectionDialog;->mAlertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 124
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/PersonalFolderSelectionDialog;->mAlertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 128
    :cond_0
    if-eqz v6, :cond_1

    .line 129
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 134
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/PersonalFolderSelectionDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v7

    .line 135
    .local v7, "data":Landroid/os/Bundle;
    const-string v0, "list_type"

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 136
    .local v1, "listType":I
    const-string v0, "list_items"

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v2

    .line 137
    .local v2, "listItems":[J
    const-string v0, "is_moving"

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    .line 138
    .local v3, "isMoving":Z
    const-string v0, "remove_from_now_playing"

    invoke-virtual {v7, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    .line 139
    .local v5, "removeFromNowPlaying":Z
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/PersonalFolderSelectionDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static/range {v0 .. v5}, Lcom/samsung/musicplus/dialog/PersonalMoveDialogFragment;->showPersonalMoveDialogFragment(Landroid/app/Activity;I[JZLjava/lang/String;Z)V

    .line 142
    return-void

    .line 128
    .end local v1    # "listType":I
    .end local v2    # "listItems":[J
    .end local v3    # "isMoving":Z
    .end local v5    # "removeFromNowPlaying":Z
    .end local v7    # "data":Landroid/os/Bundle;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 129
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method
