.class Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld$3;
.super Ljava/lang/Object;
.source "ChangeDeviceDialogOld.java"

# interfaces
.implements Landroid/net/wifi/p2p/WifiP2pManager$PeerListListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;)V
    .locals 0

    .prologue
    .line 130
    iput-object p1, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld$3;->this$0:Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private setupDeviceList(Landroid/net/wifi/p2p/WifiP2pDeviceList;)V
    .locals 5
    .param p1, "peers"    # Landroid/net/wifi/p2p/WifiP2pDeviceList;

    .prologue
    .line 144
    # getter for: Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->sWifiP2pDeviceList:Ljava/util/List;
    invoke-static {}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->access$600()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->clear()V

    .line 146
    invoke-virtual {p1}, Landroid/net/wifi/p2p/WifiP2pDeviceList;->getDeviceList()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/p2p/WifiP2pDevice;

    .line 147
    .local v0, "dev":Landroid/net/wifi/p2p/WifiP2pDevice;
    invoke-static {v0}, Lcom/samsung/musicplus/library/wifi/P2pManager;->isWifiDisplayDevice(Landroid/net/wifi/p2p/WifiP2pDevice;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {v0}, Lcom/samsung/musicplus/library/wifi/P2pManager;->isWifiBusy(Landroid/net/wifi/p2p/WifiP2pDevice;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 148
    # getter for: Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->access$000()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onPeersAvailable. Find Device Name - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v0, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Address - "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v0, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 150
    iget v2, v0, Landroid/net/wifi/p2p/WifiP2pDevice;->status:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_0

    .line 151
    # getter for: Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->sWifiP2pDeviceList:Ljava/util/List;
    invoke-static {}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->access$600()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 155
    .end local v0    # "dev":Landroid/net/wifi/p2p/WifiP2pDevice;
    :cond_1
    return-void
.end method


# virtual methods
.method public onPeersAvailable(Landroid/net/wifi/p2p/WifiP2pDeviceList;)V
    .locals 4
    .param p1, "peers"    # Landroid/net/wifi/p2p/WifiP2pDeviceList;

    .prologue
    .line 134
    # getter for: Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onPeersAvailable() - wifiP2pDeviceList: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/wifi/p2p/WifiP2pDeviceList;->getDeviceList()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 136
    iget-object v1, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld$3;->this$0:Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;

    # getter for: Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->access$500(Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/musicplus/library/wifi/WifiDisplayManager;->getInstance(Landroid/content/Context;)Lcom/samsung/musicplus/library/wifi/WifiDisplayManager;

    move-result-object v0

    .line 137
    .local v0, "wm":Lcom/samsung/musicplus/library/wifi/WifiDisplayManager;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/samsung/musicplus/library/wifi/WifiDisplayManager;->isWfdConnected()Z

    move-result v1

    if-nez v1, :cond_0

    .line 138
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld$3;->setupDeviceList(Landroid/net/wifi/p2p/WifiP2pDeviceList;)V

    .line 139
    iget-object v1, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld$3;->this$0:Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;

    iget-object v2, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld$3;->this$0:Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;

    # getter for: Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mListView:Landroid/widget/ListView;
    invoke-static {v2}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->access$100(Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;)Landroid/widget/ListView;

    move-result-object v2

    # invokes: Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->setAdapter(Landroid/widget/ListView;)V
    invoke-static {v1, v2}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->access$200(Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;Landroid/widget/ListView;)V

    .line 141
    :cond_0
    return-void
.end method
