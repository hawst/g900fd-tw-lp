.class public Lcom/samsung/musicplus/dialog/ChangeDeviceArrayAdapter;
.super Landroid/widget/ArrayAdapter;
.source "ChangeDeviceArrayAdapter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Lcom/samsung/musicplus/dialog/WifiDeviceInfo;",
        ">;"
    }
.end annotation


# static fields
.field public static final CHANGE_PLAYER_DIALOG:I = 0x0

.field private static final CLASSNAME:Ljava/lang/String;

.field public static final MULTI_SPEAKER_SELECTION_DIALOG:I = 0x1


# instance fields
.field private mCheckedList:[Z

.field private final mContext:Landroid/content/Context;

.field private final mDeviceList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/musicplus/dialog/WifiDeviceInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mDialogType:I

.field private mSelectedDeviceId:Ljava/lang/String;

.field private mTheme:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const-class v0, Lcom/samsung/musicplus/dialog/ChangeDeviceArrayAdapter;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/dialog/ChangeDeviceArrayAdapter;->CLASSNAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/util/ArrayList;ILjava/lang/String;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "textViewResourceId"    # I
    .param p4, "dialogType"    # I
    .param p5, "selectedDeviceId"    # Ljava/lang/String;
    .param p6, "theme"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/musicplus/dialog/WifiDeviceInfo;",
            ">;I",
            "Ljava/lang/String;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 75
    .local p3, "objects":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/musicplus/dialog/WifiDeviceInfo;>;"
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 76
    iput-object p1, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceArrayAdapter;->mContext:Landroid/content/Context;

    .line 77
    iput-object p3, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceArrayAdapter;->mDeviceList:Ljava/util/ArrayList;

    .line 78
    iput p4, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceArrayAdapter;->mDialogType:I

    .line 79
    iput-object p5, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceArrayAdapter;->mSelectedDeviceId:Ljava/lang/String;

    .line 80
    iput p6, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceArrayAdapter;->mTheme:I

    .line 81
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/util/ArrayList;ILjava/lang/String;[Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "textViewResourceId"    # I
    .param p4, "dialogType"    # I
    .param p5, "selectedDeviceId"    # Ljava/lang/String;
    .param p6, "isChecked"    # [Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/musicplus/dialog/WifiDeviceInfo;",
            ">;I",
            "Ljava/lang/String;",
            "[Z)V"
        }
    .end annotation

    .prologue
    .line 96
    .local p3, "objects":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/musicplus/dialog/WifiDeviceInfo;>;"
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    .line 97
    iput-object p1, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceArrayAdapter;->mContext:Landroid/content/Context;

    .line 98
    iput-object p3, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceArrayAdapter;->mDeviceList:Ljava/util/ArrayList;

    .line 99
    iput p4, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceArrayAdapter;->mDialogType:I

    .line 100
    iput-object p5, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceArrayAdapter;->mSelectedDeviceId:Ljava/lang/String;

    .line 101
    iput-object p6, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceArrayAdapter;->mCheckedList:[Z

    .line 102
    invoke-static {}, Lcom/samsung/musicplus/util/UiUtils;->getTheme()I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceArrayAdapter;->mTheme:I

    .line 103
    return-void
.end method

.method private isChecked(I)Z
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 245
    const/4 v0, 0x0

    .line 246
    .local v0, "doCheck":Z
    iget-object v1, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceArrayAdapter;->mCheckedList:[Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceArrayAdapter;->mCheckedList:[Z

    aget-boolean v1, v1, p1

    if-eqz v1, :cond_0

    .line 247
    const/4 v0, 0x1

    .line 249
    :cond_0
    return v0
.end method

.method private isSelectedDevice(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1
    .param p1, "selectedId"    # Ljava/lang/String;
    .param p2, "deviceId"    # Ljava/lang/String;

    .prologue
    .line 234
    if-eqz p1, :cond_0

    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    if-nez p1, :cond_2

    if-nez p2, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private makeCheckBox()Landroid/widget/CheckBox;
    .locals 6

    .prologue
    const/4 v3, -0x2

    const/4 v5, 0x0

    .line 203
    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v2, v3, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 205
    .local v2, "params":Landroid/widget/LinearLayout$LayoutParams;
    const/16 v3, 0x10

    iput v3, v2, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 206
    iget-object v3, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceArrayAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c0021

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 208
    .local v1, "padding":I
    new-instance v0, Landroid/widget/CheckBox;

    iget-object v3, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceArrayAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v0, v3}, Landroid/widget/CheckBox;-><init>(Landroid/content/Context;)V

    .line 209
    .local v0, "checkBox":Landroid/widget/CheckBox;
    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 210
    invoke-virtual {v0, v5, v5, v1, v5}, Landroid/widget/CheckBox;->setPadding(IIII)V

    .line 211
    invoke-virtual {v0, v5}, Landroid/widget/CheckBox;->setClickable(Z)V

    .line 212
    invoke-virtual {v0, v5}, Landroid/widget/CheckBox;->setFocusable(Z)V

    .line 213
    const v3, 0x1020001

    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setId(I)V

    .line 215
    return-object v0
.end method

.method private makeRadioBtn()Landroid/widget/RadioButton;
    .locals 5

    .prologue
    const/4 v3, -0x2

    const/4 v4, 0x0

    .line 219
    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v3, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    .line 221
    .local v1, "params":Landroid/widget/LinearLayout$LayoutParams;
    const/16 v3, 0x10

    iput v3, v1, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    .line 223
    const/4 v0, 0x0

    .line 225
    .local v0, "padding":I
    new-instance v2, Landroid/widget/RadioButton;

    iget-object v3, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceArrayAdapter;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Landroid/widget/RadioButton;-><init>(Landroid/content/Context;)V

    .line 226
    .local v2, "radioButton":Landroid/widget/RadioButton;
    invoke-virtual {v2, v1}, Landroid/widget/RadioButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 227
    invoke-virtual {v2, v4, v4, v0, v4}, Landroid/widget/RadioButton;->setPadding(IIII)V

    .line 228
    invoke-virtual {v2, v4}, Landroid/widget/RadioButton;->setClickable(Z)V

    .line 229
    invoke-virtual {v2, v4}, Landroid/widget/RadioButton;->setFocusable(Z)V

    .line 230
    return-object v2
.end method

.method private setDmrDescription(ILandroid/view/View;Lcom/samsung/musicplus/dialog/WifiDeviceInfo;)V
    .locals 3
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "info"    # Lcom/samsung/musicplus/dialog/WifiDeviceInfo;

    .prologue
    .line 130
    const v1, 0x7f0d0053

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 132
    .local v0, "description":Landroid/widget/TextView;
    iget-object v1, p3, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->deviceId:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 133
    iget-object v1, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceArrayAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "device_name"

    invoke-static {v1, v2}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 142
    :goto_0
    iget v1, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceArrayAdapter;->mTheme:I

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceArrayAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b000f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    .line 145
    return-void

    .line 136
    :cond_0
    iget v1, p3, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->deviceType:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    .line 137
    const v1, 0x7f10003d

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 139
    :cond_1
    const v1, 0x7f10003e

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    .line 142
    :cond_2
    iget-object v1, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceArrayAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0010

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    goto :goto_1
.end method

.method private setDmrIcon(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/String;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "iv"    # Landroid/widget/ImageView;
    .param p3, "uri"    # Ljava/lang/String;

    .prologue
    const v6, 0x7f020123

    .line 195
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0138

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 196
    .local v5, "size":I
    invoke-virtual {p2, p3}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 197
    invoke-virtual {p2, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 198
    invoke-static {}, Lcom/samsung/musicplus/util/AlbumArtLoader;->getAlbumArtLoader()Lcom/samsung/musicplus/util/IAlbumArtLoader;

    move-result-object v0

    move-object v1, p1

    move-object v2, p3

    move-object v3, p3

    move-object v4, p2

    invoke-interface/range {v0 .. v6}, Lcom/samsung/musicplus/util/IAlbumArtLoader;->loadArtwork(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Object;Landroid/widget/ImageView;II)V

    .line 200
    return-void
.end method

.method private setIcon(Lcom/samsung/musicplus/dialog/WifiDeviceInfo;Landroid/widget/ImageView;)V
    .locals 3
    .param p1, "info"    # Lcom/samsung/musicplus/dialog/WifiDeviceInfo;
    .param p2, "icon"    # Landroid/widget/ImageView;

    .prologue
    .line 170
    iget v1, p1, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->deviceType:I

    packed-switch v1, :pswitch_data_0

    .line 192
    :goto_0
    return-void

    .line 172
    :pswitch_0
    iget-object v1, p1, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->deviceId:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 173
    const v1, 0x7f02001d

    invoke-virtual {p2, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 174
    :cond_0
    iget-object v1, p1, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->albumArtUriStr:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 175
    const v1, 0x7f020123

    invoke-virtual {p2, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 178
    :cond_1
    iget-object v1, p1, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->albumArtUriStr:Ljava/lang/String;

    invoke-static {v1}, Lcom/samsung/musicplus/util/AlbumArtUtils;->getCachedNetworkAlbumArt(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 179
    .local v0, "d":Landroid/graphics/drawable/Drawable;
    if-nez v0, :cond_2

    .line 180
    iget-object v1, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceArrayAdapter;->mContext:Landroid/content/Context;

    iget-object v2, p1, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->albumArtUriStr:Ljava/lang/String;

    invoke-direct {p0, v1, p2, v2}, Lcom/samsung/musicplus/dialog/ChangeDeviceArrayAdapter;->setDmrIcon(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/String;)V

    goto :goto_0

    .line 182
    :cond_2
    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 187
    .end local v0    # "d":Landroid/graphics/drawable/Drawable;
    :pswitch_1
    iget v1, p1, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->wfdIcon:I

    invoke-virtual {p2, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 170
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private setSeletionItem(ILandroid/view/View;Ljava/lang/String;)V
    .locals 4
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "deviceId"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    .line 148
    iget v2, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceArrayAdapter;->mDialogType:I

    packed-switch v2, :pswitch_data_0

    .line 167
    .end local p2    # "convertView":Landroid/view/View;
    :goto_0
    return-void

    .line 151
    .restart local p2    # "convertView":Landroid/view/View;
    :pswitch_0
    invoke-direct {p0}, Lcom/samsung/musicplus/dialog/ChangeDeviceArrayAdapter;->makeRadioBtn()Landroid/widget/RadioButton;

    move-result-object v1

    .line 152
    .local v1, "radio":Landroid/widget/RadioButton;
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceArrayAdapter;->mSelectedDeviceId:Ljava/lang/String;

    invoke-direct {p0, v2, p3}, Lcom/samsung/musicplus/dialog/ChangeDeviceArrayAdapter;->isSelectedDevice(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-direct {p0, p1}, Lcom/samsung/musicplus/dialog/ChangeDeviceArrayAdapter;->isChecked(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 153
    :cond_0
    invoke-virtual {v1, v3}, Landroid/widget/RadioButton;->setChecked(Z)V

    .line 155
    :cond_1
    check-cast p2, Landroid/widget/LinearLayout;

    .end local p2    # "convertView":Landroid/view/View;
    invoke-virtual {p2, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto :goto_0

    .line 158
    .end local v1    # "radio":Landroid/widget/RadioButton;
    .restart local p2    # "convertView":Landroid/view/View;
    :pswitch_1
    invoke-direct {p0}, Lcom/samsung/musicplus/dialog/ChangeDeviceArrayAdapter;->makeCheckBox()Landroid/widget/CheckBox;

    move-result-object v0

    .line 159
    .local v0, "checkBox":Landroid/widget/CheckBox;
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/dialog/ChangeDeviceArrayAdapter;->isChecked(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 160
    invoke-virtual {v0, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 162
    :cond_2
    check-cast p2, Landroid/view/ViewGroup;

    .end local p2    # "convertView":Landroid/view/View;
    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    goto :goto_0

    .line 148
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 107
    iget-object v4, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceArrayAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    const-string v5, "layout_inflater"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 109
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v4, 0x7f040007

    const/4 v5, 0x0

    invoke-virtual {v1, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 111
    iget-object v4, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceArrayAdapter;->mDeviceList:Ljava/util/ArrayList;

    invoke-virtual {v4, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;

    .line 112
    .local v2, "info":Lcom/samsung/musicplus/dialog/WifiDeviceInfo;
    sget-object v4, Lcom/samsung/musicplus/dialog/ChangeDeviceArrayAdapter;->CLASSNAME:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getView() - position: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " info.dmrId: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v2, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->deviceId:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    const v4, 0x7f0d0051

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 115
    .local v0, "icon":Landroid/widget/ImageView;
    invoke-direct {p0, v2, v0}, Lcom/samsung/musicplus/dialog/ChangeDeviceArrayAdapter;->setIcon(Lcom/samsung/musicplus/dialog/WifiDeviceInfo;Landroid/widget/ImageView;)V

    .line 117
    const v4, 0x7f0d0052

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 118
    .local v3, "name":Landroid/widget/TextView;
    iget-object v4, v2, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->deviceName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 119
    iget v4, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceArrayAdapter;->mTheme:I

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceArrayAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b000d

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    :goto_0
    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(I)V

    .line 123
    invoke-direct {p0, p1, p2, v2}, Lcom/samsung/musicplus/dialog/ChangeDeviceArrayAdapter;->setDmrDescription(ILandroid/view/View;Lcom/samsung/musicplus/dialog/WifiDeviceInfo;)V

    .line 124
    iget-object v4, v2, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->deviceId:Ljava/lang/String;

    invoke-direct {p0, p1, p2, v4}, Lcom/samsung/musicplus/dialog/ChangeDeviceArrayAdapter;->setSeletionItem(ILandroid/view/View;Ljava/lang/String;)V

    .line 125
    invoke-virtual {p2, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 126
    return-object p2

    .line 119
    :cond_0
    iget-object v4, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceArrayAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b000e

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    goto :goto_0
.end method
