.class public Lcom/samsung/musicplus/dialog/AdaptSoundDialog;
.super Landroid/app/DialogFragment;
.source "AdaptSoundDialog.java"


# static fields
.field private static final ADAPT_SOUND_OFF:I = 0x1

.field private static final ADAPT_SOUND_ON:I = 0x0

.field private static final CLASSNAME:Ljava/lang/String;

.field public static final TAG_ADAPT_SOUND:Ljava/lang/String; = "adapt_sound"


# instance fields
.field private mAlertDialog:Landroid/app/AlertDialog;

.field private mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const-class v0, Lcom/samsung/musicplus/dialog/AdaptSoundDialog;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/dialog/AdaptSoundDialog;->CLASSNAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/samsung/musicplus/dialog/AdaptSoundDialog;->CLASSNAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/musicplus/dialog/AdaptSoundDialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/AdaptSoundDialog;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/samsung/musicplus/dialog/AdaptSoundDialog;->handlingActivityNotFound()V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/musicplus/dialog/AdaptSoundDialog;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/AdaptSoundDialog;

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/samsung/musicplus/dialog/AdaptSoundDialog;->isEnableAdaptSoundPath()Z

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/samsung/musicplus/dialog/AdaptSoundDialog;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/AdaptSoundDialog;

    .prologue
    .line 27
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/AdaptSoundDialog;->mAlertDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/musicplus/dialog/AdaptSoundDialog;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/AdaptSoundDialog;
    .param p1, "x1"    # Z

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/dialog/AdaptSoundDialog;->setAdaptSound(Z)V

    return-void
.end method

.method static synthetic access$500(Lcom/samsung/musicplus/dialog/AdaptSoundDialog;Landroid/content/Context;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/AdaptSoundDialog;
    .param p1, "x1"    # Landroid/content/Context;

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/dialog/AdaptSoundDialog;->warningAdaptSound(Landroid/content/Context;)V

    return-void
.end method

.method private handlingActivityNotFound()V
    .locals 5

    .prologue
    .line 156
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 157
    .local v1, "i":Landroid/content/Intent;
    const-string v2, "com.sec.android.app.popupuireceiver"

    const-string v3, "com.sec.android.app.popupuireceiver.DisableApp"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 159
    const-string v2, "app_package_name"

    const-string v3, "com.sec.hearingadjust"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 161
    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {p0, v1, v2}, Lcom/samsung/musicplus/dialog/AdaptSoundDialog;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 167
    :goto_0
    return-void

    .line 162
    :catch_0
    move-exception v0

    .line 163
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    sget-object v2, Lcom/samsung/musicplus/dialog/AdaptSoundDialog;->CLASSNAME:Ljava/lang/String;

    const-string v3, "Activity Not found!!!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 164
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    .line 165
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/AdaptSoundDialog;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "Target Activity Not Found"

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method private isEnableAdaptSoundPath()Z
    .locals 1

    .prologue
    .line 141
    sget-boolean v0, Lcom/samsung/musicplus/util/Version;->LIBRARY_OVER_API_1:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/musicplus/dialog/AdaptSoundDialog;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

    invoke-virtual {v0}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->isWiredHeadsetOn()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/dialog/AdaptSoundDialog;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

    invoke-virtual {v0}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->isAudioPathBT()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/AdaptSoundDialog;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

    invoke-virtual {v0}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->isWiredHeadsetOn()Z

    move-result v0

    goto :goto_0
.end method

.method private setAdaptSound(Z)V
    .locals 6
    .param p1, "turnOn"    # Z

    .prologue
    .line 105
    const/4 v1, 0x1

    .line 107
    .local v1, "isOk":Z
    :try_start_0
    invoke-static {p1}, Lcom/samsung/musicplus/util/ServiceUtils;->setAdaptSound(Z)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 116
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/AdaptSoundDialog;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v3

    const/4 v4, 0x2

    if-eqz v1, :cond_1

    const/4 v2, -0x1

    :goto_1
    const/4 v5, 0x0

    invoke-virtual {v3, v4, v2, v5}, Landroid/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 119
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/AdaptSoundDialog;->mAlertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v2}, Landroid/app/AlertDialog;->dismiss()V

    .line 121
    if-eqz p1, :cond_0

    .line 124
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/AdaptSoundDialog;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "ADSD"

    invoke-static {v2, v3}, Lcom/samsung/musicplus/util/logging/FeatureLogger;->insertLog(Landroid/content/Context;Ljava/lang/String;)V

    .line 127
    :cond_0
    return-void

    .line 108
    :catch_0
    move-exception v0

    .line 113
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    sget-object v2, Lcom/samsung/musicplus/dialog/AdaptSoundDialog;->CLASSNAME:Ljava/lang/String;

    const-string v3, "setAdaptSound(false) - IllegalArgumentException!!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 114
    const/4 v1, 0x0

    goto :goto_0

    .line 116
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private warningAdaptSound(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 147
    sget-boolean v1, Lcom/samsung/musicplus/util/Version;->LIBRARY_OVER_API_1:Z

    if-eqz v1, :cond_0

    .line 148
    const v0, 0x7f10016b

    .line 152
    .local v0, "resId":I
    :goto_0
    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/dialog/AdaptSoundDialog;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {p1, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 153
    return-void

    .line 150
    .end local v0    # "resId":I
    :cond_0
    const v0, 0x7f10016c

    .restart local v0    # "resId":I
    goto :goto_0
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 9
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 43
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/AdaptSoundDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 44
    .local v0, "a":Landroid/app/Activity;
    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    .line 45
    .local v3, "context":Landroid/content/Context;
    invoke-static {v3}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->getInstance(Landroid/content/Context;)Lcom/samsung/musicplus/library/audio/SecAudioManager;

    move-result-object v7

    iput-object v7, p0, Lcom/samsung/musicplus/dialog/AdaptSoundDialog;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

    .line 46
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/AdaptSoundDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f100006

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 47
    .local v4, "title":Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/musicplus/library/audio/AdaptSound;->getAdaptSoundOn(Landroid/content/Context;)Z

    move-result v1

    .line 49
    .local v1, "active":Z
    invoke-virtual {p0, v5, v5}, Lcom/samsung/musicplus/dialog/AdaptSoundDialog;->setStyle(II)V

    .line 51
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 53
    .local v2, "builder":Landroid/app/AlertDialog$Builder;
    const v7, 0x7f100007

    new-instance v8, Lcom/samsung/musicplus/dialog/AdaptSoundDialog$1;

    invoke-direct {v8, p0, v3}, Lcom/samsung/musicplus/dialog/AdaptSoundDialog$1;-><init>(Lcom/samsung/musicplus/dialog/AdaptSoundDialog;Landroid/content/Context;)V

    invoke-virtual {v2, v7, v8}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 67
    const v7, 0x7f10003a

    const/4 v8, 0x0

    invoke-virtual {v2, v7, v8}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 68
    invoke-virtual {v2, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 69
    const v7, 0x7f090007

    if-ne v1, v6, :cond_0

    :goto_0
    new-instance v6, Lcom/samsung/musicplus/dialog/AdaptSoundDialog$2;

    invoke-direct {v6, p0, v3}, Lcom/samsung/musicplus/dialog/AdaptSoundDialog$2;-><init>(Lcom/samsung/musicplus/dialog/AdaptSoundDialog;Landroid/content/Context;)V

    invoke-virtual {v2, v7, v5, v6}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(IILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 99
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/musicplus/dialog/AdaptSoundDialog;->mAlertDialog:Landroid/app/AlertDialog;

    .line 101
    iget-object v5, p0, Lcom/samsung/musicplus/dialog/AdaptSoundDialog;->mAlertDialog:Landroid/app/AlertDialog;

    return-object v5

    :cond_0
    move v5, v6

    .line 69
    goto :goto_0
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 131
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/AdaptSoundDialog;->mAlertDialog:Landroid/app/AlertDialog;

    if-nez v0, :cond_0

    .line 138
    :goto_0
    return-void

    .line 134
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/AdaptSoundDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/musicplus/library/audio/AdaptSound;->isAlreadyDiagnosed(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 135
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/AdaptSoundDialog;->mAlertDialog:Landroid/app/AlertDialog;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 137
    :cond_1
    invoke-super {p0}, Landroid/app/DialogFragment;->onResume()V

    goto :goto_0
.end method
