.class Lcom/samsung/musicplus/dialog/WfdErrorDialog$4;
.super Ljava/lang/Object;
.source "WfdErrorDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/dialog/WfdErrorDialog;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/dialog/WfdErrorDialog;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/dialog/WfdErrorDialog;)V
    .locals 0

    .prologue
    .line 136
    iput-object p1, p0, Lcom/samsung/musicplus/dialog/WfdErrorDialog$4;->this$0:Lcom/samsung/musicplus/dialog/WfdErrorDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1, "arg0"    # Landroid/content/DialogInterface;
    .param p2, "arg1"    # I

    .prologue
    .line 139
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/WfdErrorDialog$4;->this$0:Lcom/samsung/musicplus/dialog/WfdErrorDialog;

    invoke-virtual {v2}, Lcom/samsung/musicplus/dialog/WfdErrorDialog;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const-string v3, "music_player_pref"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/app/Activity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 141
    .local v1, "sp":Landroid/content/SharedPreferences;
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/WfdErrorDialog$4;->this$0:Lcom/samsung/musicplus/dialog/WfdErrorDialog;

    # getter for: Lcom/samsung/musicplus/dialog/WfdErrorDialog;->mExceptionalCase:I
    invoke-static {v2}, Lcom/samsung/musicplus/dialog/WfdErrorDialog;->access$300(Lcom/samsung/musicplus/dialog/WfdErrorDialog;)I

    move-result v2

    invoke-static {v2}, Lcom/samsung/musicplus/dialog/WfdErrorDialog;->getWfdExceptionalCaseKey(I)Ljava/lang/String;

    move-result-object v0

    .line 142
    .local v0, "key":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 143
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/musicplus/dialog/WfdErrorDialog$4;->this$0:Lcom/samsung/musicplus/dialog/WfdErrorDialog;

    # getter for: Lcom/samsung/musicplus/dialog/WfdErrorDialog;->mIsChecked:Z
    invoke-static {v3}, Lcom/samsung/musicplus/dialog/WfdErrorDialog;->access$000(Lcom/samsung/musicplus/dialog/WfdErrorDialog;)Z

    move-result v3

    invoke-interface {v2, v0, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 145
    :cond_0
    const-string v2, "wfd_group_play"

    if-eq v0, v2, :cond_1

    .line 146
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/WfdErrorDialog$4;->this$0:Lcom/samsung/musicplus/dialog/WfdErrorDialog;

    # invokes: Lcom/samsung/musicplus/dialog/WfdErrorDialog;->showChangeDeviceDialog()V
    invoke-static {v2}, Lcom/samsung/musicplus/dialog/WfdErrorDialog;->access$200(Lcom/samsung/musicplus/dialog/WfdErrorDialog;)V

    .line 148
    :cond_1
    return-void
.end method
