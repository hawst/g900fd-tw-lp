.class public Lcom/samsung/musicplus/dialog/ViewTypeDialogFragment;
.super Landroid/app/DialogFragment;
.source "ViewTypeDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/dialog/ViewTypeDialogFragment$ViewTypeChangeListener;
    }
.end annotation


# static fields
.field private static final EXTRA_VIEW_TYPE:Ljava/lang/String; = "music_list_view_type"

.field private static final VIEW_TYPE_GRID:I = 0x7f1000c1

.field private static final VIEW_TYPE_LIST:I = 0x7f1000b3

.field public static final VIEW_TYPE_TAG:Ljava/lang/String; = "view_by"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 75
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/dialog/ViewTypeDialogFragment;Ljava/lang/String;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/ViewTypeDialogFragment;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/dialog/ViewTypeDialogFragment;->getSelectedType(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static getInstance(I)Lcom/samsung/musicplus/dialog/ViewTypeDialogFragment;
    .locals 3
    .param p0, "viewType"    # I

    .prologue
    .line 26
    new-instance v1, Lcom/samsung/musicplus/dialog/ViewTypeDialogFragment;

    invoke-direct {v1}, Lcom/samsung/musicplus/dialog/ViewTypeDialogFragment;-><init>()V

    .line 27
    .local v1, "dialog":Lcom/samsung/musicplus/dialog/ViewTypeDialogFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 28
    .local v0, "data":Landroid/os/Bundle;
    const-string v2, "music_list_view_type"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 29
    invoke-virtual {v1, v0}, Lcom/samsung/musicplus/dialog/ViewTypeDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 30
    return-object v1
.end method

.method private getSelectedType(Ljava/lang/String;)I
    .locals 2
    .param p1, "type"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 67
    const v1, 0x7f1000b3

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/dialog/ViewTypeDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 72
    :cond_0
    :goto_0
    return v0

    .line 69
    :cond_1
    const v1, 0x7f1000c1

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/dialog/ViewTypeDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 70
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 35
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/ViewTypeDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    .line 36
    .local v3, "data":Landroid/os/Bundle;
    const-string v5, "music_list_view_type"

    invoke-virtual {v3, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    .line 37
    .local v4, "viewType":I
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/ViewTypeDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 38
    .local v0, "a":Landroid/app/Activity;
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 39
    .local v1, "builder":Landroid/app/AlertDialog$Builder;
    const v5, 0x7f1000c4

    invoke-virtual {v1, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 40
    const/4 v5, 0x2

    new-array v2, v5, [Ljava/lang/CharSequence;

    const/4 v5, 0x0

    const v6, 0x7f1000b3

    invoke-virtual {p0, v6}, Lcom/samsung/musicplus/dialog/ViewTypeDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v5

    const/4 v5, 0x1

    const v6, 0x7f1000c1

    invoke-virtual {p0, v6}, Lcom/samsung/musicplus/dialog/ViewTypeDialogFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v5

    .line 43
    .local v2, "cs":[Ljava/lang/CharSequence;
    new-instance v5, Lcom/samsung/musicplus/dialog/ViewTypeDialogFragment$1;

    invoke-direct {v5, p0, v2, v4}, Lcom/samsung/musicplus/dialog/ViewTypeDialogFragment$1;-><init>(Lcom/samsung/musicplus/dialog/ViewTypeDialogFragment;[Ljava/lang/CharSequence;I)V

    invoke-virtual {v1, v2, v4, v5}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 56
    const v5, 0x7f10003a

    new-instance v6, Lcom/samsung/musicplus/dialog/ViewTypeDialogFragment$2;

    invoke-direct {v6, p0}, Lcom/samsung/musicplus/dialog/ViewTypeDialogFragment$2;-><init>(Lcom/samsung/musicplus/dialog/ViewTypeDialogFragment;)V

    invoke-virtual {v1, v5, v6}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 63
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v5

    return-object v5
.end method
