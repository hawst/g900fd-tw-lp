.class public Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;
.super Landroid/app/DialogFragment;
.source "ChangeDeviceDialogOld.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;


# static fields
.field private static final ACTION_CHANGEPLAYER_CANCELED:Ljava/lang/String; = "com.samsung.wfd.CHANGEPLAYER_CANCELED"

.field private static final ACTION_HDMI_PLUGGED:Ljava/lang/String; = "android.intent.action.HDMI_PLUGGED"

.field private static final CLASSNAME:Ljava/lang/String;

.field private static final DEVICE_IMAGE_TYPE_RES:[I

.field private static final MY_DEVICE:Ljava/lang/String; = "MY DEVICE"

.field private static sWifiP2pDeviceList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/net/wifi/p2p/WifiP2pDevice;",
            ">;"
        }
    .end annotation
.end field

.field private static selectedWfdInfo:Lcom/samsung/musicplus/dialog/WifiDeviceInfo;


# instance fields
.field private DMR_PROJECTION:[Ljava/lang/String;

.field private mAlertDialog:Landroid/app/AlertDialog;

.field private mContext:Landroid/content/Context;

.field private final mDlnaConnectivityReceiver:Landroid/content/BroadcastReceiver;

.field private mHasOnClickListener:Z

.field private final mHdmiReceiver:Landroid/content/BroadcastReceiver;

.field private mIsHdmiConnected:Z

.field private mListView:Landroid/widget/ListView;

.field private mNic:Ljava/lang/String;

.field private mP2pManager:Lcom/samsung/musicplus/library/wifi/P2pManager;

.field private mRefreshProgressBar:Landroid/widget/ProgressBar;

.field private mSelectedDeviceId:Ljava/lang/String;

.field private final mWfdReceiver:Landroid/content/BroadcastReceiver;

.field private mWifiPeerListener:Landroid/net/wifi/p2p/WifiP2pManager$PeerListListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 55
    const-class v0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->CLASSNAME:Ljava/lang/String;

    .line 91
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->sWifiP2pDeviceList:Ljava/util/List;

    .line 544
    const/16 v0, 0xb

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->DEVICE_IMAGE_TYPE_RES:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x7f020015
        0x7f020018
        0x7f02001b
        0x7f020014
        0x7f02001c
        0x7f02001a
        0x7f020016
        0x7f020019
        0x7f020017
        0x7f02001d
        0x7f020013
    .end array-data
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 179
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 69
    iput-boolean v2, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mHasOnClickListener:Z

    .line 71
    iput-boolean v2, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mIsHdmiConnected:Z

    .line 93
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "avplayer_id"

    aput-object v1, v0, v2

    const/4 v1, 0x1

    const-string v2, "avplayer_name"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "album_art"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->DMR_PROJECTION:[Ljava/lang/String;

    .line 100
    new-instance v0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld$1;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld$1;-><init>(Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;)V

    iput-object v0, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mDlnaConnectivityReceiver:Landroid/content/BroadcastReceiver;

    .line 112
    new-instance v0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld$2;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld$2;-><init>(Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;)V

    iput-object v0, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mWfdReceiver:Landroid/content/BroadcastReceiver;

    .line 130
    new-instance v0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld$3;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld$3;-><init>(Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;)V

    iput-object v0, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mWifiPeerListener:Landroid/net/wifi/p2p/WifiP2pManager$PeerListListener;

    .line 158
    new-instance v0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld$4;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld$4;-><init>(Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;)V

    iput-object v0, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mHdmiReceiver:Landroid/content/BroadcastReceiver;

    .line 180
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    sget-object v0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->CLASSNAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;)Landroid/widget/ListView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mListView:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mAlertDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;)Landroid/widget/ProgressBar;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mRefreshProgressBar:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;Landroid/widget/ListView;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;
    .param p1, "x1"    # Landroid/widget/ListView;

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->setAdapter(Landroid/widget/ListView;)V

    return-void
.end method

.method static synthetic access$300(Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;)Landroid/net/wifi/p2p/WifiP2pManager$PeerListListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mWifiPeerListener:Landroid/net/wifi/p2p/WifiP2pManager$PeerListListener;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;)Lcom/samsung/musicplus/library/wifi/P2pManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mP2pManager:Lcom/samsung/musicplus/library/wifi/P2pManager;

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$600()Ljava/util/List;
    .locals 1

    .prologue
    .line 53
    sget-object v0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->sWifiP2pDeviceList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$700(Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;

    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mIsHdmiConnected:Z

    return v0
.end method

.method static synthetic access$702(Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;
    .param p1, "x1"    # Z

    .prologue
    .line 53
    iput-boolean p1, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mIsHdmiConnected:Z

    return p1
.end method

.method static synthetic access$800(Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->scanWifiDevices()V

    return-void
.end method

.method static synthetic access$900(Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->sendBroadcastCancel()V

    return-void
.end method

.method private addCurrentConnectedWifiDevice(Landroid/content/Context;Ljava/util/ArrayList;)Ljava/lang/String;
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/musicplus/dialog/WifiDeviceInfo;",
            ">;)",
            "Ljava/lang/String;"
        }
    .end annotation

    .prologue
    .local p2, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/musicplus/dialog/WifiDeviceInfo;>;"
    const/4 v4, 0x1

    .line 436
    const/4 v0, 0x0

    .line 437
    .local v0, "deviceId":Ljava/lang/String;
    sget-boolean v3, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_SUPPORT_WIFI_DISPLAY:Z

    if-eqz v3, :cond_4

    .line 438
    iget-object v3, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/samsung/musicplus/library/wifi/WifiDisplayManager;->getInstance(Landroid/content/Context;)Lcom/samsung/musicplus/library/wifi/WifiDisplayManager;

    move-result-object v2

    .line 440
    .local v2, "wm":Lcom/samsung/musicplus/library/wifi/WifiDisplayManager;
    if-eqz v2, :cond_4

    invoke-virtual {v2}, Lcom/samsung/musicplus/library/wifi/WifiDisplayManager;->isWfdConnected()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 441
    sget-boolean v3, Lcom/samsung/musicplus/util/Version;->LIBRARY_OVER_API_1:Z

    if-eqz v3, :cond_5

    .line 443
    sget-object v3, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->selectedWfdInfo:Lcom/samsung/musicplus/dialog/WifiDeviceInfo;

    if-eqz v3, :cond_0

    sget-object v3, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->selectedWfdInfo:Lcom/samsung/musicplus/dialog/WifiDeviceInfo;

    iget-object v3, v3, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->deviceName:Ljava/lang/String;

    if-eqz v3, :cond_0

    sget-object v3, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->selectedWfdInfo:Lcom/samsung/musicplus/dialog/WifiDeviceInfo;

    iget-object v3, v3, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->deviceName:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 445
    :cond_0
    new-instance v3, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;

    invoke-direct {v3}, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;-><init>()V

    sput-object v3, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->selectedWfdInfo:Lcom/samsung/musicplus/dialog/WifiDeviceInfo;

    .line 446
    sget-object v3, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->selectedWfdInfo:Lcom/samsung/musicplus/dialog/WifiDeviceInfo;

    iput v4, v3, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->deviceType:I

    .line 447
    sget-object v3, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->selectedWfdInfo:Lcom/samsung/musicplus/dialog/WifiDeviceInfo;

    iget-object v4, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/samsung/musicplus/library/wifi/P2pManager;->getConnectedDeviceName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->deviceName:Ljava/lang/String;

    .line 451
    sget-object v3, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->selectedWfdInfo:Lcom/samsung/musicplus/dialog/WifiDeviceInfo;

    iget-object v4, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/samsung/musicplus/library/wifi/P2pManager;->getConnectedDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->deviceId:Ljava/lang/String;

    .line 452
    sget-object v3, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->selectedWfdInfo:Lcom/samsung/musicplus/dialog/WifiDeviceInfo;

    invoke-static {p1}, Lcom/samsung/musicplus/library/wifi/P2pManager;->getConnectedDeviceType(Landroid/content/Context;)I

    move-result v4

    invoke-direct {p0, v4}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->getDeviceIcon(I)I

    move-result v4

    iput v4, v3, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->wfdIcon:I

    .line 455
    sget-object v3, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->CLASSNAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setAdpater() - selectedWfdInfo has been changed, Name:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->selectedWfdInfo:Lcom/samsung/musicplus/dialog/WifiDeviceInfo;

    iget-object v5, v5, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->deviceName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ID:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->selectedWfdInfo:Lcom/samsung/musicplus/dialog/WifiDeviceInfo;

    iget-object v5, v5, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->deviceId:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 459
    :cond_1
    sget-object v3, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->selectedWfdInfo:Lcom/samsung/musicplus/dialog/WifiDeviceInfo;

    iget-object v3, v3, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->deviceName:Ljava/lang/String;

    if-eqz v3, :cond_2

    sget-object v3, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->selectedWfdInfo:Lcom/samsung/musicplus/dialog/WifiDeviceInfo;

    iget-object v3, v3, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->deviceName:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 460
    :cond_2
    sget-object v3, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->selectedWfdInfo:Lcom/samsung/musicplus/dialog/WifiDeviceInfo;

    invoke-virtual {v2}, Lcom/samsung/musicplus/library/wifi/WifiDisplayManager;->getDisplayDeviceName()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->deviceName:Ljava/lang/String;

    .line 461
    sget-object v3, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->selectedWfdInfo:Lcom/samsung/musicplus/dialog/WifiDeviceInfo;

    invoke-virtual {v2}, Lcom/samsung/musicplus/library/wifi/WifiDisplayManager;->getDisplayDeviceAddress()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->deviceId:Ljava/lang/String;

    .line 462
    sget-object v3, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->selectedWfdInfo:Lcom/samsung/musicplus/dialog/WifiDeviceInfo;

    invoke-static {p1}, Lcom/samsung/musicplus/library/wifi/P2pManager;->getConnectedDeviceType(Landroid/content/Context;)I

    move-result v4

    invoke-direct {p0, v4}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->getDeviceIcon(I)I

    move-result v4

    iput v4, v3, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->wfdIcon:I

    .line 465
    sget-object v3, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->CLASSNAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setAdpater() - getConnectedDeviceInfo returned null, so try to get the information using wfdManager, Name :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->selectedWfdInfo:Lcom/samsung/musicplus/dialog/WifiDeviceInfo;

    iget-object v5, v5, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->deviceName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " ID:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    sget-object v5, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->selectedWfdInfo:Lcom/samsung/musicplus/dialog/WifiDeviceInfo;

    iget-object v5, v5, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->deviceId:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 472
    :cond_3
    sget-object v3, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->selectedWfdInfo:Lcom/samsung/musicplus/dialog/WifiDeviceInfo;

    invoke-virtual {p2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 473
    sget-object v3, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->selectedWfdInfo:Lcom/samsung/musicplus/dialog/WifiDeviceInfo;

    iget-object v0, v3, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->deviceId:Ljava/lang/String;

    .line 490
    .end local v2    # "wm":Lcom/samsung/musicplus/library/wifi/WifiDisplayManager;
    :cond_4
    :goto_0
    return-object v0

    .line 475
    .restart local v2    # "wm":Lcom/samsung/musicplus/library/wifi/WifiDisplayManager;
    :cond_5
    if-eqz v2, :cond_4

    invoke-virtual {v2}, Lcom/samsung/musicplus/library/wifi/WifiDisplayManager;->isWfdConnected()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 476
    new-instance v1, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;

    invoke-direct {v1}, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;-><init>()V

    .line 477
    .local v1, "info":Lcom/samsung/musicplus/dialog/WifiDeviceInfo;
    iput v4, v1, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->deviceType:I

    .line 478
    invoke-static {p1}, Lcom/samsung/musicplus/library/wifi/P2pManager;->getConnectedDeviceName(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->deviceName:Ljava/lang/String;

    .line 482
    invoke-static {p1}, Lcom/samsung/musicplus/library/wifi/P2pManager;->getConnectedDeviceId(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->deviceId:Ljava/lang/String;

    .line 483
    invoke-static {p1}, Lcom/samsung/musicplus/library/wifi/P2pManager;->getConnectedDeviceType(Landroid/content/Context;)I

    move-result v3

    invoke-direct {p0, v3}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->getDeviceIcon(I)I

    move-result v3

    iput v3, v1, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->wfdIcon:I

    .line 484
    iget-object v0, v1, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->deviceId:Ljava/lang/String;

    .line 485
    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private addDmrDevices(Landroid/content/Context;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "networkId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/musicplus/dialog/WifiDeviceInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p3, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/musicplus/dialog/WifiDeviceInfo;>;"
    const/4 v1, 0x0

    .line 554
    const/4 v3, 0x0

    .line 555
    .local v3, "selection":Ljava/lang/String;
    const/4 v4, 0x0

    .line 556
    .local v4, "selectionArgs":[Ljava/lang/String;
    if-eqz p2, :cond_0

    .line 557
    const-string v3, "nic_id = ? or avplayer_name = ?"

    .line 559
    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    .end local v4    # "selectionArgs":[Ljava/lang/String;
    aput-object p2, v4, v1

    const/4 v0, 0x1

    const-string v1, "MY DEVICE"

    aput-object v1, v4, v0

    .line 565
    .restart local v4    # "selectionArgs":[Ljava/lang/String;
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/musicplus/contents/dlna/DlnaStore$Renderer;->CONTENT_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->DMR_PROJECTION:[Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 569
    .local v6, "c":Landroid/database/Cursor;
    if-eqz v6, :cond_2

    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 571
    :cond_1
    new-instance v7, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;

    invoke-direct {v7}, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;-><init>()V

    .line 572
    .local v7, "info":Lcom/samsung/musicplus/dialog/WifiDeviceInfo;
    const-string v0, "avplayer_id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v7, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->deviceId:Ljava/lang/String;

    .line 574
    const-string v0, "avplayer_name"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v7, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->deviceName:Ljava/lang/String;

    .line 576
    const-string v0, "album_art"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v7, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->albumArtUriStr:Ljava/lang/String;

    .line 578
    const/4 v0, 0x0

    iput v0, v7, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->deviceType:I

    .line 579
    invoke-virtual {p3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 580
    sget-object v0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "dmr list - name: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "avplayer_name"

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 584
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    .line 590
    .end local v7    # "info":Lcom/samsung/musicplus/dialog/WifiDeviceInfo;
    :cond_2
    if-eqz v6, :cond_3

    .line 591
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 594
    :cond_3
    return-void

    .line 590
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_4

    .line 591
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0
.end method

.method private addMyDevice(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/musicplus/dialog/WifiDeviceInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 429
    .local p1, "devices":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/musicplus/dialog/WifiDeviceInfo;>;"
    new-instance v0, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;

    invoke-direct {v0}, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;-><init>()V

    .line 430
    .local v0, "info":Lcom/samsung/musicplus/dialog/WifiDeviceInfo;
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->deviceId:Ljava/lang/String;

    .line 431
    const v1, 0x7f1000d9

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->deviceName:Ljava/lang/String;

    .line 432
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 433
    return-void
.end method

.method private addWifiDevices(Ljava/util/List;Ljava/util/ArrayList;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/net/wifi/p2p/WifiP2pDevice;",
            ">;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/musicplus/dialog/WifiDeviceInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 494
    .local p1, "devices":Ljava/util/List;, "Ljava/util/List<Landroid/net/wifi/p2p/WifiP2pDevice;>;"
    .local p2, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/musicplus/dialog/WifiDeviceInfo;>;"
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v5

    if-nez v5, :cond_1

    .line 522
    :cond_0
    return-void

    .line 500
    :cond_1
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v4

    .line 501
    .local v4, "len":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v4, :cond_0

    .line 502
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/p2p/WifiP2pDevice;

    .line 503
    .local v0, "device":Landroid/net/wifi/p2p/WifiP2pDevice;
    new-instance v3, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;

    invoke-direct {v3}, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;-><init>()V

    .line 504
    .local v3, "info":Lcom/samsung/musicplus/dialog/WifiDeviceInfo;
    sget-boolean v5, Lcom/samsung/musicplus/util/Version;->LIBRARY_OVER_API_1:Z

    if-eqz v5, :cond_2

    .line 505
    sget-object v5, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->selectedWfdInfo:Lcom/samsung/musicplus/dialog/WifiDeviceInfo;

    if-eqz v5, :cond_2

    iget-object v5, v0, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    sget-object v6, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->selectedWfdInfo:Lcom/samsung/musicplus/dialog/WifiDeviceInfo;

    iget-object v6, v6, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->deviceId:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 507
    sget-object v5, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->CLASSNAME:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "setAdapter() continue - deviceAddress : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v0, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " deviceName : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, v0, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 501
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 512
    :cond_2
    const/4 v5, 0x1

    iput v5, v3, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->deviceType:I

    .line 513
    iget-object v5, v0, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    iput-object v5, v3, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->deviceId:Ljava/lang/String;

    .line 514
    iget-object v5, v0, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    iput-object v5, v3, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->deviceName:Ljava/lang/String;

    .line 515
    iput-object v0, v3, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->wifiP2pDevice:Landroid/net/wifi/p2p/WifiP2pDevice;

    .line 517
    iget-object v5, v0, Landroid/net/wifi/p2p/WifiP2pDevice;->primaryDeviceType:Ljava/lang/String;

    invoke-static {v5}, Lcom/samsung/musicplus/library/wifi/P2pManager;->getWfdDeviceType(Ljava/lang/String;)I

    move-result v1

    .line 518
    .local v1, "deviceType":I
    invoke-static {v1}, Lcom/samsung/musicplus/library/wifi/P2pManager;->isDongle(I)Z

    move-result v5

    iput-boolean v5, v3, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->isWfdDongle:Z

    .line 519
    invoke-direct {p0, v1}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->getDeviceIcon(I)I

    move-result v5

    iput v5, v3, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->wfdIcon:I

    .line 520
    invoke-virtual {p2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method private connectWfdDevice(Landroid/net/wifi/p2p/WifiP2pDevice;)V
    .locals 1
    .param p1, "wifiP2pDevice"    # Landroid/net/wifi/p2p/WifiP2pDevice;

    .prologue
    .line 651
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->changeToWfdDevice()V

    .line 652
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mP2pManager:Lcom/samsung/musicplus/library/wifi/P2pManager;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/library/wifi/P2pManager;->connect(Landroid/net/wifi/p2p/WifiP2pDevice;)V

    .line 653
    return-void
.end method

.method private getDeviceIcon(I)I
    .locals 1
    .param p1, "deviceType"    # I

    .prologue
    .line 525
    sget-object v0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->DEVICE_IMAGE_TYPE_RES:[I

    aget v0, v0, p1

    return v0
.end method

.method private getHelpNeverShownValue()Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 656
    iget-object v1, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mContext:Landroid/content/Context;

    const-string v2, "music_player_setting"

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 658
    .local v0, "sp":Landroid/content/SharedPreferences;
    const-string v1, "wfd_help_popup_never_shown"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method

.method private refreshList()V
    .locals 4

    .prologue
    .line 344
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->refreshDlna()V

    .line 346
    iget-boolean v0, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mIsHdmiConnected:Z

    if-nez v0, :cond_0

    .line 347
    invoke-direct {p0}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->scanWifiDevices()V

    .line 350
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mRefreshProgressBar:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 351
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mRefreshProgressBar:Landroid/widget/ProgressBar;

    new-instance v1, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld$7;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld$7;-><init>(Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;)V

    const-wide/16 v2, 0xfa0

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/ProgressBar;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 357
    return-void
.end method

.method private registerDlnaReceiver()V
    .locals 3

    .prologue
    .line 372
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 373
    .local v0, "f":Landroid/content/IntentFilter;
    const-string v1, "com.samsung.musicplus.dlna.connectivitychanged"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 374
    iget-object v1, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mDlnaConnectivityReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 375
    return-void
.end method

.method private registerHdmiReceiver()V
    .locals 3

    .prologue
    .line 360
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.HDMI_PLUGGED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 361
    .local v0, "f":Landroid/content/IntentFilter;
    iget-object v1, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mHdmiReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 362
    return-void
.end method

.method private registerWfdReceiver()V
    .locals 3

    .prologue
    .line 365
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 366
    .local v0, "f":Landroid/content/IntentFilter;
    const-string v1, "android.net.wifi.p2p.PEERS_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 367
    const-string v1, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 368
    iget-object v1, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mWfdReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 369
    return-void
.end method

.method private scanWifiDevices()V
    .locals 3

    .prologue
    .line 378
    sget-boolean v1, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_SUPPORT_WIFI_DISPLAY:Z

    if-eqz v1, :cond_0

    .line 379
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-string v2, "display"

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/display/DisplayManager;

    .line 382
    .local v0, "dm":Landroid/hardware/display/DisplayManager;
    sget-boolean v1, Lcom/samsung/musicplus/util/Version;->LIBRARY_OVER_API_1:Z

    if-eqz v1, :cond_1

    .line 383
    iget-object v1, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mP2pManager:Lcom/samsung/musicplus/library/wifi/P2pManager;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mP2pManager:Lcom/samsung/musicplus/library/wifi/P2pManager;

    invoke-virtual {v1}, Lcom/samsung/musicplus/library/wifi/P2pManager;->isP2pConnected()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {v0}, Lcom/samsung/musicplus/library/wifi/DisplayManagerCompat;->checkExceptionalCase(Landroid/hardware/display/DisplayManager;)I

    move-result v1

    if-nez v1, :cond_0

    .line 386
    iget-object v1, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mP2pManager:Lcom/samsung/musicplus/library/wifi/P2pManager;

    invoke-virtual {v1}, Lcom/samsung/musicplus/library/wifi/P2pManager;->scanDevices()V

    .line 394
    .end local v0    # "dm":Landroid/hardware/display/DisplayManager;
    :cond_0
    :goto_0
    return-void

    .line 389
    .restart local v0    # "dm":Landroid/hardware/display/DisplayManager;
    :cond_1
    iget-object v1, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mP2pManager:Lcom/samsung/musicplus/library/wifi/P2pManager;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mP2pManager:Lcom/samsung/musicplus/library/wifi/P2pManager;

    invoke-virtual {v1}, Lcom/samsung/musicplus/library/wifi/P2pManager;->isP2pConnected()Z

    move-result v1

    if-nez v1, :cond_0

    .line 390
    iget-object v1, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mP2pManager:Lcom/samsung/musicplus/library/wifi/P2pManager;

    invoke-virtual {v1}, Lcom/samsung/musicplus/library/wifi/P2pManager;->scanDevices()V

    goto :goto_0
.end method

.method private selectDevice(ILjava/lang/String;Landroid/net/wifi/p2p/WifiP2pDevice;Z)V
    .locals 5
    .param p1, "deviceType"    # I
    .param p2, "selectedDeviceId"    # Ljava/lang/String;
    .param p3, "wifiP2pDevice"    # Landroid/net/wifi/p2p/WifiP2pDevice;
    .param p4, "isWfdDongle"    # Z

    .prologue
    .line 598
    sget-object v2, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->CLASSNAME:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "selectDevice deviceType: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " selectedDeviceId: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " isWfdDongle: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 601
    iput-object p2, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mSelectedDeviceId:Ljava/lang/String;

    .line 604
    packed-switch p1, :pswitch_data_0

    .line 642
    :cond_0
    :goto_0
    return-void

    .line 606
    :pswitch_0
    if-nez p2, :cond_1

    .line 607
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->changeToDefaultPlayer()V

    .line 613
    :goto_1
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/samsung/musicplus/library/wifi/WifiDisplayManager;->getInstance(Landroid/content/Context;)Lcom/samsung/musicplus/library/wifi/WifiDisplayManager;

    move-result-object v1

    .line 614
    .local v1, "wm":Lcom/samsung/musicplus/library/wifi/WifiDisplayManager;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/samsung/musicplus/library/wifi/WifiDisplayManager;->isWfdConnected()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 615
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mContext:Landroid/content/Context;

    const v3, 0x7f1001ca

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 617
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/samsung/musicplus/library/wifi/WifiDisplayManager;->setWfdTerminateExt(Z)V

    goto :goto_0

    .line 609
    .end local v1    # "wm":Lcom/samsung/musicplus/library/wifi/WifiDisplayManager;
    :cond_1
    invoke-static {p2}, Lcom/samsung/musicplus/util/ServiceUtils;->changeToDmrPlayer(Ljava/lang/String;)V

    goto :goto_1

    .line 621
    :pswitch_1
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/samsung/musicplus/library/wifi/WifiDisplayManager;->getInstance(Landroid/content/Context;)Lcom/samsung/musicplus/library/wifi/WifiDisplayManager;

    move-result-object v1

    .line 622
    .restart local v1    # "wm":Lcom/samsung/musicplus/library/wifi/WifiDisplayManager;
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/samsung/musicplus/library/wifi/WifiDisplayManager;->isWfdConnected()Z

    move-result v2

    if-nez v2, :cond_0

    .line 627
    :cond_2
    if-eqz p4, :cond_4

    .line 628
    invoke-direct {p0}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->getHelpNeverShownValue()Z

    move-result v0

    .line 629
    .local v0, "neverShown":Z
    if-eqz v0, :cond_3

    .line 630
    invoke-direct {p0, p3}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->connectWfdDevice(Landroid/net/wifi/p2p/WifiP2pDevice;)V

    goto :goto_0

    .line 632
    :cond_3
    invoke-static {p2}, Lcom/samsung/musicplus/dialog/WfdHelpDialog;->getInstance(Ljava/lang/String;)Landroid/app/DialogFragment;

    move-result-object v2

    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    const-string v4, "wfd_dongle_dialog"

    invoke-virtual {v2, v3, v4}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 636
    .end local v0    # "neverShown":Z
    :cond_4
    invoke-direct {p0, p3}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->connectWfdDevice(Landroid/net/wifi/p2p/WifiP2pDevice;)V

    goto :goto_0

    .line 604
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private sendBroadcastCancel()V
    .locals 3

    .prologue
    .line 268
    sget-object v0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->CLASSNAME:Ljava/lang/String;

    const-string v1, "sendBroadcastChangePlayerCancel()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 269
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mContext:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.samsung.wfd.CHANGEPLAYER_CANCELED"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 270
    return-void
.end method

.method private setAdapter(Landroid/widget/ListView;)V
    .locals 10
    .param p1, "lv"    # Landroid/widget/ListView;

    .prologue
    .line 397
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->getActivity()Landroid/app/Activity;

    move-result-object v7

    .line 400
    .local v7, "a":Landroid/app/Activity;
    if-nez v7, :cond_0

    .line 426
    :goto_0
    return-void

    .line 404
    :cond_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 406
    .local v3, "devices":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/musicplus/dialog/WifiDeviceInfo;>;"
    invoke-direct {p0, v3}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->addMyDevice(Ljava/util/ArrayList;)V

    .line 408
    invoke-virtual {v7}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    .line 410
    .local v8, "context":Landroid/content/Context;
    iget-object v1, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mNic:Ljava/lang/String;

    invoke-direct {p0, v8, v1, v3}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->addDmrDevices(Landroid/content/Context;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 413
    sget-object v1, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->sWifiP2pDeviceList:Ljava/util/List;

    invoke-direct {p0, v1, v3}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->addWifiDevices(Ljava/util/List;Ljava/util/ArrayList;)V

    .line 416
    invoke-direct {p0, v8, v3}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->addCurrentConnectedWifiDevice(Landroid/content/Context;Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v9

    .line 417
    .local v9, "selectedId":Ljava/lang/String;
    if-eqz v9, :cond_1

    .line 418
    iput-object v9, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mSelectedDeviceId:Ljava/lang/String;

    .line 421
    :cond_1
    new-instance v0, Lcom/samsung/musicplus/dialog/ChangeDeviceArrayAdapter;

    iget-object v1, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mContext:Landroid/content/Context;

    const v2, 0x7f040007

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mSelectedDeviceId:Ljava/lang/String;

    invoke-static {}, Lcom/samsung/musicplus/util/UiUtils;->getTheme()I

    move-result v6

    invoke-direct/range {v0 .. v6}, Lcom/samsung/musicplus/dialog/ChangeDeviceArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/ArrayList;ILjava/lang/String;I)V

    .line 424
    .local v0, "adapter":Lcom/samsung/musicplus/dialog/ChangeDeviceArrayAdapter;
    invoke-virtual {p1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 425
    invoke-virtual {v0}, Lcom/samsung/musicplus/dialog/ChangeDeviceArrayAdapter;->notifyDataSetChanged()V

    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 337
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mAlertDialog:Landroid/app/AlertDialog;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 338
    invoke-direct {p0}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->refreshList()V

    .line 340
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 184
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 185
    sget-object v0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCreate() - savedInstanceState: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    if-eqz p1, :cond_0

    .line 187
    const-string v0, "deviceId"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mSelectedDeviceId:Ljava/lang/String;

    .line 188
    const-string v0, "nic"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mNic:Ljava/lang/String;

    .line 193
    :goto_0
    return-void

    .line 190
    :cond_0
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getDlnaPlayingDmrId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mSelectedDeviceId:Ljava/lang/String;

    .line 191
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getDlnaPlayingNic()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mNic:Ljava/lang/String;

    goto :goto_0
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 9
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v8, 0x0

    .line 204
    sget-object v6, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->CLASSNAME:Ljava/lang/String;

    const-string v7, "onCreateDialog()"

    invoke-static {v6, v7}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 207
    .local v0, "a":Landroid/app/Activity;
    const/4 v1, 0x0

    .line 209
    .local v1, "builder":Landroid/app/AlertDialog$Builder;
    new-instance v1, Landroid/app/AlertDialog$Builder;

    .end local v1    # "builder":Landroid/app/AlertDialog$Builder;
    const/4 v6, 0x5

    invoke-direct {v1, v0, v6}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 215
    .restart local v1    # "builder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->getContext()Landroid/content/Context;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mContext:Landroid/content/Context;

    .line 217
    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6}, Lcom/samsung/musicplus/library/wifi/P2pManager;->getInstance(Landroid/content/Context;)Lcom/samsung/musicplus/library/wifi/P2pManager;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mP2pManager:Lcom/samsung/musicplus/library/wifi/P2pManager;

    .line 218
    iget-object v6, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mP2pManager:Lcom/samsung/musicplus/library/wifi/P2pManager;

    if-eqz v6, :cond_0

    .line 219
    invoke-direct {p0}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->registerHdmiReceiver()V

    .line 222
    :cond_0
    iget-object v6, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mContext:Landroid/content/Context;

    const-string v7, "layout_inflater"

    invoke-virtual {v6, v7}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/LayoutInflater;

    .line 225
    .local v4, "inflater":Landroid/view/LayoutInflater;
    const v6, 0x7f040006

    invoke-virtual {v4, v6, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 226
    .local v5, "v":Landroid/view/View;
    const v6, 0x7f0d004d

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 227
    .local v2, "category":Landroid/view/View;
    const/4 v6, 0x0

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    .line 228
    const v6, 0x7f0d0050

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 230
    .local v3, "divider":Landroid/view/View;
    const v6, 0x7f0d004f

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ProgressBar;

    iput-object v6, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mRefreshProgressBar:Landroid/widget/ProgressBar;

    .line 232
    const v6, 0x102000a

    invoke-virtual {v5, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ListView;

    iput-object v6, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mListView:Landroid/widget/ListView;

    .line 233
    iget-object v6, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mListView:Landroid/widget/ListView;

    invoke-virtual {v6, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 234
    iget-object v6, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mListView:Landroid/widget/ListView;

    invoke-direct {p0, v6}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->setAdapter(Landroid/widget/ListView;)V

    .line 236
    invoke-virtual {v1, v5}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const v7, 0x7f100152

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const v7, 0x7f10012f

    invoke-virtual {v6, v7, v8}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    const v7, 0x7f10003a

    new-instance v8, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld$5;

    invoke-direct {v8, p0}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld$5;-><init>(Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;)V

    invoke-virtual {v6, v7, v8}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mAlertDialog:Landroid/app/AlertDialog;

    .line 245
    iget-object v6, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mAlertDialog:Landroid/app/AlertDialog;

    new-instance v7, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld$6;

    invoke-direct {v7, p0}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld$6;-><init>(Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;)V

    invoke-virtual {v6, v7}, Landroid/app/AlertDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 263
    invoke-direct {p0}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->refreshList()V

    .line 264
    iget-object v6, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mAlertDialog:Landroid/app/AlertDialog;

    return-object v6
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 299
    sget-object v0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->CLASSNAME:Ljava/lang/String;

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 300
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mP2pManager:Lcom/samsung/musicplus/library/wifi/P2pManager;

    if-eqz v0, :cond_1

    .line 302
    sget-boolean v0, Lcom/samsung/musicplus/util/Version;->LIBRARY_OVER_API_1:Z

    if-eqz v0, :cond_2

    .line 303
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "display"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/display/DisplayManager;

    invoke-static {v0}, Lcom/samsung/musicplus/library/wifi/DisplayManagerCompat;->checkExceptionalCase(Landroid/hardware/display/DisplayManager;)I

    move-result v0

    if-nez v0, :cond_0

    .line 305
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mP2pManager:Lcom/samsung/musicplus/library/wifi/P2pManager;

    invoke-virtual {v0, v2}, Lcom/samsung/musicplus/library/wifi/P2pManager;->requestPeers(Landroid/net/wifi/p2p/WifiP2pManager$PeerListListener;)V

    .line 306
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mP2pManager:Lcom/samsung/musicplus/library/wifi/P2pManager;

    invoke-virtual {v0}, Lcom/samsung/musicplus/library/wifi/P2pManager;->stopPeerDiscovery()V

    .line 312
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mHdmiReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 314
    :cond_1
    invoke-super {p0}, Landroid/app/DialogFragment;->onDestroy()V

    .line 315
    return-void

    .line 309
    :cond_2
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mP2pManager:Lcom/samsung/musicplus/library/wifi/P2pManager;

    invoke-virtual {v0, v2}, Lcom/samsung/musicplus/library/wifi/P2pManager;->requestPeers(Landroid/net/wifi/p2p/WifiP2pManager$PeerListListener;)V

    .line 310
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mP2pManager:Lcom/samsung/musicplus/library/wifi/P2pManager;

    invoke-virtual {v0}, Lcom/samsung/musicplus/library/wifi/P2pManager;->stopPeerDiscovery()V

    goto :goto_0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 319
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    sget-object v1, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->CLASSNAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onItemClick() - position: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " id: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 320
    if-eqz p2, :cond_1

    .line 321
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;

    .line 322
    .local v0, "info":Lcom/samsung/musicplus/dialog/WifiDeviceInfo;
    sget-boolean v1, Lcom/samsung/musicplus/util/Version;->LIBRARY_OVER_API_1:Z

    if-eqz v1, :cond_0

    .line 323
    iget v1, v0, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->deviceType:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 324
    sput-object v0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->selectedWfdInfo:Lcom/samsung/musicplus/dialog/WifiDeviceInfo;

    .line 329
    :cond_0
    :goto_0
    iget v1, v0, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->deviceType:I

    iget-object v2, v0, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->deviceId:Ljava/lang/String;

    iget-object v3, v0, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->wifiP2pDevice:Landroid/net/wifi/p2p/WifiP2pDevice;

    iget-boolean v4, v0, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->isWfdDongle:Z

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->selectDevice(ILjava/lang/String;Landroid/net/wifi/p2p/WifiP2pDevice;Z)V

    .line 332
    .end local v0    # "info":Lcom/samsung/musicplus/dialog/WifiDeviceInfo;
    :cond_1
    iget-object v1, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mAlertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->dismiss()V

    .line 333
    return-void

    .line 326
    .restart local v0    # "info":Lcom/samsung/musicplus/dialog/WifiDeviceInfo;
    :cond_2
    new-instance v1, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;

    invoke-direct {v1}, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;-><init>()V

    sput-object v1, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->selectedWfdInfo:Lcom/samsung/musicplus/dialog/WifiDeviceInfo;

    goto :goto_0
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 288
    sget-object v0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->CLASSNAME:Ljava/lang/String;

    const-string v1, "onPause()"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 289
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mDlnaConnectivityReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 291
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mP2pManager:Lcom/samsung/musicplus/library/wifi/P2pManager;

    if-eqz v0, :cond_0

    .line 292
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mWfdReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 294
    :cond_0
    invoke-super {p0}, Landroid/app/DialogFragment;->onPause()V

    .line 295
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 274
    iget-boolean v0, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mHasOnClickListener:Z

    if-nez v0, :cond_0

    .line 275
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mAlertDialog:Landroid/app/AlertDialog;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 276
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mHasOnClickListener:Z

    .line 279
    :cond_0
    invoke-direct {p0}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->registerDlnaReceiver()V

    .line 280
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mP2pManager:Lcom/samsung/musicplus/library/wifi/P2pManager;

    if-eqz v0, :cond_1

    .line 281
    invoke-direct {p0}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->registerWfdReceiver()V

    .line 283
    :cond_1
    invoke-super {p0}, Landroid/app/DialogFragment;->onResume()V

    .line 284
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 197
    const-string v0, "deviceId"

    iget-object v1, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mSelectedDeviceId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    const-string v0, "nic"

    iget-object v1, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mNic:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 200
    return-void
.end method
