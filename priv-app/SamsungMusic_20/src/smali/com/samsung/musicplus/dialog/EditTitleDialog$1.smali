.class Lcom/samsung/musicplus/dialog/EditTitleDialog$1;
.super Ljava/lang/Object;
.source "EditTitleDialog.java"

# interfaces
.implements Landroid/text/InputFilter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/dialog/EditTitleDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/dialog/EditTitleDialog;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/dialog/EditTitleDialog;)V
    .locals 0

    .prologue
    .line 104
    iput-object p1, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog$1;->this$0:Lcom/samsung/musicplus/dialog/EditTitleDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;
    .locals 5
    .param p1, "source"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "end"    # I
    .param p4, "dest"    # Landroid/text/Spanned;
    .param p5, "dstart"    # I
    .param p6, "dend"    # I

    .prologue
    const v4, 0x7f100097

    .line 108
    move-object v2, p1

    .line 109
    .local v2, "result":Ljava/lang/CharSequence;
    const-string v3, "[\\*/\\\\\\?:<>\\|\"]+"

    invoke-static {v3}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    .line 110
    .local v1, "ps":Ljava/util/regex/Pattern;
    invoke-virtual {v1, v2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 111
    .local v0, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 112
    iget-object v3, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog$1;->this$0:Lcom/samsung/musicplus/dialog/EditTitleDialog;

    # getter for: Lcom/samsung/musicplus/dialog/EditTitleDialog;->mToast:Landroid/widget/Toast;
    invoke-static {v3}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->access$000(Lcom/samsung/musicplus/dialog/EditTitleDialog;)Landroid/widget/Toast;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 113
    iget-object v3, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog$1;->this$0:Lcom/samsung/musicplus/dialog/EditTitleDialog;

    # getter for: Lcom/samsung/musicplus/dialog/EditTitleDialog;->mToast:Landroid/widget/Toast;
    invoke-static {v3}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->access$000(Lcom/samsung/musicplus/dialog/EditTitleDialog;)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/widget/Toast;->setText(I)V

    .line 114
    iget-object v3, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog$1;->this$0:Lcom/samsung/musicplus/dialog/EditTitleDialog;

    # getter for: Lcom/samsung/musicplus/dialog/EditTitleDialog;->mToast:Landroid/widget/Toast;
    invoke-static {v3}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->access$000(Lcom/samsung/musicplus/dialog/EditTitleDialog;)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 116
    :cond_0
    const-string v3, ""

    invoke-virtual {v0, v3}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 119
    :cond_1
    invoke-static {v2}, Lcom/samsung/musicplus/util/EmojiList;->hasEmojiString(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 120
    iget-object v3, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog$1;->this$0:Lcom/samsung/musicplus/dialog/EditTitleDialog;

    # getter for: Lcom/samsung/musicplus/dialog/EditTitleDialog;->mToast:Landroid/widget/Toast;
    invoke-static {v3}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->access$000(Lcom/samsung/musicplus/dialog/EditTitleDialog;)Landroid/widget/Toast;

    move-result-object v3

    if-eqz v3, :cond_2

    .line 121
    iget-object v3, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog$1;->this$0:Lcom/samsung/musicplus/dialog/EditTitleDialog;

    # getter for: Lcom/samsung/musicplus/dialog/EditTitleDialog;->mToast:Landroid/widget/Toast;
    invoke-static {v3}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->access$000(Lcom/samsung/musicplus/dialog/EditTitleDialog;)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/widget/Toast;->setText(I)V

    .line 122
    iget-object v3, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog$1;->this$0:Lcom/samsung/musicplus/dialog/EditTitleDialog;

    # getter for: Lcom/samsung/musicplus/dialog/EditTitleDialog;->mToast:Landroid/widget/Toast;
    invoke-static {v3}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->access$000(Lcom/samsung/musicplus/dialog/EditTitleDialog;)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 124
    :cond_2
    const/4 v1, 0x0

    .line 125
    invoke-static {}, Lcom/samsung/musicplus/util/EmojiList;->getPattern()Ljava/util/regex/Pattern;

    move-result-object v1

    .line 126
    const/4 v0, 0x0

    .line 127
    invoke-virtual {v1, v2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 128
    const-string v3, ""

    invoke-virtual {v0, v3}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    .line 130
    :cond_3
    return-object v2
.end method
