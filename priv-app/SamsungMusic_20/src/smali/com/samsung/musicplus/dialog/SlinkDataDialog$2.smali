.class Lcom/samsung/musicplus/dialog/SlinkDataDialog$2;
.super Ljava/lang/Object;
.source "SlinkDataDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/dialog/SlinkDataDialog;->getSlinkChargeDialog()Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/dialog/SlinkDataDialog;

.field final synthetic val$checkBox:Landroid/widget/CheckBox;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/dialog/SlinkDataDialog;Landroid/widget/CheckBox;)V
    .locals 0

    .prologue
    .line 98
    iput-object p1, p0, Lcom/samsung/musicplus/dialog/SlinkDataDialog$2;->this$0:Lcom/samsung/musicplus/dialog/SlinkDataDialog;

    iput-object p2, p0, Lcom/samsung/musicplus/dialog/SlinkDataDialog$2;->val$checkBox:Landroid/widget/CheckBox;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    .line 101
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/SlinkDataDialog$2;->this$0:Lcom/samsung/musicplus/dialog/SlinkDataDialog;

    invoke-virtual {v2}, Lcom/samsung/musicplus/dialog/SlinkDataDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "device_name"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 102
    .local v1, "name":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/SlinkDataDialog$2;->this$0:Lcom/samsung/musicplus/dialog/SlinkDataDialog;

    invoke-virtual {v2}, Lcom/samsung/musicplus/dialog/SlinkDataDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "device_id"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 103
    .local v0, "id":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/SlinkDataDialog$2;->this$0:Lcom/samsung/musicplus/dialog/SlinkDataDialog;

    invoke-virtual {v2}, Lcom/samsung/musicplus/dialog/SlinkDataDialog;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const v3, 0x2000d

    invoke-static {v2, v1, v3, v0}, Lcom/samsung/musicplus/contents/devices/DeviceListFragment;->launchTrackActivity(Landroid/app/Activity;Ljava/lang/String;ILjava/lang/String;)V

    .line 105
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/SlinkDataDialog$2;->val$checkBox:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 107
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/SlinkDataDialog$2;->this$0:Lcom/samsung/musicplus/dialog/SlinkDataDialog;

    iget-object v3, p0, Lcom/samsung/musicplus/dialog/SlinkDataDialog$2;->this$0:Lcom/samsung/musicplus/dialog/SlinkDataDialog;

    invoke-virtual {v3}, Lcom/samsung/musicplus/dialog/SlinkDataDialog;->getActivity()Landroid/app/Activity;

    move-result-object v3

    # invokes: Lcom/samsung/musicplus/dialog/SlinkDataDialog;->saveAccept(Landroid/app/Activity;)V
    invoke-static {v2, v3}, Lcom/samsung/musicplus/dialog/SlinkDataDialog;->access$000(Lcom/samsung/musicplus/dialog/SlinkDataDialog;Landroid/app/Activity;)V

    .line 109
    :cond_0
    return-void
.end method
