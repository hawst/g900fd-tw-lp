.class Lcom/samsung/musicplus/dialog/DataCheckDialog$1;
.super Ljava/lang/Object;
.source "DataCheckDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/dialog/DataCheckDialog;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/dialog/DataCheckDialog;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/dialog/DataCheckDialog;)V
    .locals 0

    .prologue
    .line 201
    iput-object p1, p0, Lcom/samsung/musicplus/dialog/DataCheckDialog$1;->this$0:Lcom/samsung/musicplus/dialog/DataCheckDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 8
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 204
    iget-object v3, p0, Lcom/samsung/musicplus/dialog/DataCheckDialog$1;->this$0:Lcom/samsung/musicplus/dialog/DataCheckDialog;

    invoke-virtual {v3}, Lcom/samsung/musicplus/dialog/DataCheckDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 205
    .local v0, "a":Landroid/app/Activity;
    const-string v3, "data_check_help"

    invoke-virtual {v0, v3, v5}, Landroid/app/Activity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 207
    .local v2, "sp":Landroid/content/SharedPreferences;
    # getter for: Lcom/samsung/musicplus/dialog/DataCheckDialog;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/dialog/DataCheckDialog;->access$000()Ljava/lang/String;

    move-result-object v3

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "DataCheckDialog() : mPrefKey="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/musicplus/dialog/DataCheckDialog$1;->this$0:Lcom/samsung/musicplus/dialog/DataCheckDialog;

    # getter for: Lcom/samsung/musicplus/dialog/DataCheckDialog;->mPrefKey:Ljava/lang/String;
    invoke-static {v7}, Lcom/samsung/musicplus/dialog/DataCheckDialog;->access$100(Lcom/samsung/musicplus/dialog/DataCheckDialog;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    iget-object v3, p0, Lcom/samsung/musicplus/dialog/DataCheckDialog$1;->this$0:Lcom/samsung/musicplus/dialog/DataCheckDialog;

    # getter for: Lcom/samsung/musicplus/dialog/DataCheckDialog;->mPrefKey:Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/musicplus/dialog/DataCheckDialog;->access$100(Lcom/samsung/musicplus/dialog/DataCheckDialog;)Ljava/lang/String;

    move-result-object v7

    iget-object v3, p0, Lcom/samsung/musicplus/dialog/DataCheckDialog$1;->this$0:Lcom/samsung/musicplus/dialog/DataCheckDialog;

    # getter for: Lcom/samsung/musicplus/dialog/DataCheckDialog;->mIsChecked:Z
    invoke-static {v3}, Lcom/samsung/musicplus/dialog/DataCheckDialog;->access$200(Lcom/samsung/musicplus/dialog/DataCheckDialog;)Z

    move-result v3

    if-nez v3, :cond_1

    move v3, v4

    :goto_0
    invoke-interface {v6, v7, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 210
    iget-object v3, p0, Lcom/samsung/musicplus/dialog/DataCheckDialog$1;->this$0:Lcom/samsung/musicplus/dialog/DataCheckDialog;

    # getter for: Lcom/samsung/musicplus/dialog/DataCheckDialog;->mPrefKey:Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/musicplus/dialog/DataCheckDialog;->access$100(Lcom/samsung/musicplus/dialog/DataCheckDialog;)Ljava/lang/String;

    move-result-object v3

    const-string v6, "data_check_help_nearby_item"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 211
    # getter for: Lcom/samsung/musicplus/dialog/DataCheckDialog;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/dialog/DataCheckDialog;->access$000()Ljava/lang/String;

    move-result-object v3

    const-string v6, "DataCheckDialog() : list instanceof CommonListFragment"

    invoke-static {v3, v6}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    new-instance v1, Landroid/content/Intent;

    const-class v3, Lcom/samsung/musicplus/contents/TrackActivity;

    invoke-direct {v1, v0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 214
    .local v1, "i":Landroid/content/Intent;
    const-string v3, "list_type"

    const v6, 0x2000b

    invoke-virtual {v1, v3, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 216
    const-string v3, "keyword"

    iget-object v6, p0, Lcom/samsung/musicplus/dialog/DataCheckDialog$1;->this$0:Lcom/samsung/musicplus/dialog/DataCheckDialog;

    # getter for: Lcom/samsung/musicplus/dialog/DataCheckDialog;->mKeyWord:Ljava/lang/String;
    invoke-static {v6}, Lcom/samsung/musicplus/dialog/DataCheckDialog;->access$300(Lcom/samsung/musicplus/dialog/DataCheckDialog;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v3, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 217
    const-string v3, "track_title"

    iget-object v6, p0, Lcom/samsung/musicplus/dialog/DataCheckDialog$1;->this$0:Lcom/samsung/musicplus/dialog/DataCheckDialog;

    # getter for: Lcom/samsung/musicplus/dialog/DataCheckDialog;->mTitle:Ljava/lang/String;
    invoke-static {v6}, Lcom/samsung/musicplus/dialog/DataCheckDialog;->access$400(Lcom/samsung/musicplus/dialog/DataCheckDialog;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v3, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 218
    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 220
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    const-string v6, "data_check_help_nearby_item"

    iget-object v7, p0, Lcom/samsung/musicplus/dialog/DataCheckDialog$1;->this$0:Lcom/samsung/musicplus/dialog/DataCheckDialog;

    # getter for: Lcom/samsung/musicplus/dialog/DataCheckDialog;->mIsChecked:Z
    invoke-static {v7}, Lcom/samsung/musicplus/dialog/DataCheckDialog;->access$200(Lcom/samsung/musicplus/dialog/DataCheckDialog;)Z

    move-result v7

    if-nez v7, :cond_2

    :goto_1
    invoke-interface {v3, v6, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 227
    .end local v0    # "a":Landroid/app/Activity;
    .end local v1    # "i":Landroid/content/Intent;
    :cond_0
    :goto_2
    iget-object v3, p0, Lcom/samsung/musicplus/dialog/DataCheckDialog$1;->this$0:Lcom/samsung/musicplus/dialog/DataCheckDialog;

    invoke-virtual {v3}, Lcom/samsung/musicplus/dialog/DataCheckDialog;->dismiss()V

    .line 228
    return-void

    .restart local v0    # "a":Landroid/app/Activity;
    :cond_1
    move v3, v5

    .line 208
    goto :goto_0

    .restart local v1    # "i":Landroid/content/Intent;
    :cond_2
    move v4, v5

    .line 220
    goto :goto_1

    .line 222
    .end local v1    # "i":Landroid/content/Intent;
    :cond_3
    iget-object v3, p0, Lcom/samsung/musicplus/dialog/DataCheckDialog$1;->this$0:Lcom/samsung/musicplus/dialog/DataCheckDialog;

    # getter for: Lcom/samsung/musicplus/dialog/DataCheckDialog;->mPrefKey:Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/musicplus/dialog/DataCheckDialog;->access$100(Lcom/samsung/musicplus/dialog/DataCheckDialog;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "data_check_help_dlna"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 223
    instance-of v3, v0, Lcom/samsung/musicplus/player/PlayerActivity;

    if-eqz v3, :cond_0

    .line 224
    check-cast v0, Lcom/samsung/musicplus/player/PlayerActivity;

    .end local v0    # "a":Landroid/app/Activity;
    invoke-virtual {v0}, Lcom/samsung/musicplus/player/PlayerActivity;->doAllShareButton()V

    goto :goto_2
.end method
