.class Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment$2;
.super Landroid/content/BroadcastReceiver;
.source "KnoxMoveDialogFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;)V
    .locals 0

    .prologue
    .line 193
    iput-object p1, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment$2;->this$0:Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 197
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 198
    .local v0, "action":Ljava/lang/String;
    const-string v5, "PACKAGENAME"

    invoke-virtual {p2, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 200
    .local v4, "packageName":Ljava/lang/String;
    const-string v5, "KnoxModeManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "mKnoxReceiver, packageName : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " action : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 203
    const-string v5, "com.sec.knox.container.FileRelayComplete"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 204
    iget-object v5, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment$2;->this$0:Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;

    # getter for: Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mKnoxMoveOperation:Lcom/samsung/musicplus/contents/MoveKnoxPersonalModeOperation;
    invoke-static {v5}, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->access$000(Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;)Lcom/samsung/musicplus/contents/MoveKnoxPersonalModeOperation;

    move-result-object v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment$2;->this$0:Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;

    # getter for: Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mKnoxMoveOperation:Lcom/samsung/musicplus/contents/MoveKnoxPersonalModeOperation;
    invoke-static {v5}, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->access$000(Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;)Lcom/samsung/musicplus/contents/MoveKnoxPersonalModeOperation;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/musicplus/contents/MoveKnoxPersonalModeOperation;->isOperationCanceled()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 235
    :cond_0
    :goto_0
    return-void

    .line 207
    :cond_1
    iget-object v5, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment$2;->this$0:Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;

    # invokes: Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->endOperation()V
    invoke-static {v5}, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->access$100(Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;)V

    .line 210
    iget-object v5, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment$2;->this$0:Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;

    # getter for: Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mKnoxMoveOperation:Lcom/samsung/musicplus/contents/MoveKnoxPersonalModeOperation;
    invoke-static {v5}, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->access$000(Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;)Lcom/samsung/musicplus/contents/MoveKnoxPersonalModeOperation;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/musicplus/contents/MoveKnoxPersonalModeOperation;->isFailed()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 211
    iget-object v5, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment$2;->this$0:Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;

    invoke-virtual {v5}, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment$2;->this$0:Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;

    # getter for: Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mKnoxMoveOperation:Lcom/samsung/musicplus/contents/MoveKnoxPersonalModeOperation;
    invoke-static {v6}, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->access$000(Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;)Lcom/samsung/musicplus/contents/MoveKnoxPersonalModeOperation;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/musicplus/contents/MoveKnoxPersonalModeOperation;->getErrorMsg()I

    move-result v6

    invoke-virtual {v5, v6}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 212
    .local v3, "msg":Ljava/lang/String;
    const-string v5, "KnoxModeManager"

    const-string v6, "mKnoxReceiver : COMPLETE but, fail occurs"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 223
    :goto_1
    iget-object v5, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment$2;->this$0:Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;

    # invokes: Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->showToast(Ljava/lang/String;)V
    invoke-static {v5, v3}, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->access$200(Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;Ljava/lang/String;)V

    goto :goto_0

    .line 214
    .end local v3    # "msg":Ljava/lang/String;
    :cond_2
    const-string v5, "SUCCESSCNT"

    invoke-virtual {p2, v5, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 216
    .local v1, "cnt":I
    if-ne v1, v8, :cond_3

    .line 217
    iget-object v5, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment$2;->this$0:Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;

    invoke-virtual {v5}, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    const v6, 0x7f1000cf

    invoke-virtual {v5, v6}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 221
    .restart local v3    # "msg":Ljava/lang/String;
    :goto_2
    const-string v5, "KnoxModeManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "mKnoxReceiver : COMPLETE, success count : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 219
    .end local v3    # "msg":Ljava/lang/String;
    :cond_3
    iget-object v5, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment$2;->this$0:Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;

    invoke-virtual {v5}, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    const v6, 0x7f1000d0

    new-array v7, v8, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-virtual {v5, v6, v7}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .restart local v3    # "msg":Ljava/lang/String;
    goto :goto_2

    .line 224
    .end local v1    # "cnt":I
    .end local v3    # "msg":Ljava/lang/String;
    :cond_4
    const-string v5, "com.sec.knox.container.FileRelayDone"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 225
    iget-object v5, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment$2;->this$0:Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;

    # getter for: Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mKnoxMoveOperation:Lcom/samsung/musicplus/contents/MoveKnoxPersonalModeOperation;
    invoke-static {v5}, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->access$000(Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;)Lcom/samsung/musicplus/contents/MoveKnoxPersonalModeOperation;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment$2;->this$0:Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;

    # getter for: Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mListItems:[J
    invoke-static {v6}, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->access$300(Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;)[J

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment$2;->this$0:Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;

    # getter for: Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mSuccessCount:I
    invoke-static {v7}, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->access$400(Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;)I

    move-result v7

    aget-wide v6, v6, v7

    invoke-virtual {v5, v6, v7}, Lcom/samsung/musicplus/contents/MoveKnoxPersonalModeOperation;->deleteListItemInDB(J)V

    .line 226
    iget-object v5, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment$2;->this$0:Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;

    # ++operator for: Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mSuccessCount:I
    invoke-static {v5}, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->access$404(Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;)I

    .line 227
    iget-object v5, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment$2;->this$0:Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;

    iget-object v6, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment$2;->this$0:Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;

    # getter for: Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mSuccessCount:I
    invoke-static {v6}, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->access$400(Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;)I

    move-result v6

    # invokes: Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->refreshProgressBar(I)V
    invoke-static {v5, v6}, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->access$500(Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;I)V

    .line 228
    const-string v5, "KnoxModeManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "mKnoxReceiver : DONE - "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment$2;->this$0:Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;

    # getter for: Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mSuccessCount:I
    invoke-static {v7}, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->access$400(Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;)I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 229
    :cond_5
    const-string v5, "com.sec.knox.container.FileRelayFail"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 230
    const-string v5, "ERRORCODE"

    invoke-virtual {p2, v5, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 231
    .local v2, "error":I
    iget-object v5, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment$2;->this$0:Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;

    # getter for: Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mKnoxMoveOperation:Lcom/samsung/musicplus/contents/MoveKnoxPersonalModeOperation;
    invoke-static {v5}, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->access$000(Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;)Lcom/samsung/musicplus/contents/MoveKnoxPersonalModeOperation;

    move-result-object v5

    invoke-virtual {v5, v2}, Lcom/samsung/musicplus/contents/MoveKnoxPersonalModeOperation;->setErrorCode(I)V

    .line 232
    const-string v5, "KnoxModeManager"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "mKnoxReceiver : FAIL - "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method
