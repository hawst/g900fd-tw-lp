.class public Lcom/samsung/musicplus/dialog/LowMemoryPopup;
.super Ljava/lang/Object;
.source "LowMemoryPopup.java"


# static fields
.field private static final LOW_STORAGE_THRESHOLD:J = 0xa00000L

.field private static final TAG:Ljava/lang/String;

.field private static mContext:Landroid/content/Context;


# instance fields
.field private isRegistered:Ljava/lang/Boolean;

.field private mMemoryMessageBox:Landroid/app/AlertDialog;

.field private final mSystemReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-class v0, Lcom/samsung/musicplus/dialog/LowMemoryPopup;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/dialog/LowMemoryPopup;->TAG:Ljava/lang/String;

    .line 24
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/musicplus/dialog/LowMemoryPopup;->mContext:Landroid/content/Context;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/musicplus/dialog/LowMemoryPopup;->isRegistered:Ljava/lang/Boolean;

    .line 44
    new-instance v1, Lcom/samsung/musicplus/dialog/LowMemoryPopup$1;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/dialog/LowMemoryPopup$1;-><init>(Lcom/samsung/musicplus/dialog/LowMemoryPopup;)V

    iput-object v1, p0, Lcom/samsung/musicplus/dialog/LowMemoryPopup;->mSystemReceiver:Landroid/content/BroadcastReceiver;

    .line 66
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/musicplus/dialog/LowMemoryPopup;->mMemoryMessageBox:Landroid/app/AlertDialog;

    .line 31
    sput-object p1, Lcom/samsung/musicplus/dialog/LowMemoryPopup;->mContext:Landroid/content/Context;

    .line 32
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 33
    .local v0, "f":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.MEDIA_SCANNER_FINISHED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 34
    iget-object v1, p0, Lcom/samsung/musicplus/dialog/LowMemoryPopup;->mSystemReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 35
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/musicplus/dialog/LowMemoryPopup;->isRegistered:Ljava/lang/Boolean;

    .line 36
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/dialog/LowMemoryPopup;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/LowMemoryPopup;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/LowMemoryPopup;->mMemoryMessageBox:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$100()Landroid/content/Context;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/samsung/musicplus/dialog/LowMemoryPopup;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200()Ljava/lang/String;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/samsung/musicplus/dialog/LowMemoryPopup;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method public static getAvailableInternalMemorySize()Ljava/lang/Long;
    .locals 8

    .prologue
    .line 59
    invoke-static {}, Landroid/os/Environment;->getDataDirectory()Ljava/io/File;

    move-result-object v4

    .line 60
    .local v4, "path":Ljava/io/File;
    new-instance v5, Landroid/os/StatFs;

    invoke-virtual {v4}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/os/StatFs;-><init>(Ljava/lang/String;)V

    .line 61
    .local v5, "stat":Landroid/os/StatFs;
    invoke-virtual {v5}, Landroid/os/StatFs;->getBlockSize()I

    move-result v6

    int-to-long v2, v6

    .line 62
    .local v2, "blockSize":J
    invoke-virtual {v5}, Landroid/os/StatFs;->getAvailableBlocks()I

    move-result v6

    int-to-long v0, v6

    .line 63
    .local v0, "availableBlocks":J
    mul-long v6, v0, v2

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    return-object v6
.end method


# virtual methods
.method public checkLowMemory()V
    .locals 6

    .prologue
    .line 52
    invoke-static {}, Lcom/samsung/musicplus/dialog/LowMemoryPopup;->getAvailableInternalMemorySize()Ljava/lang/Long;

    move-result-object v0

    .line 53
    .local v0, "availableSize":Ljava/lang/Long;
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/32 v4, 0xa00000

    cmp-long v1, v2, v4

    if-gez v1, :cond_0

    .line 54
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/LowMemoryPopup;->showLowMemoryMsg()V

    .line 56
    :cond_0
    return-void
.end method

.method public showLowMemoryMsg()V
    .locals 4

    .prologue
    .line 69
    sget-object v1, Lcom/samsung/musicplus/dialog/LowMemoryPopup;->TAG:Ljava/lang/String;

    const-string v2, "showLowMemoryMsg() is called"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    new-instance v0, Landroid/app/AlertDialog$Builder;

    sget-object v1, Lcom/samsung/musicplus/dialog/LowMemoryPopup;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 71
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    new-instance v1, Lcom/samsung/musicplus/dialog/LowMemoryPopup$2;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/dialog/LowMemoryPopup$2;-><init>(Lcom/samsung/musicplus/dialog/LowMemoryPopup;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 81
    new-instance v1, Lcom/samsung/musicplus/dialog/LowMemoryPopup$3;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/dialog/LowMemoryPopup$3;-><init>(Lcom/samsung/musicplus/dialog/LowMemoryPopup;)V

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 89
    const v1, 0x7f1001c8

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 90
    const v1, 0x7f10010b

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f100114

    new-instance v3, Lcom/samsung/musicplus/dialog/LowMemoryPopup$5;

    invoke-direct {v3, p0}, Lcom/samsung/musicplus/dialog/LowMemoryPopup$5;-><init>(Lcom/samsung/musicplus/dialog/LowMemoryPopup;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f100093

    new-instance v3, Lcom/samsung/musicplus/dialog/LowMemoryPopup$4;

    invoke-direct {v3, p0}, Lcom/samsung/musicplus/dialog/LowMemoryPopup$4;-><init>(Lcom/samsung/musicplus/dialog/LowMemoryPopup;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 114
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/musicplus/dialog/LowMemoryPopup;->mMemoryMessageBox:Landroid/app/AlertDialog;

    .line 115
    iget-object v1, p0, Lcom/samsung/musicplus/dialog/LowMemoryPopup;->mMemoryMessageBox:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 116
    return-void
.end method

.method public unregisterSystemReceiver(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 39
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/LowMemoryPopup;->isRegistered:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 40
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/LowMemoryPopup;->mSystemReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 41
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/dialog/LowMemoryPopup;->isRegistered:Ljava/lang/Boolean;

    .line 43
    :cond_0
    return-void
.end method
