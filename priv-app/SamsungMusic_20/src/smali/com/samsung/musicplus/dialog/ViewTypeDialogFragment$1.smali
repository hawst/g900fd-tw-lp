.class Lcom/samsung/musicplus/dialog/ViewTypeDialogFragment$1;
.super Ljava/lang/Object;
.source "ViewTypeDialogFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/dialog/ViewTypeDialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/dialog/ViewTypeDialogFragment;

.field final synthetic val$cs:[Ljava/lang/CharSequence;

.field final synthetic val$viewType:I


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/dialog/ViewTypeDialogFragment;[Ljava/lang/CharSequence;I)V
    .locals 0

    .prologue
    .line 43
    iput-object p1, p0, Lcom/samsung/musicplus/dialog/ViewTypeDialogFragment$1;->this$0:Lcom/samsung/musicplus/dialog/ViewTypeDialogFragment;

    iput-object p2, p0, Lcom/samsung/musicplus/dialog/ViewTypeDialogFragment$1;->val$cs:[Ljava/lang/CharSequence;

    iput p3, p0, Lcom/samsung/musicplus/dialog/ViewTypeDialogFragment$1;->val$viewType:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 46
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/ViewTypeDialogFragment$1;->this$0:Lcom/samsung/musicplus/dialog/ViewTypeDialogFragment;

    iget-object v3, p0, Lcom/samsung/musicplus/dialog/ViewTypeDialogFragment$1;->val$cs:[Ljava/lang/CharSequence;

    aget-object v3, v3, p2

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    # invokes: Lcom/samsung/musicplus/dialog/ViewTypeDialogFragment;->getSelectedType(Ljava/lang/String;)I
    invoke-static {v2, v3}, Lcom/samsung/musicplus/dialog/ViewTypeDialogFragment;->access$000(Lcom/samsung/musicplus/dialog/ViewTypeDialogFragment;Ljava/lang/String;)I

    move-result v1

    .line 47
    .local v1, "selectedType":I
    iget v2, p0, Lcom/samsung/musicplus/dialog/ViewTypeDialogFragment$1;->val$viewType:I

    if-eq v1, v2, :cond_0

    .line 48
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/ViewTypeDialogFragment$1;->this$0:Lcom/samsung/musicplus/dialog/ViewTypeDialogFragment;

    invoke-virtual {v2}, Lcom/samsung/musicplus/dialog/ViewTypeDialogFragment;->getTargetFragment()Landroid/app/Fragment;

    move-result-object v0

    .line 49
    .local v0, "fg":Landroid/app/Fragment;
    instance-of v2, v0, Lcom/samsung/musicplus/dialog/ViewTypeDialogFragment$ViewTypeChangeListener;

    if-eqz v2, :cond_0

    .line 50
    check-cast v0, Lcom/samsung/musicplus/dialog/ViewTypeDialogFragment$ViewTypeChangeListener;

    .end local v0    # "fg":Landroid/app/Fragment;
    invoke-interface {v0, v1}, Lcom/samsung/musicplus/dialog/ViewTypeDialogFragment$ViewTypeChangeListener;->onChangeViewType(I)V

    .line 53
    :cond_0
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 54
    return-void
.end method
