.class Lcom/samsung/musicplus/dialog/ChangeDeviceDialog$2;
.super Landroid/content/BroadcastReceiver;
.source "ChangeDeviceDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;)V
    .locals 0

    .prologue
    .line 285
    iput-object p1, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog$2;->this$0:Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 288
    const-string v2, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 289
    const-string v2, "wifi_state"

    const/4 v3, 0x4

    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 291
    .local v1, "state":I
    if-ne v1, v4, :cond_3

    iget-object v2, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog$2;->this$0:Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;

    # getter for: Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mWifiState:I
    invoke-static {v2}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->access$300(Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;)I

    move-result v2

    if-eq v2, v4, :cond_3

    .line 293
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog$2;->this$0:Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;

    # getter for: Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mAlertDialog:Landroid/app/AlertDialog;
    invoke-static {v2}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->access$400(Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;)Landroid/app/AlertDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog;->dismiss()V

    .line 294
    sget-boolean v2, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_SUPPORT_WIFI_DISPLAY:Z

    if-eqz v2, :cond_2

    .line 295
    sget-boolean v2, Lcom/samsung/musicplus/util/Features;->FLAG_SUPPORT_CHINA_WLAN:Z

    if-eqz v2, :cond_1

    const v0, 0x7f1001b6

    .line 296
    .local v0, "messageId":I
    :goto_0
    new-instance v2, Lcom/samsung/musicplus/dialog/NotiDialog;

    const v3, 0x7f1001c9

    invoke-direct {v2, v3, v0}, Lcom/samsung/musicplus/dialog/NotiDialog;-><init>(II)V

    iget-object v3, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog$2;->this$0:Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;

    invoke-virtual {v3}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    invoke-virtual {v2, v3, v5}, Lcom/samsung/musicplus/dialog/NotiDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 308
    .end local v0    # "messageId":I
    .end local v1    # "state":I
    :cond_0
    :goto_1
    return-void

    .line 295
    .restart local v1    # "state":I
    :cond_1
    const v0, 0x7f1001b5

    goto :goto_0

    .line 299
    :cond_2
    new-instance v2, Lcom/samsung/musicplus/dialog/NotiDialog;

    const v3, 0x7f1001b8

    const v4, 0x7f1001b7

    invoke-direct {v2, v3, v4}, Lcom/samsung/musicplus/dialog/NotiDialog;-><init>(II)V

    iget-object v3, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog$2;->this$0:Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;

    invoke-virtual {v3}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    invoke-virtual {v2, v3, v5}, Lcom/samsung/musicplus/dialog/NotiDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_1

    .line 303
    :cond_3
    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    .line 304
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog$2;->this$0:Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;

    # setter for: Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mWifiState:I
    invoke-static {v2, v1}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->access$302(Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;I)I

    .line 305
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog$2;->this$0:Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;

    iget-object v3, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog$2;->this$0:Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;

    # getter for: Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mListView:Landroid/widget/ListView;
    invoke-static {v3}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->access$100(Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;)Landroid/widget/ListView;

    move-result-object v3

    # invokes: Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->setAdapter(Landroid/widget/ListView;)V
    invoke-static {v2, v3}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->access$200(Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;Landroid/widget/ListView;)V

    goto :goto_1
.end method
