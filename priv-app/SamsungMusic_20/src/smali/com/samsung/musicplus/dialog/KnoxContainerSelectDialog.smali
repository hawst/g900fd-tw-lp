.class public Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog;
.super Landroid/app/DialogFragment;
.source "KnoxContainerSelectDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog$ContainerListAdapater;,
        Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog$ContainerInfo;
    }
.end annotation


# static fields
.field private static final LIST_ITEMS:Ljava/lang/String; = "list_items"

.field private static final TAG:Ljava/lang/String; = "KnoxModeManager"

.field private static final mContainerIcon:[I


# instance fields
.field private mArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog$ContainerInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mListItems:[J

.field private mListener:Landroid/widget/AdapterView$OnItemClickListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 99
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog;->mContainerIcon:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x7f02001e
        0x7f02001f
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 175
    new-instance v0, Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog$1;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog$1;-><init>(Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog;)V

    iput-object v0, p0, Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog;->mListener:Landroid/widget/AdapterView$OnItemClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog;->mArray:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$002(Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog;
    .param p1, "x1"    # Ljava/util/ArrayList;

    .prologue
    .line 47
    iput-object p1, p0, Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog;->mArray:Ljava/util/ArrayList;

    return-object p1
.end method

.method static synthetic access$100()[I
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog;->mContainerIcon:[I

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog;)[J
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog;->mListItems:[J

    return-object v0
.end method

.method public static getInstance([J)Landroid/app/DialogFragment;
    .locals 3
    .param p0, "items"    # [J

    .prologue
    .line 56
    new-instance v1, Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog;

    invoke-direct {v1}, Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog;-><init>()V

    .line 57
    .local v1, "selectDialog":Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 58
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "list_items"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putLongArray(Ljava/lang/String;[J)V

    .line 59
    invoke-virtual {v1, v0}, Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog;->setArguments(Landroid/os/Bundle;)V

    .line 61
    return-object v1
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 10
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v7, 0x0

    .line 66
    const-string v8, "KnoxModeManager"

    const-string v9, "KnoxContainerSelectDialog create!"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    .line 69
    .local v3, "data":Landroid/os/Bundle;
    const-string v8, "list_items"

    invoke-virtual {v3, v8}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v8

    iput-object v8, p0, Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog;->mListItems:[J

    .line 71
    iget-object v8, p0, Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog;->mListItems:[J

    if-nez v8, :cond_0

    .line 72
    const-string v8, "KnoxModeManager"

    const-string v9, "mListItems is null"

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    :goto_0
    return-object v7

    .line 75
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 76
    .local v0, "a":Landroid/app/Activity;
    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 78
    .local v2, "context":Landroid/content/Context;
    const/4 v8, 0x1

    invoke-virtual {p0, v8}, Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog;->setCancelable(Z)V

    .line 79
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 81
    .local v1, "builder":Landroid/app/AlertDialog$Builder;
    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v5

    .line 82
    .local v5, "inflater":Landroid/view/LayoutInflater;
    const v8, 0x7f040085

    invoke-virtual {v5, v8, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/FrameLayout;

    .line 83
    .local v6, "view":Landroid/widget/FrameLayout;
    invoke-virtual {v1, v6}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 85
    invoke-static {}, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;->getCallingUserrId()I

    move-result v7

    if-nez v7, :cond_1

    .line 86
    const v7, 0x7f10009f

    invoke-virtual {v1, v7}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 91
    :goto_1
    const v7, 0x7f0d0191

    invoke-virtual {v6, v7}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/GridView;

    .line 92
    .local v4, "gridview":Landroid/widget/GridView;
    new-instance v7, Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog$ContainerListAdapater;

    invoke-static {v2}, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;->getPersonaList(Landroid/content/Context;)Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager$PersonaInfoListCompat;

    move-result-object v8

    invoke-direct {v7, p0, v2, v8}, Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog$ContainerListAdapater;-><init>(Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog;Landroid/content/Context;Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager$PersonaInfoListCompat;)V

    invoke-virtual {v4, v7}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 94
    iget-object v7, p0, Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog;->mListener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v4, v7}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 96
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v7

    goto :goto_0

    .line 88
    .end local v4    # "gridview":Landroid/widget/GridView;
    :cond_1
    const v7, 0x7f1000a0

    invoke-virtual {v1, v7}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    goto :goto_1
.end method
