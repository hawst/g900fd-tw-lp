.class Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTask;
.super Landroid/os/AsyncTask;
.source "DeleteSlinkContentsDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DeleteTask"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTask$DeleteItems;,
        Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTask$OnTaskCompleteListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTask$DeleteItems;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# static fields
.field private static sDeleteTask:Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTask;


# instance fields
.field private mCompleteListener:Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTask$OnTaskCompleteListener;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 203
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 204
    return-void
.end method

.method static getInstance()Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTask;
    .locals 1

    .prologue
    .line 189
    sget-object v0, Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTask;->sDeleteTask:Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTask;

    if-nez v0, :cond_0

    .line 190
    new-instance v0, Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTask;

    invoke-direct {v0}, Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTask;-><init>()V

    sput-object v0, Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTask;->sDeleteTask:Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTask;

    .line 192
    :cond_0
    sget-object v0, Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTask;->sDeleteTask:Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTask;

    return-object v0
.end method

.method static recreateTask()Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTask;
    .locals 1

    .prologue
    .line 196
    new-instance v0, Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTask;

    invoke-direct {v0}, Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTask;-><init>()V

    sput-object v0, Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTask;->sDeleteTask:Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTask;

    return-object v0
.end method

.method static releaseTask()V
    .locals 1

    .prologue
    .line 200
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTask;->sDeleteTask:Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTask;

    .line 201
    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 162
    check-cast p1, [Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTask$DeleteItems;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTask;->doInBackground([Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTask$DeleteItems;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTask$DeleteItems;)Ljava/lang/Void;
    .locals 4
    .param p1, "params"    # [Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTask$DeleteItems;

    .prologue
    .line 212
    const/4 v1, 0x0

    aget-object v0, p1, v1

    .line 214
    .local v0, "data":Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTask$DeleteItems;
    iget-object v1, v0, Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTask$DeleteItems;->context:Landroid/content/Context;

    iget-object v2, v0, Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTask$DeleteItems;->items:[J

    invoke-static {v1, v2}, Lcom/samsung/musicplus/provider/MusicContents$Audio$DeviceContents;->delete(Landroid/content/Context;[J)V

    .line 219
    :try_start_0
    iget v1, v0, Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTask$DeleteItems;->throttle:I

    int-to-long v2, v1

    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 222
    :goto_0
    const/4 v1, 0x0

    return-object v1

    .line 220
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 162
    check-cast p1, Ljava/lang/Void;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTask;->onPostExecute(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPostExecute(Ljava/lang/Void;)V
    .locals 1
    .param p1, "result"    # Ljava/lang/Void;

    .prologue
    .line 227
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTask;->mCompleteListener:Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTask$OnTaskCompleteListener;

    if-eqz v0, :cond_0

    .line 228
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTask;->mCompleteListener:Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTask$OnTaskCompleteListener;

    invoke-interface {v0}, Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTask$OnTaskCompleteListener;->onTaskComplete()V

    .line 230
    :cond_0
    return-void
.end method

.method setOnTaskCompleteListener(Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTask$OnTaskCompleteListener;)V
    .locals 0
    .param p1, "l"    # Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTask$OnTaskCompleteListener;

    .prologue
    .line 207
    iput-object p1, p0, Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTask;->mCompleteListener:Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTask$OnTaskCompleteListener;

    .line 208
    return-void
.end method
