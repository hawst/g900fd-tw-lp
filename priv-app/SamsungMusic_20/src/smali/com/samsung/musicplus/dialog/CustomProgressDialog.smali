.class public Lcom/samsung/musicplus/dialog/CustomProgressDialog;
.super Ljava/lang/Object;
.source "CustomProgressDialog.java"


# instance fields
.field private mBuilder:Landroid/app/AlertDialog$Builder;

.field private mCanceledOnTouchOutside:Z

.field private mContext:Landroid/content/Context;

.field private mCountText:Landroid/widget/TextView;

.field private mDialog:Landroid/app/Dialog;

.field private mMaxProgress:I

.field private mPercentage:Landroid/widget/TextView;

.field private mProgressBar:Landroid/widget/ProgressBar;

.field private mTotalCnt:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/samsung/musicplus/dialog/CustomProgressDialog;->mContext:Landroid/content/Context;

    .line 29
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/samsung/musicplus/dialog/CustomProgressDialog;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/musicplus/dialog/CustomProgressDialog;->mBuilder:Landroid/app/AlertDialog$Builder;

    .line 31
    iget-object v1, p0, Lcom/samsung/musicplus/dialog/CustomProgressDialog;->mBuilder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f040003

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 32
    .local v0, "progressView":Landroid/view/View;
    const v1, 0x7f0d0047

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/samsung/musicplus/dialog/CustomProgressDialog;->mPercentage:Landroid/widget/TextView;

    .line 33
    const v1, 0x7f0d0046

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/samsung/musicplus/dialog/CustomProgressDialog;->mCountText:Landroid/widget/TextView;

    .line 34
    const v1, 0x7f0d0045

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    iput-object v1, p0, Lcom/samsung/musicplus/dialog/CustomProgressDialog;->mProgressBar:Landroid/widget/ProgressBar;

    .line 36
    iget-object v1, p0, Lcom/samsung/musicplus/dialog/CustomProgressDialog;->mBuilder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 37
    return-void
.end method


# virtual methods
.method public build()Landroid/app/Dialog;
    .locals 2

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/CustomProgressDialog;->mBuilder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/dialog/CustomProgressDialog;->mDialog:Landroid/app/Dialog;

    .line 47
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/CustomProgressDialog;->mDialog:Landroid/app/Dialog;

    iget-boolean v1, p0, Lcom/samsung/musicplus/dialog/CustomProgressDialog;->mCanceledOnTouchOutside:Z

    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 48
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/CustomProgressDialog;->mDialog:Landroid/app/Dialog;

    return-object v0
.end method

.method public dismiss()V
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/CustomProgressDialog;->mDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/CustomProgressDialog;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 98
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/musicplus/dialog/CustomProgressDialog;->mDialog:Landroid/app/Dialog;

    .line 100
    :cond_0
    return-void
.end method

.method public initCount()V
    .locals 3

    .prologue
    .line 90
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/CustomProgressDialog;->mCountText:Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 93
    :goto_0
    return-void

    .line 92
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/CustomProgressDialog;->mCountText:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/musicplus/dialog/CustomProgressDialog;->mTotalCnt:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public isShowing()Z
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/CustomProgressDialog;->mDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_0

    .line 84
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/CustomProgressDialog;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->isShowing()Z

    move-result v0

    .line 86
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setButton(ILjava/lang/String;Landroid/content/DialogInterface$OnClickListener;)V
    .locals 1
    .param p1, "subject"    # I
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "onClickListener"    # Landroid/content/DialogInterface$OnClickListener;

    .prologue
    .line 40
    const/4 v0, -0x2

    if-ne p1, v0, :cond_0

    .line 41
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/CustomProgressDialog;->mBuilder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, p2, p3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 43
    :cond_0
    return-void
.end method

.method public setCancelable(Z)V
    .locals 1
    .param p1, "flag"    # Z

    .prologue
    .line 107
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/CustomProgressDialog;->mBuilder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 108
    return-void
.end method

.method public setCanceledOnTouchOutside(Z)V
    .locals 0
    .param p1, "cancel"    # Z

    .prologue
    .line 103
    iput-boolean p1, p0, Lcom/samsung/musicplus/dialog/CustomProgressDialog;->mCanceledOnTouchOutside:Z

    .line 104
    return-void
.end method

.method public setCount(I)V
    .locals 3
    .param p1, "cnt"    # I

    .prologue
    .line 77
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/CustomProgressDialog;->mCountText:Landroid/widget/TextView;

    if-nez v0, :cond_0

    .line 80
    :goto_0
    return-void

    .line 79
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/CustomProgressDialog;->mCountText:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/musicplus/dialog/CustomProgressDialog;->mTotalCnt:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public setMax(I)V
    .locals 0
    .param p1, "max"    # I

    .prologue
    .line 60
    iput p1, p0, Lcom/samsung/musicplus/dialog/CustomProgressDialog;->mTotalCnt:I

    .line 61
    return-void
.end method

.method public setMaxProgress(I)V
    .locals 0
    .param p1, "max"    # I

    .prologue
    .line 64
    iput p1, p0, Lcom/samsung/musicplus/dialog/CustomProgressDialog;->mMaxProgress:I

    .line 65
    return-void
.end method

.method public setProgress(I)V
    .locals 4
    .param p1, "progress"    # I

    .prologue
    .line 68
    iget-object v1, p0, Lcom/samsung/musicplus/dialog/CustomProgressDialog;->mPercentage:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/musicplus/dialog/CustomProgressDialog;->mProgressBar:Landroid/widget/ProgressBar;

    if-nez v1, :cond_1

    .line 74
    :cond_0
    :goto_0
    return-void

    .line 70
    :cond_1
    int-to-float v1, p1

    iget v2, p0, Lcom/samsung/musicplus/dialog/CustomProgressDialog;->mMaxProgress:I

    int-to-float v2, v2

    div-float/2addr v1, v2

    const/high16 v2, 0x42c80000    # 100.0f

    mul-float/2addr v1, v2

    float-to-int v0, v1

    .line 72
    .local v0, "percentage":I
    iget-object v1, p0, Lcom/samsung/musicplus/dialog/CustomProgressDialog;->mPercentage:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 73
    iget-object v1, p0, Lcom/samsung/musicplus/dialog/CustomProgressDialog;->mProgressBar:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setProgress(I)V

    goto :goto_0
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/CustomProgressDialog;->mBuilder:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 53
    return-void
.end method

.method public show()V
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/CustomProgressDialog;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 57
    return-void
.end method
