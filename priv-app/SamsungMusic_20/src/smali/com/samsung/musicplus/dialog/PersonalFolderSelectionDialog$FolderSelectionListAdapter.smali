.class Lcom/samsung/musicplus/dialog/PersonalFolderSelectionDialog$FolderSelectionListAdapter;
.super Lcom/samsung/musicplus/widget/list/CommonListAdapter;
.source "PersonalFolderSelectionDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/dialog/PersonalFolderSelectionDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "FolderSelectionListAdapter"
.end annotation


# instance fields
.field private mQueryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILandroid/database/Cursor;Lcom/samsung/musicplus/util/ListUtils$QueryArgs;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "layout"    # I
    .param p3, "c"    # Landroid/database/Cursor;
    .param p4, "info"    # Lcom/samsung/musicplus/util/ListUtils$QueryArgs;
    .param p5, "flags"    # I

    .prologue
    .line 188
    invoke-direct {p0, p1, p2, p3, p5}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;I)V

    .line 189
    iput-object p4, p0, Lcom/samsung/musicplus/dialog/PersonalFolderSelectionDialog$FolderSelectionListAdapter;->mQueryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    .line 190
    invoke-direct {p0, p3}, Lcom/samsung/musicplus/dialog/PersonalFolderSelectionDialog$FolderSelectionListAdapter;->getColumnIndicesAndSetIndex(Landroid/database/Cursor;)V

    .line 191
    return-void
.end method

.method private getColumnIndicesAndSetIndex(Landroid/database/Cursor;)V
    .locals 10
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    const/4 v4, -0x1

    .line 194
    if-eqz p1, :cond_0

    .line 195
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/PersonalFolderSelectionDialog$FolderSelectionListAdapter;->mQueryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    iget-object v0, v0, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->audioIdCol:Ljava/lang/String;

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 196
    .local v1, "audioIdIdx":I
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/PersonalFolderSelectionDialog$FolderSelectionListAdapter;->mQueryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    iget-object v0, v0, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->text1Col:Ljava/lang/String;

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 197
    .local v2, "text1Idx":I
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/PersonalFolderSelectionDialog$FolderSelectionListAdapter;->mQueryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    iget-object v0, v0, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->text2Col:Ljava/lang/String;

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .local v3, "text2Idx":I
    move-object v0, p0

    move v5, v4

    move v6, v4

    move v7, v4

    move v8, v4

    move v9, v4

    .line 199
    invoke-virtual/range {v0 .. v9}, Lcom/samsung/musicplus/dialog/PersonalFolderSelectionDialog$FolderSelectionListAdapter;->setIndex(IIIIIIIII)V

    .line 201
    .end local v1    # "audioIdIdx":I
    .end local v2    # "text1Idx":I
    .end local v3    # "text2Idx":I
    :cond_0
    return-void
.end method


# virtual methods
.method protected bindTextView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "c"    # Landroid/database/Cursor;

    .prologue
    const/4 v6, -0x1

    .line 205
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;

    .line 206
    .local v3, "vh":Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;
    iget-object v4, v3, Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;->text1:Landroid/widget/TextView;

    iget v5, p0, Lcom/samsung/musicplus/dialog/PersonalFolderSelectionDialog$FolderSelectionListAdapter;->mText1Index:I

    invoke-interface {p3, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 207
    iget-object v4, v3, Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;->text2:Landroid/widget/TextView;

    if-eqz v4, :cond_1

    iget v4, p0, Lcom/samsung/musicplus/dialog/PersonalFolderSelectionDialog$FolderSelectionListAdapter;->mText2Index:I

    if-ltz v4, :cond_1

    .line 208
    iget v4, p0, Lcom/samsung/musicplus/dialog/PersonalFolderSelectionDialog$FolderSelectionListAdapter;->mText2Index:I

    invoke-interface {p3, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 209
    .local v2, "text2":Ljava/lang/String;
    const-string v4, ""

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 210
    const-string v4, "/"

    const/4 v5, 0x1

    invoke-virtual {v2, v4, v5}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v1

    .line 211
    .local v1, "start":I
    const-string v4, "/"

    invoke-virtual {v2, v4}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    .line 212
    .local v0, "end":I
    if-eq v1, v6, :cond_0

    if-eq v0, v6, :cond_0

    .line 213
    invoke-virtual {v2, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    .line 216
    .end local v0    # "end":I
    .end local v1    # "start":I
    :cond_0
    iget-object v4, v3, Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;->text2:Landroid/widget/TextView;

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 218
    .end local v2    # "text2":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method public getQueryInfo()Lcom/samsung/musicplus/util/ListUtils$QueryArgs;
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/PersonalFolderSelectionDialog$FolderSelectionListAdapter;->mQueryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    return-object v0
.end method

.method protected setTextAirView(Landroid/view/View;Landroid/widget/TextView;Landroid/widget/TextView;)V
    .locals 0
    .param p1, "v"    # Landroid/view/View;
    .param p2, "text1"    # Landroid/widget/TextView;
    .param p3, "text2"    # Landroid/widget/TextView;

    .prologue
    .line 222
    return-void
.end method
