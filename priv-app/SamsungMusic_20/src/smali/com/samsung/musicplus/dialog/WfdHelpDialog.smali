.class public Lcom/samsung/musicplus/dialog/WfdHelpDialog;
.super Landroid/app/DialogFragment;
.source "WfdHelpDialog.java"


# static fields
.field private static final CLASSNAME:Ljava/lang/String;

.field static final PREF_KEY_WFD_HELP_NEVER_SHOWN:Ljava/lang/String; = "wfd_help_popup_never_shown"

.field static final SELECTED_DEVICE:Ljava/lang/String; = "selected_device"


# instance fields
.field private mCheckBox:Landroid/widget/CheckBox;

.field private mDeviceAddress:Ljava/lang/String;

.field private mIsChecked:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lcom/samsung/musicplus/dialog/WfdHelpDialog;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/dialog/WfdHelpDialog;->CLASSNAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 41
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/dialog/WfdHelpDialog;->mIsChecked:Z

    .line 56
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/dialog/WfdHelpDialog;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/WfdHelpDialog;

    .prologue
    .line 30
    iget-boolean v0, p0, Lcom/samsung/musicplus/dialog/WfdHelpDialog;->mIsChecked:Z

    return v0
.end method

.method static synthetic access$002(Lcom/samsung/musicplus/dialog/WfdHelpDialog;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/WfdHelpDialog;
    .param p1, "x1"    # Z

    .prologue
    .line 30
    iput-boolean p1, p0, Lcom/samsung/musicplus/dialog/WfdHelpDialog;->mIsChecked:Z

    return p1
.end method

.method static synthetic access$100(Lcom/samsung/musicplus/dialog/WfdHelpDialog;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/WfdHelpDialog;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/WfdHelpDialog;->mDeviceAddress:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/musicplus/dialog/WfdHelpDialog;Landroid/content/Context;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/WfdHelpDialog;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Lcom/samsung/musicplus/dialog/WfdHelpDialog;->connectWfdDevice(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$300(Lcom/samsung/musicplus/dialog/WfdHelpDialog;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/WfdHelpDialog;
    .param p1, "x1"    # Z

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/dialog/WfdHelpDialog;->saveHelpNeverShownValue(Z)V

    return-void
.end method

.method private connectWfdDevice(Landroid/content/Context;Ljava/lang/String;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "deviceAddress"    # Ljava/lang/String;

    .prologue
    const/16 v4, 0x8

    .line 131
    const-string v1, "MusicDevice"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "connectWfdDevice deviceAddress : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->changeToWfdDevice()V

    .line 133
    const-string v1, "display"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/display/DisplayManager;

    .line 134
    .local v0, "dm":Landroid/hardware/display/DisplayManager;
    invoke-static {v0, v4, p2}, Lcom/samsung/musicplus/library/wifi/DisplayManagerCompat;->connectWifiDisplayWithMode(Landroid/hardware/display/DisplayManager;ILjava/lang/String;)V

    .line 136
    sget-object v1, Lcom/samsung/musicplus/dialog/WfdHelpDialog;->CLASSNAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "connectWifiDisplayWithMode() deviceAddress : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " connect type : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    return-void
.end method

.method private getAlertDialog(Landroid/app/Activity;)Landroid/app/Dialog;
    .locals 6
    .param p1, "a"    # Landroid/app/Activity;

    .prologue
    .line 85
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 86
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const v4, 0x7f1001c9

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 89
    const-string v4, "layout_inflater"

    invoke-virtual {p1, v4}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 91
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v4, 0x7f040005

    const/4 v5, 0x0

    invoke-virtual {v1, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 92
    .local v2, "root":Landroid/view/View;
    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 94
    const v4, 0x7f0d004b

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 95
    .local v3, "text":Landroid/widget/TextView;
    const v4, 0x7f1001cb

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(I)V

    .line 98
    const v4, 0x7f0d004c

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/CheckBox;

    iput-object v4, p0, Lcom/samsung/musicplus/dialog/WfdHelpDialog;->mCheckBox:Landroid/widget/CheckBox;

    .line 99
    iget-object v4, p0, Lcom/samsung/musicplus/dialog/WfdHelpDialog;->mCheckBox:Landroid/widget/CheckBox;

    iget-boolean v5, p0, Lcom/samsung/musicplus/dialog/WfdHelpDialog;->mIsChecked:Z

    invoke-virtual {v4, v5}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 100
    iget-object v4, p0, Lcom/samsung/musicplus/dialog/WfdHelpDialog;->mCheckBox:Landroid/widget/CheckBox;

    new-instance v5, Lcom/samsung/musicplus/dialog/WfdHelpDialog$1;

    invoke-direct {v5, p0}, Lcom/samsung/musicplus/dialog/WfdHelpDialog$1;-><init>(Lcom/samsung/musicplus/dialog/WfdHelpDialog;)V

    invoke-virtual {v4, v5}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 106
    iget-object v4, p0, Lcom/samsung/musicplus/dialog/WfdHelpDialog;->mCheckBox:Landroid/widget/CheckBox;

    new-instance v5, Lcom/samsung/musicplus/dialog/WfdHelpDialog$2;

    invoke-direct {v5, p0}, Lcom/samsung/musicplus/dialog/WfdHelpDialog$2;-><init>(Lcom/samsung/musicplus/dialog/WfdHelpDialog;)V

    invoke-virtual {v4, v5}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 113
    const v4, 0x7f100060

    new-instance v5, Lcom/samsung/musicplus/dialog/WfdHelpDialog$3;

    invoke-direct {v5, p0}, Lcom/samsung/musicplus/dialog/WfdHelpDialog$3;-><init>(Lcom/samsung/musicplus/dialog/WfdHelpDialog;)V

    invoke-virtual {v0, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 121
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v4

    return-object v4
.end method

.method public static getInstance(Ljava/lang/String;)Landroid/app/DialogFragment;
    .locals 3
    .param p0, "deviceAddress"    # Ljava/lang/String;

    .prologue
    .line 48
    new-instance v1, Lcom/samsung/musicplus/dialog/WfdHelpDialog;

    invoke-direct {v1}, Lcom/samsung/musicplus/dialog/WfdHelpDialog;-><init>()V

    .line 49
    .local v1, "dialog":Landroid/app/DialogFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 50
    .local v0, "data":Landroid/os/Bundle;
    const-string v2, "selected_device"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    invoke-virtual {v1, v0}, Landroid/app/DialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 52
    return-object v1
.end method

.method private saveHelpNeverShownValue(Z)V
    .locals 4
    .param p1, "checked"    # Z

    .prologue
    .line 125
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/WfdHelpDialog;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-string v2, "music_player_setting"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/app/Activity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 127
    .local v0, "sp":Landroid/content/SharedPreferences;
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "wfd_help_popup_never_shown"

    iget-boolean v3, p0, Lcom/samsung/musicplus/dialog/WfdHelpDialog;->mIsChecked:Z

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 128
    return-void
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 60
    if-eqz p1, :cond_0

    .line 61
    const-string v0, "wfd_help_popup_never_shown"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/musicplus/dialog/WfdHelpDialog;->mIsChecked:Z

    .line 62
    const-string v0, "selected_device"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/dialog/WfdHelpDialog;->mDeviceAddress:Ljava/lang/String;

    .line 66
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/WfdHelpDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/dialog/WfdHelpDialog;->getAlertDialog(Landroid/app/Activity;)Landroid/app/Dialog;

    move-result-object v0

    return-object v0

    .line 64
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/WfdHelpDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "selected_device"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/dialog/WfdHelpDialog;->mDeviceAddress:Ljava/lang/String;

    goto :goto_0
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 71
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/WfdHelpDialog;->mCheckBox:Landroid/widget/CheckBox;

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/WfdHelpDialog;->mCheckBox:Landroid/widget/CheckBox;

    const v1, 0x7f10005f

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setText(I)V

    .line 74
    :cond_0
    invoke-super {p0}, Landroid/app/DialogFragment;->onResume()V

    .line 75
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 79
    const-string v0, "wfd_help_popup_never_shown"

    iget-boolean v1, p0, Lcom/samsung/musicplus/dialog/WfdHelpDialog;->mIsChecked:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 80
    const-string v0, "selected_device"

    iget-object v1, p0, Lcom/samsung/musicplus/dialog/WfdHelpDialog;->mDeviceAddress:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 81
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 82
    return-void
.end method
