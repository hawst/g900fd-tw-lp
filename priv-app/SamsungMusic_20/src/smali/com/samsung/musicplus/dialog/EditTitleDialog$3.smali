.class Lcom/samsung/musicplus/dialog/EditTitleDialog$3;
.super Ljava/lang/Object;
.source "EditTitleDialog.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/dialog/EditTitleDialog;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/dialog/EditTitleDialog;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/dialog/EditTitleDialog;)V
    .locals 0

    .prologue
    .line 227
    iput-object p1, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog$3;->this$0:Lcom/samsung/musicplus/dialog/EditTitleDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 7
    .param p1, "arg0"    # Landroid/text/Editable;

    .prologue
    const/16 v4, 0x32

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 241
    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    if-lt v0, v4, :cond_1

    .line 242
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog$3;->this$0:Lcom/samsung/musicplus/dialog/EditTitleDialog;

    # getter for: Lcom/samsung/musicplus/dialog/EditTitleDialog;->mIsMaxName:Z
    invoke-static {v0}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->access$500(Lcom/samsung/musicplus/dialog/EditTitleDialog;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog$3;->this$0:Lcom/samsung/musicplus/dialog/EditTitleDialog;

    # getter for: Lcom/samsung/musicplus/dialog/EditTitleDialog;->mToast:Landroid/widget/Toast;
    invoke-static {v0}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->access$000(Lcom/samsung/musicplus/dialog/EditTitleDialog;)Landroid/widget/Toast;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 243
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog$3;->this$0:Lcom/samsung/musicplus/dialog/EditTitleDialog;

    # getter for: Lcom/samsung/musicplus/dialog/EditTitleDialog;->mToast:Landroid/widget/Toast;
    invoke-static {v0}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->access$000(Lcom/samsung/musicplus/dialog/EditTitleDialog;)Landroid/widget/Toast;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog$3;->this$0:Lcom/samsung/musicplus/dialog/EditTitleDialog;

    const v2, 0x7f1000a5

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    .line 245
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog$3;->this$0:Lcom/samsung/musicplus/dialog/EditTitleDialog;

    # getter for: Lcom/samsung/musicplus/dialog/EditTitleDialog;->mToast:Landroid/widget/Toast;
    invoke-static {v0}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->access$000(Lcom/samsung/musicplus/dialog/EditTitleDialog;)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 247
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog$3;->this$0:Lcom/samsung/musicplus/dialog/EditTitleDialog;

    # setter for: Lcom/samsung/musicplus/dialog/EditTitleDialog;->mIsMaxName:Z
    invoke-static {v0, v6}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->access$502(Lcom/samsung/musicplus/dialog/EditTitleDialog;Z)Z

    .line 251
    :goto_0
    return-void

    .line 249
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog$3;->this$0:Lcom/samsung/musicplus/dialog/EditTitleDialog;

    # setter for: Lcom/samsung/musicplus/dialog/EditTitleDialog;->mIsMaxName:Z
    invoke-static {v0, v5}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->access$502(Lcom/samsung/musicplus/dialog/EditTitleDialog;Z)Z

    goto :goto_0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1, "arg0"    # Ljava/lang/CharSequence;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I
    .param p4, "arg3"    # I

    .prologue
    .line 255
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 3
    .param p1, "text"    # Ljava/lang/CharSequence;
    .param p2, "arg1"    # I
    .param p3, "arg2"    # I
    .param p4, "arg3"    # I

    .prologue
    const/4 v2, -0x1

    .line 231
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 232
    .local v0, "name":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog$3;->this$0:Lcom/samsung/musicplus/dialog/EditTitleDialog;

    # getter for: Lcom/samsung/musicplus/dialog/EditTitleDialog;->mDefaultName:Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->access$300(Lcom/samsung/musicplus/dialog/EditTitleDialog;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog$3;->this$0:Lcom/samsung/musicplus/dialog/EditTitleDialog;

    # getter for: Lcom/samsung/musicplus/dialog/EditTitleDialog;->mDefaultName:Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->access$300(Lcom/samsung/musicplus/dialog/EditTitleDialog;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 233
    :cond_1
    iget-object v1, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog$3;->this$0:Lcom/samsung/musicplus/dialog/EditTitleDialog;

    # getter for: Lcom/samsung/musicplus/dialog/EditTitleDialog;->mDialog:Landroid/app/AlertDialog;
    invoke-static {v1}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->access$400(Lcom/samsung/musicplus/dialog/EditTitleDialog;)Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setEnabled(Z)V

    .line 237
    :goto_0
    return-void

    .line 235
    :cond_2
    iget-object v1, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog$3;->this$0:Lcom/samsung/musicplus/dialog/EditTitleDialog;

    # getter for: Lcom/samsung/musicplus/dialog/EditTitleDialog;->mDialog:Landroid/app/AlertDialog;
    invoke-static {v1}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->access$400(Lcom/samsung/musicplus/dialog/EditTitleDialog;)Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0
.end method
