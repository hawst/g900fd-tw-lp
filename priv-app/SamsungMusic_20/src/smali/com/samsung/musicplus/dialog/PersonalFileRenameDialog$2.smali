.class Lcom/samsung/musicplus/dialog/PersonalFileRenameDialog$2;
.super Ljava/lang/Object;
.source "PersonalFileRenameDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/dialog/PersonalFileRenameDialog;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/dialog/PersonalFileRenameDialog;

.field final synthetic val$mPersonalOperation:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/dialog/PersonalFileRenameDialog;Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;)V
    .locals 0

    .prologue
    .line 92
    iput-object p1, p0, Lcom/samsung/musicplus/dialog/PersonalFileRenameDialog$2;->this$0:Lcom/samsung/musicplus/dialog/PersonalFileRenameDialog;

    iput-object p2, p0, Lcom/samsung/musicplus/dialog/PersonalFileRenameDialog$2;->val$mPersonalOperation:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    .line 96
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/PersonalFileRenameDialog$2;->val$mPersonalOperation:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;

    iget-object v1, p0, Lcom/samsung/musicplus/dialog/PersonalFileRenameDialog$2;->this$0:Lcom/samsung/musicplus/dialog/PersonalFileRenameDialog;

    # getter for: Lcom/samsung/musicplus/dialog/PersonalFileRenameDialog;->mAllAppliedCheck:Z
    invoke-static {v1}, Lcom/samsung/musicplus/dialog/PersonalFileRenameDialog;->access$000(Lcom/samsung/musicplus/dialog/PersonalFileRenameDialog;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->fileRename(Z)V

    .line 98
    if-eqz p1, :cond_0

    .line 99
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 101
    :cond_0
    return-void
.end method
