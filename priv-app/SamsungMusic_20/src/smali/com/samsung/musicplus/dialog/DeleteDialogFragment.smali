.class public Lcom/samsung/musicplus/dialog/DeleteDialogFragment;
.super Landroid/app/DialogFragment;
.source "DeleteDialogFragment.java"


# static fields
.field public static final EXTRA_FINISH:Ljava/lang/String; = "need_finish"

.field public static final EXTRA_LIST_TYPE:Ljava/lang/String; = "list_type"

.field public static final EXTRA_SELECTED:Ljava/lang/String; = "selected_id"

.field public static final EXTRA_TOTAL_COUNT:Ljava/lang/String; = "total_count"


# instance fields
.field private mCount:I

.field private mFinish:Z

.field private mList:[J

.field private mListType:I

.field private mParentFragment:Landroid/app/Fragment;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 37
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/musicplus/dialog/DeleteDialogFragment;->mParentFragment:Landroid/app/Fragment;

    .line 55
    return-void
.end method

.method public constructor <init>([JIIZ)V
    .locals 1
    .param p1, "list"    # [J
    .param p2, "count"    # I
    .param p3, "listType"    # I
    .param p4, "finish"    # Z

    .prologue
    .line 39
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 37
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/musicplus/dialog/DeleteDialogFragment;->mParentFragment:Landroid/app/Fragment;

    .line 40
    iput-object p1, p0, Lcom/samsung/musicplus/dialog/DeleteDialogFragment;->mList:[J

    .line 41
    iput p2, p0, Lcom/samsung/musicplus/dialog/DeleteDialogFragment;->mCount:I

    .line 42
    iput p3, p0, Lcom/samsung/musicplus/dialog/DeleteDialogFragment;->mListType:I

    .line 43
    iput-boolean p4, p0, Lcom/samsung/musicplus/dialog/DeleteDialogFragment;->mFinish:Z

    .line 44
    return-void
.end method

.method public constructor <init>([JIIZLandroid/app/Fragment;)V
    .locals 1
    .param p1, "list"    # [J
    .param p2, "count"    # I
    .param p3, "listType"    # I
    .param p4, "finish"    # Z
    .param p5, "fragment"    # Landroid/app/Fragment;

    .prologue
    .line 46
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 37
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/musicplus/dialog/DeleteDialogFragment;->mParentFragment:Landroid/app/Fragment;

    .line 47
    iput-object p1, p0, Lcom/samsung/musicplus/dialog/DeleteDialogFragment;->mList:[J

    .line 48
    iput p2, p0, Lcom/samsung/musicplus/dialog/DeleteDialogFragment;->mCount:I

    .line 49
    iput p3, p0, Lcom/samsung/musicplus/dialog/DeleteDialogFragment;->mListType:I

    .line 50
    iput-boolean p4, p0, Lcom/samsung/musicplus/dialog/DeleteDialogFragment;->mFinish:Z

    .line 51
    iput-object p5, p0, Lcom/samsung/musicplus/dialog/DeleteDialogFragment;->mParentFragment:Landroid/app/Fragment;

    .line 52
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/dialog/DeleteDialogFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/DeleteDialogFragment;

    .prologue
    .line 19
    iget v0, p0, Lcom/samsung/musicplus/dialog/DeleteDialogFragment;->mListType:I

    return v0
.end method

.method static synthetic access$100(Lcom/samsung/musicplus/dialog/DeleteDialogFragment;)[J
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/DeleteDialogFragment;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/DeleteDialogFragment;->mList:[J

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/musicplus/dialog/DeleteDialogFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/DeleteDialogFragment;

    .prologue
    .line 19
    iget v0, p0, Lcom/samsung/musicplus/dialog/DeleteDialogFragment;->mCount:I

    return v0
.end method

.method static synthetic access$300(Lcom/samsung/musicplus/dialog/DeleteDialogFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/DeleteDialogFragment;

    .prologue
    .line 19
    iget-boolean v0, p0, Lcom/samsung/musicplus/dialog/DeleteDialogFragment;->mFinish:Z

    return v0
.end method

.method static synthetic access$400(Lcom/samsung/musicplus/dialog/DeleteDialogFragment;)Landroid/app/Fragment;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/DeleteDialogFragment;

    .prologue
    .line 19
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/DeleteDialogFragment;->mParentFragment:Landroid/app/Fragment;

    return-object v0
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 10
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v9, 0x7f1000ad

    .line 68
    if-eqz p1, :cond_0

    .line 69
    const-string v4, "selected_id"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/musicplus/dialog/DeleteDialogFragment;->mList:[J

    .line 70
    const-string v4, "total_count"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    iput v4, p0, Lcom/samsung/musicplus/dialog/DeleteDialogFragment;->mCount:I

    .line 71
    const-string v4, "list_type"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    iput v4, p0, Lcom/samsung/musicplus/dialog/DeleteDialogFragment;->mListType:I

    .line 72
    const-string v4, "need_finish"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    iput-boolean v4, p0, Lcom/samsung/musicplus/dialog/DeleteDialogFragment;->mFinish:Z

    .line 75
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/DeleteDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 76
    .local v0, "a":Landroid/app/Activity;
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 77
    .local v1, "builder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v1, v9}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 79
    const v3, 0x7f0f001a

    .line 80
    .local v3, "messageId":I
    iget v4, p0, Lcom/samsung/musicplus/dialog/DeleteDialogFragment;->mListType:I

    invoke-static {v4}, Lcom/samsung/musicplus/util/ListUtils;->isTrack(I)Z

    move-result v2

    .line 82
    .local v2, "isTrack":Z
    if-eqz v2, :cond_1

    .line 83
    const v3, 0x7f0f001a

    .line 107
    :goto_0
    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    iget v5, p0, Lcom/samsung/musicplus/dialog/DeleteDialogFragment;->mCount:I

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget v8, p0, Lcom/samsung/musicplus/dialog/DeleteDialogFragment;->mCount:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v4, v3, v5, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 109
    new-instance v4, Lcom/samsung/musicplus/dialog/DeleteDialogFragment$1;

    invoke-direct {v4, p0, v0, v2}, Lcom/samsung/musicplus/dialog/DeleteDialogFragment$1;-><init>(Lcom/samsung/musicplus/dialog/DeleteDialogFragment;Landroid/app/Activity;Z)V

    invoke-virtual {v1, v9, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 124
    const v4, 0x7f10003a

    new-instance v5, Lcom/samsung/musicplus/dialog/DeleteDialogFragment$2;

    invoke-direct {v5, p0}, Lcom/samsung/musicplus/dialog/DeleteDialogFragment$2;-><init>(Lcom/samsung/musicplus/dialog/DeleteDialogFragment;)V

    invoke-virtual {v1, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 131
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v4

    return-object v4

    .line 84
    :cond_1
    iget v4, p0, Lcom/samsung/musicplus/dialog/DeleteDialogFragment;->mListType:I

    invoke-static {v4}, Lcom/samsung/musicplus/util/ListUtils;->isPlaylistTabContent(I)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 85
    const v3, 0x7f0f0019

    goto :goto_0

    .line 87
    :cond_2
    iget v4, p0, Lcom/samsung/musicplus/dialog/DeleteDialogFragment;->mListType:I

    invoke-static {v4}, Lcom/samsung/musicplus/util/ListUtils;->getPreDefinedList(I)I

    move-result v4

    packed-switch v4, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 89
    :pswitch_1
    const v3, 0x7f0f0014

    .line 90
    goto :goto_0

    .line 92
    :pswitch_2
    const v3, 0x7f0f0015

    .line 93
    goto :goto_0

    .line 95
    :pswitch_3
    const v3, 0x7f0f0017

    .line 96
    goto :goto_0

    .line 98
    :pswitch_4
    const v3, 0x7f0f0018

    .line 99
    goto :goto_0

    .line 101
    :pswitch_5
    const v3, 0x7f0f0016

    .line 102
    goto :goto_0

    .line 87
    :pswitch_data_0
    .packed-switch 0x10002
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_3
        :pswitch_5
    .end packed-switch
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 59
    const-string v0, "selected_id"

    iget-object v1, p0, Lcom/samsung/musicplus/dialog/DeleteDialogFragment;->mList:[J

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putLongArray(Ljava/lang/String;[J)V

    .line 60
    const-string v0, "total_count"

    iget v1, p0, Lcom/samsung/musicplus/dialog/DeleteDialogFragment;->mCount:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 61
    const-string v0, "list_type"

    iget v1, p0, Lcom/samsung/musicplus/dialog/DeleteDialogFragment;->mListType:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 62
    const-string v0, "need_finish"

    iget-boolean v1, p0, Lcom/samsung/musicplus/dialog/DeleteDialogFragment;->mFinish:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 63
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 64
    return-void
.end method
