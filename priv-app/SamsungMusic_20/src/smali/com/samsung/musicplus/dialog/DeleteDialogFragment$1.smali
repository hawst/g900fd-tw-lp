.class Lcom/samsung/musicplus/dialog/DeleteDialogFragment$1;
.super Ljava/lang/Object;
.source "DeleteDialogFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/dialog/DeleteDialogFragment;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/dialog/DeleteDialogFragment;

.field final synthetic val$a:Landroid/app/Activity;

.field final synthetic val$isTrack:Z


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/dialog/DeleteDialogFragment;Landroid/app/Activity;Z)V
    .locals 0

    .prologue
    .line 109
    iput-object p1, p0, Lcom/samsung/musicplus/dialog/DeleteDialogFragment$1;->this$0:Lcom/samsung/musicplus/dialog/DeleteDialogFragment;

    iput-object p2, p0, Lcom/samsung/musicplus/dialog/DeleteDialogFragment$1;->val$a:Landroid/app/Activity;

    iput-boolean p3, p0, Lcom/samsung/musicplus/dialog/DeleteDialogFragment$1;->val$isTrack:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 7
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "id"    # I

    .prologue
    const/4 v6, 0x0

    .line 112
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/DeleteDialogFragment$1;->this$0:Lcom/samsung/musicplus/dialog/DeleteDialogFragment;

    # getter for: Lcom/samsung/musicplus/dialog/DeleteDialogFragment;->mListType:I
    invoke-static {v0}, Lcom/samsung/musicplus/dialog/DeleteDialogFragment;->access$000(Lcom/samsung/musicplus/dialog/DeleteDialogFragment;)I

    move-result v0

    invoke-static {v0}, Lcom/samsung/musicplus/util/ListUtils;->isPlaylistTabContent(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 113
    new-instance v0, Lcom/samsung/musicplus/util/FileOperationTask$DeletePlaylistTask;

    iget-object v1, p0, Lcom/samsung/musicplus/dialog/DeleteDialogFragment$1;->val$a:Landroid/app/Activity;

    iget-object v2, p0, Lcom/samsung/musicplus/dialog/DeleteDialogFragment$1;->this$0:Lcom/samsung/musicplus/dialog/DeleteDialogFragment;

    # getter for: Lcom/samsung/musicplus/dialog/DeleteDialogFragment;->mList:[J
    invoke-static {v2}, Lcom/samsung/musicplus/dialog/DeleteDialogFragment;->access$100(Lcom/samsung/musicplus/dialog/DeleteDialogFragment;)[J

    move-result-object v2

    invoke-direct {v0, v1, v2, v6}, Lcom/samsung/musicplus/util/FileOperationTask$DeletePlaylistTask;-><init>(Landroid/app/Activity;[JZ)V

    new-array v1, v6, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/util/FileOperationTask$DeletePlaylistTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 117
    :goto_0
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/DeleteDialogFragment$1;->this$0:Lcom/samsung/musicplus/dialog/DeleteDialogFragment;

    # getter for: Lcom/samsung/musicplus/dialog/DeleteDialogFragment;->mParentFragment:Landroid/app/Fragment;
    invoke-static {v0}, Lcom/samsung/musicplus/dialog/DeleteDialogFragment;->access$400(Lcom/samsung/musicplus/dialog/DeleteDialogFragment;)Landroid/app/Fragment;

    move-result-object v0

    instance-of v0, v0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;

    if-eqz v0, :cond_0

    .line 118
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/DeleteDialogFragment$1;->this$0:Lcom/samsung/musicplus/dialog/DeleteDialogFragment;

    # getter for: Lcom/samsung/musicplus/dialog/DeleteDialogFragment;->mParentFragment:Landroid/app/Fragment;
    invoke-static {v0}, Lcom/samsung/musicplus/dialog/DeleteDialogFragment;->access$400(Lcom/samsung/musicplus/dialog/DeleteDialogFragment;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;

    invoke-virtual {v0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->updateSubList()V

    .line 120
    :cond_0
    invoke-static {}, Lcom/samsung/musicplus/widget/fragment/ActionModeRemoteHandler;->getInstance()Lcom/samsung/musicplus/widget/fragment/ActionModeRemoteHandler;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/musicplus/widget/fragment/ActionModeRemoteHandler;->notifyActionModeFinishAction()V

    .line 121
    return-void

    .line 115
    :cond_1
    new-instance v0, Lcom/samsung/musicplus/util/FileOperationTask$DeleteTask;

    iget-object v1, p0, Lcom/samsung/musicplus/dialog/DeleteDialogFragment$1;->val$a:Landroid/app/Activity;

    iget-object v2, p0, Lcom/samsung/musicplus/dialog/DeleteDialogFragment$1;->this$0:Lcom/samsung/musicplus/dialog/DeleteDialogFragment;

    # getter for: Lcom/samsung/musicplus/dialog/DeleteDialogFragment;->mList:[J
    invoke-static {v2}, Lcom/samsung/musicplus/dialog/DeleteDialogFragment;->access$100(Lcom/samsung/musicplus/dialog/DeleteDialogFragment;)[J

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/musicplus/dialog/DeleteDialogFragment$1;->this$0:Lcom/samsung/musicplus/dialog/DeleteDialogFragment;

    # getter for: Lcom/samsung/musicplus/dialog/DeleteDialogFragment;->mCount:I
    invoke-static {v3}, Lcom/samsung/musicplus/dialog/DeleteDialogFragment;->access$200(Lcom/samsung/musicplus/dialog/DeleteDialogFragment;)I

    move-result v3

    iget-boolean v4, p0, Lcom/samsung/musicplus/dialog/DeleteDialogFragment$1;->val$isTrack:Z

    iget-object v5, p0, Lcom/samsung/musicplus/dialog/DeleteDialogFragment$1;->this$0:Lcom/samsung/musicplus/dialog/DeleteDialogFragment;

    # getter for: Lcom/samsung/musicplus/dialog/DeleteDialogFragment;->mFinish:Z
    invoke-static {v5}, Lcom/samsung/musicplus/dialog/DeleteDialogFragment;->access$300(Lcom/samsung/musicplus/dialog/DeleteDialogFragment;)Z

    move-result v5

    invoke-direct/range {v0 .. v5}, Lcom/samsung/musicplus/util/FileOperationTask$DeleteTask;-><init>(Landroid/app/Activity;[JIZZ)V

    new-array v1, v6, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/util/FileOperationTask$DeleteTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method
