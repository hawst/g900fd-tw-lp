.class Lcom/samsung/musicplus/dialog/PlaySpeedDialog$1;
.super Ljava/lang/Object;
.source "PlaySpeedDialog.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/dialog/PlaySpeedDialog;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/dialog/PlaySpeedDialog;)V
    .locals 0

    .prologue
    .line 90
    iput-object p1, p0, Lcom/samsung/musicplus/dialog/PlaySpeedDialog$1;->this$0:Lcom/samsung/musicplus/dialog/PlaySpeedDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 2
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromUser"    # Z

    .prologue
    .line 107
    if-eqz p3, :cond_0

    .line 108
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/PlaySpeedDialog$1;->this$0:Lcom/samsung/musicplus/dialog/PlaySpeedDialog;

    # setter for: Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->mProgress:I
    invoke-static {v0, p2}, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->access$102(Lcom/samsung/musicplus/dialog/PlaySpeedDialog;I)I

    .line 109
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/PlaySpeedDialog$1;->this$0:Lcom/samsung/musicplus/dialog/PlaySpeedDialog;

    iget-object v1, p0, Lcom/samsung/musicplus/dialog/PlaySpeedDialog$1;->this$0:Lcom/samsung/musicplus/dialog/PlaySpeedDialog;

    # getter for: Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->mProgress:I
    invoke-static {v1}, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->access$100(Lcom/samsung/musicplus/dialog/PlaySpeedDialog;)I

    move-result v1

    # invokes: Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->setProgress(I)V
    invoke-static {v0, v1}, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->access$200(Lcom/samsung/musicplus/dialog/PlaySpeedDialog;I)V

    .line 111
    :cond_0
    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 3
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 97
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/PlaySpeedDialog$1;->this$0:Lcom/samsung/musicplus/dialog/PlaySpeedDialog;

    # getter for: Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->mPlaySpeedEventHandelr:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->access$000(Lcom/samsung/musicplus/dialog/PlaySpeedDialog;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 98
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/PlaySpeedDialog$1;->this$0:Lcom/samsung/musicplus/dialog/PlaySpeedDialog;

    # getter for: Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->mPlaySpeedEventHandelr:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->access$000(Lcom/samsung/musicplus/dialog/PlaySpeedDialog;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 100
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/PlaySpeedDialog$1;->this$0:Lcom/samsung/musicplus/dialog/PlaySpeedDialog;

    # getter for: Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->mPlaySpeedEventHandelr:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->access$000(Lcom/samsung/musicplus/dialog/PlaySpeedDialog;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 101
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/PlaySpeedDialog$1;->this$0:Lcom/samsung/musicplus/dialog/PlaySpeedDialog;

    # getter for: Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->mPlaySpeedEventHandelr:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->access$000(Lcom/samsung/musicplus/dialog/PlaySpeedDialog;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 103
    :cond_1
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 93
    return-void
.end method
