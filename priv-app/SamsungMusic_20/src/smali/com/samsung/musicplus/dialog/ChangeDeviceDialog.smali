.class public Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;
.super Landroid/app/DialogFragment;
.source "ChangeDeviceDialog.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;


# static fields
.field private static final ACTION_HDMI_PLUGGED:Ljava/lang/String; = "android.intent.action.HDMI_PLUGGED"

.field private static final CLASSNAME:Ljava/lang/String;

.field private static final DEVICE_IMAGE_TYPE_RES:[I

.field private static final MY_DEVICE:Ljava/lang/String; = "MY DEVICE"

.field private static final STOP_SCAN:I = 0x64

.field private static final STOP_SCAN_DELAY:I = 0x2710


# instance fields
.field private DMR_PROJECTION:[Ljava/lang/String;

.field private mAlertDialog:Landroid/app/AlertDialog;

.field private mContext:Landroid/content/Context;

.field private mDisplayManager:Landroid/hardware/display/DisplayManager;

.field private final mDisplayManagerReceiver:Landroid/content/BroadcastReceiver;

.field private final mDlnaConnectivityReceiver:Landroid/content/BroadcastReceiver;

.field private mHasOnClickListener:Z

.field private final mHdmiReceiver:Landroid/content/BroadcastReceiver;

.field private mListView:Landroid/widget/ListView;

.field private mNic:Ljava/lang/String;

.field private mRefreshProgressBar:Landroid/widget/ProgressBar;

.field private mSelectedDeviceId:Ljava/lang/String;

.field private mSelectedDeviceType:I

.field private final mSettingsObserver:Landroid/database/ContentObserver;

.field private mWfdStopScanHandler:Landroid/os/Handler;

.field private mWifiDisplayStatus:Lcom/samsung/musicplus/library/wifi/WifiDisplayStatusCompat;

.field private mWifiState:I

.field private final mWifiStateReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 49
    const-class v0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->CLASSNAME:Ljava/lang/String;

    .line 437
    const/16 v0, 0xb

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->DEVICE_IMAGE_TYPE_RES:[I

    return-void

    :array_0
    .array-data 4
        0x7f020015
        0x7f020018
        0x7f02001b
        0x7f020014
        0x7f02001c
        0x7f02001a
        0x7f020016
        0x7f020019
        0x7f020017
        0x7f02001d
        0x7f020013
    .end array-data
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 96
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 59
    iput-boolean v2, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mHasOnClickListener:Z

    .line 63
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mSelectedDeviceType:I

    .line 77
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "avplayer_id"

    aput-object v1, v0, v2

    const/4 v1, 0x1

    const-string v2, "avplayer_name"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "album_art"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->DMR_PROJECTION:[Ljava/lang/String;

    .line 84
    new-instance v0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog$1;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog$1;-><init>(Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;)V

    iput-object v0, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mDlnaConnectivityReceiver:Landroid/content/BroadcastReceiver;

    .line 285
    new-instance v0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog$2;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog$2;-><init>(Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;)V

    iput-object v0, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mWifiStateReceiver:Landroid/content/BroadcastReceiver;

    .line 575
    new-instance v0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog$3;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog$3;-><init>(Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;)V

    iput-object v0, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mDisplayManagerReceiver:Landroid/content/BroadcastReceiver;

    .line 591
    new-instance v0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog$4;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog$4;-><init>(Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mSettingsObserver:Landroid/database/ContentObserver;

    .line 654
    new-instance v0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog$5;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog$5;-><init>(Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;)V

    iput-object v0, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mWfdStopScanHandler:Landroid/os/Handler;

    .line 667
    new-instance v0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog$6;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog$6;-><init>(Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;)V

    iput-object v0, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mHdmiReceiver:Landroid/content/BroadcastReceiver;

    .line 97
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->CLASSNAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;)Landroid/widget/ListView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mListView:Landroid/widget/ListView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;Landroid/widget/ListView;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;
    .param p1, "x1"    # Landroid/widget/ListView;

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->setAdapter(Landroid/widget/ListView;)V

    return-void
.end method

.method static synthetic access$300(Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;

    .prologue
    .line 47
    iget v0, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mWifiState:I

    return v0
.end method

.method static synthetic access$302(Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;
    .param p1, "x1"    # I

    .prologue
    .line 47
    iput p1, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mWifiState:I

    return p1
.end method

.method static synthetic access$400(Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mAlertDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->requestStopScanWifi()V

    return-void
.end method

.method static synthetic access$600(Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;

    .prologue
    .line 47
    invoke-direct {p0}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->hideRefreshProgress()V

    return-void
.end method

.method private addDmrDevices(Landroid/content/Context;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "networkId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/musicplus/dialog/WifiDeviceInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p3, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/musicplus/dialog/WifiDeviceInfo;>;"
    const/4 v1, 0x0

    .line 447
    const/4 v3, 0x0

    .line 448
    .local v3, "selection":Ljava/lang/String;
    const/4 v4, 0x0

    .line 449
    .local v4, "selectionArgs":[Ljava/lang/String;
    if-eqz p2, :cond_0

    .line 450
    const-string v3, "nic_id = ? or avplayer_name = ?"

    .line 452
    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    .end local v4    # "selectionArgs":[Ljava/lang/String;
    aput-object p2, v4, v1

    const/4 v0, 0x1

    const-string v1, "MY DEVICE"

    aput-object v1, v4, v0

    .line 458
    .restart local v4    # "selectionArgs":[Ljava/lang/String;
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/musicplus/contents/dlna/DlnaStore$Renderer;->CONTENT_URI:Landroid/net/Uri;

    iget-object v2, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->DMR_PROJECTION:[Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 462
    .local v6, "c":Landroid/database/Cursor;
    if-eqz v6, :cond_4

    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 464
    :cond_1
    new-instance v7, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;

    invoke-direct {v7}, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;-><init>()V

    .line 465
    .local v7, "info":Lcom/samsung/musicplus/dialog/WifiDeviceInfo;
    const-string v0, "avplayer_id"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v7, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->deviceId:Ljava/lang/String;

    .line 466
    const-string v0, "avplayer_name"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v7, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->deviceName:Ljava/lang/String;

    .line 468
    const-string v0, "album_art"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v7, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->albumArtUriStr:Ljava/lang/String;

    .line 470
    const/4 v0, 0x0

    iput v0, v7, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->deviceType:I

    .line 471
    const-string v0, ""

    iget-object v1, v7, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->deviceId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, ""

    iget-object v1, v7, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->deviceName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 472
    :cond_2
    sget-object v0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "dmr list.add(cursor_allshare):"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v7, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->deviceName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, v7, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->deviceId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 474
    sget-object v0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "dmr list - name: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "avplayer_name"

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 478
    invoke-virtual {p3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 481
    :cond_3
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    .line 487
    .end local v7    # "info":Lcom/samsung/musicplus/dialog/WifiDeviceInfo;
    :cond_4
    if-eqz v6, :cond_5

    .line 488
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 491
    :cond_5
    return-void

    .line 487
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_6

    .line 488
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_6
    throw v0
.end method

.method private addMyDevice(Ljava/util/ArrayList;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/musicplus/dialog/WifiDeviceInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 354
    .local p1, "devices":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/musicplus/dialog/WifiDeviceInfo;>;"
    new-instance v0, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;

    invoke-direct {v0}, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;-><init>()V

    .line 355
    .local v0, "info":Lcom/samsung/musicplus/dialog/WifiDeviceInfo;
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->deviceId:Ljava/lang/String;

    .line 356
    const v1, 0x7f1000d9

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->deviceName:Ljava/lang/String;

    .line 357
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 358
    return-void
.end method

.method private addWifiDevices(Ljava/util/ArrayList;)V
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/musicplus/dialog/WifiDeviceInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "list":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/musicplus/dialog/WifiDeviceInfo;>;"
    const/4 v12, 0x1

    .line 361
    sget-boolean v9, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_SUPPORT_WIFI_DISPLAY:Z

    if-eqz v9, :cond_0

    iget-object v9, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    invoke-static {v9}, Lcom/samsung/musicplus/library/wifi/DisplayManagerCompat;->checkExceptionalCase(Landroid/hardware/display/DisplayManager;)I

    move-result v9

    if-eqz v9, :cond_1

    .line 415
    :cond_0
    :goto_0
    return-void

    .line 366
    :cond_1
    iget-object v9, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    invoke-static {v9}, Lcom/samsung/musicplus/library/wifi/DisplayManagerCompat;->getWifiDisplayStatus(Landroid/hardware/display/DisplayManager;)Lcom/samsung/musicplus/library/wifi/WifiDisplayStatusCompat;

    move-result-object v9

    iput-object v9, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mWifiDisplayStatus:Lcom/samsung/musicplus/library/wifi/WifiDisplayStatusCompat;

    .line 367
    iget-object v9, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mWifiDisplayStatus:Lcom/samsung/musicplus/library/wifi/WifiDisplayStatusCompat;

    if-eqz v9, :cond_5

    iget-object v9, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mWifiDisplayStatus:Lcom/samsung/musicplus/library/wifi/WifiDisplayStatusCompat;

    invoke-virtual {v9}, Lcom/samsung/musicplus/library/wifi/WifiDisplayStatusCompat;->getActiveDisplayState()I

    move-result v9

    const/4 v10, 0x2

    if-eq v9, v10, :cond_5

    .line 368
    iget-object v9, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mWifiDisplayStatus:Lcom/samsung/musicplus/library/wifi/WifiDisplayStatusCompat;

    invoke-virtual {v9}, Lcom/samsung/musicplus/library/wifi/WifiDisplayStatusCompat;->getAvailableDisplays()[Lcom/samsung/musicplus/library/wifi/WifiDisplayCompat;

    move-result-object v1

    .line 370
    .local v1, "availableDisplays":[Lcom/samsung/musicplus/library/wifi/WifiDisplayCompat;
    if-eqz v1, :cond_0

    .line 371
    move-object v0, v1

    .local v0, "arr$":[Lcom/samsung/musicplus/library/wifi/WifiDisplayCompat;
    array-length v6, v0

    .local v6, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_1
    if-ge v4, v6, :cond_0

    aget-object v2, v0, v4

    .line 372
    .local v2, "d":Lcom/samsung/musicplus/library/wifi/WifiDisplayCompat;
    invoke-virtual {v2}, Lcom/samsung/musicplus/library/wifi/WifiDisplayCompat;->isAvailable()Z

    move-result v9

    if-eqz v9, :cond_3

    .line 373
    new-instance v5, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;

    invoke-direct {v5}, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;-><init>()V

    .line 374
    .local v5, "info":Lcom/samsung/musicplus/dialog/WifiDeviceInfo;
    iput v12, v5, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->deviceType:I

    .line 375
    invoke-virtual {v2}, Lcom/samsung/musicplus/library/wifi/WifiDisplayCompat;->getPrimaryDeviceType()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/samsung/musicplus/library/wifi/DisplayManagerCompat;->getDeviceType(Ljava/lang/String;)I

    move-result v3

    .line 377
    .local v3, "deviceType":I
    if-nez v3, :cond_2

    .line 378
    iput-boolean v12, v5, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->isWfdDongle:Z

    .line 380
    :cond_2
    invoke-virtual {v2}, Lcom/samsung/musicplus/library/wifi/WifiDisplayCompat;->getDeviceAddress()Ljava/lang/String;

    move-result-object v9

    iput-object v9, v5, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->deviceId:Ljava/lang/String;

    .line 381
    invoke-virtual {v2}, Lcom/samsung/musicplus/library/wifi/WifiDisplayCompat;->getDeviceName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/String;->isEmpty()Z

    move-result v9

    if-eqz v9, :cond_4

    .line 382
    invoke-virtual {v2}, Lcom/samsung/musicplus/library/wifi/WifiDisplayCompat;->getDeviceAddress()Ljava/lang/String;

    move-result-object v9

    iput-object v9, v5, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->deviceName:Ljava/lang/String;

    .line 386
    :goto_2
    invoke-direct {p0, v3}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->getDeviceIcon(I)I

    move-result v9

    iput v9, v5, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->wfdIcon:I

    .line 387
    sget-object v9, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->CLASSNAME:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "list.add(availableDisplay):"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, v5, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->deviceName:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " id: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, v5, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->deviceId:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 389
    invoke-virtual {p1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 371
    .end local v3    # "deviceType":I
    .end local v5    # "info":Lcom/samsung/musicplus/dialog/WifiDeviceInfo;
    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 384
    .restart local v3    # "deviceType":I
    .restart local v5    # "info":Lcom/samsung/musicplus/dialog/WifiDeviceInfo;
    :cond_4
    invoke-virtual {v2}, Lcom/samsung/musicplus/library/wifi/WifiDisplayCompat;->getDeviceName()Ljava/lang/String;

    move-result-object v9

    iput-object v9, v5, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->deviceName:Ljava/lang/String;

    goto :goto_2

    .line 394
    .end local v0    # "arr$":[Lcom/samsung/musicplus/library/wifi/WifiDisplayCompat;
    .end local v1    # "availableDisplays":[Lcom/samsung/musicplus/library/wifi/WifiDisplayCompat;
    .end local v2    # "d":Lcom/samsung/musicplus/library/wifi/WifiDisplayCompat;
    .end local v3    # "deviceType":I
    .end local v4    # "i$":I
    .end local v5    # "info":Lcom/samsung/musicplus/dialog/WifiDeviceInfo;
    .end local v6    # "len$":I
    :cond_5
    iget-object v9, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mWifiDisplayStatus:Lcom/samsung/musicplus/library/wifi/WifiDisplayStatusCompat;

    if-eqz v9, :cond_0

    .line 395
    iget-object v9, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mWifiDisplayStatus:Lcom/samsung/musicplus/library/wifi/WifiDisplayStatusCompat;

    invoke-virtual {v9}, Lcom/samsung/musicplus/library/wifi/WifiDisplayStatusCompat;->getActiveDisplay()Lcom/samsung/musicplus/library/wifi/WifiDisplayCompat;

    move-result-object v8

    .line 396
    .local v8, "wd":Lcom/samsung/musicplus/library/wifi/WifiDisplayCompat;
    if-eqz v8, :cond_0

    .line 397
    new-instance v5, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;

    invoke-direct {v5}, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;-><init>()V

    .line 398
    .restart local v5    # "info":Lcom/samsung/musicplus/dialog/WifiDeviceInfo;
    iput v12, v5, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->deviceType:I

    .line 399
    invoke-virtual {v8}, Lcom/samsung/musicplus/library/wifi/WifiDisplayCompat;->getDeviceName()Ljava/lang/String;

    move-result-object v9

    iput-object v9, v5, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->deviceName:Ljava/lang/String;

    .line 403
    invoke-virtual {v8}, Lcom/samsung/musicplus/library/wifi/WifiDisplayCompat;->getDeviceAddress()Ljava/lang/String;

    move-result-object v9

    iput-object v9, v5, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->deviceId:Ljava/lang/String;

    .line 404
    invoke-virtual {v8}, Lcom/samsung/musicplus/library/wifi/WifiDisplayCompat;->getPrimaryDeviceType()Ljava/lang/String;

    move-result-object v7

    .line 406
    .local v7, "primaryDeviceType":Ljava/lang/String;
    invoke-static {v7}, Lcom/samsung/musicplus/library/wifi/DisplayManagerCompat;->getDeviceType(Ljava/lang/String;)I

    move-result v3

    .line 407
    .restart local v3    # "deviceType":I
    invoke-direct {p0, v3}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->getDeviceIcon(I)I

    move-result v9

    iput v9, v5, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->wfdIcon:I

    .line 408
    iget-object v9, v5, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->deviceId:Ljava/lang/String;

    iput-object v9, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mSelectedDeviceId:Ljava/lang/String;

    .line 409
    sget-object v9, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->CLASSNAME:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "list.add(wfdConnected true):"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, v5, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->deviceName:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " id: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, v5, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->deviceId:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 411
    invoke-virtual {p1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method

.method private getDeviceIcon(I)I
    .locals 1
    .param p1, "deviceType"    # I

    .prologue
    .line 418
    sget-object v0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->DEVICE_IMAGE_TYPE_RES:[I

    aget v0, v0, p1

    return v0
.end method

.method private getHelpNeverShownValue()Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 618
    iget-object v1, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mContext:Landroid/content/Context;

    const-string v2, "music_player_setting"

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 620
    .local v0, "sp":Landroid/content/SharedPreferences;
    const-string v1, "wfd_help_popup_never_shown"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method

.method private hideRefreshProgress()V
    .locals 2

    .prologue
    .line 262
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mRefreshProgressBar:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    .line 263
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mRefreshProgressBar:Landroid/widget/ProgressBar;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 265
    :cond_0
    return-void
.end method

.method private registerDisplayManagerReceiver()V
    .locals 4

    .prologue
    .line 189
    invoke-direct {p0}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->requestScanWifi()V

    .line 190
    invoke-direct {p0}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->showRefreshProgress()V

    .line 192
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mDisplayManagerReceiver:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.hardware.display.action.WIFI_DISPLAY_STATUS_CHANGED"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 194
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/musicplus/library/wifi/DisplayManagerCompat;->GLOBAL_WIFI_DISPLAY_SETTING_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mSettingsObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 196
    return-void
.end method

.method private registerDlnaReceiver()V
    .locals 3

    .prologue
    .line 268
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 269
    .local v0, "f":Landroid/content/IntentFilter;
    const-string v1, "com.samsung.musicplus.dlna.connectivitychanged"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 270
    iget-object v1, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mDlnaConnectivityReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 271
    return-void
.end method

.method private registerHdmiReceiver()V
    .locals 3

    .prologue
    .line 280
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 281
    .local v0, "f":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.HDMI_PLUGGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 282
    iget-object v1, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mHdmiReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 283
    return-void
.end method

.method private registerWifiStateReceiver()V
    .locals 3

    .prologue
    .line 274
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 275
    .local v0, "f":Landroid/content/IntentFilter;
    const-string v1, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 276
    iget-object v1, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mWifiStateReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 277
    return-void
.end method

.method private requestScanWifi()V
    .locals 6

    .prologue
    .line 624
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    invoke-static {v2}, Lcom/samsung/musicplus/library/wifi/DisplayManagerCompat;->getWifiDisplayStatus(Landroid/hardware/display/DisplayManager;)Lcom/samsung/musicplus/library/wifi/WifiDisplayStatusCompat;

    move-result-object v1

    .line 625
    .local v1, "wsc":Lcom/samsung/musicplus/library/wifi/WifiDisplayStatusCompat;
    if-eqz v1, :cond_0

    .line 626
    invoke-virtual {v1}, Lcom/samsung/musicplus/library/wifi/WifiDisplayStatusCompat;->getScanState()I

    move-result v0

    .line 627
    .local v0, "scanState":I
    sget-object v2, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->CLASSNAME:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "scanning start! - scanState : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 628
    if-nez v0, :cond_0

    .line 629
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    invoke-static {v2}, Lcom/samsung/musicplus/library/wifi/DisplayManagerCompat;->scanWifiDisplays(Landroid/hardware/display/DisplayManager;)V

    .line 634
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mWfdStopScanHandler:Landroid/os/Handler;

    const/16 v3, 0x64

    const-wide/16 v4, 0x2710

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 637
    .end local v0    # "scanState":I
    :cond_0
    return-void
.end method

.method private requestStopScanWifi()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 640
    iget-object v3, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    invoke-static {v3}, Lcom/samsung/musicplus/library/wifi/DisplayManagerCompat;->getWifiDisplayStatus(Landroid/hardware/display/DisplayManager;)Lcom/samsung/musicplus/library/wifi/WifiDisplayStatusCompat;

    move-result-object v2

    .line 641
    .local v2, "wsc":Lcom/samsung/musicplus/library/wifi/WifiDisplayStatusCompat;
    if-eqz v2, :cond_0

    .line 642
    invoke-virtual {v2}, Lcom/samsung/musicplus/library/wifi/WifiDisplayStatusCompat;->getScanState()I

    move-result v1

    .line 643
    .local v1, "scanState":I
    sget-object v3, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->CLASSNAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "scanning stop! - scanState : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 644
    if-ne v1, v6, :cond_0

    .line 645
    invoke-virtual {v2}, Lcom/samsung/musicplus/library/wifi/WifiDisplayStatusCompat;->getActiveDisplayState()I

    move-result v0

    .line 646
    .local v0, "disPlayState":I
    sget-object v3, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->CLASSNAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "scanning stop! - disPlayState : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 647
    if-eq v0, v6, :cond_0

    .line 648
    iget-object v3, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    invoke-static {v3}, Lcom/samsung/musicplus/library/wifi/DisplayManagerCompat;->stopScan(Landroid/hardware/display/DisplayManager;)V

    .line 652
    .end local v0    # "disPlayState":I
    .end local v1    # "scanState":I
    :cond_0
    return-void
.end method

.method private selectDevice(ILjava/lang/String;Landroid/net/wifi/p2p/WifiP2pDevice;Z)V
    .locals 3
    .param p1, "deviceType"    # I
    .param p2, "selectedDeviceId"    # Ljava/lang/String;
    .param p3, "wifiP2pDevice"    # Landroid/net/wifi/p2p/WifiP2pDevice;
    .param p4, "isWfdDongle"    # Z

    .prologue
    .line 495
    sget-object v0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "selectDevice deviceType: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " selectedDeviceId: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " isWfdDongle: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 499
    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mSelectedDeviceId:Ljava/lang/String;

    if-eqz v0, :cond_1

    :cond_0
    if-eqz p2, :cond_2

    iget-object v0, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mSelectedDeviceId:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 502
    :cond_1
    sget-object v0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->CLASSNAME:Ljava/lang/String;

    const-string v1, "Same device is selected. Ignore it."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 519
    :goto_0
    return-void

    .line 505
    :cond_2
    iput-object p2, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mSelectedDeviceId:Ljava/lang/String;

    .line 506
    iput p1, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mSelectedDeviceType:I

    .line 509
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 511
    :pswitch_0
    invoke-direct {p0, p2}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->selectDmrDevice(Ljava/lang/String;)V

    goto :goto_0

    .line 514
    :pswitch_1
    invoke-direct {p0, p2, p4}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->selectWifiDevice(Ljava/lang/String;Z)V

    goto :goto_0

    .line 509
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private selectDmrDevice(Ljava/lang/String;)V
    .locals 5
    .param p1, "selectedDeviceId"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    .line 552
    if-nez p1, :cond_2

    .line 553
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->changeToDefaultPlayer()V

    .line 559
    :goto_0
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    if-eqz v2, :cond_1

    .line 560
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    invoke-static {v2}, Lcom/samsung/musicplus/library/wifi/DisplayManagerCompat;->getWifiDisplayStatus(Landroid/hardware/display/DisplayManager;)Lcom/samsung/musicplus/library/wifi/WifiDisplayStatusCompat;

    move-result-object v1

    .line 562
    .local v1, "wsc":Lcom/samsung/musicplus/library/wifi/WifiDisplayStatusCompat;
    if-eqz v1, :cond_1

    .line 563
    invoke-virtual {v1}, Lcom/samsung/musicplus/library/wifi/WifiDisplayStatusCompat;->getActiveDisplayState()I

    move-result v0

    .line 564
    .local v0, "curStatus":I
    const/4 v2, 0x2

    if-eq v0, v2, :cond_0

    if-ne v0, v4, :cond_1

    .line 566
    :cond_0
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    invoke-static {v2}, Lcom/samsung/musicplus/library/wifi/DisplayManagerCompat;->disconnectExt(Landroid/hardware/display/DisplayManager;)V

    .line 567
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mContext:Landroid/content/Context;

    const v3, 0x7f1001ca

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 571
    .end local v0    # "curStatus":I
    .end local v1    # "wsc":Lcom/samsung/musicplus/library/wifi/WifiDisplayStatusCompat;
    :cond_1
    return-void

    .line 555
    :cond_2
    invoke-static {p1}, Lcom/samsung/musicplus/util/ServiceUtils;->changeToDmrPlayer(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private selectWifiDevice(Ljava/lang/String;Z)V
    .locals 5
    .param p1, "selectedDeviceId"    # Ljava/lang/String;
    .param p2, "isWfdDongle"    # Z

    .prologue
    .line 522
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    if-eqz v2, :cond_0

    .line 523
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    invoke-static {v2}, Lcom/samsung/musicplus/library/wifi/DisplayManagerCompat;->getWifiDisplayStatus(Landroid/hardware/display/DisplayManager;)Lcom/samsung/musicplus/library/wifi/WifiDisplayStatusCompat;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/musicplus/library/wifi/WifiDisplayStatusCompat;->getActiveDisplay()Lcom/samsung/musicplus/library/wifi/WifiDisplayCompat;

    move-result-object v1

    .line 525
    .local v1, "wd":Lcom/samsung/musicplus/library/wifi/WifiDisplayCompat;
    if-eqz v1, :cond_0

    if-eqz p1, :cond_0

    .line 526
    invoke-virtual {v1}, Lcom/samsung/musicplus/library/wifi/WifiDisplayCompat;->getDeviceAddress()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 549
    .end local v1    # "wd":Lcom/samsung/musicplus/library/wifi/WifiDisplayCompat;
    :goto_0
    return-void

    .line 538
    :cond_0
    if-eqz p2, :cond_2

    .line 539
    invoke-direct {p0}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->getHelpNeverShownValue()Z

    move-result v0

    .line 540
    .local v0, "neverShown":Z
    if-eqz v0, :cond_1

    .line 541
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->connectWifiDisplay(Ljava/lang/String;)V

    goto :goto_0

    .line 543
    :cond_1
    invoke-static {p1}, Lcom/samsung/musicplus/dialog/WfdHelpDialog;->getInstance(Ljava/lang/String;)Landroid/app/DialogFragment;

    move-result-object v2

    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    const-string v4, "wfd_dongle_dialog"

    invoke-virtual {v2, v3, v4}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 547
    .end local v0    # "neverShown":Z
    :cond_2
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->connectWifiDisplay(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private setAdapter(Landroid/widget/ListView;)V
    .locals 12
    .param p1, "lv"    # Landroid/widget/ListView;

    .prologue
    const/4 v4, 0x0

    .line 312
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->getActivity()Landroid/app/Activity;

    move-result-object v7

    .line 315
    .local v7, "a":Landroid/app/Activity;
    if-nez v7, :cond_0

    .line 351
    :goto_0
    return-void

    .line 319
    :cond_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 321
    .local v3, "devices":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/musicplus/dialog/WifiDeviceInfo;>;"
    invoke-direct {p0, v3}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->addMyDevice(Ljava/util/ArrayList;)V

    .line 323
    invoke-virtual {v7}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v8

    .line 325
    .local v8, "context":Landroid/content/Context;
    iget-object v1, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mNic:Ljava/lang/String;

    invoke-direct {p0, v8, v1, v3}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->addDmrDevices(Landroid/content/Context;Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 328
    invoke-direct {p0, v3}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->addWifiDevices(Ljava/util/ArrayList;)V

    .line 331
    iget-object v1, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mSelectedDeviceId:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 332
    sget-object v1, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->CLASSNAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mSelectedDeviceId"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v5, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mSelectedDeviceId:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 333
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .local v10, "i$":Ljava/util/Iterator;
    :cond_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;

    .line 334
    .local v11, "wifiDeviceInfo":Lcom/samsung/musicplus/dialog/WifiDeviceInfo;
    iget-object v9, v11, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->deviceId:Ljava/lang/String;

    .line 335
    .local v9, "deviceId":Ljava/lang/String;
    if-eqz v9, :cond_1

    iget-object v1, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mSelectedDeviceId:Ljava/lang/String;

    invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 336
    sget-object v1, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->CLASSNAME:Ljava/lang/String;

    const-string v2, "mSelectedDeviceId Index move to Top"

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 337
    invoke-virtual {v3, v11}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 338
    invoke-virtual {v3, v4, v11}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 346
    .end local v9    # "deviceId":Ljava/lang/String;
    .end local v10    # "i$":Ljava/util/Iterator;
    .end local v11    # "wifiDeviceInfo":Lcom/samsung/musicplus/dialog/WifiDeviceInfo;
    :cond_2
    new-instance v0, Lcom/samsung/musicplus/dialog/ChangeDeviceArrayAdapter;

    iget-object v1, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mContext:Landroid/content/Context;

    const v2, 0x7f040007

    iget-object v5, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mSelectedDeviceId:Ljava/lang/String;

    invoke-static {}, Lcom/samsung/musicplus/util/UiUtils;->getTheme()I

    move-result v6

    invoke-direct/range {v0 .. v6}, Lcom/samsung/musicplus/dialog/ChangeDeviceArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/ArrayList;ILjava/lang/String;I)V

    .line 349
    .local v0, "adapter":Lcom/samsung/musicplus/dialog/ChangeDeviceArrayAdapter;
    invoke-virtual {p1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 350
    invoke-virtual {v0}, Lcom/samsung/musicplus/dialog/ChangeDeviceArrayAdapter;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method private showRefreshProgress()V
    .locals 2

    .prologue
    .line 256
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mRefreshProgressBar:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    .line 257
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mRefreshProgressBar:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 259
    :cond_0
    return-void
.end method


# virtual methods
.method public connectWifiDisplay(Ljava/lang/String;)V
    .locals 4
    .param p1, "deviceAddress"    # Ljava/lang/String;

    .prologue
    const/16 v3, 0x8

    .line 605
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 606
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/wifi/DisplayManagerCompat;->setActivityState(Landroid/hardware/display/DisplayManager;I)V

    .line 608
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->changeToWfdDevice()V

    .line 609
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    invoke-static {v0, v3, p1}, Lcom/samsung/musicplus/library/wifi/DisplayManagerCompat;->connectWifiDisplayWithMode(Landroid/hardware/display/DisplayManager;ILjava/lang/String;)V

    .line 611
    sget-object v0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "connectWifiDisplayWithMode() deviceAddress : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " connect type : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 615
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 244
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mAlertDialog:Landroid/app/AlertDialog;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 246
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->refreshDlna()V

    .line 248
    sget-boolean v0, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_SUPPORT_WIFI_DISPLAY:Z

    if-eqz v0, :cond_0

    .line 249
    invoke-direct {p0}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->requestScanWifi()V

    .line 251
    :cond_0
    invoke-direct {p0}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->showRefreshProgress()V

    .line 253
    :cond_1
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 101
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 102
    sget-object v0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCreate() - savedInstanceState: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    if-eqz p1, :cond_1

    .line 104
    const-string v0, "deviceId"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mSelectedDeviceId:Ljava/lang/String;

    .line 105
    const-string v0, "nic"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mNic:Ljava/lang/String;

    .line 110
    :goto_0
    sget-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_DATA_CHECK_POPUP:Z

    if-eqz v0, :cond_0

    .line 111
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->refreshDlna()V

    .line 113
    :cond_0
    return-void

    .line 107
    :cond_1
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getDlnaPlayingDmrId()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mSelectedDeviceId:Ljava/lang/String;

    .line 108
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getDlnaPlayingNic()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mNic:Ljava/lang/String;

    goto :goto_0
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 11
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v10, 0x0

    const/4 v9, 0x0

    .line 128
    sget-object v7, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->CLASSNAME:Ljava/lang/String;

    const-string v8, "onCreateDialog()"

    invoke-static {v7, v8}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 130
    .local v0, "a":Landroid/app/Activity;
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 133
    .local v1, "builder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->getContext()Landroid/content/Context;

    move-result-object v7

    iput-object v7, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mContext:Landroid/content/Context;

    .line 135
    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    const-string v8, "display"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/hardware/display/DisplayManager;

    iput-object v7, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    .line 138
    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    const-string v8, "wifi"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/net/wifi/WifiManager;

    .line 140
    .local v6, "wifiManager":Landroid/net/wifi/WifiManager;
    invoke-virtual {v6}, Landroid/net/wifi/WifiManager;->getWifiState()I

    move-result v7

    iput v7, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mWifiState:I

    .line 142
    iget-object v7, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mContext:Landroid/content/Context;

    const-string v8, "layout_inflater"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/LayoutInflater;

    .line 145
    .local v4, "inflater":Landroid/view/LayoutInflater;
    const v7, 0x7f040006

    invoke-virtual {v4, v7, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 146
    .local v5, "v":Landroid/view/View;
    const v7, 0x7f0d004d

    invoke-virtual {v5, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 147
    .local v2, "category":Landroid/view/View;
    invoke-virtual {v2, v10}, Landroid/view/View;->setVisibility(I)V

    .line 148
    const v7, 0x7f0d0050

    invoke-virtual {v5, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 150
    .local v3, "divider":Landroid/view/View;
    const v7, 0x7f0d004f

    invoke-virtual {v5, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ProgressBar;

    iput-object v7, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mRefreshProgressBar:Landroid/widget/ProgressBar;

    .line 152
    const v7, 0x102000a

    invoke-virtual {v5, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/ListView;

    iput-object v7, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mListView:Landroid/widget/ListView;

    .line 153
    iget-object v7, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mListView:Landroid/widget/ListView;

    invoke-virtual {v7, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 154
    iget-object v7, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mListView:Landroid/widget/ListView;

    invoke-direct {p0, v7}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->setAdapter(Landroid/widget/ListView;)V

    .line 156
    invoke-virtual {v1, v5}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    const v8, 0x7f100152

    invoke-virtual {v7, v8}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    const v8, 0x7f10012f

    invoke-virtual {v7, v8, v9}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    const v8, 0x7f10003a

    invoke-virtual {v7, v8, v9}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v7

    invoke-virtual {v7}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v7

    iput-object v7, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mAlertDialog:Landroid/app/AlertDialog;

    .line 160
    sget-boolean v7, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_SUPPORT_WIFI_DISPLAY:Z

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    invoke-static {v7}, Lcom/samsung/musicplus/library/wifi/DisplayManagerCompat;->checkExceptionalCase(Landroid/hardware/display/DisplayManager;)I

    move-result v7

    if-nez v7, :cond_0

    .line 163
    iget-object v7, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    invoke-static {v7, v10}, Lcom/samsung/musicplus/library/wifi/DisplayManagerCompat;->setActivityState(Landroid/hardware/display/DisplayManager;I)V

    .line 165
    iget-object v7, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    const/4 v8, 0x1

    invoke-static {v7, v8}, Lcom/samsung/musicplus/library/wifi/DisplayManagerCompat;->setActivityState(Landroid/hardware/display/DisplayManager;I)V

    .line 169
    :cond_0
    iget-object v7, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mAlertDialog:Landroid/app/AlertDialog;

    return-object v7
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 215
    sget-object v0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->CLASSNAME:Ljava/lang/String;

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    sget-boolean v0, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_SUPPORT_WIFI_DISPLAY:Z

    if-eqz v0, :cond_0

    .line 217
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mWfdStopScanHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 218
    invoke-direct {p0}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->requestStopScanWifi()V

    .line 219
    iget v0, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mSelectedDeviceType:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 220
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/wifi/DisplayManagerCompat;->setActivityState(Landroid/hardware/display/DisplayManager;I)V

    .line 224
    :cond_0
    invoke-super {p0}, Landroid/app/DialogFragment;->onDestroy()V

    .line 225
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 229
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    sget-object v1, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->CLASSNAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onItemClick() - position: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " id: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    const/4 v0, 0x0

    .line 231
    .local v0, "info":Lcom/samsung/musicplus/dialog/WifiDeviceInfo;
    if-eqz p2, :cond_0

    .line 232
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "info":Lcom/samsung/musicplus/dialog/WifiDeviceInfo;
    check-cast v0, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;

    .line 234
    .restart local v0    # "info":Lcom/samsung/musicplus/dialog/WifiDeviceInfo;
    :cond_0
    if-nez v0, :cond_1

    .line 240
    :goto_0
    return-void

    .line 237
    :cond_1
    iget v1, v0, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->deviceType:I

    iget-object v2, v0, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->deviceId:Ljava/lang/String;

    iget-object v3, v0, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->wifiP2pDevice:Landroid/net/wifi/p2p/WifiP2pDevice;

    iget-boolean v4, v0, Lcom/samsung/musicplus/dialog/WifiDeviceInfo;->isWfdDongle:Z

    invoke-direct {p0, v1, v2, v3, v4}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->selectDevice(ILjava/lang/String;Landroid/net/wifi/p2p/WifiP2pDevice;Z)V

    .line 239
    iget-object v1, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mAlertDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->dismiss()V

    goto :goto_0
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 200
    sget-object v0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->CLASSNAME:Ljava/lang/String;

    const-string v1, "onPause()"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mDlnaConnectivityReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 203
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mWifiStateReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 204
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mHdmiReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 205
    sget-boolean v0, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_SUPPORT_WIFI_DISPLAY:Z

    if-eqz v0, :cond_0

    .line 206
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mDisplayManagerReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 207
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mSettingsObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 210
    :cond_0
    invoke-super {p0}, Landroid/app/DialogFragment;->onPause()V

    .line 211
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 174
    iget-boolean v0, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mHasOnClickListener:Z

    if-nez v0, :cond_0

    .line 175
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mAlertDialog:Landroid/app/AlertDialog;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 176
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mHasOnClickListener:Z

    .line 179
    :cond_0
    invoke-direct {p0}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->registerDlnaReceiver()V

    .line 180
    invoke-direct {p0}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->registerWifiStateReceiver()V

    .line 181
    invoke-direct {p0}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->registerHdmiReceiver()V

    .line 182
    sget-boolean v0, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_SUPPORT_WIFI_DISPLAY:Z

    if-eqz v0, :cond_1

    .line 183
    invoke-direct {p0}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->registerDisplayManagerReceiver()V

    .line 185
    :cond_1
    invoke-super {p0}, Landroid/app/DialogFragment;->onResume()V

    .line 186
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 117
    const-string v0, "deviceId"

    iget-object v1, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mSelectedDeviceId:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    const-string v0, "nic"

    iget-object v1, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialog;->mNic:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 120
    return-void
.end method
