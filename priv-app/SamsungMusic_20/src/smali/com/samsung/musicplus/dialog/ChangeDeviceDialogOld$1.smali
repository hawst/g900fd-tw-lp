.class Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld$1;
.super Landroid/content/BroadcastReceiver;
.source "ChangeDeviceDialogOld.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;)V
    .locals 0

    .prologue
    .line 100
    iput-object p1, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld$1;->this$0:Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 103
    # getter for: Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mDlnaConnectivityReceiver - action: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    const-string v1, "com.samsung.musicplus.dlna.connectivitychanged.extra.what"

    const/4 v2, -0x1

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 106
    .local v0, "what":I
    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 107
    :cond_0
    iget-object v1, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld$1;->this$0:Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;

    iget-object v2, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld$1;->this$0:Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;

    # getter for: Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mListView:Landroid/widget/ListView;
    invoke-static {v2}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->access$100(Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;)Landroid/widget/ListView;

    move-result-object v2

    # invokes: Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->setAdapter(Landroid/widget/ListView;)V
    invoke-static {v1, v2}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->access$200(Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;Landroid/widget/ListView;)V

    .line 109
    :cond_1
    return-void
.end method
