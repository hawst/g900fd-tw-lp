.class Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment$3;
.super Ljava/lang/Object;
.source "KnoxMoveDialogFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->onCompleted()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;)V
    .locals 0

    .prologue
    .line 256
    iput-object p1, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment$3;->this$0:Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private makeToastMsg()Ljava/lang/String;
    .locals 9

    .prologue
    const v8, 0x7f100120

    const v7, 0x7f100099

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 268
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment$3;->this$0:Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;

    # getter for: Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->access$600(Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 269
    .local v1, "res":Landroid/content/res/Resources;
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment$3;->this$0:Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;

    # getter for: Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mSuccessCount:I
    invoke-static {v2}, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->access$400(Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;)I

    move-result v2

    if-ne v2, v5, :cond_2

    .line 274
    const v2, 0x7f1000ce

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-array v4, v5, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment$3;->this$0:Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;

    # getter for: Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mKnoxMode:Z
    invoke-static {v2}, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->access$700(Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    :goto_0
    aput-object v2, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 284
    .local v0, "msg":Ljava/lang/String;
    :goto_1
    return-object v0

    .line 274
    .end local v0    # "msg":Ljava/lang/String;
    :cond_0
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment$3;->this$0:Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;

    # getter for: Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mContainerName:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->access$800(Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    invoke-virtual {v1, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment$3;->this$0:Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;

    # getter for: Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mContainerName:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->access$800(Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;)Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    .line 279
    :cond_2
    const v2, 0x7f1000d1

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v2, 0x2

    new-array v4, v2, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment$3;->this$0:Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;

    # getter for: Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mSuccessCount:I
    invoke-static {v2}, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->access$400(Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v4, v6

    iget-object v2, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment$3;->this$0:Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;

    # getter for: Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mKnoxMode:Z
    invoke-static {v2}, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->access$700(Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v1, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    :goto_2
    aput-object v2, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "msg":Ljava/lang/String;
    goto :goto_1

    .end local v0    # "msg":Ljava/lang/String;
    :cond_3
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment$3;->this$0:Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;

    # getter for: Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mContainerName:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->access$800(Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_4

    invoke-virtual {v1, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    goto :goto_2

    :cond_4
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment$3;->this$0:Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;

    # getter for: Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mContainerName:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->access$800(Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;)Ljava/lang/String;

    move-result-object v2

    goto :goto_2
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 260
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment$3;->this$0:Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;

    # getter for: Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->mKnoxMoveOperation:Lcom/samsung/musicplus/contents/MoveKnoxPersonalModeOperation;
    invoke-static {v0}, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->access$000(Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;)Lcom/samsung/musicplus/contents/MoveKnoxPersonalModeOperation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/musicplus/contents/MoveKnoxPersonalModeOperation;->isFailed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 261
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment$3;->this$0:Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;

    invoke-direct {p0}, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment$3;->makeToastMsg()Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->showToast(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->access$200(Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;Ljava/lang/String;)V

    .line 263
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment$3;->this$0:Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;

    # invokes: Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->endOperation()V
    invoke-static {v0}, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;->access$100(Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;)V

    .line 264
    return-void
.end method
