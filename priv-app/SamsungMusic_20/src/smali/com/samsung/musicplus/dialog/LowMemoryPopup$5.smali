.class Lcom/samsung/musicplus/dialog/LowMemoryPopup$5;
.super Ljava/lang/Object;
.source "LowMemoryPopup.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/dialog/LowMemoryPopup;->showLowMemoryMsg()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/dialog/LowMemoryPopup;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/dialog/LowMemoryPopup;)V
    .locals 0

    .prologue
    .line 91
    iput-object p1, p0, Lcom/samsung/musicplus/dialog/LowMemoryPopup$5;->this$0:Lcom/samsung/musicplus/dialog/LowMemoryPopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 1
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "id"    # I

    .prologue
    .line 94
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/LowMemoryPopup$5;->this$0:Lcom/samsung/musicplus/dialog/LowMemoryPopup;

    # getter for: Lcom/samsung/musicplus/dialog/LowMemoryPopup;->mMemoryMessageBox:Landroid/app/AlertDialog;
    invoke-static {v0}, Lcom/samsung/musicplus/dialog/LowMemoryPopup;->access$000(Lcom/samsung/musicplus/dialog/LowMemoryPopup;)Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 95
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->killMusicProcess()V

    .line 96
    return-void
.end method
