.class Lcom/samsung/musicplus/dialog/AdaptSoundDialog$1;
.super Ljava/lang/Object;
.source "AdaptSoundDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/dialog/AdaptSoundDialog;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/dialog/AdaptSoundDialog;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/dialog/AdaptSoundDialog;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 54
    iput-object p1, p0, Lcom/samsung/musicplus/dialog/AdaptSoundDialog$1;->this$0:Lcom/samsung/musicplus/dialog/AdaptSoundDialog;

    iput-object p2, p0, Lcom/samsung/musicplus/dialog/AdaptSoundDialog$1;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "arg0"    # Landroid/content/DialogInterface;
    .param p2, "arg1"    # I

    .prologue
    .line 58
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.hearingadjust.launch"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 59
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/samsung/musicplus/dialog/AdaptSoundDialog$1;->val$context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 60
    iget-object v1, p0, Lcom/samsung/musicplus/dialog/AdaptSoundDialog$1;->this$0:Lcom/samsung/musicplus/dialog/AdaptSoundDialog;

    invoke-virtual {v1, v0}, Lcom/samsung/musicplus/dialog/AdaptSoundDialog;->startActivity(Landroid/content/Intent;)V

    .line 65
    :goto_0
    return-void

    .line 62
    :cond_0
    # getter for: Lcom/samsung/musicplus/dialog/AdaptSoundDialog;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/dialog/AdaptSoundDialog;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Not found hearingadjust application"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 63
    iget-object v1, p0, Lcom/samsung/musicplus/dialog/AdaptSoundDialog$1;->this$0:Lcom/samsung/musicplus/dialog/AdaptSoundDialog;

    # invokes: Lcom/samsung/musicplus/dialog/AdaptSoundDialog;->handlingActivityNotFound()V
    invoke-static {v1}, Lcom/samsung/musicplus/dialog/AdaptSoundDialog;->access$100(Lcom/samsung/musicplus/dialog/AdaptSoundDialog;)V

    goto :goto_0
.end method
