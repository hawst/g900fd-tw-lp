.class Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog$SimpleAdapterTheme;
.super Landroid/widget/SimpleAdapter;
.source "DlnaDmsDetailInfoDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SimpleAdapterTheme"
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private mTheme:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/List;I[Ljava/lang/String;[II)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p3, "resource"    # I
    .param p4, "from"    # [Ljava/lang/String;
    .param p5, "to"    # [I
    .param p6, "theme"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/List",
            "<+",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "*>;>;I[",
            "Ljava/lang/String;",
            "[II)V"
        }
    .end annotation

    .prologue
    .line 123
    .local p2, "data":Ljava/util/List;, "Ljava/util/List<+Ljava/util/Map<Ljava/lang/String;*>;>;"
    invoke-direct/range {p0 .. p5}, Landroid/widget/SimpleAdapter;-><init>(Landroid/content/Context;Ljava/util/List;I[Ljava/lang/String;[I)V

    .line 117
    invoke-static {}, Lcom/samsung/musicplus/util/UiUtils;->getTheme()I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog$SimpleAdapterTheme;->mTheme:I

    .line 124
    iput p6, p0, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog$SimpleAdapterTheme;->mTheme:I

    .line 125
    iput-object p1, p0, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog$SimpleAdapterTheme;->mContext:Landroid/content/Context;

    .line 126
    return-void
.end method

.method private getMainTextColor(Landroid/content/Context;I)I
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "theme"    # I

    .prologue
    .line 136
    const v0, 0x7f0b000e

    .line 137
    .local v0, "colorRes":I
    packed-switch p2, :pswitch_data_0

    .line 147
    :goto_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    return v1

    .line 139
    :pswitch_0
    const v0, 0x7f0b000d

    .line 140
    goto :goto_0

    .line 142
    :pswitch_1
    const v0, 0x7f0b000e

    .line 143
    goto :goto_0

    .line 137
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private getSubTextColor(Landroid/content/Context;I)I
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "theme"    # I

    .prologue
    .line 151
    const v0, 0x7f0b0010

    .line 152
    .local v0, "colorRes":I
    packed-switch p2, :pswitch_data_0

    .line 163
    :goto_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    return v1

    .line 155
    :pswitch_0
    const v0, 0x7f0b000f

    .line 156
    goto :goto_0

    .line 158
    :pswitch_1
    const v0, 0x7f0b0010

    .line 159
    goto :goto_0

    .line 152
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private setAllTextsColor(Landroid/content/Context;Landroid/view/View;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "main"    # Landroid/view/View;
    .param p3, "theme"    # I

    .prologue
    .line 167
    const v2, 0x7f0d0114

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 168
    .local v1, "type":Landroid/view/View;
    invoke-direct {p0, p1, p3}, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog$SimpleAdapterTheme;->getMainTextColor(Landroid/content/Context;I)I

    move-result v2

    invoke-direct {p0, v1, v2}, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog$SimpleAdapterTheme;->setTextColor(Landroid/view/View;I)V

    .line 169
    const v2, 0x7f0d0115

    invoke-virtual {p2, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 170
    .local v0, "info":Landroid/view/View;
    invoke-direct {p0, p1, p3}, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog$SimpleAdapterTheme;->getSubTextColor(Landroid/content/Context;I)I

    move-result v2

    invoke-direct {p0, v0, v2}, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog$SimpleAdapterTheme;->setTextColor(Landroid/view/View;I)V

    .line 171
    return-void
.end method

.method private setTextColor(Landroid/view/View;I)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;
    .param p2, "color"    # I

    .prologue
    .line 174
    instance-of v0, p1, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 175
    check-cast p1, Landroid/widget/TextView;

    .end local p1    # "v":Landroid/view/View;
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 177
    :cond_0
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 130
    invoke-super {p0, p1, p2, p3}, Landroid/widget/SimpleAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 131
    .local v0, "main":Landroid/view/View;
    iget-object v1, p0, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog$SimpleAdapterTheme;->mContext:Landroid/content/Context;

    iget v2, p0, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog$SimpleAdapterTheme;->mTheme:I

    invoke-direct {p0, v1, v0, v2}, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog$SimpleAdapterTheme;->setAllTextsColor(Landroid/content/Context;Landroid/view/View;I)V

    .line 132
    return-object v0
.end method
