.class Lcom/samsung/musicplus/dialog/EditTitleDialog$4;
.super Landroid/text/InputFilter$LengthFilter;
.source "EditTitleDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/dialog/EditTitleDialog;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/dialog/EditTitleDialog;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/dialog/EditTitleDialog;I)V
    .locals 0
    .param p2, "x0"    # I

    .prologue
    .line 259
    iput-object p1, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog$4;->this$0:Lcom/samsung/musicplus/dialog/EditTitleDialog;

    invoke-direct {p0, p2}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    return-void
.end method


# virtual methods
.method public filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;
    .locals 7
    .param p1, "source"    # Ljava/lang/CharSequence;
    .param p2, "start"    # I
    .param p3, "end"    # I
    .param p4, "dest"    # Landroid/text/Spanned;
    .param p5, "dstart"    # I
    .param p6, "dend"    # I

    .prologue
    const/4 v4, 0x1

    .line 263
    invoke-super/range {p0 .. p6}, Landroid/text/InputFilter$LengthFilter;->filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;

    move-result-object v0

    .line 265
    .local v0, "rst":Ljava/lang/CharSequence;
    if-eqz v0, :cond_0

    .line 266
    iget-object v1, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog$4;->this$0:Lcom/samsung/musicplus/dialog/EditTitleDialog;

    # setter for: Lcom/samsung/musicplus/dialog/EditTitleDialog;->mIsMaxName:Z
    invoke-static {v1, v4}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->access$502(Lcom/samsung/musicplus/dialog/EditTitleDialog;Z)Z

    .line 267
    iget-object v1, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog$4;->this$0:Lcom/samsung/musicplus/dialog/EditTitleDialog;

    # getter for: Lcom/samsung/musicplus/dialog/EditTitleDialog;->mToast:Landroid/widget/Toast;
    invoke-static {v1}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->access$000(Lcom/samsung/musicplus/dialog/EditTitleDialog;)Landroid/widget/Toast;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 268
    iget-object v1, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog$4;->this$0:Lcom/samsung/musicplus/dialog/EditTitleDialog;

    # getter for: Lcom/samsung/musicplus/dialog/EditTitleDialog;->mToast:Landroid/widget/Toast;
    invoke-static {v1}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->access$000(Lcom/samsung/musicplus/dialog/EditTitleDialog;)Landroid/widget/Toast;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog$4;->this$0:Lcom/samsung/musicplus/dialog/EditTitleDialog;

    const v3, 0x7f1000a5

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const/16 v6, 0x32

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    .line 270
    iget-object v1, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog$4;->this$0:Lcom/samsung/musicplus/dialog/EditTitleDialog;

    # getter for: Lcom/samsung/musicplus/dialog/EditTitleDialog;->mToast:Landroid/widget/Toast;
    invoke-static {v1}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->access$000(Lcom/samsung/musicplus/dialog/EditTitleDialog;)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 273
    :cond_0
    return-object v0
.end method
