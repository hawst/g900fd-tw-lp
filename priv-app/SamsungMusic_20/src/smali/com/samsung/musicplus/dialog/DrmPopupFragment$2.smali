.class Lcom/samsung/musicplus/dialog/DrmPopupFragment$2;
.super Ljava/lang/Object;
.source "DrmPopupFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/dialog/DrmPopupFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/dialog/DrmPopupFragment;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/dialog/DrmPopupFragment;)V
    .locals 0

    .prologue
    .line 208
    iput-object p1, p0, Lcom/samsung/musicplus/dialog/DrmPopupFragment$2;->this$0:Lcom/samsung/musicplus/dialog/DrmPopupFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 9
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    const/4 v8, 0x0

    .line 211
    iget-object v6, p0, Lcom/samsung/musicplus/dialog/DrmPopupFragment$2;->this$0:Lcom/samsung/musicplus/dialog/DrmPopupFragment;

    invoke-virtual {v6}, Lcom/samsung/musicplus/dialog/DrmPopupFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v6

    const-string v7, "path"

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 213
    .local v4, "path":Ljava/lang/String;
    iget-object v6, p0, Lcom/samsung/musicplus/dialog/DrmPopupFragment$2;->this$0:Lcom/samsung/musicplus/dialog/DrmPopupFragment;

    # invokes: Lcom/samsung/musicplus/dialog/DrmPopupFragment;->getAudioId(Ljava/lang/String;)J
    invoke-static {v6, v4}, Lcom/samsung/musicplus/dialog/DrmPopupFragment;->access$100(Lcom/samsung/musicplus/dialog/DrmPopupFragment;Ljava/lang/String;)J

    move-result-wide v2

    .line 214
    .local v2, "id":J
    const-wide/16 v6, 0x0

    cmp-long v6, v2, v6

    if-lez v6, :cond_0

    .line 215
    sget-object v6, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v6}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v6

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v5

    .line 217
    .local v5, "uri":Landroid/net/Uri;
    iget-object v6, p0, Lcom/samsung/musicplus/dialog/DrmPopupFragment$2;->this$0:Lcom/samsung/musicplus/dialog/DrmPopupFragment;

    invoke-virtual {v6}, Lcom/samsung/musicplus/dialog/DrmPopupFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 219
    .local v0, "cr":Landroid/content/ContentResolver;
    invoke-virtual {v0, v5, v8, v8}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 221
    .end local v0    # "cr":Landroid/content/ContentResolver;
    .end local v5    # "uri":Landroid/net/Uri;
    :cond_0
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 222
    .local v1, "f":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 223
    iget-object v6, p0, Lcom/samsung/musicplus/dialog/DrmPopupFragment$2;->this$0:Lcom/samsung/musicplus/dialog/DrmPopupFragment;

    invoke-virtual {v6}, Lcom/samsung/musicplus/dialog/DrmPopupFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-virtual {v6}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    const v7, 0x7f100051

    const/4 v8, 0x0

    invoke-static {v6, v7, v8}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v6

    invoke-virtual {v6}, Landroid/widget/Toast;->show()V

    .line 228
    :goto_0
    return-void

    .line 226
    :cond_1
    const-string v6, "MusicDrm"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Failed to delete file "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
