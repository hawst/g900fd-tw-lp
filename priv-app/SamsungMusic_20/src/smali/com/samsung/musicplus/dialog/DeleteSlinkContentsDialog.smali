.class public Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog;
.super Landroid/app/DialogFragment;
.source "DeleteSlinkContentsDialog.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTask;,
        Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTaskDialog;
    }
.end annotation


# static fields
.field private static DIALOG_TAG:Ljava/lang/String; = null

.field private static final EXTRA_SELECTED:Ljava/lang/String; = "selected_id"

.field private static final EXTRA_THROTTLE_TIME:Ljava/lang/String; = "throttle_time"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-string v0, "delete_slink_contents"

    sput-object v0, Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog;->DIALOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 162
    return-void
.end method

.method private static getInstance([JI)Landroid/app/DialogFragment;
    .locals 3
    .param p0, "ids"    # [J
    .param p1, "time"    # I

    .prologue
    .line 39
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 40
    .local v0, "data":Landroid/os/Bundle;
    const-string v2, "selected_id"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putLongArray(Ljava/lang/String;[J)V

    .line 41
    const-string v2, "throttle_time"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 42
    new-instance v1, Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog;

    invoke-direct {v1}, Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog;-><init>()V

    .line 43
    .local v1, "fg":Landroid/app/DialogFragment;
    invoke-virtual {v1, v0}, Landroid/app/DialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 44
    return-object v1
.end method

.method public static show(Landroid/content/Context;Landroid/app/FragmentManager;[JI)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "manager"    # Landroid/app/FragmentManager;
    .param p2, "ids"    # [J
    .param p3, "time"    # I

    .prologue
    .line 30
    sget-object v2, Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog;->DIALOG_TAG:Ljava/lang/String;

    invoke-virtual {p1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    .line 31
    .local v1, "fg":Landroid/app/Fragment;
    if-nez v1, :cond_0

    .line 33
    invoke-static {p2, p3}, Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog;->getInstance([JI)Landroid/app/DialogFragment;

    move-result-object v0

    .line 34
    .local v0, "dialog":Landroid/app/DialogFragment;
    sget-object v2, Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog;->DIALOG_TAG:Ljava/lang/String;

    invoke-virtual {v0, p1, v2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 36
    .end local v0    # "dialog":Landroid/app/DialogFragment;
    :cond_0
    return-void
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 9
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 49
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 50
    .local v0, "a":Landroid/app/Activity;
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 51
    .local v1, "builder":Landroid/app/AlertDialog$Builder;
    const v3, 0x7f1000ad

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 53
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "selected_id"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v2

    .line 54
    .local v2, "items":[J
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f001a

    array-length v5, v2

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    array-length v8, v2

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v3, v4, v5, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 57
    const v3, 0x7f100114

    new-instance v4, Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$1;

    invoke-direct {v4, p0, v0, v2}, Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$1;-><init>(Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog;Landroid/app/Activity;[J)V

    invoke-virtual {v1, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 65
    const v3, 0x7f10003a

    new-instance v4, Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$2;

    invoke-direct {v4, p0}, Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$2;-><init>(Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog;)V

    invoke-virtual {v1, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 70
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    return-object v3
.end method
