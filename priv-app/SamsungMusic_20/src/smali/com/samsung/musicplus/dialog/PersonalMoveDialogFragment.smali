.class public Lcom/samsung/musicplus/dialog/PersonalMoveDialogFragment;
.super Landroid/app/DialogFragment;
.source "PersonalMoveDialogFragment.java"

# interfaces
.implements Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$OnMoveSecretBoxListener;


# static fields
.field private static final IS_FOLDER:Ljava/lang/String; = "is_folder"

.field private static final IS_MOVING:Ljava/lang/String; = "is_moving"

.field private static final LIST_ITEMS:Ljava/lang/String; = "list_items"

.field private static final REMOVE_FROM_NOW_PLAYING:Ljava/lang/String; = "remove_from_now_playing"

.field private static final SELECTION_DEST_PATH:Ljava/lang/String; = "selection_dest_path"

.field private static final TAG:Ljava/lang/String; = "PersonalFileOperation"


# instance fields
.field private mFileCount:I

.field private mMoveSecretBoxOperation:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;

.field private mProgressDialog:Lcom/samsung/musicplus/dialog/CustomProgressDialog;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 54
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/musicplus/dialog/PersonalMoveDialogFragment;->mFileCount:I

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/dialog/PersonalMoveDialogFragment;)Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/PersonalMoveDialogFragment;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/PersonalMoveDialogFragment;->mMoveSecretBoxOperation:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;

    return-object v0
.end method

.method public static getInstance([JZZLjava/lang/String;Z)Landroid/app/DialogFragment;
    .locals 3
    .param p0, "list"    # [J
    .param p1, "moving"    # Z
    .param p2, "folder"    # Z
    .param p3, "selectionDestPath"    # Ljava/lang/String;
    .param p4, "removeFromNowPlaying"    # Z

    .prologue
    .line 58
    new-instance v1, Lcom/samsung/musicplus/dialog/PersonalMoveDialogFragment;

    invoke-direct {v1}, Lcom/samsung/musicplus/dialog/PersonalMoveDialogFragment;-><init>()V

    .line 59
    .local v1, "moveDialog":Lcom/samsung/musicplus/dialog/PersonalMoveDialogFragment;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 61
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "list_items"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putLongArray(Ljava/lang/String;[J)V

    .line 62
    const-string v2, "is_moving"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 63
    const-string v2, "is_folder"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 64
    const-string v2, "selection_dest_path"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    const-string v2, "remove_from_now_playing"

    invoke-virtual {v0, v2, p4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 66
    invoke-virtual {v1, v0}, Lcom/samsung/musicplus/dialog/PersonalMoveDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 67
    return-object v1
.end method

.method public static showPersonalMoveDialogFragment(Landroid/app/Activity;I[JZLjava/lang/String;Z)V
    .locals 4
    .param p0, "a"    # Landroid/app/Activity;
    .param p1, "listType"    # I
    .param p2, "items"    # [J
    .param p3, "isMoving"    # Z
    .param p4, "selectionDestPath"    # Ljava/lang/String;
    .param p5, "removeFromNowPlaying"    # Z

    .prologue
    const/4 v3, 0x0

    .line 199
    invoke-static {p1}, Lcom/samsung/musicplus/util/ListUtils;->isTab(I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p1}, Lcom/samsung/musicplus/util/ListUtils;->getListContentType(I)I

    move-result v1

    const/4 v2, 0x7

    if-ne v1, v2, :cond_0

    .line 201
    const/4 v1, 0x1

    invoke-static {p2, p3, v1, p4, p5}, Lcom/samsung/musicplus/dialog/PersonalMoveDialogFragment;->getInstance([JZZLjava/lang/String;Z)Landroid/app/DialogFragment;

    move-result-object v0

    .line 207
    .local v0, "moveDialog":Landroid/app/DialogFragment;
    :goto_0
    invoke-virtual {v0, v3}, Landroid/app/DialogFragment;->setCancelable(Z)V

    .line 208
    invoke-virtual {p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "personalMoveDialog"

    invoke-virtual {v0, v1, v2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 209
    return-void

    .line 204
    .end local v0    # "moveDialog":Landroid/app/DialogFragment;
    :cond_0
    invoke-static {p2, p3, v3, p4, p5}, Lcom/samsung/musicplus/dialog/PersonalMoveDialogFragment;->getInstance([JZZLjava/lang/String;Z)Landroid/app/DialogFragment;

    move-result-object v0

    .restart local v0    # "moveDialog":Landroid/app/DialogFragment;
    goto :goto_0
.end method


# virtual methods
.method public onAbortMove(Ljava/lang/String;)V
    .locals 4
    .param p1, "msg"    # Ljava/lang/String;

    .prologue
    .line 177
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/PersonalMoveDialogFragment;->mProgressDialog:Lcom/samsung/musicplus/dialog/CustomProgressDialog;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/musicplus/dialog/PersonalMoveDialogFragment;->mProgressDialog:Lcom/samsung/musicplus/dialog/CustomProgressDialog;

    invoke-virtual {v2}, Lcom/samsung/musicplus/dialog/CustomProgressDialog;->isShowing()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 178
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/PersonalMoveDialogFragment;->mProgressDialog:Lcom/samsung/musicplus/dialog/CustomProgressDialog;

    invoke-virtual {v2}, Lcom/samsung/musicplus/dialog/CustomProgressDialog;->dismiss()V

    .line 180
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/PersonalMoveDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 181
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    const v2, 0x7f1000b7

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 182
    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 183
    const v2, 0x7f100114

    new-instance v3, Lcom/samsung/musicplus/dialog/PersonalMoveDialogFragment$2;

    invoke-direct {v3, p0}, Lcom/samsung/musicplus/dialog/PersonalMoveDialogFragment$2;-><init>(Lcom/samsung/musicplus/dialog/PersonalMoveDialogFragment;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 190
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 191
    .local v1, "dialog":Landroid/app/AlertDialog;
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 192
    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 193
    return-void
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 1
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 131
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/PersonalMoveDialogFragment;->mMoveSecretBoxOperation:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;

    if-eqz v0, :cond_0

    .line 132
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/PersonalMoveDialogFragment;->mMoveSecretBoxOperation:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;

    invoke-virtual {v0}, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->stopMove()V

    .line 134
    :cond_0
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    .line 135
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 11
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v10, 0x0

    const/4 v7, 0x0

    .line 72
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/PersonalMoveDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v6

    .line 73
    .local v6, "data":Landroid/os/Bundle;
    const-string v0, "list_items"

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v1

    .line 74
    .local v1, "listItems":[J
    const-string v0, "is_moving"

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    .line 75
    .local v2, "isMoving":Z
    const-string v0, "is_folder"

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    .line 76
    .local v3, "isFolder":Z
    const-string v0, "selection_dest_path"

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 77
    .local v4, "selectionDestPath":Ljava/lang/String;
    const-string v0, "remove_from_now_playing"

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    .line 79
    .local v5, "removeFromNowPlaying":Z
    if-nez v1, :cond_0

    .line 80
    const-string v0, "PersonalFileOperation"

    const-string v8, "PersonalMoveDialogFragment, mListItems is null"

    invoke-static {v0, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v7

    .line 126
    :goto_0
    return-object v0

    .line 84
    :cond_0
    new-instance v0, Lcom/samsung/musicplus/dialog/CustomProgressDialog;

    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/PersonalMoveDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v8

    invoke-direct {v0, v8}, Lcom/samsung/musicplus/dialog/CustomProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/musicplus/dialog/PersonalMoveDialogFragment;->mProgressDialog:Lcom/samsung/musicplus/dialog/CustomProgressDialog;

    .line 85
    if-eqz v2, :cond_1

    .line 86
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/PersonalMoveDialogFragment;->mProgressDialog:Lcom/samsung/musicplus/dialog/CustomProgressDialog;

    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/PersonalMoveDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f1000a8

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Lcom/samsung/musicplus/dialog/CustomProgressDialog;->setTitle(Ljava/lang/String;)V

    .line 91
    :goto_1
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/PersonalMoveDialogFragment;->mProgressDialog:Lcom/samsung/musicplus/dialog/CustomProgressDialog;

    invoke-virtual {v0, v10}, Lcom/samsung/musicplus/dialog/CustomProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 92
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/PersonalMoveDialogFragment;->mProgressDialog:Lcom/samsung/musicplus/dialog/CustomProgressDialog;

    invoke-virtual {v0, v10}, Lcom/samsung/musicplus/dialog/CustomProgressDialog;->setCancelable(Z)V

    .line 93
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/PersonalMoveDialogFragment;->mProgressDialog:Lcom/samsung/musicplus/dialog/CustomProgressDialog;

    const/4 v8, -0x2

    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/PersonalMoveDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f10003a

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    new-instance v10, Lcom/samsung/musicplus/dialog/PersonalMoveDialogFragment$1;

    invoke-direct {v10, p0}, Lcom/samsung/musicplus/dialog/PersonalMoveDialogFragment$1;-><init>(Lcom/samsung/musicplus/dialog/PersonalMoveDialogFragment;)V

    invoke-virtual {v0, v8, v9, v10}, Lcom/samsung/musicplus/dialog/CustomProgressDialog;->setButton(ILjava/lang/String;Landroid/content/DialogInterface$OnClickListener;)V

    .line 104
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/PersonalMoveDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->getInstance(Landroid/content/Context;)Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/dialog/PersonalMoveDialogFragment;->mMoveSecretBoxOperation:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;

    .line 106
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/PersonalMoveDialogFragment;->mMoveSecretBoxOperation:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;

    invoke-virtual {v0, p0}, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->setOnMoveSecretBoxListener(Lcom/samsung/musicplus/contents/MoveSecretBoxOperation$OnMoveSecretBoxListener;)V

    .line 108
    if-eqz v3, :cond_2

    .line 109
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/PersonalMoveDialogFragment;->mProgressDialog:Lcom/samsung/musicplus/dialog/CustomProgressDialog;

    iget-object v8, p0, Lcom/samsung/musicplus/dialog/PersonalMoveDialogFragment;->mMoveSecretBoxOperation:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;

    invoke-virtual {v8, v1}, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->getTotalCount([J)I

    move-result v8

    invoke-virtual {v0, v8}, Lcom/samsung/musicplus/dialog/CustomProgressDialog;->setMax(I)V

    .line 114
    :goto_2
    if-nez p1, :cond_3

    .line 115
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/PersonalMoveDialogFragment;->mMoveSecretBoxOperation:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->startToMoveFiles([JZZLjava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_4

    .line 117
    const-string v0, "PersonalFileOperation"

    const-string v8, "onCreateDialog(). startToMoveFiles() Fail"

    invoke-static {v0, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v7

    .line 118
    goto :goto_0

    .line 88
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/PersonalMoveDialogFragment;->mProgressDialog:Lcom/samsung/musicplus/dialog/CustomProgressDialog;

    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/PersonalMoveDialogFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f1000b7

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v0, v8}, Lcom/samsung/musicplus/dialog/CustomProgressDialog;->setTitle(Ljava/lang/String;)V

    goto :goto_1

    .line 111
    :cond_2
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/PersonalMoveDialogFragment;->mProgressDialog:Lcom/samsung/musicplus/dialog/CustomProgressDialog;

    array-length v8, v1

    invoke-virtual {v0, v8}, Lcom/samsung/musicplus/dialog/CustomProgressDialog;->setMax(I)V

    goto :goto_2

    .line 121
    :cond_3
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/PersonalMoveDialogFragment;->mMoveSecretBoxOperation:Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;

    invoke-virtual {v0}, Lcom/samsung/musicplus/contents/MoveSecretBoxOperation;->isMoveFilesThreadWorking()Z

    move-result v0

    if-nez v0, :cond_4

    .line 122
    const-string v0, "PersonalFileOperation"

    const-string v8, "onCreateDialog(). MoveFilesThread is NOT working"

    invoke-static {v0, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v7

    .line 123
    goto/16 :goto_0

    .line 126
    :cond_4
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/PersonalMoveDialogFragment;->mProgressDialog:Lcom/samsung/musicplus/dialog/CustomProgressDialog;

    invoke-virtual {v0}, Lcom/samsung/musicplus/dialog/CustomProgressDialog;->build()Landroid/app/Dialog;

    move-result-object v0

    goto/16 :goto_0
.end method

.method public onFileNameExist(Ljava/lang/String;)V
    .locals 5
    .param p1, "targetPath"    # Ljava/lang/String;

    .prologue
    .line 160
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/PersonalMoveDialogFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 161
    .local v0, "a":Landroid/app/Activity;
    invoke-static {p1}, Lcom/samsung/musicplus/dialog/PersonalFileRenameDialog;->getInstance(Ljava/lang/String;)Landroid/app/DialogFragment;

    move-result-object v2

    .line 162
    .local v2, "renameDialog":Landroid/app/DialogFragment;
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/PersonalMoveDialogFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v3

    .line 163
    .local v3, "transaction":Landroid/app/FragmentTransaction;
    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/app/DialogFragment;->setCancelable(Z)V

    .line 164
    if-eqz v0, :cond_0

    .line 166
    :try_start_0
    const-string v4, "personalRenameDialog"

    invoke-virtual {v3, v2, v4}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 167
    invoke-virtual {v3}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 173
    :cond_0
    :goto_0
    return-void

    .line 169
    :catch_0
    move-exception v1

    .line 170
    .local v1, "e":Ljava/lang/IllegalStateException;
    invoke-virtual {v1}, Ljava/lang/IllegalStateException;->printStackTrace()V

    goto :goto_0
.end method

.method public onMoveFinished()V
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/PersonalMoveDialogFragment;->mProgressDialog:Lcom/samsung/musicplus/dialog/CustomProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/dialog/PersonalMoveDialogFragment;->mProgressDialog:Lcom/samsung/musicplus/dialog/CustomProgressDialog;

    invoke-virtual {v0}, Lcom/samsung/musicplus/dialog/CustomProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 140
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/PersonalMoveDialogFragment;->mProgressDialog:Lcom/samsung/musicplus/dialog/CustomProgressDialog;

    invoke-virtual {v0}, Lcom/samsung/musicplus/dialog/CustomProgressDialog;->dismiss()V

    .line 142
    :cond_0
    return-void
.end method

.method public onUpdateProgress(II)V
    .locals 2
    .param p1, "cnt"    # I
    .param p2, "progress"    # I

    .prologue
    .line 146
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/PersonalMoveDialogFragment;->mProgressDialog:Lcom/samsung/musicplus/dialog/CustomProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/dialog/PersonalMoveDialogFragment;->mProgressDialog:Lcom/samsung/musicplus/dialog/CustomProgressDialog;

    invoke-virtual {v0}, Lcom/samsung/musicplus/dialog/CustomProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 147
    if-lez p1, :cond_1

    .line 148
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/PersonalMoveDialogFragment;->mProgressDialog:Lcom/samsung/musicplus/dialog/CustomProgressDialog;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/dialog/CustomProgressDialog;->setCount(I)V

    .line 149
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/PersonalMoveDialogFragment;->mProgressDialog:Lcom/samsung/musicplus/dialog/CustomProgressDialog;

    invoke-virtual {v0, p2}, Lcom/samsung/musicplus/dialog/CustomProgressDialog;->setProgress(I)V

    .line 156
    :cond_0
    :goto_0
    return-void

    .line 151
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/PersonalMoveDialogFragment;->mProgressDialog:Lcom/samsung/musicplus/dialog/CustomProgressDialog;

    invoke-virtual {v0}, Lcom/samsung/musicplus/dialog/CustomProgressDialog;->initCount()V

    .line 152
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/PersonalMoveDialogFragment;->mProgressDialog:Lcom/samsung/musicplus/dialog/CustomProgressDialog;

    invoke-virtual {v0, p2}, Lcom/samsung/musicplus/dialog/CustomProgressDialog;->setMaxProgress(I)V

    .line 153
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/PersonalMoveDialogFragment;->mProgressDialog:Lcom/samsung/musicplus/dialog/CustomProgressDialog;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/dialog/CustomProgressDialog;->setProgress(I)V

    goto :goto_0
.end method
