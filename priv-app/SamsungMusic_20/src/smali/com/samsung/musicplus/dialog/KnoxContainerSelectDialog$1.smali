.class Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog$1;
.super Ljava/lang/Object;
.source "KnoxContainerSelectDialog.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog;)V
    .locals 0

    .prologue
    .line 175
    iput-object p1, p0, Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog$1;->this$0:Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 179
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog$1;->this$0:Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog;

    invoke-virtual {v2}, Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    .line 180
    .local v0, "dialog":Landroid/app/Dialog;
    if-eqz v0, :cond_0

    .line 181
    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 183
    :cond_0
    const-string v2, "KnoxModeManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "container id : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    new-instance v1, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;

    iget-object v2, p0, Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog$1;->this$0:Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog;

    # getter for: Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog;->mListItems:[J
    invoke-static {v2}, Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog;->access$200(Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog;)[J

    move-result-object v3

    iget-object v2, p0, Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog$1;->this$0:Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog;

    # getter for: Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog;->mArray:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog;->access$000(Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog$ContainerInfo;

    iget-object v2, v2, Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog$ContainerInfo;->name:Ljava/lang/String;

    invoke-direct {v1, v3, p4, p5, v2}, Lcom/samsung/musicplus/dialog/KnoxMoveDialogFragment;-><init>([JJLjava/lang/String;)V

    .line 186
    .local v1, "fr":Landroid/app/DialogFragment;
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/app/DialogFragment;->setCancelable(Z)V

    .line 187
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog$1;->this$0:Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog;

    invoke-virtual {v2}, Lcom/samsung/musicplus/dialog/KnoxContainerSelectDialog;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "knoxMoveDialog"

    invoke-virtual {v1, v2, v3}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 188
    return-void
.end method
