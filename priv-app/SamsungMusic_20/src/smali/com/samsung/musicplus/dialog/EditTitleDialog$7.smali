.class Lcom/samsung/musicplus/dialog/EditTitleDialog$7;
.super Ljava/lang/Object;
.source "EditTitleDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/dialog/EditTitleDialog;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/dialog/EditTitleDialog;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/dialog/EditTitleDialog;)V
    .locals 0

    .prologue
    .line 290
    iput-object p1, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog$7;->this$0:Lcom/samsung/musicplus/dialog/EditTitleDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 12
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    const/4 v11, 0x0

    .line 294
    iget-object v8, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog$7;->this$0:Lcom/samsung/musicplus/dialog/EditTitleDialog;

    invoke-virtual {v8}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 295
    .local v0, "a":Landroid/app/Activity;
    iget-object v8, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog$7;->this$0:Lcom/samsung/musicplus/dialog/EditTitleDialog;

    # getter for: Lcom/samsung/musicplus/dialog/EditTitleDialog;->mView:Landroid/view/View;
    invoke-static {v8}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->access$200(Lcom/samsung/musicplus/dialog/EditTitleDialog;)Landroid/view/View;

    move-result-object v8

    const v9, 0x7f0d00a2

    invoke-virtual {v8, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/EditText;

    .line 297
    .local v4, "edit":Landroid/widget/EditText;
    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v7

    .line 298
    .local v7, "name":Ljava/lang/String;
    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    .line 299
    .local v3, "context":Landroid/content/Context;
    iget-object v8, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog$7;->this$0:Lcom/samsung/musicplus/dialog/EditTitleDialog;

    invoke-virtual {v8}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->getActivity()Landroid/app/Activity;

    move-result-object v8

    const-string v9, "input_method"

    invoke-virtual {v8, v9}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/inputmethod/InputMethodManager;

    .line 301
    .local v6, "imm":Landroid/view/inputmethod/InputMethodManager;
    iget-object v8, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog$7;->this$0:Lcom/samsung/musicplus/dialog/EditTitleDialog;

    # getter for: Lcom/samsung/musicplus/dialog/EditTitleDialog;->mEditText:Landroid/widget/EditText;
    invoke-static {v8}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->access$600(Lcom/samsung/musicplus/dialog/EditTitleDialog;)Landroid/widget/EditText;

    move-result-object v8

    invoke-virtual {v8}, Landroid/widget/EditText;->getWindowToken()Landroid/os/IBinder;

    move-result-object v8

    invoke-virtual {v6, v8, v11}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 302
    if-eqz v7, :cond_0

    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_0

    .line 303
    iget-object v8, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog$7;->this$0:Lcom/samsung/musicplus/dialog/EditTitleDialog;

    # invokes: Lcom/samsung/musicplus/dialog/EditTitleDialog;->idForplaylist(Ljava/lang/String;)I
    invoke-static {v8, v7}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->access$700(Lcom/samsung/musicplus/dialog/EditTitleDialog;Ljava/lang/String;)I

    move-result v5

    .line 304
    .local v5, "id":I
    # getter for: Lcom/samsung/musicplus/dialog/EditTitleDialog;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->access$800()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "create dialog!!!! name = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " id = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/musicplus/library/iLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 306
    const/4 v8, -0x1

    if-eq v5, v8, :cond_1

    .line 307
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 308
    .local v2, "builder":Landroid/app/AlertDialog$Builder;
    iget-object v8, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog$7;->this$0:Lcom/samsung/musicplus/dialog/EditTitleDialog;

    const v9, 0x7f100126

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    aput-object v7, v10, v11

    invoke-virtual {v8, v9, v10}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v8

    const v9, 0x7f100114

    new-instance v10, Lcom/samsung/musicplus/dialog/EditTitleDialog$7$1;

    invoke-direct {v10, p0}, Lcom/samsung/musicplus/dialog/EditTitleDialog$7$1;-><init>(Lcom/samsung/musicplus/dialog/EditTitleDialog$7;)V

    invoke-virtual {v8, v9, v10}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 318
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 319
    .local v1, "alert":Landroid/app/AlertDialog;
    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    .line 332
    .end local v1    # "alert":Landroid/app/AlertDialog;
    .end local v2    # "builder":Landroid/app/AlertDialog$Builder;
    .end local v5    # "id":I
    :cond_0
    :goto_0
    invoke-static {}, Lcom/samsung/musicplus/widget/fragment/ActionModeRemoteHandler;->getInstance()Lcom/samsung/musicplus/widget/fragment/ActionModeRemoteHandler;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/musicplus/widget/fragment/ActionModeRemoteHandler;->notifyActionModeFinishAction()V

    .line 333
    return-void

    .line 321
    .restart local v5    # "id":I
    :cond_1
    iget-object v8, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog$7;->this$0:Lcom/samsung/musicplus/dialog/EditTitleDialog;

    # invokes: Lcom/samsung/musicplus/dialog/EditTitleDialog;->handleCreatePlaylist(Landroid/content/Context;Ljava/lang/String;)V
    invoke-static {v8, v3, v7}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->access$900(Lcom/samsung/musicplus/dialog/EditTitleDialog;Landroid/content/Context;Ljava/lang/String;)V

    .line 326
    iget-object v8, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog$7;->this$0:Lcom/samsung/musicplus/dialog/EditTitleDialog;

    # getter for: Lcom/samsung/musicplus/dialog/EditTitleDialog;->mListMode:I
    invoke-static {v8}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->access$1000(Lcom/samsung/musicplus/dialog/EditTitleDialog;)I

    move-result v8

    const/16 v9, 0x8

    if-ne v8, v9, :cond_0

    iget-object v8, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog$7;->this$0:Lcom/samsung/musicplus/dialog/EditTitleDialog;

    # getter for: Lcom/samsung/musicplus/dialog/EditTitleDialog;->mListType:I
    invoke-static {v8}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->access$1100(Lcom/samsung/musicplus/dialog/EditTitleDialog;)I

    move-result v8

    invoke-static {v8}, Lcom/samsung/musicplus/util/ListUtils;->isTab(I)Z

    move-result v8

    if-eqz v8, :cond_0

    .line 328
    iget-object v8, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog$7;->this$0:Lcom/samsung/musicplus/dialog/EditTitleDialog;

    # invokes: Lcom/samsung/musicplus/dialog/EditTitleDialog;->showProgressDialog()V
    invoke-static {v8}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->access$1200(Lcom/samsung/musicplus/dialog/EditTitleDialog;)V

    goto :goto_0
.end method
