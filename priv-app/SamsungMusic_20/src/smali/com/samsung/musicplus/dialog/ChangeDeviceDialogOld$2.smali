.class Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld$2;
.super Landroid/content/BroadcastReceiver;
.source "ChangeDeviceDialogOld.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;)V
    .locals 0

    .prologue
    .line 112
    iput-object p1, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld$2;->this$0:Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 115
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 116
    .local v0, "action":Ljava/lang/String;
    # getter for: Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->access$000()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mWfdReceiver() - Action : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    const-string v2, "android.net.wifi.p2p.PEERS_CHANGED"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 119
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld$2;->this$0:Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;

    # getter for: Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mP2pManager:Lcom/samsung/musicplus/library/wifi/P2pManager;
    invoke-static {v2}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->access$400(Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;)Lcom/samsung/musicplus/library/wifi/P2pManager;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld$2;->this$0:Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;

    # getter for: Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mWifiPeerListener:Landroid/net/wifi/p2p/WifiP2pManager$PeerListListener;
    invoke-static {v3}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->access$300(Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;)Landroid/net/wifi/p2p/WifiP2pManager$PeerListListener;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/samsung/musicplus/library/wifi/P2pManager;->requestPeers(Landroid/net/wifi/p2p/WifiP2pManager$PeerListListener;)V

    .line 127
    :cond_0
    :goto_0
    return-void

    .line 120
    :cond_1
    const-string v2, "android.net.wifi.p2p.CONNECTION_STATE_CHANGE"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 121
    const-string v2, "networkInfo"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/net/NetworkInfo;

    .line 123
    .local v1, "networkInfo":Landroid/net/NetworkInfo;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 124
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld$2;->this$0:Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;

    # getter for: Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mP2pManager:Lcom/samsung/musicplus/library/wifi/P2pManager;
    invoke-static {v2}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->access$400(Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;)Lcom/samsung/musicplus/library/wifi/P2pManager;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld$2;->this$0:Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;

    # getter for: Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->mWifiPeerListener:Landroid/net/wifi/p2p/WifiP2pManager$PeerListListener;
    invoke-static {v3}, Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;->access$300(Lcom/samsung/musicplus/dialog/ChangeDeviceDialogOld;)Landroid/net/wifi/p2p/WifiP2pManager$PeerListListener;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/samsung/musicplus/library/wifi/P2pManager;->requestPeers(Landroid/net/wifi/p2p/WifiP2pManager$PeerListListener;)V

    goto :goto_0
.end method
