.class public Lcom/samsung/musicplus/dialog/PersonalPageConfirmHelpDialog;
.super Landroid/app/DialogFragment;
.source "PersonalPageConfirmHelpDialog.java"

# interfaces
.implements Lcom/samsung/musicplus/settings/PlayerSettingPreference;


# static fields
.field private static final ITEMS_MOVING:Ljava/lang/String; = "items_moving"

.field private static final LIST_ITEMS:Ljava/lang/String; = "list_items"

.field private static final LIST_TYPE:Ljava/lang/String; = "list_type"

.field private static final REMOVE_FROM_NOW_PLAYING:Ljava/lang/String; = "remove_from_now_playing"


# instance fields
.field private mListType:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/dialog/PersonalPageConfirmHelpDialog;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/PersonalPageConfirmHelpDialog;

    .prologue
    .line 39
    iget v0, p0, Lcom/samsung/musicplus/dialog/PersonalPageConfirmHelpDialog;->mListType:I

    return v0
.end method

.method public static getInstance(I[JZZ)Landroid/app/DialogFragment;
    .locals 3
    .param p0, "listType"    # I
    .param p1, "items"    # [J
    .param p2, "isMoving"    # Z
    .param p3, "removeFromNowPlaying"    # Z

    .prologue
    .line 52
    new-instance v1, Lcom/samsung/musicplus/dialog/PersonalPageConfirmHelpDialog;

    invoke-direct {v1}, Lcom/samsung/musicplus/dialog/PersonalPageConfirmHelpDialog;-><init>()V

    .line 53
    .local v1, "confirmDialog":Lcom/samsung/musicplus/dialog/PersonalPageConfirmHelpDialog;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 54
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "list_type"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 55
    const-string v2, "list_items"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putLongArray(Ljava/lang/String;[J)V

    .line 56
    const-string v2, "items_moving"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 57
    const-string v2, "remove_from_now_playing"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 58
    invoke-virtual {v1, v0}, Lcom/samsung/musicplus/dialog/PersonalPageConfirmHelpDialog;->setArguments(Landroid/os/Bundle;)V

    .line 59
    return-object v1
.end method

.method public static showConfirmDialogFragment(Landroid/app/Activity;I[JZZ)V
    .locals 8
    .param p0, "a"    # Landroid/app/Activity;
    .param p1, "listType"    # I
    .param p2, "items"    # [J
    .param p3, "isMoving"    # Z
    .param p4, "removeFromNowPlaying"    # Z

    .prologue
    const/4 v1, 0x0

    .line 126
    const-string v0, "music_player_pref"

    invoke-virtual {p0, v0, v1}, Landroid/app/Activity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v7

    .line 128
    .local v7, "sp":Landroid/content/SharedPreferences;
    const-string v0, "personal_mode_help"

    invoke-interface {v7, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 129
    if-nez p3, :cond_0

    .line 130
    invoke-static {p0, p1, p2, p3, p4}, Lcom/samsung/musicplus/dialog/PersonalFolderSelectionDialog;->showPersonalFolderSelectionDialog(Landroid/app/Activity;I[JZZ)V

    .line 141
    :goto_0
    return-void

    .line 133
    :cond_0
    const/4 v4, 0x0

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move v3, p3

    move v5, p4

    invoke-static/range {v0 .. v5}, Lcom/samsung/musicplus/dialog/PersonalMoveDialogFragment;->showPersonalMoveDialogFragment(Landroid/app/Activity;I[JZLjava/lang/String;Z)V

    goto :goto_0

    .line 137
    :cond_1
    invoke-static {p1, p2, p3, p4}, Lcom/samsung/musicplus/dialog/PersonalPageConfirmHelpDialog;->getInstance(I[JZZ)Landroid/app/DialogFragment;

    move-result-object v6

    .line 139
    .local v6, "confirmDialog":Landroid/app/DialogFragment;
    invoke-virtual {p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v1, "confirmDialog"

    invoke-virtual {v6, v0, v1}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 14
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v0, 0x0

    .line 64
    invoke-virtual {p0, v0, v0}, Lcom/samsung/musicplus/dialog/PersonalPageConfirmHelpDialog;->setStyle(II)V

    .line 66
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/PersonalPageConfirmHelpDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v8

    .line 67
    .local v8, "data":Landroid/os/Bundle;
    const-string v0, "items_moving"

    invoke-virtual {v8, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    .line 68
    .local v3, "isMoving":Z
    const-string v0, "list_items"

    invoke-virtual {v8, v0}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v5

    .line 69
    .local v5, "listItems":[J
    const-string v0, "remove_from_now_playing"

    invoke-virtual {v8, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v6

    .line 70
    .local v6, "removeFromNowPlaying":Z
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/PersonalPageConfirmHelpDialog;->getActivity()Landroid/app/Activity;

    move-result-object v4

    .line 72
    .local v4, "a":Landroid/app/Activity;
    const-string v0, "list_type"

    const/4 v1, -0x1

    invoke-virtual {v8, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/dialog/PersonalPageConfirmHelpDialog;->mListType:I

    .line 74
    new-instance v7, Landroid/app/AlertDialog$Builder;

    invoke-direct {v7, v4}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 75
    .local v7, "builder":Landroid/app/AlertDialog$Builder;
    const-string v0, "layout_inflater"

    invoke-virtual {v4, v0}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/view/LayoutInflater;

    .line 77
    .local v10, "inflater":Landroid/view/LayoutInflater;
    const v0, 0x7f040005

    const/4 v1, 0x0

    invoke-virtual {v10, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v11

    .line 78
    .local v11, "mainView":Landroid/view/View;
    invoke-virtual {v7, v11}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 80
    const v0, 0x7f0d004b

    invoke-virtual {v11, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    .line 81
    .local v9, "help":Landroid/widget/TextView;
    if-eqz v3, :cond_0

    const v0, 0x7f1000d3

    :goto_0
    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/dialog/PersonalPageConfirmHelpDialog;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 84
    const v0, 0x7f0d004c

    invoke-virtual {v11, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    .line 85
    .local v2, "checkBox":Landroid/widget/CheckBox;
    new-instance v0, Lcom/samsung/musicplus/dialog/PersonalPageConfirmHelpDialog$1;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/dialog/PersonalPageConfirmHelpDialog$1;-><init>(Lcom/samsung/musicplus/dialog/PersonalPageConfirmHelpDialog;)V

    invoke-virtual {v2, v0}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 91
    const v0, 0x7f10005f

    invoke-virtual {v2, v0}, Landroid/widget/CheckBox;->setText(I)V

    .line 93
    if-eqz v3, :cond_1

    const v0, 0x7f1000a8

    :goto_1
    invoke-virtual {v7, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v12

    const v13, 0x7f100114

    new-instance v0, Lcom/samsung/musicplus/dialog/PersonalPageConfirmHelpDialog$3;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/samsung/musicplus/dialog/PersonalPageConfirmHelpDialog$3;-><init>(Lcom/samsung/musicplus/dialog/PersonalPageConfirmHelpDialog;Landroid/widget/CheckBox;ZLandroid/app/Activity;[JZ)V

    invoke-virtual {v12, v13, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f10003a

    new-instance v12, Lcom/samsung/musicplus/dialog/PersonalPageConfirmHelpDialog$2;

    invoke-direct {v12, p0}, Lcom/samsung/musicplus/dialog/PersonalPageConfirmHelpDialog$2;-><init>(Lcom/samsung/musicplus/dialog/PersonalPageConfirmHelpDialog;)V

    invoke-virtual {v0, v1, v12}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 121
    invoke-virtual {v7}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0

    .line 81
    .end local v2    # "checkBox":Landroid/widget/CheckBox;
    :cond_0
    const v0, 0x7f1000d4

    goto :goto_0

    .line 93
    .restart local v2    # "checkBox":Landroid/widget/CheckBox;
    :cond_1
    const v0, 0x7f1000b7

    goto :goto_1
.end method
