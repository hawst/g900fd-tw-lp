.class Lcom/samsung/musicplus/dialog/PlaySpeedDialog$3;
.super Ljava/lang/Object;
.source "PlaySpeedDialog.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/dialog/PlaySpeedDialog;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/dialog/PlaySpeedDialog;)V
    .locals 0

    .prologue
    .line 126
    iput-object p1, p0, Lcom/samsung/musicplus/dialog/PlaySpeedDialog$3;->this$0:Lcom/samsung/musicplus/dialog/PlaySpeedDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "v"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v2, 0x0

    .line 129
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/PlaySpeedDialog$3;->this$0:Lcom/samsung/musicplus/dialog/PlaySpeedDialog;

    # getter for: Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->mPause:Z
    invoke-static {v0}, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->access$300(Lcom/samsung/musicplus/dialog/PlaySpeedDialog;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 136
    :cond_0
    :goto_0
    return v2

    .line 133
    :cond_1
    const/16 v0, 0x42

    if-eq p2, v0, :cond_2

    const/16 v0, 0x17

    if-ne p2, v0, :cond_0

    .line 134
    :cond_2
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/PlaySpeedDialog$3;->this$0:Lcom/samsung/musicplus/dialog/PlaySpeedDialog;

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    # invokes: Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->handlePlaySpeed(II)Z
    invoke-static {v0, v1, v2}, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->access$400(Lcom/samsung/musicplus/dialog/PlaySpeedDialog;II)Z

    goto :goto_0
.end method
