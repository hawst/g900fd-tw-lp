.class public Lcom/samsung/musicplus/dialog/DrmPopupFragment;
.super Landroid/app/DialogFragment;
.source "DrmPopupFragment.java"


# instance fields
.field private final mBuyListener:Landroid/content/DialogInterface$OnClickListener;

.field private final mCancelListener:Landroid/content/DialogInterface$OnClickListener;

.field private final mDeleteListener:Landroid/content/DialogInterface$OnClickListener;

.field private final mPlayListener:Landroid/content/DialogInterface$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 174
    new-instance v0, Lcom/samsung/musicplus/dialog/DrmPopupFragment$1;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/dialog/DrmPopupFragment$1;-><init>(Lcom/samsung/musicplus/dialog/DrmPopupFragment;)V

    iput-object v0, p0, Lcom/samsung/musicplus/dialog/DrmPopupFragment;->mBuyListener:Landroid/content/DialogInterface$OnClickListener;

    .line 208
    new-instance v0, Lcom/samsung/musicplus/dialog/DrmPopupFragment$2;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/dialog/DrmPopupFragment$2;-><init>(Lcom/samsung/musicplus/dialog/DrmPopupFragment;)V

    iput-object v0, p0, Lcom/samsung/musicplus/dialog/DrmPopupFragment;->mDeleteListener:Landroid/content/DialogInterface$OnClickListener;

    .line 252
    new-instance v0, Lcom/samsung/musicplus/dialog/DrmPopupFragment$3;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/dialog/DrmPopupFragment$3;-><init>(Lcom/samsung/musicplus/dialog/DrmPopupFragment;)V

    iput-object v0, p0, Lcom/samsung/musicplus/dialog/DrmPopupFragment;->mPlayListener:Landroid/content/DialogInterface$OnClickListener;

    .line 260
    new-instance v0, Lcom/samsung/musicplus/dialog/DrmPopupFragment$4;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/dialog/DrmPopupFragment$4;-><init>(Lcom/samsung/musicplus/dialog/DrmPopupFragment;)V

    iput-object v0, p0, Lcom/samsung/musicplus/dialog/DrmPopupFragment;->mCancelListener:Landroid/content/DialogInterface$OnClickListener;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/dialog/DrmPopupFragment;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/DrmPopupFragment;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/dialog/DrmPopupFragment;->startBrowser(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$100(Lcom/samsung/musicplus/dialog/DrmPopupFragment;Ljava/lang/String;)J
    .locals 2
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/DrmPopupFragment;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/dialog/DrmPopupFragment;->getAudioId(Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method private getAudioId(Ljava/lang/String;)J
    .locals 7
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 232
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/DrmPopupFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 233
    .local v0, "cr":Landroid/content/ContentResolver;
    const/4 v6, 0x0

    .line 235
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    sget-object v1, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const-string v3, "_data=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 240
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 241
    const/4 v1, 0x0

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v2

    .line 245
    if-eqz v6, :cond_0

    .line 246
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 249
    :cond_0
    :goto_0
    return-wide v2

    .line 245
    :cond_1
    if-eqz v6, :cond_2

    .line 246
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 249
    :cond_2
    const-wide/16 v2, -0x1

    goto :goto_0

    .line 245
    :catchall_0
    move-exception v1

    if-eqz v6, :cond_3

    .line 246
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v1
.end method

.method private getDrmDialogFactory()Landroid/app/AlertDialog;
    .locals 9

    .prologue
    const v8, 0x1040013

    const v7, 0x1040009

    .line 69
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/DrmPopupFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 70
    .local v0, "data":Landroid/os/Bundle;
    const-string v4, "path"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/samsung/musicplus/dialog/DrmPopupFragment;->getFileName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 71
    .local v2, "name":Ljava/lang/String;
    new-instance v3, Landroid/util/TypedValue;

    invoke-direct {v3}, Landroid/util/TypedValue;-><init>()V

    .line 72
    .local v3, "out":Landroid/util/TypedValue;
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/DrmPopupFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v4

    const v5, 0x1010355

    const/4 v6, 0x1

    invoke-virtual {v4, v5, v3, v6}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 74
    const-string v4, "type"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 116
    :pswitch_0
    new-instance v4, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/DrmPopupFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget v5, v3, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const-string v5, "text1"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    invoke-direct {p0, v5}, Lcom/samsung/musicplus/dialog/DrmPopupFragment;->getMessage(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const v5, 0x104000a

    iget-object v6, p0, Lcom/samsung/musicplus/dialog/DrmPopupFragment;->mCancelListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v4, v5, v6}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 123
    .local v1, "dialog":Landroid/app/AlertDialog;
    :goto_0
    return-object v1

    .line 76
    .end local v1    # "dialog":Landroid/app/AlertDialog;
    :pswitch_1
    new-instance v4, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/DrmPopupFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget v5, v3, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const-string v5, "text1"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    const-string v6, "count"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    invoke-direct {p0, v5, v2, v6}, Lcom/samsung/musicplus/dialog/DrmPopupFragment;->getMessage(ILjava/lang/String;I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/musicplus/dialog/DrmPopupFragment;->mPlayListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v4, v8, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/musicplus/dialog/DrmPopupFragment;->mCancelListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v4, v7, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 84
    .restart local v1    # "dialog":Landroid/app/AlertDialog;
    goto :goto_0

    .line 86
    .end local v1    # "dialog":Landroid/app/AlertDialog;
    :pswitch_2
    new-instance v4, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/DrmPopupFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget v5, v3, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "text1"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    invoke-direct {p0, v6}, Lcom/samsung/musicplus/dialog/DrmPopupFragment;->getMessage(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ".\n"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "text2"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    invoke-direct {p0, v6}, Lcom/samsung/musicplus/dialog/DrmPopupFragment;->getMessage(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/musicplus/dialog/DrmPopupFragment;->mDeleteListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v4, v8, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/musicplus/dialog/DrmPopupFragment;->mCancelListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v4, v7, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 94
    .restart local v1    # "dialog":Landroid/app/AlertDialog;
    goto/16 :goto_0

    .line 96
    .end local v1    # "dialog":Landroid/app/AlertDialog;
    :pswitch_3
    new-instance v4, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/DrmPopupFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget v5, v3, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const-string v5, "text1"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    invoke-direct {p0, v5}, Lcom/samsung/musicplus/dialog/DrmPopupFragment;->getMessage(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/musicplus/dialog/DrmPopupFragment;->mBuyListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v4, v8, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/musicplus/dialog/DrmPopupFragment;->mCancelListener:Landroid/content/DialogInterface$OnClickListener;

    invoke-virtual {v4, v7, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 101
    .restart local v1    # "dialog":Landroid/app/AlertDialog;
    goto/16 :goto_0

    .line 74
    nop

    :pswitch_data_0
    .packed-switch 0xb
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method private getFileName(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 58
    const/16 v2, 0x2f

    invoke-virtual {p1, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    .line 60
    .local v0, "index":I
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_0

    .line 61
    add-int/lit8 v2, v0, 0x1

    invoke-virtual {p1, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 65
    .local v1, "name":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 63
    .end local v1    # "name":Ljava/lang/String;
    :cond_0
    const v2, 0x7f1001bb

    invoke-virtual {p0, v2}, Lcom/samsung/musicplus/dialog/DrmPopupFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    .restart local v1    # "name":Ljava/lang/String;
    goto :goto_0
.end method

.method private getMessage(I)Ljava/lang/String;
    .locals 2
    .param p1, "stringType"    # I

    .prologue
    .line 127
    const/4 v0, 0x0

    const/4 v1, -0x1

    invoke-direct {p0, p1, v0, v1}, Lcom/samsung/musicplus/dialog/DrmPopupFragment;->getMessage(ILjava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getMessage(ILjava/lang/String;I)Ljava/lang/String;
    .locals 8
    .param p1, "stringType"    # I
    .param p2, "fileName"    # Ljava/lang/String;
    .param p3, "count"    # I

    .prologue
    .line 131
    const/4 v0, 0x0

    .line 132
    .local v0, "message":Ljava/lang/String;
    packed-switch p1, :pswitch_data_0

    .line 171
    :goto_0
    return-object v0

    .line 134
    :pswitch_0
    const v1, 0x7f10006b

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/dialog/DrmPopupFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 135
    goto :goto_0

    .line 137
    :pswitch_1
    const v1, 0x7f100065

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/dialog/DrmPopupFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 138
    goto :goto_0

    .line 140
    :pswitch_2
    const v1, 0x7f10006a

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/dialog/DrmPopupFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 141
    goto :goto_0

    .line 143
    :pswitch_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/DrmPopupFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f0013

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, p3, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const v2, 0x7f100066

    invoke-virtual {p0, v2}, Lcom/samsung/musicplus/dialog/DrmPopupFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 146
    goto :goto_0

    .line 148
    :pswitch_4
    const v1, 0x7f100063

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/dialog/DrmPopupFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 149
    goto :goto_0

    .line 151
    :pswitch_5
    const v1, 0x7f100069

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/dialog/DrmPopupFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 152
    goto :goto_0

    .line 154
    :pswitch_6
    const v1, 0x7f100067

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/dialog/DrmPopupFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 155
    goto :goto_0

    .line 157
    :pswitch_7
    const v1, 0x7f100062

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/dialog/DrmPopupFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 158
    goto/16 :goto_0

    .line 160
    :pswitch_8
    const v1, 0x7f100064

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/dialog/DrmPopupFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 161
    goto/16 :goto_0

    .line 163
    :pswitch_9
    const v1, 0x7f100068

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/dialog/DrmPopupFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 164
    goto/16 :goto_0

    .line 166
    :pswitch_a
    const v1, 0x7f100124

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/dialog/DrmPopupFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 167
    goto/16 :goto_0

    .line 132
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method public static newInstance(Landroid/os/Bundle;)Lcom/samsung/musicplus/dialog/DrmPopupFragment;
    .locals 1
    .param p0, "data"    # Landroid/os/Bundle;

    .prologue
    .line 47
    new-instance v0, Lcom/samsung/musicplus/dialog/DrmPopupFragment;

    invoke-direct {v0}, Lcom/samsung/musicplus/dialog/DrmPopupFragment;-><init>()V

    .line 48
    .local v0, "f":Lcom/samsung/musicplus/dialog/DrmPopupFragment;
    invoke-virtual {v0, p0}, Lcom/samsung/musicplus/dialog/DrmPopupFragment;->setArguments(Landroid/os/Bundle;)V

    .line 49
    return-object v0
.end method

.method private startBrowser(Ljava/lang/String;)V
    .locals 4
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 182
    if-nez p1, :cond_1

    .line 185
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/DrmPopupFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f100063

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 206
    :cond_0
    :goto_0
    return-void

    .line 190
    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 191
    .local v0, "i":Landroid/content/Intent;
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 192
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 194
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/DrmPopupFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/high16 v2, 0x10000

    invoke-virtual {v1, v0, v2}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 197
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/DrmPopupFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 198
    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/dialog/DrmPopupFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 200
    :cond_2
    const-string v1, "DrmPopupFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DrmPopupFragment() - could not find a suitable activity for launching license url: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/DrmPopupFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f100024

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/samsung/musicplus/dialog/DrmPopupFragment;->getDrmDialogFactory()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method
