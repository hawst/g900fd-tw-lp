.class public Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTaskDialog;
.super Landroid/app/DialogFragment;
.source "DeleteSlinkContentsDialog.java"

# interfaces
.implements Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTask$OnTaskCompleteListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DeleteTaskDialog"
.end annotation


# static fields
.field private static DIALOG_TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 80
    const-string v0, "delete_task_slink_contents"

    sput-object v0, Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTaskDialog;->DIALOG_TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method private executeDeleteTask()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 112
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTaskDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "selected_id"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v1

    .line 113
    .local v1, "ids":[J
    new-instance v0, Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTask$DeleteItems;

    invoke-direct {v0}, Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTask$DeleteItems;-><init>()V

    .line 114
    .local v0, "data":Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTask$DeleteItems;
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTaskDialog;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    iput-object v3, v0, Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTask$DeleteItems;->context:Landroid/content/Context;

    .line 115
    iput-object v1, v0, Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTask$DeleteItems;->items:[J

    .line 116
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTaskDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "throttle_time"

    const/16 v5, 0xfa0

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    iput v3, v0, Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTask$DeleteItems;->throttle:I

    .line 118
    invoke-static {}, Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTask;->getInstance()Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTask;

    move-result-object v2

    .line 119
    .local v2, "task":Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTask;
    invoke-virtual {v2}, Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v3

    sget-object v4, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    if-ne v3, v4, :cond_0

    .line 120
    invoke-static {}, Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTask;->recreateTask()Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTask;

    move-result-object v2

    .line 122
    :cond_0
    invoke-virtual {v2, p0}, Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTask;->setOnTaskCompleteListener(Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTask$OnTaskCompleteListener;)V

    .line 123
    const/4 v3, 0x3

    new-array v3, v3, [Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTask$DeleteItems;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v4, 0x1

    aput-object v6, v3, v4

    const/4 v4, 0x2

    aput-object v6, v3, v4

    invoke-virtual {v2, v3}, Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 124
    return-void
.end method

.method private static getInstance([JI)Landroid/app/DialogFragment;
    .locals 3
    .param p0, "ids"    # [J
    .param p1, "throttleTime"    # I

    .prologue
    .line 93
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 94
    .local v0, "data":Landroid/os/Bundle;
    const-string v2, "selected_id"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putLongArray(Ljava/lang/String;[J)V

    .line 95
    const-string v2, "throttle_time"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 96
    new-instance v1, Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTaskDialog;

    invoke-direct {v1}, Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTaskDialog;-><init>()V

    .line 97
    .local v1, "fg":Landroid/app/DialogFragment;
    invoke-virtual {v1, v0}, Landroid/app/DialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 98
    return-object v1
.end method

.method private retainDeleteTask()V
    .locals 3

    .prologue
    .line 127
    invoke-static {}, Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTask;->getInstance()Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTask;

    move-result-object v0

    .line 128
    .local v0, "task":Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTask;
    invoke-virtual {v0}, Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTask;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v1

    sget-object v2, Landroid/os/AsyncTask$Status;->FINISHED:Landroid/os/AsyncTask$Status;

    if-ne v1, v2, :cond_0

    .line 129
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTaskDialog;->onTaskComplete()V

    .line 133
    :goto_0
    return-void

    .line 131
    :cond_0
    invoke-virtual {v0, p0}, Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTask;->setOnTaskCompleteListener(Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTask$OnTaskCompleteListener;)V

    goto :goto_0
.end method

.method public static show(Landroid/content/Context;Landroid/app/FragmentManager;[JI)V
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "manager"    # Landroid/app/FragmentManager;
    .param p2, "ids"    # [J
    .param p3, "throttleTime"    # I

    .prologue
    .line 84
    sget-object v2, Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTaskDialog;->DIALOG_TAG:Ljava/lang/String;

    invoke-virtual {p1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    .line 85
    .local v1, "fg":Landroid/app/Fragment;
    if-nez v1, :cond_0

    .line 87
    invoke-static {p2, p3}, Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTaskDialog;->getInstance([JI)Landroid/app/DialogFragment;

    move-result-object v0

    .line 88
    .local v0, "dialog":Landroid/app/DialogFragment;
    sget-object v2, Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTaskDialog;->DIALOG_TAG:Ljava/lang/String;

    invoke-virtual {v0, p1, v2}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 90
    .end local v0    # "dialog":Landroid/app/DialogFragment;
    :cond_0
    return-void
.end method

.method private showResultToast()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 155
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTaskDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "selected_id"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v0

    .line 156
    .local v0, "ids":[J
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTaskDialog;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f000b

    array-length v4, v0

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    array-length v6, v0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 158
    .local v1, "msg":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTaskDialog;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v1, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 159
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 103
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 104
    if-nez p1, :cond_0

    .line 105
    invoke-direct {p0}, Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTaskDialog;->executeDeleteTask()V

    .line 109
    :goto_0
    return-void

    .line 107
    :cond_0
    invoke-direct {p0}, Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTaskDialog;->retainDeleteTask()V

    goto :goto_0
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x0

    .line 138
    invoke-virtual {p0, v3}, Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTaskDialog;->setCancelable(Z)V

    .line 140
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTaskDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 141
    .local v0, "a":Landroid/app/Activity;
    new-instance v1, Landroid/app/ProgressDialog;

    invoke-direct {v1, v0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 142
    .local v1, "dialog":Landroid/app/ProgressDialog;
    const v2, 0x7f100129

    invoke-virtual {v0, v2}, Landroid/app/Activity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 143
    invoke-virtual {v1, v3}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 144
    return-object v1
.end method

.method public onTaskComplete()V
    .locals 0

    .prologue
    .line 149
    invoke-direct {p0}, Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTaskDialog;->showResultToast()V

    .line 150
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTaskDialog;->dismiss()V

    .line 151
    invoke-static {}, Lcom/samsung/musicplus/dialog/DeleteSlinkContentsDialog$DeleteTask;->releaseTask()V

    .line 152
    return-void
.end method
