.class public Lcom/samsung/musicplus/dialog/SlinkDataDialog;
.super Landroid/app/DialogFragment;
.source "SlinkDataDialog.java"

# interfaces
.implements Lcom/samsung/musicplus/settings/PlayerSettingPreference;


# static fields
.field private static final KEY_DEVICE_ID:Ljava/lang/String; = "device_id"

.field private static final KEY_DEVICE_NAME:Ljava/lang/String; = "device_name"

.field private static final KEY_SLINK_DATA_SHOWN:Ljava/lang/String; = "slink_data_shown"

.field public static final TAG:Ljava/lang/String; = "slink_data"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/dialog/SlinkDataDialog;Landroid/app/Activity;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/SlinkDataDialog;
    .param p1, "x1"    # Landroid/app/Activity;

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/dialog/SlinkDataDialog;->saveAccept(Landroid/app/Activity;)V

    return-void
.end method

.method public static getInstance(Ljava/lang/String;Ljava/lang/String;)Landroid/app/DialogFragment;
    .locals 3
    .param p0, "name"    # Ljava/lang/String;
    .param p1, "keyword"    # Ljava/lang/String;

    .prologue
    .line 38
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 39
    .local v0, "data":Landroid/os/Bundle;
    const-string v2, "device_name"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    const-string v2, "device_id"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    new-instance v1, Lcom/samsung/musicplus/dialog/SlinkDataDialog;

    invoke-direct {v1}, Lcom/samsung/musicplus/dialog/SlinkDataDialog;-><init>()V

    .line 42
    .local v1, "fg":Landroid/app/DialogFragment;
    invoke-virtual {v1, v0}, Landroid/app/DialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 43
    return-object v1
.end method

.method private getSlinkChargeDialog()Landroid/app/Dialog;
    .locals 13

    .prologue
    const/4 v12, 0x0

    .line 64
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/SlinkDataDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 66
    .local v0, "a":Landroid/app/Activity;
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-static {}, Lcom/samsung/musicplus/util/UiUtils;->isWhiteTheme()Z

    move-result v9

    if-eqz v9, :cond_0

    const/4 v9, 0x5

    :goto_0
    invoke-direct {v1, v0, v9}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;I)V

    .line 69
    .local v1, "builder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->getContext()Landroid/content/Context;

    move-result-object v3

    .line 70
    .local v3, "context":Landroid/content/Context;
    const-string v9, "layout_inflater"

    invoke-virtual {v3, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/LayoutInflater;

    .line 72
    .local v4, "inflater":Landroid/view/LayoutInflater;
    const v9, 0x7f040005

    invoke-virtual {v4, v9, v12}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 73
    .local v5, "root":Landroid/view/View;
    invoke-virtual {v1, v5}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 76
    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0b000e

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getColor(I)I

    move-result v8

    .line 79
    .local v8, "textColor":I
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 80
    .local v6, "sb":Ljava/lang/StringBuilder;
    const v9, 0x7f100143

    invoke-virtual {p0, v9}, Lcom/samsung/musicplus/dialog/SlinkDataDialog;->getText(I)Ljava/lang/CharSequence;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 81
    const-string v9, "\n"

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 82
    const v9, 0x7f100141

    invoke-virtual {p0, v9}, Lcom/samsung/musicplus/dialog/SlinkDataDialog;->getText(I)Ljava/lang/CharSequence;

    move-result-object v9

    invoke-virtual {v6, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;)Ljava/lang/StringBuilder;

    .line 84
    const v9, 0x7f0d004b

    invoke-virtual {v5, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    .line 85
    .local v7, "text":Landroid/widget/TextView;
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v7, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 86
    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setTextColor(I)V

    .line 88
    const v9, 0x7f0d004c

    invoke-virtual {v5, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    .line 89
    .local v2, "checkBox":Landroid/widget/CheckBox;
    invoke-virtual {v2, v8}, Landroid/widget/CheckBox;->setTextColor(I)V

    .line 90
    new-instance v9, Lcom/samsung/musicplus/dialog/SlinkDataDialog$1;

    invoke-direct {v9, p0}, Lcom/samsung/musicplus/dialog/SlinkDataDialog$1;-><init>(Lcom/samsung/musicplus/dialog/SlinkDataDialog;)V

    invoke-virtual {v2, v9}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 97
    const v9, 0x7f100142

    invoke-virtual {v1, v9}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v9

    const v10, 0x7f100114

    new-instance v11, Lcom/samsung/musicplus/dialog/SlinkDataDialog$2;

    invoke-direct {v11, p0, v2}, Lcom/samsung/musicplus/dialog/SlinkDataDialog$2;-><init>(Lcom/samsung/musicplus/dialog/SlinkDataDialog;Landroid/widget/CheckBox;)V

    invoke-virtual {v9, v10, v11}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v9

    const v10, 0x7f10003a

    invoke-virtual {v9, v10, v12}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v9

    invoke-virtual {v9}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v9

    return-object v9

    .line 66
    .end local v1    # "builder":Landroid/app/AlertDialog$Builder;
    .end local v2    # "checkBox":Landroid/widget/CheckBox;
    .end local v3    # "context":Landroid/content/Context;
    .end local v4    # "inflater":Landroid/view/LayoutInflater;
    .end local v5    # "root":Landroid/view/View;
    .end local v6    # "sb":Ljava/lang/StringBuilder;
    .end local v7    # "text":Landroid/widget/TextView;
    .end local v8    # "textColor":I
    :cond_0
    const/4 v9, 0x4

    goto/16 :goto_0
.end method

.method public static isUserAcceptWarning(Landroid/app/Activity;)Z
    .locals 3
    .param p0, "a"    # Landroid/app/Activity;

    .prologue
    const/4 v2, 0x0

    .line 47
    const-string v1, "music_player_pref"

    invoke-virtual {p0, v1, v2}, Landroid/app/Activity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 48
    .local v0, "sp":Landroid/content/SharedPreferences;
    const-string v1, "slink_data_shown"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    return v1
.end method

.method private saveAccept(Landroid/app/Activity;)V
    .locals 4
    .param p1, "a"    # Landroid/app/Activity;

    .prologue
    .line 52
    const-string v2, "music_player_pref"

    const/4 v3, 0x0

    invoke-virtual {p1, v2, v3}, Landroid/app/Activity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 53
    .local v1, "sp":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 54
    .local v0, "edit":Landroid/content/SharedPreferences$Editor;
    const-string v2, "slink_data_shown"

    const/4 v3, 0x1

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 55
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 56
    return-void
.end method


# virtual methods
.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/samsung/musicplus/dialog/SlinkDataDialog;->getSlinkChargeDialog()Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method
