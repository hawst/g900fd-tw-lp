.class Lcom/samsung/musicplus/dialog/LowMemoryPopup$4;
.super Ljava/lang/Object;
.source "LowMemoryPopup.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/dialog/LowMemoryPopup;->showLowMemoryMsg()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/dialog/LowMemoryPopup;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/dialog/LowMemoryPopup;)V
    .locals 0

    .prologue
    .line 98
    iput-object p1, p0, Lcom/samsung/musicplus/dialog/LowMemoryPopup$4;->this$0:Lcom/samsung/musicplus/dialog/LowMemoryPopup;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "id"    # I

    .prologue
    .line 101
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/LowMemoryPopup$4;->this$0:Lcom/samsung/musicplus/dialog/LowMemoryPopup;

    # getter for: Lcom/samsung/musicplus/dialog/LowMemoryPopup;->mMemoryMessageBox:Landroid/app/AlertDialog;
    invoke-static {v2}, Lcom/samsung/musicplus/dialog/LowMemoryPopup;->access$000(Lcom/samsung/musicplus/dialog/LowMemoryPopup;)Landroid/app/AlertDialog;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog;->dismiss()V

    .line 102
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->killMusicProcess()V

    .line 103
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.settings.INTERNAL_STORAGE_SETTINGS"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 104
    .local v1, "intent":Landroid/content/Intent;
    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 106
    :try_start_0
    # getter for: Lcom/samsung/musicplus/dialog/LowMemoryPopup;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/samsung/musicplus/dialog/LowMemoryPopup;->access$100()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 112
    :goto_0
    return-void

    .line 107
    :catch_0
    move-exception v0

    .line 108
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    # getter for: Lcom/samsung/musicplus/dialog/LowMemoryPopup;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/dialog/LowMemoryPopup;->access$200()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Activity Not found!!!"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    .line 110
    # getter for: Lcom/samsung/musicplus/dialog/LowMemoryPopup;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/samsung/musicplus/dialog/LowMemoryPopup;->access$100()Landroid/content/Context;

    move-result-object v2

    const-string v3, "Target Activity Not Found"

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
