.class public Lcom/samsung/musicplus/dialog/NotiDialog;
.super Landroid/app/DialogFragment;
.source "NotiDialog.java"


# instance fields
.field private mAlertDialog:Landroid/app/AlertDialog;

.field private mMessage:I

.field private mTitle:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 18
    return-void
.end method

.method public constructor <init>(II)V
    .locals 0
    .param p1, "title"    # I
    .param p2, "message"    # I

    .prologue
    .line 20
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 21
    iput p1, p0, Lcom/samsung/musicplus/dialog/NotiDialog;->mTitle:I

    .line 22
    iput p2, p0, Lcom/samsung/musicplus/dialog/NotiDialog;->mMessage:I

    .line 23
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 27
    if-eqz p1, :cond_0

    .line 28
    const-string v0, "title"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/dialog/NotiDialog;->mTitle:I

    .line 29
    const-string v0, "message"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/dialog/NotiDialog;->mMessage:I

    .line 31
    :cond_0
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCreate(Landroid/os/Bundle;)V

    .line 32
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 37
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/NotiDialog;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget v1, p0, Lcom/samsung/musicplus/dialog/NotiDialog;->mTitle:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/musicplus/dialog/NotiDialog;->mMessage:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f100114

    new-instance v2, Lcom/samsung/musicplus/dialog/NotiDialog$1;

    invoke-direct {v2, p0}, Lcom/samsung/musicplus/dialog/NotiDialog$1;-><init>(Lcom/samsung/musicplus/dialog/NotiDialog;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/dialog/NotiDialog;->mAlertDialog:Landroid/app/AlertDialog;

    .line 46
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/NotiDialog;->mAlertDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 51
    const-string v0, "title"

    iget v1, p0, Lcom/samsung/musicplus/dialog/NotiDialog;->mTitle:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 52
    const-string v0, "message"

    iget v1, p0, Lcom/samsung/musicplus/dialog/NotiDialog;->mMessage:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 53
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 54
    return-void
.end method
