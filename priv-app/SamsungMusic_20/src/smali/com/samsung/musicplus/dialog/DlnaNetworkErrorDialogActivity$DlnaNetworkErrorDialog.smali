.class public Lcom/samsung/musicplus/dialog/DlnaNetworkErrorDialogActivity$DlnaNetworkErrorDialog;
.super Landroid/app/DialogFragment;
.source "DlnaNetworkErrorDialogActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/dialog/DlnaNetworkErrorDialogActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DlnaNetworkErrorDialog"
.end annotation


# static fields
.field private static final KEY_DEVICE:Ljava/lang/String; = "name"


# instance fields
.field private mDeviceName:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 59
    return-void
.end method

.method public static getInstance(Ljava/lang/String;)Landroid/app/DialogFragment;
    .locals 3
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 50
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 51
    .local v0, "data":Landroid/os/Bundle;
    const-string v2, "name"

    invoke-virtual {v0, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    new-instance v1, Lcom/samsung/musicplus/dialog/DlnaNetworkErrorDialogActivity$DlnaNetworkErrorDialog;

    invoke-direct {v1}, Lcom/samsung/musicplus/dialog/DlnaNetworkErrorDialogActivity$DlnaNetworkErrorDialog;-><init>()V

    .line 53
    .local v1, "fg":Landroid/app/DialogFragment;
    invoke-virtual {v1, v0}, Landroid/app/DialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 54
    return-object v1
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 1
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 63
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/DlnaNetworkErrorDialogActivity$DlnaNetworkErrorDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 64
    .local v0, "a":Landroid/app/Activity;
    if-eqz v0, :cond_0

    .line 65
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 67
    :cond_0
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    .line 68
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 9
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v8, 0x0

    .line 72
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/DlnaNetworkErrorDialogActivity$DlnaNetworkErrorDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    .line 73
    .local v2, "data":Landroid/os/Bundle;
    const-string v5, "name"

    invoke-virtual {v2, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/musicplus/dialog/DlnaNetworkErrorDialogActivity$DlnaNetworkErrorDialog;->mDeviceName:Ljava/lang/String;

    .line 75
    const/4 v3, 0x0

    .line 76
    .local v3, "errText":Ljava/lang/String;
    const/4 v4, 0x0

    .line 77
    .local v4, "errTitle":Ljava/lang/String;
    iget-object v5, p0, Lcom/samsung/musicplus/dialog/DlnaNetworkErrorDialogActivity$DlnaNetworkErrorDialog;->mDeviceName:Ljava/lang/String;

    if-eqz v5, :cond_0

    .line 78
    const v5, 0x7f10005a

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    iget-object v7, p0, Lcom/samsung/musicplus/dialog/DlnaNetworkErrorDialogActivity$DlnaNetworkErrorDialog;->mDeviceName:Ljava/lang/String;

    aput-object v7, v6, v8

    invoke-virtual {p0, v5, v6}, Lcom/samsung/musicplus/dialog/DlnaNetworkErrorDialogActivity$DlnaNetworkErrorDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 79
    const v5, 0x7f1000e0

    invoke-virtual {p0, v5}, Lcom/samsung/musicplus/dialog/DlnaNetworkErrorDialogActivity$DlnaNetworkErrorDialog;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 85
    :goto_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/DlnaNetworkErrorDialogActivity$DlnaNetworkErrorDialog;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-direct {v0, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 86
    .local v0, "builder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 88
    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 89
    const v5, 0x7f100114

    new-instance v6, Lcom/samsung/musicplus/dialog/DlnaNetworkErrorDialogActivity$DlnaNetworkErrorDialog$1;

    invoke-direct {v6, p0}, Lcom/samsung/musicplus/dialog/DlnaNetworkErrorDialogActivity$DlnaNetworkErrorDialog$1;-><init>(Lcom/samsung/musicplus/dialog/DlnaNetworkErrorDialogActivity$DlnaNetworkErrorDialog;)V

    invoke-virtual {v0, v5, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 100
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    .line 101
    .local v1, "d":Landroid/app/Dialog;
    invoke-virtual {v1, v8}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 102
    return-object v1

    .line 81
    .end local v0    # "builder":Landroid/app/AlertDialog$Builder;
    .end local v1    # "d":Landroid/app/Dialog;
    :cond_0
    const v5, 0x7f100125

    invoke-virtual {p0, v5}, Lcom/samsung/musicplus/dialog/DlnaNetworkErrorDialogActivity$DlnaNetworkErrorDialog;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 82
    const v5, 0x7f10007d

    invoke-virtual {p0, v5}, Lcom/samsung/musicplus/dialog/DlnaNetworkErrorDialogActivity$DlnaNetworkErrorDialog;->getString(I)Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method
