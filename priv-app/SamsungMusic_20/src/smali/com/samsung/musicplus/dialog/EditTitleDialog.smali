.class public Lcom/samsung/musicplus/dialog/EditTitleDialog;
.super Landroid/app/DialogFragment;
.source "EditTitleDialog.java"

# interfaces
.implements Lcom/samsung/musicplus/util/UiUtils$Defs;


# static fields
.field private static final CLASSNAME:Ljava/lang/String;

.field private static final MAX_LENGTH_OF_PLAYLIST_NAME:I = 0x32


# instance fields
.field public filterSpecialChar:Landroid/text/InputFilter;

.field private mActivity:Landroid/app/Activity;

.field private mDefaultName:Ljava/lang/String;

.field private mDialog:Landroid/app/AlertDialog;

.field private mEditText:Landroid/widget/EditText;

.field private mIsKeyboardVisible:Z

.field private mIsMaxName:Z

.field private mKeyWord:Ljava/lang/String;

.field private mKeyboardIntentFilter:Landroid/content/IntentFilter;

.field private mKeyboardReceiver:Landroid/content/BroadcastReceiver;

.field private mListMode:I

.field private mListType:I

.field private final mLoadingHandler:Landroid/os/Handler;

.field private mLoadingProgress:Landroid/app/ProgressDialog;

.field private mPlayListAlbumArt:Landroid/graphics/Bitmap;

.field protected mPlayListView:Landroid/widget/ListView;

.field protected mPlaylistListViewLayout:Landroid/widget/LinearLayout;

.field private mReceiveTime:J

.field private mSelectedListId:[J

.field private mShowSIP:Ljava/lang/Runnable;

.field private mToast:Landroid/widget/Toast;

.field private mView:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 64
    const-class v0, Lcom/samsung/musicplus/dialog/EditTitleDialog;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->CLASSNAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 134
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 68
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mIsMaxName:Z

    .line 90
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mReceiveTime:J

    .line 92
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mIsKeyboardVisible:Z

    .line 96
    iput-object v2, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mPlaylistListViewLayout:Landroid/widget/LinearLayout;

    .line 98
    iput-object v2, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mPlayListView:Landroid/widget/ListView;

    .line 100
    iput-object v2, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mLoadingProgress:Landroid/app/ProgressDialog;

    .line 102
    iput-object v2, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mDefaultName:Ljava/lang/String;

    .line 104
    new-instance v0, Lcom/samsung/musicplus/dialog/EditTitleDialog$1;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/dialog/EditTitleDialog$1;-><init>(Lcom/samsung/musicplus/dialog/EditTitleDialog;)V

    iput-object v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->filterSpecialChar:Landroid/text/InputFilter;

    .line 486
    new-instance v0, Lcom/samsung/musicplus/dialog/EditTitleDialog$11;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/dialog/EditTitleDialog$11;-><init>(Lcom/samsung/musicplus/dialog/EditTitleDialog;)V

    iput-object v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mShowSIP:Ljava/lang/Runnable;

    .line 732
    iput-object v2, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mActivity:Landroid/app/Activity;

    .line 754
    new-instance v0, Lcom/samsung/musicplus/dialog/EditTitleDialog$13;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/dialog/EditTitleDialog$13;-><init>(Lcom/samsung/musicplus/dialog/EditTitleDialog;)V

    iput-object v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mLoadingHandler:Landroid/os/Handler;

    .line 138
    return-void
.end method

.method public constructor <init>(II)V
    .locals 3
    .param p1, "listType"    # I
    .param p2, "listMode"    # I

    .prologue
    const/4 v2, 0x0

    .line 140
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 68
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mIsMaxName:Z

    .line 90
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mReceiveTime:J

    .line 92
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mIsKeyboardVisible:Z

    .line 96
    iput-object v2, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mPlaylistListViewLayout:Landroid/widget/LinearLayout;

    .line 98
    iput-object v2, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mPlayListView:Landroid/widget/ListView;

    .line 100
    iput-object v2, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mLoadingProgress:Landroid/app/ProgressDialog;

    .line 102
    iput-object v2, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mDefaultName:Ljava/lang/String;

    .line 104
    new-instance v0, Lcom/samsung/musicplus/dialog/EditTitleDialog$1;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/dialog/EditTitleDialog$1;-><init>(Lcom/samsung/musicplus/dialog/EditTitleDialog;)V

    iput-object v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->filterSpecialChar:Landroid/text/InputFilter;

    .line 486
    new-instance v0, Lcom/samsung/musicplus/dialog/EditTitleDialog$11;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/dialog/EditTitleDialog$11;-><init>(Lcom/samsung/musicplus/dialog/EditTitleDialog;)V

    iput-object v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mShowSIP:Ljava/lang/Runnable;

    .line 732
    iput-object v2, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mActivity:Landroid/app/Activity;

    .line 754
    new-instance v0, Lcom/samsung/musicplus/dialog/EditTitleDialog$13;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/dialog/EditTitleDialog$13;-><init>(Lcom/samsung/musicplus/dialog/EditTitleDialog;)V

    iput-object v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mLoadingHandler:Landroid/os/Handler;

    .line 141
    iput p1, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mListType:I

    .line 142
    iput p2, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mListMode:I

    .line 143
    iput-object v2, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mKeyWord:Ljava/lang/String;

    .line 144
    return-void
.end method

.method public constructor <init>(IILjava/lang/String;)V
    .locals 3
    .param p1, "listType"    # I
    .param p2, "listMode"    # I
    .param p3, "keyWord"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 146
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 68
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mIsMaxName:Z

    .line 90
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mReceiveTime:J

    .line 92
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mIsKeyboardVisible:Z

    .line 96
    iput-object v2, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mPlaylistListViewLayout:Landroid/widget/LinearLayout;

    .line 98
    iput-object v2, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mPlayListView:Landroid/widget/ListView;

    .line 100
    iput-object v2, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mLoadingProgress:Landroid/app/ProgressDialog;

    .line 102
    iput-object v2, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mDefaultName:Ljava/lang/String;

    .line 104
    new-instance v0, Lcom/samsung/musicplus/dialog/EditTitleDialog$1;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/dialog/EditTitleDialog$1;-><init>(Lcom/samsung/musicplus/dialog/EditTitleDialog;)V

    iput-object v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->filterSpecialChar:Landroid/text/InputFilter;

    .line 486
    new-instance v0, Lcom/samsung/musicplus/dialog/EditTitleDialog$11;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/dialog/EditTitleDialog$11;-><init>(Lcom/samsung/musicplus/dialog/EditTitleDialog;)V

    iput-object v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mShowSIP:Ljava/lang/Runnable;

    .line 732
    iput-object v2, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mActivity:Landroid/app/Activity;

    .line 754
    new-instance v0, Lcom/samsung/musicplus/dialog/EditTitleDialog$13;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/dialog/EditTitleDialog$13;-><init>(Lcom/samsung/musicplus/dialog/EditTitleDialog;)V

    iput-object v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mLoadingHandler:Landroid/os/Handler;

    .line 147
    iput p1, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mListType:I

    .line 148
    iput p2, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mListMode:I

    .line 149
    iput-object p3, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mKeyWord:Ljava/lang/String;

    .line 150
    return-void
.end method

.method public constructor <init>(II[J)V
    .locals 3
    .param p1, "listType"    # I
    .param p2, "listMode"    # I
    .param p3, "selectedListId"    # [J

    .prologue
    const/4 v2, 0x0

    .line 152
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 68
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mIsMaxName:Z

    .line 90
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mReceiveTime:J

    .line 92
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mIsKeyboardVisible:Z

    .line 96
    iput-object v2, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mPlaylistListViewLayout:Landroid/widget/LinearLayout;

    .line 98
    iput-object v2, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mPlayListView:Landroid/widget/ListView;

    .line 100
    iput-object v2, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mLoadingProgress:Landroid/app/ProgressDialog;

    .line 102
    iput-object v2, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mDefaultName:Ljava/lang/String;

    .line 104
    new-instance v0, Lcom/samsung/musicplus/dialog/EditTitleDialog$1;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/dialog/EditTitleDialog$1;-><init>(Lcom/samsung/musicplus/dialog/EditTitleDialog;)V

    iput-object v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->filterSpecialChar:Landroid/text/InputFilter;

    .line 486
    new-instance v0, Lcom/samsung/musicplus/dialog/EditTitleDialog$11;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/dialog/EditTitleDialog$11;-><init>(Lcom/samsung/musicplus/dialog/EditTitleDialog;)V

    iput-object v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mShowSIP:Ljava/lang/Runnable;

    .line 732
    iput-object v2, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mActivity:Landroid/app/Activity;

    .line 754
    new-instance v0, Lcom/samsung/musicplus/dialog/EditTitleDialog$13;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/dialog/EditTitleDialog$13;-><init>(Lcom/samsung/musicplus/dialog/EditTitleDialog;)V

    iput-object v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mLoadingHandler:Landroid/os/Handler;

    .line 153
    iput p1, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mListType:I

    .line 154
    iput p2, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mListMode:I

    .line 155
    iput-object p3, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mSelectedListId:[J

    .line 156
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/dialog/EditTitleDialog;)Landroid/widget/Toast;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/EditTitleDialog;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mToast:Landroid/widget/Toast;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/musicplus/dialog/EditTitleDialog;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/EditTitleDialog;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mShowSIP:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/samsung/musicplus/dialog/EditTitleDialog;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/EditTitleDialog;

    .prologue
    .line 62
    iget v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mListMode:I

    return v0
.end method

.method static synthetic access$1100(Lcom/samsung/musicplus/dialog/EditTitleDialog;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/EditTitleDialog;

    .prologue
    .line 62
    iget v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mListType:I

    return v0
.end method

.method static synthetic access$1200(Lcom/samsung/musicplus/dialog/EditTitleDialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/EditTitleDialog;

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->showProgressDialog()V

    return-void
.end method

.method static synthetic access$1302(Lcom/samsung/musicplus/dialog/EditTitleDialog;J)J
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/EditTitleDialog;
    .param p1, "x1"    # J

    .prologue
    .line 62
    iput-wide p1, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mReceiveTime:J

    return-wide p1
.end method

.method static synthetic access$1402(Lcom/samsung/musicplus/dialog/EditTitleDialog;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/EditTitleDialog;
    .param p1, "x1"    # Z

    .prologue
    .line 62
    iput-boolean p1, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mIsKeyboardVisible:Z

    return p1
.end method

.method static synthetic access$1500(Lcom/samsung/musicplus/dialog/EditTitleDialog;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/EditTitleDialog;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mLoadingHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/samsung/musicplus/dialog/EditTitleDialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/EditTitleDialog;

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->hideProgressDialog()V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/musicplus/dialog/EditTitleDialog;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/EditTitleDialog;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/musicplus/dialog/EditTitleDialog;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/EditTitleDialog;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mDefaultName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/musicplus/dialog/EditTitleDialog;)Landroid/app/AlertDialog;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/EditTitleDialog;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mDialog:Landroid/app/AlertDialog;

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/musicplus/dialog/EditTitleDialog;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/EditTitleDialog;

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mIsMaxName:Z

    return v0
.end method

.method static synthetic access$502(Lcom/samsung/musicplus/dialog/EditTitleDialog;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/EditTitleDialog;
    .param p1, "x1"    # Z

    .prologue
    .line 62
    iput-boolean p1, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mIsMaxName:Z

    return p1
.end method

.method static synthetic access$600(Lcom/samsung/musicplus/dialog/EditTitleDialog;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/EditTitleDialog;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mEditText:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$700(Lcom/samsung/musicplus/dialog/EditTitleDialog;Ljava/lang/String;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/EditTitleDialog;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->idForplaylist(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method static synthetic access$800()Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->CLASSNAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$900(Lcom/samsung/musicplus/dialog/EditTitleDialog;Landroid/content/Context;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/dialog/EditTitleDialog;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 62
    invoke-direct {p0, p1, p2}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->handleCreatePlaylist(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method

.method private fillUpPlaylistName()Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 457
    iget v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mListMode:I

    const/4 v2, 0x6

    if-eq v0, v2, :cond_0

    iget v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mListMode:I

    const/4 v2, 0x7

    if-eq v0, v2, :cond_0

    iget v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mListMode:I

    const/16 v2, 0xa

    if-eq v0, v2, :cond_0

    iget v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mListMode:I

    const/16 v2, 0x10

    if-ne v0, v2, :cond_2

    .line 462
    :cond_0
    invoke-direct {p0}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->makePlaylistName()Ljava/lang/String;

    move-result-object v8

    .line 463
    .local v8, "suggestName":Ljava/lang/String;
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v0, v8}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 482
    .end local v8    # "suggestName":Ljava/lang/String;
    :cond_1
    :goto_0
    return-object v3

    .line 466
    :cond_2
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mKeyWord:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mKeyWord:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_1

    .line 470
    :cond_3
    invoke-direct {p0}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->getPlaylistUri()Landroid/net/Uri;

    move-result-object v0

    iget-object v2, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mKeyWord:Ljava/lang/String;

    invoke-static {v0, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 471
    .local v1, "uri":Landroid/net/Uri;
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "name"

    aput-object v5, v2, v4

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 474
    .local v6, "c":Landroid/database/Cursor;
    const-string v7, ""

    .line 475
    .local v7, "playListName":Ljava/lang/String;
    if-eqz v6, :cond_4

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 476
    const-string v0, "name"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 478
    :cond_4
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v0, v7}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 479
    if-eqz v6, :cond_5

    .line 480
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_5
    move-object v3, v7

    .line 482
    goto :goto_0
.end method

.method private getDialogOKResource(I)I
    .locals 1
    .param p1, "listMode"    # I

    .prologue
    .line 579
    const/4 v0, -0x1

    .line 580
    .local v0, "dialogTitle":I
    packed-switch p1, :pswitch_data_0

    .line 595
    :pswitch_0
    const v0, 0x7f100114

    .line 598
    :goto_0
    return v0

    .line 584
    :pswitch_1
    const v0, 0x7f100047

    .line 585
    goto :goto_0

    .line 587
    :pswitch_2
    const v0, 0x7f100147

    .line 588
    goto :goto_0

    .line 592
    :pswitch_3
    const v0, 0x7f1000b0

    .line 593
    goto :goto_0

    .line 580
    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_1
        :pswitch_1
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private getDialogTitleResource(I)I
    .locals 1
    .param p1, "listMode"    # I

    .prologue
    .line 558
    const/4 v0, -0x1

    .line 559
    .local v0, "dialogTitle":I
    packed-switch p1, :pswitch_data_0

    .line 575
    :goto_0
    :pswitch_0
    return v0

    .line 563
    :pswitch_1
    const v0, 0x7f1000ac

    .line 564
    goto :goto_0

    .line 567
    :pswitch_2
    const v0, 0x7f1000b1

    .line 568
    goto :goto_0

    .line 570
    :pswitch_3
    const v0, 0x7f10000f

    .line 571
    goto :goto_0

    .line 559
    nop

    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method private getPlaylistUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 393
    sget-object v0, Lcom/samsung/musicplus/util/ListUtils;->PLAYLISTS_EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    return-object v0
.end method

.method private handleCreatePlaylist(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "name"    # Ljava/lang/String;

    .prologue
    .line 608
    iget-object v1, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mKeyWord:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mKeyWord:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-gez v1, :cond_0

    .line 615
    :goto_0
    return-void

    .line 612
    :cond_0
    iget-object v1, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mKeyWord:Ljava/lang/String;

    invoke-direct {p0, p1, p2, v1}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->savePlayList(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 613
    invoke-direct {p0, p2}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->idForplaylist(Ljava/lang/String;)I

    move-result v0

    .line 614
    .local v0, "id":I
    invoke-direct {p0, p1, p2, v0}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->onHandleCreatePlaylist(Landroid/content/Context;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method private hideProgressDialog()V
    .locals 1

    .prologue
    .line 746
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mLoadingProgress:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mLoadingProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 747
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mActivity:Landroid/app/Activity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->isDestroyed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 748
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mLoadingProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 750
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mLoadingProgress:Landroid/app/ProgressDialog;

    .line 752
    :cond_1
    return-void
.end method

.method private idForplaylist(Ljava/lang/String;)I
    .locals 8
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 653
    const/4 v6, 0x0

    .line 654
    .local v6, "c":Landroid/database/Cursor;
    const/4 v7, -0x1

    .line 656
    .local v7, "id":I
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-direct {p0}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->getPlaylistUri()Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const-string v3, "name=? COLLATE NOCASE"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    const-string v5, "name"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 661
    if-eqz v6, :cond_2

    .line 662
    sget-object v0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "count = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 663
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 664
    invoke-interface {v6}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v0

    if-nez v0, :cond_0

    .line 665
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v7

    .line 671
    :cond_0
    :goto_0
    if-eqz v6, :cond_1

    .line 672
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 675
    :cond_1
    sget-object v0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "idForPlaylist !!!! name = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " id = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 676
    return v7

    .line 668
    :cond_2
    :try_start_1
    sget-object v0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->CLASSNAME:Ljava/lang/String;

    const-string v1, "idForPlaylist Couldn\'t get cursor"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 671
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_3

    .line 672
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_3
    throw v0
.end method

.method private makePlaylistName()Ljava/lang/String;
    .locals 14

    .prologue
    .line 680
    const v1, 0x7f1000da

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 681
    .local v7, "defaultName":Ljava/lang/String;
    const/4 v9, 0x1

    .line 683
    .local v9, "num":I
    const/4 v1, 0x1

    new-array v2, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v4, "name"

    aput-object v4, v2, v1

    .line 686
    .local v2, "cols":[Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 687
    .local v0, "resolver":Landroid/content/ContentResolver;
    const-string v3, "name != \'\'"

    .line 688
    .local v3, "whereclause":Ljava/lang/String;
    invoke-direct {p0}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->getPlaylistUri()Landroid/net/Uri;

    move-result-object v1

    const/4 v4, 0x0

    const-string v5, "name"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 691
    .local v6, "c":Landroid/database/Cursor;
    if-nez v6, :cond_0

    .line 692
    const/4 v12, 0x0

    .line 717
    :goto_0
    return-object v12

    .line 695
    :cond_0
    const-string v1, "%s %03d"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v7, v4, v5

    const/4 v5, 0x1

    add-int/lit8 v10, v9, 0x1

    .end local v9    # "num":I
    .local v10, "num":I
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v4, v5

    invoke-static {v1, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    .line 703
    .local v12, "suggestedname":Ljava/lang/String;
    const/4 v8, 0x0

    .local v8, "done":Z
    move v9, v10

    .line 704
    .end local v10    # "num":I
    .restart local v9    # "num":I
    :cond_1
    if-nez v8, :cond_3

    .line 705
    const/4 v8, 0x1

    .line 706
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 707
    :goto_1
    invoke-interface {v6}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v1

    if-nez v1, :cond_1

    .line 708
    const/4 v1, 0x0

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 709
    .local v11, "playlistname":Ljava/lang/String;
    invoke-virtual {v11, v12}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_2

    .line 710
    const-string v1, "%s %03d"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v7, v4, v5

    const/4 v5, 0x1

    add-int/lit8 v10, v9, 0x1

    .end local v9    # "num":I
    .restart local v10    # "num":I
    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v4, v5

    invoke-static {v1, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    .line 711
    const/4 v8, 0x0

    move v9, v10

    .line 713
    .end local v10    # "num":I
    .restart local v9    # "num":I
    :cond_2
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    goto :goto_1

    .line 716
    .end local v11    # "playlistname":Ljava/lang/String;
    :cond_3
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method

.method private onHandleCreatePlaylist(Landroid/content/Context;Ljava/lang/String;I)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "id"    # I

    .prologue
    .line 404
    sget-object v1, Lcom/samsung/musicplus/dialog/EditTitleDialog;->CLASSNAME:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onHandleCreatePlaylist Type = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mListType:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Mode = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mListMode:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " PlaylistName = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " PlaylistId = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/samsung/musicplus/library/iLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 406
    iget v1, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mListType:I

    invoke-static {v1}, Lcom/samsung/musicplus/util/ListUtils;->isTrack(I)Z

    move-result v1

    if-nez v1, :cond_0

    iget v1, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mListMode:I

    const/4 v3, 0x6

    if-ne v1, v3, :cond_3

    .line 407
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->getActivity()Landroid/app/Activity;

    move-result-object v2

    .line 408
    .local v2, "a":Landroid/app/Activity;
    iget v1, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mListMode:I

    const/16 v3, 0x8

    if-ne v1, v3, :cond_2

    .line 409
    sget-boolean v1, Lcom/samsung/musicplus/util/Features;->FLAG_SUPPORT_SPLIT_LIST_VIEW:Z

    if-eqz v1, :cond_1

    instance-of v1, v2, Lcom/samsung/musicplus/MusicMainActivity;

    if-nez v1, :cond_2

    .line 410
    :cond_1
    invoke-virtual {v2}, Landroid/app/Activity;->finish()V

    .line 413
    :cond_2
    iget-object v1, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mSelectedListId:[J

    if-eqz v1, :cond_6

    .line 414
    instance-of v1, v2, Lcom/samsung/musicplus/player/PlayerActivity;

    if-eqz v1, :cond_4

    .line 415
    new-instance v1, Lcom/samsung/musicplus/util/FileOperationTask$AddToThisPlaylistTask;

    iget-object v3, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mSelectedListId:[J

    int-to-long v4, p3

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/samsung/musicplus/util/FileOperationTask$AddToThisPlaylistTask;-><init>(Landroid/app/Activity;[JJ)V

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Void;

    invoke-virtual {v1, v3}, Lcom/samsung/musicplus/util/FileOperationTask$AddToThisPlaylistTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 432
    .end local v2    # "a":Landroid/app/Activity;
    :cond_3
    :goto_0
    return-void

    .line 417
    .restart local v2    # "a":Landroid/app/Activity;
    :cond_4
    sget-boolean v1, Lcom/samsung/musicplus/util/Features;->FLAG_SUPPORT_SPLIT_LIST_VIEW:Z

    if-eqz v1, :cond_5

    instance-of v1, v2, Lcom/samsung/musicplus/MusicMainActivity;

    if-eqz v1, :cond_5

    .line 418
    new-instance v1, Lcom/samsung/musicplus/util/FileOperationTask$AddToPlaylistTask;

    iget-object v3, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mSelectedListId:[J

    int-to-long v4, p3

    const/4 v6, 0x0

    const/4 v7, 0x1

    move-object v8, p2

    invoke-direct/range {v1 .. v8}, Lcom/samsung/musicplus/util/FileOperationTask$AddToPlaylistTask;-><init>(Landroid/app/Activity;[JJZZLjava/lang/String;)V

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Void;

    invoke-virtual {v1, v3}, Lcom/samsung/musicplus/util/FileOperationTask$AddToPlaylistTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 420
    :cond_5
    new-instance v1, Lcom/samsung/musicplus/util/FileOperationTask$AddToPlaylistTask;

    iget-object v3, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mSelectedListId:[J

    int-to-long v4, p3

    const/4 v6, 0x1

    const/4 v7, 0x1

    move-object v8, p2

    invoke-direct/range {v1 .. v8}, Lcom/samsung/musicplus/util/FileOperationTask$AddToPlaylistTask;-><init>(Landroid/app/Activity;[JJZZLjava/lang/String;)V

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Void;

    invoke-virtual {v1, v3}, Lcom/samsung/musicplus/util/FileOperationTask$AddToPlaylistTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 424
    :cond_6
    invoke-virtual {v2}, Landroid/app/Activity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 425
    .local v0, "i":Landroid/content/Intent;
    new-instance v0, Landroid/content/Intent;

    .end local v0    # "i":Landroid/content/Intent;
    const-class v1, Lcom/samsung/musicplus/contents/TrackActivity;

    invoke-direct {v0, v2, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 426
    .restart local v0    # "i":Landroid/content/Intent;
    const-string v1, "list_type"

    const v3, 0x20004

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 427
    const-string v1, "keyword"

    int-to-long v4, p3

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 428
    const-string v1, "track_title"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 429
    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private savePlayList(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "playlistId"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x1

    .line 625
    sget-object v4, Lcom/samsung/musicplus/dialog/EditTitleDialog;->CLASSNAME:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "savePlayList name = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " playlistId = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/musicplus/library/iLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 626
    invoke-direct {p0}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->getPlaylistUri()Landroid/net/Uri;

    move-result-object v2

    .line 627
    .local v2, "uri":Landroid/net/Uri;
    if-nez p3, :cond_0

    .line 628
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3, v7}, Landroid/content/ContentValues;-><init>(I)V

    .line 629
    .local v3, "values":Landroid/content/ContentValues;
    const-string v4, "name"

    invoke-virtual {v3, v4, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 631
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 650
    :goto_0
    return-void

    .line 632
    :catch_0
    move-exception v0

    .line 633
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    sget-object v4, Lcom/samsung/musicplus/dialog/EditTitleDialog;->CLASSNAME:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "savePlayList() IllegalArgumentException-- uri = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/musicplus/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 634
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 637
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    .end local v3    # "values":Landroid/content/ContentValues;
    :cond_0
    new-instance v3, Landroid/content/ContentValues;

    const/4 v4, 0x2

    invoke-direct {v3, v4}, Landroid/content/ContentValues;-><init>(I)V

    .line 638
    .restart local v3    # "values":Landroid/content/ContentValues;
    const-string v4, "name"

    invoke-virtual {v3, v4, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 639
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "/Playlists/"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 641
    .local v1, "path":Ljava/lang/String;
    const-string v4, "_data"

    invoke-virtual {v3, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 642
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "_id=?"

    new-array v6, v7, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object p3, v6, v7

    invoke-virtual {v4, v2, v3, v5, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0
.end method

.method private setKeyboardBroadcastReceiver()V
    .locals 4

    .prologue
    .line 538
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mKeyboardIntentFilter:Landroid/content/IntentFilter;

    if-nez v2, :cond_0

    .line 539
    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    iput-object v2, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mKeyboardIntentFilter:Landroid/content/IntentFilter;

    .line 540
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mKeyboardIntentFilter:Landroid/content/IntentFilter;

    const-string v3, "ResponseAxT9Info"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 543
    :cond_0
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mKeyboardReceiver:Landroid/content/BroadcastReceiver;

    if-nez v2, :cond_1

    .line 544
    new-instance v2, Lcom/samsung/musicplus/dialog/EditTitleDialog$12;

    invoke-direct {v2, p0}, Lcom/samsung/musicplus/dialog/EditTitleDialog$12;-><init>(Lcom/samsung/musicplus/dialog/EditTitleDialog;)V

    iput-object v2, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mKeyboardReceiver:Landroid/content/BroadcastReceiver;

    .line 552
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 553
    .local v0, "a":Landroid/app/Activity;
    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 554
    .local v1, "context":Landroid/content/Context;
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mKeyboardReceiver:Landroid/content/BroadcastReceiver;

    iget-object v3, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mKeyboardIntentFilter:Landroid/content/IntentFilter;

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 555
    return-void
.end method

.method private showProgressDialog()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 735
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mLoadingHandler:Landroid/os/Handler;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 736
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mLoadingProgress:Landroid/app/ProgressDialog;

    .line 737
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mActivity:Landroid/app/Activity;

    .line 738
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mLoadingProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v4}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 739
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mLoadingProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v4}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 740
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mLoadingProgress:Landroid/app/ProgressDialog;

    const v1, 0x7f1000a1

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 741
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mLoadingProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 742
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mLoadingHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 743
    return-void
.end method


# virtual methods
.method protected getSelectedIds()[J
    .locals 1

    .prologue
    .line 729
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mSelectedListId:[J

    return-object v0
.end method

.method protected initializeView(Landroid/os/Bundle;Landroid/view/View;)V
    .locals 0
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;
    .param p2, "parent"    # Landroid/view/View;

    .prologue
    .line 451
    return-void
.end method

.method protected onCancelled()V
    .locals 0

    .prologue
    .line 439
    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "ShowToast"
        }
    .end annotation

    .prologue
    const v7, 0x7f10003a

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 188
    if-eqz p1, :cond_0

    .line 189
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->onLoadSavedInstanceState(Landroid/os/Bundle;)V

    .line 191
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 192
    .local v0, "a":Landroid/app/Activity;
    const-string v2, "layout_inflater"

    invoke-virtual {v0, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 194
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v2, 0x7f040028

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mView:Landroid/view/View;

    .line 195
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mView:Landroid/view/View;

    instance-of v2, v2, Lcom/samsung/musicplus/widget/WFILinearLayout;

    if-eqz v2, :cond_1

    .line 196
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mView:Landroid/view/View;

    check-cast v2, Lcom/samsung/musicplus/widget/WFILinearLayout;

    new-instance v3, Lcom/samsung/musicplus/dialog/EditTitleDialog$2;

    invoke-direct {v3, p0}, Lcom/samsung/musicplus/dialog/EditTitleDialog$2;-><init>(Lcom/samsung/musicplus/dialog/EditTitleDialog;)V

    invoke-virtual {v2, v3}, Lcom/samsung/musicplus/widget/WFILinearLayout;->setOnWindowFocusChangeListener(Lcom/samsung/musicplus/widget/WFILinearLayout$OnWindowFocusChangeListener;)V

    .line 208
    :cond_1
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mView:Landroid/view/View;

    const v3, 0x7f0d00a2

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    iput-object v2, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mEditText:Landroid/widget/EditText;

    .line 210
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mView:Landroid/view/View;

    const v3, 0x7f0d00a3

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mPlaylistListViewLayout:Landroid/widget/LinearLayout;

    .line 211
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mView:Landroid/view/View;

    const v3, 0x7f0d00a4

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ListView;

    iput-object v2, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mPlayListView:Landroid/widget/ListView;

    .line 213
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mView:Landroid/view/View;

    invoke-virtual {p0, p1, v2}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->initializeView(Landroid/os/Bundle;Landroid/view/View;)V

    .line 214
    invoke-direct {p0}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->fillUpPlaylistName()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mDefaultName:Ljava/lang/String;

    .line 216
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const-string v3, ""

    invoke-static {v2, v3, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mToast:Landroid/widget/Toast;

    .line 218
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v2, v6}, Landroid/widget/EditText;->setSelectAllOnFocus(Z)V

    .line 219
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->requestFocus()Z

    .line 226
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mEditText:Landroid/widget/EditText;

    const-string v3, "inputType=PredictionOff;inputType=filename"

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setPrivateImeOptions(Ljava/lang/String;)V

    .line 227
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mEditText:Landroid/widget/EditText;

    new-instance v3, Lcom/samsung/musicplus/dialog/EditTitleDialog$3;

    invoke-direct {v3, p0}, Lcom/samsung/musicplus/dialog/EditTitleDialog$3;-><init>(Lcom/samsung/musicplus/dialog/EditTitleDialog;)V

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 258
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mEditText:Landroid/widget/EditText;

    const/4 v3, 0x2

    new-array v3, v3, [Landroid/text/InputFilter;

    iget-object v4, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->filterSpecialChar:Landroid/text/InputFilter;

    aput-object v4, v3, v5

    new-instance v4, Lcom/samsung/musicplus/dialog/EditTitleDialog$4;

    const/16 v5, 0x32

    invoke-direct {v4, p0, v5}, Lcom/samsung/musicplus/dialog/EditTitleDialog$4;-><init>(Lcom/samsung/musicplus/dialog/EditTitleDialog;I)V

    aput-object v4, v3, v6

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setFilters([Landroid/text/InputFilter;)V

    .line 278
    iget v2, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mListMode:I

    const/16 v3, 0x10

    if-ne v2, v3, :cond_2

    .line 279
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v3, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mView:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f10000f

    invoke-virtual {p0, v3}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    new-instance v3, Lcom/samsung/musicplus/dialog/EditTitleDialog$5;

    invoke-direct {v3, p0}, Lcom/samsung/musicplus/dialog/EditTitleDialog$5;-><init>(Lcom/samsung/musicplus/dialog/EditTitleDialog;)V

    invoke-virtual {v2, v7, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mDialog:Landroid/app/AlertDialog;

    .line 345
    :goto_0
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mDialog:Landroid/app/AlertDialog;

    new-instance v3, Lcom/samsung/musicplus/dialog/EditTitleDialog$8;

    invoke-direct {v3, p0}, Lcom/samsung/musicplus/dialog/EditTitleDialog$8;-><init>(Lcom/samsung/musicplus/dialog/EditTitleDialog;)V

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 357
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mEditText:Landroid/widget/EditText;

    new-instance v3, Lcom/samsung/musicplus/dialog/EditTitleDialog$9;

    invoke-direct {v3, p0}, Lcom/samsung/musicplus/dialog/EditTitleDialog$9;-><init>(Lcom/samsung/musicplus/dialog/EditTitleDialog;)V

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 373
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mEditText:Landroid/widget/EditText;

    new-instance v3, Lcom/samsung/musicplus/dialog/EditTitleDialog$10;

    invoke-direct {v3, p0}, Lcom/samsung/musicplus/dialog/EditTitleDialog$10;-><init>(Lcom/samsung/musicplus/dialog/EditTitleDialog;)V

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 384
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mDialog:Landroid/app/AlertDialog;

    return-object v2

    .line 288
    :cond_2
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iget-object v3, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mView:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    iget v3, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mListMode:I

    invoke-direct {p0, v3}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->getDialogTitleResource(I)I

    move-result v3

    invoke-virtual {p0, v3}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    iget v3, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mListMode:I

    invoke-direct {p0, v3}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->getDialogOKResource(I)I

    move-result v3

    new-instance v4, Lcom/samsung/musicplus/dialog/EditTitleDialog$7;

    invoke-direct {v4, p0}, Lcom/samsung/musicplus/dialog/EditTitleDialog$7;-><init>(Lcom/samsung/musicplus/dialog/EditTitleDialog;)V

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    new-instance v3, Lcom/samsung/musicplus/dialog/EditTitleDialog$6;

    invoke-direct {v3, p0}, Lcom/samsung/musicplus/dialog/EditTitleDialog$6;-><init>(Lcom/samsung/musicplus/dialog/EditTitleDialog;)V

    invoke-virtual {v2, v7, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mDialog:Landroid/app/AlertDialog;

    goto :goto_0
.end method

.method protected onLoadSavedInstanceState(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 176
    const-string v0, "listType"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mListType:I

    .line 177
    const-string v0, "listMode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mListMode:I

    .line 178
    const-string v0, "keyWord"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mKeyWord:Ljava/lang/String;

    .line 179
    const-string v0, "image"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mPlayListAlbumArt:Landroid/graphics/Bitmap;

    .line 180
    const-string v0, "showKeyboard"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mIsKeyboardVisible:Z

    .line 181
    const-string v0, "isMaxName"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mIsMaxName:Z

    .line 182
    const-string v0, "selectedId"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mSelectedListId:[J

    .line 183
    return-void
.end method

.method public onPause()V
    .locals 6

    .prologue
    .line 524
    sget-object v2, Lcom/samsung/musicplus/dialog/EditTitleDialog;->CLASSNAME:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onPause() isKeyboardVisible = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mIsKeyboardVisible:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 525
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mReceiveTime:J

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x96

    cmp-long v2, v2, v4

    if-gez v2, :cond_0

    .line 526
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mIsKeyboardVisible:Z

    .line 528
    :cond_0
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mKeyboardReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v2, :cond_1

    .line 529
    invoke-virtual {p0}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 530
    .local v0, "a":Landroid/app/Activity;
    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 532
    .local v1, "context":Landroid/content/Context;
    iget-object v2, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mKeyboardReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 534
    .end local v0    # "a":Landroid/app/Activity;
    .end local v1    # "context":Landroid/content/Context;
    :cond_1
    invoke-super {p0}, Landroid/app/DialogFragment;->onPause()V

    .line 535
    return-void
.end method

.method public onResume()V
    .locals 4

    .prologue
    .line 502
    sget-object v0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onResume() isKeyboardVisible = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mIsKeyboardVisible:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 504
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mEditText:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->isCursorVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 505
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->clearFocus()V

    .line 506
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    .line 507
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mEditText:Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelectAllOnFocus(Z)V

    .line 508
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mEditText:Landroid/widget/EditText;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setCursorVisible(Z)V

    .line 510
    :cond_0
    invoke-direct {p0}, Lcom/samsung/musicplus/dialog/EditTitleDialog;->setKeyboardBroadcastReceiver()V

    .line 511
    iget-boolean v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mIsKeyboardVisible:Z

    if-eqz v0, :cond_1

    .line 512
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mView:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mShowSIP:Ljava/lang/Runnable;

    const-wide/16 v2, 0xc8

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 519
    :cond_1
    invoke-super {p0}, Landroid/app/DialogFragment;->onResume()V

    .line 520
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 160
    const-string v0, "listType"

    iget v1, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mListType:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 161
    const-string v0, "listMode"

    iget v1, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mListMode:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 162
    const-string v0, "keyWord"

    iget-object v1, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mKeyWord:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    const-string v0, "image"

    iget-object v1, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mPlayListAlbumArt:Landroid/graphics/Bitmap;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 164
    const-string v0, "showKeyboard"

    iget-boolean v1, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mIsKeyboardVisible:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 165
    const-string v0, "isMaxName"

    iget-boolean v1, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mIsMaxName:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 166
    const-string v0, "selectedId"

    iget-object v1, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mSelectedListId:[J

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putLongArray(Ljava/lang/String;[J)V

    .line 167
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 168
    return-void
.end method

.method protected setVisibleEditBox(Z)V
    .locals 2
    .param p1, "visible"    # Z

    .prologue
    .line 721
    if-eqz p1, :cond_0

    .line 722
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mEditText:Landroid/widget/EditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setVisibility(I)V

    .line 726
    :goto_0
    return-void

    .line 724
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/dialog/EditTitleDialog;->mEditText:Landroid/widget/EditText;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setVisibility(I)V

    goto :goto_0
.end method
