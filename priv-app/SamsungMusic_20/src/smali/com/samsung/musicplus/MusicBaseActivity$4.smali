.class Lcom/samsung/musicplus/MusicBaseActivity$4;
.super Ljava/lang/Object;
.source "MusicBaseActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/MusicBaseActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/MusicBaseActivity;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/MusicBaseActivity;)V
    .locals 0

    .prologue
    .line 553
    iput-object p1, p0, Lcom/samsung/musicplus/MusicBaseActivity$4;->this$0:Lcom/samsung/musicplus/MusicBaseActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 559
    const-string v0, "MusicUi"

    const-string v1, "AS: Back key pressed and AVPlayer action will be canceled"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 560
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->changeToDefaultPlayer()V

    .line 561
    return-void
.end method
