.class Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqControlBarChangeListener;
.super Ljava/lang/Object;
.source "SoundAliveV2Setting.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/settings/SoundAliveV2Setting;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UserEqControlBarChangeListener"
.end annotation


# instance fields
.field private final mHideBubble:Ljava/lang/Runnable;

.field final synthetic this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;


# direct methods
.method private constructor <init>(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)V
    .locals 1

    .prologue
    .line 1321
    iput-object p1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqControlBarChangeListener;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1405
    new-instance v0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqControlBarChangeListener$1;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqControlBarChangeListener$1;-><init>(Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqControlBarChangeListener;)V

    iput-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqControlBarChangeListener;->mHideBubble:Ljava/lang/Runnable;

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;Lcom/samsung/musicplus/settings/SoundAliveV2Setting$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/musicplus/settings/SoundAliveV2Setting;
    .param p2, "x1"    # Lcom/samsung/musicplus/settings/SoundAliveV2Setting$1;

    .prologue
    .line 1321
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqControlBarChangeListener;-><init>(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 9
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromUser"    # Z

    .prologue
    const/4 v4, 0x7

    const-wide/high16 v6, 0x3ff8000000000000L    # 1.5

    const/4 v8, 0x2

    .line 1325
    iget-object v2, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqControlBarChangeListener;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mBubble:Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;
    invoke-static {v2}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$2400(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;

    move-result-object v2

    if-nez v2, :cond_1

    .line 1362
    :cond_0
    :goto_0
    return-void

    .line 1328
    :cond_1
    iget-object v2, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqControlBarChangeListener;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mCurrentTab:I
    invoke-static {v2}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$2500(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)I

    move-result v2

    if-ne v2, v8, :cond_2

    .line 1329
    iget-object v2, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqControlBarChangeListener;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mBubble:Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;
    invoke-static {v2}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$2400(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;

    move-result-object v2

    add-int/lit8 v3, p2, -0xa

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;->setBubbleText(Ljava/lang/CharSequence;)V

    .line 1331
    :cond_2
    invoke-virtual {p1}, Landroid/widget/SeekBar;->getThumbCentralX()I

    move-result v0

    .line 1332
    .local v0, "bubbleX":I
    invoke-virtual {p1}, Landroid/widget/SeekBar;->getThumbCentralY()I

    move-result v1

    .line 1334
    .local v1, "bubbleY":I
    iget-object v2, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqControlBarChangeListener;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # invokes: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->isPortrait()Z
    invoke-static {v2}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$2600(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Z

    move-result v2

    if-nez v2, :cond_3

    add-int/lit8 v2, p2, -0xa

    if-gt v2, v4, :cond_4

    :cond_3
    add-int/lit8 v2, p2, -0xa

    if-le v2, v4, :cond_7

    .line 1335
    :cond_4
    int-to-double v2, v1

    invoke-virtual {p1}, Landroid/widget/SeekBar;->getThumbHeight()I

    move-result v4

    int-to-double v4, v4

    mul-double/2addr v4, v6

    add-double/2addr v2, v4

    double-to-int v1, v2

    .line 1340
    :goto_1
    iget-object v2, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqControlBarChangeListener;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mBubble:Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;
    invoke-static {v2}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$2400(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;

    move-result-object v3

    monitor-enter v3

    .line 1341
    :try_start_0
    iget-object v2, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqControlBarChangeListener;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mBubble:Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;
    invoke-static {v2}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$2400(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;

    move-result-object v2

    invoke-virtual {v2, p1, v0, v1}, Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;->setBubblePosition(Landroid/view/View;II)V

    .line 1342
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1343
    iget-object v2, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqControlBarChangeListener;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mIsReset:Z
    invoke-static {v2}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$2700(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Z

    move-result v2

    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqControlBarChangeListener;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mCurrentTab:I
    invoke-static {v2}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$2500(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)I

    move-result v2

    if-ne v2, v8, :cond_5

    iget-object v2, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqControlBarChangeListener;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->isCancel:Z
    invoke-static {v2}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$2800(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 1344
    iget-object v2, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqControlBarChangeListener;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mBubble:Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;
    invoke-static {v2}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$2400(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;->showBubble()V

    .line 1345
    iget-object v2, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqControlBarChangeListener;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mBubble:Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;
    invoke-static {v2}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$2400(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;->invalidate()V

    .line 1347
    :cond_5
    iget-object v2, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqControlBarChangeListener;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    const/4 v3, 0x0

    # setter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mIsReset:Z
    invoke-static {v2, v3}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$2702(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;Z)Z

    .line 1349
    iget-object v2, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqControlBarChangeListener;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mIsBubbleRunable:Z
    invoke-static {v2}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$2900(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 1350
    iget-object v2, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqControlBarChangeListener;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mBubble:Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;
    invoke-static {v2}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$2400(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqControlBarChangeListener;->mHideBubble:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 1352
    :cond_6
    iget-object v2, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqControlBarChangeListener;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    iget-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqControlBarChangeListener;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mBubble:Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;
    invoke-static {v3}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$2400(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqControlBarChangeListener;->mHideBubble:Ljava/lang/Runnable;

    const-wide/16 v6, 0x3e8

    invoke-virtual {v3, v4, v6, v7}, Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;->postDelayed(Ljava/lang/Runnable;J)Z

    move-result v3

    # setter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mIsBubbleRunable:Z
    invoke-static {v2, v3}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$2902(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;Z)Z

    .line 1354
    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$3000()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Eq control changed fromTouch : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1355
    if-eqz p3, :cond_0

    iget-object v2, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqControlBarChangeListener;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mCurrentTab:I
    invoke-static {v2}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$2500(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)I

    move-result v2

    if-ne v2, v8, :cond_0

    .line 1359
    iget-object v2, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqControlBarChangeListener;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # invokes: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->setAdvancedBandLevel()V
    invoke-static {v2}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$3100(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)V

    .line 1360
    iget-object v2, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqControlBarChangeListener;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    invoke-virtual {v2}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/musicplus/util/SoundAliveHeadsetUtil;->notifySoundAliveChanged(Landroid/content/Context;)V

    goto/16 :goto_0

    .line 1337
    :cond_7
    int-to-double v2, v1

    invoke-virtual {p1}, Landroid/widget/SeekBar;->getThumbHeight()I

    move-result v4

    int-to-double v4, v4

    mul-double/2addr v4, v6

    sub-double/2addr v2, v4

    double-to-int v1, v2

    goto/16 :goto_1

    .line 1342
    :catchall_0
    move-exception v2

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v2
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 10
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    const/4 v4, 0x7

    const-wide/high16 v8, 0x3ff8000000000000L    # 1.5

    .line 1366
    invoke-virtual {p1}, Landroid/widget/SeekBar;->getThumbCentralX()I

    move-result v0

    .line 1367
    .local v0, "bubbleX":I
    invoke-virtual {p1}, Landroid/widget/SeekBar;->getThumbCentralY()I

    move-result v1

    .line 1368
    .local v1, "bubbleY":I
    invoke-virtual {p1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v2

    .line 1370
    .local v2, "progress":I
    iget-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqControlBarChangeListener;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # invokes: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->isPortrait()Z
    invoke-static {v3}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$2600(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Z

    move-result v3

    if-nez v3, :cond_0

    add-int/lit8 v3, v2, -0xa

    if-gt v3, v4, :cond_1

    :cond_0
    add-int/lit8 v3, v2, -0xa

    if-le v3, v4, :cond_3

    .line 1371
    :cond_1
    int-to-double v4, v1

    invoke-virtual {p1}, Landroid/widget/SeekBar;->getThumbHeight()I

    move-result v3

    int-to-double v6, v3

    mul-double/2addr v6, v8

    add-double/2addr v4, v6

    double-to-int v1, v4

    .line 1376
    :goto_0
    iget-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqControlBarChangeListener;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mCurrentTab:I
    invoke-static {v3}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$2500(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_2

    .line 1377
    iget-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqControlBarChangeListener;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mBubble:Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;
    invoke-static {v3}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$2400(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;

    move-result-object v3

    add-int/lit8 v4, v2, -0xa

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;->setBubbleText(Ljava/lang/CharSequence;)V

    .line 1379
    :cond_2
    iget-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqControlBarChangeListener;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mBubble:Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;
    invoke-static {v3}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$2400(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;

    move-result-object v3

    invoke-virtual {v3, p1, v0, v1}, Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;->setBubblePosition(Landroid/view/View;II)V

    .line 1380
    iget-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqControlBarChangeListener;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mBubble:Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;
    invoke-static {v3}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$2400(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;->showBubble()V

    .line 1381
    iget-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqControlBarChangeListener;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mBubble:Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;
    invoke-static {v3}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$2400(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;->invalidate()V

    .line 1382
    return-void

    .line 1373
    :cond_3
    int-to-double v4, v1

    invoke-virtual {p1}, Landroid/widget/SeekBar;->getThumbHeight()I

    move-result v3

    int-to-double v6, v3

    mul-double/2addr v6, v8

    sub-double/2addr v4, v6

    double-to-int v1, v4

    goto :goto_0
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 4
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    const/16 v2, 0xc

    const/4 v3, 0x0

    .line 1386
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqControlBarChangeListener;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mBubble:Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;
    invoke-static {v0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$2400(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;->hideBubble()V

    .line 1387
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqControlBarChangeListener;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mBubble:Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;
    invoke-static {v0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$2400(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;->invalidate()V

    .line 1389
    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mLastControlledTab:Z
    invoke-static {}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$3200()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqControlBarChangeListener;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSoundAlivePresetGridview:Landroid/widget/GridView;
    invoke-static {v0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$500(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Landroid/widget/GridView;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 1390
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqControlBarChangeListener;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    iget-object v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqControlBarChangeListener;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSoundAlivePresetGridview:Landroid/widget/GridView;
    invoke-static {v1}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$500(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Landroid/widget/GridView;

    move-result-object v1

    # invokes: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->clearAllsoundAlivePresetCell(Landroid/widget/GridView;)V
    invoke-static {v0, v1}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$600(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;Landroid/widget/GridView;)V

    .line 1391
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqControlBarChangeListener;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;
    invoke-static {v0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$000(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Lcom/samsung/musicplus/library/audio/SecAudioManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->isHDMIConnect()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1392
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqControlBarChangeListener;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    iget-object v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqControlBarChangeListener;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSoundAlivePresetGridview:Landroid/widget/GridView;
    invoke-static {v1}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$500(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Landroid/widget/GridView;

    move-result-object v1

    # invokes: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->selectSoundAlivePresetCell(Landroid/widget/GridView;I)V
    invoke-static {v0, v1, v2}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$700(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;Landroid/widget/GridView;I)V

    .line 1394
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqControlBarChangeListener;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # invokes: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->setSquareEffect(I)V
    invoke-static {v0, v2}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$800(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;I)V

    .line 1396
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqControlBarChangeListener;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mCheckBoxAuto:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$900(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1397
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqControlBarChangeListener;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # invokes: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->setCheckedAuto(Z)V
    invoke-static {v0, v3}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$1000(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;Z)V

    .line 1398
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqControlBarChangeListener;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    iget-object v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqControlBarChangeListener;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    invoke-virtual {v1}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f1000e5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1403
    :cond_1
    return-void
.end method
