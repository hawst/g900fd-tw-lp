.class Lcom/samsung/musicplus/settings/SoundAliveSetting$SeekBubbleUpdateListener;
.super Ljava/lang/Object;
.source "SoundAliveSetting.java"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/settings/SoundAliveSetting;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SeekBubbleUpdateListener"
.end annotation


# instance fields
.field private mBubble:Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;

.field private final mHideBubble:Ljava/lang/Runnable;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;)V
    .locals 1
    .param p1, "bubble"    # Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;

    .prologue
    .line 987
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 980
    new-instance v0, Lcom/samsung/musicplus/settings/SoundAliveSetting$SeekBubbleUpdateListener$1;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/settings/SoundAliveSetting$SeekBubbleUpdateListener$1;-><init>(Lcom/samsung/musicplus/settings/SoundAliveSetting$SeekBubbleUpdateListener;)V

    iput-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$SeekBubbleUpdateListener;->mHideBubble:Ljava/lang/Runnable;

    .line 988
    iput-object p1, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$SeekBubbleUpdateListener;->mBubble:Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;

    .line 989
    return-void
.end method

.method static synthetic access$1400(Lcom/samsung/musicplus/settings/SoundAliveSetting$SeekBubbleUpdateListener;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/SoundAliveSetting$SeekBubbleUpdateListener;

    .prologue
    .line 976
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/SoundAliveSetting$SeekBubbleUpdateListener;->hideBubble()V

    return-void
.end method

.method private hideBubble()V
    .locals 1

    .prologue
    .line 1010
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$SeekBubbleUpdateListener;->mBubble:Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;

    invoke-virtual {v0}, Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;->hideBubble()V

    .line 1011
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$SeekBubbleUpdateListener;->mBubble:Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;

    invoke-virtual {v0}, Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;->invalidate()V

    .line 1012
    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 4
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromUser"    # Z

    .prologue
    .line 993
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/settings/SoundAliveSetting$SeekBubbleUpdateListener;->updateBubble(Landroid/widget/SeekBar;)V

    .line 995
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$SeekBubbleUpdateListener;->mBubble:Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;

    iget-object v1, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$SeekBubbleUpdateListener;->mHideBubble:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 996
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$SeekBubbleUpdateListener;->mBubble:Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;

    iget-object v1, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$SeekBubbleUpdateListener;->mHideBubble:Ljava/lang/Runnable;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 997
    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 1001
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/settings/SoundAliveSetting$SeekBubbleUpdateListener;->updateBubble(Landroid/widget/SeekBar;)V

    .line 1002
    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 1006
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/SoundAliveSetting$SeekBubbleUpdateListener;->hideBubble()V

    .line 1007
    return-void
.end method

.method protected updateBubble(Landroid/widget/SeekBar;)V
    .locals 8
    .param p1, "controlBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 1015
    invoke-virtual {p1}, Landroid/widget/SeekBar;->getThumbCentralX()I

    move-result v0

    .line 1016
    .local v0, "bubbleX":I
    invoke-virtual {p1}, Landroid/widget/SeekBar;->getThumbCentralY()I

    move-result v3

    invoke-virtual {p1}, Landroid/widget/SeekBar;->getThumbHeight()I

    move-result v4

    int-to-double v4, v4

    const-wide/high16 v6, 0x3ff8000000000000L    # 1.5

    mul-double/2addr v4, v6

    double-to-int v4, v4

    add-int v1, v3, v4

    .line 1017
    .local v1, "bubbleY":I
    iget-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$SeekBubbleUpdateListener;->mBubble:Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;

    invoke-virtual {v3, p1, v0, v1}, Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;->setBubblePosition(Landroid/view/View;II)V

    .line 1020
    instance-of v3, p1, Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;

    if-eqz v3, :cond_0

    .line 1021
    check-cast p1, Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;

    .end local p1    # "controlBar":Landroid/widget/SeekBar;
    invoke-virtual {p1}, Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;->getValue()I

    move-result v2

    .line 1025
    .local v2, "progress":I
    :goto_0
    iget-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$SeekBubbleUpdateListener;->mBubble:Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;->setBubbleText(Ljava/lang/CharSequence;)V

    .line 1027
    iget-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$SeekBubbleUpdateListener;->mBubble:Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;

    invoke-virtual {v3}, Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;->showBubble()V

    .line 1028
    iget-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$SeekBubbleUpdateListener;->mBubble:Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;

    invoke-virtual {v3}, Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;->invalidate()V

    .line 1029
    return-void

    .line 1023
    .end local v2    # "progress":I
    .restart local p1    # "controlBar":Landroid/widget/SeekBar;
    :cond_0
    invoke-virtual {p1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v2

    .restart local v2    # "progress":I
    goto :goto_0
.end method
