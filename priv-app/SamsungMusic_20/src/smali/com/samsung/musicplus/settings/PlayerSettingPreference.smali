.class public interface abstract Lcom/samsung/musicplus/settings/PlayerSettingPreference;
.super Ljava/lang/Object;
.source "PlayerSettingPreference.java"


# static fields
.field public static final AUTO_OFF_COMPLETED:Ljava/lang/String; = "completed"

.field public static final PREF_KEY_ADAPT_SOUND:Ljava/lang/String; = "adapt_sound"

.field public static final PREF_KEY_ALBUM:Ljava/lang/String; = "album"

.field public static final PREF_KEY_ALBUM_VIEW_TYPE:Ljava/lang/String; = "album_view_type"

.field public static final PREF_KEY_ALL_SHARE:Ljava/lang/String; = "music_all_share"

.field public static final PREF_KEY_ARTIST:Ljava/lang/String; = "artist"

.field public static final PREF_KEY_AUDIO_QUALITY:Ljava/lang/String; = "com.samsung.musicplus.store.settings.audio_quality"

.field public static final PREF_KEY_BIGPOND:Ljava/lang/String; = "music_bigpond"

.field public static final PREF_KEY_COMPOSER:Ljava/lang/String; = "composer"

.field public static final PREF_KEY_COMPOSER_VIEW_TYPE:Ljava/lang/String; = "composer_view_type"

.field public static final PREF_KEY_FAVOURITE_PLAYED:Ljava/lang/String; = "favourite_played"

.field public static final PREF_KEY_FOLDER:Ljava/lang/String; = "folder"

.field public static final PREF_KEY_GENRE:Ljava/lang/String; = "genre"

.field public static final PREF_KEY_GENRE_VIEW_TYPE:Ljava/lang/String; = "genre_view_type"

.field public static final PREF_KEY_K2HD:Ljava/lang/String; = "K2HD"

.field public static final PREF_KEY_LYRIC:Ljava/lang/String; = "lyric"

.field public static final PREF_KEY_MOST_PLAYED:Ljava/lang/String; = "most_played"

.field public static final PREF_KEY_MUSIC_AUTO_OFF:Ljava/lang/String; = "music_auto_off"

.field public static final PREF_KEY_MUSIC_AUTO_OFF_CUSTOM_HOUR:Ljava/lang/String; = "music_auto_off_custom_hour"

.field public static final PREF_KEY_MUSIC_AUTO_OFF_CUSTOM_MIN:Ljava/lang/String; = "music_auto_off_custom_min"

.field public static final PREF_KEY_MUSIC_MENU:Ljava/lang/String; = "music_menu"

.field public static final PREF_KEY_MUSIC_SQUARE:Ljava/lang/String; = "music_square"

.field public static final PREF_KEY_NEW_SOUND_ALIVE:Ljava/lang/String; = "new_sound_alive"

.field public static final PREF_KEY_NEW_SOUND_ALIVE_AUTO:Ljava/lang/String; = "sa_auto_check"

.field public static final PREF_KEY_NEW_SOUND_ALIVE_BAND_LEVEL:Ljava/lang/String; = "sa_band_level"

.field public static final PREF_KEY_NEW_SOUND_ALIVE_SQUARE_POSITION:Ljava/lang/String; = "sa_square_position"

.field public static final PREF_KEY_NEW_SOUND_ALIVE_STRENGTH:Ljava/lang/String; = "sa_use_strength"

.field public static final PREF_KEY_NEW_SOUND_ALIVE_USE_PRESET:Ljava/lang/String; = "sa_use_preset_effect"

.field public static final PREF_KEY_PERSONAL_MODE_HELP:Ljava/lang/String; = "personal_mode_help"

.field public static final PREF_KEY_PLAYLIST:Ljava/lang/String; = "playlist"

.field public static final PREF_KEY_PLAYLIST_MENU_LIST:Ljava/lang/String; = "playlist_menu_list"

.field public static final PREF_KEY_PLAYLIST_MENU_LIST_ORDER:Ljava/lang/String; = "playlist_menu_list_order"

.field public static final PREF_KEY_PLAYLIST_VIEW_TYPE:Ljava/lang/String; = "playlist_view_type"

.field public static final PREF_KEY_PLAY_SPEED:Ljava/lang/String; = "play_speed"

.field public static final PREF_KEY_RECENTLY_ADDED:Ljava/lang/String; = "recently_added"

.field public static final PREF_KEY_RECENTLY_PLAYED:Ljava/lang/String; = "recently_played"

.field public static final PREF_KEY_RECORDINGS:Ljava/lang/String; = "recordings"

.field public static final PREF_KEY_REPEAT:Ljava/lang/String; = "repeat"

.field public static final PREF_KEY_SAVED_SQUARE:Ljava/lang/String; = "savedSquare"

.field public static final PREF_KEY_SHUFFLE:Ljava/lang/String; = "shuffle"

.field public static final PREF_KEY_SLINK_REGISTER:Ljava/lang/String; = "slink_register"

.field public static final PREF_KEY_SLINK_REGISTER_CATEGORY:Ljava/lang/String; = "slink_register_category"

.field public static final PREF_KEY_SMART_VOLUME:Ljava/lang/String; = "smart_volume"

.field public static final PREF_KEY_SOUND_ALIVE:Ljava/lang/String; = "sound_alive"

.field public static final PREF_KEY_SOUND_ALIVE_USER_EQ:Ljava/lang/String; = "user_eq"

.field public static final PREF_KEY_SOUND_ALIVE_USER_EXT:Ljava/lang/String; = "user_ext"

.field public static final PREF_KEY_TAB_MENU_LIST:Ljava/lang/String; = "tab_menu_list"

.field public static final PREF_KEY_TAB_MENU_LIST_ORDER:Ljava/lang/String; = "tab_menu_list_order"

.field public static final PREF_KEY_VOICE_CONTROL:Ljava/lang/String; = "voice_control"

.field public static final PREF_KEY_WFD_ALL_TOGETHER:Ljava/lang/String; = "wfd_all_together"

.field public static final PREF_KEY_WFD_GROUP_PLAY:Ljava/lang/String; = "wfd_group_play"

.field public static final PREF_KEY_WFD_LIMITED_PLAY:Ljava/lang/String; = "wfd_limited_play"

.field public static final PREF_KEY_WFD_POWER_SAVING:Ljava/lang/String; = "wfd_power_saving"

.field public static final PREF_KEY_WFD_SIDE_SYNC:Ljava/lang/String; = "wfd_side_sync"

.field public static final PREF_KEY_WFD_WIFI_DIRECT:Ljava/lang/String; = "wfd_wifi_direct"

.field public static final PREF_KEY_WFD_WIFI_HOTSPOT:Ljava/lang/String; = "wfd_wifi_hotspot"

.field public static final PREF_KEY_YEAR:Ljava/lang/String; = "year"

.field public static final PREF_MUSIC_PLAYER:Ljava/lang/String; = "music_player_pref"

.field public static final PREF_MUSIC_SERVICE:Ljava/lang/String; = "music_service_pref"
