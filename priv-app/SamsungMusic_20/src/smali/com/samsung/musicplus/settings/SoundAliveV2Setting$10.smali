.class Lcom/samsung/musicplus/settings/SoundAliveV2Setting$10;
.super Ljava/lang/Object;
.source "SoundAliveV2Setting.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->initSoundAliveGridView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)V
    .locals 0

    .prologue
    .line 696
    iput-object p1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$10;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 698
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    if-ltz p3, :cond_1

    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$10;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;
    invoke-static {v0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$000(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Lcom/samsung/musicplus/library/audio/SecAudioManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->isHDMIConnect()Z

    move-result v0

    if-nez v0, :cond_1

    .line 699
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$10;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    const/4 v1, 0x1

    # setter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSquareCellSelected:Z
    invoke-static {v0, v1}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$402(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;Z)Z

    .line 700
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$10;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    iget-object v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$10;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSoundAlivePresetGridview:Landroid/widget/GridView;
    invoke-static {v1}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$500(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Landroid/widget/GridView;

    move-result-object v1

    # invokes: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->clearAllsoundAlivePresetCell(Landroid/widget/GridView;)V
    invoke-static {v0, v1}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$600(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;Landroid/widget/GridView;)V

    .line 701
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$10;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    iget-object v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$10;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSoundAlivePresetGridview:Landroid/widget/GridView;
    invoke-static {v1}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$500(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Landroid/widget/GridView;

    move-result-object v1

    # invokes: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->selectSoundAlivePresetCell(Landroid/widget/GridView;I)V
    invoke-static {v0, v1, p3}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$700(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;Landroid/widget/GridView;I)V

    .line 702
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$10;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # invokes: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->setSquareEffect(I)V
    invoke-static {v0, p3}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$800(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;I)V

    .line 703
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$10;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mCheckBoxAuto:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$900(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Landroid/widget/CheckBox;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$10;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mCheckBoxAuto:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$900(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 704
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$10;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    const/4 v1, 0x0

    # invokes: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->setCheckedAuto(Z)V
    invoke-static {v0, v1}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$1000(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;Z)V

    .line 706
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$10;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    invoke-virtual {v0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/musicplus/util/SoundAliveHeadsetUtil;->notifySoundAliveChanged(Landroid/content/Context;)V

    .line 708
    :cond_1
    return-void
.end method
