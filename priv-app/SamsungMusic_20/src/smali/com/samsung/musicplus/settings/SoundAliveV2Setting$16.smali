.class Lcom/samsung/musicplus/settings/SoundAliveV2Setting$16;
.super Ljava/lang/Object;
.source "SoundAliveV2Setting.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->initLayout(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)V
    .locals 0

    .prologue
    .line 1190
    iput-object p1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$16;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 1193
    iget-object v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$16;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;
    invoke-static {v1}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$000(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Lcom/samsung/musicplus/library/audio/SecAudioManager;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$16;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;
    invoke-static {v1}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$000(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Lcom/samsung/musicplus/library/audio/SecAudioManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->isHDMIConnect()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1194
    iget-object v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$16;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    invoke-virtual {v1}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f100168

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1196
    .local v0, "msg":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$16;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mToast:Landroid/widget/Toast;
    invoke-static {v1}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$1100(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Landroid/widget/Toast;

    move-result-object v1

    if-nez v1, :cond_0

    .line 1197
    iget-object v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$16;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    iget-object v2, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$16;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    invoke-virtual {v2}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v0, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v2

    # setter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mToast:Landroid/widget/Toast;
    invoke-static {v1, v2}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$1102(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;Landroid/widget/Toast;)Landroid/widget/Toast;

    .line 1201
    :goto_0
    iget-object v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$16;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mToast:Landroid/widget/Toast;
    invoke-static {v1}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$1100(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 1202
    iget-object v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$16;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mCheckBoxAuto:Landroid/widget/CheckBox;
    invoke-static {v1}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$900(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Landroid/widget/CheckBox;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 1203
    invoke-static {v4}, Lcom/samsung/musicplus/util/ServiceUtils;->setNewSoundAliveAuto(Z)V

    .line 1217
    .end local v0    # "msg":Ljava/lang/String;
    :goto_1
    return-void

    .line 1199
    .restart local v0    # "msg":Ljava/lang/String;
    :cond_0
    iget-object v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$16;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mToast:Landroid/widget/Toast;
    invoke-static {v1}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$1100(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1206
    .end local v0    # "msg":Ljava/lang/String;
    :cond_1
    iget-object v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$16;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mCheckBoxAuto:Landroid/widget/CheckBox;
    invoke-static {v1}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$900(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Landroid/widget/CheckBox;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1207
    invoke-static {v2}, Lcom/samsung/musicplus/util/ServiceUtils;->setNewSoundAliveAuto(Z)V

    .line 1208
    iget-object v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$16;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # invokes: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->updateSquarePosition()V
    invoke-static {v1}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$2300(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)V

    .line 1209
    iget-object v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$16;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    invoke-virtual {v1}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/musicplus/util/SoundAliveHeadsetUtil;->notifySoundAliveChanged(Landroid/content/Context;)V

    .line 1210
    iget-object v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$16;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # setter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSquareCellSelected:Z
    invoke-static {v1, v2}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$402(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;Z)Z

    goto :goto_1

    .line 1212
    :cond_2
    invoke-static {v4}, Lcom/samsung/musicplus/util/ServiceUtils;->setNewSoundAliveAuto(Z)V

    .line 1213
    iget-object v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$16;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    iget-object v2, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$16;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    invoke-virtual {v2}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f1000e5

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    goto :goto_1
.end method
