.class Lcom/samsung/musicplus/settings/PlayerSettingFragment$2;
.super Ljava/lang/Object;
.source "PlayerSettingFragment.java"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/settings/PlayerSettingFragment;->initializeSettingMenu()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/settings/PlayerSettingFragment;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/settings/PlayerSettingFragment;)V
    .locals 0

    .prologue
    .line 245
    iput-object p1, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment$2;->this$0:Lcom/samsung/musicplus/settings/PlayerSettingFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 2
    .param p1, "preference"    # Landroid/preference/Preference;
    .param p2, "newValue"    # Ljava/lang/Object;

    .prologue
    .line 248
    iget-object v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment$2;->this$0:Lcom/samsung/musicplus/settings/PlayerSettingFragment;

    # getter for: Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mSlinkRegister:Landroid/preference/CheckBoxPreference;
    invoke-static {v0}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->access$200(Lcom/samsung/musicplus/settings/PlayerSettingFragment;)Landroid/preference/CheckBoxPreference;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment$2;->this$0:Lcom/samsung/musicplus/settings/PlayerSettingFragment;

    # getter for: Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->access$100(Lcom/samsung/musicplus/settings/PlayerSettingFragment;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/musicplus/provider/MusicContents$Audio$Devices;->isSingedIn(Landroid/content/Context;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 249
    const/4 v0, 0x0

    return v0
.end method
