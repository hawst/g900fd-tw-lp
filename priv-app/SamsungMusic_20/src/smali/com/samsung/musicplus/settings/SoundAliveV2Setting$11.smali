.class Lcom/samsung/musicplus/settings/SoundAliveV2Setting$11;
.super Ljava/lang/Object;
.source "SoundAliveV2Setting.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->initSoundAliveGridView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)V
    .locals 0

    .prologue
    .line 711
    iput-object p1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$11;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 7
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 715
    iget-object v5, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$11;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSoundAlivePresetGridview:Landroid/widget/GridView;
    invoke-static {v5}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$500(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Landroid/widget/GridView;

    move-result-object v5

    if-nez v5, :cond_0

    .line 811
    .end local p1    # "v":Landroid/view/View;
    :goto_0
    return v3

    .line 719
    .restart local p1    # "v":Landroid/view/View;
    :cond_0
    check-cast p1, Landroid/widget/GridView;

    .end local p1    # "v":Landroid/view/View;
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    float-to-int v5, v5

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    float-to-int v6, v6

    invoke-virtual {p1, v5, v6}, Landroid/widget/GridView;->pointToPosition(II)I

    move-result v2

    .line 722
    .local v2, "position":I
    iget-object v5, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$11;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;
    invoke-static {v5}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$000(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Lcom/samsung/musicplus/library/audio/SecAudioManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->isHDMIConnect()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 723
    const/16 v3, 0xc

    if-eq v2, v3, :cond_1

    .line 724
    iget-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$11;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    invoke-virtual {v3}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v5, 0x7f100168

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 726
    .local v1, "msg":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$11;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mToast:Landroid/widget/Toast;
    invoke-static {v3}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$1100(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Landroid/widget/Toast;

    move-result-object v3

    if-nez v3, :cond_2

    .line 727
    iget-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$11;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    iget-object v5, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$11;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    invoke-virtual {v5}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    # setter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mToast:Landroid/widget/Toast;
    invoke-static {v3, v5}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$1102(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;Landroid/widget/Toast;)Landroid/widget/Toast;

    .line 732
    :goto_1
    iget-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$11;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mToast:Landroid/widget/Toast;
    invoke-static {v3}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$1100(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .end local v1    # "msg":Ljava/lang/String;
    :cond_1
    move v3, v4

    .line 736
    goto :goto_0

    .line 730
    .restart local v1    # "msg":Ljava/lang/String;
    :cond_2
    iget-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$11;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mToast:Landroid/widget/Toast;
    invoke-static {v3}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$1100(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 739
    .end local v1    # "msg":Ljava/lang/String;
    :cond_3
    const/4 v0, 0x0

    .line 741
    .local v0, "consumed":Z
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v5

    packed-switch v5, :pswitch_data_0

    .line 807
    :goto_2
    if-eqz v0, :cond_4

    .line 808
    iget-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$11;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    invoke-virtual {v3}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/musicplus/util/SoundAliveHeadsetUtil;->notifySoundAliveChanged(Landroid/content/Context;)V

    :cond_4
    move v3, v4

    .line 811
    goto :goto_0

    .line 743
    :pswitch_0
    iget-object v5, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$11;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # setter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSquareCellSelected:Z
    invoke-static {v5, v3}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$402(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;Z)Z

    .line 744
    iget-object v5, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$11;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # setter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mIsDown:Z
    invoke-static {v5, v3}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$1202(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;Z)Z

    .line 745
    if-ltz v2, :cond_6

    .line 746
    iget-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$11;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    iget-object v5, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$11;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSoundAlivePresetGridview:Landroid/widget/GridView;
    invoke-static {v5}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$500(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Landroid/widget/GridView;

    move-result-object v5

    # invokes: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->clearAllsoundAlivePresetCell(Landroid/widget/GridView;)V
    invoke-static {v3, v5}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$600(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;Landroid/widget/GridView;)V

    .line 747
    iget-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$11;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    iget-object v5, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$11;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSoundAlivePresetGridview:Landroid/widget/GridView;
    invoke-static {v5}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$500(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Landroid/widget/GridView;

    move-result-object v5

    # invokes: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->selectSoundAlivePresetCell(Landroid/widget/GridView;I)V
    invoke-static {v3, v5, v2}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$700(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;Landroid/widget/GridView;I)V

    .line 748
    iget-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$11;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # invokes: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->setSquareEffect(I)V
    invoke-static {v3, v2}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$800(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;I)V

    .line 749
    iget-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$11;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mCheckBoxAuto:Landroid/widget/CheckBox;
    invoke-static {v3}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$900(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Landroid/widget/CheckBox;

    move-result-object v3

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$11;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mCheckBoxAuto:Landroid/widget/CheckBox;
    invoke-static {v3}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$900(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Landroid/widget/CheckBox;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 750
    iget-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$11;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # invokes: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->setCheckedAuto(Z)V
    invoke-static {v3, v4}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$1000(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;Z)V

    .line 751
    iget-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$11;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    iget-object v5, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$11;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    invoke-virtual {v5}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f1000e5

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 756
    :cond_5
    iget-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$11;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # setter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mLastSelectedCell:I
    invoke-static {v3, v2}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$1302(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;I)I

    .line 758
    :cond_6
    const/4 v0, 0x1

    .line 761
    :pswitch_1
    iget-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$11;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mIsDown:Z
    invoke-static {v3}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$1200(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 762
    iget-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$11;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mLastSelectedCell:I
    invoke-static {v3}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$1300(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)I

    move-result v3

    if-eq v3, v2, :cond_7

    .line 763
    iget-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$11;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    iget-object v5, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$11;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSoundAlivePresetGridview:Landroid/widget/GridView;
    invoke-static {v5}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$500(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Landroid/widget/GridView;

    move-result-object v5

    # invokes: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->clearAllsoundAlivePresetCell(Landroid/widget/GridView;)V
    invoke-static {v3, v5}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$600(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;Landroid/widget/GridView;)V

    .line 764
    iget-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$11;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    iget-object v5, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$11;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSoundAlivePresetGridview:Landroid/widget/GridView;
    invoke-static {v5}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$500(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Landroid/widget/GridView;

    move-result-object v5

    # invokes: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->selectSoundAlivePresetCell(Landroid/widget/GridView;I)V
    invoke-static {v3, v5, v2}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$700(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;Landroid/widget/GridView;I)V

    .line 765
    iget-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$11;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # invokes: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->setSquareEffect(I)V
    invoke-static {v3, v2}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$800(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;I)V

    .line 766
    if-ltz v2, :cond_7

    .line 767
    iget-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$11;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # setter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mLastSelectedCell:I
    invoke-static {v3, v2}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$1302(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;I)I

    .line 772
    :cond_7
    const/4 v0, 0x1

    .line 775
    :pswitch_2
    iget-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$11;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mIsDown:Z
    invoke-static {v3}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$1200(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 776
    iget-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$11;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # setter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mIsDown:Z
    invoke-static {v3, v4}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$1202(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;Z)Z

    .line 777
    iget-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$11;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    iget-object v5, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$11;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSoundAlivePresetGridview:Landroid/widget/GridView;
    invoke-static {v5}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$500(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Landroid/widget/GridView;

    move-result-object v5

    # invokes: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->clearAllsoundAlivePresetCell(Landroid/widget/GridView;)V
    invoke-static {v3, v5}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$600(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;Landroid/widget/GridView;)V

    .line 778
    if-ltz v2, :cond_a

    .line 779
    iget-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$11;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    iget-object v5, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$11;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSoundAlivePresetGridview:Landroid/widget/GridView;
    invoke-static {v5}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$500(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Landroid/widget/GridView;

    move-result-object v5

    # invokes: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->selectSoundAlivePresetCell(Landroid/widget/GridView;I)V
    invoke-static {v3, v5, v2}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$700(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;Landroid/widget/GridView;I)V

    .line 780
    iget-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$11;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # invokes: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->setSquareEffect(I)V
    invoke-static {v3, v2}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$800(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;I)V

    .line 792
    :cond_8
    :goto_3
    iget-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$11;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mIsDown:Z
    invoke-static {v3}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$1200(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Z

    move-result v3

    if-nez v3, :cond_9

    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->isPlaying()Z

    move-result v3

    if-nez v3, :cond_9

    .line 793
    iget-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$11;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    invoke-virtual {v3}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v5, 0x7f1000e4

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 796
    .restart local v1    # "msg":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$11;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mToast:Landroid/widget/Toast;
    invoke-static {v3}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$1100(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Landroid/widget/Toast;

    move-result-object v3

    if-nez v3, :cond_b

    .line 797
    iget-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$11;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    iget-object v5, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$11;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    invoke-static {v5, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    # setter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mToast:Landroid/widget/Toast;
    invoke-static {v3, v5}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$1102(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;Landroid/widget/Toast;)Landroid/widget/Toast;

    .line 802
    :goto_4
    iget-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$11;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mToast:Landroid/widget/Toast;
    invoke-static {v3}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$1100(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Toast;->show()V

    .line 804
    .end local v1    # "msg":Ljava/lang/String;
    :cond_9
    const/4 v0, 0x1

    goto/16 :goto_2

    .line 786
    :cond_a
    iget-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$11;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    iget-object v5, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$11;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSoundAlivePresetGridview:Landroid/widget/GridView;
    invoke-static {v5}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$500(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Landroid/widget/GridView;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$11;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mLastSelectedCell:I
    invoke-static {v6}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$1300(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)I

    move-result v6

    # invokes: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->selectSoundAlivePresetCell(Landroid/widget/GridView;I)V
    invoke-static {v3, v5, v6}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$700(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;Landroid/widget/GridView;I)V

    .line 788
    iget-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$11;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    iget-object v5, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$11;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mLastSelectedCell:I
    invoke-static {v5}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$1300(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)I

    move-result v5

    # invokes: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->setSquareEffect(I)V
    invoke-static {v3, v5}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$800(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;I)V

    goto :goto_3

    .line 800
    .restart local v1    # "msg":Ljava/lang/String;
    :cond_b
    iget-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$11;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mToast:Landroid/widget/Toast;
    invoke-static {v3}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$1100(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Landroid/widget/Toast;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    goto :goto_4

    .line 741
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
