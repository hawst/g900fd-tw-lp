.class Lcom/samsung/musicplus/settings/SoundAliveV2Setting$12;
.super Ljava/lang/Object;
.source "SoundAliveV2Setting.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->initSoundAliveGridView()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)V
    .locals 0

    .prologue
    .line 815
    iput-object p1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$12;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 5
    .param p1, "v"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 818
    const/4 v1, 0x0

    .line 820
    .local v1, "result":Z
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    const/16 v2, 0x17

    if-ne p2, v2, :cond_0

    .line 822
    const/4 v1, 0x0

    .line 823
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->isPlaying()Z

    move-result v2

    if-nez v2, :cond_0

    .line 824
    iget-object v2, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$12;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    invoke-virtual {v2}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f1000e4

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 827
    .local v0, "msg":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$12;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mToast:Landroid/widget/Toast;
    invoke-static {v2}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$1100(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Landroid/widget/Toast;

    move-result-object v2

    if-nez v2, :cond_1

    .line 828
    iget-object v2, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$12;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    iget-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$12;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    const/4 v4, 0x0

    invoke-static {v3, v0, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v3

    # setter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mToast:Landroid/widget/Toast;
    invoke-static {v2, v3}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$1102(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;Landroid/widget/Toast;)Landroid/widget/Toast;

    .line 833
    :goto_0
    iget-object v2, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$12;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mToast:Landroid/widget/Toast;
    invoke-static {v2}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$1100(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 836
    .end local v0    # "msg":Ljava/lang/String;
    :cond_0
    return v1

    .line 831
    .restart local v0    # "msg":Ljava/lang/String;
    :cond_1
    iget-object v2, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$12;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mToast:Landroid/widget/Toast;
    invoke-static {v2}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$1100(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
