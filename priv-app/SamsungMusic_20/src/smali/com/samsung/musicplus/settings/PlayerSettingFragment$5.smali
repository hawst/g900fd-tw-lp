.class Lcom/samsung/musicplus/settings/PlayerSettingFragment$5;
.super Landroid/os/Handler;
.source "PlayerSettingFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/settings/PlayerSettingFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/settings/PlayerSettingFragment;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/settings/PlayerSettingFragment;)V
    .locals 0

    .prologue
    .line 375
    iput-object p1, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment$5;->this$0:Lcom/samsung/musicplus/settings/PlayerSettingFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 378
    iget-object v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment$5;->this$0:Lcom/samsung/musicplus/settings/PlayerSettingFragment;

    # getter for: Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->access$400(Lcom/samsung/musicplus/settings/PlayerSettingFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment$5;->this$0:Lcom/samsung/musicplus/settings/PlayerSettingFragment;

    # getter for: Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->access$100(Lcom/samsung/musicplus/settings/PlayerSettingFragment;)Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/musicplus/provider/MusicContents$Audio$Devices;->isSingedIn(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 379
    sput-boolean v2, Lcom/samsung/musicplus/MusicMainActivity;->sNeedUpdateTab:Z

    .line 380
    iget-object v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment$5;->this$0:Lcom/samsung/musicplus/settings/PlayerSettingFragment;

    # invokes: Lcom/samsung/musicplus/settings/PlayerSettingFragment;->dismissProgressDialog()V
    invoke-static {v0}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->access$500(Lcom/samsung/musicplus/settings/PlayerSettingFragment;)V

    .line 381
    iget-object v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment$5;->this$0:Lcom/samsung/musicplus/settings/PlayerSettingFragment;

    # getter for: Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mSlinkRegister:Landroid/preference/CheckBoxPreference;
    invoke-static {v0}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->access$200(Lcom/samsung/musicplus/settings/PlayerSettingFragment;)Landroid/preference/CheckBoxPreference;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 389
    :goto_0
    return-void

    .line 382
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment$5;->this$0:Lcom/samsung/musicplus/settings/PlayerSettingFragment;

    # getter for: Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v0}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->access$400(Lcom/samsung/musicplus/settings/PlayerSettingFragment;)Landroid/app/ProgressDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment$5;->this$0:Lcom/samsung/musicplus/settings/PlayerSettingFragment;

    # getter for: Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mCount:I
    invoke-static {v0}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->access$600(Lcom/samsung/musicplus/settings/PlayerSettingFragment;)I

    move-result v0

    const/16 v1, 0xa

    if-ne v0, v1, :cond_1

    .line 383
    iget-object v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment$5;->this$0:Lcom/samsung/musicplus/settings/PlayerSettingFragment;

    # invokes: Lcom/samsung/musicplus/settings/PlayerSettingFragment;->dismissProgressDialog()V
    invoke-static {v0}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->access$500(Lcom/samsung/musicplus/settings/PlayerSettingFragment;)V

    .line 384
    iget-object v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment$5;->this$0:Lcom/samsung/musicplus/settings/PlayerSettingFragment;

    # getter for: Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mSlinkRegister:Landroid/preference/CheckBoxPreference;
    invoke-static {v0}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->access$200(Lcom/samsung/musicplus/settings/PlayerSettingFragment;)Landroid/preference/CheckBoxPreference;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto :goto_0

    .line 386
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment$5;->this$0:Lcom/samsung/musicplus/settings/PlayerSettingFragment;

    # operator++ for: Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mCount:I
    invoke-static {v0}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->access$608(Lcom/samsung/musicplus/settings/PlayerSettingFragment;)I

    .line 387
    iget-object v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment$5;->this$0:Lcom/samsung/musicplus/settings/PlayerSettingFragment;

    # getter for: Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->access$700(Lcom/samsung/musicplus/settings/PlayerSettingFragment;)Landroid/os/Handler;

    move-result-object v0

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method
