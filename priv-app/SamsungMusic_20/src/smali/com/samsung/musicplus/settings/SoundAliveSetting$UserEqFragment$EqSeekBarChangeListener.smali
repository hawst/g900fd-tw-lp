.class Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqFragment$EqSeekBarChangeListener;
.super Lcom/samsung/musicplus/settings/SoundAliveSetting$SeekBubbleUpdateListener;
.source "SoundAliveSetting.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EqSeekBarChangeListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqFragment;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqFragment;Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;)V
    .locals 0
    .param p2, "bubble"    # Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;

    .prologue
    .line 496
    iput-object p1, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqFragment$EqSeekBarChangeListener;->this$0:Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqFragment;

    .line 497
    invoke-direct {p0, p2}, Lcom/samsung/musicplus/settings/SoundAliveSetting$SeekBubbleUpdateListener;-><init>(Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;)V

    .line 498
    return-void
.end method

.method private getCurrentValues()[I
    .locals 3

    .prologue
    .line 507
    const/4 v2, 0x7

    new-array v0, v2, [I

    .line 508
    .local v0, "eqValue":[I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, v0

    if-ge v1, v2, :cond_0

    .line 509
    iget-object v2, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqFragment$EqSeekBarChangeListener;->this$0:Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqFragment;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqFragment;->mEqSeekBar:[Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;
    invoke-static {v2}, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqFragment;->access$1200(Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqFragment;)[Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;

    move-result-object v2

    aget-object v2, v2, v1

    invoke-virtual {v2}, Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;->getValue()I

    move-result v2

    aput v2, v0, v1

    .line 508
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 511
    :cond_0
    return-object v0
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 2
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromUser"    # Z

    .prologue
    .line 502
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqFragment$EqSeekBarChangeListener;->getCurrentValues()[I

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/samsung/musicplus/util/ServiceUtils;->setSoundAliveUser([I[I)V

    .line 503
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/musicplus/settings/SoundAliveSetting$SeekBubbleUpdateListener;->onProgressChanged(Landroid/widget/SeekBar;IZ)V

    .line 504
    return-void
.end method
