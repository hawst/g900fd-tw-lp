.class public Lcom/samsung/musicplus/settings/PlayerSettingActivity;
.super Lcom/samsung/musicplus/MusicBaseActivity;
.source "PlayerSettingActivity.java"


# instance fields
.field private mMenuKeyCount:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/samsung/musicplus/MusicBaseActivity;-><init>()V

    .line 13
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingActivity;->mMenuKeyCount:I

    return-void
.end method

.method private handleLgtKeyCount(I)V
    .locals 3
    .param p1, "keyCode"    # I

    .prologue
    const/4 v2, 0x0

    .line 34
    sget-boolean v0, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_CHECK_KOR_LGT:Z

    if-eqz v0, :cond_0

    .line 35
    const/16 v0, 0x52

    if-ne p1, v0, :cond_1

    .line 36
    iget v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingActivity;->mMenuKeyCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingActivity;->mMenuKeyCount:I

    .line 40
    :goto_0
    iget v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingActivity;->mMenuKeyCount:I

    const/16 v1, 0xa

    if-ne v0, v1, :cond_0

    .line 41
    const/16 v0, -0x3e7

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/samsung/musicplus/settings/PlayerSettingActivity;->showDialog(ILandroid/os/Bundle;)Z

    .line 42
    iput v2, p0, Lcom/samsung/musicplus/settings/PlayerSettingActivity;->mMenuKeyCount:I

    .line 45
    :cond_0
    return-void

    .line 38
    :cond_1
    iput v2, p0, Lcom/samsung/musicplus/settings/PlayerSettingActivity;->mMenuKeyCount:I

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 17
    invoke-super {p0, p1}, Lcom/samsung/musicplus/MusicBaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 19
    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/PlayerSettingActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "PlayerSetting"

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 20
    .local v0, "fg":Landroid/app/Fragment;
    if-nez v0, :cond_0

    .line 21
    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/PlayerSettingActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    const v2, 0x1020002

    new-instance v3, Lcom/samsung/musicplus/settings/PlayerSettingFragment;

    invoke-direct {v3}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;-><init>()V

    const-string v4, "PlayerSetting"

    invoke-virtual {v1, v2, v3, v4}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commit()I

    .line 25
    :cond_0
    return-void
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/settings/PlayerSettingActivity;->handleLgtKeyCount(I)V

    .line 30
    invoke-super {p0, p1, p2}, Lcom/samsung/musicplus/MusicBaseActivity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method
