.class Lcom/samsung/musicplus/settings/SoundAliveV2Setting$17;
.super Ljava/lang/Object;
.source "SoundAliveV2Setting.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/settings/SoundAliveV2Setting;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)V
    .locals 0

    .prologue
    .line 1481
    iput-object p1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$17;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 5
    .param p1, "v"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/16 v4, 0xc

    const/4 v3, 0x0

    .line 1484
    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$3000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " onKey keyevent = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "  keycode = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1485
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    const/16 v0, 0x13

    if-eq p2, v0, :cond_0

    const/16 v0, 0x14

    if-ne p2, v0, :cond_2

    .line 1487
    :cond_0
    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mLastControlledTab:Z
    invoke-static {}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$3200()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$17;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSoundAlivePresetGridview:Landroid/widget/GridView;
    invoke-static {v0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$500(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Landroid/widget/GridView;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 1488
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$17;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    iget-object v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$17;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSoundAlivePresetGridview:Landroid/widget/GridView;
    invoke-static {v1}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$500(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Landroid/widget/GridView;

    move-result-object v1

    # invokes: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->clearAllsoundAlivePresetCell(Landroid/widget/GridView;)V
    invoke-static {v0, v1}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$600(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;Landroid/widget/GridView;)V

    .line 1489
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$17;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;
    invoke-static {v0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$000(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Lcom/samsung/musicplus/library/audio/SecAudioManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->isHDMIConnect()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1490
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$17;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    iget-object v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$17;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSoundAlivePresetGridview:Landroid/widget/GridView;
    invoke-static {v1}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$500(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Landroid/widget/GridView;

    move-result-object v1

    # invokes: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->selectSoundAlivePresetCell(Landroid/widget/GridView;I)V
    invoke-static {v0, v1, v4}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$700(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;Landroid/widget/GridView;I)V

    .line 1492
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$17;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # invokes: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->setSquareEffect(I)V
    invoke-static {v0, v4}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$800(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;I)V

    .line 1494
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$17;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mCheckBoxAuto:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$900(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1495
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$17;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # invokes: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->setCheckedAuto(Z)V
    invoke-static {v0, v3}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$1000(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;Z)V

    .line 1496
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$17;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    iget-object v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$17;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    invoke-virtual {v1}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f1000e5

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1502
    :cond_2
    return v3
.end method
