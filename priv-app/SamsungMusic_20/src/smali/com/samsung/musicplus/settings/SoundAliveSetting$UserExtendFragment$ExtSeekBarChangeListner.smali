.class Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment$ExtSeekBarChangeListner;
.super Lcom/samsung/musicplus/settings/SoundAliveSetting$SeekBubbleUpdateListener;
.source "SoundAliveSetting.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ExtSeekBarChangeListner"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment;Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;)V
    .locals 0
    .param p2, "bubble"    # Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;

    .prologue
    .line 757
    iput-object p1, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment$ExtSeekBarChangeListner;->this$0:Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment;

    .line 758
    invoke-direct {p0, p2}, Lcom/samsung/musicplus/settings/SoundAliveSetting$SeekBubbleUpdateListener;-><init>(Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;)V

    .line 759
    return-void
.end method

.method private getCurrentValues()[I
    .locals 3

    .prologue
    .line 780
    const/4 v2, 0x5

    new-array v0, v2, [I

    .line 781
    .local v0, "effect":[I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, v0

    if-ge v1, v2, :cond_0

    .line 782
    iget-object v2, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment$ExtSeekBarChangeListner;->this$0:Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment;->mExtSeekBar:[Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;
    invoke-static {v2}, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment;->access$1300(Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment;)[Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;

    move-result-object v2

    aget-object v2, v2, v1

    invoke-virtual {v2}, Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;->getValue()I

    move-result v2

    aput v2, v0, v1

    .line 781
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 784
    :cond_0
    return-object v0
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 2
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromUser"    # Z

    .prologue
    .line 775
    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment$ExtSeekBarChangeListner;->getCurrentValues()[I

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/util/ServiceUtils;->setSoundAliveUser([I[I)V

    .line 776
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/musicplus/settings/SoundAliveSetting$SeekBubbleUpdateListener;->onProgressChanged(Landroid/widget/SeekBar;IZ)V

    .line 777
    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 3
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 765
    invoke-virtual {p1}, Landroid/widget/SeekBar;->getMax()I

    move-result v1

    if-nez v1, :cond_0

    .line 766
    iget-object v1, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment$ExtSeekBarChangeListner;->this$0:Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment;

    const v2, 0x7f100165

    invoke-virtual {v1, v2}, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 767
    .local v0, "message":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/widget/SeekBar;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 771
    .end local v0    # "message":Ljava/lang/String;
    :goto_0
    return-void

    .line 770
    :cond_0
    invoke-super {p0, p1}, Lcom/samsung/musicplus/settings/SoundAliveSetting$SeekBubbleUpdateListener;->onStartTrackingTouch(Landroid/widget/SeekBar;)V

    goto :goto_0
.end method
