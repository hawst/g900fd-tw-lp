.class Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;
.super Landroid/widget/BaseAdapter;
.source "MyMusicMenuReorderListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MyMusicMenuReorderAdapter"
.end annotation


# instance fields
.field private mActiveMenu:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private mActiveMenuOrigin:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private mArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$ViewItem;",
            ">;"
        }
    .end annotation
.end field

.field private mCloneArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$ViewItem;",
            ">;"
        }
    .end annotation
.end field

.field private mContext:Landroid/content/Context;

.field private mScrollState:I

.field final synthetic this$0:Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;


# direct methods
.method public constructor <init>(Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;Landroid/content/Context;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 671
    iput-object p1, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->this$0:Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 661
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->mContext:Landroid/content/Context;

    .line 874
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->mScrollState:I

    .line 672
    iput-object p2, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->mContext:Landroid/content/Context;

    .line 673
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->makePrefMenuArray()V

    .line 674
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;

    .prologue
    .line 659
    iget-object v0, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->mArray:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$002(Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;
    .param p1, "x1"    # Ljava/util/ArrayList;

    .prologue
    .line 659
    iput-object p1, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->mArray:Ljava/util/ArrayList;

    return-object p1
.end method

.method static synthetic access$100(Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;

    .prologue
    .line 659
    iget-object v0, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->mActiveMenu:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;IZ)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;
    .param p1, "x1"    # I
    .param p2, "x2"    # Z

    .prologue
    .line 659
    invoke-direct {p0, p1, p2}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->refreshPrefValue(IZ)V

    return-void
.end method

.method static synthetic access$300(Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;

    .prologue
    .line 659
    iget v0, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->mScrollState:I

    return v0
.end method

.method static synthetic access$500(Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;

    .prologue
    .line 659
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->isItemOrderChanged()Z

    move-result v0

    return v0
.end method

.method static synthetic access$600(Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;

    .prologue
    .line 659
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->isActiveItemChanged()Z

    move-result v0

    return v0
.end method

.method private checkDefaultActiveMenu(I)Z
    .locals 1
    .param p1, "listType"    # I

    .prologue
    .line 735
    const v0, 0x10006

    if-eq p1, v0, :cond_0

    const v0, 0x10008

    if-eq p1, v0, :cond_0

    const v0, 0x20011

    if-ne p1, v0, :cond_1

    .line 736
    :cond_0
    const/4 v0, 0x0

    .line 738
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private isActiveItemChanged()Z
    .locals 10

    .prologue
    .line 828
    const/4 v0, 0x0

    .line 832
    .local v0, "changed":Z
    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->getCount()I

    move-result v1

    .line 833
    .local v1, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_3

    .line 834
    iget-object v5, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->mArray:Ljava/util/ArrayList;

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$ViewItem;

    iget v4, v5, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$ViewItem;->type:I

    .line 835
    .local v4, "type":I
    iget-object v5, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->this$0:Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;

    # getter for: Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mTabMenuReorder:Z
    invoke-static {v5}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->access$700(Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 836
    invoke-static {v4}, Lcom/samsung/musicplus/util/ListUtils;->getDefaultTabPrefValue(I)Ljava/lang/String;

    move-result-object v3

    .line 841
    .local v3, "pref":Ljava/lang/String;
    :goto_1
    invoke-static {v4}, Lcom/samsung/musicplus/util/ListUtils;->isFixedList(I)Z

    move-result v5

    if-nez v5, :cond_0

    int-to-long v6, v4

    const-wide/16 v8, -0xb

    cmp-long v5, v6, v8

    if-nez v5, :cond_2

    .line 833
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 838
    .end local v3    # "pref":Ljava/lang/String;
    :cond_1
    int-to-long v6, v4

    invoke-static {v6, v7}, Lcom/samsung/musicplus/util/ListUtils;->getDefaultPlayListPrefValue(J)Ljava/lang/String;

    move-result-object v3

    .restart local v3    # "pref":Ljava/lang/String;
    goto :goto_1

    .line 845
    :cond_2
    iget-object v5, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->mActiveMenu:Ljava/util/HashMap;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->mActiveMenuOrigin:Ljava/util/HashMap;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->mActiveMenu:Ljava/util/HashMap;

    invoke-virtual {v5, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    iget-object v6, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->mActiveMenuOrigin:Ljava/util/HashMap;

    invoke-virtual {v6, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 846
    const/4 v0, 0x1

    .line 850
    .end local v3    # "pref":Ljava/lang/String;
    .end local v4    # "type":I
    :cond_3
    return v0
.end method

.method private isItemOrderChanged()Z
    .locals 6

    .prologue
    .line 811
    const/4 v0, 0x0

    .line 815
    .local v0, "changed":Z
    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->getCount()I

    move-result v2

    .line 816
    .local v2, "count":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-ge v4, v2, :cond_0

    .line 817
    iget-object v5, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->mCloneArray:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$ViewItem;

    iget-object v1, v5, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$ViewItem;->text:Ljava/lang/String;

    .line 818
    .local v1, "clone":Ljava/lang/String;
    iget-object v5, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->mArray:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$ViewItem;

    iget-object v3, v5, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$ViewItem;->text:Ljava/lang/String;

    .line 819
    .local v3, "current":Ljava/lang/String;
    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 820
    const/4 v0, 0x1

    .line 824
    .end local v1    # "clone":Ljava/lang/String;
    .end local v3    # "current":Ljava/lang/String;
    :cond_0
    return v0

    .line 816
    .restart local v1    # "clone":Ljava/lang/String;
    .restart local v3    # "current":Ljava/lang/String;
    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method

.method private makePrefMenuArray()V
    .locals 13

    .prologue
    const/4 v12, 0x0

    .line 677
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    iput-object v7, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->mArray:Ljava/util/ArrayList;

    iput-object v7, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->mCloneArray:Ljava/util/ArrayList;

    .line 678
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    iput-object v7, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->mActiveMenu:Ljava/util/HashMap;

    .line 679
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    iput-object v7, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->mActiveMenuOrigin:Ljava/util/HashMap;

    .line 681
    iget-object v7, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->this$0:Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;

    # getter for: Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mTabMenuReorder:Z
    invoke-static {v7}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->access$700(Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 682
    iget-object v7, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->this$0:Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;

    # getter for: Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mPreference:Landroid/content/SharedPreferences;
    invoke-static {v7}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->access$800(Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;)Landroid/content/SharedPreferences;

    move-result-object v7

    const-string v8, "tab_menu_list_order"

    invoke-static {}, Lcom/samsung/musicplus/util/ListUtils;->getDefaultTabOrder()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v7, v8, v9}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 689
    .local v3, "tmpString":Ljava/lang/String;
    :goto_0
    new-instance v2, Ljava/util/StringTokenizer;

    const-string v7, "|"

    invoke-direct {v2, v3, v7}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 694
    .local v2, "st":Ljava/util/StringTokenizer;
    :cond_0
    :goto_1
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->hasMoreElements()Z

    move-result v7

    if-eqz v7, :cond_5

    .line 695
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextElement()Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 696
    .local v4, "token":Ljava/lang/String;
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 697
    .local v5, "type":I
    const v7, 0x1000a

    if-eq v5, v7, :cond_0

    .line 700
    new-instance v6, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$ViewItem;

    invoke-direct {v6}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$ViewItem;-><init>()V

    .line 701
    .local v6, "vh":Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$ViewItem;
    iput v5, v6, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$ViewItem;->type:I

    .line 702
    iget-object v7, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->this$0:Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;

    # getter for: Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mTabMenuReorder:Z
    invoke-static {v7}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->access$700(Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 703
    iget-object v7, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->mContext:Landroid/content/Context;

    iget v8, v6, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$ViewItem;->type:I

    invoke-static {v8}, Lcom/samsung/musicplus/util/ListUtils;->getDefaultTabName(I)I

    move-result v8

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$ViewItem;->text:Ljava/lang/String;

    .line 707
    :goto_2
    iget-object v7, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->mArray:Ljava/util/ArrayList;

    invoke-virtual {v7, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 709
    invoke-static {v5}, Lcom/samsung/musicplus/util/ListUtils;->isFixedList(I)Z

    move-result v7

    if-nez v7, :cond_0

    const-wide/16 v8, -0xb

    int-to-long v10, v5

    cmp-long v7, v8, v10

    if-eqz v7, :cond_0

    .line 713
    iget-object v7, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->this$0:Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;

    # getter for: Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mTabMenuReorder:Z
    invoke-static {v7}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->access$700(Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 714
    invoke-static {v5}, Lcom/samsung/musicplus/util/ListUtils;->getDefaultTabPrefValue(I)Ljava/lang/String;

    move-result-object v1

    .line 718
    .local v1, "pref":Ljava/lang/String;
    :goto_3
    iget-object v7, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->this$0:Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;

    # getter for: Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mPreference:Landroid/content/SharedPreferences;
    invoke-static {v7}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->access$800(Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;)Landroid/content/SharedPreferences;

    move-result-object v7

    const/4 v8, 0x1

    invoke-interface {v7, v1, v8}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 723
    .local v0, "active":Z
    invoke-direct {p0, v5}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->checkDefaultActiveMenu(I)Z

    move-result v7

    if-nez v7, :cond_4

    iget-object v7, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->this$0:Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;

    # getter for: Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mPreference:Landroid/content/SharedPreferences;
    invoke-static {v7}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->access$800(Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;)Landroid/content/SharedPreferences;

    move-result-object v7

    invoke-interface {v7, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_4

    .line 724
    iget-object v7, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->mActiveMenu:Ljava/util/HashMap;

    invoke-static {v12}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v7, v1, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 725
    iget-object v7, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->mActiveMenuOrigin:Ljava/util/HashMap;

    invoke-static {v12}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v7, v1, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 726
    iget-object v7, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->this$0:Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;

    # invokes: Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->savePreference(Ljava/lang/String;Z)V
    invoke-static {v7, v1, v12}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->access$900(Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;Ljava/lang/String;Z)V

    goto/16 :goto_1

    .line 685
    .end local v0    # "active":Z
    .end local v1    # "pref":Ljava/lang/String;
    .end local v2    # "st":Ljava/util/StringTokenizer;
    .end local v3    # "tmpString":Ljava/lang/String;
    .end local v4    # "token":Ljava/lang/String;
    .end local v5    # "type":I
    .end local v6    # "vh":Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$ViewItem;
    :cond_1
    iget-object v7, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->this$0:Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;

    # getter for: Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mPreference:Landroid/content/SharedPreferences;
    invoke-static {v7}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->access$800(Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;)Landroid/content/SharedPreferences;

    move-result-object v7

    const-string v8, "playlist_menu_list_order"

    const-string v9, "-11|-12|-13|-14"

    invoke-interface {v7, v8, v9}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .restart local v3    # "tmpString":Ljava/lang/String;
    goto/16 :goto_0

    .line 705
    .restart local v2    # "st":Ljava/util/StringTokenizer;
    .restart local v4    # "token":Ljava/lang/String;
    .restart local v5    # "type":I
    .restart local v6    # "vh":Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$ViewItem;
    :cond_2
    iget-object v7, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->mContext:Landroid/content/Context;

    iget v8, v6, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$ViewItem;->type:I

    int-to-long v8, v8

    invoke-static {v8, v9}, Lcom/samsung/musicplus/util/ListUtils;->getDefaultPlaylistName(J)I

    move-result v8

    invoke-virtual {v7, v8}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v6, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$ViewItem;->text:Ljava/lang/String;

    goto :goto_2

    .line 716
    :cond_3
    int-to-long v8, v5

    invoke-static {v8, v9}, Lcom/samsung/musicplus/util/ListUtils;->getDefaultPlayListPrefValue(J)Ljava/lang/String;

    move-result-object v1

    .restart local v1    # "pref":Ljava/lang/String;
    goto :goto_3

    .line 728
    .restart local v0    # "active":Z
    :cond_4
    iget-object v7, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->mActiveMenu:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v7, v1, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 729
    iget-object v7, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->mActiveMenuOrigin:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    invoke-virtual {v7, v1, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_1

    .line 732
    .end local v0    # "active":Z
    .end local v1    # "pref":Ljava/lang/String;
    .end local v4    # "token":Ljava/lang/String;
    .end local v5    # "type":I
    .end local v6    # "vh":Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$ViewItem;
    :cond_5
    return-void
.end method

.method private refreshPrefValue(IZ)V
    .locals 4
    .param p1, "type"    # I
    .param p2, "value"    # Z

    .prologue
    .line 798
    iget-object v1, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->this$0:Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;

    # getter for: Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mTabMenuReorder:Z
    invoke-static {v1}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->access$700(Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 799
    invoke-static {p1}, Lcom/samsung/musicplus/util/ListUtils;->getDefaultTabPrefValue(I)Ljava/lang/String;

    move-result-object v0

    .line 803
    .local v0, "pref":Ljava/lang/String;
    :goto_0
    iget-object v1, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->mActiveMenu:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 804
    iget-object v1, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->mContext:Landroid/content/Context;

    instance-of v1, v1, Lcom/samsung/musicplus/settings/MyMusicMenuReorderActivity;

    if-eqz v1, :cond_1

    .line 805
    iget-object v1, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->mContext:Landroid/content/Context;

    check-cast v1, Lcom/samsung/musicplus/settings/MyMusicMenuReorderActivity;

    invoke-direct {p0}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->isActiveItemChanged()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-direct {p0}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->isItemOrderChanged()Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_0
    const/4 v2, 0x1

    :goto_1
    invoke-virtual {v1, v2}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderActivity;->enableDoneButton(Z)V

    .line 808
    :cond_1
    return-void

    .line 801
    .end local v0    # "pref":Ljava/lang/String;
    :cond_2
    int-to-long v2, p1

    invoke-static {v2, v3}, Lcom/samsung/musicplus/util/ListUtils;->getDefaultPlayListPrefValue(J)Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "pref":Ljava/lang/String;
    goto :goto_0

    .line 805
    :cond_3
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private updateView(Landroid/view/View;Landroid/widget/CheckBox;I)V
    .locals 5
    .param p1, "view"    # Landroid/view/View;
    .param p2, "check"    # Landroid/widget/CheckBox;
    .param p3, "type"    # I

    .prologue
    const/4 v4, 0x1

    .line 782
    invoke-static {p3}, Lcom/samsung/musicplus/util/ListUtils;->isFixedList(I)Z

    move-result v0

    if-nez v0, :cond_0

    int-to-long v0, p3

    const-wide/16 v2, -0xb

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 783
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 784
    invoke-virtual {p2, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 794
    :goto_0
    return-void

    .line 786
    :cond_1
    invoke-virtual {p1, v4}, Landroid/view/View;->setEnabled(Z)V

    .line 787
    invoke-virtual {p2, v4}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 788
    iget-object v0, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->this$0:Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;

    # getter for: Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mTabMenuReorder:Z
    invoke-static {v0}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->access$700(Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 789
    iget-object v0, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->mActiveMenu:Ljava/util/HashMap;

    invoke-static {p3}, Lcom/samsung/musicplus/util/ListUtils;->getDefaultTabPrefValue(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p2, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0

    .line 791
    :cond_2
    iget-object v0, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->mActiveMenu:Ljava/util/HashMap;

    int-to-long v2, p3

    invoke-static {v2, v3}, Lcom/samsung/musicplus/util/ListUtils;->getDefaultPlayListPrefValue(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-virtual {p2, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0
.end method


# virtual methods
.method public getArrayList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$ViewItem;",
            ">;"
        }
    .end annotation

    .prologue
    .line 855
    iget-object v0, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->mArray:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    return-object v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 743
    iget-object v0, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->mArray:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 748
    iget-object v0, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->mArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 753
    iget-object v0, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->mArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$ViewItem;

    iget v0, v0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$ViewItem;->type:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "position"    # I
    .param p2, "view"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 758
    move v2, p1

    .line 760
    .local v2, "pos":I
    if-nez p2, :cond_0

    .line 761
    iget-object v4, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->mContext:Landroid/content/Context;

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 762
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v4, 0x7f04006e

    const/4 v5, 0x0

    invoke-virtual {v1, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 765
    .end local v1    # "inflater":Landroid/view/LayoutInflater;
    :cond_0
    const v4, 0x7f0d0138

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    .line 766
    .local v3, "text":Landroid/widget/TextView;
    const v4, 0x7f0d0139

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    .line 767
    .local v0, "check":Landroid/widget/CheckBox;
    new-instance v4, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter$1;

    invoke-direct {v4, p0, v2}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter$1;-><init>(Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;I)V

    invoke-virtual {v0, v4}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 775
    iget-object v4, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->mArray:Ljava/util/ArrayList;

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$ViewItem;

    iget v4, v4, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$ViewItem;->type:I

    invoke-direct {p0, p2, v0, v4}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->updateView(Landroid/view/View;Landroid/widget/CheckBox;I)V

    .line 776
    iget-object v4, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->mArray:Ljava/util/ArrayList;

    invoke-virtual {v4, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$ViewItem;

    iget-object v4, v4, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$ViewItem;->text:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 778
    return-object p2
.end method

.method public setScrollSate(I)V
    .locals 0
    .param p1, "scrollState"    # I

    .prologue
    .line 877
    iput p1, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->mScrollState:I

    .line 878
    return-void
.end method

.method public setToggleCheck([I)V
    .locals 10
    .param p1, "items"    # [I

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 859
    move-object v0, p1

    .local v0, "arr$":[I
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_5

    aget v1, v0, v2

    .line 861
    .local v1, "i":I
    iget-object v5, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->this$0:Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;

    # getter for: Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mTabMenuReorder:Z
    invoke-static {v5}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->access$700(Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 862
    iget-object v5, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->mArray:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$ViewItem;

    iget v5, v5, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$ViewItem;->type:I

    invoke-static {v5}, Lcom/samsung/musicplus/util/ListUtils;->getDefaultTabPrefValue(I)Ljava/lang/String;

    move-result-object v4

    .line 866
    .local v4, "pref":Ljava/lang/String;
    :goto_1
    iget-object v8, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->mActiveMenu:Ljava/util/HashMap;

    iget-object v5, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->mActiveMenu:Ljava/util/HashMap;

    invoke-virtual {v5, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-nez v5, :cond_3

    move v5, v6

    :goto_2
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v8, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 867
    iget-object v5, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->mContext:Landroid/content/Context;

    instance-of v5, v5, Lcom/samsung/musicplus/settings/MyMusicMenuReorderActivity;

    if-eqz v5, :cond_1

    .line 868
    iget-object v5, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->mContext:Landroid/content/Context;

    check-cast v5, Lcom/samsung/musicplus/settings/MyMusicMenuReorderActivity;

    invoke-direct {p0}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->isActiveItemChanged()Z

    move-result v8

    if-nez v8, :cond_0

    invoke-direct {p0}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->isItemOrderChanged()Z

    move-result v8

    if-eqz v8, :cond_4

    :cond_0
    move v8, v6

    :goto_3
    invoke-virtual {v5, v8}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderActivity;->enableDoneButton(Z)V

    .line 859
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 864
    .end local v4    # "pref":Ljava/lang/String;
    :cond_2
    iget-object v5, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->mArray:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$ViewItem;

    iget v5, v5, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$ViewItem;->type:I

    int-to-long v8, v5

    invoke-static {v8, v9}, Lcom/samsung/musicplus/util/ListUtils;->getDefaultPlayListPrefValue(J)Ljava/lang/String;

    move-result-object v4

    .restart local v4    # "pref":Ljava/lang/String;
    goto :goto_1

    :cond_3
    move v5, v7

    .line 866
    goto :goto_2

    :cond_4
    move v8, v7

    .line 868
    goto :goto_3

    .line 872
    .end local v1    # "i":I
    .end local v4    # "pref":Ljava/lang/String;
    :cond_5
    return-void
.end method
