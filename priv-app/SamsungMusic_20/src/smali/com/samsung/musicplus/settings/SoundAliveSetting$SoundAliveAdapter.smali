.class Lcom/samsung/musicplus/settings/SoundAliveSetting$SoundAliveAdapter;
.super Landroid/widget/ArrayAdapter;
.source "SoundAliveSetting.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/settings/SoundAliveSetting;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "SoundAliveAdapter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/ArrayAdapter",
        "<",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field private mAudioPath:I

.field final synthetic this$0:Lcom/samsung/musicplus/settings/SoundAliveSetting;


# direct methods
.method public constructor <init>(Lcom/samsung/musicplus/settings/SoundAliveSetting;Landroid/content/Context;I[Ljava/lang/String;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "textViewResourceId"    # I
    .param p4, "objects"    # [Ljava/lang/String;

    .prologue
    .line 170
    iput-object p1, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$SoundAliveAdapter;->this$0:Lcom/samsung/musicplus/settings/SoundAliveSetting;

    .line 171
    invoke-direct {p0, p2, p3, p4}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    .line 174
    const/4 v0, 0x2

    iput v0, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$SoundAliveAdapter;->mAudioPath:I

    .line 172
    return-void
.end method

.method private isEnableEffect(I)Z
    .locals 2
    .param p1, "soundAlive"    # I

    .prologue
    .line 181
    iget v1, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$SoundAliveAdapter;->mAudioPath:I

    invoke-static {v1, p1}, Lcom/samsung/musicplus/util/SoundAliveUtils;->getSoundAliveErrorMessage(II)I

    move-result v0

    .line 182
    .local v0, "message":I
    sget v1, Lcom/samsung/musicplus/util/SoundAliveUtils;->UNDEFINED:I

    if-ne v0, v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 194
    const/4 p2, 0x0

    .line 196
    invoke-super {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;->getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 197
    .local v0, "v":Landroid/view/View;
    # getter for: Lcom/samsung/musicplus/settings/SoundAliveSetting;->EQ_LIST:[I
    invoke-static {}, Lcom/samsung/musicplus/settings/SoundAliveSetting;->access$000()[I

    move-result-object v1

    aget v1, v1, p1

    invoke-direct {p0, v1}, Lcom/samsung/musicplus/settings/SoundAliveSetting$SoundAliveAdapter;->isEnableEffect(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 198
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 200
    :cond_0
    return-object v0
.end method

.method public isEnabled(I)Z
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 187
    # getter for: Lcom/samsung/musicplus/settings/SoundAliveSetting;->EQ_LIST:[I
    invoke-static {}, Lcom/samsung/musicplus/settings/SoundAliveSetting;->access$000()[I

    move-result-object v0

    aget v0, v0, p1

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/settings/SoundAliveSetting$SoundAliveAdapter;->isEnableEffect(I)Z

    move-result v0

    return v0
.end method

.method public setAudioPath(I)V
    .locals 0
    .param p1, "path"    # I

    .prologue
    .line 177
    iput p1, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$SoundAliveAdapter;->mAudioPath:I

    .line 178
    return-void
.end method
