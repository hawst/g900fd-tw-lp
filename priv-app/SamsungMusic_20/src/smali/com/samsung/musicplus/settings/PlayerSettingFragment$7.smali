.class Lcom/samsung/musicplus/settings/PlayerSettingFragment$7;
.super Ljava/lang/Object;
.source "PlayerSettingFragment.java"

# interfaces
.implements Landroid/app/TimePickerDialog$OnTimeSetListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/settings/PlayerSettingFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/settings/PlayerSettingFragment;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/settings/PlayerSettingFragment;)V
    .locals 0

    .prologue
    .line 642
    iput-object p1, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment$7;->this$0:Lcom/samsung/musicplus/settings/PlayerSettingFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTimeSet(Landroid/widget/TimePicker;II)V
    .locals 4
    .param p1, "arg0"    # Landroid/widget/TimePicker;
    .param p2, "hour"    # I
    .param p3, "minute"    # I

    .prologue
    .line 646
    if-nez p2, :cond_0

    if-nez p3, :cond_0

    .line 647
    iget-object v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment$7;->this$0:Lcom/samsung/musicplus/settings/PlayerSettingFragment;

    # getter for: Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mMusicAutoOff:Landroid/preference/ListPreference;
    invoke-static {v0}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->access$900(Lcom/samsung/musicplus/settings/PlayerSettingFragment;)Landroid/preference/ListPreference;

    move-result-object v0

    const-string v1, "0"

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 648
    iget-object v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment$7;->this$0:Lcom/samsung/musicplus/settings/PlayerSettingFragment;

    iget-object v1, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment$7;->this$0:Lcom/samsung/musicplus/settings/PlayerSettingFragment;

    const/4 v2, 0x0

    # setter for: Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mCuAutoOffMin:I
    invoke-static {v1, v2}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->access$1102(Lcom/samsung/musicplus/settings/PlayerSettingFragment;I)I

    move-result v1

    # setter for: Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mCuAutoOffHour:I
    invoke-static {v0, v1}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->access$1002(Lcom/samsung/musicplus/settings/PlayerSettingFragment;I)I

    .line 649
    iget-object v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment$7;->this$0:Lcom/samsung/musicplus/settings/PlayerSettingFragment;

    const-string v1, "0"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    # invokes: Lcom/samsung/musicplus/settings/PlayerSettingFragment;->setAlarm(I)V
    invoke-static {v0, v1}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->access$1200(Lcom/samsung/musicplus/settings/PlayerSettingFragment;I)V

    .line 655
    :goto_0
    iget-object v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment$7;->this$0:Lcom/samsung/musicplus/settings/PlayerSettingFragment;

    iget-object v1, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment$7;->this$0:Lcom/samsung/musicplus/settings/PlayerSettingFragment;

    # getter for: Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mUiPreference:Landroid/content/SharedPreferences;
    invoke-static {v1}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->access$1300(Lcom/samsung/musicplus/settings/PlayerSettingFragment;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "music_auto_off_custom_hour"

    iget-object v3, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment$7;->this$0:Lcom/samsung/musicplus/settings/PlayerSettingFragment;

    # getter for: Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mCuAutoOffHour:I
    invoke-static {v3}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->access$1000(Lcom/samsung/musicplus/settings/PlayerSettingFragment;)I

    move-result v3

    # invokes: Lcom/samsung/musicplus/settings/PlayerSettingFragment;->savePreference(Landroid/content/SharedPreferences;Ljava/lang/String;I)V
    invoke-static {v0, v1, v2, v3}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->access$1400(Lcom/samsung/musicplus/settings/PlayerSettingFragment;Landroid/content/SharedPreferences;Ljava/lang/String;I)V

    .line 656
    iget-object v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment$7;->this$0:Lcom/samsung/musicplus/settings/PlayerSettingFragment;

    iget-object v1, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment$7;->this$0:Lcom/samsung/musicplus/settings/PlayerSettingFragment;

    # getter for: Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mUiPreference:Landroid/content/SharedPreferences;
    invoke-static {v1}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->access$1300(Lcom/samsung/musicplus/settings/PlayerSettingFragment;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "music_auto_off_custom_min"

    iget-object v3, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment$7;->this$0:Lcom/samsung/musicplus/settings/PlayerSettingFragment;

    # getter for: Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mCuAutoOffMin:I
    invoke-static {v3}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->access$1100(Lcom/samsung/musicplus/settings/PlayerSettingFragment;)I

    move-result v3

    # invokes: Lcom/samsung/musicplus/settings/PlayerSettingFragment;->savePreference(Landroid/content/SharedPreferences;Ljava/lang/String;I)V
    invoke-static {v0, v1, v2, v3}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->access$1400(Lcom/samsung/musicplus/settings/PlayerSettingFragment;Landroid/content/SharedPreferences;Ljava/lang/String;I)V

    .line 657
    return-void

    .line 651
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment$7;->this$0:Lcom/samsung/musicplus/settings/PlayerSettingFragment;

    # getter for: Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mMusicAutoOff:Landroid/preference/ListPreference;
    invoke-static {v0}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->access$900(Lcom/samsung/musicplus/settings/PlayerSettingFragment;)Landroid/preference/ListPreference;

    move-result-object v0

    const-string v1, "-999"

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 652
    invoke-virtual {p0, p2, p3}, Lcom/samsung/musicplus/settings/PlayerSettingFragment$7;->updateCuAutoOffTime(II)V

    goto :goto_0
.end method

.method updateCuAutoOffTime(II)V
    .locals 3
    .param p1, "hour"    # I
    .param p2, "minute"    # I

    .prologue
    .line 660
    iget-object v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment$7;->this$0:Lcom/samsung/musicplus/settings/PlayerSettingFragment;

    # setter for: Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mCuAutoOffHour:I
    invoke-static {v0, p1}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->access$1002(Lcom/samsung/musicplus/settings/PlayerSettingFragment;I)I

    .line 661
    iget-object v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment$7;->this$0:Lcom/samsung/musicplus/settings/PlayerSettingFragment;

    # setter for: Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mCuAutoOffMin:I
    invoke-static {v0, p2}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->access$1102(Lcom/samsung/musicplus/settings/PlayerSettingFragment;I)I

    .line 662
    iget-object v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment$7;->this$0:Lcom/samsung/musicplus/settings/PlayerSettingFragment;

    const v1, 0x36ee80

    mul-int/2addr v1, p1

    const v2, 0xea60

    mul-int/2addr v2, p2

    add-int/2addr v1, v2

    # invokes: Lcom/samsung/musicplus/settings/PlayerSettingFragment;->setAlarm(I)V
    invoke-static {v0, v1}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->access$1200(Lcom/samsung/musicplus/settings/PlayerSettingFragment;I)V

    .line 663
    return-void
.end method
