.class Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqView;
.super Landroid/view/View;
.source "SoundAliveV2Setting.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/settings/SoundAliveV2Setting;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UserEqView"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;


# direct methods
.method public constructor <init>(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;Landroid/content/Context;)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 1530
    iput-object p1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqView;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    .line 1531
    invoke-direct {p0, p2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 1532
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 14
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 1536
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 1539
    const/high16 v8, 0x40a00000    # 5.0f

    .line 1540
    .local v8, "textPosX":F
    const/high16 v5, 0x429c0000    # 78.0f

    .line 1541
    .local v5, "paddingTopPortrait":F
    const/high16 v4, 0x42960000    # 75.0f

    .line 1543
    .local v4, "paddingTopHorizontal":F
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 1544
    const/4 v9, 0x0

    iget-object v10, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqView;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mDbLinePaddingTop:I
    invoke-static {v10}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$3400(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)I

    move-result v10

    int-to-float v10, v10

    invoke-virtual {p1, v9, v10}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1546
    new-instance v6, Landroid/graphics/Paint;

    const/4 v9, 0x1

    invoke-direct {v6, v9}, Landroid/graphics/Paint;-><init>(I)V

    .line 1549
    .local v6, "pnt":Landroid/graphics/Paint;
    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->THEME:I
    invoke-static {}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$3500()I

    move-result v9

    if-nez v9, :cond_5

    .line 1550
    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-static {v9, v10, v11}, Landroid/graphics/Color;->rgb(III)I

    move-result v9

    invoke-virtual {v6, v9}, Landroid/graphics/Paint;->setColor(I)V

    .line 1555
    :goto_0
    iget-object v9, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqView;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # invokes: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->getScreenRawWidth()I
    invoke-static {v9}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$3600(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)I

    move-result v7

    .line 1556
    .local v7, "screenRawidth":I
    const/4 v3, 0x0

    .line 1557
    .local v3, "offset":I
    iget-object v9, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqView;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # invokes: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->isPortrait()Z
    invoke-static {v9}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$2600(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Z

    move-result v1

    .line 1559
    .local v1, "isPortrait":Z
    if-nez v1, :cond_0

    .line 1560
    const/high16 v8, 0x42480000    # 50.0f

    .line 1563
    :cond_0
    if-eqz v1, :cond_1

    const/16 v9, 0x5a0

    if-ge v7, v9, :cond_2

    :cond_1
    if-nez v1, :cond_6

    const/16 v9, 0xa00

    if-lt v7, v9, :cond_6

    .line 1565
    :cond_2
    const/high16 v9, 0x42180000    # 38.0f

    invoke-virtual {v6, v9}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 1566
    if-eqz v1, :cond_3

    .line 1567
    const/high16 v8, 0x41a00000    # 20.0f

    .line 1569
    :cond_3
    const/high16 v5, 0x42e20000    # 113.0f

    .line 1570
    const/high16 v4, 0x42d40000    # 106.0f

    .line 1571
    iget-object v9, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqView;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    const-wide v10, 0x3fea3d70a3d70a3dL    # 0.82

    # setter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mRatePortlinePosY:D
    invoke-static {v9, v10, v11}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$3702(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;D)D

    .line 1572
    iget-object v9, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqView;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    const-wide v10, 0x3fea666666666666L    # 0.825

    # setter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mRateLandlinePosY:D
    invoke-static {v9, v10, v11}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$3802(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;D)D

    .line 1635
    :cond_4
    :goto_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_2
    const/16 v9, 0xa

    if-gt v0, v9, :cond_1d

    .line 1636
    if-eqz v1, :cond_1c

    .line 1637
    iget-object v9, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqView;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSeekBarHeight:I
    invoke-static {v9}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$4000(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)I

    move-result v9

    mul-int/2addr v9, v0

    div-int/lit8 v9, v9, 0xa

    int-to-double v10, v9

    iget-object v9, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqView;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mRatePortlinePosY:D
    invoke-static {v9}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$3700(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)D

    move-result-wide v12

    mul-double/2addr v10, v12

    float-to-double v12, v5

    add-double/2addr v10, v12

    double-to-float v2, v10

    .line 1642
    .local v2, "linePosY":F
    :goto_3
    sparse-switch v0, :sswitch_data_0

    .line 1635
    :goto_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1552
    .end local v0    # "i":I
    .end local v1    # "isPortrait":Z
    .end local v2    # "linePosY":F
    .end local v3    # "offset":I
    .end local v7    # "screenRawidth":I
    :cond_5
    const/16 v9, 0xf5

    const/16 v10, 0xf5

    const/16 v11, 0xf5

    invoke-static {v9, v10, v11}, Landroid/graphics/Color;->rgb(III)I

    move-result v9

    invoke-virtual {v6, v9}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_0

    .line 1573
    .restart local v1    # "isPortrait":Z
    .restart local v3    # "offset":I
    .restart local v7    # "screenRawidth":I
    :cond_6
    if-eqz v1, :cond_7

    const/16 v9, 0x438

    if-ge v7, v9, :cond_8

    :cond_7
    if-nez v1, :cond_9

    const/16 v9, 0x780

    if-lt v7, v9, :cond_9

    .line 1574
    :cond_8
    const/high16 v9, 0x42180000    # 38.0f

    invoke-virtual {v6, v9}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 1575
    iget-object v9, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqView;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    const-wide v10, 0x3fea7ef9db22d0e5L    # 0.828

    # setter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mRatePortlinePosY:D
    invoke-static {v9, v10, v11}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$3702(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;D)D

    .line 1576
    iget-object v9, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqView;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    const-wide v10, 0x3fea6e978d4fdf3bL    # 0.826

    # setter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mRateLandlinePosY:D
    invoke-static {v9, v10, v11}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$3802(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;D)D

    goto :goto_1

    .line 1577
    :cond_9
    if-eqz v1, :cond_a

    const/16 v9, 0x2d0

    if-ge v7, v9, :cond_b

    :cond_a
    if-nez v1, :cond_e

    const/16 v9, 0x500

    if-lt v7, v9, :cond_e

    .line 1578
    :cond_b
    const/high16 v9, 0x41c80000    # 25.0f

    invoke-virtual {v6, v9}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 1579
    if-eqz v1, :cond_c

    .line 1580
    const/high16 v8, 0x41400000    # 12.0f

    .line 1582
    :cond_c
    const/16 v3, 0x8

    .line 1583
    iget-object v9, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqView;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # invokes: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->getDensity()I
    invoke-static {v9}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$3900(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)I

    move-result v9

    const/16 v10, 0x140

    if-ne v9, v10, :cond_d

    .line 1585
    const/high16 v5, 0x42540000    # 53.0f

    .line 1586
    const/high16 v4, 0x42480000    # 50.0f

    .line 1587
    iget-object v9, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqView;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    const-wide v10, 0x3fea7ef9db22d0e5L    # 0.828

    # setter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mRatePortlinePosY:D
    invoke-static {v9, v10, v11}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$3702(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;D)D

    .line 1588
    iget-object v9, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqView;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    const-wide v10, 0x3fea5604189374bcL    # 0.823

    # setter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mRateLandlinePosY:D
    invoke-static {v9, v10, v11}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$3802(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;D)D

    goto/16 :goto_1

    .line 1590
    :cond_d
    iget-object v9, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqView;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    const-wide v10, 0x3fea2d0e56041893L    # 0.818

    # setter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mRatePortlinePosY:D
    invoke-static {v9, v10, v11}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$3702(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;D)D

    .line 1591
    iget-object v9, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqView;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    const-wide v10, 0x3fe62d0e56041893L    # 0.693

    # setter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mRateLandlinePosY:D
    invoke-static {v9, v10, v11}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$3802(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;D)D

    goto/16 :goto_1

    .line 1594
    :cond_e
    if-eqz v1, :cond_f

    const/16 v9, 0x21c

    if-gt v7, v9, :cond_10

    :cond_f
    if-nez v1, :cond_12

    const/16 v9, 0x3c0

    if-le v7, v9, :cond_12

    .line 1595
    :cond_10
    const/high16 v9, 0x41a00000    # 20.0f

    invoke-virtual {v6, v9}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 1596
    iget-object v9, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqView;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # invokes: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->getDensity()I
    invoke-static {v9}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$3900(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)I

    move-result v9

    const/16 v10, 0xf0

    if-ne v9, v10, :cond_11

    .line 1598
    iget-object v9, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqView;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    const-wide v10, 0x3febc6a7ef9db22dL    # 0.868

    # setter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mRatePortlinePosY:D
    invoke-static {v9, v10, v11}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$3702(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;D)D

    .line 1599
    iget-object v9, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqView;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    const-wide v10, 0x3fe9b22d0e560419L    # 0.803

    # setter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mRateLandlinePosY:D
    invoke-static {v9, v10, v11}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$3802(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;D)D

    goto/16 :goto_1

    .line 1601
    :cond_11
    iget-object v9, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqView;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    const-wide v10, 0x3fea2d0e56041893L    # 0.818

    # setter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mRatePortlinePosY:D
    invoke-static {v9, v10, v11}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$3702(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;D)D

    .line 1602
    iget-object v9, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqView;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    const-wide v10, 0x3fe62d0e56041893L    # 0.693

    # setter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mRateLandlinePosY:D
    invoke-static {v9, v10, v11}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$3802(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;D)D

    goto/16 :goto_1

    .line 1605
    :cond_12
    if-eqz v1, :cond_13

    const/16 v9, 0x1e0

    if-gt v7, v9, :cond_14

    :cond_13
    if-nez v1, :cond_15

    const/16 v9, 0x320

    if-le v7, v9, :cond_15

    .line 1606
    :cond_14
    const/high16 v9, 0x41a00000    # 20.0f

    invoke-virtual {v6, v9}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 1607
    const/high16 v5, 0x42240000    # 41.0f

    .line 1608
    const/high16 v4, 0x42140000    # 37.0f

    .line 1609
    iget-object v9, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqView;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    const-wide v10, 0x3fea7ef9db22d0e5L    # 0.828

    # setter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mRatePortlinePosY:D
    invoke-static {v9, v10, v11}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$3702(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;D)D

    .line 1610
    iget-object v9, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqView;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    const-wide v10, 0x3fea45a1cac08312L    # 0.821

    # setter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mRateLandlinePosY:D
    invoke-static {v9, v10, v11}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$3802(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;D)D

    .line 1611
    if-nez v1, :cond_4

    .line 1612
    const/high16 v8, 0x41a00000    # 20.0f

    goto/16 :goto_1

    .line 1614
    :cond_15
    if-eqz v1, :cond_16

    const/16 v9, 0x140

    if-gt v7, v9, :cond_17

    :cond_16
    if-nez v1, :cond_18

    const/16 v9, 0x1e0

    if-le v7, v9, :cond_18

    .line 1615
    :cond_17
    const/high16 v9, 0x41a00000    # 20.0f

    invoke-virtual {v6, v9}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 1616
    const/high16 v5, 0x41f00000    # 30.0f

    .line 1617
    const/high16 v4, 0x41f00000    # 30.0f

    .line 1618
    iget-object v9, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqView;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    const-wide v10, 0x3feab851eb851eb8L    # 0.835

    # setter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mRatePortlinePosY:D
    invoke-static {v9, v10, v11}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$3702(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;D)D

    .line 1619
    iget-object v9, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqView;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    const-wide v10, 0x3fe9eb851eb851ecL    # 0.81

    # setter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mRateLandlinePosY:D
    invoke-static {v9, v10, v11}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$3802(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;D)D

    .line 1620
    if-nez v1, :cond_4

    .line 1621
    const/high16 v8, -0x40800000    # -1.0f

    goto/16 :goto_1

    .line 1623
    :cond_18
    if-eqz v1, :cond_19

    const/16 v9, 0xf0

    if-gt v7, v9, :cond_1a

    :cond_19
    if-nez v1, :cond_1b

    const/16 v9, 0x140

    if-le v7, v9, :cond_1b

    .line 1624
    :cond_1a
    const/high16 v9, 0x41600000    # 14.0f

    invoke-virtual {v6, v9}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 1625
    iget-object v9, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqView;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    const-wide v10, 0x3fe9eb851eb851ecL    # 0.81

    # setter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mRatePortlinePosY:D
    invoke-static {v9, v10, v11}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$3702(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;D)D

    .line 1626
    iget-object v9, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqView;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    const-wide v10, 0x3fe5c28f5c28f5c3L    # 0.68

    # setter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mRateLandlinePosY:D
    invoke-static {v9, v10, v11}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$3802(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;D)D

    goto/16 :goto_1

    .line 1628
    :cond_1b
    const/high16 v9, 0x41300000    # 11.0f

    invoke-virtual {v6, v9}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 1629
    const/high16 v8, 0x40000000    # 2.0f

    .line 1630
    iget-object v9, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqView;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    const-wide v10, 0x3fe9eb851eb851ecL    # 0.81

    # setter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mRatePortlinePosY:D
    invoke-static {v9, v10, v11}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$3702(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;D)D

    .line 1631
    iget-object v9, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqView;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    const-wide v10, 0x3fe5c28f5c28f5c3L    # 0.68

    # setter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mRateLandlinePosY:D
    invoke-static {v9, v10, v11}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$3802(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;D)D

    goto/16 :goto_1

    .line 1639
    .restart local v0    # "i":I
    :cond_1c
    iget-object v9, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqView;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSeekBarHeight:I
    invoke-static {v9}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$4000(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)I

    move-result v9

    mul-int/2addr v9, v0

    div-int/lit8 v9, v9, 0xa

    int-to-double v10, v9

    iget-object v9, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqView;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mRateLandlinePosY:D
    invoke-static {v9}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$3800(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)D

    move-result-wide v12

    mul-double/2addr v10, v12

    float-to-double v12, v4

    add-double/2addr v10, v12

    double-to-float v2, v10

    .restart local v2    # "linePosY":F
    goto/16 :goto_3

    .line 1644
    :sswitch_0
    const-string v9, " 10dB"

    invoke-virtual {v6}, Landroid/graphics/Paint;->ascent()F

    move-result v10

    const/high16 v11, 0x40000000    # 2.0f

    div-float/2addr v10, v11

    sub-float v10, v2, v10

    invoke-virtual {p1, v9, v8, v10, v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto/16 :goto_4

    .line 1647
    :sswitch_1
    const-string v9, "  0dB"

    int-to-float v10, v3

    add-float/2addr v10, v8

    invoke-virtual {v6}, Landroid/graphics/Paint;->ascent()F

    move-result v11

    const/high16 v12, 0x40000000    # 2.0f

    div-float/2addr v11, v12

    sub-float v11, v2, v11

    invoke-virtual {p1, v9, v10, v11, v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto/16 :goto_4

    .line 1651
    :sswitch_2
    const-string v9, "-10dB"

    const/high16 v10, 0x3f800000    # 1.0f

    sub-float v10, v8, v10

    invoke-virtual {v6}, Landroid/graphics/Paint;->ascent()F

    move-result v11

    const/high16 v12, 0x40000000    # 2.0f

    div-float/2addr v11, v12

    sub-float v11, v2, v11

    invoke-virtual {p1, v9, v10, v11, v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto/16 :goto_4

    .line 1657
    .end local v2    # "linePosY":F
    :cond_1d
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 1658
    return-void

    .line 1642
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x5 -> :sswitch_1
        0xa -> :sswitch_2
    .end sparse-switch
.end method
