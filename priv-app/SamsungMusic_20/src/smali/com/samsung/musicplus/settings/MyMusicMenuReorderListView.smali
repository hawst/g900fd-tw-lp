.class public Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;
.super Lcom/samsung/musicplus/widget/list/MusicListView;
.source "MyMusicMenuReorderListView.java"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;
.implements Lcom/samsung/musicplus/settings/PlayerSettingPreference;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;,
        Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$ViewItem;,
        Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$RemoveListener;,
        Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$DropListener;,
        Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$DragListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "MenuReorder"


# instance fields
.field private mActionBarHeight:I

.field protected mAdapter:Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;

.field private mContext:Landroid/content/Context;

.field private mDragBitmap:Landroid/graphics/Bitmap;

.field private mDragListener:Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$DragListener;

.field private mDragPointY:I

.field private mDragPos:I

.field private mDragView:Landroid/widget/ImageView;

.field protected mDropListener:Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$DropListener;

.field private mHeight:I

.field private mItemHeightExpanded:I

.field private mItemHeightHalf:I

.field private mItemHeightNormal:I

.field private mLowerBound:I

.field private final mOnClickListener:Landroid/widget/AdapterView$OnItemClickListener;

.field private mPreference:Landroid/content/SharedPreferences;

.field protected mRemoveListener:Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$RemoveListener;

.field private mSrcDragPos:I

.field private mTabMenuReorder:Z

.field private mTempRect:Landroid/graphics/Rect;

.field private mTouchSlop:I

.field private mTrashcan:Landroid/graphics/drawable/Drawable;

.field private mUI:Landroid/os/Handler;

.field private mUpperBound:I

.field private mWindowManager:Landroid/view/WindowManager;

.field private mWindowParams:Landroid/view/WindowManager$LayoutParams;


# direct methods
.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "enableTabMenu"    # Z

    .prologue
    .line 110
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/widget/list/MusicListView;-><init>(Landroid/content/Context;)V

    .line 105
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mTabMenuReorder:Z

    .line 478
    new-instance v0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$1;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$1;-><init>(Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;)V

    iput-object v0, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mUI:Landroid/os/Handler;

    .line 563
    new-instance v0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$2;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$2;-><init>(Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;)V

    iput-object v0, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mDragListener:Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$DragListener;

    .line 570
    new-instance v0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$3;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$3;-><init>(Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;)V

    iput-object v0, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mDropListener:Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$DropListener;

    .line 588
    new-instance v0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$4;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$4;-><init>(Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;)V

    iput-object v0, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mRemoveListener:Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$RemoveListener;

    .line 882
    new-instance v0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$5;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$5;-><init>(Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;)V

    iput-object v0, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mOnClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 111
    invoke-direct {p0, p1, p2}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->initialize(Landroid/content/Context;Z)V

    .line 112
    return-void
.end method

.method static synthetic access$200(Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mUI:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$700(Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;

    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mTabMenuReorder:Z

    return v0
.end method

.method static synthetic access$800(Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;)Landroid/content/SharedPreferences;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mPreference:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic access$900(Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;Ljava/lang/String;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Z

    .prologue
    .line 59
    invoke-direct {p0, p1, p2}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->savePreference(Ljava/lang/String;Z)V

    return-void
.end method

.method private adjustScrollBounds(I)V
    .locals 1
    .param p1, "y"    # I

    .prologue
    .line 304
    iget v0, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mHeight:I

    div-int/lit8 v0, v0, 0x3

    if-lt p1, v0, :cond_0

    .line 305
    iget v0, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mHeight:I

    div-int/lit8 v0, v0, 0x3

    iput v0, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mUpperBound:I

    .line 307
    :cond_0
    iget v0, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mHeight:I

    mul-int/lit8 v0, v0, 0x2

    div-int/lit8 v0, v0, 0x3

    if-gt p1, v0, :cond_1

    .line 308
    iget v0, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mHeight:I

    mul-int/lit8 v0, v0, 0x2

    div-int/lit8 v0, v0, 0x3

    iput v0, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mLowerBound:I

    .line 310
    :cond_1
    return-void
.end method

.method private checkActiveMenu(I)Z
    .locals 6
    .param p1, "pos"    # I

    .prologue
    .line 207
    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v3

    check-cast v3, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;

    invoke-virtual {v3}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->getArrayList()Ljava/util/ArrayList;

    move-result-object v0

    .line 210
    .local v0, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$ViewItem;>;"
    iget-boolean v3, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mTabMenuReorder:Z

    if-eqz v3, :cond_0

    .line 211
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$ViewItem;

    iget v3, v3, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$ViewItem;->type:I

    invoke-static {v3}, Lcom/samsung/musicplus/util/ListUtils;->getDefaultTabPrefValue(I)Ljava/lang/String;

    move-result-object v2

    .line 215
    .local v2, "type":Ljava/lang/String;
    :goto_0
    iget-object v3, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mPreference:Landroid/content/SharedPreferences;

    const/4 v4, 0x1

    invoke-interface {v3, v2, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 217
    .local v1, "pref":Z
    const-string v3, "MenuReorder"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mList : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", pref value : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 219
    return v1

    .line 213
    .end local v1    # "pref":Z
    .end local v2    # "type":Ljava/lang/String;
    :cond_0
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$ViewItem;

    iget v3, v3, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$ViewItem;->type:I

    int-to-long v4, v3

    invoke-static {v4, v5}, Lcom/samsung/musicplus/util/ListUtils;->getDefaultPlayListPrefValue(J)Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "type":Ljava/lang/String;
    goto :goto_0
.end method

.method private doExpansion()V
    .locals 11

    .prologue
    .line 352
    iget v8, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mDragPos:I

    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->getFirstVisiblePosition()I

    move-result v9

    sub-int v0, v8, v9

    .line 353
    .local v0, "childnum":I
    iget v8, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mDragPos:I

    iget v9, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mSrcDragPos:I

    if-le v8, v9, :cond_0

    .line 354
    add-int/lit8 v0, v0, 0x1

    .line 356
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->getHeaderViewsCount()I

    move-result v4

    .line 357
    .local v4, "numheaders":I
    iget v8, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mSrcDragPos:I

    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->getFirstVisiblePosition()I

    move-result v9

    sub-int/2addr v8, v9

    invoke-virtual {p0, v8}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 358
    .local v1, "first":Landroid/view/View;
    const-string v8, "MenuReorder"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "doExpansion childnum:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " numheaders:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " first:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " mDragPos : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mDragPos:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " getFirstVisiblePosition() : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->getFirstVisiblePosition()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 362
    const/4 v3, 0x0

    .line 363
    .local v3, "i":I
    :goto_0
    invoke-virtual {p0, v3}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    .line 364
    .local v7, "vv":Landroid/view/View;
    if-nez v7, :cond_1

    .line 405
    return-void

    .line 368
    :cond_1
    iget v2, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mItemHeightNormal:I

    .line 369
    .local v2, "height":I
    const/4 v6, 0x0

    .line 370
    .local v6, "visibility":I
    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->getLastVisiblePosition()I

    move-result v8

    if-ne v3, v8, :cond_3

    iget v8, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mSrcDragPos:I

    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->getLastVisiblePosition()I

    move-result v9

    if-ne v8, v9, :cond_3

    .line 372
    const/4 v2, 0x1

    .line 373
    const/4 v6, 0x4

    .line 400
    :cond_2
    :goto_1
    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    .line 401
    .local v5, "params":Landroid/view/ViewGroup$LayoutParams;
    iput v2, v5, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 402
    invoke-virtual {v7, v5}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 403
    invoke-virtual {v7, v6}, Landroid/view/View;->setVisibility(I)V

    .line 362
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 374
    .end local v5    # "params":Landroid/view/ViewGroup$LayoutParams;
    :cond_3
    iget v8, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mDragPos:I

    if-ge v8, v4, :cond_5

    if-ne v3, v4, :cond_5

    .line 377
    invoke-virtual {v7, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    .line 378
    const/4 v6, 0x4

    goto :goto_1

    .line 380
    :cond_4
    iget v2, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mItemHeightExpanded:I

    goto :goto_1

    .line 382
    :cond_5
    invoke-virtual {v7, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_8

    .line 384
    iget v8, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mDragPos:I

    iget v9, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mSrcDragPos:I

    if-eq v8, v9, :cond_6

    invoke-virtual {p0, v7}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->getPositionForView(Landroid/view/View;)I

    move-result v8

    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->getCount()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    if-ne v8, v9, :cond_7

    .line 386
    :cond_6
    const/4 v6, 0x4

    goto :goto_1

    .line 392
    :cond_7
    const/4 v2, 0x1

    .line 393
    const/4 v6, 0x4

    goto :goto_1

    .line 395
    :cond_8
    if-ne v3, v0, :cond_2

    .line 396
    iget v8, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mDragPos:I

    if-lt v8, v4, :cond_2

    iget v8, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mDragPos:I

    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->getCount()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    if-ge v8, v9, :cond_2

    .line 397
    iget v2, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mItemHeightExpanded:I

    goto :goto_1
.end method

.method private dragView(II)V
    .locals 5
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    const/4 v4, 0x0

    .line 526
    const-string v1, "MenuReorder"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DragView "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 528
    iget-object v1, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    iput v4, v1, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 529
    iget-object v1, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    iget v2, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mItemHeightHalf:I

    sub-int v2, p2, v2

    iget v3, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mActionBarHeight:I

    add-int/2addr v2, v3

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 530
    iget-object v1, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mWindowManager:Landroid/view/WindowManager;

    iget-object v2, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mDragView:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v1, v2, v3}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 532
    iget-object v1, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mTrashcan:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_0

    .line 533
    iget-object v1, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mDragView:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getWidth()I

    move-result v0

    .line 534
    .local v0, "width":I
    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->getHeight()I

    move-result v1

    mul-int/lit8 v1, v1, 0x3

    div-int/lit8 v1, v1, 0x4

    if-le p2, v1, :cond_1

    .line 535
    iget-object v1, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mTrashcan:Landroid/graphics/drawable/Drawable;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->setLevel(I)Z

    .line 542
    .end local v0    # "width":I
    :cond_0
    :goto_0
    return-void

    .line 536
    .restart local v0    # "width":I
    :cond_1
    if-lez v0, :cond_2

    div-int/lit8 v1, v0, 0x4

    if-le p1, v1, :cond_2

    .line 537
    iget-object v1, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mTrashcan:Landroid/graphics/drawable/Drawable;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->setLevel(I)Z

    goto :goto_0

    .line 539
    :cond_2
    iget-object v1, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mTrashcan:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v4}, Landroid/graphics/drawable/Drawable;->setLevel(I)Z

    goto :goto_0
.end method

.method private getItemForPosition(II)I
    .locals 4
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 289
    iget v2, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mDragPointY:I

    sub-int v2, p2, v2

    iget v3, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mItemHeightNormal:I

    div-int/lit8 v3, v3, 0x3

    sub-int v0, v2, v3

    .line 290
    .local v0, "adjustedy":I
    invoke-direct {p0, p1, v0}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->myPointToPosition(II)I

    move-result v1

    .line 291
    .local v1, "pos":I
    if-ltz v1, :cond_1

    .line 292
    iget v2, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mSrcDragPos:I

    if-gt v1, v2, :cond_0

    .line 293
    add-int/lit8 v1, v1, 0x1

    .line 300
    :cond_0
    :goto_0
    return v1

    .line 295
    :cond_1
    if-gez v0, :cond_0

    .line 298
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private initAdapter(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 133
    new-instance v0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;

    invoke-direct {v0, p0, p1}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;-><init>(Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mAdapter:Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;

    .line 134
    iget-object v0, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mAdapter:Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 135
    return-void
.end method

.method private initialize(Landroid/content/Context;Z)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "enableTabMenu"    # Z

    .prologue
    .line 115
    iput-object p1, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mContext:Landroid/content/Context;

    .line 116
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    iput v1, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mTouchSlop:I

    .line 117
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    iput-object v1, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mTempRect:Landroid/graphics/Rect;

    .line 118
    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 119
    .local v0, "res":Landroid/content/res/Resources;
    const v1, 0x7f0c0139

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mItemHeightNormal:I

    .line 120
    iget v1, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mItemHeightNormal:I

    div-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mItemHeightHalf:I

    .line 121
    iget v1, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mItemHeightNormal:I

    mul-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mItemHeightExpanded:I

    .line 122
    iget-object v1, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mContext:Landroid/content/Context;

    const-string v2, "music_player_pref"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mPreference:Landroid/content/SharedPreferences;

    .line 123
    iput-boolean p2, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mTabMenuReorder:Z

    .line 125
    iget-object v1, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mDropListener:Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$DropListener;

    invoke-direct {p0, v1}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->setDropListener(Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$DropListener;)V

    .line 126
    iget-object v1, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mRemoveListener:Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$RemoveListener;

    invoke-direct {p0, v1}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->setRemoveListener(Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$RemoveListener;)V

    .line 128
    iget-object v1, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mContext:Landroid/content/Context;

    invoke-direct {p0, v1}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->initAdapter(Landroid/content/Context;)V

    .line 129
    iget-object v1, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mOnClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 130
    return-void
.end method

.method private myPointToPosition(II)I
    .locals 5
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 268
    if-gez p2, :cond_0

    .line 271
    iget v4, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mItemHeightNormal:I

    add-int/2addr v4, p2

    invoke-direct {p0, p1, v4}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->myPointToPosition(II)I

    move-result v3

    .line 272
    .local v3, "pos":I
    if-lez v3, :cond_0

    .line 273
    add-int/lit8 v4, v3, -0x1

    .line 285
    .end local v3    # "pos":I
    :goto_0
    return v4

    .line 277
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->getChildCount()I

    move-result v1

    .line 278
    .local v1, "count":I
    add-int/lit8 v2, v1, -0x1

    .local v2, "i":I
    :goto_1
    if-ltz v2, :cond_2

    .line 279
    invoke-virtual {p0, v2}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 280
    .local v0, "child":Landroid/view/View;
    iget-object v4, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mTempRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v4}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 281
    iget-object v4, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mTempRect:Landroid/graphics/Rect;

    invoke-virtual {v4, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 282
    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->getFirstVisiblePosition()I

    move-result v4

    add-int/2addr v4, v2

    goto :goto_0

    .line 278
    :cond_1
    add-int/lit8 v2, v2, -0x1

    goto :goto_1

    .line 285
    .end local v0    # "child":Landroid/view/View;
    :cond_2
    const/4 v4, -0x1

    goto :goto_0
.end method

.method private savePreference(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 641
    const-string v1, "MenuReorder"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "savePreference key : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " value : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 642
    iget-object v1, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mPreference:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 643
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 644
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v1

    if-nez v1, :cond_0

    .line 645
    const-string v1, "MenuReorder"

    const-string v2, "Failed to write file for string type!"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 647
    :cond_0
    return-void
.end method

.method private savePreference(Ljava/lang/String;Z)V
    .locals 4
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Z

    .prologue
    .line 632
    const-string v1, "MenuReorder"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "savePreference key : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " value : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 633
    iget-object v1, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mPreference:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 634
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 635
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v1

    if-nez v1, :cond_0

    .line 636
    const-string v1, "MenuReorder"

    const-string v2, "Failed to write file for boolean type!"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 638
    :cond_0
    return-void
.end method

.method private setDropListener(Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$DropListener;)V
    .locals 0
    .param p1, "l"    # Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$DropListener;

    .prologue
    .line 607
    iput-object p1, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mDropListener:Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$DropListener;

    .line 608
    return-void
.end method

.method private setRemoveListener(Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$RemoveListener;)V
    .locals 0
    .param p1, "l"    # Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$RemoveListener;

    .prologue
    .line 616
    iput-object p1, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mRemoveListener:Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$RemoveListener;

    .line 617
    return-void
.end method

.method private startDragging(Landroid/graphics/Bitmap;II)V
    .locals 7
    .param p1, "bm"    # Landroid/graphics/Bitmap;
    .param p2, "x"    # I
    .param p3, "y"    # I

    .prologue
    const/4 v6, -0x2

    const/4 v5, 0x0

    .line 491
    const-string v2, "MenuReorder"

    const-string v3, "StartDragging"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 492
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->stopDragging()V

    .line 494
    new-instance v2, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v2}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    iput-object v2, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    .line 495
    iget-object v2, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    const/16 v3, 0x33

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 496
    iget-object v2, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    iput v5, v2, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 497
    iget-object v2, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    iget v3, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mItemHeightHalf:I

    sub-int v3, p3, v3

    iget v4, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mActionBarHeight:I

    add-int/2addr v3, v4

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 499
    iget-object v2, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    iput v6, v2, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 500
    iget-object v2, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    iput v6, v2, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 501
    iget-object v2, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    const/16 v3, 0x398

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 506
    iget-object v2, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    const/4 v3, -0x3

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->format:I

    .line 507
    iget-object v2, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    iput v5, v2, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    .line 509
    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 510
    .local v0, "context":Landroid/content/Context;
    new-instance v1, Landroid/widget/ImageView;

    invoke-direct {v1, v0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 515
    .local v1, "v":Landroid/widget/ImageView;
    invoke-virtual {v1, v5, v5, v5, v5}, Landroid/widget/ImageView;->setPadding(IIII)V

    .line 516
    invoke-virtual {v1, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 517
    const/16 v2, 0xaa

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageAlpha(I)V

    .line 518
    iput-object p1, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mDragBitmap:Landroid/graphics/Bitmap;

    .line 520
    const-string v2, "window"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/WindowManager;

    iput-object v2, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mWindowManager:Landroid/view/WindowManager;

    .line 521
    iget-object v2, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mWindowManager:Landroid/view/WindowManager;

    iget-object v3, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v2, v1, v3}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 522
    iput-object v1, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mDragView:Landroid/widget/ImageView;

    .line 523
    return-void
.end method

.method private stopDragging()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 545
    iget-object v1, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mUI:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 546
    iget-object v1, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mDragView:Landroid/widget/ImageView;

    if-eqz v1, :cond_0

    .line 547
    iget-object v1, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mDragView:Landroid/widget/ImageView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 548
    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "window"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 550
    .local v0, "wm":Landroid/view/WindowManager;
    iget-object v1, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mDragView:Landroid/widget/ImageView;

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 551
    iget-object v1, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mDragView:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 552
    iput-object v3, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mDragView:Landroid/widget/ImageView;

    .line 554
    .end local v0    # "wm":Landroid/view/WindowManager;
    :cond_0
    iget-object v1, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mDragBitmap:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_1

    .line 555
    iget-object v1, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mDragBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 556
    iput-object v3, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mDragBitmap:Landroid/graphics/Bitmap;

    .line 558
    :cond_1
    iget-object v1, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mTrashcan:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_2

    .line 559
    iget-object v1, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mTrashcan:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v4}, Landroid/graphics/drawable/Drawable;->setLevel(I)Z

    .line 561
    :cond_2
    return-void
.end method

.method private unExpandViews(Z)V
    .locals 7
    .param p1, "deletion"    # Z

    .prologue
    const/4 v6, 0x0

    .line 316
    const/4 v0, 0x0

    .line 317
    .local v0, "i":I
    :goto_0
    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 318
    .local v3, "v":Landroid/view/View;
    if-nez v3, :cond_1

    .line 319
    if-eqz p1, :cond_0

    .line 320
    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->getFirstVisiblePosition()I

    move-result v2

    .line 321
    .local v2, "position":I
    invoke-virtual {p0, v6}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    move-result v4

    .line 322
    .local v4, "y":I
    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 323
    invoke-virtual {p0, v2, v4}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->setSelectionFromTop(II)V

    .line 326
    .end local v2    # "position":I
    .end local v4    # "y":I
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->layoutChildren()V

    .line 327
    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->getChildAt(I)Landroid/view/View;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 331
    :goto_1
    if-nez v3, :cond_1

    .line 332
    return-void

    .line 335
    :cond_1
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 336
    .local v1, "params":Landroid/view/ViewGroup$LayoutParams;
    iget v5, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mItemHeightNormal:I

    iput v5, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 337
    invoke-virtual {v3, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 338
    invoke-virtual {v3, v6}, Landroid/view/View;->setVisibility(I)V

    .line 316
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 329
    .end local v1    # "params":Landroid/view/ViewGroup$LayoutParams;
    :catch_0
    move-exception v5

    goto :goto_1
.end method


# virtual methods
.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 12
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v8, 0x1

    .line 224
    const-string v9, "MenuReorder"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "onInterceptTouchEvent "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 227
    .local v0, "a":Landroid/app/Activity;
    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    .line 228
    .local v1, "ab":Landroid/app/ActionBar;
    invoke-virtual {v1}, Landroid/app/ActionBar;->getHeight()I

    move-result v9

    iput v9, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mActionBarHeight:I

    .line 230
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v9

    packed-switch v9, :pswitch_data_0

    .line 264
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/list/MusicListView;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v8

    :goto_1
    return v8

    .line 232
    :pswitch_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v9

    float-to-int v6, v9

    .line 233
    .local v6, "x":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v9

    float-to-int v7, v9

    .line 234
    .local v7, "y":I
    invoke-virtual {p0, v6, v7}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->pointToPosition(II)I

    move-result v4

    .line 235
    .local v4, "itemnum":I
    const/4 v9, -0x1

    if-eq v4, v9, :cond_0

    .line 238
    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->getFirstVisiblePosition()I

    move-result v9

    sub-int v9, v4, v9

    invoke-virtual {p0, v9}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    .line 239
    .local v3, "item":Landroid/view/ViewGroup;
    invoke-virtual {v3}, Landroid/view/ViewGroup;->getTop()I

    move-result v9

    sub-int v9, v7, v9

    iput v9, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mDragPointY:I

    .line 243
    const v9, 0x7f0d0137

    invoke-virtual {v3, v9}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v9

    invoke-virtual {v9}, Landroid/view/View;->getRight()I

    move-result v9

    if-ge v6, v9, :cond_1

    .line 244
    invoke-virtual {v3, v8}, Landroid/view/ViewGroup;->setDrawingCacheEnabled(Z)V

    .line 245
    invoke-virtual {v3}, Landroid/view/ViewGroup;->destroyDrawingCache()V

    .line 250
    invoke-virtual {v3}, Landroid/view/ViewGroup;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v9

    invoke-static {v9}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 251
    .local v2, "bitmap":Landroid/graphics/Bitmap;
    invoke-direct {p0, v2, v6, v7}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->startDragging(Landroid/graphics/Bitmap;II)V

    .line 252
    iput v4, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mDragPos:I

    .line 253
    iget v9, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mDragPos:I

    iput v9, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mSrcDragPos:I

    .line 254
    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->getHeight()I

    move-result v9

    iput v9, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mHeight:I

    .line 255
    iget v5, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mTouchSlop:I

    .line 256
    .local v5, "touchSlop":I
    sub-int v9, v7, v5

    iget v10, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mHeight:I

    div-int/lit8 v10, v10, 0x3

    invoke-static {v9, v10}, Ljava/lang/Math;->min(II)I

    move-result v9

    iput v9, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mUpperBound:I

    .line 257
    add-int v9, v7, v5

    iget v10, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mHeight:I

    mul-int/lit8 v10, v10, 0x2

    div-int/lit8 v10, v10, 0x3

    invoke-static {v9, v10}, Ljava/lang/Math;->max(II)I

    move-result v9

    iput v9, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mLowerBound:I

    goto :goto_1

    .line 260
    .end local v2    # "bitmap":Landroid/graphics/Bitmap;
    .end local v5    # "touchSlop":I
    :cond_1
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->stopDragging()V

    goto :goto_0

    .line 230
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 0
    .param p1, "view"    # Landroid/widget/AbsListView;
    .param p2, "firstVisibleItem"    # I
    .param p3, "visibleItemCount"    # I
    .param p4, "totalItemCount"    # I

    .prologue
    .line 899
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 1
    .param p1, "view"    # Landroid/widget/AbsListView;
    .param p2, "scrollState"    # I

    .prologue
    .line 903
    iget-object v0, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mAdapter:Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;

    instance-of v0, v0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;

    if-eqz v0, :cond_0

    .line 904
    iget-object v0, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mAdapter:Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;

    invoke-virtual {v0, p2}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->setScrollSate(I)V

    .line 906
    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 11
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v6, 0x1

    const/4 v10, 0x0

    .line 409
    const-string v7, "MenuReorder"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "onTouchEvent 1"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 411
    iget-object v7, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mDragListener:Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$DragListener;

    if-nez v7, :cond_0

    iget-object v7, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mDropListener:Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$DropListener;

    if-eqz v7, :cond_b

    :cond_0
    iget-object v7, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mDragView:Landroid/widget/ImageView;

    if-eqz v7, :cond_b

    .line 412
    const-string v7, "MenuReorder"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "onTouchEvent 2"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 414
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 415
    .local v0, "action":I
    packed-switch v0, :pswitch_data_0

    .line 475
    .end local v0    # "action":I
    :cond_1
    :goto_0
    return v6

    .line 418
    .restart local v0    # "action":I
    :pswitch_0
    iget-object v7, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mDragView:Landroid/widget/ImageView;

    iget-object v8, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mTempRect:Landroid/graphics/Rect;

    invoke-virtual {v7, v8}, Landroid/widget/ImageView;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 419
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->stopDragging()V

    .line 420
    iget-object v7, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mDropListener:Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$DropListener;

    if-eqz v7, :cond_2

    iget v7, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mDragPos:I

    if-ltz v7, :cond_2

    iget v7, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mDragPos:I

    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->getCount()I

    move-result v8

    if-ge v7, v8, :cond_2

    .line 421
    iget-object v7, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mDropListener:Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$DropListener;

    iget v8, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mSrcDragPos:I

    iget v9, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mDragPos:I

    invoke-interface {v7, v8, v9}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$DropListener;->drop(II)V

    .line 423
    :cond_2
    invoke-direct {p0, v6}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->unExpandViews(Z)V

    goto :goto_0

    .line 427
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v7

    float-to-int v4, v7

    .line 428
    .local v4, "x":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v7

    float-to-int v5, v7

    .line 429
    .local v5, "y":I
    invoke-direct {p0, v4, v5}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->dragView(II)V

    .line 431
    invoke-direct {p0, v4, v5}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->getItemForPosition(II)I

    move-result v1

    .line 432
    .local v1, "itemnum":I
    const-string v7, "MenuReorder"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "X : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " Y : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "ItemNum : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 434
    if-ltz v1, :cond_1

    .line 435
    if-eqz v0, :cond_3

    iget v7, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mDragPos:I

    if-eq v1, v7, :cond_5

    .line 436
    :cond_3
    iget-object v7, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mDragListener:Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$DragListener;

    if-eqz v7, :cond_4

    .line 437
    iget-object v7, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mDragListener:Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$DragListener;

    iget v8, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mDragPos:I

    invoke-interface {v7, v8, v1}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$DragListener;->drag(II)V

    .line 439
    :cond_4
    iput v1, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mDragPos:I

    .line 440
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->doExpansion()V

    .line 442
    :cond_5
    const/4 v3, 0x0

    .line 443
    .local v3, "speed":I
    invoke-direct {p0, v5}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->adjustScrollBounds(I)V

    .line 444
    iget v7, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mLowerBound:I

    if-le v5, v7, :cond_9

    .line 446
    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->getLastVisiblePosition()I

    move-result v7

    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->getCount()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    if-ge v7, v8, :cond_8

    .line 447
    iget v7, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mHeight:I

    iget v8, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mLowerBound:I

    add-int/2addr v7, v8

    div-int/lit8 v7, v7, 0x2

    if-le v5, v7, :cond_7

    const/16 v3, 0x10

    .line 464
    :cond_6
    :goto_1
    if-eqz v3, :cond_1

    .line 465
    iget-object v7, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mUI:Landroid/os/Handler;

    invoke-virtual {v7, v10}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    .line 466
    .local v2, "msg":Landroid/os/Message;
    iput v3, v2, Landroid/os/Message;->arg1:I

    .line 467
    iget-object v7, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mUI:Landroid/os/Handler;

    invoke-virtual {v7, v10}, Landroid/os/Handler;->removeMessages(I)V

    .line 468
    iget-object v7, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mUI:Landroid/os/Handler;

    invoke-virtual {v7, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto/16 :goto_0

    .line 447
    .end local v2    # "msg":Landroid/os/Message;
    :cond_7
    const/4 v3, 0x4

    goto :goto_1

    .line 449
    :cond_8
    const/4 v3, 0x1

    goto :goto_1

    .line 451
    :cond_9
    iget v7, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mUpperBound:I

    if-ge v5, v7, :cond_6

    .line 453
    iget v7, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mUpperBound:I

    div-int/lit8 v7, v7, 0x2

    if-ge v5, v7, :cond_a

    const/16 v3, -0x10

    .line 454
    :goto_2
    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->getFirstVisiblePosition()I

    move-result v7

    if-nez v7, :cond_6

    invoke-virtual {p0, v10}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    invoke-virtual {v7}, Landroid/view/View;->getTop()I

    move-result v7

    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->getPaddingTop()I

    move-result v8

    if-lt v7, v8, :cond_6

    .line 461
    const/4 v3, 0x0

    goto :goto_1

    .line 453
    :cond_a
    const/4 v3, -0x4

    goto :goto_2

    .line 475
    .end local v0    # "action":I
    .end local v1    # "itemnum":I
    .end local v3    # "speed":I
    .end local v4    # "x":I
    .end local v5    # "y":I
    :cond_b
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/list/MusicListView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v6

    goto/16 :goto_0

    .line 415
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected removePlaylistItem(I)V
    .locals 2
    .param p1, "which"    # I

    .prologue
    .line 596
    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->getFirstVisiblePosition()I

    move-result v1

    sub-int v1, p1, v1

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 598
    .local v0, "v":Landroid/view/View;
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 599
    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->invalidateViews()V

    .line 600
    return-void
.end method

.method public saveActiveMenu()V
    .locals 14

    .prologue
    .line 159
    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->getCount()I

    move-result v0

    .line 160
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_3

    .line 161
    iget-object v9, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mAdapter:Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;

    # getter for: Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->mArray:Ljava/util/ArrayList;
    invoke-static {v9}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->access$000(Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;)Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$ViewItem;

    iget v8, v9, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$ViewItem;->type:I

    .line 162
    .local v8, "type":I
    iget-boolean v9, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mTabMenuReorder:Z

    if-eqz v9, :cond_1

    .line 163
    invoke-static {v8}, Lcom/samsung/musicplus/util/ListUtils;->getDefaultTabPrefValue(I)Ljava/lang/String;

    move-result-object v3

    .line 168
    .local v3, "pref":Ljava/lang/String;
    :goto_1
    invoke-static {v8}, Lcom/samsung/musicplus/util/ListUtils;->isFixedList(I)Z

    move-result v9

    if-nez v9, :cond_0

    int-to-long v10, v8

    const-wide/16 v12, -0xb

    cmp-long v9, v10, v12

    if-nez v9, :cond_2

    .line 160
    :cond_0
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 165
    .end local v3    # "pref":Ljava/lang/String;
    :cond_1
    int-to-long v10, v8

    invoke-static {v10, v11}, Lcom/samsung/musicplus/util/ListUtils;->getDefaultPlayListPrefValue(J)Ljava/lang/String;

    move-result-object v3

    .restart local v3    # "pref":Ljava/lang/String;
    goto :goto_1

    .line 172
    :cond_2
    iget-object v9, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mAdapter:Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;

    # getter for: Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->mActiveMenu:Ljava/util/HashMap;
    invoke-static {v9}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->access$100(Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;)Ljava/util/HashMap;

    move-result-object v9

    invoke-virtual {v9, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/Boolean;

    invoke-virtual {v9}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    invoke-direct {p0, v3, v9}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->savePreference(Ljava/lang/String;Z)V

    goto :goto_2

    .line 177
    .end local v3    # "pref":Ljava/lang/String;
    .end local v8    # "type":I
    :cond_3
    iget-boolean v9, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mTabMenuReorder:Z

    if-eqz v9, :cond_6

    .line 178
    iget-object v9, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mPreference:Landroid/content/SharedPreferences;

    const-string v10, "tab_menu_list_order"

    invoke-static {}, Lcom/samsung/musicplus/util/ListUtils;->getDefaultTabOrder()Ljava/lang/String;

    move-result-object v11

    invoke-interface {v9, v10, v11}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 184
    .local v6, "setting":Ljava/lang/String;
    :goto_3
    new-instance v7, Ljava/util/StringTokenizer;

    const-string v9, "|"

    invoke-direct {v7, v6, v9}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    .local v7, "strToken":Ljava/util/StringTokenizer;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 188
    .local v5, "sb":Ljava/lang/StringBuilder;
    const/4 v2, 0x0

    .line 189
    .local v2, "position":I
    :goto_4
    invoke-virtual {v7}, Ljava/util/StringTokenizer;->hasMoreElements()Z

    move-result v9

    if-eqz v9, :cond_7

    .line 190
    invoke-virtual {v7}, Ljava/util/StringTokenizer;->nextElement()Ljava/lang/Object;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 191
    .local v4, "savedList":I
    invoke-static {v4}, Lcom/samsung/musicplus/util/ListUtils;->isFixedList(I)Z

    move-result v9

    if-nez v9, :cond_4

    invoke-direct {p0, v2}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->checkActiveMenu(I)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 192
    :cond_4
    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 193
    invoke-virtual {v7}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v9

    if-eqz v9, :cond_5

    .line 194
    const-string v9, "|"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 197
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 181
    .end local v2    # "position":I
    .end local v4    # "savedList":I
    .end local v5    # "sb":Ljava/lang/StringBuilder;
    .end local v6    # "setting":Ljava/lang/String;
    .end local v7    # "strToken":Ljava/util/StringTokenizer;
    :cond_6
    iget-object v9, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mPreference:Landroid/content/SharedPreferences;

    const-string v10, "playlist_menu_list_order"

    const-string v11, "-11|-12|-13|-14"

    invoke-interface {v9, v10, v11}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .restart local v6    # "setting":Ljava/lang/String;
    goto :goto_3

    .line 199
    .restart local v2    # "position":I
    .restart local v5    # "sb":Ljava/lang/StringBuilder;
    .restart local v7    # "strToken":Ljava/util/StringTokenizer;
    :cond_7
    iget-boolean v9, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mTabMenuReorder:Z

    if-eqz v9, :cond_8

    .line 200
    const-string v9, "tab_menu_list"

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {p0, v9, v10}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->savePreference(Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    :goto_5
    return-void

    .line 202
    :cond_8
    const-string v9, "playlist_menu_list"

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {p0, v9, v10}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->savePreference(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_5
.end method

.method public saveReorderMenu()V
    .locals 6

    .prologue
    .line 138
    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v4

    check-cast v4, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;

    invoke-virtual {v4}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->getArrayList()Ljava/util/ArrayList;

    move-result-object v0

    .line 140
    .local v0, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$ViewItem;>;"
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 141
    .local v3, "temp":Ljava/lang/StringBuilder;
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 142
    .local v1, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_0

    .line 143
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$ViewItem;

    iget v4, v4, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$ViewItem;->type:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 144
    const-string v4, "|"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 142
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 147
    :cond_0
    iget-boolean v4, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mTabMenuReorder:Z

    if-eqz v4, :cond_1

    .line 148
    const-string v4, "tab_menu_list_order"

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v4, v5}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->savePreference(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    const/4 v4, 0x1

    sput-boolean v4, Lcom/samsung/musicplus/MusicMainActivity;->sNeedUpdateTab:Z

    .line 153
    :goto_1
    return-void

    .line 151
    :cond_1
    const-string v4, "playlist_menu_list_order"

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0, v4, v5}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->savePreference(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public toggleCheckState([I)V
    .locals 1
    .param p1, "items"    # [I

    .prologue
    .line 650
    iget-object v0, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mAdapter:Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->setToggleCheck([I)V

    .line 651
    return-void
.end method

.method public unregisterDragListener()V
    .locals 1

    .prologue
    .line 603
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mDragListener:Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$DragListener;

    .line 604
    return-void
.end method

.method public unregisterDropListener()V
    .locals 1

    .prologue
    .line 611
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mDropListener:Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$DropListener;

    .line 613
    return-void
.end method
