.class public Lcom/samsung/musicplus/settings/MyMusicMenuReorderActivity;
.super Lcom/samsung/musicplus/MusicBaseActivity;
.source "MyMusicMenuReorderActivity.java"


# static fields
.field public static final EXTRA_TAB_MENU:Ljava/lang/String; = "TabMenuReorder"


# instance fields
.field private mIsChagnedItem:Z

.field private mListView:Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;

.field mTwMultiSelectedListener:Landroid/widget/AdapterView$OnTwMultiSelectedListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Lcom/samsung/musicplus/MusicBaseActivity;-><init>()V

    .line 22
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderActivity;->mIsChagnedItem:Z

    .line 80
    new-instance v0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderActivity$1;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderActivity$1;-><init>(Lcom/samsung/musicplus/settings/MyMusicMenuReorderActivity;)V

    iput-object v0, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderActivity;->mTwMultiSelectedListener:Landroid/widget/AdapterView$OnTwMultiSelectedListener;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/settings/MyMusicMenuReorderActivity;)Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/MyMusicMenuReorderActivity;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderActivity;->mListView:Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;

    return-object v0
.end method

.method private initListView(Z)V
    .locals 4
    .param p1, "tabMenu"    # Z

    .prologue
    const/4 v3, -0x1

    .line 43
    new-instance v1, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;

    invoke-direct {v1, p0, p1}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;-><init>(Landroid/content/Context;Z)V

    iput-object v1, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderActivity;->mListView:Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;

    .line 46
    iget-object v1, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderActivity;->mListView:Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 47
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, 0x0

    invoke-direct {v0, v3, v3, v1}, Landroid/widget/LinearLayout$LayoutParams;-><init>(IIF)V

    .line 49
    .local v0, "containerParams":Landroid/widget/LinearLayout$LayoutParams;
    iget-object v1, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderActivity;->mListView:Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;

    invoke-virtual {v1, v0}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 50
    iget-object v1, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderActivity;->mListView:Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;

    iget-object v2, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderActivity;->mTwMultiSelectedListener:Landroid/widget/AdapterView$OnTwMultiSelectedListener;

    invoke-virtual {v1, v2}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->setTwMultiSelectedListener(Landroid/widget/AdapterView$OnTwMultiSelectedListener;)V

    .line 51
    return-void
.end method

.method private saveListOrder()V
    .locals 1

    .prologue
    .line 71
    iget-boolean v0, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderActivity;->mIsChagnedItem:Z

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderActivity;->mListView:Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;

    invoke-virtual {v0}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->saveReorderMenu()V

    .line 73
    iget-object v0, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderActivity;->mListView:Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;

    invoke-virtual {v0}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->saveActiveMenu()V

    .line 74
    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderActivity;->finish()V

    .line 78
    :goto_0
    return-void

    .line 76
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderActivity;->finish()V

    goto :goto_0
.end method


# virtual methods
.method public enableDoneButton(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 54
    iput-boolean p1, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderActivity;->mIsChagnedItem:Z

    .line 55
    return-void
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderActivity;->saveListOrder()V

    .line 60
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 26
    invoke-super {p0, p1}, Lcom/samsung/musicplus/MusicBaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 27
    const v3, 0x7f04007a

    invoke-virtual {p0, v3}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderActivity;->setContentView(I)V

    .line 29
    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 30
    .local v0, "data":Landroid/os/Bundle;
    const-string v3, "TabMenuReorder"

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    .line 32
    .local v2, "tabMenu":Z
    invoke-direct {p0, v2}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderActivity;->initListView(Z)V

    .line 33
    const v3, 0x7f0d016e

    invoke-virtual {p0, v3}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 34
    .local v1, "reorderList":Landroid/view/ViewGroup;
    iget-object v3, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderActivity;->mListView:Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 35
    if-eqz v2, :cond_0

    .line 36
    const v3, 0x7f100173

    invoke-virtual {p0, v3}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderActivity;->setTitle(I)V

    .line 40
    :goto_0
    return-void

    .line 38
    :cond_0
    const v3, 0x7f100158

    invoke-virtual {p0, v3}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderActivity;->setTitle(I)V

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 64
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 65
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderActivity;->saveListOrder()V

    .line 67
    :cond_0
    const/4 v0, 0x0

    return v0
.end method
