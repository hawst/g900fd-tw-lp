.class Lcom/samsung/musicplus/settings/SoundAliveV2Setting$13;
.super Landroid/content/BroadcastReceiver;
.source "SoundAliveV2Setting.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/settings/SoundAliveV2Setting;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)V
    .locals 0

    .prologue
    .line 1067
    iput-object p1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$13;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 1070
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 1071
    .local v0, "action":Ljava/lang/String;
    const-string v2, "com.samsung.musicplus.action.AUDIO_PATH_CHANGED"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1072
    iget-object v2, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$13;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;
    invoke-static {v2}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$000(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Lcom/samsung/musicplus/library/audio/SecAudioManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->getCurrentAudioPath()I

    move-result v1

    .line 1074
    .local v1, "audioPath":I
    iget-object v2, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$13;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # invokes: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->disableStrength(I)V
    invoke-static {v2, v1}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$1400(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;I)V

    .line 1075
    iget-object v2, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$13;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # invokes: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->disableUsePreset(I)V
    invoke-static {v2, v1}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$1500(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;I)V

    .line 1076
    iget-object v2, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$13;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;
    invoke-static {v2}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$000(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Lcom/samsung/musicplus/library/audio/SecAudioManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->isHDMIConnect()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1077
    iget-object v2, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$13;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # invokes: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->resetCurrentValue()V
    invoke-static {v2}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$1600(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)V

    .line 1103
    .end local v1    # "audioPath":I
    :cond_0
    :goto_0
    return-void

    .line 1079
    :cond_1
    const-string v2, "com.android.music.playstatechanged"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string v2, "com.android.music.metachanged"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$13;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mCheckBoxAuto:Landroid/widget/CheckBox;
    invoke-static {v2}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$900(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Landroid/widget/CheckBox;

    move-result-object v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$13;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mCheckBoxAuto:Landroid/widget/CheckBox;
    invoke-static {v2}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$900(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Landroid/widget/CheckBox;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1082
    :cond_2
    iget-object v2, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$13;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # invokes: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->updateAutoPlay()V
    invoke-static {v2}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$1700(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)V

    goto :goto_0

    .line 1083
    :cond_3
    const-string v2, "com.samsung.musicplus.action.SOUND_AVLIE_CHANGED"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1088
    iget-object v2, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$13;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    iget-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$13;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSoundAlivePresetGridview:Landroid/widget/GridView;
    invoke-static {v3}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$500(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Landroid/widget/GridView;

    move-result-object v3

    # invokes: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->clearAllsoundAlivePresetCell(Landroid/widget/GridView;)V
    invoke-static {v2, v3}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$600(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;Landroid/widget/GridView;)V

    .line 1089
    iget-object v2, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$13;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mCheckBoxAuto:Landroid/widget/CheckBox;
    invoke-static {v2}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$900(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Landroid/widget/CheckBox;

    move-result-object v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$13;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mCheckBoxAuto:Landroid/widget/CheckBox;
    invoke-static {v2}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$900(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Landroid/widget/CheckBox;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 1090
    iget-object v2, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$13;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mCheckBoxAuto:Landroid/widget/CheckBox;
    invoke-static {v2}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$900(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Landroid/widget/CheckBox;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 1096
    :cond_4
    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    new-instance v3, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$13$1;

    invoke-direct {v3, p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$13$1;-><init>(Lcom/samsung/musicplus/settings/SoundAliveV2Setting$13;)V

    const-wide/16 v4, 0x12c

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method
