.class public Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment;
.super Landroid/app/Fragment;
.source "SoundAliveSetting.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/settings/SoundAliveSetting;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "UserExtendFragment"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment$UserExtView;,
        Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment$ExtSeekBarChangeListner;
    }
.end annotation


# static fields
.field private static final EXT_LEVEL_RES_ID:[I

.field private static final EXT_MAX_PROGRESS:I = 0x3

.field private static final EXT_SEEKBAR_RES_ID:[I

.field public static final EXT_STRING_RES_ID:[I


# instance fields
.field private final mExtSeekBar:[Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x5

    .line 642
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment;->EXT_SEEKBAR_RES_ID:[I

    .line 647
    new-array v0, v1, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment;->EXT_STRING_RES_ID:[I

    .line 651
    new-array v0, v1, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment;->EXT_LEVEL_RES_ID:[I

    return-void

    .line 642
    nop

    :array_0
    .array-data 4
        0x7f0d015b
        0x7f0d015d
        0x7f0d015f
        0x7f0d0161
        0x7f0d0163
    .end array-data

    .line 647
    :array_1
    .array-data 4
        0x7f100081
        0x7f100082
        0x7f100083
        0x7f100084
        0x7f100085
    .end array-data

    .line 651
    :array_2
    .array-data 4
        0x7f0d015a
        0x7f0d015c
        0x7f0d015e
        0x7f0d0160
        0x7f0d0162
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 641
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 657
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;

    iput-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment;->mExtSeekBar:[Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;

    .line 788
    return-void
.end method

.method static synthetic access$1300(Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment;)[Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment;

    .prologue
    .line 641
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment;->mExtSeekBar:[Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;

    return-object v0
.end method

.method private createExtendedView(Landroid/view/View;)V
    .locals 14
    .param p1, "root"    # Landroid/view/View;

    .prologue
    const/4 v13, 0x5

    const/4 v12, 0x0

    .line 691
    const v9, 0x7f0d013e

    invoke-virtual {p1, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;

    .line 692
    .local v0, "bubble":Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;
    const v9, 0x7f0200b9

    invoke-virtual {v0, v9}, Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;->setBubbleBackgroundDrawable(I)V

    .line 693
    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0c0023

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v9

    invoke-virtual {v0, v9}, Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;->setBubbleFontSize(F)V

    .line 694
    const/4 v9, 0x4

    invoke-virtual {v0, v9}, Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;->setBubbleAlign(I)V

    .line 696
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment;->loadSoundAliveUserExt()[I

    move-result-object v4

    .line 697
    .local v4, "extendEffect":[I
    new-array v1, v13, [Landroid/view/View;

    .line 698
    .local v1, "effect":[Landroid/view/View;
    const/4 v5, 0x0

    .line 700
    .local v5, "extendText":Landroid/widget/TextView;
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    if-ge v6, v13, :cond_0

    .line 702
    sget-object v9, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment;->EXT_SEEKBAR_RES_ID:[I

    aget v9, v9, v6

    invoke-virtual {p1, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    aput-object v9, v1, v6

    .line 703
    aget-object v9, v1, v6

    const v10, 0x7f0d01be

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    .end local v5    # "extendText":Landroid/widget/TextView;
    check-cast v5, Landroid/widget/TextView;

    .line 704
    .restart local v5    # "extendText":Landroid/widget/TextView;
    sget-object v9, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment;->EXT_STRING_RES_ID:[I

    aget v9, v9, v6

    invoke-virtual {v5, v9}, Landroid/widget/TextView;->setText(I)V

    .line 707
    iget-object v10, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment;->mExtSeekBar:[Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;

    aget-object v9, v1, v6

    const v11, 0x7f0d01bd

    invoke-virtual {v9, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;

    aput-object v9, v10, v6

    .line 708
    iget-object v9, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment;->mExtSeekBar:[Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;

    aget-object v9, v9, v6

    const/4 v10, 0x3

    invoke-virtual {v9, v10}, Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;->setMax(I)V

    .line 709
    iget-object v9, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment;->mExtSeekBar:[Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;

    aget-object v9, v9, v6

    sget-object v10, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment;->EXT_STRING_RES_ID:[I

    aget v10, v10, v6

    invoke-virtual {v9, v10}, Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;->setTitle(I)V

    .line 710
    iget-object v9, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment;->mExtSeekBar:[Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;

    aget-object v9, v9, v6

    aget v10, v4, v6

    invoke-virtual {v9, v10}, Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;->setProgress(I)V

    .line 711
    iget-object v9, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment;->mExtSeekBar:[Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;

    aget-object v9, v9, v6

    new-instance v10, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment$ExtSeekBarChangeListner;

    invoke-direct {v10, p0, v0}, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment$ExtSeekBarChangeListner;-><init>(Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment;Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;)V

    invoke-virtual {v9, v10}, Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 700
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 714
    :cond_0
    iget-object v9, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment;->mExtSeekBar:[Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;

    aget-object v9, v9, v12

    new-instance v10, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment$1;

    invoke-direct {v10, p0}, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment$1;-><init>(Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment;)V

    invoke-virtual {v9, v10}, Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 725
    iget-object v9, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment;->mExtSeekBar:[Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;

    aget-object v9, v9, v12

    invoke-virtual {v9}, Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;->getPaddingTop()I

    move-result v8

    .line 726
    .local v8, "topPadding":I
    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0c002b

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    .line 729
    .local v7, "seekBarHeight":I
    const/4 v6, 0x0

    :goto_1
    sget-object v9, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment;->EXT_LEVEL_RES_ID:[I

    array-length v9, v9

    if-ge v6, v9, :cond_1

    .line 730
    sget-object v9, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment;->EXT_LEVEL_RES_ID:[I

    aget v9, v9, v6

    invoke-virtual {p1, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    .line 731
    .local v2, "extLevelRoot":Landroid/widget/LinearLayout;
    new-instance v3, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment$UserExtView;

    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment;->getActivity()Landroid/app/Activity;

    move-result-object v9

    invoke-direct {v3, v9, v7, v8}, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment$UserExtView;-><init>(Landroid/content/Context;II)V

    .line 732
    .local v3, "extLevelView":Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment$UserExtView;
    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 729
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 734
    .end local v2    # "extLevelRoot":Landroid/widget/LinearLayout;
    .end local v3    # "extLevelView":Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment$UserExtView;
    :cond_1
    return-void
.end method

.method private loadSoundAliveUserExt()[I
    .locals 10

    .prologue
    const/4 v9, 0x5

    .line 737
    new-array v0, v9, [I

    .line 738
    .local v0, "extraEffect":[I
    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    const-string v7, "music_service_pref"

    const/4 v8, 0x4

    invoke-virtual {v6, v7, v8}, Landroid/app/Activity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 740
    .local v2, "preference":Landroid/content/SharedPreferences;
    const-string v6, "user_ext"

    const-string v7, "0|0|0|0|0|"

    invoke-interface {v2, v6, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 742
    .local v3, "savedEffect":Ljava/lang/String;
    new-instance v4, Ljava/util/StringTokenizer;

    const-string v6, "|"

    invoke-direct {v4, v3, v6}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 744
    .local v4, "strToken":Ljava/util/StringTokenizer;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v9, :cond_1

    .line 745
    invoke-virtual {v4}, Ljava/util/StringTokenizer;->hasMoreElements()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 746
    invoke-virtual {v4}, Ljava/util/StringTokenizer;->nextElement()Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    .line 747
    .local v5, "token":Ljava/lang/String;
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    aput v6, v0, v1

    .line 744
    .end local v5    # "token":Ljava/lang/String;
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 749
    :cond_0
    const/4 v6, 0x0

    aput v6, v0, v1

    goto :goto_1

    .line 752
    :cond_1
    return-object v0
.end method


# virtual methods
.method public disalbe3dEffect()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 672
    const-string v0, "MusicSetting"

    const-string v1, "disalbe3dEffect"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 673
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment;->mExtSeekBar:[Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;

    aget-object v0, v0, v2

    invoke-virtual {v0}, Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;->getMax()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    .line 674
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment;->mExtSeekBar:[Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;

    aget-object v0, v0, v2

    invoke-virtual {v0}, Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;->getProgress()I

    move-result v0

    if-lez v0, :cond_0

    .line 675
    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f100165

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 678
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment;->mExtSeekBar:[Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;

    aget-object v0, v0, v2

    invoke-virtual {v0, v2}, Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;->setProgress(I)V

    .line 679
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment;->mExtSeekBar:[Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;

    aget-object v0, v0, v2

    invoke-virtual {v0, v2}, Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;->setMax(I)V

    .line 681
    :cond_1
    return-void
.end method

.method public enable3dEffect()V
    .locals 4

    .prologue
    const/4 v3, 0x3

    const/4 v2, 0x0

    .line 684
    const-string v0, "MusicSetting"

    const-string v1, "enable3dEffect"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 685
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment;->mExtSeekBar:[Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;

    aget-object v0, v0, v2

    invoke-virtual {v0}, Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;->getMax()I

    move-result v0

    if-eq v0, v3, :cond_0

    .line 686
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment;->mExtSeekBar:[Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;

    aget-object v0, v0, v2

    invoke-virtual {v0, v3}, Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;->setMax(I)V

    .line 688
    :cond_0
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 667
    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment;->createExtendedView(Landroid/view/View;)V

    .line 668
    invoke-super {p0, p1}, Landroid/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 669
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 662
    const v0, 0x7f040074

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
