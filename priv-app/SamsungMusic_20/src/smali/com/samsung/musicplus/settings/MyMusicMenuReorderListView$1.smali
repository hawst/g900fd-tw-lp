.class Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$1;
.super Landroid/os/Handler;
.source "MyMusicMenuReorderListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;)V
    .locals 0

    .prologue
    .line 478
    iput-object p1, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$1;->this$0:Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 480
    iget-object v1, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$1;->this$0:Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;

    # getter for: Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mUI:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->access$200(Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 481
    .local v0, "msg1":Landroid/os/Message;
    iget v1, p1, Landroid/os/Message;->arg1:I

    iput v1, v0, Landroid/os/Message;->arg1:I

    .line 482
    iget-object v1, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$1;->this$0:Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;

    iget-object v1, v1, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mAdapter:Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;

    # getter for: Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->mScrollState:I
    invoke-static {v1}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->access$300(Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;)I

    move-result v1

    if-nez v1, :cond_0

    .line 483
    iget-object v1, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$1;->this$0:Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;

    iget v2, p1, Landroid/os/Message;->arg1:I

    const/16 v3, 0x1e

    invoke-virtual {v1, v2, v3}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->smoothScrollBy(II)V

    .line 484
    iget-object v1, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$1;->this$0:Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;

    # getter for: Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mUI:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->access$200(Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 487
    :goto_0
    return-void

    .line 486
    :cond_0
    iget-object v1, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$1;->this$0:Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;

    # getter for: Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mUI:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->access$200(Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method
