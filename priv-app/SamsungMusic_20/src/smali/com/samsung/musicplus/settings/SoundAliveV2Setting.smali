.class public Lcom/samsung/musicplus/settings/SoundAliveV2Setting;
.super Lcom/samsung/musicplus/MusicBaseActivity;
.source "SoundAliveV2Setting.java"

# interfaces
.implements Lcom/samsung/musicplus/service/PlayerServiceCommandAction;
.implements Lcom/samsung/musicplus/service/PlayerServiceStateAction;
.implements Lcom/samsung/musicplus/settings/PlayerSettingPreference;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/settings/SoundAliveV2Setting$SoundAliveAdapter;,
        Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqLevelView;,
        Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqView;,
        Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqControlBarChangeListener;
    }
.end annotation


# static fields
.field private static final ADVANCED:I = 0x2

.field private static final ADVANCED_TAB:Z = true

.field private static final BASE_POSTION:I = 0xa

.field private static final BASIC:I = 0x1

.field private static final BASIC_TAB:Z = false

.field private static final CLASSNAME:Ljava/lang/String;

.field private static final DB_SCALE_LEVEL_EQ:I = 0xa

.field private static final EQ_SEEKBAR_RES_ID:[I

.field private static final EQ_STRING_RES_ID:[I

.field private static final INSTANCE_AUTO_STATE:Ljava/lang/String; = "auto_state"

.field private static final INSTANCE_BAND_LEVEL:Ljava/lang/String; = "band_level"

.field private static final INSTANCE_SQUARE_POSITION:Ljava/lang/String; = "square_position"

.field private static final INSTANCE_STRENGTH:Ljava/lang/String; = "strength"

.field private static final INSTANCE_USE_PRESET:Ljava/lang/String; = "use_preset"

.field private static final MENU_CANCEL:I = 0x7f0d0200

.field private static final MENU_DONE:I = 0x7f0d01ca

.field private static final MENU_HOME:I = 0x102002c

.field private static final MENU_RESET:I = 0x7f0d01ff

.field private static final SA_EFFECT_OFF:I = 0x0

.field private static final SA_EFFECT_ON:I = 0x1

.field private static final SELECT_TAB:Ljava/lang/String; = "tab"

.field private static final SET_STRENGTH_FORCE_OFF:I = 0x2

.field private static final SET_STRENGTH_FORCE_ON:I = 0x1

.field private static final SET_STRENGTH_TOGGLE:I = 0x0

.field private static final SOUND_ALIVE_UI_UPDATE_DELAY:I = 0x12c

.field private static final SQUARE_CELL_UNSELECTED:I = -0x1

.field private static final SQUARE_COLUMN_COUNT:I = 0x5

.field private static final TAB_COUNT:I = 0x2

.field private static THEME:I

.field private static mLastControlledTab:Z

.field private static mSquarePosition:I

.field private static sMusicPlayerEntrance:Z


# instance fields
.field private isCancel:Z

.field private mAdvancedButton:Landroid/widget/TextView;

.field private mAdvancedLayout:Landroid/view/ViewGroup;

.field private mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

.field private mBasicButton:Landroid/widget/TextView;

.field private mBasicLayout:Landroid/view/ViewGroup;

.field private mBubble:Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;

.field private mCheckBoxAuto:Landroid/widget/CheckBox;

.field private mCurrentTab:I

.field private mDbLinePaddingTop:I

.field private mDoSetPrevEffect:Z

.field private mEntranceAutoChecked:Z

.field private mEntranceBandLevel:[I

.field private final mEqControlBar:[Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;

.field private final mEqControlBarKeyListener:Landroid/view/View$OnKeyListener;

.field private mImageViewEffctClub:Landroid/widget/ImageView;

.field private mImageViewEffctConcertHall:Landroid/widget/ImageView;

.field private mImageViewEffctNone:Landroid/widget/ImageView;

.field private mImageViewEffctStudio:Landroid/widget/ImageView;

.field private mImageViewEffctTubeAmpEffect:Landroid/widget/ImageView;

.field private mImageViewEffctVirtual71ch:Landroid/widget/ImageView;

.field private mIsBubbleRunable:Z

.field private mIsDown:Z

.field private mIsReset:Z

.field private mLastSelectedCell:I

.field private mNewSoundAliveButton3D:Landroid/widget/TextView;

.field private mNewSoundAliveButtonBass:Landroid/widget/TextView;

.field private mNewSoundAliveButtonClarity:Landroid/widget/TextView;

.field private mPrevValues:[I

.field private mRateLandlinePosY:D

.field private mRatePortlinePosY:D

.field private mSeekBarHeight:I

.field private mSelectedUsePreset:I

.field private mSoundAliveEffecClub:Landroid/widget/RelativeLayout;

.field private mSoundAliveEffecConcertHall:Landroid/widget/RelativeLayout;

.field private mSoundAliveEffecTubeAmpEffect:Landroid/widget/RelativeLayout;

.field private mSoundAliveEffecVirtual71ch:Landroid/widget/RelativeLayout;

.field private mSoundAliveEffectNone:Landroid/widget/RelativeLayout;

.field private mSoundAliveEffectStudio:Landroid/widget/RelativeLayout;

.field private mSoundAlivePresetGridview:Landroid/widget/GridView;

.field private mSquareCellSelected:Z

.field private final mStateReceiver:Landroid/content/BroadcastReceiver;

.field private mToast:Landroid/widget/Toast;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x7

    .line 63
    const-class v0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->CLASSNAME:Ljava/lang/String;

    .line 158
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->EQ_SEEKBAR_RES_ID:[I

    .line 164
    new-array v0, v1, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->EQ_STRING_RES_ID:[I

    .line 199
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mLastControlledTab:Z

    .line 201
    invoke-static {}, Lcom/samsung/musicplus/util/UiUtils;->getTheme()I

    move-result v0

    sput v0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->THEME:I

    .line 842
    const/4 v0, -0x1

    sput v0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSquarePosition:I

    return-void

    .line 158
    :array_0
    .array-data 4
        0x7f0d0142
        0x7f0d0143
        0x7f0d0144
        0x7f0d0145
        0x7f0d0146
        0x7f0d0147
        0x7f0d0148
    .end array-data

    .line 164
    :array_1
    .array-data 4
        0x7f100075
        0x7f100076
        0x7f100077
        0x7f100078
        0x7f100079
        0x7f10007a
        0x7f10007b
    .end array-data
.end method

.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x7

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 60
    invoke-direct {p0}, Lcom/samsung/musicplus/MusicBaseActivity;-><init>()V

    .line 140
    iput v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSelectedUsePreset:I

    .line 148
    iput-wide v2, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mRatePortlinePosY:D

    .line 150
    iput-wide v2, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mRateLandlinePosY:D

    .line 152
    new-array v0, v4, [Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;

    iput-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mEqControlBar:[Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;

    .line 154
    new-array v0, v4, [I

    iput-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mEntranceBandLevel:[I

    .line 156
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mEntranceAutoChecked:Z

    .line 187
    iput-boolean v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mIsReset:Z

    .line 189
    iput-boolean v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->isCancel:Z

    .line 207
    iput-boolean v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mDoSetPrevEffect:Z

    .line 682
    iput-boolean v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSquareCellSelected:Z

    .line 684
    iput-boolean v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mIsDown:Z

    .line 686
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mLastSelectedCell:I

    .line 1067
    new-instance v0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$13;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$13;-><init>(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)V

    iput-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mStateReceiver:Landroid/content/BroadcastReceiver;

    .line 1481
    new-instance v0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$17;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$17;-><init>(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)V

    iput-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mEqControlBarKeyListener:Landroid/view/View$OnKeyListener;

    .line 1845
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Lcom/samsung/musicplus/library/audio/SecAudioManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;III)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/SoundAliveV2Setting;
    .param p1, "x1"    # I
    .param p2, "x2"    # I
    .param p3, "x3"    # I

    .prologue
    .line 60
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->enableStrengthEffect(III)V

    return-void
.end method

.method static synthetic access$1000(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/SoundAliveV2Setting;
    .param p1, "x1"    # Z

    .prologue
    .line 60
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->setCheckedAuto(Z)V

    return-void
.end method

.method static synthetic access$1100(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Landroid/widget/Toast;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mToast:Landroid/widget/Toast;

    return-object v0
.end method

.method static synthetic access$1102(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;Landroid/widget/Toast;)Landroid/widget/Toast;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/SoundAliveV2Setting;
    .param p1, "x1"    # Landroid/widget/Toast;

    .prologue
    .line 60
    iput-object p1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mToast:Landroid/widget/Toast;

    return-object p1
.end method

.method static synthetic access$1200(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    .prologue
    .line 60
    iget-boolean v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mIsDown:Z

    return v0
.end method

.method static synthetic access$1202(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/SoundAliveV2Setting;
    .param p1, "x1"    # Z

    .prologue
    .line 60
    iput-boolean p1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mIsDown:Z

    return p1
.end method

.method static synthetic access$1300(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    .prologue
    .line 60
    iget v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mLastSelectedCell:I

    return v0
.end method

.method static synthetic access$1302(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/SoundAliveV2Setting;
    .param p1, "x1"    # I

    .prologue
    .line 60
    iput p1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mLastSelectedCell:I

    return p1
.end method

.method static synthetic access$1400(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/SoundAliveV2Setting;
    .param p1, "x1"    # I

    .prologue
    .line 60
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->disableStrength(I)V

    return-void
.end method

.method static synthetic access$1500(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/SoundAliveV2Setting;
    .param p1, "x1"    # I

    .prologue
    .line 60
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->disableUsePreset(I)V

    return-void
.end method

.method static synthetic access$1600(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->resetCurrentValue()V

    return-void
.end method

.method static synthetic access$1700(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->updateAutoPlay()V

    return-void
.end method

.method static synthetic access$1800(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->updateValuesFromService()V

    return-void
.end method

.method static synthetic access$1900(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/SoundAliveV2Setting;
    .param p1, "x1"    # I

    .prologue
    .line 60
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->setSelectedTab(I)V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;Landroid/view/View;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/SoundAliveV2Setting;
    .param p1, "x1"    # Landroid/view/View;
    .param p2, "x2"    # I

    .prologue
    .line 60
    invoke-direct {p0, p1, p2}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->enalbePresetEffect(Landroid/view/View;I)V

    return-void
.end method

.method static synthetic access$2000()I
    .locals 1

    .prologue
    .line 60
    sget v0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSquarePosition:I

    return v0
.end method

.method static synthetic access$2100(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->updateBandLevel()V

    return-void
.end method

.method static synthetic access$2200(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->updateStrength()V

    return-void
.end method

.method static synthetic access$2300(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->updateSquarePosition()V

    return-void
.end method

.method static synthetic access$2400(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mBubble:Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    .prologue
    .line 60
    iget v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mCurrentTab:I

    return v0
.end method

.method static synthetic access$2600(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->isPortrait()Z

    move-result v0

    return v0
.end method

.method static synthetic access$2700(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    .prologue
    .line 60
    iget-boolean v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mIsReset:Z

    return v0
.end method

.method static synthetic access$2702(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/SoundAliveV2Setting;
    .param p1, "x1"    # Z

    .prologue
    .line 60
    iput-boolean p1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mIsReset:Z

    return p1
.end method

.method static synthetic access$2800(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    .prologue
    .line 60
    iget-boolean v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->isCancel:Z

    return v0
.end method

.method static synthetic access$2900(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    .prologue
    .line 60
    iget-boolean v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mIsBubbleRunable:Z

    return v0
.end method

.method static synthetic access$2902(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/SoundAliveV2Setting;
    .param p1, "x1"    # Z

    .prologue
    .line 60
    iput-boolean p1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mIsBubbleRunable:Z

    return p1
.end method

.method static synthetic access$300(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;IZ)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/SoundAliveV2Setting;
    .param p1, "x1"    # I
    .param p2, "x2"    # Z

    .prologue
    .line 60
    invoke-direct {p0, p1, p2}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->setUsePresetEffect(IZ)V

    return-void
.end method

.method static synthetic access$3000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 60
    sget-object v0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->CLASSNAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$3100(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->setAdvancedBandLevel()V

    return-void
.end method

.method static synthetic access$3200()Z
    .locals 1

    .prologue
    .line 60
    sget-boolean v0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mLastControlledTab:Z

    return v0
.end method

.method static synthetic access$3400(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    .prologue
    .line 60
    iget v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mDbLinePaddingTop:I

    return v0
.end method

.method static synthetic access$3500()I
    .locals 1

    .prologue
    .line 60
    sget v0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->THEME:I

    return v0
.end method

.method static synthetic access$3600(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->getScreenRawWidth()I

    move-result v0

    return v0
.end method

.method static synthetic access$3700(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)D
    .locals 2
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    .prologue
    .line 60
    iget-wide v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mRatePortlinePosY:D

    return-wide v0
.end method

.method static synthetic access$3702(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;D)D
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/SoundAliveV2Setting;
    .param p1, "x1"    # D

    .prologue
    .line 60
    iput-wide p1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mRatePortlinePosY:D

    return-wide p1
.end method

.method static synthetic access$3800(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)D
    .locals 2
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    .prologue
    .line 60
    iget-wide v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mRateLandlinePosY:D

    return-wide v0
.end method

.method static synthetic access$3802(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;D)D
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/SoundAliveV2Setting;
    .param p1, "x1"    # D

    .prologue
    .line 60
    iput-wide p1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mRateLandlinePosY:D

    return-wide p1
.end method

.method static synthetic access$3900(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->getDensity()I

    move-result v0

    return v0
.end method

.method static synthetic access$4000(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    .prologue
    .line 60
    iget v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSeekBarHeight:I

    return v0
.end method

.method static synthetic access$402(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/SoundAliveV2Setting;
    .param p1, "x1"    # Z

    .prologue
    .line 60
    iput-boolean p1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSquareCellSelected:Z

    return p1
.end method

.method static synthetic access$500(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Landroid/widget/GridView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSoundAlivePresetGridview:Landroid/widget/GridView;

    return-object v0
.end method

.method static synthetic access$600(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;Landroid/widget/GridView;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/SoundAliveV2Setting;
    .param p1, "x1"    # Landroid/widget/GridView;

    .prologue
    .line 60
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->clearAllsoundAlivePresetCell(Landroid/widget/GridView;)V

    return-void
.end method

.method static synthetic access$700(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;Landroid/widget/GridView;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/SoundAliveV2Setting;
    .param p1, "x1"    # Landroid/widget/GridView;
    .param p2, "x2"    # I

    .prologue
    .line 60
    invoke-direct {p0, p1, p2}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->selectSoundAlivePresetCell(Landroid/widget/GridView;I)V

    return-void
.end method

.method static synthetic access$800(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/SoundAliveV2Setting;
    .param p1, "x1"    # I

    .prologue
    .line 60
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->setSquareEffect(I)V

    return-void
.end method

.method static synthetic access$900(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mCheckBoxAuto:Landroid/widget/CheckBox;

    return-object v0
.end method

.method private clearAllsoundAlivePresetCell(Landroid/widget/GridView;)V
    .locals 6
    .param p1, "v"    # Landroid/widget/GridView;

    .prologue
    .line 856
    invoke-virtual {p1}, Landroid/widget/GridView;->getChildCount()I

    move-result v3

    .line 857
    .local v3, "size":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v3, :cond_1

    .line 858
    invoke-virtual {p1, v1}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 859
    .local v0, "child":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 860
    const v4, 0x7f0d0184

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    .line 862
    .local v2, "image":Landroid/widget/ImageView;
    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0200bb

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 864
    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Landroid/view/View;->setSelected(Z)V

    .line 857
    .end local v2    # "image":Landroid/widget/ImageView;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 867
    .end local v0    # "child":Landroid/view/View;
    :cond_1
    const/4 v4, -0x1

    sput v4, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSquarePosition:I

    .line 868
    return-void
.end method

.method private clearEffectBottomLineImage(Landroid/widget/RelativeLayout;)V
    .locals 3
    .param p1, "layout"    # Landroid/widget/RelativeLayout;

    .prologue
    .line 580
    const v1, 0x7f0d0173

    invoke-virtual {p1, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 581
    .local v0, "image":Landroid/widget/ImageView;
    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0072

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 583
    const v1, 0x7f100192

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 584
    return-void
.end method

.method private clearEffectImage()V
    .locals 3

    .prologue
    .line 558
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mImageViewEffctTubeAmpEffect:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020092

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 560
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mImageViewEffctVirtual71ch:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020094

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 562
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mImageViewEffctNone:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02008c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 564
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mImageViewEffctStudio:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020090

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 566
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mImageViewEffctClub:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02008e

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 568
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mImageViewEffctConcertHall:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02008a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 571
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSoundAliveEffecTubeAmpEffect:Landroid/widget/RelativeLayout;

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->clearEffectBottomLineImage(Landroid/widget/RelativeLayout;)V

    .line 572
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSoundAliveEffecVirtual71ch:Landroid/widget/RelativeLayout;

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->clearEffectBottomLineImage(Landroid/widget/RelativeLayout;)V

    .line 573
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSoundAliveEffectNone:Landroid/widget/RelativeLayout;

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->clearEffectBottomLineImage(Landroid/widget/RelativeLayout;)V

    .line 574
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSoundAliveEffectStudio:Landroid/widget/RelativeLayout;

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->clearEffectBottomLineImage(Landroid/widget/RelativeLayout;)V

    .line 575
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSoundAliveEffecClub:Landroid/widget/RelativeLayout;

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->clearEffectBottomLineImage(Landroid/widget/RelativeLayout;)V

    .line 576
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSoundAliveEffecConcertHall:Landroid/widget/RelativeLayout;

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->clearEffectBottomLineImage(Landroid/widget/RelativeLayout;)V

    .line 577
    return-void
.end method

.method private disableAutoEffect()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 442
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mCheckBoxAuto:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 443
    invoke-direct {p0, v2}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->setCheckedAuto(Z)V

    .line 444
    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f1000e5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 448
    :cond_0
    return-void
.end method

.method private disableSquareEffect(I)V
    .locals 3
    .param p1, "effect"    # I

    .prologue
    .line 435
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->getStrengthValue()[I

    move-result-object v0

    .line 436
    .local v0, "newStrength":[I
    aget v1, v0, p1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 437
    const/4 v1, 0x0

    invoke-static {v1}, Lcom/samsung/musicplus/util/ServiceUtils;->setNewSoundAliveSquarePosition([I)V

    .line 439
    :cond_0
    return-void
.end method

.method private disableStrength(I)V
    .locals 3
    .param p1, "audioPath"    # I

    .prologue
    const/4 v2, 0x0

    .line 1138
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mNewSoundAliveButton3D:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 1139
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mNewSoundAliveButton3D:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x7

    invoke-static {p1, v0}, Lcom/samsung/musicplus/util/SoundAliveUtilsV2;->getNewSoundAliveErrorMessage(II)I

    move-result v0

    sget v1, Lcom/samsung/musicplus/util/SoundAliveUtilsV2;->UNDEFINED:I

    if-eq v0, v1, :cond_0

    .line 1142
    invoke-static {v2, v2}, Lcom/samsung/musicplus/util/ServiceUtils;->setNewSoundAliveEachStrength(II)V

    .line 1144
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mNewSoundAliveButton3D:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setSelected(Z)V

    .line 1148
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mNewSoundAliveButtonBass:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 1149
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mNewSoundAliveButtonBass:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0x8

    invoke-static {p1, v0}, Lcom/samsung/musicplus/util/SoundAliveUtilsV2;->getNewSoundAliveErrorMessage(II)I

    move-result v0

    sget v1, Lcom/samsung/musicplus/util/SoundAliveUtilsV2;->UNDEFINED:I

    if-eq v0, v1, :cond_1

    .line 1152
    const/4 v0, 0x1

    invoke-static {v0, v2}, Lcom/samsung/musicplus/util/ServiceUtils;->setNewSoundAliveEachStrength(II)V

    .line 1154
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mNewSoundAliveButtonBass:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setSelected(Z)V

    .line 1158
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mNewSoundAliveButtonClarity:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    .line 1159
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mNewSoundAliveButtonClarity:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_2

    const/16 v0, 0x9

    invoke-static {p1, v0}, Lcom/samsung/musicplus/util/SoundAliveUtilsV2;->getNewSoundAliveErrorMessage(II)I

    move-result v0

    sget v1, Lcom/samsung/musicplus/util/SoundAliveUtilsV2;->UNDEFINED:I

    if-eq v0, v1, :cond_2

    .line 1162
    const/4 v0, 0x2

    invoke-static {v0, v2}, Lcom/samsung/musicplus/util/ServiceUtils;->setNewSoundAliveEachStrength(II)V

    .line 1164
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mNewSoundAliveButtonClarity:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setSelected(Z)V

    .line 1167
    :cond_2
    return-void
.end method

.method private disableUsePreset(I)V
    .locals 2
    .param p1, "audioPath"    # I

    .prologue
    .line 1132
    iget v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSelectedUsePreset:I

    invoke-static {p1, v0}, Lcom/samsung/musicplus/util/SoundAliveUtilsV2;->getNewSoundAliveErrorMessage(II)I

    move-result v0

    sget v1, Lcom/samsung/musicplus/util/SoundAliveUtilsV2;->UNDEFINED:I

    if-eq v0, v1, :cond_0

    .line 1133
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->setUsePresetEffect(IZ)V

    .line 1135
    :cond_0
    return-void
.end method

.method private drawDBLine()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1284
    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0c002b

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    iput v4, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSeekBarHeight:I

    .line 1285
    iget-object v4, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mEqControlBar:[Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mEqControlBar:[Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;

    aget-object v4, v4, v6

    if-eqz v4, :cond_0

    .line 1286
    iget-object v4, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mEqControlBar:[Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;

    aget-object v4, v4, v6

    invoke-virtual {v4}, Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;->getPaddingTop()I

    move-result v4

    iput v4, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mDbLinePaddingTop:I

    .line 1289
    :cond_0
    iget-object v4, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mAdvancedLayout:Landroid/view/ViewGroup;

    const v5, 0x7f0d0140

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    .line 1291
    .local v0, "lineEqLayout":Landroid/widget/LinearLayout;
    new-instance v3, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqView;

    invoke-direct {v3, p0, p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqView;-><init>(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;Landroid/content/Context;)V

    .line 1292
    .local v3, "lineEqView":Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqView;
    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1294
    iget-object v4, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mAdvancedLayout:Landroid/view/ViewGroup;

    const v5, 0x7f0d0141

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 1296
    .local v1, "lineEqLevelLine":Landroid/widget/LinearLayout;
    new-instance v2, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqLevelView;

    invoke-direct {v2, p0, p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqLevelView;-><init>(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;Landroid/content/Context;)V

    .line 1297
    .local v2, "lineEqLevelView":Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqLevelView;
    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 1299
    return-void
.end method

.method private enableStrengthEffect(III)V
    .locals 1
    .param p1, "message"    # I
    .param p2, "effect"    # I
    .param p3, "mode"    # I

    .prologue
    .line 423
    sget v0, Lcom/samsung/musicplus/util/SoundAliveUtilsV2;->UNDEFINED:I

    if-ne p1, v0, :cond_0

    .line 424
    invoke-direct {p0, p2, p3}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->setStrength(II)V

    .line 425
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSoundAlivePresetGridview:Landroid/widget/GridView;

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->clearAllsoundAlivePresetCell(Landroid/widget/GridView;)V

    .line 426
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->disableAutoEffect()V

    .line 427
    invoke-direct {p0, p2}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->disableSquareEffect(I)V

    .line 428
    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/musicplus/util/SoundAliveHeadsetUtil;->notifySoundAliveChanged(Landroid/content/Context;)V

    .line 432
    :goto_0
    return-void

    .line 430
    :cond_0
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method private enalbePresetEffect(Landroid/view/View;I)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "effect"    # I

    .prologue
    const/4 v3, 0x1

    .line 643
    iget-object v2, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

    invoke-virtual {v2}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->getCurrentAudioPath()I

    move-result v2

    invoke-static {v2, p2}, Lcom/samsung/musicplus/util/SoundAliveUtilsV2;->getNewSoundAliveErrorMessage(II)I

    move-result v1

    .line 646
    .local v1, "message":I
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 647
    .local v0, "id":I
    packed-switch v0, :pswitch_data_0

    .line 677
    :cond_0
    :goto_0
    :pswitch_0
    sget v2, Lcom/samsung/musicplus/util/SoundAliveUtils;->UNDEFINED:I

    if-eq v1, v2, :cond_1

    .line 678
    const/4 v2, 0x0

    invoke-static {p0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    .line 680
    :cond_1
    return-void

    .line 649
    :pswitch_1
    sget v2, Lcom/samsung/musicplus/util/SoundAliveUtils;->UNDEFINED:I

    if-ne v1, v2, :cond_0

    .line 650
    invoke-direct {p0, p2, v3}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->setUsePresetEffect(IZ)V

    goto :goto_0

    .line 654
    :pswitch_2
    sget v2, Lcom/samsung/musicplus/util/SoundAliveUtils;->UNDEFINED:I

    if-ne v1, v2, :cond_0

    .line 655
    invoke-direct {p0, p2, v3}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->setUsePresetEffect(IZ)V

    goto :goto_0

    .line 659
    :pswitch_3
    sget v2, Lcom/samsung/musicplus/util/SoundAliveUtils;->UNDEFINED:I

    if-ne v1, v2, :cond_0

    .line 660
    invoke-direct {p0, p2, v3}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->setUsePresetEffect(IZ)V

    goto :goto_0

    .line 664
    :pswitch_4
    sget v2, Lcom/samsung/musicplus/util/SoundAliveUtils;->UNDEFINED:I

    if-ne v1, v2, :cond_0

    .line 665
    invoke-direct {p0, p2, v3}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->setUsePresetEffect(IZ)V

    goto :goto_0

    .line 669
    :pswitch_5
    sget v2, Lcom/samsung/musicplus/util/SoundAliveUtils;->UNDEFINED:I

    if-ne v1, v2, :cond_0

    .line 670
    invoke-direct {p0, p2, v3}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->setUsePresetEffect(IZ)V

    goto :goto_0

    .line 647
    nop

    :pswitch_data_0
    .packed-switch 0x7f0d0175
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method private getAutoState(Landroid/os/Bundle;)Z
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 312
    const/4 v0, 0x1

    .line 314
    .local v0, "autoState":Z
    if-eqz p1, :cond_0

    .line 315
    const-string v1, "auto_state"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 319
    :goto_0
    return v0

    .line 317
    :cond_0
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->loadNewSoundAliveAuto()Z

    move-result v0

    goto :goto_0
.end method

.method private getCurrentValue(I)[I
    .locals 3
    .param p1, "tab"    # I

    .prologue
    .line 1430
    const/4 v0, 0x0

    .line 1431
    .local v0, "eqValue":[I
    const/4 v2, 0x2

    if-ne p1, v2, :cond_1

    .line 1432
    const/4 v2, 0x7

    new-array v0, v2, [I

    .line 1433
    iget-object v2, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mEqControlBar:[Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;

    if-eqz v2, :cond_1

    if-eqz v0, :cond_1

    .line 1434
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, v0

    if-ge v1, v2, :cond_1

    .line 1435
    iget-object v2, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mEqControlBar:[Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;

    aget-object v2, v2, v1

    if-eqz v2, :cond_0

    .line 1436
    iget-object v2, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mEqControlBar:[Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;

    aget-object v2, v2, v1

    invoke-virtual {v2}, Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;->getProgress()I

    move-result v2

    add-int/lit8 v2, v2, -0xa

    aput v2, v0, v1

    .line 1437
    iget-object v2, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mEqControlBar:[Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;

    aget-object v2, v2, v1

    invoke-virtual {v2}, Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;->setContentDescription()V

    .line 1434
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1442
    .end local v1    # "i":I
    :cond_1
    return-object v0
.end method

.method private getDensity()I
    .locals 1

    .prologue
    .line 1789
    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    return v0
.end method

.method private getScreenRawWidth()I
    .locals 3

    .prologue
    .line 1780
    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 1782
    .local v0, "display":Landroid/view/Display;
    new-instance v1, Landroid/graphics/Point;

    invoke-direct {v1}, Landroid/graphics/Point;-><init>()V

    .line 1783
    .local v1, "point":Landroid/graphics/Point;
    invoke-virtual {v0, v1}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 1784
    iget v2, v1, Landroid/graphics/Point;->x:I

    return v2
.end method

.method private getSquareColumn(II)I
    .locals 1
    .param p1, "position"    # I
    .param p2, "columns"    # I

    .prologue
    .line 888
    rem-int v0, p1, p2

    return v0
.end method

.method private getSquarePosition([I)I
    .locals 4
    .param p1, "position"    # [I

    .prologue
    const/16 v1, 0xc

    .line 336
    if-nez p1, :cond_0

    .line 342
    :goto_0
    return v1

    .line 340
    :cond_0
    const/4 v2, 0x0

    :try_start_0
    aget v2, p1, v2

    mul-int/lit8 v2, v2, 0x5

    const/4 v3, 0x1

    aget v3, p1, v3

    add-int/2addr v2, v3

    sput v2, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSquarePosition:I
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move v1, v2

    goto :goto_0

    .line 341
    :catch_0
    move-exception v0

    .line 342
    .local v0, "e":Ljava/lang/IndexOutOfBoundsException;
    goto :goto_0
.end method

.method private getSquarePosition()[I
    .locals 4

    .prologue
    const/4 v3, 0x5

    .line 1050
    const/4 v0, 0x2

    new-array v0, v0, [I

    const/4 v1, 0x0

    sget v2, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSquarePosition:I

    invoke-direct {p0, v2, v3}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->getSquareRow(II)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x1

    sget v2, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSquarePosition:I

    invoke-direct {p0, v2, v3}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->getSquareColumn(II)I

    move-result v2

    aput v2, v0, v1

    return-object v0
.end method

.method private getSquareRow(II)I
    .locals 1
    .param p1, "position"    # I
    .param p2, "columns"    # I

    .prologue
    .line 884
    div-int v0, p1, p2

    return v0
.end method

.method private getStrengthValue()[I
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1058
    iget-object v5, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mNewSoundAliveButton3D:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->isSelected()Z

    move-result v5

    if-eqz v5, :cond_0

    move v0, v3

    .line 1059
    .local v0, "strength_3D":I
    :goto_0
    iget-object v5, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mNewSoundAliveButtonBass:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->isSelected()Z

    move-result v5

    if-eqz v5, :cond_1

    move v1, v3

    .line 1060
    .local v1, "strength_Bass":I
    :goto_1
    iget-object v5, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mNewSoundAliveButtonClarity:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->isSelected()Z

    move-result v5

    if-eqz v5, :cond_2

    move v2, v3

    .line 1062
    .local v2, "strength_Clarity":I
    :goto_2
    const/4 v5, 0x3

    new-array v5, v5, [I

    aput v0, v5, v4

    aput v1, v5, v3

    const/4 v3, 0x2

    aput v2, v5, v3

    return-object v5

    .end local v0    # "strength_3D":I
    .end local v1    # "strength_Bass":I
    .end local v2    # "strength_Clarity":I
    :cond_0
    move v0, v4

    .line 1058
    goto :goto_0

    .restart local v0    # "strength_3D":I
    :cond_1
    move v1, v4

    .line 1059
    goto :goto_1

    .restart local v1    # "strength_Bass":I
    :cond_2
    move v2, v4

    .line 1060
    goto :goto_2
.end method

.method private initLayout(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const v6, 0x7f1001a7

    const/4 v5, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x2

    .line 1170
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mBasicButton:Landroid/widget/TextView;

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p0, v6, v1}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 1171
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mBasicButton:Landroid/widget/TextView;

    new-instance v1, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$14;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$14;-><init>(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1178
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mAdvancedButton:Landroid/widget/TextView;

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p0, v6, v1}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 1179
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mAdvancedButton:Landroid/widget/TextView;

    new-instance v1, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$15;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$15;-><init>(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1190
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mCheckBoxAuto:Landroid/widget/CheckBox;

    new-instance v1, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$16;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$16;-><init>(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)V

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 1220
    if-eqz p1, :cond_0

    .line 1221
    const-string v0, "tab"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->setSelectedTab(I)V

    .line 1225
    :goto_0
    return-void

    .line 1223
    :cond_0
    invoke-direct {p0, v4}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->setSelectedTab(I)V

    goto :goto_0
.end method

.method private initPresetEffect()V
    .locals 2

    .prologue
    .line 587
    const v0, 0x7f0d0179

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSoundAliveEffectStudio:Landroid/widget/RelativeLayout;

    .line 588
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSoundAliveEffectStudio:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$4;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$4;-><init>(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 596
    const v0, 0x7f0d0172

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSoundAliveEffectNone:Landroid/widget/RelativeLayout;

    .line 597
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSoundAliveEffectNone:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$5;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$5;-><init>(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 605
    const v0, 0x7f0d017b

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSoundAliveEffecClub:Landroid/widget/RelativeLayout;

    .line 606
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSoundAliveEffecClub:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$6;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$6;-><init>(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 614
    const v0, 0x7f0d017d

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSoundAliveEffecConcertHall:Landroid/widget/RelativeLayout;

    .line 615
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSoundAliveEffecConcertHall:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$7;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$7;-><init>(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 623
    const v0, 0x7f0d0175

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSoundAliveEffecTubeAmpEffect:Landroid/widget/RelativeLayout;

    .line 624
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSoundAliveEffecTubeAmpEffect:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$8;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$8;-><init>(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 632
    const v0, 0x7f0d0177

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    iput-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSoundAliveEffecVirtual71ch:Landroid/widget/RelativeLayout;

    .line 633
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSoundAliveEffecVirtual71ch:Landroid/widget/RelativeLayout;

    new-instance v1, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$9;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$9;-><init>(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 640
    return-void
.end method

.method private initSoundAliveGridView()V
    .locals 2

    .prologue
    .line 692
    const v0, 0x7f0d0181

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/GridView;

    iput-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSoundAlivePresetGridview:Landroid/widget/GridView;

    .line 693
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSoundAlivePresetGridview:Landroid/widget/GridView;

    new-instance v1, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$SoundAliveAdapter;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$SoundAliveAdapter;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 694
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->matchAlign()V

    .line 696
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSoundAlivePresetGridview:Landroid/widget/GridView;

    new-instance v1, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$10;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$10;-><init>(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)V

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 711
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSoundAlivePresetGridview:Landroid/widget/GridView;

    new-instance v1, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$11;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$11;-><init>(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)V

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 815
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSoundAlivePresetGridview:Landroid/widget/GridView;

    new-instance v1, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$12;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$12;-><init>(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)V

    invoke-virtual {v0, v1}, Landroid/widget/GridView;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 839
    return-void
.end method

.method private initStrengthEffect()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 375
    const v0, 0x7f0d016f

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mNewSoundAliveButton3D:Landroid/widget/TextView;

    .line 376
    const v0, 0x7f0d0170

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mNewSoundAliveButtonBass:Landroid/widget/TextView;

    .line 377
    const v0, 0x7f0d0171

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mNewSoundAliveButtonClarity:Landroid/widget/TextView;

    .line 379
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mNewSoundAliveButton3D:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 380
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mNewSoundAliveButtonBass:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 381
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mNewSoundAliveButtonClarity:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 382
    invoke-static {}, Lcom/samsung/musicplus/util/UiUtils;->isUseScrollText()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 383
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mNewSoundAliveButtonClarity:Landroid/widget/TextView;

    invoke-static {}, Landroid/text/method/ScrollingMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 385
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mNewSoundAliveButton3D:Landroid/widget/TextView;

    new-instance v1, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$1;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$1;-><init>(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 396
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mNewSoundAliveButtonBass:Landroid/widget/TextView;

    new-instance v1, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$2;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$2;-><init>(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 408
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mNewSoundAliveButtonClarity:Landroid/widget/TextView;

    new-instance v1, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$3;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$3;-><init>(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 420
    return-void
.end method

.method private isAutoCheckedChanged()Z
    .locals 2

    .prologue
    .line 932
    iget-boolean v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mEntranceAutoChecked:Z

    iget-object v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mCheckBoxAuto:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-eq v0, v1, :cond_0

    .line 933
    const/4 v0, 0x1

    .line 935
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isBandLevelChanged()Z
    .locals 4

    .prologue
    .line 939
    const/4 v2, 0x2

    invoke-direct {p0, v2}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->getCurrentValue(I)[I

    move-result-object v0

    .line 941
    .local v0, "currentBandLevel":[I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/4 v2, 0x7

    if-ge v1, v2, :cond_1

    .line 942
    iget-object v2, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mEntranceBandLevel:[I

    aget v2, v2, v1

    aget v3, v0, v1

    if-eq v2, v3, :cond_0

    .line 943
    const/4 v2, 0x1

    .line 946
    :goto_1
    return v2

    .line 941
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 946
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private isPortrait()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 1773
    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    if-ne v1, v0, :cond_0

    .line 1776
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private loadBandLevelPref(Landroid/os/Bundle;Z)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;
    .param p2, "isReload"    # Z

    .prologue
    const/4 v7, 0x7

    .line 1447
    if-eqz p1, :cond_0

    .line 1448
    const-string v4, "band_level"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v0

    .line 1455
    .local v0, "currentUserEq":[I
    :goto_0
    new-array v2, v7, [Landroid/view/View;

    .line 1456
    .local v2, "eqControlView":[Landroid/view/View;
    const/4 v1, 0x0

    .line 1458
    .local v1, "eqControlText":Landroid/widget/TextView;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    if-ge v3, v7, :cond_3

    .line 1460
    iget-object v4, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mAdvancedLayout:Landroid/view/ViewGroup;

    sget-object v5, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->EQ_SEEKBAR_RES_ID:[I

    aget v5, v5, v3

    invoke-virtual {v4, v5}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v4

    aput-object v4, v2, v3

    .line 1461
    aget-object v4, v2, v3

    const v5, 0x7f0d01be

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .end local v1    # "eqControlText":Landroid/widget/TextView;
    check-cast v1, Landroid/widget/TextView;

    .line 1462
    .restart local v1    # "eqControlText":Landroid/widget/TextView;
    sget-object v4, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->EQ_STRING_RES_ID:[I

    aget v4, v4, v3

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(I)V

    .line 1465
    iget-object v5, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mEqControlBar:[Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;

    aget-object v4, v2, v3

    const v6, 0x7f0d01bd

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;

    aput-object v4, v5, v3

    .line 1466
    iget-object v4, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mEqControlBar:[Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;

    aget-object v4, v4, v3

    sget-object v5, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->EQ_STRING_RES_ID:[I

    aget v5, v5, v3

    invoke-virtual {v4, v5}, Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;->setTitle(I)V

    .line 1467
    iget-object v4, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mEqControlBar:[Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;

    aget-object v5, v4, v3

    if-nez v0, :cond_2

    const/4 v4, 0x0

    :goto_2
    add-int/lit8 v4, v4, 0xa

    invoke-virtual {v5, v4}, Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;->setProgress(I)V

    .line 1469
    iget-object v4, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mEqControlBar:[Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;

    aget-object v4, v4, v3

    const/16 v5, 0xa

    invoke-virtual {v4, v5}, Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;->setBaseValue(I)V

    .line 1470
    iget-object v4, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mEqControlBar:[Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;

    aget-object v4, v4, v3

    new-instance v5, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqControlBarChangeListener;

    const/4 v6, 0x0

    invoke-direct {v5, p0, v6}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqControlBarChangeListener;-><init>(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;Lcom/samsung/musicplus/settings/SoundAliveV2Setting$1;)V

    invoke-virtual {v4, v5}, Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 1476
    iget-object v4, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mEqControlBar:[Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;

    aget-object v4, v4, v3

    iget-object v5, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mEqControlBarKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v4, v5}, Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 1477
    iget-object v4, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mEqControlBar:[Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;

    aget-object v4, v4, v3

    invoke-virtual {v4}, Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;->setContentDescription()V

    .line 1458
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 1449
    .end local v0    # "currentUserEq":[I
    .end local v1    # "eqControlText":Landroid/widget/TextView;
    .end local v2    # "eqControlView":[Landroid/view/View;
    .end local v3    # "i":I
    :cond_0
    if-eqz p2, :cond_1

    .line 1450
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mEntranceBandLevel:[I

    .restart local v0    # "currentUserEq":[I
    goto :goto_0

    .line 1452
    .end local v0    # "currentUserEq":[I
    :cond_1
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->loadNewSoundAliveBandLevel()[I

    move-result-object v0

    .restart local v0    # "currentUserEq":[I
    goto :goto_0

    .line 1467
    .restart local v1    # "eqControlText":Landroid/widget/TextView;
    .restart local v2    # "eqControlView":[Landroid/view/View;
    .restart local v3    # "i":I
    :cond_2
    aget v4, v0, v3

    goto :goto_2

    .line 1479
    :cond_3
    return-void
.end method

.method private loadPrevPreferences(Landroid/os/Bundle;Z)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;
    .param p2, "isReload"    # Z

    .prologue
    .line 269
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->getAutoState(Landroid/os/Bundle;)Z

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->setCheckedAuto(Z)V

    .line 271
    invoke-direct {p0, p1, p2}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->loadUsePresetPref(Landroid/os/Bundle;Z)V

    .line 272
    invoke-direct {p0, p1, p2}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->loadBandLevelPref(Landroid/os/Bundle;Z)V

    .line 274
    iget-object v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mCheckBoxAuto:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 275
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getGenre()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/musicplus/library/audio/SoundAliveV2;->getSoundaliveAudioEffect(Ljava/lang/String;)I

    move-result v0

    .line 276
    .local v0, "position":I
    iget-object v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSoundAlivePresetGridview:Landroid/widget/GridView;

    invoke-direct {p0, v1, v0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->selectSoundAlivePresetCell(Landroid/widget/GridView;I)V

    .line 277
    if-eqz p2, :cond_0

    .line 278
    invoke-direct {p0, v0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->setSquareEffect(I)V

    .line 295
    .end local v0    # "position":I
    :cond_0
    :goto_0
    return-void

    .line 281
    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->loadSquarePositionPref(Landroid/os/Bundle;Z)V

    .line 284
    if-eqz p2, :cond_3

    sget v1, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSquarePosition:I

    const/4 v2, -0x1

    if-le v1, v2, :cond_3

    .line 285
    sget v1, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSquarePosition:I

    invoke-direct {p0, v1}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->setSquareEffect(I)V

    .line 292
    :cond_2
    :goto_1
    iget-object v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mPrevValues:[I

    invoke-static {v1}, Lcom/samsung/musicplus/util/ServiceUtils;->setNewSoundAliveBandLevel([I)V

    .line 293
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->loadStrengthPref(Landroid/os/Bundle;)V

    goto :goto_0

    .line 286
    :cond_3
    iget v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mCurrentTab:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_4

    iget-boolean v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mDoSetPrevEffect:Z

    if-eqz v1, :cond_4

    iget-boolean v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSquareCellSelected:Z

    if-eqz v1, :cond_4

    .line 287
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->setPrevBandLevelEffect()V

    .line 288
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSquareCellSelected:Z

    goto :goto_1

    .line 289
    :cond_4
    iget v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mCurrentTab:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    iget-boolean v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mDoSetPrevEffect:Z

    if-eqz v1, :cond_2

    .line 290
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->setPrevBandLevelEffect()V

    goto :goto_1
.end method

.method private loadSquarePositionPref(Landroid/os/Bundle;Z)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;
    .param p2, "isReload"    # Z

    .prologue
    .line 324
    if-eqz p1, :cond_1

    .line 325
    const-string v1, "square_position"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    sput v1, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSquarePosition:I

    .line 333
    :cond_0
    :goto_0
    return-void

    .line 327
    :cond_1
    const/4 v0, 0x0

    .line 328
    .local v0, "squarePosition":[I
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->loadNewSoundAliveSquarePosition()[I

    move-result-object v0

    .line 329
    if-eqz v0, :cond_0

    .line 330
    const/4 v1, 0x0

    aget v1, v0, v1

    mul-int/lit8 v1, v1, 0x5

    const/4 v2, 0x1

    aget v2, v0, v2

    add-int/2addr v1, v2

    sput v1, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSquarePosition:I

    goto :goto_0
.end method

.method private loadStrengthPref(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x1

    .line 357
    if-eqz p1, :cond_0

    .line 358
    const-string v2, "strength"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v0

    .line 363
    .local v0, "currentStrength":[I
    :goto_0
    if-eqz v0, :cond_2

    .line 364
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    const/4 v2, 0x3

    if-ge v1, v2, :cond_2

    .line 365
    aget v2, v0, v1

    if-ne v2, v3, :cond_1

    .line 366
    invoke-direct {p0, v1, v3}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->setStrength(II)V

    .line 364
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 360
    .end local v0    # "currentStrength":[I
    .end local v1    # "i":I
    :cond_0
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->loadNewSoundAliveStrength()[I

    move-result-object v0

    .restart local v0    # "currentStrength":[I
    goto :goto_0

    .line 368
    .restart local v1    # "i":I
    :cond_1
    const/4 v2, 0x2

    invoke-direct {p0, v1, v2}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->setStrength(II)V

    goto :goto_2

    .line 372
    .end local v1    # "i":I
    :cond_2
    return-void
.end method

.method private loadUsePresetPref(Landroid/os/Bundle;Z)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;
    .param p2, "isReload"    # Z

    .prologue
    .line 347
    if-eqz p1, :cond_0

    .line 348
    const-string v0, "use_preset"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSelectedUsePreset:I

    .line 352
    :goto_0
    iget v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSelectedUsePreset:I

    invoke-direct {p0, v0, p2}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->setUsePresetEffect(IZ)V

    .line 353
    return-void

    .line 350
    :cond_0
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->loadNewSoundAliveUsePreset()I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSelectedUsePreset:I

    goto :goto_0
.end method

.method private matchAlign()V
    .locals 14

    .prologue
    const/4 v11, 0x0

    const/high16 v13, 0x40000000    # 2.0f

    .line 893
    const v10, 0x7f0d0182

    invoke-virtual {p0, v10}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 894
    .local v0, "leftTextView":Landroid/widget/TextView;
    const v10, 0x7f0d0183

    invoke-virtual {p0, v10}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 896
    .local v5, "rightTextView":Landroid/widget/TextView;
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 897
    .local v3, "rectForLeft":Landroid/graphics/Rect;
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    .line 899
    .local v4, "rectForRight":Landroid/graphics/Rect;
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v10

    invoke-interface {v10}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v6

    .line 900
    .local v6, "textForLeft":Ljava/lang/String;
    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v10

    invoke-interface {v10}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v7

    .line 902
    .local v7, "textForRight":Ljava/lang/String;
    invoke-virtual {v0}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v1

    .line 903
    .local v1, "paintForLeftText":Landroid/graphics/Paint;
    invoke-virtual {v5}, Landroid/widget/TextView;->getPaint()Landroid/text/TextPaint;

    move-result-object v2

    .line 905
    .local v2, "paintForRightText":Landroid/graphics/Paint;
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v10

    invoke-interface {v10}, Ljava/lang/CharSequence;->length()I

    move-result v10

    invoke-virtual {v1, v6, v11, v10, v3}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 907
    invoke-virtual {v5}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v10

    invoke-interface {v10}, Ljava/lang/CharSequence;->length()I

    move-result v10

    invoke-virtual {v2, v7, v11, v10, v4}, Landroid/graphics/Paint;->getTextBounds(Ljava/lang/String;IILandroid/graphics/Rect;)V

    .line 910
    invoke-virtual {v1, v6}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v8

    .line 911
    .local v8, "widthForLeftText":F
    invoke-virtual {v2, v7}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v9

    .line 915
    .local v9, "widthForRightText":F
    iget v10, v4, Landroid/graphics/Rect;->top:I

    iget v11, v4, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v10, v11

    invoke-static {v10}, Ljava/lang/Math;->abs(I)I

    move-result v10

    int-to-float v10, v10

    sub-float v10, v9, v10

    div-float/2addr v10, v13

    sub-float/2addr v10, v13

    invoke-virtual {v5, v10}, Landroid/widget/TextView;->setTranslationX(F)V

    .line 920
    neg-float v10, v8

    iget v11, v3, Landroid/graphics/Rect;->top:I

    iget v12, v3, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v11, v12

    invoke-static {v11}, Ljava/lang/Math;->abs(I)I

    move-result v11

    int-to-float v11, v11

    add-float/2addr v10, v11

    div-float/2addr v10, v13

    const/high16 v11, 0x40800000    # 4.0f

    add-float/2addr v10, v11

    invoke-virtual {v0, v10}, Landroid/widget/TextView;->setTranslationX(F)V

    .line 922
    return-void
.end method

.method private resetCurrentValue()V
    .locals 6

    .prologue
    const/16 v5, 0xc

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1507
    invoke-direct {p0, v3}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->setCheckedAuto(Z)V

    .line 1509
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v1, 0x7

    if-ge v0, v1, :cond_0

    .line 1510
    iput-boolean v4, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mIsReset:Z

    .line 1511
    iget-object v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mEqControlBar:[Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;

    aget-object v1, v1, v0

    const/16 v2, 0xa

    invoke-virtual {v1, v2}, Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;->setProgress(I)V

    .line 1509
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1513
    :cond_0
    const/4 v0, 0x0

    :goto_1
    const/4 v1, 0x3

    if-ge v0, v1, :cond_1

    .line 1514
    const/4 v1, 0x2

    invoke-direct {p0, v0, v1}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->setStrength(II)V

    .line 1513
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1517
    :cond_1
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->setPrevBandLevelEffect()V

    .line 1518
    iget-object v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSoundAlivePresetGridview:Landroid/widget/GridView;

    invoke-direct {p0, v1}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->clearAllsoundAlivePresetCell(Landroid/widget/GridView;)V

    .line 1519
    iget-object v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSoundAlivePresetGridview:Landroid/widget/GridView;

    invoke-direct {p0, v1, v5}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->selectSoundAlivePresetCell(Landroid/widget/GridView;I)V

    .line 1520
    invoke-direct {p0, v5}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->setSquareEffect(I)V

    .line 1521
    invoke-direct {p0, v3, v4}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->setUsePresetEffect(IZ)V

    .line 1522
    return-void
.end method

.method private saveEntranceValue()V
    .locals 3

    .prologue
    .line 925
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v1, 0x7

    if-ge v0, v1, :cond_0

    .line 926
    iget-object v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mEntranceBandLevel:[I

    invoke-static {v0}, Lcom/samsung/musicplus/util/ServiceUtils;->getNewSoundAliveBandLevel(I)I

    move-result v2

    aput v2, v1, v0

    .line 925
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 928
    :cond_0
    iget-object v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mCheckBoxAuto:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    iput-boolean v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mEntranceAutoChecked:Z

    .line 929
    return-void
.end method

.method private saveNewSoundAlivePreferences(I[I[I[IZ)V
    .locals 10
    .param p1, "usePreset"    # I
    .param p2, "squarePosition"    # [I
    .param p3, "bandLevel"    # [I
    .param p4, "strength"    # [I
    .param p5, "auto"    # Z

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    const/4 v7, 0x2

    .line 1804
    const-string v4, "sa_use_preset_effect"

    invoke-static {v4, p1}, Lcom/samsung/musicplus/util/ServiceUtils;->savePreferencesInt(Ljava/lang/String;I)V

    .line 1807
    if-eqz p2, :cond_1

    .line 1808
    const-string v2, ""

    .line 1809
    .local v2, "squarePositionString":Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v7, :cond_0

    .line 1810
    const-string v4, "%s%d|"

    new-array v5, v7, [Ljava/lang/Object;

    aput-object v2, v5, v8

    aget v6, p2, v1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v9

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1809
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1813
    :cond_0
    const-string v4, "sa_square_position"

    invoke-static {v4, v2}, Lcom/samsung/musicplus/util/ServiceUtils;->savePreferencesString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1818
    .end local v1    # "i":I
    .end local v2    # "squarePositionString":Ljava/lang/String;
    :cond_1
    if-eqz p3, :cond_3

    .line 1819
    const-string v0, ""

    .line 1820
    .local v0, "bandLevelString":Ljava/lang/String;
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    const/4 v4, 0x7

    if-ge v1, v4, :cond_2

    .line 1821
    const-string v4, "%s%d|"

    new-array v5, v7, [Ljava/lang/Object;

    aput-object v0, v5, v8

    aget v6, p3, v1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v9

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1820
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1823
    :cond_2
    const-string v4, "sa_band_level"

    invoke-static {v4, v0}, Lcom/samsung/musicplus/util/ServiceUtils;->savePreferencesString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1828
    .end local v0    # "bandLevelString":Ljava/lang/String;
    .end local v1    # "i":I
    :cond_3
    if-eqz p4, :cond_5

    .line 1829
    const-string v3, ""

    .line 1830
    .local v3, "strengthString":Ljava/lang/String;
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    const/4 v4, 0x3

    if-ge v1, v4, :cond_4

    .line 1831
    const-string v4, "%s%d|"

    new-array v5, v7, [Ljava/lang/Object;

    aput-object v3, v5, v8

    aget v6, p4, v1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v9

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 1830
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1833
    :cond_4
    const-string v4, "sa_use_strength"

    invoke-static {v4, v3}, Lcom/samsung/musicplus/util/ServiceUtils;->savePreferencesString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1837
    .end local v1    # "i":I
    .end local v3    # "strengthString":Ljava/lang/String;
    :cond_5
    const-string v4, "sa_auto_check"

    invoke-static {v4, p5}, Lcom/samsung/musicplus/util/ServiceUtils;->savePreferencesBoolean(Ljava/lang/String;Z)V

    .line 1838
    return-void
.end method

.method private saveSoundAlivePreference()V
    .locals 6

    .prologue
    .line 1041
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->getCurrentValue(I)[I

    move-result-object v3

    .line 1042
    .local v3, "bandLevel":[I
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->getSquarePosition()[I

    move-result-object v2

    .line 1043
    .local v2, "squarePosition":[I
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->getStrengthValue()[I

    move-result-object v4

    .line 1045
    .local v4, "strength":[I
    iget v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSelectedUsePreset:I

    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mCheckBoxAuto:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v5

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->saveNewSoundAlivePreferences(I[I[I[IZ)V

    .line 1047
    return-void
.end method

.method private selectSoundAlivePresetCell(Landroid/widget/GridView;I)V
    .locals 4
    .param p1, "v"    # Landroid/widget/GridView;
    .param p2, "position"    # I

    .prologue
    .line 871
    if-eqz p1, :cond_0

    .line 872
    invoke-virtual {p1, p2}, Landroid/widget/GridView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 873
    .local v0, "child":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 874
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/view/View;->setSelected(Z)V

    .line 875
    const v2, 0x7f0d0184

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 877
    .local v1, "image":Landroid/widget/ImageView;
    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0200bc

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 881
    .end local v0    # "child":Landroid/view/View;
    .end local v1    # "image":Landroid/widget/ImageView;
    :cond_0
    return-void
.end method

.method private setAdvancedBandLevel()V
    .locals 3

    .prologue
    .line 1415
    iget-object v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

    invoke-virtual {v1}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->isHDMIConnect()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1416
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->resetCurrentValue()V

    .line 1417
    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f100168

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1418
    .local v0, "msg":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mToast:Landroid/widget/Toast;

    if-nez v1, :cond_0

    .line 1419
    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mToast:Landroid/widget/Toast;

    .line 1423
    :goto_0
    iget-object v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mToast:Landroid/widget/Toast;

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 1427
    .end local v0    # "msg":Ljava/lang/String;
    :goto_1
    return-void

    .line 1421
    .restart local v0    # "msg":Ljava/lang/String;
    :cond_0
    iget-object v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mToast:Landroid/widget/Toast;

    invoke-virtual {v1, v0}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1426
    .end local v0    # "msg":Ljava/lang/String;
    :cond_1
    const/4 v1, 0x2

    invoke-direct {p0, v1}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->getCurrentValue(I)[I

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/musicplus/util/ServiceUtils;->setNewSoundAliveBandLevel([I)V

    goto :goto_1
.end method

.method private setButtonStyle(Landroid/widget/TextView;Landroid/widget/TextView;)V
    .locals 6
    .param p1, "btn1"    # Landroid/widget/TextView;
    .param p2, "btn2"    # Landroid/widget/TextView;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1303
    invoke-virtual {p1, v5}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 1304
    invoke-virtual {p1, v5}, Landroid/widget/TextView;->setSelected(Z)V

    .line 1306
    const-string v0, "sec-roboto-regular"

    invoke-static {v0, v4}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 1308
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const v1, 0x7f1001a6

    new-array v2, v5, [Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p0, v1, v2}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/widget/TextView;->getTag()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1312
    invoke-virtual {p2, v5}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 1313
    invoke-virtual {p2, v4}, Landroid/widget/TextView;->setSelected(Z)V

    .line 1315
    const-string v0, "sec-roboto-light"

    invoke-static {v0, v4}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 1317
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const v1, 0x7f1001a5

    new-array v2, v5, [Ljava/lang/Object;

    invoke-virtual {p2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {p0, v1, v2}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p2}, Landroid/widget/TextView;->getTag()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1319
    return-void
.end method

.method private setCheckedAuto(Z)V
    .locals 1
    .param p1, "auto"    # Z

    .prologue
    .line 1525
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mCheckBoxAuto:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 1526
    invoke-static {p1}, Lcom/samsung/musicplus/util/ServiceUtils;->setNewSoundAliveAuto(Z)V

    .line 1527
    return-void
.end method

.method private setPrevBandLevelEffect()V
    .locals 4

    .prologue
    .line 298
    const/4 v3, 0x7

    new-array v0, v3, [I

    .line 300
    .local v0, "eqValue":[I
    iget-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mEqControlBar:[Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;

    if-eqz v3, :cond_1

    if-eqz v0, :cond_1

    .line 301
    array-length v2, v0

    .line 302
    .local v2, "length":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 303
    iget-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mEqControlBar:[Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;

    aget-object v3, v3, v1

    if-eqz v3, :cond_0

    .line 304
    iget-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mEqControlBar:[Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;

    aget-object v3, v3, v1

    invoke-virtual {v3}, Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;->getProgress()I

    move-result v3

    add-int/lit8 v3, v3, -0xa

    aput v3, v0, v1

    .line 305
    invoke-static {v0}, Lcom/samsung/musicplus/util/ServiceUtils;->setNewSoundAliveBandLevel([I)V

    .line 302
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 309
    .end local v1    # "i":I
    .end local v2    # "length":I
    :cond_1
    return-void
.end method

.method private setSelectedTab(I)V
    .locals 4
    .param p1, "tab"    # I

    .prologue
    const/16 v1, 0x8

    const/4 v0, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1268
    if-ne p1, v3, :cond_1

    .line 1269
    iput v3, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mCurrentTab:I

    .line 1270
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mBasicLayout:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1271
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mAdvancedLayout:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1272
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mBasicButton:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mAdvancedButton:Landroid/widget/TextView;

    invoke-direct {p0, v0, v1}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->setButtonStyle(Landroid/widget/TextView;Landroid/widget/TextView;)V

    .line 1273
    sput-boolean v2, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mLastControlledTab:Z

    .line 1281
    :cond_0
    :goto_0
    return-void

    .line 1274
    :cond_1
    if-ne p1, v0, :cond_0

    .line 1275
    iput v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mCurrentTab:I

    .line 1276
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mBasicLayout:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1277
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mAdvancedLayout:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 1278
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mAdvancedButton:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mBasicButton:Landroid/widget/TextView;

    invoke-direct {p0, v0, v1}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->setButtonStyle(Landroid/widget/TextView;Landroid/widget/TextView;)V

    .line 1279
    sput-boolean v3, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mLastControlledTab:Z

    goto :goto_0
.end method

.method private setSquareEffect(I)V
    .locals 4
    .param p1, "position"    # I

    .prologue
    const/4 v3, 0x5

    .line 845
    sget-object v0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setSquareEffect square Index : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 846
    const/4 v0, -0x1

    if-le p1, v0, :cond_0

    .line 847
    const/4 v0, 0x2

    new-array v0, v0, [I

    const/4 v1, 0x0

    invoke-direct {p0, p1, v3}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->getSquareRow(II)I

    move-result v2

    aput v2, v0, v1

    const/4 v1, 0x1

    invoke-direct {p0, p1, v3}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->getSquareColumn(II)I

    move-result v2

    aput v2, v0, v1

    invoke-static {v0}, Lcom/samsung/musicplus/util/ServiceUtils;->setNewSoundAliveSquarePosition([I)V

    .line 851
    sput p1, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSquarePosition:I

    .line 853
    :cond_0
    return-void
.end method

.method private setStrength(II)V
    .locals 5
    .param p1, "id"    # I
    .param p2, "mode"    # I

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 457
    const/4 v0, 0x0

    .line 459
    .local v0, "button":Landroid/widget/TextView;
    packed-switch p1, :pswitch_data_0

    .line 501
    :cond_0
    :goto_0
    return-void

    .line 461
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mNewSoundAliveButton3D:Landroid/widget/TextView;

    .line 473
    :goto_1
    if-eqz v0, :cond_0

    .line 474
    if-nez p2, :cond_3

    .line 475
    invoke-virtual {v0}, Landroid/widget/TextView;->isSelected()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 476
    invoke-static {p1, v3}, Lcom/samsung/musicplus/util/ServiceUtils;->setNewSoundAliveEachStrength(II)V

    .line 477
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setSelected(Z)V

    .line 494
    :cond_1
    :goto_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const v2, 0x7f100181

    invoke-virtual {p0, v2}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/widget/TextView;->isSelected()Z

    move-result v1

    if-eqz v1, :cond_7

    const v1, 0x7f10019e

    :goto_3
    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 464
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mNewSoundAliveButtonBass:Landroid/widget/TextView;

    .line 465
    goto :goto_1

    .line 467
    :pswitch_2
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mNewSoundAliveButtonClarity:Landroid/widget/TextView;

    .line 468
    goto :goto_1

    .line 479
    :cond_2
    invoke-static {p1, v2}, Lcom/samsung/musicplus/util/ServiceUtils;->setNewSoundAliveEachStrength(II)V

    .line 480
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setSelected(Z)V

    goto :goto_2

    .line 482
    :cond_3
    if-ne p2, v2, :cond_5

    .line 483
    iget v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mCurrentTab:I

    if-ne v1, v4, :cond_4

    .line 484
    invoke-static {p1, v2}, Lcom/samsung/musicplus/util/ServiceUtils;->setNewSoundAliveEachStrength(II)V

    .line 486
    :cond_4
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setSelected(Z)V

    goto :goto_2

    .line 487
    :cond_5
    if-ne p2, v4, :cond_1

    .line 488
    iget v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mCurrentTab:I

    if-ne v1, v4, :cond_6

    .line 489
    invoke-static {p1, v3}, Lcom/samsung/musicplus/util/ServiceUtils;->setNewSoundAliveEachStrength(II)V

    .line 491
    :cond_6
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setSelected(Z)V

    goto :goto_2

    .line 494
    :cond_7
    const v1, 0x7f100192

    goto :goto_3

    .line 459
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private setUsePresetEffect(IZ)V
    .locals 3
    .param p1, "selectedUsePreset"    # I
    .param p2, "isReload"    # Z

    .prologue
    .line 508
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->clearEffectImage()V

    .line 510
    packed-switch p1, :pswitch_data_0

    .line 544
    :goto_0
    iput p1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSelectedUsePreset:I

    .line 545
    if-eqz p2, :cond_0

    .line 546
    invoke-static {p1}, Lcom/samsung/musicplus/util/ServiceUtils;->setNewSoundAliveUsePreset(I)V

    .line 548
    :cond_0
    return-void

    .line 512
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mImageViewEffctClub:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02008f

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 514
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSoundAliveEffecClub:Landroid/widget/RelativeLayout;

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->updateEffectBottomLineImage(Landroid/widget/RelativeLayout;)V

    goto :goto_0

    .line 517
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mImageViewEffctConcertHall:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02008b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 519
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSoundAliveEffecConcertHall:Landroid/widget/RelativeLayout;

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->updateEffectBottomLineImage(Landroid/widget/RelativeLayout;)V

    goto :goto_0

    .line 522
    :pswitch_2
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mImageViewEffctNone:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02008d

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 524
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSoundAliveEffectNone:Landroid/widget/RelativeLayout;

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->updateEffectBottomLineImage(Landroid/widget/RelativeLayout;)V

    goto :goto_0

    .line 527
    :pswitch_3
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mImageViewEffctStudio:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020091

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 529
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSoundAliveEffectStudio:Landroid/widget/RelativeLayout;

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->updateEffectBottomLineImage(Landroid/widget/RelativeLayout;)V

    goto :goto_0

    .line 532
    :pswitch_4
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mImageViewEffctTubeAmpEffect:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020093

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 534
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSoundAliveEffecTubeAmpEffect:Landroid/widget/RelativeLayout;

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->updateEffectBottomLineImage(Landroid/widget/RelativeLayout;)V

    goto :goto_0

    .line 537
    :pswitch_5
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mImageViewEffctVirtual71ch:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020095

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 539
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSoundAliveEffecVirtual71ch:Landroid/widget/RelativeLayout;

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->updateEffectBottomLineImage(Landroid/widget/RelativeLayout;)V

    goto/16 :goto_0

    .line 510
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_4
        :pswitch_5
        :pswitch_3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private updateAutoPlay()V
    .locals 1

    .prologue
    .line 1120
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mCheckBoxAuto:Landroid/widget/CheckBox;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mCheckBoxAuto:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1121
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->updateSoundAliveValues()V

    .line 1123
    :cond_0
    return-void
.end method

.method private updateBandLevel()V
    .locals 5

    .prologue
    .line 1242
    const/4 v0, 0x0

    .line 1244
    .local v0, "bandLevel":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/4 v2, 0x7

    if-ge v1, v2, :cond_0

    .line 1245
    invoke-static {v1}, Lcom/samsung/musicplus/util/ServiceUtils;->getNewSoundAliveBandLevel(I)I

    move-result v0

    .line 1247
    iget-object v2, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mEqControlBar:[Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;

    aget-object v2, v2, v1

    add-int/lit8 v3, v0, 0xa

    invoke-virtual {v2, v3}, Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;->setProgress(I)V

    .line 1248
    iget-object v2, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mEqControlBar:[Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;

    aget-object v2, v2, v1

    invoke-virtual {v2}, Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;->setContentDescription()V

    .line 1249
    sget-object v2, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->CLASSNAME:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "UpdateBandLevel bandNum : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Level : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1244
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1252
    :cond_0
    iget-object v2, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mBubble:Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;

    invoke-virtual {v2}, Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;->isShown()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1253
    iget-object v2, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mBubble:Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;

    invoke-virtual {v2}, Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;->hideBubble()V

    .line 1255
    :cond_1
    return-void
.end method

.method private updateEffectBottomLineImage(Landroid/widget/RelativeLayout;)V
    .locals 3
    .param p1, "layout"    # Landroid/widget/RelativeLayout;

    .prologue
    .line 551
    const v1, 0x7f0d0173

    invoke-virtual {p1, v1}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 552
    .local v0, "image":Landroid/widget/ImageView;
    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0073

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 554
    const v1, 0x7f10019e

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 555
    return-void
.end method

.method private updateSoundAliveValues()V
    .locals 0

    .prologue
    .line 1126
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->updateSquarePosition()V

    .line 1127
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->updateBandLevel()V

    .line 1128
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->updateStrength()V

    .line 1129
    return-void
.end method

.method private updateSquarePosition()V
    .locals 2

    .prologue
    .line 1228
    iget-object v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSoundAlivePresetGridview:Landroid/widget/GridView;

    if-eqz v1, :cond_0

    .line 1229
    iget-object v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mCheckBoxAuto:Landroid/widget/CheckBox;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mCheckBoxAuto:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1230
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getGenre()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/musicplus/library/audio/SoundAliveV2;->getSoundaliveAudioEffect(Ljava/lang/String;)I

    move-result v0

    .line 1231
    .local v0, "position":I
    iget-object v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSoundAlivePresetGridview:Landroid/widget/GridView;

    invoke-direct {p0, v1}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->clearAllsoundAlivePresetCell(Landroid/widget/GridView;)V

    .line 1232
    iget-object v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSoundAlivePresetGridview:Landroid/widget/GridView;

    invoke-direct {p0, v1, v0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->selectSoundAlivePresetCell(Landroid/widget/GridView;I)V

    .line 1233
    invoke-direct {p0, v0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->setSquareEffect(I)V

    .line 1236
    .end local v0    # "position":I
    :cond_0
    return-void
.end method

.method private updateStrength()V
    .locals 2

    .prologue
    .line 1258
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v1, 0x3

    if-ge v0, v1, :cond_1

    .line 1259
    invoke-static {v0}, Lcom/samsung/musicplus/util/ServiceUtils;->getNewSoundAliveRoundedStrength(I)I

    move-result v1

    if-eqz v1, :cond_0

    .line 1260
    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->setStrength(II)V

    .line 1258
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1262
    :cond_0
    const/4 v1, 0x2

    invoke-direct {p0, v0, v1}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->setStrength(II)V

    goto :goto_1

    .line 1265
    :cond_1
    return-void
.end method

.method private updateValuesFromService()V
    .locals 3

    .prologue
    .line 1111
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getNewSoundAliveCurrentPreset()I

    move-result v1

    iput v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSelectedUsePreset:I

    .line 1112
    iget v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSelectedUsePreset:I

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->setUsePresetEffect(IZ)V

    .line 1113
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getNewSoundAliveSquarePosition()[I

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->getSquarePosition([I)I

    move-result v0

    .line 1114
    .local v0, "position":I
    iget-object v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSoundAlivePresetGridview:Landroid/widget/GridView;

    invoke-direct {p0, v1, v0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->selectSoundAlivePresetCell(Landroid/widget/GridView;I)V

    .line 1115
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->updateBandLevel()V

    .line 1116
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->updateStrength()V

    .line 1117
    return-void
.end method


# virtual methods
.method public onBackPressed()V
    .locals 0

    .prologue
    .line 1021
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->saveSoundAlivePreference()V

    .line 1033
    invoke-super {p0}, Lcom/samsung/musicplus/MusicBaseActivity;->onBackPressed()V

    .line 1034
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x0

    .line 211
    sget-object v1, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->CLASSNAME:Ljava/lang/String;

    const-string v2, " onCreate"

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    invoke-super {p0, p1}, Lcom/samsung/musicplus/MusicBaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 213
    if-nez p1, :cond_0

    .line 217
    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "SDAL"

    invoke-static {v1, v2}, Lcom/samsung/musicplus/util/logging/FeatureLogger;->insertLog(Landroid/content/Context;Ljava/lang/String;)V

    .line 219
    :cond_0
    const v1, 0x7f04006f

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->setContentView(I)V

    .line 221
    sput-boolean v4, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->sMusicPlayerEntrance:Z

    .line 223
    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->getInstance(Landroid/content/Context;)Lcom/samsung/musicplus/library/audio/SecAudioManager;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

    .line 229
    const v1, 0x7f0d0149

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mBasicLayout:Landroid/view/ViewGroup;

    .line 231
    const v1, 0x7f0d013f

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mAdvancedLayout:Landroid/view/ViewGroup;

    .line 233
    const v1, 0x7f0d013a

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mBasicButton:Landroid/widget/TextView;

    .line 234
    const v1, 0x7f0d013b

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mAdvancedButton:Landroid/widget/TextView;

    .line 236
    const v1, 0x7f0d013e

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;

    iput-object v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mBubble:Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;

    .line 237
    iget-object v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mBubble:Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;

    const v2, 0x7f0200b9

    invoke-virtual {v1, v2}, Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;->setBubbleBackgroundDrawable(I)V

    .line 239
    iget-object v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mBubble:Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;

    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c0023

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v1, v2}, Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;->setBubbleFontSize(F)V

    .line 240
    iget-object v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mBubble:Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;->setBubbleAlign(I)V

    .line 242
    const v1, 0x7f0d0176

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mImageViewEffctTubeAmpEffect:Landroid/widget/ImageView;

    .line 243
    const v1, 0x7f0d0178

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mImageViewEffctVirtual71ch:Landroid/widget/ImageView;

    .line 244
    const v1, 0x7f0d0174

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mImageViewEffctNone:Landroid/widget/ImageView;

    .line 245
    const v1, 0x7f0d017a

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mImageViewEffctStudio:Landroid/widget/ImageView;

    .line 246
    const v1, 0x7f0d017c

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mImageViewEffctClub:Landroid/widget/ImageView;

    .line 247
    const v1, 0x7f0d017e

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mImageViewEffctConcertHall:Landroid/widget/ImageView;

    .line 248
    const v1, 0x7f0d017f

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iput-object v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mCheckBoxAuto:Landroid/widget/CheckBox;

    .line 250
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->initLayout(Landroid/os/Bundle;)V

    .line 251
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->initSoundAliveGridView()V

    .line 252
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->initPresetEffect()V

    .line 253
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->initStrengthEffect()V

    .line 254
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->drawDBLine()V

    .line 256
    invoke-direct {p0, p1, v4}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->loadPrevPreferences(Landroid/os/Bundle;Z)V

    .line 258
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 259
    .local v0, "f":Landroid/content/IntentFilter;
    const-string v1, "com.samsung.musicplus.action.AUDIO_PATH_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 260
    const-string v1, "com.samsung.musicplus.action.SOUND_AVLIE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 261
    const-string v1, "com.android.music.playstatechanged"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 262
    const-string v1, "com.android.music.metachanged"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 263
    iget-object v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mStateReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 264
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->saveEntranceValue()V

    .line 265
    const/4 v1, 0x2

    invoke-direct {p0, v1}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->getCurrentValue(I)[I

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mPrevValues:[I

    .line 266
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 989
    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    const v1, 0x7f12000c

    invoke-virtual {v0, v1, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 990
    invoke-super {p0, p1}, Lcom/samsung/musicplus/MusicBaseActivity;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 968
    sget-object v0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->CLASSNAME:Ljava/lang/String;

    const-string v1, " onDestroy"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 969
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mStateReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 970
    invoke-super {p0}, Lcom/samsung/musicplus/MusicBaseActivity;->onDestroy()V

    .line 971
    return-void
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 1794
    const/16 v0, 0x52

    if-ne v0, p1, :cond_0

    .line 1795
    const/4 v0, 0x1

    .line 1797
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/samsung/musicplus/MusicBaseActivity;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    const/4 v1, 0x1

    .line 995
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 1015
    :goto_0
    invoke-super {p0, p1}, Lcom/samsung/musicplus/MusicBaseActivity;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    return v0

    .line 997
    :sswitch_0
    iput-boolean v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mDoSetPrevEffect:Z

    .line 998
    iput-boolean v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSquareCellSelected:Z

    .line 999
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->resetCurrentValue()V

    goto :goto_0

    .line 1002
    :sswitch_1
    iput-boolean v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mDoSetPrevEffect:Z

    .line 1003
    iput-boolean v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->isCancel:Z

    .line 1004
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->loadPrevPreferences(Landroid/os/Bundle;Z)V

    .line 1005
    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->finish()V

    goto :goto_0

    .line 1009
    :sswitch_2
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->saveSoundAlivePreference()V

    .line 1010
    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->finish()V

    goto :goto_0

    .line 995
    :sswitch_data_0
    .sparse-switch
        0x102002c -> :sswitch_2
        0x7f0d01ca -> :sswitch_2
        0x7f0d01ff -> :sswitch_0
        0x7f0d0200 -> :sswitch_1
    .end sparse-switch
.end method

.method protected onPause()V
    .locals 0

    .prologue
    .line 951
    invoke-super {p0}, Lcom/samsung/musicplus/MusicBaseActivity;->onPause()V

    .line 952
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 956
    invoke-super {p0}, Lcom/samsung/musicplus/MusicBaseActivity;->onResume()V

    .line 961
    sget-boolean v0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->sMusicPlayerEntrance:Z

    if-eqz v0, :cond_0

    .line 962
    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->finish()V

    .line 964
    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 975
    const-string v0, "tab"

    iget v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mCurrentTab:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 976
    const-string v0, "band_level"

    const/4 v1, 0x2

    invoke-direct {p0, v1}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->getCurrentValue(I)[I

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    .line 977
    const-string v0, "use_preset"

    iget v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSelectedUsePreset:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 978
    const-string v0, "square_position"

    sget v1, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSquarePosition:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 979
    const-string v0, "strength"

    invoke-direct {p0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->getStrengthValue()[I

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    .line 980
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mCheckBoxAuto:Landroid/widget/CheckBox;

    if-eqz v0, :cond_0

    .line 981
    const-string v0, "auto_state"

    iget-object v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mCheckBoxAuto:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 984
    :cond_0
    invoke-super {p0, p1}, Lcom/samsung/musicplus/MusicBaseActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 985
    return-void
.end method
