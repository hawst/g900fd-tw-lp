.class Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$3;
.super Ljava/lang/Object;
.source "MyMusicMenuReorderListView.java"

# interfaces
.implements Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$DropListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;)V
    .locals 0

    .prologue
    .line 570
    iput-object p1, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$3;->this$0:Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public drop(II)V
    .locals 4
    .param p1, "from"    # I
    .param p2, "to"    # I

    .prologue
    .line 573
    if-eq p1, p2, :cond_1

    .line 574
    iget-object v2, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$3;->this$0:Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;

    invoke-virtual {v2}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v2

    check-cast v2, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;

    invoke-virtual {v2}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->getArrayList()Ljava/util/ArrayList;

    move-result-object v0

    .line 576
    .local v0, "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$ViewItem;>;"
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$ViewItem;

    .line 577
    .local v1, "vh":Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$ViewItem;
    invoke-virtual {v0, p2, v1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 578
    iget-object v2, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$3;->this$0:Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;

    iget-object v2, v2, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mAdapter:Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;

    # setter for: Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->mArray:Ljava/util/ArrayList;
    invoke-static {v2, v0}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->access$002(Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 579
    iget-object v2, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$3;->this$0:Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;

    iget-object v2, v2, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mAdapter:Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;

    invoke-virtual {v2}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->notifyDataSetChanged()V

    .line 580
    iget-object v2, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$3;->this$0:Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;

    # getter for: Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->access$400(Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;)Landroid/content/Context;

    move-result-object v2

    instance-of v2, v2, Lcom/samsung/musicplus/settings/MyMusicMenuReorderActivity;

    if-eqz v2, :cond_1

    .line 581
    iget-object v2, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$3;->this$0:Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;

    # getter for: Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->access$400(Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;)Landroid/content/Context;

    move-result-object v2

    check-cast v2, Lcom/samsung/musicplus/settings/MyMusicMenuReorderActivity;

    iget-object v3, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$3;->this$0:Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;

    iget-object v3, v3, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mAdapter:Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;

    # invokes: Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->isItemOrderChanged()Z
    invoke-static {v3}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->access$500(Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$3;->this$0:Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;

    iget-object v3, v3, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->mAdapter:Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;

    # invokes: Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->isActiveItemChanged()Z
    invoke-static {v3}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;->access$600(Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$MyMusicMenuReorderAdapter;)Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_0
    const/4 v3, 0x1

    :goto_0
    invoke-virtual {v2, v3}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderActivity;->enableDoneButton(Z)V

    .line 585
    .end local v0    # "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$ViewItem;>;"
    .end local v1    # "vh":Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$ViewItem;
    :cond_1
    return-void

    .line 581
    .restart local v0    # "arrayList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$ViewItem;>;"
    .restart local v1    # "vh":Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$ViewItem;
    :cond_2
    const/4 v3, 0x0

    goto :goto_0
.end method
