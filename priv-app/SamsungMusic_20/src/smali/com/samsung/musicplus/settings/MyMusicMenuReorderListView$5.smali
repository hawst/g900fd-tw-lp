.class Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$5;
.super Ljava/lang/Object;
.source "MyMusicMenuReorderListView.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;)V
    .locals 0

    .prologue
    .line 882
    iput-object p1, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView$5;->this$0:Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 885
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    long-to-int v0, p4

    invoke-static {v0}, Lcom/samsung/musicplus/util/ListUtils;->isFixedList(I)Z

    move-result v0

    if-nez v0, :cond_0

    const-wide/16 v0, -0xb

    cmp-long v0, p4, v0

    if-nez v0, :cond_1

    .line 892
    :cond_0
    :goto_0
    return-void

    .line 889
    :cond_1
    if-eqz p2, :cond_0

    .line 890
    const v0, 0x7f0d0139

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->performClick()Z

    goto :goto_0
.end method
