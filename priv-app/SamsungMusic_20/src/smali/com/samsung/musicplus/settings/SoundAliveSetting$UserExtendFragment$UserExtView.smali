.class Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment$UserExtView;
.super Landroid/view/View;
.source "SoundAliveSetting.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "UserExtView"
.end annotation


# static fields
.field private static final DB_SCALE_LEVEL_EXT:I = 0x3


# instance fields
.field private mIsPortrait:Z

.field private mRawWidth:I

.field private mSeekBarHeight:I

.field private mTopPadding:I


# direct methods
.method public constructor <init>(Landroid/content/Context;II)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "eqBarHeight"    # I
    .param p3, "topPadding"    # I

    .prologue
    const/4 v0, 0x1

    .line 800
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 795
    iput-boolean v0, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment$UserExtView;->mIsPortrait:Z

    .line 801
    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment$UserExtView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    if-ne v1, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment$UserExtView;->mIsPortrait:Z

    .line 802
    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment$UserExtView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v0, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment$UserExtView;->mRawWidth:I

    .line 803
    iput p2, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment$UserExtView;->mSeekBarHeight:I

    .line 804
    iput p3, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment$UserExtView;->mTopPadding:I

    .line 805
    return-void

    .line 801
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 14
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v13, 0x0

    const/high16 v12, 0x40000000    # 2.0f

    .line 809
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 811
    const/4 v3, 0x0

    .line 813
    .local v3, "linePosY":F
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 814
    const/4 v10, 0x0

    iget v11, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment$UserExtView;->mTopPadding:I

    int-to-float v11, v11

    invoke-virtual {p1, v10, v11}, Landroid/graphics/Canvas;->translate(FF)V

    .line 816
    new-instance v4, Landroid/graphics/Paint;

    const/4 v10, 0x1

    invoke-direct {v4, v10}, Landroid/graphics/Paint;-><init>(I)V

    .line 818
    .local v4, "pnt":Landroid/graphics/Paint;
    invoke-static {v13, v13, v13}, Landroid/graphics/Color;->rgb(III)I

    move-result v10

    invoke-virtual {v4, v10}, Landroid/graphics/Paint;->setColor(I)V

    .line 824
    iget v5, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment$UserExtView;->mRawWidth:I

    .line 825
    .local v5, "screenRawidth":I
    iget-boolean v1, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment$UserExtView;->mIsPortrait:Z

    .line 827
    .local v1, "isPortrait":Z
    if-eqz v1, :cond_0

    const/16 v10, 0x438

    if-ge v5, v10, :cond_1

    :cond_0
    if-nez v1, :cond_3

    const/16 v10, 0x780

    if-lt v5, v10, :cond_3

    .line 828
    :cond_1
    const/high16 v10, 0x42180000    # 38.0f

    invoke-virtual {v4, v10}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 829
    const/high16 v2, 0x40c00000    # 6.0f

    .line 830
    .local v2, "linePosX":F
    const-wide v8, 0x3fea2d0e56041893L    # 0.818

    .line 831
    .local v8, "ratePortlinePosY":D
    const-wide v6, 0x3fe62d0e56041893L    # 0.693

    .line 864
    .local v6, "rateLandlinePosY":D
    :goto_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    const/4 v10, 0x3

    if-gt v0, v10, :cond_f

    .line 865
    if-eqz v1, :cond_e

    .line 866
    iget v10, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment$UserExtView;->mSeekBarHeight:I

    mul-int/2addr v10, v0

    div-int/lit8 v10, v10, 0x3

    int-to-double v10, v10

    mul-double/2addr v10, v8

    double-to-float v3, v10

    .line 874
    :cond_2
    :goto_2
    packed-switch v0, :pswitch_data_0

    .line 864
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 832
    .end local v0    # "i":I
    .end local v2    # "linePosX":F
    .end local v6    # "rateLandlinePosY":D
    .end local v8    # "ratePortlinePosY":D
    :cond_3
    if-eqz v1, :cond_4

    const/16 v10, 0x2d0

    if-ge v5, v10, :cond_5

    :cond_4
    if-nez v1, :cond_7

    const/16 v10, 0x500

    if-lt v5, v10, :cond_7

    .line 834
    :cond_5
    const/high16 v10, 0x41e00000    # 28.0f

    invoke-virtual {v4, v10}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 835
    const/high16 v2, 0x40a00000    # 5.0f

    .line 836
    .restart local v2    # "linePosX":F
    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment$UserExtView;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    invoke-virtual {v10}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v10

    iget v10, v10, Landroid/util/DisplayMetrics;->densityDpi:I

    const/16 v11, 0xf0

    if-ne v10, v11, :cond_6

    .line 838
    const-wide v8, 0x3febc6a7ef9db22dL    # 0.868

    .line 839
    .restart local v8    # "ratePortlinePosY":D
    const-wide v6, 0x3fe9eb851eb851ecL    # 0.81

    .restart local v6    # "rateLandlinePosY":D
    goto :goto_0

    .line 841
    .end local v6    # "rateLandlinePosY":D
    .end local v8    # "ratePortlinePosY":D
    :cond_6
    const-wide v8, 0x3fead0e560418937L    # 0.838

    .line 842
    .restart local v8    # "ratePortlinePosY":D
    const-wide v6, 0x3fe6666666666666L    # 0.7

    .restart local v6    # "rateLandlinePosY":D
    goto :goto_0

    .line 844
    .end local v2    # "linePosX":F
    .end local v6    # "rateLandlinePosY":D
    .end local v8    # "ratePortlinePosY":D
    :cond_7
    if-eqz v1, :cond_8

    const/16 v10, 0x21c

    if-ge v5, v10, :cond_9

    :cond_8
    if-nez v1, :cond_a

    const/16 v10, 0x3c0

    if-lt v5, v10, :cond_a

    .line 846
    :cond_9
    const/high16 v10, 0x41c00000    # 24.0f

    invoke-virtual {v4, v10}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 847
    const/high16 v2, 0x40a00000    # 5.0f

    .line 848
    .restart local v2    # "linePosX":F
    const-wide v8, 0x3fea24dd2f1a9fbeL    # 0.817

    .line 849
    .restart local v8    # "ratePortlinePosY":D
    const-wide v6, 0x3fe6147ae147ae14L    # 0.69

    .restart local v6    # "rateLandlinePosY":D
    goto :goto_0

    .line 851
    .end local v2    # "linePosX":F
    .end local v6    # "rateLandlinePosY":D
    .end local v8    # "ratePortlinePosY":D
    :cond_a
    if-eqz v1, :cond_b

    const/16 v10, 0x140

    if-gt v5, v10, :cond_c

    :cond_b
    if-nez v1, :cond_d

    const/16 v10, 0x1e0

    if-le v5, v10, :cond_d

    .line 852
    :cond_c
    const/high16 v10, 0x41a00000    # 20.0f

    invoke-virtual {v4, v10}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 853
    const-wide v8, 0x3fea24dd2f1a9fbeL    # 0.817

    .line 854
    .restart local v8    # "ratePortlinePosY":D
    const-wide v6, 0x3fe5c28f5c28f5c3L    # 0.68

    .line 861
    .restart local v6    # "rateLandlinePosY":D
    :goto_4
    const/high16 v2, 0x40400000    # 3.0f

    .restart local v2    # "linePosX":F
    goto/16 :goto_0

    .line 856
    .end local v2    # "linePosX":F
    .end local v6    # "rateLandlinePosY":D
    .end local v8    # "ratePortlinePosY":D
    :cond_d
    const/high16 v10, 0x41600000    # 14.0f

    invoke-virtual {v4, v10}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 857
    sget-object v10, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {v4, v10}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 858
    const-wide v8, 0x3fea147ae147ae14L    # 0.815

    .line 859
    .restart local v8    # "ratePortlinePosY":D
    const-wide v6, 0x3fe3d70a3d70a3d7L    # 0.62

    .restart local v6    # "rateLandlinePosY":D
    goto :goto_4

    .line 868
    .restart local v0    # "i":I
    .restart local v2    # "linePosX":F
    :cond_e
    iget v10, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment$UserExtView;->mSeekBarHeight:I

    mul-int/2addr v10, v0

    div-int/lit8 v10, v10, 0x3

    int-to-double v10, v10

    mul-double/2addr v10, v6

    double-to-float v3, v10

    .line 869
    const/high16 v10, 0x40400000    # 3.0f

    cmpl-float v10, v2, v10

    if-nez v10, :cond_2

    .line 870
    const/high16 v2, 0x3f800000    # 1.0f

    goto/16 :goto_2

    .line 876
    :pswitch_0
    const-string v10, "3"

    invoke-virtual {v4}, Landroid/graphics/Paint;->ascent()F

    move-result v11

    div-float/2addr v11, v12

    sub-float v11, v3, v11

    invoke-virtual {p1, v10, v2, v11, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto/16 :goto_3

    .line 879
    :pswitch_1
    const-string v10, "2"

    invoke-virtual {v4}, Landroid/graphics/Paint;->ascent()F

    move-result v11

    div-float/2addr v11, v12

    sub-float v11, v3, v11

    invoke-virtual {p1, v10, v2, v11, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto/16 :goto_3

    .line 882
    :pswitch_2
    const-string v10, "1"

    invoke-virtual {v4}, Landroid/graphics/Paint;->ascent()F

    move-result v11

    div-float/2addr v11, v12

    sub-float v11, v3, v11

    invoke-virtual {p1, v10, v2, v11, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto/16 :goto_3

    .line 885
    :pswitch_3
    const-string v10, "0"

    invoke-virtual {v4}, Landroid/graphics/Paint;->ascent()F

    move-result v11

    div-float/2addr v11, v12

    sub-float v11, v3, v11

    invoke-virtual {p1, v10, v2, v11, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto/16 :goto_3

    .line 889
    :cond_f
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 890
    return-void

    .line 874
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
