.class public Lcom/samsung/musicplus/settings/SoundAliveSetting;
.super Lcom/samsung/musicplus/MusicBaseActivity;
.source "SoundAliveSetting.java"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/settings/SoundAliveSetting$SeekBubbleUpdateListener;,
        Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqPagerAdapter;,
        Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment;,
        Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqFragment;,
        Lcom/samsung/musicplus/settings/SoundAliveSetting$SoundAliveAdapter;
    }
.end annotation


# static fields
.field private static final EQ_LIST:[I

.field private static final TAG:Ljava/lang/String; = "MusicSetting"


# instance fields
.field private mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

.field private mAudioPathReceiver:Landroid/content/BroadcastReceiver;

.field private mExtendedButton:Landroid/view/View;

.field private mPreference:Landroid/content/SharedPreferences;

.field private mSpinner:Landroid/widget/Spinner;

.field private mSpinnerAdapter:Lcom/samsung/musicplus/settings/SoundAliveSetting$SoundAliveAdapter;

.field private mUserEqPager:Landroid/support/v4/view/ViewPager;

.field private mUserEqPagerAdapter:Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqPagerAdapter;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 115
    const/16 v0, 0x10

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/samsung/musicplus/settings/SoundAliveSetting;->EQ_LIST:[I

    return-void

    :array_0
    .array-data 4
        -0x1
        0x0
        0x1
        0x2
        0x3
        0x4
        0x5
        0x11
        0x6
        0x7
        0x8
        0x10
        0xa
        0xb
        0xc
        0xd
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 75
    invoke-direct {p0}, Lcom/samsung/musicplus/MusicBaseActivity;-><init>()V

    .line 205
    new-instance v0, Lcom/samsung/musicplus/settings/SoundAliveSetting$1;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/settings/SoundAliveSetting$1;-><init>(Lcom/samsung/musicplus/settings/SoundAliveSetting;)V

    iput-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting;->mAudioPathReceiver:Landroid/content/BroadcastReceiver;

    .line 976
    return-void
.end method

.method static synthetic access$000()[I
    .locals 1

    .prologue
    .line 75
    sget-object v0, Lcom/samsung/musicplus/settings/SoundAliveSetting;->EQ_LIST:[I

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/musicplus/settings/SoundAliveSetting;)Lcom/samsung/musicplus/library/audio/SecAudioManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/SoundAliveSetting;

    .prologue
    .line 75
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/samsung/musicplus/settings/SoundAliveSetting;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/SoundAliveSetting;

    .prologue
    .line 75
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/SoundAliveSetting;->activateUserEq()V

    return-void
.end method

.method static synthetic access$1100(Lcom/samsung/musicplus/settings/SoundAliveSetting;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/SoundAliveSetting;

    .prologue
    .line 75
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/SoundAliveSetting;->deactivateUserEq()V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/musicplus/settings/SoundAliveSetting;)Lcom/samsung/musicplus/settings/SoundAliveSetting$SoundAliveAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/SoundAliveSetting;

    .prologue
    .line 75
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting;->mSpinnerAdapter:Lcom/samsung/musicplus/settings/SoundAliveSetting$SoundAliveAdapter;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/musicplus/settings/SoundAliveSetting;I)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/SoundAliveSetting;
    .param p1, "x1"    # I

    .prologue
    .line 75
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/settings/SoundAliveSetting;->getEqPosition(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/samsung/musicplus/settings/SoundAliveSetting;)Landroid/widget/Spinner;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/SoundAliveSetting;

    .prologue
    .line 75
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting;->mSpinner:Landroid/widget/Spinner;

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/musicplus/settings/SoundAliveSetting;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/SoundAliveSetting;

    .prologue
    .line 75
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/SoundAliveSetting;->isDisableExtended()Z

    move-result v0

    return v0
.end method

.method static synthetic access$600(Lcom/samsung/musicplus/settings/SoundAliveSetting;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/SoundAliveSetting;

    .prologue
    .line 75
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting;->mExtendedButton:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$700(Lcom/samsung/musicplus/settings/SoundAliveSetting;Landroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/SoundAliveSetting;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 75
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/settings/SoundAliveSetting;->disableExtended(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$800(Lcom/samsung/musicplus/settings/SoundAliveSetting;Landroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/SoundAliveSetting;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 75
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/settings/SoundAliveSetting;->enableExtended(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$900(Lcom/samsung/musicplus/settings/SoundAliveSetting;)Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqPagerAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/SoundAliveSetting;

    .prologue
    .line 75
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting;->mUserEqPagerAdapter:Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqPagerAdapter;

    return-object v0
.end method

.method private activateUserEq()V
    .locals 4

    .prologue
    .line 387
    iget-object v1, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting;->mSpinner:Landroid/widget/Spinner;

    sget-object v2, Lcom/samsung/musicplus/settings/SoundAliveSetting;->EQ_LIST:[I

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/widget/Spinner;->setSelection(IZ)V

    .line 388
    const v1, 0x7f0d0151

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/settings/SoundAliveSetting;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 389
    .local v0, "main":Landroid/view/View;
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 390
    return-void
.end method

.method private deactivateUserEq()V
    .locals 2

    .prologue
    .line 393
    const v1, 0x7f0d0151

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/settings/SoundAliveSetting;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 394
    .local v0, "main":Landroid/view/View;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 395
    return-void
.end method

.method private disableExtended(Landroid/view/View;)V
    .locals 2
    .param p1, "effect"    # Landroid/view/View;

    .prologue
    const/4 v1, 0x0

    .line 352
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting;->mUserEqPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 353
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting;->mUserEqPagerAdapter:Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqPagerAdapter;

    invoke-virtual {v0}, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqPagerAdapter;->disableExtended()V

    .line 356
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting;->mUserEqPagerAdapter:Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqPagerAdapter;

    invoke-virtual {v0}, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqPagerAdapter;->notifyDataSetChanged()V

    .line 358
    invoke-virtual {p1, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 359
    instance-of v0, p1, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 360
    check-cast p1, Landroid/widget/TextView;

    .end local p1    # "effect":Landroid/view/View;
    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/SoundAliveSetting;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0015

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 363
    :cond_0
    return-void
.end method

.method private enableExtended(Landroid/view/View;)V
    .locals 2
    .param p1, "effect"    # Landroid/view/View;

    .prologue
    .line 366
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting;->mUserEqPagerAdapter:Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqPagerAdapter;

    invoke-virtual {v0}, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqPagerAdapter;->enableExtended()V

    .line 369
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting;->mUserEqPagerAdapter:Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqPagerAdapter;

    invoke-virtual {v0}, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqPagerAdapter;->notifyDataSetChanged()V

    .line 371
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/View;->setEnabled(Z)V

    .line 372
    instance-of v0, p1, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 373
    check-cast p1, Landroid/widget/TextView;

    .end local p1    # "effect":Landroid/view/View;
    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/SoundAliveSetting;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0014

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 376
    :cond_0
    return-void
.end method

.method private getEqPosition(I)I
    .locals 2
    .param p1, "eq"    # I

    .prologue
    .line 133
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v1, Lcom/samsung/musicplus/settings/SoundAliveSetting;->EQ_LIST:[I

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 134
    sget-object v1, Lcom/samsung/musicplus/settings/SoundAliveSetting;->EQ_LIST:[I

    aget v1, v1, v0

    if-ne p1, v1, :cond_0

    .line 138
    .end local v0    # "i":I
    :goto_1
    return v0

    .line 133
    .restart local v0    # "i":I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 138
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private getEqStrinArray([I)[Ljava/lang/String;
    .locals 3
    .param p1, "eqList"    # [I

    .prologue
    .line 158
    array-length v2, p1

    new-array v0, v2, [Ljava/lang/String;

    .line 159
    .local v0, "eqString":[Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, p1

    if-ge v1, v2, :cond_0

    .line 160
    aget v2, p1, v1

    invoke-direct {p0, v2}, Lcom/samsung/musicplus/settings/SoundAliveSetting;->getEqString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 159
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 162
    :cond_0
    return-object v0
.end method

.method private getEqString(I)Ljava/lang/String;
    .locals 1
    .param p1, "eq"    # I

    .prologue
    .line 148
    invoke-static {p1}, Lcom/samsung/musicplus/util/SoundAliveUtils;->getEffectName(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/settings/SoundAliveSetting;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private isDisableExtended()Z
    .locals 2

    .prologue
    .line 379
    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/SoundAliveSetting;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->getInstance(Landroid/content/Context;)Lcom/samsung/musicplus/library/audio/SecAudioManager;

    move-result-object v0

    .line 380
    .local v0, "am":Lcom/samsung/musicplus/library/audio/SecAudioManager;
    invoke-virtual {v0}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->isAudioPathBT()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->isAudioPathLineOut()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 381
    :cond_0
    const/4 v1, 0x1

    .line 383
    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private loadSoundAliveValue()I
    .locals 4

    .prologue
    .line 345
    iget-object v1, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting;->mPreference:Landroid/content/SharedPreferences;

    const-string v2, "sound_alive"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 347
    .local v0, "alive":I
    const-string v1, "MusicSetting"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "loadSoundAliveValue sound alive : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 348
    return v0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 329
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 341
    :goto_0
    return-void

    .line 331
    :pswitch_0
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/SoundAliveSetting;->activateUserEq()V

    .line 332
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting;->mUserEqPager:Landroid/support/v4/view/ViewPager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    goto :goto_0

    .line 335
    :pswitch_1
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/SoundAliveSetting;->activateUserEq()V

    .line 336
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting;->mUserEqPager:Landroid/support/v4/view/ViewPager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    goto :goto_0

    .line 329
    :pswitch_data_0
    .packed-switch 0x7f0d014f
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 232
    invoke-super {p0, p1}, Lcom/samsung/musicplus/MusicBaseActivity;->onCreate(Landroid/os/Bundle;)V

    .line 233
    if-nez p1, :cond_0

    .line 236
    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/SoundAliveSetting;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "SDAL"

    invoke-static {v3, v4}, Lcom/samsung/musicplus/util/logging/FeatureLogger;->insertLog(Landroid/content/Context;Ljava/lang/String;)V

    .line 238
    :cond_0
    const v3, 0x7f040072

    invoke-virtual {p0, v3}, Lcom/samsung/musicplus/settings/SoundAliveSetting;->setContentView(I)V

    .line 239
    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/SoundAliveSetting;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->getInstance(Landroid/content/Context;)Lcom/samsung/musicplus/library/audio/SecAudioManager;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

    .line 241
    const-string v3, "music_service_pref"

    const/4 v4, 0x4

    invoke-virtual {p0, v3, v4}, Lcom/samsung/musicplus/settings/SoundAliveSetting;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting;->mPreference:Landroid/content/SharedPreferences;

    .line 243
    iget-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting;->mPreference:Landroid/content/SharedPreferences;

    invoke-interface {v3, p0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 245
    new-instance v3, Lcom/samsung/musicplus/settings/SoundAliveSetting$SoundAliveAdapter;

    const v4, 0x1090008

    sget-object v5, Lcom/samsung/musicplus/settings/SoundAliveSetting;->EQ_LIST:[I

    invoke-direct {p0, v5}, Lcom/samsung/musicplus/settings/SoundAliveSetting;->getEqStrinArray([I)[Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, p0, p0, v4, v5}, Lcom/samsung/musicplus/settings/SoundAliveSetting$SoundAliveAdapter;-><init>(Lcom/samsung/musicplus/settings/SoundAliveSetting;Landroid/content/Context;I[Ljava/lang/String;)V

    iput-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting;->mSpinnerAdapter:Lcom/samsung/musicplus/settings/SoundAliveSetting$SoundAliveAdapter;

    .line 247
    iget-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting;->mSpinnerAdapter:Lcom/samsung/musicplus/settings/SoundAliveSetting$SoundAliveAdapter;

    const v4, 0x1090009

    invoke-virtual {v3, v4}, Lcom/samsung/musicplus/settings/SoundAliveSetting$SoundAliveAdapter;->setDropDownViewResource(I)V

    .line 248
    iget-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting;->mSpinnerAdapter:Lcom/samsung/musicplus/settings/SoundAliveSetting$SoundAliveAdapter;

    iget-object v4, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

    invoke-virtual {v4}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->getCurrentAudioPath()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/samsung/musicplus/settings/SoundAliveSetting$SoundAliveAdapter;->setAudioPath(I)V

    .line 250
    const v3, 0x7f0d014e

    invoke-virtual {p0, v3}, Lcom/samsung/musicplus/settings/SoundAliveSetting;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Spinner;

    iput-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting;->mSpinner:Landroid/widget/Spinner;

    .line 251
    iget-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting;->mSpinner:Landroid/widget/Spinner;

    iget-object v4, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting;->mSpinnerAdapter:Lcom/samsung/musicplus/settings/SoundAliveSetting$SoundAliveAdapter;

    invoke-virtual {v3, v4}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 252
    iget-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting;->mSpinner:Landroid/widget/Spinner;

    invoke-direct {p0}, Lcom/samsung/musicplus/settings/SoundAliveSetting;->loadSoundAliveValue()I

    move-result v4

    invoke-direct {p0, v4}, Lcom/samsung/musicplus/settings/SoundAliveSetting;->getEqPosition(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/Spinner;->setSelection(I)V

    .line 253
    iget-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting;->mSpinner:Landroid/widget/Spinner;

    new-instance v4, Lcom/samsung/musicplus/settings/SoundAliveSetting$2;

    invoke-direct {v4, p0}, Lcom/samsung/musicplus/settings/SoundAliveSetting$2;-><init>(Lcom/samsung/musicplus/settings/SoundAliveSetting;)V

    invoke-virtual {v3, v4}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 271
    const v3, 0x7f0d0151

    invoke-virtual {p0, v3}, Lcom/samsung/musicplus/settings/SoundAliveSetting;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 272
    .local v1, "main":Landroid/view/View;
    new-instance v3, Lcom/samsung/musicplus/settings/SoundAliveSetting$3;

    invoke-direct {v3, p0}, Lcom/samsung/musicplus/settings/SoundAliveSetting$3;-><init>(Lcom/samsung/musicplus/settings/SoundAliveSetting;)V

    invoke-virtual {v1, v3}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 281
    new-instance v3, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqPagerAdapter;

    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/SoundAliveSetting;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqPagerAdapter;-><init>(Landroid/app/FragmentManager;)V

    iput-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting;->mUserEqPagerAdapter:Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqPagerAdapter;

    .line 283
    const v3, 0x7f0d00ee

    invoke-virtual {p0, v3}, Lcom/samsung/musicplus/settings/SoundAliveSetting;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/support/v4/view/ViewPager;

    iput-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting;->mUserEqPager:Landroid/support/v4/view/ViewPager;

    .line 284
    iget-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting;->mUserEqPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v3, p0}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 285
    iget-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting;->mUserEqPager:Landroid/support/v4/view/ViewPager;

    iget-object v4, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting;->mUserEqPagerAdapter:Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqPagerAdapter;

    invoke-virtual {v3, v4}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 286
    iget-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting;->mUserEqPager:Landroid/support/v4/view/ViewPager;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 288
    const v3, 0x7f0d014f

    invoke-virtual {p0, v3}, Lcom/samsung/musicplus/settings/SoundAliveSetting;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 289
    .local v2, "userEq":Landroid/view/View;
    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 290
    const v3, 0x7f0d0150

    invoke-virtual {p0, v3}, Lcom/samsung/musicplus/settings/SoundAliveSetting;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting;->mExtendedButton:Landroid/view/View;

    .line 291
    iget-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting;->mExtendedButton:Landroid/view/View;

    invoke-virtual {v3, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 293
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/SoundAliveSetting;->isDisableExtended()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 294
    iget-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting;->mExtendedButton:Landroid/view/View;

    invoke-direct {p0, v3}, Lcom/samsung/musicplus/settings/SoundAliveSetting;->disableExtended(Landroid/view/View;)V

    .line 303
    :goto_0
    new-instance v0, Landroid/content/IntentFilter;

    const-string v3, "com.samsung.musicplus.action.AUDIO_PATH_CHANGED"

    invoke-direct {v0, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 304
    .local v0, "f":Landroid/content/IntentFilter;
    iget-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting;->mAudioPathReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v3, v0}, Lcom/samsung/musicplus/settings/SoundAliveSetting;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 305
    return-void

    .line 296
    .end local v0    # "f":Landroid/content/IntentFilter;
    :cond_1
    iget-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

    invoke-virtual {v3}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->isAudioPathSpeaker()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 297
    iget-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting;->mUserEqPagerAdapter:Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqPagerAdapter;

    invoke-virtual {v3}, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqPagerAdapter;->disalbe3dEffect()V

    goto :goto_0

    .line 299
    :cond_2
    iget-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting;->mUserEqPagerAdapter:Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqPagerAdapter;

    invoke-virtual {v3}, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqPagerAdapter;->enable3dEffect()V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 309
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting;->mAudioPathReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/settings/SoundAliveSetting;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 310
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting;->mPreference:Landroid/content/SharedPreferences;

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 311
    invoke-super {p0}, Lcom/samsung/musicplus/MusicBaseActivity;->onDestroy()V

    .line 312
    return-void
.end method

.method public onPageScrollStateChanged(I)V
    .locals 0
    .param p1, "state"    # I

    .prologue
    .line 399
    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 0
    .param p1, "position"    # I
    .param p2, "positionOffset"    # F
    .param p3, "positionOffsetPixels"    # I

    .prologue
    .line 403
    return-void
.end method

.method public onPageSelected(I)V
    .locals 0
    .param p1, "position"    # I

    .prologue
    .line 407
    return-void
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 4
    .param p1, "pref"    # Landroid/content/SharedPreferences;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 318
    const-string v1, "MusicSetting"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onSharedPreferenceChanged pref : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " Key : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 319
    const-string v1, "sound_alive"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 320
    const-string v1, "sound_alive"

    const/4 v2, 0x0

    invoke-interface {p1, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 322
    .local v0, "alive":I
    const-string v1, "MusicSetting"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, " key is sound alive and value is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    iget-object v1, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting;->mSpinner:Landroid/widget/Spinner;

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/settings/SoundAliveSetting;->getEqPosition(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/Spinner;->setSelection(I)V

    .line 325
    .end local v0    # "alive":I
    :cond_0
    return-void
.end method
