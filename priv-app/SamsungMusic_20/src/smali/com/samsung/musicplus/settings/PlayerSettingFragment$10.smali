.class Lcom/samsung/musicplus/settings/PlayerSettingFragment$10;
.super Landroid/content/BroadcastReceiver;
.source "PlayerSettingFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/settings/PlayerSettingFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/settings/PlayerSettingFragment;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/settings/PlayerSettingFragment;)V
    .locals 0

    .prologue
    .line 976
    iput-object p1, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment$10;->this$0:Lcom/samsung/musicplus/settings/PlayerSettingFragment;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 979
    const-string v0, "MusicSetting"

    const-string v1, "Auto off receiver gets Intent ACTION_MUSIC_AUTO_OFF!"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 980
    iget-object v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment$10;->this$0:Lcom/samsung/musicplus/settings/PlayerSettingFragment;

    # getter for: Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mMusicAutoOff:Landroid/preference/ListPreference;
    invoke-static {v0}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->access$900(Lcom/samsung/musicplus/settings/PlayerSettingFragment;)Landroid/preference/ListPreference;

    move-result-object v0

    const-string v1, "0"

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 981
    iget-object v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment$10;->this$0:Lcom/samsung/musicplus/settings/PlayerSettingFragment;

    # getter for: Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mSummaryUpdateHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->access$1500(Lcom/samsung/musicplus/settings/PlayerSettingFragment;)Landroid/os/Handler;

    move-result-object v0

    const/4 v1, 0x1

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 982
    return-void
.end method
