.class Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqFragment$UserEqView;
.super Landroid/view/View;
.source "SoundAliveSetting.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UserEqView"
.end annotation


# static fields
.field private static final DB_SCALE_LEVEL_EQ:I = 0xa


# instance fields
.field private mIsPortrait:Z

.field private mRawWidth:I

.field private mSeekBarHeight:I

.field private mTopPadding:I

.field final synthetic this$0:Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqFragment;


# direct methods
.method public constructor <init>(Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqFragment;Landroid/content/Context;II)V
    .locals 2
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "eqBarHeight"    # I
    .param p4, "topPadding"    # I

    .prologue
    const/4 v0, 0x1

    .line 526
    iput-object p1, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqFragment$UserEqView;->this$0:Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqFragment;

    .line 527
    invoke-direct {p0, p2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 522
    iput-boolean v0, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqFragment$UserEqView;->mIsPortrait:Z

    .line 528
    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqFragment$UserEqView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    if-ne v1, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqFragment$UserEqView;->mIsPortrait:Z

    .line 529
    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqFragment$UserEqView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v0, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqFragment$UserEqView;->mRawWidth:I

    .line 530
    iput p3, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqFragment$UserEqView;->mSeekBarHeight:I

    .line 531
    iput p4, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqFragment$UserEqView;->mTopPadding:I

    .line 532
    return-void

    .line 528
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 18
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 536
    invoke-super/range {p0 .. p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 538
    const/high16 v5, 0x42540000    # 53.0f

    .line 540
    .local v5, "linePosX":F
    const/high16 v16, 0x40a00000    # 5.0f

    .line 544
    .local v16, "textPosX":F
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->save()I

    .line 545
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqFragment$UserEqView;->mTopPadding:I

    int-to-float v3, v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    .line 547
    new-instance v7, Landroid/graphics/Paint;

    const/4 v2, 0x1

    invoke-direct {v7, v2}, Landroid/graphics/Paint;-><init>(I)V

    .line 549
    .local v7, "pnt":Landroid/graphics/Paint;
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v6, 0x0

    invoke-static {v2, v3, v6}, Landroid/graphics/Color;->rgb(III)I

    move-result v2

    invoke-virtual {v7, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 557
    move-object/from16 v0, p0

    iget-boolean v9, v0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqFragment$UserEqView;->mIsPortrait:Z

    .line 560
    .local v9, "isPortrait":Z
    if-eqz v9, :cond_0

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqFragment$UserEqView;->mRawWidth:I

    const/16 v3, 0x438

    if-ge v2, v3, :cond_1

    :cond_0
    if-nez v9, :cond_2

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqFragment$UserEqView;->mRawWidth:I

    const/16 v3, 0x780

    if-lt v2, v3, :cond_2

    .line 561
    :cond_1
    const/high16 v2, 0x42180000    # 38.0f

    invoke-virtual {v7, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 562
    const/high16 v5, 0x42860000    # 67.0f

    .line 563
    const/high16 v11, 0x41880000    # 17.0f

    .line 564
    .local v11, "offsetShortLinePosX":F
    const/high16 v10, 0x40400000    # 3.0f

    .line 565
    .local v10, "offsetLinePosY":F
    const-wide v14, 0x3fea2d0e56041893L    # 0.818

    .line 566
    .local v14, "ratePortlinePosY":D
    const-wide v12, 0x3fe62d0e56041893L    # 0.693

    .line 607
    .local v12, "rateLandlinePosY":D
    :goto_0
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_1
    const/16 v2, 0xa

    if-gt v8, v2, :cond_11

    .line 608
    if-eqz v9, :cond_10

    .line 609
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqFragment$UserEqView;->mSeekBarHeight:I

    mul-int/2addr v2, v8

    div-int/lit8 v2, v2, 0xa

    int-to-double v2, v2

    mul-double/2addr v2, v14

    double-to-float v4, v2

    .line 614
    .local v4, "linePosY":F
    :goto_2
    sparse-switch v8, :sswitch_data_0

    .line 626
    sub-float v3, v5, v11

    add-float v6, v4, v10

    move-object/from16 v2, p1

    invoke-virtual/range {v2 .. v7}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 607
    :goto_3
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 567
    .end local v4    # "linePosY":F
    .end local v8    # "i":I
    .end local v10    # "offsetLinePosY":F
    .end local v11    # "offsetShortLinePosX":F
    .end local v12    # "rateLandlinePosY":D
    .end local v14    # "ratePortlinePosY":D
    :cond_2
    if-eqz v9, :cond_3

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqFragment$UserEqView;->mRawWidth:I

    const/16 v3, 0x21c

    if-gt v2, v3, :cond_4

    :cond_3
    if-nez v9, :cond_6

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqFragment$UserEqView;->mRawWidth:I

    const/16 v3, 0x3c0

    if-le v2, v3, :cond_6

    .line 568
    :cond_4
    const/high16 v2, 0x41a00000    # 20.0f

    invoke-virtual {v7, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 569
    const/high16 v5, 0x42200000    # 40.0f

    .line 570
    const/high16 v11, 0x41400000    # 12.0f

    .line 571
    .restart local v11    # "offsetShortLinePosX":F
    const/high16 v10, 0x40400000    # 3.0f

    .line 572
    .restart local v10    # "offsetLinePosY":F
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqFragment$UserEqView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->densityDpi:I

    const/16 v3, 0xf0

    if-ne v2, v3, :cond_5

    .line 574
    const-wide v14, 0x3febc6a7ef9db22dL    # 0.868

    .line 575
    .restart local v14    # "ratePortlinePosY":D
    const-wide v12, 0x3fe9b22d0e560419L    # 0.803

    .restart local v12    # "rateLandlinePosY":D
    goto :goto_0

    .line 577
    .end local v12    # "rateLandlinePosY":D
    .end local v14    # "ratePortlinePosY":D
    :cond_5
    const-wide v14, 0x3fea2d0e56041893L    # 0.818

    .line 578
    .restart local v14    # "ratePortlinePosY":D
    const-wide v12, 0x3fe62d0e56041893L    # 0.693

    .restart local v12    # "rateLandlinePosY":D
    goto :goto_0

    .line 581
    .end local v10    # "offsetLinePosY":F
    .end local v11    # "offsetShortLinePosX":F
    .end local v12    # "rateLandlinePosY":D
    .end local v14    # "ratePortlinePosY":D
    :cond_6
    if-eqz v9, :cond_7

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqFragment$UserEqView;->mRawWidth:I

    const/16 v3, 0x1e0

    if-gt v2, v3, :cond_8

    :cond_7
    if-nez v9, :cond_9

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqFragment$UserEqView;->mRawWidth:I

    const/16 v3, 0x320

    if-le v2, v3, :cond_9

    .line 582
    :cond_8
    const/high16 v2, 0x41a00000    # 20.0f

    invoke-virtual {v7, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 583
    const/high16 v5, 0x42200000    # 40.0f

    .line 584
    const-wide v14, 0x3fea2d0e56041893L    # 0.818

    .line 585
    .restart local v14    # "ratePortlinePosY":D
    const-wide v12, 0x3fe624dd2f1a9fbeL    # 0.692

    .line 603
    .restart local v12    # "rateLandlinePosY":D
    :goto_4
    const/high16 v11, 0x41400000    # 12.0f

    .line 604
    .restart local v11    # "offsetShortLinePosX":F
    const/high16 v10, 0x40000000    # 2.0f

    .restart local v10    # "offsetLinePosY":F
    goto/16 :goto_0

    .line 586
    .end local v10    # "offsetLinePosY":F
    .end local v11    # "offsetShortLinePosX":F
    .end local v12    # "rateLandlinePosY":D
    .end local v14    # "ratePortlinePosY":D
    :cond_9
    if-eqz v9, :cond_a

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqFragment$UserEqView;->mRawWidth:I

    const/16 v3, 0x140

    if-gt v2, v3, :cond_b

    :cond_a
    if-nez v9, :cond_c

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqFragment$UserEqView;->mRawWidth:I

    const/16 v3, 0x1e0

    if-le v2, v3, :cond_c

    .line 587
    :cond_b
    const/high16 v2, 0x41a00000    # 20.0f

    invoke-virtual {v7, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 588
    const/high16 v5, 0x42200000    # 40.0f

    .line 589
    const-wide v14, 0x3fea24dd2f1a9fbeL    # 0.817

    .line 590
    .restart local v14    # "ratePortlinePosY":D
    const-wide v12, 0x3fe4cccccccccccdL    # 0.65

    .restart local v12    # "rateLandlinePosY":D
    goto :goto_4

    .line 591
    .end local v12    # "rateLandlinePosY":D
    .end local v14    # "ratePortlinePosY":D
    :cond_c
    if-eqz v9, :cond_d

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqFragment$UserEqView;->mRawWidth:I

    const/16 v3, 0xf0

    if-gt v2, v3, :cond_e

    :cond_d
    if-nez v9, :cond_f

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqFragment$UserEqView;->mRawWidth:I

    const/16 v3, 0x140

    if-le v2, v3, :cond_f

    .line 592
    :cond_e
    const/high16 v2, 0x41600000    # 14.0f

    invoke-virtual {v7, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 593
    const/high16 v5, 0x424c0000    # 51.0f

    .line 594
    const-wide v14, 0x3fe9eb851eb851ecL    # 0.81

    .line 595
    .restart local v14    # "ratePortlinePosY":D
    const-wide v12, 0x3fe5c28f5c28f5c3L    # 0.68

    .restart local v12    # "rateLandlinePosY":D
    goto :goto_4

    .line 597
    .end local v12    # "rateLandlinePosY":D
    .end local v14    # "ratePortlinePosY":D
    :cond_f
    const/high16 v2, 0x41300000    # 11.0f

    invoke-virtual {v7, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 598
    const/high16 v16, 0x40000000    # 2.0f

    .line 599
    const/high16 v5, 0x42200000    # 40.0f

    .line 600
    const-wide v14, 0x3fe9eb851eb851ecL    # 0.81

    .line 601
    .restart local v14    # "ratePortlinePosY":D
    const-wide v12, 0x3fe5c28f5c28f5c3L    # 0.68

    .restart local v12    # "rateLandlinePosY":D
    goto :goto_4

    .line 611
    .restart local v8    # "i":I
    .restart local v10    # "offsetLinePosY":F
    .restart local v11    # "offsetShortLinePosX":F
    :cond_10
    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqFragment$UserEqView;->mSeekBarHeight:I

    mul-int/2addr v2, v8

    div-int/lit8 v2, v2, 0xa

    int-to-double v2, v2

    mul-double/2addr v2, v12

    double-to-float v4, v2

    .restart local v4    # "linePosY":F
    goto/16 :goto_2

    .line 616
    :sswitch_0
    const-string v2, " 10db"

    invoke-virtual {v7}, Landroid/graphics/Paint;->ascent()F

    move-result v3

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v3, v6

    sub-float v3, v4, v3

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1, v3, v7}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto/16 :goto_3

    .line 619
    :sswitch_1
    const-string v2, "  0db"

    invoke-virtual {v7}, Landroid/graphics/Paint;->ascent()F

    move-result v3

    const/high16 v6, 0x40000000    # 2.0f

    div-float/2addr v3, v6

    sub-float v3, v4, v3

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-virtual {v0, v2, v1, v3, v7}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto/16 :goto_3

    .line 622
    :sswitch_2
    const-string v2, "-10db"

    const/high16 v3, 0x3f800000    # 1.0f

    sub-float v3, v16, v3

    invoke-virtual {v7}, Landroid/graphics/Paint;->ascent()F

    move-result v6

    const/high16 v17, 0x40000000    # 2.0f

    div-float v6, v6, v17

    sub-float v6, v4, v6

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3, v6, v7}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto/16 :goto_3

    .line 631
    .end local v4    # "linePosY":F
    :cond_11
    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Canvas;->restore()V

    .line 632
    return-void

    .line 614
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x5 -> :sswitch_1
        0xa -> :sswitch_2
    .end sparse-switch
.end method
