.class Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqPagerAdapter;
.super Landroid/support/v13/app/FragmentPagerAdapter;
.source "SoundAliveSetting.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/settings/SoundAliveSetting;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "UserEqPagerAdapter"
.end annotation


# static fields
.field private static final EQ:I = 0x0

.field private static final EQ_TAB:[I

.field private static final EXTENDED:I = 0x1


# instance fields
.field private mCurrent:Ljava/lang/Object;

.field mIsEnable3dEffect:Z

.field private mItemCount:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 905
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqPagerAdapter;->EQ_TAB:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x0
        0x1
    .end array-data
.end method

.method public constructor <init>(Landroid/app/FragmentManager;)V
    .locals 1
    .param p1, "fm"    # Landroid/app/FragmentManager;

    .prologue
    .line 912
    invoke-direct {p0, p1}, Landroid/support/v13/app/FragmentPagerAdapter;-><init>(Landroid/app/FragmentManager;)V

    .line 909
    sget-object v0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqPagerAdapter;->EQ_TAB:[I

    array-length v0, v0

    iput v0, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqPagerAdapter;->mItemCount:I

    .line 948
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqPagerAdapter;->mIsEnable3dEffect:Z

    .line 913
    return-void
.end method


# virtual methods
.method public disableExtended()V
    .locals 1

    .prologue
    .line 941
    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqPagerAdapter;->mItemCount:I

    .line 942
    return-void
.end method

.method public disalbe3dEffect()V
    .locals 1

    .prologue
    .line 951
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqPagerAdapter;->mIsEnable3dEffect:Z

    .line 952
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqPagerAdapter;->mCurrent:Ljava/lang/Object;

    instance-of v0, v0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment;

    if-eqz v0, :cond_0

    .line 953
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqPagerAdapter;->mCurrent:Ljava/lang/Object;

    check-cast v0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment;

    invoke-virtual {v0}, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment;->disalbe3dEffect()V

    .line 955
    :cond_0
    return-void
.end method

.method public enable3dEffect()V
    .locals 1

    .prologue
    .line 958
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqPagerAdapter;->mIsEnable3dEffect:Z

    .line 959
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqPagerAdapter;->mCurrent:Ljava/lang/Object;

    instance-of v0, v0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment;

    if-eqz v0, :cond_0

    .line 960
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqPagerAdapter;->mCurrent:Ljava/lang/Object;

    check-cast v0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment;

    invoke-virtual {v0}, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment;->enable3dEffect()V

    .line 962
    :cond_0
    return-void
.end method

.method public enableExtended()V
    .locals 1

    .prologue
    .line 945
    sget-object v0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqPagerAdapter;->EQ_TAB:[I

    array-length v0, v0

    iput v0, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqPagerAdapter;->mItemCount:I

    .line 946
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 966
    iget v0, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqPagerAdapter;->mItemCount:I

    return v0
.end method

.method public getItem(I)Landroid/app/Fragment;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 917
    packed-switch p1, :pswitch_data_0

    .line 923
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 919
    :pswitch_0
    new-instance v0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqFragment;

    invoke-direct {v0}, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqFragment;-><init>()V

    goto :goto_0

    .line 921
    :pswitch_1
    new-instance v0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment;

    invoke-direct {v0}, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment;-><init>()V

    goto :goto_0

    .line 917
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setPrimaryItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 1
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I
    .param p3, "object"    # Ljava/lang/Object;

    .prologue
    .line 931
    invoke-super {p0, p1, p2, p3}, Landroid/support/v13/app/FragmentPagerAdapter;->setPrimaryItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V

    .line 932
    iput-object p3, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqPagerAdapter;->mCurrent:Ljava/lang/Object;

    .line 933
    iget-boolean v0, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqPagerAdapter;->mIsEnable3dEffect:Z

    if-eqz v0, :cond_0

    .line 934
    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqPagerAdapter;->enable3dEffect()V

    .line 938
    :goto_0
    return-void

    .line 936
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqPagerAdapter;->disalbe3dEffect()V

    goto :goto_0
.end method
