.class Lcom/samsung/musicplus/settings/SoundAliveSetting$1;
.super Landroid/content/BroadcastReceiver;
.source "SoundAliveSetting.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/settings/SoundAliveSetting;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/settings/SoundAliveSetting;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/settings/SoundAliveSetting;)V
    .locals 0

    .prologue
    .line 205
    iput-object p1, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$1;->this$0:Lcom/samsung/musicplus/settings/SoundAliveSetting;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 210
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$1;->this$0:Lcom/samsung/musicplus/settings/SoundAliveSetting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveSetting;->mSpinnerAdapter:Lcom/samsung/musicplus/settings/SoundAliveSetting$SoundAliveAdapter;
    invoke-static {v0}, Lcom/samsung/musicplus/settings/SoundAliveSetting;->access$200(Lcom/samsung/musicplus/settings/SoundAliveSetting;)Lcom/samsung/musicplus/settings/SoundAliveSetting$SoundAliveAdapter;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$1;->this$0:Lcom/samsung/musicplus/settings/SoundAliveSetting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveSetting;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;
    invoke-static {v1}, Lcom/samsung/musicplus/settings/SoundAliveSetting;->access$100(Lcom/samsung/musicplus/settings/SoundAliveSetting;)Lcom/samsung/musicplus/library/audio/SecAudioManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->getCurrentAudioPath()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/settings/SoundAliveSetting$SoundAliveAdapter;->setAudioPath(I)V

    .line 211
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$1;->this$0:Lcom/samsung/musicplus/settings/SoundAliveSetting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveSetting;->mSpinnerAdapter:Lcom/samsung/musicplus/settings/SoundAliveSetting$SoundAliveAdapter;
    invoke-static {v0}, Lcom/samsung/musicplus/settings/SoundAliveSetting;->access$200(Lcom/samsung/musicplus/settings/SoundAliveSetting;)Lcom/samsung/musicplus/settings/SoundAliveSetting$SoundAliveAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/musicplus/settings/SoundAliveSetting$SoundAliveAdapter;->notifyDataSetChanged()V

    .line 212
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$1;->this$0:Lcom/samsung/musicplus/settings/SoundAliveSetting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveSetting;->mSpinner:Landroid/widget/Spinner;
    invoke-static {v0}, Lcom/samsung/musicplus/settings/SoundAliveSetting;->access$400(Lcom/samsung/musicplus/settings/SoundAliveSetting;)Landroid/widget/Spinner;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$1;->this$0:Lcom/samsung/musicplus/settings/SoundAliveSetting;

    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getSoundAlive()I

    move-result v2

    # invokes: Lcom/samsung/musicplus/settings/SoundAliveSetting;->getEqPosition(I)I
    invoke-static {v1, v2}, Lcom/samsung/musicplus/settings/SoundAliveSetting;->access$300(Lcom/samsung/musicplus/settings/SoundAliveSetting;I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 215
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$1;->this$0:Lcom/samsung/musicplus/settings/SoundAliveSetting;

    # invokes: Lcom/samsung/musicplus/settings/SoundAliveSetting;->isDisableExtended()Z
    invoke-static {v0}, Lcom/samsung/musicplus/settings/SoundAliveSetting;->access$500(Lcom/samsung/musicplus/settings/SoundAliveSetting;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 216
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$1;->this$0:Lcom/samsung/musicplus/settings/SoundAliveSetting;

    iget-object v1, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$1;->this$0:Lcom/samsung/musicplus/settings/SoundAliveSetting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveSetting;->mExtendedButton:Landroid/view/View;
    invoke-static {v1}, Lcom/samsung/musicplus/settings/SoundAliveSetting;->access$600(Lcom/samsung/musicplus/settings/SoundAliveSetting;)Landroid/view/View;

    move-result-object v1

    # invokes: Lcom/samsung/musicplus/settings/SoundAliveSetting;->disableExtended(Landroid/view/View;)V
    invoke-static {v0, v1}, Lcom/samsung/musicplus/settings/SoundAliveSetting;->access$700(Lcom/samsung/musicplus/settings/SoundAliveSetting;Landroid/view/View;)V

    .line 227
    :goto_0
    return-void

    .line 218
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$1;->this$0:Lcom/samsung/musicplus/settings/SoundAliveSetting;

    iget-object v1, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$1;->this$0:Lcom/samsung/musicplus/settings/SoundAliveSetting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveSetting;->mExtendedButton:Landroid/view/View;
    invoke-static {v1}, Lcom/samsung/musicplus/settings/SoundAliveSetting;->access$600(Lcom/samsung/musicplus/settings/SoundAliveSetting;)Landroid/view/View;

    move-result-object v1

    # invokes: Lcom/samsung/musicplus/settings/SoundAliveSetting;->enableExtended(Landroid/view/View;)V
    invoke-static {v0, v1}, Lcom/samsung/musicplus/settings/SoundAliveSetting;->access$800(Lcom/samsung/musicplus/settings/SoundAliveSetting;Landroid/view/View;)V

    .line 219
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$1;->this$0:Lcom/samsung/musicplus/settings/SoundAliveSetting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveSetting;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;
    invoke-static {v0}, Lcom/samsung/musicplus/settings/SoundAliveSetting;->access$100(Lcom/samsung/musicplus/settings/SoundAliveSetting;)Lcom/samsung/musicplus/library/audio/SecAudioManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->isAudioPathSpeaker()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 222
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$1;->this$0:Lcom/samsung/musicplus/settings/SoundAliveSetting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveSetting;->mUserEqPagerAdapter:Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqPagerAdapter;
    invoke-static {v0}, Lcom/samsung/musicplus/settings/SoundAliveSetting;->access$900(Lcom/samsung/musicplus/settings/SoundAliveSetting;)Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqPagerAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqPagerAdapter;->disalbe3dEffect()V

    goto :goto_0

    .line 224
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$1;->this$0:Lcom/samsung/musicplus/settings/SoundAliveSetting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveSetting;->mUserEqPagerAdapter:Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqPagerAdapter;
    invoke-static {v0}, Lcom/samsung/musicplus/settings/SoundAliveSetting;->access$900(Lcom/samsung/musicplus/settings/SoundAliveSetting;)Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqPagerAdapter;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqPagerAdapter;->enable3dEffect()V

    goto :goto_0
.end method
