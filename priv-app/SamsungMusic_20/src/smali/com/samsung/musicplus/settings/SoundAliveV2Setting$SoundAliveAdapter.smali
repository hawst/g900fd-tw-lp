.class Lcom/samsung/musicplus/settings/SoundAliveV2Setting$SoundAliveAdapter;
.super Landroid/widget/BaseAdapter;
.source "SoundAliveV2Setting.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/settings/SoundAliveV2Setting;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "SoundAliveAdapter"
.end annotation


# static fields
.field private static final CLASSIC:I = 0x2

.field private static final JAZZ:I = 0x10

.field private static final NORMAL:I = 0xc

.field private static final POP:I = 0xd

.field private static final ROCK:I = 0xa


# instance fields
.field private context:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "c"    # Landroid/content/Context;

    .prologue
    .line 1858
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 1859
    iput-object p1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$SoundAliveAdapter;->context:Landroid/content/Context;

    .line 1860
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 1863
    const/16 v0, 0x19

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 1867
    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 1871
    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 1876
    iget-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$SoundAliveAdapter;->context:Landroid/content/Context;

    const v4, 0x7f04007e

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 1878
    .local v2, "view":Landroid/view/View;
    const v3, 0x7f0d0185

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 1879
    .local v1, "text":Landroid/widget/TextView;
    const v3, 0x7f0d0184

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 1882
    .local v0, "imageView":Landroid/widget/ImageView;
    if-nez p2, :cond_0

    .line 1883
    sparse-switch p1, :sswitch_data_0

    .line 1901
    const-string v3, " "

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1908
    :goto_0
    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSquarePosition:I
    invoke-static {}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$2000()I

    move-result v3

    if-ne v3, p1, :cond_1

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mLastControlledTab:Z
    invoke-static {}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$3200()Z

    move-result v3

    if-nez v3, :cond_1

    .line 1909
    iget-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$SoundAliveAdapter;->context:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0200bc

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 1915
    :goto_1
    return-object v2

    .line 1885
    :sswitch_0
    iget-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$SoundAliveAdapter;->context:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f10003f

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1888
    :sswitch_1
    iget-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$SoundAliveAdapter;->context:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f100140

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1891
    :sswitch_2
    iget-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$SoundAliveAdapter;->context:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f100109

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1894
    :sswitch_3
    iget-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$SoundAliveAdapter;->context:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f100128

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1897
    :sswitch_4
    iget-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$SoundAliveAdapter;->context:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f100098

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 1905
    :cond_0
    move-object v2, p2

    goto :goto_0

    .line 1912
    :cond_1
    iget-object v3, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$SoundAliveAdapter;->context:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0200bb

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    .line 1883
    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0xa -> :sswitch_1
        0xc -> :sswitch_2
        0xd -> :sswitch_3
        0x10 -> :sswitch_4
    .end sparse-switch
.end method
