.class Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqLevelView;
.super Landroid/view/View;
.source "SoundAliveV2Setting.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/settings/SoundAliveV2Setting;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "UserEqLevelView"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;


# direct methods
.method public constructor <init>(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;Landroid/content/Context;)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 1662
    iput-object p1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqLevelView;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    .line 1663
    invoke-direct {p0, p2}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 1664
    return-void
.end method


# virtual methods
.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 14
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 1669
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 1671
    const/high16 v8, 0x42540000    # 53.0f

    .line 1674
    .local v8, "linePosX":F
    const/high16 v12, 0x42a40000    # 82.0f

    .line 1675
    .local v12, "paddingTopPortrait":F
    const/high16 v11, 0x42960000    # 75.0f

    .line 1677
    .local v11, "paddingTopHorizontal":F
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 1678
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqLevelView;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mDbLinePaddingTop:I
    invoke-static {v1}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$3400(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    .line 1680
    new-instance v5, Landroid/graphics/Paint;

    const/4 v0, 0x1

    invoke-direct {v5, v0}, Landroid/graphics/Paint;-><init>(I)V

    .line 1681
    .local v5, "pnt":Landroid/graphics/Paint;
    const/16 v0, 0xaa

    const/16 v1, 0xaa

    const/16 v2, 0xaa

    invoke-static {v0, v1, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 1683
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqLevelView;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # invokes: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->getScreenRawWidth()I
    invoke-static {v0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$3600(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)I

    move-result v13

    .line 1684
    .local v13, "screenRawidth":I
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqLevelView;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # invokes: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->isPortrait()Z
    invoke-static {v0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$2600(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)Z

    move-result v7

    .line 1685
    .local v7, "isPortrait":Z
    if-eqz v7, :cond_0

    const/16 v0, 0x5a0

    if-ge v13, v0, :cond_1

    :cond_0
    if-nez v7, :cond_2

    const/16 v0, 0xa00

    if-lt v13, v0, :cond_2

    .line 1687
    :cond_1
    const/high16 v0, 0x42180000    # 38.0f

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 1688
    const/high16 v8, 0x43b40000    # 360.0f

    .line 1689
    const/high16 v10, 0x40400000    # 3.0f

    .line 1690
    .local v10, "offsetLinePosY":F
    const/high16 v12, 0x42e20000    # 113.0f

    .line 1691
    const/high16 v11, 0x42d40000    # 106.0f

    .line 1692
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqLevelView;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    const-wide v2, 0x3fea76c8b4395810L    # 0.827

    # setter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mRatePortlinePosY:D
    invoke-static {v0, v2, v3}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$3702(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;D)D

    .line 1693
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqLevelView;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    const-wide v2, 0x3fea666666666666L    # 0.825

    # setter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mRateLandlinePosY:D
    invoke-static {v0, v2, v3}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$3802(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;D)D

    .line 1755
    :goto_0
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_1
    const/16 v0, 0xa

    if-gt v6, v0, :cond_18

    .line 1756
    if-eqz v7, :cond_17

    .line 1757
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqLevelView;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSeekBarHeight:I
    invoke-static {v0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$4000(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)I

    move-result v0

    mul-int/2addr v0, v6

    int-to-float v0, v0

    const/high16 v1, 0x41200000    # 10.0f

    div-float/2addr v0, v1

    iget-object v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqLevelView;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mRatePortlinePosY:D
    invoke-static {v1}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$3700(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)D

    move-result-wide v2

    double-to-float v1, v2

    mul-float/2addr v0, v1

    add-float v9, v0, v12

    .line 1759
    .local v9, "linePosY":F
    const/high16 v1, 0x41200000    # 10.0f

    add-float v2, v9, v10

    const v0, 0x44598000    # 870.0f

    add-float v3, v8, v0

    add-float v4, v9, v10

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 1755
    :goto_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 1694
    .end local v6    # "i":I
    .end local v9    # "linePosY":F
    .end local v10    # "offsetLinePosY":F
    :cond_2
    if-eqz v7, :cond_3

    const/16 v0, 0x438

    if-ge v13, v0, :cond_4

    :cond_3
    if-nez v7, :cond_5

    const/16 v0, 0x780

    if-lt v13, v0, :cond_5

    .line 1695
    :cond_4
    const/high16 v0, 0x42180000    # 38.0f

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 1696
    const/high16 v8, 0x42860000    # 67.0f

    .line 1697
    const/high16 v10, 0x40400000    # 3.0f

    .line 1698
    .restart local v10    # "offsetLinePosY":F
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqLevelView;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    const-wide v2, 0x3fea7ef9db22d0e5L    # 0.828

    # setter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mRatePortlinePosY:D
    invoke-static {v0, v2, v3}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$3702(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;D)D

    .line 1699
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqLevelView;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    const-wide v2, 0x3fea6e978d4fdf3bL    # 0.826

    # setter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mRateLandlinePosY:D
    invoke-static {v0, v2, v3}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$3802(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;D)D

    goto :goto_0

    .line 1700
    .end local v10    # "offsetLinePosY":F
    :cond_5
    if-eqz v7, :cond_6

    const/16 v0, 0x2d0

    if-ge v13, v0, :cond_7

    :cond_6
    if-nez v7, :cond_9

    const/16 v0, 0x500

    if-lt v13, v0, :cond_9

    .line 1701
    :cond_7
    const/high16 v0, 0x41a00000    # 20.0f

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 1702
    const/high16 v8, -0x3c8b0000    # -245.0f

    .line 1703
    const/high16 v10, 0x40400000    # 3.0f

    .line 1704
    .restart local v10    # "offsetLinePosY":F
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqLevelView;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # invokes: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->getDensity()I
    invoke-static {v0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$3900(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)I

    move-result v0

    const/16 v1, 0x140

    if-ne v0, v1, :cond_8

    .line 1706
    const/high16 v12, 0x42540000    # 53.0f

    .line 1707
    const/high16 v11, 0x42480000    # 50.0f

    .line 1708
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqLevelView;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    const-wide v2, 0x3fea7ef9db22d0e5L    # 0.828

    # setter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mRatePortlinePosY:D
    invoke-static {v0, v2, v3}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$3702(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;D)D

    .line 1709
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqLevelView;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    const-wide v2, 0x3fea5604189374bcL    # 0.823

    # setter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mRateLandlinePosY:D
    invoke-static {v0, v2, v3}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$3802(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;D)D

    goto/16 :goto_0

    .line 1711
    :cond_8
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqLevelView;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    const-wide v2, 0x3fea2d0e56041893L    # 0.818

    # setter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mRatePortlinePosY:D
    invoke-static {v0, v2, v3}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$3702(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;D)D

    .line 1712
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqLevelView;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    const-wide v2, 0x3fe62d0e56041893L    # 0.693

    # setter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mRateLandlinePosY:D
    invoke-static {v0, v2, v3}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$3802(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;D)D

    goto/16 :goto_0

    .line 1714
    .end local v10    # "offsetLinePosY":F
    :cond_9
    if-eqz v7, :cond_a

    const/16 v0, 0x21c

    if-gt v13, v0, :cond_b

    :cond_a
    if-nez v7, :cond_d

    const/16 v0, 0x3c0

    if-le v13, v0, :cond_d

    .line 1715
    :cond_b
    const/high16 v0, 0x41a00000    # 20.0f

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 1716
    const/high16 v8, 0x42200000    # 40.0f

    .line 1717
    const/high16 v10, 0x40400000    # 3.0f

    .line 1718
    .restart local v10    # "offsetLinePosY":F
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqLevelView;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # invokes: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->getDensity()I
    invoke-static {v0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$3900(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)I

    move-result v0

    const/16 v1, 0xf0

    if-ne v0, v1, :cond_c

    .line 1720
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqLevelView;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    const-wide v2, 0x3febc6a7ef9db22dL    # 0.868

    # setter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mRatePortlinePosY:D
    invoke-static {v0, v2, v3}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$3702(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;D)D

    .line 1721
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqLevelView;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    const-wide v2, 0x3fe9b22d0e560419L    # 0.803

    # setter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mRateLandlinePosY:D
    invoke-static {v0, v2, v3}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$3802(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;D)D

    goto/16 :goto_0

    .line 1723
    :cond_c
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqLevelView;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    const-wide v2, 0x3fea2d0e56041893L    # 0.818

    # setter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mRatePortlinePosY:D
    invoke-static {v0, v2, v3}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$3702(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;D)D

    .line 1724
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqLevelView;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    const-wide v2, 0x3fe62d0e56041893L    # 0.693

    # setter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mRateLandlinePosY:D
    invoke-static {v0, v2, v3}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$3802(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;D)D

    goto/16 :goto_0

    .line 1727
    .end local v10    # "offsetLinePosY":F
    :cond_d
    if-eqz v7, :cond_e

    const/16 v0, 0x1e0

    if-gt v13, v0, :cond_f

    :cond_e
    if-nez v7, :cond_10

    const/16 v0, 0x320

    if-le v13, v0, :cond_10

    .line 1728
    :cond_f
    const/high16 v0, 0x41a00000    # 20.0f

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 1729
    const/high16 v8, 0x42200000    # 40.0f

    .line 1730
    const/high16 v12, 0x42240000    # 41.0f

    .line 1731
    const/high16 v11, 0x42140000    # 37.0f

    .line 1732
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqLevelView;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    const-wide v2, 0x3fea7ef9db22d0e5L    # 0.828

    # setter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mRatePortlinePosY:D
    invoke-static {v0, v2, v3}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$3702(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;D)D

    .line 1733
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqLevelView;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    const-wide v2, 0x3fea45a1cac08312L    # 0.821

    # setter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mRateLandlinePosY:D
    invoke-static {v0, v2, v3}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$3802(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;D)D

    .line 1752
    :goto_3
    const/high16 v10, 0x40000000    # 2.0f

    .restart local v10    # "offsetLinePosY":F
    goto/16 :goto_0

    .line 1734
    .end local v10    # "offsetLinePosY":F
    :cond_10
    if-eqz v7, :cond_11

    const/16 v0, 0x140

    if-gt v13, v0, :cond_12

    :cond_11
    if-nez v7, :cond_13

    const/16 v0, 0x1e0

    if-le v13, v0, :cond_13

    .line 1735
    :cond_12
    const/high16 v0, 0x41a00000    # 20.0f

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 1736
    const/high16 v8, 0x42200000    # 40.0f

    .line 1737
    const/high16 v12, 0x41f00000    # 30.0f

    .line 1738
    const/high16 v11, 0x41f00000    # 30.0f

    .line 1739
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqLevelView;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    const-wide v2, 0x3feab851eb851eb8L    # 0.835

    # setter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mRatePortlinePosY:D
    invoke-static {v0, v2, v3}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$3702(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;D)D

    .line 1740
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqLevelView;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    const-wide v2, 0x3fe9eb851eb851ecL    # 0.81

    # setter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mRateLandlinePosY:D
    invoke-static {v0, v2, v3}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$3802(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;D)D

    goto :goto_3

    .line 1741
    :cond_13
    if-eqz v7, :cond_14

    const/16 v0, 0xf0

    if-gt v13, v0, :cond_15

    :cond_14
    if-nez v7, :cond_16

    const/16 v0, 0x140

    if-le v13, v0, :cond_16

    .line 1742
    :cond_15
    const/high16 v0, 0x41600000    # 14.0f

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 1743
    const/high16 v8, 0x424c0000    # 51.0f

    .line 1744
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqLevelView;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    const-wide v2, 0x3fe9eb851eb851ecL    # 0.81

    # setter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mRatePortlinePosY:D
    invoke-static {v0, v2, v3}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$3702(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;D)D

    .line 1745
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqLevelView;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    const-wide v2, 0x3fe5c28f5c28f5c3L    # 0.68

    # setter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mRateLandlinePosY:D
    invoke-static {v0, v2, v3}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$3802(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;D)D

    goto :goto_3

    .line 1747
    :cond_16
    const/high16 v0, 0x41300000    # 11.0f

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 1748
    const/high16 v8, 0x42200000    # 40.0f

    .line 1749
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqLevelView;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    const-wide v2, 0x3fe9eb851eb851ecL    # 0.81

    # setter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mRatePortlinePosY:D
    invoke-static {v0, v2, v3}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$3702(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;D)D

    .line 1750
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqLevelView;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    const-wide v2, 0x3fe5c28f5c28f5c3L    # 0.68

    # setter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mRateLandlinePosY:D
    invoke-static {v0, v2, v3}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$3802(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;D)D

    goto :goto_3

    .line 1762
    .restart local v6    # "i":I
    .restart local v10    # "offsetLinePosY":F
    :cond_17
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqLevelView;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mSeekBarHeight:I
    invoke-static {v0}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$4000(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)I

    move-result v0

    mul-int/2addr v0, v6

    int-to-float v0, v0

    const/high16 v1, 0x41200000    # 10.0f

    div-float/2addr v0, v1

    iget-object v1, p0, Lcom/samsung/musicplus/settings/SoundAliveV2Setting$UserEqLevelView;->this$0:Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    # getter for: Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->mRateLandlinePosY:D
    invoke-static {v1}, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;->access$3800(Lcom/samsung/musicplus/settings/SoundAliveV2Setting;)D

    move-result-wide v2

    double-to-float v1, v2

    mul-float/2addr v0, v1

    add-float v9, v0, v11

    .line 1764
    .restart local v9    # "linePosY":F
    const/4 v1, 0x0

    add-float v2, v9, v10

    const/high16 v0, 0x44480000    # 800.0f

    add-float v3, v8, v0

    add-float v4, v9, v10

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    goto/16 :goto_2

    .line 1768
    .end local v9    # "linePosY":F
    :cond_18
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    .line 1769
    return-void
.end method
