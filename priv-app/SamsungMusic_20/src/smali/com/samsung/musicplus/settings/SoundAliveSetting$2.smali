.class Lcom/samsung/musicplus/settings/SoundAliveSetting$2;
.super Ljava/lang/Object;
.source "SoundAliveSetting.java"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/settings/SoundAliveSetting;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/settings/SoundAliveSetting;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/settings/SoundAliveSetting;)V
    .locals 0

    .prologue
    .line 253
    iput-object p1, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$2;->this$0:Lcom/samsung/musicplus/settings/SoundAliveSetting;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 4
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 255
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const-string v1, "MusicSetting"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onItemSelected position : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " id : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    # getter for: Lcom/samsung/musicplus/settings/SoundAliveSetting;->EQ_LIST:[I
    invoke-static {}, Lcom/samsung/musicplus/settings/SoundAliveSetting;->access$000()[I

    move-result-object v1

    aget v0, v1, p3

    .line 257
    .local v0, "eq":I
    const/16 v1, 0xd

    if-ne v0, v1, :cond_0

    .line 259
    iget-object v1, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$2;->this$0:Lcom/samsung/musicplus/settings/SoundAliveSetting;

    # invokes: Lcom/samsung/musicplus/settings/SoundAliveSetting;->activateUserEq()V
    invoke-static {v1}, Lcom/samsung/musicplus/settings/SoundAliveSetting;->access$1000(Lcom/samsung/musicplus/settings/SoundAliveSetting;)V

    .line 264
    :goto_0
    invoke-static {v0}, Lcom/samsung/musicplus/util/ServiceUtils;->setSoundAlive(I)V

    .line 265
    return-void

    .line 261
    :cond_0
    iget-object v1, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$2;->this$0:Lcom/samsung/musicplus/settings/SoundAliveSetting;

    # invokes: Lcom/samsung/musicplus/settings/SoundAliveSetting;->deactivateUserEq()V
    invoke-static {v1}, Lcom/samsung/musicplus/settings/SoundAliveSetting;->access$1100(Lcom/samsung/musicplus/settings/SoundAliveSetting;)V

    goto :goto_0
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 268
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    return-void
.end method
