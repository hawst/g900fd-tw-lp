.class public Lcom/samsung/musicplus/settings/PlayerSettingFragment;
.super Landroid/preference/PreferenceFragment;
.source "PlayerSettingFragment.java"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Lcom/samsung/musicplus/settings/PlayerSettingPreference;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/settings/PlayerSettingFragment$CustomTimePickerDialog;
    }
.end annotation


# static fields
.field private static final AUTO_OFF_CUSTOM:Ljava/lang/String; = "-999"

.field private static final AUTO_OFF_DEFAULT:Ljava/lang/String; = "0"

.field private static final DELAY_DISSMISS_PROGRESS_DIALOG:I = 0x3e8

.field private static final DISMISS_PROGRES_COUNT:I = 0x64

.field private static final DIVIDER:Ljava/lang/String; = ", "

.field private static final HANDLE_DISSMISS_PROGRESS_DIALOG:I = 0x0

.field public static final REQUEST_CODE_CHANGED_ADAPT_SOUND:I = 0x2

.field public static final REQUEST_CODE_CHANGED_PLAY_SPEED:I = 0x1

.field private static final TAG:Ljava/lang/String; = "MusicSetting"

.field private static final UPDATE_AUTO_OFF:I = 0x1


# instance fields
.field private final SAFX_GENRE_INFO:Ljava/lang/String;

.field private final SAFX_REQUEST_GENRE:Ljava/lang/String;

.field private mAdaptSound:Landroid/preference/Preference;

.field private mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

.field private mAudioPathChangedReceiver:Landroid/content/BroadcastReceiver;

.field private final mAutoOffReceiver:Landroid/content/BroadcastReceiver;

.field private mContext:Landroid/content/Context;

.field private mCount:I

.field private mCuAutoOffHour:I

.field private mCuAutoOffMin:I

.field private mHandler:Landroid/os/Handler;

.field private mK2HD:Landroid/preference/CheckBoxPreference;

.field private mMusicAutoOff:Landroid/preference/ListPreference;

.field private mPause:Z

.field private mPlayListsMenuSetting:Landroid/preference/Preference;

.field private mPlaySpeed:Landroid/preference/Preference;

.field private mPrefScreen:Landroid/preference/PreferenceScreen;

.field private mProgressDialog:Landroid/app/ProgressDialog;

.field private mSAEffectInfoReceiver:Landroid/content/BroadcastReceiver;

.field private mSlinkRegister:Landroid/preference/CheckBoxPreference;

.field private mSmartVolume:Landroid/preference/CheckBoxPreference;

.field private mSoundAlive:Landroid/preference/Preference;

.field private final mSummaryUpdateHandler:Landroid/os/Handler;

.field private mTabMenuSetting:Landroid/preference/Preference;

.field mTimeSetListener:Landroid/app/TimePickerDialog$OnTimeSetListener;

.field private mUiPreference:Landroid/content/SharedPreferences;

.field private mVoiceControl:Landroid/preference/CheckBoxPreference;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 71
    invoke-direct {p0}, Landroid/preference/PreferenceFragment;-><init>()V

    .line 125
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mPause:Z

    .line 127
    const-string v0, "com.sec.android.app.safx.ACTION_REQUEST_GENRE"

    iput-object v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->SAFX_REQUEST_GENRE:Ljava/lang/String;

    .line 129
    const-string v0, "com.sec.android.app.SA_GENRE_INFO"

    iput-object v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->SAFX_GENRE_INFO:Ljava/lang/String;

    .line 375
    new-instance v0, Lcom/samsung/musicplus/settings/PlayerSettingFragment$5;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/settings/PlayerSettingFragment$5;-><init>(Lcom/samsung/musicplus/settings/PlayerSettingFragment;)V

    iput-object v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mHandler:Landroid/os/Handler;

    .line 591
    new-instance v0, Lcom/samsung/musicplus/settings/PlayerSettingFragment$6;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/settings/PlayerSettingFragment$6;-><init>(Lcom/samsung/musicplus/settings/PlayerSettingFragment;)V

    iput-object v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mSummaryUpdateHandler:Landroid/os/Handler;

    .line 642
    new-instance v0, Lcom/samsung/musicplus/settings/PlayerSettingFragment$7;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/settings/PlayerSettingFragment$7;-><init>(Lcom/samsung/musicplus/settings/PlayerSettingFragment;)V

    iput-object v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mTimeSetListener:Landroid/app/TimePickerDialog$OnTimeSetListener;

    .line 976
    new-instance v0, Lcom/samsung/musicplus/settings/PlayerSettingFragment$10;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/settings/PlayerSettingFragment$10;-><init>(Lcom/samsung/musicplus/settings/PlayerSettingFragment;)V

    iput-object v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mAutoOffReceiver:Landroid/content/BroadcastReceiver;

    .line 985
    new-instance v0, Lcom/samsung/musicplus/settings/PlayerSettingFragment$11;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/settings/PlayerSettingFragment$11;-><init>(Lcom/samsung/musicplus/settings/PlayerSettingFragment;)V

    iput-object v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mAudioPathChangedReceiver:Landroid/content/BroadcastReceiver;

    .line 993
    new-instance v0, Lcom/samsung/musicplus/settings/PlayerSettingFragment$12;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/settings/PlayerSettingFragment$12;-><init>(Lcom/samsung/musicplus/settings/PlayerSettingFragment;)V

    iput-object v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mSAEffectInfoReceiver:Landroid/content/BroadcastReceiver;

    .line 1006
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/settings/PlayerSettingFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/PlayerSettingFragment;

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->showSlinkRegisterDialog()V

    return-void
.end method

.method static synthetic access$100(Lcom/samsung/musicplus/settings/PlayerSettingFragment;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/PlayerSettingFragment;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/samsung/musicplus/settings/PlayerSettingFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/PlayerSettingFragment;

    .prologue
    .line 71
    iget v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mCuAutoOffHour:I

    return v0
.end method

.method static synthetic access$1002(Lcom/samsung/musicplus/settings/PlayerSettingFragment;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/PlayerSettingFragment;
    .param p1, "x1"    # I

    .prologue
    .line 71
    iput p1, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mCuAutoOffHour:I

    return p1
.end method

.method static synthetic access$1100(Lcom/samsung/musicplus/settings/PlayerSettingFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/PlayerSettingFragment;

    .prologue
    .line 71
    iget v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mCuAutoOffMin:I

    return v0
.end method

.method static synthetic access$1102(Lcom/samsung/musicplus/settings/PlayerSettingFragment;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/PlayerSettingFragment;
    .param p1, "x1"    # I

    .prologue
    .line 71
    iput p1, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mCuAutoOffMin:I

    return p1
.end method

.method static synthetic access$1200(Lcom/samsung/musicplus/settings/PlayerSettingFragment;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/PlayerSettingFragment;
    .param p1, "x1"    # I

    .prologue
    .line 71
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->setAlarm(I)V

    return-void
.end method

.method static synthetic access$1300(Lcom/samsung/musicplus/settings/PlayerSettingFragment;)Landroid/content/SharedPreferences;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/PlayerSettingFragment;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mUiPreference:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/samsung/musicplus/settings/PlayerSettingFragment;Landroid/content/SharedPreferences;Ljava/lang/String;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/PlayerSettingFragment;
    .param p1, "x1"    # Landroid/content/SharedPreferences;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # I

    .prologue
    .line 71
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->savePreference(Landroid/content/SharedPreferences;Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic access$1500(Lcom/samsung/musicplus/settings/PlayerSettingFragment;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/PlayerSettingFragment;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mSummaryUpdateHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/samsung/musicplus/settings/PlayerSettingFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/PlayerSettingFragment;

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->updateSoundAlive()V

    return-void
.end method

.method static synthetic access$1700(Lcom/samsung/musicplus/settings/PlayerSettingFragment;II)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/PlayerSettingFragment;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 71
    invoke-direct {p0, p1, p2}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->updateSAEffectState(II)V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/musicplus/settings/PlayerSettingFragment;)Landroid/preference/CheckBoxPreference;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/PlayerSettingFragment;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mSlinkRegister:Landroid/preference/CheckBoxPreference;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/musicplus/settings/PlayerSettingFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/PlayerSettingFragment;

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->showProgressDialog()V

    return-void
.end method

.method static synthetic access$400(Lcom/samsung/musicplus/settings/PlayerSettingFragment;)Landroid/app/ProgressDialog;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/PlayerSettingFragment;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/musicplus/settings/PlayerSettingFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/PlayerSettingFragment;

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->dismissProgressDialog()V

    return-void
.end method

.method static synthetic access$600(Lcom/samsung/musicplus/settings/PlayerSettingFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/PlayerSettingFragment;

    .prologue
    .line 71
    iget v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mCount:I

    return v0
.end method

.method static synthetic access$608(Lcom/samsung/musicplus/settings/PlayerSettingFragment;)I
    .locals 2
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/PlayerSettingFragment;

    .prologue
    .line 71
    iget v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mCount:I

    return v0
.end method

.method static synthetic access$700(Lcom/samsung/musicplus/settings/PlayerSettingFragment;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/PlayerSettingFragment;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$800(Lcom/samsung/musicplus/settings/PlayerSettingFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/PlayerSettingFragment;

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->updateAutoOffSummary()V

    return-void
.end method

.method static synthetic access$900(Lcom/samsung/musicplus/settings/PlayerSettingFragment;)Landroid/preference/ListPreference;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/PlayerSettingFragment;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mMusicAutoOff:Landroid/preference/ListPreference;

    return-object v0
.end method

.method private canShowDialog()Z
    .locals 1

    .prologue
    .line 922
    iget-boolean v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mPause:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private disableVoiceControl()V
    .locals 2

    .prologue
    .line 430
    iget-object v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mPrefScreen:Landroid/preference/PreferenceScreen;

    iget-object v1, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mVoiceControl:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 431
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mVoiceControl:Landroid/preference/CheckBoxPreference;

    .line 432
    return-void
.end method

.method private dismissProgressDialog()V
    .locals 1

    .prologue
    .line 408
    iget-object v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    .line 409
    iget-object v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 410
    iget-object v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    .line 413
    :cond_0
    return-void
.end method

.method private getCustomSetTime()Ljava/lang/CharSequence;
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 707
    iget v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mCuAutoOffHour:I

    if-nez v0, :cond_1

    .line 708
    iget v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mCuAutoOffMin:I

    if-ne v0, v3, :cond_0

    .line 709
    const v0, 0x7f100017

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 730
    :goto_0
    return-object v0

    .line 710
    :cond_0
    iget v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mCuAutoOffMin:I

    if-le v0, v3, :cond_7

    .line 711
    const v0, 0x7f10001d

    new-array v1, v3, [Ljava/lang/Object;

    iget v2, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mCuAutoOffMin:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p0, v0, v1}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 713
    :cond_1
    iget v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mCuAutoOffHour:I

    if-ne v0, v3, :cond_4

    .line 714
    iget v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mCuAutoOffMin:I

    if-nez v0, :cond_2

    .line 715
    const v0, 0x7f100014

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 716
    :cond_2
    iget v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mCuAutoOffMin:I

    if-ne v0, v3, :cond_3

    .line 717
    const v0, 0x7f100015

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 718
    :cond_3
    iget v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mCuAutoOffMin:I

    if-le v0, v3, :cond_7

    .line 719
    const v0, 0x7f100016

    new-array v1, v3, [Ljava/lang/Object;

    iget v2, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mCuAutoOffMin:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p0, v0, v1}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 721
    :cond_4
    iget v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mCuAutoOffHour:I

    if-le v0, v3, :cond_7

    .line 722
    iget v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mCuAutoOffMin:I

    if-nez v0, :cond_5

    .line 723
    const v0, 0x7f10001a

    new-array v1, v3, [Ljava/lang/Object;

    iget v2, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mCuAutoOffHour:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p0, v0, v1}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 724
    :cond_5
    iget v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mCuAutoOffMin:I

    if-ne v0, v3, :cond_6

    .line 725
    const v0, 0x7f10001b

    new-array v1, v3, [Ljava/lang/Object;

    iget v2, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mCuAutoOffHour:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {p0, v0, v1}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 726
    :cond_6
    iget v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mCuAutoOffMin:I

    if-le v0, v3, :cond_7

    .line 727
    const v0, 0x7f10001c

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    iget v2, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mCuAutoOffHour:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    iget v2, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mCuAutoOffMin:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {p0, v0, v1}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 730
    :cond_7
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method private getSAUserEffectSummary()Ljava/lang/StringBuilder;
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 764
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    .line 766
    .local v6, "summary":Ljava/lang/StringBuilder;
    const/4 v4, 0x0

    .line 768
    .local v4, "isEqSet":Z
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getSoundAliveUserEQ()[I

    move-result-object v1

    .line 771
    .local v1, "eq":[I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    const/4 v7, 0x7

    if-ge v3, v7, :cond_0

    .line 772
    if-nez v1, :cond_5

    .line 773
    const-string v7, "MusicSetting"

    const-string v8, "User eq is null, please check codes."

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 783
    :cond_0
    :goto_1
    const-string v0, ", "

    .line 784
    .local v0, "divider":Ljava/lang/String;
    const-string v7, ""

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 785
    const-string v0, ""

    .line 788
    :cond_1
    const/4 v5, 0x0

    .line 789
    .local v5, "isExtSet":Z
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getSoundAliveUserExt()[I

    move-result-object v2

    .line 791
    .local v2, "extend":[I
    const/4 v3, 0x0

    :goto_2
    const/4 v7, 0x5

    if-ge v3, v7, :cond_2

    .line 792
    if-nez v2, :cond_7

    .line 793
    const-string v7, "MusicSetting"

    const-string v8, "User extend effect is null, please check codes."

    invoke-static {v7, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 810
    :cond_2
    if-nez v4, :cond_3

    if-eqz v5, :cond_4

    .line 811
    :cond_3
    const-string v7, " ("

    invoke-virtual {v6, v9, v7}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 812
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->length()I

    move-result v7

    const-string v8, ")"

    invoke-virtual {v6, v7, v8}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 814
    :cond_4
    return-object v6

    .line 776
    .end local v0    # "divider":Ljava/lang/String;
    .end local v2    # "extend":[I
    .end local v5    # "isExtSet":Z
    :cond_5
    aget v7, v1, v3

    if-eqz v7, :cond_6

    .line 777
    const/4 v4, 0x1

    .line 778
    iget-object v7, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mContext:Landroid/content/Context;

    invoke-static {v7}, Lcom/samsung/musicplus/util/UiUtils;->getEqString(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 771
    :cond_6
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 800
    .restart local v0    # "divider":Ljava/lang/String;
    .restart local v2    # "extend":[I
    .restart local v5    # "isExtSet":Z
    :cond_7
    iget-object v7, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

    invoke-virtual {v7}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->isWiredHeadsetOn()Z

    move-result v7

    if-nez v7, :cond_8

    .line 801
    aput v9, v2, v9

    .line 803
    :cond_8
    aget v7, v2, v3

    if-eqz v7, :cond_9

    .line 804
    const/4 v5, 0x1

    .line 805
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    sget-object v8, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserExtendFragment;->EXT_STRING_RES_ID:[I

    aget v8, v8, v3

    invoke-virtual {p0, v8}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 806
    const-string v0, ", "

    .line 791
    :cond_9
    add-int/lit8 v3, v3, 0x1

    goto :goto_2
.end method

.method private handleVoicePref(I)V
    .locals 8
    .param p1, "voice"    # I

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 540
    if-nez p1, :cond_1

    .line 541
    new-instance v2, Landroid/content/Intent;

    const-string v5, "android.intent.action.VOICE_SETTING_BARGEIN"

    invoke-direct {v2, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 542
    .local v2, "intent":Landroid/content/Intent;
    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    invoke-virtual {v2, v5}, Landroid/content/Intent;->resolveActivity(Landroid/content/pm/PackageManager;)Landroid/content/ComponentName;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 543
    invoke-virtual {p0, v2}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->startActivity(Landroid/content/Intent;)V

    .line 569
    .end local v2    # "intent":Landroid/content/Intent;
    :goto_0
    return-void

    .line 545
    .restart local v2    # "intent":Landroid/content/Intent;
    :cond_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 546
    .local v1, "i":Landroid/content/Intent;
    const-string v5, "com.sec.android.app.popupuireceiver"

    const-string v6, "com.sec.android.app.popupuireceiver.DisableApp"

    invoke-virtual {v1, v5, v6}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 548
    const-string v5, "app_package_name"

    const-string v6, "com.vlingo.midas"

    invoke-virtual {v1, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 550
    const/4 v5, 0x0

    :try_start_0
    invoke-virtual {p0, v1, v5}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 551
    :catch_0
    move-exception v0

    .line 552
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v5, "MusicSetting"

    const-string v6, "Activity Not found!!!"

    invoke-static {v5, v6}, Lcom/samsung/musicplus/library/iLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 553
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    .line 554
    iget-object v5, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mContext:Landroid/content/Context;

    const-string v6, "Target Activity Not Found"

    invoke-static {v5, v6, v7}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 560
    .end local v0    # "e":Landroid/content/ActivityNotFoundException;
    .end local v1    # "i":Landroid/content/Intent;
    .end local v2    # "intent":Landroid/content/Intent;
    :cond_1
    iget-object v5, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    .line 561
    .local v4, "r":Landroid/content/ContentResolver;
    const-string v5, "voice_input_control_music"

    invoke-static {v4, v5, v6}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    .line 564
    .local v3, "music":I
    if-nez v3, :cond_2

    .line 565
    const-string v5, "voice_input_control_music"

    invoke-static {v4, v5, v7}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_0

    .line 567
    :cond_2
    const-string v5, "voice_input_control_music"

    invoke-static {v4, v5, v6}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_0
.end method

.method private initializeSettingMenu()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 168
    iget-object v3, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mPrefScreen:Landroid/preference/PreferenceScreen;

    const-string v4, "tab_menu_list"

    invoke-virtual {v3, v4}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mTabMenuSetting:Landroid/preference/Preference;

    .line 171
    iget-object v3, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mPrefScreen:Landroid/preference/PreferenceScreen;

    const-string v4, "playlist_menu_list"

    invoke-virtual {v3, v4}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mPlayListsMenuSetting:Landroid/preference/Preference;

    .line 174
    iget-object v3, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mPrefScreen:Landroid/preference/PreferenceScreen;

    const-string v4, "sound_alive"

    invoke-virtual {v3, v4}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mSoundAlive:Landroid/preference/Preference;

    .line 177
    sget-boolean v3, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_SUPPORT_ADAPT_SOUND:Z

    if-eqz v3, :cond_0

    .line 178
    new-instance v3, Landroid/preference/Preference;

    iget-object v4, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mAdaptSound:Landroid/preference/Preference;

    .line 179
    iget-object v3, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mAdaptSound:Landroid/preference/Preference;

    const-string v4, "adapt_sound"

    invoke-virtual {v3, v4}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    .line 180
    iget-object v3, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mAdaptSound:Landroid/preference/Preference;

    const v4, 0x7f100006

    invoke-virtual {v3, v4}, Landroid/preference/Preference;->setTitle(I)V

    .line 181
    iget-object v3, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mAdaptSound:Landroid/preference/Preference;

    const/4 v4, 0x3

    invoke-virtual {v3, v4}, Landroid/preference/Preference;->setOrder(I)V

    .line 182
    iget-object v3, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mPrefScreen:Landroid/preference/PreferenceScreen;

    iget-object v4, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mAdaptSound:Landroid/preference/Preference;

    invoke-virtual {v3, v4}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 186
    :cond_0
    iget-object v3, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mPrefScreen:Landroid/preference/PreferenceScreen;

    const-string v4, "play_speed"

    invoke-virtual {v3, v4}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mPlaySpeed:Landroid/preference/Preference;

    .line 189
    iget-object v3, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mPrefScreen:Landroid/preference/PreferenceScreen;

    const-string v4, "music_auto_off"

    invoke-virtual {v3, v4}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    check-cast v3, Landroid/preference/ListPreference;

    iput-object v3, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mMusicAutoOff:Landroid/preference/ListPreference;

    .line 190
    iget-object v3, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mMusicAutoOff:Landroid/preference/ListPreference;

    invoke-virtual {v3, p0}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 191
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->updateAutoOffstatus()V

    .line 194
    iget-object v3, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mPrefScreen:Landroid/preference/PreferenceScreen;

    const-string v4, "lyric"

    invoke-virtual {v3, v4}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    .line 195
    .local v0, "lyric":Landroid/preference/CheckBoxPreference;
    iget-object v3, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mUiPreference:Landroid/content/SharedPreferences;

    const-string v4, "lyric"

    invoke-interface {v3, v4, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/preference/CheckBoxPreference;->setDefaultValue(Ljava/lang/Object;)V

    .line 197
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->isLocalOrNoList()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->isDmrPlaying()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 198
    :cond_1
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 204
    :goto_0
    iget-object v3, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mPrefScreen:Landroid/preference/PreferenceScreen;

    const-string v4, "smart_volume"

    invoke-virtual {v3, v4}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    check-cast v3, Landroid/preference/CheckBoxPreference;

    iput-object v3, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mSmartVolume:Landroid/preference/CheckBoxPreference;

    .line 205
    sget-boolean v3, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_SMART_VOLUME:Z

    if-nez v3, :cond_2

    .line 206
    iget-object v3, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mPrefScreen:Landroid/preference/PreferenceScreen;

    iget-object v4, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mSmartVolume:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v3, v4}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 207
    iput-object v5, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mSmartVolume:Landroid/preference/CheckBoxPreference;

    .line 211
    :cond_2
    iget-object v3, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mPrefScreen:Landroid/preference/PreferenceScreen;

    const-string v4, "voice_control"

    invoke-virtual {v3, v4}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    check-cast v3, Landroid/preference/CheckBoxPreference;

    iput-object v3, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mVoiceControl:Landroid/preference/CheckBoxPreference;

    .line 212
    sget-boolean v3, Lcom/samsung/musicplus/util/Version;->LIBRARY_API_1:Z

    if-nez v3, :cond_3

    sget-boolean v3, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_VOICE_COMMAND_CONTROL:Z

    if-eqz v3, :cond_7

    .line 213
    :cond_3
    iget-object v3, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/samsung/musicplus/library/audio/SvoiceManager;->getInstance(Landroid/content/Context;)Lcom/samsung/musicplus/library/audio/SvoiceManager;

    move-result-object v1

    .line 214
    .local v1, "manager":Lcom/samsung/musicplus/library/audio/SvoiceManager;
    if-eqz v1, :cond_6

    iget-object v3, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/samsung/musicplus/library/audio/SvoiceManager;->isExistsSvoiceApp(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-virtual {v1}, Lcom/samsung/musicplus/library/audio/SvoiceManager;->isVoiceControlEnabled()Z

    move-result v3

    if-eqz v3, :cond_6

    sget-boolean v3, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_SUPPORT_SVOICE_FOR_GEAR:Z

    if-nez v3, :cond_6

    .line 226
    .end local v1    # "manager":Lcom/samsung/musicplus/library/audio/SvoiceManager;
    :goto_1
    iget-object v3, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mPrefScreen:Landroid/preference/PreferenceScreen;

    const-string v4, "K2HD"

    invoke-virtual {v3, v4}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    check-cast v3, Landroid/preference/CheckBoxPreference;

    iput-object v3, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mK2HD:Landroid/preference/CheckBoxPreference;

    .line 227
    sget-boolean v3, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_K2HD:Z

    if-nez v3, :cond_4

    .line 228
    iget-object v3, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mPrefScreen:Landroid/preference/PreferenceScreen;

    iget-object v4, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mK2HD:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v3, v4}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 229
    iput-object v5, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mK2HD:Landroid/preference/CheckBoxPreference;

    .line 233
    :cond_4
    iget-object v3, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mPrefScreen:Landroid/preference/PreferenceScreen;

    const-string v4, "slink_register_category"

    invoke-virtual {v3, v4}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v2

    check-cast v2, Landroid/preference/PreferenceCategory;

    .line 234
    .local v2, "slinkCategory":Landroid/preference/PreferenceCategory;
    iget-object v3, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mPrefScreen:Landroid/preference/PreferenceScreen;

    const-string v4, "slink_register"

    invoke-virtual {v3, v4}, Landroid/preference/PreferenceScreen;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v3

    check-cast v3, Landroid/preference/CheckBoxPreference;

    iput-object v3, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mSlinkRegister:Landroid/preference/CheckBoxPreference;

    .line 235
    iget-object v3, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/samsung/musicplus/provider/MusicContents$Audio$Devices;->isSupportSlink(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_8

    iget-object v3, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/samsung/musicplus/provider/MusicContents$Audio$Devices;->hasSamsungAccount(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 236
    iget-object v3, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mSlinkRegister:Landroid/preference/CheckBoxPreference;

    iget-object v4, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/samsung/musicplus/provider/MusicContents$Audio$Devices;->isSingedIn(Landroid/content/Context;)Z

    move-result v4

    invoke-virtual {v3, v4}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 237
    iget-object v3, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mSlinkRegister:Landroid/preference/CheckBoxPreference;

    new-instance v4, Lcom/samsung/musicplus/settings/PlayerSettingFragment$1;

    invoke-direct {v4, p0}, Lcom/samsung/musicplus/settings/PlayerSettingFragment$1;-><init>(Lcom/samsung/musicplus/settings/PlayerSettingFragment;)V

    invoke-virtual {v3, v4}, Landroid/preference/CheckBoxPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 245
    iget-object v3, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mSlinkRegister:Landroid/preference/CheckBoxPreference;

    new-instance v4, Lcom/samsung/musicplus/settings/PlayerSettingFragment$2;

    invoke-direct {v4, p0}, Lcom/samsung/musicplus/settings/PlayerSettingFragment$2;-><init>(Lcom/samsung/musicplus/settings/PlayerSettingFragment;)V

    invoke-virtual {v3, v4}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 256
    :goto_2
    return-void

    .line 200
    .end local v2    # "slinkCategory":Landroid/preference/PreferenceCategory;
    :cond_5
    invoke-virtual {v0, v6}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    goto/16 :goto_0

    .line 219
    .restart local v1    # "manager":Lcom/samsung/musicplus/library/audio/SvoiceManager;
    :cond_6
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->disableVoiceControl()V

    goto :goto_1

    .line 222
    .end local v1    # "manager":Lcom/samsung/musicplus/library/audio/SvoiceManager;
    :cond_7
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->disableVoiceControl()V

    goto :goto_1

    .line 253
    .restart local v2    # "slinkCategory":Landroid/preference/PreferenceCategory;
    :cond_8
    iget-object v3, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mPrefScreen:Landroid/preference/PreferenceScreen;

    invoke-virtual {v3, v2}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    .line 254
    iput-object v5, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mSlinkRegister:Landroid/preference/CheckBoxPreference;

    goto :goto_2
.end method

.method private isLocalOrNoList()Z
    .locals 2

    .prologue
    .line 913
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getCurrentPath()Ljava/lang/String;

    move-result-object v0

    .line 916
    .local v0, "path":Ljava/lang/String;
    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/samsung/musicplus/util/UiUtils;->isLocalContents(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private launchDeregisterDevices(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const v5, 0x7f100053

    .line 342
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 343
    .local v1, "dialogBuilder":Landroid/app/AlertDialog$Builder;
    move-object v2, p1

    .line 344
    .local v2, "mContext":Landroid/content/Context;
    invoke-virtual {v1, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 345
    const v3, 0x7f100054

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 346
    const v3, 0x7f10003a

    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/samsung/musicplus/settings/PlayerSettingFragment$3;

    invoke-direct {v4, p0}, Lcom/samsung/musicplus/settings/PlayerSettingFragment$3;-><init>(Lcom/samsung/musicplus/settings/PlayerSettingFragment;)V

    invoke-virtual {v1, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 352
    invoke-virtual {p1, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/samsung/musicplus/settings/PlayerSettingFragment$4;

    invoke-direct {v4, p0, v2}, Lcom/samsung/musicplus/settings/PlayerSettingFragment$4;-><init>(Lcom/samsung/musicplus/settings/PlayerSettingFragment;Landroid/content/Context;)V

    invoke-virtual {v1, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 369
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 370
    .local v0, "dialog":Landroid/app/AlertDialog;
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 371
    return-void
.end method

.method private savePreference(Landroid/content/SharedPreferences;Ljava/lang/String;I)V
    .locals 4
    .param p1, "sp"    # Landroid/content/SharedPreferences;
    .param p2, "key"    # Ljava/lang/String;
    .param p3, "value"    # I

    .prologue
    .line 416
    const-string v1, "MusicSetting"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "savePreference key : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " value : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 417
    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 418
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, p2, p3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 419
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 420
    return-void
.end method

.method private savePreference(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V
    .locals 4
    .param p1, "sp"    # Landroid/content/SharedPreferences;
    .param p2, "key"    # Ljava/lang/String;
    .param p3, "value"    # Z

    .prologue
    .line 423
    const-string v1, "MusicSetting"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "savePreference key : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " value : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 424
    invoke-interface {p1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 425
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, p2, p3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 426
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 427
    return-void
.end method

.method private setAlarm(I)V
    .locals 8
    .param p1, "autoOffTime"    # I

    .prologue
    .line 686
    iget-object v2, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mContext:Landroid/content/Context;

    const-string v3, "alarm"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 687
    .local v0, "am":Landroid/app/AlarmManager;
    iget-object v2, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mContext:Landroid/content/Context;

    const/4 v3, 0x0

    new-instance v4, Landroid/content/Intent;

    const-string v5, "com.samsung.musicplus.intent.action.MUSIC_AUTO_OFF"

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v5, 0x8000000

    invoke-static {v2, v3, v4, v5}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 689
    .local v1, "pi":Landroid/app/PendingIntent;
    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 690
    if-eqz p1, :cond_0

    .line 691
    const/4 v2, 0x3

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    int-to-long v6, p1

    add-long/2addr v4, v6

    invoke-virtual {v0, v2, v4, v5, v1}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 695
    :cond_0
    iget-object v2, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mSummaryUpdateHandler:Landroid/os/Handler;

    const/4 v3, 0x1

    const-wide/16 v4, 0x64

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 696
    return-void
.end method

.method private setAutoOff(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "newValue"    # Ljava/lang/Object;

    .prologue
    const/4 v5, 0x1

    const/4 v7, 0x0

    .line 667
    iput v7, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mCuAutoOffHour:I

    .line 668
    iput v7, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mCuAutoOffMin:I

    move-object v6, p1

    .line 669
    check-cast v6, Ljava/lang/String;

    .line 671
    .local v6, "sr":Ljava/lang/String;
    const-string v1, "-999"

    invoke-virtual {v1, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 672
    iget-object v1, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mUiPreference:Landroid/content/SharedPreferences;

    const-string v2, "music_auto_off_custom_hour"

    invoke-interface {v1, v2, v7}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mCuAutoOffHour:I

    .line 673
    iget-object v1, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mUiPreference:Landroid/content/SharedPreferences;

    const-string v2, "music_auto_off_custom_min"

    invoke-interface {v1, v2, v7}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mCuAutoOffMin:I

    .line 674
    new-instance v0, Lcom/samsung/musicplus/settings/PlayerSettingFragment$CustomTimePickerDialog;

    iget-object v1, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mTimeSetListener:Landroid/app/TimePickerDialog$OnTimeSetListener;

    iget v3, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mCuAutoOffHour:I

    iget v4, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mCuAutoOffMin:I

    invoke-direct/range {v0 .. v5}, Lcom/samsung/musicplus/settings/PlayerSettingFragment$CustomTimePickerDialog;-><init>(Landroid/content/Context;Landroid/app/TimePickerDialog$OnTimeSetListener;IIZ)V

    .line 676
    .local v0, "dialog":Lcom/samsung/musicplus/settings/PlayerSettingFragment$CustomTimePickerDialog;
    const v1, 0x7f100049

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/settings/PlayerSettingFragment$CustomTimePickerDialog;->setTitle(I)V

    .line 677
    invoke-virtual {v0}, Lcom/samsung/musicplus/settings/PlayerSettingFragment$CustomTimePickerDialog;->show()V

    move v5, v7

    .line 682
    .end local v0    # "dialog":Lcom/samsung/musicplus/settings/PlayerSettingFragment$CustomTimePickerDialog;
    :goto_0
    return v5

    .line 681
    :cond_0
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->setAlarm(I)V

    goto :goto_0
.end method

.method private setK2HD(Landroid/content/SharedPreferences;)V
    .locals 3
    .param p1, "sp"    # Landroid/content/SharedPreferences;

    .prologue
    .line 860
    const-string v1, "K2HD"

    const/4 v2, 0x0

    invoke-interface {p1, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 861
    .local v0, "bK2HD":Z
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->showK2HDDialog()V

    .line 862
    const-string v1, "K2HD"

    invoke-static {v1, v0}, Lcom/samsung/musicplus/util/ServiceUtils;->savePreferencesBoolean(Ljava/lang/String;Z)V

    .line 863
    invoke-static {v0}, Lcom/samsung/musicplus/util/ServiceUtils;->setK2HD(Z)V

    .line 864
    return-void
.end method

.method private setSmartVolume(Landroid/content/SharedPreferences;)V
    .locals 3
    .param p1, "sp"    # Landroid/content/SharedPreferences;

    .prologue
    .line 835
    const-string v1, "smart_volume"

    const/4 v2, 0x0

    invoke-interface {p1, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 836
    .local v0, "smartVolumeOn":Z
    const-string v1, "smart_volume"

    invoke-static {v1, v0}, Lcom/samsung/musicplus/util/ServiceUtils;->savePreferencesBoolean(Ljava/lang/String;Z)V

    .line 838
    if-eqz v0, :cond_0

    .line 839
    iget-object v1, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->getStreamVolume(I)I

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->showSmartVolumeDialog(I)V

    .line 843
    iget-object v1, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mContext:Landroid/content/Context;

    const-string v2, "SMVL"

    invoke-static {v1, v2}, Lcom/samsung/musicplus/util/logging/FeatureLogger;->insertLog(Landroid/content/Context;Ljava/lang/String;)V

    .line 845
    :cond_0
    invoke-static {v0}, Lcom/samsung/musicplus/util/ServiceUtils;->setSmartVolume(Z)V

    .line 846
    return-void
.end method

.method private showAdaptSoundDialog()V
    .locals 4

    .prologue
    .line 938
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->canShowDialog()Z

    move-result v2

    if-nez v2, :cond_1

    .line 947
    :cond_0
    :goto_0
    return-void

    .line 941
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "adapt_sound"

    invoke-virtual {v2, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    .line 942
    .local v1, "fg":Landroid/app/Fragment;
    if-nez v1, :cond_0

    .line 943
    new-instance v0, Lcom/samsung/musicplus/dialog/AdaptSoundDialog;

    invoke-direct {v0}, Lcom/samsung/musicplus/dialog/AdaptSoundDialog;-><init>()V

    .line 944
    .local v0, "dialog":Lcom/samsung/musicplus/dialog/AdaptSoundDialog;
    const/4 v2, 0x2

    invoke-virtual {v0, p0, v2}, Lcom/samsung/musicplus/dialog/AdaptSoundDialog;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 945
    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "adapt_sound"

    invoke-virtual {v0, v2, v3}, Lcom/samsung/musicplus/dialog/AdaptSoundDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private showK2HDDialog()V
    .locals 3

    .prologue
    .line 966
    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f100002

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f100003

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f100114

    new-instance v2, Lcom/samsung/musicplus/settings/PlayerSettingFragment$9;

    invoke-direct {v2, p0}, Lcom/samsung/musicplus/settings/PlayerSettingFragment$9;-><init>(Lcom/samsung/musicplus/settings/PlayerSettingFragment;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 973
    return-void
.end method

.method private showPlaySpeedDialog()V
    .locals 4

    .prologue
    .line 926
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->canShowDialog()Z

    move-result v2

    if-nez v2, :cond_1

    .line 935
    :cond_0
    :goto_0
    return-void

    .line 929
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "play_speed"

    invoke-virtual {v2, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    .line 930
    .local v1, "fg":Landroid/app/Fragment;
    if-nez v1, :cond_0

    .line 931
    new-instance v0, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;

    invoke-direct {v0}, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;-><init>()V

    .line 932
    .local v0, "dialog":Lcom/samsung/musicplus/dialog/PlaySpeedDialog;
    const/4 v2, 0x1

    invoke-virtual {v0, p0, v2}, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 933
    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    const-string v3, "play_speed"

    invoke-virtual {v0, v2, v3}, Lcom/samsung/musicplus/dialog/PlaySpeedDialog;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private showProgressDialog()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 393
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->canShowDialog()Z

    move-result v0

    if-nez v0, :cond_1

    .line 405
    :cond_0
    :goto_0
    return-void

    .line 396
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_2

    .line 397
    iget-object v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mContext:Landroid/content/Context;

    const v2, 0x7f100055

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 398
    iget-object v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 399
    iget-object v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v4}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 400
    iget-object v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    .line 402
    :cond_2
    iget-object v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 403
    iget-object v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method

.method private showSlinkRegisterDialog()V
    .locals 1

    .prologue
    .line 334
    iget-object v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/musicplus/provider/MusicContents$Audio$Devices;->isSingedIn(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 335
    iget-object v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mContext:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->launchDeregisterDevices(Landroid/content/Context;)V

    .line 339
    :goto_0
    return-void

    .line 337
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/musicplus/provider/MusicContents$Audio$Devices;->startLoginActivity(Landroid/content/Context;)V

    goto :goto_0
.end method

.method private showSmartVolumeDialog(I)V
    .locals 5
    .param p1, "volume"    # I

    .prologue
    .line 950
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->canShowDialog()Z

    move-result v1

    if-nez v1, :cond_0

    .line 962
    :goto_0
    return-void

    .line 953
    :cond_0
    const v1, 0x7f100162

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 954
    .local v0, "dialogMessage":Ljava/lang/String;
    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f100163

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f100114

    new-instance v3, Lcom/samsung/musicplus/settings/PlayerSettingFragment$8;

    invoke-direct {v3, p0}, Lcom/samsung/musicplus/settings/PlayerSettingFragment$8;-><init>(Lcom/samsung/musicplus/settings/PlayerSettingFragment;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_0
.end method

.method private updateAdaptSoundState()V
    .locals 3

    .prologue
    .line 818
    iget-object v1, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mAdaptSound:Landroid/preference/Preference;

    if-eqz v1, :cond_1

    .line 819
    iget-object v1, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/musicplus/library/audio/AdaptSound;->getAdaptSoundOn(Landroid/content/Context;)Z

    move-result v0

    .line 820
    .local v0, "isChecked":Z
    if-eqz v0, :cond_2

    .line 821
    iget-object v1, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mAdaptSound:Landroid/preference/Preference;

    const v2, 0x7f100115

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setSummary(I)V

    .line 826
    :goto_0
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->isLocalOrNoList()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->isDmrPlaying()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 827
    :cond_0
    iget-object v1, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mAdaptSound:Landroid/preference/Preference;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 832
    .end local v0    # "isChecked":Z
    :cond_1
    :goto_1
    return-void

    .line 823
    .restart local v0    # "isChecked":Z
    :cond_2
    iget-object v1, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mAdaptSound:Landroid/preference/Preference;

    const v2, 0x7f100113

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setSummary(I)V

    goto :goto_0

    .line 829
    :cond_3
    iget-object v1, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mAdaptSound:Landroid/preference/Preference;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setEnabled(Z)V

    goto :goto_1
.end method

.method private updateAutoOffSummary()V
    .locals 2

    .prologue
    .line 699
    const-string v0, "-999"

    iget-object v1, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mMusicAutoOff:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 700
    iget-object v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mMusicAutoOff:Landroid/preference/ListPreference;

    invoke-direct {p0}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->getCustomSetTime()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 704
    :goto_0
    return-void

    .line 702
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mMusicAutoOff:Landroid/preference/ListPreference;

    iget-object v1, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mMusicAutoOff:Landroid/preference/ListPreference;

    invoke-virtual {v1}, Landroid/preference/ListPreference;->getEntry()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private updateAutoOffstatus()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 622
    iget-object v2, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mContext:Landroid/content/Context;

    const-string v3, "music_service_pref"

    const/4 v4, 0x4

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 624
    .local v1, "pref":Landroid/content/SharedPreferences;
    const-string v2, "completed"

    const-string v3, "music_auto_off"

    const-string v4, "0"

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 625
    iget-object v2, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mMusicAutoOff:Landroid/preference/ListPreference;

    const-string v3, "0"

    invoke-virtual {v2, v3}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 626
    const-string v2, "music_auto_off"

    const-string v3, "-1"

    invoke-static {v2, v3}, Lcom/samsung/musicplus/util/ServiceUtils;->savePreferencesString(Ljava/lang/String;Ljava/lang/String;)V

    .line 629
    :cond_0
    iget-object v2, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mContext:Landroid/content/Context;

    new-instance v3, Landroid/content/Intent;

    const-string v4, "com.samsung.musicplus.intent.action.MUSIC_AUTO_OFF"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v4, 0x20000000

    invoke-static {v2, v5, v3, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 631
    .local v0, "pi":Landroid/app/PendingIntent;
    if-nez v0, :cond_1

    .line 632
    iget-object v2, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mMusicAutoOff:Landroid/preference/ListPreference;

    const-string v3, "0"

    invoke-virtual {v2, v3}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 635
    :cond_1
    iget-object v2, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mMusicAutoOff:Landroid/preference/ListPreference;

    invoke-virtual {v2}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v2

    const-string v3, "-999"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 636
    iget-object v2, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mUiPreference:Landroid/content/SharedPreferences;

    const-string v3, "music_auto_off_custom_hour"

    invoke-interface {v2, v3, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mCuAutoOffHour:I

    .line 637
    iget-object v2, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mUiPreference:Landroid/content/SharedPreferences;

    const-string v3, "music_auto_off_custom_min"

    invoke-interface {v2, v3, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mCuAutoOffMin:I

    .line 639
    :cond_2
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->updateAutoOffSummary()V

    .line 640
    return-void
.end method

.method private updateNewSoundAliveState()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 494
    const-string v0, ""

    .line 496
    .local v0, "effectName":Ljava/lang/String;
    const/4 v4, 0x0

    .line 497
    .local v4, "usePreset":I
    const/16 v3, 0xc

    .line 499
    .local v3, "squarePosition":I
    const/4 v1, 0x0

    .line 501
    .local v1, "isAuto":Z
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->loadNewSoundAliveUsePreset()I

    move-result v4

    .line 502
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->loadNewSoundAliveSquarePosition()[I

    move-result-object v2

    .line 503
    .local v2, "squareEffect":[I
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->loadNewSoundAliveAuto()Z

    move-result v1

    .line 505
    if-eqz v2, :cond_0

    .line 506
    aget v5, v2, v7

    mul-int/lit8 v5, v5, 0x5

    aget v6, v2, v8

    add-int v3, v5, v6

    .line 509
    :cond_0
    if-eqz v1, :cond_2

    .line 510
    const v5, 0x7f100028

    invoke-virtual {p0, v5}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 515
    :goto_0
    if-eqz v4, :cond_1

    .line 516
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " ("

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {v4}, Lcom/samsung/musicplus/util/SoundAliveUtilsV2;->getPresetEffectNameInternal(I)I

    move-result v6

    invoke-virtual {p0, v6}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ")"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 518
    :cond_1
    iget-object v5, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mSoundAlive:Landroid/preference/Preference;

    invoke-virtual {v5, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 520
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->isDmrPlaying()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 521
    iget-object v5, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mSoundAlive:Landroid/preference/Preference;

    invoke-virtual {v5, v7}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 525
    :goto_1
    return-void

    .line 512
    :cond_2
    invoke-static {v3}, Lcom/samsung/musicplus/util/SoundAliveUtilsV2;->getSquareEffectNameInternal(I)I

    move-result v5

    invoke-virtual {p0, v5}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 523
    :cond_3
    iget-object v5, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mSoundAlive:Landroid/preference/Preference;

    invoke-virtual {v5, v8}, Landroid/preference/Preference;->setEnabled(Z)V

    goto :goto_1
.end method

.method private updatePlayListsMenuSummary()V
    .locals 8

    .prologue
    .line 887
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 888
    .local v1, "playlistMenuString":Ljava/lang/StringBuilder;
    iget-object v5, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mUiPreference:Landroid/content/SharedPreferences;

    const-string v6, "playlist_menu_list"

    const-string v7, "-11|-12|-13|-14"

    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 890
    .local v0, "playlistMenu":Ljava/lang/String;
    new-instance v3, Ljava/util/StringTokenizer;

    const-string v5, "|"

    invoke-direct {v3, v0, v5}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 893
    .local v3, "strToken":Ljava/util/StringTokenizer;
    :cond_0
    :goto_0
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->hasMoreElements()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 894
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->nextElement()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 895
    .local v4, "token":Ljava/lang/String;
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 897
    .local v2, "plid":I
    int-to-long v6, v2

    invoke-static {v6, v7}, Lcom/samsung/musicplus/util/ListUtils;->getDefaultPlaylistName(J)I

    move-result v5

    invoke-virtual {p0, v5}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 898
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->hasMoreElements()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 899
    const-string v5, ", "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 902
    .end local v2    # "plid":I
    .end local v4    # "token":Ljava/lang/String;
    :cond_1
    iget-object v5, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mPlayListsMenuSetting:Landroid/preference/Preference;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 903
    return-void
.end method

.method private updatePlaySpeedState()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 734
    iget-object v2, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mContext:Landroid/content/Context;

    const-string v3, "music_service_pref"

    const/4 v4, 0x4

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 736
    .local v0, "pref":Landroid/content/SharedPreferences;
    const-string v2, "%.1f"

    new-array v3, v7, [Ljava/lang/Object;

    const-string v4, "play_speed"

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 737
    .local v1, "savedPlaySpeed":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mPlaySpeed:Landroid/preference/Preference;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "x"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 739
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->isSupportPlaySpeed()Z

    move-result v2

    if-nez v2, :cond_0

    .line 740
    iget-object v2, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mPlaySpeed:Landroid/preference/Preference;

    invoke-virtual {v2, v6}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 744
    :goto_0
    return-void

    .line 742
    :cond_0
    iget-object v2, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mPlaySpeed:Landroid/preference/Preference;

    invoke-virtual {v2, v7}, Landroid/preference/Preference;->setEnabled(Z)V

    goto :goto_0
.end method

.method private updateSAEffectState(II)V
    .locals 3
    .param p1, "genre"    # I
    .param p2, "preset"    # I

    .prologue
    .line 528
    iget-object v1, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mSoundAlive:Landroid/preference/Preference;

    if-nez v1, :cond_0

    .line 537
    :goto_0
    return-void

    .line 531
    :cond_0
    const-string v0, ""

    .line 532
    .local v0, "effectName":Ljava/lang/String;
    invoke-static {p1}, Lcom/samsung/musicplus/util/SoundAliveUtilsV2;->getSquareEffectName(I)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 533
    if-eqz p2, :cond_1

    .line 534
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p2}, Lcom/samsung/musicplus/util/SoundAliveUtilsV2;->getPresetEffectName(I)I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 536
    :cond_1
    iget-object v1, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mSoundAlive:Landroid/preference/Preference;

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private updateSettingEntry()V
    .locals 0

    .prologue
    .line 464
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->updateSoundAlive()V

    .line 465
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->updateTabMenuSummary()V

    .line 466
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->updatePlayListsMenuSummary()V

    .line 467
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->updatePlaySpeedState()V

    .line 468
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->updateAdaptSoundState()V

    .line 469
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->updateSmartVolumeState()V

    .line 470
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->updateSlinkRegisterState()V

    .line 471
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->updateVoiceControlState()V

    .line 472
    return-void
.end method

.method private updateSlinkRegisterState()V
    .locals 2

    .prologue
    .line 475
    iget-object v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mSlinkRegister:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/musicplus/provider/MusicContents$Audio$Devices;->hasSamsungAccount(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 476
    iget-object v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mSlinkRegister:Landroid/preference/CheckBoxPreference;

    iget-object v1, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/samsung/musicplus/provider/MusicContents$Audio$Devices;->isSingedIn(Landroid/content/Context;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 478
    :cond_0
    return-void
.end method

.method private updateSmartVolumeState()V
    .locals 2

    .prologue
    .line 849
    iget-object v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mSmartVolume:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_1

    .line 850
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->isLocalOrNoList()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->isDmrPlaying()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 851
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mSmartVolume:Landroid/preference/CheckBoxPreference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 857
    :cond_1
    :goto_0
    return-void

    .line 853
    :cond_2
    iget-object v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mSmartVolume:Landroid/preference/CheckBoxPreference;

    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->isActiveSmartVolume()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 854
    iget-object v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mSmartVolume:Landroid/preference/CheckBoxPreference;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    goto :goto_0
.end method

.method private updateSoundAlive()V
    .locals 2

    .prologue
    .line 481
    sget-boolean v1, Lcom/samsung/musicplus/util/Features;->FLAG_SUPPORT_EXTERNAL_SOUNDALIVE:Z

    if-eqz v1, :cond_0

    .line 482
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.safx.ACTION_REQUEST_GENRE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 483
    .local v0, "i":Landroid/content/Intent;
    iget-object v1, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 491
    .end local v0    # "i":Landroid/content/Intent;
    :goto_0
    return-void

    .line 485
    :cond_0
    sget-boolean v1, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_SUPPORT_NEW_SOUNDALIVE:Z

    if-eqz v1, :cond_1

    .line 486
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->updateNewSoundAliveState()V

    goto :goto_0

    .line 488
    :cond_1
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->updateSoundAliveState()V

    goto :goto_0
.end method

.method private updateSoundAliveState()V
    .locals 5

    .prologue
    .line 747
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getSoundAlive()I

    move-result v1

    .line 748
    .local v1, "mode":I
    invoke-static {v1}, Lcom/samsung/musicplus/util/SoundAliveUtils;->getEffectName(I)I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 750
    .local v0, "effectName":Ljava/lang/String;
    const/16 v2, 0xd

    if-eq v1, v2, :cond_0

    .line 751
    iget-object v2, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mSoundAlive:Landroid/preference/Preference;

    invoke-virtual {v2, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 756
    :goto_0
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->isDmrPlaying()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 757
    iget-object v2, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mSoundAlive:Landroid/preference/Preference;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 761
    :goto_1
    return-void

    .line 753
    :cond_0
    iget-object v2, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mSoundAlive:Landroid/preference/Preference;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-direct {p0}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->getSAUserEffectSummary()Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 759
    :cond_1
    iget-object v2, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mSoundAlive:Landroid/preference/Preference;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/preference/Preference;->setEnabled(Z)V

    goto :goto_1
.end method

.method private updateTabMenuSummary()V
    .locals 8

    .prologue
    .line 867
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 868
    .local v3, "tabMenuString":Ljava/lang/StringBuilder;
    iget-object v5, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mUiPreference:Landroid/content/SharedPreferences;

    const-string v6, "tab_menu_list"

    invoke-static {}, Lcom/samsung/musicplus/util/ListUtils;->getDefaultTab()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 869
    .local v2, "tabMenu":Ljava/lang/String;
    new-instance v1, Ljava/util/StringTokenizer;

    const-string v5, "|"

    invoke-direct {v1, v2, v5}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 872
    .local v1, "strToken":Ljava/util/StringTokenizer;
    :cond_0
    :goto_0
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->hasMoreElements()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 873
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->nextElement()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 874
    .local v4, "token":Ljava/lang/String;
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 875
    .local v0, "list":I
    const v5, 0x1000a

    if-eq v0, v5, :cond_0

    .line 878
    invoke-static {v0}, Lcom/samsung/musicplus/util/ListUtils;->getDefaultTabName(I)I

    move-result v5

    invoke-virtual {p0, v5}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 879
    invoke-virtual {v1}, Ljava/util/StringTokenizer;->hasMoreElements()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 880
    const-string v5, ", "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 883
    .end local v0    # "list":I
    .end local v4    # "token":Ljava/lang/String;
    :cond_1
    iget-object v5, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mTabMenuSetting:Landroid/preference/Preference;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 884
    return-void
.end method

.method private updateVoiceControlState()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 572
    iget-object v3, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mVoiceControl:Landroid/preference/CheckBoxPreference;

    if-eqz v3, :cond_0

    .line 573
    iget-object v3, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 574
    .local v1, "r":Landroid/content/ContentResolver;
    const-string v3, "voice_input_control"

    invoke-static {v1, v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    .line 575
    .local v2, "voice":I
    if-nez v2, :cond_1

    .line 576
    iget-object v3, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mVoiceControl:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v3, v4}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 587
    .end local v1    # "r":Landroid/content/ContentResolver;
    .end local v2    # "voice":I
    :cond_0
    :goto_0
    return-void

    .line 578
    .restart local v1    # "r":Landroid/content/ContentResolver;
    .restart local v2    # "voice":I
    :cond_1
    const-string v3, "voice_input_control_music"

    invoke-static {v1, v3, v4}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 580
    .local v0, "music":I
    if-nez v0, :cond_2

    .line 581
    iget-object v3, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mVoiceControl:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v3, v4}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto :goto_0

    .line 583
    :cond_2
    iget-object v3, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mVoiceControl:Landroid/preference/CheckBoxPreference;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    goto :goto_0
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 2
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    const/4 v0, -0x1

    .line 604
    packed-switch p1, :pswitch_data_0

    .line 619
    :cond_0
    :goto_0
    return-void

    .line 606
    :pswitch_0
    if-ne p2, v0, :cond_0

    .line 607
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->updatePlaySpeedState()V

    goto :goto_0

    .line 611
    :pswitch_1
    if-ne p2, v0, :cond_1

    .line 612
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->updateAdaptSoundState()V

    goto :goto_0

    .line 613
    :cond_1
    if-nez p2, :cond_0

    .line 614
    iget-object v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mContext:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/audio/AdaptSound;->setAdaptSoundOn(Landroid/content/Context;Z)V

    .line 615
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->updateAdaptSoundState()V

    goto :goto_0

    .line 604
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 133
    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 135
    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mContext:Landroid/content/Context;

    .line 136
    iget-object v3, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->getInstance(Landroid/content/Context;)Lcom/samsung/musicplus/library/audio/SecAudioManager;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

    .line 137
    iget-object v3, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mContext:Landroid/content/Context;

    const-string v4, "music_player_pref"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mUiPreference:Landroid/content/SharedPreferences;

    .line 139
    new-instance v3, Landroid/app/ProgressDialog;

    iget-object v4, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mProgressDialog:Landroid/app/ProgressDialog;

    .line 141
    const v3, 0x7f070001

    invoke-virtual {p0, v3}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->addPreferencesFromResource(I)V

    .line 142
    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mPrefScreen:Landroid/preference/PreferenceScreen;

    .line 144
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->initializeSettingMenu()V

    .line 146
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 147
    .local v1, "autoOff":Landroid/content/IntentFilter;
    const-string v3, "com.samsung.musicplus.intent.action.MUSIC_AUTO_OFF"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 148
    iget-object v3, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mAutoOffReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v3, v4, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 150
    new-instance v0, Landroid/content/IntentFilter;

    const-string v3, "com.samsung.musicplus.action.AUDIO_PATH_CHANGED"

    invoke-direct {v0, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 151
    .local v0, "audioPath":Landroid/content/IntentFilter;
    iget-object v3, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mAudioPathChangedReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v3, v4, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 153
    sget-boolean v3, Lcom/samsung/musicplus/util/Features;->FLAG_SUPPORT_EXTERNAL_SOUNDALIVE:Z

    if-eqz v3, :cond_0

    .line 154
    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "com.sec.android.app.SA_GENRE_INFO"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 155
    .local v2, "saEffectInfo":Landroid/content/IntentFilter;
    iget-object v3, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mSAEffectInfoReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v3, v4, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 157
    .end local v2    # "saEffectInfo":Landroid/content/IntentFilter;
    :cond_0
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 455
    iget-object v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mAutoOffReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 456
    iget-object v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mAudioPathChangedReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 457
    sget-boolean v0, Lcom/samsung/musicplus/util/Features;->FLAG_SUPPORT_EXTERNAL_SOUNDALIVE:Z

    if-eqz v0, :cond_0

    .line 458
    iget-object v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mSAEffectInfoReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 460
    :cond_0
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onDestroy()V

    .line 461
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 436
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mPause:Z

    .line 437
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onPause()V

    .line 438
    iget-object v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mPrefScreen:Landroid/preference/PreferenceScreen;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mPrefScreen:Landroid/preference/PreferenceScreen;

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 439
    iget-object v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mPrefScreen:Landroid/preference/PreferenceScreen;

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 441
    :cond_0
    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 3
    .param p1, "pref"    # Landroid/preference/Preference;
    .param p2, "newValue"    # Ljava/lang/Object;

    .prologue
    .line 304
    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    .line 306
    .local v0, "key":Ljava/lang/String;
    const-string v2, "music_auto_off"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 307
    invoke-direct {p0, p2}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->setAutoOff(Ljava/lang/Object;)Z

    move-result v1

    .line 308
    .local v1, "savePreValue":Z
    if-nez v1, :cond_0

    .line 309
    const/4 v2, 0x0

    .line 312
    .end local v1    # "savePreValue":Z
    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x1

    goto :goto_0
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 8
    .param p1, "preferenceScreen"    # Landroid/preference/PreferenceScreen;
    .param p2, "preference"    # Landroid/preference/Preference;

    .prologue
    const/4 v4, 0x1

    const/4 v7, 0x0

    .line 260
    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    .line 261
    .local v2, "key":Ljava/lang/String;
    new-instance v1, Landroid/content/Intent;

    iget-object v5, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mContext:Landroid/content/Context;

    const-class v6, Lcom/samsung/musicplus/settings/MyMusicMenuReorderActivity;

    invoke-direct {v1, v5, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 263
    .local v1, "i":Landroid/content/Intent;
    const-string v5, "tab_menu_list"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 264
    const-string v5, "TabMenuReorder"

    invoke-virtual {v1, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 265
    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->startActivity(Landroid/content/Intent;)V

    .line 299
    :goto_0
    return v4

    .line 266
    :cond_0
    const-string v5, "playlist_menu_list"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 267
    const-string v5, "TabMenuReorder"

    invoke-virtual {v1, v5, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 268
    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 269
    :cond_1
    const-string v5, "sound_alive"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    .line 270
    sget-boolean v5, Lcom/samsung/musicplus/util/Features;->FLAG_SUPPORT_EXTERNAL_SOUNDALIVE:Z

    if-eqz v5, :cond_2

    .line 271
    new-instance v1, Landroid/content/Intent;

    .end local v1    # "i":Landroid/content/Intent;
    const-string v5, "android.media.action.DISPLAY_AUDIO_EFFECT_CONTROL_PANEL"

    invoke-direct {v1, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 272
    .restart local v1    # "i":Landroid/content/Intent;
    const-string v5, "android.media.extra.AUDIO_SESSION"

    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getAudioSessionId()I

    move-result v6

    invoke-virtual {v1, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 274
    const/4 v5, 0x0

    :try_start_0
    invoke-virtual {p0, v1, v5}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 275
    :catch_0
    move-exception v0

    .line 276
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v5, "MusicSetting"

    const-string v6, "Activity Not found!!!"

    invoke-static {v5, v6}, Lcom/samsung/musicplus/library/iLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    invoke-virtual {v0}, Landroid/content/ActivityNotFoundException;->printStackTrace()V

    .line 278
    iget-object v5, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mContext:Landroid/content/Context;

    const-string v6, "Target Activity Not Found"

    invoke-static {v5, v6, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 281
    .end local v0    # "e":Landroid/content/ActivityNotFoundException;
    :cond_2
    sget-boolean v5, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_SUPPORT_NEW_SOUNDALIVE:Z

    if-eqz v5, :cond_3

    .line 282
    new-instance v5, Landroid/content/Intent;

    iget-object v6, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mContext:Landroid/content/Context;

    const-class v7, Lcom/samsung/musicplus/settings/SoundAliveV2Setting;

    invoke-direct {v5, v6, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v5}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 284
    :cond_3
    new-instance v5, Landroid/content/Intent;

    iget-object v6, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mContext:Landroid/content/Context;

    const-class v7, Lcom/samsung/musicplus/settings/SoundAliveSetting;

    invoke-direct {v5, v6, v7}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v5}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 287
    :cond_4
    const-string v5, "adapt_sound"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 288
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->showAdaptSoundDialog()V

    goto :goto_0

    .line 289
    :cond_5
    const-string v5, "play_speed"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    .line 290
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->showPlaySpeedDialog()V

    goto :goto_0

    .line 291
    :cond_6
    const-string v5, "voice_control"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 292
    iget-object v5, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "voice_input_control"

    invoke-static {v5, v6, v7}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    .line 294
    .local v3, "voice":I
    const-string v5, "MusicSetting"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Voice control is : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    invoke-direct {p0, v3}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->handleVoicePref(I)V

    goto/16 :goto_0

    .line 297
    .end local v3    # "voice":I
    :cond_7
    invoke-super {p0, p1, p2}, Landroid/preference/PreferenceFragment;->onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z

    move-result v4

    goto/16 :goto_0
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 445
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mPause:Z

    .line 446
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onResume()V

    .line 447
    iget-object v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mPrefScreen:Landroid/preference/PreferenceScreen;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mPrefScreen:Landroid/preference/PreferenceScreen;

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 448
    iget-object v0, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mPrefScreen:Landroid/preference/PreferenceScreen;

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->getSharedPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 450
    :cond_0
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->updateSettingEntry()V

    .line 451
    return-void
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 3
    .param p1, "sp"    # Landroid/content/SharedPreferences;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 317
    const-string v1, "smart_volume"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 318
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->setSmartVolume(Landroid/content/SharedPreferences;)V

    .line 331
    :cond_0
    :goto_0
    return-void

    .line 319
    :cond_1
    const-string v1, "lyric"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 320
    const-string v1, "lyric"

    const/4 v2, 0x1

    invoke-interface {p1, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 321
    .local v0, "lyric":Z
    iget-object v1, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mUiPreference:Landroid/content/SharedPreferences;

    const-string v2, "lyric"

    invoke-direct {p0, v1, v2, v0}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->savePreference(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V

    .line 322
    if-eqz v0, :cond_0

    .line 326
    iget-object v1, p0, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->mContext:Landroid/content/Context;

    const-string v2, "LYRC"

    invoke-static {v1, v2}, Lcom/samsung/musicplus/util/logging/FeatureLogger;->insertLog(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 328
    .end local v0    # "lyric":Z
    :cond_2
    const-string v1, "K2HD"

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 329
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/settings/PlayerSettingFragment;->setK2HD(Landroid/content/SharedPreferences;)V

    goto :goto_0
.end method
