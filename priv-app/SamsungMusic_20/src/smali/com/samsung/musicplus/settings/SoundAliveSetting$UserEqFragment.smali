.class public Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqFragment;
.super Landroid/app/Fragment;
.source "SoundAliveSetting.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/settings/SoundAliveSetting;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "UserEqFragment"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqFragment$UserEqView;,
        Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqFragment$EqSeekBarChangeListener;
    }
.end annotation


# static fields
.field private static final BASE_POSTION:I = 0xa

.field private static final EQ_SEEKBAR_RES_ID:[I

.field private static final EQ_STRING_RES_ID:[I


# instance fields
.field private final mEqSeekBar:[Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x7

    .line 416
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqFragment;->EQ_SEEKBAR_RES_ID:[I

    .line 421
    new-array v0, v1, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqFragment;->EQ_STRING_RES_ID:[I

    return-void

    .line 416
    :array_0
    .array-data 4
        0x7f0d0153
        0x7f0d0154
        0x7f0d0155
        0x7f0d0156
        0x7f0d0157
        0x7f0d0158
        0x7f0d0159
    .end array-data

    .line 421
    :array_1
    .array-data 4
        0x7f100075
        0x7f100076
        0x7f100077
        0x7f100078
        0x7f100079
        0x7f10007a
        0x7f10007b
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 414
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 429
    const/4 v0, 0x7

    new-array v0, v0, [Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;

    iput-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqFragment;->mEqSeekBar:[Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;

    .line 515
    return-void
.end method

.method static synthetic access$1200(Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqFragment;)[Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqFragment;

    .prologue
    .line 414
    iget-object v0, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqFragment;->mEqSeekBar:[Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;

    return-object v0
.end method

.method private createEqView(Landroid/view/View;)V
    .locals 13
    .param p1, "root"    # Landroid/view/View;

    .prologue
    const/4 v12, 0x7

    .line 444
    const v9, 0x7f0d013e

    invoke-virtual {p1, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;

    .line 445
    .local v0, "bubble":Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;
    const v9, 0x7f0200b9

    invoke-virtual {v0, v9}, Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;->setBubbleBackgroundDrawable(I)V

    .line 446
    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0c0023

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v9

    invoke-virtual {v0, v9}, Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;->setBubbleFontSize(F)V

    .line 447
    const/4 v9, 0x4

    invoke-virtual {v0, v9}, Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;->setBubbleAlign(I)V

    .line 449
    new-array v2, v12, [Landroid/view/View;

    .line 450
    .local v2, "eq":[Landroid/view/View;
    const/4 v4, 0x0

    .line 451
    .local v4, "eqText":Landroid/widget/TextView;
    invoke-direct {p0}, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqFragment;->loadSoundAliveUserEQ()[I

    move-result-object v5

    .line 452
    .local v5, "eqValue":[I
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    if-ge v6, v12, :cond_0

    .line 454
    sget-object v9, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqFragment;->EQ_SEEKBAR_RES_ID:[I

    aget v9, v9, v6

    invoke-virtual {p1, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    aput-object v9, v2, v6

    .line 455
    aget-object v9, v2, v6

    const v10, 0x7f0d01be

    invoke-virtual {v9, v10}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .end local v4    # "eqText":Landroid/widget/TextView;
    check-cast v4, Landroid/widget/TextView;

    .line 456
    .restart local v4    # "eqText":Landroid/widget/TextView;
    sget-object v9, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqFragment;->EQ_STRING_RES_ID:[I

    aget v9, v9, v6

    invoke-virtual {v4, v9}, Landroid/widget/TextView;->setText(I)V

    .line 459
    iget-object v10, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqFragment;->mEqSeekBar:[Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;

    aget-object v9, v2, v6

    const v11, 0x7f0d01bd

    invoke-virtual {v9, v11}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;

    aput-object v9, v10, v6

    .line 460
    iget-object v9, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqFragment;->mEqSeekBar:[Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;

    aget-object v9, v9, v6

    aget v10, v5, v6

    add-int/lit8 v10, v10, 0xa

    invoke-virtual {v9, v10}, Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;->setProgress(I)V

    .line 461
    iget-object v9, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqFragment;->mEqSeekBar:[Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;

    aget-object v9, v9, v6

    const/16 v10, 0xa

    invoke-virtual {v9, v10}, Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;->setBaseValue(I)V

    .line 462
    iget-object v9, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqFragment;->mEqSeekBar:[Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;

    aget-object v9, v9, v6

    sget-object v10, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqFragment;->EQ_STRING_RES_ID:[I

    aget v10, v10, v6

    invoke-virtual {v9, v10}, Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;->setTitle(I)V

    .line 463
    iget-object v9, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqFragment;->mEqSeekBar:[Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;

    aget-object v9, v9, v6

    new-instance v10, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqFragment$EqSeekBarChangeListener;

    invoke-direct {v10, p0, v0}, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqFragment$EqSeekBarChangeListener;-><init>(Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqFragment;Lcom/samsung/musicplus/widget/progress/TwSeekBarBubble;)V

    invoke-virtual {v9, v10}, Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    .line 452
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 466
    :cond_0
    iget-object v9, p0, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqFragment;->mEqSeekBar:[Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;

    const/4 v10, 0x0

    aget-object v9, v9, v10

    invoke-virtual {v9}, Lcom/samsung/musicplus/widget/progress/UserEQSeekBar;->getPaddingTop()I

    move-result v8

    .line 467
    .local v8, "topPadding":I
    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0c002b

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    .line 470
    .local v7, "seekBarHeight":I
    const v9, 0x7f0d0152

    invoke-virtual {p1, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    .line 471
    .local v1, "dbScale":Landroid/widget/LinearLayout;
    new-instance v3, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqFragment$UserEqView;

    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqFragment;->getActivity()Landroid/app/Activity;

    move-result-object v9

    invoke-direct {v3, p0, v9, v7, v8}, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqFragment$UserEqView;-><init>(Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqFragment;Landroid/content/Context;II)V

    .line 472
    .local v3, "eqDbScaleView":Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqFragment$UserEqView;
    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    .line 473
    return-void
.end method

.method private loadSoundAliveUserEQ()[I
    .locals 10

    .prologue
    const/4 v9, 0x7

    .line 476
    new-array v0, v9, [I

    .line 477
    .local v0, "eq":[I
    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqFragment;->getActivity()Landroid/app/Activity;

    move-result-object v6

    const-string v7, "music_service_pref"

    const/4 v8, 0x4

    invoke-virtual {v6, v7, v8}, Landroid/app/Activity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 479
    .local v2, "preference":Landroid/content/SharedPreferences;
    const-string v6, "user_eq"

    const-string v7, "0|0|0|0|0|0|0|"

    invoke-interface {v2, v6, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 481
    .local v3, "savedEq":Ljava/lang/String;
    new-instance v4, Ljava/util/StringTokenizer;

    const-string v6, "|"

    invoke-direct {v4, v3, v6}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 483
    .local v4, "strToken":Ljava/util/StringTokenizer;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v9, :cond_1

    .line 484
    invoke-virtual {v4}, Ljava/util/StringTokenizer;->hasMoreElements()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 485
    invoke-virtual {v4}, Ljava/util/StringTokenizer;->nextElement()Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    .line 486
    .local v5, "token":Ljava/lang/String;
    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    aput v6, v0, v1

    .line 483
    .end local v5    # "token":Ljava/lang/String;
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 488
    :cond_0
    const/4 v6, 0x0

    aput v6, v0, v1

    goto :goto_1

    .line 491
    :cond_1
    return-object v0
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 439
    invoke-virtual {p0}, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/settings/SoundAliveSetting$UserEqFragment;->createEqView(Landroid/view/View;)V

    .line 440
    invoke-super {p0, p1}, Landroid/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 441
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 434
    const v0, 0x7f040073

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
