.class Lcom/samsung/musicplus/settings/MyMusicMenuReorderActivity$1;
.super Ljava/lang/Object;
.source "MyMusicMenuReorderActivity.java"

# interfaces
.implements Landroid/widget/AdapterView$OnTwMultiSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/settings/MyMusicMenuReorderActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mDragSelectedPositions:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/samsung/musicplus/settings/MyMusicMenuReorderActivity;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/settings/MyMusicMenuReorderActivity;)V
    .locals 1

    .prologue
    .line 80
    iput-object p1, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderActivity$1;->this$0:Lcom/samsung/musicplus/settings/MyMusicMenuReorderActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderActivity$1;->mDragSelectedPositions:Ljava/util/ArrayList;

    return-void
.end method

.method private getSelectionList()[I
    .locals 4

    .prologue
    .line 86
    iget-object v3, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderActivity$1;->mDragSelectedPositions:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 87
    .local v0, "count":I
    new-array v2, v0, [I

    .line 89
    .local v2, "selectionList":[I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 90
    iget-object v3, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderActivity$1;->mDragSelectedPositions:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    aput v3, v2, v1

    .line 89
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 93
    :cond_0
    return-object v2
.end method


# virtual methods
.method public OnTwMultiSelectStart(II)V
    .locals 1
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 111
    iget-object v0, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderActivity$1;->mDragSelectedPositions:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 112
    return-void
.end method

.method public OnTwMultiSelectStop(II)V
    .locals 2
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 116
    iget-object v0, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderActivity$1;->mDragSelectedPositions:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 117
    iget-object v0, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderActivity$1;->this$0:Lcom/samsung/musicplus/settings/MyMusicMenuReorderActivity;

    # getter for: Lcom/samsung/musicplus/settings/MyMusicMenuReorderActivity;->mListView:Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;
    invoke-static {v0}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderActivity;->access$000(Lcom/samsung/musicplus/settings/MyMusicMenuReorderActivity;)Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;

    move-result-object v0

    invoke-direct {p0}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderActivity$1;->getSelectionList()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->toggleCheckState([I)V

    .line 118
    iget-object v0, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderActivity$1;->this$0:Lcom/samsung/musicplus/settings/MyMusicMenuReorderActivity;

    # getter for: Lcom/samsung/musicplus/settings/MyMusicMenuReorderActivity;->mListView:Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;
    invoke-static {v0}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderActivity;->access$000(Lcom/samsung/musicplus/settings/MyMusicMenuReorderActivity;)Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/musicplus/settings/MyMusicMenuReorderListView;->invalidateViews()V

    .line 120
    :cond_0
    return-void
.end method

.method public onTwMultiSelected(Landroid/widget/AdapterView;Landroid/view/View;IJZZZ)V
    .locals 2
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .param p6, "isShiftpress"    # Z
    .param p7, "isCtrlpress"    # Z
    .param p8, "isPentpress"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJZZZ)V"
        }
    .end annotation

    .prologue
    .line 99
    .local p1, "adapterView":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    if-ltz p3, :cond_0

    .line 100
    new-instance v0, Ljava/lang/Integer;

    invoke-direct {v0, p3}, Ljava/lang/Integer;-><init>(I)V

    .line 101
    .local v0, "selectedPosition":Ljava/lang/Integer;
    iget-object v1, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderActivity$1;->mDragSelectedPositions:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 102
    iget-object v1, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderActivity$1;->mDragSelectedPositions:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 107
    .end local v0    # "selectedPosition":Ljava/lang/Integer;
    :cond_0
    :goto_0
    return-void

    .line 104
    .restart local v0    # "selectedPosition":Ljava/lang/Integer;
    :cond_1
    iget-object v1, p0, Lcom/samsung/musicplus/settings/MyMusicMenuReorderActivity$1;->mDragSelectedPositions:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
