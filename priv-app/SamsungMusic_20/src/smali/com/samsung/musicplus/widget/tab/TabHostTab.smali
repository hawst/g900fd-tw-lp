.class public Lcom/samsung/musicplus/widget/tab/TabHostTab;
.super Lcom/samsung/musicplus/widget/tab/AbsTabController;
.source "TabHostTab.java"

# interfaces
.implements Landroid/widget/TabHost$OnTabChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/widget/tab/TabHostTab$DummyTabFactory;
    }
.end annotation


# static fields
.field private static final TAB_CONTENT_ID:I = 0x1020011


# instance fields
.field private final SEC_ROBOTO_LIGHT:Landroid/graphics/Typeface;

.field private final SEC_ROBOTO_REGULAR:Landroid/graphics/Typeface;

.field private mFragment:Landroid/app/Fragment;

.field private mRootView:Landroid/view/View;

.field private mTabHost:Landroid/widget/TabHost;


# direct methods
.method public constructor <init>(Landroid/app/Fragment;)V
    .locals 3
    .param p1, "fragment"    # Landroid/app/Fragment;

    .prologue
    const/4 v1, 0x0

    .line 70
    invoke-virtual {p1}, Landroid/app/Fragment;->getChildFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/widget/tab/AbsTabController;-><init>(Landroid/app/FragmentManager;)V

    .line 219
    const-string v0, "sec-roboto-light"

    invoke-static {v0, v1}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/widget/tab/TabHostTab;->SEC_ROBOTO_LIGHT:Landroid/graphics/Typeface;

    .line 221
    const-string v0, "sec-roboto-regular"

    invoke-static {v0, v1}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/widget/tab/TabHostTab;->SEC_ROBOTO_REGULAR:Landroid/graphics/Typeface;

    .line 71
    iput-object p1, p0, Lcom/samsung/musicplus/widget/tab/TabHostTab;->mFragment:Landroid/app/Fragment;

    .line 72
    iget-object v0, p0, Lcom/samsung/musicplus/widget/tab/TabHostTab;->mFragment:Landroid/app/Fragment;

    invoke-virtual {v0}, Landroid/app/Fragment;->getView()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/widget/tab/TabHostTab;->mRootView:Landroid/view/View;

    .line 73
    iget-object v0, p0, Lcom/samsung/musicplus/widget/tab/TabHostTab;->mRootView:Landroid/view/View;

    const v1, 0x1020012

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TabHost;

    iput-object v0, p0, Lcom/samsung/musicplus/widget/tab/TabHostTab;->mTabHost:Landroid/widget/TabHost;

    .line 74
    iget-object v0, p0, Lcom/samsung/musicplus/widget/tab/TabHostTab;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->setup()V

    .line 75
    iget-object v0, p0, Lcom/samsung/musicplus/widget/tab/TabHostTab;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v0, p0}, Landroid/widget/TabHost;->setOnTabChangedListener(Landroid/widget/TabHost$OnTabChangeListener;)V

    .line 78
    new-instance v0, Lcom/samsung/musicplus/widget/tab/TabHostTabTalkBack;

    iget-object v1, p0, Lcom/samsung/musicplus/widget/tab/TabHostTab;->mFragment:Landroid/app/Fragment;

    invoke-virtual {v1}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/widget/tab/TabHostTab;->mTabHost:Landroid/widget/TabHost;

    invoke-direct {v0, v1, v2}, Lcom/samsung/musicplus/widget/tab/TabHostTabTalkBack;-><init>(Landroid/content/Context;Landroid/widget/TabHost;)V

    iput-object v0, p0, Lcom/samsung/musicplus/widget/tab/TabHostTab;->mTalkBack:Lcom/samsung/musicplus/widget/tab/AbsTabTalkBack;

    .line 79
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/widget/tab/TabHostTab;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/tab/TabHostTab;

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/tab/TabHostTab;->scrollSelector()V

    return-void
.end method

.method private resetCurrnetTab(I)Z
    .locals 6
    .param p1, "value"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 268
    :try_start_0
    iget-object v4, p0, Lcom/samsung/musicplus/widget/tab/TabHostTab;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    const-string v5, "mCurrentTab"

    invoke-virtual {v4, v5}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    .line 269
    .local v0, "currentTabField":Ljava/lang/reflect/Field;
    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 270
    iget-object v4, p0, Lcom/samsung/musicplus/widget/tab/TabHostTab;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v0, v4, p1}, Ljava/lang/reflect/Field;->setInt(Ljava/lang/Object;I)V
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2

    .line 281
    .end local v0    # "currentTabField":Ljava/lang/reflect/Field;
    :goto_0
    return v2

    .line 271
    :catch_0
    move-exception v1

    .line 272
    .local v1, "e":Ljava/lang/NoSuchFieldException;
    const-string v2, "MusicTab"

    const-string v4, "resetCurrnetTab() - NoSuchFieldException"

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v3

    .line 273
    goto :goto_0

    .line 274
    .end local v1    # "e":Ljava/lang/NoSuchFieldException;
    :catch_1
    move-exception v1

    .line 275
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    const-string v2, "MusicTab"

    const-string v4, "resetCurrnetTab() - IllegalArgumentException"

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v3

    .line 276
    goto :goto_0

    .line 277
    .end local v1    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v1

    .line 278
    .local v1, "e":Ljava/lang/IllegalAccessException;
    const-string v2, "MusicTab"

    const-string v4, "resetCurrnetTab() - IllegalAccessException"

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v3

    .line 279
    goto :goto_0
.end method

.method private resetTabText()V
    .locals 5

    .prologue
    const v4, 0x7f0d01bc

    .line 225
    iget-object v3, p0, Lcom/samsung/musicplus/widget/tab/TabHostTab;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v3}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v2

    .line 226
    .local v2, "tw":Landroid/widget/TabWidget;
    const/4 v1, 0x0

    .line 227
    .local v1, "tv":Landroid/widget/TextView;
    if-eqz v2, :cond_1

    .line 228
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {v2}, Landroid/widget/TabWidget;->getChildCount()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 229
    invoke-virtual {v2, v0}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .end local v1    # "tv":Landroid/widget/TextView;
    check-cast v1, Landroid/widget/TextView;

    .line 230
    .restart local v1    # "tv":Landroid/widget/TextView;
    if-eqz v1, :cond_0

    .line 231
    iget-object v3, p0, Lcom/samsung/musicplus/widget/tab/TabHostTab;->SEC_ROBOTO_LIGHT:Landroid/graphics/Typeface;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 228
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 236
    .end local v0    # "i":I
    :cond_1
    iget-object v3, p0, Lcom/samsung/musicplus/widget/tab/TabHostTab;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v3}, Landroid/widget/TabHost;->getCurrentTabView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .end local v1    # "tv":Landroid/widget/TextView;
    check-cast v1, Landroid/widget/TextView;

    .line 237
    .restart local v1    # "tv":Landroid/widget/TextView;
    if-eqz v1, :cond_2

    .line 238
    iget-object v3, p0, Lcom/samsung/musicplus/widget/tab/TabHostTab;->SEC_ROBOTO_REGULAR:Landroid/graphics/Typeface;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 240
    :cond_2
    return-void
.end method

.method private scrollSelector()V
    .locals 6

    .prologue
    .line 246
    iget-object v4, p0, Lcom/samsung/musicplus/widget/tab/TabHostTab;->mRootView:Landroid/view/View;

    const v5, 0x7f0d0103

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/HorizontalScrollView;

    .line 248
    .local v0, "scrollView":Landroid/widget/HorizontalScrollView;
    if-eqz v0, :cond_0

    iget-object v4, p0, Lcom/samsung/musicplus/widget/tab/TabHostTab;->mTabHost:Landroid/widget/TabHost;

    if-nez v4, :cond_1

    .line 257
    :cond_0
    :goto_0
    return-void

    .line 251
    :cond_1
    invoke-virtual {v0}, Landroid/widget/HorizontalScrollView;->getWidth()I

    move-result v2

    .line 252
    .local v2, "w":I
    iget-object v4, p0, Lcom/samsung/musicplus/widget/tab/TabHostTab;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v4}, Landroid/widget/TabHost;->getCurrentTabView()Landroid/view/View;

    move-result-object v1

    .line 253
    .local v1, "v":Landroid/view/View;
    if-eqz v1, :cond_0

    .line 254
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v4

    invoke-virtual {v1}, Landroid/view/View;->getWidth()I

    move-result v5

    sub-int v5, v2, v5

    div-int/lit8 v5, v5, 0x2

    sub-int v3, v4, v5

    .line 255
    .local v3, "x":I
    invoke-virtual {v0}, Landroid/widget/HorizontalScrollView;->getScrollY()I

    move-result v4

    invoke-virtual {v0, v3, v4}, Landroid/widget/HorizontalScrollView;->smoothScrollTo(II)V

    goto :goto_0
.end method


# virtual methods
.method protected addTabInternal(I)V
    .locals 5
    .param p1, "tabId"    # I

    .prologue
    .line 90
    iget-object v2, p0, Lcom/samsung/musicplus/widget/tab/TabHostTab;->mFragment:Landroid/app/Fragment;

    invoke-virtual {v2}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f040090

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 92
    .local v0, "tab":Landroid/view/View;
    const v2, 0x7f0d01bc

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-static {p1}, Lcom/samsung/musicplus/util/ListUtils;->getDefaultTabName(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 93
    iget-object v2, p0, Lcom/samsung/musicplus/widget/tab/TabHostTab;->mTabHost:Landroid/widget/TabHost;

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/widget/TabHost$TabSpec;->setIndicator(Landroid/view/View;)Landroid/widget/TabHost$TabSpec;

    move-result-object v1

    .line 94
    .local v1, "tabSpec":Landroid/widget/TabHost$TabSpec;
    new-instance v2, Lcom/samsung/musicplus/widget/tab/TabHostTab$DummyTabFactory;

    iget-object v3, p0, Lcom/samsung/musicplus/widget/tab/TabHostTab;->mFragment:Landroid/app/Fragment;

    invoke-virtual {v3}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/musicplus/widget/tab/TabHostTab$DummyTabFactory;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v2}, Landroid/widget/TabHost$TabSpec;->setContent(Landroid/widget/TabHost$TabContentFactory;)Landroid/widget/TabHost$TabSpec;

    .line 95
    iget-object v2, p0, Lcom/samsung/musicplus/widget/tab/TabHostTab;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v2, v1}, Landroid/widget/TabHost;->addTab(Landroid/widget/TabHost$TabSpec;)V

    .line 96
    return-void
.end method

.method public getSelectedTabPosition()I
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/samsung/musicplus/widget/tab/TabHostTab;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getCurrentTab()I

    move-result v0

    return v0
.end method

.method protected initTab()V
    .locals 1

    .prologue
    .line 85
    const v0, 0x7fffffff

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/widget/tab/TabHostTab;->resetCurrnetTab(I)Z

    .line 86
    return-void
.end method

.method public onMultiWindowStateChanged(Z)V
    .locals 1
    .param p1, "isMultiWindow"    # Z

    .prologue
    .line 286
    iget-object v0, p0, Lcom/samsung/musicplus/widget/tab/TabHostTab;->mCurrentContent:Ljava/lang/Object;

    instance-of v0, v0, Lcom/samsung/musicplus/widget/fragment/OnMusicFragmentInterface;

    if-eqz v0, :cond_0

    .line 287
    iget-object v0, p0, Lcom/samsung/musicplus/widget/tab/TabHostTab;->mCurrentContent:Ljava/lang/Object;

    check-cast v0, Lcom/samsung/musicplus/widget/fragment/OnMusicFragmentInterface;

    invoke-interface {v0, p1}, Lcom/samsung/musicplus/widget/fragment/OnMusicFragmentInterface;->onMultiWindowModeChanged(Z)V

    .line 289
    :cond_0
    return-void
.end method

.method public onTabChanged(Ljava/lang/String;)V
    .locals 7
    .param p1, "tabId"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 159
    iget-boolean v2, p0, Lcom/samsung/musicplus/widget/tab/TabHostTab;->mOnTabChangeEnabled:Z

    if-nez v2, :cond_0

    .line 217
    :goto_0
    return-void

    .line 164
    :cond_0
    iget-object v2, p0, Lcom/samsung/musicplus/widget/tab/TabHostTab;->mTalkBack:Lcom/samsung/musicplus/widget/tab/AbsTabTalkBack;

    iget-object v3, p0, Lcom/samsung/musicplus/widget/tab/TabHostTab;->mCurrentTabId:Ljava/lang/Integer;

    invoke-virtual {p0, v3}, Lcom/samsung/musicplus/widget/tab/TabHostTab;->findPosition(Ljava/lang/Integer;)I

    move-result v3

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/tab/TabHostTab;->getTabCount()I

    move-result v4

    invoke-virtual {v2, v3, v4, v5}, Lcom/samsung/musicplus/widget/tab/AbsTabTalkBack;->setTabViewTts(IIZ)V

    .line 166
    iget-object v2, p0, Lcom/samsung/musicplus/widget/tab/TabHostTab;->mPager:Landroid/support/v4/view/ViewPager;

    if-eqz v2, :cond_2

    .line 167
    iget-object v2, p0, Lcom/samsung/musicplus/widget/tab/TabHostTab;->mCurrentContent:Ljava/lang/Object;

    instance-of v2, v2, Lcom/samsung/musicplus/widget/tab/OnTabListener;

    if-eqz v2, :cond_1

    .line 168
    iget-object v2, p0, Lcom/samsung/musicplus/widget/tab/TabHostTab;->mCurrentContent:Ljava/lang/Object;

    check-cast v2, Lcom/samsung/musicplus/widget/tab/OnTabListener;

    invoke-interface {v2}, Lcom/samsung/musicplus/widget/tab/OnTabListener;->onTabUnselected()V

    .line 170
    :cond_1
    iget-object v2, p0, Lcom/samsung/musicplus/widget/tab/TabHostTab;->mPager:Landroid/support/v4/view/ViewPager;

    iget-object v3, p0, Lcom/samsung/musicplus/widget/tab/TabHostTab;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v3}, Landroid/widget/TabHost;->getCurrentTab()I

    move-result v3

    invoke-virtual {v2, v3, v5}, Landroid/support/v4/view/ViewPager;->setCurrentItem(IZ)V

    .line 205
    :goto_1
    iget-object v2, p0, Lcom/samsung/musicplus/widget/tab/TabHostTab;->mTabHost:Landroid/widget/TabHost;

    new-instance v3, Lcom/samsung/musicplus/widget/tab/TabHostTab$1;

    invoke-direct {v3, p0}, Lcom/samsung/musicplus/widget/tab/TabHostTab$1;-><init>(Lcom/samsung/musicplus/widget/tab/TabHostTab;)V

    invoke-virtual {v2, v3}, Landroid/widget/TabHost;->post(Ljava/lang/Runnable;)Z

    .line 214
    iget-object v2, p0, Lcom/samsung/musicplus/widget/tab/TabHostTab;->mTalkBack:Lcom/samsung/musicplus/widget/tab/AbsTabTalkBack;

    iget-object v3, p0, Lcom/samsung/musicplus/widget/tab/TabHostTab;->mCurrentTabId:Ljava/lang/Integer;

    invoke-virtual {p0, v3}, Lcom/samsung/musicplus/widget/tab/TabHostTab;->findPosition(Ljava/lang/Integer;)I

    move-result v3

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/tab/TabHostTab;->getTabCount()I

    move-result v4

    invoke-virtual {v2, v3, v4, v6}, Lcom/samsung/musicplus/widget/tab/AbsTabTalkBack;->setTabViewTts(IIZ)V

    .line 216
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/tab/TabHostTab;->resetTabText()V

    goto :goto_0

    .line 172
    :cond_2
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/musicplus/widget/tab/TabHostTab;->mCurrentTabId:Ljava/lang/Integer;

    .line 173
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/tab/TabHostTab;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 176
    .local v1, "ft":Landroid/app/FragmentTransaction;
    iget-object v2, p0, Lcom/samsung/musicplus/widget/tab/TabHostTab;->mCurrentContent:Ljava/lang/Object;

    if-eqz v2, :cond_3

    .line 177
    iget-object v2, p0, Lcom/samsung/musicplus/widget/tab/TabHostTab;->mCurrentContent:Ljava/lang/Object;

    check-cast v2, Landroid/app/Fragment;

    invoke-virtual {v1, v2}, Landroid/app/FragmentTransaction;->detach(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 181
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/tab/TabHostTab;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 182
    .local v0, "fg":Landroid/app/Fragment;
    if-nez v0, :cond_6

    .line 183
    iget-object v2, p0, Lcom/samsung/musicplus/widget/tab/TabHostTab;->mCurrentTabId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/samsung/musicplus/contents/ContentsFragmentFactory;->getNewInstance(ILjava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 184
    const v2, 0x1020011

    invoke-virtual {v1, v2, v0, p1}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 192
    :goto_2
    invoke-virtual {v0, v6}, Landroid/app/Fragment;->setHasOptionsMenu(Z)V

    .line 194
    iget-object v2, p0, Lcom/samsung/musicplus/widget/tab/TabHostTab;->mCurrentContent:Ljava/lang/Object;

    instance-of v2, v2, Lcom/samsung/musicplus/widget/tab/OnTabListener;

    if-eqz v2, :cond_4

    .line 195
    iget-object v2, p0, Lcom/samsung/musicplus/widget/tab/TabHostTab;->mCurrentContent:Ljava/lang/Object;

    check-cast v2, Lcom/samsung/musicplus/widget/tab/OnTabListener;

    invoke-interface {v2}, Lcom/samsung/musicplus/widget/tab/OnTabListener;->onTabUnselected()V

    .line 197
    :cond_4
    instance-of v2, v0, Lcom/samsung/musicplus/widget/tab/OnTabListener;

    if-eqz v2, :cond_5

    move-object v2, v0

    .line 198
    check-cast v2, Lcom/samsung/musicplus/widget/tab/OnTabListener;

    invoke-interface {v2}, Lcom/samsung/musicplus/widget/tab/OnTabListener;->onTabSelected()V

    .line 201
    :cond_5
    iput-object v0, p0, Lcom/samsung/musicplus/widget/tab/TabHostTab;->mCurrentContent:Ljava/lang/Object;

    .line 202
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto :goto_1

    .line 189
    :cond_6
    invoke-virtual {v1, v0}, Landroid/app/FragmentTransaction;->attach(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    goto :goto_2
.end method

.method public removeAllTabs()V
    .locals 1

    .prologue
    .line 106
    invoke-super {p0}, Lcom/samsung/musicplus/widget/tab/AbsTabController;->removeAllTabs()V

    .line 107
    iget-object v0, p0, Lcom/samsung/musicplus/widget/tab/TabHostTab;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->clearAllTabs()V

    .line 108
    return-void
.end method

.method public removeTab(I)V
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 100
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/tab/AbsTabController;->removeTab(I)V

    .line 101
    iget-object v0, p0, Lcom/samsung/musicplus/widget/tab/TabHostTab;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/musicplus/widget/tab/TabHostTab;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v1}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/widget/TabWidget;->getChildTabViewAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->removeView(Landroid/view/View;)V

    .line 102
    return-void
.end method

.method protected setTabSelectedInternal(I)V
    .locals 5
    .param p1, "position"    # I

    .prologue
    const v4, 0x7fffffff

    const/4 v3, -0x1

    .line 112
    iget-object v2, p0, Lcom/samsung/musicplus/widget/tab/TabHostTab;->mPager:Landroid/support/v4/view/ViewPager;

    if-eqz v2, :cond_1

    .line 118
    iget-object v2, p0, Lcom/samsung/musicplus/widget/tab/TabHostTab;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v2}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v1

    .line 119
    .local v1, "widget":Landroid/widget/TabWidget;
    invoke-virtual {v1}, Landroid/widget/TabWidget;->getDescendantFocusability()I

    move-result v0

    .line 120
    .local v0, "oldFocusability":I
    const/high16 v2, 0x60000

    invoke-virtual {v1, v2}, Landroid/widget/TabWidget;->setDescendantFocusability(I)V

    .line 124
    iget-object v2, p0, Lcom/samsung/musicplus/widget/tab/TabHostTab;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v2}, Landroid/widget/TabHost;->getCurrentTab()I

    move-result v2

    if-ne v2, v4, :cond_0

    .line 125
    invoke-direct {p0, v3}, Lcom/samsung/musicplus/widget/tab/TabHostTab;->resetCurrnetTab(I)Z

    .line 127
    :cond_0
    iget-object v2, p0, Lcom/samsung/musicplus/widget/tab/TabHostTab;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v2, p1}, Landroid/widget/TabHost;->setCurrentTab(I)V

    .line 128
    invoke-virtual {v1, v0}, Landroid/widget/TabWidget;->setDescendantFocusability(I)V

    .line 137
    .end local v0    # "oldFocusability":I
    .end local v1    # "widget":Landroid/widget/TabWidget;
    :goto_0
    return-void

    .line 132
    :cond_1
    iget-object v2, p0, Lcom/samsung/musicplus/widget/tab/TabHostTab;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v2}, Landroid/widget/TabHost;->getCurrentTab()I

    move-result v2

    if-ne v2, v4, :cond_2

    .line 133
    invoke-direct {p0, v3}, Lcom/samsung/musicplus/widget/tab/TabHostTab;->resetCurrnetTab(I)Z

    .line 135
    :cond_2
    iget-object v2, p0, Lcom/samsung/musicplus/widget/tab/TabHostTab;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v2, p1}, Landroid/widget/TabHost;->setCurrentTab(I)V

    goto :goto_0
.end method

.method public setVisibleTab(Z)V
    .locals 3
    .param p1, "visible"    # Z

    .prologue
    .line 146
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/tab/AbsTabController;->setVisibleTab(Z)V

    .line 147
    iget-object v1, p0, Lcom/samsung/musicplus/widget/tab/TabHostTab;->mRootView:Landroid/view/View;

    const v2, 0x7f0d0103

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/HorizontalScrollView;

    .line 149
    .local v0, "scrollView":Landroid/widget/HorizontalScrollView;
    if-eqz v0, :cond_0

    .line 150
    if-eqz p1, :cond_1

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/HorizontalScrollView;->setVisibility(I)V

    .line 152
    :cond_0
    return-void

    .line 150
    :cond_1
    const/16 v1, 0x8

    goto :goto_0
.end method
