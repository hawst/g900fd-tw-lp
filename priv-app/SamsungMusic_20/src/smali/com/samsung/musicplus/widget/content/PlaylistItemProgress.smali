.class public Lcom/samsung/musicplus/widget/content/PlaylistItemProgress;
.super Ljava/lang/Object;
.source "PlaylistItemProgress.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/widget/content/PlaylistItemProgress$PlaylistSongInfo;
    }
.end annotation


# static fields
.field private static sPlaylistItemProgress:Lcom/samsung/musicplus/widget/content/PlaylistItemProgress;

.field protected static sPlaylistSongInfo:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/samsung/musicplus/widget/content/PlaylistItemProgress$PlaylistSongInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/musicplus/widget/content/PlaylistItemProgress;->sPlaylistSongInfo:Ljava/util/HashMap;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    sget-object v0, Lcom/samsung/musicplus/widget/content/PlaylistItemProgress;->sPlaylistSongInfo:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 43
    return-void
.end method

.method public static getPlaylistItemProgress()Lcom/samsung/musicplus/widget/content/PlaylistItemProgress;
    .locals 2

    .prologue
    .line 31
    sget-object v0, Lcom/samsung/musicplus/widget/content/PlaylistItemProgress;->sPlaylistItemProgress:Lcom/samsung/musicplus/widget/content/PlaylistItemProgress;

    if-nez v0, :cond_1

    .line 32
    const-class v1, Lcom/samsung/musicplus/widget/content/PlaylistItemProgress;

    monitor-enter v1

    .line 33
    :try_start_0
    sget-object v0, Lcom/samsung/musicplus/widget/content/PlaylistItemProgress;->sPlaylistItemProgress:Lcom/samsung/musicplus/widget/content/PlaylistItemProgress;

    if-nez v0, :cond_0

    .line 34
    new-instance v0, Lcom/samsung/musicplus/widget/content/PlaylistItemProgress;

    invoke-direct {v0}, Lcom/samsung/musicplus/widget/content/PlaylistItemProgress;-><init>()V

    sput-object v0, Lcom/samsung/musicplus/widget/content/PlaylistItemProgress;->sPlaylistItemProgress:Lcom/samsung/musicplus/widget/content/PlaylistItemProgress;

    .line 36
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 38
    :cond_1
    sget-object v0, Lcom/samsung/musicplus/widget/content/PlaylistItemProgress;->sPlaylistItemProgress:Lcom/samsung/musicplus/widget/content/PlaylistItemProgress;

    return-object v0

    .line 36
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public updateListView(Landroid/content/Context;Landroid/widget/ImageView;Landroid/widget/TextView;JI)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "iv"    # Landroid/widget/ImageView;
    .param p3, "text1"    # Landroid/widget/TextView;
    .param p4, "plid"    # J
    .param p6, "viewType"    # I

    .prologue
    .line 55
    new-instance v0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;

    sget-object v7, Lcom/samsung/musicplus/widget/content/PlaylistItemProgress;->sPlaylistSongInfo:Ljava/util/HashMap;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-wide v4, p4

    move v6, p6

    invoke-direct/range {v0 .. v7}, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;-><init>(Landroid/content/Context;Landroid/widget/ImageView;Landroid/widget/TextView;JILjava/util/HashMap;)V

    .line 57
    .local v0, "task":Lcom/samsung/musicplus/widget/content/PlaylistItemTask;
    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p3, v1}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 58
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 59
    return-void
.end method

.method public updateListView(Landroid/content/Context;Landroid/widget/ImageView;Landroid/widget/TextView;Landroid/widget/TextView;JI)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "iv"    # Landroid/widget/ImageView;
    .param p3, "text1"    # Landroid/widget/TextView;
    .param p4, "text2"    # Landroid/widget/TextView;
    .param p5, "plid"    # J
    .param p7, "viewType"    # I

    .prologue
    .line 47
    new-instance v1, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;

    sget-object v9, Lcom/samsung/musicplus/widget/content/PlaylistItemProgress;->sPlaylistSongInfo:Ljava/util/HashMap;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-wide/from16 v6, p5

    move/from16 v8, p7

    invoke-direct/range {v1 .. v9}, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;-><init>(Landroid/content/Context;Landroid/widget/ImageView;Landroid/widget/TextView;Landroid/widget/TextView;JILjava/util/HashMap;)V

    .line 49
    .local v1, "task":Lcom/samsung/musicplus/widget/content/PlaylistItemTask;
    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p3, v0}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 50
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/Object;

    invoke-virtual {v1, v0}, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 51
    return-void
.end method

.method public updateListView(Landroid/content/Context;Landroid/widget/ImageView;Landroid/widget/TextView;ZJ)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "iv"    # Landroid/widget/ImageView;
    .param p3, "text1"    # Landroid/widget/TextView;
    .param p4, "showTotal"    # Z
    .param p5, "plid"    # J

    .prologue
    .line 62
    new-instance v0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;

    sget-object v7, Lcom/samsung/musicplus/widget/content/PlaylistItemProgress;->sPlaylistSongInfo:Ljava/util/HashMap;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-wide v4, p5

    move v6, p4

    invoke-direct/range {v0 .. v7}, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;-><init>(Landroid/content/Context;Landroid/widget/ImageView;Landroid/widget/TextView;JZLjava/util/HashMap;)V

    .line 63
    .local v0, "task":Lcom/samsung/musicplus/widget/content/PlaylistItemTask;
    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p3, v1}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 64
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 65
    return-void
.end method
