.class abstract Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView$GridBuilder;
.super Ljava/lang/Object;
.source "TwoWayGridView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x402
    name = "GridBuilder"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;


# direct methods
.method private constructor <init>(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;)V
    .locals 0

    .prologue
    .line 826
    iput-object p1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView$GridBuilder;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;
    .param p2, "x1"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView$1;

    .prologue
    .line 826
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView$GridBuilder;-><init>(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;)V

    return-void
.end method


# virtual methods
.method protected abstract arrowScroll(I)Z
.end method

.method protected abstract fillGap(Z)V
.end method

.method protected abstract isCandidateSelection(II)Z
.end method

.method protected abstract layoutChildren()V
.end method

.method protected makeAndAddView(IIZIZI)Landroid/view/View;
    .locals 10
    .param p1, "position"    # I
    .param p2, "y"    # I
    .param p3, "flow"    # Z
    .param p4, "children"    # I
    .param p5, "selected"    # Z
    .param p6, "where"    # I

    .prologue
    .line 862
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView$GridBuilder;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;

    iget-boolean v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mDataChanged:Z

    if-nez v0, :cond_0

    .line 864
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView$GridBuilder;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mRecycler:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$RecycleBin;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$RecycleBin;->getActiveView(I)Landroid/view/View;

    move-result-object v1

    .line 865
    .local v1, "child":Landroid/view/View;
    if-eqz v1, :cond_0

    .line 868
    const/4 v7, 0x1

    move-object v0, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    move/from16 v8, p6

    invoke-virtual/range {v0 .. v8}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView$GridBuilder;->setupChild(Landroid/view/View;IIZIZZI)V

    move-object v9, v1

    .line 881
    .end local v1    # "child":Landroid/view/View;
    .local v9, "child":Landroid/view/View;
    :goto_0
    return-object v9

    .line 876
    .end local v9    # "child":Landroid/view/View;
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView$GridBuilder;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;

    iget-object v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView$GridBuilder;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;

    iget-object v2, v2, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mIsScrap:[Z

    invoke-virtual {v0, p1, v2}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->obtainView(I[Z)Landroid/view/View;

    move-result-object v1

    .line 879
    .restart local v1    # "child":Landroid/view/View;
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView$GridBuilder;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mIsScrap:[Z

    const/4 v2, 0x0

    aget-boolean v7, v0, v2

    move-object v0, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    move/from16 v8, p6

    invoke-virtual/range {v0 .. v8}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView$GridBuilder;->setupChild(Landroid/view/View;IIZIZZI)V

    move-object v9, v1

    .line 881
    .end local v1    # "child":Landroid/view/View;
    .restart local v9    # "child":Landroid/view/View;
    goto :goto_0
.end method

.method protected abstract onMeasure(II)V
.end method

.method protected abstract setSelectionInt(I)V
.end method

.method protected abstract setupChild(Landroid/view/View;IIZIZZI)V
.end method
