.class Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;
.super Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;
.source "TwoWayAbsListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "HorizontalTouchHandler"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler$HorizontalPositionScroller;,
        Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler$HorizontalFlingRunnable;
    }
.end annotation


# instance fields
.field mLastX:I

.field mMotionViewNewLeft:I

.field mMotionViewOriginalLeft:I

.field final synthetic this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;)V
    .locals 0

    .prologue
    .line 4688
    iput-object p1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    invoke-direct {p0, p1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;-><init>(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;)V

    .line 5399
    return-void
.end method


# virtual methods
.method protected getFlingRunnable()Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$FlingRunnable;
    .locals 2

    .prologue
    .line 4708
    new-instance v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler$HorizontalFlingRunnable;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler$HorizontalFlingRunnable;-><init>(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$1;)V

    return-object v0
.end method

.method protected getPositionScroller()Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$PositionScroller;
    .locals 1

    .prologue
    .line 4714
    new-instance v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler$HorizontalPositionScroller;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler$HorizontalPositionScroller;-><init>(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;)V

    return-object v0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 11
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v10, 0x4

    const/4 v6, 0x1

    const/4 v8, -0x1

    const/4 v7, 0x0

    .line 4720
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 4731
    .local v0, "action":I
    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    move v6, v7

    .line 4778
    :goto_1
    return v6

    .line 4733
    :pswitch_0
    iget-object v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    iget v2, v8, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mTouchMode:I

    .line 4735
    .local v2, "touchMode":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v8

    float-to-int v4, v8

    .line 4736
    .local v4, "x":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v8

    float-to-int v5, v8

    .line 4738
    .local v5, "y":I
    iget-object v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    invoke-virtual {v8, v4}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->findMotionRowX(I)I

    move-result v1

    .line 4739
    .local v1, "motionPosition":I
    if-eq v2, v10, :cond_1

    if-ltz v1, :cond_1

    .line 4742
    iget-object v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    iget-object v9, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    iget v9, v9, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mFirstPosition:I

    sub-int v9, v1, v9

    invoke-virtual {v8, v9}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 4743
    .local v3, "v":Landroid/view/View;
    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v8

    iput v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->mMotionViewOriginalLeft:I

    .line 4744
    iget-object v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    iput v4, v8, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mMotionX:I

    .line 4745
    iget-object v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    iput v5, v8, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mMotionY:I

    .line 4746
    iget-object v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    iput v1, v8, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mMotionPosition:I

    .line 4747
    iget-object v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    iput v7, v8, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mTouchMode:I

    .line 4748
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->clearScrollingCache()V

    .line 4750
    .end local v3    # "v":Landroid/view/View;
    :cond_1
    const/high16 v8, -0x80000000

    iput v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->mLastX:I

    .line 4751
    if-ne v2, v10, :cond_0

    goto :goto_1

    .line 4758
    .end local v1    # "motionPosition":I
    .end local v2    # "touchMode":I
    .end local v4    # "x":I
    .end local v5    # "y":I
    :pswitch_1
    iget-object v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    iget v8, v8, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mTouchMode:I

    packed-switch v8, :pswitch_data_1

    goto :goto_0

    .line 4760
    :pswitch_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v8

    float-to-int v4, v8

    .line 4761
    .restart local v4    # "x":I
    iget-object v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    iget v8, v8, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mMotionX:I

    sub-int v8, v4, v8

    invoke-virtual {p0, v8}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->startScrollIfNeeded(I)Z

    move-result v8

    if-eqz v8, :cond_0

    goto :goto_1

    .line 4770
    .end local v4    # "x":I
    :pswitch_3
    iget-object v6, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    iput v8, v6, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mTouchMode:I

    .line 4771
    iget-object v6, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    # setter for: Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mActivePointerId:I
    invoke-static {v6, v8}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->access$3302(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;I)I

    .line 4772
    invoke-virtual {p0, v7}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->reportScrollStateChange(I)V

    goto :goto_0

    .line 4731
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_3
        :pswitch_1
    .end packed-switch

    .line 4758
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
    .end packed-switch
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 24
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 4784
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->isEnabled()Z

    move-result v20

    if-nez v20, :cond_2

    .line 4787
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->isClickable()Z

    move-result v20

    if-nez v20, :cond_0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->isLongClickable()Z

    move-result v20

    if-eqz v20, :cond_1

    :cond_0
    const/16 v20, 0x1

    .line 5053
    :goto_0
    return v20

    .line 4787
    :cond_1
    const/16 v20, 0x0

    goto :goto_0

    .line 4798
    :cond_2
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    .line 4803
    .local v4, "action":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mVelocityTracker:Landroid/view/VelocityTracker;
    invoke-static/range {v20 .. v20}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->access$2700(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;)Landroid/view/VelocityTracker;

    move-result-object v20

    if-nez v20, :cond_3

    .line 4804
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v21

    # setter for: Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mVelocityTracker:Landroid/view/VelocityTracker;
    invoke-static/range {v20 .. v21}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->access$2702(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;Landroid/view/VelocityTracker;)Landroid/view/VelocityTracker;

    .line 4806
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mVelocityTracker:Landroid/view/VelocityTracker;
    invoke-static/range {v20 .. v20}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->access$2700(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;)Landroid/view/VelocityTracker;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 4808
    packed-switch v4, :pswitch_data_0

    .line 5053
    :cond_4
    :goto_1
    const/16 v20, 0x1

    goto :goto_0

    .line 4810
    :pswitch_0
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v20

    move/from16 v0, v20

    float-to-int v0, v0

    move/from16 v18, v0

    .line 4811
    .local v18, "x":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getY()F

    move-result v20

    move/from16 v0, v20

    float-to-int v0, v0

    move/from16 v19, v0

    .line 4812
    .local v19, "y":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->pointToPosition(II)I

    move-result v13

    .line 4813
    .local v13, "motionPosition":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-boolean v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mDataChanged:Z

    move/from16 v20, v0

    if-nez v20, :cond_6

    .line 4814
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mTouchMode:I

    move/from16 v20, v0

    const/16 v21, 0x4

    move/from16 v0, v20

    move/from16 v1, v21

    if-eq v0, v1, :cond_8

    if-ltz v13, :cond_8

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v20

    check-cast v20, Landroid/widget/ListAdapter;

    move-object/from16 v0, v20

    invoke-interface {v0, v13}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v20

    if-eqz v20, :cond_8

    .line 4818
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    move/from16 v0, v21

    move-object/from16 v1, v20

    iput v0, v1, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mTouchMode:I

    .line 4820
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mPendingCheckForTap:Ljava/lang/Runnable;
    invoke-static/range {v20 .. v20}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->access$2800(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;)Ljava/lang/Runnable;

    move-result-object v20

    if-nez v20, :cond_5

    .line 4821
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    new-instance v21, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$CheckForTap;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v22, v0

    invoke-direct/range {v21 .. v22}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$CheckForTap;-><init>(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;)V

    # setter for: Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mPendingCheckForTap:Ljava/lang/Runnable;
    invoke-static/range {v20 .. v21}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->access$2802(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 4823
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v21, v0

    # getter for: Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mPendingCheckForTap:Ljava/lang/Runnable;
    invoke-static/range {v21 .. v21}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->access$2800(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;)Ljava/lang/Runnable;

    move-result-object v21

    invoke-static {}, Landroid/view/ViewConfiguration;->getTapTimeout()I

    move-result v22

    move/from16 v0, v22

    int-to-long v0, v0

    move-wide/from16 v22, v0

    invoke-virtual/range {v20 .. v23}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 4843
    :cond_6
    :goto_2
    if-ltz v13, :cond_7

    .line 4845
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mFirstPosition:I

    move/from16 v21, v0

    sub-int v21, v13, v21

    invoke-virtual/range {v20 .. v21}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v16

    .line 4846
    .local v16, "v":Landroid/view/View;
    invoke-virtual/range {v16 .. v16}, Landroid/view/View;->getLeft()I

    move-result v20

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->mMotionViewOriginalLeft:I

    .line 4848
    .end local v16    # "v":Landroid/view/View;
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    move/from16 v0, v18

    move-object/from16 v1, v20

    iput v0, v1, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mMotionX:I

    .line 4849
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    move/from16 v0, v19

    move-object/from16 v1, v20

    iput v0, v1, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mMotionY:I

    .line 4850
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iput v13, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mMotionPosition:I

    .line 4851
    const/high16 v20, -0x80000000

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->mLastX:I

    goto/16 :goto_1

    .line 4825
    :cond_8
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getEdgeFlags()I

    move-result v20

    if-eqz v20, :cond_9

    if-gez v13, :cond_9

    .line 4829
    const/16 v20, 0x0

    goto/16 :goto_0

    .line 4832
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mTouchMode:I

    move/from16 v20, v0

    const/16 v21, 0x4

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_6

    .line 4834
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->createScrollingCache()V

    .line 4835
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    const/16 v21, 0x3

    move/from16 v0, v21

    move-object/from16 v1, v20

    iput v0, v1, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mTouchMode:I

    .line 4836
    const/16 v20, 0x0

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->mMotionCorrection:I

    .line 4837
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->findMotionRowX(I)I

    move-result v13

    .line 4838
    const/16 v20, 0x1

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->reportScrollStateChange(I)V

    goto/16 :goto_2

    .line 4856
    .end local v13    # "motionPosition":I
    .end local v18    # "x":I
    .end local v19    # "y":I
    :pswitch_1
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getX()F

    move-result v20

    move/from16 v0, v20

    float-to-int v0, v0

    move/from16 v18, v0

    .line 4857
    .restart local v18    # "x":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mMotionX:I

    move/from16 v20, v0

    sub-int v9, v18, v20

    .line 4858
    .local v9, "deltaX":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mTouchMode:I

    move/from16 v20, v0

    packed-switch v20, :pswitch_data_1

    goto/16 :goto_1

    .line 4864
    :pswitch_2
    move-object/from16 v0, p0

    invoke-virtual {v0, v9}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->startScrollIfNeeded(I)Z

    goto/16 :goto_1

    .line 4874
    :pswitch_3
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->mLastX:I

    move/from16 v20, v0

    move/from16 v0, v18

    move/from16 v1, v20

    if-eq v0, v1, :cond_4

    .line 4875
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->mMotionCorrection:I

    move/from16 v20, v0

    sub-int v9, v9, v20

    .line 4876
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->mLastX:I

    move/from16 v20, v0

    const/high16 v21, -0x80000000

    move/from16 v0, v20

    move/from16 v1, v21

    if-eq v0, v1, :cond_d

    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->mLastX:I

    move/from16 v20, v0

    sub-int v11, v18, v20

    .line 4879
    .local v11, "incrementalDeltaX":I
    :goto_3
    const/4 v5, 0x0

    .line 4880
    .local v5, "atEdge":Z
    if-eqz v11, :cond_a

    .line 4881
    move-object/from16 v0, p0

    invoke-virtual {v0, v9, v11}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->trackMotionScroll(II)Z

    move-result v5

    .line 4885
    :cond_a
    if-eqz v5, :cond_c

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildCount()I

    move-result v20

    if-lez v20, :cond_c

    .line 4890
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->findMotionRowX(I)I

    move-result v13

    .line 4891
    .restart local v13    # "motionPosition":I
    if-ltz v13, :cond_b

    .line 4892
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mFirstPosition:I

    move/from16 v21, v0

    sub-int v21, v13, v21

    invoke-virtual/range {v20 .. v21}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v14

    .line 4893
    .local v14, "motionView":Landroid/view/View;
    invoke-virtual {v14}, Landroid/view/View;->getLeft()I

    move-result v20

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->mMotionViewOriginalLeft:I

    .line 4895
    .end local v14    # "motionView":Landroid/view/View;
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    move/from16 v0, v18

    move-object/from16 v1, v20

    iput v0, v1, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mMotionX:I

    .line 4896
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iput v13, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mMotionPosition:I

    .line 4897
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->invalidate()V

    .line 4899
    .end local v13    # "motionPosition":I
    :cond_c
    move/from16 v0, v18

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->mLastX:I

    goto/16 :goto_1

    .end local v5    # "atEdge":Z
    .end local v11    # "incrementalDeltaX":I
    :cond_d
    move v11, v9

    .line 4876
    goto :goto_3

    .line 4908
    .end local v9    # "deltaX":I
    .end local v18    # "x":I
    :pswitch_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mTouchMode:I

    move/from16 v20, v0

    packed-switch v20, :pswitch_data_2

    .line 5003
    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-virtual/range {v20 .. v21}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->setPressed(Z)V

    .line 5006
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->invalidate()V

    .line 5008
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getHandler()Landroid/os/Handler;

    move-result-object v10

    .line 5009
    .local v10, "handler":Landroid/os/Handler;
    if-eqz v10, :cond_e

    .line 5010
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mPendingCheckForLongPress:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$CheckForLongPress;
    invoke-static/range {v20 .. v20}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->access$600(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;)Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$CheckForLongPress;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v10, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 5013
    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mVelocityTracker:Landroid/view/VelocityTracker;
    invoke-static/range {v20 .. v20}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->access$2700(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;)Landroid/view/VelocityTracker;

    move-result-object v20

    if-eqz v20, :cond_f

    .line 5014
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mVelocityTracker:Landroid/view/VelocityTracker;
    invoke-static/range {v20 .. v20}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->access$2700(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;)Landroid/view/VelocityTracker;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Landroid/view/VelocityTracker;->recycle()V

    .line 5015
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    # setter for: Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mVelocityTracker:Landroid/view/VelocityTracker;
    invoke-static/range {v20 .. v21}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->access$2702(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;Landroid/view/VelocityTracker;)Landroid/view/VelocityTracker;

    .line 5018
    :cond_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    const/16 v21, -0x1

    # setter for: Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mActivePointerId:I
    invoke-static/range {v20 .. v21}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->access$3302(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;I)I

    goto/16 :goto_1

    .line 4912
    .end local v10    # "handler":Landroid/os/Handler;
    :pswitch_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v13, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mMotionPosition:I

    .line 4913
    .restart local v13    # "motionPosition":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mFirstPosition:I

    move/from16 v21, v0

    sub-int v21, v13, v21

    invoke-virtual/range {v20 .. v21}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 4914
    .local v6, "child":Landroid/view/View;
    if-eqz v6, :cond_18

    invoke-virtual {v6}, Landroid/view/View;->hasFocusable()Z

    move-result v20

    if-nez v20, :cond_18

    .line 4915
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mTouchMode:I

    move/from16 v20, v0

    if-eqz v20, :cond_10

    .line 4916
    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-virtual {v6, v0}, Landroid/view/View;->setPressed(Z)V

    .line 4919
    :cond_10
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mPerformClick:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$PerformClick;
    invoke-static/range {v20 .. v20}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->access$2900(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;)Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$PerformClick;

    move-result-object v20

    if-nez v20, :cond_11

    .line 4920
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    new-instance v21, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$PerformClick;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    invoke-direct/range {v21 .. v23}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$PerformClick;-><init>(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$1;)V

    # setter for: Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mPerformClick:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$PerformClick;
    invoke-static/range {v20 .. v21}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->access$2902(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$PerformClick;)Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$PerformClick;

    .line 4923
    :cond_11
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mPerformClick:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$PerformClick;
    invoke-static/range {v20 .. v20}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->access$2900(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;)Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$PerformClick;

    move-result-object v15

    .line 4924
    .local v15, "performClick":Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$PerformClick;
    iput-object v6, v15, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$PerformClick;->mChild:Landroid/view/View;

    .line 4925
    iput v13, v15, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$PerformClick;->mClickMotionPosition:I

    .line 4926
    invoke-virtual {v15}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$PerformClick;->rememberWindowAttachCount()V

    .line 4928
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iput v13, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mResurrectToPosition:I

    .line 4930
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mTouchMode:I

    move/from16 v20, v0

    if-eqz v20, :cond_12

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mTouchMode:I

    move/from16 v20, v0

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_17

    .line 4931
    :cond_12
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getHandler()Landroid/os/Handler;

    move-result-object v10

    .line 4932
    .restart local v10    # "handler":Landroid/os/Handler;
    if-eqz v10, :cond_13

    .line 4933
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mTouchMode:I

    move/from16 v20, v0

    if-nez v20, :cond_15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mPendingCheckForTap:Ljava/lang/Runnable;
    invoke-static/range {v20 .. v20}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->access$2800(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;)Ljava/lang/Runnable;

    move-result-object v20

    :goto_5
    move-object/from16 v0, v20

    invoke-virtual {v10, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 4936
    :cond_13
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    move/from16 v0, v21

    move-object/from16 v1, v20

    iput v0, v1, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mLayoutMode:I

    .line 4937
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-boolean v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mDataChanged:Z

    move/from16 v20, v0

    if-nez v20, :cond_16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mAdapter:Landroid/widget/ListAdapter;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-interface {v0, v13}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v20

    if-eqz v20, :cond_16

    .line 4938
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    const/16 v21, 0x1

    move/from16 v0, v21

    move-object/from16 v1, v20

    iput v0, v1, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mTouchMode:I

    .line 4939
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mMotionPosition:I

    move/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->setSelectedPositionInt(I)V

    .line 4940
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->layoutChildren()V

    .line 4941
    const/16 v20, 0x1

    move/from16 v0, v20

    invoke-virtual {v6, v0}, Landroid/view/View;->setPressed(Z)V

    .line 4942
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v6}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->positionSelector(Landroid/view/View;)V

    .line 4943
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    const/16 v21, 0x1

    invoke-virtual/range {v20 .. v21}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->setPressed(Z)V

    .line 4944
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSelector:Landroid/graphics/drawable/Drawable;

    move-object/from16 v20, v0

    if-eqz v20, :cond_14

    .line 4945
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSelector:Landroid/graphics/drawable/Drawable;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/graphics/drawable/Drawable;->getCurrent()Landroid/graphics/drawable/Drawable;

    move-result-object v8

    .line 4946
    .local v8, "d":Landroid/graphics/drawable/Drawable;
    if-eqz v8, :cond_14

    instance-of v0, v8, Landroid/graphics/drawable/TransitionDrawable;

    move/from16 v20, v0

    if-eqz v20, :cond_14

    .line 4947
    check-cast v8, Landroid/graphics/drawable/TransitionDrawable;

    .end local v8    # "d":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v8}, Landroid/graphics/drawable/TransitionDrawable;->resetTransition()V

    .line 4950
    :cond_14
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    new-instance v21, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler$1;

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v6, v15}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler$1;-><init>(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;Landroid/view/View;Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$PerformClick;)V

    invoke-static {}, Landroid/view/ViewConfiguration;->getPressedStateDuration()I

    move-result v22

    move/from16 v0, v22

    int-to-long v0, v0

    move-wide/from16 v22, v0

    invoke-virtual/range {v20 .. v23}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 4963
    :goto_6
    const/16 v20, 0x1

    goto/16 :goto_0

    .line 4933
    :cond_15
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mPendingCheckForLongPress:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$CheckForLongPress;
    invoke-static/range {v20 .. v20}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->access$600(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;)Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$CheckForLongPress;

    move-result-object v20

    goto/16 :goto_5

    .line 4961
    :cond_16
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    const/16 v21, -0x1

    move/from16 v0, v21

    move-object/from16 v1, v20

    iput v0, v1, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mTouchMode:I

    goto :goto_6

    .line 4964
    .end local v10    # "handler":Landroid/os/Handler;
    :cond_17
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-boolean v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mDataChanged:Z

    move/from16 v20, v0

    if-nez v20, :cond_18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mAdapter:Landroid/widget/ListAdapter;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-interface {v0, v13}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v20

    if-eqz v20, :cond_18

    .line 4965
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v15}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->post(Ljava/lang/Runnable;)Z

    .line 4968
    .end local v15    # "performClick":Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$PerformClick;
    :cond_18
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    const/16 v21, -0x1

    move/from16 v0, v21

    move-object/from16 v1, v20

    iput v0, v1, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mTouchMode:I

    goto/16 :goto_4

    .line 4971
    .end local v6    # "child":Landroid/view/View;
    .end local v13    # "motionPosition":I
    :pswitch_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildCount()I

    move-result v7

    .line 4972
    .local v7, "childCount":I
    if-lez v7, :cond_1c

    .line 4973
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mFirstPosition:I

    move/from16 v20, v0

    if-nez v20, :cond_19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-virtual/range {v20 .. v21}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Landroid/view/View;->getLeft()I

    move-result v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mListPadding:Landroid/graphics/Rect;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    if-lt v0, v1, :cond_19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mFirstPosition:I

    move/from16 v20, v0

    add-int v20, v20, v7

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mItemCount:I

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    if-ge v0, v1, :cond_19

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    add-int/lit8 v21, v7, -0x1

    invoke-virtual/range {v20 .. v21}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Landroid/view/View;->getRight()I

    move-result v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getWidth()I

    move-result v21

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mListPadding:Landroid/graphics/Rect;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v22, v0

    sub-int v21, v21, v22

    move/from16 v0, v20

    move/from16 v1, v21

    if-gt v0, v1, :cond_19

    .line 4977
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    const/16 v21, -0x1

    move/from16 v0, v21

    move-object/from16 v1, v20

    iput v0, v1, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mTouchMode:I

    .line 4978
    const/16 v20, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->reportScrollStateChange(I)V

    goto/16 :goto_4

    .line 4980
    :cond_19
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mVelocityTracker:Landroid/view/VelocityTracker;
    invoke-static/range {v20 .. v20}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->access$2700(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;)Landroid/view/VelocityTracker;

    move-result-object v17

    .line 4981
    .local v17, "velocityTracker":Landroid/view/VelocityTracker;
    const/16 v20, 0x3e8

    move-object/from16 v0, v17

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/view/VelocityTracker;->computeCurrentVelocity(I)V

    .line 4982
    invoke-virtual/range {v17 .. v17}, Landroid/view/VelocityTracker;->getXVelocity()F

    move-result v20

    move/from16 v0, v20

    float-to-int v12, v0

    .line 4984
    .local v12, "initialVelocity":I
    invoke-static {v12}, Ljava/lang/Math;->abs(I)I

    move-result v20

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v21, v0

    # getter for: Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mMinimumVelocity:I
    invoke-static/range {v21 .. v21}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->access$3100(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;)I

    move-result v21

    move/from16 v0, v20

    move/from16 v1, v21

    if-le v0, v1, :cond_1b

    .line 4985
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->mFlingRunnable:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$FlingRunnable;

    move-object/from16 v20, v0

    if-nez v20, :cond_1a

    .line 4986
    new-instance v20, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler$HorizontalFlingRunnable;

    const/16 v21, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    move-object/from16 v2, v21

    invoke-direct {v0, v1, v2}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler$HorizontalFlingRunnable;-><init>(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$1;)V

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->mFlingRunnable:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$FlingRunnable;

    .line 4988
    :cond_1a
    const/16 v20, 0x2

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->reportScrollStateChange(I)V

    .line 4990
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->mFlingRunnable:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$FlingRunnable;

    move-object/from16 v20, v0

    neg-int v0, v12

    move/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$FlingRunnable;->start(I)V

    goto/16 :goto_4

    .line 4992
    :cond_1b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    const/16 v21, -0x1

    move/from16 v0, v21

    move-object/from16 v1, v20

    iput v0, v1, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mTouchMode:I

    .line 4993
    const/16 v20, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->reportScrollStateChange(I)V

    goto/16 :goto_4

    .line 4997
    .end local v12    # "initialVelocity":I
    .end local v17    # "velocityTracker":Landroid/view/VelocityTracker;
    :cond_1c
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    const/16 v21, -0x1

    move/from16 v0, v21

    move-object/from16 v1, v20

    iput v0, v1, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mTouchMode:I

    .line 4998
    const/16 v20, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->reportScrollStateChange(I)V

    goto/16 :goto_4

    .line 5030
    .end local v7    # "childCount":I
    :pswitch_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    const/16 v21, -0x1

    move/from16 v0, v21

    move-object/from16 v1, v20

    iput v0, v1, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mTouchMode:I

    .line 5031
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-virtual/range {v20 .. v21}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->setPressed(Z)V

    .line 5032
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mMotionPosition:I

    move/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mFirstPosition:I

    move/from16 v22, v0

    sub-int v21, v21, v22

    invoke-virtual/range {v20 .. v21}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v14

    .line 5033
    .restart local v14    # "motionView":Landroid/view/View;
    if-eqz v14, :cond_1d

    .line 5034
    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-virtual {v14, v0}, Landroid/view/View;->setPressed(Z)V

    .line 5036
    :cond_1d
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->clearScrollingCache()V

    .line 5038
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getHandler()Landroid/os/Handler;

    move-result-object v10

    .line 5039
    .restart local v10    # "handler":Landroid/os/Handler;
    if-eqz v10, :cond_1e

    .line 5040
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mPendingCheckForLongPress:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$CheckForLongPress;
    invoke-static/range {v20 .. v20}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->access$600(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;)Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$CheckForLongPress;

    move-result-object v20

    move-object/from16 v0, v20

    invoke-virtual {v10, v0}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 5043
    :cond_1e
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mVelocityTracker:Landroid/view/VelocityTracker;
    invoke-static/range {v20 .. v20}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->access$2700(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;)Landroid/view/VelocityTracker;

    move-result-object v20

    if-eqz v20, :cond_1f

    .line 5044
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    # getter for: Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mVelocityTracker:Landroid/view/VelocityTracker;
    invoke-static/range {v20 .. v20}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->access$2700(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;)Landroid/view/VelocityTracker;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Landroid/view/VelocityTracker;->recycle()V

    .line 5045
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    # setter for: Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mVelocityTracker:Landroid/view/VelocityTracker;
    invoke-static/range {v20 .. v21}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->access$2702(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;Landroid/view/VelocityTracker;)Landroid/view/VelocityTracker;

    .line 5048
    :cond_1f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v20, v0

    const/16 v21, -0x1

    # setter for: Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mActivePointerId:I
    invoke-static/range {v20 .. v21}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->access$3302(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;I)I

    goto/16 :goto_1

    .line 4808
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_4
        :pswitch_1
        :pswitch_7
    .end packed-switch

    .line 4858
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 4908
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method resurrectSelection()Z
    .locals 19

    .prologue
    .line 5059
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildCount()I

    move-result v2

    .line 5061
    .local v2, "childCount":I
    if-gtz v2, :cond_0

    .line 5062
    const/16 v17, 0x0

    .line 5153
    :goto_0
    return v17

    .line 5065
    :cond_0
    const/4 v12, 0x0

    .line 5067
    .local v12, "selectedLeft":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mListPadding:Landroid/graphics/Rect;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v3, v0, Landroid/graphics/Rect;->top:I

    .line 5068
    .local v3, "childrenLeft":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getRight()I

    move-result v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getLeft()I

    move-result v18

    sub-int v17, v17, v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mListPadding:Landroid/graphics/Rect;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v18, v0

    sub-int v4, v17, v18

    .line 5069
    .local v4, "childrenRight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v6, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mFirstPosition:I

    .line 5070
    .local v6, "firstPosition":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v15, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mResurrectToPosition:I

    .line 5071
    .local v15, "toPosition":I
    const/4 v5, 0x1

    .line 5073
    .local v5, "down":Z
    if-lt v15, v6, :cond_3

    add-int v17, v6, v2

    move/from16 v0, v17

    if-ge v15, v0, :cond_3

    .line 5074
    move v13, v15

    .line 5076
    .local v13, "selectedPos":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mFirstPosition:I

    move/from16 v18, v0

    sub-int v18, v13, v18

    invoke-virtual/range {v17 .. v18}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v11

    .line 5077
    .local v11, "selected":Landroid/view/View;
    invoke-virtual {v11}, Landroid/view/View;->getLeft()I

    move-result v12

    .line 5078
    invoke-virtual {v11}, Landroid/view/View;->getRight()I

    move-result v14

    .line 5081
    .local v14, "selectedRight":I
    if-ge v12, v3, :cond_2

    .line 5082
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getHorizontalFadingEdgeLength()I

    move-result v17

    add-int v12, v3, v17

    .line 5138
    .end local v11    # "selected":Landroid/view/View;
    .end local v14    # "selectedRight":I
    :cond_1
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v17, v0

    const/16 v18, -0x1

    move/from16 v0, v18

    move-object/from16 v1, v17

    iput v0, v1, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mResurrectToPosition:I

    .line 5139
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->mFlingRunnable:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$FlingRunnable;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 5140
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v17, v0

    const/16 v18, -0x1

    move/from16 v0, v18

    move-object/from16 v1, v17

    iput v0, v1, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mTouchMode:I

    .line 5141
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->clearScrollingCache()V

    .line 5142
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iput v12, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSpecificTop:I

    .line 5143
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v13, v5}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->lookForSelectablePosition(IZ)I

    move-result v13

    .line 5144
    if-lt v13, v6, :cond_b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getLastVisiblePosition()I

    move-result v17

    move/from16 v0, v17

    if-gt v13, v0, :cond_b

    .line 5145
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v17, v0

    const/16 v18, 0x4

    move/from16 v0, v18

    move-object/from16 v1, v17

    iput v0, v1, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mLayoutMode:I

    .line 5146
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v13}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->setSelectionInt(I)V

    .line 5147
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->invokeOnItemScrollListener()V

    .line 5151
    :goto_2
    const/16 v17, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->reportScrollStateChange(I)V

    .line 5153
    if-ltz v13, :cond_c

    const/16 v17, 0x1

    goto/16 :goto_0

    .line 5083
    .restart local v11    # "selected":Landroid/view/View;
    .restart local v14    # "selectedRight":I
    :cond_2
    if-le v14, v4, :cond_1

    .line 5084
    invoke-virtual {v11}, Landroid/view/View;->getMeasuredWidth()I

    move-result v17

    sub-int v17, v4, v17

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v18, v0

    invoke-virtual/range {v18 .. v18}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getHorizontalFadingEdgeLength()I

    move-result v18

    sub-int v12, v17, v18

    goto/16 :goto_1

    .line 5088
    .end local v11    # "selected":Landroid/view/View;
    .end local v13    # "selectedPos":I
    .end local v14    # "selectedRight":I
    :cond_3
    if-ge v15, v6, :cond_7

    .line 5090
    move v13, v6

    .line 5091
    .restart local v13    # "selectedPos":I
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_3
    if-ge v7, v2, :cond_1

    .line 5092
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v7}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v16

    .line 5093
    .local v16, "v":Landroid/view/View;
    invoke-virtual/range {v16 .. v16}, Landroid/view/View;->getLeft()I

    move-result v9

    .line 5095
    .local v9, "left":I
    if-nez v7, :cond_5

    .line 5097
    move v12, v9

    .line 5099
    if-gtz v6, :cond_4

    if-ge v9, v3, :cond_5

    .line 5102
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getHorizontalFadingEdgeLength()I

    move-result v17

    add-int v3, v3, v17

    .line 5105
    :cond_5
    if-lt v9, v3, :cond_6

    .line 5107
    add-int v13, v6, v7

    .line 5108
    move v12, v9

    .line 5109
    goto/16 :goto_1

    .line 5091
    :cond_6
    add-int/lit8 v7, v7, 0x1

    goto :goto_3

    .line 5113
    .end local v7    # "i":I
    .end local v9    # "left":I
    .end local v13    # "selectedPos":I
    .end local v16    # "v":Landroid/view/View;
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget v8, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mItemCount:I

    .line 5114
    .local v8, "itemCount":I
    const/4 v5, 0x0

    .line 5115
    add-int v17, v6, v2

    add-int/lit8 v13, v17, -0x1

    .line 5117
    .restart local v13    # "selectedPos":I
    add-int/lit8 v7, v2, -0x1

    .restart local v7    # "i":I
    :goto_4
    if-ltz v7, :cond_1

    .line 5118
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v7}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v16

    .line 5119
    .restart local v16    # "v":Landroid/view/View;
    invoke-virtual/range {v16 .. v16}, Landroid/view/View;->getLeft()I

    move-result v9

    .line 5120
    .restart local v9    # "left":I
    invoke-virtual/range {v16 .. v16}, Landroid/view/View;->getRight()I

    move-result v10

    .line 5122
    .local v10, "right":I
    add-int/lit8 v17, v2, -0x1

    move/from16 v0, v17

    if-ne v7, v0, :cond_9

    .line 5123
    move v12, v9

    .line 5124
    add-int v17, v6, v2

    move/from16 v0, v17

    if-lt v0, v8, :cond_8

    if-le v10, v4, :cond_9

    .line 5125
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getHorizontalFadingEdgeLength()I

    move-result v17

    sub-int v4, v4, v17

    .line 5129
    :cond_9
    if-gt v10, v4, :cond_a

    .line 5130
    add-int v13, v6, v7

    .line 5131
    move v12, v9

    .line 5132
    goto/16 :goto_1

    .line 5117
    :cond_a
    add-int/lit8 v7, v7, -0x1

    goto :goto_4

    .line 5149
    .end local v7    # "i":I
    .end local v8    # "itemCount":I
    .end local v9    # "left":I
    .end local v10    # "right":I
    .end local v16    # "v":Landroid/view/View;
    :cond_b
    const/4 v13, -0x1

    goto/16 :goto_2

    .line 5153
    :cond_c
    const/16 v17, 0x0

    goto/16 :goto_0
.end method

.method trackMotionScroll(II)Z
    .locals 26
    .param p1, "delta"    # I
    .param p2, "incrementalDelta"    # I

    .prologue
    .line 5160
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildCount()I

    move-result v4

    .line 5161
    .local v4, "childCount":I
    if-nez v4, :cond_0

    .line 5162
    const/16 v24, 0x1

    .line 5292
    :goto_0
    return v24

    .line 5165
    :cond_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    invoke-virtual/range {v24 .. v25}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/view/View;->getLeft()I

    move-result v9

    .line 5166
    .local v9, "firstLeft":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v24, v0

    add-int/lit8 v25, v4, -0x1

    invoke-virtual/range {v24 .. v25}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v24

    invoke-virtual/range {v24 .. v24}, Landroid/view/View;->getRight()I

    move-result v15

    .line 5168
    .local v15, "lastRight":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mListPadding:Landroid/graphics/Rect;

    move-object/from16 v17, v0

    .line 5171
    .local v17, "listPadding":Landroid/graphics/Rect;
    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v24, v0

    sub-int v20, v24, v9

    .line 5172
    .local v20, "spaceAbove":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getWidth()I

    move-result v24

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v25, v0

    sub-int v8, v24, v25

    .line 5173
    .local v8, "end":I
    sub-int v21, v15, v8

    .line 5175
    .local v21, "spaceBelow":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getWidth()I

    move-result v24

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getPaddingRight()I

    move-result v25

    sub-int v24, v24, v25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getPaddingLeft()I

    move-result v25

    sub-int v23, v24, v25

    .line 5176
    .local v23, "width":I
    if-gez p1, :cond_1

    .line 5177
    add-int/lit8 v24, v23, -0x1

    move/from16 v0, v24

    neg-int v0, v0

    move/from16 v24, v0

    move/from16 v0, v24

    move/from16 v1, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result p1

    .line 5182
    :goto_1
    if-gez p2, :cond_2

    .line 5183
    add-int/lit8 v24, v23, -0x1

    move/from16 v0, v24

    neg-int v0, v0

    move/from16 v24, v0

    move/from16 v0, v24

    move/from16 v1, p2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result p2

    .line 5188
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget v10, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mFirstPosition:I

    .line 5190
    .local v10, "firstPosition":I
    if-nez v10, :cond_3

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v24, v0

    move/from16 v0, v24

    if-lt v9, v0, :cond_3

    if-ltz p1, :cond_3

    .line 5194
    const/16 v24, 0x1

    goto/16 :goto_0

    .line 5179
    .end local v10    # "firstPosition":I
    :cond_1
    add-int/lit8 v24, v23, -0x1

    move/from16 v0, v24

    move/from16 v1, p1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result p1

    goto :goto_1

    .line 5185
    :cond_2
    add-int/lit8 v24, v23, -0x1

    move/from16 v0, v24

    move/from16 v1, p2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result p2

    goto :goto_2

    .line 5197
    .restart local v10    # "firstPosition":I
    :cond_3
    add-int v24, v10, v4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mItemCount:I

    move/from16 v25, v0

    move/from16 v0, v24

    move/from16 v1, v25

    if-ne v0, v1, :cond_4

    if-gt v15, v8, :cond_4

    if-gtz p1, :cond_4

    .line 5201
    const/16 v24, 0x1

    goto/16 :goto_0

    .line 5204
    :cond_4
    if-gez p2, :cond_c

    const/4 v7, 0x1

    .line 5206
    .local v7, "down":Z
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->isInTouchMode()Z

    move-result v14

    .line 5207
    .local v14, "inTouchMode":Z
    if-eqz v14, :cond_5

    .line 5208
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->hideSelector()V

    .line 5211
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getHeaderViewsCount()I

    move-result v12

    .line 5212
    .local v12, "headerViewsCount":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mItemCount:I

    move/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v25, v0

    invoke-virtual/range {v25 .. v25}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getFooterViewsCount()I

    move-result v25

    sub-int v11, v24, v25

    .line 5214
    .local v11, "footerViewsStart":I
    const/16 v22, 0x0

    .line 5215
    .local v22, "start":I
    const/4 v6, 0x0

    .line 5217
    .local v6, "count":I
    if-eqz v7, :cond_f

    .line 5218
    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/Rect;->left:I

    move/from16 v24, v0

    sub-int v16, v24, p2

    .line 5219
    .local v16, "left":I
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_4
    if-ge v13, v4, :cond_6

    .line 5220
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v13}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 5221
    .local v3, "child":Landroid/view/View;
    invoke-virtual {v3}, Landroid/view/View;->getRight()I

    move-result v24

    move/from16 v0, v24

    move/from16 v1, v16

    if-lt v0, v1, :cond_d

    .line 5260
    .end local v3    # "child":Landroid/view/View;
    .end local v16    # "left":I
    :cond_6
    move-object/from16 v0, p0

    iget v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->mMotionViewOriginalLeft:I

    move/from16 v24, v0

    add-int v24, v24, p1

    move/from16 v0, v24

    move-object/from16 v1, p0

    iput v0, v1, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->mMotionViewNewLeft:I

    .line 5262
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v24, v0

    const/16 v25, 0x1

    move/from16 v0, v25

    move-object/from16 v1, v24

    iput-boolean v0, v1, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mBlockLayoutRequests:Z

    .line 5264
    if-lez v6, :cond_7

    .line 5265
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    move/from16 v1, v22

    # invokes: Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->detachViewsFromParent(II)V
    invoke-static {v0, v1, v6}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->access$3600(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;II)V

    .line 5267
    :cond_7
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    move/from16 v1, p2

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->offsetChildrenLeftAndRight(I)V

    .line 5269
    if-eqz v7, :cond_8

    .line 5270
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mFirstPosition:I

    move/from16 v25, v0

    add-int v25, v25, v6

    move/from16 v0, v25

    move-object/from16 v1, v24

    iput v0, v1, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mFirstPosition:I

    .line 5273
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->invalidate()V

    .line 5275
    invoke-static/range {p2 .. p2}, Ljava/lang/Math;->abs(I)I

    move-result v2

    .line 5276
    .local v2, "absIncrementalDelta":I
    move/from16 v0, v20

    if-lt v0, v2, :cond_9

    move/from16 v0, v21

    if-ge v0, v2, :cond_a

    .line 5277
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v7}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->fillGap(Z)V

    .line 5280
    :cond_a
    if-nez v14, :cond_b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSelectedPosition:I

    move/from16 v24, v0

    const/16 v25, -0x1

    move/from16 v0, v24

    move/from16 v1, v25

    if-eq v0, v1, :cond_b

    .line 5281
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSelectedPosition:I

    move/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    iget v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mFirstPosition:I

    move/from16 v25, v0

    sub-int v5, v24, v25

    .line 5282
    .local v5, "childIndex":I
    if-ltz v5, :cond_b

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildCount()I

    move-result v24

    move/from16 v0, v24

    if-ge v5, v0, :cond_b

    .line 5283
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v25, v0

    move-object/from16 v0, v25

    invoke-virtual {v0, v5}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v25

    invoke-virtual/range {v24 .. v25}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->positionSelector(Landroid/view/View;)V

    .line 5287
    .end local v5    # "childIndex":I
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v24, v0

    const/16 v25, 0x0

    move/from16 v0, v25

    move-object/from16 v1, v24

    iput-boolean v0, v1, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mBlockLayoutRequests:Z

    .line 5289
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->invokeOnItemScrollListener()V

    .line 5292
    const/16 v24, 0x0

    goto/16 :goto_0

    .line 5204
    .end local v2    # "absIncrementalDelta":I
    .end local v6    # "count":I
    .end local v7    # "down":Z
    .end local v11    # "footerViewsStart":I
    .end local v12    # "headerViewsCount":I
    .end local v13    # "i":I
    .end local v14    # "inTouchMode":Z
    .end local v22    # "start":I
    :cond_c
    const/4 v7, 0x0

    goto/16 :goto_3

    .line 5224
    .restart local v3    # "child":Landroid/view/View;
    .restart local v6    # "count":I
    .restart local v7    # "down":Z
    .restart local v11    # "footerViewsStart":I
    .restart local v12    # "headerViewsCount":I
    .restart local v13    # "i":I
    .restart local v14    # "inTouchMode":Z
    .restart local v16    # "left":I
    .restart local v22    # "start":I
    :cond_d
    add-int/lit8 v6, v6, 0x1

    .line 5225
    add-int v18, v10, v13

    .line 5226
    .local v18, "position":I
    move/from16 v0, v18

    if-lt v0, v12, :cond_e

    move/from16 v0, v18

    if-ge v0, v11, :cond_e

    .line 5227
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mRecycler:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$RecycleBin;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v3}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$RecycleBin;->addScrapView(Landroid/view/View;)V

    .line 5219
    :cond_e
    add-int/lit8 v13, v13, 0x1

    goto/16 :goto_4

    .line 5238
    .end local v3    # "child":Landroid/view/View;
    .end local v13    # "i":I
    .end local v16    # "left":I
    .end local v18    # "position":I
    :cond_f
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v24, v0

    invoke-virtual/range {v24 .. v24}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getWidth()I

    move-result v24

    move-object/from16 v0, v17

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v25, v0

    sub-int v24, v24, v25

    sub-int v19, v24, p2

    .line 5239
    .local v19, "right":I
    add-int/lit8 v13, v4, -0x1

    .restart local v13    # "i":I
    :goto_5
    if-ltz v13, :cond_6

    .line 5240
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v13}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 5241
    .restart local v3    # "child":Landroid/view/View;
    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v24

    move/from16 v0, v24

    move/from16 v1, v19

    if-le v0, v1, :cond_6

    .line 5244
    move/from16 v22, v13

    .line 5245
    add-int/lit8 v6, v6, 0x1

    .line 5246
    add-int v18, v10, v13

    .line 5247
    .restart local v18    # "position":I
    move/from16 v0, v18

    if-lt v0, v12, :cond_10

    move/from16 v0, v18

    if-ge v0, v11, :cond_10

    .line 5248
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mRecycler:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$RecycleBin;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    invoke-virtual {v0, v3}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$RecycleBin;->addScrapView(Landroid/view/View;)V

    .line 5239
    :cond_10
    add-int/lit8 v13, v13, -0x1

    goto :goto_5
.end method
