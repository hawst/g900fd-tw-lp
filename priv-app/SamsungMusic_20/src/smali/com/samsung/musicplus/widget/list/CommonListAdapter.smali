.class public Lcom/samsung/musicplus/widget/list/CommonListAdapter;
.super Landroid/widget/ResourceCursorAdapter;
.source "CommonListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = false

.field private static final TAG:Ljava/lang/String; = "MusicUiList"


# instance fields
.field protected mActionModeHistory:Z

.field protected mAlbumArtParsingEnabled:Z

.field protected mAlbumArtSize:I

.field protected mAlbumIdIndex:I

.field private mAlbumLoader:Lcom/samsung/musicplus/util/IAlbumArtLoader;

.field protected mAudioIndex:I

.field protected mBitDepthIndex:I

.field protected mContext:Landroid/content/Context;

.field protected mIsActionMode:Z

.field protected mKeyIndex:I

.field protected mSamplingRateIndex:I

.field public mScrollState:I

.field protected mSecretBoxIndex:I

.field private mSelectedKeyword:Ljava/lang/String;

.field protected mText1Index:I

.field protected mText2Index:I

.field protected mText3Index:I

.field private mTextAirViewListener:Lcom/samsung/musicplus/library/hardware/AirView$OnAirViewPopupListener;

.field private mTheme:I

.field private mViewType:I


# direct methods
.method public constructor <init>(Landroid/content/Context;ILandroid/database/Cursor;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "layout"    # I
    .param p3, "c"    # Landroid/database/Cursor;
    .param p4, "flags"    # I

    .prologue
    const/4 v1, 0x0

    .line 116
    invoke-direct {p0, p1, p2, p3, p4}, Landroid/widget/ResourceCursorAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;I)V

    .line 79
    iput-boolean v1, p0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->mIsActionMode:Z

    .line 81
    iput-boolean v1, p0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->mActionModeHistory:Z

    .line 102
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->mSelectedKeyword:Ljava/lang/String;

    .line 109
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->mAlbumArtParsingEnabled:Z

    .line 225
    invoke-static {}, Lcom/samsung/musicplus/util/UiUtils;->getTheme()I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->mTheme:I

    .line 253
    new-instance v0, Lcom/samsung/musicplus/widget/list/CommonListAdapter$1;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/widget/list/CommonListAdapter$1;-><init>(Lcom/samsung/musicplus/widget/list/CommonListAdapter;)V

    iput-object v0, p0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->mTextAirViewListener:Lcom/samsung/musicplus/library/hardware/AirView$OnAirViewPopupListener;

    .line 555
    iput v1, p0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->mScrollState:I

    .line 117
    iput-object p1, p0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->mContext:Landroid/content/Context;

    .line 118
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0138

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->mAlbumArtSize:I

    .line 120
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/widget/list/CommonListAdapter;Landroid/view/View;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/list/CommonListAdapter;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->getTextHoverView(Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private getAlbumArtId(Landroid/database/Cursor;)J
    .locals 3
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 531
    const-wide/16 v0, -0x1

    .line 532
    .local v0, "albumId":J
    iget v2, p0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->mAlbumIdIndex:I

    if-ltz v2, :cond_0

    .line 533
    iget v2, p0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->mAlbumIdIndex:I

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 535
    :cond_0
    return-wide v0
.end method

.method private getTextHoverView(Landroid/view/View;)Landroid/view/View;
    .locals 13
    .param p1, "parent"    # Landroid/view/View;

    .prologue
    const/4 v9, 0x0

    const/4 v12, 0x0

    .line 270
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    .line 272
    .local v3, "tag":Ljava/lang/Object;
    instance-of v10, v3, Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;

    if-eqz v10, :cond_2

    move-object v8, v3

    .line 273
    check-cast v8, Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;

    .line 278
    .local v8, "vh":Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;
    iget-object v10, p0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->mContext:Landroid/content/Context;

    const-string v11, "layout_inflater"

    invoke-virtual {v10, v11}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 280
    .local v2, "li":Landroid/view/LayoutInflater;
    const/4 v7, 0x0

    .line 281
    .local v7, "v":Landroid/view/View;
    iget v10, p0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->mTheme:I

    if-nez v10, :cond_3

    .line 282
    const v10, 0x7f040019

    invoke-virtual {v2, v10, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    .line 287
    :goto_0
    iget-object v9, v8, Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;->text1:Landroid/widget/TextView;

    invoke-virtual {v9}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    .line 288
    .local v4, "text1":Ljava/lang/CharSequence;
    const/4 v5, 0x0

    .line 289
    .local v5, "text2":Ljava/lang/CharSequence;
    iget-object v9, v8, Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;->text2:Landroid/widget/TextView;

    if-eqz v9, :cond_0

    .line 290
    iget-object v9, v8, Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;->text2:Landroid/widget/TextView;

    invoke-virtual {v9}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v5

    .line 295
    :cond_0
    if-eqz v4, :cond_4

    iget-object v9, v8, Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;->text1:Landroid/widget/TextView;

    invoke-direct {p0, v9}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->isEllipsis(Landroid/widget/TextView;)Z

    move-result v9

    if-eqz v9, :cond_4

    if-eqz v5, :cond_4

    iget-object v9, v8, Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;->text2:Landroid/widget/TextView;

    invoke-direct {p0, v9}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->isEllipsis(Landroid/widget/TextView;)Z

    move-result v9

    if-eqz v9, :cond_4

    .line 296
    const v9, 0x7f0d0067

    invoke-virtual {v7, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 297
    .local v6, "title":Landroid/widget/TextView;
    const v9, 0x7f0d007d

    invoke-virtual {v7, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 299
    .local v0, "artist":Landroid/widget/TextView;
    invoke-virtual {v6, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 300
    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 302
    invoke-virtual {v6, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 303
    invoke-virtual {v0, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 315
    .end local v0    # "artist":Landroid/widget/TextView;
    .end local v2    # "li":Landroid/view/LayoutInflater;
    .end local v4    # "text1":Ljava/lang/CharSequence;
    .end local v5    # "text2":Ljava/lang/CharSequence;
    .end local v6    # "title":Landroid/widget/TextView;
    .end local v7    # "v":Landroid/view/View;
    .end local v8    # "vh":Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;
    :cond_1
    :goto_1
    return-object v7

    :cond_2
    move-object v7, v9

    .line 275
    goto :goto_1

    .line 284
    .restart local v2    # "li":Landroid/view/LayoutInflater;
    .restart local v7    # "v":Landroid/view/View;
    .restart local v8    # "vh":Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;
    :cond_3
    const v10, 0x7f040018

    invoke-virtual {v2, v10, v9}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    goto :goto_0

    .line 305
    .restart local v4    # "text1":Ljava/lang/CharSequence;
    .restart local v5    # "text2":Ljava/lang/CharSequence;
    :cond_4
    const v9, 0x7f0d006b

    invoke-virtual {v7, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 307
    .local v1, "fullText":Landroid/widget/TextView;
    if-eqz v4, :cond_5

    iget-object v9, v8, Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;->text1:Landroid/widget/TextView;

    invoke-direct {p0, v9}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->isEllipsis(Landroid/widget/TextView;)Z

    move-result v9

    if-eqz v9, :cond_5

    .line 308
    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 309
    invoke-virtual {v1, v12}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 310
    :cond_5
    if-eqz v5, :cond_1

    iget-object v9, v8, Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;->text2:Landroid/widget/TextView;

    invoke-direct {p0, v9}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->isEllipsis(Landroid/widget/TextView;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 311
    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 312
    invoke-virtual {v1, v12}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method

.method private isEllipsis(Landroid/widget/TextView;)Z
    .locals 1
    .param p1, "v"    # Landroid/widget/TextView;

    .prologue
    .line 319
    invoke-static {p1}, Lcom/samsung/musicplus/library/view/TextViewCompat;->isEllipsis(Landroid/widget/TextView;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 552
    const/4 v0, 0x0

    return v0
.end method

.method protected bindOtherView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "c"    # Landroid/database/Cursor;

    .prologue
    const/4 v4, 0x0

    .line 411
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;

    .line 412
    .local v1, "vh":Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;
    iget-boolean v2, p0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->mIsActionMode:Z

    if-eqz v2, :cond_3

    .line 413
    iget-object v2, v1, Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;->checkbox:Landroid/widget/CheckBox;

    if-nez v2, :cond_1

    .line 414
    const v2, 0x7f0d00a6

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 415
    .local v0, "checkBox":Landroid/view/View;
    instance-of v2, v0, Landroid/view/ViewStub;

    if-eqz v2, :cond_0

    .line 416
    check-cast v0, Landroid/view/ViewStub;

    .end local v0    # "checkBox":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 418
    :cond_0
    const v2, 0x7f0d004c

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    iput-object v2, v1, Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;->checkbox:Landroid/widget/CheckBox;

    .line 420
    :cond_1
    iget-object v2, v1, Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;->checkbox:Landroid/widget/CheckBox;

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setAlpha(F)V

    .line 421
    iget-object v2, v1, Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;->checkbox:Landroid/widget/CheckBox;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setTranslationX(F)V

    .line 422
    iget-object v2, v1, Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;->checkbox:Landroid/widget/CheckBox;

    invoke-virtual {v2, v4}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 429
    :cond_2
    :goto_0
    return-void

    .line 424
    :cond_3
    iget-object v2, v1, Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;->checkbox:Landroid/widget/CheckBox;

    if-eqz v2, :cond_2

    .line 425
    iget-object v2, v1, Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;->checkbox:Landroid/widget/CheckBox;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 426
    iget-object v2, v1, Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;->checkbox:Landroid/widget/CheckBox;

    invoke-virtual {v2, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0
.end method

.method protected bindTextView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "c"    # Landroid/database/Cursor;

    .prologue
    .line 390
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;

    .line 391
    .local v2, "vh":Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;
    iget-object v3, v2, Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;->text1:Landroid/widget/TextView;

    iget v4, p0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->mText1Index:I

    invoke-interface {p3, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {p2, v4}, Lcom/samsung/musicplus/util/UiUtils;->transUnknownString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 392
    iget-object v3, v2, Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;->text2:Landroid/widget/TextView;

    if-eqz v3, :cond_0

    iget v3, p0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->mText2Index:I

    if-ltz v3, :cond_0

    .line 393
    iget v3, p0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->mText2Index:I

    invoke-interface {p3, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p2, v3}, Lcom/samsung/musicplus/util/UiUtils;->transUnknownString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 394
    .local v0, "text2":Ljava/lang/String;
    iget-object v3, v2, Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;->text2:Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 396
    .end local v0    # "text2":Ljava/lang/String;
    :cond_0
    iget-object v3, v2, Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;->text3:Landroid/widget/TextView;

    if-eqz v3, :cond_1

    iget v3, p0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->mText3Index:I

    if-ltz v3, :cond_1

    .line 397
    iget v3, p0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->mText3Index:I

    invoke-interface {p3, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {p2, v3}, Lcom/samsung/musicplus/util/UiUtils;->transUnknownString(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 398
    .local v1, "text3":Ljava/lang/String;
    iget-object v3, v2, Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;->text3:Landroid/widget/TextView;

    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 400
    .end local v1    # "text3":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method protected bindThumbnailView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "c"    # Landroid/database/Cursor;

    .prologue
    .line 371
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;

    .line 372
    .local v0, "vh":Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;
    iget v1, p0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->mViewType:I

    if-nez v1, :cond_0

    .line 373
    iget-object v1, v0, Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;->thumbnail:Landroid/widget/ImageView;

    invoke-virtual {p0, p3}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->getArtKey(Landroid/database/Cursor;)Ljava/lang/Object;

    move-result-object v2

    const v3, 0x7f02003b

    invoke-virtual {p0, p2, v1, v2, v3}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->setArtWorkView(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/Object;I)Z

    .line 379
    :goto_0
    return-void

    .line 376
    :cond_0
    iget-object v1, v0, Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;->thumbnail:Landroid/widget/ImageView;

    invoke-virtual {p0, p3}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->getArtKey(Landroid/database/Cursor;)Ljava/lang/Object;

    move-result-object v2

    const v3, 0x7f02003a

    invoke-virtual {p0, p2, v1, v2, v3}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->setGridArtWorkView(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/Object;I)Z

    goto :goto_0
.end method

.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "c"    # Landroid/database/Cursor;

    .prologue
    .line 327
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;

    .line 328
    .local v0, "vh":Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;
    iget-boolean v1, p0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->mAlbumArtParsingEnabled:Z

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;->thumbnail:Landroid/widget/ImageView;

    if-eqz v1, :cond_0

    .line 329
    invoke-virtual {p0, p1, p2, p3}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->bindThumbnailView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    .line 331
    :cond_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->bindTextView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    .line 332
    invoke-virtual {p0, p1, p2, p3}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->bindOtherView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    .line 337
    sget-boolean v1, Lcom/samsung/musicplus/util/Features;->FLAG_SUPPORT_SPLIT_LIST_VIEW:Z

    if-eqz v1, :cond_1

    .line 338
    invoke-virtual {p0, p1, p3}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->setSelectedPositionBackground(Landroid/view/View;Landroid/database/Cursor;)V

    .line 340
    :cond_1
    return-void
.end method

.method protected getArtKey(Landroid/database/Cursor;)Ljava/lang/Object;
    .locals 2
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 520
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->getAlbumArtId(Landroid/database/Cursor;)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method protected getArtworkInBackground(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/Object;I)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "iv"    # Landroid/widget/ImageView;
    .param p3, "artKey"    # Ljava/lang/Object;
    .param p4, "resId"    # I

    .prologue
    .line 484
    invoke-virtual {p2, p4}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 485
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->mAlbumLoader:Lcom/samsung/musicplus/util/IAlbumArtLoader;

    if-nez v0, :cond_0

    .line 486
    invoke-static {}, Lcom/samsung/musicplus/util/AlbumArtLoader;->getAlbumArtLoader()Lcom/samsung/musicplus/util/IAlbumArtLoader;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->mAlbumLoader:Lcom/samsung/musicplus/util/IAlbumArtLoader;

    .line 488
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->mAlbumLoader:Lcom/samsung/musicplus/util/IAlbumArtLoader;

    sget-object v2, Lcom/samsung/musicplus/util/AlbumArtUtils;->DEFAULT_ARTWORK_URI_STRING:Ljava/lang/String;

    iget v5, p0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->mAlbumArtSize:I

    iget v1, p0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->mScrollState:I

    if-eqz v1, :cond_1

    const/4 v7, 0x1

    :goto_0
    move-object v1, p1

    move-object v3, p3

    move-object v4, p2

    move v6, p4

    invoke-interface/range {v0 .. v7}, Lcom/samsung/musicplus/util/IAlbumArtLoader;->loadArtwork(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Object;Landroid/widget/ImageView;IIZ)V

    .line 490
    return-void

    .line 488
    :cond_1
    const/4 v7, 0x0

    goto :goto_0
.end method

.method protected getCachedArtwork(Ljava/lang/Object;)Landroid/graphics/drawable/Drawable;
    .locals 2
    .param p1, "artKey"    # Ljava/lang/Object;

    .prologue
    .line 547
    check-cast p1, Ljava/lang/Long;

    .end local p1    # "artKey":Ljava/lang/Object;
    invoke-virtual {p1}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/samsung/musicplus/util/AlbumArtUtils;->getCachedArtwork(J)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method protected getGridArtworkInBackground(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/Object;I)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "iv"    # Landroid/widget/ImageView;
    .param p3, "artKey"    # Ljava/lang/Object;
    .param p4, "resId"    # I

    .prologue
    .line 502
    const v0, 0x7f02003a

    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 503
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->mAlbumLoader:Lcom/samsung/musicplus/util/IAlbumArtLoader;

    if-nez v0, :cond_0

    .line 504
    invoke-static {}, Lcom/samsung/musicplus/util/AlbumArtLoader;->getAlbumArtLoader()Lcom/samsung/musicplus/util/IAlbumArtLoader;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->mAlbumLoader:Lcom/samsung/musicplus/util/IAlbumArtLoader;

    .line 506
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0049

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->mAlbumArtSize:I

    .line 508
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->mAlbumLoader:Lcom/samsung/musicplus/util/IAlbumArtLoader;

    sget-object v2, Lcom/samsung/musicplus/util/AlbumArtUtils;->DEFAULT_ARTWORK_URI_STRING:Ljava/lang/String;

    iget v5, p0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->mAlbumArtSize:I

    iget v1, p0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->mScrollState:I

    if-eqz v1, :cond_1

    const/4 v7, 0x1

    :goto_0
    move-object v1, p1

    move-object v3, p3

    move-object v4, p2

    move v6, p4

    invoke-interface/range {v0 .. v7}, Lcom/samsung/musicplus/util/IAlbumArtLoader;->loadGridArtwork(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Object;Landroid/widget/ImageView;IIZ)V

    .line 510
    return-void

    .line 508
    :cond_1
    const/4 v7, 0x0

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 136
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 140
    const/4 p2, 0x0

    .line 142
    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/widget/ResourceCursorAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected getViewHolder()Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;
    .locals 1

    .prologue
    .line 222
    new-instance v0, Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;-><init>(Lcom/samsung/musicplus/widget/list/CommonListAdapter;)V

    return-object v0
.end method

.method protected hideBottomDivider(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "c"    # Landroid/database/Cursor;

    .prologue
    .line 343
    const v1, 0x7f0d00bb

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 344
    .local v0, "divider":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 345
    invoke-interface {p3}, Landroid/database/Cursor;->isLast()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 346
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 351
    :cond_0
    :goto_0
    return-void

    .line 348
    :cond_1
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method protected newOtherView(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 205
    iget-boolean v2, p0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->mActionModeHistory:Z

    if-eqz v2, :cond_1

    .line 206
    const v2, 0x7f0d00a6

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 207
    .local v0, "checkBox":Landroid/view/View;
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;

    .line 208
    .local v1, "vh":Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;
    instance-of v2, v0, Landroid/view/ViewStub;

    if-eqz v2, :cond_0

    .line 209
    check-cast v0, Landroid/view/ViewStub;

    .end local v0    # "checkBox":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 211
    :cond_0
    const v2, 0x7f0d004c

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    iput-object v2, v1, Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;->checkbox:Landroid/widget/CheckBox;

    .line 213
    .end local v1    # "vh":Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;
    :cond_1
    return-void
.end method

.method protected newTextAirView(Landroid/view/View;Landroid/widget/TextView;Landroid/widget/TextView;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;
    .param p2, "text1"    # Landroid/widget/TextView;
    .param p3, "text2"    # Landroid/widget/TextView;

    .prologue
    .line 244
    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    .line 251
    :cond_0
    :goto_0
    return-void

    .line 250
    :cond_1
    iget-object v3, p0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->mTextAirViewListener:Lcom/samsung/musicplus/library/hardware/AirView$OnAirViewPopupListener;

    const/4 v4, 0x1

    const/4 v5, 0x0

    move-object v0, p1

    move-object v1, p2

    move-object v2, p3

    invoke-static/range {v0 .. v5}, Lcom/samsung/musicplus/library/hardware/AirView;->setView(Landroid/view/View;Landroid/widget/TextView;Landroid/widget/TextView;Lcom/samsung/musicplus/library/hardware/AirView$OnAirViewPopupListener;ZZ)V

    goto :goto_0
.end method

.method protected newTextView(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 177
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;

    .line 178
    .local v0, "vh":Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;
    const v1, 0x7f0d00a0

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v0, Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;->text2:Landroid/widget/TextView;

    .line 179
    iget-object v1, v0, Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;->text2:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 180
    iget v1, p0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->mText2Index:I

    if-ltz v1, :cond_2

    .line 181
    iget-object v1, v0, Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;->text2:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 186
    :cond_0
    :goto_0
    const v1, 0x7f0d00a1

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v0, Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;->text3:Landroid/widget/TextView;

    .line 187
    iget-object v1, v0, Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;->text3:Landroid/widget/TextView;

    if-eqz v1, :cond_1

    .line 188
    iget v1, p0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->mText3Index:I

    if-ltz v1, :cond_3

    .line 189
    iget-object v1, v0, Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;->text3:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 194
    :cond_1
    :goto_1
    const v1, 0x7f0d009f

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v0, Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;->text1:Landroid/widget/TextView;

    .line 195
    invoke-virtual {p1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 196
    return-void

    .line 183
    :cond_2
    iget-object v1, v0, Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;->text2:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 191
    :cond_3
    iget-object v1, v0, Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;->text3:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method

.method protected newThumbnailView(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 165
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;

    .line 166
    .local v0, "vh":Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;
    const v1, 0x7f0d00a5

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, v0, Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;->thumbnail:Landroid/widget/ImageView;

    .line 167
    invoke-virtual {p1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 168
    return-void
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "c"    # Landroid/database/Cursor;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 147
    const-string v2, "MusicUiList"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " newView"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    invoke-super {p0, p1, p2, p3}, Landroid/widget/ResourceCursorAdapter;->newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 149
    .local v0, "v":Landroid/view/View;
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->getViewHolder()Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;

    move-result-object v1

    .line 150
    .local v1, "vh":Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;
    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 151
    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->newThumbnailView(Landroid/view/View;)V

    .line 152
    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->newTextView(Landroid/view/View;)V

    .line 153
    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->newOtherView(Landroid/view/View;)V

    .line 154
    iget-object v2, v1, Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;->text1:Landroid/widget/TextView;

    iget-object v3, v1, Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;->text2:Landroid/widget/TextView;

    invoke-virtual {p0, v0, v2, v3}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->setTextAirView(Landroid/view/View;Landroid/widget/TextView;Landroid/widget/TextView;)V

    .line 155
    return-object v0
.end method

.method public setActionMode(Z)V
    .locals 1
    .param p1, "isActionMode"    # Z

    .prologue
    .line 123
    iput-boolean p1, p0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->mIsActionMode:Z

    .line 124
    if-eqz p1, :cond_0

    .line 126
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->mActionModeHistory:Z

    .line 128
    :cond_0
    return-void
.end method

.method public setAlbumArtParsingEnabled(Z)V
    .locals 0
    .param p1, "albumArtParsingEnabled"    # Z

    .prologue
    .line 112
    iput-boolean p1, p0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->mAlbumArtParsingEnabled:Z

    .line 113
    return-void
.end method

.method protected setArtWorkView(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/Object;I)Z
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "iv"    # Landroid/widget/ImageView;
    .param p3, "artKey"    # Ljava/lang/Object;
    .param p4, "resId"    # I

    .prologue
    .line 442
    invoke-virtual {p2, p3}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 444
    invoke-virtual {p0, p3}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->getCachedArtwork(Ljava/lang/Object;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 445
    .local v0, "d":Landroid/graphics/drawable/Drawable;
    if-nez v0, :cond_0

    .line 446
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->getArtworkInBackground(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/Object;I)V

    .line 450
    :goto_0
    if-eqz v0, :cond_1

    const/4 v1, 0x1

    :goto_1
    return v1

    .line 448
    :cond_0
    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 450
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method protected setGridArtWorkView(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/Object;I)Z
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "iv"    # Landroid/widget/ImageView;
    .param p3, "artKey"    # Ljava/lang/Object;
    .param p4, "resId"    # I

    .prologue
    .line 464
    invoke-virtual {p2, p3}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    move-object v1, p3

    .line 466
    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/samsung/musicplus/util/AlbumArtUtils;->getGridCachedArtwork(J)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 467
    .local v0, "d":Landroid/graphics/drawable/Drawable;
    if-nez v0, :cond_0

    .line 468
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->getGridArtworkInBackground(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/Object;I)V

    .line 472
    :goto_0
    if-eqz v0, :cond_1

    const/4 v1, 0x1

    :goto_1
    return v1

    .line 470
    :cond_0
    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 472
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method public setIndex(IIIIIIIII)V
    .locals 0
    .param p1, "audioId"    # I
    .param p2, "text1"    # I
    .param p3, "text2"    # I
    .param p4, "text3"    # I
    .param p5, "albumId"    # I
    .param p6, "keyWord"    # I
    .param p7, "samplingRate"    # I
    .param p8, "bitDepth"    # I
    .param p9, "secretBox"    # I

    .prologue
    .line 85
    iput p1, p0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->mAudioIndex:I

    .line 86
    iput p2, p0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->mText1Index:I

    .line 87
    iput p3, p0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->mText2Index:I

    .line 88
    iput p4, p0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->mText3Index:I

    .line 89
    iput p5, p0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->mAlbumIdIndex:I

    .line 90
    iput p6, p0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->mKeyIndex:I

    .line 91
    iput p7, p0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->mSamplingRateIndex:I

    .line 92
    iput p8, p0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->mBitDepthIndex:I

    .line 93
    iput p9, p0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->mSecretBoxIndex:I

    .line 94
    return-void
.end method

.method public setScrollSate(I)V
    .locals 0
    .param p1, "scrollState"    # I

    .prologue
    .line 558
    iput p1, p0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->mScrollState:I

    .line 559
    return-void
.end method

.method public setSelectedKeyword(Ljava/lang/String;)V
    .locals 0
    .param p1, "keyword"    # Ljava/lang/String;

    .prologue
    .line 105
    iput-object p1, p0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->mSelectedKeyword:Ljava/lang/String;

    .line 106
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->notifyDataSetChanged()V

    .line 107
    return-void
.end method

.method protected setSelectedPositionBackground(Landroid/view/View;Landroid/database/Cursor;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .param p2, "c"    # Landroid/database/Cursor;

    .prologue
    .line 354
    if-eqz p2, :cond_0

    iget v0, p0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->mKeyIndex:I

    const/4 v1, -0x1

    if-le v0, v1, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->mSelectedKeyword:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->mSelectedKeyword:Ljava/lang/String;

    iget v1, p0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->mKeyIndex:I

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 356
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020118

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 360
    :goto_0
    return-void

    .line 358
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_0
.end method

.method protected setTextAirView(Landroid/view/View;Landroid/widget/TextView;Landroid/widget/TextView;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;
    .param p2, "text1"    # Landroid/widget/TextView;
    .param p3, "text2"    # Landroid/widget/TextView;

    .prologue
    .line 238
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/musicplus/util/UiUtils;->checkHoverUI(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 239
    invoke-virtual {p0, p1, p2, p3}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->newTextAirView(Landroid/view/View;Landroid/widget/TextView;Landroid/widget/TextView;)V

    .line 241
    :cond_0
    return-void
.end method

.method protected setTheme(I)V
    .locals 0
    .param p1, "theme"    # I

    .prologue
    .line 234
    iput p1, p0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->mTheme:I

    .line 235
    return-void
.end method

.method public setViewType(I)V
    .locals 0
    .param p1, "viewType"    # I

    .prologue
    .line 99
    iput p1, p0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->mViewType:I

    .line 100
    return-void
.end method
