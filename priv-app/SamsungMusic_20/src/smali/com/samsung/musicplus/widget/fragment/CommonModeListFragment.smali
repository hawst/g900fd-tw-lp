.class public Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;
.super Lcom/samsung/musicplus/widget/fragment/CommonListFragment;
.source "CommonModeListFragment.java"


# static fields
.field public static final CLOSE_LOADING_POPUP:I = 0x1

.field public static final HEADER:Ljava/lang/String; = "header"

.field public static final MODE:Ljava/lang/String; = "mode"

.field public static final SHOW_DELETING_POPUP:I = 0x2

.field public static final SHOW_LOADING_POPUP:I


# instance fields
.field private mActivity:Landroid/app/Activity;

.field private mLoadingProgress:Landroid/app/ProgressDialog;

.field private final mLoadingProgressBarHandler:Landroid/os/Handler;

.field protected mMode:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;-><init>()V

    .line 49
    new-instance v0, Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment$1;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment$1;-><init>(Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;)V

    iput-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;->mLoadingProgressBarHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;)Landroid/app/ProgressDialog;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;

    .prologue
    .line 15
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;->mLoadingProgress:Landroid/app/ProgressDialog;

    return-object v0
.end method

.method static synthetic access$002(Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;
    .param p1, "x1"    # Landroid/app/ProgressDialog;

    .prologue
    .line 15
    iput-object p1, p0, Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;->mLoadingProgress:Landroid/app/ProgressDialog;

    return-object p1
.end method

.method static synthetic access$100(Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;)Landroid/app/Activity;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;

    .prologue
    .line 15
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;->mActivity:Landroid/app/Activity;

    return-object v0
.end method


# virtual methods
.method public hideLoading()V
    .locals 2

    .prologue
    .line 42
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;->mLoadingProgressBarHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 43
    return-void
.end method

.method public hideLoading(I)V
    .locals 4
    .param p1, "delay"    # I

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;->mLoadingProgressBarHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    int-to-long v2, p1

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 47
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 101
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 103
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;->getAbsListView()Landroid/widget/AbsListView;

    move-result-object v0

    .line 104
    .local v0, "lv":Landroid/widget/AbsListView;
    if-eqz v0, :cond_0

    .line 105
    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;->unregisterForContextMenu(Landroid/view/View;)V

    .line 107
    :cond_0
    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 88
    iput-object p1, p0, Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;->mActivity:Landroid/app/Activity;

    .line 89
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->onAttach(Landroid/app/Activity;)V

    .line 90
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 94
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 95
    .local v0, "data":Landroid/os/Bundle;
    const-string v1, "mode"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;->mMode:I

    .line 96
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 97
    return-void
.end method

.method public onDestroy()V
    .locals 4

    .prologue
    .line 111
    iget-object v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;->mLoadingProgressBarHandler:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 112
    iget-object v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;->mLoadingProgress:Landroid/app/ProgressDialog;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;->mLoadingProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 114
    :try_start_0
    iget-object v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;->mLoadingProgress:Landroid/app/ProgressDialog;

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 123
    :cond_0
    :goto_0
    invoke-super {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->onDestroy()V

    .line 124
    return-void

    .line 115
    :catch_0
    move-exception v0

    .line 119
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v1, "MusicUiList"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "mLoadingProgressBarHandler "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public showDeleting()V
    .locals 2

    .prologue
    .line 38
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;->mLoadingProgressBarHandler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 39
    return-void
.end method

.method public showLoading()V
    .locals 2

    .prologue
    .line 34
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;->mLoadingProgressBarHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 35
    return-void
.end method
