.class public abstract Lcom/samsung/musicplus/widget/fragment/TwListFragment;
.super Lcom/samsung/musicplus/MusicBaseFragment;
.source "TwListFragment.java"

# interfaces
.implements Lcom/samsung/musicplus/widget/fragment/OnMusicFragmentInterface;


# static fields
.field public static final MUSIC_GRID_VIEW:I = 0x1

.field public static final MUSIC_LIST_VIEW:I


# instance fields
.field private mAdapter:Landroid/widget/ListAdapter;

.field private mEmptyContentViewStub:Landroid/view/View;

.field private mEmptyViewStub:Landroid/view/View;

.field private final mHandler:Landroid/os/Handler;

.field private final mInvalidator:Ljava/lang/Runnable;

.field private mListContainer:Landroid/view/View;

.field private mListShown:Z

.field private mListView:Lcom/sec/android/touchwiz/widget/TwAbsListView;

.field private mNoItemBackgroundView:Landroid/widget/ImageView;

.field private mNoItemImageView:Landroid/widget/ImageView;

.field private mNoItemTextView:Landroid/widget/TextView;

.field private final mOnClickListener:Lcom/sec/android/touchwiz/widget/TwAdapterView$OnItemClickListener;

.field private final mOnLongClickListener:Lcom/sec/android/touchwiz/widget/TwAdapterView$OnItemLongClickListener;

.field private mProgressContainer:Landroid/view/View;

.field private final mRequestFocus:Ljava/lang/Runnable;

.field private mViewType:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 93
    invoke-direct {p0}, Lcom/samsung/musicplus/MusicBaseFragment;-><init>()V

    .line 49
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mHandler:Landroid/os/Handler;

    .line 51
    new-instance v0, Lcom/samsung/musicplus/widget/fragment/TwListFragment$1;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/widget/fragment/TwListFragment$1;-><init>(Lcom/samsung/musicplus/widget/fragment/TwListFragment;)V

    iput-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mRequestFocus:Ljava/lang/Runnable;

    .line 58
    new-instance v0, Lcom/samsung/musicplus/widget/fragment/TwListFragment$2;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/widget/fragment/TwListFragment$2;-><init>(Lcom/samsung/musicplus/widget/fragment/TwListFragment;)V

    iput-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mOnClickListener:Lcom/sec/android/touchwiz/widget/TwAdapterView$OnItemClickListener;

    .line 65
    new-instance v0, Lcom/samsung/musicplus/widget/fragment/TwListFragment$3;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/widget/fragment/TwListFragment$3;-><init>(Lcom/samsung/musicplus/widget/fragment/TwListFragment;)V

    iput-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mOnLongClickListener:Lcom/sec/android/touchwiz/widget/TwAdapterView$OnItemLongClickListener;

    .line 430
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mViewType:I

    .line 518
    new-instance v0, Lcom/samsung/musicplus/widget/fragment/TwListFragment$4;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/widget/fragment/TwListFragment$4;-><init>(Lcom/samsung/musicplus/widget/fragment/TwListFragment;)V

    iput-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mInvalidator:Ljava/lang/Runnable;

    .line 94
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/widget/fragment/TwListFragment;)Lcom/sec/android/touchwiz/widget/TwAbsListView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/fragment/TwListFragment;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwAbsListView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/musicplus/widget/fragment/TwListFragment;)Landroid/widget/ListAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/fragment/TwListFragment;

    .prologue
    .line 47
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mAdapter:Landroid/widget/ListAdapter;

    return-object v0
.end method

.method private ensureGrid()V
    .locals 5

    .prologue
    .line 400
    iget-object v3, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwAbsListView;

    instance-of v3, v3, Lcom/sec/android/touchwiz/widget/TwGridView;

    if-eqz v3, :cond_0

    .line 424
    :goto_0
    return-void

    .line 404
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->getView()Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    .line 405
    .local v2, "root":Landroid/view/ViewGroup;
    if-nez v2, :cond_1

    .line 406
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "Content view not yet created"

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 409
    :cond_1
    const v3, 0x7f0d008d

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 410
    .local v1, "rawGridViewStub":Landroid/view/View;
    instance-of v3, v1, Landroid/view/ViewStub;

    if-eqz v3, :cond_2

    .line 411
    check-cast v1, Landroid/view/ViewStub;

    .end local v1    # "rawGridViewStub":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 414
    :cond_2
    const v3, 0x7f0d00a7

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 415
    .local v0, "rawGridView":Landroid/view/View;
    instance-of v3, v0, Lcom/sec/android/touchwiz/widget/TwGridView;

    if-nez v3, :cond_3

    .line 416
    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "Content has view with id attribute \'R.id.grid\' that is not a MusicGridView class"

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 420
    :cond_3
    check-cast v0, Lcom/sec/android/touchwiz/widget/TwAbsListView;

    .end local v0    # "rawGridView":Landroid/view/View;
    iput-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwAbsListView;

    .line 421
    iget-object v3, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwAbsListView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/sec/android/touchwiz/widget/TwAbsListView;->setEnableDragBlock(Z)V

    .line 422
    iget-object v3, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwAbsListView;

    iget-object v4, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mOnClickListener:Lcom/sec/android/touchwiz/widget/TwAdapterView$OnItemClickListener;

    invoke-virtual {v3, v4}, Lcom/sec/android/touchwiz/widget/TwAbsListView;->setOnItemClickListener(Lcom/sec/android/touchwiz/widget/TwAdapterView$OnItemClickListener;)V

    .line 423
    iget-object v3, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwAbsListView;

    iget-object v4, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mOnLongClickListener:Lcom/sec/android/touchwiz/widget/TwAdapterView$OnItemLongClickListener;

    invoke-virtual {v3, v4}, Lcom/sec/android/touchwiz/widget/TwAbsListView;->setOnItemLongClickListener(Lcom/sec/android/touchwiz/widget/TwAdapterView$OnItemLongClickListener;)V

    goto :goto_0
.end method

.method private ensureList()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 367
    iget-object v2, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwAbsListView;

    instance-of v2, v2, Lcom/sec/android/touchwiz/widget/TwListView;

    if-eqz v2, :cond_0

    .line 397
    :goto_0
    return-void

    .line 371
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->getView()Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 372
    .local v1, "root":Landroid/view/ViewGroup;
    if-nez v1, :cond_1

    .line 373
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Content view not yet created"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 376
    :cond_1
    const v2, 0x7f0d0089

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mProgressContainer:Landroid/view/View;

    .line 377
    const v2, 0x7f0d008b

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mListContainer:Landroid/view/View;

    .line 378
    const v2, 0x7f0d008c

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 379
    .local v0, "rawListView":Landroid/view/View;
    instance-of v2, v0, Lcom/sec/android/touchwiz/widget/TwListView;

    if-nez v2, :cond_2

    .line 380
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Content has view with id attribute \'R.id.list\' that is not a MusicListView class"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 384
    :cond_2
    check-cast v0, Lcom/sec/android/touchwiz/widget/TwAbsListView;

    .end local v0    # "rawListView":Landroid/view/View;
    iput-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwAbsListView;

    .line 385
    iget-object v2, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwAbsListView;

    invoke-virtual {v2, v4}, Lcom/sec/android/touchwiz/widget/TwAbsListView;->setEnableDragBlock(Z)V

    .line 386
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mListShown:Z

    .line 387
    iget-object v2, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwAbsListView;

    iget-object v3, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mOnClickListener:Lcom/sec/android/touchwiz/widget/TwAdapterView$OnItemClickListener;

    invoke-virtual {v2, v3}, Lcom/sec/android/touchwiz/widget/TwAbsListView;->setOnItemClickListener(Lcom/sec/android/touchwiz/widget/TwAdapterView$OnItemClickListener;)V

    .line 388
    iget-object v2, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwAbsListView;

    iget-object v3, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mOnLongClickListener:Lcom/sec/android/touchwiz/widget/TwAdapterView$OnItemLongClickListener;

    invoke-virtual {v2, v3}, Lcom/sec/android/touchwiz/widget/TwAbsListView;->setOnItemLongClickListener(Lcom/sec/android/touchwiz/widget/TwAdapterView$OnItemLongClickListener;)V

    .line 389
    iget-object v2, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mAdapter:Landroid/widget/ListAdapter;

    if-nez v2, :cond_3

    .line 392
    iget-object v2, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mProgressContainer:Landroid/view/View;

    if-eqz v2, :cond_3

    .line 393
    invoke-direct {p0, v4, v4}, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->setListShown(ZZ)V

    .line 396
    :cond_3
    iget-object v2, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mRequestFocus:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method private setEmptyViewIcon()V
    .locals 2

    .prologue
    .line 263
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0d0048

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mNoItemImageView:Landroid/widget/ImageView;

    .line 264
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mNoItemImageView:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 265
    return-void
.end method

.method private setListShown(ZZ)V
    .locals 6
    .param p1, "shown"    # Z
    .param p2, "animate"    # Z

    .prologue
    const v5, 0x7f050005

    const v4, 0x7f050004

    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 319
    iget-boolean v0, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mListShown:Z

    if-ne v0, p1, :cond_0

    .line 352
    :goto_0
    return-void

    .line 323
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mProgressContainer:Landroid/view/View;

    if-nez v0, :cond_1

    .line 324
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t be used with a custom content view"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 326
    :cond_1
    iput-boolean p1, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mListShown:Z

    .line 327
    if-eqz p1, :cond_3

    .line 328
    if-eqz p2, :cond_2

    .line 329
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mProgressContainer:Landroid/view/View;

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1, v5}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 331
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mListContainer:Landroid/view/View;

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1, v4}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 337
    :goto_1
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mProgressContainer:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 338
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mListContainer:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 334
    :cond_2
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mProgressContainer:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 335
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mListContainer:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    goto :goto_1

    .line 340
    :cond_3
    if-eqz p2, :cond_4

    .line 341
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mProgressContainer:Landroid/view/View;

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1, v4}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 343
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mListContainer:Landroid/view/View;

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1, v5}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 349
    :goto_2
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mProgressContainer:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 350
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mListContainer:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 346
    :cond_4
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mProgressContainer:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 347
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mListContainer:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    goto :goto_2
.end method


# virtual methods
.method public getAbsListView()Lcom/sec/android/touchwiz/widget/TwAbsListView;
    .locals 1

    .prologue
    .line 471
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwAbsListView;

    return-object v0
.end method

.method public getListAdapter()Landroid/widget/ListAdapter;
    .locals 1

    .prologue
    .line 358
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mAdapter:Landroid/widget/ListAdapter;

    return-object v0
.end method

.method public getListContainer()Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 475
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mListContainer:Landroid/view/View;

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method

.method public getListType()I
    .locals 1

    .prologue
    .line 539
    const/4 v0, 0x0

    return v0
.end method

.method public getListView()Lcom/sec/android/touchwiz/widget/TwListView;
    .locals 1

    .prologue
    .line 486
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->ensureList()V

    .line 487
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwAbsListView;

    check-cast v0, Lcom/sec/android/touchwiz/widget/TwListView;

    return-object v0
.end method

.method public getProgressContainer()Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 479
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mProgressContainer:Landroid/view/View;

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method

.method public getSelectedItemId()J
    .locals 2

    .prologue
    .line 207
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->ensureList()V

    .line 208
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwAbsListView;

    invoke-virtual {v0}, Lcom/sec/android/touchwiz/widget/TwAbsListView;->getSelectedItemId()J

    move-result-wide v0

    return-wide v0
.end method

.method public getSelectedItemPosition()I
    .locals 1

    .prologue
    .line 199
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->ensureList()V

    .line 200
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwAbsListView;

    invoke-virtual {v0}, Lcom/sec/android/touchwiz/widget/TwAbsListView;->getSelectedItemPosition()I

    move-result v0

    return v0
.end method

.method public getViewCount()I
    .locals 1

    .prologue
    .line 462
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwAbsListView;

    invoke-virtual {v0}, Lcom/sec/android/touchwiz/widget/TwAbsListView;->getCount()I

    move-result v0

    return v0
.end method

.method public getViewType()I
    .locals 1

    .prologue
    .line 458
    iget v0, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mViewType:I

    return v0
.end method

.method protected invalidateAllViews()V
    .locals 4

    .prologue
    .line 504
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mAdapter:Landroid/widget/ListAdapter;

    instance-of v0, v0, Lcom/samsung/musicplus/widget/list/TrackListAdapter;

    if-eqz v0, :cond_0

    .line 505
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwAbsListView;

    if-eqz v0, :cond_0

    .line 508
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwAbsListView;

    iget-object v1, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mInvalidator:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/sec/android/touchwiz/widget/TwAbsListView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 509
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwAbsListView;

    iget-object v1, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mInvalidator:Ljava/lang/Runnable;

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/touchwiz/widget/TwAbsListView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 512
    :cond_0
    return-void
.end method

.method public isEmptyList()Z
    .locals 1

    .prologue
    .line 544
    const/4 v0, 0x0

    return v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 98
    const v0, 0x7f04001c

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDestroyView()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 124
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mRequestFocus:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 125
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwAbsListView;

    if-eqz v0, :cond_0

    .line 126
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwAbsListView;

    invoke-virtual {v0, v2}, Lcom/sec/android/touchwiz/widget/TwAbsListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 128
    :cond_0
    iput-object v2, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwAbsListView;

    .line 129
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mListShown:Z

    .line 130
    iput-object v2, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mListContainer:Landroid/view/View;

    iput-object v2, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mProgressContainer:Landroid/view/View;

    .line 131
    iput-object v2, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mEmptyViewStub:Landroid/view/View;

    .line 132
    iput-object v2, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mEmptyContentViewStub:Landroid/view/View;

    .line 133
    iput-object v2, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mNoItemImageView:Landroid/widget/ImageView;

    .line 134
    iput-object v2, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mNoItemTextView:Landroid/widget/TextView;

    .line 135
    iput-object v2, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mNoItemBackgroundView:Landroid/widget/ImageView;

    .line 136
    invoke-super {p0}, Lcom/samsung/musicplus/MusicBaseFragment;->onDestroyView()V

    .line 137
    return-void
.end method

.method public onFragmentWindowFocusChanged(Z)V
    .locals 0
    .param p1, "hasFocus"    # Z

    .prologue
    .line 535
    return-void
.end method

.method public onListItemClick(Lcom/sec/android/touchwiz/widget/TwAbsListView;Landroid/view/View;IJ)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/touchwiz/widget/TwAbsListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    .line 151
    return-void
.end method

.method public onListItemLongClick(Lcom/sec/android/touchwiz/widget/TwAbsListView;Landroid/view/View;IJ)Z
    .locals 1
    .param p1, "l"    # Lcom/sec/android/touchwiz/widget/TwAbsListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    .line 164
    const/4 v0, 0x0

    return v0
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mAdapter:Landroid/widget/ListAdapter;

    instance-of v0, v0, Lcom/samsung/musicplus/widget/list/TrackListAdapter;

    if-eqz v0, :cond_0

    .line 114
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mAdapter:Landroid/widget/ListAdapter;

    check-cast v0, Lcom/samsung/musicplus/widget/list/TrackListAdapter;

    invoke-virtual {v0}, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->stopUpdateView()V

    .line 116
    :cond_0
    invoke-super {p0}, Lcom/samsung/musicplus/MusicBaseFragment;->onStop()V

    .line 117
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 106
    invoke-super {p0, p1, p2}, Lcom/samsung/musicplus/MusicBaseFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 107
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->ensureList()V

    .line 108
    return-void
.end method

.method public receivePlayerState(Ljava/lang/String;)V
    .locals 1
    .param p1, "action"    # Ljava/lang/String;

    .prologue
    .line 492
    const-string v0, "com.android.music.playstatechanged"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/musicplus/util/UiUtils;->isTalkBackEnabled(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 498
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->invalidateAllViews()V

    .line 501
    :cond_1
    return-void
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .locals 4
    .param p1, "adapter"    # Landroid/widget/ListAdapter;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 171
    iget-object v3, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mAdapter:Landroid/widget/ListAdapter;

    if-eqz v3, :cond_3

    move v0, v1

    .line 172
    .local v0, "hadAdapter":Z
    :goto_0
    iput-object p1, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mAdapter:Landroid/widget/ListAdapter;

    .line 173
    iget-object v3, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwAbsListView;

    if-eqz v3, :cond_2

    .line 174
    iget-object v3, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwAbsListView;

    invoke-virtual {v3, p1}, Lcom/sec/android/touchwiz/widget/TwAbsListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 175
    iget-boolean v3, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mListShown:Z

    if-nez v3, :cond_1

    if-nez v0, :cond_1

    .line 178
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->getView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v3

    if-eqz v3, :cond_0

    move v2, v1

    :cond_0
    invoke-direct {p0, v1, v2}, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->setListShown(ZZ)V

    .line 180
    :cond_1
    iget-object v1, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwAbsListView;

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->registerForContextMenu(Landroid/view/View;)V

    .line 182
    :cond_2
    return-void

    .end local v0    # "hadAdapter":Z
    :cond_3
    move v0, v2

    .line 171
    goto :goto_0
.end method

.method protected setEmptyView(I)V
    .locals 3
    .param p1, "noItemTextId"    # I

    .prologue
    .line 217
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->getView()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f0d0098

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mEmptyContentViewStub:Landroid/view/View;

    .line 218
    iget-object v1, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mEmptyContentViewStub:Landroid/view/View;

    instance-of v1, v1, Landroid/view/ViewStub;

    if-eqz v1, :cond_0

    .line 219
    iget-object v1, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mEmptyContentViewStub:Landroid/view/View;

    check-cast v1, Landroid/view/ViewStub;

    invoke-virtual {v1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 222
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 223
    .local v0, "noItemText":Ljava/lang/CharSequence;
    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->setEmptyViewTextOnly(Ljava/lang/CharSequence;)V

    .line 224
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->setEmptyViewNoBackground()V

    .line 225
    return-void
.end method

.method protected setEmptyView(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1, "noItemText"    # Ljava/lang/CharSequence;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 234
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->setEmptyViewText(Ljava/lang/CharSequence;)V

    .line 235
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->setEmptyViewIcon()V

    .line 236
    sget-boolean v0, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_MASS_PROJECT:Z

    if-eqz v0, :cond_0

    .line 237
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->setEmptyViewNoBackground()V

    .line 239
    :cond_0
    return-void
.end method

.method protected setEmptyViewNoBackground()V
    .locals 2

    .prologue
    .line 268
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0d0100

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mNoItemBackgroundView:Landroid/widget/ImageView;

    .line 269
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mNoItemBackgroundView:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 270
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mNoItemBackgroundView:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 272
    :cond_0
    return-void
.end method

.method protected setEmptyViewText(Ljava/lang/CharSequence;)V
    .locals 3
    .param p1, "noItemText"    # Ljava/lang/CharSequence;

    .prologue
    .line 249
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->getView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 250
    .local v0, "root":Landroid/view/ViewGroup;
    iget-object v1, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mEmptyViewStub:Landroid/view/View;

    if-nez v1, :cond_0

    .line 251
    const v1, 0x7f0d008e

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mEmptyViewStub:Landroid/view/View;

    .line 252
    iget-object v1, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mEmptyViewStub:Landroid/view/View;

    instance-of v1, v1, Landroid/view/ViewStub;

    if-eqz v1, :cond_0

    .line 253
    iget-object v1, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mEmptyViewStub:Landroid/view/View;

    check-cast v1, Landroid/view/ViewStub;

    invoke-virtual {v1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 257
    :cond_0
    iget-object v1, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwAbsListView;

    iget-object v2, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mEmptyViewStub:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/sec/android/touchwiz/widget/TwAbsListView;->setEmptyView(Landroid/view/View;)V

    .line 258
    const v1, 0x7f0d0049

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mNoItemTextView:Landroid/widget/TextView;

    .line 259
    iget-object v1, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mNoItemTextView:Landroid/widget/TextView;

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 260
    return-void
.end method

.method protected setEmptyViewTextOnly(Ljava/lang/CharSequence;)V
    .locals 2
    .param p1, "noItemText"    # Ljava/lang/CharSequence;

    .prologue
    .line 275
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mNoItemImageView:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 276
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mNoItemImageView:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 278
    :cond_0
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->setEmptyViewText(Ljava/lang/CharSequence;)V

    .line 279
    return-void
.end method

.method public setListShown(Z)V
    .locals 1
    .param p1, "shown"    # Z

    .prologue
    .line 297
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->setListShown(ZZ)V

    .line 298
    return-void
.end method

.method public setListShownNoAnimation(Z)V
    .locals 1
    .param p1, "shown"    # Z

    .prologue
    .line 305
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->setListShown(ZZ)V

    .line 306
    return-void
.end method

.method public setSelection(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 191
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->ensureList()V

    .line 192
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwAbsListView;

    invoke-virtual {v0, p1}, Lcom/sec/android/touchwiz/widget/TwAbsListView;->setSelection(I)V

    .line 193
    return-void
.end method

.method protected setViewType(I)V
    .locals 4
    .param p1, "viewType"    # I

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 438
    iput p1, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mViewType:I

    .line 439
    iget v0, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mViewType:I

    if-nez v0, :cond_1

    .line 440
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwAbsListView;

    instance-of v0, v0, Lcom/sec/android/touchwiz/widget/TwGridView;

    if-eqz v0, :cond_0

    .line 441
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwAbsListView;

    invoke-virtual {v0, v2}, Lcom/sec/android/touchwiz/widget/TwAbsListView;->setVisibility(I)V

    .line 442
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwAbsListView;

    invoke-virtual {v0, v3}, Lcom/sec/android/touchwiz/widget/TwAbsListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 444
    :cond_0
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->ensureList()V

    .line 445
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwAbsListView;

    invoke-virtual {v0, v1}, Lcom/sec/android/touchwiz/widget/TwAbsListView;->setVisibility(I)V

    .line 454
    :goto_0
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mRequestFocus:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 455
    return-void

    .line 447
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwAbsListView;

    instance-of v0, v0, Lcom/sec/android/touchwiz/widget/TwListView;

    if-eqz v0, :cond_2

    .line 448
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwAbsListView;

    invoke-virtual {v0, v2}, Lcom/sec/android/touchwiz/widget/TwAbsListView;->setVisibility(I)V

    .line 449
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwAbsListView;

    invoke-virtual {v0, v3}, Lcom/sec/android/touchwiz/widget/TwAbsListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 451
    :cond_2
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->ensureGrid()V

    .line 452
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->mListView:Lcom/sec/android/touchwiz/widget/TwAbsListView;

    invoke-virtual {v0, v1}, Lcom/sec/android/touchwiz/widget/TwAbsListView;->setVisibility(I)V

    goto :goto_0
.end method
