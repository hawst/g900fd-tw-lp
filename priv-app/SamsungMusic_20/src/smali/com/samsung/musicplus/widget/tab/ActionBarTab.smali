.class public Lcom/samsung/musicplus/widget/tab/ActionBarTab;
.super Lcom/samsung/musicplus/widget/tab/AbsTabController;
.source "ActionBarTab.java"

# interfaces
.implements Landroid/app/ActionBar$TabListener;


# static fields
.field private static final SUB_ACTIONBAR_DIM_ALPHA:F = 0.35f


# instance fields
.field private mActionBar:Landroid/app/ActionBar;

.field private mActionBarSubBackground:Landroid/graphics/drawable/Drawable;

.field private mActionBarSubDimBackground:Landroid/graphics/drawable/Drawable;

.field private mContext:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/app/FragmentManager;Landroid/app/ActionBar;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "fm"    # Landroid/app/FragmentManager;
    .param p3, "bar"    # Landroid/app/ActionBar;

    .prologue
    const/4 v0, 0x0

    .line 38
    invoke-direct {p0, p2}, Lcom/samsung/musicplus/widget/tab/AbsTabController;-><init>(Landroid/app/FragmentManager;)V

    .line 33
    iput-object v0, p0, Lcom/samsung/musicplus/widget/tab/ActionBarTab;->mActionBarSubBackground:Landroid/graphics/drawable/Drawable;

    .line 35
    iput-object v0, p0, Lcom/samsung/musicplus/widget/tab/ActionBarTab;->mActionBarSubDimBackground:Landroid/graphics/drawable/Drawable;

    .line 39
    iput-object p1, p0, Lcom/samsung/musicplus/widget/tab/ActionBarTab;->mContext:Landroid/content/Context;

    .line 40
    iput-object p3, p0, Lcom/samsung/musicplus/widget/tab/ActionBarTab;->mActionBar:Landroid/app/ActionBar;

    .line 42
    new-instance v0, Lcom/samsung/musicplus/widget/tab/ActionBarTabTalkBack;

    iget-object v1, p0, Lcom/samsung/musicplus/widget/tab/ActionBarTab;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/musicplus/widget/tab/ActionBarTab;->mActionBar:Landroid/app/ActionBar;

    invoke-direct {v0, v1, v2}, Lcom/samsung/musicplus/widget/tab/ActionBarTabTalkBack;-><init>(Landroid/content/Context;Landroid/app/ActionBar;)V

    iput-object v0, p0, Lcom/samsung/musicplus/widget/tab/ActionBarTab;->mTalkBack:Lcom/samsung/musicplus/widget/tab/AbsTabTalkBack;

    .line 43
    iget-object v0, p0, Lcom/samsung/musicplus/widget/tab/ActionBarTab;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0200f6

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/widget/tab/ActionBarTab;->mActionBarSubBackground:Landroid/graphics/drawable/Drawable;

    .line 44
    iget-object v0, p0, Lcom/samsung/musicplus/widget/tab/ActionBarTab;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0200f7

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/widget/tab/ActionBarTab;->mActionBarSubDimBackground:Landroid/graphics/drawable/Drawable;

    .line 45
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/widget/tab/ActionBarTab;->setEnableTab(Z)V

    .line 46
    return-void
.end method

.method private setHasEmbeddedTabs(Z)V
    .locals 7
    .param p1, "hasEmbeddedTabs"    # Z

    .prologue
    .line 189
    :try_start_0
    iget-object v2, p0, Lcom/samsung/musicplus/widget/tab/ActionBarTab;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    const-string v3, "setHasEmbeddedTabs"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Class;

    const/4 v5, 0x0

    sget-object v6, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    .line 191
    .local v1, "setHasEmbeddedTabs":Ljava/lang/reflect/Method;
    if-eqz v1, :cond_0

    .line 192
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 193
    iget-object v2, p0, Lcom/samsung/musicplus/widget/tab/ActionBarTab;->mActionBar:Landroid/app/ActionBar;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_3

    .line 204
    .end local v1    # "setHasEmbeddedTabs":Ljava/lang/reflect/Method;
    :cond_0
    :goto_0
    return-void

    .line 195
    :catch_0
    move-exception v0

    .line 196
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    const-string v2, "MusicTab"

    const-string v3, "setHasEmbeddedTabs() - NoSuchMethodException"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 197
    .end local v0    # "e":Ljava/lang/NoSuchMethodException;
    :catch_1
    move-exception v0

    .line 198
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v2, "MusicTab"

    const-string v3, "setHasEmbeddedTabs() - IllegalArgumentException"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 199
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v0

    .line 200
    .local v0, "e":Ljava/lang/IllegalAccessException;
    const-string v2, "MusicTab"

    const-string v3, "setHasEmbeddedTabs() - IllegalAccessException"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 201
    .end local v0    # "e":Ljava/lang/IllegalAccessException;
    :catch_3
    move-exception v0

    .line 202
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    const-string v2, "MusicTab"

    const-string v3, "setHasEmbeddedTabs() - InvocationTargetException"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method protected addTabInternal(I)V
    .locals 2
    .param p1, "tabId"    # I

    .prologue
    .line 56
    iget-object v1, p0, Lcom/samsung/musicplus/widget/tab/ActionBarTab;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v1}, Landroid/app/ActionBar;->newTab()Landroid/app/ActionBar$Tab;

    move-result-object v0

    .line 57
    .local v0, "tab":Landroid/app/ActionBar$Tab;
    invoke-static {p1}, Lcom/samsung/musicplus/util/ListUtils;->getDefaultTabName(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/ActionBar$Tab;->setText(I)Landroid/app/ActionBar$Tab;

    .line 58
    invoke-virtual {v0, p0}, Landroid/app/ActionBar$Tab;->setTabListener(Landroid/app/ActionBar$TabListener;)Landroid/app/ActionBar$Tab;

    .line 59
    iget-object v1, p0, Lcom/samsung/musicplus/widget/tab/ActionBarTab;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v1, v0}, Landroid/app/ActionBar;->addTab(Landroid/app/ActionBar$Tab;)V

    .line 60
    return-void
.end method

.method public getSelectedTabPosition()I
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/samsung/musicplus/widget/tab/ActionBarTab;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->getSelectedNavigationIndex()I

    move-result v0

    return v0
.end method

.method protected initTab()V
    .locals 1

    .prologue
    .line 50
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/widget/tab/ActionBarTab;->setHasEmbeddedTabs(Z)V

    .line 51
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/tab/ActionBarTab;->setStackedTabMaxWidth()V

    .line 52
    return-void
.end method

.method public onMultiWindowStateChanged(Z)V
    .locals 4
    .param p1, "isMultiWindow"    # Z

    .prologue
    .line 232
    iget-object v3, p0, Lcom/samsung/musicplus/widget/tab/ActionBarTab;->mPager:Landroid/support/v4/view/ViewPager;

    if-eqz v3, :cond_1

    .line 233
    iget-object v3, p0, Lcom/samsung/musicplus/widget/tab/ActionBarTab;->mFragments:Landroid/util/SparseArray;

    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    move-result v2

    .line 234
    .local v2, "size":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_2

    .line 235
    iget-object v3, p0, Lcom/samsung/musicplus/widget/tab/ActionBarTab;->mFragments:Landroid/util/SparseArray;

    invoke-virtual {v3, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    .line 236
    .local v1, "o":Ljava/lang/Object;
    instance-of v3, v1, Lcom/samsung/musicplus/widget/fragment/OnMusicFragmentInterface;

    if-eqz v3, :cond_0

    .line 237
    check-cast v1, Lcom/samsung/musicplus/widget/fragment/OnMusicFragmentInterface;

    .end local v1    # "o":Ljava/lang/Object;
    invoke-interface {v1, p1}, Lcom/samsung/musicplus/widget/fragment/OnMusicFragmentInterface;->onMultiWindowModeChanged(Z)V

    .line 234
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 241
    .end local v0    # "i":I
    .end local v2    # "size":I
    :cond_1
    iget-object v3, p0, Lcom/samsung/musicplus/widget/tab/ActionBarTab;->mCurrentContent:Ljava/lang/Object;

    instance-of v3, v3, Lcom/samsung/musicplus/widget/fragment/OnMusicFragmentInterface;

    if-eqz v3, :cond_2

    .line 242
    iget-object v3, p0, Lcom/samsung/musicplus/widget/tab/ActionBarTab;->mCurrentContent:Ljava/lang/Object;

    check-cast v3, Lcom/samsung/musicplus/widget/fragment/OnMusicFragmentInterface;

    invoke-interface {v3, p1}, Lcom/samsung/musicplus/widget/fragment/OnMusicFragmentInterface;->onMultiWindowModeChanged(Z)V

    .line 245
    :cond_2
    return-void
.end method

.method public onTabReselected(Landroid/app/ActionBar$Tab;Landroid/app/FragmentTransaction;)V
    .locals 0
    .param p1, "tab"    # Landroid/app/ActionBar$Tab;
    .param p2, "ft"    # Landroid/app/FragmentTransaction;

    .prologue
    .line 179
    return-void
.end method

.method public onTabSelected(Landroid/app/ActionBar$Tab;Landroid/app/FragmentTransaction;)V
    .locals 4
    .param p1, "tab"    # Landroid/app/ActionBar$Tab;
    .param p2, "ft"    # Landroid/app/FragmentTransaction;

    .prologue
    .line 132
    iget-boolean v2, p0, Lcom/samsung/musicplus/widget/tab/ActionBarTab;->mOnTabChangeEnabled:Z

    if-nez v2, :cond_0

    .line 159
    :goto_0
    return-void

    .line 135
    :cond_0
    invoke-virtual {p1}, Landroid/app/ActionBar$Tab;->getPosition()I

    move-result v1

    .line 136
    .local v1, "position":I
    iget-object v2, p0, Lcom/samsung/musicplus/widget/tab/ActionBarTab;->mPager:Landroid/support/v4/view/ViewPager;

    if-eqz v2, :cond_3

    .line 137
    iget-object v2, p0, Lcom/samsung/musicplus/widget/tab/ActionBarTab;->mCurrentContent:Ljava/lang/Object;

    instance-of v2, v2, Lcom/samsung/musicplus/widget/tab/OnTabListener;

    if-eqz v2, :cond_1

    .line 138
    iget-object v2, p0, Lcom/samsung/musicplus/widget/tab/ActionBarTab;->mCurrentContent:Ljava/lang/Object;

    check-cast v2, Lcom/samsung/musicplus/widget/tab/OnTabListener;

    invoke-interface {v2}, Lcom/samsung/musicplus/widget/tab/OnTabListener;->onTabUnselected()V

    .line 140
    :cond_1
    iget-object v2, p0, Lcom/samsung/musicplus/widget/tab/ActionBarTab;->mPager:Landroid/support/v4/view/ViewPager;

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Landroid/support/v4/view/ViewPager;->setCurrentItem(IZ)V

    .line 158
    :cond_2
    :goto_1
    iget-object v2, p0, Lcom/samsung/musicplus/widget/tab/ActionBarTab;->mTalkBack:Lcom/samsung/musicplus/widget/tab/AbsTabTalkBack;

    invoke-virtual {v2, p1}, Lcom/samsung/musicplus/widget/tab/AbsTabTalkBack;->getSelectedTabTts(Landroid/app/ActionBar$Tab;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Landroid/app/ActionBar$Tab;->setContentDescription(Ljava/lang/CharSequence;)Landroid/app/ActionBar$Tab;

    goto :goto_0

    .line 142
    :cond_3
    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/widget/tab/ActionBarTab;->getTabId(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/musicplus/widget/tab/ActionBarTab;->mCurrentTabId:Ljava/lang/Integer;

    .line 143
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/tab/ActionBarTab;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/musicplus/widget/tab/ActionBarTab;->mCurrentTabId:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 144
    .local v0, "fg":Landroid/app/Fragment;
    if-nez v0, :cond_4

    .line 145
    iget-object v2, p0, Lcom/samsung/musicplus/widget/tab/ActionBarTab;->mCurrentTabId:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/samsung/musicplus/contents/ContentsFragmentFactory;->getNewInstance(ILjava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 146
    const v2, 0x7f0d0104

    iget-object v3, p0, Lcom/samsung/musicplus/widget/tab/ActionBarTab;->mCurrentTabId:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v2, v0, v3}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 151
    :goto_2
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/app/Fragment;->setHasOptionsMenu(Z)V

    .line 152
    iput-object v0, p0, Lcom/samsung/musicplus/widget/tab/ActionBarTab;->mCurrentContent:Ljava/lang/Object;

    .line 154
    iget-object v2, p0, Lcom/samsung/musicplus/widget/tab/ActionBarTab;->mCurrentContent:Ljava/lang/Object;

    instance-of v2, v2, Lcom/samsung/musicplus/widget/tab/OnTabListener;

    if-eqz v2, :cond_2

    .line 155
    iget-object v2, p0, Lcom/samsung/musicplus/widget/tab/ActionBarTab;->mCurrentContent:Ljava/lang/Object;

    check-cast v2, Lcom/samsung/musicplus/widget/tab/OnTabListener;

    invoke-interface {v2}, Lcom/samsung/musicplus/widget/tab/OnTabListener;->onTabSelected()V

    goto :goto_1

    .line 148
    :cond_4
    invoke-virtual {p2, v0}, Landroid/app/FragmentTransaction;->attach(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    goto :goto_2
.end method

.method public onTabUnselected(Landroid/app/ActionBar$Tab;Landroid/app/FragmentTransaction;)V
    .locals 1
    .param p1, "tab"    # Landroid/app/ActionBar$Tab;
    .param p2, "ft"    # Landroid/app/FragmentTransaction;

    .prologue
    .line 163
    iget-object v0, p0, Lcom/samsung/musicplus/widget/tab/ActionBarTab;->mPager:Landroid/support/v4/view/ViewPager;

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/musicplus/widget/tab/ActionBarTab;->mOnTabChangeEnabled:Z

    if-nez v0, :cond_1

    .line 174
    :cond_0
    :goto_0
    return-void

    .line 166
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/widget/tab/ActionBarTab;->mCurrentContent:Ljava/lang/Object;

    instance-of v0, v0, Lcom/samsung/musicplus/widget/tab/OnTabListener;

    if-eqz v0, :cond_2

    .line 167
    iget-object v0, p0, Lcom/samsung/musicplus/widget/tab/ActionBarTab;->mCurrentContent:Ljava/lang/Object;

    check-cast v0, Lcom/samsung/musicplus/widget/tab/OnTabListener;

    invoke-interface {v0}, Lcom/samsung/musicplus/widget/tab/OnTabListener;->onTabUnselected()V

    .line 169
    :cond_2
    iget-object v0, p0, Lcom/samsung/musicplus/widget/tab/ActionBarTab;->mCurrentContent:Ljava/lang/Object;

    if-eqz v0, :cond_3

    .line 170
    iget-object v0, p0, Lcom/samsung/musicplus/widget/tab/ActionBarTab;->mCurrentContent:Ljava/lang/Object;

    check-cast v0, Landroid/app/Fragment;

    invoke-virtual {p2, v0}, Landroid/app/FragmentTransaction;->detach(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 173
    :cond_3
    iget-object v0, p0, Lcom/samsung/musicplus/widget/tab/ActionBarTab;->mTalkBack:Lcom/samsung/musicplus/widget/tab/AbsTabTalkBack;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/widget/tab/AbsTabTalkBack;->getUnselectedTabTts(Landroid/app/ActionBar$Tab;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/app/ActionBar$Tab;->setContentDescription(Ljava/lang/CharSequence;)Landroid/app/ActionBar$Tab;

    goto :goto_0
.end method

.method public removeAllTabs()V
    .locals 1

    .prologue
    .line 70
    invoke-super {p0}, Lcom/samsung/musicplus/widget/tab/AbsTabController;->removeAllTabs()V

    .line 71
    iget-object v0, p0, Lcom/samsung/musicplus/widget/tab/ActionBarTab;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->removeAllTabs()V

    .line 72
    return-void
.end method

.method public removeTab(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 64
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/tab/AbsTabController;->removeTab(I)V

    .line 65
    iget-object v0, p0, Lcom/samsung/musicplus/widget/tab/ActionBarTab;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, p1}, Landroid/app/ActionBar;->removeTabAt(I)V

    .line 66
    return-void
.end method

.method public setEnableTab(Z)V
    .locals 5
    .param p1, "enable"    # Z

    .prologue
    .line 108
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/tab/AbsTabController;->setEnableTab(Z)V

    .line 110
    :try_start_0
    iget-object v3, p0, Lcom/samsung/musicplus/widget/tab/ActionBarTab;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string v4, "mTabScrollView"

    invoke-virtual {v3, v4}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v2

    .line 111
    .local v2, "tabScrollViewFiled":Ljava/lang/reflect/Field;
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 112
    iget-object v3, p0, Lcom/samsung/musicplus/widget/tab/ActionBarTab;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v2, v3}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 113
    .local v1, "tabScrollView":Landroid/view/View;
    if-eqz v1, :cond_0

    .line 117
    invoke-virtual {v1, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 118
    if-eqz p1, :cond_1

    const/high16 v3, 0x3f800000    # 1.0f

    :goto_0
    invoke-virtual {v1, v3}, Landroid/view/View;->setAlpha(F)V
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2

    .line 128
    .end local v1    # "tabScrollView":Landroid/view/View;
    .end local v2    # "tabScrollViewFiled":Ljava/lang/reflect/Field;
    :cond_0
    :goto_1
    return-void

    .line 118
    .restart local v1    # "tabScrollView":Landroid/view/View;
    .restart local v2    # "tabScrollViewFiled":Ljava/lang/reflect/Field;
    :cond_1
    const v3, 0x3eb33333    # 0.35f

    goto :goto_0

    .line 121
    .end local v1    # "tabScrollView":Landroid/view/View;
    .end local v2    # "tabScrollViewFiled":Ljava/lang/reflect/Field;
    :catch_0
    move-exception v0

    .line 122
    .local v0, "e":Ljava/lang/NoSuchFieldException;
    const-string v3, "MusicTab"

    const-string v4, "setVisibleTab() - NoSuchFieldException"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 123
    .end local v0    # "e":Ljava/lang/NoSuchFieldException;
    :catch_1
    move-exception v0

    .line 124
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v3, "MusicTab"

    const-string v4, "setVisibleTab() - IllegalArgumentException"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 125
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v0

    .line 126
    .local v0, "e":Ljava/lang/IllegalAccessException;
    const-string v3, "MusicTab"

    const-string v4, "setVisibleTab() - IllegalAccessException"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public setStackedTabMaxWidth()V
    .locals 6

    .prologue
    .line 213
    :try_start_0
    iget-object v4, p0, Lcom/samsung/musicplus/widget/tab/ActionBarTab;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    const-string v5, "mTabScrollView"

    invoke-virtual {v4, v5}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v3

    .line 214
    .local v3, "tabScrollViewFiled":Ljava/lang/reflect/Field;
    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 215
    iget-object v4, p0, Lcom/samsung/musicplus/widget/tab/ActionBarTab;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v3, v4}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    .line 217
    .local v1, "scrollingTabContainerView":Ljava/lang/Object;
    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    const-string v5, "mStackedTabMaxWidth"

    invoke-virtual {v4, v5}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v2

    .line 219
    .local v2, "stackedTabMaxWidthFiled":Ljava/lang/reflect/Field;
    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 220
    const v4, 0x186a0

    invoke-virtual {v2, v1, v4}, Ljava/lang/reflect/Field;->setInt(Ljava/lang/Object;I)V
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2

    .line 228
    .end local v1    # "scrollingTabContainerView":Ljava/lang/Object;
    .end local v2    # "stackedTabMaxWidthFiled":Ljava/lang/reflect/Field;
    .end local v3    # "tabScrollViewFiled":Ljava/lang/reflect/Field;
    :goto_0
    return-void

    .line 221
    :catch_0
    move-exception v0

    .line 222
    .local v0, "e":Ljava/lang/NoSuchFieldException;
    const-string v4, "MusicTab"

    const-string v5, "setStackedTabMaxWidth() - NoSuchFieldException"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 223
    .end local v0    # "e":Ljava/lang/NoSuchFieldException;
    :catch_1
    move-exception v0

    .line 224
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v4, "MusicTab"

    const-string v5, "setStackedTabMaxWidth() - IllegalArgumentException"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 225
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v0

    .line 226
    .local v0, "e":Ljava/lang/IllegalAccessException;
    const-string v4, "MusicTab"

    const-string v5, "setStackedTabMaxWidth() - IllegalAccessException"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected setTabSelectedInternal(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 76
    iget-object v0, p0, Lcom/samsung/musicplus/widget/tab/ActionBarTab;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0}, Landroid/app/ActionBar;->getNavigationItemCount()I

    move-result v0

    if-ge v0, p1, :cond_0

    .line 80
    :goto_0
    return-void

    .line 79
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/widget/tab/ActionBarTab;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v0, p1}, Landroid/app/ActionBar;->setSelectedNavigationItem(I)V

    goto :goto_0
.end method

.method public setVisibleTab(Z)V
    .locals 5
    .param p1, "visible"    # Z

    .prologue
    .line 89
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/tab/AbsTabController;->setVisibleTab(Z)V

    .line 91
    :try_start_0
    iget-object v3, p0, Lcom/samsung/musicplus/widget/tab/ActionBarTab;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string v4, "mTabScrollView"

    invoke-virtual {v3, v4}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v2

    .line 92
    .local v2, "tabScrollViewFiled":Ljava/lang/reflect/Field;
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 93
    iget-object v3, p0, Lcom/samsung/musicplus/widget/tab/ActionBarTab;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v2, v3}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 94
    .local v1, "tabScrollView":Landroid/view/View;
    if-eqz v1, :cond_0

    .line 95
    if-eqz p1, :cond_1

    const/4 v3, 0x0

    :goto_0
    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2

    .line 104
    .end local v1    # "tabScrollView":Landroid/view/View;
    .end local v2    # "tabScrollViewFiled":Ljava/lang/reflect/Field;
    :cond_0
    :goto_1
    return-void

    .line 95
    .restart local v1    # "tabScrollView":Landroid/view/View;
    .restart local v2    # "tabScrollViewFiled":Ljava/lang/reflect/Field;
    :cond_1
    const/16 v3, 0x8

    goto :goto_0

    .line 97
    .end local v1    # "tabScrollView":Landroid/view/View;
    .end local v2    # "tabScrollViewFiled":Ljava/lang/reflect/Field;
    :catch_0
    move-exception v0

    .line 98
    .local v0, "e":Ljava/lang/NoSuchFieldException;
    const-string v3, "MusicTab"

    const-string v4, "setVisibleTab() - NoSuchFieldException"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 99
    .end local v0    # "e":Ljava/lang/NoSuchFieldException;
    :catch_1
    move-exception v0

    .line 100
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v3, "MusicTab"

    const-string v4, "setVisibleTab() - IllegalArgumentException"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 101
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v0

    .line 102
    .local v0, "e":Ljava/lang/IllegalAccessException;
    const-string v3, "MusicTab"

    const-string v4, "setVisibleTab() - IllegalAccessException"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method
