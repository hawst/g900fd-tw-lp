.class public Lcom/samsung/musicplus/widget/database/IndexView;
.super Ljava/lang/Object;
.source "IndexView.java"

# interfaces
.implements Landroid/widget/AbsListView$OnScrollListener;
.implements Lcom/sec/android/touchwiz/widget/TwLangIndexScrollView$OnIndexSelectedListener;


# static fields
.field public static final INDEX_VIEW_TAG:Ljava/lang/String; = "index"


# instance fields
.field private mAdapter:Landroid/widget/Adapter;

.field private mContext:Landroid/content/Context;

.field private mDefaultViewCount:I

.field private mExpandableAdapter:Landroid/widget/ExpandableListAdapter;

.field private mIndexCharArray:[Ljava/lang/String;

.field private mList:Landroid/widget/ListView;

.field private mScrollListener:Landroid/widget/AbsListView$OnScrollListener;

.field private mTwIndexScrollView:Lcom/sec/android/touchwiz/widget/TwLangIndexScrollView;

.field private mTwIndexer:Lcom/sec/android/touchwiz/widget/TwCursorIndexer;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 92
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/musicplus/widget/database/IndexView;->mDefaultViewCount:I

    .line 93
    iput-object p1, p0, Lcom/samsung/musicplus/widget/database/IndexView;->mContext:Landroid/content/Context;

    .line 94
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/widget/database/IndexView;->makeIndexScrollView(Landroid/content/Context;)Lcom/sec/android/touchwiz/widget/TwLangIndexScrollView;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/widget/database/IndexView;->mTwIndexScrollView:Lcom/sec/android/touchwiz/widget/TwLangIndexScrollView;

    .line 95
    iget-object v0, p0, Lcom/samsung/musicplus/widget/database/IndexView;->mTwIndexScrollView:Lcom/sec/android/touchwiz/widget/TwLangIndexScrollView;

    invoke-virtual {v0, p0}, Lcom/sec/android/touchwiz/widget/TwLangIndexScrollView;->setOnIndexSelectedListener(Lcom/sec/android/touchwiz/widget/TwLangIndexScrollView$OnIndexSelectedListener;)V

    .line 96
    return-void
.end method

.method private makeIndexScrollView(Landroid/content/Context;)Lcom/sec/android/touchwiz/widget/TwLangIndexScrollView;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 159
    new-instance v0, Lcom/sec/android/touchwiz/widget/TwLangIndexScrollView;

    invoke-direct {v0, p1}, Lcom/sec/android/touchwiz/widget/TwLangIndexScrollView;-><init>(Landroid/content/Context;)V

    .line 160
    .local v0, "sv":Lcom/sec/android/touchwiz/widget/TwLangIndexScrollView;
    invoke-static {}, Lcom/samsung/musicplus/util/UiUtils;->isWhiteTheme()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v0, v1}, Lcom/sec/android/touchwiz/widget/TwLangIndexScrollView;->setIndexScrollViewTheme(I)V

    .line 161
    const-string v1, "index"

    invoke-virtual {v0, v1}, Lcom/sec/android/touchwiz/widget/TwLangIndexScrollView;->setTag(Ljava/lang/Object;)V

    .line 162
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/sec/android/touchwiz/widget/TwLangIndexScrollView;->setSubscrollLimit(I)V

    .line 163
    invoke-virtual {v0, v2}, Lcom/sec/android/touchwiz/widget/TwLangIndexScrollView;->setVerticalScrollBarEnabled(Z)V

    .line 164
    return-object v0

    :cond_0
    move v1, v2

    .line 160
    goto :goto_0
.end method

.method private unregisterOldIndexer()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 49
    iget-object v0, p0, Lcom/samsung/musicplus/widget/database/IndexView;->mTwIndexer:Lcom/sec/android/touchwiz/widget/TwCursorIndexer;

    if-eqz v0, :cond_1

    .line 50
    iget-object v0, p0, Lcom/samsung/musicplus/widget/database/IndexView;->mAdapter:Landroid/widget/Adapter;

    if-eqz v0, :cond_0

    .line 51
    iget-object v0, p0, Lcom/samsung/musicplus/widget/database/IndexView;->mAdapter:Landroid/widget/Adapter;

    iget-object v1, p0, Lcom/samsung/musicplus/widget/database/IndexView;->mTwIndexer:Lcom/sec/android/touchwiz/widget/TwCursorIndexer;

    invoke-interface {v0, v1}, Landroid/widget/Adapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 53
    :cond_0
    iput-object v2, p0, Lcom/samsung/musicplus/widget/database/IndexView;->mTwIndexer:Lcom/sec/android/touchwiz/widget/TwCursorIndexer;

    .line 56
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/widget/database/IndexView;->mExpandableAdapter:Landroid/widget/ExpandableListAdapter;

    if-eqz v0, :cond_3

    .line 57
    iget-object v0, p0, Lcom/samsung/musicplus/widget/database/IndexView;->mTwIndexer:Lcom/sec/android/touchwiz/widget/TwCursorIndexer;

    if-eqz v0, :cond_3

    .line 58
    iget-object v0, p0, Lcom/samsung/musicplus/widget/database/IndexView;->mAdapter:Landroid/widget/Adapter;

    if-eqz v0, :cond_2

    .line 59
    iget-object v0, p0, Lcom/samsung/musicplus/widget/database/IndexView;->mAdapter:Landroid/widget/Adapter;

    iget-object v1, p0, Lcom/samsung/musicplus/widget/database/IndexView;->mTwIndexer:Lcom/sec/android/touchwiz/widget/TwCursorIndexer;

    invoke-interface {v0, v1}, Landroid/widget/Adapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 61
    :cond_2
    iput-object v2, p0, Lcom/samsung/musicplus/widget/database/IndexView;->mTwIndexer:Lcom/sec/android/touchwiz/widget/TwCursorIndexer;

    .line 64
    :cond_3
    return-void
.end method


# virtual methods
.method public getIndexScrollView()Landroid/view/View;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/samsung/musicplus/widget/database/IndexView;->mTwIndexScrollView:Lcom/sec/android/touchwiz/widget/TwLangIndexScrollView;

    return-object v0
.end method

.method public onIndexSelected(I)V
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 68
    iget-object v0, p0, Lcom/samsung/musicplus/widget/database/IndexView;->mList:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    .line 71
    iget-object v0, p0, Lcom/samsung/musicplus/widget/database/IndexView;->mList:Landroid/widget/ListView;

    iget v1, p0, Lcom/samsung/musicplus/widget/database/IndexView;->mDefaultViewCount:I

    add-int/2addr v1, p1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelection(I)V

    .line 73
    :cond_0
    return-void
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 1
    .param p1, "view"    # Landroid/widget/AbsListView;
    .param p2, "firstVisibleItem"    # I
    .param p3, "visibleItemCount"    # I
    .param p4, "totalItemCount"    # I

    .prologue
    .line 86
    iget-object v0, p0, Lcom/samsung/musicplus/widget/database/IndexView;->mTwIndexScrollView:Lcom/sec/android/touchwiz/widget/TwLangIndexScrollView;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/sec/android/touchwiz/widget/TwLangIndexScrollView;->onScroll(Landroid/widget/AbsListView;III)V

    .line 87
    iget-object v0, p0, Lcom/samsung/musicplus/widget/database/IndexView;->mScrollListener:Landroid/widget/AbsListView$OnScrollListener;

    if-eqz v0, :cond_0

    .line 88
    iget-object v0, p0, Lcom/samsung/musicplus/widget/database/IndexView;->mScrollListener:Landroid/widget/AbsListView$OnScrollListener;

    invoke-interface {v0, p1, p2, p3, p4}, Landroid/widget/AbsListView$OnScrollListener;->onScroll(Landroid/widget/AbsListView;III)V

    .line 90
    :cond_0
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 1
    .param p1, "view"    # Landroid/widget/AbsListView;
    .param p2, "scrollState"    # I

    .prologue
    .line 77
    iget-object v0, p0, Lcom/samsung/musicplus/widget/database/IndexView;->mTwIndexScrollView:Lcom/sec/android/touchwiz/widget/TwLangIndexScrollView;

    invoke-virtual {v0, p1, p2}, Lcom/sec/android/touchwiz/widget/TwLangIndexScrollView;->onScrollStateChanged(Landroid/widget/AbsListView;I)V

    .line 78
    iget-object v0, p0, Lcom/samsung/musicplus/widget/database/IndexView;->mScrollListener:Landroid/widget/AbsListView$OnScrollListener;

    if-eqz v0, :cond_0

    .line 79
    iget-object v0, p0, Lcom/samsung/musicplus/widget/database/IndexView;->mScrollListener:Landroid/widget/AbsListView$OnScrollListener;

    invoke-interface {v0, p1, p2}, Landroid/widget/AbsListView$OnScrollListener;->onScrollStateChanged(Landroid/widget/AbsListView;I)V

    .line 81
    :cond_0
    return-void
.end method

.method public setAdapter(Landroid/widget/Adapter;)V
    .locals 1
    .param p1, "a"    # Landroid/widget/Adapter;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 133
    iget-object v0, p0, Lcom/samsung/musicplus/widget/database/IndexView;->mAdapter:Landroid/widget/Adapter;

    if-eqz v0, :cond_0

    .line 134
    iget-object v0, p0, Lcom/samsung/musicplus/widget/database/IndexView;->mAdapter:Landroid/widget/Adapter;

    if-eq v0, p1, :cond_0

    .line 136
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/database/IndexView;->unregisterOldIndexer()V

    .line 139
    :cond_0
    iput-object p1, p0, Lcom/samsung/musicplus/widget/database/IndexView;->mAdapter:Landroid/widget/Adapter;

    .line 140
    return-void
.end method

.method public setDefaultViewCount(I)V
    .locals 0
    .param p1, "count"    # I

    .prologue
    .line 107
    if-ltz p1, :cond_0

    .line 108
    iput p1, p0, Lcom/samsung/musicplus/widget/database/IndexView;->mDefaultViewCount:I

    .line 110
    :cond_0
    return-void
.end method

.method public setExpandableAdapter(Landroid/widget/ExpandableListAdapter;)V
    .locals 1
    .param p1, "a"    # Landroid/widget/ExpandableListAdapter;

    .prologue
    .line 143
    iget-object v0, p0, Lcom/samsung/musicplus/widget/database/IndexView;->mExpandableAdapter:Landroid/widget/ExpandableListAdapter;

    if-eqz v0, :cond_0

    .line 144
    iget-object v0, p0, Lcom/samsung/musicplus/widget/database/IndexView;->mExpandableAdapter:Landroid/widget/ExpandableListAdapter;

    if-eq v0, p1, :cond_0

    .line 146
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/database/IndexView;->unregisterOldIndexer()V

    .line 149
    :cond_0
    iput-object p1, p0, Lcom/samsung/musicplus/widget/database/IndexView;->mExpandableAdapter:Landroid/widget/ExpandableListAdapter;

    .line 150
    return-void
.end method

.method public setIndexViewEnabled(Z)V
    .locals 2
    .param p1, "enabled"    # Z

    .prologue
    .line 194
    iget-object v0, p0, Lcom/samsung/musicplus/widget/database/IndexView;->mTwIndexScrollView:Lcom/sec/android/touchwiz/widget/TwLangIndexScrollView;

    if-eqz v0, :cond_0

    .line 195
    if-eqz p1, :cond_1

    .line 196
    iget-object v0, p0, Lcom/samsung/musicplus/widget/database/IndexView;->mTwIndexScrollView:Lcom/sec/android/touchwiz/widget/TwLangIndexScrollView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/touchwiz/widget/TwLangIndexScrollView;->setVisibility(I)V

    .line 201
    :cond_0
    :goto_0
    return-void

    .line 198
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/widget/database/IndexView;->mTwIndexScrollView:Lcom/sec/android/touchwiz/widget/TwLangIndexScrollView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/sec/android/touchwiz/widget/TwLangIndexScrollView;->setVisibility(I)V

    goto :goto_0
.end method

.method public setIndexer(Landroid/database/Cursor;I)V
    .locals 3
    .param p1, "c"    # Landroid/database/Cursor;
    .param p2, "indexColumn"    # I

    .prologue
    .line 175
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/database/IndexView;->unregisterOldIndexer()V

    .line 178
    iget-object v0, p0, Lcom/samsung/musicplus/widget/database/IndexView;->mIndexCharArray:[Ljava/lang/String;

    if-nez v0, :cond_0

    .line 179
    iget-object v0, p0, Lcom/samsung/musicplus/widget/database/IndexView;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f090004

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/widget/database/IndexView;->mIndexCharArray:[Ljava/lang/String;

    .line 181
    :cond_0
    new-instance v0, Lcom/sec/android/touchwiz/widget/TwCursorIndexer;

    iget-object v1, p0, Lcom/samsung/musicplus/widget/database/IndexView;->mIndexCharArray:[Ljava/lang/String;

    const/4 v2, 0x0

    invoke-direct {v0, p1, p2, v1, v2}, Lcom/sec/android/touchwiz/widget/TwCursorIndexer;-><init>(Landroid/database/Cursor;I[Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/samsung/musicplus/widget/database/IndexView;->mTwIndexer:Lcom/sec/android/touchwiz/widget/TwCursorIndexer;

    .line 182
    iget-object v0, p0, Lcom/samsung/musicplus/widget/database/IndexView;->mAdapter:Landroid/widget/Adapter;

    if-eqz v0, :cond_1

    .line 183
    iget-object v0, p0, Lcom/samsung/musicplus/widget/database/IndexView;->mAdapter:Landroid/widget/Adapter;

    iget-object v1, p0, Lcom/samsung/musicplus/widget/database/IndexView;->mTwIndexer:Lcom/sec/android/touchwiz/widget/TwCursorIndexer;

    invoke-interface {v0, v1}, Landroid/widget/Adapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 185
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/widget/database/IndexView;->mExpandableAdapter:Landroid/widget/ExpandableListAdapter;

    if-eqz v0, :cond_2

    .line 186
    iget-object v0, p0, Lcom/samsung/musicplus/widget/database/IndexView;->mExpandableAdapter:Landroid/widget/ExpandableListAdapter;

    iget-object v1, p0, Lcom/samsung/musicplus/widget/database/IndexView;->mTwIndexer:Lcom/sec/android/touchwiz/widget/TwCursorIndexer;

    invoke-interface {v0, v1}, Landroid/widget/ExpandableListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 188
    :cond_2
    iget-object v0, p0, Lcom/samsung/musicplus/widget/database/IndexView;->mTwIndexScrollView:Lcom/sec/android/touchwiz/widget/TwLangIndexScrollView;

    if-eqz v0, :cond_3

    .line 189
    iget-object v0, p0, Lcom/samsung/musicplus/widget/database/IndexView;->mTwIndexScrollView:Lcom/sec/android/touchwiz/widget/TwLangIndexScrollView;

    iget-object v1, p0, Lcom/samsung/musicplus/widget/database/IndexView;->mTwIndexer:Lcom/sec/android/touchwiz/widget/TwCursorIndexer;

    invoke-virtual {v0, v1}, Lcom/sec/android/touchwiz/widget/TwLangIndexScrollView;->setIndexer(Lcom/sec/android/touchwiz/widget/TwAbstractIndexer;)V

    .line 191
    :cond_3
    return-void
.end method

.method public setList(Landroid/widget/ListView;)V
    .locals 2
    .param p1, "lv"    # Landroid/widget/ListView;

    .prologue
    .line 99
    iget-object v0, p0, Lcom/samsung/musicplus/widget/database/IndexView;->mList:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    .line 100
    iget-object v0, p0, Lcom/samsung/musicplus/widget/database/IndexView;->mList:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 102
    :cond_0
    invoke-virtual {p1, p0}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 103
    iput-object p1, p0, Lcom/samsung/musicplus/widget/database/IndexView;->mList:Landroid/widget/ListView;

    .line 104
    return-void
.end method

.method public setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V
    .locals 0
    .param p1, "l"    # Landroid/widget/AbsListView$OnScrollListener;

    .prologue
    .line 45
    iput-object p1, p0, Lcom/samsung/musicplus/widget/database/IndexView;->mScrollListener:Landroid/widget/AbsListView$OnScrollListener;

    .line 46
    return-void
.end method
