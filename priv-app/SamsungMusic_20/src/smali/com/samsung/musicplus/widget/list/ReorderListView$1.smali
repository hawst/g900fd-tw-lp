.class Lcom/samsung/musicplus/widget/list/ReorderListView$1;
.super Landroid/os/Handler;
.source "ReorderListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/widget/list/ReorderListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/widget/list/ReorderListView;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/widget/list/ReorderListView;)V
    .locals 0

    .prologue
    .line 422
    iput-object p1, p0, Lcom/samsung/musicplus/widget/list/ReorderListView$1;->this$0:Lcom/samsung/musicplus/widget/list/ReorderListView;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 424
    iget-object v2, p0, Lcom/samsung/musicplus/widget/list/ReorderListView$1;->this$0:Lcom/samsung/musicplus/widget/list/ReorderListView;

    # getter for: Lcom/samsung/musicplus/widget/list/ReorderListView;->mUI:Landroid/os/Handler;
    invoke-static {v2}, Lcom/samsung/musicplus/widget/list/ReorderListView;->access$000(Lcom/samsung/musicplus/widget/list/ReorderListView;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    .line 425
    .local v1, "msg1":Landroid/os/Message;
    iget v2, p1, Landroid/os/Message;->arg1:I

    iput v2, v1, Landroid/os/Message;->arg1:I

    .line 426
    iget-object v2, p0, Lcom/samsung/musicplus/widget/list/ReorderListView$1;->this$0:Lcom/samsung/musicplus/widget/list/ReorderListView;

    invoke-virtual {v2}, Lcom/samsung/musicplus/widget/list/ReorderListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;

    .line 427
    .local v0, "adapter":Lcom/samsung/musicplus/widget/list/CommonListAdapter;
    if-eqz v0, :cond_0

    iget v2, v0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->mScrollState:I

    if-nez v2, :cond_0

    .line 428
    iget-object v2, p0, Lcom/samsung/musicplus/widget/list/ReorderListView$1;->this$0:Lcom/samsung/musicplus/widget/list/ReorderListView;

    iget v3, p1, Landroid/os/Message;->arg1:I

    const/16 v4, 0x1e

    invoke-virtual {v2, v3, v4}, Lcom/samsung/musicplus/widget/list/ReorderListView;->smoothScrollBy(II)V

    .line 429
    iget-object v2, p0, Lcom/samsung/musicplus/widget/list/ReorderListView$1;->this$0:Lcom/samsung/musicplus/widget/list/ReorderListView;

    # getter for: Lcom/samsung/musicplus/widget/list/ReorderListView;->mUI:Landroid/os/Handler;
    invoke-static {v2}, Lcom/samsung/musicplus/widget/list/ReorderListView;->access$000(Lcom/samsung/musicplus/widget/list/ReorderListView;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 432
    :goto_0
    return-void

    .line 431
    :cond_0
    iget-object v2, p0, Lcom/samsung/musicplus/widget/list/ReorderListView$1;->this$0:Lcom/samsung/musicplus/widget/list/ReorderListView;

    # getter for: Lcom/samsung/musicplus/widget/list/ReorderListView;->mUI:Landroid/os/Handler;
    invoke-static {v2}, Lcom/samsung/musicplus/widget/list/ReorderListView;->access$000(Lcom/samsung/musicplus/widget/list/ReorderListView;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method
