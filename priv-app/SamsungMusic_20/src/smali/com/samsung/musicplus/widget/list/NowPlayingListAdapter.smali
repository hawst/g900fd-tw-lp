.class public Lcom/samsung/musicplus/widget/list/NowPlayingListAdapter;
.super Lcom/samsung/musicplus/contents/TrackListProgressAdapter;
.source "NowPlayingListAdapter.java"


# static fields
.field private static final LINE1_TEXT_COLOR:I = 0x7f0b004b

.field private static final PLAYING_TEXT_COLOR:I = 0x7f0b004d


# instance fields
.field private mEnableAirView:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;IIILandroid/database/Cursor;IZ)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "layout"    # I
    .param p3, "colorstate"    # I
    .param p4, "animation"    # I
    .param p5, "c"    # Landroid/database/Cursor;
    .param p6, "flags"    # I
    .param p7, "enableAirView"    # Z

    .prologue
    .line 56
    invoke-direct {p0, p1, p2, p5, p6}, Lcom/samsung/musicplus/contents/TrackListProgressAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;I)V

    .line 39
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/widget/list/NowPlayingListAdapter;->mEnableAirView:Z

    .line 57
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/widget/list/NowPlayingListAdapter;->setTextColor(Landroid/content/Context;)V

    .line 58
    invoke-virtual {p0, p4}, Lcom/samsung/musicplus/widget/list/NowPlayingListAdapter;->setPlayingViewAnimation(I)V

    .line 59
    iput-boolean p7, p0, Lcom/samsung/musicplus/widget/list/NowPlayingListAdapter;->mEnableAirView:Z

    .line 61
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/widget/list/NowPlayingListAdapter;->setPlayingTextColorState(Landroid/content/res/ColorStateList;)V

    .line 62
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;IILandroid/database/Cursor;IZ)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "layout"    # I
    .param p3, "animation"    # I
    .param p4, "c"    # Landroid/database/Cursor;
    .param p5, "flags"    # I
    .param p6, "enableAirView"    # Z

    .prologue
    .line 48
    invoke-direct {p0, p1, p2, p4, p5}, Lcom/samsung/musicplus/contents/TrackListProgressAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;I)V

    .line 39
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/widget/list/NowPlayingListAdapter;->mEnableAirView:Z

    .line 49
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/widget/list/NowPlayingListAdapter;->setTextColor(Landroid/content/Context;)V

    .line 50
    invoke-virtual {p0, p3}, Lcom/samsung/musicplus/widget/list/NowPlayingListAdapter;->setPlayingViewAnimation(I)V

    .line 51
    iput-boolean p6, p0, Lcom/samsung/musicplus/widget/list/NowPlayingListAdapter;->mEnableAirView:Z

    .line 52
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILandroid/database/Cursor;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "layout"    # I
    .param p3, "c"    # Landroid/database/Cursor;
    .param p4, "flags"    # I

    .prologue
    .line 42
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/musicplus/contents/TrackListProgressAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;I)V

    .line 39
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/widget/list/NowPlayingListAdapter;->mEnableAirView:Z

    .line 43
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/widget/list/NowPlayingListAdapter;->setTextColor(Landroid/content/Context;)V

    .line 44
    return-void
.end method

.method private setTextColor(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 65
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b004d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/widget/list/NowPlayingListAdapter;->setPlayingTextColor(I)V

    .line 66
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b004b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/widget/list/NowPlayingListAdapter;->setDefaultColor(I)V

    .line 67
    return-void
.end method

.method private updateNowPlayingPosition()V
    .locals 2

    .prologue
    .line 86
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getListPosition()I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/samsung/musicplus/widget/list/NowPlayingListAdapter;->mPlayingId:J

    .line 87
    return-void
.end method


# virtual methods
.method protected bindNowPlayingView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "c"    # Landroid/database/Cursor;

    .prologue
    .line 103
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/musicplus/contents/TrackListProgressAdapter$TrackProgressViewHolder;

    .line 107
    .local v1, "vh":Lcom/samsung/musicplus/contents/TrackListProgressAdapter$TrackProgressViewHolder;
    invoke-interface {p3}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    .line 108
    .local v0, "position":I
    iget-object v2, v1, Lcom/samsung/musicplus/contents/TrackListProgressAdapter$TrackProgressViewHolder;->animation:Landroid/widget/ImageView;

    iget-object v3, v1, Lcom/samsung/musicplus/contents/TrackListProgressAdapter$TrackProgressViewHolder;->text1:Landroid/widget/TextView;

    int-to-long v4, v0

    invoke-virtual {p0, v2, v3, v4, v5}, Lcom/samsung/musicplus/widget/list/NowPlayingListAdapter;->updateNowPlayingView(Landroid/widget/ImageView;Landroid/widget/TextView;J)V

    .line 110
    iget-object v2, v1, Lcom/samsung/musicplus/contents/TrackListProgressAdapter$TrackProgressViewHolder;->progress:Landroid/widget/ProgressBar;

    if-eqz v2, :cond_0

    .line 111
    iget-object v2, v1, Lcom/samsung/musicplus/contents/TrackListProgressAdapter$TrackProgressViewHolder;->progress:Landroid/widget/ProgressBar;

    iget-object v3, v1, Lcom/samsung/musicplus/contents/TrackListProgressAdapter$TrackProgressViewHolder;->animation:Landroid/widget/ImageView;

    int-to-long v4, v0

    invoke-virtual {p0, v2, v3, v4, v5}, Lcom/samsung/musicplus/widget/list/NowPlayingListAdapter;->updateProgressView(Landroid/widget/ProgressBar;Landroid/widget/ImageView;J)V

    .line 113
    :cond_0
    return-void
.end method

.method public changeCursor(Landroid/database/Cursor;)V
    .locals 0
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/list/NowPlayingListAdapter;->updateNowPlayingPosition()V

    .line 73
    invoke-super {p0, p1}, Lcom/samsung/musicplus/contents/TrackListProgressAdapter;->changeCursor(Landroid/database/Cursor;)V

    .line 74
    return-void
.end method

.method public setPlayingId(J)V
    .locals 0
    .param p1, "id"    # J

    .prologue
    .line 78
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/list/NowPlayingListAdapter;->updateNowPlayingPosition()V

    .line 79
    return-void
.end method

.method protected setTextAirView(Landroid/view/View;Landroid/widget/TextView;Landroid/widget/TextView;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;
    .param p2, "text1"    # Landroid/widget/TextView;
    .param p3, "text2"    # Landroid/widget/TextView;

    .prologue
    .line 91
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/NowPlayingListAdapter;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/musicplus/util/UiUtils;->checkHoverUI(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 92
    iget-boolean v0, p0, Lcom/samsung/musicplus/widget/list/NowPlayingListAdapter;->mEnableAirView:Z

    if-eqz v0, :cond_1

    .line 93
    invoke-virtual {p0, p1, p2, p3}, Lcom/samsung/musicplus/widget/list/NowPlayingListAdapter;->newTextAirView(Landroid/view/View;Landroid/widget/TextView;Landroid/widget/TextView;)V

    .line 99
    :cond_0
    :goto_0
    return-void

    .line 95
    :cond_1
    invoke-static {p2}, Lcom/samsung/musicplus/library/hardware/AirView;->setDisable(Landroid/view/View;)V

    .line 96
    invoke-static {p3}, Lcom/samsung/musicplus/library/hardware/AirView;->setDisable(Landroid/view/View;)V

    goto :goto_0
.end method
