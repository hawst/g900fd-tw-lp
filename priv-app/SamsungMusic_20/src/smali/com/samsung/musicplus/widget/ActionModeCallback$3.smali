.class Lcom/samsung/musicplus/widget/ActionModeCallback$3;
.super Ljava/lang/Object;
.source "ActionModeCallback.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/widget/ActionModeCallback;->onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/widget/ActionModeCallback;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/widget/ActionModeCallback;)V
    .locals 0

    .prologue
    .line 128
    iput-object p1, p0, Lcom/samsung/musicplus/widget/ActionModeCallback$3;->this$0:Lcom/samsung/musicplus/widget/ActionModeCallback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 131
    iget-object v0, p0, Lcom/samsung/musicplus/widget/ActionModeCallback$3;->this$0:Lcom/samsung/musicplus/widget/ActionModeCallback;

    # getter for: Lcom/samsung/musicplus/widget/ActionModeCallback;->mCheckBox:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/samsung/musicplus/widget/ActionModeCallback;->access$000(Lcom/samsung/musicplus/widget/ActionModeCallback;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 132
    iget-object v0, p0, Lcom/samsung/musicplus/widget/ActionModeCallback$3;->this$0:Lcom/samsung/musicplus/widget/ActionModeCallback;

    # invokes: Lcom/samsung/musicplus/widget/ActionModeCallback;->setSelectAll(Z)V
    invoke-static {v0, v1}, Lcom/samsung/musicplus/widget/ActionModeCallback;->access$100(Lcom/samsung/musicplus/widget/ActionModeCallback;Z)V

    .line 133
    iget-object v0, p0, Lcom/samsung/musicplus/widget/ActionModeCallback$3;->this$0:Lcom/samsung/musicplus/widget/ActionModeCallback;

    # getter for: Lcom/samsung/musicplus/widget/ActionModeCallback;->mCheckBox:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/samsung/musicplus/widget/ActionModeCallback;->access$000(Lcom/samsung/musicplus/widget/ActionModeCallback;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 138
    :goto_0
    iget-object v0, p0, Lcom/samsung/musicplus/widget/ActionModeCallback$3;->this$0:Lcom/samsung/musicplus/widget/ActionModeCallback;

    invoke-virtual {v0}, Lcom/samsung/musicplus/widget/ActionModeCallback;->invalidateActionMode()V

    .line 139
    const v0, 0x8000

    invoke-virtual {p1, v0}, Landroid/view/View;->sendAccessibilityEvent(I)V

    .line 140
    return-void

    .line 135
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/widget/ActionModeCallback$3;->this$0:Lcom/samsung/musicplus/widget/ActionModeCallback;

    # invokes: Lcom/samsung/musicplus/widget/ActionModeCallback;->setSelectAll(Z)V
    invoke-static {v0, v2}, Lcom/samsung/musicplus/widget/ActionModeCallback;->access$100(Lcom/samsung/musicplus/widget/ActionModeCallback;Z)V

    .line 136
    iget-object v0, p0, Lcom/samsung/musicplus/widget/ActionModeCallback$3;->this$0:Lcom/samsung/musicplus/widget/ActionModeCallback;

    # getter for: Lcom/samsung/musicplus/widget/ActionModeCallback;->mCheckBox:Landroid/widget/CheckBox;
    invoke-static {v0}, Lcom/samsung/musicplus/widget/ActionModeCallback;->access$000(Lcom/samsung/musicplus/widget/ActionModeCallback;)Landroid/widget/CheckBox;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0
.end method
