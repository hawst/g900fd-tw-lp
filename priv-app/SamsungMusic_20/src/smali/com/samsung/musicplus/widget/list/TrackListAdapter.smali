.class public Lcom/samsung/musicplus/widget/list/TrackListAdapter;
.super Lcom/samsung/musicplus/widget/list/CommonListAdapter;
.source "TrackListAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/widget/list/TrackListAdapter$TrackViewHolder;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "MusicUiList"


# instance fields
.field private mAnim:Landroid/graphics/drawable/AnimationDrawable;

.field private mDefaultTextColor:I

.field protected mDurationIndex:I

.field private mNumberStringResource:I

.field private mPaddingRight:I

.field private mPlayingAnimation:I

.field protected mPlayingId:J

.field private mPlayingTextColor:I

.field private mPlayingTextColorState:Landroid/content/res/ColorStateList;

.field private mShufflePosition:I


# direct methods
.method public constructor <init>(Landroid/content/Context;ILandroid/database/Cursor;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "layout"    # I
    .param p3, "c"    # Landroid/database/Cursor;
    .param p4, "flags"    # I

    .prologue
    const/4 v2, -0x1

    .line 48
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;I)V

    .line 113
    const v0, 0x7f050006

    iput v0, p0, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->mPlayingAnimation:I

    .line 124
    iput v2, p0, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->mPaddingRight:I

    .line 148
    iput v2, p0, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->mShufflePosition:I

    .line 49
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getCurrentAudioId()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->setPlayingId(J)V

    .line 51
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020023

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->setPlayingTextColorState(Landroid/content/res/ColorStateList;)V

    .line 52
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b002b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->setDefaultColor(I)V

    .line 53
    iput v2, p0, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->mPaddingRight:I

    .line 54
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILandroid/database/Cursor;II)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "layout"    # I
    .param p3, "c"    # Landroid/database/Cursor;
    .param p4, "flags"    # I
    .param p5, "padding"    # I

    .prologue
    const/4 v1, -0x1

    .line 57
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;I)V

    .line 113
    const v0, 0x7f050006

    iput v0, p0, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->mPlayingAnimation:I

    .line 124
    iput v1, p0, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->mPaddingRight:I

    .line 148
    iput v1, p0, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->mShufflePosition:I

    .line 58
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getCurrentAudioId()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->setPlayingId(J)V

    .line 60
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f020023

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->setPlayingTextColorState(Landroid/content/res/ColorStateList;)V

    .line 61
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b002b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->setDefaultColor(I)V

    .line 62
    iput p5, p0, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->mPaddingRight:I

    .line 63
    return-void
.end method

.method private markNowPlayingView(Landroid/widget/ImageView;Landroid/widget/TextView;)V
    .locals 1
    .param p1, "animation"    # Landroid/widget/ImageView;
    .param p2, "text1"    # Landroid/widget/TextView;

    .prologue
    .line 369
    if-eqz p1, :cond_0

    .line 370
    invoke-virtual {p1}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/musicplus/util/UiUtils;->isTalkBackEnabled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 371
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 379
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->mPlayingTextColorState:Landroid/content/res/ColorStateList;

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 380
    return-void

    .line 373
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 374
    iget v0, p0, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->mPlayingAnimation:I

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 375
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->updatePlayingAnimation(Landroid/widget/ImageView;)V

    goto :goto_0
.end method

.method private setPersonalIcon(Landroid/widget/ImageView;Landroid/database/Cursor;)V
    .locals 4
    .param p1, "personalIcon"    # Landroid/widget/ImageView;
    .param p2, "c"    # Landroid/database/Cursor;

    .prologue
    .line 427
    if-nez p1, :cond_0

    .line 441
    :goto_0
    return-void

    .line 431
    :cond_0
    const-wide/16 v0, 0x0

    .line 432
    .local v0, "isSecreoBoxFile":J
    iget v2, p0, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->mSecretBoxIndex:I

    if-ltz v2, :cond_1

    .line 433
    iget v2, p0, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->mSecretBoxIndex:I

    invoke-interface {p2, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    int-to-long v0, v2

    .line 436
    :cond_1
    const-wide/16 v2, 0x1

    cmp-long v2, v0, v2

    if-nez v2, :cond_2

    .line 437
    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 439
    :cond_2
    const/16 v2, 0x8

    invoke-virtual {p1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private setUhqTag(Landroid/view/View;Landroid/database/Cursor;)V
    .locals 3
    .param p1, "uhqTag"    # Landroid/view/View;
    .param p2, "c"    # Landroid/database/Cursor;

    .prologue
    .line 408
    if-nez p1, :cond_0

    .line 424
    :goto_0
    return-void

    .line 412
    :cond_0
    const/4 v1, 0x0

    .line 413
    .local v1, "samplingRate":I
    const/4 v0, 0x0

    .line 414
    .local v0, "bitDepth":I
    iget v2, p0, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->mSamplingRateIndex:I

    if-ltz v2, :cond_1

    iget v2, p0, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->mBitDepthIndex:I

    if-ltz v2, :cond_1

    .line 415
    iget v2, p0, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->mSamplingRateIndex:I

    invoke-interface {p2, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 416
    iget v2, p0, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->mBitDepthIndex:I

    invoke-interface {p2, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 419
    :cond_1
    invoke-static {}, Lcom/samsung/musicplus/util/UiUtils;->isSupportUhqa()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-static {v1, v0}, Lcom/samsung/musicplus/util/UiUtils;->isUhqa(II)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 420
    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 422
    :cond_2
    const/16 v2, 0x8

    invoke-virtual {p1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method private setVisibilityExtraInfoView(Landroid/widget/ImageView;Landroid/widget/ImageView;Landroid/view/View;)V
    .locals 4
    .param p1, "animation"    # Landroid/widget/ImageView;
    .param p2, "personalIcon"    # Landroid/widget/ImageView;
    .param p3, "extraInfoViewGroup"    # Landroid/view/View;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 501
    if-nez p3, :cond_0

    .line 515
    :goto_0
    return-void

    .line 505
    :cond_0
    const/4 v0, 0x0

    .line 506
    .local v0, "isVisible":Z
    if-nez p1, :cond_2

    .line 507
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Landroid/widget/ImageView;->getVisibility()I

    move-result v3

    if-nez v3, :cond_1

    move v0, v1

    .line 514
    :goto_1
    if-eqz v0, :cond_7

    :goto_2
    invoke-virtual {p3, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_1
    move v0, v2

    .line 507
    goto :goto_1

    .line 508
    :cond_2
    if-nez p2, :cond_4

    .line 509
    invoke-virtual {p1}, Landroid/widget/ImageView;->getVisibility()I

    move-result v3

    if-nez v3, :cond_3

    move v0, v1

    :goto_3
    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_3

    .line 511
    :cond_4
    invoke-virtual {p1}, Landroid/widget/ImageView;->getVisibility()I

    move-result v3

    if-eqz v3, :cond_5

    invoke-virtual {p2}, Landroid/widget/ImageView;->getVisibility()I

    move-result v3

    if-nez v3, :cond_6

    :cond_5
    move v0, v1

    :goto_4
    goto :goto_1

    :cond_6
    move v0, v2

    goto :goto_4

    .line 514
    :cond_7
    const/16 v2, 0x8

    goto :goto_2
.end method

.method private unmarkNowPlayingView(Landroid/widget/ImageView;Landroid/widget/TextView;)V
    .locals 1
    .param p1, "animation"    # Landroid/widget/ImageView;
    .param p2, "text1"    # Landroid/widget/TextView;

    .prologue
    .line 355
    if-eqz p1, :cond_0

    .line 356
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 357
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 359
    :cond_0
    iget v0, p0, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->mDefaultTextColor:I

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 360
    return-void
.end method

.method private updatePlayingAnimation(Landroid/widget/ImageView;)V
    .locals 1
    .param p1, "animView"    # Landroid/widget/ImageView;

    .prologue
    .line 389
    invoke-virtual {p1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/AnimationDrawable;

    iput-object v0, p0, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->mAnim:Landroid/graphics/drawable/AnimationDrawable;

    .line 390
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 391
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->isPreparing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 392
    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 405
    :cond_0
    :goto_0
    return-void

    .line 394
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 395
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->mAnim:Landroid/graphics/drawable/AnimationDrawable;

    if-eqz v0, :cond_0

    .line 396
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->mAnim:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->start()V

    goto :goto_0

    .line 401
    :cond_2
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->mAnim:Landroid/graphics/drawable/AnimationDrawable;

    if-eqz v0, :cond_0

    .line 402
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->mAnim:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->stop()V

    goto :goto_0
.end method


# virtual methods
.method protected bindNowPlayingView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "c"    # Landroid/database/Cursor;

    .prologue
    .line 322
    iget v1, p0, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->mAudioIndex:I

    if-gez v1, :cond_0

    .line 323
    const-string v1, "MusicUiList"

    const-string v2, "Your track trying to bindNowplayingView, but it can\'t update because the audio column index is under 0. Please check your codes"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 329
    :goto_0
    return-void

    .line 326
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/widget/list/TrackListAdapter$TrackViewHolder;

    .line 327
    .local v0, "vh":Lcom/samsung/musicplus/widget/list/TrackListAdapter$TrackViewHolder;
    iget-object v1, v0, Lcom/samsung/musicplus/widget/list/TrackListAdapter$TrackViewHolder;->animation:Landroid/widget/ImageView;

    iget-object v2, v0, Lcom/samsung/musicplus/widget/list/TrackListAdapter$TrackViewHolder;->text1:Landroid/widget/TextView;

    iget v3, p0, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->mAudioIndex:I

    invoke-interface {p3, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-virtual {p0, v1, v2, v4, v5}, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->updateNowPlayingView(Landroid/widget/ImageView;Landroid/widget/TextView;J)V

    goto :goto_0
.end method

.method protected bindOtherView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "c"    # Landroid/database/Cursor;

    .prologue
    .line 306
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/widget/list/TrackListAdapter$TrackViewHolder;

    .line 307
    .local v0, "vh":Lcom/samsung/musicplus/widget/list/TrackListAdapter$TrackViewHolder;
    iget-object v1, v0, Lcom/samsung/musicplus/widget/list/TrackListAdapter$TrackViewHolder;->uhqTag:Landroid/view/View;

    invoke-direct {p0, v1, p3}, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->setUhqTag(Landroid/view/View;Landroid/database/Cursor;)V

    .line 308
    iget-object v1, v0, Lcom/samsung/musicplus/widget/list/TrackListAdapter$TrackViewHolder;->personalIcon:Landroid/widget/ImageView;

    invoke-direct {p0, v1, p3}, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->setPersonalIcon(Landroid/widget/ImageView;Landroid/database/Cursor;)V

    .line 309
    iget-object v1, v0, Lcom/samsung/musicplus/widget/list/TrackListAdapter$TrackViewHolder;->animation:Landroid/widget/ImageView;

    iget-object v2, v0, Lcom/samsung/musicplus/widget/list/TrackListAdapter$TrackViewHolder;->personalIcon:Landroid/widget/ImageView;

    iget-object v3, v0, Lcom/samsung/musicplus/widget/list/TrackListAdapter$TrackViewHolder;->extraInfoViewGroup:Landroid/view/View;

    invoke-direct {p0, v1, v2, v3}, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->setVisibilityExtraInfoView(Landroid/widget/ImageView;Landroid/widget/ImageView;Landroid/view/View;)V

    .line 310
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->bindOtherView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    .line 311
    return-void
.end method

.method protected bindTextView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "c"    # Landroid/database/Cursor;

    .prologue
    .line 296
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/widget/list/TrackListAdapter$TrackViewHolder;

    .line 297
    .local v0, "vh":Lcom/samsung/musicplus/widget/list/TrackListAdapter$TrackViewHolder;
    iget-object v1, v0, Lcom/samsung/musicplus/widget/list/TrackListAdapter$TrackViewHolder;->duration:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->mDurationIndex:I

    if-ltz v1, :cond_0

    .line 298
    iget-object v1, v0, Lcom/samsung/musicplus/widget/list/TrackListAdapter$TrackViewHolder;->duration:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 299
    iget-object v1, v0, Lcom/samsung/musicplus/widget/list/TrackListAdapter$TrackViewHolder;->duration:Landroid/widget/TextView;

    iget v2, p0, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->mDurationIndex:I

    invoke-interface {p3, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-virtual {p0, p2, v1, v2}, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->setDuration(Landroid/content/Context;Landroid/widget/TextView;I)V

    .line 301
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->bindTextView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    .line 302
    return-void
.end method

.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 7
    .param p1, "v"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "c"    # Landroid/database/Cursor;

    .prologue
    const/4 v6, 0x0

    .line 271
    invoke-interface {p3, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 272
    .local v2, "id":J
    const-wide/16 v4, -0x64

    cmp-long v4, v4, v2

    if-nez v4, :cond_1

    .line 273
    invoke-virtual {p0, p1, p2, p3}, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->setShuffleOfTextView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    .line 292
    :cond_0
    :goto_0
    return-void

    .line 276
    :cond_1
    iget v4, p0, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->mPaddingRight:I

    const/4 v5, -0x1

    if-le v4, v5, :cond_2

    .line 277
    const v4, 0x7f0d00bd

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 278
    .local v1, "item":Landroid/view/View;
    if-eqz v1, :cond_2

    .line 279
    iget v4, p0, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->mPaddingRight:I

    invoke-virtual {v1, v6, v6, v4, v6}, Landroid/view/View;->setPadding(IIII)V

    .line 282
    .end local v1    # "item":Landroid/view/View;
    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->bindNowPlayingView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    .line 283
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    .line 284
    const v4, 0x7f0d00bb

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 285
    .local v0, "divider":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 286
    iget-boolean v4, p0, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->mIsActionMode:Z

    if-eqz v4, :cond_3

    .line 287
    const v4, 0x7f020022

    invoke-virtual {v0, v4}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_0

    .line 289
    :cond_3
    const v4, 0x7f020020

    invoke-virtual {v0, v4}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method protected getDefaultTextColor()I
    .locals 1

    .prologue
    .line 92
    iget v0, p0, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->mDefaultTextColor:I

    return v0
.end method

.method protected getPlayingTextColor()I
    .locals 1

    .prologue
    .line 96
    iget v0, p0, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->mPlayingTextColor:I

    return v0
.end method

.method protected getPlayingTextColorState()Landroid/content/res/ColorStateList;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->mPlayingTextColorState:Landroid/content/res/ColorStateList;

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 174
    iget v0, p0, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->mShufflePosition:I

    if-ne p1, v0, :cond_0

    .line 175
    const/4 p2, 0x0

    .line 177
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected getViewHolder()Lcom/samsung/musicplus/widget/list/CommonListAdapter$ViewHolder;
    .locals 1

    .prologue
    .line 245
    new-instance v0, Lcom/samsung/musicplus/widget/list/TrackListAdapter$TrackViewHolder;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/widget/list/TrackListAdapter$TrackViewHolder;-><init>(Lcom/samsung/musicplus/widget/list/TrackListAdapter;)V

    return-object v0
.end method

.method protected newAnimationView(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 208
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/musicplus/widget/list/TrackListAdapter$TrackViewHolder;

    .line 209
    .local v1, "vh":Lcom/samsung/musicplus/widget/list/TrackListAdapter$TrackViewHolder;
    const v2, 0x7f0d00c0

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 210
    .local v0, "animation":Landroid/view/View;
    instance-of v2, v0, Landroid/view/ViewStub;

    if-eqz v2, :cond_0

    .line 211
    check-cast v0, Landroid/view/ViewStub;

    .end local v0    # "animation":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 213
    :cond_0
    const v2, 0x7f0d00c4

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, v1, Lcom/samsung/musicplus/widget/list/TrackListAdapter$TrackViewHolder;->animation:Landroid/widget/ImageView;

    .line 214
    invoke-virtual {p1, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 215
    return-void
.end method

.method protected newOtherView(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 219
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/musicplus/widget/list/TrackListAdapter$TrackViewHolder;

    .line 220
    .local v3, "vh":Lcom/samsung/musicplus/widget/list/TrackListAdapter$TrackViewHolder;
    const v4, 0x7f0d005a

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 221
    .local v2, "uhqStub":Landroid/view/View;
    instance-of v4, v2, Landroid/view/ViewStub;

    if-eqz v4, :cond_3

    .line 222
    check-cast v2, Landroid/view/ViewStub;

    .end local v2    # "uhqStub":Landroid/view/View;
    invoke-virtual {v2}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 223
    const v4, 0x7f0d0065

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, v3, Lcom/samsung/musicplus/widget/list/TrackListAdapter$TrackViewHolder;->uhqTag:Landroid/view/View;

    .line 228
    :cond_0
    :goto_0
    const v4, 0x7f0d00bf

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 229
    .local v1, "personalStub":Landroid/view/View;
    instance-of v4, v1, Landroid/view/ViewStub;

    if-eqz v4, :cond_4

    .line 230
    check-cast v1, Landroid/view/ViewStub;

    .end local v1    # "personalStub":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 231
    const v4, 0x7f0d0105

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    iput-object v4, v3, Lcom/samsung/musicplus/widget/list/TrackListAdapter$TrackViewHolder;->personalIcon:Landroid/widget/ImageView;

    .line 236
    :cond_1
    :goto_1
    const v4, 0x7f0d00be

    invoke-virtual {p1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 237
    .local v0, "group":Landroid/view/View;
    if-eqz v0, :cond_2

    .line 238
    iput-object v0, v3, Lcom/samsung/musicplus/widget/list/TrackListAdapter$TrackViewHolder;->extraInfoViewGroup:Landroid/view/View;

    .line 240
    :cond_2
    invoke-virtual {p1, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 241
    return-void

    .line 224
    .end local v0    # "group":Landroid/view/View;
    .restart local v2    # "uhqStub":Landroid/view/View;
    :cond_3
    instance-of v4, v2, Landroid/widget/ImageView;

    if-eqz v4, :cond_0

    .line 225
    iput-object v2, v3, Lcom/samsung/musicplus/widget/list/TrackListAdapter$TrackViewHolder;->uhqTag:Landroid/view/View;

    goto :goto_0

    .line 232
    .end local v2    # "uhqStub":Landroid/view/View;
    .restart local v1    # "personalStub":Landroid/view/View;
    :cond_4
    instance-of v4, v1, Landroid/widget/ImageView;

    if-eqz v4, :cond_1

    .line 233
    check-cast v1, Landroid/widget/ImageView;

    .end local v1    # "personalStub":Landroid/view/View;
    iput-object v1, v3, Lcom/samsung/musicplus/widget/list/TrackListAdapter$TrackViewHolder;->personalIcon:Landroid/widget/ImageView;

    goto :goto_1
.end method

.method protected newTextView(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 196
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/widget/list/TrackListAdapter$TrackViewHolder;

    .line 197
    .local v0, "vh":Lcom/samsung/musicplus/widget/list/TrackListAdapter$TrackViewHolder;
    const v1, 0x7f0d00cb

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v0, Lcom/samsung/musicplus/widget/list/TrackListAdapter$TrackViewHolder;->duration:Landroid/widget/TextView;

    .line 198
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->newTextView(Landroid/view/View;)V

    .line 199
    return-void
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "c"    # Landroid/database/Cursor;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v6, 0x0

    .line 182
    invoke-interface {p2, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 183
    .local v0, "id":J
    const-wide/16 v4, -0x64

    cmp-long v4, v4, v0

    if-nez v4, :cond_0

    .line 184
    const-string v4, "layout_inflater"

    invoke-virtual {p1, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/LayoutInflater;

    .line 186
    .local v2, "li":Landroid/view/LayoutInflater;
    const v4, 0x7f04002e

    invoke-virtual {v2, v4, p3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 191
    .end local v2    # "li":Landroid/view/LayoutInflater;
    :goto_0
    return-object v3

    .line 189
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 190
    .local v3, "v":Landroid/view/View;
    invoke-virtual {p0, v3}, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->newAnimationView(Landroid/view/View;)V

    goto :goto_0
.end method

.method protected final setDefaultColor(I)V
    .locals 0
    .param p1, "color"    # I

    .prologue
    .line 75
    iput p1, p0, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->mDefaultTextColor:I

    .line 76
    return-void
.end method

.method protected setDuration(Landroid/content/Context;Landroid/widget/TextView;I)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "duration"    # Landroid/widget/TextView;
    .param p3, "milisec"    # I

    .prologue
    .line 452
    if-nez p3, :cond_0

    .line 458
    const-string v1, ""

    invoke-virtual {p2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 463
    :goto_0
    return-void

    .line 460
    :cond_0
    div-int/lit16 v0, p3, 0x3e8

    .line 461
    .local v0, "secs":I
    int-to-long v2, v0

    invoke-static {p1, v2, v3}, Lcom/samsung/musicplus/util/UiUtils;->makeTimeString(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public setDurationIndex(I)V
    .locals 0
    .param p1, "duration"    # I

    .prologue
    .line 145
    iput p1, p0, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->mDurationIndex:I

    .line 146
    return-void
.end method

.method public setNumberString(I)V
    .locals 0
    .param p1, "stringResource"    # I

    .prologue
    .line 168
    iput p1, p0, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->mNumberStringResource:I

    .line 169
    return-void
.end method

.method public setPaddingRight(I)V
    .locals 0
    .param p1, "padding"    # I

    .prologue
    .line 66
    iput p1, p0, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->mPaddingRight:I

    .line 67
    return-void
.end method

.method public setPlayingId(J)V
    .locals 1
    .param p1, "id"    # J

    .prologue
    .line 134
    iput-wide p1, p0, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->mPlayingId:J

    .line 135
    return-void
.end method

.method protected final setPlayingTextColor(I)V
    .locals 0
    .param p1, "color"    # I

    .prologue
    .line 84
    iput p1, p0, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->mPlayingTextColor:I

    .line 85
    return-void
.end method

.method protected final setPlayingTextColorState(Landroid/content/res/ColorStateList;)V
    .locals 0
    .param p1, "color"    # Landroid/content/res/ColorStateList;

    .prologue
    .line 88
    iput-object p1, p0, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->mPlayingTextColorState:Landroid/content/res/ColorStateList;

    .line 89
    return-void
.end method

.method protected final setPlayingViewAnimation(I)V
    .locals 0
    .param p1, "resId"    # I

    .prologue
    .line 266
    iput p1, p0, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->mPlayingAnimation:I

    .line 267
    return-void
.end method

.method protected setShuffleOfTextView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 10
    .param p1, "v"    # Landroid/view/View;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "c"    # Landroid/database/Cursor;

    .prologue
    const v9, 0x7f0d00af

    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 466
    const v3, 0x7f0d00ae

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 467
    .local v2, "text":Landroid/widget/TextView;
    const/4 v0, 0x0

    .line 470
    .local v0, "countOfItems":I
    instance-of v3, p3, Lcom/samsung/musicplus/widget/database/MusicCursor;

    if-eqz v3, :cond_0

    .line 471
    check-cast p3, Lcom/samsung/musicplus/widget/database/MusicCursor;

    .end local p3    # "c":Landroid/database/Cursor;
    invoke-virtual {p3}, Lcom/samsung/musicplus/widget/database/MusicCursor;->getRealCursorCount()I

    move-result v0

    .line 473
    :cond_0
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget v4, p0, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->mNumberStringResource:I

    new-array v5, v8, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {v3, v4, v0, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 475
    .local v1, "number":Ljava/lang/String;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const v4, 0x7f1000bf

    invoke-virtual {p2, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 478
    iget-boolean v3, p0, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->mIsActionMode:Z

    if-eqz v3, :cond_1

    .line 479
    invoke-virtual {p1, v8}, Landroid/view/View;->setClickable(Z)V

    .line 480
    const/high16 v3, 0x3f000000    # 0.5f

    invoke-virtual {p1, v3}, Landroid/view/View;->setAlpha(F)V

    .line 481
    iget-object v3, p0, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b0030

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    .line 482
    invoke-virtual {p1, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/view/View;->setEnabled(Z)V

    .line 490
    :goto_0
    const-string v3, "MusicUiList"

    const-string v4, "bindView shuffle End"

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 491
    return-void

    .line 484
    :cond_1
    invoke-virtual {p1, v7}, Landroid/view/View;->setClickable(Z)V

    .line 485
    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {p1, v3}, Landroid/view/View;->setAlpha(F)V

    .line 486
    iget-object v3, p0, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b00aa

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 487
    invoke-virtual {p1, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, v8}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_0
.end method

.method public setShufflePosition(I)V
    .locals 0
    .param p1, "position"    # I

    .prologue
    .line 157
    iput p1, p0, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->mShufflePosition:I

    .line 158
    return-void
.end method

.method public stopUpdateView()V
    .locals 1

    .prologue
    .line 255
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->mAnim:Landroid/graphics/drawable/AnimationDrawable;

    if-eqz v0, :cond_0

    .line 256
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->mAnim:Landroid/graphics/drawable/AnimationDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->stop()V

    .line 258
    :cond_0
    return-void
.end method

.method protected updateNowPlayingView(Landroid/widget/ImageView;Landroid/widget/TextView;J)V
    .locals 3
    .param p1, "animation"    # Landroid/widget/ImageView;
    .param p2, "text1"    # Landroid/widget/TextView;
    .param p3, "currentId"    # J

    .prologue
    .line 341
    iget-wide v0, p0, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->mPlayingId:J

    cmp-long v0, v0, p3

    if-nez v0, :cond_0

    .line 342
    invoke-direct {p0, p1, p2}, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->markNowPlayingView(Landroid/widget/ImageView;Landroid/widget/TextView;)V

    .line 346
    :goto_0
    return-void

    .line 344
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->unmarkNowPlayingView(Landroid/widget/ImageView;Landroid/widget/TextView;)V

    goto :goto_0
.end method
