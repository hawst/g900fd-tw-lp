.class Lcom/samsung/musicplus/widget/tab/AbsTabController$MusicPagerAdapter;
.super Landroid/support/v13/app/FragmentPagerAdapter;
.source "AbsTabController.java"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/widget/tab/AbsTabController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MusicPagerAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/widget/tab/AbsTabController;


# direct methods
.method public constructor <init>(Lcom/samsung/musicplus/widget/tab/AbsTabController;Landroid/app/FragmentManager;)V
    .locals 0
    .param p2, "fm"    # Landroid/app/FragmentManager;

    .prologue
    .line 338
    iput-object p1, p0, Lcom/samsung/musicplus/widget/tab/AbsTabController$MusicPagerAdapter;->this$0:Lcom/samsung/musicplus/widget/tab/AbsTabController;

    .line 339
    invoke-direct {p0, p2}, Landroid/support/v13/app/FragmentPagerAdapter;-><init>(Landroid/app/FragmentManager;)V

    .line 340
    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 392
    iget-object v0, p0, Lcom/samsung/musicplus/widget/tab/AbsTabController$MusicPagerAdapter;->this$0:Lcom/samsung/musicplus/widget/tab/AbsTabController;

    invoke-virtual {v0}, Lcom/samsung/musicplus/widget/tab/AbsTabController;->getTabCount()I

    move-result v0

    return v0
.end method

.method public getItem(I)Landroid/app/Fragment;
    .locals 4
    .param p1, "position"    # I

    .prologue
    .line 347
    iget-object v2, p0, Lcom/samsung/musicplus/widget/tab/AbsTabController$MusicPagerAdapter;->this$0:Lcom/samsung/musicplus/widget/tab/AbsTabController;

    # getter for: Lcom/samsung/musicplus/widget/tab/AbsTabController;->mTabIds:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/samsung/musicplus/widget/tab/AbsTabController;->access$000(Lcom/samsung/musicplus/widget/tab/AbsTabController;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/samsung/musicplus/contents/ContentsFragmentFactory;->getNewInstance(ILjava/lang/String;)Landroid/app/Fragment;

    move-result-object v1

    .line 353
    .local v1, "fg":Landroid/app/Fragment;
    invoke-virtual {v1}, Landroid/app/Fragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 354
    .local v0, "b":Landroid/os/Bundle;
    const-string v2, "album_art_parsing"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 355
    invoke-virtual {v1, v0}, Landroid/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 356
    return-object v1
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 402
    iget-object v0, p0, Lcom/samsung/musicplus/widget/tab/AbsTabController$MusicPagerAdapter;->this$0:Lcom/samsung/musicplus/widget/tab/AbsTabController;

    # getter for: Lcom/samsung/musicplus/widget/tab/AbsTabController;->mTabIds:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/musicplus/widget/tab/AbsTabController;->access$000(Lcom/samsung/musicplus/widget/tab/AbsTabController;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public getItemPosition(Ljava/lang/Object;)I
    .locals 1
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    .line 397
    const/4 v0, -0x2

    return v0
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 2
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I

    .prologue
    .line 364
    invoke-super {p0, p1, p2}, Landroid/support/v13/app/FragmentPagerAdapter;->instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Fragment;

    .line 365
    .local v0, "fg":Landroid/app/Fragment;
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/Fragment;->setHasOptionsMenu(Z)V

    .line 370
    iget-object v1, p0, Lcom/samsung/musicplus/widget/tab/AbsTabController$MusicPagerAdapter;->this$0:Lcom/samsung/musicplus/widget/tab/AbsTabController;

    iget-object v1, v1, Lcom/samsung/musicplus/widget/tab/AbsTabController;->mFragments:Landroid/util/SparseArray;

    invoke-virtual {v1, p2, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 371
    return-object v0
.end method

.method public onPageScrollStateChanged(I)V
    .locals 0
    .param p1, "state"    # I

    .prologue
    .line 407
    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 0
    .param p1, "position"    # I
    .param p2, "positionOffset"    # F
    .param p3, "positionOffsetPixels"    # I

    .prologue
    .line 412
    return-void
.end method

.method public onPageSelected(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 416
    iget-object v0, p0, Lcom/samsung/musicplus/widget/tab/AbsTabController$MusicPagerAdapter;->this$0:Lcom/samsung/musicplus/widget/tab/AbsTabController;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/widget/tab/AbsTabController;->setTabSelectedInternal(I)V

    .line 417
    return-void
.end method

.method public setPrimaryItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 2
    .param p1, "container"    # Landroid/view/ViewGroup;
    .param p2, "position"    # I
    .param p3, "object"    # Ljava/lang/Object;

    .prologue
    .line 383
    invoke-super {p0, p1, p2, p3}, Landroid/support/v13/app/FragmentPagerAdapter;->setPrimaryItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V

    .line 384
    iget-object v0, p0, Lcom/samsung/musicplus/widget/tab/AbsTabController$MusicPagerAdapter;->this$0:Lcom/samsung/musicplus/widget/tab/AbsTabController;

    # getter for: Lcom/samsung/musicplus/widget/tab/AbsTabController;->mTabIds:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/musicplus/widget/tab/AbsTabController;->access$000(Lcom/samsung/musicplus/widget/tab/AbsTabController;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/widget/tab/AbsTabController$MusicPagerAdapter;->this$0:Lcom/samsung/musicplus/widget/tab/AbsTabController;

    # getter for: Lcom/samsung/musicplus/widget/tab/AbsTabController;->mTabIds:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/musicplus/widget/tab/AbsTabController;->access$000(Lcom/samsung/musicplus/widget/tab/AbsTabController;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 385
    iget-object v1, p0, Lcom/samsung/musicplus/widget/tab/AbsTabController$MusicPagerAdapter;->this$0:Lcom/samsung/musicplus/widget/tab/AbsTabController;

    iget-object v0, p0, Lcom/samsung/musicplus/widget/tab/AbsTabController$MusicPagerAdapter;->this$0:Lcom/samsung/musicplus/widget/tab/AbsTabController;

    # getter for: Lcom/samsung/musicplus/widget/tab/AbsTabController;->mTabIds:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/samsung/musicplus/widget/tab/AbsTabController;->access$000(Lcom/samsung/musicplus/widget/tab/AbsTabController;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    iput-object v0, v1, Lcom/samsung/musicplus/widget/tab/AbsTabController;->mCurrentTabId:Ljava/lang/Integer;

    .line 387
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/widget/tab/AbsTabController$MusicPagerAdapter;->this$0:Lcom/samsung/musicplus/widget/tab/AbsTabController;

    iput-object p3, v0, Lcom/samsung/musicplus/widget/tab/AbsTabController;->mCurrentContent:Ljava/lang/Object;

    .line 388
    return-void
.end method
