.class public Lcom/samsung/musicplus/widget/control/FowardRewindControlTask;
.super Landroid/os/AsyncTask;
.source "FowardRewindControlTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# static fields
.field public static final FAST_FORWARD:I = 0x1

.field public static final INTERVAL_TIME:I = 0x190

.field public static final REWIND:I = 0x2

.field public static final SINGLE_CLICK_TIME:I = 0x12c

.field private static final TAG:Ljava/lang/String; = "MusicControl"


# instance fields
.field private mIsCancel:Z

.field private mIsInfoEventSent:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 57
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 77
    iput-boolean v0, p0, Lcom/samsung/musicplus/widget/control/FowardRewindControlTask;->mIsCancel:Z

    .line 83
    iput-boolean v0, p0, Lcom/samsung/musicplus/widget/control/FowardRewindControlTask;->mIsInfoEventSent:Z

    return-void
.end method

.method private sendInfo(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "command"    # Ljava/lang/String;

    .prologue
    .line 163
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.musicplus.info"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 164
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "command"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 165
    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 166
    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 57
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/widget/control/FowardRewindControlTask;->doInBackground([Ljava/lang/Object;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected varargs doInBackground([Ljava/lang/Object;)Ljava/lang/Void;
    .locals 10
    .param p1, "params"    # [Ljava/lang/Object;

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 95
    aget-object v0, p1, v6

    check-cast v0, Landroid/content/Context;

    .line 96
    .local v0, "context":Landroid/content/Context;
    aget-object v3, p1, v5

    .line 97
    .local v3, "v":Ljava/lang/Object;
    const/4 v4, 0x2

    aget-object v1, p1, v4

    check-cast v1, Ljava/lang/Integer;

    .line 101
    .local v1, "direction":Ljava/lang/Integer;
    :cond_0
    :goto_0
    const-wide/16 v8, 0x190

    :try_start_0
    invoke-static {v8, v9}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 106
    :goto_1
    iget-boolean v4, p0, Lcom/samsung/musicplus/widget/control/FowardRewindControlTask;->mIsCancel:Z

    if-eqz v4, :cond_2

    .line 141
    iget-boolean v4, p0, Lcom/samsung/musicplus/widget/control/FowardRewindControlTask;->mIsInfoEventSent:Z

    if-eqz v4, :cond_1

    .line 142
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 153
    :cond_1
    :goto_2
    const/4 v4, 0x0

    return-object v4

    .line 102
    :catch_0
    move-exception v2

    .line 103
    .local v2, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v2}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1

    .line 110
    .end local v2    # "e":Ljava/lang/InterruptedException;
    :cond_2
    instance-of v4, v3, Landroid/view/View;

    if-eqz v4, :cond_3

    move-object v4, v3

    .line 113
    check-cast v4, Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->isPressed()Z

    move-result v4

    if-nez v4, :cond_4

    move v4, v5

    :goto_3
    iput-boolean v4, p0, Lcom/samsung/musicplus/widget/control/FowardRewindControlTask;->mIsCancel:Z

    .line 114
    const-string v7, "MusicControl"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "FF, REW Task second check isPressed ? "

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-boolean v4, p0, Lcom/samsung/musicplus/widget/control/FowardRewindControlTask;->mIsCancel:Z

    if-nez v4, :cond_5

    move v4, v5

    :goto_4
    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v7, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    :cond_3
    const-string v4, "MusicControl"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "FF, REW Task v ? "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " direction : "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 117
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    packed-switch v4, :pswitch_data_1

    goto :goto_0

    .line 119
    :pswitch_0
    new-instance v4, Landroid/content/Intent;

    const-string v7, "com.samsung.musicplus.action.PLAYBACK_FORWARD"

    invoke-direct {v4, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 120
    iget-boolean v4, p0, Lcom/samsung/musicplus/widget/control/FowardRewindControlTask;->mIsInfoEventSent:Z

    if-nez v4, :cond_0

    .line 122
    const-string v4, "fastforward_down"

    invoke-direct {p0, v0, v4}, Lcom/samsung/musicplus/widget/control/FowardRewindControlTask;->sendInfo(Landroid/content/Context;Ljava/lang/String;)V

    .line 123
    iput-boolean v5, p0, Lcom/samsung/musicplus/widget/control/FowardRewindControlTask;->mIsInfoEventSent:Z

    goto/16 :goto_0

    :cond_4
    move v4, v6

    .line 113
    goto :goto_3

    :cond_5
    move v4, v6

    .line 114
    goto :goto_4

    .line 127
    :pswitch_1
    new-instance v4, Landroid/content/Intent;

    const-string v7, "com.samsung.musicplus.action.PLAYBACK_REWIND"

    invoke-direct {v4, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 128
    iget-boolean v4, p0, Lcom/samsung/musicplus/widget/control/FowardRewindControlTask;->mIsInfoEventSent:Z

    if-nez v4, :cond_0

    .line 130
    const-string v4, "rewind_down"

    invoke-direct {p0, v0, v4}, Lcom/samsung/musicplus/widget/control/FowardRewindControlTask;->sendInfo(Landroid/content/Context;Ljava/lang/String;)V

    .line 131
    iput-boolean v5, p0, Lcom/samsung/musicplus/widget/control/FowardRewindControlTask;->mIsInfoEventSent:Z

    goto/16 :goto_0

    .line 144
    :pswitch_2
    const-string v4, "fastforward_up"

    invoke-direct {p0, v0, v4}, Lcom/samsung/musicplus/widget/control/FowardRewindControlTask;->sendInfo(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 147
    :pswitch_3
    const-string v4, "rewind_up"

    invoke-direct {p0, v0, v4}, Lcom/samsung/musicplus/widget/control/FowardRewindControlTask;->sendInfo(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_2

    .line 142
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 117
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setCancel()V
    .locals 1

    .prologue
    .line 89
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/widget/control/FowardRewindControlTask;->mIsCancel:Z

    .line 90
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/widget/control/FowardRewindControlTask;->cancel(Z)Z

    .line 91
    return-void
.end method
