.class public interface abstract Lcom/samsung/musicplus/widget/ActionModeCallback$OnActionModeListener;
.super Ljava/lang/Object;
.source "ActionModeCallback.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/widget/ActionModeCallback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnActionModeListener"
.end annotation


# virtual methods
.method public abstract onActionItemClicked(Landroid/view/MenuItem;[J[I)V
.end method

.method public abstract onDestroyActionMode()V
.end method

.method public abstract onInflateOptionsMenu(Landroid/view/Menu;)V
.end method

.method public abstract onPrepareActionMode(Landroid/view/Menu;)V
.end method
