.class Lcom/samsung/musicplus/widget/AnimatedToggleButton$Config;
.super Ljava/lang/Object;
.source "AnimatedToggleButton.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/widget/AnimatedToggleButton;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Config"
.end annotation


# instance fields
.field private buttonAnimationTime:I

.field private circleRadius:I

.field private edgeAnimationTime:I

.field private edgeColor:I

.field private edgeSize:I

.field private focusedImageA:I

.field private focusedImageB:I

.field private imageA:I

.field private imageB:I

.field private pressedImageA:I

.field private pressedImageB:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v3, -0x1

    .line 846
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 847
    sget-object v1, Lcom/sec/android/app/music/R$styleable;->AnimatedToggleButton:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 850
    .local v0, "attributes":Landroid/content/res/TypedArray;
    const/4 v1, 0x0

    const/4 v2, -0x1

    :try_start_0
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton$Config;->imageA:I

    .line 851
    const/4 v1, 0x1

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton$Config;->imageB:I

    .line 853
    iget v1, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton$Config;->imageA:I

    if-eq v1, v3, :cond_0

    iget v1, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton$Config;->imageB:I

    if-ne v1, v3, :cond_1

    .line 854
    :cond_0
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Could not obtain image resId from XML"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 884
    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    throw v1

    .line 857
    :cond_1
    const/4 v1, 0x2

    const/4 v2, -0x1

    :try_start_1
    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton$Config;->focusedImageA:I

    .line 859
    const/4 v1, 0x3

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton$Config;->focusedImageB:I

    .line 862
    const/4 v1, 0x4

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton$Config;->pressedImageA:I

    .line 864
    const/4 v1, 0x5

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton$Config;->pressedImageB:I

    .line 867
    const/4 v1, 0x6

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton$Config;->edgeSize:I

    .line 869
    const/16 v1, 0x8

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton$Config;->circleRadius:I

    .line 872
    iget v1, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton$Config;->circleRadius:I

    if-nez v1, :cond_2

    .line 873
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Could not obtain a valid circle radius from XML"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 876
    :cond_2
    const/16 v1, 0x9

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton$Config;->edgeColor:I

    .line 878
    const/16 v1, 0xa

    const/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton$Config;->edgeAnimationTime:I

    .line 880
    const/16 v1, 0xb

    const/16 v2, 0x28a

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton$Config;->buttonAnimationTime:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 884
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 887
    return-void
.end method


# virtual methods
.method public getButtonAnimationTime()I
    .locals 1

    .prologue
    .line 827
    iget v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton$Config;->buttonAnimationTime:I

    return v0
.end method

.method public getCircleRadius()I
    .locals 1

    .prologue
    .line 815
    iget v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton$Config;->circleRadius:I

    return v0
.end method

.method public getEdgeAnimationTime()I
    .locals 1

    .prologue
    .line 823
    iget v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton$Config;->edgeAnimationTime:I

    return v0
.end method

.method public getEdgeColor()I
    .locals 1

    .prologue
    .line 819
    iget v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton$Config;->edgeColor:I

    return v0
.end method

.method public getEdgeSize()I
    .locals 1

    .prologue
    .line 811
    iget v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton$Config;->edgeSize:I

    return v0
.end method

.method public getFocusedImageA()I
    .locals 1

    .prologue
    .line 831
    iget v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton$Config;->focusedImageA:I

    return v0
.end method

.method public getFocusedImageB()I
    .locals 1

    .prologue
    .line 835
    iget v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton$Config;->focusedImageB:I

    return v0
.end method

.method public getImageA()I
    .locals 1

    .prologue
    .line 803
    iget v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton$Config;->imageA:I

    return v0
.end method

.method public getImageB()I
    .locals 1

    .prologue
    .line 807
    iget v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton$Config;->imageB:I

    return v0
.end method

.method public getPressedImageA()I
    .locals 1

    .prologue
    .line 839
    iget v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton$Config;->pressedImageA:I

    return v0
.end method

.method public getPressedImageB()I
    .locals 1

    .prologue
    .line 843
    iget v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton$Config;->pressedImageB:I

    return v0
.end method
