.class public Lcom/samsung/musicplus/widget/tab/MusicViewPager;
.super Landroid/support/v4/view/ViewPager;
.source "MusicViewPager.java"


# instance fields
.field private mSwipeable:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 18
    invoke-direct {p0, p1}, Landroid/support/v4/view/ViewPager;-><init>(Landroid/content/Context;)V

    .line 15
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/widget/tab/MusicViewPager;->mSwipeable:Z

    .line 19
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attr"    # Landroid/util/AttributeSet;

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Landroid/support/v4/view/ViewPager;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 15
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/widget/tab/MusicViewPager;->mSwipeable:Z

    .line 23
    return-void
.end method


# virtual methods
.method public executeKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 45
    iget-boolean v0, p0, Lcom/samsung/musicplus/widget/tab/MusicViewPager;->mSwipeable:Z

    if-eqz v0, :cond_0

    .line 48
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 27
    iget-boolean v0, p0, Lcom/samsung/musicplus/widget/tab/MusicViewPager;->mSwipeable:Z

    if-eqz v0, :cond_0

    .line 32
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 1
    .param p1, "state"    # Landroid/os/Parcelable;

    .prologue
    .line 63
    const/4 v0, 0x0

    invoke-super {p0, v0}, Landroid/support/v4/view/ViewPager;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 64
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 37
    iget-boolean v0, p0, Lcom/samsung/musicplus/widget/tab/MusicViewPager;->mSwipeable:Z

    if-eqz v0, :cond_0

    .line 40
    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public setSwipeable(Z)V
    .locals 0
    .param p1, "swipeable"    # Z

    .prologue
    .line 52
    iput-boolean p1, p0, Lcom/samsung/musicplus/widget/tab/MusicViewPager;->mSwipeable:Z

    .line 53
    return-void
.end method
