.class public Lcom/samsung/musicplus/widget/content/PlaylistItemTask;
.super Landroid/os/AsyncTask;
.source "PlaylistItemTask.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/Object;",
        "Ljava/lang/Boolean;",
        ">;"
    }
.end annotation


# instance fields
.field private final DEBUG:Z

.field private final PLAYLISTITEM_PROJECTION:[Ljava/lang/String;

.field private final TAG:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mImageView:Landroid/widget/ImageView;

.field private mInfo:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/samsung/musicplus/widget/content/PlaylistItemProgress$PlaylistSongInfo;",
            ">;"
        }
    .end annotation
.end field

.field private mIsShowTotal:Z

.field private mPlaylistId:J

.field private mQueryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

.field private mText1:Landroid/widget/TextView;

.field private mText2:Landroid/widget/TextView;

.field private mViewType:I


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 58
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 37
    iput-boolean v3, p0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->mIsShowTotal:Z

    .line 49
    const-string v0, "MusicUi"

    iput-object v0, p0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->TAG:Ljava/lang/String;

    .line 51
    iput-boolean v3, p0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->DEBUG:Z

    .line 53
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "album_id"

    aput-object v2, v0, v1

    const-string v1, "duration"

    aput-object v1, v0, v3

    iput-object v0, p0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->PLAYLISTITEM_PROJECTION:[Ljava/lang/String;

    .line 59
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/widget/ImageView;Landroid/widget/TextView;JILjava/util/HashMap;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "image"    # Landroid/widget/ImageView;
    .param p3, "text"    # Landroid/widget/TextView;
    .param p4, "playlistId"    # J
    .param p6, "viewType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/widget/ImageView;",
            "Landroid/widget/TextView;",
            "JI",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/samsung/musicplus/widget/content/PlaylistItemProgress$PlaylistSongInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p7, "hash":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Lcom/samsung/musicplus/widget/content/PlaylistItemProgress$PlaylistSongInfo;>;"
    const/4 v3, 0x1

    .line 63
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 37
    iput-boolean v3, p0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->mIsShowTotal:Z

    .line 49
    const-string v0, "MusicUi"

    iput-object v0, p0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->TAG:Ljava/lang/String;

    .line 51
    iput-boolean v3, p0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->DEBUG:Z

    .line 53
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "album_id"

    aput-object v2, v0, v1

    const-string v1, "duration"

    aput-object v1, v0, v3

    iput-object v0, p0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->PLAYLISTITEM_PROJECTION:[Ljava/lang/String;

    .line 64
    iput-object p1, p0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->mContext:Landroid/content/Context;

    .line 65
    iput-object p2, p0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->mImageView:Landroid/widget/ImageView;

    .line 66
    iput-object p3, p0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->mText1:Landroid/widget/TextView;

    .line 67
    iput-wide p4, p0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->mPlaylistId:J

    .line 68
    iput p6, p0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->mViewType:I

    .line 69
    iput-object p7, p0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->mInfo:Ljava/util/HashMap;

    .line 70
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/widget/ImageView;Landroid/widget/TextView;JZLjava/util/HashMap;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "image"    # Landroid/widget/ImageView;
    .param p3, "text"    # Landroid/widget/TextView;
    .param p4, "playlistId"    # J
    .param p6, "isShowTotal"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/widget/ImageView;",
            "Landroid/widget/TextView;",
            "JZ",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/samsung/musicplus/widget/content/PlaylistItemProgress$PlaylistSongInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p7, "hash":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Lcom/samsung/musicplus/widget/content/PlaylistItemProgress$PlaylistSongInfo;>;"
    const/4 v3, 0x1

    .line 86
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 37
    iput-boolean v3, p0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->mIsShowTotal:Z

    .line 49
    const-string v0, "MusicUi"

    iput-object v0, p0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->TAG:Ljava/lang/String;

    .line 51
    iput-boolean v3, p0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->DEBUG:Z

    .line 53
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "album_id"

    aput-object v2, v0, v1

    const-string v1, "duration"

    aput-object v1, v0, v3

    iput-object v0, p0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->PLAYLISTITEM_PROJECTION:[Ljava/lang/String;

    .line 87
    iput-object p1, p0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->mContext:Landroid/content/Context;

    .line 88
    iput-object p2, p0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->mImageView:Landroid/widget/ImageView;

    .line 89
    iput-object p3, p0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->mText1:Landroid/widget/TextView;

    .line 90
    iput-wide p4, p0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->mPlaylistId:J

    .line 91
    iput-object p7, p0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->mInfo:Ljava/util/HashMap;

    .line 92
    iput-boolean p6, p0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->mIsShowTotal:Z

    .line 93
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/widget/ImageView;Landroid/widget/TextView;Landroid/widget/TextView;JILjava/util/HashMap;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "image"    # Landroid/widget/ImageView;
    .param p3, "text1"    # Landroid/widget/TextView;
    .param p4, "text2"    # Landroid/widget/TextView;
    .param p5, "playlistId"    # J
    .param p7, "viewType"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/widget/ImageView;",
            "Landroid/widget/TextView;",
            "Landroid/widget/TextView;",
            "JI",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Long;",
            "Lcom/samsung/musicplus/widget/content/PlaylistItemProgress$PlaylistSongInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p8, "hash":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Long;Lcom/samsung/musicplus/widget/content/PlaylistItemProgress$PlaylistSongInfo;>;"
    const/4 v3, 0x1

    .line 74
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 37
    iput-boolean v3, p0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->mIsShowTotal:Z

    .line 49
    const-string v0, "MusicUi"

    iput-object v0, p0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->TAG:Ljava/lang/String;

    .line 51
    iput-boolean v3, p0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->DEBUG:Z

    .line 53
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "album_id"

    aput-object v2, v0, v1

    const-string v1, "duration"

    aput-object v1, v0, v3

    iput-object v0, p0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->PLAYLISTITEM_PROJECTION:[Ljava/lang/String;

    .line 75
    iput-object p1, p0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->mContext:Landroid/content/Context;

    .line 76
    iput-object p2, p0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->mImageView:Landroid/widget/ImageView;

    .line 77
    iput-object p3, p0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->mText1:Landroid/widget/TextView;

    .line 78
    iput-object p4, p0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->mText2:Landroid/widget/TextView;

    .line 79
    iput-wide p5, p0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->mPlaylistId:J

    .line 80
    iput p7, p0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->mViewType:I

    .line 81
    iput-object p8, p0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->mInfo:Ljava/util/HashMap;

    .line 82
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/widget/content/PlaylistItemTask;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/content/PlaylistItemTask;
    .param p1, "x1"    # Z

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->updateView(Z)V

    return-void
.end method

.method private getArtworkInBackground(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/Object;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "iv"    # Landroid/widget/ImageView;
    .param p3, "artKey"    # Ljava/lang/Object;

    .prologue
    .line 267
    invoke-static {}, Lcom/samsung/musicplus/util/AlbumArtLoader;->getAlbumArtLoader()Lcom/samsung/musicplus/util/IAlbumArtLoader;

    move-result-object v0

    .line 268
    .local v0, "loader":Lcom/samsung/musicplus/util/IAlbumArtLoader;
    iget v1, p0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->mViewType:I

    if-nez v1, :cond_0

    .line 269
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0138

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 270
    .local v5, "size":I
    sget-object v2, Lcom/samsung/musicplus/util/AlbumArtUtils;->DEFAULT_ARTWORK_URI_STRING:Ljava/lang/String;

    const v6, 0x7f02003b

    move-object v1, p1

    move-object v3, p3

    move-object v4, p2

    invoke-interface/range {v0 .. v6}, Lcom/samsung/musicplus/util/IAlbumArtLoader;->loadArtwork(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Object;Landroid/widget/ImageView;II)V

    .line 277
    :goto_0
    return-void

    .line 273
    .end local v5    # "size":I
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0049

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v5

    .line 274
    .restart local v5    # "size":I
    sget-object v2, Lcom/samsung/musicplus/util/AlbumArtUtils;->DEFAULT_ARTWORK_URI_STRING:Ljava/lang/String;

    const v6, 0x7f02003a

    move-object v1, p1

    move-object v3, p3

    move-object v4, p2

    invoke-interface/range {v0 .. v6}, Lcom/samsung/musicplus/util/IAlbumArtLoader;->loadGridArtwork(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Object;Landroid/widget/ImageView;II)V

    goto :goto_0
.end method

.method private isSameTag()Z
    .locals 2

    .prologue
    .line 280
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 281
    .local v0, "code":Ljava/lang/Integer;
    iget-object v1, p0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->mText1:Landroid/widget/TextView;

    if-nez v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 282
    const/4 v1, 0x0

    .line 284
    :goto_1
    return v1

    .line 281
    :cond_0
    iget-object v1, p0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->mText1:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTag()Ljava/lang/Object;

    move-result-object v1

    goto :goto_0

    .line 284
    :cond_1
    const/4 v1, 0x1

    goto :goto_1
.end method

.method private updateView(Z)V
    .locals 14
    .param p1, "isPreExecute"    # Z

    .prologue
    .line 194
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->isSameTag()Z

    move-result v9

    if-eqz v9, :cond_0

    iget-object v9, p0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->mInfo:Ljava/util/HashMap;

    iget-wide v10, p0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->mPlaylistId:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_1

    .line 263
    :cond_0
    :goto_0
    return-void

    .line 197
    :cond_1
    const/4 v2, 0x0

    .line 198
    .local v2, "count":I
    const/4 v8, 0x0

    .line 199
    .local v8, "total":I
    const-wide/16 v0, -0x1

    .line 200
    .local v0, "albumId":J
    iget-object v9, p0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->mInfo:Ljava/util/HashMap;

    iget-wide v10, p0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->mPlaylistId:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/samsung/musicplus/widget/content/PlaylistItemProgress$PlaylistSongInfo;

    .line 201
    .local v6, "info":Lcom/samsung/musicplus/widget/content/PlaylistItemProgress$PlaylistSongInfo;
    if-eqz v6, :cond_2

    .line 202
    iget v2, v6, Lcom/samsung/musicplus/widget/content/PlaylistItemProgress$PlaylistSongInfo;->mCount:I

    .line 203
    iget-wide v10, v6, Lcom/samsung/musicplus/widget/content/PlaylistItemProgress$PlaylistSongInfo;->mTime:J

    long-to-int v8, v10

    .line 204
    iget-wide v0, v6, Lcom/samsung/musicplus/widget/content/PlaylistItemProgress$PlaylistSongInfo;->mAlbumId:J

    .line 207
    :cond_2
    const-string v9, "MusicUi"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "isPreExecute = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " playlistId = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-wide v12, p0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->mPlaylistId:J

    invoke-virtual {v10, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " hash = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " PlaylistSongInfo info = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " count = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " total = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " albumId = "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    if-lez v2, :cond_5

    .line 213
    iget-object v9, p0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f0f000a

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-virtual {v9, v10, v2, v11}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 217
    .local v3, "countText":Ljava/lang/String;
    :goto_1
    iget-object v9, p0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->mContext:Landroid/content/Context;

    int-to-long v10, v8

    invoke-static {v9, v10, v11}, Lcom/samsung/musicplus/util/UiUtils;->makeTimeString(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v5

    .line 218
    .local v5, "durationText":Ljava/lang/String;
    iget-object v9, p0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->mText2:Landroid/widget/TextView;

    if-eqz v9, :cond_6

    .line 219
    iget-object v9, p0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->mText1:Landroid/widget/TextView;

    invoke-virtual {v9, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 220
    iget-object v9, p0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->mText2:Landroid/widget/TextView;

    invoke-virtual {v9, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 232
    :goto_2
    iget-object v9, p0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->mImageView:Landroid/widget/ImageView;

    if-eqz v9, :cond_0

    .line 236
    const-wide/16 v10, -0x1

    cmp-long v9, v0, v10

    if-eqz v9, :cond_a

    .line 237
    iget-object v9, p0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v9}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v7

    .line 238
    .local v7, "tag":Ljava/lang/Object;
    if-eqz v7, :cond_3

    check-cast v7, Ljava/lang/Long;

    .end local v7    # "tag":Ljava/lang/Object;
    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    cmp-long v9, v10, v0

    if-eqz v9, :cond_4

    .line 240
    :cond_3
    iget v9, p0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->mViewType:I

    if-nez v9, :cond_8

    .line 241
    invoke-static {v0, v1}, Lcom/samsung/musicplus/util/AlbumArtUtils;->getCachedArtwork(J)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .line 245
    .local v4, "d":Landroid/graphics/drawable/Drawable;
    :goto_3
    if-nez v4, :cond_9

    .line 246
    iget-object v9, p0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->mContext:Landroid/content/Context;

    iget-object v10, p0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->mImageView:Landroid/widget/ImageView;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-direct {p0, v9, v10, v11}, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->getArtworkInBackground(Landroid/content/Context;Landroid/widget/ImageView;Ljava/lang/Object;)V

    .line 262
    .end local v4    # "d":Landroid/graphics/drawable/Drawable;
    :cond_4
    :goto_4
    iget-object v9, p0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->mImageView:Landroid/widget/ImageView;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 215
    .end local v3    # "countText":Ljava/lang/String;
    .end local v5    # "durationText":Ljava/lang/String;
    :cond_5
    iget-object v9, p0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f100106

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .restart local v3    # "countText":Ljava/lang/String;
    goto :goto_1

    .line 222
    .restart local v5    # "durationText":Ljava/lang/String;
    :cond_6
    iget-boolean v9, p0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->mIsShowTotal:Z

    if-eqz v9, :cond_7

    .line 223
    iget-object v9, p0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->mText1:Landroid/widget/TextView;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " / "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 224
    iget-object v9, p0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->mText1:Landroid/widget/TextView;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " / "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget-object v11, p0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->mContext:Landroid/content/Context;

    invoke-static {v11, v5}, Lcom/samsung/musicplus/util/UiUtils;->getDurationTalkback(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 227
    :cond_7
    iget-object v9, p0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->mText1:Landroid/widget/TextView;

    invoke-virtual {v9, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 243
    :cond_8
    invoke-static {v0, v1}, Lcom/samsung/musicplus/util/AlbumArtUtils;->getGridCachedArtwork(J)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    .restart local v4    # "d":Landroid/graphics/drawable/Drawable;
    goto :goto_3

    .line 248
    :cond_9
    iget-object v9, p0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->mImageView:Landroid/widget/ImageView;

    invoke-virtual {v9, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_4

    .line 252
    .end local v4    # "d":Landroid/graphics/drawable/Drawable;
    :cond_a
    iget-wide v10, p0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->mPlaylistId:J

    const-wide/16 v12, -0x10

    cmp-long v9, v10, v12

    if-nez v9, :cond_c

    .line 253
    iget v9, p0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->mViewType:I

    if-nez v9, :cond_b

    .line 254
    iget-object v9, p0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->mImageView:Landroid/widget/ImageView;

    const v10, 0x7f020033

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_4

    .line 256
    :cond_b
    iget-object v9, p0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->mImageView:Landroid/widget/ImageView;

    const v10, 0x7f020034

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_4

    .line 259
    :cond_c
    iget-object v9, p0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->mImageView:Landroid/widget/ImageView;

    const v10, 0x7f02003a

    invoke-virtual {v9, v10}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_4
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Object;)Ljava/lang/Boolean;
    .locals 20
    .param p1, "obj"    # [Ljava/lang/Object;

    .prologue
    .line 117
    invoke-direct/range {p0 .. p0}, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->isSameTag()Z

    move-result v3

    if-nez v3, :cond_1

    .line 118
    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    .line 172
    :cond_0
    :goto_0
    return-object v3

    .line 120
    :cond_1
    const/4 v11, 0x0

    .line 121
    .local v11, "count":I
    const-wide/16 v14, 0x0

    .line 122
    .local v14, "totalDuration":J
    const/16 v16, 0x0

    .line 123
    .local v16, "update":Z
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->mText1:Landroid/widget/TextView;

    if-eqz v3, :cond_c

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->mContext:Landroid/content/Context;

    if-eqz v3, :cond_c

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->mQueryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    iget-object v3, v3, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->uri:Landroid/net/Uri;

    if-eqz v3, :cond_c

    .line 124
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 125
    .local v2, "resolver":Landroid/content/ContentResolver;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->mQueryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    iget-object v3, v3, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->uri:Landroid/net/Uri;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->PLAYLISTITEM_PROJECTION:[Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->mQueryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v5, v0, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->selection:Ljava/lang/String;

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->mQueryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v7, v0, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->orderBy:Ljava/lang/String;

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 128
    .local v10, "c":Landroid/database/Cursor;
    const-wide/16 v8, -0x1

    .line 130
    .local v8, "albumId":J
    if-eqz v10, :cond_b

    :try_start_0
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_b

    .line 131
    invoke-interface {v10}, Landroid/database/Cursor;->getCount()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v5

    .line 132
    .end local v11    # "count":I
    .local v5, "count":I
    if-lez v5, :cond_a

    .line 133
    const-wide/16 v6, 0x0

    .line 135
    .end local v14    # "totalDuration":J
    .local v6, "totalDuration":J
    :goto_1
    const/4 v3, 0x0

    :try_start_1
    invoke-interface {v10, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v8

    .line 137
    :cond_2
    const/4 v3, 0x1

    invoke-interface {v10, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    .line 138
    .local v12, "duration":J
    const-wide/16 v18, 0x0

    cmp-long v3, v12, v18

    if-lez v3, :cond_3

    .line 139
    const-wide/16 v18, 0x3e8

    div-long v18, v12, v18

    add-long v6, v6, v18

    .line 141
    :cond_3
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_2

    .line 146
    .end local v12    # "duration":J
    :goto_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->mText1:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->isAttachedToWindow()Z

    move-result v3

    if-nez v3, :cond_4

    .line 147
    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v3

    .line 166
    if-eqz v10, :cond_0

    .line 167
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 149
    :cond_4
    :try_start_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->mInfo:Ljava/util/HashMap;

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->mPlaylistId:J

    move-wide/from16 v18, v0

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/musicplus/widget/content/PlaylistItemProgress$PlaylistSongInfo;

    .line 150
    .local v4, "info":Lcom/samsung/musicplus/widget/content/PlaylistItemProgress$PlaylistSongInfo;
    if-nez v4, :cond_7

    .line 151
    new-instance v4, Lcom/samsung/musicplus/widget/content/PlaylistItemProgress$PlaylistSongInfo;

    .end local v4    # "info":Lcom/samsung/musicplus/widget/content/PlaylistItemProgress$PlaylistSongInfo;
    invoke-direct/range {v4 .. v9}, Lcom/samsung/musicplus/widget/content/PlaylistItemProgress$PlaylistSongInfo;-><init>(IJJ)V

    .line 152
    .restart local v4    # "info":Lcom/samsung/musicplus/widget/content/PlaylistItemProgress$PlaylistSongInfo;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->mInfo:Ljava/util/HashMap;

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->mPlaylistId:J

    move-wide/from16 v18, v0

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v3, v0, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 153
    const/16 v16, 0x1

    .line 166
    :cond_5
    :goto_3
    if-eqz v10, :cond_6

    .line 167
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 172
    .end local v2    # "resolver":Landroid/content/ContentResolver;
    .end local v4    # "info":Lcom/samsung/musicplus/widget/content/PlaylistItemProgress$PlaylistSongInfo;
    .end local v8    # "albumId":J
    .end local v10    # "c":Landroid/database/Cursor;
    :cond_6
    :goto_4
    invoke-static/range {v16 .. v16}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    goto/16 :goto_0

    .line 156
    .restart local v2    # "resolver":Landroid/content/ContentResolver;
    .restart local v4    # "info":Lcom/samsung/musicplus/widget/content/PlaylistItemProgress$PlaylistSongInfo;
    .restart local v8    # "albumId":J
    .restart local v10    # "c":Landroid/database/Cursor;
    :cond_7
    :try_start_3
    iget v3, v4, Lcom/samsung/musicplus/widget/content/PlaylistItemProgress$PlaylistSongInfo;->mCount:I

    if-ne v3, v5, :cond_8

    iget-wide v0, v4, Lcom/samsung/musicplus/widget/content/PlaylistItemProgress$PlaylistSongInfo;->mTime:J

    move-wide/from16 v18, v0

    cmp-long v3, v18, v6

    if-nez v3, :cond_8

    iget-wide v0, v4, Lcom/samsung/musicplus/widget/content/PlaylistItemProgress$PlaylistSongInfo;->mAlbumId:J

    move-wide/from16 v18, v0

    cmp-long v3, v18, v8

    if-eqz v3, :cond_5

    .line 158
    :cond_8
    iput v5, v4, Lcom/samsung/musicplus/widget/content/PlaylistItemProgress$PlaylistSongInfo;->mCount:I

    .line 159
    iput-wide v6, v4, Lcom/samsung/musicplus/widget/content/PlaylistItemProgress$PlaylistSongInfo;->mTime:J

    .line 160
    iput-wide v8, v4, Lcom/samsung/musicplus/widget/content/PlaylistItemProgress$PlaylistSongInfo;->mAlbumId:J

    .line 161
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->mInfo:Ljava/util/HashMap;

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->mPlaylistId:J

    move-wide/from16 v18, v0

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v3, v0, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 162
    const/16 v16, 0x1

    goto :goto_3

    .line 166
    .end local v4    # "info":Lcom/samsung/musicplus/widget/content/PlaylistItemProgress$PlaylistSongInfo;
    .end local v5    # "count":I
    .end local v6    # "totalDuration":J
    .restart local v11    # "count":I
    .restart local v14    # "totalDuration":J
    :catchall_0
    move-exception v3

    move-wide v6, v14

    .end local v14    # "totalDuration":J
    .restart local v6    # "totalDuration":J
    move v5, v11

    .end local v11    # "count":I
    .restart local v5    # "count":I
    :goto_5
    if-eqz v10, :cond_9

    .line 167
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    :cond_9
    throw v3

    .line 166
    :catchall_1
    move-exception v3

    goto :goto_5

    .end local v6    # "totalDuration":J
    .restart local v14    # "totalDuration":J
    :cond_a
    move-wide v6, v14

    .end local v14    # "totalDuration":J
    .restart local v6    # "totalDuration":J
    goto/16 :goto_1

    .end local v5    # "count":I
    .end local v6    # "totalDuration":J
    .restart local v11    # "count":I
    .restart local v14    # "totalDuration":J
    :cond_b
    move-wide v6, v14

    .end local v14    # "totalDuration":J
    .restart local v6    # "totalDuration":J
    move v5, v11

    .end local v11    # "count":I
    .restart local v5    # "count":I
    goto/16 :goto_2

    .end local v2    # "resolver":Landroid/content/ContentResolver;
    .end local v5    # "count":I
    .end local v6    # "totalDuration":J
    .end local v8    # "albumId":J
    .end local v10    # "c":Landroid/database/Cursor;
    .restart local v11    # "count":I
    .restart local v14    # "totalDuration":J
    :cond_c
    move-wide v6, v14

    .end local v14    # "totalDuration":J
    .restart local v6    # "totalDuration":J
    move v5, v11

    .end local v11    # "count":I
    .restart local v5    # "count":I
    goto :goto_4
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 29
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->doInBackground([Ljava/lang/Object;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Ljava/lang/Boolean;)V
    .locals 4
    .param p1, "result"    # Ljava/lang/Boolean;

    .prologue
    .line 177
    invoke-super {p0, p1}, Landroid/os/AsyncTask;->onPostExecute(Ljava/lang/Object;)V

    .line 183
    iget-object v0, p0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 184
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/samsung/musicplus/widget/content/PlaylistItemTask$1;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/widget/content/PlaylistItemTask$1;-><init>(Lcom/samsung/musicplus/widget/content/PlaylistItemTask;)V

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 191
    :cond_0
    return-void
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 29
    check-cast p1, Ljava/lang/Boolean;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->onPostExecute(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 4

    .prologue
    .line 97
    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    .line 100
    iget-object v0, p0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_0

    .line 101
    iget-object v0, p0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/samsung/musicplus/util/UiUtils;->getFavorietListId(Landroid/content/Context;)J

    .line 103
    :cond_0
    new-instance v0, Lcom/samsung/musicplus/util/ListUtils$PlaylistTrackQueryArgs;

    iget-wide v2, p0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->mPlaylistId:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/musicplus/util/ListUtils$PlaylistTrackQueryArgs;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->mQueryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    .line 106
    iget-object v0, p0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->mQueryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    iget-object v0, v0, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->uri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "-1"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 107
    const-string v0, "MusicUi"

    const-string v1, "Music unknown issue(-1) occured : error."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    iget-object v0, p0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->mQueryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->uri:Landroid/net/Uri;

    .line 110
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->mText1:Landroid/widget/TextView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_2

    .line 111
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/widget/content/PlaylistItemTask;->updateView(Z)V

    .line 113
    :cond_2
    return-void
.end method
