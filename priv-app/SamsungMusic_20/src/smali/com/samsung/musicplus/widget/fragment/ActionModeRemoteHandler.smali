.class public Lcom/samsung/musicplus/widget/fragment/ActionModeRemoteHandler;
.super Ljava/lang/Object;
.source "ActionModeRemoteHandler.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/widget/fragment/ActionModeRemoteHandler$ActionModeRemoteHandle;
    }
.end annotation


# static fields
.field private static final mRemoteHandleList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/samsung/musicplus/widget/fragment/ActionModeRemoteHandler$ActionModeRemoteHandle;",
            ">;>;"
        }
    .end annotation
.end field

.field private static final sInstance:Lcom/samsung/musicplus/widget/fragment/ActionModeRemoteHandler;


# instance fields
.field private mLastActionModeState:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    new-instance v0, Lcom/samsung/musicplus/widget/fragment/ActionModeRemoteHandler;

    invoke-direct {v0}, Lcom/samsung/musicplus/widget/fragment/ActionModeRemoteHandler;-><init>()V

    sput-object v0, Lcom/samsung/musicplus/widget/fragment/ActionModeRemoteHandler;->sInstance:Lcom/samsung/musicplus/widget/fragment/ActionModeRemoteHandler;

    .line 17
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/samsung/musicplus/widget/fragment/ActionModeRemoteHandler;->mRemoteHandleList:Ljava/util/ArrayList;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/widget/fragment/ActionModeRemoteHandler;->mLastActionModeState:Z

    .line 27
    return-void
.end method

.method public static getInstance()Lcom/samsung/musicplus/widget/fragment/ActionModeRemoteHandler;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/samsung/musicplus/widget/fragment/ActionModeRemoteHandler;->sInstance:Lcom/samsung/musicplus/widget/fragment/ActionModeRemoteHandler;

    return-object v0
.end method


# virtual methods
.method public isLastActionMode()Z
    .locals 1

    .prologue
    .line 64
    iget-boolean v0, p0, Lcom/samsung/musicplus/widget/fragment/ActionModeRemoteHandler;->mLastActionModeState:Z

    return v0
.end method

.method public notifyActionModeFinishAction()V
    .locals 4

    .prologue
    .line 53
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/samsung/musicplus/widget/fragment/ActionModeRemoteHandler;->mLastActionModeState:Z

    .line 54
    sget-object v3, Lcom/samsung/musicplus/widget/fragment/ActionModeRemoteHandler;->mRemoteHandleList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/ref/WeakReference;

    .line 55
    .local v2, "wRefHandle":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/samsung/musicplus/widget/fragment/ActionModeRemoteHandler$ActionModeRemoteHandle;>;"
    invoke-virtual {v2}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/widget/fragment/ActionModeRemoteHandler$ActionModeRemoteHandle;

    .line 56
    .local v0, "handle":Lcom/samsung/musicplus/widget/fragment/ActionModeRemoteHandler$ActionModeRemoteHandle;
    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/samsung/musicplus/widget/fragment/ActionModeRemoteHandler$ActionModeRemoteHandle;->isFragmentAdded()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Lcom/samsung/musicplus/widget/fragment/ActionModeRemoteHandler$ActionModeRemoteHandle;->isCurrentActionMode()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 57
    invoke-interface {v0}, Lcom/samsung/musicplus/widget/fragment/ActionModeRemoteHandler$ActionModeRemoteHandle;->setFinishActionMode()V

    goto :goto_0

    .line 60
    .end local v0    # "handle":Lcom/samsung/musicplus/widget/fragment/ActionModeRemoteHandler$ActionModeRemoteHandle;
    .end local v2    # "wRefHandle":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/samsung/musicplus/widget/fragment/ActionModeRemoteHandler$ActionModeRemoteHandle;>;"
    :cond_1
    sget-object v3, Lcom/samsung/musicplus/widget/fragment/ActionModeRemoteHandler;->mRemoteHandleList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 61
    return-void
.end method

.method public registerActionModeHandle(Lcom/samsung/musicplus/widget/fragment/ActionModeRemoteHandler$ActionModeRemoteHandle;)V
    .locals 2
    .param p1, "handle"    # Lcom/samsung/musicplus/widget/fragment/ActionModeRemoteHandler$ActionModeRemoteHandle;

    .prologue
    .line 30
    if-nez p1, :cond_0

    .line 36
    :goto_0
    return-void

    .line 33
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/widget/fragment/ActionModeRemoteHandler;->mLastActionModeState:Z

    .line 34
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/widget/fragment/ActionModeRemoteHandler;->unregisterActionModeHandle(Lcom/samsung/musicplus/widget/fragment/ActionModeRemoteHandler$ActionModeRemoteHandle;)V

    .line 35
    sget-object v0, Lcom/samsung/musicplus/widget/fragment/ActionModeRemoteHandler;->mRemoteHandleList:Ljava/util/ArrayList;

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public unregisterActionModeHandle(Lcom/samsung/musicplus/widget/fragment/ActionModeRemoteHandler$ActionModeRemoteHandle;)V
    .locals 3
    .param p1, "handle"    # Lcom/samsung/musicplus/widget/fragment/ActionModeRemoteHandler$ActionModeRemoteHandle;

    .prologue
    .line 39
    if-nez p1, :cond_1

    .line 50
    :cond_0
    :goto_0
    return-void

    .line 43
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    sget-object v2, Lcom/samsung/musicplus/widget/fragment/ActionModeRemoteHandler;->mRemoteHandleList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 44
    sget-object v2, Lcom/samsung/musicplus/widget/fragment/ActionModeRemoteHandler;->mRemoteHandleList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/ref/WeakReference;

    .line 45
    .local v1, "wRefHandle":Ljava/lang/ref/WeakReference;, "Ljava/lang/ref/WeakReference<Lcom/samsung/musicplus/widget/fragment/ActionModeRemoteHandler$ActionModeRemoteHandle;>;"
    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 46
    sget-object v2, Lcom/samsung/musicplus/widget/fragment/ActionModeRemoteHandler;->mRemoteHandleList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    .line 43
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method
