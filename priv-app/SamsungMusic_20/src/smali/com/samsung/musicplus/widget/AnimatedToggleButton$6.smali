.class Lcom/samsung/musicplus/widget/AnimatedToggleButton$6;
.super Ljava/lang/Object;
.source "AnimatedToggleButton.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/widget/AnimatedToggleButton;->initEdgeAnimation()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/widget/AnimatedToggleButton;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/widget/AnimatedToggleButton;)V
    .locals 0

    .prologue
    .line 599
    iput-object p1, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton$6;->this$0:Lcom/samsung/musicplus/widget/AnimatedToggleButton;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 8
    .param p1, "valueAnimator"    # Landroid/animation/ValueAnimator;

    .prologue
    .line 604
    const/high16 v1, -0x3f800000    # -4.0f

    iget-object v2, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton$6;->this$0:Lcom/samsung/musicplus/widget/AnimatedToggleButton;

    # getter for: Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mMaxEdgeWidth:F
    invoke-static {v2}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->access$1400(Lcom/samsung/musicplus/widget/AnimatedToggleButton;)F

    move-result v2

    mul-float/2addr v1, v2

    float-to-double v2, v1

    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedFraction()F

    move-result v1

    const/high16 v4, 0x3f000000    # 0.5f

    sub-float/2addr v1, v4

    float-to-double v4, v1

    const-wide/high16 v6, 0x4000000000000000L    # 2.0

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v4

    mul-double/2addr v2, v4

    iget-object v1, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton$6;->this$0:Lcom/samsung/musicplus/widget/AnimatedToggleButton;

    # getter for: Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mMaxEdgeWidth:F
    invoke-static {v1}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->access$1400(Lcom/samsung/musicplus/widget/AnimatedToggleButton;)F

    move-result v1

    float-to-double v4, v1

    add-double/2addr v2, v4

    double-to-float v0, v2

    .line 607
    .local v0, "result":F
    iget-object v1, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton$6;->this$0:Lcom/samsung/musicplus/widget/AnimatedToggleButton;

    # invokes: Lcom/samsung/musicplus/widget/AnimatedToggleButton;->updateEdgeWidth(F)V
    invoke-static {v1, v0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->access$900(Lcom/samsung/musicplus/widget/AnimatedToggleButton;F)V

    .line 608
    iget-object v1, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton$6;->this$0:Lcom/samsung/musicplus/widget/AnimatedToggleButton;

    invoke-virtual {v1}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->invalidate()V

    .line 609
    return-void
.end method
