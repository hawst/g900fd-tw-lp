.class public Lcom/samsung/musicplus/widget/tab/ActionBarTabTalkBack;
.super Lcom/samsung/musicplus/widget/tab/AbsTabTalkBack;
.source "ActionBarTabTalkBack.java"


# instance fields
.field private final mActionBar:Landroid/app/ActionBar;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/app/ActionBar;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "bar"    # Landroid/app/ActionBar;

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/widget/tab/AbsTabTalkBack;-><init>(Landroid/content/Context;)V

    .line 20
    iput-object p2, p0, Lcom/samsung/musicplus/widget/tab/ActionBarTabTalkBack;->mActionBar:Landroid/app/ActionBar;

    .line 21
    new-instance v0, Lcom/samsung/musicplus/widget/tab/ActionBarTabTalkBack$1;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/widget/tab/ActionBarTabTalkBack$1;-><init>(Lcom/samsung/musicplus/widget/tab/ActionBarTabTalkBack;)V

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/widget/tab/ActionBarTabTalkBack;->setAccessibilityEventRunnable(Ljava/lang/Runnable;)V

    .line 31
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/widget/tab/ActionBarTabTalkBack;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/tab/ActionBarTabTalkBack;

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/tab/ActionBarTabTalkBack;->getCurrentTabView()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private getCurrentTabView()Landroid/view/View;
    .locals 7

    .prologue
    .line 78
    const/4 v3, 0x0

    .line 80
    .local v3, "v":Landroid/view/View;
    :try_start_0
    iget-object v4, p0, Lcom/samsung/musicplus/widget/tab/ActionBarTabTalkBack;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    const-string v5, "twGetSelectedTabView"

    const/4 v6, 0x0

    new-array v6, v6, [Ljava/lang/Class;

    invoke-virtual {v4, v5, v6}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    .line 82
    .local v2, "twGetSelectedTabView":Ljava/lang/reflect/Method;
    if-eqz v2, :cond_0

    .line 83
    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Ljava/lang/reflect/Method;->setAccessible(Z)V

    .line 84
    iget-object v4, p0, Lcom/samsung/musicplus/widget/tab/ActionBarTabTalkBack;->mActionBar:Landroid/app/ActionBar;

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v2, v4, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    move-object v0, v4

    check-cast v0, Landroid/view/View;

    move-object v3, v0
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_3

    .line 95
    .end local v2    # "twGetSelectedTabView":Ljava/lang/reflect/Method;
    :cond_0
    :goto_0
    return-object v3

    .line 86
    :catch_0
    move-exception v1

    .line 87
    .local v1, "e":Ljava/lang/NoSuchMethodException;
    const-string v4, "MusicTabTalkBack"

    const-string v5, "getCurrentTabView() - NoSuchMethodException"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 88
    .end local v1    # "e":Ljava/lang/NoSuchMethodException;
    :catch_1
    move-exception v1

    .line 89
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    const-string v4, "MusicTabTalkBack"

    const-string v5, "getCurrentTabView() - IllegalArgumentException"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 90
    .end local v1    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v1

    .line 91
    .local v1, "e":Ljava/lang/IllegalAccessException;
    const-string v4, "MusicTabTalkBack"

    const-string v5, "getCurrentTabView() - IllegalAccessException"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 92
    .end local v1    # "e":Ljava/lang/IllegalAccessException;
    :catch_3
    move-exception v1

    .line 93
    .local v1, "e":Ljava/lang/reflect/InvocationTargetException;
    const-string v4, "MusicTabTalkBack"

    const-string v5, "getCurrentTabView() - InvocationTargetException"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public getSelectedTabTts(Landroid/app/ActionBar$Tab;)Ljava/lang/String;
    .locals 3
    .param p1, "tab"    # Landroid/app/ActionBar$Tab;

    .prologue
    .line 68
    invoke-virtual {p1}, Landroid/app/ActionBar$Tab;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1}, Landroid/app/ActionBar$Tab;->getPosition()I

    move-result v1

    iget-object v2, p0, Lcom/samsung/musicplus/widget/tab/ActionBarTabTalkBack;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v2}, Landroid/app/ActionBar;->getTabCount()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/samsung/musicplus/widget/tab/ActionBarTabTalkBack;->getSelectedTabTts(Ljava/lang/CharSequence;II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUnselectedTabTts(Landroid/app/ActionBar$Tab;)Ljava/lang/String;
    .locals 3
    .param p1, "tab"    # Landroid/app/ActionBar$Tab;

    .prologue
    .line 56
    invoke-virtual {p1}, Landroid/app/ActionBar$Tab;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1}, Landroid/app/ActionBar$Tab;->getPosition()I

    move-result v1

    iget-object v2, p0, Lcom/samsung/musicplus/widget/tab/ActionBarTabTalkBack;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v2}, Landroid/app/ActionBar;->getTabCount()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/samsung/musicplus/widget/tab/ActionBarTabTalkBack;->getUnselectedTabTts(Ljava/lang/CharSequence;II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setTabsTts(I)V
    .locals 4
    .param p1, "position"    # I

    .prologue
    .line 35
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/tab/AbsTabTalkBack;->setTabsTts(I)V

    .line 36
    iget-object v3, p0, Lcom/samsung/musicplus/widget/tab/ActionBarTabTalkBack;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v3}, Landroid/app/ActionBar;->getTabCount()I

    move-result v2

    .line 37
    .local v2, "tabCount":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_0

    .line 38
    iget-object v3, p0, Lcom/samsung/musicplus/widget/tab/ActionBarTabTalkBack;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v3, v0}, Landroid/app/ActionBar;->getTabAt(I)Landroid/app/ActionBar$Tab;

    move-result-object v1

    .line 39
    .local v1, "tab":Landroid/app/ActionBar$Tab;
    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/widget/tab/ActionBarTabTalkBack;->getUnselectedTabTts(Landroid/app/ActionBar$Tab;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/app/ActionBar$Tab;->setContentDescription(Ljava/lang/CharSequence;)Landroid/app/ActionBar$Tab;

    .line 37
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 41
    .end local v1    # "tab":Landroid/app/ActionBar$Tab;
    :cond_0
    const/4 v3, -0x1

    if-le p1, v3, :cond_1

    .line 42
    iget-object v3, p0, Lcom/samsung/musicplus/widget/tab/ActionBarTabTalkBack;->mActionBar:Landroid/app/ActionBar;

    invoke-virtual {v3, p1}, Landroid/app/ActionBar;->getTabAt(I)Landroid/app/ActionBar$Tab;

    move-result-object v1

    .line 43
    .restart local v1    # "tab":Landroid/app/ActionBar$Tab;
    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/widget/tab/ActionBarTabTalkBack;->getSelectedTabTts(Landroid/app/ActionBar$Tab;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/app/ActionBar$Tab;->setContentDescription(Ljava/lang/CharSequence;)Landroid/app/ActionBar$Tab;

    .line 45
    .end local v1    # "tab":Landroid/app/ActionBar$Tab;
    :cond_1
    return-void
.end method
