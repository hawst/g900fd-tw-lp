.class public Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;
.super Lcom/samsung/musicplus/widget/fragment/CommonListFragment;
.source "CommonSelectionListFragment.java"

# interfaces
.implements Lcom/samsung/musicplus/widget/ActionModeCallback$OnActionModeListener;
.implements Lcom/samsung/musicplus/widget/fragment/ActionModeRemoteHandler$ActionModeRemoteHandle;
.implements Lcom/samsung/musicplus/widget/fragment/OnSconnectCallback;
.implements Lcom/samsung/musicplus/widget/tab/OnTabListener;


# static fields
.field public static final ACTION_DELETE:Ljava/lang/String; = "com.samsung.musicplus.delete"

.field private static final ACTION_MODE:Ljava/lang/String; = "action_mode"

.field private static final DELETE_MODE:Ljava/lang/String; = "delete_mode"

.field private static final LIST_COUNT:Ljava/lang/String; = "count"

.field private static final LIST_POSITION:Ljava/lang/String; = "positions"

.field private static final POPUP_STATE:Ljava/lang/String; = "popup"

.field private static final RESTORE_MODE:Ljava/lang/String; = "restore_mode"


# instance fields
.field private mActionModeCallBack:Lcom/samsung/musicplus/widget/ActionModeCallback;

.field private mAddToFavorites:Landroid/view/MenuItem;

.field private mAddToPersonal:Landroid/view/MenuItem;

.field private mAddToPlayList:Landroid/view/MenuItem;

.field private mDelete:Landroid/view/MenuItem;

.field private mDetails:Landroid/view/MenuItem;

.field private mDone:Landroid/view/MenuItem;

.field private mFavoriteSelectionIds:[J

.field private mHasSelectableItem:Z

.field private mInternalLoadFinished:Z

.field private mIsActionMode:Z

.field private mIsDeleteMode:Z

.field private mIsPopupShown:Z

.field private mIsRestoreMode:Z

.field private mKeyListener:Landroid/content/BroadcastReceiver;

.field private mKnoxGate:Lcom/samsung/musicplus/util/KnoxGateManager;

.field private mListCount:I

.field private mRemoveFromFavorites:Landroid/view/MenuItem;

.field private mReoveFromPersonal:Landroid/view/MenuItem;

.field private mSelectedPositions:[I

.field private mSelectionAniUtil:Lcom/samsung/musicplus/util/SelectionAnimationUtil;

.field private mSetAs:Landroid/view/MenuItem;

.field mTwMultiSelectedListener:Landroid/widget/AdapterView$OnTwMultiSelectedListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 78
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;-><init>()V

    .line 83
    iput-boolean v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mIsRestoreMode:Z

    .line 85
    iput-boolean v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mIsActionMode:Z

    .line 87
    iput-boolean v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mIsDeleteMode:Z

    .line 89
    iput-boolean v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mInternalLoadFinished:Z

    .line 91
    iput-boolean v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mHasSelectableItem:Z

    .line 93
    iput-boolean v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mIsPopupShown:Z

    .line 854
    new-instance v0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment$3;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment$3;-><init>(Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;)V

    iput-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mKeyListener:Landroid/content/BroadcastReceiver;

    .line 999
    new-instance v0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment$4;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment$4;-><init>(Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;)V

    iput-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mTwMultiSelectedListener:Landroid/widget/AdapterView$OnTwMultiSelectedListener;

    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;)Lcom/samsung/musicplus/widget/ActionModeCallback;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;

    .prologue
    .line 78
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mActionModeCallBack:Lcom/samsung/musicplus/widget/ActionModeCallback;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;[J[I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;
    .param p1, "x1"    # [J
    .param p2, "x2"    # [I

    .prologue
    .line 78
    invoke-direct {p0, p1, p2}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->deleteItem([J[I)V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;

    .prologue
    .line 78
    iget-boolean v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mIsActionMode:Z

    return v0
.end method

.method static synthetic access$300(Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;Landroid/widget/AbsListView;[I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;
    .param p1, "x1"    # Landroid/widget/AbsListView;
    .param p2, "x2"    # [I

    .prologue
    .line 78
    invoke-direct {p0, p1, p2}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->startActionMode(Landroid/widget/AbsListView;[I)V

    return-void
.end method

.method private addToFavorites([J)V
    .locals 6
    .param p1, "selectedItemIds"    # [J

    .prologue
    const/4 v5, 0x0

    .line 870
    new-instance v0, Lcom/samsung/musicplus/util/FileOperationTask$SmartFavoriteTask;

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v3, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mFavoriteSelectionIds:[J

    const/4 v4, 0x1

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/samsung/musicplus/util/FileOperationTask$SmartFavoriteTask;-><init>(Landroid/app/Activity;[J[JZZ)V

    new-array v1, v5, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/util/FileOperationTask$SmartFavoriteTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 872
    return-void
.end method

.method private addtoPlaylist([J[I)V
    .locals 4
    .param p1, "selectedItemIds"    # [J
    .param p2, "selectedPositions"    # [I

    .prologue
    const/4 v3, 0x2

    .line 811
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/samsung/musicplus/contents/extra/MusicSelectListActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 812
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "list_type"

    const v2, 0x10004

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 813
    const-string v1, "list_mode"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 814
    const-string v1, "header_mode"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 816
    iget v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mList:I

    invoke-static {v1}, Lcom/samsung/musicplus/util/ListUtils;->isTab(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 817
    const-string v1, "selected_id"

    iget v2, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mList:I

    invoke-direct {p0, v2, p2}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->getSongList(I[I)[J

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[J)Landroid/content/Intent;

    .line 825
    :goto_0
    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->startActivity(Landroid/content/Intent;)V

    .line 826
    return-void

    .line 820
    :cond_0
    iget v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mList:I

    const v2, 0x20001

    if-ne v1, v2, :cond_1

    .line 821
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->getAlignedSelectedItemIds()[J

    move-result-object p1

    .line 823
    :cond_1
    const-string v1, "selected_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[J)Landroid/content/Intent;

    goto :goto_0
.end method

.method private deleteItem([J[I)V
    .locals 6
    .param p1, "selectedItemIds"    # [J
    .param p2, "selectedPositions"    # [I

    .prologue
    .line 829
    array-length v2, p1

    .line 831
    .local v2, "selectedItemCounts":I
    iget v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mList:I

    invoke-static {v0}, Lcom/samsung/musicplus/util/ListUtils;->isTab(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mList:I

    invoke-static {v0}, Lcom/samsung/musicplus/util/ListUtils;->isPlaylistTabContent(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 832
    iget v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mList:I

    invoke-direct {p0, v0, p2}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->getSongList(I[I)[J

    move-result-object p1

    .line 834
    :cond_0
    new-instance v0, Lcom/samsung/musicplus/dialog/DeleteDialogFragment;

    iget v3, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mList:I

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->getFragment()Landroid/app/Fragment;

    move-result-object v5

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/samsung/musicplus/dialog/DeleteDialogFragment;-><init>([JIIZLandroid/app/Fragment;)V

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v3, "deleteDialog"

    invoke-virtual {v0, v1, v3}, Lcom/samsung/musicplus/dialog/DeleteDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 836
    return-void
.end method

.method private getAlignedSelectedItemIds()[J
    .locals 10

    .prologue
    .line 1081
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->getAbsListView()Landroid/widget/AbsListView;

    move-result-object v4

    .line 1082
    .local v4, "lv":Landroid/widget/AbsListView;
    invoke-virtual {v4}, Landroid/widget/AbsListView;->getCheckedItemPositions()Landroid/util/SparseBooleanArray;

    move-result-object v6

    .line 1083
    .local v6, "sp":Landroid/util/SparseBooleanArray;
    if-nez v6, :cond_1

    .line 1084
    const/4 v7, 0x0

    new-array v3, v7, [J

    .line 1099
    :cond_0
    return-object v3

    .line 1086
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1087
    .local v2, "idStates":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Long;>;"
    invoke-virtual {v6}, Landroid/util/SparseBooleanArray;->size()I

    move-result v5

    .line 1088
    .local v5, "size":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v5, :cond_3

    .line 1089
    invoke-virtual {v6, v1}, Landroid/util/SparseBooleanArray;->valueAt(I)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 1090
    invoke-virtual {v6, v1}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    move-result v7

    invoke-virtual {v4, v7}, Landroid/widget/AbsListView;->getItemIdAtPosition(I)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1088
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1093
    :cond_3
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 1094
    .local v0, "count":I
    new-array v3, v0, [J

    .line 1096
    .local v3, "ids":[J
    const/4 v1, 0x0

    :goto_1
    if-ge v1, v0, :cond_0

    .line 1097
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Long;

    invoke-virtual {v7}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    aput-wide v8, v3, v1

    .line 1096
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private getSongList(I[I)[J
    .locals 11
    .param p1, "list"    # I
    .param p2, "positions"    # [I

    .prologue
    const/4 v0, 0x0

    .line 261
    if-nez p2, :cond_1

    .line 262
    new-array v0, v0, [J

    .line 309
    :cond_0
    :goto_0
    return-object v0

    .line 265
    :cond_1
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 266
    .local v8, "keywords":Ljava/lang/StringBuilder;
    array-length v9, p2

    .line 267
    .local v9, "length":I
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_1
    if-ge v7, v9, :cond_3

    .line 268
    const-string v0, "\'"

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 269
    aget v0, p2, v7

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->getKeyWord(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 270
    const-string v0, "\'"

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 271
    add-int/lit8 v0, v9, -0x1

    if-ge v7, v0, :cond_2

    .line 272
    const-string v0, ","

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 267
    :cond_2
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 276
    :cond_3
    const/4 v10, 0x0

    .line 277
    .local v10, "selectionGroup":Ljava/lang/String;
    invoke-static {p1}, Lcom/samsung/musicplus/util/ListUtils;->getPreDefinedList(I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 298
    :goto_2
    :pswitch_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " IN ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " ) AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "is_music"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "=1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 300
    .local v3, "selction":Ljava/lang/String;
    const/4 v6, 0x0

    .line 302
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/musicplus/provider/MusicContents;->MUSIC_CONTENTS_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "_id"

    aput-object v5, v2, v4

    const/4 v4, 0x0

    const-string v5, "track"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 306
    invoke-static {v6}, Lcom/samsung/musicplus/util/ListUtils;->getSongListForCursor(Landroid/database/Cursor;)[J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 308
    if-eqz v6, :cond_0

    .line 309
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 279
    .end local v3    # "selction":Ljava/lang/String;
    .end local v6    # "c":Landroid/database/Cursor;
    :pswitch_1
    const-string v10, "album_id"

    .line 281
    goto :goto_2

    .line 283
    :pswitch_2
    const-string v10, "artist_id"

    .line 285
    goto :goto_2

    .line 287
    :pswitch_3
    const-string v10, "genre_name"

    .line 288
    goto :goto_2

    .line 290
    :pswitch_4
    const-string v10, "bucket_id"

    .line 291
    goto :goto_2

    .line 293
    :pswitch_5
    const-string v10, "composer"

    goto :goto_2

    .line 308
    .restart local v3    # "selction":Ljava/lang/String;
    .restart local v6    # "c":Landroid/database/Cursor;
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_4

    .line 309
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v0

    .line 277
    :pswitch_data_0
    .packed-switch 0x10002
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private handlePersonalModeAction([JZ)V
    .locals 6
    .param p1, "selectedItemIds"    # [J
    .param p2, "isMove"    # Z

    .prologue
    .line 956
    iget v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mList:I

    const v1, 0x20007

    if-ne v0, v1, :cond_0

    const/4 v5, 0x1

    .line 957
    .local v5, "removeFromNowPlaying":Z
    :goto_0
    iget v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mList:I

    invoke-direct {p0, v0, p1}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->hasNowPlayingSong(I[J)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 958
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mList:I

    invoke-static {v0, v1, p1, p2, v5}, Lcom/samsung/musicplus/dialog/PersonalPageConfirmHelpDialog;->showConfirmDialogFragment(Landroid/app/Activity;I[JZZ)V

    .line 970
    :goto_1
    return-void

    .line 956
    .end local v5    # "removeFromNowPlaying":Z
    :cond_0
    const/4 v5, 0x0

    goto :goto_0

    .line 961
    .restart local v5    # "removeFromNowPlaying":Z
    :cond_1
    if-nez p2, :cond_2

    .line 965
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mList:I

    invoke-static {v0, v1, p1, p2, v5}, Lcom/samsung/musicplus/dialog/PersonalFolderSelectionDialog;->showPersonalFolderSelectionDialog(Landroid/app/Activity;I[JZZ)V

    goto :goto_1

    .line 962
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mList:I

    const/4 v4, 0x0

    move-object v2, p1

    move v3, p2

    invoke-static/range {v0 .. v5}, Lcom/samsung/musicplus/dialog/PersonalMoveDialogFragment;->showPersonalMoveDialogFragment(Landroid/app/Activity;I[JZLjava/lang/String;Z)V

    goto :goto_1
.end method

.method private hasNowPlayingSong(I[J)Z
    .locals 8
    .param p1, "listType"    # I
    .param p2, "selectedItemIds"    # [J

    .prologue
    const/4 v4, 0x0

    .line 973
    invoke-static {p1}, Lcom/samsung/musicplus/util/ListUtils;->isTab(I)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-static {p1}, Lcom/samsung/musicplus/util/ListUtils;->getListContentType(I)I

    move-result v5

    const/4 v6, 0x7

    if-ne v5, v6, :cond_1

    .line 992
    :cond_0
    :goto_0
    return v4

    .line 978
    :cond_1
    array-length v3, p2

    .line 979
    .local v3, "length":I
    if-eqz v3, :cond_2

    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->isPlaying()Z

    move-result v5

    if-nez v5, :cond_3

    .line 980
    :cond_2
    const-string v5, "MusicUiList"

    const-string v6, "Personal page operation, not playing or lengh is 0!!"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 984
    :cond_3
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getCurrentAudioId()J

    move-result-wide v0

    .line 985
    .local v0, "audioId":J
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v3, :cond_0

    .line 986
    aget-wide v6, p2, v2

    cmp-long v5, v0, v6

    if-nez v5, :cond_4

    .line 987
    const-string v4, "MusicUiList"

    const-string v5, "Personal page operation, has now playing song!!"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 988
    const/4 v4, 0x1

    goto :goto_0

    .line 985
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method private hasSelectableItem()Z
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 320
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->getAbsListView()Landroid/widget/AbsListView;

    move-result-object v2

    .line 321
    .local v2, "lv":Landroid/widget/AbsListView;
    if-nez v2, :cond_1

    .line 322
    const-string v4, "MusicUiList"

    const-string v5, "Called getSelectableItemCount() but listView is null yet."

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 332
    :cond_0
    :goto_0
    return v3

    .line 326
    :cond_1
    invoke-virtual {v2}, Landroid/widget/AbsListView;->getCount()I

    move-result v0

    .line 327
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v0, :cond_0

    .line 328
    invoke-virtual {v2, v1}, Landroid/widget/AbsListView;->getItemIdAtPosition(I)J

    move-result-wide v4

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-lez v4, :cond_2

    .line 329
    const/4 v3, 0x1

    goto :goto_0

    .line 327
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private isSlinkTrackList(I)Z
    .locals 2
    .param p1, "list"    # I

    .prologue
    .line 647
    invoke-static {p1}, Lcom/samsung/musicplus/util/ListUtils;->getPreDefinedList(I)I

    move-result v0

    const v1, 0x2000d

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isSupportPersonalTrack()Z
    .locals 4

    .prologue
    .line 651
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mKey:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 652
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mKey:Ljava/lang/String;

    const-wide/16 v2, -0xb

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 653
    const/4 v0, 0x1

    .line 659
    :goto_0
    return v0

    .line 655
    :cond_0
    iget v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mList:I

    const v1, 0x20004

    if-ne v0, v1, :cond_1

    .line 656
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mKey:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/samsung/musicplus/util/ListUtils;->isUserMadePlayList(J)Z

    move-result v0

    goto :goto_0

    .line 659
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private launchSetAs(J)V
    .locals 5
    .param p1, "selectedItemId"    # J

    .prologue
    .line 840
    sget-boolean v2, Lcom/samsung/musicplus/util/Version;->LIBRARY_OVER_API_1:Z

    if-eqz v2, :cond_0

    .line 841
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const-class v3, Lcom/samsung/musicplus/player/SetAsActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 846
    .local v0, "i":Landroid/content/Intent;
    :goto_0
    sget-object v2, Lcom/samsung/musicplus/provider/MusicContents;->MUSIC_CONTENTS_URI:Landroid/net/Uri;

    invoke-static {v2, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 850
    .local v1, "uri":Landroid/net/Uri;
    const-string v2, "extra_uri"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 851
    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->startActivity(Landroid/content/Intent;)V

    .line 852
    return-void

    .line 843
    .end local v0    # "i":Landroid/content/Intent;
    .end local v1    # "uri":Landroid/net/Uri;
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.sec.android.music.intent.action.LAUNCH_SET_AS"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .restart local v0    # "i":Landroid/content/Intent;
    goto :goto_0
.end method

.method private prepareSlinkOption(Landroid/content/Context;Landroid/view/Menu;[JZ)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "menu"    # Landroid/view/Menu;
    .param p3, "selectedIds"    # [J
    .param p4, "hasSelectedItem"    # Z

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 639
    const v4, 0x7f0d01ef

    invoke-interface {p2, v4}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v1

    .line 640
    .local v1, "m":Landroid/view/MenuItem;
    iget v4, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mList:I

    invoke-direct {p0, v4}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->isSlinkTrackList(I)Z

    move-result v4

    if-nez v4, :cond_0

    iget v4, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mList:I

    invoke-static {v4}, Lcom/samsung/musicplus/util/ListUtils;->isDefaultMusicContent(I)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-static {p1}, Lcom/samsung/musicplus/provider/MusicContents$Audio$Devices;->isSingedIn(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-static {p1}, Lcom/samsung/musicplus/provider/MusicContents$Audio$DeviceContents;->isNetworkInitialized(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    move v0, v2

    .line 643
    .local v0, "isEnableSendOtherDevice":Z
    :goto_0
    if-eqz v0, :cond_2

    if-eqz p4, :cond_2

    :goto_1
    invoke-interface {v1, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 644
    return-void

    .end local v0    # "isEnableSendOtherDevice":Z
    :cond_1
    move v0, v3

    .line 640
    goto :goto_0

    .restart local v0    # "isEnableSendOtherDevice":Z
    :cond_2
    move v2, v3

    .line 643
    goto :goto_1
.end method

.method private setActionMode(Z)V
    .locals 1
    .param p1, "isActionMode"    # Z

    .prologue
    .line 247
    iput-boolean p1, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mIsActionMode:Z

    .line 248
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mAdapter:Landroid/widget/CursorAdapter;

    instance-of v0, v0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;

    if-eqz v0, :cond_0

    .line 249
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mAdapter:Landroid/widget/CursorAdapter;

    check-cast v0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->setActionMode(Z)V

    .line 251
    :cond_0
    if-nez p1, :cond_1

    .line 252
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mIsDeleteMode:Z

    .line 254
    :cond_1
    return-void
.end method

.method private setMultiSelected()V
    .locals 1

    .prologue
    .line 239
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->getValidListItemCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 240
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->setMultiSelectionEnable(Z)V

    .line 244
    :goto_0
    return-void

    .line 242
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->setMultiSelectionEnable(Z)V

    goto :goto_0
.end method

.method private setMultiSelectionEnable(Z)V
    .locals 2
    .param p1, "enable"    # Z

    .prologue
    .line 172
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->getAbsListView()Landroid/widget/AbsListView;

    move-result-object v0

    .line 173
    .local v0, "av":Landroid/widget/AbsListView;
    if-eqz v0, :cond_0

    .line 174
    invoke-virtual {v0, p1}, Landroid/widget/AbsListView;->setEnableDragBlock(Z)V

    .line 176
    if-eqz p1, :cond_1

    .line 177
    iget-object v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mTwMultiSelectedListener:Landroid/widget/AdapterView$OnTwMultiSelectedListener;

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setTwMultiSelectedListener(Landroid/widget/AdapterView$OnTwMultiSelectedListener;)V

    .line 178
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setEnableOnclickInMultiSelectMode(Z)V

    .line 183
    :cond_0
    :goto_0
    return-void

    .line 180
    :cond_1
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setTwMultiSelectedListener(Landroid/widget/AdapterView$OnTwMultiSelectedListener;)V

    goto :goto_0
.end method

.method private startActionMode(Landroid/widget/AbsListView;[I)V
    .locals 8
    .param p1, "lv"    # Landroid/widget/AbsListView;
    .param p2, "positions"    # [I

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 420
    if-nez p1, :cond_0

    .line 457
    :goto_0
    return-void

    .line 424
    :cond_0
    invoke-direct {p0, v6}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->setActionMode(Z)V

    .line 426
    iput-boolean v6, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mInternalLoadFinished:Z

    .line 429
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->initLoader()V

    .line 433
    new-instance v3, Lcom/samsung/musicplus/widget/ActionModeCallback;

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->getView()Landroid/view/View;

    move-result-object v5

    invoke-direct {v3, v4, v5, p1, p0}, Lcom/samsung/musicplus/widget/ActionModeCallback;-><init>(Landroid/content/Context;Landroid/view/View;Landroid/widget/AbsListView;Lcom/samsung/musicplus/widget/ActionModeCallback$OnActionModeListener;)V

    iput-object v3, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mActionModeCallBack:Lcom/samsung/musicplus/widget/ActionModeCallback;

    .line 434
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mActionModeCallBack:Lcom/samsung/musicplus/widget/ActionModeCallback;

    invoke-virtual {v3, v4}, Landroid/app/Activity;->startActionMode(Landroid/view/ActionMode$Callback;)Landroid/view/ActionMode;

    .line 435
    const/4 v3, 0x2

    invoke-virtual {p1, v3}, Landroid/widget/AbsListView;->setChoiceMode(I)V

    .line 436
    invoke-virtual {p1}, Landroid/widget/AbsListView;->clearChoices()V

    .line 437
    if-eqz p2, :cond_1

    .line 438
    array-length v2, p2

    .line 439
    .local v2, "length":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v2, :cond_2

    .line 440
    aget v3, p2, v1

    invoke-virtual {p1, v3, v6}, Landroid/widget/AbsListView;->setItemChecked(IZ)V

    .line 439
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 443
    .end local v1    # "i":I
    .end local v2    # "length":I
    :cond_1
    iget-object v3, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mAdapter:Landroid/widget/CursorAdapter;

    if-eqz v3, :cond_2

    .line 444
    invoke-virtual {p1, v7, v7}, Landroid/widget/AbsListView;->setItemChecked(IZ)V

    .line 447
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->invalidateActionMode()V

    .line 448
    iget-object v3, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mSelectionAniUtil:Lcom/samsung/musicplus/util/SelectionAnimationUtil;

    iget v4, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mList:I

    invoke-static {v4}, Lcom/samsung/musicplus/util/ListUtils;->isGridType(I)Z

    move-result v4

    invoke-virtual {v3, p1, v6, v4}, Lcom/samsung/musicplus/util/SelectionAnimationUtil;->setCheckBoxAnimation(Landroid/widget/AbsListView;ZZ)V

    .line 449
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->getAbsListView()Landroid/widget/AbsListView;

    move-result-object v0

    .line 450
    .local v0, "av":Landroid/widget/AbsListView;
    if-eqz v0, :cond_3

    .line 451
    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->unregisterForContextMenu(Landroid/view/View;)V

    .line 453
    :cond_3
    iget-boolean v3, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mIsPopupShown:Z

    if-eqz v3, :cond_4

    .line 454
    iget-object v3, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mActionModeCallBack:Lcom/samsung/musicplus/widget/ActionModeCallback;

    invoke-virtual {v3}, Lcom/samsung/musicplus/widget/ActionModeCallback;->showPopupMenu()V

    .line 456
    :cond_4
    invoke-static {}, Lcom/samsung/musicplus/widget/fragment/ActionModeRemoteHandler;->getInstance()Lcom/samsung/musicplus/widget/fragment/ActionModeRemoteHandler;

    move-result-object v3

    invoke-virtual {v3, p0}, Lcom/samsung/musicplus/widget/fragment/ActionModeRemoteHandler;->registerActionModeHandle(Lcom/samsung/musicplus/widget/fragment/ActionModeRemoteHandler$ActionModeRemoteHandle;)V

    goto :goto_0
.end method

.method private updateDoneOptionItem()V
    .locals 3

    .prologue
    .line 474
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->getAbsListView()Landroid/widget/AbsListView;

    move-result-object v0

    .line 475
    .local v0, "av":Landroid/widget/AbsListView;
    iget-object v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mDone:Landroid/view/MenuItem;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 476
    iget-object v2, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mDone:Landroid/view/MenuItem;

    invoke-virtual {v0}, Landroid/widget/AbsListView;->getCheckedItemCount()I

    move-result v1

    if-lez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-interface {v2, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 478
    :cond_0
    return-void

    .line 476
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public actionSconnect()Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 797
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->getAbsListView()Landroid/widget/AbsListView;

    move-result-object v1

    .line 798
    .local v1, "lv":Landroid/widget/AbsListView;
    if-nez v1, :cond_1

    .line 799
    const-string v3, "MusicUiList"

    const-string v4, "actionSconnect but listView is null yet."

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 807
    :cond_0
    :goto_0
    return v2

    .line 803
    :cond_1
    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->hasSelectedItem(Landroid/widget/AbsListView;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 804
    invoke-virtual {v1}, Landroid/widget/AbsListView;->getCheckedItemIds()[J

    move-result-object v0

    .line 805
    .local v0, "items":[J
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/samsung/musicplus/util/UiUtils;->launchSconnect(Landroid/app/Activity;[J)Z

    move-result v2

    goto :goto_0
.end method

.method public finishActionMode()V
    .locals 1

    .prologue
    .line 484
    sget-boolean v0, Lcom/samsung/musicplus/util/Features;->FLAG_SUPPORT_SPLIT_LIST_VIEW:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mSplitSubFragment:Landroid/app/Fragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mSplitSubFragment:Landroid/app/Fragment;

    instance-of v0, v0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;

    if-eqz v0, :cond_0

    .line 486
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mSplitSubFragment:Landroid/app/Fragment;

    check-cast v0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;

    invoke-virtual {v0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->finishActionMode()V

    .line 489
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mActionModeCallBack:Lcom/samsung/musicplus/widget/ActionModeCallback;

    if-eqz v0, :cond_1

    .line 490
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mActionModeCallBack:Lcom/samsung/musicplus/widget/ActionModeCallback;

    invoke-virtual {v0}, Lcom/samsung/musicplus/widget/ActionModeCallback;->finish()V

    .line 492
    :cond_1
    return-void
.end method

.method protected getAudioIds([J[I)[J
    .locals 0
    .param p1, "selectedItemIds"    # [J
    .param p2, "positions"    # [I

    .prologue
    .line 511
    return-object p1
.end method

.method protected handlePersonalModeMenu(Landroid/view/Menu;Z)V
    .locals 7
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "userPlaylist"    # Z

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 907
    sget-boolean v3, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_SUPPORT_SECRET_BOX:Z

    if-nez v3, :cond_1

    .line 953
    :cond_0
    :goto_0
    return-void

    .line 911
    :cond_1
    if-nez p1, :cond_2

    .line 912
    const-string v3, "MusicUiList"

    const-string v4, "handleSecretBoxMenu() in CommonListFragment- Menu is null, Something\'s wrong!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 917
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->getAbsListView()Landroid/widget/AbsListView;

    move-result-object v0

    .line 918
    .local v0, "lv":Landroid/widget/AbsListView;
    if-nez v0, :cond_3

    .line 919
    const-string v3, "MusicUiList"

    const-string v4, "handleSecretBoxMenu() : getAbsListView() is null, Something\'s wrong!"

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 924
    :cond_3
    invoke-virtual {v0}, Landroid/widget/AbsListView;->getCheckedItemIds()[J

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mActionModeCallBack:Lcom/samsung/musicplus/widget/ActionModeCallback;

    invoke-virtual {v4}, Lcom/samsung/musicplus/widget/ActionModeCallback;->getSelectedPositions()[I

    move-result-object v4

    invoke-virtual {p0, v3, v4}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->getAudioIds([J[I)[J

    move-result-object v2

    .line 929
    .local v2, "selectedIds":[J
    array-length v3, v2

    if-eqz v3, :cond_4

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/musicplus/library/storage/PrivateMode;->isPrivateDirMounted(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_5

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/musicplus/library/storage/PrivateMode;->isReadyToMove(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 931
    :cond_4
    iget-object v3, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mAddToPersonal:Landroid/view/MenuItem;

    invoke-interface {v3, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 932
    iget-object v3, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mReoveFromPersonal:Landroid/view/MenuItem;

    invoke-interface {v3, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 935
    :cond_5
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->getContext()Landroid/content/Context;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mQueryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    invoke-static {v3, v4, v2, p2}, Lcom/samsung/musicplus/util/ListUtils;->getPersonalModeInList(Landroid/content/Context;Lcom/samsung/musicplus/util/ListUtils$QueryArgs;[JZ)I

    move-result v1

    .line 937
    .local v1, "mode":I
    iget-object v3, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mAddToPersonal:Landroid/view/MenuItem;

    if-eqz v3, :cond_6

    .line 938
    if-ne v1, v6, :cond_7

    .line 939
    iget-object v3, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mAddToPersonal:Landroid/view/MenuItem;

    invoke-interface {v3, v6}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 945
    :cond_6
    :goto_1
    iget-object v3, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mReoveFromPersonal:Landroid/view/MenuItem;

    if-eqz v3, :cond_0

    .line 946
    if-nez v1, :cond_8

    .line 947
    iget-object v3, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mReoveFromPersonal:Landroid/view/MenuItem;

    invoke-interface {v3, v6}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 941
    :cond_7
    iget-object v3, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mAddToPersonal:Landroid/view/MenuItem;

    invoke-interface {v3, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_1

    .line 949
    :cond_8
    iget-object v3, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mReoveFromPersonal:Landroid/view/MenuItem;

    invoke-interface {v3, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method protected hasSelectedItem(Landroid/widget/AbsListView;)Z
    .locals 1
    .param p1, "lv"    # Landroid/widget/AbsListView;

    .prologue
    .line 495
    invoke-virtual {p1}, Landroid/widget/AbsListView;->getCheckedItemCount()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected invalidateActionMode()V
    .locals 1

    .prologue
    .line 463
    sget-boolean v0, Lcom/samsung/musicplus/util/Features;->FLAG_SUPPORT_SPLIT_LIST_VIEW:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mSplitSubFragment:Landroid/app/Fragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mSplitSubFragment:Landroid/app/Fragment;

    instance-of v0, v0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;

    if-eqz v0, :cond_0

    .line 465
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mSplitSubFragment:Landroid/app/Fragment;

    check-cast v0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;

    invoke-virtual {v0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->invalidateActionMode()V

    .line 467
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mActionModeCallBack:Lcom/samsung/musicplus/widget/ActionModeCallback;

    if-eqz v0, :cond_1

    .line 468
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mActionModeCallBack:Lcom/samsung/musicplus/widget/ActionModeCallback;

    invoke-virtual {v0}, Lcom/samsung/musicplus/widget/ActionModeCallback;->invalidateActionMode()V

    .line 470
    :cond_1
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->updateDoneOptionItem()V

    .line 471
    return-void
.end method

.method public isActionMode()Z
    .locals 1

    .prologue
    .line 257
    iget-boolean v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mIsActionMode:Z

    return v0
.end method

.method public isCurrentActionMode()Z
    .locals 1

    .prologue
    .line 1072
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->isActionMode()Z

    move-result v0

    return v0
.end method

.method protected isDeleteMode()Z
    .locals 1

    .prologue
    .line 398
    iget-boolean v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mIsDeleteMode:Z

    return v0
.end method

.method protected isEnableListShuffle(I)Z
    .locals 1
    .param p1, "list"    # I

    .prologue
    .line 370
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->isEnableListShuffle(I)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mIsActionMode:Z

    if-eqz v0, :cond_0

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isFragmentAdded()Z
    .locals 1

    .prologue
    .line 1067
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->isAdded()Z

    move-result v0

    return v0
.end method

.method protected isSelectedOneItem(Landroid/widget/AbsListView;)Z
    .locals 2
    .param p1, "lv"    # Landroid/widget/AbsListView;

    .prologue
    const/4 v0, 0x1

    .line 499
    invoke-virtual {p1}, Landroid/widget/AbsListView;->getCheckedItemCount()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected launchDetails(J)V
    .locals 7
    .param p1, "selectedItemId"    # J

    .prologue
    .line 881
    iget v4, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mList:I

    invoke-static {v4}, Lcom/samsung/musicplus/util/ListUtils;->getPreDefinedList(I)I

    move-result v2

    .line 882
    .local v2, "listType":I
    const v4, 0x2000b

    if-eq v2, v4, :cond_0

    invoke-direct {p0, v2}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->isSlinkTrackList(I)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 883
    :cond_0
    invoke-static {v2}, Lcom/samsung/musicplus/util/ListUtils;->convertToUri(I)Landroid/net/Uri;

    move-result-object v4

    invoke-static {v4, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v4

    iget v5, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mList:I

    invoke-static {}, Lcom/samsung/musicplus/util/UiUtils;->getTheme()I

    move-result v6

    invoke-static {v4, v5, v6}, Lcom/samsung/musicplus/dialog/DlnaDmsDetailInfoDialog;->getInstance(Ljava/lang/String;II)Landroid/app/DialogFragment;

    move-result-object v0

    .line 886
    .local v0, "fg":Landroid/app/DialogFragment;
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v4

    const-string v5, "dlnaDms_detail_info"

    invoke-virtual {v0, v4, v5}, Landroid/app/DialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 898
    .end local v0    # "fg":Landroid/app/DialogFragment;
    :goto_0
    return-void

    .line 888
    :cond_1
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const-class v5, Lcom/samsung/musicplus/mediainfo/MediaInfoActivity;

    invoke-direct {v1, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 889
    .local v1, "i":Landroid/content/Intent;
    sget-object v4, Lcom/samsung/musicplus/provider/MusicContents;->MUSIC_CONTENTS_URI:Landroid/net/Uri;

    invoke-static {v4, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    .line 893
    .local v3, "uri":Landroid/net/Uri;
    if-eqz v3, :cond_2

    .line 894
    const-string v4, "uri"

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 896
    :cond_2
    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public onActionItemClicked(Landroid/view/MenuItem;[J[I)V
    .locals 5
    .param p1, "item"    # Landroid/view/MenuItem;
    .param p2, "selectedItemIds"    # [J
    .param p3, "selectedPositions"    # [I

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 712
    array-length v2, p2

    if-nez v2, :cond_1

    .line 713
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->finishActionMode()V

    .line 776
    :cond_0
    :goto_0
    return-void

    .line 716
    :cond_1
    const/4 v0, 0x0

    .line 717
    .local v0, "actionModeHandle":Z
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    .line 772
    :goto_1
    if-nez v0, :cond_0

    .line 774
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->finishActionMode()V

    goto :goto_0

    .line 719
    :sswitch_0
    invoke-direct {p0, p2, p3}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->addtoPlaylist([J[I)V

    .line 720
    const/4 v0, 0x1

    .line 721
    goto :goto_1

    .line 723
    :sswitch_1
    invoke-direct {p0, p2, p3}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->deleteItem([J[I)V

    .line 724
    const/4 v0, 0x1

    .line 725
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "SLUD"

    invoke-static {v2, v3}, Lcom/samsung/musicplus/util/logging/FeatureLogger;->insertLog(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_1

    .line 728
    :sswitch_2
    invoke-direct {p0, p2, p3}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->deleteItem([J[I)V

    .line 729
    const/4 v0, 0x1

    .line 730
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "SLOD"

    invoke-static {v2, v3}, Lcom/samsung/musicplus/util/logging/FeatureLogger;->insertLog(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_1

    .line 733
    :sswitch_3
    aget-wide v2, p2, v3

    invoke-direct {p0, v2, v3}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->launchSetAs(J)V

    .line 734
    const/4 v0, 0x1

    .line 735
    goto :goto_1

    .line 737
    :sswitch_4
    invoke-direct {p0, p2}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->addToFavorites([J)V

    goto :goto_1

    .line 740
    :sswitch_5
    invoke-virtual {p0, p2}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->removeFavorites([J)V

    goto :goto_1

    .line 743
    :sswitch_6
    aget-wide v2, p2, v3

    invoke-virtual {p0, v2, v3}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->launchDetails(J)V

    goto :goto_1

    .line 746
    :sswitch_7
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const v3, 0x7f0d01d4

    invoke-static {v2, p2, v3}, Lcom/samsung/musicplus/util/KnoxGateManager;->showKnoxDialog(Landroid/app/Activity;[JI)V

    goto :goto_1

    .line 749
    :sswitch_8
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    const v3, 0x7f0d01d5

    invoke-static {v2, p2, v3}, Lcom/samsung/musicplus/util/KnoxGateManager;->showKnoxDialog(Landroid/app/Activity;[JI)V

    goto :goto_1

    .line 752
    :sswitch_9
    invoke-direct {p0, p2, v4}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->handlePersonalModeAction([JZ)V

    .line 753
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "MVTP"

    invoke-static {v2, v3}, Lcom/samsung/musicplus/util/logging/FeatureLogger;->insertLog(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_1

    .line 756
    :sswitch_a
    invoke-direct {p0, p2, v3}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->handlePersonalModeAction([JZ)V

    goto :goto_1

    .line 760
    :sswitch_b
    iget v2, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mList:I

    invoke-direct {p0, v2}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->isSlinkTrackList(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 761
    const/4 v1, 0x2

    .line 765
    .local v1, "type":I
    :goto_2
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2, v1, p2, v4}, Lcom/samsung/musicplus/provider/MusicContents$Audio$DeviceContents;->startDeviceSelectActivityForResult(Landroid/app/Activity;I[JI)V

    goto :goto_1

    .line 763
    .end local v1    # "type":I
    :cond_2
    const/4 v1, 0x1

    .restart local v1    # "type":I
    goto :goto_2

    .line 717
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0d01a3 -> :sswitch_3
        0x7f0d01ca -> :sswitch_1
        0x7f0d01cd -> :sswitch_0
        0x7f0d01d0 -> :sswitch_2
        0x7f0d01d2 -> :sswitch_4
        0x7f0d01d3 -> :sswitch_5
        0x7f0d01d4 -> :sswitch_7
        0x7f0d01d5 -> :sswitch_8
        0x7f0d01d6 -> :sswitch_9
        0x7f0d01d7 -> :sswitch_a
        0x7f0d01d8 -> :sswitch_6
        0x7f0d01ef -> :sswitch_b
    .end sparse-switch
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 121
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 122
    if-eqz p1, :cond_0

    .line 123
    const-string v1, "restore_mode"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mIsRestoreMode:Z

    .line 124
    const-string v1, "action_mode"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mIsActionMode:Z

    .line 125
    const-string v1, "delete_mode"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mIsDeleteMode:Z

    .line 126
    const-string v1, "count"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mListCount:I

    .line 127
    const-string v1, "positions"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mSelectedPositions:[I

    .line 128
    const-string v1, "popup"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mIsPopupShown:Z

    .line 130
    :cond_0
    iget-object v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mKnoxGate:Lcom/samsung/musicplus/util/KnoxGateManager;

    if-nez v1, :cond_1

    .line 131
    new-instance v1, Lcom/samsung/musicplus/util/KnoxGateManager;

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/musicplus/util/KnoxGateManager;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mKnoxGate:Lcom/samsung/musicplus/util/KnoxGateManager;

    .line 134
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mKeyListener:Landroid/content/BroadcastReceiver;

    new-instance v3, Landroid/content/IntentFilter;

    const-string v4, "com.samsung.musicplus.delete"

    invoke-direct {v3, v4}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 136
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->getAbsListView()Landroid/widget/AbsListView;

    move-result-object v0

    .line 137
    .local v0, "lv":Landroid/widget/AbsListView;
    if-eqz v0, :cond_2

    .line 138
    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->unregisterForContextMenu(Landroid/view/View;)V

    .line 140
    :cond_2
    new-instance v1, Lcom/samsung/musicplus/util/SelectionAnimationUtil;

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/musicplus/util/SelectionAnimationUtil;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mSelectionAniUtil:Lcom/samsung/musicplus/util/SelectionAnimationUtil;

    .line 141
    return-void
.end method

.method public onDestroyActionMode()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 664
    invoke-direct {p0, v3}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->setActionMode(Z)V

    .line 665
    iput-boolean v3, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mIsPopupShown:Z

    .line 666
    iput-object v2, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mActionModeCallBack:Lcom/samsung/musicplus/widget/ActionModeCallback;

    .line 668
    iput-boolean v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mInternalLoadFinished:Z

    .line 671
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->initLoader()V

    .line 672
    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->toggleHeaderAndShuffle(Z)V

    .line 673
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->updateNumberView()V

    .line 674
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->getAbsListView()Landroid/widget/AbsListView;

    move-result-object v0

    .line 675
    .local v0, "lv":Landroid/widget/AbsListView;
    if-nez v0, :cond_1

    .line 701
    :cond_0
    :goto_0
    return-void

    .line 679
    :cond_1
    instance-of v1, v0, Lcom/samsung/musicplus/widget/list/MusicListView;

    if-eqz v1, :cond_2

    move-object v1, v0

    .line 682
    check-cast v1, Lcom/samsung/musicplus/widget/list/MusicListView;

    invoke-virtual {v1, v2}, Lcom/samsung/musicplus/widget/list/MusicListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 685
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->isResumed()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 686
    iget-object v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mSelectionAniUtil:Lcom/samsung/musicplus/util/SelectionAnimationUtil;

    iget v2, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mList:I

    invoke-static {v2}, Lcom/samsung/musicplus/util/ListUtils;->isGridType(I)Z

    move-result v2

    invoke-virtual {v1, v0, v3, v2}, Lcom/samsung/musicplus/util/SelectionAnimationUtil;->setCheckBoxAnimation(Landroid/widget/AbsListView;ZZ)V

    .line 690
    :cond_3
    invoke-virtual {v0}, Landroid/widget/AbsListView;->clearChoices()V

    .line 691
    new-instance v1, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment$2;

    invoke-direct {v1, p0, v0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment$2;-><init>(Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;Landroid/widget/AbsListView;)V

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->post(Ljava/lang/Runnable;)Z

    .line 697
    invoke-virtual {v0}, Landroid/widget/AbsListView;->invalidateViews()V

    .line 698
    iget-object v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mKnoxGate:Lcom/samsung/musicplus/util/KnoxGateManager;

    if-eqz v1, :cond_0

    .line 699
    iget-object v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mKnoxGate:Lcom/samsung/musicplus/util/KnoxGateManager;

    invoke-virtual {v1}, Lcom/samsung/musicplus/util/KnoxGateManager;->release()V

    goto :goto_0
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 705
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->finishActionMode()V

    .line 706
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mKeyListener:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 707
    invoke-super {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->onDestroyView()V

    .line 708
    return-void
.end method

.method public onInflateOptionsMenu(Landroid/view/Menu;)V
    .locals 5
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v4, 0x0

    .line 535
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getMenuInflater()Landroid/view/MenuInflater;

    move-result-object v0

    .line 536
    .local v0, "inflater":Landroid/view/MenuInflater;
    iget-boolean v2, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mIsDeleteMode:Z

    if-eqz v2, :cond_1

    .line 537
    const/high16 v2, 0x7f120000

    invoke-virtual {v0, v2, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 538
    const v2, 0x7f0d01ca

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mDone:Landroid/view/MenuItem;

    .line 539
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->updateDoneOptionItem()V

    .line 560
    :cond_0
    :goto_0
    return-void

    .line 542
    :cond_1
    const v2, 0x7f120002

    invoke-virtual {v0, v2, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 543
    const v2, 0x7f0d01cd

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mAddToPlayList:Landroid/view/MenuItem;

    .line 544
    const v2, 0x7f0d01d0

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mDelete:Landroid/view/MenuItem;

    .line 545
    const v2, 0x7f0d01a3

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mSetAs:Landroid/view/MenuItem;

    .line 546
    const v2, 0x7f0d01d2

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mAddToFavorites:Landroid/view/MenuItem;

    .line 547
    const v2, 0x7f0d01d3

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mRemoveFromFavorites:Landroid/view/MenuItem;

    .line 548
    const v2, 0x7f0d01d8

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mDetails:Landroid/view/MenuItem;

    .line 549
    const v2, 0x7f0d01d6

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mAddToPersonal:Landroid/view/MenuItem;

    .line 550
    const v2, 0x7f0d01d7

    invoke-interface {p1, v2}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mReoveFromPersonal:Landroid/view/MenuItem;

    .line 552
    iget v2, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mList:I

    invoke-static {v2}, Lcom/samsung/musicplus/util/ListUtils;->isTrack(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 553
    const v2, 0x7f0d01ef

    const v3, 0x7f1000be

    invoke-interface {p1, v4, v2, v4, v3}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v1

    .line 555
    .local v1, "m":Landroid/view/MenuItem;
    invoke-interface {v1, v4}, Landroid/view/MenuItem;->setShowAsAction(I)V

    .line 557
    invoke-interface {v1, v4}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public onListItemClick(Landroid/widget/AbsListView;Landroid/view/View;IJ)V
    .locals 2
    .param p1, "l"    # Landroid/widget/AbsListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    .line 337
    iget-boolean v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mIsActionMode:Z

    if-eqz v0, :cond_1

    .line 338
    const-wide/16 v0, 0x0

    cmp-long v0, p4, v0

    if-ltz v0, :cond_0

    .line 339
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->invalidateActionMode()V

    .line 347
    :cond_0
    :goto_0
    return-void

    .line 343
    :cond_1
    iget-boolean v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mIsActionMode:Z

    if-eqz v0, :cond_2

    const-wide/16 v0, -0x64

    cmp-long v0, p4, v0

    if-eqz v0, :cond_0

    .line 346
    :cond_2
    invoke-super/range {p0 .. p5}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->onListItemClick(Landroid/widget/AbsListView;Landroid/view/View;IJ)V

    goto :goto_0
.end method

.method public onListItemLongClick(Landroid/widget/AbsListView;Landroid/view/View;IJ)Z
    .locals 6
    .param p1, "l"    # Landroid/widget/AbsListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 351
    const-string v2, "MusicUiList"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " onListItemLongClick "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " position : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " id: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 353
    iget-boolean v2, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mIsActionMode:Z

    if-nez v2, :cond_0

    const-wide/16 v2, 0x0

    cmp-long v2, p4, v2

    if-ltz v2, :cond_0

    .line 359
    new-array v2, v0, [I

    aput p3, v2, v1

    invoke-direct {p0, p1, v2}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->startActionMode(Landroid/widget/AbsListView;[I)V

    .line 362
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "SLLP"

    invoke-static {v1, v2}, Lcom/samsung/musicplus/util/logging/FeatureLogger;->insertLog(Landroid/content/Context;Ljava/lang/String;)V

    .line 365
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V
    .locals 4
    .param p2, "data"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "loader":Landroid/content/Loader;, "Landroid/content/Loader<Landroid/database/Cursor;>;"
    const/4 v2, 0x0

    .line 199
    invoke-super {p0, p1, p2}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V

    .line 200
    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    iput-boolean v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mHasSelectableItem:Z

    .line 202
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->getAbsListView()Landroid/widget/AbsListView;

    move-result-object v0

    .line 203
    .local v0, "lv":Landroid/widget/AbsListView;
    iget-boolean v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mIsRestoreMode:Z

    if-eqz v1, :cond_4

    if-eqz v0, :cond_4

    .line 204
    iput-boolean v2, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mIsRestoreMode:Z

    .line 211
    iget v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mListCount:I

    if-lez v1, :cond_1

    iget v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mListCount:I

    invoke-virtual {v0}, Landroid/widget/AbsListView;->getCount()I

    move-result v3

    if-eq v1, v3, :cond_1

    .line 212
    iput-boolean v2, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mIsActionMode:Z

    .line 213
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->setMultiSelected()V

    .line 236
    :goto_1
    return-void

    .end local v0    # "lv":Landroid/widget/AbsListView;
    :cond_0
    move v1, v2

    .line 200
    goto :goto_0

    .line 216
    .restart local v0    # "lv":Landroid/widget/AbsListView;
    :cond_1
    invoke-static {}, Lcom/samsung/musicplus/widget/fragment/ActionModeRemoteHandler;->getInstance()Lcom/samsung/musicplus/widget/fragment/ActionModeRemoteHandler;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/musicplus/widget/fragment/ActionModeRemoteHandler;->isLastActionMode()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 217
    iget-object v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mSelectedPositions:[I

    invoke-direct {p0, v0, v1}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->startActionMode(Landroid/widget/AbsListView;[I)V

    .line 234
    :cond_2
    :goto_2
    iput-boolean v2, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mInternalLoadFinished:Z

    .line 235
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->setMultiSelected()V

    goto :goto_1

    .line 219
    :cond_3
    iput-boolean v2, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mIsActionMode:Z

    goto :goto_2

    .line 221
    :cond_4
    iget-boolean v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mInternalLoadFinished:Z

    if-nez v1, :cond_2

    .line 226
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    new-instance v3, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment$1;

    invoke-direct {v3, p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment$1;-><init>(Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;)V

    invoke-virtual {v1, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_2
.end method

.method public bridge synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Landroid/content/Loader;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 78
    check-cast p2, Landroid/database/Cursor;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 187
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    const v2, 0x7f0d01dc

    if-ne v1, v2, :cond_0

    .line 188
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->getAbsListView()Landroid/widget/AbsListView;

    move-result-object v0

    .line 189
    .local v0, "av":Landroid/widget/AbsListView;
    if-eqz v0, :cond_0

    .line 190
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->startActionMode(Landroid/widget/AbsListView;[I)V

    .line 191
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "SLOS"

    invoke-static {v1, v2}, Lcom/samsung/musicplus/util/logging/FeatureLogger;->insertLog(Landroid/content/Context;Ljava/lang/String;)V

    .line 194
    .end local v0    # "av":Landroid/widget/AbsListView;
    :cond_0
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    return v1
.end method

.method public onPrepareActionMode(Landroid/view/Menu;)V
    .locals 10
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 564
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->getAbsListView()Landroid/widget/AbsListView;

    move-result-object v3

    .line 565
    .local v3, "lv":Landroid/widget/AbsListView;
    if-nez v3, :cond_0

    .line 566
    const-string v5, "MusicUiList"

    const-string v6, "Called onPrepareActionMode() but listView is null yet."

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 636
    :goto_0
    return-void

    .line 570
    :cond_0
    iget-object v5, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mActionModeCallBack:Lcom/samsung/musicplus/widget/ActionModeCallback;

    if-nez v5, :cond_1

    .line 571
    const-string v5, "MusicUiList"

    const-string v6, "Called onPrepareActionMode() but mActionModeCallBack is null."

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 575
    :cond_1
    instance-of v5, v3, Lcom/samsung/musicplus/widget/list/MusicListView;

    if-eqz v5, :cond_2

    move-object v5, v3

    .line 578
    check-cast v5, Lcom/samsung/musicplus/widget/list/MusicListView;

    const/4 v8, 0x0

    invoke-virtual {v5, v8}, Lcom/samsung/musicplus/widget/list/MusicListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 581
    :cond_2
    invoke-virtual {p0, v3}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->hasSelectedItem(Landroid/widget/AbsListView;)Z

    move-result v0

    .line 582
    .local v0, "hasSelectedItem":Z
    invoke-virtual {p0, v3}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->isSelectedOneItem(Landroid/widget/AbsListView;)Z

    move-result v2

    .line 584
    .local v2, "isSelectedOne":Z
    iget-boolean v5, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mIsDeleteMode:Z

    if-eqz v5, :cond_3

    .line 585
    iget-object v5, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mDone:Landroid/view/MenuItem;

    invoke-interface {v5, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 588
    :cond_3
    iget v5, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mList:I

    invoke-static {v5}, Lcom/samsung/musicplus/util/ListUtils;->isTrack(I)Z

    move-result v5

    if-nez v5, :cond_4

    iget v5, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mList:I

    invoke-static {v5}, Lcom/samsung/musicplus/util/ListUtils;->isPlaylistTabContent(I)Z

    move-result v5

    if-nez v5, :cond_5

    .line 589
    :cond_4
    iget-object v5, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mAddToPlayList:Landroid/view/MenuItem;

    invoke-interface {v5, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 591
    :cond_5
    iget v5, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mList:I

    const v8, 0x20004

    if-eq v5, v8, :cond_6

    .line 592
    iget-object v5, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mDelete:Landroid/view/MenuItem;

    invoke-interface {v5, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 597
    :cond_6
    iget v5, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mList:I

    invoke-static {v5}, Lcom/samsung/musicplus/util/ListUtils;->isTrack(I)Z

    move-result v5

    if-eqz v5, :cond_f

    .line 598
    invoke-virtual {v3}, Landroid/widget/AbsListView;->getCheckedItemIds()[J

    move-result-object v5

    iget-object v8, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mActionModeCallBack:Lcom/samsung/musicplus/widget/ActionModeCallback;

    invoke-virtual {v8}, Lcom/samsung/musicplus/widget/ActionModeCallback;->getSelectedPositions()[I

    move-result-object v8

    invoke-virtual {p0, v5, v8}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->getAudioIds([J[I)[J

    move-result-object v4

    .line 601
    .local v4, "selectedIds":[J
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {p0, v5, p1, v4, v0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->prepareSlinkOption(Landroid/content/Context;Landroid/view/Menu;[JZ)V

    .line 603
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/musicplus/util/KnoxGateManager;->isKnoxModeOn(Landroid/content/Context;)Z

    move-result v5

    if-eqz v5, :cond_8

    .line 604
    iget-object v5, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mSetAs:Landroid/view/MenuItem;

    invoke-interface {v5, v7}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 612
    :goto_1
    iget-object v5, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mDetails:Landroid/view/MenuItem;

    invoke-interface {v5, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 613
    iget-object v5, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mAddToPlayList:Landroid/view/MenuItem;

    invoke-interface {v5, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 616
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5, v4}, Lcom/samsung/musicplus/util/UiUtils;->checkFavorites(Landroid/content/Context;[J)[J

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mFavoriteSelectionIds:[J

    .line 618
    iget-object v5, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mFavoriteSelectionIds:[J

    if-nez v5, :cond_b

    move v1, v6

    .line 619
    .local v1, "isAllFavorites":Z
    :goto_2
    iget-object v8, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mAddToFavorites:Landroid/view/MenuItem;

    if-eqz v0, :cond_c

    if-nez v1, :cond_c

    move v5, v6

    :goto_3
    invoke-interface {v8, v5}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 620
    iget-object v5, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mRemoveFromFavorites:Landroid/view/MenuItem;

    if-eqz v0, :cond_d

    if-eqz v1, :cond_d

    :goto_4
    invoke-interface {v5, v6}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 623
    iget-object v5, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mKnoxGate:Lcom/samsung/musicplus/util/KnoxGateManager;

    if-eqz v5, :cond_7

    .line 624
    array-length v5, v4

    if-lez v5, :cond_e

    .line 625
    iget-object v5, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mKnoxGate:Lcom/samsung/musicplus/util/KnoxGateManager;

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->getContext()Landroid/content/Context;

    move-result-object v6

    iget v7, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mList:I

    invoke-virtual {v5, v6, p1, v7}, Lcom/samsung/musicplus/util/KnoxGateManager;->setKnoxMenuVisibility(Landroid/content/Context;Landroid/view/Menu;I)V

    .line 631
    :cond_7
    :goto_5
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->isSupportPersonalTrack()Z

    move-result v5

    invoke-virtual {p0, p1, v5}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->handlePersonalModeMenu(Landroid/view/Menu;Z)V

    goto/16 :goto_0

    .line 605
    .end local v1    # "isAllFavorites":Z
    :cond_8
    if-eqz v2, :cond_9

    array-length v5, v4

    if-nez v5, :cond_a

    .line 610
    :cond_9
    iget-object v5, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mSetAs:Landroid/view/MenuItem;

    invoke-interface {v5, v2}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_1

    .line 605
    :cond_a
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->getContext()Landroid/content/Context;

    move-result-object v5

    aget-wide v8, v4, v7

    invoke-static {v5, v8, v9}, Lcom/samsung/musicplus/util/UiUtils;->isPersonalModeSong(Landroid/content/Context;J)Z

    move-result v5

    if-eqz v5, :cond_9

    .line 608
    iget-object v5, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mSetAs:Landroid/view/MenuItem;

    invoke-interface {v5, v7}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_1

    :cond_b
    move v1, v7

    .line 618
    goto :goto_2

    .restart local v1    # "isAllFavorites":Z
    :cond_c
    move v5, v7

    .line 619
    goto :goto_3

    :cond_d
    move v6, v7

    .line 620
    goto :goto_4

    .line 627
    :cond_e
    iget-object v5, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mKnoxGate:Lcom/samsung/musicplus/util/KnoxGateManager;

    invoke-virtual {v5, p1}, Lcom/samsung/musicplus/util/KnoxGateManager;->disableKnoxMenu(Landroid/view/Menu;)V

    goto :goto_5

    .line 633
    .end local v1    # "isAllFavorites":Z
    .end local v4    # "selectedIds":[J
    :cond_f
    const v5, 0x7f0d01d1

    invoke-interface {p1, v5, v7}, Landroid/view/Menu;->setGroupVisible(IZ)V

    goto/16 :goto_0
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 167
    const v0, 0x7f0d01dc

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iget-boolean v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mHasSelectableItem:Z

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 168
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 169
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x0

    .line 147
    const-string v0, "restore_mode"

    iget-boolean v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mIsActionMode:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 148
    const-string v0, "action_mode"

    iget-boolean v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mIsActionMode:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 149
    const-string v0, "delete_mode"

    iget-boolean v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mIsDeleteMode:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 151
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mActionModeCallBack:Lcom/samsung/musicplus/widget/ActionModeCallback;

    if-eqz v0, :cond_0

    .line 152
    const-string v0, "count"

    iget-object v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mActionModeCallBack:Lcom/samsung/musicplus/widget/ActionModeCallback;

    invoke-virtual {v1}, Lcom/samsung/musicplus/widget/ActionModeCallback;->getListCount()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 153
    const-string v0, "positions"

    iget-object v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mActionModeCallBack:Lcom/samsung/musicplus/widget/ActionModeCallback;

    invoke-virtual {v1}, Lcom/samsung/musicplus/widget/ActionModeCallback;->getSelectedPositions()[I

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    .line 154
    const-string v0, "popup"

    iget-object v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mActionModeCallBack:Lcom/samsung/musicplus/widget/ActionModeCallback;

    invoke-virtual {v1}, Lcom/samsung/musicplus/widget/ActionModeCallback;->isPopupShowing()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 157
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->isSplitSubListFragment()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 158
    const-string v0, "restore_mode"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 159
    const-string v0, "action_mode"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 160
    const-string v0, "delete_mode"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 162
    :cond_1
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 163
    return-void
.end method

.method public onTabSelected()V
    .locals 0

    .prologue
    .line 781
    return-void
.end method

.method public onTabUnselected()V
    .locals 1

    .prologue
    .line 785
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->finishActionMode()V

    .line 786
    sget-boolean v0, Lcom/samsung/musicplus/util/Features;->FLAG_SUPPORT_SPLIT_LIST_VIEW:Z

    if-eqz v0, :cond_0

    .line 787
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->clearListSelectedPosition()V

    .line 788
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mSplitSubFragment:Landroid/app/Fragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mSplitSubFragment:Landroid/app/Fragment;

    instance-of v0, v0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;

    if-eqz v0, :cond_0

    .line 789
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->removeSubFragment()V

    .line 793
    :cond_0
    return-void
.end method

.method protected removeFavorites([J)V
    .locals 6
    .param p1, "selectedItemIds"    # [J

    .prologue
    const/4 v5, 0x0

    .line 876
    new-instance v0, Lcom/samsung/musicplus/util/FileOperationTask$SmartFavoriteTask;

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget-object v3, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mFavoriteSelectionIds:[J

    const/4 v4, 0x1

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/samsung/musicplus/util/FileOperationTask$SmartFavoriteTask;-><init>(Landroid/app/Activity;[J[JZZ)V

    new-array v1, v5, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/util/FileOperationTask$SmartFavoriteTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 878
    return-void
.end method

.method public setFinishActionMode()V
    .locals 0

    .prologue
    .line 1062
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->finishActionMode()V

    .line 1063
    return-void
.end method

.method public startActionMode()V
    .locals 2

    .prologue
    .line 405
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mIsDeleteMode:Z

    .line 406
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->getAbsListView()Landroid/widget/AbsListView;

    move-result-object v0

    .line 407
    .local v0, "lv":Landroid/widget/AbsListView;
    if-eqz v0, :cond_0

    .line 408
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->startActionMode(Landroid/widget/AbsListView;[I)V

    .line 410
    :cond_0
    return-void
.end method

.method protected toggleHeaderAndShuffle(Z)V
    .locals 3
    .param p1, "visible"    # Z

    .prologue
    .line 380
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->getView()Landroid/view/View;

    move-result-object v1

    if-nez v1, :cond_1

    .line 387
    :cond_0
    :goto_0
    return-void

    .line 383
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->getView()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f0d008f

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 384
    .local v0, "view":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 385
    if-eqz p1, :cond_2

    const/4 v1, 0x0

    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_2
    const/16 v1, 0x8

    goto :goto_1
.end method

.method protected updateNumberView()V
    .locals 0

    .prologue
    .line 393
    invoke-super {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->updateNumberView()V

    .line 395
    return-void
.end method

.method public updateSubList()V
    .locals 0

    .prologue
    .line 996
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->removeSubFragment()V

    .line 997
    return-void
.end method
