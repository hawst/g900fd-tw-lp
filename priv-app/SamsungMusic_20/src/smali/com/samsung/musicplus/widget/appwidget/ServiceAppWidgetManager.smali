.class public Lcom/samsung/musicplus/widget/appwidget/ServiceAppWidgetManager;
.super Ljava/lang/Object;
.source "ServiceAppWidgetManager.java"


# instance fields
.field private final mAlbumArtHandler:Landroid/os/Handler;

.field private mMusicWidget:Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider;

.field private mService:Lcom/samsung/musicplus/service/PlayerService;

.field private mSize:I


# direct methods
.method public constructor <init>(Lcom/samsung/musicplus/service/PlayerService;)V
    .locals 2
    .param p1, "s"    # Lcom/samsung/musicplus/service/PlayerService;

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    new-instance v0, Lcom/samsung/musicplus/widget/appwidget/ServiceAppWidgetManager$1;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/widget/appwidget/ServiceAppWidgetManager$1;-><init>(Lcom/samsung/musicplus/widget/appwidget/ServiceAppWidgetManager;)V

    iput-object v0, p0, Lcom/samsung/musicplus/widget/appwidget/ServiceAppWidgetManager;->mAlbumArtHandler:Landroid/os/Handler;

    .line 50
    iput-object p1, p0, Lcom/samsung/musicplus/widget/appwidget/ServiceAppWidgetManager;->mService:Lcom/samsung/musicplus/service/PlayerService;

    .line 51
    invoke-static {}, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider;->getInstance()Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/widget/appwidget/ServiceAppWidgetManager;->mMusicWidget:Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider;

    .line 52
    invoke-virtual {p1}, Lcom/samsung/musicplus/service/PlayerService;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0194

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/widget/appwidget/ServiceAppWidgetManager;->mSize:I

    .line 53
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/widget/appwidget/ServiceAppWidgetManager;)Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/appwidget/ServiceAppWidgetManager;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/samsung/musicplus/widget/appwidget/ServiceAppWidgetManager;->mMusicWidget:Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider;

    return-object v0
.end method

.method private setAlbumArt()V
    .locals 8

    .prologue
    .line 72
    iget-object v0, p0, Lcom/samsung/musicplus/widget/appwidget/ServiceAppWidgetManager;->mService:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 73
    .local v1, "context":Landroid/content/Context;
    iget-object v0, p0, Lcom/samsung/musicplus/widget/appwidget/ServiceAppWidgetManager;->mService:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->getListType()I

    move-result v2

    .line 74
    .local v2, "listType":I
    iget-object v0, p0, Lcom/samsung/musicplus/widget/appwidget/ServiceAppWidgetManager;->mService:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->getAlbumId()J

    move-result-wide v6

    .line 76
    .local v6, "albumId":J
    iget-object v0, p0, Lcom/samsung/musicplus/widget/appwidget/ServiceAppWidgetManager;->mAlbumArtHandler:Landroid/os/Handler;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 77
    invoke-static {}, Lcom/samsung/musicplus/util/AlbumArtLoader;->getAlbumArtLoader()Lcom/samsung/musicplus/util/IAlbumArtLoader;

    move-result-object v0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iget v4, p0, Lcom/samsung/musicplus/widget/appwidget/ServiceAppWidgetManager;->mSize:I

    iget-object v5, p0, Lcom/samsung/musicplus/widget/appwidget/ServiceAppWidgetManager;->mAlbumArtHandler:Landroid/os/Handler;

    invoke-interface/range {v0 .. v5}, Lcom/samsung/musicplus/util/IAlbumArtLoader;->loadArtwork(Landroid/content/Context;ILjava/lang/Object;ILandroid/os/Handler;)V

    .line 79
    return-void
.end method


# virtual methods
.method public performUpdate(Ljava/lang/String;)V
    .locals 4
    .param p1, "action"    # Ljava/lang/String;

    .prologue
    .line 57
    :try_start_0
    iget-object v1, p0, Lcom/samsung/musicplus/widget/appwidget/ServiceAppWidgetManager;->mMusicWidget:Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider;

    iget-object v2, p0, Lcom/samsung/musicplus/widget/appwidget/ServiceAppWidgetManager;->mService:Lcom/samsung/musicplus/service/PlayerService;

    const v3, 0x7f040095

    invoke-virtual {v1, v2, p1, v3}, Lcom/sec/android/app/music/appwidget/MusicAppWidgetProvider;->performUpdate(Lcom/samsung/musicplus/service/PlayerService;Ljava/lang/String;I)V

    .line 58
    const-string v1, "com.android.music.metachanged"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 63
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/appwidget/ServiceAppWidgetManager;->setAlbumArt()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 69
    :cond_0
    :goto_0
    return-void

    .line 65
    :catch_0
    move-exception v0

    .line 67
    .local v0, "e":Ljava/lang/IllegalStateException;
    const-string v1, "MusicWidget"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "performUpdate : IllegalStateException occurred"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
