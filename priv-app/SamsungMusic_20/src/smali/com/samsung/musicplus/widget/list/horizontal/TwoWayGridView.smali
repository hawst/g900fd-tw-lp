.class public Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;
.super Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;
.source "TwoWayGridView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView$1;,
        Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView$HorizontalGridBuilder;,
        Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView$VerticalGridBuilder;,
        Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView$GridBuilder;
    }
.end annotation


# static fields
.field public static final AUTO_FIT:I = -0x1

.field public static final DEBUG:Z = false

.field public static final NO_STRETCH:I = 0x0

.field public static final STRETCH_COLUMN_WIDTH:I = 0x2

.field public static final STRETCH_SPACING:I = 0x1

.field public static final STRETCH_SPACING_UNIFORM:I = 0x3

.field public static final TAG:Ljava/lang/String; = "TwoWayGridView"


# instance fields
.field private mColumnWidth:I

.field private mGravity:I

.field protected mGridBuilder:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView$GridBuilder;

.field private mHorizontalSpacing:I

.field private mNumColumns:I

.field private mNumRows:I

.field private mReferenceView:Landroid/view/View;

.field private mReferenceViewInSelectedRow:Landroid/view/View;

.field private mRequestedColumnWidth:I

.field private mRequestedHorizontalSpacing:I

.field private mRequestedNumColumns:I

.field private mRequestedNumRows:I

.field private mRequestedRowHeight:I

.field private mRequestedVerticalSpacing:I

.field private mRowHeight:I

.field private mStretchMode:I

.field private final mTempRect:Landroid/graphics/Rect;

.field private mVerticalSpacing:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    const/4 v0, -0x1

    const/4 v1, 0x0

    .line 97
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;-><init>(Landroid/content/Context;)V

    .line 72
    iput v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mNumColumns:I

    .line 73
    iput v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mNumRows:I

    .line 75
    iput v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mHorizontalSpacing:I

    .line 77
    iput v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mVerticalSpacing:I

    .line 79
    const/4 v0, 0x2

    iput v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mStretchMode:I

    .line 87
    iput-object v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mReferenceView:Landroid/view/View;

    .line 88
    iput-object v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mReferenceViewInSelectedRow:Landroid/view/View;

    .line 90
    const/4 v0, 0x3

    iput v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mGravity:I

    .line 92
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mTempRect:Landroid/graphics/Rect;

    .line 94
    iput-object v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mGridBuilder:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView$GridBuilder;

    .line 98
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->setupGridType()V

    .line 99
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 102
    const v0, 0x1010071

    invoke-direct {p0, p1, p2, v0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 103
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 106
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 72
    const/4 v8, -0x1

    iput v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mNumColumns:I

    .line 73
    const/4 v8, -0x1

    iput v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mNumRows:I

    .line 75
    const/4 v8, 0x0

    iput v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mHorizontalSpacing:I

    .line 77
    const/4 v8, 0x0

    iput v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mVerticalSpacing:I

    .line 79
    const/4 v8, 0x2

    iput v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mStretchMode:I

    .line 87
    const/4 v8, 0x0

    iput-object v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mReferenceView:Landroid/view/View;

    .line 88
    const/4 v8, 0x0

    iput-object v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mReferenceViewInSelectedRow:Landroid/view/View;

    .line 90
    const/4 v8, 0x3

    iput v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mGravity:I

    .line 92
    new-instance v8, Landroid/graphics/Rect;

    invoke-direct {v8}, Landroid/graphics/Rect;-><init>()V

    iput-object v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mTempRect:Landroid/graphics/Rect;

    .line 94
    const/4 v8, 0x0

    iput-object v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mGridBuilder:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView$GridBuilder;

    .line 108
    sget-object v8, Lcom/sec/android/app/music/R$styleable;->TwoWayGridView:[I

    const/4 v9, 0x0

    invoke-virtual {p1, p2, v8, p3, v9}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 111
    .local v0, "a":Landroid/content/res/TypedArray;
    const/4 v8, 0x1

    const/4 v9, 0x0

    invoke-virtual {v0, v8, v9}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v2

    .line 113
    .local v2, "hSpacing":I
    invoke-virtual {p0, v2}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->setHorizontalSpacing(I)V

    .line 115
    const/4 v8, 0x2

    const/4 v9, 0x0

    invoke-virtual {v0, v8, v9}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v7

    .line 117
    .local v7, "vSpacing":I
    invoke-virtual {p0, v7}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->setVerticalSpacing(I)V

    .line 119
    const/4 v8, 0x3

    const/4 v9, 0x2

    invoke-virtual {v0, v8, v9}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v3

    .line 120
    .local v3, "index":I
    if-ltz v3, :cond_0

    .line 121
    invoke-virtual {p0, v3}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->setStretchMode(I)V

    .line 124
    :cond_0
    const/4 v8, 0x4

    const/4 v9, -0x1

    invoke-virtual {v0, v8, v9}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v1

    .line 125
    .local v1, "columnWidth":I
    if-lez v1, :cond_1

    .line 126
    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->setColumnWidth(I)V

    .line 129
    :cond_1
    const/4 v8, 0x5

    const/4 v9, -0x1

    invoke-virtual {v0, v8, v9}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v6

    .line 130
    .local v6, "rowHeight":I
    if-lez v6, :cond_2

    .line 131
    invoke-virtual {p0, v6}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->setRowHeight(I)V

    .line 134
    :cond_2
    const/4 v8, 0x6

    const/4 v9, 0x1

    invoke-virtual {v0, v8, v9}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v4

    .line 135
    .local v4, "numColumns":I
    invoke-virtual {p0, v4}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->setNumColumns(I)V

    .line 137
    const/4 v8, 0x7

    const/4 v9, 0x1

    invoke-virtual {v0, v8, v9}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v5

    .line 138
    .local v5, "numRows":I
    invoke-virtual {p0, v5}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->setNumRows(I)V

    .line 140
    const/4 v8, 0x0

    const/4 v9, -0x1

    invoke-virtual {v0, v8, v9}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v3

    .line 141
    if-ltz v3, :cond_3

    .line 142
    invoke-virtual {p0, v3}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->setGravity(I)V

    .line 145
    :cond_3
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 146
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->setupGridType()V

    .line 147
    return-void
.end method

.method static synthetic access$1000(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;

    .prologue
    .line 61
    iget v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mRequestedHorizontalSpacing:I

    return v0
.end method

.method static synthetic access$1100(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;

    .prologue
    .line 61
    iget v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mRequestedColumnWidth:I

    return v0
.end method

.method static synthetic access$1200(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;

    .prologue
    .line 61
    iget v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mRequestedVerticalSpacing:I

    return v0
.end method

.method static synthetic access$1300(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;

    .prologue
    .line 61
    iget v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mRequestedNumColumns:I

    return v0
.end method

.method static synthetic access$1400(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;II)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 61
    invoke-virtual {p0, p1, p2}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->setMeasuredDimension(II)V

    return-void
.end method

.method static synthetic access$1500(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;

    .prologue
    .line 61
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->detachAllViewsFromParent()V

    return-void
.end method

.method static synthetic access$1600(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;
    .param p1, "x1"    # Landroid/view/View;
    .param p2, "x2"    # I
    .param p3, "x3"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    .line 61
    invoke-virtual {p0, p1, p2, p3}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->attachViewToParent(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method static synthetic access$1700(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;
    .param p1, "x1"    # Landroid/view/View;
    .param p2, "x2"    # I
    .param p3, "x3"    # Landroid/view/ViewGroup$LayoutParams;
    .param p4, "x4"    # Z

    .prologue
    .line 61
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)Z

    move-result v0

    return v0
.end method

.method static synthetic access$1800(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;Landroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 61
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->cleanupLayoutState(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$1900(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;

    .prologue
    .line 61
    iget v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mGravity:I

    return v0
.end method

.method static synthetic access$2000(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;

    .prologue
    .line 61
    iget v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mNumRows:I

    return v0
.end method

.method static synthetic access$2002(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;
    .param p1, "x1"    # I

    .prologue
    .line 61
    iput p1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mNumRows:I

    return p1
.end method

.method static synthetic access$2100(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;

    .prologue
    .line 61
    iget v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mRowHeight:I

    return v0
.end method

.method static synthetic access$2102(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;
    .param p1, "x1"    # I

    .prologue
    .line 61
    iput p1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mRowHeight:I

    return p1
.end method

.method static synthetic access$2200(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;

    .prologue
    .line 61
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->detachAllViewsFromParent()V

    return-void
.end method

.method static synthetic access$2300(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;

    .prologue
    .line 61
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->getHorizontalScrollbarHeight()I

    move-result v0

    return v0
.end method

.method static synthetic access$2400(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;II)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 61
    invoke-virtual {p0, p1, p2}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->setMeasuredDimension(II)V

    return-void
.end method

.method static synthetic access$2500(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;

    .prologue
    .line 61
    iget v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mRequestedRowHeight:I

    return v0
.end method

.method static synthetic access$2600(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;

    .prologue
    .line 61
    iget v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mRequestedNumRows:I

    return v0
.end method

.method static synthetic access$2700(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;
    .param p1, "x1"    # Landroid/view/View;
    .param p2, "x2"    # I
    .param p3, "x3"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    .line 61
    invoke-virtual {p0, p1, p2, p3}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->attachViewToParent(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method static synthetic access$2800(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;
    .param p1, "x1"    # Landroid/view/View;
    .param p2, "x2"    # I
    .param p3, "x3"    # Landroid/view/ViewGroup$LayoutParams;
    .param p4, "x4"    # Z

    .prologue
    .line 61
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->addViewInLayout(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;Z)Z

    move-result v0

    return v0
.end method

.method static synthetic access$2900(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;Landroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 61
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->cleanupLayoutState(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$300(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;

    .prologue
    .line 61
    iget v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mNumColumns:I

    return v0
.end method

.method static synthetic access$302(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;
    .param p1, "x1"    # I

    .prologue
    .line 61
    iput p1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mNumColumns:I

    return p1
.end method

.method static synthetic access$400(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;

    .prologue
    .line 61
    iget v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mVerticalSpacing:I

    return v0
.end method

.method static synthetic access$402(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;
    .param p1, "x1"    # I

    .prologue
    .line 61
    iput p1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mVerticalSpacing:I

    return p1
.end method

.method static synthetic access$500(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mReferenceView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$502(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;Landroid/view/View;)Landroid/view/View;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mReferenceView:Landroid/view/View;

    return-object p1
.end method

.method static synthetic access$600(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;

    .prologue
    .line 61
    iget v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mColumnWidth:I

    return v0
.end method

.method static synthetic access$602(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;
    .param p1, "x1"    # I

    .prologue
    .line 61
    iput p1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mColumnWidth:I

    return p1
.end method

.method static synthetic access$700(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;

    .prologue
    .line 61
    iget v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mHorizontalSpacing:I

    return v0
.end method

.method static synthetic access$702(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;
    .param p1, "x1"    # I

    .prologue
    .line 61
    iput p1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mHorizontalSpacing:I

    return p1
.end method

.method static synthetic access$800(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;

    .prologue
    .line 61
    iget v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mStretchMode:I

    return v0
.end method

.method static synthetic access$900(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;

    .prologue
    .line 61
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mReferenceViewInSelectedRow:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$902(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;Landroid/view/View;)Landroid/view/View;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 61
    iput-object p1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mReferenceViewInSelectedRow:Landroid/view/View;

    return-object p1
.end method

.method private commonKey(IILandroid/view/KeyEvent;)Z
    .locals 7
    .param p1, "keyCode"    # I
    .param p2, "count"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v2, 0x0

    const/16 v6, 0x82

    const/16 v5, 0x21

    const/4 v3, 0x1

    .line 393
    iget-object v4, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mAdapter:Landroid/widget/ListAdapter;

    if-nez v4, :cond_0

    .line 489
    :goto_0
    return v2

    .line 397
    :cond_0
    iget-boolean v4, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mDataChanged:Z

    if-eqz v4, :cond_1

    .line 398
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->layoutChildren()V

    .line 401
    :cond_1
    const/4 v1, 0x0

    .line 402
    .local v1, "handled":Z
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    .line 404
    .local v0, "action":I
    if-eq v0, v3, :cond_3

    .line 405
    iget v4, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mSelectedPosition:I

    if-gez v4, :cond_2

    .line 406
    sparse-switch p1, :sswitch_data_0

    .line 419
    :cond_2
    sparse-switch p1, :sswitch_data_1

    .line 478
    :cond_3
    :goto_1
    if-eqz v1, :cond_a

    move v2, v3

    .line 479
    goto :goto_0

    .line 414
    :sswitch_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->resurrectSelection()Z

    move v2, v3

    .line 415
    goto :goto_0

    .line 421
    :sswitch_1
    invoke-virtual {p3}, Landroid/view/KeyEvent;->isAltPressed()Z

    move-result v4

    if-nez v4, :cond_4

    .line 422
    iget-object v4, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mGridBuilder:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView$GridBuilder;

    const/16 v5, 0x11

    invoke-virtual {v4, v5}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView$GridBuilder;->arrowScroll(I)Z

    move-result v1

    goto :goto_1

    .line 424
    :cond_4
    invoke-virtual {p0, v5}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->fullScroll(I)Z

    move-result v1

    .line 426
    goto :goto_1

    .line 429
    :sswitch_2
    invoke-virtual {p3}, Landroid/view/KeyEvent;->isAltPressed()Z

    move-result v4

    if-nez v4, :cond_5

    .line 430
    iget-object v4, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mGridBuilder:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView$GridBuilder;

    const/16 v5, 0x42

    invoke-virtual {v4, v5}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView$GridBuilder;->arrowScroll(I)Z

    move-result v1

    goto :goto_1

    .line 432
    :cond_5
    invoke-virtual {p0, v6}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->fullScroll(I)Z

    move-result v1

    .line 434
    goto :goto_1

    .line 437
    :sswitch_3
    invoke-virtual {p3}, Landroid/view/KeyEvent;->isAltPressed()Z

    move-result v4

    if-nez v4, :cond_6

    .line 438
    iget-object v4, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mGridBuilder:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView$GridBuilder;

    invoke-virtual {v4, v5}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView$GridBuilder;->arrowScroll(I)Z

    move-result v1

    goto :goto_1

    .line 441
    :cond_6
    invoke-virtual {p0, v5}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->fullScroll(I)Z

    move-result v1

    .line 443
    goto :goto_1

    .line 446
    :sswitch_4
    invoke-virtual {p3}, Landroid/view/KeyEvent;->isAltPressed()Z

    move-result v4

    if-nez v4, :cond_7

    .line 447
    iget-object v4, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mGridBuilder:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView$GridBuilder;

    invoke-virtual {v4, v6}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView$GridBuilder;->arrowScroll(I)Z

    move-result v1

    goto :goto_1

    .line 449
    :cond_7
    invoke-virtual {p0, v6}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->fullScroll(I)Z

    move-result v1

    .line 451
    goto :goto_1

    .line 455
    :sswitch_5
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->getChildCount()I

    move-result v2

    if-lez v2, :cond_8

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v2

    if-nez v2, :cond_8

    .line 456
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->keyPressed()V

    :cond_8
    move v2, v3

    .line 459
    goto :goto_0

    .line 464
    :sswitch_6
    invoke-virtual {p3}, Landroid/view/KeyEvent;->isShiftPressed()Z

    move-result v4

    if-nez v4, :cond_9

    .line 465
    invoke-virtual {p0, v6}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->pageScroll(I)Z

    move-result v1

    goto :goto_1

    .line 467
    :cond_9
    invoke-virtual {p0, v5}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->pageScroll(I)Z

    move-result v1

    goto :goto_1

    .line 481
    :cond_a
    packed-switch v0, :pswitch_data_0

    goto/16 :goto_0

    .line 483
    :pswitch_0
    invoke-super {p0, p1, p3}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v2

    goto/16 :goto_0

    .line 485
    :pswitch_1
    invoke-super {p0, p1, p3}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v2

    goto/16 :goto_0

    .line 487
    :pswitch_2
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->onKeyMultiple(IILandroid/view/KeyEvent;)Z

    move-result v2

    goto/16 :goto_0

    .line 406
    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_0
        0x14 -> :sswitch_0
        0x15 -> :sswitch_0
        0x16 -> :sswitch_0
        0x17 -> :sswitch_0
        0x3e -> :sswitch_0
        0x42 -> :sswitch_0
    .end sparse-switch

    .line 419
    :sswitch_data_1
    .sparse-switch
        0x13 -> :sswitch_3
        0x14 -> :sswitch_4
        0x15 -> :sswitch_1
        0x16 -> :sswitch_2
        0x17 -> :sswitch_5
        0x3e -> :sswitch_6
        0x42 -> :sswitch_5
    .end sparse-switch

    .line 481
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private setupGridType()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 150
    iget-boolean v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mScrollVertically:Z

    if-eqz v0, :cond_0

    .line 151
    new-instance v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView$VerticalGridBuilder;

    invoke-direct {v0, p0, v1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView$VerticalGridBuilder;-><init>(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView$1;)V

    iput-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mGridBuilder:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView$GridBuilder;

    .line 155
    :goto_0
    return-void

    .line 153
    :cond_0
    new-instance v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView$HorizontalGridBuilder;

    invoke-direct {v0, p0, v1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView$HorizontalGridBuilder;-><init>(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView$1;)V

    iput-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mGridBuilder:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView$GridBuilder;

    goto :goto_0
.end method


# virtual methods
.method protected attachLayoutAnimationParameters(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;II)V
    .locals 4
    .param p1, "child"    # Landroid/view/View;
    .param p2, "params"    # Landroid/view/ViewGroup$LayoutParams;
    .param p3, "index"    # I
    .param p4, "count"    # I

    .prologue
    .line 295
    iget-object v0, p2, Landroid/view/ViewGroup$LayoutParams;->layoutAnimationParameters:Landroid/view/animation/LayoutAnimationController$AnimationParameters;

    check-cast v0, Landroid/view/animation/GridLayoutAnimationController$AnimationParameters;

    .line 298
    .local v0, "animationParams":Landroid/view/animation/GridLayoutAnimationController$AnimationParameters;
    if-nez v0, :cond_0

    .line 299
    new-instance v0, Landroid/view/animation/GridLayoutAnimationController$AnimationParameters;

    .end local v0    # "animationParams":Landroid/view/animation/GridLayoutAnimationController$AnimationParameters;
    invoke-direct {v0}, Landroid/view/animation/GridLayoutAnimationController$AnimationParameters;-><init>()V

    .line 300
    .restart local v0    # "animationParams":Landroid/view/animation/GridLayoutAnimationController$AnimationParameters;
    iput-object v0, p2, Landroid/view/ViewGroup$LayoutParams;->layoutAnimationParameters:Landroid/view/animation/LayoutAnimationController$AnimationParameters;

    .line 303
    :cond_0
    iput p4, v0, Landroid/view/animation/GridLayoutAnimationController$AnimationParameters;->count:I

    .line 304
    iput p3, v0, Landroid/view/animation/GridLayoutAnimationController$AnimationParameters;->index:I

    .line 305
    iget v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mNumColumns:I

    iput v2, v0, Landroid/view/animation/GridLayoutAnimationController$AnimationParameters;->columnsCount:I

    .line 306
    iget v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mNumColumns:I

    div-int v2, p4, v2

    iput v2, v0, Landroid/view/animation/GridLayoutAnimationController$AnimationParameters;->rowsCount:I

    .line 308
    iget-boolean v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mStackFromBottom:Z

    if-nez v2, :cond_1

    .line 309
    iget v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mNumColumns:I

    rem-int v2, p3, v2

    iput v2, v0, Landroid/view/animation/GridLayoutAnimationController$AnimationParameters;->column:I

    .line 310
    iget v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mNumColumns:I

    div-int v2, p3, v2

    iput v2, v0, Landroid/view/animation/GridLayoutAnimationController$AnimationParameters;->row:I

    .line 317
    :goto_0
    return-void

    .line 312
    :cond_1
    add-int/lit8 v2, p4, -0x1

    sub-int v1, v2, p3

    .line 314
    .local v1, "invertedIndex":I
    iget v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mNumColumns:I

    add-int/lit8 v2, v2, -0x1

    iget v3, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mNumColumns:I

    rem-int v3, v1, v3

    sub-int/2addr v2, v3

    iput v2, v0, Landroid/view/animation/GridLayoutAnimationController$AnimationParameters;->column:I

    .line 315
    iget v2, v0, Landroid/view/animation/GridLayoutAnimationController$AnimationParameters;->rowsCount:I

    add-int/lit8 v2, v2, -0x1

    iget v3, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mNumColumns:I

    div-int v3, v1, v3

    sub-int/2addr v2, v3

    iput v2, v0, Landroid/view/animation/GridLayoutAnimationController$AnimationParameters;->row:I

    goto :goto_0
.end method

.method protected computeHorizontalScrollExtent()I
    .locals 10

    .prologue
    const/4 v8, 0x0

    .line 771
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->getChildCount()I

    move-result v1

    .line 772
    .local v1, "count":I
    if-lez v1, :cond_2

    iget-boolean v9, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mScrollVertically:Z

    if-nez v9, :cond_2

    .line 773
    iget v4, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mNumRows:I

    .line 774
    .local v4, "numRows":I
    add-int v9, v1, v4

    add-int/lit8 v9, v9, -0x1

    div-int v0, v9, v4

    .line 776
    .local v0, "columnCount":I
    mul-int/lit8 v2, v0, 0x64

    .line 778
    .local v2, "extent":I
    invoke-virtual {p0, v8}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 779
    .local v6, "view":Landroid/view/View;
    invoke-virtual {v6}, Landroid/view/View;->getLeft()I

    move-result v3

    .line 780
    .local v3, "left":I
    invoke-virtual {v6}, Landroid/view/View;->getWidth()I

    move-result v7

    .line 781
    .local v7, "width":I
    if-lez v7, :cond_0

    .line 782
    mul-int/lit8 v8, v3, 0x64

    div-int/2addr v8, v7

    add-int/2addr v2, v8

    .line 785
    :cond_0
    add-int/lit8 v8, v1, -0x1

    invoke-virtual {p0, v8}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 786
    invoke-virtual {v6}, Landroid/view/View;->getRight()I

    move-result v5

    .line 787
    .local v5, "right":I
    invoke-virtual {v6}, Landroid/view/View;->getWidth()I

    move-result v7

    .line 788
    if-lez v7, :cond_1

    .line 789
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->getWidth()I

    move-result v8

    sub-int v8, v5, v8

    mul-int/lit8 v8, v8, 0x64

    div-int/2addr v8, v7

    sub-int/2addr v2, v8

    .line 794
    .end local v0    # "columnCount":I
    .end local v2    # "extent":I
    .end local v3    # "left":I
    .end local v4    # "numRows":I
    .end local v5    # "right":I
    .end local v6    # "view":Landroid/view/View;
    .end local v7    # "width":I
    :cond_1
    :goto_0
    return v2

    :cond_2
    move v2, v8

    goto :goto_0
.end method

.method protected computeHorizontalScrollOffset()I
    .locals 10

    .prologue
    const/4 v6, 0x0

    .line 799
    iget v7, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mFirstPosition:I

    if-ltz v7, :cond_0

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->getChildCount()I

    move-result v7

    if-lez v7, :cond_0

    iget-boolean v7, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mScrollVertically:Z

    if-nez v7, :cond_0

    .line 800
    invoke-virtual {p0, v6}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 801
    .local v3, "view":Landroid/view/View;
    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v1

    .line 802
    .local v1, "left":I
    invoke-virtual {v3}, Landroid/view/View;->getWidth()I

    move-result v5

    .line 803
    .local v5, "width":I
    if-lez v5, :cond_0

    .line 804
    iget v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mNumRows:I

    .line 805
    .local v2, "numRows":I
    iget v7, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mFirstPosition:I

    div-int v4, v7, v2

    .line 806
    .local v4, "whichColumn":I
    iget v7, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mItemCount:I

    add-int/2addr v7, v2

    add-int/lit8 v7, v7, -0x1

    div-int v0, v7, v2

    .line 807
    .local v0, "columnCount":I
    mul-int/lit8 v7, v4, 0x64

    mul-int/lit8 v8, v1, 0x64

    div-int/2addr v8, v5

    sub-int/2addr v7, v8

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->getScrollX()I

    move-result v8

    int-to-float v8, v8

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->getWidth()I

    move-result v9

    int-to-float v9, v9

    div-float/2addr v8, v9

    int-to-float v9, v0

    mul-float/2addr v8, v9

    const/high16 v9, 0x42c80000    # 100.0f

    mul-float/2addr v8, v9

    float-to-int v8, v8

    add-int/2addr v7, v8

    invoke-static {v7, v6}, Ljava/lang/Math;->max(II)I

    move-result v6

    .line 811
    .end local v0    # "columnCount":I
    .end local v1    # "left":I
    .end local v2    # "numRows":I
    .end local v3    # "view":Landroid/view/View;
    .end local v4    # "whichColumn":I
    .end local v5    # "width":I
    :cond_0
    return v6
.end method

.method protected computeHorizontalScrollRange()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 817
    iget-boolean v3, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mScrollVertically:Z

    if-eqz v3, :cond_0

    .line 822
    :goto_0
    return v2

    .line 820
    :cond_0
    iget v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mNumRows:I

    .line 821
    .local v1, "numRows":I
    iget v3, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mItemCount:I

    add-int/2addr v3, v1

    add-int/lit8 v3, v3, -0x1

    div-int v0, v3, v1

    .line 822
    .local v0, "columnCount":I
    mul-int/lit8 v3, v0, 0x64

    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    goto :goto_0
.end method

.method protected computeVerticalScrollExtent()I
    .locals 10

    .prologue
    const/4 v8, 0x0

    .line 715
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->getChildCount()I

    move-result v1

    .line 716
    .local v1, "count":I
    if-lez v1, :cond_2

    iget-boolean v9, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mScrollVertically:Z

    if-eqz v9, :cond_2

    .line 717
    iget v4, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mNumColumns:I

    .line 718
    .local v4, "numColumns":I
    add-int v9, v1, v4

    add-int/lit8 v9, v9, -0x1

    div-int v5, v9, v4

    .line 720
    .local v5, "rowCount":I
    mul-int/lit8 v2, v5, 0x64

    .line 722
    .local v2, "extent":I
    invoke-virtual {p0, v8}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    .line 723
    .local v7, "view":Landroid/view/View;
    invoke-virtual {v7}, Landroid/view/View;->getTop()I

    move-result v6

    .line 724
    .local v6, "top":I
    invoke-virtual {v7}, Landroid/view/View;->getHeight()I

    move-result v3

    .line 725
    .local v3, "height":I
    if-lez v3, :cond_0

    .line 726
    mul-int/lit8 v8, v6, 0x64

    div-int/2addr v8, v3

    add-int/2addr v2, v8

    .line 729
    :cond_0
    add-int/lit8 v8, v1, -0x1

    invoke-virtual {p0, v8}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    .line 730
    invoke-virtual {v7}, Landroid/view/View;->getBottom()I

    move-result v0

    .line 731
    .local v0, "bottom":I
    invoke-virtual {v7}, Landroid/view/View;->getHeight()I

    move-result v3

    .line 732
    if-lez v3, :cond_1

    .line 733
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->getHeight()I

    move-result v8

    sub-int v8, v0, v8

    mul-int/lit8 v8, v8, 0x64

    div-int/2addr v8, v3

    sub-int/2addr v2, v8

    .line 738
    .end local v0    # "bottom":I
    .end local v2    # "extent":I
    .end local v3    # "height":I
    .end local v4    # "numColumns":I
    .end local v5    # "rowCount":I
    .end local v6    # "top":I
    .end local v7    # "view":Landroid/view/View;
    :cond_1
    :goto_0
    return v2

    :cond_2
    move v2, v8

    goto :goto_0
.end method

.method protected computeVerticalScrollOffset()I
    .locals 10

    .prologue
    const/4 v6, 0x0

    .line 743
    iget v7, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mFirstPosition:I

    if-ltz v7, :cond_0

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->getChildCount()I

    move-result v7

    if-lez v7, :cond_0

    iget-boolean v7, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mScrollVertically:Z

    if-eqz v7, :cond_0

    .line 744
    invoke-virtual {p0, v6}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 745
    .local v4, "view":Landroid/view/View;
    invoke-virtual {v4}, Landroid/view/View;->getTop()I

    move-result v3

    .line 746
    .local v3, "top":I
    invoke-virtual {v4}, Landroid/view/View;->getHeight()I

    move-result v0

    .line 747
    .local v0, "height":I
    if-lez v0, :cond_0

    .line 748
    iget v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mNumColumns:I

    .line 749
    .local v1, "numColumns":I
    iget v7, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mFirstPosition:I

    div-int v5, v7, v1

    .line 750
    .local v5, "whichRow":I
    iget v7, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mItemCount:I

    add-int/2addr v7, v1

    add-int/lit8 v7, v7, -0x1

    div-int v2, v7, v1

    .line 751
    .local v2, "rowCount":I
    mul-int/lit8 v7, v5, 0x64

    mul-int/lit8 v8, v3, 0x64

    div-int/2addr v8, v0

    sub-int/2addr v7, v8

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->getScrollY()I

    move-result v8

    int-to-float v8, v8

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->getHeight()I

    move-result v9

    int-to-float v9, v9

    div-float/2addr v8, v9

    int-to-float v9, v2

    mul-float/2addr v8, v9

    const/high16 v9, 0x42c80000    # 100.0f

    mul-float/2addr v8, v9

    float-to-int v8, v8

    add-int/2addr v7, v8

    invoke-static {v7, v6}, Ljava/lang/Math;->max(II)I

    move-result v6

    .line 755
    .end local v0    # "height":I
    .end local v1    # "numColumns":I
    .end local v2    # "rowCount":I
    .end local v3    # "top":I
    .end local v4    # "view":Landroid/view/View;
    .end local v5    # "whichRow":I
    :cond_0
    return v6
.end method

.method protected computeVerticalScrollRange()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 761
    iget-boolean v3, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mScrollVertically:Z

    if-nez v3, :cond_0

    .line 766
    :goto_0
    return v2

    .line 764
    :cond_0
    iget v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mNumColumns:I

    .line 765
    .local v0, "numColumns":I
    iget v3, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mItemCount:I

    add-int/2addr v3, v0

    add-int/lit8 v3, v3, -0x1

    div-int v1, v3, v0

    .line 766
    .local v1, "rowCount":I
    mul-int/lit8 v3, v1, 0x64

    invoke-static {v3, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    goto :goto_0
.end method

.method fillGap(Z)V
    .locals 1
    .param p1, "down"    # Z

    .prologue
    .line 228
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mGridBuilder:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView$GridBuilder;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView$GridBuilder;->fillGap(Z)V

    .line 229
    return-void
.end method

.method findMotionRowX(I)I
    .locals 4
    .param p1, "x"    # I

    .prologue
    .line 256
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->getChildCount()I

    move-result v0

    .line 257
    .local v0, "childCount":I
    if-lez v0, :cond_3

    .line 259
    iget v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mNumRows:I

    .line 260
    .local v2, "numRows":I
    iget-boolean v3, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mStackFromBottom:Z

    if-nez v3, :cond_1

    .line 261
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_3

    .line 262
    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getRight()I

    move-result v3

    if-gt p1, v3, :cond_0

    .line 263
    iget v3, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mFirstPosition:I

    add-int/2addr v3, v1

    .line 274
    .end local v1    # "i":I
    .end local v2    # "numRows":I
    :goto_1
    return v3

    .line 261
    .restart local v1    # "i":I
    .restart local v2    # "numRows":I
    :cond_0
    add-int/2addr v1, v2

    goto :goto_0

    .line 267
    .end local v1    # "i":I
    :cond_1
    add-int/lit8 v1, v0, -0x1

    .restart local v1    # "i":I
    :goto_2
    if-ltz v1, :cond_3

    .line 268
    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v3

    if-lt p1, v3, :cond_2

    .line 269
    iget v3, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mFirstPosition:I

    add-int/2addr v3, v1

    goto :goto_1

    .line 267
    :cond_2
    sub-int/2addr v1, v2

    goto :goto_2

    .line 274
    .end local v1    # "i":I
    .end local v2    # "numRows":I
    :cond_3
    const/4 v3, -0x1

    goto :goto_1
.end method

.method findMotionRowY(I)I
    .locals 4
    .param p1, "y"    # I

    .prologue
    .line 233
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->getChildCount()I

    move-result v0

    .line 234
    .local v0, "childCount":I
    if-lez v0, :cond_3

    .line 236
    iget v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mNumColumns:I

    .line 237
    .local v2, "numColumns":I
    iget-boolean v3, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mStackFromBottom:Z

    if-nez v3, :cond_1

    .line 238
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_3

    .line 239
    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getBottom()I

    move-result v3

    if-gt p1, v3, :cond_0

    .line 240
    iget v3, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mFirstPosition:I

    add-int/2addr v3, v1

    .line 251
    .end local v1    # "i":I
    .end local v2    # "numColumns":I
    :goto_1
    return v3

    .line 238
    .restart local v1    # "i":I
    .restart local v2    # "numColumns":I
    :cond_0
    add-int/2addr v1, v2

    goto :goto_0

    .line 244
    .end local v1    # "i":I
    :cond_1
    add-int/lit8 v1, v0, -0x1

    .restart local v1    # "i":I
    :goto_2
    if-ltz v1, :cond_3

    .line 245
    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v3

    if-lt p1, v3, :cond_2

    .line 246
    iget v3, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mFirstPosition:I

    add-int/2addr v3, v1

    goto :goto_1

    .line 244
    :cond_2
    sub-int/2addr v1, v2

    goto :goto_2

    .line 251
    .end local v1    # "i":I
    .end local v2    # "numColumns":I
    :cond_3
    const/4 v3, -0x1

    goto :goto_1
.end method

.method fullScroll(I)Z
    .locals 3
    .param p1, "direction"    # I

    .prologue
    const/4 v2, 0x2

    .line 527
    const/4 v0, 0x0

    .line 528
    .local v0, "moved":Z
    const/16 v1, 0x21

    if-ne p1, v1, :cond_2

    .line 529
    iput v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mLayoutMode:I

    .line 530
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->setSelectionInt(I)V

    .line 531
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->invokeOnItemScrollListener()V

    .line 532
    const/4 v0, 0x1

    .line 540
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    .line 544
    :cond_1
    return v0

    .line 533
    :cond_2
    const/16 v1, 0x82

    if-ne p1, v1, :cond_0

    .line 534
    iput v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mLayoutMode:I

    .line 535
    iget v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mItemCount:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->setSelectionInt(I)V

    .line 536
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->invokeOnItemScrollListener()V

    .line 537
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public bridge synthetic getAdapter()Landroid/widget/Adapter;
    .locals 1

    .prologue
    .line 61
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    return-object v0
.end method

.method public getAdapter()Landroid/widget/ListAdapter;
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mAdapter:Landroid/widget/ListAdapter;

    return-object v0
.end method

.method public getStretchMode()I
    .locals 1

    .prologue
    .line 652
    iget v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mStretchMode:I

    return v0
.end method

.method protected layoutChildren()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 321
    iget-boolean v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mBlockLayoutRequests:Z

    .line 322
    .local v0, "blockLayoutRequests":Z
    if-nez v0, :cond_0

    .line 323
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mBlockLayoutRequests:Z

    .line 327
    :cond_0
    :try_start_0
    invoke-super {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->layoutChildren()V

    .line 329
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->invalidate()V

    .line 331
    iget-object v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mAdapter:Landroid/widget/ListAdapter;

    if-nez v1, :cond_2

    .line 332
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->resetList()V

    .line 333
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->invokeOnItemScrollListener()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 340
    if-nez v0, :cond_1

    .line 341
    iput-boolean v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mBlockLayoutRequests:Z

    .line 344
    :cond_1
    :goto_0
    return-void

    .line 337
    :cond_2
    :try_start_1
    iget-object v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mGridBuilder:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView$GridBuilder;

    invoke-virtual {v1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView$GridBuilder;->layoutChildren()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 340
    if-nez v0, :cond_1

    .line 341
    iput-boolean v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mBlockLayoutRequests:Z

    goto :goto_0

    .line 340
    :catchall_0
    move-exception v1

    if-nez v0, :cond_3

    .line 341
    iput-boolean v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mBlockLayoutRequests:Z

    :cond_3
    throw v1
.end method

.method lookForSelectablePosition(IZ)I
    .locals 3
    .param p1, "position"    # I
    .param p2, "lookDown"    # Z

    .prologue
    const/4 v1, -0x1

    .line 211
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mAdapter:Landroid/widget/ListAdapter;

    .line 212
    .local v0, "adapter":Landroid/widget/ListAdapter;
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->isInTouchMode()Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_0
    move p1, v1

    .line 219
    .end local p1    # "position":I
    :cond_1
    :goto_0
    return p1

    .line 216
    .restart local p1    # "position":I
    :cond_2
    if-ltz p1, :cond_3

    iget v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mItemCount:I

    if-lt p1, v2, :cond_1

    :cond_3
    move p1, v1

    .line 217
    goto :goto_0
.end method

.method protected onFocusChanged(ZILandroid/graphics/Rect;)V
    .locals 9
    .param p1, "gainFocus"    # Z
    .param p2, "direction"    # I
    .param p3, "previouslyFocusedRect"    # Landroid/graphics/Rect;

    .prologue
    .line 551
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->onFocusChanged(ZILandroid/graphics/Rect;)V

    .line 553
    const/4 v1, -0x1

    .line 554
    .local v1, "closestChildIndex":I
    if-eqz p1, :cond_2

    if-eqz p3, :cond_2

    .line 555
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->getScrollX()I

    move-result v7

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->getScrollY()I

    move-result v8

    invoke-virtual {p3, v7, v8}, Landroid/graphics/Rect;->offset(II)V

    .line 559
    iget-object v6, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mTempRect:Landroid/graphics/Rect;

    .line 560
    .local v6, "otherRect":Landroid/graphics/Rect;
    const v4, 0x7fffffff

    .line 561
    .local v4, "minDistance":I
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->getChildCount()I

    move-result v0

    .line 562
    .local v0, "childCount":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v0, :cond_2

    .line 564
    iget-object v7, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mGridBuilder:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView$GridBuilder;

    invoke-virtual {v7, v3, p2}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView$GridBuilder;->isCandidateSelection(II)Z

    move-result v7

    if-nez v7, :cond_1

    .line 562
    :cond_0
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 568
    :cond_1
    invoke-virtual {p0, v3}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 569
    .local v5, "other":Landroid/view/View;
    invoke-virtual {v5, v6}, Landroid/view/View;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 570
    invoke-virtual {p0, v5, v6}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 571
    invoke-static {p3, v6, p2}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->getDistance(Landroid/graphics/Rect;Landroid/graphics/Rect;I)I

    move-result v2

    .line 573
    .local v2, "distance":I
    if-ge v2, v4, :cond_0

    .line 574
    move v4, v2

    .line 575
    move v1, v3

    goto :goto_1

    .line 580
    .end local v0    # "childCount":I
    .end local v2    # "distance":I
    .end local v3    # "i":I
    .end local v4    # "minDistance":I
    .end local v5    # "other":Landroid/view/View;
    .end local v6    # "otherRect":Landroid/graphics/Rect;
    :cond_2
    if-ltz v1, :cond_3

    .line 581
    iget v7, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mFirstPosition:I

    add-int/2addr v7, v1

    invoke-virtual {p0, v7}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->setSelection(I)V

    .line 585
    :goto_2
    return-void

    .line 583
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->requestLayout()V

    goto :goto_2
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 379
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0, p2}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->commonKey(IILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public onKeyMultiple(IILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "repeatCount"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 384
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->commonKey(IILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 389
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0, p2}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->commonKey(IILandroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method protected onMeasure(II)V
    .locals 1
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "WrongCall"
        }
    .end annotation

    .prologue
    .line 281
    invoke-super {p0, p1, p2}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->onMeasure(II)V

    .line 282
    iget-boolean v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mScrollVertically:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mGridBuilder:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView$GridBuilder;

    instance-of v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView$VerticalGridBuilder;

    if-eqz v0, :cond_1

    :cond_0
    iget-boolean v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mScrollVertically:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mGridBuilder:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView$GridBuilder;

    instance-of v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView$HorizontalGridBuilder;

    if-nez v0, :cond_2

    .line 284
    :cond_1
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->setupGridType()V

    .line 287
    :cond_2
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mGridBuilder:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView$GridBuilder;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView$GridBuilder;->onMeasure(II)V

    .line 288
    return-void
.end method

.method pageScroll(I)Z
    .locals 5
    .param p1, "direction"    # I

    .prologue
    const/4 v1, 0x0

    .line 501
    const/4 v0, -0x1

    .line 503
    .local v0, "nextPage":I
    const/16 v2, 0x21

    if-ne p1, v2, :cond_2

    .line 504
    iget v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mSelectedPosition:I

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->getChildCount()I

    move-result v3

    sub-int/2addr v2, v3

    add-int/lit8 v2, v2, -0x1

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 509
    :cond_0
    :goto_0
    if-ltz v0, :cond_1

    .line 510
    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->setSelectionInt(I)V

    .line 511
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->invokeOnItemScrollListener()V

    .line 513
    const/4 v1, 0x1

    .line 516
    :cond_1
    return v1

    .line 505
    :cond_2
    const/16 v2, 0x82

    if-ne p1, v2, :cond_0

    .line 506
    iget v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mItemCount:I

    add-int/lit8 v2, v2, -0x1

    iget v3, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mSelectedPosition:I

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->getChildCount()I

    move-result v4

    add-int/2addr v3, v4

    add-int/lit8 v3, v3, -0x1

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0
.end method

.method public bridge synthetic setAdapter(Landroid/widget/Adapter;)V
    .locals 0
    .param p1, "x0"    # Landroid/widget/Adapter;

    .prologue
    .line 61
    check-cast p1, Landroid/widget/ListAdapter;

    .end local p1    # "x0":Landroid/widget/Adapter;
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .locals 6
    .param p1, "adapter"    # Landroid/widget/ListAdapter;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 173
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->resetList()V

    .line 174
    iget-object v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mRecycler:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$RecycleBin;

    invoke-virtual {v1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$RecycleBin;->clear()V

    .line 175
    iput-object p1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mAdapter:Landroid/widget/ListAdapter;

    .line 177
    const/4 v1, -0x1

    iput v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mOldSelectedPosition:I

    .line 178
    const-wide/high16 v2, -0x8000000000000000L

    iput-wide v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mOldSelectedRowId:J

    .line 180
    iget-object v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mAdapter:Landroid/widget/ListAdapter;

    if-eqz v1, :cond_1

    .line 181
    iget v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mItemCount:I

    iput v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mOldItemCount:I

    .line 182
    iget-object v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v1}, Landroid/widget/ListAdapter;->getCount()I

    move-result v1

    iput v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mItemCount:I

    .line 183
    iput-boolean v5, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mDataChanged:Z

    .line 184
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->checkFocus()V

    .line 189
    iget-object v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mRecycler:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$RecycleBin;

    iget-object v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v2}, Landroid/widget/ListAdapter;->getViewTypeCount()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$RecycleBin;->setViewTypeCount(I)V

    .line 192
    iget-boolean v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mStackFromBottom:Z

    if-eqz v1, :cond_0

    .line 193
    iget v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mItemCount:I

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {p0, v1, v4}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->lookForSelectablePosition(IZ)I

    move-result v0

    .line 197
    .local v0, "position":I
    :goto_0
    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->setSelectedPositionInt(I)V

    .line 198
    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->setNextSelectedPositionInt(I)V

    .line 199
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->checkSelectionChanged()V

    .line 206
    .end local v0    # "position":I
    :goto_1
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->requestLayout()V

    .line 207
    return-void

    .line 195
    :cond_0
    invoke-virtual {p0, v4, v5}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->lookForSelectablePosition(IZ)I

    move-result v0

    .restart local v0    # "position":I
    goto :goto_0

    .line 201
    .end local v0    # "position":I
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->checkFocus()V

    .line 203
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->checkSelectionChanged()V

    goto :goto_1
.end method

.method public final setColumnWidth(I)V
    .locals 1
    .param p1, "columnWidth"    # I

    .prologue
    .line 663
    iget v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mRequestedColumnWidth:I

    if-eq p1, v0, :cond_0

    .line 664
    iput p1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mRequestedColumnWidth:I

    .line 665
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->requestLayoutIfNecessary()V

    .line 667
    :cond_0
    return-void
.end method

.method public setGravity(I)V
    .locals 1
    .param p1, "gravity"    # I

    .prologue
    .line 597
    iget v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mGravity:I

    if-eq v0, p1, :cond_0

    .line 598
    iput p1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mGravity:I

    .line 599
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->requestLayoutIfNecessary()V

    .line 601
    :cond_0
    return-void
.end method

.method public final setHorizontalSpacing(I)V
    .locals 1
    .param p1, "horizontalSpacing"    # I

    .prologue
    .line 613
    iget v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mRequestedHorizontalSpacing:I

    if-eq p1, v0, :cond_0

    .line 614
    iput p1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mRequestedHorizontalSpacing:I

    .line 615
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->requestLayoutIfNecessary()V

    .line 617
    :cond_0
    return-void
.end method

.method public final setNumColumns(I)V
    .locals 1
    .param p1, "numColumns"    # I

    .prologue
    .line 691
    iget v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mRequestedNumColumns:I

    if-eq p1, v0, :cond_0

    .line 692
    iput p1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mRequestedNumColumns:I

    .line 693
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->requestLayoutIfNecessary()V

    .line 695
    :cond_0
    return-void
.end method

.method public final setNumRows(I)V
    .locals 1
    .param p1, "numRows"    # I

    .prologue
    .line 705
    iget v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mRequestedNumRows:I

    if-eq p1, v0, :cond_0

    .line 706
    iput p1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mRequestedNumRows:I

    .line 707
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->requestLayoutIfNecessary()V

    .line 709
    :cond_0
    return-void
.end method

.method public final setRowHeight(I)V
    .locals 1
    .param p1, "rowHeight"    # I

    .prologue
    .line 677
    iget v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mRequestedRowHeight:I

    if-eq p1, v0, :cond_0

    .line 678
    iput p1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mRequestedRowHeight:I

    .line 679
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->requestLayoutIfNecessary()V

    .line 681
    :cond_0
    return-void
.end method

.method public setSelection(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 358
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->isInTouchMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 359
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->setNextSelectedPositionInt(I)V

    .line 363
    :goto_0
    const/4 v0, 0x2

    iput v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mLayoutMode:I

    .line 364
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->requestLayout()V

    .line 365
    return-void

    .line 361
    :cond_0
    iput p1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mResurrectToPosition:I

    goto :goto_0
.end method

.method setSelectionInt(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 374
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mGridBuilder:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView$GridBuilder;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView$GridBuilder;->setSelectionInt(I)V

    .line 375
    return-void
.end method

.method public final setStretchMode(I)V
    .locals 1
    .param p1, "stretchMode"    # I

    .prologue
    .line 645
    iget v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mStretchMode:I

    if-eq p1, v0, :cond_0

    .line 646
    iput p1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mStretchMode:I

    .line 647
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->requestLayoutIfNecessary()V

    .line 649
    :cond_0
    return-void
.end method

.method public final setVerticalSpacing(I)V
    .locals 1
    .param p1, "verticalSpacing"    # I

    .prologue
    .line 630
    iget v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mRequestedVerticalSpacing:I

    if-eq p1, v0, :cond_0

    .line 631
    iput p1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->mRequestedVerticalSpacing:I

    .line 632
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayGridView;->requestLayoutIfNecessary()V

    .line 634
    :cond_0
    return-void
.end method
