.class public Lcom/samsung/musicplus/widget/appwidget/CommonServiceAppWidgetProvider;
.super Landroid/appwidget/AppWidgetProvider;
.source "CommonServiceAppWidgetProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/widget/appwidget/CommonServiceAppWidgetProvider$WidgetRemoteViewBuilder;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "MusicAppWidget"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Landroid/appwidget/AppWidgetProvider;-><init>()V

    .line 246
    return-void
.end method


# virtual methods
.method protected notifyAppWidget(Lcom/samsung/musicplus/widget/appwidget/CommonServiceAppWidgetProvider$WidgetRemoteViewBuilder;Ljava/lang/Object;)V
    .locals 7
    .param p1, "remoteViewBuilder"    # Lcom/samsung/musicplus/widget/appwidget/CommonServiceAppWidgetProvider$WidgetRemoteViewBuilder;
    .param p2, "instance"    # Ljava/lang/Object;

    .prologue
    .line 217
    invoke-virtual {p1}, Lcom/samsung/musicplus/widget/appwidget/CommonServiceAppWidgetProvider$WidgetRemoteViewBuilder;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 218
    .local v1, "context":Landroid/content/Context;
    invoke-static {v1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v3

    .line 219
    .local v3, "mgr":Landroid/appwidget/AppWidgetManager;
    new-instance v4, Landroid/content/ComponentName;

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v5

    invoke-direct {v4, v1, v5}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v3, v4}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v0

    .line 220
    .local v0, "appWidgetIds":[I
    array-length v4, v0

    if-gtz v4, :cond_0

    .line 244
    :goto_0
    return-void

    .line 236
    :cond_0
    :try_start_0
    invoke-virtual {p1}, Lcom/samsung/musicplus/widget/appwidget/CommonServiceAppWidgetProvider$WidgetRemoteViewBuilder;->build()Landroid/widget/RemoteViews;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Landroid/appwidget/AppWidgetManager;->updateAppWidget([ILandroid/widget/RemoteViews;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 237
    :catch_0
    move-exception v2

    .line 238
    .local v2, "e":Ljava/lang/IllegalArgumentException;
    const-string v4, "MusicAppWidget"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "notifyAppWidget : IllegalArgumentException occurred"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/musicplus/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 239
    invoke-virtual {v2}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    .line 240
    .end local v2    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v2

    .line 242
    .local v2, "e":Ljava/lang/IllegalStateException;
    const-string v4, "MusicAppWidget"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "notifyAppWidget : IllegalStateException occurred"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/musicplus/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onAppWidgetOptionsChanged(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;ILandroid/os/Bundle;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appWidgetManager"    # Landroid/appwidget/AppWidgetManager;
    .param p3, "appWidgetId"    # I
    .param p4, "newOptions"    # Landroid/os/Bundle;

    .prologue
    .line 70
    const-string v5, "appWidgetMinWidth"

    invoke-virtual {p4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    .line 71
    .local v4, "minWidth":I
    const-string v5, "appWidgetMaxWidth"

    invoke-virtual {p4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 72
    .local v2, "maxWidth":I
    const-string v5, "appWidgetMinHeight"

    invoke-virtual {p4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 73
    .local v3, "minHeight":I
    const-string v5, "appWidgetMaxHeight"

    invoke-virtual {p4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 74
    .local v1, "maxHeight":I
    const-string v5, "appWidgetCategory"

    invoke-virtual {p4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 75
    .local v0, "category":I
    const-string v5, "MusicAppWidget"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "onAppWidgetOptionsChanged appWidgetId : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " category : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " minWidth: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " maxWidth: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " minHeight : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " maxHeight: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 78
    invoke-super {p0, p1, p2, p3, p4}, Landroid/appwidget/AppWidgetProvider;->onAppWidgetOptionsChanged(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;ILandroid/os/Bundle;)V

    .line 79
    return-void
.end method

.method public onDeleted(Landroid/content/Context;[I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appWidgetIds"    # [I

    .prologue
    .line 55
    const-string v0, "MusicAppWidget"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "onDeleted"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    invoke-super {p0, p1, p2}, Landroid/appwidget/AppWidgetProvider;->onDeleted(Landroid/content/Context;[I)V

    .line 57
    return-void
.end method

.method public onDisabled(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 37
    const-string v0, "MusicAppWidget"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "onDisabled"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    invoke-super {p0, p1}, Landroid/appwidget/AppWidgetProvider;->onDisabled(Landroid/content/Context;)V

    .line 39
    return-void
.end method

.method public onEnabled(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 31
    const-string v0, "MusicAppWidget"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "onEnabled"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 32
    invoke-super {p0, p1}, Landroid/appwidget/AppWidgetProvider;->onEnabled(Landroid/content/Context;)V

    .line 33
    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 61
    const-string v0, "MusicAppWidget"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "onReceive action : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    invoke-super {p0, p1, p2}, Landroid/appwidget/AppWidgetProvider;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    .line 63
    return-void
.end method

.method public onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "mgr"    # Landroid/appwidget/AppWidgetManager;
    .param p3, "appWidgetIds"    # [I

    .prologue
    .line 43
    const-string v1, "MusicAppWidget"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "onUpdate appWidgetIds.length"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    array-length v3, p3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.musicplus.intent.action.UPDATE_WIDGET"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 47
    .local v0, "i":Landroid/content/Intent;
    new-instance v1, Landroid/content/ComponentName;

    const-class v2, Lcom/samsung/musicplus/service/PlayerService;

    invoke-direct {v1, p1, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 49
    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 50
    invoke-super {p0, p1, p2, p3}, Landroid/appwidget/AppWidgetProvider;->onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    .line 51
    return-void
.end method

.method public performUpdate(Lcom/samsung/musicplus/service/PlayerService;Ljava/lang/String;I)V
    .locals 10
    .param p1, "s"    # Lcom/samsung/musicplus/service/PlayerService;
    .param p2, "action"    # Ljava/lang/String;
    .param p3, "layout"    # I

    .prologue
    const-wide/16 v8, -0x1

    .line 89
    invoke-virtual {p1}, Lcom/samsung/musicplus/service/PlayerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 90
    .local v2, "context":Landroid/content/Context;
    invoke-static {v2}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v3

    .line 93
    .local v3, "mgr":Landroid/appwidget/AppWidgetManager;
    :try_start_0
    new-instance v0, Landroid/content/ComponentName;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-direct {v0, v2, v1}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v3, v0}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 99
    .local v4, "appWidgetIds":[I
    const-string v0, "MusicAppWidget"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, "performUpdate s :"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, " action : "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, " appWidgetIds.length : "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    array-length v5, v4

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    array-length v0, v4

    if-gtz v0, :cond_1

    .line 125
    .end local v4    # "appWidgetIds":[I
    :cond_0
    :goto_0
    return-void

    .line 94
    :catch_0
    move-exception v7

    .line 95
    .local v7, "e":Ljava/lang/RuntimeException;
    const-string v0, "MusicAppWidget"

    const-string v1, "Failed getAppWidgetIds call"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 106
    .end local v7    # "e":Ljava/lang/RuntimeException;
    .restart local v4    # "appWidgetIds":[I
    :cond_1
    const-string v0, "com.android.music.metachanged"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    move-object v0, p0

    move-object v1, p1

    move v5, p3

    .line 107
    invoke-virtual/range {v0 .. v5}, Lcom/samsung/musicplus/widget/appwidget/CommonServiceAppWidgetProvider;->updateMetaInfo(Lcom/samsung/musicplus/service/PlayerService;Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[II)V

    .line 108
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/samsung/musicplus/service/PlayerService;->getAudioId()J

    move-result-wide v0

    cmp-long v0, v0, v8

    if-lez v0, :cond_0

    .line 109
    invoke-virtual {p0, v3, v4}, Lcom/samsung/musicplus/widget/appwidget/CommonServiceAppWidgetProvider;->updateList(Landroid/appwidget/AppWidgetManager;[I)V

    goto :goto_0

    .line 111
    :cond_2
    const-string v0, "com.android.music.playstatechanged"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    move-object v0, p0

    move-object v1, p1

    move v5, p3

    .line 112
    invoke-virtual/range {v0 .. v5}, Lcom/samsung/musicplus/widget/appwidget/CommonServiceAppWidgetProvider;->updatePlayStatus(Lcom/samsung/musicplus/service/PlayerService;Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[II)V

    .line 113
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/samsung/musicplus/service/PlayerService;->getAudioId()J

    move-result-wide v0

    cmp-long v0, v0, v8

    if-lez v0, :cond_0

    .line 114
    invoke-virtual {p0, v3, v4}, Lcom/samsung/musicplus/widget/appwidget/CommonServiceAppWidgetProvider;->updateList(Landroid/appwidget/AppWidgetManager;[I)V

    goto :goto_0

    .line 116
    :cond_3
    const-string v0, "com.android.music.settingchanged"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    move-object v0, p0

    move-object v1, p1

    move v5, p3

    .line 117
    invoke-virtual/range {v0 .. v5}, Lcom/samsung/musicplus/widget/appwidget/CommonServiceAppWidgetProvider;->updateSettingStatus(Lcom/samsung/musicplus/service/PlayerService;Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[II)V

    goto :goto_0

    .line 118
    :cond_4
    const-string v0, "com.samsung.musicplus.action.PREPARE_COMPLETED"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 119
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move v5, p3

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/musicplus/widget/appwidget/CommonServiceAppWidgetProvider;->updateProgress(Lcom/samsung/musicplus/service/PlayerService;Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[IIZ)V

    goto :goto_0

    .line 120
    :cond_5
    const-string v0, "com.samsung.musicplus.action.META_EDITED"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    move-object v0, p0

    move-object v1, p1

    move v5, p3

    .line 121
    invoke-virtual/range {v0 .. v5}, Lcom/samsung/musicplus/widget/appwidget/CommonServiceAppWidgetProvider;->updateTitles(Lcom/samsung/musicplus/service/PlayerService;Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[II)V

    goto :goto_0

    .line 122
    :cond_6
    const-string v0, "com.samsung.musicplus.action.QUEUE_CHANGED"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 123
    invoke-virtual {p0, v3, v4}, Lcom/samsung/musicplus/widget/appwidget/CommonServiceAppWidgetProvider;->updateList(Landroid/appwidget/AppWidgetManager;[I)V

    goto :goto_0
.end method

.method public setAlbumArt(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "album"    # Landroid/graphics/Bitmap;

    .prologue
    .line 135
    return-void
.end method

.method protected updateList(Landroid/appwidget/AppWidgetManager;[I)V
    .locals 1
    .param p1, "mgr"    # Landroid/appwidget/AppWidgetManager;
    .param p2, "appWidgetIds"    # [I

    .prologue
    .line 197
    const v0, 0x7f0d01c1

    invoke-virtual {p1, p2, v0}, Landroid/appwidget/AppWidgetManager;->notifyAppWidgetViewDataChanged([II)V

    .line 198
    return-void
.end method

.method protected updateMetaInfo(Lcom/samsung/musicplus/service/PlayerService;Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[II)V
    .locals 0
    .param p1, "s"    # Lcom/samsung/musicplus/service/PlayerService;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "mgr"    # Landroid/appwidget/AppWidgetManager;
    .param p4, "appWidgetIds"    # [I
    .param p5, "layout"    # I

    .prologue
    .line 211
    return-void
.end method

.method protected updatePlayStatus(Lcom/samsung/musicplus/service/PlayerService;Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[II)V
    .locals 0
    .param p1, "s"    # Lcom/samsung/musicplus/service/PlayerService;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "mgr"    # Landroid/appwidget/AppWidgetManager;
    .param p4, "appWidgetIds"    # [I
    .param p5, "layout"    # I

    .prologue
    .line 148
    return-void
.end method

.method protected updateProgress(Lcom/samsung/musicplus/service/PlayerService;Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[IIZ)V
    .locals 0
    .param p1, "s"    # Lcom/samsung/musicplus/service/PlayerService;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "mgr"    # Landroid/appwidget/AppWidgetManager;
    .param p4, "appWidgetIds"    # [I
    .param p5, "layout"    # I
    .param p6, "value"    # Z

    .prologue
    .line 188
    return-void
.end method

.method protected updateSettingStatus(Lcom/samsung/musicplus/service/PlayerService;Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[II)V
    .locals 0
    .param p1, "s"    # Lcom/samsung/musicplus/service/PlayerService;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "mgr"    # Landroid/appwidget/AppWidgetManager;
    .param p4, "appWidgetIds"    # [I
    .param p5, "layout"    # I

    .prologue
    .line 161
    return-void
.end method

.method protected updateTitles(Lcom/samsung/musicplus/service/PlayerService;Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[II)V
    .locals 0
    .param p1, "s"    # Lcom/samsung/musicplus/service/PlayerService;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "mgr"    # Landroid/appwidget/AppWidgetManager;
    .param p4, "appWidgetIds"    # [I
    .param p5, "layout"    # I

    .prologue
    .line 174
    return-void
.end method
