.class public Lcom/samsung/musicplus/widget/progress/MusicProgressBarScrubbing;
.super Ljava/lang/Object;
.source "MusicProgressBarScrubbing.java"


# static fields
.field private static final CLASSNAME:Ljava/lang/String;

.field public static final HALF_SPEED_SCRUBBING:I = 0x2

.field public static final HI_SPEED_SCRUBBING:I = 0x1

.field public static final QUARTER_SPEED_SCRUBBING:I = 0x4


# instance fields
.field private mComparedProgress:I

.field private mContext:Landroid/content/Context;

.field private mParentView:Landroid/view/View;

.field private mPopupWindow:Landroid/widget/PopupWindow;

.field private mSavedProgress:I

.field private mSavedScrubbingSpeed:I

.field private mScrubbingSpeed:I

.field private mStartedProgress:I

.field private mTextView:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/samsung/musicplus/widget/progress/MusicProgressBarScrubbing;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/widget/progress/MusicProgressBarScrubbing;->CLASSNAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/View;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "parentView"    # Landroid/view/View;

    .prologue
    const/4 v6, 0x0

    const/4 v5, -0x2

    const/4 v3, -0x1

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    const/4 v2, 0x1

    iput v2, p0, Lcom/samsung/musicplus/widget/progress/MusicProgressBarScrubbing;->mScrubbingSpeed:I

    .line 34
    iput v3, p0, Lcom/samsung/musicplus/widget/progress/MusicProgressBarScrubbing;->mStartedProgress:I

    .line 36
    iput v3, p0, Lcom/samsung/musicplus/widget/progress/MusicProgressBarScrubbing;->mComparedProgress:I

    .line 38
    iput v3, p0, Lcom/samsung/musicplus/widget/progress/MusicProgressBarScrubbing;->mSavedProgress:I

    .line 40
    iput v3, p0, Lcom/samsung/musicplus/widget/progress/MusicProgressBarScrubbing;->mSavedScrubbingSpeed:I

    .line 49
    iput-object p1, p0, Lcom/samsung/musicplus/widget/progress/MusicProgressBarScrubbing;->mContext:Landroid/content/Context;

    .line 50
    iput-object p2, p0, Lcom/samsung/musicplus/widget/progress/MusicProgressBarScrubbing;->mParentView:Landroid/view/View;

    .line 52
    sget-object v2, Lcom/samsung/musicplus/widget/progress/MusicProgressBarScrubbing;->CLASSNAME:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "MusicProgressBarScrubbing() mContext:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/musicplus/widget/progress/MusicProgressBarScrubbing;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " mParentView:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/musicplus/widget/progress/MusicProgressBarScrubbing;->mParentView:Landroid/view/View;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    iget-object v2, p0, Lcom/samsung/musicplus/widget/progress/MusicProgressBarScrubbing;->mContext:Landroid/content/Context;

    const-string v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 57
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v2, 0x7f04000d

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 59
    .local v1, "vContentView":Landroid/view/View;
    new-instance v2, Landroid/widget/PopupWindow;

    invoke-direct {v2, v1, v5, v5, v6}, Landroid/widget/PopupWindow;-><init>(Landroid/view/View;IIZ)V

    iput-object v2, p0, Lcom/samsung/musicplus/widget/progress/MusicProgressBarScrubbing;->mPopupWindow:Landroid/widget/PopupWindow;

    .line 61
    const v2, 0x7f0d0064

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/samsung/musicplus/widget/progress/MusicProgressBarScrubbing;->mTextView:Landroid/widget/TextView;

    .line 63
    iget-object v2, p0, Lcom/samsung/musicplus/widget/progress/MusicProgressBarScrubbing;->mTextView:Landroid/widget/TextView;

    const v3, 0x7f10014a

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 65
    return-void
.end method


# virtual methods
.method public cancelScrubbingSpeedInfoPopup()V
    .locals 3

    .prologue
    .line 78
    sget-object v0, Lcom/samsung/musicplus/widget/progress/MusicProgressBarScrubbing;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "cancelScrubbingSpeedInfoPopup() mPopupWindow:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/widget/progress/MusicProgressBarScrubbing;->mPopupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 80
    iget-object v0, p0, Lcom/samsung/musicplus/widget/progress/MusicProgressBarScrubbing;->mPopupWindow:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/widget/progress/MusicProgressBarScrubbing;->mPopupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/samsung/musicplus/widget/progress/MusicProgressBarScrubbing;->mPopupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 83
    :cond_0
    return-void
.end method

.method public reprocessProgress(I)I
    .locals 3
    .param p1, "progress"    # I

    .prologue
    .line 124
    iget v0, p0, Lcom/samsung/musicplus/widget/progress/MusicProgressBarScrubbing;->mSavedScrubbingSpeed:I

    iget v1, p0, Lcom/samsung/musicplus/widget/progress/MusicProgressBarScrubbing;->mScrubbingSpeed:I

    if-eq v0, v1, :cond_0

    .line 125
    sget-object v0, Lcom/samsung/musicplus/widget/progress/MusicProgressBarScrubbing;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "reprocessProgress() mScrubbingSpeed was changed to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/musicplus/widget/progress/MusicProgressBarScrubbing;->mScrubbingSpeed:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 128
    iget v0, p0, Lcom/samsung/musicplus/widget/progress/MusicProgressBarScrubbing;->mSavedProgress:I

    iput v0, p0, Lcom/samsung/musicplus/widget/progress/MusicProgressBarScrubbing;->mStartedProgress:I

    .line 129
    iput p1, p0, Lcom/samsung/musicplus/widget/progress/MusicProgressBarScrubbing;->mComparedProgress:I

    .line 131
    :cond_0
    iget v0, p0, Lcom/samsung/musicplus/widget/progress/MusicProgressBarScrubbing;->mScrubbingSpeed:I

    iput v0, p0, Lcom/samsung/musicplus/widget/progress/MusicProgressBarScrubbing;->mSavedScrubbingSpeed:I

    .line 132
    iget v0, p0, Lcom/samsung/musicplus/widget/progress/MusicProgressBarScrubbing;->mScrubbingSpeed:I

    packed-switch v0, :pswitch_data_0

    .line 143
    :goto_0
    :pswitch_0
    iput p1, p0, Lcom/samsung/musicplus/widget/progress/MusicProgressBarScrubbing;->mSavedProgress:I

    .line 144
    return p1

    .line 134
    :pswitch_1
    iget v0, p0, Lcom/samsung/musicplus/widget/progress/MusicProgressBarScrubbing;->mStartedProgress:I

    iget v1, p0, Lcom/samsung/musicplus/widget/progress/MusicProgressBarScrubbing;->mComparedProgress:I

    sub-int v1, p1, v1

    div-int/lit8 v1, v1, 0x2

    add-int p1, v0, v1

    .line 135
    goto :goto_0

    .line 137
    :pswitch_2
    iget v0, p0, Lcom/samsung/musicplus/widget/progress/MusicProgressBarScrubbing;->mStartedProgress:I

    iget v1, p0, Lcom/samsung/musicplus/widget/progress/MusicProgressBarScrubbing;->mComparedProgress:I

    sub-int v1, p1, v1

    div-int/lit8 v1, v1, 0x4

    add-int p1, v0, v1

    .line 139
    goto :goto_0

    .line 132
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public setScrubbingSpeed(Landroid/view/View;Landroid/view/MotionEvent;)I
    .locals 9
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const v8, 0x7f10014a

    .line 86
    iget-object v6, p0, Lcom/samsung/musicplus/widget/progress/MusicProgressBarScrubbing;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 87
    .local v4, "res":Landroid/content/res/Resources;
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 88
    .local v0, "action":I
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    float-to-int v3, v6

    .line 89
    .local v3, "nTouchPointY":I
    const v6, 0x7f0c004b

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 90
    .local v1, "heightOfHalf":I
    const v6, 0x7f0c004e

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 91
    .local v2, "heightOfhigh":I
    const/4 v5, 0x1

    .line 92
    .local v5, "returnValue":I
    iget-object v6, p0, Lcom/samsung/musicplus/widget/progress/MusicProgressBarScrubbing;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setText(I)V

    .line 93
    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v6

    sub-int v6, v3, v6

    if-lez v6, :cond_1

    .line 119
    :cond_0
    :goto_0
    iput v5, p0, Lcom/samsung/musicplus/widget/progress/MusicProgressBarScrubbing;->mScrubbingSpeed:I

    .line 120
    return v5

    .line 103
    :cond_1
    if-gez v3, :cond_0

    .line 105
    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v6

    add-int v7, v1, v2

    if-le v6, v7, :cond_2

    .line 106
    iget-object v6, p0, Lcom/samsung/musicplus/widget/progress/MusicProgressBarScrubbing;->mTextView:Landroid/widget/TextView;

    const v7, 0x7f10014b

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(I)V

    .line 107
    const/4 v5, 0x4

    goto :goto_0

    .line 108
    :cond_2
    invoke-static {v3}, Ljava/lang/Math;->abs(I)I

    move-result v6

    if-le v6, v2, :cond_3

    .line 109
    iget-object v6, p0, Lcom/samsung/musicplus/widget/progress/MusicProgressBarScrubbing;->mTextView:Landroid/widget/TextView;

    const v7, 0x7f100149

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(I)V

    .line 110
    const/4 v5, 0x2

    goto :goto_0

    .line 112
    :cond_3
    iget-object v6, p0, Lcom/samsung/musicplus/widget/progress/MusicProgressBarScrubbing;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v6, v8}, Landroid/widget/TextView;->setText(I)V

    .line 113
    const/4 v5, 0x1

    .line 114
    const/4 v6, 0x2

    if-ne v0, v6, :cond_0

    iget-object v6, p0, Lcom/samsung/musicplus/widget/progress/MusicProgressBarScrubbing;->mPopupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v6}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v6

    if-nez v6, :cond_0

    .line 115
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/progress/MusicProgressBarScrubbing;->showScrubbingSpeedInfoPopup()V

    goto :goto_0
.end method

.method public showScrubbingSpeedInfoPopup()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 69
    sget-object v0, Lcom/samsung/musicplus/widget/progress/MusicProgressBarScrubbing;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "showScrubbingSpeedInfoPopup() mPopupWindow:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/widget/progress/MusicProgressBarScrubbing;->mPopupWindow:Landroid/widget/PopupWindow;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    iget-object v0, p0, Lcom/samsung/musicplus/widget/progress/MusicProgressBarScrubbing;->mParentView:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/widget/progress/MusicProgressBarScrubbing;->mPopupWindow:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Lcom/samsung/musicplus/widget/progress/MusicProgressBarScrubbing;->mPopupWindow:Landroid/widget/PopupWindow;

    iget-object v1, p0, Lcom/samsung/musicplus/widget/progress/MusicProgressBarScrubbing;->mParentView:Landroid/view/View;

    const/16 v2, 0x11

    invoke-virtual {v0, v1, v2, v3, v3}, Landroid/widget/PopupWindow;->showAtLocation(Landroid/view/View;III)V

    .line 74
    :cond_0
    return-void
.end method
