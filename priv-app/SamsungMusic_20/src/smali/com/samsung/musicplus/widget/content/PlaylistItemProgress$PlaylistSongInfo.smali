.class public Lcom/samsung/musicplus/widget/content/PlaylistItemProgress$PlaylistSongInfo;
.super Ljava/lang/Object;
.source "PlaylistItemProgress.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/widget/content/PlaylistItemProgress;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PlaylistSongInfo"
.end annotation


# instance fields
.field public mAlbumId:J

.field public mCount:I

.field public mTime:J


# direct methods
.method public constructor <init>(IJJ)V
    .locals 0
    .param p1, "count"    # I
    .param p2, "time"    # J
    .param p4, "albumId"    # J

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput p1, p0, Lcom/samsung/musicplus/widget/content/PlaylistItemProgress$PlaylistSongInfo;->mCount:I

    .line 23
    iput-wide p2, p0, Lcom/samsung/musicplus/widget/content/PlaylistItemProgress$PlaylistSongInfo;->mTime:J

    .line 24
    iput-wide p4, p0, Lcom/samsung/musicplus/widget/content/PlaylistItemProgress$PlaylistSongInfo;->mAlbumId:J

    .line 25
    return-void
.end method
