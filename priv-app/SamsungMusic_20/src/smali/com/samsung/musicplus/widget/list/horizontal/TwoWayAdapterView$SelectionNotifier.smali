.class Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView$SelectionNotifier;
.super Landroid/os/Handler;
.source "TwoWayAdapterView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SelectionNotifier"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;


# direct methods
.method private constructor <init>(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;)V
    .locals 0

    .prologue
    .line 862
    .local p0, "this":Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView$SelectionNotifier;, "Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView<TT;>.SelectionNotifier;"
    iput-object p1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView$SelectionNotifier;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;
    .param p2, "x1"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView$1;

    .prologue
    .line 862
    .local p0, "this":Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView$SelectionNotifier;, "Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView<TT;>.SelectionNotifier;"
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView$SelectionNotifier;-><init>(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 1

    .prologue
    .line 864
    .local p0, "this":Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView$SelectionNotifier;, "Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView<TT;>.SelectionNotifier;"
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView$SelectionNotifier;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;

    iget-boolean v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mDataChanged:Z

    if-eqz v0, :cond_0

    .line 868
    invoke-virtual {p0, p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView$SelectionNotifier;->post(Ljava/lang/Runnable;)Z

    .line 872
    :goto_0
    return-void

    .line 870
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView$SelectionNotifier;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;

    # invokes: Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->fireOnSelected()V
    invoke-static {v0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->access$000(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;)V

    goto :goto_0
.end method
