.class public abstract Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;
.super Landroid/view/ViewGroup;
.source "TwoWayAdapterView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView$1;,
        Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView$SelectionNotifier;,
        Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView$AdapterContextMenuInfo;,
        Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView$OnItemSelectedListener;,
        Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView$OnItemLongClickListener;,
        Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView$OnItemClickListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T::",
        "Landroid/widget/Adapter;",
        ">",
        "Landroid/view/ViewGroup;"
    }
.end annotation


# static fields
.field public static final INVALID_POSITION:I = -0x1

.field public static final INVALID_ROW_ID:J = -0x8000000000000000L

.field public static final ITEM_VIEW_TYPE_HEADER_OR_FOOTER:I = -0x2

.field public static final ITEM_VIEW_TYPE_IGNORE:I = -0x1

.field static final SYNC_FIRST_POSITION:I = 0x1

.field static final SYNC_MAX_DURATION_MILLIS:I = 0x64

.field static final SYNC_SELECTED_POSITION:I


# instance fields
.field mBlockLayoutRequests:Z

.field protected mContext:Landroid/content/Context;

.field mDataChanged:Z

.field private mDesiredFocusableInTouchModeState:Z

.field private mDesiredFocusableState:Z

.field private mEmptyView:Landroid/view/View;

.field mFirstPosition:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation
.end field

.field mInLayout:Z

.field protected mIsVertical:Z

.field mItemCount:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation
.end field

.field private mLayoutHeight:I

.field private mLayoutWidth:I

.field mNeedSync:Z

.field mNextSelectedPosition:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation
.end field

.field mNextSelectedRowId:J

.field mOldItemCount:I

.field mOldSelectedPosition:I

.field mOldSelectedRowId:J

.field mOnItemClickListener:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView$OnItemClickListener;

.field mOnItemLongClickListener:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView$OnItemLongClickListener;

.field mOnItemSelectedListener:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView$OnItemSelectedListener;

.field mSelectedPosition:I
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation
.end field

.field mSelectedRowId:J

.field private mSelectionNotifier:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView$SelectionNotifier;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView",
            "<TT;>.SelectionNotifier;"
        }
    .end annotation
.end field

.field mSpecificTop:I

.field mSyncMode:I

.field mSyncPosition:I

.field mSyncRowId:J

.field mSyncSize:J


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .local p0, "this":Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;, "Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView<TT;>;"
    const/4 v4, -0x1

    const-wide/high16 v2, -0x8000000000000000L

    const/4 v1, 0x0

    .line 250
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 74
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mContext:Landroid/content/Context;

    .line 79
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mIsVertical:Z

    .line 84
    iput v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mFirstPosition:I

    .line 101
    iput-wide v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mSyncRowId:J

    .line 111
    iput-boolean v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mNeedSync:Z

    .line 148
    iput-boolean v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mInLayout:Z

    .line 174
    iput v4, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mNextSelectedPosition:I

    .line 180
    iput-wide v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mNextSelectedRowId:J

    .line 185
    iput v4, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mSelectedPosition:I

    .line 191
    iput-wide v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mSelectedRowId:J

    .line 223
    iput v4, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mOldSelectedPosition:I

    .line 228
    iput-wide v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mOldSelectedRowId:J

    .line 247
    iput-boolean v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mBlockLayoutRequests:Z

    .line 251
    iput-object p1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mContext:Landroid/content/Context;

    .line 252
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .local p0, "this":Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;, "Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView<TT;>;"
    const/4 v4, -0x1

    const-wide/high16 v2, -0x8000000000000000L

    const/4 v1, 0x0

    .line 255
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 74
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mContext:Landroid/content/Context;

    .line 79
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mIsVertical:Z

    .line 84
    iput v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mFirstPosition:I

    .line 101
    iput-wide v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mSyncRowId:J

    .line 111
    iput-boolean v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mNeedSync:Z

    .line 148
    iput-boolean v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mInLayout:Z

    .line 174
    iput v4, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mNextSelectedPosition:I

    .line 180
    iput-wide v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mNextSelectedRowId:J

    .line 185
    iput v4, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mSelectedPosition:I

    .line 191
    iput-wide v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mSelectedRowId:J

    .line 223
    iput v4, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mOldSelectedPosition:I

    .line 228
    iput-wide v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mOldSelectedRowId:J

    .line 247
    iput-boolean v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mBlockLayoutRequests:Z

    .line 256
    iput-object p1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mContext:Landroid/content/Context;

    .line 257
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .local p0, "this":Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;, "Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView<TT;>;"
    const/4 v4, -0x1

    const-wide/high16 v2, -0x8000000000000000L

    const/4 v1, 0x0

    .line 260
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 74
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mContext:Landroid/content/Context;

    .line 79
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mIsVertical:Z

    .line 84
    iput v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mFirstPosition:I

    .line 101
    iput-wide v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mSyncRowId:J

    .line 111
    iput-boolean v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mNeedSync:Z

    .line 148
    iput-boolean v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mInLayout:Z

    .line 174
    iput v4, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mNextSelectedPosition:I

    .line 180
    iput-wide v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mNextSelectedRowId:J

    .line 185
    iput v4, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mSelectedPosition:I

    .line 191
    iput-wide v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mSelectedRowId:J

    .line 223
    iput v4, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mOldSelectedPosition:I

    .line 228
    iput-wide v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mOldSelectedRowId:J

    .line 247
    iput-boolean v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mBlockLayoutRequests:Z

    .line 261
    iput-object p1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mContext:Landroid/content/Context;

    .line 262
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->fireOnSelected()V

    return-void
.end method

.method private fireOnSelected()V
    .locals 6

    .prologue
    .line 899
    .local p0, "this":Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;, "Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView<TT;>;"
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mOnItemSelectedListener:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView$OnItemSelectedListener;

    if-nez v0, :cond_0

    .line 910
    :goto_0
    return-void

    .line 902
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->getSelectedItemPosition()I

    move-result v3

    .line 903
    .local v3, "selection":I
    if-ltz v3, :cond_1

    .line 904
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->getSelectedView()Landroid/view/View;

    move-result-object v2

    .line 905
    .local v2, "v":Landroid/view/View;
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mOnItemSelectedListener:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView$OnItemSelectedListener;

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v1

    invoke-interface {v1, v3}, Landroid/widget/Adapter;->getItemId(I)J

    move-result-wide v4

    move-object v1, p0

    invoke-interface/range {v0 .. v5}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView$OnItemSelectedListener;->onItemSelected(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;Landroid/view/View;IJ)V

    goto :goto_0

    .line 908
    .end local v2    # "v":Landroid/view/View;
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mOnItemSelectedListener:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView$OnItemSelectedListener;

    invoke-interface {v0, p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView$OnItemSelectedListener;->onNothingSelected(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;)V

    goto :goto_0
.end method

.method private updateEmptyStatus(Z)V
    .locals 6
    .param p1, "empty"    # Z
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "WrongCall"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;, "Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView<TT;>;"
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 742
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->isInFilterMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 743
    const/4 p1, 0x0

    .line 746
    :cond_0
    if-eqz p1, :cond_3

    .line 747
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mEmptyView:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 748
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mEmptyView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 749
    invoke-virtual {p0, v2}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->setVisibility(I)V

    .line 758
    :goto_0
    iget-boolean v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mDataChanged:Z

    if-eqz v0, :cond_1

    .line 759
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->getLeft()I

    move-result v2

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->getTop()I

    move-result v3

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->getRight()I

    move-result v4

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->getBottom()I

    move-result v5

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->onLayout(ZIIII)V

    .line 765
    :cond_1
    :goto_1
    return-void

    .line 752
    :cond_2
    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->setVisibility(I)V

    goto :goto_0

    .line 762
    :cond_3
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mEmptyView:Landroid/view/View;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mEmptyView:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 763
    :cond_4
    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->setVisibility(I)V

    goto :goto_1
.end method


# virtual methods
.method public addView(Landroid/view/View;)V
    .locals 2
    .param p1, "child"    # Landroid/view/View;

    .prologue
    .line 468
    .local p0, "this":Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;, "Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView<TT;>;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "addView(View) is not supported in AdapterView"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public addView(Landroid/view/View;I)V
    .locals 2
    .param p1, "child"    # Landroid/view/View;
    .param p2, "index"    # I

    .prologue
    .line 481
    .local p0, "this":Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;, "Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView<TT;>;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "addView(View, int) is not supported in AdapterView"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .locals 2
    .param p1, "child"    # Landroid/view/View;
    .param p2, "index"    # I
    .param p3, "params"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    .line 509
    .local p0, "this":Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;, "Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView<TT;>;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "addView(View, int, LayoutParams) is not supported in AdapterView"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 2
    .param p1, "child"    # Landroid/view/View;
    .param p2, "params"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    .line 494
    .local p0, "this":Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;, "Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView<TT;>;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "addView(View, LayoutParams) is not supported in AdapterView"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected canAnimate()Z
    .locals 1

    .prologue
    .line 944
    .local p0, "this":Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;, "Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView<TT;>;"
    invoke-super {p0}, Landroid/view/ViewGroup;->canAnimate()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mItemCount:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method checkFocus()V
    .locals 6

    .prologue
    .local p0, "this":Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;, "Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView<TT;>;"
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 722
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    .line 723
    .local v0, "adapter":Landroid/widget/Adapter;, "TT;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/widget/Adapter;->getCount()I

    move-result v3

    if-nez v3, :cond_5

    :cond_0
    move v1, v4

    .line 724
    .local v1, "empty":Z
    :goto_0
    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->isInFilterMode()Z

    move-result v3

    if-eqz v3, :cond_6

    :cond_1
    move v2, v4

    .line 728
    .local v2, "focusable":Z
    :goto_1
    if-eqz v2, :cond_7

    iget-boolean v3, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mDesiredFocusableInTouchModeState:Z

    if-eqz v3, :cond_7

    move v3, v4

    :goto_2
    invoke-super {p0, v3}, Landroid/view/ViewGroup;->setFocusableInTouchMode(Z)V

    .line 729
    if-eqz v2, :cond_8

    iget-boolean v3, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mDesiredFocusableState:Z

    if-eqz v3, :cond_8

    move v3, v4

    :goto_3
    invoke-super {p0, v3}, Landroid/view/ViewGroup;->setFocusable(Z)V

    .line 730
    iget-object v3, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mEmptyView:Landroid/view/View;

    if-eqz v3, :cond_4

    .line 731
    if-eqz v0, :cond_2

    invoke-interface {v0}, Landroid/widget/Adapter;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_3

    :cond_2
    move v5, v4

    :cond_3
    invoke-direct {p0, v5}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->updateEmptyStatus(Z)V

    .line 733
    :cond_4
    return-void

    .end local v1    # "empty":Z
    .end local v2    # "focusable":Z
    :cond_5
    move v1, v5

    .line 723
    goto :goto_0

    .restart local v1    # "empty":Z
    :cond_6
    move v2, v5

    .line 724
    goto :goto_1

    .restart local v2    # "focusable":Z
    :cond_7
    move v3, v5

    .line 728
    goto :goto_2

    :cond_8
    move v3, v5

    .line 729
    goto :goto_3
.end method

.method checkSelectionChanged()V
    .locals 4

    .prologue
    .line 1011
    .local p0, "this":Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;, "Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView<TT;>;"
    iget v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mSelectedPosition:I

    iget v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mOldSelectedPosition:I

    if-ne v0, v1, :cond_0

    iget-wide v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mSelectedRowId:J

    iget-wide v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mOldSelectedRowId:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 1012
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->selectionChanged()V

    .line 1013
    iget v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mSelectedPosition:I

    iput v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mOldSelectedPosition:I

    .line 1014
    iget-wide v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mSelectedRowId:J

    iput-wide v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mOldSelectedRowId:J

    .line 1016
    :cond_1
    return-void
.end method

.method protected dispatchRestoreInstanceState(Landroid/util/SparseArray;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 802
    .local p0, "this":Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;, "Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView<TT;>;"
    .local p1, "container":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Landroid/os/Parcelable;>;"
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->dispatchThawSelfOnly(Landroid/util/SparseArray;)V

    .line 803
    return-void
.end method

.method protected dispatchSaveInstanceState(Landroid/util/SparseArray;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Landroid/os/Parcelable;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 794
    .local p0, "this":Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;, "Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView<TT;>;"
    .local p1, "container":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Landroid/os/Parcelable;>;"
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->dispatchFreezeSelfOnly(Landroid/util/SparseArray;)V

    .line 795
    return-void
.end method

.method findSyncPosition()I
    .locals 20

    .prologue
    .line 1027
    .local p0, "this":Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;, "Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView<TT;>;"
    move-object/from16 v0, p0

    iget v3, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mItemCount:I

    .line 1029
    .local v3, "count":I
    if-nez v3, :cond_1

    .line 1030
    const/4 v13, -0x1

    .line 1102
    :cond_0
    :goto_0
    return v13

    .line 1033
    :cond_1
    move-object/from16 v0, p0

    iget-wide v10, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mSyncRowId:J

    .line 1034
    .local v10, "idToMatch":J
    move-object/from16 v0, p0

    iget v13, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mSyncPosition:I

    .line 1037
    .local v13, "seed":I
    const-wide/high16 v16, -0x8000000000000000L

    cmp-long v16, v10, v16

    if-nez v16, :cond_2

    .line 1038
    const/4 v13, -0x1

    goto :goto_0

    .line 1042
    :cond_2
    const/16 v16, 0x0

    move/from16 v0, v16

    invoke-static {v0, v13}, Ljava/lang/Math;->max(II)I

    move-result v13

    .line 1043
    add-int/lit8 v16, v3, -0x1

    move/from16 v0, v16

    invoke-static {v0, v13}, Ljava/lang/Math;->min(II)I

    move-result v13

    .line 1045
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v16

    const-wide/16 v18, 0x64

    add-long v4, v16, v18

    .line 1050
    .local v4, "endTime":J
    move v6, v13

    .line 1053
    .local v6, "first":I
    move v9, v13

    .line 1056
    .local v9, "last":I
    const/4 v12, 0x0

    .line 1066
    .local v12, "next":Z
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v2

    .line 1067
    .local v2, "adapter":Landroid/widget/Adapter;, "TT;"
    if-nez v2, :cond_5

    .line 1068
    const/4 v13, -0x1

    goto :goto_0

    .line 1086
    .local v7, "hitFirst":Z
    .local v8, "hitLast":Z
    .local v14, "rowId":J
    :cond_3
    if-nez v7, :cond_4

    if-eqz v12, :cond_9

    if-nez v8, :cond_9

    .line 1088
    :cond_4
    add-int/lit8 v9, v9, 0x1

    .line 1089
    move v13, v9

    .line 1091
    const/4 v12, 0x0

    .line 1071
    .end local v7    # "hitFirst":Z
    .end local v8    # "hitLast":Z
    .end local v14    # "rowId":J
    :cond_5
    :goto_1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v16

    cmp-long v16, v16, v4

    if-gtz v16, :cond_6

    .line 1072
    invoke-interface {v2, v13}, Landroid/widget/Adapter;->getItemId(I)J

    move-result-wide v14

    .line 1073
    .restart local v14    # "rowId":J
    cmp-long v16, v14, v10

    if-eqz v16, :cond_0

    .line 1078
    add-int/lit8 v16, v3, -0x1

    move/from16 v0, v16

    if-ne v9, v0, :cond_7

    const/4 v8, 0x1

    .line 1079
    .restart local v8    # "hitLast":Z
    :goto_2
    if-nez v6, :cond_8

    const/4 v7, 0x1

    .line 1081
    .restart local v7    # "hitFirst":Z
    :goto_3
    if-eqz v8, :cond_3

    if-eqz v7, :cond_3

    .line 1102
    .end local v7    # "hitFirst":Z
    .end local v8    # "hitLast":Z
    .end local v14    # "rowId":J
    :cond_6
    const/4 v13, -0x1

    goto :goto_0

    .line 1078
    .restart local v14    # "rowId":J
    :cond_7
    const/4 v8, 0x0

    goto :goto_2

    .line 1079
    .restart local v8    # "hitLast":Z
    :cond_8
    const/4 v7, 0x0

    goto :goto_3

    .line 1092
    .restart local v7    # "hitFirst":Z
    :cond_9
    if-nez v8, :cond_a

    if-nez v12, :cond_5

    if-nez v7, :cond_5

    .line 1094
    :cond_a
    add-int/lit8 v6, v6, -0x1

    .line 1095
    move v13, v6

    .line 1097
    const/4 v12, 0x1

    goto :goto_1
.end method

.method public abstract getAdapter()Landroid/widget/Adapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation
.end method

.method public getCount()I
    .locals 1
    .annotation runtime Landroid/view/ViewDebug$CapturedViewProperty;
    .end annotation

    .prologue
    .line 599
    .local p0, "this":Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;, "Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView<TT;>;"
    iget v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mItemCount:I

    return v0
.end method

.method public getEmptyView()Landroid/view/View;
    .locals 1

    .prologue
    .line 682
    .local p0, "this":Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;, "Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView<TT;>;"
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mEmptyView:Landroid/view/View;

    return-object v0
.end method

.method public getFirstVisiblePosition()I
    .locals 1

    .prologue
    .line 642
    .local p0, "this":Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;, "Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView<TT;>;"
    iget v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mFirstPosition:I

    return v0
.end method

.method public getItemAtPosition(I)Ljava/lang/Object;
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 774
    .local p0, "this":Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;, "Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView<TT;>;"
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    .line 775
    .local v0, "adapter":Landroid/widget/Adapter;, "TT;"
    if-eqz v0, :cond_0

    if-gez p1, :cond_1

    :cond_0
    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_1
    invoke-interface {v0, p1}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    goto :goto_0
.end method

.method public getItemIdAtPosition(I)J
    .locals 4
    .param p1, "position"    # I

    .prologue
    .line 779
    .local p0, "this":Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;, "Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView<TT;>;"
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    .line 780
    .local v0, "adapter":Landroid/widget/Adapter;, "TT;"
    if-eqz v0, :cond_0

    if-gez p1, :cond_1

    :cond_0
    const-wide/high16 v2, -0x8000000000000000L

    :goto_0
    return-wide v2

    :cond_1
    invoke-interface {v0, p1}, Landroid/widget/Adapter;->getItemId(I)J

    move-result-wide v2

    goto :goto_0
.end method

.method public getLastVisiblePosition()I
    .locals 2

    .prologue
    .line 652
    .local p0, "this":Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;, "Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView<TT;>;"
    iget v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mFirstPosition:I

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->getChildCount()I

    move-result v1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public final getOnItemClickListener()Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView$OnItemClickListener;
    .locals 1

    .prologue
    .line 302
    .local p0, "this":Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;, "Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView<TT;>;"
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mOnItemClickListener:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView$OnItemClickListener;

    return-object v0
.end method

.method public final getOnItemLongClickListener()Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView$OnItemLongClickListener;
    .locals 1

    .prologue
    .line 365
    .local p0, "this":Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;, "Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView<TT;>;"
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mOnItemLongClickListener:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView$OnItemLongClickListener;

    return-object v0
.end method

.method public final getOnItemSelectedListener()Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView$OnItemSelectedListener;
    .locals 1

    .prologue
    .line 409
    .local p0, "this":Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;, "Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView<TT;>;"
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mOnItemSelectedListener:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView$OnItemSelectedListener;

    return-object v0
.end method

.method public getPositionForView(Landroid/view/View;)I
    .locals 7
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .local p0, "this":Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;, "Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView<TT;>;"
    const/4 v5, -0x1

    .line 612
    move-object v3, p1

    .line 615
    .local v3, "listItem":Landroid/view/View;
    :goto_0
    :try_start_0
    invoke-virtual {v3}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    check-cast v4, Landroid/view/View;

    .local v4, "v":Landroid/view/View;
    invoke-virtual {v4, p0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v6

    if-nez v6, :cond_1

    .line 616
    move-object v3, v4

    goto :goto_0

    .line 618
    .end local v4    # "v":Landroid/view/View;
    :catch_0
    move-exception v1

    .line 632
    :cond_0
    :goto_1
    return v5

    .line 624
    .restart local v4    # "v":Landroid/view/View;
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->getChildCount()I

    move-result v0

    .line 625
    .local v0, "childCount":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_2
    if-ge v2, v0, :cond_0

    .line 626
    invoke-virtual {p0, v2}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 627
    iget v5, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mFirstPosition:I

    add-int/2addr v5, v2

    goto :goto_1

    .line 625
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_2
.end method

.method public getSelectedItem()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 583
    .local p0, "this":Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;, "Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView<TT;>;"
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    .line 584
    .local v0, "adapter":Landroid/widget/Adapter;, "TT;"
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->getSelectedItemPosition()I

    move-result v1

    .line 585
    .local v1, "selection":I
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/widget/Adapter;->getCount()I

    move-result v2

    if-lez v2, :cond_0

    if-ltz v1, :cond_0

    .line 586
    invoke-interface {v0, v1}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    .line 588
    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public getSelectedItemId()J
    .locals 2
    .annotation runtime Landroid/view/ViewDebug$CapturedViewProperty;
    .end annotation

    .prologue
    .line 569
    .local p0, "this":Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;, "Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView<TT;>;"
    iget-wide v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mNextSelectedRowId:J

    return-wide v0
.end method

.method public getSelectedItemPosition()I
    .locals 1
    .annotation runtime Landroid/view/ViewDebug$CapturedViewProperty;
    .end annotation

    .prologue
    .line 560
    .local p0, "this":Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;, "Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView<TT;>;"
    iget v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mNextSelectedPosition:I

    return v0
.end method

.method public abstract getSelectedView()Landroid/view/View;
.end method

.method handleDataChanged()V
    .locals 10

    .prologue
    .local p0, "this":Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;, "Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView<TT;>;"
    const-wide/high16 v8, -0x8000000000000000L

    const/4 v7, 0x1

    const/4 v6, -0x1

    const/4 v5, 0x0

    .line 948
    iget v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mItemCount:I

    .line 949
    .local v0, "count":I
    const/4 v1, 0x0

    .line 951
    .local v1, "found":Z
    if-lez v0, :cond_4

    .line 956
    iget-boolean v4, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mNeedSync:Z

    if-eqz v4, :cond_0

    .line 959
    iput-boolean v5, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mNeedSync:Z

    .line 963
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->findSyncPosition()I

    move-result v2

    .line 964
    .local v2, "newPos":I
    if-ltz v2, :cond_0

    .line 966
    invoke-virtual {p0, v2, v7}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->lookForSelectablePosition(IZ)I

    move-result v3

    .line 967
    .local v3, "selectablePos":I
    if-ne v3, v2, :cond_0

    .line 969
    invoke-virtual {p0, v2}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->setNextSelectedPositionInt(I)V

    .line 970
    const/4 v1, 0x1

    .line 974
    .end local v2    # "newPos":I
    .end local v3    # "selectablePos":I
    :cond_0
    if-nez v1, :cond_4

    .line 976
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->getSelectedItemPosition()I

    move-result v2

    .line 979
    .restart local v2    # "newPos":I
    if-lt v2, v0, :cond_1

    .line 980
    add-int/lit8 v2, v0, -0x1

    .line 982
    :cond_1
    if-gez v2, :cond_2

    .line 983
    const/4 v2, 0x0

    .line 987
    :cond_2
    invoke-virtual {p0, v2, v7}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->lookForSelectablePosition(IZ)I

    move-result v3

    .line 988
    .restart local v3    # "selectablePos":I
    if-gez v3, :cond_3

    .line 990
    invoke-virtual {p0, v2, v5}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->lookForSelectablePosition(IZ)I

    move-result v3

    .line 992
    :cond_3
    if-ltz v3, :cond_4

    .line 993
    invoke-virtual {p0, v3}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->setNextSelectedPositionInt(I)V

    .line 994
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->checkSelectionChanged()V

    .line 995
    const/4 v1, 0x1

    .line 999
    .end local v2    # "newPos":I
    .end local v3    # "selectablePos":I
    :cond_4
    if-nez v1, :cond_5

    .line 1001
    iput v6, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mSelectedPosition:I

    .line 1002
    iput-wide v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mSelectedRowId:J

    .line 1003
    iput v6, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mNextSelectedPosition:I

    .line 1004
    iput-wide v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mNextSelectedRowId:J

    .line 1005
    iput-boolean v5, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mNeedSync:Z

    .line 1006
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->checkSelectionChanged()V

    .line 1008
    :cond_5
    return-void
.end method

.method isInFilterMode()Z
    .locals 1

    .prologue
    .line 692
    .local p0, "this":Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;, "Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView<TT;>;"
    const/4 v0, 0x0

    return v0
.end method

.method protected isVertical()Z
    .locals 1

    .prologue
    .line 1224
    .local p0, "this":Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;, "Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView<TT;>;"
    iget-boolean v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mIsVertical:Z

    return v0
.end method

.method lookForSelectablePosition(IZ)I
    .locals 0
    .param p1, "position"    # I
    .param p2, "lookDown"    # Z

    .prologue
    .line 1114
    .local p0, "this":Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;, "Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView<TT;>;"
    return p1
.end method

.method public offsetChildrenLeftAndRight(I)V
    .locals 3
    .param p1, "offset"    # I

    .prologue
    .line 1211
    .local p0, "this":Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;, "Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView<TT;>;"
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->getChildCount()I

    move-result v0

    .line 1213
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 1214
    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 1215
    .local v2, "v":Landroid/view/View;
    invoke-virtual {v2, p1}, Landroid/view/View;->offsetLeftAndRight(I)V

    .line 1213
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1217
    .end local v2    # "v":Landroid/view/View;
    :cond_0
    return-void
.end method

.method public offsetChildrenTopAndBottom(I)V
    .locals 3
    .param p1, "offset"    # I

    .prologue
    .line 1196
    .local p0, "this":Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;, "Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView<TT;>;"
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->getChildCount()I

    move-result v0

    .line 1198
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 1199
    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 1200
    .local v2, "v":Landroid/view/View;
    invoke-virtual {v2, p1}, Landroid/view/View;->offsetTopAndBottom(I)V

    .line 1198
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1202
    .end local v2    # "v":Landroid/view/View;
    :cond_0
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 1
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 549
    .local p0, "this":Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;, "Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView<TT;>;"
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->getHeight()I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mLayoutHeight:I

    .line 550
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->getWidth()I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mLayoutWidth:I

    .line 551
    return-void
.end method

.method public performItemClick(Landroid/view/View;IJ)Z
    .locals 7
    .param p1, "view"    # Landroid/view/View;
    .param p2, "position"    # I
    .param p3, "id"    # J

    .prologue
    .local p0, "this":Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;, "Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView<TT;>;"
    const/4 v0, 0x0

    .line 315
    iget-object v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mOnItemClickListener:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView$OnItemClickListener;

    if-eqz v1, :cond_0

    .line 316
    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->playSoundEffect(I)V

    .line 317
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mOnItemClickListener:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView$OnItemClickListener;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move-wide v4, p3

    invoke-interface/range {v0 .. v5}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView$OnItemClickListener;->onItemClick(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;Landroid/view/View;IJ)V

    .line 318
    const/4 v0, 0x1

    .line 321
    :cond_0
    return v0
.end method

.method rememberSyncState()V
    .locals 6

    .prologue
    .local p0, "this":Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;, "Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView<TT;>;"
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1147
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->getChildCount()I

    move-result v2

    if-lez v2, :cond_1

    .line 1148
    iput-boolean v5, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mNeedSync:Z

    .line 1149
    iget-boolean v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mIsVertical:Z

    if-eqz v2, :cond_2

    .line 1150
    iget v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mLayoutHeight:I

    int-to-long v2, v2

    iput-wide v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mSyncSize:J

    .line 1154
    :goto_0
    iget v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mSelectedPosition:I

    if-ltz v2, :cond_4

    .line 1156
    iget v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mSelectedPosition:I

    iget v3, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mFirstPosition:I

    sub-int/2addr v2, v3

    invoke-virtual {p0, v2}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1157
    .local v1, "v":Landroid/view/View;
    iget-wide v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mNextSelectedRowId:J

    iput-wide v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mSyncRowId:J

    .line 1158
    iget v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mNextSelectedPosition:I

    iput v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mSyncPosition:I

    .line 1159
    if-eqz v1, :cond_0

    .line 1160
    iget-boolean v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mIsVertical:Z

    if-eqz v2, :cond_3

    .line 1161
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v2

    iput v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mSpecificTop:I

    .line 1166
    :cond_0
    :goto_1
    iput v4, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mSyncMode:I

    .line 1187
    .end local v1    # "v":Landroid/view/View;
    :cond_1
    :goto_2
    return-void

    .line 1152
    :cond_2
    iget v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mLayoutWidth:I

    int-to-long v2, v2

    iput-wide v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mSyncSize:J

    goto :goto_0

    .line 1163
    .restart local v1    # "v":Landroid/view/View;
    :cond_3
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v2

    iput v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mSpecificTop:I

    goto :goto_1

    .line 1169
    .end local v1    # "v":Landroid/view/View;
    :cond_4
    invoke-virtual {p0, v4}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1170
    .restart local v1    # "v":Landroid/view/View;
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    .line 1171
    .local v0, "adapter":Landroid/widget/Adapter;, "TT;"
    iget v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mFirstPosition:I

    if-ltz v2, :cond_6

    iget v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mFirstPosition:I

    invoke-interface {v0}, Landroid/widget/Adapter;->getCount()I

    move-result v3

    if-ge v2, v3, :cond_6

    .line 1172
    iget v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mFirstPosition:I

    invoke-interface {v0, v2}, Landroid/widget/Adapter;->getItemId(I)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mSyncRowId:J

    .line 1176
    :goto_3
    iget v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mFirstPosition:I

    iput v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mSyncPosition:I

    .line 1177
    if-eqz v1, :cond_5

    .line 1178
    iget-boolean v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mIsVertical:Z

    if-eqz v2, :cond_7

    .line 1179
    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v2

    iput v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mSpecificTop:I

    .line 1184
    :cond_5
    :goto_4
    iput v5, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mSyncMode:I

    goto :goto_2

    .line 1174
    :cond_6
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mSyncRowId:J

    goto :goto_3

    .line 1181
    :cond_7
    invoke-virtual {v1}, Landroid/view/View;->getLeft()I

    move-result v2

    iput v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mSpecificTop:I

    goto :goto_4
.end method

.method public removeAllViews()V
    .locals 2

    .prologue
    .line 544
    .local p0, "this":Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;, "Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView<TT;>;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "removeAllViews() is not supported in AdapterView"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public removeView(Landroid/view/View;)V
    .locals 2
    .param p1, "child"    # Landroid/view/View;

    .prologue
    .line 522
    .local p0, "this":Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;, "Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView<TT;>;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "removeView(View) is not supported in AdapterView"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public removeViewAt(I)V
    .locals 2
    .param p1, "index"    # I

    .prologue
    .line 534
    .local p0, "this":Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;, "Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView<TT;>;"
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "removeViewAt(int) is not supported in AdapterView"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method selectionChanged()V
    .locals 2

    .prologue
    .line 876
    .local p0, "this":Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;, "Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView<TT;>;"
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mOnItemSelectedListener:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView$OnItemSelectedListener;

    if-eqz v0, :cond_2

    .line 877
    iget-boolean v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mInLayout:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mBlockLayoutRequests:Z

    if-eqz v0, :cond_3

    .line 882
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mSelectionNotifier:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView$SelectionNotifier;

    if-nez v0, :cond_1

    .line 883
    new-instance v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView$SelectionNotifier;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView$SelectionNotifier;-><init>(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView$1;)V

    iput-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mSelectionNotifier:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView$SelectionNotifier;

    .line 885
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mSelectionNotifier:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView$SelectionNotifier;

    iget-object v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mSelectionNotifier:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView$SelectionNotifier;

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView$SelectionNotifier;->post(Ljava/lang/Runnable;)Z

    .line 896
    :cond_2
    :goto_0
    return-void

    .line 887
    :cond_3
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->fireOnSelected()V

    goto :goto_0
.end method

.method public abstract setAdapter(Landroid/widget/Adapter;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation
.end method

.method public setEmptyView(Landroid/view/View;)V
    .locals 3
    .param p1, "emptyView"    # Landroid/view/View;

    .prologue
    .line 667
    .local p0, "this":Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;, "Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView<TT;>;"
    iput-object p1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mEmptyView:Landroid/view/View;

    .line 669
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    .line 670
    .local v0, "adapter":Landroid/widget/Adapter;, "TT;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/widget/Adapter;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    const/4 v1, 0x1

    .line 671
    .local v1, "empty":Z
    :goto_0
    invoke-direct {p0, v1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->updateEmptyStatus(Z)V

    .line 672
    return-void

    .line 670
    .end local v1    # "empty":Z
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setFocusable(Z)V
    .locals 5
    .param p1, "focusable"    # Z

    .prologue
    .local p0, "this":Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;, "Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView<TT;>;"
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 697
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    .line 698
    .local v0, "adapter":Landroid/widget/Adapter;, "TT;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/widget/Adapter;->getCount()I

    move-result v4

    if-nez v4, :cond_3

    :cond_0
    move v1, v3

    .line 700
    .local v1, "empty":Z
    :goto_0
    iput-boolean p1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mDesiredFocusableState:Z

    .line 701
    if-nez p1, :cond_1

    .line 702
    iput-boolean v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mDesiredFocusableInTouchModeState:Z

    .line 705
    :cond_1
    if-eqz p1, :cond_4

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->isInFilterMode()Z

    move-result v4

    if-eqz v4, :cond_4

    :cond_2
    :goto_1
    invoke-super {p0, v3}, Landroid/view/ViewGroup;->setFocusable(Z)V

    .line 706
    return-void

    .end local v1    # "empty":Z
    :cond_3
    move v1, v2

    .line 698
    goto :goto_0

    .restart local v1    # "empty":Z
    :cond_4
    move v3, v2

    .line 705
    goto :goto_1
.end method

.method public setFocusableInTouchMode(Z)V
    .locals 5
    .param p1, "focusable"    # Z

    .prologue
    .local p0, "this":Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;, "Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView<TT;>;"
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 710
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    .line 711
    .local v0, "adapter":Landroid/widget/Adapter;, "TT;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/widget/Adapter;->getCount()I

    move-result v4

    if-nez v4, :cond_3

    :cond_0
    move v1, v3

    .line 713
    .local v1, "empty":Z
    :goto_0
    iput-boolean p1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mDesiredFocusableInTouchModeState:Z

    .line 714
    if-eqz p1, :cond_1

    .line 715
    iput-boolean v3, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mDesiredFocusableState:Z

    .line 718
    :cond_1
    if-eqz p1, :cond_4

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->isInFilterMode()Z

    move-result v4

    if-eqz v4, :cond_4

    :cond_2
    :goto_1
    invoke-super {p0, v3}, Landroid/view/ViewGroup;->setFocusableInTouchMode(Z)V

    .line 719
    return-void

    .end local v1    # "empty":Z
    :cond_3
    move v1, v2

    .line 711
    goto :goto_0

    .restart local v1    # "empty":Z
    :cond_4
    move v3, v2

    .line 718
    goto :goto_1
.end method

.method protected setIsVertical(Z)V
    .locals 0
    .param p1, "vertical"    # Z

    .prologue
    .line 1220
    .local p0, "this":Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;, "Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView<TT;>;"
    iput-boolean p1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mIsVertical:Z

    .line 1221
    return-void
.end method

.method setNextSelectedPositionInt(I)V
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 1132
    .local p0, "this":Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;, "Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView<TT;>;"
    iput p1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mNextSelectedPosition:I

    .line 1133
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->getItemIdAtPosition(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mNextSelectedRowId:J

    .line 1135
    iget-boolean v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mNeedSync:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mSyncMode:I

    if-nez v0, :cond_0

    if-ltz p1, :cond_0

    .line 1136
    iput p1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mSyncPosition:I

    .line 1137
    iget-wide v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mNextSelectedRowId:J

    iput-wide v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mSyncRowId:J

    .line 1139
    :cond_0
    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 2
    .param p1, "l"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 785
    .local p0, "this":Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;, "Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView<TT;>;"
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Don\'t call setOnClickListener for an AdapterView. You probably want setOnItemClickListener instead"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setOnItemClickListener(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView$OnItemClickListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView$OnItemClickListener;

    .prologue
    .line 294
    .local p0, "this":Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;, "Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView<TT;>;"
    iput-object p1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mOnItemClickListener:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView$OnItemClickListener;

    .line 295
    return-void
.end method

.method public setOnItemLongClickListener(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView$OnItemLongClickListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView$OnItemLongClickListener;

    .prologue
    .line 354
    .local p0, "this":Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;, "Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView<TT;>;"
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->isLongClickable()Z

    move-result v0

    if-nez v0, :cond_0

    .line 355
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->setLongClickable(Z)V

    .line 357
    :cond_0
    iput-object p1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mOnItemLongClickListener:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView$OnItemLongClickListener;

    .line 358
    return-void
.end method

.method public setOnItemSelectedListener(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView$OnItemSelectedListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView$OnItemSelectedListener;

    .prologue
    .line 405
    .local p0, "this":Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;, "Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView<TT;>;"
    iput-object p1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mOnItemSelectedListener:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView$OnItemSelectedListener;

    .line 406
    return-void
.end method

.method setSelectedPositionInt(I)V
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 1122
    .local p0, "this":Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;, "Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView<TT;>;"
    iput p1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mSelectedPosition:I

    .line 1123
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->getItemIdAtPosition(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->mSelectedRowId:J

    .line 1124
    return-void
.end method

.method public abstract setSelection(I)V
.end method
