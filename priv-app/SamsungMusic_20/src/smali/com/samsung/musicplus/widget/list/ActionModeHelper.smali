.class public Lcom/samsung/musicplus/widget/list/ActionModeHelper;
.super Ljava/lang/Object;
.source "ActionModeHelper.java"


# instance fields
.field private mActionModeEnabled:Z

.field private mChoiceMode:I

.field private mOnDisabledItemClickListener:Lcom/samsung/musicplus/widget/list/OnDisabledItemClickListener;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    iput-boolean v0, p0, Lcom/samsung/musicplus/widget/list/ActionModeHelper;->mActionModeEnabled:Z

    .line 10
    iput v0, p0, Lcom/samsung/musicplus/widget/list/ActionModeHelper;->mChoiceMode:I

    .line 15
    return-void
.end method


# virtual methods
.method public convertChoiceMode(I)I
    .locals 2
    .param p1, "choiceMode"    # I

    .prologue
    .line 40
    iput p1, p0, Lcom/samsung/musicplus/widget/list/ActionModeHelper;->mChoiceMode:I

    .line 41
    iget v0, p0, Lcom/samsung/musicplus/widget/list/ActionModeHelper;->mChoiceMode:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 42
    const/4 v0, 0x2

    .line 44
    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/samsung/musicplus/widget/list/ActionModeHelper;->mChoiceMode:I

    goto :goto_0
.end method

.method public interceptItemClick(Landroid/view/View;IJ)Z
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "position"    # I
    .param p3, "id"    # J

    .prologue
    .line 65
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Landroid/view/View;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 66
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/ActionModeHelper;->mOnDisabledItemClickListener:Lcom/samsung/musicplus/widget/list/OnDisabledItemClickListener;

    if-eqz v0, :cond_0

    .line 67
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/ActionModeHelper;->mOnDisabledItemClickListener:Lcom/samsung/musicplus/widget/list/OnDisabledItemClickListener;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/samsung/musicplus/widget/list/OnDisabledItemClickListener;->onDisabledItemClick(Landroid/view/View;IJ)V

    .line 69
    :cond_0
    const/4 v0, 0x1

    .line 71
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isCustomChoiceMode()Z
    .locals 2

    .prologue
    .line 61
    iget v0, p0, Lcom/samsung/musicplus/widget/list/ActionModeHelper;->mChoiceMode:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/samsung/musicplus/widget/list/ActionModeHelper;->mActionModeEnabled:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isMultipleChoiceModeEnabled()Z
    .locals 2

    .prologue
    .line 49
    iget v0, p0, Lcom/samsung/musicplus/widget/list/ActionModeHelper;->mChoiceMode:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/samsung/musicplus/widget/list/ActionModeHelper;->mActionModeEnabled:Z

    if-eqz v0, :cond_1

    .line 50
    :cond_0
    const/4 v0, 0x1

    .line 52
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setActionModeEnabled(Z)V
    .locals 0
    .param p1, "enabled"    # Z

    .prologue
    .line 31
    iput-boolean p1, p0, Lcom/samsung/musicplus/widget/list/ActionModeHelper;->mActionModeEnabled:Z

    .line 32
    return-void
.end method

.method public setOnDisabledItemClickListener(Lcom/samsung/musicplus/widget/list/OnDisabledItemClickListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/musicplus/widget/list/OnDisabledItemClickListener;

    .prologue
    .line 27
    iput-object p1, p0, Lcom/samsung/musicplus/widget/list/ActionModeHelper;->mOnDisabledItemClickListener:Lcom/samsung/musicplus/widget/list/OnDisabledItemClickListener;

    .line 28
    return-void
.end method
