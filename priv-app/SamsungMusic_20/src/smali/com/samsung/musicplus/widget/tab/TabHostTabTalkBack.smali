.class public Lcom/samsung/musicplus/widget/tab/TabHostTabTalkBack;
.super Lcom/samsung/musicplus/widget/tab/AbsTabTalkBack;
.source "TabHostTabTalkBack.java"


# instance fields
.field private final mTabHost:Landroid/widget/TabHost;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/widget/TabHost;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "tabHost"    # Landroid/widget/TabHost;

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/widget/tab/AbsTabTalkBack;-><init>(Landroid/content/Context;)V

    .line 18
    iput-object p2, p0, Lcom/samsung/musicplus/widget/tab/TabHostTabTalkBack;->mTabHost:Landroid/widget/TabHost;

    .line 19
    new-instance v0, Lcom/samsung/musicplus/widget/tab/TabHostTabTalkBack$1;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/widget/tab/TabHostTabTalkBack$1;-><init>(Lcom/samsung/musicplus/widget/tab/TabHostTabTalkBack;)V

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/widget/tab/TabHostTabTalkBack;->setAccessibilityEventRunnable(Ljava/lang/Runnable;)V

    .line 27
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/widget/tab/TabHostTabTalkBack;)Landroid/widget/TabHost;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/tab/TabHostTabTalkBack;

    .prologue
    .line 12
    iget-object v0, p0, Lcom/samsung/musicplus/widget/tab/TabHostTabTalkBack;->mTabHost:Landroid/widget/TabHost;

    return-object v0
.end method


# virtual methods
.method public setTabViewTts(IIZ)V
    .locals 3
    .param p1, "position"    # I
    .param p2, "tabCount"    # I
    .param p3, "selected"    # Z

    .prologue
    .line 50
    iget-object v2, p0, Lcom/samsung/musicplus/widget/tab/TabHostTabTalkBack;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v2}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/widget/TabWidget;->getChildTabViewAt(I)Landroid/view/View;

    move-result-object v0

    .line 51
    .local v0, "tab":Landroid/view/View;
    if-nez v0, :cond_0

    .line 61
    :goto_0
    return-void

    .line 55
    :cond_0
    const v2, 0x7f0d01bc

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 56
    .local v1, "title":Landroid/widget/TextView;
    if-eqz p3, :cond_1

    .line 57
    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {p0, v2, p1, p2}, Lcom/samsung/musicplus/widget/tab/TabHostTabTalkBack;->getSelectedTabTts(Ljava/lang/CharSequence;II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 59
    :cond_1
    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {p0, v2, p1, p2}, Lcom/samsung/musicplus/widget/tab/TabHostTabTalkBack;->getUnselectedTabTts(Ljava/lang/CharSequence;II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public setTabsTts(I)V
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 31
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/tab/AbsTabTalkBack;->setTabsTts(I)V

    .line 32
    iget-object v2, p0, Lcom/samsung/musicplus/widget/tab/TabHostTabTalkBack;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v2}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/TabWidget;->getTabCount()I

    move-result v1

    .line 33
    .local v1, "tabCount":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v1, :cond_0

    .line 34
    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/samsung/musicplus/widget/tab/TabHostTabTalkBack;->setTabViewTts(IIZ)V

    .line 33
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 36
    :cond_0
    const/4 v2, -0x1

    if-le p1, v2, :cond_1

    .line 37
    const/4 v2, 0x1

    invoke-virtual {p0, p1, v1, v2}, Lcom/samsung/musicplus/widget/tab/TabHostTabTalkBack;->setTabViewTts(IIZ)V

    .line 39
    :cond_1
    return-void
.end method
