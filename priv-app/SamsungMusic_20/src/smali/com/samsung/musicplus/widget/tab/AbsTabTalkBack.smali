.class public Lcom/samsung/musicplus/widget/tab/AbsTabTalkBack;
.super Ljava/lang/Object;
.source "AbsTabTalkBack.java"


# static fields
.field public static final INVALID_POSITION:I = -0x1

.field protected static final TAG:Ljava/lang/String; = "MusicTabTalkBack"


# instance fields
.field private mAccessibilityEventRunnable:Ljava/lang/Runnable;

.field private final mContext:Landroid/content/Context;

.field private final mHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/widget/tab/AbsTabTalkBack;->mHandler:Landroid/os/Handler;

    .line 23
    iput-object p1, p0, Lcom/samsung/musicplus/widget/tab/AbsTabTalkBack;->mContext:Landroid/content/Context;

    .line 24
    return-void
.end method


# virtual methods
.method public getSelectedTabTts(Landroid/app/ActionBar$Tab;)Ljava/lang/String;
    .locals 1
    .param p1, "tab"    # Landroid/app/ActionBar$Tab;

    .prologue
    .line 120
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getSelectedTabTts(Ljava/lang/CharSequence;II)Ljava/lang/String;
    .locals 4
    .param p1, "tabText"    # Ljava/lang/CharSequence;
    .param p2, "position"    # I
    .param p3, "tabCount"    # I

    .prologue
    .line 46
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 47
    .local v0, "sb":Ljava/lang/StringBuilder;
    iget-object v1, p0, Lcom/samsung/musicplus/widget/tab/AbsTabTalkBack;->mContext:Landroid/content/Context;

    const v2, 0x7f1001a6

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 48
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 49
    invoke-virtual {p0, p2, p3}, Lcom/samsung/musicplus/widget/tab/AbsTabTalkBack;->getTabOrderText(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 50
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method protected getTabOrderText(II)Ljava/lang/String;
    .locals 4
    .param p1, "position"    # I
    .param p2, "tabCount"    # I

    .prologue
    .line 77
    iget-object v0, p0, Lcom/samsung/musicplus/widget/tab/AbsTabTalkBack;->mContext:Landroid/content/Context;

    const v1, 0x7f1001a7

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    add-int/lit8 v3, p1, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUnselectedTabTts(Landroid/app/ActionBar$Tab;)Ljava/lang/String;
    .locals 1
    .param p1, "tab"    # Landroid/app/ActionBar$Tab;

    .prologue
    .line 109
    const/4 v0, 0x0

    return-object v0
.end method

.method protected getUnselectedTabTts(Ljava/lang/CharSequence;II)Ljava/lang/String;
    .locals 4
    .param p1, "tabText"    # Ljava/lang/CharSequence;
    .param p2, "position"    # I
    .param p3, "tabCount"    # I

    .prologue
    .line 62
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 63
    .local v0, "sb":Ljava/lang/StringBuilder;
    iget-object v1, p0, Lcom/samsung/musicplus/widget/tab/AbsTabTalkBack;->mContext:Landroid/content/Context;

    const v2, 0x7f1001a5

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 64
    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 65
    invoke-virtual {p0, p2, p3}, Lcom/samsung/musicplus/widget/tab/AbsTabTalkBack;->getTabOrderText(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 66
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method protected setAccessibilityEventRunnable(Ljava/lang/Runnable;)V
    .locals 0
    .param p1, "runnalbe"    # Ljava/lang/Runnable;

    .prologue
    .line 94
    iput-object p1, p0, Lcom/samsung/musicplus/widget/tab/AbsTabTalkBack;->mAccessibilityEventRunnable:Ljava/lang/Runnable;

    .line 95
    return-void
.end method

.method public setTabViewTts(IIZ)V
    .locals 0
    .param p1, "position"    # I
    .param p2, "tabCount"    # I
    .param p3, "selected"    # Z

    .prologue
    .line 137
    return-void
.end method

.method public setTabsTts(I)V
    .locals 0
    .param p1, "position"    # I

    .prologue
    .line 35
    return-void
.end method

.method public speechCurrentTab()V
    .locals 4

    .prologue
    .line 84
    iget-object v0, p0, Lcom/samsung/musicplus/widget/tab/AbsTabTalkBack;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/musicplus/widget/tab/AbsTabTalkBack;->mAccessibilityEventRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 85
    return-void
.end method
