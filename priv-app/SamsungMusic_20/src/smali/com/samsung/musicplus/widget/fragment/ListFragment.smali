.class public abstract Lcom/samsung/musicplus/widget/fragment/ListFragment;
.super Lcom/samsung/musicplus/MusicBaseFragment;
.source "ListFragment.java"

# interfaces
.implements Lcom/samsung/musicplus/widget/fragment/OnMusicFragmentInterface;


# static fields
.field public static final MUSIC_GRID_VIEW:I = 0x1

.field public static final MUSIC_LIST_VIEW:I


# instance fields
.field private mAdapter:Landroid/widget/ListAdapter;

.field private mEmptyContentViewStub:Landroid/view/View;

.field private mEmptyViewStub:Landroid/view/View;

.field private final mHandler:Landroid/os/Handler;

.field private final mInvalidator:Ljava/lang/Runnable;

.field private mListContainer:Landroid/view/View;

.field private mListShown:Z

.field private mListView:Landroid/widget/AbsListView;

.field private mNoItemBackgroundView:Landroid/widget/ImageView;

.field private mNoItemImageView:Landroid/widget/ImageView;

.field private mNoItemTextView:Landroid/widget/TextView;

.field private final mOnClickListener:Landroid/widget/AdapterView$OnItemClickListener;

.field private final mOnLongClickListener:Landroid/widget/AdapterView$OnItemLongClickListener;

.field private mProgressContainer:Landroid/view/View;

.field private final mRequestFocus:Ljava/lang/Runnable;

.field private mViewType:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 94
    invoke-direct {p0}, Lcom/samsung/musicplus/MusicBaseFragment;-><init>()V

    .line 50
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mHandler:Landroid/os/Handler;

    .line 52
    new-instance v0, Lcom/samsung/musicplus/widget/fragment/ListFragment$1;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/widget/fragment/ListFragment$1;-><init>(Lcom/samsung/musicplus/widget/fragment/ListFragment;)V

    iput-object v0, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mRequestFocus:Ljava/lang/Runnable;

    .line 59
    new-instance v0, Lcom/samsung/musicplus/widget/fragment/ListFragment$2;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/widget/fragment/ListFragment$2;-><init>(Lcom/samsung/musicplus/widget/fragment/ListFragment;)V

    iput-object v0, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mOnClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    .line 66
    new-instance v0, Lcom/samsung/musicplus/widget/fragment/ListFragment$3;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/widget/fragment/ListFragment$3;-><init>(Lcom/samsung/musicplus/widget/fragment/ListFragment;)V

    iput-object v0, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mOnLongClickListener:Landroid/widget/AdapterView$OnItemLongClickListener;

    .line 436
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mViewType:I

    .line 524
    new-instance v0, Lcom/samsung/musicplus/widget/fragment/ListFragment$4;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/widget/fragment/ListFragment$4;-><init>(Lcom/samsung/musicplus/widget/fragment/ListFragment;)V

    iput-object v0, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mInvalidator:Ljava/lang/Runnable;

    .line 95
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/widget/fragment/ListFragment;)Landroid/widget/AbsListView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/fragment/ListFragment;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mListView:Landroid/widget/AbsListView;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/musicplus/widget/fragment/ListFragment;)Landroid/widget/ListAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/fragment/ListFragment;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mAdapter:Landroid/widget/ListAdapter;

    return-object v0
.end method

.method private ensureGrid()V
    .locals 5

    .prologue
    .line 406
    iget-object v3, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mListView:Landroid/widget/AbsListView;

    instance-of v3, v3, Lcom/samsung/musicplus/widget/list/MusicGridView;

    if-eqz v3, :cond_0

    .line 430
    :goto_0
    return-void

    .line 410
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/ListFragment;->getView()Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    .line 411
    .local v2, "root":Landroid/view/ViewGroup;
    if-nez v2, :cond_1

    .line 412
    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "Content view not yet created"

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 415
    :cond_1
    const v3, 0x7f0d008d

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 416
    .local v1, "rawGridViewStub":Landroid/view/View;
    instance-of v3, v1, Landroid/view/ViewStub;

    if-eqz v3, :cond_2

    .line 417
    check-cast v1, Landroid/view/ViewStub;

    .end local v1    # "rawGridViewStub":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 420
    :cond_2
    const v3, 0x7f0d00a7

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 421
    .local v0, "rawGridView":Landroid/view/View;
    instance-of v3, v0, Lcom/samsung/musicplus/widget/list/MusicGridView;

    if-nez v3, :cond_3

    .line 422
    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "Content has view with id attribute \'R.id.grid\' that is not a MusicGridView class"

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 426
    :cond_3
    check-cast v0, Landroid/widget/AbsListView;

    .end local v0    # "rawGridView":Landroid/view/View;
    iput-object v0, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mListView:Landroid/widget/AbsListView;

    .line 427
    iget-object v3, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mListView:Landroid/widget/AbsListView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/AbsListView;->setEnableDragBlock(Z)V

    .line 428
    iget-object v3, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mListView:Landroid/widget/AbsListView;

    iget-object v4, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mOnClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v3, v4}, Landroid/widget/AbsListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 429
    iget-object v3, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mListView:Landroid/widget/AbsListView;

    iget-object v4, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mOnLongClickListener:Landroid/widget/AdapterView$OnItemLongClickListener;

    invoke-virtual {v3, v4}, Landroid/widget/AbsListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    goto :goto_0
.end method

.method private ensureList()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 373
    iget-object v2, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mListView:Landroid/widget/AbsListView;

    instance-of v2, v2, Lcom/samsung/musicplus/widget/list/MusicListView;

    if-eqz v2, :cond_0

    .line 403
    :goto_0
    return-void

    .line 377
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/ListFragment;->getView()Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    .line 378
    .local v1, "root":Landroid/view/ViewGroup;
    if-nez v1, :cond_1

    .line 379
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Content view not yet created"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 382
    :cond_1
    const v2, 0x7f0d0089

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mProgressContainer:Landroid/view/View;

    .line 383
    const v2, 0x7f0d008b

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mListContainer:Landroid/view/View;

    .line 384
    const v2, 0x7f0d008c

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 385
    .local v0, "rawListView":Landroid/view/View;
    instance-of v2, v0, Lcom/samsung/musicplus/widget/list/MusicListView;

    if-nez v2, :cond_2

    .line 386
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Content has view with id attribute \'R.id.list\' that is not a MusicListView class"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 390
    :cond_2
    check-cast v0, Landroid/widget/AbsListView;

    .end local v0    # "rawListView":Landroid/view/View;
    iput-object v0, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mListView:Landroid/widget/AbsListView;

    .line 391
    iget-object v2, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mListView:Landroid/widget/AbsListView;

    invoke-virtual {v2, v4}, Landroid/widget/AbsListView;->setEnableDragBlock(Z)V

    .line 392
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mListShown:Z

    .line 393
    iget-object v2, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mListView:Landroid/widget/AbsListView;

    iget-object v3, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mOnClickListener:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/AbsListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 394
    iget-object v2, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mListView:Landroid/widget/AbsListView;

    iget-object v3, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mOnLongClickListener:Landroid/widget/AdapterView$OnItemLongClickListener;

    invoke-virtual {v2, v3}, Landroid/widget/AbsListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    .line 395
    iget-object v2, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mAdapter:Landroid/widget/ListAdapter;

    if-nez v2, :cond_3

    .line 398
    iget-object v2, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mProgressContainer:Landroid/view/View;

    if-eqz v2, :cond_3

    .line 399
    invoke-direct {p0, v4, v4}, Lcom/samsung/musicplus/widget/fragment/ListFragment;->setListShown(ZZ)V

    .line 402
    :cond_3
    iget-object v2, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mRequestFocus:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method private setEmptyViewIcon()V
    .locals 2

    .prologue
    .line 264
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/ListFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0d0048

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mNoItemImageView:Landroid/widget/ImageView;

    .line 265
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mNoItemImageView:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 266
    return-void
.end method

.method private setListShown(ZZ)V
    .locals 6
    .param p1, "shown"    # Z
    .param p2, "animate"    # Z

    .prologue
    const v5, 0x7f050005

    const v4, 0x7f050004

    const/16 v3, 0x8

    const/4 v2, 0x0

    .line 320
    iget-boolean v0, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mListShown:Z

    if-ne v0, p1, :cond_0

    .line 358
    :goto_0
    return-void

    .line 324
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mProgressContainer:Landroid/view/View;

    if-nez v0, :cond_1

    .line 325
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t be used with a custom content view"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 328
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mListContainer:Landroid/view/View;

    if-nez v0, :cond_2

    .line 329
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Can\'t be used with a null mListContainer"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 332
    :cond_2
    iput-boolean p1, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mListShown:Z

    .line 333
    if-eqz p1, :cond_4

    .line 334
    if-eqz p2, :cond_3

    .line 335
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mProgressContainer:Landroid/view/View;

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/ListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1, v5}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 337
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mListContainer:Landroid/view/View;

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/ListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1, v4}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 343
    :goto_1
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mProgressContainer:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 344
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mListContainer:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 340
    :cond_3
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mProgressContainer:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 341
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mListContainer:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    goto :goto_1

    .line 346
    :cond_4
    if-eqz p2, :cond_5

    .line 347
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mProgressContainer:Landroid/view/View;

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/ListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1, v4}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 349
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mListContainer:Landroid/view/View;

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/ListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1, v5}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 355
    :goto_2
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mProgressContainer:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 356
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mListContainer:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 352
    :cond_5
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mProgressContainer:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 353
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mListContainer:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    goto :goto_2
.end method


# virtual methods
.method public getAbsListView()Landroid/widget/AbsListView;
    .locals 1

    .prologue
    .line 477
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mListView:Landroid/widget/AbsListView;

    return-object v0
.end method

.method public getListAdapter()Landroid/widget/ListAdapter;
    .locals 1

    .prologue
    .line 364
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mAdapter:Landroid/widget/ListAdapter;

    return-object v0
.end method

.method public getListContainer()Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 481
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mListContainer:Landroid/view/View;

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method

.method public getListType()I
    .locals 1

    .prologue
    .line 545
    const/4 v0, 0x0

    return v0
.end method

.method public getListView()Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 492
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/fragment/ListFragment;->ensureList()V

    .line 493
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mListView:Landroid/widget/AbsListView;

    check-cast v0, Lcom/samsung/musicplus/widget/list/MusicListView;

    return-object v0
.end method

.method public getProgressContainer()Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 485
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mProgressContainer:Landroid/view/View;

    check-cast v0, Landroid/view/ViewGroup;

    return-object v0
.end method

.method public getSelectedItemId()J
    .locals 2

    .prologue
    .line 208
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/fragment/ListFragment;->ensureList()V

    .line 209
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mListView:Landroid/widget/AbsListView;

    invoke-virtual {v0}, Landroid/widget/AbsListView;->getSelectedItemId()J

    move-result-wide v0

    return-wide v0
.end method

.method public getSelectedItemPosition()I
    .locals 1

    .prologue
    .line 200
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/fragment/ListFragment;->ensureList()V

    .line 201
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mListView:Landroid/widget/AbsListView;

    invoke-virtual {v0}, Landroid/widget/AbsListView;->getSelectedItemPosition()I

    move-result v0

    return v0
.end method

.method public getViewCount()I
    .locals 1

    .prologue
    .line 468
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mListView:Landroid/widget/AbsListView;

    invoke-virtual {v0}, Landroid/widget/AbsListView;->getCount()I

    move-result v0

    return v0
.end method

.method public getViewType()I
    .locals 1

    .prologue
    .line 464
    iget v0, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mViewType:I

    return v0
.end method

.method protected invalidateAllViews()V
    .locals 4

    .prologue
    .line 510
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mAdapter:Landroid/widget/ListAdapter;

    instance-of v0, v0, Lcom/samsung/musicplus/widget/list/TrackListAdapter;

    if-eqz v0, :cond_0

    .line 511
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mListView:Landroid/widget/AbsListView;

    if-eqz v0, :cond_0

    .line 514
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mListView:Landroid/widget/AbsListView;

    iget-object v1, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mInvalidator:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 515
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mListView:Landroid/widget/AbsListView;

    iget-object v1, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mInvalidator:Ljava/lang/Runnable;

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/AbsListView;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 518
    :cond_0
    return-void
.end method

.method public isEmptyList()Z
    .locals 1

    .prologue
    .line 550
    const/4 v0, 0x0

    return v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 99
    const v0, 0x7f04001c

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDestroyView()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 125
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mRequestFocus:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 126
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mListView:Landroid/widget/AbsListView;

    if-eqz v0, :cond_0

    .line 127
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mListView:Landroid/widget/AbsListView;

    invoke-virtual {v0, v2}, Landroid/widget/AbsListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 129
    :cond_0
    iput-object v2, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mListView:Landroid/widget/AbsListView;

    .line 130
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mListShown:Z

    .line 131
    iput-object v2, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mListContainer:Landroid/view/View;

    iput-object v2, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mProgressContainer:Landroid/view/View;

    .line 132
    iput-object v2, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mEmptyViewStub:Landroid/view/View;

    .line 133
    iput-object v2, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mEmptyContentViewStub:Landroid/view/View;

    .line 134
    iput-object v2, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mNoItemImageView:Landroid/widget/ImageView;

    .line 135
    iput-object v2, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mNoItemTextView:Landroid/widget/TextView;

    .line 136
    iput-object v2, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mNoItemBackgroundView:Landroid/widget/ImageView;

    .line 137
    invoke-super {p0}, Lcom/samsung/musicplus/MusicBaseFragment;->onDestroyView()V

    .line 138
    return-void
.end method

.method public onFragmentWindowFocusChanged(Z)V
    .locals 0
    .param p1, "hasFocus"    # Z

    .prologue
    .line 541
    return-void
.end method

.method public onListItemClick(Landroid/widget/AbsListView;Landroid/view/View;IJ)V
    .locals 0
    .param p1, "l"    # Landroid/widget/AbsListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    .line 152
    return-void
.end method

.method public onListItemLongClick(Landroid/widget/AbsListView;Landroid/view/View;IJ)Z
    .locals 1
    .param p1, "l"    # Landroid/widget/AbsListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    .line 165
    const/4 v0, 0x0

    return v0
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mAdapter:Landroid/widget/ListAdapter;

    instance-of v0, v0, Lcom/samsung/musicplus/widget/list/TrackListAdapter;

    if-eqz v0, :cond_0

    .line 115
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mAdapter:Landroid/widget/ListAdapter;

    check-cast v0, Lcom/samsung/musicplus/widget/list/TrackListAdapter;

    invoke-virtual {v0}, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->stopUpdateView()V

    .line 117
    :cond_0
    invoke-super {p0}, Lcom/samsung/musicplus/MusicBaseFragment;->onStop()V

    .line 118
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 107
    invoke-super {p0, p1, p2}, Lcom/samsung/musicplus/MusicBaseFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 108
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/fragment/ListFragment;->ensureList()V

    .line 109
    return-void
.end method

.method public receivePlayerState(Ljava/lang/String;)V
    .locals 1
    .param p1, "action"    # Ljava/lang/String;

    .prologue
    .line 498
    const-string v0, "com.android.music.playstatechanged"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/ListFragment;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/musicplus/util/UiUtils;->isTalkBackEnabled(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 504
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/ListFragment;->invalidateAllViews()V

    .line 507
    :cond_1
    return-void
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .locals 4
    .param p1, "adapter"    # Landroid/widget/ListAdapter;

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 172
    iget-object v3, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mAdapter:Landroid/widget/ListAdapter;

    if-eqz v3, :cond_3

    move v0, v1

    .line 173
    .local v0, "hadAdapter":Z
    :goto_0
    iput-object p1, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mAdapter:Landroid/widget/ListAdapter;

    .line 174
    iget-object v3, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mListView:Landroid/widget/AbsListView;

    if-eqz v3, :cond_2

    .line 175
    iget-object v3, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mListView:Landroid/widget/AbsListView;

    invoke-virtual {v3, p1}, Landroid/widget/AbsListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 176
    iget-boolean v3, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mListShown:Z

    if-nez v3, :cond_1

    if-nez v0, :cond_1

    .line 179
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/ListFragment;->getView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v3

    if-eqz v3, :cond_0

    move v2, v1

    :cond_0
    invoke-direct {p0, v1, v2}, Lcom/samsung/musicplus/widget/fragment/ListFragment;->setListShown(ZZ)V

    .line 181
    :cond_1
    iget-object v1, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mListView:Landroid/widget/AbsListView;

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/widget/fragment/ListFragment;->registerForContextMenu(Landroid/view/View;)V

    .line 183
    :cond_2
    return-void

    .end local v0    # "hadAdapter":Z
    :cond_3
    move v0, v2

    .line 172
    goto :goto_0
.end method

.method protected setEmptyView(I)V
    .locals 3
    .param p1, "noItemTextId"    # I

    .prologue
    .line 218
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/ListFragment;->getView()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f0d0098

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mEmptyContentViewStub:Landroid/view/View;

    .line 219
    iget-object v1, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mEmptyContentViewStub:Landroid/view/View;

    instance-of v1, v1, Landroid/view/ViewStub;

    if-eqz v1, :cond_0

    .line 220
    iget-object v1, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mEmptyContentViewStub:Landroid/view/View;

    check-cast v1, Landroid/view/ViewStub;

    invoke-virtual {v1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 223
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/ListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    .line 224
    .local v0, "noItemText":Ljava/lang/CharSequence;
    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/widget/fragment/ListFragment;->setEmptyViewTextOnly(Ljava/lang/CharSequence;)V

    .line 225
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/ListFragment;->setEmptyViewNoBackground()V

    .line 226
    return-void
.end method

.method protected setEmptyView(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1, "noItemText"    # Ljava/lang/CharSequence;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 235
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/widget/fragment/ListFragment;->setEmptyViewText(Ljava/lang/CharSequence;)V

    .line 236
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/fragment/ListFragment;->setEmptyViewIcon()V

    .line 237
    sget-boolean v0, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_MASS_PROJECT:Z

    if-eqz v0, :cond_0

    .line 238
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/ListFragment;->setEmptyViewNoBackground()V

    .line 240
    :cond_0
    return-void
.end method

.method protected setEmptyViewNoBackground()V
    .locals 2

    .prologue
    .line 269
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/ListFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0d0100

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mNoItemBackgroundView:Landroid/widget/ImageView;

    .line 270
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mNoItemBackgroundView:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 271
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mNoItemBackgroundView:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 273
    :cond_0
    return-void
.end method

.method protected setEmptyViewText(Ljava/lang/CharSequence;)V
    .locals 3
    .param p1, "noItemText"    # Ljava/lang/CharSequence;

    .prologue
    .line 250
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/ListFragment;->getView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 251
    .local v0, "root":Landroid/view/ViewGroup;
    iget-object v1, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mEmptyViewStub:Landroid/view/View;

    if-nez v1, :cond_0

    .line 252
    const v1, 0x7f0d008e

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mEmptyViewStub:Landroid/view/View;

    .line 253
    iget-object v1, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mEmptyViewStub:Landroid/view/View;

    instance-of v1, v1, Landroid/view/ViewStub;

    if-eqz v1, :cond_0

    .line 254
    iget-object v1, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mEmptyViewStub:Landroid/view/View;

    check-cast v1, Landroid/view/ViewStub;

    invoke-virtual {v1}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 258
    :cond_0
    iget-object v1, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mListView:Landroid/widget/AbsListView;

    iget-object v2, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mEmptyViewStub:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/widget/AbsListView;->setEmptyView(Landroid/view/View;)V

    .line 259
    const v1, 0x7f0d0049

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mNoItemTextView:Landroid/widget/TextView;

    .line 260
    iget-object v1, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mNoItemTextView:Landroid/widget/TextView;

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 261
    return-void
.end method

.method protected setEmptyViewTextOnly(Ljava/lang/CharSequence;)V
    .locals 2
    .param p1, "noItemText"    # Ljava/lang/CharSequence;

    .prologue
    .line 276
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mNoItemImageView:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 277
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mNoItemImageView:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 279
    :cond_0
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/widget/fragment/ListFragment;->setEmptyViewText(Ljava/lang/CharSequence;)V

    .line 280
    return-void
.end method

.method public setListShown(Z)V
    .locals 1
    .param p1, "shown"    # Z

    .prologue
    .line 298
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/samsung/musicplus/widget/fragment/ListFragment;->setListShown(ZZ)V

    .line 299
    return-void
.end method

.method public setListShownNoAnimation(Z)V
    .locals 1
    .param p1, "shown"    # Z

    .prologue
    .line 306
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/samsung/musicplus/widget/fragment/ListFragment;->setListShown(ZZ)V

    .line 307
    return-void
.end method

.method public setSelection(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 192
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/fragment/ListFragment;->ensureList()V

    .line 193
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mListView:Landroid/widget/AbsListView;

    invoke-virtual {v0, p1}, Landroid/widget/AbsListView;->setSelection(I)V

    .line 194
    return-void
.end method

.method protected setViewType(I)V
    .locals 4
    .param p1, "viewType"    # I

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 444
    iput p1, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mViewType:I

    .line 445
    iget v0, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mViewType:I

    if-nez v0, :cond_1

    .line 446
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mListView:Landroid/widget/AbsListView;

    instance-of v0, v0, Lcom/samsung/musicplus/widget/list/MusicGridView;

    if-eqz v0, :cond_0

    .line 447
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mListView:Landroid/widget/AbsListView;

    invoke-virtual {v0, v2}, Landroid/widget/AbsListView;->setVisibility(I)V

    .line 448
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mListView:Landroid/widget/AbsListView;

    invoke-virtual {v0, v3}, Landroid/widget/AbsListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 450
    :cond_0
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/fragment/ListFragment;->ensureList()V

    .line 451
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mListView:Landroid/widget/AbsListView;

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setVisibility(I)V

    .line 460
    :goto_0
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mRequestFocus:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 461
    return-void

    .line 453
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mListView:Landroid/widget/AbsListView;

    instance-of v0, v0, Lcom/samsung/musicplus/widget/list/MusicListView;

    if-eqz v0, :cond_2

    .line 454
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mListView:Landroid/widget/AbsListView;

    invoke-virtual {v0, v2}, Landroid/widget/AbsListView;->setVisibility(I)V

    .line 455
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mListView:Landroid/widget/AbsListView;

    invoke-virtual {v0, v3}, Landroid/widget/AbsListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 457
    :cond_2
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/fragment/ListFragment;->ensureGrid()V

    .line 458
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment;->mListView:Landroid/widget/AbsListView;

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->setVisibility(I)V

    goto :goto_0
.end method
