.class Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$CheckForLongPress;
.super Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$WindowRunnnable;
.source "TwoWayAbsListView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CheckForLongPress"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;


# direct methods
.method private constructor <init>(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;)V
    .locals 1

    .prologue
    .line 1868
    iput-object p1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$CheckForLongPress;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$WindowRunnnable;-><init>(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$1;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;
    .param p2, "x1"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$1;

    .prologue
    .line 1868
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$CheckForLongPress;-><init>(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 1870
    iget-object v6, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$CheckForLongPress;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    iget v5, v6, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mMotionPosition:I

    .line 1871
    .local v5, "motionPosition":I
    iget-object v6, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$CheckForLongPress;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    iget-object v7, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$CheckForLongPress;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    iget v7, v7, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mFirstPosition:I

    sub-int v7, v5, v7

    invoke-virtual {v6, v7}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1872
    .local v0, "child":Landroid/view/View;
    if-eqz v0, :cond_1

    .line 1873
    iget-object v6, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$CheckForLongPress;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    iget v4, v6, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mMotionPosition:I

    .line 1874
    .local v4, "longPressPosition":I
    iget-object v6, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$CheckForLongPress;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    iget-object v6, v6, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mAdapter:Landroid/widget/ListAdapter;

    iget-object v7, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$CheckForLongPress;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    iget v7, v7, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mMotionPosition:I

    invoke-interface {v6, v7}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v2

    .line 1876
    .local v2, "longPressId":J
    const/4 v1, 0x0

    .line 1877
    .local v1, "handled":Z
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$CheckForLongPress;->sameWindow()Z

    move-result v6

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$CheckForLongPress;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    iget-boolean v6, v6, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mDataChanged:Z

    if-nez v6, :cond_0

    .line 1878
    iget-object v6, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$CheckForLongPress;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    # invokes: Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->performLongPress(Landroid/view/View;IJ)Z
    invoke-static {v6, v0, v4, v2, v3}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->access$500(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;Landroid/view/View;IJ)Z

    move-result v1

    .line 1880
    :cond_0
    if-eqz v1, :cond_2

    .line 1881
    iget-object v6, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$CheckForLongPress;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    const/4 v7, -0x1

    iput v7, v6, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mTouchMode:I

    .line 1882
    iget-object v6, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$CheckForLongPress;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    invoke-virtual {v6, v8}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->setPressed(Z)V

    .line 1883
    invoke-virtual {v0, v8}, Landroid/view/View;->setPressed(Z)V

    .line 1889
    .end local v1    # "handled":Z
    .end local v2    # "longPressId":J
    .end local v4    # "longPressPosition":I
    :cond_1
    :goto_0
    return-void

    .line 1885
    .restart local v1    # "handled":Z
    .restart local v2    # "longPressId":J
    .restart local v4    # "longPressPosition":I
    :cond_2
    iget-object v6, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$CheckForLongPress;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    const/4 v7, 0x2

    iput v7, v6, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mTouchMode:I

    goto :goto_0
.end method
