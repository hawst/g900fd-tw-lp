.class public interface abstract Lcom/samsung/musicplus/widget/tab/ITabController;
.super Ljava/lang/Object;
.source "ITabController.java"


# virtual methods
.method public abstract addTab(Ljava/lang/Integer;)V
.end method

.method public abstract enablePagerMode(Landroid/support/v4/view/ViewPager;)V
.end method

.method public abstract getCurrentContent()Ljava/lang/Object;
.end method

.method public abstract getCurrentTabId()Ljava/lang/Integer;
.end method

.method public abstract getTabCount()I
.end method

.method public abstract initializeTabs(Ljava/util/ArrayList;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation
.end method

.method public abstract onMultiWindowStateChanged(Z)V
.end method

.method public abstract onServiceConnected()V
.end method

.method public abstract onStart()V
.end method

.method public abstract onStop()V
.end method

.method public abstract receivePlayerState(Ljava/lang/String;)V
.end method

.method public abstract removeTab(I)V
.end method

.method public abstract setEnableTab(Z)V
.end method

.method public abstract setSwipable(Z)V
.end method

.method public abstract setTabSelected(Ljava/lang/Integer;)V
.end method

.method public abstract setVisibleTab(Z)V
.end method
