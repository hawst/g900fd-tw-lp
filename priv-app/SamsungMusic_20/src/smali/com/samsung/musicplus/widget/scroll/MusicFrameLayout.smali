.class public Lcom/samsung/musicplus/widget/scroll/MusicFrameLayout;
.super Landroid/widget/FrameLayout;
.source "MusicFrameLayout.java"


# static fields
.field private static final DEBUG:Z = false

.field private static final DEBUG_TAG:Ljava/lang/String; = "DEBUG_TH"

.field private static final HEADER_HEIGHT:I = 0x82

.field private static final TAB_MOVING:Z


# instance fields
.field private mIsPress:Z

.field private mPressHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 29
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 55
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/widget/scroll/MusicFrameLayout;->mIsPress:Z

    .line 57
    new-instance v0, Lcom/samsung/musicplus/widget/scroll/MusicFrameLayout$1;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/widget/scroll/MusicFrameLayout$1;-><init>(Lcom/samsung/musicplus/widget/scroll/MusicFrameLayout;)V

    iput-object v0, p0, Lcom/samsung/musicplus/widget/scroll/MusicFrameLayout;->mPressHandler:Landroid/os/Handler;

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 55
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/widget/scroll/MusicFrameLayout;->mIsPress:Z

    .line 57
    new-instance v0, Lcom/samsung/musicplus/widget/scroll/MusicFrameLayout$1;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/widget/scroll/MusicFrameLayout$1;-><init>(Lcom/samsung/musicplus/widget/scroll/MusicFrameLayout;)V

    iput-object v0, p0, Lcom/samsung/musicplus/widget/scroll/MusicFrameLayout;->mPressHandler:Landroid/os/Handler;

    .line 34
    return-void
.end method

.method static synthetic access$002(Lcom/samsung/musicplus/widget/scroll/MusicFrameLayout;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/scroll/MusicFrameLayout;
    .param p1, "x1"    # Z

    .prologue
    .line 17
    iput-boolean p1, p0, Lcom/samsung/musicplus/widget/scroll/MusicFrameLayout;->mIsPress:Z

    return p1
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 104
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 107
    const/4 v0, 0x0

    .line 110
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 0
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 51
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    .line 53
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 0
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 40
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;->onSizeChanged(IIII)V

    .line 41
    return-void
.end method
