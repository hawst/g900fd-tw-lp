.class public Lcom/samsung/musicplus/widget/ActionModeCallback;
.super Ljava/lang/Object;
.source "ActionModeCallback.java"

# interfaces
.implements Landroid/view/ActionMode$Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/widget/ActionModeCallback$OnActionModeListener;
    }
.end annotation


# static fields
.field private static final DESELECT_ALL:I = 0x7f0d01fe

.field private static final SELECT_ALL:I = 0x7f0d01fd


# instance fields
.field private mAbsListView:Landroid/widget/AbsListView;

.field private mActionMode:Landroid/view/ActionMode;

.field private mActionModeListener:Lcom/samsung/musicplus/widget/ActionModeCallback$OnActionModeListener;

.field private mCheckBox:Landroid/widget/CheckBox;

.field private mContext:Landroid/content/Context;

.field private mMenu:Landroid/view/Menu;

.field private mPopupMenu:Lcom/samsung/musicplus/widget/AnchorSizePopupMenu;

.field private mRootView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;Landroid/widget/AbsListView;Lcom/samsung/musicplus/widget/ActionModeCallback$OnActionModeListener;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "rootView"    # Landroid/view/View;
    .param p3, "alv"    # Landroid/widget/AbsListView;
    .param p4, "listener"    # Lcom/samsung/musicplus/widget/ActionModeCallback$OnActionModeListener;

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput-object p1, p0, Lcom/samsung/musicplus/widget/ActionModeCallback;->mContext:Landroid/content/Context;

    .line 61
    iput-object p2, p0, Lcom/samsung/musicplus/widget/ActionModeCallback;->mRootView:Landroid/view/View;

    .line 62
    iput-object p3, p0, Lcom/samsung/musicplus/widget/ActionModeCallback;->mAbsListView:Landroid/widget/AbsListView;

    .line 63
    iput-object p4, p0, Lcom/samsung/musicplus/widget/ActionModeCallback;->mActionModeListener:Lcom/samsung/musicplus/widget/ActionModeCallback$OnActionModeListener;

    .line 64
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/widget/ActionModeCallback;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/ActionModeCallback;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/samsung/musicplus/widget/ActionModeCallback;->mCheckBox:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/musicplus/widget/ActionModeCallback;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/ActionModeCallback;
    .param p1, "x1"    # Z

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/widget/ActionModeCallback;->setSelectAll(Z)V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/musicplus/widget/ActionModeCallback;)Lcom/samsung/musicplus/widget/AnchorSizePopupMenu;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/ActionModeCallback;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/samsung/musicplus/widget/ActionModeCallback;->mPopupMenu:Lcom/samsung/musicplus/widget/AnchorSizePopupMenu;

    return-object v0
.end method

.method private getValidListItemCount()I
    .locals 6

    .prologue
    .line 291
    const/4 v2, 0x0

    .line 292
    .local v2, "validCount":I
    iget-object v3, p0, Lcom/samsung/musicplus/widget/ActionModeCallback;->mAbsListView:Landroid/widget/AbsListView;

    invoke-virtual {v3}, Landroid/widget/AbsListView;->getCount()I

    move-result v0

    .line 293
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 294
    iget-object v3, p0, Lcom/samsung/musicplus/widget/ActionModeCallback;->mAbsListView:Landroid/widget/AbsListView;

    invoke-virtual {v3, v1}, Landroid/widget/AbsListView;->getItemIdAtPosition(I)J

    move-result-wide v4

    invoke-direct {p0, v4, v5}, Lcom/samsung/musicplus/widget/ActionModeCallback;->isValidItem(J)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 295
    add-int/lit8 v2, v2, 0x1

    .line 293
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 298
    :cond_1
    return v2
.end method

.method private isValidItem(J)Z
    .locals 3
    .param p1, "itemId"    # J

    .prologue
    .line 302
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-ltz v0, :cond_0

    .line 303
    const/4 v0, 0x1

    .line 305
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setSelectAll(Z)V
    .locals 4
    .param p1, "isSelectAll"    # Z

    .prologue
    .line 282
    iget-object v2, p0, Lcom/samsung/musicplus/widget/ActionModeCallback;->mAbsListView:Landroid/widget/AbsListView;

    invoke-virtual {v2}, Landroid/widget/AbsListView;->getCount()I

    move-result v0

    .line 283
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 284
    iget-object v2, p0, Lcom/samsung/musicplus/widget/ActionModeCallback;->mAbsListView:Landroid/widget/AbsListView;

    invoke-virtual {v2, v1}, Landroid/widget/AbsListView;->getItemIdAtPosition(I)J

    move-result-wide v2

    invoke-direct {p0, v2, v3}, Lcom/samsung/musicplus/widget/ActionModeCallback;->isValidItem(J)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 285
    iget-object v2, p0, Lcom/samsung/musicplus/widget/ActionModeCallback;->mAbsListView:Landroid/widget/AbsListView;

    invoke-virtual {v2, v1, p1}, Landroid/widget/AbsListView;->setItemChecked(IZ)V

    .line 283
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 288
    :cond_1
    return-void
.end method


# virtual methods
.method public finish()V
    .locals 1

    .prologue
    .line 337
    iget-object v0, p0, Lcom/samsung/musicplus/widget/ActionModeCallback;->mActionMode:Landroid/view/ActionMode;

    if-eqz v0, :cond_0

    .line 338
    iget-object v0, p0, Lcom/samsung/musicplus/widget/ActionModeCallback;->mActionMode:Landroid/view/ActionMode;

    invoke-virtual {v0}, Landroid/view/ActionMode;->finish()V

    .line 339
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/musicplus/widget/ActionModeCallback;->mActionMode:Landroid/view/ActionMode;

    .line 341
    :cond_0
    return-void
.end method

.method public getListCount()I
    .locals 1

    .prologue
    .line 309
    iget-object v0, p0, Lcom/samsung/musicplus/widget/ActionModeCallback;->mAbsListView:Landroid/widget/AbsListView;

    invoke-virtual {v0}, Landroid/widget/AbsListView;->getCount()I

    move-result v0

    return v0
.end method

.method public getSelectedPositions()[I
    .locals 9

    .prologue
    .line 313
    iget-object v8, p0, Lcom/samsung/musicplus/widget/ActionModeCallback;->mAbsListView:Landroid/widget/AbsListView;

    invoke-virtual {v8}, Landroid/widget/AbsListView;->getCheckedItemPositions()Landroid/util/SparseBooleanArray;

    move-result-object v7

    .line 314
    .local v7, "sp":Landroid/util/SparseBooleanArray;
    if-eqz v7, :cond_0

    invoke-virtual {v7}, Landroid/util/SparseBooleanArray;->size()I

    move-result v8

    if-nez v8, :cond_2

    .line 315
    :cond_0
    const/4 v5, 0x0

    .line 333
    :cond_1
    return-object v5

    .line 319
    :cond_2
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 320
    .local v4, "positionList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-virtual {v7}, Landroid/util/SparseBooleanArray;->size()I

    move-result v6

    .line 321
    .local v6, "size":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v6, :cond_4

    .line 322
    invoke-virtual {v7, v0}, Landroid/util/SparseBooleanArray;->valueAt(I)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 323
    invoke-virtual {v7, v0}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v4, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 321
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 328
    :cond_4
    const/4 v0, 0x0

    .line 329
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v8

    new-array v5, v8, [I

    .line 330
    .local v5, "positions":[I
    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    .line 331
    .local v3, "in":Ljava/lang/Integer;
    add-int/lit8 v1, v0, 0x1

    .end local v0    # "i":I
    .local v1, "i":I
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v8

    aput v8, v5, v0

    move v0, v1

    .line 332
    .end local v1    # "i":I
    .restart local v0    # "i":I
    goto :goto_1
.end method

.method public invalidataTitle()V
    .locals 11

    .prologue
    const v10, 0x7f1000de

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 227
    iget-object v7, p0, Lcom/samsung/musicplus/widget/ActionModeCallback;->mAbsListView:Landroid/widget/AbsListView;

    invoke-virtual {v7}, Landroid/widget/AbsListView;->getCheckedItemCount()I

    move-result v3

    .line 228
    .local v3, "selectedCount":I
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/ActionModeCallback;->getValidListItemCount()I

    move-result v4

    .line 230
    .local v4, "validListItemCount":I
    if-nez v4, :cond_1

    .line 231
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/ActionModeCallback;->finish()V

    .line 279
    :cond_0
    :goto_0
    return-void

    .line 234
    :cond_1
    if-lez v3, :cond_2

    if-ne v3, v4, :cond_2

    move v2, v5

    .line 236
    .local v2, "enableSelectAll":Z
    :goto_1
    if-nez v3, :cond_3

    move v1, v5

    .line 238
    .local v1, "enableDeselectAll":Z
    :goto_2
    iget-object v7, p0, Lcom/samsung/musicplus/widget/ActionModeCallback;->mActionMode:Landroid/view/ActionMode;

    if-eqz v7, :cond_0

    .line 239
    iget-object v7, p0, Lcom/samsung/musicplus/widget/ActionModeCallback;->mActionMode:Landroid/view/ActionMode;

    invoke-virtual {v7}, Landroid/view/ActionMode;->getCustomView()Landroid/view/View;

    move-result-object v7

    const v8, 0x7f0d0041

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 241
    .local v0, "actionModeTitle":Landroid/widget/TextView;
    if-eqz v0, :cond_4

    .line 242
    iget-object v7, p0, Lcom/samsung/musicplus/widget/ActionModeCallback;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    new-array v8, v6, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v5

    invoke-virtual {v7, v10, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 266
    :goto_3
    iget-object v7, p0, Lcom/samsung/musicplus/widget/ActionModeCallback;->mCheckBox:Landroid/widget/CheckBox;

    if-eqz v7, :cond_0

    .line 267
    iget-object v7, p0, Lcom/samsung/musicplus/widget/ActionModeCallback;->mCheckBox:Landroid/widget/CheckBox;

    if-nez v2, :cond_5

    :goto_4
    invoke-virtual {v7, v6}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_0

    .end local v0    # "actionModeTitle":Landroid/widget/TextView;
    .end local v1    # "enableDeselectAll":Z
    .end local v2    # "enableSelectAll":Z
    :cond_2
    move v2, v6

    .line 234
    goto :goto_1

    .restart local v2    # "enableSelectAll":Z
    :cond_3
    move v1, v6

    .line 236
    goto :goto_2

    .line 246
    .restart local v0    # "actionModeTitle":Landroid/widget/TextView;
    .restart local v1    # "enableDeselectAll":Z
    :cond_4
    iget-object v7, p0, Lcom/samsung/musicplus/widget/ActionModeCallback;->mActionMode:Landroid/view/ActionMode;

    invoke-virtual {v7}, Landroid/view/ActionMode;->getCustomView()Landroid/view/View;

    move-result-object v7

    const v8, 0x7f0d00ac

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .end local v0    # "actionModeTitle":Landroid/widget/TextView;
    check-cast v0, Landroid/widget/TextView;

    .line 248
    .restart local v0    # "actionModeTitle":Landroid/widget/TextView;
    iget-object v7, p0, Lcom/samsung/musicplus/widget/ActionModeCallback;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    new-array v8, v6, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v5

    invoke-virtual {v7, v10, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    :cond_5
    move v6, v5

    .line 267
    goto :goto_4
.end method

.method public invalidateActionMode()V
    .locals 2

    .prologue
    .line 221
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/ActionModeCallback;->invalidataTitle()V

    .line 222
    iget-object v0, p0, Lcom/samsung/musicplus/widget/ActionModeCallback;->mActionModeListener:Lcom/samsung/musicplus/widget/ActionModeCallback$OnActionModeListener;

    iget-object v1, p0, Lcom/samsung/musicplus/widget/ActionModeCallback;->mMenu:Landroid/view/Menu;

    invoke-interface {v0, v1}, Lcom/samsung/musicplus/widget/ActionModeCallback$OnActionModeListener;->onPrepareActionMode(Landroid/view/Menu;)V

    .line 223
    return-void
.end method

.method public isPopupShowing()Z
    .locals 1

    .prologue
    .line 344
    iget-object v0, p0, Lcom/samsung/musicplus/widget/ActionModeCallback;->mPopupMenu:Lcom/samsung/musicplus/widget/AnchorSizePopupMenu;

    if-eqz v0, :cond_0

    .line 345
    iget-object v0, p0, Lcom/samsung/musicplus/widget/ActionModeCallback;->mPopupMenu:Lcom/samsung/musicplus/widget/AnchorSizePopupMenu;

    invoke-virtual {v0}, Lcom/samsung/musicplus/widget/AnchorSizePopupMenu;->isShowing()Z

    move-result v0

    .line 347
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onActionItemClicked(Landroid/view/ActionMode;Landroid/view/MenuItem;)Z
    .locals 3
    .param p1, "mode"    # Landroid/view/ActionMode;
    .param p2, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 211
    iget-object v0, p0, Lcom/samsung/musicplus/widget/ActionModeCallback;->mActionModeListener:Lcom/samsung/musicplus/widget/ActionModeCallback$OnActionModeListener;

    iget-object v1, p0, Lcom/samsung/musicplus/widget/ActionModeCallback;->mAbsListView:Landroid/widget/AbsListView;

    invoke-virtual {v1}, Landroid/widget/AbsListView;->getCheckedItemIds()[J

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/ActionModeCallback;->getSelectedPositions()[I

    move-result-object v2

    invoke-interface {v0, p2, v1, v2}, Lcom/samsung/musicplus/widget/ActionModeCallback$OnActionModeListener;->onActionItemClicked(Landroid/view/MenuItem;[J[I)V

    .line 213
    const/4 v0, 0x1

    return v0
.end method

.method public onCreateActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 6
    .param p1, "mode"    # Landroid/view/ActionMode;
    .param p2, "menu"    # Landroid/view/Menu;

    .prologue
    .line 68
    iput-object p1, p0, Lcom/samsung/musicplus/widget/ActionModeCallback;->mActionMode:Landroid/view/ActionMode;

    .line 69
    iput-object p2, p0, Lcom/samsung/musicplus/widget/ActionModeCallback;->mMenu:Landroid/view/Menu;

    .line 72
    iget-object v3, p0, Lcom/samsung/musicplus/widget/ActionModeCallback;->mActionModeListener:Lcom/samsung/musicplus/widget/ActionModeCallback$OnActionModeListener;

    invoke-interface {v3, p2}, Lcom/samsung/musicplus/widget/ActionModeCallback$OnActionModeListener;->onInflateOptionsMenu(Landroid/view/Menu;)V

    .line 119
    iget-object v3, p0, Lcom/samsung/musicplus/widget/ActionModeCallback;->mContext:Landroid/content/Context;

    invoke-static {v3}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v3

    const v4, 0x7f04002d

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 121
    .local v0, "actionModeView":Landroid/view/View;
    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v4, -0x2

    const/4 v5, -0x1

    invoke-direct {v3, v4, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 123
    invoke-virtual {p1, v0}, Landroid/view/ActionMode;->setCustomView(Landroid/view/View;)V

    .line 124
    const v3, 0x7f0d00a9

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 125
    .local v2, "selectAllLayout":Landroid/view/View;
    const v3, 0x7f0d00aa

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 126
    .local v1, "selectAll":Landroid/view/View;
    const v3, 0x7f0d00ab

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CheckBox;

    iput-object v3, p0, Lcom/samsung/musicplus/widget/ActionModeCallback;->mCheckBox:Landroid/widget/CheckBox;

    .line 127
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 128
    new-instance v3, Lcom/samsung/musicplus/widget/ActionModeCallback$3;

    invoke-direct {v3, p0}, Lcom/samsung/musicplus/widget/ActionModeCallback$3;-><init>(Lcom/samsung/musicplus/widget/ActionModeCallback;)V

    invoke-virtual {v1, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 142
    iget-object v3, p0, Lcom/samsung/musicplus/widget/ActionModeCallback;->mContext:Landroid/content/Context;

    invoke-static {v3, v1}, Lcom/samsung/musicplus/util/UiUtils;->setReferenceListBackgroundWithHover(Landroid/content/Context;Landroid/view/View;)V

    .line 185
    const/4 v3, 0x1

    return v3
.end method

.method public onDestroyActionMode(Landroid/view/ActionMode;)V
    .locals 3
    .param p1, "mode"    # Landroid/view/ActionMode;

    .prologue
    .line 191
    iget-object v1, p0, Lcom/samsung/musicplus/widget/ActionModeCallback;->mRootView:Landroid/view/View;

    const v2, 0x7f0d00a9

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 192
    .local v0, "selectAllLayout":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 193
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 195
    :cond_0
    iget-object v1, p0, Lcom/samsung/musicplus/widget/ActionModeCallback;->mCheckBox:Landroid/widget/CheckBox;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 201
    iget-object v1, p0, Lcom/samsung/musicplus/widget/ActionModeCallback;->mActionModeListener:Lcom/samsung/musicplus/widget/ActionModeCallback$OnActionModeListener;

    invoke-interface {v1}, Lcom/samsung/musicplus/widget/ActionModeCallback$OnActionModeListener;->onDestroyActionMode()V

    .line 202
    return-void
.end method

.method public onPrepareActionMode(Landroid/view/ActionMode;Landroid/view/Menu;)Z
    .locals 1
    .param p1, "mode"    # Landroid/view/ActionMode;
    .param p2, "menu"    # Landroid/view/Menu;

    .prologue
    .line 206
    const/4 v0, 0x1

    return v0
.end method

.method public showPopupMenu()V
    .locals 2

    .prologue
    .line 351
    iget-object v0, p0, Lcom/samsung/musicplus/widget/ActionModeCallback;->mRootView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 352
    iget-object v0, p0, Lcom/samsung/musicplus/widget/ActionModeCallback;->mRootView:Landroid/view/View;

    new-instance v1, Lcom/samsung/musicplus/widget/ActionModeCallback$6;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/widget/ActionModeCallback$6;-><init>(Lcom/samsung/musicplus/widget/ActionModeCallback;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 359
    :cond_0
    return-void
.end method
