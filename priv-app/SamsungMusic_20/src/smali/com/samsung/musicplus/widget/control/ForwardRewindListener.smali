.class public Lcom/samsung/musicplus/widget/control/ForwardRewindListener;
.super Ljava/lang/Object;
.source "ForwardRewindListener.java"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mControlTask:Lcom/samsung/musicplus/widget/control/FowardRewindControlTask;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/samsung/musicplus/widget/control/ForwardRewindListener;->mContext:Landroid/content/Context;

    .line 20
    return-void
.end method

.method private cancelEventAndPlay(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v1, 0x0

    .line 72
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 81
    :goto_0
    :pswitch_0
    invoke-virtual {p1, v1}, Landroid/view/View;->playSoundEffect(I)V

    .line 82
    return-void

    .line 74
    :pswitch_1
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->playNext()V

    goto :goto_0

    .line 77
    :pswitch_2
    invoke-static {v1}, Lcom/samsung/musicplus/util/ServiceUtils;->playPrev(Z)V

    goto :goto_0

    .line 72
    :pswitch_data_0
    .packed-switch 0x7f0d00f9
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private setControlTaskRun(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v1, 0x3

    const/4 v5, 0x0

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 61
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 69
    :goto_0
    :pswitch_0
    return-void

    .line 63
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/musicplus/widget/control/ForwardRewindListener;->mControlTask:Lcom/samsung/musicplus/widget/control/FowardRewindControlTask;

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/samsung/musicplus/widget/control/ForwardRewindListener;->mContext:Landroid/content/Context;

    aput-object v2, v1, v5

    aput-object p1, v1, v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/widget/control/FowardRewindControlTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 66
    :pswitch_2
    iget-object v0, p0, Lcom/samsung/musicplus/widget/control/ForwardRewindListener;->mControlTask:Lcom/samsung/musicplus/widget/control/FowardRewindControlTask;

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/samsung/musicplus/widget/control/ForwardRewindListener;->mContext:Landroid/content/Context;

    aput-object v2, v1, v5

    aput-object p1, v1, v3

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/widget/control/FowardRewindControlTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    .line 61
    :pswitch_data_0
    .packed-switch 0x7f0d00f9
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/samsung/musicplus/widget/control/ForwardRewindListener;->mControlTask:Lcom/samsung/musicplus/widget/control/FowardRewindControlTask;

    if-eqz v0, :cond_0

    .line 24
    iget-object v0, p0, Lcom/samsung/musicplus/widget/control/ForwardRewindListener;->mControlTask:Lcom/samsung/musicplus/widget/control/FowardRewindControlTask;

    invoke-virtual {v0}, Lcom/samsung/musicplus/widget/control/FowardRewindControlTask;->setCancel()V

    .line 25
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/musicplus/widget/control/ForwardRewindListener;->mControlTask:Lcom/samsung/musicplus/widget/control/FowardRewindControlTask;

    .line 27
    :cond_0
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v3, 0x0

    .line 31
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    .line 57
    :cond_0
    :goto_0
    :pswitch_0
    const/4 v2, 0x0

    return v2

    .line 33
    :pswitch_1
    iget-object v2, p0, Lcom/samsung/musicplus/widget/control/ForwardRewindListener;->mControlTask:Lcom/samsung/musicplus/widget/control/FowardRewindControlTask;

    if-eqz v2, :cond_1

    .line 34
    iget-object v2, p0, Lcom/samsung/musicplus/widget/control/ForwardRewindListener;->mControlTask:Lcom/samsung/musicplus/widget/control/FowardRewindControlTask;

    invoke-virtual {v2}, Lcom/samsung/musicplus/widget/control/FowardRewindControlTask;->setCancel()V

    .line 35
    iput-object v3, p0, Lcom/samsung/musicplus/widget/control/ForwardRewindListener;->mControlTask:Lcom/samsung/musicplus/widget/control/FowardRewindControlTask;

    .line 37
    :cond_1
    new-instance v2, Lcom/samsung/musicplus/widget/control/FowardRewindControlTask;

    invoke-direct {v2}, Lcom/samsung/musicplus/widget/control/FowardRewindControlTask;-><init>()V

    iput-object v2, p0, Lcom/samsung/musicplus/widget/control/ForwardRewindListener;->mControlTask:Lcom/samsung/musicplus/widget/control/FowardRewindControlTask;

    .line 38
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/widget/control/ForwardRewindListener;->setControlTaskRun(Landroid/view/View;)V

    goto :goto_0

    .line 43
    :pswitch_2
    iget-object v2, p0, Lcom/samsung/musicplus/widget/control/ForwardRewindListener;->mControlTask:Lcom/samsung/musicplus/widget/control/FowardRewindControlTask;

    if-eqz v2, :cond_2

    .line 44
    iget-object v2, p0, Lcom/samsung/musicplus/widget/control/ForwardRewindListener;->mControlTask:Lcom/samsung/musicplus/widget/control/FowardRewindControlTask;

    invoke-virtual {v2}, Lcom/samsung/musicplus/widget/control/FowardRewindControlTask;->setCancel()V

    .line 45
    iput-object v3, p0, Lcom/samsung/musicplus/widget/control/ForwardRewindListener;->mControlTask:Lcom/samsung/musicplus/widget/control/FowardRewindControlTask;

    .line 47
    :cond_2
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v2

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getDownTime()J

    move-result-wide v4

    sub-long v0, v2, v4

    .line 48
    .local v0, "pressdTime":J
    const-wide/16 v2, 0x12c

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    .line 51
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/widget/control/ForwardRewindListener;->cancelEventAndPlay(Landroid/view/View;)V

    goto :goto_0

    .line 31
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
