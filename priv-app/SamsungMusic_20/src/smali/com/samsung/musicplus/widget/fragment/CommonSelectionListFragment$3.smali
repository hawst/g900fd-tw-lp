.class Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment$3;
.super Landroid/content/BroadcastReceiver;
.source "CommonSelectionListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;)V
    .locals 0

    .prologue
    .line 854
    iput-object p1, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment$3;->this$0:Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 857
    iget-object v3, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment$3;->this$0:Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;

    invoke-virtual {v3}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->getAbsListView()Landroid/widget/AbsListView;

    move-result-object v0

    .line 858
    .local v0, "lv":Landroid/widget/AbsListView;
    iget-object v3, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment$3;->this$0:Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;

    # getter for: Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mActionModeCallBack:Lcom/samsung/musicplus/widget/ActionModeCallback;
    invoke-static {v3}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->access$000(Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;)Lcom/samsung/musicplus/widget/ActionModeCallback;

    move-result-object v3

    if-eqz v3, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/widget/AbsListView;->getCheckedItemCount()I

    move-result v3

    if-lez v3, :cond_0

    .line 859
    invoke-virtual {v0}, Landroid/widget/AbsListView;->getCheckedItemIds()[J

    move-result-object v1

    .line 860
    .local v1, "selectedItemIds":[J
    iget-object v3, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment$3;->this$0:Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;

    # getter for: Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mActionModeCallBack:Lcom/samsung/musicplus/widget/ActionModeCallback;
    invoke-static {v3}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->access$000(Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;)Lcom/samsung/musicplus/widget/ActionModeCallback;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/musicplus/widget/ActionModeCallback;->getSelectedPositions()[I

    move-result-object v2

    .line 861
    .local v2, "selectedPositions":[I
    iget-object v3, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment$3;->this$0:Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;

    invoke-virtual {v3}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->isResumed()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 862
    iget-object v3, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment$3;->this$0:Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;

    # invokes: Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->deleteItem([J[I)V
    invoke-static {v3, v1, v2}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->access$100(Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;[J[I)V

    .line 865
    .end local v1    # "selectedItemIds":[J
    .end local v2    # "selectedPositions":[I
    :cond_0
    return-void
.end method
