.class public Lcom/samsung/musicplus/widget/control/FowardRewindControlTaskMediaButton;
.super Lcom/samsung/musicplus/widget/control/FowardRewindControlTask;
.source "FowardRewindControlTaskMediaButton.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "direction"    # I

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/control/FowardRewindControlTask;-><init>()V

    .line 12
    packed-switch p2, :pswitch_data_0

    .line 22
    :goto_0
    return-void

    .line 14
    :pswitch_0
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.musicplus.action.PLAYBACK_FORWARD"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    .line 17
    :pswitch_1
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.musicplus.action.PLAYBACK_REWIND"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    .line 12
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
