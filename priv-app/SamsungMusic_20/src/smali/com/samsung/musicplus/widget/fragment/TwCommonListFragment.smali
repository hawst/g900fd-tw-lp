.class public Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;
.super Lcom/samsung/musicplus/widget/fragment/TwListFragment;
.source "TwCommonListFragment.java"

# interfaces
.implements Landroid/app/LoaderManager$LoaderCallbacks;
.implements Lcom/samsung/musicplus/contents/menu/CommonListMenuHandler;
.implements Lcom/samsung/musicplus/settings/PlayerSettingPreference;
.implements Lcom/samsung/musicplus/util/UiUtils$Defs;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/samsung/musicplus/widget/fragment/TwListFragment;",
        "Lcom/samsung/musicplus/settings/PlayerSettingPreference;",
        "Landroid/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Lcom/samsung/musicplus/util/UiUtils$Defs;",
        "Lcom/samsung/musicplus/contents/menu/CommonListMenuHandler;"
    }
.end annotation


# static fields
.field private static final DEBUG:Z = false

.field private static final DEBUG_LOADER:Z = false

.field protected static final TAG:Ljava/lang/String; = "MusicUiList"


# instance fields
.field protected mAdapter:Landroid/widget/CursorAdapter;

.field private mAdapterLayout:I

.field protected mAlbumArtLoader:Lcom/samsung/musicplus/util/IAlbumArtLoader;

.field private mAlbumArtParsingEnabled:Z

.field protected mAlbumIdIdx:I

.field protected mAudioIdIdx:I

.field protected mBitDepthIdx:I

.field protected mCountOfData:I

.field protected mDurationIdx:I

.field protected mIndex:Lcom/samsung/musicplus/widget/database/IndexView;

.field protected mIndexIdx:I

.field private mInfo:Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;

.field private mIsEmptyList:Z

.field protected mIsPause:Z

.field protected mIsStop:Z

.field protected mKey:Ljava/lang/String;

.field protected mKeyWordIdx:I

.field protected mList:I

.field private mListPosition:I

.field protected mNoItemTextId:I

.field protected mNumberTextId:I

.field private mNumberView:Landroid/widget/TextView;

.field protected mPreferences:Landroid/content/SharedPreferences;

.field protected mQueryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

.field protected mSamplingRateIdx:I

.field protected mSecretBoxIdx:I

.field protected mSelectedPosionKeyword:Ljava/lang/String;

.field protected mSplitSubFragment:Landroid/app/Fragment;

.field protected mText1Idx:I

.field protected mText2Idx:I

.field protected mText3Idx:I

.field private mUiHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 75
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/fragment/TwListFragment;-><init>()V

    .line 101
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mIsEmptyList:Z

    .line 103
    iput-object v2, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mSelectedPosionKeyword:Ljava/lang/String;

    .line 105
    iput-object v2, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mSplitSubFragment:Landroid/app/Fragment;

    .line 139
    iput-boolean v1, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mIsPause:Z

    .line 141
    iput-boolean v1, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mIsStop:Z

    .line 145
    iput v1, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mAdapterLayout:I

    .line 931
    new-instance v0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment$2;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment$2;-><init>(Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;)V

    iput-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mUiHandler:Landroid/os/Handler;

    return-void
.end method

.method private detachIndexView()V
    .locals 3

    .prologue
    .line 532
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->getListContainer()Landroid/view/ViewGroup;

    move-result-object v1

    .line 533
    .local v1, "vg":Landroid/view/ViewGroup;
    const-string v2, "index"

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    .line 534
    .local v0, "index":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 535
    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 537
    :cond_0
    return-void
.end method

.method private invalidateListWithAlbumArtParsing()V
    .locals 2

    .prologue
    .line 420
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->setAlbumArtParsingEnabled(Z)V

    .line 421
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->getAbsListView()Lcom/sec/android/touchwiz/widget/TwAbsListView;

    move-result-object v0

    .line 422
    .local v0, "lv":Lcom/sec/android/touchwiz/widget/TwAbsListView;
    if-eqz v0, :cond_0

    .line 423
    invoke-virtual {v0}, Lcom/sec/android/touchwiz/widget/TwAbsListView;->invalidateViews()V

    .line 425
    :cond_0
    return-void
.end method

.method private isValidItem(J)Z
    .locals 3
    .param p1, "itemId"    # J

    .prologue
    .line 954
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-ltz v0, :cond_0

    .line 955
    const/4 v0, 0x1

    .line 957
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setAdapterSelectedKeyword(Ljava/lang/String;)V
    .locals 1
    .param p1, "keyword"    # Ljava/lang/String;

    .prologue
    .line 775
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mAdapter:Landroid/widget/CursorAdapter;

    instance-of v0, v0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;

    if-eqz v0, :cond_0

    .line 776
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mAdapter:Landroid/widget/CursorAdapter;

    check-cast v0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->setSelectedKeyword(Ljava/lang/String;)V

    .line 778
    :cond_0
    return-void
.end method

.method private setEmptyListState()V
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mAdapter:Landroid/widget/CursorAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mAdapter:Landroid/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/widget/CursorAdapter;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 240
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mIsEmptyList:Z

    .line 244
    :goto_0
    return-void

    .line 242
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mIsEmptyList:Z

    goto :goto_0
.end method

.method private setListToTop()V
    .locals 2

    .prologue
    .line 552
    iget v1, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mListPosition:I

    if-lez v1, :cond_0

    .line 553
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->getAbsListView()Lcom/sec/android/touchwiz/widget/TwAbsListView;

    move-result-object v0

    .line 554
    .local v0, "lv":Lcom/sec/android/touchwiz/widget/TwAbsListView;
    if-nez v0, :cond_1

    .line 566
    .end local v0    # "lv":Lcom/sec/android/touchwiz/widget/TwAbsListView;
    :cond_0
    :goto_0
    return-void

    .line 558
    .restart local v0    # "lv":Lcom/sec/android/touchwiz/widget/TwAbsListView;
    :cond_1
    new-instance v1, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment$1;

    invoke-direct {v1, p0, v0}, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment$1;-><init>(Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;Lcom/sec/android/touchwiz/widget/TwAbsListView;)V

    invoke-virtual {v0, v1}, Lcom/sec/android/touchwiz/widget/TwAbsListView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method


# virtual methods
.method protected addOtherView(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 6
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    const/4 v5, 0x1

    .line 258
    if-nez p1, :cond_1

    .line 290
    .end local p1    # "c":Landroid/database/Cursor;
    :cond_0
    :goto_0
    return-object p1

    .line 262
    .restart local p1    # "c":Landroid/database/Cursor;
    :cond_1
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    .line 263
    .local v0, "count":I
    iput v0, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mCountOfData:I

    .line 264
    if-eqz v0, :cond_0

    .line 273
    new-instance v2, Lcom/samsung/musicplus/widget/database/MusicCursor;

    invoke-direct {v2, p1}, Lcom/samsung/musicplus/widget/database/MusicCursor;-><init>(Landroid/database/Cursor;)V

    .line 274
    .local v2, "music":Lcom/samsung/musicplus/widget/database/MusicCursor;
    const/4 v1, 0x0

    .line 276
    .local v1, "hasHeaderView":Z
    iget v3, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mList:I

    invoke-virtual {p0, v3}, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->isEnableListShuffle(I)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 277
    invoke-virtual {v2, v5}, Lcom/samsung/musicplus/widget/database/MusicCursor;->setShuffleView(Z)V

    .line 278
    const/4 v1, 0x1

    .line 279
    iget-object v3, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mAdapter:Landroid/widget/CursorAdapter;

    instance-of v3, v3, Lcom/samsung/musicplus/widget/list/TrackListAdapter;

    if-eqz v3, :cond_2

    .line 280
    iget-object v3, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mAdapter:Landroid/widget/CursorAdapter;

    check-cast v3, Lcom/samsung/musicplus/widget/list/TrackListAdapter;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->setShufflePosition(I)V

    .line 285
    :cond_2
    if-eqz v1, :cond_3

    .line 286
    iget-object v3, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mIndex:Lcom/samsung/musicplus/widget/database/IndexView;

    if-eqz v3, :cond_3

    .line 287
    iget-object v3, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mIndex:Lcom/samsung/musicplus/widget/database/IndexView;

    invoke-virtual {v3, v5}, Lcom/samsung/musicplus/widget/database/IndexView;->setDefaultViewCount(I)V

    :cond_3
    move-object p1, v2

    .line 290
    goto :goto_0
.end method

.method public clearListSelectedPosition()V
    .locals 1

    .prologue
    .line 770
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mSelectedPosionKeyword:Ljava/lang/String;

    .line 771
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mSelectedPosionKeyword:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->setAdapterSelectedKeyword(Ljava/lang/String;)V

    .line 772
    return-void
.end method

.method protected createAdapter()Landroid/widget/CursorAdapter;
    .locals 5

    .prologue
    .line 524
    new-instance v1, Lcom/samsung/musicplus/widget/list/CommonListAdapter;

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget v0, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mAdapterLayout:I

    if-nez v0, :cond_0

    const v0, 0x7f040033

    :goto_0
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct {v1, v2, v0, v3, v4}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;I)V

    return-object v1

    :cond_0
    iget v0, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mAdapterLayout:I

    goto :goto_0
.end method

.method protected createMusicMenuHandler()Lcom/samsung/musicplus/common/menu/MusicMenuHandler;
    .locals 0

    .prologue
    .line 899
    return-object p0
.end method

.method protected createMusicMenus()Lcom/samsung/musicplus/common/menu/AbstractMusicMenus;
    .locals 1

    .prologue
    .line 158
    new-instance v0, Lcom/samsung/musicplus/contents/menu/CommonListMenus;

    invoke-direct {v0}, Lcom/samsung/musicplus/contents/menu/CommonListMenus;-><init>()V

    return-object v0
.end method

.method protected dispatchReCreateLoader()V
    .locals 3

    .prologue
    .line 878
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    iget v1, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mList:I

    invoke-virtual {v0, v1}, Landroid/app/LoaderManager;->destroyLoader(I)V

    .line 879
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    iget v1, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mList:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    .line 880
    return-void
.end method

.method public getAudioId(I)J
    .locals 5
    .param p1, "position"    # I

    .prologue
    .line 822
    const-wide/16 v0, -0x1

    .line 823
    .local v0, "audioId":J
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v3

    invoke-interface {v3, p1}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/database/Cursor;

    .line 824
    .local v2, "c":Landroid/database/Cursor;
    if-eqz v2, :cond_0

    iget v3, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mAudioIdIdx:I

    const/4 v4, -0x1

    if-le v3, v4, :cond_0

    .line 825
    iget v3, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mAudioIdIdx:I

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 827
    :cond_0
    return-wide v0
.end method

.method public getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 884
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    return-object v0
.end method

.method public getContextSelectedId(Landroid/view/ContextMenu$ContextMenuInfo;)J
    .locals 4
    .param p1, "menuInfo"    # Landroid/view/ContextMenu$ContextMenuInfo;

    .prologue
    .line 702
    move-object v0, p1

    check-cast v0, Landroid/widget/AdapterView$AdapterContextMenuInfo;

    .line 703
    .local v0, "mi":Landroid/widget/AdapterView$AdapterContextMenuInfo;
    iget-wide v2, v0, Landroid/widget/AdapterView$AdapterContextMenuInfo;->id:J

    return-wide v2
.end method

.method public getFragment()Landroid/app/Fragment;
    .locals 0

    .prologue
    .line 909
    return-object p0
.end method

.method public getKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 904
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mKey:Ljava/lang/String;

    return-object v0
.end method

.method public getKeyWord(I)Ljava/lang/String;
    .locals 5
    .param p1, "position"    # I

    .prologue
    .line 848
    const-string v2, ""

    .line 849
    .local v2, "keyWord":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v3

    invoke-interface {v3, p1}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 850
    .local v0, "c":Landroid/database/Cursor;
    if-eqz v0, :cond_0

    iget v3, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mKeyWordIdx:I

    const/4 v4, -0x1

    if-le v3, v4, :cond_0

    .line 851
    iget v3, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mKeyWordIdx:I

    invoke-interface {v0}, Landroid/database/Cursor;->getColumnCount()I

    move-result v4

    if-ge v3, v4, :cond_0

    .line 853
    :try_start_0
    iget v3, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mKeyWordIdx:I

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 859
    :cond_0
    :goto_0
    return-object v2

    .line 854
    :catch_0
    move-exception v1

    .line 855
    .local v1, "e":Ljava/lang/IndexOutOfBoundsException;
    invoke-virtual {v1}, Ljava/lang/IndexOutOfBoundsException;->printStackTrace()V

    goto :goto_0
.end method

.method public getList()I
    .locals 1

    .prologue
    .line 889
    iget v0, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mList:I

    return v0
.end method

.method public getListItemCount()I
    .locals 2

    .prologue
    .line 804
    const/4 v0, 0x0

    .line 805
    .local v0, "count":I
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->getAbsListView()Lcom/sec/android/touchwiz/widget/TwAbsListView;

    move-result-object v1

    .line 806
    .local v1, "lv":Lcom/sec/android/touchwiz/widget/TwAbsListView;
    if-eqz v1, :cond_0

    .line 807
    invoke-virtual {v1}, Lcom/sec/android/touchwiz/widget/TwAbsListView;->getCount()I

    move-result v0

    .line 809
    :cond_0
    return v0
.end method

.method protected getListSongCount(I)I
    .locals 2
    .param p1, "listType"    # I

    .prologue
    .line 813
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->getListItemCount()I

    move-result v0

    .line 814
    .local v0, "count":I
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->isEnableListShuffle(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 815
    add-int/lit8 v0, v0, -0x1

    .line 817
    :cond_0
    return v0
.end method

.method public getListType()I
    .locals 1

    .prologue
    .line 962
    iget v0, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mList:I

    return v0
.end method

.method protected getNumberOfItmes()I
    .locals 1

    .prologue
    .line 799
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->getListItemCount()I

    move-result v0

    return v0
.end method

.method protected getPreferences()Landroid/content/SharedPreferences;
    .locals 3

    .prologue
    .line 687
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mPreferences:Landroid/content/SharedPreferences;

    if-nez v0, :cond_0

    .line 688
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "music_player_pref"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mPreferences:Landroid/content/SharedPreferences;

    .line 691
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mPreferences:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method public getQueryArgs()Lcom/samsung/musicplus/util/ListUtils$QueryArgs;
    .locals 1

    .prologue
    .line 894
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mQueryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    return-object v0
.end method

.method public getSongList(II)[J
    .locals 11
    .param p1, "listType"    # I
    .param p2, "position"    # I

    .prologue
    .line 709
    invoke-virtual {p0, p2}, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->getKeyWord(I)Ljava/lang/String;

    move-result-object v9

    .line 710
    .local v9, "keyWord":Ljava/lang/String;
    iget v0, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mList:I

    invoke-static {v0}, Lcom/samsung/musicplus/util/ListUtils;->getSubTrackList(I)I

    move-result v0

    invoke-static {v0, v9}, Lcom/samsung/musicplus/util/ListUtils;->getMusicListInfo(ILjava/lang/String;)Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;

    move-result-object v10

    .line 711
    .local v10, "li":Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;
    iget-object v6, v10, Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;->queryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    .line 712
    .local v6, "args":Lcom/samsung/musicplus/util/ListUtils$QueryArgs;
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, v6, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->uri:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    iget-object v3, v6, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->selection:Ljava/lang/String;

    iget-object v4, v6, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->selectionArgs:[Ljava/lang/String;

    iget-object v5, v6, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->orderBy:Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 716
    .local v7, "c":Landroid/database/Cursor;
    invoke-static {v7}, Lcom/samsung/musicplus/util/ListUtils;->getSongListForCursor(Landroid/database/Cursor;)[J

    move-result-object v8

    .line 717
    .local v8, "id":[J
    if-eqz v7, :cond_0

    .line 718
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 720
    :cond_0
    return-object v8
.end method

.method public getTitleString(I)Ljava/lang/String;
    .locals 5
    .param p1, "position"    # I

    .prologue
    .line 832
    const-string v2, ""

    .line 833
    .local v2, "title":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v3

    invoke-interface {v3, p1}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 834
    .local v0, "c":Landroid/database/Cursor;
    if-eqz v0, :cond_0

    iget v3, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mText1Idx:I

    const/4 v4, -0x1

    if-le v3, v4, :cond_0

    .line 835
    iget v3, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mText1Idx:I

    invoke-interface {v0}, Landroid/database/Cursor;->getColumnCount()I

    move-result v4

    if-ge v3, v4, :cond_0

    .line 837
    :try_start_0
    iget v3, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mText1Idx:I

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 843
    :cond_0
    :goto_0
    return-object v2

    .line 838
    :catch_0
    move-exception v1

    .line 839
    .local v1, "e":Ljava/lang/IndexOutOfBoundsException;
    invoke-virtual {v1}, Ljava/lang/IndexOutOfBoundsException;->printStackTrace()V

    goto :goto_0
.end method

.method protected getValidListItemCount()I
    .locals 6

    .prologue
    .line 939
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->getAbsListView()Lcom/sec/android/touchwiz/widget/TwAbsListView;

    move-result-object v2

    .line 940
    .local v2, "lv":Lcom/sec/android/touchwiz/widget/TwAbsListView;
    if-eqz v2, :cond_1

    .line 941
    const/4 v3, 0x0

    .line 942
    .local v3, "validCount":I
    invoke-virtual {v2}, Lcom/sec/android/touchwiz/widget/TwAbsListView;->getCount()I

    move-result v0

    .line 943
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_2

    .line 944
    invoke-virtual {v2, v1}, Lcom/sec/android/touchwiz/widget/TwAbsListView;->getItemIdAtPosition(I)J

    move-result-wide v4

    invoke-direct {p0, v4, v5}, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->isValidItem(J)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 945
    add-int/lit8 v3, v3, 0x1

    .line 943
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 950
    .end local v0    # "count":I
    .end local v1    # "i":I
    .end local v3    # "validCount":I
    :cond_1
    const/4 v3, 0x0

    :cond_2
    return v3
.end method

.method public handlePrepareContextMenu(Landroid/view/Menu;JI)V
    .locals 0
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "itemId"    # J
    .param p4, "position"    # I

    .prologue
    .line 919
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->prepareContextMenu(Landroid/view/Menu;JI)V

    .line 920
    return-void
.end method

.method public handlePrepareOptionMenu(Landroid/view/Menu;)V
    .locals 0
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 914
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->prepareOptionMenu(Landroid/view/Menu;)V

    .line 915
    return-void
.end method

.method protected initLoader()V
    .locals 4

    .prologue
    .line 518
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v1

    iget v2, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mList:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, p0}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    move-result-object v0

    .line 519
    .local v0, "l":Landroid/content/Loader;, "Landroid/content/Loader<Landroid/database/Cursor;>;"
    const-string v2, "MusicUiList"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "initLoader : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " loader : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " l.isReset() : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-nez v0, :cond_0

    const-string v1, " Loader is null"

    :goto_0
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 521
    return-void

    .line 519
    :cond_0
    invoke-virtual {v0}, Landroid/content/Loader;->isReset()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto :goto_0
.end method

.method protected initializeList()V
    .locals 1

    .prologue
    .line 495
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->createAdapter()Landroid/widget/CursorAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mAdapter:Landroid/widget/CursorAdapter;

    .line 496
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mAdapter:Landroid/widget/CursorAdapter;

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 497
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->setListShown(Z)V

    .line 500
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->updateListInfo()Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mInfo:Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;

    .line 501
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->initLoader()V

    .line 502
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->setListToTop()V

    .line 507
    return-void
.end method

.method public isEmptyList()Z
    .locals 1

    .prologue
    .line 967
    iget-boolean v0, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mIsEmptyList:Z

    return v0
.end method

.method protected isEnableListShuffle(I)Z
    .locals 1
    .param p1, "list"    # I

    .prologue
    .line 300
    invoke-static {p1}, Lcom/samsung/musicplus/util/ListUtils;->isTrack(I)Z

    move-result v0

    return v0
.end method

.method protected isShowNumberView(I)Z
    .locals 4
    .param p1, "count"    # I

    .prologue
    const/4 v3, 0x1

    .line 462
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->getView()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f0d008f

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 463
    .local v0, "view":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 464
    if-nez p1, :cond_1

    .line 465
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 471
    :cond_0
    :goto_0
    return v3

    .line 466
    :cond_1
    iget v1, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mList:I

    invoke-static {v1}, Lcom/samsung/musicplus/util/ListUtils;->isGridType(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 467
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method protected launchSplitSubFragment(Ljava/lang/String;)V
    .locals 6
    .param p1, "keyWord"    # Ljava/lang/String;

    .prologue
    .line 743
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 744
    .local v0, "a":Landroid/app/Activity;
    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0}, Landroid/app/Activity;->isDestroyed()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->isAdded()Z

    move-result v2

    if-nez v2, :cond_1

    .line 767
    :cond_0
    :goto_0
    return-void

    .line 748
    :cond_1
    iget-object v2, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mAdapter:Landroid/widget/CursorAdapter;

    instance-of v2, v2, Lcom/samsung/musicplus/widget/list/CommonListAdapter;

    if-eqz v2, :cond_2

    .line 749
    iget-object v2, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mAdapter:Landroid/widget/CursorAdapter;

    check-cast v2, Lcom/samsung/musicplus/widget/list/CommonListAdapter;

    invoke-virtual {v2, p1}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->setSelectedKeyword(Ljava/lang/String;)V

    .line 752
    :cond_2
    iget-object v2, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mSplitSubFragment:Landroid/app/Fragment;

    if-eqz v2, :cond_3

    .line 753
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->getChildFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 754
    .local v1, "transaction":Landroid/app/FragmentTransaction;
    iget-object v2, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mSplitSubFragment:Landroid/app/Fragment;

    invoke-virtual {v1, v2}, Landroid/app/FragmentTransaction;->detach(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 755
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 757
    .end local v1    # "transaction":Landroid/app/FragmentTransaction;
    :cond_3
    iget v2, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mList:I

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/samsung/musicplus/contents/ContentsFragmentFactory;->getNewInstance(ILjava/lang/String;)Landroid/app/Fragment;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mSplitSubFragment:Landroid/app/Fragment;

    .line 759
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->setHasOptionsMenu(Z)V

    .line 761
    iget-object v2, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mSplitSubFragment:Landroid/app/Fragment;

    invoke-virtual {v2}, Landroid/app/Fragment;->isAdded()Z

    move-result v2

    if-nez v2, :cond_0

    .line 762
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->getChildFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 763
    .restart local v1    # "transaction":Landroid/app/FragmentTransaction;
    const v2, 0x7f0d0097

    iget-object v3, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mSplitSubFragment:Landroid/app/Fragment;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "tabcontent_split_sub"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mList:I

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 765
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto :goto_0
.end method

.method protected needEmptyView(Landroid/database/Cursor;)Z
    .locals 1
    .param p1, "data"    # Landroid/database/Cursor;

    .prologue
    .line 781
    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 393
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->getUserVisibleHint()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 394
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mAlbumArtParsingEnabled:Z

    .line 395
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->initializeList()V

    .line 399
    :goto_0
    iget-boolean v0, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mAlbumArtParsingEnabled:Z

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->setAlbumArtParsingEnabled(Z)V

    .line 401
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget v1, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mList:I

    invoke-static {v0, v1}, Lcom/samsung/musicplus/util/ListUtils;->isSupportSplitSub(Landroid/content/Context;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 402
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mAdapter:Landroid/widget/CursorAdapter;

    instance-of v0, v0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;

    if-eqz v0, :cond_0

    .line 403
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mAdapter:Landroid/widget/CursorAdapter;

    check-cast v0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;

    iget-object v1, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mSelectedPosionKeyword:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->setSelectedKeyword(Ljava/lang/String;)V

    .line 407
    :cond_0
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 408
    return-void

    .line 397
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mUiHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 343
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->onAttach(Landroid/app/Activity;)V

    .line 344
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 354
    invoke-static {}, Lcom/samsung/musicplus/util/AlbumArtLoader;->getAlbumArtLoader()Lcom/samsung/musicplus/util/IAlbumArtLoader;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mAlbumArtLoader:Lcom/samsung/musicplus/util/IAlbumArtLoader;

    .line 355
    if-eqz p1, :cond_0

    .line 356
    const-string v1, "list"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mList:I

    .line 357
    const-string v1, "key"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mKey:Ljava/lang/String;

    .line 358
    const-string v1, "album_art_parsing"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mAlbumArtParsingEnabled:Z

    .line 367
    :goto_0
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 368
    return-void

    .line 361
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 362
    .local v0, "data":Landroid/os/Bundle;
    const-string v1, "list"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mList:I

    .line 363
    const-string v1, "key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mKey:Ljava/lang/String;

    .line 364
    const-string v1, "album_art_parsing"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mAlbumArtParsingEnabled:Z

    goto :goto_0
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/content/Loader;
    .locals 7
    .param p1, "id"    # I
    .param p2, "args"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 163
    iget-object v1, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mQueryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    if-nez v1, :cond_0

    .line 164
    const-string v1, "MusicUiList"

    const-string v2, "mQueryArgs is null"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 165
    const/4 v0, 0x0

    .line 181
    :goto_0
    return-object v0

    .line 168
    :cond_0
    const-string v1, "MusicUiList"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "onCreateLoader : selection - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mQueryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    iget-object v3, v3, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->selection:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 169
    new-instance v0, Landroid/content/CursorLoader;

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mQueryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    iget-object v2, v2, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->uri:Landroid/net/Uri;

    iget-object v3, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mQueryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    iget-object v3, v3, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->projection:[Ljava/lang/String;

    iget-object v4, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mQueryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    iget-object v4, v4, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->selection:Ljava/lang/String;

    iget-object v5, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mQueryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    iget-object v5, v5, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->selectionArgs:[Ljava/lang/String;

    iget-object v6, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mQueryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    iget-object v6, v6, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->orderBy:Ljava/lang/String;

    invoke-direct/range {v0 .. v6}, Landroid/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    .local v0, "cl":Landroid/content/CursorLoader;
    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v2, v3}, Landroid/content/CursorLoader;->setUpdateThrottle(J)V

    .line 178
    const-string v1, "MusicUiList"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onCreateLoader : loader "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 375
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 628
    invoke-super {p0}, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->onDestroy()V

    .line 629
    return-void
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 618
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->detachIndexView()V

    .line 619
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mUiHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 620
    invoke-super {p0}, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->onDestroyView()V

    .line 621
    return-void
.end method

.method public onDetach()V
    .locals 0

    .prologue
    .line 636
    invoke-super {p0}, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->onDetach()V

    .line 637
    return-void
.end method

.method public onListItemClick(Lcom/sec/android/touchwiz/widget/TwAbsListView;Landroid/view/View;IJ)V
    .locals 0
    .param p1, "l"    # Lcom/sec/android/touchwiz/widget/TwAbsListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    .line 725
    return-void
.end method

.method public onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V
    .locals 5
    .param p2, "data"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 186
    .local p1, "loader":Landroid/content/Loader;, "Landroid/content/Loader<Landroid/database/Cursor;>;"
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 187
    .local v0, "a":Landroid/app/Activity;
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->getAbsListView()Lcom/sec/android/touchwiz/widget/TwAbsListView;

    move-result-object v1

    .line 188
    .local v1, "lv":Lcom/sec/android/touchwiz/widget/TwAbsListView;
    if-eqz v1, :cond_0

    instance-of v2, v1, Lcom/sec/android/touchwiz/widget/TwListView;

    if-eqz v2, :cond_0

    .line 194
    check-cast v1, Lcom/sec/android/touchwiz/widget/TwListView;

    .end local v1    # "lv":Lcom/sec/android/touchwiz/widget/TwAbsListView;
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/sec/android/touchwiz/widget/TwListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 200
    :cond_0
    const-string v3, "MusicUiList"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "onLoadFinished : loader "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " data size : "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    if-nez p2, :cond_5

    const-string v2, "Cursor is null"

    :goto_0
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    iget v2, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mList:I

    invoke-virtual {p1}, Landroid/content/Loader;->getId()I

    move-result v3

    if-ne v2, v3, :cond_3

    .line 205
    invoke-virtual {p0, p2}, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->setColumnIndicesAndSetIndex(Landroid/database/Cursor;)V

    .line 206
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->setListShown(Z)V

    .line 207
    iget-object v2, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mAdapter:Landroid/widget/CursorAdapter;

    if-eqz v2, :cond_2

    .line 208
    invoke-virtual {p0, p2}, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->needEmptyView(Landroid/database/Cursor;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 209
    iget v2, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mNoItemTextId:I

    invoke-virtual {p0, v2}, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->setEmptyView(I)V

    .line 211
    :cond_1
    iget-object v2, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mAdapter:Landroid/widget/CursorAdapter;

    invoke-virtual {p0, p2}, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->addOtherView(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/CursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 212
    iget-object v2, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mAdapter:Landroid/widget/CursorAdapter;

    instance-of v2, v2, Lcom/samsung/musicplus/widget/list/TrackListAdapter;

    if-eqz v2, :cond_2

    .line 213
    instance-of v2, v0, Lcom/samsung/musicplus/MusicBaseActivity;

    if-eqz v2, :cond_2

    .line 214
    invoke-virtual {v0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    .line 218
    :cond_2
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->setEmptyListState()V

    .line 220
    :cond_3
    iget v2, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mList:I

    invoke-static {v2}, Lcom/samsung/musicplus/util/ListUtils;->isTab(I)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 221
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->updateNumberView()V

    .line 235
    :cond_4
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentManager;->invalidateOptionsMenu()V

    .line 236
    return-void

    .line 200
    :cond_5
    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto :goto_0
.end method

.method public bridge synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Landroid/content/Loader;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 75
    check-cast p2, Landroid/database/Cursor;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/content/Loader;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 248
    .local p1, "loader":Landroid/content/Loader;, "Landroid/content/Loader<Landroid/database/Cursor;>;"
    const-string v0, "MusicUiList"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "onLoaderReset : loader "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 253
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mAdapter:Landroid/widget/CursorAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 255
    return-void
.end method

.method public onMultiWindowModeChanged(Z)V
    .locals 0
    .param p1, "isMultiWindow"    # Z

    .prologue
    .line 929
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 595
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mIsPause:Z

    .line 596
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->getAbsListView()Lcom/sec/android/touchwiz/widget/TwAbsListView;

    move-result-object v0

    .line 597
    .local v0, "lv":Lcom/sec/android/touchwiz/widget/TwAbsListView;
    if-eqz v0, :cond_0

    .line 598
    invoke-virtual {v0}, Lcom/sec/android/touchwiz/widget/TwAbsListView;->getFirstVisiblePosition()I

    move-result v1

    iput v1, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mListPosition:I

    .line 601
    :cond_0
    invoke-super {p0}, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->onPause()V

    .line 602
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 585
    invoke-super {p0}, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->onResume()V

    .line 586
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mIsPause:Z

    .line 588
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 573
    const-string v0, "list"

    iget v1, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mList:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 574
    const-string v0, "key"

    iget-object v1, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mKey:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 575
    const-string v0, "album_art_parsing"

    iget-boolean v1, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mAlbumArtParsingEnabled:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 577
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 578
    return-void
.end method

.method public onServiceConnected()V
    .locals 0

    .prologue
    .line 925
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 541
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mIsStop:Z

    .line 545
    invoke-super {p0}, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->onStart()V

    .line 546
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 606
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mIsStop:Z

    .line 610
    invoke-super {p0}, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->onStop()V

    .line 611
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 385
    invoke-super {p0, p1, p2}, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 386
    return-void
.end method

.method protected prepareContextMenu(Landroid/view/Menu;JI)V
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "itemId"    # J
    .param p4, "position"    # I

    .prologue
    .line 679
    iget v0, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mList:I

    invoke-static {v0}, Lcom/samsung/musicplus/util/ListUtils;->isTab(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 684
    :cond_0
    return-void
.end method

.method protected prepareOptionMenu(Landroid/view/Menu;)V
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 661
    iget v0, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mList:I

    invoke-static {v0}, Lcom/samsung/musicplus/util/ListUtils;->isTab(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 662
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->prepareViewOption(Landroid/view/Menu;)V

    .line 666
    :cond_0
    return-void
.end method

.method protected prepareViewOption(Landroid/view/Menu;)V
    .locals 0
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 651
    return-void
.end method

.method public receivePlayerState(Ljava/lang/String;)V
    .locals 1
    .param p1, "action"    # Ljava/lang/String;

    .prologue
    .line 787
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mSplitSubFragment:Landroid/app/Fragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mSplitSubFragment:Landroid/app/Fragment;

    instance-of v0, v0, Lcom/samsung/musicplus/widget/fragment/OnMusicFragmentInterface;

    if-eqz v0, :cond_0

    .line 788
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mSplitSubFragment:Landroid/app/Fragment;

    check-cast v0, Lcom/samsung/musicplus/widget/fragment/OnMusicFragmentInterface;

    invoke-interface {v0, p1}, Lcom/samsung/musicplus/widget/fragment/OnMusicFragmentInterface;->receivePlayerState(Ljava/lang/String;)V

    .line 790
    :cond_0
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->receivePlayerState(Ljava/lang/String;)V

    .line 791
    return-void
.end method

.method protected setAdapterLayout(I)V
    .locals 0
    .param p1, "layout"    # I

    .prologue
    .line 646
    iput p1, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mAdapterLayout:I

    .line 647
    return-void
.end method

.method protected setAlbumArtParsingEnabled(Z)V
    .locals 1
    .param p1, "albumArtParsingEnabled"    # Z

    .prologue
    .line 428
    iput-boolean p1, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mAlbumArtParsingEnabled:Z

    .line 429
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mAdapter:Landroid/widget/CursorAdapter;

    instance-of v0, v0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;

    if-eqz v0, :cond_0

    .line 430
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mAdapter:Landroid/widget/CursorAdapter;

    check-cast v0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->setAlbumArtParsingEnabled(Z)V

    .line 432
    :cond_0
    return-void
.end method

.method protected setColumnIndicesAndSetIndex(Landroid/database/Cursor;)V
    .locals 10
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 309
    if-eqz p1, :cond_2

    .line 310
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mQueryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    iget-object v0, v0, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->audioIdCol:Ljava/lang/String;

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mAudioIdIdx:I

    .line 311
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mQueryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    iget-object v0, v0, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->keyWord:Ljava/lang/String;

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mKeyWordIdx:I

    .line 312
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mQueryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    iget-object v0, v0, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->albumIdCol:Ljava/lang/String;

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mAlbumIdIdx:I

    .line 313
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mQueryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    iget-object v0, v0, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->text1Col:Ljava/lang/String;

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mText1Idx:I

    .line 314
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mQueryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    iget-object v0, v0, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->text2Col:Ljava/lang/String;

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mText2Idx:I

    .line 315
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mQueryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    iget-object v0, v0, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->text3Col:Ljava/lang/String;

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mText3Idx:I

    .line 316
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mQueryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    iget-object v0, v0, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->samplingRateCol:Ljava/lang/String;

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mSamplingRateIdx:I

    .line 317
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mQueryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    iget-object v0, v0, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->durationCol:Ljava/lang/String;

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mDurationIdx:I

    .line 318
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mQueryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    iget-object v0, v0, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->bitDepthCol:Ljava/lang/String;

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mBitDepthIdx:I

    .line 319
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mQueryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    iget-object v0, v0, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->secretBoxCol:Ljava/lang/String;

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mSecretBoxIdx:I

    .line 321
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mQueryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    iget-object v0, v0, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->indexBy:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 322
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mQueryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    iget-object v0, v0, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->indexBy:Ljava/lang/String;

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mIndexIdx:I

    .line 325
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mAdapter:Landroid/widget/CursorAdapter;

    instance-of v0, v0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;

    if-eqz v0, :cond_1

    .line 326
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mAdapter:Landroid/widget/CursorAdapter;

    check-cast v0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;

    iget v1, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mAudioIdIdx:I

    iget v2, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mText1Idx:I

    iget v3, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mText2Idx:I

    iget v4, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mText3Idx:I

    iget v5, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mAlbumIdIdx:I

    iget v6, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mKeyWordIdx:I

    iget v7, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mSamplingRateIdx:I

    iget v8, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mBitDepthIdx:I

    iget v9, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mSecretBoxIdx:I

    invoke-virtual/range {v0 .. v9}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->setIndex(IIIIIIIII)V

    .line 329
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mAdapter:Landroid/widget/CursorAdapter;

    check-cast v0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->getViewType()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->setViewType(I)V

    .line 331
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mAdapter:Landroid/widget/CursorAdapter;

    instance-of v0, v0, Lcom/samsung/musicplus/widget/list/TrackListAdapter;

    if-eqz v0, :cond_2

    .line 332
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mAdapter:Landroid/widget/CursorAdapter;

    check-cast v0, Lcom/samsung/musicplus/widget/list/TrackListAdapter;

    iget v1, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mNumberTextId:I

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->setNumberString(I)V

    .line 333
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mAdapter:Landroid/widget/CursorAdapter;

    check-cast v0, Lcom/samsung/musicplus/widget/list/TrackListAdapter;

    iget v1, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mDurationIdx:I

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->setDurationIndex(I)V

    .line 336
    :cond_2
    return-void
.end method

.method public setHasOptionsMenu(Z)V
    .locals 1
    .param p1, "hasMenu"    # Z

    .prologue
    .line 729
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mSplitSubFragment:Landroid/app/Fragment;

    if-eqz v0, :cond_1

    .line 730
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mSplitSubFragment:Landroid/app/Fragment;

    invoke-virtual {v0, p1}, Landroid/app/Fragment;->setHasOptionsMenu(Z)V

    .line 736
    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-super {p0, v0}, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->setHasOptionsMenu(Z)V

    .line 740
    :goto_1
    return-void

    .line 736
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 738
    :cond_1
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->setHasOptionsMenu(Z)V

    goto :goto_1
.end method

.method protected setNumberOfTextView(Landroid/widget/TextView;Landroid/content/Context;I)V
    .locals 6
    .param p1, "text"    # Landroid/widget/TextView;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "count"    # I

    .prologue
    .line 870
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget v2, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mNumberTextId:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, p3, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 871
    .local v0, "number":Ljava/lang/String;
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 872
    return-void
.end method

.method protected setNumberView(Landroid/widget/TextView;)V
    .locals 0
    .param p1, "v"    # Landroid/widget/TextView;

    .prologue
    .line 440
    iput-object p1, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mNumberView:Landroid/widget/TextView;

    .line 441
    return-void
.end method

.method public setUserVisibleHint(Z)V
    .locals 1
    .param p1, "isVisibleToUser"    # Z

    .prologue
    .line 412
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/fragment/TwListFragment;->setUserVisibleHint(Z)V

    .line 413
    iget-boolean v0, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mAlbumArtParsingEnabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_1

    .line 417
    :cond_0
    :goto_0
    return-void

    .line 416
    :cond_1
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->invalidateListWithAlbumArtParsing()V

    goto :goto_0
.end method

.method protected updateListInfo()Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;
    .locals 3

    .prologue
    .line 480
    iget v1, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mList:I

    iget-object v2, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mKey:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/samsung/musicplus/util/ListUtils;->getMusicListInfo(ILjava/lang/String;)Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;

    move-result-object v0

    .line 481
    .local v0, "info":Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;
    iget-object v1, v0, Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;->queryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    iput-object v1, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mQueryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    .line 482
    iget v1, v0, Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;->numberOfTextId:I

    iput v1, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mNumberTextId:I

    .line 483
    iget v1, v0, Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;->noItemTextId:I

    iput v1, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mNoItemTextId:I

    .line 485
    return-object v0
.end method

.method protected updateNumberView()V
    .locals 3

    .prologue
    .line 447
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->getNumberOfItmes()I

    move-result v0

    .line 448
    .local v0, "count":I
    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->isShowNumberView(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 449
    iget-object v1, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mNumberView:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 450
    iget-object v1, p0, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->mNumberView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p0, v1, v2, v0}, Lcom/samsung/musicplus/widget/fragment/TwCommonListFragment;->setNumberOfTextView(Landroid/widget/TextView;Landroid/content/Context;I)V

    .line 453
    :cond_0
    return-void
.end method
