.class public Lcom/samsung/musicplus/widget/AnimatedToggleButton;
.super Landroid/widget/ImageView;
.source "AnimatedToggleButton.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/widget/AnimatedToggleButton$7;,
        Lcom/samsung/musicplus/widget/AnimatedToggleButton$Config;,
        Lcom/samsung/musicplus/widget/AnimatedToggleButton$ButtonState;,
        Lcom/samsung/musicplus/widget/AnimatedToggleButton$AnimationState;
    }
.end annotation


# static fields
.field private static final KEY_BMP_A:Ljava/lang/String; = "KEY_BMP_A"

.field private static final KEY_BMP_B:Ljava/lang/String; = "KEY_BMP_B"

.field private static final KEY_BMP_FOC_A:Ljava/lang/String; = "KEY_BMP_FOC_A"

.field private static final KEY_BMP_FOC_B:Ljava/lang/String; = "KEY_BMP_FOC_B"

.field private static final KEY_BMP_PRE_A:Ljava/lang/String; = "KEY_BMP_PRE_A"

.field private static final KEY_BMP_PRE_B:Ljava/lang/String; = "KEY_BMP_PRE_B"

.field private static final VI_CONCEPT_VER:I = 0x2


# instance fields
.field private mAnimationState:Lcom/samsung/musicplus/widget/AnimatedToggleButton$AnimationState;

.field private mBitmapA:Landroid/graphics/Bitmap;

.field private mBitmapAToDraw:Landroid/graphics/Bitmap;

.field private mBitmapB:Landroid/graphics/Bitmap;

.field private mBitmapBToDraw:Landroid/graphics/Bitmap;

.field private mButtonAnimation:Landroid/animation/ValueAnimator;

.field private mButtonAnimationTime:I

.field private mCircleRadius:F

.field private mCircleRadiusOrig:F

.field private mConfig:Lcom/samsung/musicplus/widget/AnimatedToggleButton$Config;

.field private mContext:Landroid/content/Context;

.field private volatile mCurButtonState:Lcom/samsung/musicplus/widget/AnimatedToggleButton$ButtonState;

.field private mDefaultPressedAnimation:Landroid/animation/ValueAnimator;

.field private mDisplayMetrics:Landroid/util/DisplayMetrics;

.field private mEdgeAnimation:Landroid/animation/ValueAnimator;

.field private mEdgeAnimationTime:I

.field private mEdgeBitmap:Landroid/graphics/Bitmap;

.field private mEdgeCanvas:Landroid/graphics/Canvas;

.field private mEdgeColor:I

.field private mEdgePaint:Landroid/graphics/Paint;

.field private mFocusedBitmapA:Landroid/graphics/Bitmap;

.field private mFocusedBitmapB:Landroid/graphics/Bitmap;

.field private mMaskBitmap:Landroid/graphics/Bitmap;

.field private mMaskCanvas:Landroid/graphics/Canvas;

.field private mMaskPaint:Landroid/graphics/Paint;

.field private mMaskPaintDstIn:Landroid/graphics/Paint;

.field private mMaskPaintDstOut:Landroid/graphics/Paint;

.field private mMaxEdgeOrig:F

.field private mMaxEdgeWidth:F

.field private mMaxRadius:F

.field private mNormalPaint:Landroid/graphics/Paint;

.field private mOriginalBitmaps:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private mPressedBitmapA:Landroid/graphics/Bitmap;

.field private mPressedBitmapB:Landroid/graphics/Bitmap;

.field private mTransitionBitmapA:Landroid/graphics/Bitmap;

.field private mTransitionBitmapB:Landroid/graphics/Bitmap;

.field private mTransitionCanvasA:Landroid/graphics/Canvas;

.field private mTransitionCanvasB:Landroid/graphics/Canvas;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 113
    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 76
    sget-object v0, Lcom/samsung/musicplus/widget/AnimatedToggleButton$AnimationState;->A:Lcom/samsung/musicplus/widget/AnimatedToggleButton$AnimationState;

    iput-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mAnimationState:Lcom/samsung/musicplus/widget/AnimatedToggleButton$AnimationState;

    .line 127
    sget-object v0, Lcom/samsung/musicplus/widget/AnimatedToggleButton$ButtonState;->RELEASED:Lcom/samsung/musicplus/widget/AnimatedToggleButton$ButtonState;

    iput-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mCurButtonState:Lcom/samsung/musicplus/widget/AnimatedToggleButton$ButtonState;

    .line 114
    iput-object p1, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mContext:Landroid/content/Context;

    .line 115
    new-instance v0, Lcom/samsung/musicplus/widget/AnimatedToggleButton$Config;

    invoke-direct {v0, p1, p2}, Lcom/samsung/musicplus/widget/AnimatedToggleButton$Config;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mConfig:Lcom/samsung/musicplus/widget/AnimatedToggleButton$Config;

    .line 116
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mIsDetachedFromWindow:Z

    .line 117
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/widget/AnimatedToggleButton;Landroid/view/MotionEvent;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/AnimatedToggleButton;
    .param p1, "x1"    # Landroid/view/MotionEvent;

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->isTouchDown(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/samsung/musicplus/widget/AnimatedToggleButton;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/AnimatedToggleButton;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->hasCustomPressedBitmaps()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1000(Lcom/samsung/musicplus/widget/AnimatedToggleButton;)Lcom/samsung/musicplus/widget/AnimatedToggleButton$AnimationState;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/AnimatedToggleButton;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mAnimationState:Lcom/samsung/musicplus/widget/AnimatedToggleButton$AnimationState;

    return-object v0
.end method

.method static synthetic access$1002(Lcom/samsung/musicplus/widget/AnimatedToggleButton;Lcom/samsung/musicplus/widget/AnimatedToggleButton$AnimationState;)Lcom/samsung/musicplus/widget/AnimatedToggleButton$AnimationState;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/AnimatedToggleButton;
    .param p1, "x1"    # Lcom/samsung/musicplus/widget/AnimatedToggleButton$AnimationState;

    .prologue
    .line 43
    iput-object p1, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mAnimationState:Lcom/samsung/musicplus/widget/AnimatedToggleButton$AnimationState;

    return-object p1
.end method

.method static synthetic access$1100(Lcom/samsung/musicplus/widget/AnimatedToggleButton;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/AnimatedToggleButton;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->initMask()V

    return-void
.end method

.method static synthetic access$1200(Lcom/samsung/musicplus/widget/AnimatedToggleButton;)F
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/AnimatedToggleButton;

    .prologue
    .line 43
    iget v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mMaxRadius:F

    return v0
.end method

.method static synthetic access$1300(Lcom/samsung/musicplus/widget/AnimatedToggleButton;F)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/AnimatedToggleButton;
    .param p1, "x1"    # F

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->updateMask(F)V

    return-void
.end method

.method static synthetic access$1400(Lcom/samsung/musicplus/widget/AnimatedToggleButton;)F
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/AnimatedToggleButton;

    .prologue
    .line 43
    iget v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mMaxEdgeWidth:F

    return v0
.end method

.method static synthetic access$200(Lcom/samsung/musicplus/widget/AnimatedToggleButton;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/AnimatedToggleButton;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->performPressWithCustomBitmap()V

    return-void
.end method

.method static synthetic access$300(Lcom/samsung/musicplus/widget/AnimatedToggleButton;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/AnimatedToggleButton;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->startDefaultPressedAnimation()V

    return-void
.end method

.method static synthetic access$400(Lcom/samsung/musicplus/widget/AnimatedToggleButton;Landroid/view/MotionEvent;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/AnimatedToggleButton;
    .param p1, "x1"    # Landroid/view/MotionEvent;

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->isTouchUp(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Lcom/samsung/musicplus/widget/AnimatedToggleButton;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/AnimatedToggleButton;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->performReleaseWithCustomBitmap()V

    return-void
.end method

.method static synthetic access$600(Lcom/samsung/musicplus/widget/AnimatedToggleButton;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/AnimatedToggleButton;

    .prologue
    .line 43
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->revertDefaultPressedAnimation()V

    return-void
.end method

.method static synthetic access$700(Lcom/samsung/musicplus/widget/AnimatedToggleButton;Landroid/view/KeyEvent;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/AnimatedToggleButton;
    .param p1, "x1"    # Landroid/view/KeyEvent;

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->isEnterDown(Landroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$800(Lcom/samsung/musicplus/widget/AnimatedToggleButton;Landroid/view/KeyEvent;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/AnimatedToggleButton;
    .param p1, "x1"    # Landroid/view/KeyEvent;

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->isEnterUp(Landroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$900(Lcom/samsung/musicplus/widget/AnimatedToggleButton;F)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/AnimatedToggleButton;
    .param p1, "x1"    # F

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->updateEdgeWidth(F)V

    return-void
.end method

.method private bitmapScalingNeeded(IILandroid/view/ViewGroup$LayoutParams;)Z
    .locals 3
    .param p1, "availableWidth"    # I
    .param p2, "availableHeight"    # I
    .param p3, "layoutParams"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    const/4 v0, 0x0

    .line 498
    if-lez p1, :cond_0

    if-gtz p2, :cond_1

    .line 501
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-direct {p0, p3}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->isWrapContent(Landroid/view/ViewGroup$LayoutParams;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-direct {p0, p3}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->isMatchParent(Landroid/view/ViewGroup$LayoutParams;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mBitmapA:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    iget v2, p3, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-ne v1, v2, :cond_3

    iget-object v1, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mBitmapA:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    iget v2, p3, Landroid/view/ViewGroup$LayoutParams;->width:I

    if-ne v1, v2, :cond_3

    :cond_2
    iget v1, p3, Landroid/view/ViewGroup$LayoutParams;->width:I

    if-lt p1, v1, :cond_3

    iget v1, p3, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-ge p2, v1, :cond_0

    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private changeAnimationState(Lcom/samsung/musicplus/widget/AnimatedToggleButton$AnimationState;)V
    .locals 1
    .param p1, "animationState"    # Lcom/samsung/musicplus/widget/AnimatedToggleButton$AnimationState;

    .prologue
    .line 408
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mButtonAnimation:Landroid/animation/ValueAnimator;

    if-nez v0, :cond_0

    .line 429
    :goto_0
    return-void

    .line 412
    :cond_0
    sget-object v0, Lcom/samsung/musicplus/widget/AnimatedToggleButton$AnimationState;->A:Lcom/samsung/musicplus/widget/AnimatedToggleButton$AnimationState;

    if-ne v0, p1, :cond_1

    .line 413
    sget-object v0, Lcom/samsung/musicplus/widget/AnimatedToggleButton$AnimationState;->AB:Lcom/samsung/musicplus/widget/AnimatedToggleButton$AnimationState;

    iput-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mAnimationState:Lcom/samsung/musicplus/widget/AnimatedToggleButton$AnimationState;

    .line 414
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mButtonAnimation:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 415
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->tryStartEdgeAnimation()V

    goto :goto_0

    .line 416
    :cond_1
    sget-object v0, Lcom/samsung/musicplus/widget/AnimatedToggleButton$AnimationState;->B:Lcom/samsung/musicplus/widget/AnimatedToggleButton$AnimationState;

    if-ne v0, p1, :cond_2

    .line 417
    sget-object v0, Lcom/samsung/musicplus/widget/AnimatedToggleButton$AnimationState;->BA:Lcom/samsung/musicplus/widget/AnimatedToggleButton$AnimationState;

    iput-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mAnimationState:Lcom/samsung/musicplus/widget/AnimatedToggleButton$AnimationState;

    .line 418
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mButtonAnimation:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->reverse()V

    .line 419
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->tryRevertEdgeAnimation()V

    goto :goto_0

    .line 420
    :cond_2
    sget-object v0, Lcom/samsung/musicplus/widget/AnimatedToggleButton$AnimationState;->AB:Lcom/samsung/musicplus/widget/AnimatedToggleButton$AnimationState;

    if-ne v0, p1, :cond_3

    .line 421
    sget-object v0, Lcom/samsung/musicplus/widget/AnimatedToggleButton$AnimationState;->BA:Lcom/samsung/musicplus/widget/AnimatedToggleButton$AnimationState;

    iput-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mAnimationState:Lcom/samsung/musicplus/widget/AnimatedToggleButton$AnimationState;

    .line 422
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mButtonAnimation:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->reverse()V

    .line 423
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->tryRevertEdgeAnimation()V

    goto :goto_0

    .line 425
    :cond_3
    sget-object v0, Lcom/samsung/musicplus/widget/AnimatedToggleButton$AnimationState;->AB:Lcom/samsung/musicplus/widget/AnimatedToggleButton$AnimationState;

    iput-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mAnimationState:Lcom/samsung/musicplus/widget/AnimatedToggleButton$AnimationState;

    .line 426
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mButtonAnimation:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->reverse()V

    .line 427
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->tryRevertEdgeAnimation()V

    goto :goto_0
.end method

.method private createBitmapCopies()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 203
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mOriginalBitmaps:Ljava/util/Map;

    if-nez v0, :cond_2

    .line 204
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mOriginalBitmaps:Ljava/util/Map;

    .line 208
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mBitmapA:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mBitmapB:Landroid/graphics/Bitmap;

    if-nez v0, :cond_3

    .line 209
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Bitmaps A or B not loaded"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 205
    :cond_2
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mOriginalBitmaps:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 206
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Original bitmaps cache is not empty on initialization!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 211
    :cond_3
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mOriginalBitmaps:Ljava/util/Map;

    const-string v1, "KEY_BMP_A"

    iget-object v2, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mBitmapA:Landroid/graphics/Bitmap;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 212
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mOriginalBitmaps:Ljava/util/Map;

    const-string v1, "KEY_BMP_B"

    iget-object v2, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mBitmapB:Landroid/graphics/Bitmap;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 213
    const-string v0, "KEY_BMP_FOC_A"

    iget-object v1, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mFocusedBitmapA:Landroid/graphics/Bitmap;

    invoke-direct {p0, v0, v1}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->tryAddBitmapCopy(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 214
    const-string v0, "KEY_BMP_FOC_B"

    iget-object v1, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mFocusedBitmapB:Landroid/graphics/Bitmap;

    invoke-direct {p0, v0, v1}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->tryAddBitmapCopy(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 215
    const-string v0, "KEY_BMP_PRE_A"

    iget-object v1, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mPressedBitmapA:Landroid/graphics/Bitmap;

    invoke-direct {p0, v0, v1}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->tryAddBitmapCopy(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 216
    const-string v0, "KEY_BMP_PRE_B"

    iget-object v1, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mPressedBitmapB:Landroid/graphics/Bitmap;

    invoke-direct {p0, v0, v1}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->tryAddBitmapCopy(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 217
    return-void
.end method

.method private drawA(Landroid/graphics/Canvas;)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 713
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mBitmapAToDraw:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mNormalPaint:Landroid/graphics/Paint;

    invoke-direct {p0, p1, v0, v1}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->drawBitmap(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/Paint;)V

    .line 714
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mEdgeBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mNormalPaint:Landroid/graphics/Paint;

    invoke-direct {p0, p1, v0, v1}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->drawBitmap(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/Paint;)V

    .line 715
    return-void
.end method

.method private drawAToB(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v1, 0x0

    .line 695
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mTransitionBitmapA:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 696
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mTransitionBitmapB:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 698
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mTransitionCanvasA:Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mBitmapAToDraw:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mNormalPaint:Landroid/graphics/Paint;

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->drawBitmap(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/Paint;)V

    .line 699
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mTransitionCanvasB:Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mBitmapBToDraw:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mNormalPaint:Landroid/graphics/Paint;

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->drawBitmap(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/Paint;)V

    .line 700
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mTransitionCanvasA:Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mMaskBitmap:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mMaskPaintDstOut:Landroid/graphics/Paint;

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->drawBitmap(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/Paint;)V

    .line 701
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mTransitionCanvasB:Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mMaskBitmap:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mMaskPaintDstIn:Landroid/graphics/Paint;

    invoke-direct {p0, v0, v1, v2}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->drawBitmap(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/Paint;)V

    .line 703
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mTransitionBitmapA:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mNormalPaint:Landroid/graphics/Paint;

    invoke-direct {p0, p1, v0, v1}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->drawBitmap(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/Paint;)V

    .line 704
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mTransitionBitmapB:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mNormalPaint:Landroid/graphics/Paint;

    invoke-direct {p0, p1, v0, v1}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->drawBitmap(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/Paint;)V

    .line 705
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mEdgeBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mNormalPaint:Landroid/graphics/Paint;

    invoke-direct {p0, p1, v0, v1}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->drawBitmap(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/Paint;)V

    .line 706
    return-void
.end method

.method private drawB(Landroid/graphics/Canvas;)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 718
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mBitmapBToDraw:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mNormalPaint:Landroid/graphics/Paint;

    invoke-direct {p0, p1, v0, v1}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->drawBitmap(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/Paint;)V

    .line 719
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mEdgeBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mNormalPaint:Landroid/graphics/Paint;

    invoke-direct {p0, p1, v0, v1}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->drawBitmap(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/Paint;)V

    .line 720
    return-void
.end method

.method private drawBToA(Landroid/graphics/Canvas;)V
    .locals 0
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 709
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->drawAToB(Landroid/graphics/Canvas;)V

    .line 710
    return-void
.end method

.method private drawBitmap(Landroid/graphics/Canvas;Landroid/graphics/Bitmap;Landroid/graphics/Paint;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "bmp"    # Landroid/graphics/Bitmap;
    .param p3, "paint"    # Landroid/graphics/Paint;

    .prologue
    .line 730
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->getWidth()I

    move-result v0

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    int-to-float v0, v0

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->getHeight()I

    move-result v1

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    invoke-virtual {p1, p2, v0, v1, p3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 732
    return-void
.end method

.method private getAntiAliasedPaint()Landroid/graphics/Paint;
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 661
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    .line 662
    .local v0, "paint":Landroid/graphics/Paint;
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 663
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 664
    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setDither(Z)V

    .line 665
    return-object v0
.end method

.method private getScaleRatio(IILandroid/view/ViewGroup$LayoutParams;)F
    .locals 4
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "layoutParams"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    .line 487
    int-to-float v2, p1

    iget v3, p3, Landroid/view/ViewGroup$LayoutParams;->width:I

    int-to-float v3, v3

    div-float v1, v2, v3

    .line 488
    .local v1, "widthRatio":F
    int-to-float v2, p2

    iget v3, p3, Landroid/view/ViewGroup$LayoutParams;->height:I

    int-to-float v3, v3

    div-float v0, v2, v3

    .line 489
    .local v0, "heightRatio":F
    cmpg-float v2, v1, v0

    if-gez v2, :cond_0

    .end local v1    # "widthRatio":F
    :goto_0
    return v1

    .restart local v1    # "widthRatio":F
    :cond_0
    move v1, v0

    goto :goto_0
.end method

.method private getScaledBitmap(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;
    .locals 2
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 543
    const/4 v1, 0x1

    invoke-static {p1, p2, p3, v1}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 544
    .local v0, "scaled":Landroid/graphics/Bitmap;
    return-object v0
.end method

.method private hasCustomFocusedBitmaps()Z
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 188
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mConfig:Lcom/samsung/musicplus/widget/AnimatedToggleButton$Config;

    invoke-virtual {v0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton$Config;->getFocusedImageA()I

    move-result v0

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mConfig:Lcom/samsung/musicplus/widget/AnimatedToggleButton$Config;

    invoke-virtual {v0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton$Config;->getFocusedImageA()I

    move-result v0

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private hasCustomPressedBitmaps()Z
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 192
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mConfig:Lcom/samsung/musicplus/widget/AnimatedToggleButton$Config;

    invoke-virtual {v0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton$Config;->getPressedImageA()I

    move-result v0

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mConfig:Lcom/samsung/musicplus/widget/AnimatedToggleButton$Config;

    invoke-virtual {v0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton$Config;->getPressedImageA()I

    move-result v0

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private initBitmaps()V
    .locals 2

    .prologue
    .line 169
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mConfig:Lcom/samsung/musicplus/widget/AnimatedToggleButton$Config;

    invoke-virtual {v1}, Lcom/samsung/musicplus/widget/AnimatedToggleButton$Config;->getImageA()I

    move-result v1

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mBitmapA:Landroid/graphics/Bitmap;

    .line 170
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mConfig:Lcom/samsung/musicplus/widget/AnimatedToggleButton$Config;

    invoke-virtual {v1}, Lcom/samsung/musicplus/widget/AnimatedToggleButton$Config;->getImageB()I

    move-result v1

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mBitmapB:Landroid/graphics/Bitmap;

    .line 171
    return-void
.end method

.method private initButtonAnimation()V
    .locals 4

    .prologue
    .line 556
    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mButtonAnimation:Landroid/animation/ValueAnimator;

    .line 557
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mButtonAnimation:Landroid/animation/ValueAnimator;

    iget v1, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mButtonAnimationTime:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 558
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mButtonAnimation:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/interpolator/SineInOut80;

    invoke-direct {v1}, Landroid/view/animation/interpolator/SineInOut80;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 559
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mButtonAnimation:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/samsung/musicplus/widget/AnimatedToggleButton$4;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton$4;-><init>(Lcom/samsung/musicplus/widget/AnimatedToggleButton;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 586
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mButtonAnimation:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/samsung/musicplus/widget/AnimatedToggleButton$5;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton$5;-><init>(Lcom/samsung/musicplus/widget/AnimatedToggleButton;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 593
    return-void

    .line 556
    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private initDefaultPressedAnimation()V
    .locals 4

    .prologue
    .line 382
    const/4 v0, 0x2

    new-array v0, v0, [F

    const/4 v1, 0x0

    const/4 v2, 0x0

    aput v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mMaxEdgeWidth:F

    aput v2, v0, v1

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mDefaultPressedAnimation:Landroid/animation/ValueAnimator;

    .line 383
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mDefaultPressedAnimation:Landroid/animation/ValueAnimator;

    iget v1, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mEdgeAnimationTime:I

    div-int/lit8 v1, v1, 0x2

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 384
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mDefaultPressedAnimation:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/interpolator/QuintOut80;

    invoke-direct {v1}, Landroid/view/animation/interpolator/QuintOut80;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 385
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mDefaultPressedAnimation:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/samsung/musicplus/widget/AnimatedToggleButton$3;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton$3;-><init>(Lcom/samsung/musicplus/widget/AnimatedToggleButton;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 392
    return-void
.end method

.method private initEdge()V
    .locals 6

    .prologue
    const/high16 v5, 0x3f000000    # 0.5f

    const/high16 v4, 0x40000000    # 2.0f

    .line 641
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mEdgeBitmap:Landroid/graphics/Bitmap;

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->tryRecycleBitmap(Landroid/graphics/Bitmap;)V

    .line 642
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mDisplayMetrics:Landroid/util/DisplayMetrics;

    iget v1, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mCircleRadius:F

    mul-float/2addr v1, v4

    iget v2, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mMaxEdgeWidth:F

    mul-float/2addr v2, v4

    add-float/2addr v1, v2

    add-float/2addr v1, v5

    float-to-int v1, v1

    iget v2, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mCircleRadius:F

    mul-float/2addr v2, v4

    iget v3, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mMaxEdgeWidth:F

    mul-float/2addr v3, v4

    add-float/2addr v2, v3

    add-float/2addr v2, v5

    float-to-int v2, v2

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(Landroid/util/DisplayMetrics;IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mEdgeBitmap:Landroid/graphics/Bitmap;

    .line 644
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->getAntiAliasedPaint()Landroid/graphics/Paint;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mEdgePaint:Landroid/graphics/Paint;

    .line 645
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mEdgePaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mEdgeColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 646
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mEdgePaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 647
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mEdgePaint:Landroid/graphics/Paint;

    const/16 v1, 0xff

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 648
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mEdgeBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mEdgeCanvas:Landroid/graphics/Canvas;

    .line 649
    return-void
.end method

.method private initEdgeAnimation()V
    .locals 4

    .prologue
    .line 596
    const/4 v0, 0x2

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mEdgeAnimation:Landroid/animation/ValueAnimator;

    .line 597
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mEdgeAnimation:Landroid/animation/ValueAnimator;

    iget v1, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mEdgeAnimationTime:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 598
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mEdgeAnimation:Landroid/animation/ValueAnimator;

    new-instance v1, Landroid/view/animation/interpolator/QuintOut80;

    invoke-direct {v1}, Landroid/view/animation/interpolator/QuintOut80;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 599
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mEdgeAnimation:Landroid/animation/ValueAnimator;

    new-instance v1, Lcom/samsung/musicplus/widget/AnimatedToggleButton$6;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton$6;-><init>(Lcom/samsung/musicplus/widget/AnimatedToggleButton;)V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 611
    return-void

    .line 596
    nop

    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method private initFocusedBitmaps()V
    .locals 2

    .prologue
    .line 174
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->hasCustomFocusedBitmaps()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 175
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mConfig:Lcom/samsung/musicplus/widget/AnimatedToggleButton$Config;

    invoke-virtual {v1}, Lcom/samsung/musicplus/widget/AnimatedToggleButton$Config;->getFocusedImageA()I

    move-result v1

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mFocusedBitmapA:Landroid/graphics/Bitmap;

    .line 176
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mConfig:Lcom/samsung/musicplus/widget/AnimatedToggleButton$Config;

    invoke-virtual {v1}, Lcom/samsung/musicplus/widget/AnimatedToggleButton$Config;->getFocusedImageB()I

    move-result v1

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mFocusedBitmapB:Landroid/graphics/Bitmap;

    .line 178
    :cond_0
    return-void
.end method

.method private initMask()V
    .locals 5

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    .line 623
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mMaskBitmap:Landroid/graphics/Bitmap;

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->tryRecycleBitmap(Landroid/graphics/Bitmap;)V

    .line 624
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mDisplayMetrics:Landroid/util/DisplayMetrics;

    iget-object v1, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mBitmapA:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    iget-object v2, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mBitmapA:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(Landroid/util/DisplayMetrics;IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mMaskBitmap:Landroid/graphics/Bitmap;

    .line 626
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->getAntiAliasedPaint()Landroid/graphics/Paint;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mMaskPaint:Landroid/graphics/Paint;

    .line 627
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mMaskPaint:Landroid/graphics/Paint;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 628
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mMaskPaint:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 629
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mMaskBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mMaskCanvas:Landroid/graphics/Canvas;

    .line 630
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mMaskCanvas:Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mMaskBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v4

    iget-object v2, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mMaskBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v4

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mMaskPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 632
    return-void
.end method

.method private initPaints()V
    .locals 3

    .prologue
    .line 548
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->getAntiAliasedPaint()Landroid/graphics/Paint;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mNormalPaint:Landroid/graphics/Paint;

    .line 549
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->getAntiAliasedPaint()Landroid/graphics/Paint;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mMaskPaintDstOut:Landroid/graphics/Paint;

    .line 550
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mMaskPaintDstOut:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->DST_OUT:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 551
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->getAntiAliasedPaint()Landroid/graphics/Paint;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mMaskPaintDstIn:Landroid/graphics/Paint;

    .line 552
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mMaskPaintDstIn:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->DST_IN:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    .line 553
    return-void
.end method

.method private initPressedBitmaps()V
    .locals 2

    .prologue
    .line 181
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->hasCustomPressedBitmaps()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 182
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mConfig:Lcom/samsung/musicplus/widget/AnimatedToggleButton$Config;

    invoke-virtual {v1}, Lcom/samsung/musicplus/widget/AnimatedToggleButton$Config;->getPressedImageA()I

    move-result v1

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mPressedBitmapA:Landroid/graphics/Bitmap;

    .line 183
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mConfig:Lcom/samsung/musicplus/widget/AnimatedToggleButton$Config;

    invoke-virtual {v1}, Lcom/samsung/musicplus/widget/AnimatedToggleButton$Config;->getPressedImageB()I

    move-result v1

    invoke-static {v0, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mPressedBitmapB:Landroid/graphics/Bitmap;

    .line 185
    :cond_0
    return-void
.end method

.method private initTouchAndKeyListener()V
    .locals 1

    .prologue
    .line 319
    new-instance v0, Lcom/samsung/musicplus/widget/AnimatedToggleButton$1;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton$1;-><init>(Lcom/samsung/musicplus/widget/AnimatedToggleButton;)V

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 336
    new-instance v0, Lcom/samsung/musicplus/widget/AnimatedToggleButton$2;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton$2;-><init>(Lcom/samsung/musicplus/widget/AnimatedToggleButton;)V

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 351
    return-void
.end method

.method private initTransitionCanvas()V
    .locals 4

    .prologue
    .line 614
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mDisplayMetrics:Landroid/util/DisplayMetrics;

    iget-object v1, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mBitmapA:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    iget-object v2, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mBitmapA:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(Landroid/util/DisplayMetrics;IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mTransitionBitmapA:Landroid/graphics/Bitmap;

    .line 616
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mDisplayMetrics:Landroid/util/DisplayMetrics;

    iget-object v1, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mBitmapA:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    iget-object v2, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mBitmapA:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(Landroid/util/DisplayMetrics;IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mTransitionBitmapB:Landroid/graphics/Bitmap;

    .line 618
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mTransitionBitmapA:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mTransitionCanvasA:Landroid/graphics/Canvas;

    .line 619
    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mTransitionBitmapB:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mTransitionCanvasB:Landroid/graphics/Canvas;

    .line 620
    return-void
.end method

.method private initValuesFromConfig()V
    .locals 1

    .prologue
    .line 276
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mConfig:Lcom/samsung/musicplus/widget/AnimatedToggleButton$Config;

    invoke-virtual {v0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton$Config;->getEdgeSize()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mMaxEdgeWidth:F

    .line 277
    iget v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mMaxEdgeWidth:F

    iput v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mMaxEdgeOrig:F

    .line 278
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mConfig:Lcom/samsung/musicplus/widget/AnimatedToggleButton$Config;

    invoke-virtual {v0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton$Config;->getCircleRadius()I

    move-result v0

    int-to-float v0, v0

    iput v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mCircleRadius:F

    .line 279
    iget v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mCircleRadius:F

    iput v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mCircleRadiusOrig:F

    .line 280
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mConfig:Lcom/samsung/musicplus/widget/AnimatedToggleButton$Config;

    invoke-virtual {v0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton$Config;->getEdgeColor()I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mEdgeColor:I

    .line 281
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mConfig:Lcom/samsung/musicplus/widget/AnimatedToggleButton$Config;

    invoke-virtual {v0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton$Config;->getEdgeAnimationTime()I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mEdgeAnimationTime:I

    .line 282
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mConfig:Lcom/samsung/musicplus/widget/AnimatedToggleButton$Config;

    invoke-virtual {v0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton$Config;->getButtonAnimationTime()I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mButtonAnimationTime:I

    .line 283
    iget v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mCircleRadius:F

    iput v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mMaxRadius:F

    .line 284
    return-void
.end method

.method private initView()V
    .locals 2

    .prologue
    .line 254
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->initBitmaps()V

    .line 255
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mBitmapA:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mBitmapB:Landroid/graphics/Bitmap;

    invoke-direct {p0, v0, v1}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->setBitmapsToDraw(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V

    .line 256
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->initFocusedBitmaps()V

    .line 257
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->initPressedBitmaps()V

    .line 258
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->createBitmapCopies()V

    .line 259
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->initValuesFromConfig()V

    .line 260
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->initPaints()V

    .line 261
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->initMask()V

    .line 262
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->initEdge()V

    .line 263
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->initTransitionCanvas()V

    .line 264
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->initButtonAnimation()V

    .line 268
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->initDefaultPressedAnimation()V

    .line 270
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->initTouchAndKeyListener()V

    .line 271
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->hasFocus()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->setFocused(Z)V

    .line 272
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->setLayerType(ILandroid/graphics/Paint;)V

    .line 273
    return-void
.end method

.method private isEnterDown(Landroid/view/KeyEvent;)Z
    .locals 2
    .param p1, "keyEvent"    # Landroid/view/KeyEvent;

    .prologue
    .line 362
    const/16 v0, 0x42

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isEnterUp(Landroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "keyEvent"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v0, 0x1

    .line 367
    const/16 v1, 0x42

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    if-ne v1, v2, :cond_0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-ne v0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isMatchParent(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 2
    .param p1, "params"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    const/4 v1, -0x1

    .line 537
    iget v0, p1, Landroid/view/ViewGroup$LayoutParams;->width:I

    if-eq v0, v1, :cond_0

    iget v0, p1, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isTouchDown(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "motionEvent"    # Landroid/view/MotionEvent;

    .prologue
    .line 354
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isTouchUp(Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "motionEvent"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v0, 0x1

    .line 358
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-ne v0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isWrapContent(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 2
    .param p1, "params"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    const/4 v1, -0x2

    .line 532
    iget v0, p1, Landroid/view/ViewGroup$LayoutParams;->width:I

    if-eq v0, v1, :cond_0

    iget v0, p1, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private performPressWithCustomBitmap()V
    .locals 2

    .prologue
    .line 372
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mPressedBitmapA:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mPressedBitmapB:Landroid/graphics/Bitmap;

    invoke-direct {p0, v0, v1}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->setBitmapsToDraw(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V

    .line 373
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->invalidate()V

    .line 374
    return-void
.end method

.method private performReleaseWithCustomBitmap()V
    .locals 2

    .prologue
    .line 377
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mBitmapA:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mBitmapB:Landroid/graphics/Bitmap;

    invoke-direct {p0, v0, v1}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->setBitmapsToDraw(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V

    .line 378
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->invalidate()V

    .line 379
    return-void
.end method

.method private restoreBitmapsFromCopies()V
    .locals 2

    .prologue
    .line 226
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mOriginalBitmaps:Ljava/util/Map;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mOriginalBitmaps:Ljava/util/Map;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mOriginalBitmaps:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 227
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Original bitmaps cache is null or empty!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 229
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mOriginalBitmaps:Ljava/util/Map;

    const-string v1, "KEY_BMP_A"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mBitmapA:Landroid/graphics/Bitmap;

    .line 230
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mOriginalBitmaps:Ljava/util/Map;

    const-string v1, "KEY_BMP_B"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mBitmapB:Landroid/graphics/Bitmap;

    .line 231
    const-string v0, "KEY_BMP_FOC_A"

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->tryGetBitmapFromCopy(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mFocusedBitmapA:Landroid/graphics/Bitmap;

    .line 232
    const-string v0, "KEY_BMP_FOC_B"

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->tryGetBitmapFromCopy(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mFocusedBitmapB:Landroid/graphics/Bitmap;

    .line 233
    const-string v0, "KEY_BMP_PRE_A"

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->tryGetBitmapFromCopy(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mPressedBitmapA:Landroid/graphics/Bitmap;

    .line 234
    const-string v0, "KEY_BMP_PRE_B"

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->tryGetBitmapFromCopy(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mPressedBitmapB:Landroid/graphics/Bitmap;

    .line 235
    return-void
.end method

.method private revertDefaultPressedAnimation()V
    .locals 1

    .prologue
    .line 399
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mDefaultPressedAnimation:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->reverse()V

    .line 400
    return-void
.end method

.method private scaleBitmaps(II)V
    .locals 2
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 515
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->restoreBitmapsFromCopies()V

    .line 516
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mBitmapA:Landroid/graphics/Bitmap;

    invoke-direct {p0, v0, p1, p2}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->getScaledBitmap(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mBitmapA:Landroid/graphics/Bitmap;

    .line 517
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mBitmapB:Landroid/graphics/Bitmap;

    invoke-direct {p0, v0, p1, p2}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->getScaledBitmap(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mBitmapB:Landroid/graphics/Bitmap;

    .line 518
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mBitmapA:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mBitmapB:Landroid/graphics/Bitmap;

    invoke-direct {p0, v0, v1}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->setBitmapsToDraw(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V

    .line 519
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->hasCustomFocusedBitmaps()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 520
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mFocusedBitmapA:Landroid/graphics/Bitmap;

    invoke-direct {p0, v0, p1, p2}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->getScaledBitmap(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mFocusedBitmapA:Landroid/graphics/Bitmap;

    .line 521
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mFocusedBitmapB:Landroid/graphics/Bitmap;

    invoke-direct {p0, v0, p1, p2}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->getScaledBitmap(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mFocusedBitmapB:Landroid/graphics/Bitmap;

    .line 523
    :cond_0
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->hasCustomPressedBitmaps()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 524
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mPressedBitmapA:Landroid/graphics/Bitmap;

    invoke-direct {p0, v0, p1, p2}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->getScaledBitmap(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mPressedBitmapA:Landroid/graphics/Bitmap;

    .line 525
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mPressedBitmapB:Landroid/graphics/Bitmap;

    invoke-direct {p0, v0, p1, p2}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->getScaledBitmap(Landroid/graphics/Bitmap;II)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mPressedBitmapB:Landroid/graphics/Bitmap;

    .line 527
    :cond_1
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->initTransitionCanvas()V

    .line 528
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->hasFocus()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->setFocused(Z)V

    .line 529
    return-void
.end method

.method private scaleBitmaps(Landroid/view/ViewGroup$LayoutParams;)V
    .locals 2
    .param p1, "layoutParams"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    .line 511
    iget v0, p1, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget v1, p1, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-direct {p0, v0, v1}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->scaleBitmaps(II)V

    .line 512
    return-void
.end method

.method private scaleRadiusAndEdge(F)V
    .locals 1
    .param p1, "ratio"    # F

    .prologue
    .line 493
    iget v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mCircleRadiusOrig:F

    mul-float/2addr v0, p1

    iput v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mCircleRadius:F

    iput v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mMaxRadius:F

    .line 494
    iget v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mMaxEdgeOrig:F

    mul-float/2addr v0, p1

    iput v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mMaxEdgeWidth:F

    .line 495
    return-void
.end method

.method private setBitmapsToDraw(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "bitmapA"    # Landroid/graphics/Bitmap;
    .param p2, "bitmapB"    # Landroid/graphics/Bitmap;

    .prologue
    .line 403
    iput-object p1, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mBitmapAToDraw:Landroid/graphics/Bitmap;

    .line 404
    iput-object p2, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mBitmapBToDraw:Landroid/graphics/Bitmap;

    .line 405
    return-void
.end method

.method private setFocused(Z)V
    .locals 2
    .param p1, "focused"    # Z

    .prologue
    .line 307
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->hasCustomFocusedBitmaps()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    .line 308
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mFocusedBitmapA:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mFocusedBitmapB:Landroid/graphics/Bitmap;

    invoke-direct {p0, v0, v1}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->setBitmapsToDraw(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V

    .line 309
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->invalidate()V

    .line 311
    :cond_0
    return-void
.end method

.method private setNotFocused()V
    .locals 2

    .prologue
    .line 314
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mBitmapA:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mBitmapB:Landroid/graphics/Bitmap;

    invoke-direct {p0, v0, v1}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->setBitmapsToDraw(Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)V

    .line 315
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->invalidate()V

    .line 316
    return-void
.end method

.method private setStateInternal(Lcom/samsung/musicplus/widget/AnimatedToggleButton$ButtonState;Z)V
    .locals 2
    .param p1, "state"    # Lcom/samsung/musicplus/widget/AnimatedToggleButton$ButtonState;
    .param p2, "withAnimation"    # Z

    .prologue
    .line 150
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mCurButtonState:Lcom/samsung/musicplus/widget/AnimatedToggleButton$ButtonState;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/widget/AnimatedToggleButton$ButtonState;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 151
    iget-boolean v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mIsDetachedFromWindow:Z

    if-nez v0, :cond_0

    if-eqz p2, :cond_0

    .line 152
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mAnimationState:Lcom/samsung/musicplus/widget/AnimatedToggleButton$AnimationState;

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->changeAnimationState(Lcom/samsung/musicplus/widget/AnimatedToggleButton$AnimationState;)V

    .line 155
    :cond_0
    sget-object v0, Lcom/samsung/musicplus/widget/AnimatedToggleButton$ButtonState;->PRESSED:Lcom/samsung/musicplus/widget/AnimatedToggleButton$ButtonState;

    if-ne v0, p1, :cond_3

    .line 156
    sget-object v0, Lcom/samsung/musicplus/widget/AnimatedToggleButton$AnimationState;->A:Lcom/samsung/musicplus/widget/AnimatedToggleButton$AnimationState;

    iget-object v1, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mAnimationState:Lcom/samsung/musicplus/widget/AnimatedToggleButton$AnimationState;

    if-ne v0, v1, :cond_1

    .line 157
    sget-object v0, Lcom/samsung/musicplus/widget/AnimatedToggleButton$AnimationState;->B:Lcom/samsung/musicplus/widget/AnimatedToggleButton$AnimationState;

    iput-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mAnimationState:Lcom/samsung/musicplus/widget/AnimatedToggleButton$AnimationState;

    .line 164
    :cond_1
    :goto_0
    iput-object p1, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mCurButtonState:Lcom/samsung/musicplus/widget/AnimatedToggleButton$ButtonState;

    .line 166
    :cond_2
    return-void

    .line 160
    :cond_3
    sget-object v0, Lcom/samsung/musicplus/widget/AnimatedToggleButton$AnimationState;->B:Lcom/samsung/musicplus/widget/AnimatedToggleButton$AnimationState;

    iget-object v1, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mAnimationState:Lcom/samsung/musicplus/widget/AnimatedToggleButton$AnimationState;

    if-ne v0, v1, :cond_1

    .line 161
    sget-object v0, Lcom/samsung/musicplus/widget/AnimatedToggleButton$AnimationState;->A:Lcom/samsung/musicplus/widget/AnimatedToggleButton$AnimationState;

    iput-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mAnimationState:Lcom/samsung/musicplus/widget/AnimatedToggleButton$AnimationState;

    goto :goto_0
.end method

.method private startDefaultPressedAnimation()V
    .locals 1

    .prologue
    .line 395
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mDefaultPressedAnimation:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 396
    return-void
.end method

.method private tryAddBitmapCopy(Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 220
    if-eqz p2, :cond_0

    .line 221
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mOriginalBitmaps:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 223
    :cond_0
    return-void
.end method

.method private tryCancelAnimation(Landroid/animation/ValueAnimator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/ValueAnimator;

    .prologue
    .line 778
    if-eqz p1, :cond_0

    .line 779
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->cancel()V

    .line 781
    :cond_0
    return-void
.end method

.method private tryGetBitmapFromCopy(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 238
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mOriginalBitmaps:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 239
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mOriginalBitmaps:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 241
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private tryRecycleBitmap(Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 784
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    .line 785
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    .line 787
    :cond_0
    return-void
.end method

.method private tryRevertEdgeAnimation()V
    .locals 1

    .prologue
    .line 438
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mEdgeAnimation:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    .line 439
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mEdgeAnimation:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->reverse()V

    .line 441
    :cond_0
    return-void
.end method

.method private tryStartEdgeAnimation()V
    .locals 1

    .prologue
    .line 432
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mEdgeAnimation:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    .line 433
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mEdgeAnimation:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 435
    :cond_0
    return-void
.end method

.method private updateEdgeWidth(F)V
    .locals 5
    .param p1, "edgeWidth"    # F

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 652
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mEdgeBitmap:Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 653
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mEdgePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 654
    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-lez v0, :cond_0

    .line 655
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mEdgeCanvas:Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mEdgeBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v3

    iget-object v2, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mEdgeBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    iget v3, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mCircleRadius:F

    iget-object v4, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mEdgePaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 658
    :cond_0
    return-void
.end method

.method private updateMask(F)V
    .locals 4
    .param p1, "radius"    # F

    .prologue
    const/high16 v3, 0x40000000    # 2.0f

    .line 635
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mMaskBitmap:Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Bitmap;->eraseColor(I)V

    .line 636
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mMaskCanvas:Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mMaskBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v1, v3

    iget-object v2, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mMaskBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    div-float/2addr v2, v3

    iget-object v3, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mMaskPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v1, v2, p1, v3}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 638
    return-void
.end method


# virtual methods
.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 248
    invoke-super {p0}, Landroid/widget/ImageView;->onAttachedToWindow()V

    .line 249
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mDisplayMetrics:Landroid/util/DisplayMetrics;

    .line 250
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->initView()V

    .line 251
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 755
    iget-object v2, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mBitmapA:Landroid/graphics/Bitmap;

    invoke-direct {p0, v2}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->tryRecycleBitmap(Landroid/graphics/Bitmap;)V

    .line 756
    iget-object v2, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mBitmapB:Landroid/graphics/Bitmap;

    invoke-direct {p0, v2}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->tryRecycleBitmap(Landroid/graphics/Bitmap;)V

    .line 757
    iget-object v2, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mFocusedBitmapA:Landroid/graphics/Bitmap;

    invoke-direct {p0, v2}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->tryRecycleBitmap(Landroid/graphics/Bitmap;)V

    .line 758
    iget-object v2, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mFocusedBitmapB:Landroid/graphics/Bitmap;

    invoke-direct {p0, v2}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->tryRecycleBitmap(Landroid/graphics/Bitmap;)V

    .line 759
    iget-object v2, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mEdgeBitmap:Landroid/graphics/Bitmap;

    invoke-direct {p0, v2}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->tryRecycleBitmap(Landroid/graphics/Bitmap;)V

    .line 760
    iget-object v2, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mMaskBitmap:Landroid/graphics/Bitmap;

    invoke-direct {p0, v2}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->tryRecycleBitmap(Landroid/graphics/Bitmap;)V

    .line 761
    iget-object v2, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mTransitionBitmapA:Landroid/graphics/Bitmap;

    invoke-direct {p0, v2}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->tryRecycleBitmap(Landroid/graphics/Bitmap;)V

    .line 762
    iget-object v2, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mTransitionBitmapB:Landroid/graphics/Bitmap;

    invoke-direct {p0, v2}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->tryRecycleBitmap(Landroid/graphics/Bitmap;)V

    .line 763
    iget-object v2, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mOriginalBitmaps:Ljava/util/Map;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mOriginalBitmaps:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 764
    iget-object v2, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mOriginalBitmaps:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 765
    .local v0, "bmp":Landroid/graphics/Bitmap;
    invoke-direct {p0, v0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->tryRecycleBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 767
    .end local v0    # "bmp":Landroid/graphics/Bitmap;
    :cond_0
    iget-object v2, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mOriginalBitmaps:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->clear()V

    .line 769
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_1
    iput-object v3, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mBitmapAToDraw:Landroid/graphics/Bitmap;

    .line 770
    iput-object v3, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mBitmapBToDraw:Landroid/graphics/Bitmap;

    .line 771
    iget-object v2, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mButtonAnimation:Landroid/animation/ValueAnimator;

    invoke-direct {p0, v2}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->tryCancelAnimation(Landroid/animation/ValueAnimator;)V

    .line 772
    iget-object v2, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mEdgeAnimation:Landroid/animation/ValueAnimator;

    invoke-direct {p0, v2}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->tryCancelAnimation(Landroid/animation/ValueAnimator;)V

    .line 773
    iget-object v2, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mDefaultPressedAnimation:Landroid/animation/ValueAnimator;

    invoke-direct {p0, v2}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->tryCancelAnimation(Landroid/animation/ValueAnimator;)V

    .line 774
    invoke-super {p0}, Landroid/widget/ImageView;->onDetachedFromWindow()V

    .line 775
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 670
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->getImageMatrix()Landroid/graphics/Matrix;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    .line 671
    sget-object v0, Lcom/samsung/musicplus/widget/AnimatedToggleButton$7;->$SwitchMap$com$samsung$musicplus$widget$AnimatedToggleButton$AnimationState:[I

    iget-object v1, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mAnimationState:Lcom/samsung/musicplus/widget/AnimatedToggleButton$AnimationState;

    invoke-virtual {v1}, Lcom/samsung/musicplus/widget/AnimatedToggleButton$AnimationState;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 687
    :goto_0
    return-void

    .line 673
    :pswitch_0
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->drawAToB(Landroid/graphics/Canvas;)V

    goto :goto_0

    .line 676
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->drawBToA(Landroid/graphics/Canvas;)V

    goto :goto_0

    .line 679
    :pswitch_2
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->drawA(Landroid/graphics/Canvas;)V

    goto :goto_0

    .line 682
    :pswitch_3
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->drawB(Landroid/graphics/Canvas;)V

    goto :goto_0

    .line 671
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected onFocusChanged(ZILandroid/graphics/Rect;)V
    .locals 1
    .param p1, "gainFocus"    # Z
    .param p2, "direction"    # I
    .param p3, "previouslyFocusedRect"    # Landroid/graphics/Rect;

    .prologue
    .line 295
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->hasCustomFocusedBitmaps()Z

    move-result v0

    if-nez v0, :cond_0

    .line 296
    invoke-super {p0, p1, p2, p3}, Landroid/widget/ImageView;->onFocusChanged(ZILandroid/graphics/Rect;)V

    .line 304
    :goto_0
    return-void

    .line 298
    :cond_0
    if-eqz p1, :cond_1

    .line 299
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->hasFocus()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->setFocused(Z)V

    goto :goto_0

    .line 301
    :cond_1
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->setNotFocused()V

    goto :goto_0
.end method

.method protected onMeasure(II)V
    .locals 11
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/high16 v10, -0x80000000

    .line 452
    invoke-super {p0, p1, p2}, Landroid/widget/ImageView;->onMeasure(II)V

    .line 454
    iget-object v9, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mContext:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v5

    .line 455
    .local v5, "metrics":Landroid/util/DisplayMetrics;
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    .line 457
    .local v4, "layoutParams":Landroid/view/ViewGroup$LayoutParams;
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    .line 458
    .local v1, "availableWidth":I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 459
    .local v0, "availableHeight":I
    invoke-direct {p0, v1, v0, v4}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->bitmapScalingNeeded(IILandroid/view/ViewGroup$LayoutParams;)Z

    move-result v9

    if-eqz v9, :cond_2

    .line 460
    iget v9, v4, Landroid/view/ViewGroup$LayoutParams;->width:I

    if-lt v1, v9, :cond_0

    iget v9, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-ge v0, v9, :cond_1

    .line 461
    :cond_0
    invoke-direct {p0, v1, v0, v4}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->getScaleRatio(IILandroid/view/ViewGroup$LayoutParams;)F

    move-result v6

    .line 462
    .local v6, "ratio":F
    iget v9, v4, Landroid/view/ViewGroup$LayoutParams;->width:I

    int-to-float v9, v9

    mul-float/2addr v9, v6

    float-to-int v8, v9

    .line 463
    .local v8, "scaledWidth":I
    iget v9, v4, Landroid/view/ViewGroup$LayoutParams;->height:I

    int-to-float v9, v9

    mul-float/2addr v9, v6

    float-to-int v7, v9

    .line 464
    .local v7, "scaledHeight":I
    invoke-direct {p0, v8, v7}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->scaleBitmaps(II)V

    .line 465
    invoke-static {v8, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v9

    invoke-static {v7, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v10

    invoke-virtual {p0, v9, v10}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->setMeasuredDimension(II)V

    .line 467
    invoke-direct {p0, v6}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->scaleRadiusAndEdge(F)V

    .line 484
    .end local v6    # "ratio":F
    .end local v7    # "scaledHeight":I
    .end local v8    # "scaledWidth":I
    :goto_0
    return-void

    .line 470
    :cond_1
    invoke-direct {p0, v4}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->scaleBitmaps(Landroid/view/ViewGroup$LayoutParams;)V

    .line 471
    iget v9, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mCircleRadiusOrig:F

    iput v9, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mCircleRadius:F

    .line 472
    iget v9, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mMaxEdgeOrig:F

    iput v9, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mMaxEdgeWidth:F

    .line 473
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->initEdge()V

    .line 474
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->initMask()V

    .line 478
    :cond_2
    iget-object v9, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mBitmapA:Landroid/graphics/Bitmap;

    invoke-virtual {v9, v5}, Landroid/graphics/Bitmap;->getScaledWidth(Landroid/util/DisplayMetrics;)I

    move-result v9

    invoke-static {v9, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    .line 480
    .local v3, "desiredWSpec":I
    iget-object v9, p0, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->mBitmapB:Landroid/graphics/Bitmap;

    invoke-virtual {v9, v5}, Landroid/graphics/Bitmap;->getScaledHeight(Landroid/util/DisplayMetrics;)I

    move-result v9

    invoke-static {v9, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 483
    .local v2, "desiredHSpec":I
    invoke-virtual {p0, v3, v2}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->setMeasuredDimension(II)V

    goto :goto_0
.end method

.method public setImageBitmap(Landroid/graphics/Bitmap;)V
    .locals 0
    .param p1, "bm"    # Landroid/graphics/Bitmap;

    .prologue
    .line 744
    return-void
.end method

.method public setImageDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 740
    return-void
.end method

.method public setImageResource(I)V
    .locals 0
    .param p1, "resId"    # I

    .prologue
    .line 736
    return-void
.end method

.method public setImageURI(Landroid/net/Uri;)V
    .locals 0
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 748
    return-void
.end method

.method public declared-synchronized setState(Lcom/samsung/musicplus/widget/AnimatedToggleButton$ButtonState;)V
    .locals 1
    .param p1, "state"    # Lcom/samsung/musicplus/widget/AnimatedToggleButton$ButtonState;

    .prologue
    .line 136
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    invoke-direct {p0, p1, v0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->setStateInternal(Lcom/samsung/musicplus/widget/AnimatedToggleButton$ButtonState;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 137
    monitor-exit p0

    return-void

    .line 136
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized setStateWithoutAnimation(Lcom/samsung/musicplus/widget/AnimatedToggleButton$ButtonState;)V
    .locals 1
    .param p1, "state"    # Lcom/samsung/musicplus/widget/AnimatedToggleButton$ButtonState;

    .prologue
    .line 146
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-direct {p0, p1, v0}, Lcom/samsung/musicplus/widget/AnimatedToggleButton;->setStateInternal(Lcom/samsung/musicplus/widget/AnimatedToggleButton$ButtonState;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 147
    monitor-exit p0

    return-void

    .line 146
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
