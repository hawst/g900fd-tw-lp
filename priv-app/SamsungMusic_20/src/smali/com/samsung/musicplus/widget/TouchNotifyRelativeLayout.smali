.class public Lcom/samsung/musicplus/widget/TouchNotifyRelativeLayout;
.super Landroid/widget/RelativeLayout;
.source "TouchNotifyRelativeLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/widget/TouchNotifyRelativeLayout$OnTouchNotifyListener;
    }
.end annotation


# instance fields
.field private mTouchNotifyListener:Lcom/samsung/musicplus/widget/TouchNotifyRelativeLayout$OnTouchNotifyListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 29
    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 21
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 22
    return-void
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/samsung/musicplus/widget/TouchNotifyRelativeLayout;->mTouchNotifyListener:Lcom/samsung/musicplus/widget/TouchNotifyRelativeLayout$OnTouchNotifyListener;

    if-eqz v0, :cond_0

    .line 43
    iget-object v0, p0, Lcom/samsung/musicplus/widget/TouchNotifyRelativeLayout;->mTouchNotifyListener:Lcom/samsung/musicplus/widget/TouchNotifyRelativeLayout$OnTouchNotifyListener;

    invoke-interface {v0, p0, p1}, Lcom/samsung/musicplus/widget/TouchNotifyRelativeLayout$OnTouchNotifyListener;->onTouchNotify(Landroid/view/View;Landroid/view/MotionEvent;)V

    .line 45
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public setOnTouchNotifyListener(Lcom/samsung/musicplus/widget/TouchNotifyRelativeLayout$OnTouchNotifyListener;)V
    .locals 0
    .param p1, "l"    # Lcom/samsung/musicplus/widget/TouchNotifyRelativeLayout$OnTouchNotifyListener;

    .prologue
    .line 37
    iput-object p1, p0, Lcom/samsung/musicplus/widget/TouchNotifyRelativeLayout;->mTouchNotifyListener:Lcom/samsung/musicplus/widget/TouchNotifyRelativeLayout$OnTouchNotifyListener;

    .line 38
    return-void
.end method
