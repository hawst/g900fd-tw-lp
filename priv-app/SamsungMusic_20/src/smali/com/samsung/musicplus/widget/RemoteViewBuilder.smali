.class public Lcom/samsung/musicplus/widget/RemoteViewBuilder;
.super Ljava/lang/Object;
.source "RemoteViewBuilder.java"


# instance fields
.field protected mContext:Landroid/content/Context;

.field protected mIsNoSongsPlaying:Z

.field protected mRemoteView:Landroid/widget/RemoteViews;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "layoutId"    # I

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/widget/RemoteViewBuilder;->mIsNoSongsPlaying:Z

    .line 57
    iput-object p1, p0, Lcom/samsung/musicplus/widget/RemoteViewBuilder;->mContext:Landroid/content/Context;

    .line 58
    invoke-direct {p0, p2}, Lcom/samsung/musicplus/widget/RemoteViewBuilder;->createRemoteview(I)V

    .line 59
    return-void
.end method

.method private createRemoteview(I)V
    .locals 2
    .param p1, "layoutId"    # I

    .prologue
    .line 62
    new-instance v0, Landroid/widget/RemoteViews;

    iget-object v1, p0, Lcom/samsung/musicplus/widget/RemoteViewBuilder;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/samsung/musicplus/widget/RemoteViewBuilder;->mRemoteView:Landroid/widget/RemoteViews;

    .line 63
    return-void
.end method


# virtual methods
.method public build()Landroid/widget/RemoteViews;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/samsung/musicplus/widget/RemoteViewBuilder;->mRemoteView:Landroid/widget/RemoteViews;

    return-object v0
.end method

.method public getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/samsung/musicplus/widget/RemoteViewBuilder;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method protected getPlayerServiceIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/app/PendingIntent;
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "action"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x0

    .line 110
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 111
    .local v0, "i":Landroid/content/Intent;
    new-instance v1, Landroid/content/ComponentName;

    iget-object v2, p0, Lcom/samsung/musicplus/widget/RemoteViewBuilder;->mContext:Landroid/content/Context;

    const-class v3, Lcom/samsung/musicplus/service/PlayerService;

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 112
    iget-object v1, p0, Lcom/samsung/musicplus/widget/RemoteViewBuilder;->mContext:Landroid/content/Context;

    invoke-static {v1, v4, v0, v4}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    return-object v1
.end method

.method public setAlbumArt(Landroid/graphics/Bitmap;)Lcom/samsung/musicplus/widget/RemoteViewBuilder;
    .locals 3
    .param p1, "album"    # Landroid/graphics/Bitmap;

    .prologue
    const v2, 0x7f0d0054

    .line 124
    if-nez p1, :cond_0

    .line 125
    iget-object v0, p0, Lcom/samsung/musicplus/widget/RemoteViewBuilder;->mRemoteView:Landroid/widget/RemoteViews;

    const v1, 0x7f02003a

    invoke-virtual {v0, v2, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 131
    :goto_0
    return-object p0

    .line 128
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/widget/RemoteViewBuilder;->mRemoteView:Landroid/widget/RemoteViews;

    invoke-virtual {v0, v2, p1}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method protected setButtons()Lcom/samsung/musicplus/widget/RemoteViewBuilder;
    .locals 4

    .prologue
    .line 89
    iget-object v0, p0, Lcom/samsung/musicplus/widget/RemoteViewBuilder;->mRemoteView:Landroid/widget/RemoteViews;

    const v1, 0x7f0d00fa

    iget-object v2, p0, Lcom/samsung/musicplus/widget/RemoteViewBuilder;->mContext:Landroid/content/Context;

    const-string v3, "com.samsung.musicplus.musicservicecommand.togglepause"

    invoke-virtual {p0, v2, v3}, Lcom/samsung/musicplus/widget/RemoteViewBuilder;->getPlayerServiceIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 92
    iget-object v0, p0, Lcom/samsung/musicplus/widget/RemoteViewBuilder;->mRemoteView:Landroid/widget/RemoteViews;

    const v1, 0x7f0d00f9

    iget-object v2, p0, Lcom/samsung/musicplus/widget/RemoteViewBuilder;->mContext:Landroid/content/Context;

    const-string v3, "com.samsung.musicplus.musicservicecommand.previous"

    invoke-virtual {p0, v2, v3}, Lcom/samsung/musicplus/widget/RemoteViewBuilder;->getPlayerServiceIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 95
    iget-object v0, p0, Lcom/samsung/musicplus/widget/RemoteViewBuilder;->mRemoteView:Landroid/widget/RemoteViews;

    const v1, 0x7f0d00fc

    iget-object v2, p0, Lcom/samsung/musicplus/widget/RemoteViewBuilder;->mContext:Landroid/content/Context;

    const-string v3, "com.samsung.musicplus.musicservicecommand.next"

    invoke-virtual {p0, v2, v3}, Lcom/samsung/musicplus/widget/RemoteViewBuilder;->getPlayerServiceIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 98
    return-object p0
.end method

.method public setConnectivityImage(Z)Lcom/samsung/musicplus/widget/RemoteViewBuilder;
    .locals 3
    .param p1, "show"    # Z

    .prologue
    .line 249
    iget-object v1, p0, Lcom/samsung/musicplus/widget/RemoteViewBuilder;->mRemoteView:Landroid/widget/RemoteViews;

    const v2, 0x7f0d018a

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v2, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 251
    return-object p0

    .line 249
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setK2HD(Z)Lcom/samsung/musicplus/widget/RemoteViewBuilder;
    .locals 3
    .param p1, "isK2HD"    # Z

    .prologue
    const v2, 0x7f0d0188

    .line 157
    if-eqz p1, :cond_0

    .line 158
    iget-object v0, p0, Lcom/samsung/musicplus/widget/RemoteViewBuilder;->mRemoteView:Landroid/widget/RemoteViews;

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 163
    :goto_0
    return-object p0

    .line 160
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/widget/RemoteViewBuilder;->mRemoteView:Landroid/widget/RemoteViews;

    const/16 v1, 0x8

    invoke-virtual {v0, v2, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_0
.end method

.method public setMarqueeEnabled(Z)Lcom/samsung/musicplus/widget/RemoteViewBuilder;
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 261
    return-object p0
.end method

.method public setPersonalIcon(I)Lcom/samsung/musicplus/widget/RemoteViewBuilder;
    .locals 3
    .param p1, "personalMode"    # I

    .prologue
    .line 242
    iget-object v1, p0, Lcom/samsung/musicplus/widget/RemoteViewBuilder;->mRemoteView:Landroid/widget/RemoteViews;

    const v2, 0x7f0d0063

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v2, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 244
    return-object p0

    .line 242
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setPlayController(I)Lcom/samsung/musicplus/widget/RemoteViewBuilder;
    .locals 3
    .param p1, "listType"    # I

    .prologue
    const/4 v2, 0x0

    .line 196
    iget-object v0, p0, Lcom/samsung/musicplus/widget/RemoteViewBuilder;->mRemoteView:Landroid/widget/RemoteViews;

    const v1, 0x7f0d00fc

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 197
    iget-object v0, p0, Lcom/samsung/musicplus/widget/RemoteViewBuilder;->mRemoteView:Landroid/widget/RemoteViews;

    const v1, 0x7f0d00f9

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 198
    return-object p0
.end method

.method public setPlayStatus(IZ)Lcom/samsung/musicplus/widget/RemoteViewBuilder;
    .locals 4
    .param p1, "listType"    # I
    .param p2, "isPlaying"    # Z

    .prologue
    const v3, 0x7f0d00fa

    .line 174
    if-eqz p2, :cond_0

    .line 175
    iget-object v0, p0, Lcom/samsung/musicplus/widget/RemoteViewBuilder;->mRemoteView:Landroid/widget/RemoteViews;

    const v1, 0x7f020124

    invoke-virtual {v0, v3, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 177
    iget-object v0, p0, Lcom/samsung/musicplus/widget/RemoteViewBuilder;->mRemoteView:Landroid/widget/RemoteViews;

    iget-object v1, p0, Lcom/samsung/musicplus/widget/RemoteViewBuilder;->mContext:Landroid/content/Context;

    const v2, 0x7f100193

    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Landroid/widget/RemoteViews;->setContentDescription(ILjava/lang/CharSequence;)V

    .line 186
    :goto_0
    return-object p0

    .line 180
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/widget/RemoteViewBuilder;->mRemoteView:Landroid/widget/RemoteViews;

    const v1, 0x7f020125

    invoke-virtual {v0, v3, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 182
    iget-object v0, p0, Lcom/samsung/musicplus/widget/RemoteViewBuilder;->mRemoteView:Landroid/widget/RemoteViews;

    iget-object v1, p0, Lcom/samsung/musicplus/widget/RemoteViewBuilder;->mContext:Landroid/content/Context;

    const v2, 0x7f100194

    invoke-virtual {v1, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Landroid/widget/RemoteViews;->setContentDescription(ILjava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public setTitles(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/musicplus/widget/RemoteViewBuilder;
    .locals 5
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "artist"    # Ljava/lang/String;

    .prologue
    const v4, 0x7f0d00d6

    const/4 v1, 0x0

    const v3, 0x7f0d00d5

    .line 209
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 210
    iget-object v0, p0, Lcom/samsung/musicplus/widget/RemoteViewBuilder;->mRemoteView:Landroid/widget/RemoteViews;

    invoke-virtual {v0, v4, p1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 211
    iget-object v0, p0, Lcom/samsung/musicplus/widget/RemoteViewBuilder;->mRemoteView:Landroid/widget/RemoteViews;

    invoke-virtual {v0, v3, p2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 212
    iget-object v0, p0, Lcom/samsung/musicplus/widget/RemoteViewBuilder;->mRemoteView:Landroid/widget/RemoteViews;

    invoke-virtual {v0, v3, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 213
    iput-boolean v1, p0, Lcom/samsung/musicplus/widget/RemoteViewBuilder;->mIsNoSongsPlaying:Z

    .line 221
    :goto_0
    return-object p0

    .line 215
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/widget/RemoteViewBuilder;->mRemoteView:Landroid/widget/RemoteViews;

    iget-object v1, p0, Lcom/samsung/musicplus/widget/RemoteViewBuilder;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f100104

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v4, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 217
    iget-object v0, p0, Lcom/samsung/musicplus/widget/RemoteViewBuilder;->mRemoteView:Landroid/widget/RemoteViews;

    const/4 v1, 0x4

    invoke-virtual {v0, v3, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 218
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/widget/RemoteViewBuilder;->mIsNoSongsPlaying:Z

    goto :goto_0
.end method

.method public setUHQ(Z)Lcom/samsung/musicplus/widget/RemoteViewBuilder;
    .locals 3
    .param p1, "isUHQ"    # Z

    .prologue
    const v2, 0x7f0d0189

    .line 141
    if-eqz p1, :cond_0

    .line 142
    iget-object v0, p0, Lcom/samsung/musicplus/widget/RemoteViewBuilder;->mRemoteView:Landroid/widget/RemoteViews;

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 147
    :goto_0
    return-object p0

    .line 144
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/widget/RemoteViewBuilder;->mRemoteView:Landroid/widget/RemoteViews;

    const/16 v1, 0x8

    invoke-virtual {v0, v2, v1}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_0
.end method

.method public updateExtraButtons(II)Lcom/samsung/musicplus/widget/RemoteViewBuilder;
    .locals 0
    .param p1, "shuffle"    # I
    .param p2, "repeat"    # I

    .prologue
    .line 232
    return-object p0
.end method
