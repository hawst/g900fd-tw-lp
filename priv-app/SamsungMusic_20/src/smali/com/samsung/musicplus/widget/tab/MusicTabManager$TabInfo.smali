.class public final Lcom/samsung/musicplus/widget/tab/MusicTabManager$TabInfo;
.super Ljava/lang/Object;
.source "MusicTabManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/widget/tab/MusicTabManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TabInfo"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/widget/tab/MusicTabManager$TabInfo$DummyTabFactory;
    }
.end annotation


# instance fields
.field private final args:Landroid/os/Bundle;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private final clss:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field public fragment:Landroid/app/Fragment;

.field public final tag:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/Class;Landroid/os/Bundle;)V
    .locals 0
    .param p1, "_tag"    # Ljava/lang/String;
    .param p3, "_args"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/Class",
            "<*>;",
            "Landroid/os/Bundle;",
            ")V"
        }
    .end annotation

    .prologue
    .line 83
    .local p2, "_class":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    iput-object p1, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager$TabInfo;->tag:Ljava/lang/String;

    .line 85
    iput-object p2, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager$TabInfo;->clss:Ljava/lang/Class;

    .line 86
    iput-object p3, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager$TabInfo;->args:Landroid/os/Bundle;

    .line 87
    return-void
.end method
