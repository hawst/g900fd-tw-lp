.class public final enum Lcom/samsung/musicplus/widget/AnimatedToggleButton$ButtonState;
.super Ljava/lang/Enum;
.source "AnimatedToggleButton.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/widget/AnimatedToggleButton;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "ButtonState"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/samsung/musicplus/widget/AnimatedToggleButton$ButtonState;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/samsung/musicplus/widget/AnimatedToggleButton$ButtonState;

.field public static final enum PRESSED:Lcom/samsung/musicplus/widget/AnimatedToggleButton$ButtonState;

.field public static final enum RELEASED:Lcom/samsung/musicplus/widget/AnimatedToggleButton$ButtonState;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 124
    new-instance v0, Lcom/samsung/musicplus/widget/AnimatedToggleButton$ButtonState;

    const-string v1, "PRESSED"

    invoke-direct {v0, v1, v2}, Lcom/samsung/musicplus/widget/AnimatedToggleButton$ButtonState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/musicplus/widget/AnimatedToggleButton$ButtonState;->PRESSED:Lcom/samsung/musicplus/widget/AnimatedToggleButton$ButtonState;

    new-instance v0, Lcom/samsung/musicplus/widget/AnimatedToggleButton$ButtonState;

    const-string v1, "RELEASED"

    invoke-direct {v0, v1, v3}, Lcom/samsung/musicplus/widget/AnimatedToggleButton$ButtonState;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/samsung/musicplus/widget/AnimatedToggleButton$ButtonState;->RELEASED:Lcom/samsung/musicplus/widget/AnimatedToggleButton$ButtonState;

    .line 123
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/samsung/musicplus/widget/AnimatedToggleButton$ButtonState;

    sget-object v1, Lcom/samsung/musicplus/widget/AnimatedToggleButton$ButtonState;->PRESSED:Lcom/samsung/musicplus/widget/AnimatedToggleButton$ButtonState;

    aput-object v1, v0, v2

    sget-object v1, Lcom/samsung/musicplus/widget/AnimatedToggleButton$ButtonState;->RELEASED:Lcom/samsung/musicplus/widget/AnimatedToggleButton$ButtonState;

    aput-object v1, v0, v3

    sput-object v0, Lcom/samsung/musicplus/widget/AnimatedToggleButton$ButtonState;->$VALUES:[Lcom/samsung/musicplus/widget/AnimatedToggleButton$ButtonState;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 123
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/samsung/musicplus/widget/AnimatedToggleButton$ButtonState;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 123
    const-class v0, Lcom/samsung/musicplus/widget/AnimatedToggleButton$ButtonState;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/widget/AnimatedToggleButton$ButtonState;

    return-object v0
.end method

.method public static values()[Lcom/samsung/musicplus/widget/AnimatedToggleButton$ButtonState;
    .locals 1

    .prologue
    .line 123
    sget-object v0, Lcom/samsung/musicplus/widget/AnimatedToggleButton$ButtonState;->$VALUES:[Lcom/samsung/musicplus/widget/AnimatedToggleButton$ButtonState;

    invoke-virtual {v0}, [Lcom/samsung/musicplus/widget/AnimatedToggleButton$ButtonState;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/samsung/musicplus/widget/AnimatedToggleButton$ButtonState;

    return-object v0
.end method
