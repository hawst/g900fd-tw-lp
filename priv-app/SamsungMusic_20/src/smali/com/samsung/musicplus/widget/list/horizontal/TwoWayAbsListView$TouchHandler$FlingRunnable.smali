.class public abstract Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$FlingRunnable;
.super Ljava/lang/Object;
.source "TwoWayAbsListView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x404
    name = "FlingRunnable"
.end annotation


# instance fields
.field protected final mScroller:Landroid/widget/Scroller;

.field final synthetic this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;)V
    .locals 2

    .prologue
    .line 3676
    iput-object p1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$FlingRunnable;->this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3677
    new-instance v0, Landroid/widget/Scroller;

    iget-object v1, p1, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    invoke-virtual {v1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/Scroller;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$FlingRunnable;->mScroller:Landroid/widget/Scroller;

    .line 3678
    return-void
.end method


# virtual methods
.method protected endFling()V
    .locals 2

    .prologue
    .line 3685
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$FlingRunnable;->this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    const/4 v1, -0x1

    iput v1, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mTouchMode:I

    .line 3687
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$FlingRunnable;->this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->reportScrollStateChange(I)V

    .line 3688
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$FlingRunnable;->this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;

    invoke-virtual {v0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->clearScrollingCache()V

    .line 3690
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$FlingRunnable;->this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    invoke-virtual {v0, p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 3692
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$FlingRunnable;->this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->mPositionScroller:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$PositionScroller;

    if-eqz v0, :cond_0

    .line 3693
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$FlingRunnable;->this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    iget-object v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$FlingRunnable;->this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;

    iget-object v1, v1, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->mPositionScroller:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$PositionScroller;

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 3695
    :cond_0
    return-void
.end method

.method public abstract run()V
.end method

.method abstract start(I)V
.end method

.method abstract startScroll(II)V
.end method
