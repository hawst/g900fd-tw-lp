.class public Lcom/samsung/musicplus/widget/list/ReorderListView;
.super Lcom/samsung/musicplus/widget/list/MusicListView;
.source "ReorderListView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/widget/list/ReorderListView$DropListener;,
        Lcom/samsung/musicplus/widget/list/ReorderListView$DragListener;
    }
.end annotation


# static fields
.field private static final CLASSNAME:Ljava/lang/String;

.field private static final MOVE_LOG:Z


# instance fields
.field private mActionBarHeight:I

.field private mDragBitmap:Landroid/graphics/Bitmap;

.field private mDragListener:Lcom/samsung/musicplus/widget/list/ReorderListView$DragListener;

.field private mDragPointY:I

.field private mDragPos:I

.field private mDragView:Landroid/widget/ImageView;

.field private mDropListener:Lcom/samsung/musicplus/widget/list/ReorderListView$DropListener;

.field private mHeight:I

.field private mItemHeightExpanded:I

.field private mItemHeightHalf:I

.field private mItemHeightNormal:I

.field private mLowerBound:I

.field private mSrcDragPos:I

.field private final mTempRect:Landroid/graphics/Rect;

.field private mTouchSlop:I

.field private mTrashcan:Landroid/graphics/drawable/Drawable;

.field private mUI:Landroid/os/Handler;

.field private mUpperBound:I

.field private mWindowManager:Landroid/view/WindowManager;

.field private mWindowParams:Landroid/view/WindowManager$LayoutParams;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const-class v0, Lcom/samsung/musicplus/widget/list/ReorderListView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/widget/list/ReorderListView;->CLASSNAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 101
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/widget/list/MusicListView;-><init>(Landroid/content/Context;)V

    .line 79
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mTempRect:Landroid/graphics/Rect;

    .line 422
    new-instance v0, Lcom/samsung/musicplus/widget/list/ReorderListView$1;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/widget/list/ReorderListView$1;-><init>(Lcom/samsung/musicplus/widget/list/ReorderListView;)V

    iput-object v0, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mUI:Landroid/os/Handler;

    .line 102
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/widget/list/ReorderListView;->initListView(Landroid/content/Context;)V

    .line 103
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 106
    invoke-direct {p0, p1, p2}, Lcom/samsung/musicplus/widget/list/MusicListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 79
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mTempRect:Landroid/graphics/Rect;

    .line 422
    new-instance v0, Lcom/samsung/musicplus/widget/list/ReorderListView$1;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/widget/list/ReorderListView$1;-><init>(Lcom/samsung/musicplus/widget/list/ReorderListView;)V

    iput-object v0, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mUI:Landroid/os/Handler;

    .line 107
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/widget/list/ReorderListView;->initListView(Landroid/content/Context;)V

    .line 108
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 111
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/musicplus/widget/list/MusicListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 79
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mTempRect:Landroid/graphics/Rect;

    .line 422
    new-instance v0, Lcom/samsung/musicplus/widget/list/ReorderListView$1;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/widget/list/ReorderListView$1;-><init>(Lcom/samsung/musicplus/widget/list/ReorderListView;)V

    iput-object v0, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mUI:Landroid/os/Handler;

    .line 112
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/widget/list/ReorderListView;->initListView(Landroid/content/Context;)V

    .line 113
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/widget/list/ReorderListView;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/list/ReorderListView;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mUI:Landroid/os/Handler;

    return-object v0
.end method

.method private adjustScrollBounds(I)V
    .locals 1
    .param p1, "y"    # I

    .prologue
    .line 229
    iget v0, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mHeight:I

    div-int/lit8 v0, v0, 0x3

    if-lt p1, v0, :cond_0

    .line 230
    iget v0, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mHeight:I

    div-int/lit8 v0, v0, 0x3

    iput v0, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mUpperBound:I

    .line 232
    :cond_0
    iget v0, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mHeight:I

    mul-int/lit8 v0, v0, 0x2

    div-int/lit8 v0, v0, 0x3

    if-gt p1, v0, :cond_1

    .line 233
    iget v0, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mHeight:I

    mul-int/lit8 v0, v0, 0x2

    div-int/lit8 v0, v0, 0x3

    iput v0, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mLowerBound:I

    .line 235
    :cond_1
    return-void
.end method

.method private doExpansion()V
    .locals 12

    .prologue
    const/4 v11, 0x0

    .line 290
    iget v8, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mDragPos:I

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/ReorderListView;->getFirstVisiblePosition()I

    move-result v9

    sub-int v0, v8, v9

    .line 291
    .local v0, "childnum":I
    iget v8, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mDragPos:I

    iget v9, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mSrcDragPos:I

    if-le v8, v9, :cond_0

    .line 292
    add-int/lit8 v0, v0, 0x1

    .line 294
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/ReorderListView;->getHeaderViewsCount()I

    move-result v4

    .line 295
    .local v4, "numheaders":I
    iget v8, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mSrcDragPos:I

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/ReorderListView;->getFirstVisiblePosition()I

    move-result v9

    sub-int/2addr v8, v9

    invoke-virtual {p0, v8}, Lcom/samsung/musicplus/widget/list/ReorderListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 296
    .local v1, "first":Landroid/view/View;
    sget-object v8, Lcom/samsung/musicplus/widget/list/ReorderListView;->CLASSNAME:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "doExpansion childnum:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " numheaders:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " first:"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 298
    const/4 v3, 0x0

    .line 299
    .local v3, "i":I
    :goto_0
    invoke-virtual {p0, v3}, Lcom/samsung/musicplus/widget/list/ReorderListView;->getChildAt(I)Landroid/view/View;

    move-result-object v7

    .line 300
    .local v7, "vv":Landroid/view/View;
    if-nez v7, :cond_1

    .line 349
    return-void

    .line 304
    :cond_1
    iget v2, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mItemHeightNormal:I

    .line 305
    .local v2, "height":I
    const/4 v6, 0x0

    .line 306
    .local v6, "visibility":I
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/ReorderListView;->getLastVisiblePosition()I

    move-result v8

    if-ne v3, v8, :cond_4

    iget v8, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mSrcDragPos:I

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/ReorderListView;->getLastVisiblePosition()I

    move-result v9

    if-ne v8, v9, :cond_4

    .line 308
    const/4 v2, 0x1

    .line 309
    const/4 v6, 0x4

    .line 339
    :cond_2
    :goto_1
    iget v8, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mDragPos:I

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/ReorderListView;->getCount()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    if-ne v8, v9, :cond_3

    if-ne v3, v0, :cond_3

    .line 340
    iget v2, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mItemHeightExpanded:I

    .line 341
    iget v8, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mItemHeightNormal:I

    invoke-virtual {v7, v11, v11, v11, v8}, Landroid/view/View;->setPadding(IIII)V

    .line 344
    :cond_3
    invoke-virtual {v7}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    .line 345
    .local v5, "params":Landroid/view/ViewGroup$LayoutParams;
    iput v2, v5, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 346
    invoke-virtual {v7, v5}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 347
    invoke-virtual {v7, v6}, Landroid/view/View;->setVisibility(I)V

    .line 298
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 310
    .end local v5    # "params":Landroid/view/ViewGroup$LayoutParams;
    :cond_4
    iget v8, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mDragPos:I

    if-ge v8, v4, :cond_6

    if-ne v3, v4, :cond_6

    .line 313
    invoke-virtual {v7, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_5

    .line 314
    const/4 v6, 0x4

    goto :goto_1

    .line 317
    :cond_5
    iget v2, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mItemHeightExpanded:I

    goto :goto_1

    .line 319
    :cond_6
    invoke-virtual {v7, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_9

    .line 321
    iget v8, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mDragPos:I

    iget v9, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mSrcDragPos:I

    if-eq v8, v9, :cond_7

    invoke-virtual {p0, v7}, Lcom/samsung/musicplus/widget/list/ReorderListView;->getPositionForView(Landroid/view/View;)I

    move-result v8

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/ReorderListView;->getCount()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    if-ne v8, v9, :cond_8

    .line 323
    :cond_7
    const/4 v6, 0x4

    goto :goto_1

    .line 330
    :cond_8
    const/4 v2, 0x1

    .line 331
    const/4 v6, 0x4

    goto :goto_1

    .line 333
    :cond_9
    if-ne v3, v0, :cond_2

    .line 334
    iget v8, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mDragPos:I

    if-lt v8, v4, :cond_2

    iget v8, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mDragPos:I

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/ReorderListView;->getCount()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    if-ge v8, v9, :cond_2

    .line 335
    iget v2, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mItemHeightExpanded:I

    goto :goto_1
.end method

.method private dragView(II)V
    .locals 5
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    const/4 v4, 0x0

    .line 475
    iget-object v1, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    iput v4, v1, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 476
    iget-object v1, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    iget v2, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mItemHeightHalf:I

    sub-int v2, p2, v2

    iget v3, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mActionBarHeight:I

    add-int/2addr v2, v3

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 477
    iget-object v1, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mWindowManager:Landroid/view/WindowManager;

    iget-object v2, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mDragView:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v1, v2, v3}, Landroid/view/WindowManager;->updateViewLayout(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 479
    iget-object v1, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mTrashcan:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_0

    .line 480
    iget-object v1, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mDragView:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getWidth()I

    move-result v0

    .line 481
    .local v0, "width":I
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/ReorderListView;->getHeight()I

    move-result v1

    mul-int/lit8 v1, v1, 0x3

    div-int/lit8 v1, v1, 0x4

    if-le p2, v1, :cond_1

    .line 482
    iget-object v1, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mTrashcan:Landroid/graphics/drawable/Drawable;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->setLevel(I)Z

    .line 489
    .end local v0    # "width":I
    :cond_0
    :goto_0
    return-void

    .line 483
    .restart local v0    # "width":I
    :cond_1
    if-lez v0, :cond_2

    div-int/lit8 v1, v0, 0x4

    if-le p1, v1, :cond_2

    .line 484
    iget-object v1, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mTrashcan:Landroid/graphics/drawable/Drawable;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->setLevel(I)Z

    goto :goto_0

    .line 486
    :cond_2
    iget-object v1, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mTrashcan:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v4}, Landroid/graphics/drawable/Drawable;->setLevel(I)Z

    goto :goto_0
.end method

.method private getItemForPosition(II)I
    .locals 4
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 211
    iget v2, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mDragPointY:I

    sub-int v2, p2, v2

    iget v3, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mItemHeightNormal:I

    div-int/lit8 v3, v3, 0x3

    sub-int v0, v2, v3

    .line 212
    .local v0, "adjustedy":I
    invoke-direct {p0, p1, v0}, Lcom/samsung/musicplus/widget/list/ReorderListView;->myPointToPosition(II)I

    move-result v1

    .line 213
    .local v1, "pos":I
    if-ltz v1, :cond_1

    .line 214
    iget v2, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mSrcDragPos:I

    if-gt v1, v2, :cond_0

    .line 215
    add-int/lit8 v1, v1, 0x1

    .line 222
    :cond_0
    :goto_0
    return v1

    .line 217
    :cond_1
    if-gez v0, :cond_0

    .line 220
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private initListView(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 116
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    iput v1, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mTouchSlop:I

    .line 117
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/ReorderListView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 118
    .local v0, "res":Landroid/content/res/Resources;
    const v1, 0x7f0c013a

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mItemHeightNormal:I

    .line 119
    iget v1, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mItemHeightNormal:I

    div-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mItemHeightHalf:I

    .line 120
    iget v1, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mItemHeightNormal:I

    mul-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mItemHeightExpanded:I

    .line 121
    return-void
.end method

.method private myPointToPosition(II)I
    .locals 6
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 185
    if-gez p2, :cond_0

    .line 188
    iget v5, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mItemHeightNormal:I

    add-int/2addr v5, p2

    invoke-direct {p0, p1, v5}, Lcom/samsung/musicplus/widget/list/ReorderListView;->myPointToPosition(II)I

    move-result v4

    .line 189
    .local v4, "pos":I
    if-lez v4, :cond_0

    .line 190
    add-int/lit8 v5, v4, -0x1

    .line 203
    .end local v4    # "pos":I
    :goto_0
    return v5

    .line 194
    :cond_0
    iget-object v2, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mTempRect:Landroid/graphics/Rect;

    .line 195
    .local v2, "frame":Landroid/graphics/Rect;
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/ReorderListView;->getChildCount()I

    move-result v1

    .line 196
    .local v1, "count":I
    add-int/lit8 v3, v1, -0x1

    .local v3, "i":I
    :goto_1
    if-ltz v3, :cond_2

    .line 197
    invoke-virtual {p0, v3}, Lcom/samsung/musicplus/widget/list/ReorderListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 198
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0, v2}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 199
    invoke-virtual {v2, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 200
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/ReorderListView;->getFirstVisiblePosition()I

    move-result v5

    add-int/2addr v5, v3

    goto :goto_0

    .line 196
    :cond_1
    add-int/lit8 v3, v3, -0x1

    goto :goto_1

    .line 203
    .end local v0    # "child":Landroid/view/View;
    :cond_2
    const/4 v5, -0x1

    goto :goto_0
.end method

.method private startDragging(Landroid/graphics/Bitmap;II)V
    .locals 7
    .param p1, "bm"    # Landroid/graphics/Bitmap;
    .param p2, "x"    # I
    .param p3, "y"    # I

    .prologue
    const/4 v6, -0x2

    const/4 v5, 0x0

    .line 436
    sget-object v2, Lcom/samsung/musicplus/widget/list/ReorderListView;->CLASSNAME:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "startDragging x: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " y:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 437
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/list/ReorderListView;->stopDragging()V

    .line 439
    new-instance v2, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v2}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    iput-object v2, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    .line 440
    iget-object v2, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    const/16 v3, 0x33

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 441
    iget-object v2, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    iput v5, v2, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 442
    iget-object v2, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    iget v3, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mItemHeightHalf:I

    sub-int v3, p3, v3

    iget v4, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mActionBarHeight:I

    add-int/2addr v3, v4

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 444
    iget-object v2, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    iput v6, v2, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 445
    iget-object v2, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    iput v6, v2, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 446
    iget-object v2, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    const/16 v3, 0x398

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 451
    iget-object v2, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    const/4 v3, -0x3

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->format:I

    .line 452
    iget-object v2, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    iput v5, v2, Landroid/view/WindowManager$LayoutParams;->windowAnimations:I

    .line 454
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/ReorderListView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 455
    .local v0, "context":Landroid/content/Context;
    new-instance v1, Landroid/widget/ImageView;

    invoke-direct {v1, v0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    .line 460
    .local v1, "v":Landroid/widget/ImageView;
    invoke-virtual {v1, v5, v5, v5, v5}, Landroid/widget/ImageView;->setPadding(IIII)V

    .line 461
    invoke-virtual {v1, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 462
    const/16 v2, 0xaa

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageAlpha(I)V

    .line 463
    iput-object p1, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mDragBitmap:Landroid/graphics/Bitmap;

    .line 465
    const-string v2, "window"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/WindowManager;

    iput-object v2, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mWindowManager:Landroid/view/WindowManager;

    .line 466
    iget-object v2, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mWindowManager:Landroid/view/WindowManager;

    iget-object v3, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mWindowParams:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v2, v1, v3}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 467
    iput-object v1, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mDragView:Landroid/widget/ImageView;

    .line 468
    return-void
.end method

.method private stopDragging()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 501
    sget-object v1, Lcom/samsung/musicplus/widget/list/ReorderListView;->CLASSNAME:Ljava/lang/String;

    const-string v2, "stopDragging"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 502
    iget-object v1, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mUI:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 503
    iget-object v1, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mDragView:Landroid/widget/ImageView;

    if-eqz v1, :cond_0

    .line 504
    iget-object v1, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mDragView:Landroid/widget/ImageView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 505
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/ReorderListView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "window"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 507
    .local v0, "wm":Landroid/view/WindowManager;
    iget-object v1, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mDragView:Landroid/widget/ImageView;

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 508
    iget-object v1, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mDragView:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 509
    iput-object v3, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mDragView:Landroid/widget/ImageView;

    .line 511
    .end local v0    # "wm":Landroid/view/WindowManager;
    :cond_0
    iget-object v1, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mDragBitmap:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_1

    .line 512
    iget-object v1, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mDragBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 513
    iput-object v3, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mDragBitmap:Landroid/graphics/Bitmap;

    .line 515
    :cond_1
    iget-object v1, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mTrashcan:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_2

    .line 516
    iget-object v1, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mTrashcan:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v4}, Landroid/graphics/drawable/Drawable;->setLevel(I)Z

    .line 518
    :cond_2
    return-void
.end method

.method private unExpandViews(Z)V
    .locals 9
    .param p1, "deletion"    # Z

    .prologue
    const/4 v8, 0x0

    .line 241
    const/4 v0, 0x0

    .line 242
    .local v0, "i":I
    :goto_0
    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/widget/list/ReorderListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 243
    .local v3, "v":Landroid/view/View;
    sget-object v5, Lcom/samsung/musicplus/widget/list/ReorderListView;->CLASSNAME:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "unExpandViews getChildAt("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "):"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 244
    if-nez v3, :cond_3

    .line 245
    if-eqz p1, :cond_2

    .line 247
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/ReorderListView;->getFirstVisiblePosition()I

    move-result v2

    .line 248
    .local v2, "position":I
    invoke-virtual {p0, v8}, Lcom/samsung/musicplus/widget/list/ReorderListView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    if-nez v5, :cond_1

    .line 270
    .end local v2    # "position":I
    :cond_0
    return-void

    .line 251
    .restart local v2    # "position":I
    :cond_1
    invoke-virtual {p0, v8}, Lcom/samsung/musicplus/widget/list/ReorderListView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    move-result v4

    .line 252
    .local v4, "y":I
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/ReorderListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/samsung/musicplus/widget/list/ReorderListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 253
    invoke-virtual {p0, v2, v4}, Lcom/samsung/musicplus/widget/list/ReorderListView;->setSelectionFromTop(II)V

    .line 258
    .end local v2    # "position":I
    .end local v4    # "y":I
    :cond_2
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/ReorderListView;->layoutChildren()V

    .line 259
    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/widget/list/ReorderListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 260
    sget-object v5, Lcom/samsung/musicplus/widget/list/ReorderListView;->CLASSNAME:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "unExpandViews after layoutChildren() getChildAt("

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "):"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 269
    :goto_1
    if-eqz v3, :cond_0

    .line 273
    :cond_3
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 274
    .local v1, "params":Landroid/view/ViewGroup$LayoutParams;
    iget v5, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mItemHeightNormal:I

    iput v5, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 275
    invoke-virtual {v3, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 276
    invoke-virtual {v3, v8}, Landroid/view/View;->setVisibility(I)V

    .line 241
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    .line 262
    .end local v1    # "params":Landroid/view/ViewGroup$LayoutParams;
    :catch_0
    move-exception v5

    goto :goto_1
.end method


# virtual methods
.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 12
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v8, 0x1

    .line 127
    iget-object v9, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mDragListener:Lcom/samsung/musicplus/widget/list/ReorderListView$DragListener;

    if-nez v9, :cond_0

    iget-object v9, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mDropListener:Lcom/samsung/musicplus/widget/list/ReorderListView$DropListener;

    if-eqz v9, :cond_2

    .line 132
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/ReorderListView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 133
    .local v0, "a":Landroid/app/Activity;
    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v1

    .line 134
    .local v1, "ab":Landroid/app/ActionBar;
    if-eqz v1, :cond_1

    .line 135
    invoke-virtual {v1}, Landroid/app/ActionBar;->getHeight()I

    move-result v9

    iput v9, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mActionBarHeight:I

    .line 138
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v9

    packed-switch v9, :pswitch_data_0

    .line 174
    .end local v0    # "a":Landroid/app/Activity;
    .end local v1    # "ab":Landroid/app/ActionBar;
    :cond_2
    :goto_0
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/list/MusicListView;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v8

    :goto_1
    return v8

    .line 140
    .restart local v0    # "a":Landroid/app/Activity;
    .restart local v1    # "ab":Landroid/app/ActionBar;
    :pswitch_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/ReorderListView;->removeDragView()V

    .line 141
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v9

    float-to-int v6, v9

    .line 142
    .local v6, "x":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v9

    float-to-int v7, v9

    .line 143
    .local v7, "y":I
    invoke-virtual {p0, v6, v7}, Lcom/samsung/musicplus/widget/list/ReorderListView;->pointToPosition(II)I

    move-result v4

    .line 144
    .local v4, "itemnum":I
    sget-object v9, Lcom/samsung/musicplus/widget/list/ReorderListView;->CLASSNAME:Ljava/lang/String;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "onInterceptTouchEvent itemnum:"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v9, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 145
    const/4 v9, -0x1

    if-eq v4, v9, :cond_2

    .line 148
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/ReorderListView;->getFirstVisiblePosition()I

    move-result v9

    sub-int v9, v4, v9

    invoke-virtual {p0, v9}, Lcom/samsung/musicplus/widget/list/ReorderListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/view/ViewGroup;

    .line 149
    .local v3, "item":Landroid/view/ViewGroup;
    invoke-virtual {v3}, Landroid/view/ViewGroup;->getTop()I

    move-result v9

    sub-int v9, v7, v9

    iput v9, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mDragPointY:I

    .line 153
    const v9, 0x7f0d0137

    invoke-virtual {v3, v9}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v9

    invoke-virtual {v9}, Landroid/view/View;->getRight()I

    move-result v9

    if-ge v6, v9, :cond_3

    .line 154
    invoke-virtual {v3, v8}, Landroid/view/ViewGroup;->setDrawingCacheEnabled(Z)V

    .line 155
    invoke-virtual {v3}, Landroid/view/ViewGroup;->destroyDrawingCache()V

    .line 160
    invoke-virtual {v3}, Landroid/view/ViewGroup;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v9

    invoke-static {v9}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 161
    .local v2, "bitmap":Landroid/graphics/Bitmap;
    invoke-direct {p0, v2, v6, v7}, Lcom/samsung/musicplus/widget/list/ReorderListView;->startDragging(Landroid/graphics/Bitmap;II)V

    .line 162
    iput v4, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mDragPos:I

    .line 163
    iget v9, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mDragPos:I

    iput v9, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mSrcDragPos:I

    .line 164
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/ReorderListView;->getHeight()I

    move-result v9

    iput v9, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mHeight:I

    .line 165
    iget v5, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mTouchSlop:I

    .line 166
    .local v5, "touchSlop":I
    sub-int v9, v7, v5

    iget v10, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mHeight:I

    div-int/lit8 v10, v10, 0x3

    invoke-static {v9, v10}, Ljava/lang/Math;->min(II)I

    move-result v9

    iput v9, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mUpperBound:I

    .line 167
    add-int v9, v7, v5

    iget v10, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mHeight:I

    mul-int/lit8 v10, v10, 0x2

    div-int/lit8 v10, v10, 0x3

    invoke-static {v9, v10}, Ljava/lang/Math;->max(II)I

    move-result v9

    iput v9, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mLowerBound:I

    goto/16 :goto_1

    .line 170
    .end local v2    # "bitmap":Landroid/graphics/Bitmap;
    .end local v5    # "touchSlop":I
    :cond_3
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/list/ReorderListView;->stopDragging()V

    goto/16 :goto_0

    .line 138
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 9
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v8, 0x0

    .line 356
    iget-object v6, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mDragListener:Lcom/samsung/musicplus/widget/list/ReorderListView$DragListener;

    if-nez v6, :cond_0

    iget-object v6, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mDropListener:Lcom/samsung/musicplus/widget/list/ReorderListView$DropListener;

    if-eqz v6, :cond_b

    :cond_0
    iget-object v6, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mDragView:Landroid/widget/ImageView;

    if-eqz v6, :cond_b

    .line 357
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 361
    .local v0, "action":I
    packed-switch v0, :pswitch_data_0

    .line 418
    :cond_1
    :goto_0
    const/4 v6, 0x1

    .line 420
    .end local v0    # "action":I
    :goto_1
    return v6

    .line 364
    .restart local v0    # "action":I
    :pswitch_0
    iget-object v6, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mDropListener:Lcom/samsung/musicplus/widget/list/ReorderListView$DropListener;

    if-eqz v6, :cond_2

    iget v6, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mDragPos:I

    if-ltz v6, :cond_2

    iget v6, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mDragPos:I

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/ReorderListView;->getCount()I

    move-result v7

    if-ge v6, v7, :cond_2

    .line 365
    iget-object v6, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mDropListener:Lcom/samsung/musicplus/widget/list/ReorderListView$DropListener;

    iget v7, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mSrcDragPos:I

    iget v8, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mDragPos:I

    invoke-interface {v6, v7, v8}, Lcom/samsung/musicplus/widget/list/ReorderListView$DropListener;->drop(II)V

    goto :goto_0

    .line 369
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/ReorderListView;->removeDragView()V

    goto :goto_0

    .line 374
    :pswitch_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    float-to-int v4, v6

    .line 375
    .local v4, "x":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    float-to-int v5, v6

    .line 376
    .local v5, "y":I
    invoke-direct {p0, v4, v5}, Lcom/samsung/musicplus/widget/list/ReorderListView;->dragView(II)V

    .line 378
    invoke-direct {p0, v4, v5}, Lcom/samsung/musicplus/widget/list/ReorderListView;->getItemForPosition(II)I

    move-result v1

    .line 379
    .local v1, "itemnum":I
    if-ltz v1, :cond_1

    .line 380
    if-eqz v0, :cond_3

    iget v6, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mDragPos:I

    if-eq v1, v6, :cond_5

    .line 381
    :cond_3
    iget-object v6, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mDragListener:Lcom/samsung/musicplus/widget/list/ReorderListView$DragListener;

    if-eqz v6, :cond_4

    .line 382
    iget-object v6, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mDragListener:Lcom/samsung/musicplus/widget/list/ReorderListView$DragListener;

    iget v7, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mDragPos:I

    invoke-interface {v6, v7, v1}, Lcom/samsung/musicplus/widget/list/ReorderListView$DragListener;->drag(II)V

    .line 384
    :cond_4
    iput v1, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mDragPos:I

    .line 385
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/list/ReorderListView;->doExpansion()V

    .line 387
    :cond_5
    const/4 v3, 0x0

    .line 388
    .local v3, "speed":I
    invoke-direct {p0, v5}, Lcom/samsung/musicplus/widget/list/ReorderListView;->adjustScrollBounds(I)V

    .line 389
    iget v6, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mLowerBound:I

    if-le v5, v6, :cond_9

    .line 391
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/ReorderListView;->getLastVisiblePosition()I

    move-result v6

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/ReorderListView;->getCount()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    if-ge v6, v7, :cond_8

    .line 392
    iget v6, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mHeight:I

    iget v7, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mLowerBound:I

    add-int/2addr v6, v7

    div-int/lit8 v6, v6, 0x2

    if-le v5, v6, :cond_7

    const/16 v3, 0x10

    .line 409
    :cond_6
    :goto_2
    if-eqz v3, :cond_1

    .line 410
    iget-object v6, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mUI:Landroid/os/Handler;

    invoke-virtual {v6, v8}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v2

    .line 411
    .local v2, "msg":Landroid/os/Message;
    iput v3, v2, Landroid/os/Message;->arg1:I

    .line 412
    iget-object v6, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mUI:Landroid/os/Handler;

    invoke-virtual {v6, v8}, Landroid/os/Handler;->removeMessages(I)V

    .line 413
    iget-object v6, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mUI:Landroid/os/Handler;

    invoke-virtual {v6, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0

    .line 392
    .end local v2    # "msg":Landroid/os/Message;
    :cond_7
    const/4 v3, 0x4

    goto :goto_2

    .line 394
    :cond_8
    const/4 v3, 0x1

    goto :goto_2

    .line 396
    :cond_9
    iget v6, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mUpperBound:I

    if-ge v5, v6, :cond_6

    .line 398
    iget v6, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mUpperBound:I

    div-int/lit8 v6, v6, 0x2

    if-ge v5, v6, :cond_a

    const/16 v3, -0x10

    .line 399
    :goto_3
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/ReorderListView;->getFirstVisiblePosition()I

    move-result v6

    if-nez v6, :cond_6

    invoke-virtual {p0, v8}, Lcom/samsung/musicplus/widget/list/ReorderListView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/View;->getTop()I

    move-result v6

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/ReorderListView;->getPaddingTop()I

    move-result v7

    if-lt v6, v7, :cond_6

    .line 406
    const/4 v3, 0x0

    goto :goto_2

    .line 398
    :cond_a
    const/4 v3, -0x4

    goto :goto_3

    .line 420
    .end local v0    # "action":I
    .end local v1    # "itemnum":I
    .end local v3    # "speed":I
    .end local v4    # "x":I
    .end local v5    # "y":I
    :cond_b
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/list/MusicListView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v6

    goto/16 :goto_1

    .line 361
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public removeDragView()V
    .locals 2

    .prologue
    .line 492
    iget-object v1, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mDragView:Landroid/widget/ImageView;

    if-eqz v1, :cond_0

    .line 493
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mTempRect:Landroid/graphics/Rect;

    .line 494
    .local v0, "r":Landroid/graphics/Rect;
    iget-object v1, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mDragView:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->getDrawingRect(Landroid/graphics/Rect;)V

    .line 495
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/list/ReorderListView;->stopDragging()V

    .line 496
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/samsung/musicplus/widget/list/ReorderListView;->unExpandViews(Z)V

    .line 498
    .end local v0    # "r":Landroid/graphics/Rect;
    :cond_0
    return-void
.end method

.method public setDragListener(Lcom/samsung/musicplus/widget/list/ReorderListView$DragListener;)V
    .locals 0
    .param p1, "l"    # Lcom/samsung/musicplus/widget/list/ReorderListView$DragListener;

    .prologue
    .line 525
    iput-object p1, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mDragListener:Lcom/samsung/musicplus/widget/list/ReorderListView$DragListener;

    .line 526
    return-void
.end method

.method public setDropListener(Lcom/samsung/musicplus/widget/list/ReorderListView$DropListener;)V
    .locals 0
    .param p1, "l"    # Lcom/samsung/musicplus/widget/list/ReorderListView$DropListener;

    .prologue
    .line 529
    iput-object p1, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mDropListener:Lcom/samsung/musicplus/widget/list/ReorderListView$DropListener;

    .line 530
    return-void
.end method

.method public setTrashcan(Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .param p1, "trash"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 521
    iput-object p1, p0, Lcom/samsung/musicplus/widget/list/ReorderListView;->mTrashcan:Landroid/graphics/drawable/Drawable;

    .line 522
    return-void
.end method
