.class Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment$1;
.super Landroid/os/Handler;
.source "CommonModeListFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;)V
    .locals 0

    .prologue
    .line 49
    iput-object p1, p0, Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment$1;->this$0:Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 52
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 83
    :cond_0
    :goto_0
    return-void

    .line 54
    :pswitch_0
    iget-object v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment$1;->this$0:Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;

    # getter for: Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;->mLoadingProgress:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;->access$000(Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;)Landroid/app/ProgressDialog;

    move-result-object v1

    if-nez v1, :cond_1

    .line 55
    iget-object v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment$1;->this$0:Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;

    new-instance v2, Landroid/app/ProgressDialog;

    iget-object v3, p0, Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment$1;->this$0:Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;

    # getter for: Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;->mActivity:Landroid/app/Activity;
    invoke-static {v3}, Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;->access$100(Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;)Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    # setter for: Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;->mLoadingProgress:Landroid/app/ProgressDialog;
    invoke-static {v1, v2}, Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;->access$002(Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    .line 56
    iget-object v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment$1;->this$0:Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;

    # getter for: Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;->mLoadingProgress:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;->access$000(Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;)Landroid/app/ProgressDialog;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment$1;->this$0:Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;

    # getter for: Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;->mActivity:Landroid/app/Activity;
    invoke-static {v2}, Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;->access$100(Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;)Landroid/app/Activity;

    move-result-object v2

    const v3, 0x7f1000a1

    invoke-virtual {v2, v3}, Landroid/app/Activity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 58
    :cond_1
    iget-object v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment$1;->this$0:Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;

    # getter for: Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;->mLoadingProgress:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;->access$000(Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->show()V

    goto :goto_0

    .line 61
    :pswitch_1
    iget-object v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment$1;->this$0:Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;

    # getter for: Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;->mLoadingProgress:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;->access$000(Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;)Landroid/app/ProgressDialog;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment$1;->this$0:Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;

    # getter for: Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;->mLoadingProgress:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;->access$000(Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 63
    :try_start_0
    iget-object v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment$1;->this$0:Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;

    # getter for: Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;->mLoadingProgress:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;->access$000(Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 70
    :goto_1
    iget-object v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment$1;->this$0:Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;

    const/4 v2, 0x0

    # setter for: Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;->mLoadingProgress:Landroid/app/ProgressDialog;
    invoke-static {v1, v2}, Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;->access$002(Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    goto :goto_0

    .line 64
    :catch_0
    move-exception v0

    .line 68
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v1, "MusicUiList"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "Window\'s IllegalArgument "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 74
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :pswitch_2
    iget-object v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment$1;->this$0:Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;

    # getter for: Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;->mLoadingProgress:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;->access$000(Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;)Landroid/app/ProgressDialog;

    move-result-object v1

    if-nez v1, :cond_2

    .line 75
    iget-object v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment$1;->this$0:Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;

    new-instance v2, Landroid/app/ProgressDialog;

    iget-object v3, p0, Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment$1;->this$0:Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;

    # getter for: Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;->mActivity:Landroid/app/Activity;
    invoke-static {v3}, Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;->access$100(Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;)Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    # setter for: Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;->mLoadingProgress:Landroid/app/ProgressDialog;
    invoke-static {v1, v2}, Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;->access$002(Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;Landroid/app/ProgressDialog;)Landroid/app/ProgressDialog;

    .line 76
    iget-object v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment$1;->this$0:Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;

    # getter for: Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;->mLoadingProgress:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;->access$000(Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;)Landroid/app/ProgressDialog;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment$1;->this$0:Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;

    const v3, 0x7f100129

    invoke-virtual {v2, v3}, Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 78
    :cond_2
    iget-object v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment$1;->this$0:Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;

    # getter for: Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;->mLoadingProgress:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;->access$000(Lcom/samsung/musicplus/widget/fragment/CommonModeListFragment;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->show()V

    goto/16 :goto_0

    .line 52
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
