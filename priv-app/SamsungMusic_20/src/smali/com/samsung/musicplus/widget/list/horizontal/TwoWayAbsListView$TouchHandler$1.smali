.class Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$1;
.super Ljava/lang/Object;
.source "TwoWayAbsListView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->clearScrollingCache()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;)V
    .locals 0

    .prologue
    .line 3619
    iput-object p1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$1;->this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3621
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$1;->this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    iget-boolean v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mCachingStarted:Z

    if-eqz v0, :cond_1

    .line 3622
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$1;->this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    iput-boolean v1, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mCachingStarted:Z

    .line 3623
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$1;->this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    # invokes: Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->setChildrenDrawnWithCacheEnabled(Z)V
    invoke-static {v0, v1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->access$2500(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;Z)V

    .line 3624
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$1;->this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    invoke-virtual {v0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getPersistentDrawingCache()I

    move-result v0

    and-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_0

    .line 3625
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$1;->this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    # invokes: Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->setChildrenDrawingCacheEnabled(Z)V
    invoke-static {v0, v1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->access$2600(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;Z)V

    .line 3627
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$1;->this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    invoke-virtual {v0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->isAlwaysDrawnWithCacheEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    .line 3628
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$1;->this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    invoke-virtual {v0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->invalidate()V

    .line 3631
    :cond_1
    return-void
.end method
