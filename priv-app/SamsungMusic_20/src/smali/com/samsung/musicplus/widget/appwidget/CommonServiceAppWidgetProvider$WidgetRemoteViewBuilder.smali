.class public Lcom/samsung/musicplus/widget/appwidget/CommonServiceAppWidgetProvider$WidgetRemoteViewBuilder;
.super Lcom/samsung/musicplus/widget/RemoteViewBuilder;
.source "CommonServiceAppWidgetProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/widget/appwidget/CommonServiceAppWidgetProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xc
    name = "WidgetRemoteViewBuilder"
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "layoutId"    # I

    .prologue
    .line 249
    invoke-direct {p0, p1, p2}, Lcom/samsung/musicplus/widget/RemoteViewBuilder;-><init>(Landroid/content/Context;I)V

    .line 250
    return-void
.end method


# virtual methods
.method public setAlbumArt(Landroid/graphics/Bitmap;)Lcom/samsung/musicplus/widget/RemoteViewBuilder;
    .locals 1
    .param p1, "album"    # Landroid/graphics/Bitmap;

    .prologue
    .line 270
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/RemoteViewBuilder;->setAlbumArt(Landroid/graphics/Bitmap;)Lcom/samsung/musicplus/widget/RemoteViewBuilder;

    move-result-object v0

    return-object v0
.end method

.method public setButtons()Lcom/samsung/musicplus/widget/RemoteViewBuilder;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 254
    invoke-super {p0}, Lcom/samsung/musicplus/widget/RemoteViewBuilder;->setButtons()Lcom/samsung/musicplus/widget/RemoteViewBuilder;

    .line 256
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/samsung/musicplus/widget/appwidget/CommonServiceAppWidgetProvider$WidgetRemoteViewBuilder;->mContext:Landroid/content/Context;

    const-class v2, Lcom/samsung/musicplus/MusicMainActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 257
    .local v0, "i":Landroid/content/Intent;
    iget-boolean v1, p0, Lcom/samsung/musicplus/widget/appwidget/CommonServiceAppWidgetProvider$WidgetRemoteViewBuilder;->mIsNoSongsPlaying:Z

    if-eqz v1, :cond_0

    .line 258
    const-string v1, "track_launch"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 262
    :goto_0
    const v1, 0x8000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 263
    iget-object v1, p0, Lcom/samsung/musicplus/widget/appwidget/CommonServiceAppWidgetProvider$WidgetRemoteViewBuilder;->mRemoteView:Landroid/widget/RemoteViews;

    const v2, 0x7f0d0054

    iget-object v3, p0, Lcom/samsung/musicplus/widget/appwidget/CommonServiceAppWidgetProvider$WidgetRemoteViewBuilder;->mContext:Landroid/content/Context;

    const/high16 v4, 0x8000000

    invoke-static {v3, v5, v0, v4}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 265
    return-object p0

    .line 260
    :cond_0
    const-string v1, "track_launch"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_0
.end method

.method public setPlayController(I)Lcom/samsung/musicplus/widget/RemoteViewBuilder;
    .locals 1
    .param p1, "listType"    # I

    .prologue
    .line 280
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/RemoteViewBuilder;->setPlayController(I)Lcom/samsung/musicplus/widget/RemoteViewBuilder;

    move-result-object v0

    return-object v0
.end method

.method public setPlayStatus(IZ)Lcom/samsung/musicplus/widget/RemoteViewBuilder;
    .locals 1
    .param p1, "listType"    # I
    .param p2, "isPlaying"    # Z

    .prologue
    .line 275
    invoke-super {p0, p1, p2}, Lcom/samsung/musicplus/widget/RemoteViewBuilder;->setPlayStatus(IZ)Lcom/samsung/musicplus/widget/RemoteViewBuilder;

    move-result-object v0

    return-object v0
.end method

.method public setTitles(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/musicplus/widget/RemoteViewBuilder;
    .locals 1
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "artist"    # Ljava/lang/String;

    .prologue
    .line 285
    invoke-super {p0, p1, p2}, Lcom/samsung/musicplus/widget/RemoteViewBuilder;->setTitles(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/musicplus/widget/RemoteViewBuilder;

    move-result-object v0

    return-object v0
.end method

.method public showProgress(Z)V
    .locals 3
    .param p1, "value"    # Z

    .prologue
    .line 294
    iget-object v1, p0, Lcom/samsung/musicplus/widget/appwidget/CommonServiceAppWidgetProvider$WidgetRemoteViewBuilder;->mRemoteView:Landroid/widget/RemoteViews;

    const v2, 0x7f0d01ba

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v2, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 295
    return-void

    .line 294
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public updateExtraButtons(II)Lcom/samsung/musicplus/widget/RemoteViewBuilder;
    .locals 1
    .param p1, "shuffle"    # I
    .param p2, "repeat"    # I

    .prologue
    .line 290
    invoke-super {p0, p1, p2}, Lcom/samsung/musicplus/widget/RemoteViewBuilder;->updateExtraButtons(II)Lcom/samsung/musicplus/widget/RemoteViewBuilder;

    move-result-object v0

    return-object v0
.end method
