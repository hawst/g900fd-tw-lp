.class public abstract Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;
.super Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;
.source "TwoWayAbsListView.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnTouchModeChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$1;,
        Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;,
        Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler;,
        Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;,
        Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$RecycleBin;,
        Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$RecyclerListener;,
        Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$LayoutParams;,
        Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$CheckForTap;,
        Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$CheckForKeyLongPress;,
        Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$CheckForLongPress;,
        Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$PerformClick;,
        Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$WindowRunnnable;,
        Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$SavedState;,
        Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$OnScrollListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView",
        "<",
        "Landroid/widget/ListAdapter;",
        ">;",
        "Landroid/view/ViewTreeObserver$OnTouchModeChangeListener;"
    }
.end annotation


# static fields
.field private static final DEBUG:Z = false

.field private static final INVALID_POINTER:I = -0x1

.field static final LAYOUT_FORCE_BOTTOM:I = 0x3

.field static final LAYOUT_FORCE_TOP:I = 0x1

.field static final LAYOUT_MOVE_SELECTION:I = 0x6

.field static final LAYOUT_NORMAL:I = 0x0

.field static final LAYOUT_SET_SELECTION:I = 0x2

.field static final LAYOUT_SPECIFIC:I = 0x4

.field static final LAYOUT_SYNC:I = 0x5

.field private static final PROFILE_FLINGING:Z = false

.field private static final PROFILE_SCROLLING:Z = false

.field static final SCROLL_HORIZONTAL:I = 0x1

.field static final SCROLL_VERTICAL:I = 0x0

.field private static final TAG:Ljava/lang/String; = "TwoWayAbsListView"

.field static final TOUCH_MODE_DONE_WAITING:I = 0x2

.field static final TOUCH_MODE_DOWN:I = 0x0

.field static final TOUCH_MODE_FLING:I = 0x4

.field private static final TOUCH_MODE_OFF:I = 0x1

.field private static final TOUCH_MODE_ON:I = 0x0

.field static final TOUCH_MODE_REST:I = -0x1

.field static final TOUCH_MODE_SCROLL:I = 0x3

.field static final TOUCH_MODE_TAP:I = 0x1

.field private static final TOUCH_MODE_UNKNOWN:I = -0x1

.field public static final TRANSCRIPT_MODE_ALWAYS_SCROLL:I = 0x2

.field public static final TRANSCRIPT_MODE_DISABLED:I = 0x0

.field public static final TRANSCRIPT_MODE_NORMAL:I = 0x1


# instance fields
.field private mActivePointerId:I

.field mAdapter:Landroid/widget/ListAdapter;

.field private mCacheColorHint:I

.field mCachingStarted:Z

.field private mClearScrollingCache:Ljava/lang/Runnable;

.field private mContextMenuInfo:Landroid/view/ContextMenu$ContextMenuInfo;

.field private mDensityScale:F

.field mDrawSelectorOnTop:Z

.field private mFlingProfilingStarted:Z

.field private mIsChildViewEnabled:Z

.field final mIsScrap:[Z

.field private mLastScrollState:I

.field private mLastTouchMode:I

.field mLayoutMode:I

.field mListPadding:Landroid/graphics/Rect;

.field private mMinimumVelocity:I

.field mMotionPosition:I

.field mMotionX:I

.field mMotionY:I

.field private mOnScrollListener:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$OnScrollListener;

.field private mPendingCheckForKeyLongPress:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$CheckForKeyLongPress;

.field private mPendingCheckForLongPress:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$CheckForLongPress;

.field private mPendingCheckForTap:Ljava/lang/Runnable;

.field private mPerformClick:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$PerformClick;

.field protected mPortraitOrientation:Z

.field final mRecycler:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$RecycleBin;

.field mResurrectToPosition:I

.field mScrollDown:Landroid/view/View;

.field mScrollLeft:Landroid/view/View;

.field private mScrollProfilingStarted:Z

.field mScrollRight:Landroid/view/View;

.field mScrollUp:Landroid/view/View;

.field protected mScrollVertically:Z

.field private mScrollVerticallyLandscape:Z

.field private mScrollVerticallyPortrait:Z

.field mScrollingCacheEnabled:Z

.field mSelectedTop:I

.field mSelectionBottomPadding:I

.field mSelectionLeftPadding:I

.field mSelectionRightPadding:I

.field mSelectionTopPadding:I

.field mSelector:Landroid/graphics/drawable/Drawable;

.field mSelectorRect:Landroid/graphics/Rect;

.field private mSmoothScrollbarEnabled:Z

.field mStackFromBottom:Z

.field mTextFilter:Landroid/widget/EditText;

.field private mTouchFrame:Landroid/graphics/Rect;

.field protected mTouchHandler:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;

.field mTouchMode:I

.field private mTouchSlop:I

.field private mTranscriptMode:I

.field private mVelocityTracker:Landroid/view/VelocityTracker;

.field mWidthMeasureSpec:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x1

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 534
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;-><init>(Landroid/content/Context;)V

    .line 202
    iput v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mLayoutMode:I

    .line 217
    iput-boolean v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mDrawSelectorOnTop:Z

    .line 227
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSelectorRect:Landroid/graphics/Rect;

    .line 233
    new-instance v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$RecycleBin;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$RecycleBin;-><init>(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;)V

    iput-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mRecycler:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$RecycleBin;

    .line 238
    iput v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSelectionLeftPadding:I

    .line 243
    iput v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSelectionTopPadding:I

    .line 248
    iput v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSelectionRightPadding:I

    .line 253
    iput v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSelectionBottomPadding:I

    .line 258
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mListPadding:Landroid/graphics/Rect;

    .line 263
    iput v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mWidthMeasureSpec:I

    .line 312
    iput v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mTouchMode:I

    .line 323
    iput v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSelectedTop:I

    .line 361
    iput-boolean v3, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSmoothScrollbarEnabled:Z

    .line 381
    iput v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mResurrectToPosition:I

    .line 383
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mContextMenuInfo:Landroid/view/ContextMenu$ContextMenuInfo;

    .line 392
    iput v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mLastTouchMode:I

    .line 395
    iput-boolean v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mScrollProfilingStarted:Z

    .line 398
    iput-boolean v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mFlingProfilingStarted:Z

    .line 440
    iput v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mLastScrollState:I

    .line 466
    new-array v0, v3, [Z

    iput-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mIsScrap:[Z

    .line 476
    iput v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mActivePointerId:I

    .line 535
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->initAbsListView()V

    .line 536
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->setupScrollInfo()V

    .line 540
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 543
    const v0, 0x101006a

    invoke-direct {p0, p1, p2, v0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 544
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v11, -0x1

    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 547
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 202
    iput v10, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mLayoutMode:I

    .line 217
    iput-boolean v10, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mDrawSelectorOnTop:Z

    .line 227
    new-instance v8, Landroid/graphics/Rect;

    invoke-direct {v8}, Landroid/graphics/Rect;-><init>()V

    iput-object v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSelectorRect:Landroid/graphics/Rect;

    .line 233
    new-instance v8, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$RecycleBin;

    invoke-direct {v8, p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$RecycleBin;-><init>(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;)V

    iput-object v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mRecycler:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$RecycleBin;

    .line 238
    iput v10, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSelectionLeftPadding:I

    .line 243
    iput v10, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSelectionTopPadding:I

    .line 248
    iput v10, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSelectionRightPadding:I

    .line 253
    iput v10, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSelectionBottomPadding:I

    .line 258
    new-instance v8, Landroid/graphics/Rect;

    invoke-direct {v8}, Landroid/graphics/Rect;-><init>()V

    iput-object v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mListPadding:Landroid/graphics/Rect;

    .line 263
    iput v10, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mWidthMeasureSpec:I

    .line 312
    iput v11, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mTouchMode:I

    .line 323
    iput v10, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSelectedTop:I

    .line 361
    iput-boolean v9, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSmoothScrollbarEnabled:Z

    .line 381
    iput v11, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mResurrectToPosition:I

    .line 383
    const/4 v8, 0x0

    iput-object v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mContextMenuInfo:Landroid/view/ContextMenu$ContextMenuInfo;

    .line 392
    iput v11, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mLastTouchMode:I

    .line 395
    iput-boolean v10, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mScrollProfilingStarted:Z

    .line 398
    iput-boolean v10, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mFlingProfilingStarted:Z

    .line 440
    iput v10, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mLastScrollState:I

    .line 466
    new-array v8, v9, [Z

    iput-object v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mIsScrap:[Z

    .line 476
    iput v11, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mActivePointerId:I

    .line 548
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->initAbsListView()V

    .line 550
    sget-object v8, Lcom/sec/android/app/music/R$styleable;->TwoWayAbsListView:[I

    invoke-virtual {p1, p2, v8, p3, v10}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 553
    .local v0, "a":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v10}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 554
    .local v2, "d":Landroid/graphics/drawable/Drawable;
    if-eqz v2, :cond_0

    .line 555
    invoke-virtual {p0, v2}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->setSelector(Landroid/graphics/drawable/Drawable;)V

    .line 558
    :cond_0
    invoke-virtual {v0, v9, v10}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v8

    iput-boolean v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mDrawSelectorOnTop:Z

    .line 561
    const/4 v8, 0x2

    invoke-virtual {v0, v8, v10}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v6

    .line 562
    .local v6, "stackFromBottom":Z
    invoke-virtual {p0, v6}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->setStackFromBottom(Z)V

    .line 564
    const/4 v8, 0x3

    invoke-virtual {v0, v8, v9}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v4

    .line 565
    .local v4, "scrollingCacheEnabled":Z
    invoke-virtual {p0, v4}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->setScrollingCacheEnabled(Z)V

    .line 570
    const/4 v8, 0x4

    invoke-virtual {v0, v8, v10}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v7

    .line 572
    .local v7, "transcriptMode":I
    invoke-virtual {p0, v7}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->setTranscriptMode(I)V

    .line 574
    const/4 v8, 0x5

    invoke-virtual {v0, v8, v10}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    .line 575
    .local v1, "color":I
    invoke-direct {p0, v1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->setCacheColorHint(I)V

    .line 580
    const/4 v8, 0x6

    invoke-virtual {v0, v8, v9}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v5

    .line 581
    .local v5, "smoothScrollbar":Z
    invoke-virtual {p0, v5}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->setSmoothScrollbarEnabled(Z)V

    .line 583
    const/4 v8, 0x7

    invoke-virtual {v0, v8, v10}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v3

    .line 584
    .local v3, "scrollDirection":I
    if-nez v3, :cond_1

    move v8, v9

    :goto_0
    iput-boolean v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mScrollVerticallyPortrait:Z

    .line 586
    const/16 v8, 0x8

    invoke-virtual {v0, v8, v10}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v3

    .line 587
    if-nez v3, :cond_2

    :goto_1
    iput-boolean v9, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mScrollVerticallyLandscape:Z

    .line 589
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 590
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->setupScrollInfo()V

    .line 591
    return-void

    :cond_1
    move v8, v10

    .line 584
    goto :goto_0

    :cond_2
    move v9, v10

    .line 587
    goto :goto_1
.end method

.method static synthetic access$1200(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;Landroid/view/View;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;
    .param p1, "x1"    # Landroid/view/View;
    .param p2, "x2"    # Z

    .prologue
    .line 85
    invoke-virtual {p0, p1, p2}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->removeDetachedView(Landroid/view/View;Z)V

    return-void
.end method

.method static synthetic access$1300(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;Landroid/view/View;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;
    .param p1, "x1"    # Landroid/view/View;
    .param p2, "x2"    # Z

    .prologue
    .line 85
    invoke-virtual {p0, p1, p2}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->removeDetachedView(Landroid/view/View;Z)V

    return-void
.end method

.method static synthetic access$1400(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;Landroid/view/View;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;
    .param p1, "x1"    # Landroid/view/View;
    .param p2, "x2"    # Z

    .prologue
    .line 85
    invoke-virtual {p0, p1, p2}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->removeDetachedView(Landroid/view/View;Z)V

    return-void
.end method

.method static synthetic access$1500(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;Landroid/view/View;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;
    .param p1, "x1"    # Landroid/view/View;
    .param p2, "x2"    # Z

    .prologue
    .line 85
    invoke-virtual {p0, p1, p2}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->removeDetachedView(Landroid/view/View;Z)V

    return-void
.end method

.method static synthetic access$1600(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;Landroid/view/View;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;
    .param p1, "x1"    # Landroid/view/View;
    .param p2, "x2"    # Z

    .prologue
    .line 85
    invoke-virtual {p0, p1, p2}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->removeDetachedView(Landroid/view/View;Z)V

    return-void
.end method

.method static synthetic access$1700(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;
    .param p1, "x1"    # Z

    .prologue
    .line 85
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->setChildrenDrawingCacheEnabled(Z)V

    return-void
.end method

.method static synthetic access$1800(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    .prologue
    .line 85
    iget v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mLastTouchMode:I

    return v0
.end method

.method static synthetic access$1802(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;
    .param p1, "x1"    # I

    .prologue
    .line 85
    iput p1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mLastTouchMode:I

    return p1
.end method

.method static synthetic access$1900(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    .prologue
    .line 85
    iget v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mTouchSlop:I

    return v0
.end method

.method static synthetic access$200(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    .prologue
    .line 85
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getWindowAttachCount()I

    move-result v0

    return v0
.end method

.method static synthetic access$2000(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    .prologue
    .line 85
    iget v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mLastScrollState:I

    return v0
.end method

.method static synthetic access$2002(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;
    .param p1, "x1"    # I

    .prologue
    .line 85
    iput p1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mLastScrollState:I

    return p1
.end method

.method static synthetic access$2100(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;)Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$OnScrollListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    .prologue
    .line 85
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mOnScrollListener:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$OnScrollListener;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;
    .param p1, "x1"    # Z

    .prologue
    .line 85
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->setChildrenDrawnWithCacheEnabled(Z)V

    return-void
.end method

.method static synthetic access$2300(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;
    .param p1, "x1"    # Z

    .prologue
    .line 85
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->setChildrenDrawingCacheEnabled(Z)V

    return-void
.end method

.method static synthetic access$2400(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    .prologue
    .line 85
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mClearScrollingCache:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$2402(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;
    .param p1, "x1"    # Ljava/lang/Runnable;

    .prologue
    .line 85
    iput-object p1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mClearScrollingCache:Ljava/lang/Runnable;

    return-object p1
.end method

.method static synthetic access$2500(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;
    .param p1, "x1"    # Z

    .prologue
    .line 85
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->setChildrenDrawnWithCacheEnabled(Z)V

    return-void
.end method

.method static synthetic access$2600(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;
    .param p1, "x1"    # Z

    .prologue
    .line 85
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->setChildrenDrawingCacheEnabled(Z)V

    return-void
.end method

.method static synthetic access$2700(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;)Landroid/view/VelocityTracker;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    .prologue
    .line 85
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mVelocityTracker:Landroid/view/VelocityTracker;

    return-object v0
.end method

.method static synthetic access$2702(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;Landroid/view/VelocityTracker;)Landroid/view/VelocityTracker;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;
    .param p1, "x1"    # Landroid/view/VelocityTracker;

    .prologue
    .line 85
    iput-object p1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mVelocityTracker:Landroid/view/VelocityTracker;

    return-object p1
.end method

.method static synthetic access$2800(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    .prologue
    .line 85
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mPendingCheckForTap:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$2802(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;Ljava/lang/Runnable;)Ljava/lang/Runnable;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;
    .param p1, "x1"    # Ljava/lang/Runnable;

    .prologue
    .line 85
    iput-object p1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mPendingCheckForTap:Ljava/lang/Runnable;

    return-object p1
.end method

.method static synthetic access$2900(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;)Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$PerformClick;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    .prologue
    .line 85
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mPerformClick:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$PerformClick;

    return-object v0
.end method

.method static synthetic access$2902(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$PerformClick;)Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$PerformClick;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;
    .param p1, "x1"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$PerformClick;

    .prologue
    .line 85
    iput-object p1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mPerformClick:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$PerformClick;

    return-object p1
.end method

.method static synthetic access$300(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    .prologue
    .line 85
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getWindowAttachCount()I

    move-result v0

    return v0
.end method

.method static synthetic access$3100(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    .prologue
    .line 85
    iget v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mMinimumVelocity:I

    return v0
.end method

.method static synthetic access$3302(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;
    .param p1, "x1"    # I

    .prologue
    .line 85
    iput p1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mActivePointerId:I

    return p1
.end method

.method static synthetic access$3400(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;II)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 85
    invoke-virtual {p0, p1, p2}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->detachViewsFromParent(II)V

    return-void
.end method

.method static synthetic access$3600(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;II)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 85
    invoke-virtual {p0, p1, p2}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->detachViewsFromParent(II)V

    return-void
.end method

.method static synthetic access$500(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;Landroid/view/View;IJ)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;
    .param p1, "x1"    # Landroid/view/View;
    .param p2, "x2"    # I
    .param p3, "x3"    # J

    .prologue
    .line 85
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->performLongPress(Landroid/view/View;IJ)Z

    move-result v0

    return v0
.end method

.method static synthetic access$600(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;)Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$CheckForLongPress;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    .prologue
    .line 85
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mPendingCheckForLongPress:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$CheckForLongPress;

    return-object v0
.end method

.method static synthetic access$602(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$CheckForLongPress;)Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$CheckForLongPress;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;
    .param p1, "x1"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$CheckForLongPress;

    .prologue
    .line 85
    iput-object p1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mPendingCheckForLongPress:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$CheckForLongPress;

    return-object p1
.end method

.method private checkScrap(Ljava/util/ArrayList;)Z
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .line 3010
    .local p1, "scrap":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    if-nez p1, :cond_1

    const/4 v2, 0x1

    .line 3028
    :cond_0
    return v2

    .line 3011
    :cond_1
    const/4 v2, 0x1

    .line 3013
    .local v2, "result":Z
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 3014
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 3015
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    .line 3016
    .local v3, "view":Landroid/view/View;
    invoke-virtual {v3}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 3017
    const/4 v2, 0x0

    .line 3018
    const-string v4, "Consistency"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "TwoWayAbsListView "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " has a view in its scrap heap still attached to a parent: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3021
    :cond_2
    invoke-virtual {p0, v3}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->indexOfChild(Landroid/view/View;)I

    move-result v4

    if-ltz v4, :cond_3

    .line 3022
    const/4 v2, 0x0

    .line 3023
    const-string v4, "Consistency"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "TwoWayAbsListView "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " has a view in its scrap heap that is also a direct child: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3014
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private drawSelector(Landroid/graphics/Canvas;)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 1627
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->shouldShowSelector()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSelectorRect:Landroid/graphics/Rect;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSelectorRect:Landroid/graphics/Rect;

    invoke-virtual {v1}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1628
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSelector:Landroid/graphics/drawable/Drawable;

    .line 1629
    .local v0, "selector":Landroid/graphics/drawable/Drawable;
    iget-object v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSelectorRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 1630
    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1632
    .end local v0    # "selector":Landroid/graphics/drawable/Drawable;
    :cond_0
    return-void
.end method

.method static getDistance(Landroid/graphics/Rect;Landroid/graphics/Rect;I)I
    .locals 8
    .param p0, "source"    # Landroid/graphics/Rect;
    .param p1, "dest"    # Landroid/graphics/Rect;
    .param p2, "direction"    # I

    .prologue
    .line 2512
    sparse-switch p2, :sswitch_data_0

    .line 2545
    new-instance v6, Ljava/lang/IllegalArgumentException;

    const-string v7, "direction must be one of {FOCUS_UP, FOCUS_DOWN, FOCUS_LEFT, FOCUS_RIGHT, FOCUS_FORWARD, FOCUS_BACKWARD}."

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 2514
    :sswitch_0
    iget v4, p0, Landroid/graphics/Rect;->right:I

    .line 2515
    .local v4, "sX":I
    iget v6, p0, Landroid/graphics/Rect;->top:I

    invoke-virtual {p0}, Landroid/graphics/Rect;->height()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    add-int v5, v6, v7

    .line 2516
    .local v5, "sY":I
    iget v0, p1, Landroid/graphics/Rect;->left:I

    .line 2517
    .local v0, "dX":I
    iget v6, p1, Landroid/graphics/Rect;->top:I

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    add-int v1, v6, v7

    .line 2549
    .local v1, "dY":I
    :goto_0
    sub-int v2, v0, v4

    .line 2550
    .local v2, "deltaX":I
    sub-int v3, v1, v5

    .line 2551
    .local v3, "deltaY":I
    mul-int v6, v3, v3

    mul-int v7, v2, v2

    add-int/2addr v6, v7

    return v6

    .line 2520
    .end local v0    # "dX":I
    .end local v1    # "dY":I
    .end local v2    # "deltaX":I
    .end local v3    # "deltaY":I
    .end local v4    # "sX":I
    .end local v5    # "sY":I
    :sswitch_1
    iget v6, p0, Landroid/graphics/Rect;->left:I

    invoke-virtual {p0}, Landroid/graphics/Rect;->width()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    add-int v4, v6, v7

    .line 2521
    .restart local v4    # "sX":I
    iget v5, p0, Landroid/graphics/Rect;->bottom:I

    .line 2522
    .restart local v5    # "sY":I
    iget v6, p1, Landroid/graphics/Rect;->left:I

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    add-int v0, v6, v7

    .line 2523
    .restart local v0    # "dX":I
    iget v1, p1, Landroid/graphics/Rect;->top:I

    .line 2524
    .restart local v1    # "dY":I
    goto :goto_0

    .line 2526
    .end local v0    # "dX":I
    .end local v1    # "dY":I
    .end local v4    # "sX":I
    .end local v5    # "sY":I
    :sswitch_2
    iget v4, p0, Landroid/graphics/Rect;->left:I

    .line 2527
    .restart local v4    # "sX":I
    iget v6, p0, Landroid/graphics/Rect;->top:I

    invoke-virtual {p0}, Landroid/graphics/Rect;->height()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    add-int v5, v6, v7

    .line 2528
    .restart local v5    # "sY":I
    iget v0, p1, Landroid/graphics/Rect;->right:I

    .line 2529
    .restart local v0    # "dX":I
    iget v6, p1, Landroid/graphics/Rect;->top:I

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    add-int v1, v6, v7

    .line 2530
    .restart local v1    # "dY":I
    goto :goto_0

    .line 2532
    .end local v0    # "dX":I
    .end local v1    # "dY":I
    .end local v4    # "sX":I
    .end local v5    # "sY":I
    :sswitch_3
    iget v6, p0, Landroid/graphics/Rect;->left:I

    invoke-virtual {p0}, Landroid/graphics/Rect;->width()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    add-int v4, v6, v7

    .line 2533
    .restart local v4    # "sX":I
    iget v5, p0, Landroid/graphics/Rect;->top:I

    .line 2534
    .restart local v5    # "sY":I
    iget v6, p1, Landroid/graphics/Rect;->left:I

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    add-int v0, v6, v7

    .line 2535
    .restart local v0    # "dX":I
    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    .line 2536
    .restart local v1    # "dY":I
    goto :goto_0

    .line 2539
    .end local v0    # "dX":I
    .end local v1    # "dY":I
    .end local v4    # "sX":I
    .end local v5    # "sY":I
    :sswitch_4
    iget v6, p0, Landroid/graphics/Rect;->right:I

    invoke-virtual {p0}, Landroid/graphics/Rect;->width()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    add-int v4, v6, v7

    .line 2540
    .restart local v4    # "sX":I
    iget v6, p0, Landroid/graphics/Rect;->top:I

    invoke-virtual {p0}, Landroid/graphics/Rect;->height()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    add-int v5, v6, v7

    .line 2541
    .restart local v5    # "sY":I
    iget v6, p1, Landroid/graphics/Rect;->left:I

    invoke-virtual {p1}, Landroid/graphics/Rect;->width()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    add-int v0, v6, v7

    .line 2542
    .restart local v0    # "dX":I
    iget v6, p1, Landroid/graphics/Rect;->top:I

    invoke-virtual {p1}, Landroid/graphics/Rect;->height()I

    move-result v7

    div-int/lit8 v7, v7, 0x2

    add-int v1, v6, v7

    .line 2543
    .restart local v1    # "dY":I
    goto :goto_0

    .line 2512
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_4
        0x2 -> :sswitch_4
        0x11 -> :sswitch_2
        0x21 -> :sswitch_3
        0x42 -> :sswitch_0
        0x82 -> :sswitch_1
    .end sparse-switch
.end method

.method private initAbsListView()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 595
    invoke-virtual {p0, v2}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->setClickable(Z)V

    .line 596
    invoke-virtual {p0, v2}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->setFocusableInTouchMode(Z)V

    .line 597
    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->setWillNotDraw(Z)V

    .line 598
    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->setAlwaysDrawnWithCacheEnabled(Z)V

    .line 599
    invoke-virtual {p0, v2}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->setScrollingCacheEnabled(Z)V

    .line 601
    iget-object v3, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mContext:Landroid/content/Context;

    invoke-static {v3}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 602
    .local v0, "configuration":Landroid/view/ViewConfiguration;
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v3

    iput v3, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mTouchSlop:I

    .line 603
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v3

    iput v3, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mMinimumVelocity:I

    .line 604
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->density:F

    iput v3, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mDensityScale:F

    .line 605
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget v3, v3, Landroid/content/res/Configuration;->orientation:I

    const/4 v4, 0x2

    if-eq v3, v4, :cond_0

    move v1, v2

    :cond_0
    iput-boolean v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mPortraitOrientation:Z

    .line 607
    iput-boolean v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mScrollVertically:Z

    .line 610
    return-void
.end method

.method private orientationChanged()Z
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 629
    iget-boolean v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mPortraitOrientation:Z

    .line 630
    .local v1, "temp":Z
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    const/4 v5, 0x2

    if-eq v2, v5, :cond_1

    move v2, v3

    :goto_0
    iput-boolean v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mPortraitOrientation:Z

    .line 633
    iget-boolean v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mPortraitOrientation:Z

    if-eq v1, v2, :cond_2

    move v0, v3

    .line 634
    .local v0, "result":Z
    :goto_1
    if-eqz v0, :cond_0

    .line 635
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->setupScrollInfo()V

    .line 636
    iget-object v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mRecycler:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$RecycleBin;

    invoke-virtual {v2}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$RecycleBin;->scrapActiveViews()V

    .line 639
    :cond_0
    return v0

    .end local v0    # "result":Z
    :cond_1
    move v2, v4

    .line 630
    goto :goto_0

    :cond_2
    move v0, v4

    .line 633
    goto :goto_1
.end method

.method private performLongPress(Landroid/view/View;IJ)Z
    .locals 7
    .param p1, "child"    # Landroid/view/View;
    .param p2, "longPressPosition"    # I
    .param p3, "longPressId"    # J

    .prologue
    .line 1917
    const/4 v6, 0x0

    .line 1919
    .local v6, "handled":Z
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mOnItemLongClickListener:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView$OnItemLongClickListener;

    if-eqz v0, :cond_0

    .line 1920
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mOnItemLongClickListener:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView$OnItemLongClickListener;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move-wide v4, p3

    invoke-interface/range {v0 .. v5}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView$OnItemLongClickListener;->onItemLongClick(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;Landroid/view/View;IJ)Z

    move-result v6

    .line 1923
    :cond_0
    if-nez v6, :cond_1

    .line 1924
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->createContextMenuInfo(Landroid/view/View;IJ)Landroid/view/ContextMenu$ContextMenuInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mContextMenuInfo:Landroid/view/ContextMenu$ContextMenuInfo;

    .line 1925
    invoke-super {p0, p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->showContextMenuForChild(Landroid/view/View;)Z

    move-result v6

    .line 1927
    :cond_1
    if-eqz v6, :cond_2

    .line 1928
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->performHapticFeedback(I)Z

    .line 1930
    :cond_2
    return v6
.end method

.method private positionSelector(IIII)V
    .locals 5
    .param p1, "l"    # I
    .param p2, "t"    # I
    .param p3, "r"    # I
    .param p4, "b"    # I

    .prologue
    .line 1551
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSelectorRect:Landroid/graphics/Rect;

    iget v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSelectionLeftPadding:I

    sub-int v1, p1, v1

    iget v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSelectionTopPadding:I

    sub-int v2, p2, v2

    iget v3, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSelectionRightPadding:I

    add-int/2addr v3, p3

    iget v4, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSelectionBottomPadding:I

    add-int/2addr v4, p4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 1553
    return-void
.end method

.method private setCacheColorHint(I)V
    .locals 3
    .param p1, "color"    # I

    .prologue
    .line 2927
    iget v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mCacheColorHint:I

    if-eq p1, v2, :cond_1

    .line 2928
    iput p1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mCacheColorHint:I

    .line 2929
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildCount()I

    move-result v0

    .line 2930
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 2931
    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/view/View;->setDrawingCacheBackgroundColor(I)V

    .line 2930
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2933
    :cond_0
    iget-object v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mRecycler:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$RecycleBin;

    invoke-virtual {v2, p1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$RecycleBin;->setCacheColorHint(I)V

    .line 2935
    .end local v0    # "count":I
    .end local v1    # "i":I
    :cond_1
    return-void
.end method

.method private setupScrollInfo()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 615
    iget-boolean v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mPortraitOrientation:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mScrollVerticallyPortrait:Z

    :goto_0
    iput-boolean v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mScrollVertically:Z

    .line 616
    iget-boolean v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mScrollVertically:Z

    if-eqz v0, :cond_1

    .line 617
    new-instance v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler;-><init>(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;)V

    iput-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mTouchHandler:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;

    .line 618
    invoke-virtual {p0, v2}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->setVerticalScrollBarEnabled(Z)V

    .line 619
    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->setHorizontalScrollBarEnabled(Z)V

    .line 626
    :goto_1
    return-void

    .line 615
    :cond_0
    iget-boolean v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mScrollVerticallyLandscape:Z

    goto :goto_0

    .line 621
    :cond_1
    new-instance v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;-><init>(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;)V

    iput-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mTouchHandler:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;

    .line 622
    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->setVerticalScrollBarEnabled(Z)V

    .line 623
    invoke-virtual {p0, v2}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->setHorizontalScrollBarEnabled(Z)V

    goto :goto_1
.end method

.method private useDefaultSelector()V
    .locals 2

    .prologue
    .line 813
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x1080062

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->setSelector(Landroid/graphics/drawable/Drawable;)V

    .line 815
    return-void
.end method


# virtual methods
.method public addTouchables(Ljava/util/ArrayList;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2121
    .local p1, "views":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildCount()I

    move-result v2

    .line 2122
    .local v2, "count":I
    iget v3, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mFirstPosition:I

    .line 2123
    .local v3, "firstPosition":I
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mAdapter:Landroid/widget/ListAdapter;

    .line 2125
    .local v0, "adapter":Landroid/widget/ListAdapter;
    if-nez v0, :cond_1

    .line 2136
    :cond_0
    return-void

    .line 2129
    :cond_1
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    if-ge v4, v2, :cond_0

    .line 2130
    invoke-virtual {p0, v4}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 2131
    .local v1, "child":Landroid/view/View;
    add-int v5, v3, v4

    invoke-interface {v0, v5}, Landroid/widget/ListAdapter;->isEnabled(I)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 2132
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2134
    :cond_2
    invoke-virtual {v1, p1}, Landroid/view/View;->addTouchables(Ljava/util/ArrayList;)V

    .line 2129
    add-int/lit8 v4, v4, 0x1

    goto :goto_0
.end method

.method protected checkConsistency(I)Z
    .locals 10
    .param p1, "consistency"    # I

    .prologue
    .line 2979
    const/4 v4, 0x1

    .line 2981
    .local v4, "result":Z
    const/4 v1, 0x1

    .line 2985
    .local v1, "checkLayout":Z
    iget-object v7, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mRecycler:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$RecycleBin;

    # getter for: Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$RecycleBin;->mActiveViews:[Landroid/view/View;
    invoke-static {v7}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$RecycleBin;->access$900(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$RecycleBin;)[Landroid/view/View;

    move-result-object v0

    .line 2986
    .local v0, "activeViews":[Landroid/view/View;
    array-length v2, v0

    .line 2987
    .local v2, "count":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v2, :cond_1

    .line 2988
    aget-object v7, v0, v3

    if-eqz v7, :cond_0

    .line 2989
    const/4 v4, 0x0

    .line 2990
    const-string v7, "Consistency"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "AbsListView "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " has a view in its active recycler: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    aget-object v9, v0, v3

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2987
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 2997
    :cond_1
    iget-object v7, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mRecycler:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$RecycleBin;

    # getter for: Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$RecycleBin;->mCurrentScrap:Ljava/util/ArrayList;
    invoke-static {v7}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$RecycleBin;->access$1000(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$RecycleBin;)Ljava/util/ArrayList;

    move-result-object v5

    .line 2998
    .local v5, "scrap":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    invoke-direct {p0, v5}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->checkScrap(Ljava/util/ArrayList;)Z

    move-result v7

    if-nez v7, :cond_2

    const/4 v4, 0x0

    .line 2999
    :cond_2
    iget-object v7, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mRecycler:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$RecycleBin;

    # getter for: Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$RecycleBin;->mScrapViews:[Ljava/util/ArrayList;
    invoke-static {v7}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$RecycleBin;->access$1100(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$RecycleBin;)[Ljava/util/ArrayList;

    move-result-object v6

    .line 3000
    .local v6, "scraps":[Ljava/util/ArrayList;, "[Ljava/util/ArrayList<Landroid/view/View;>;"
    array-length v2, v6

    .line 3001
    const/4 v3, 0x0

    :goto_1
    if-ge v3, v2, :cond_4

    .line 3002
    aget-object v7, v6, v3

    invoke-direct {p0, v7}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->checkScrap(Ljava/util/ArrayList;)Z

    move-result v7

    if-nez v7, :cond_3

    const/4 v4, 0x0

    .line 3001
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 3006
    :cond_4
    return v4
.end method

.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1
    .param p1, "p"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    .line 2832
    instance-of v0, p1, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$LayoutParams;

    return v0
.end method

.method protected computeHorizontalScrollExtent()I
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 1149
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildCount()I

    move-result v0

    .line 1150
    .local v0, "count":I
    if-lez v0, :cond_3

    iget-boolean v7, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mScrollVertically:Z

    if-nez v7, :cond_3

    .line 1151
    iget-boolean v7, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSmoothScrollbarEnabled:Z

    if-eqz v7, :cond_2

    .line 1152
    mul-int/lit8 v1, v0, 0x64

    .line 1154
    .local v1, "extent":I
    invoke-virtual {p0, v6}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 1155
    .local v4, "view":Landroid/view/View;
    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    move-result v2

    .line 1156
    .local v2, "left":I
    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v5

    .line 1157
    .local v5, "width":I
    if-lez v5, :cond_0

    .line 1158
    mul-int/lit8 v6, v2, 0x64

    div-int/2addr v6, v5

    add-int/2addr v1, v6

    .line 1161
    :cond_0
    add-int/lit8 v6, v0, -0x1

    invoke-virtual {p0, v6}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 1162
    invoke-virtual {v4}, Landroid/view/View;->getRight()I

    move-result v3

    .line 1163
    .local v3, "right":I
    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v5

    .line 1164
    if-lez v5, :cond_1

    .line 1165
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getWidth()I

    move-result v6

    sub-int v6, v3, v6

    mul-int/lit8 v6, v6, 0x64

    div-int/2addr v6, v5

    sub-int/2addr v1, v6

    .line 1173
    .end local v1    # "extent":I
    .end local v2    # "left":I
    .end local v3    # "right":I
    .end local v4    # "view":Landroid/view/View;
    .end local v5    # "width":I
    :cond_1
    :goto_0
    return v1

    .line 1170
    :cond_2
    const/4 v1, 0x1

    goto :goto_0

    :cond_3
    move v1, v6

    .line 1173
    goto :goto_0
.end method

.method protected computeHorizontalScrollOffset()I
    .locals 11

    .prologue
    const/4 v7, 0x0

    .line 1178
    iget v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mFirstPosition:I

    .line 1179
    .local v2, "firstPosition":I
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildCount()I

    move-result v0

    .line 1180
    .local v0, "childCount":I
    if-ltz v2, :cond_0

    if-lez v0, :cond_0

    iget-boolean v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mScrollVertically:Z

    if-nez v8, :cond_0

    .line 1181
    iget-boolean v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSmoothScrollbarEnabled:Z

    if-eqz v8, :cond_1

    .line 1182
    invoke-virtual {p0, v7}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 1183
    .local v5, "view":Landroid/view/View;
    invoke-virtual {v5}, Landroid/view/View;->getLeft()I

    move-result v4

    .line 1184
    .local v4, "left":I
    invoke-virtual {v5}, Landroid/view/View;->getWidth()I

    move-result v6

    .line 1185
    .local v6, "width":I
    if-lez v6, :cond_0

    .line 1186
    mul-int/lit8 v8, v2, 0x64

    mul-int/lit8 v9, v4, 0x64

    div-int/2addr v9, v6

    sub-int/2addr v8, v9

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getScrollX()I

    move-result v9

    int-to-float v9, v9

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getWidth()I

    move-result v10

    int-to-float v10, v10

    div-float/2addr v9, v10

    iget v10, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mItemCount:I

    int-to-float v10, v10

    mul-float/2addr v9, v10

    const/high16 v10, 0x42c80000    # 100.0f

    mul-float/2addr v9, v10

    float-to-int v9, v9

    add-int/2addr v8, v9

    invoke-static {v8, v7}, Ljava/lang/Math;->max(II)I

    move-result v7

    .line 1202
    .end local v4    # "left":I
    .end local v5    # "view":Landroid/view/View;
    .end local v6    # "width":I
    :cond_0
    :goto_0
    return v7

    .line 1191
    :cond_1
    iget v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mItemCount:I

    .line 1192
    .local v1, "count":I
    if-nez v2, :cond_2

    .line 1193
    const/4 v3, 0x0

    .line 1199
    .local v3, "index":I
    :goto_1
    int-to-float v7, v2

    int-to-float v8, v0

    int-to-float v9, v3

    int-to-float v10, v1

    div-float/2addr v9, v10

    mul-float/2addr v8, v9

    add-float/2addr v7, v8

    float-to-int v7, v7

    goto :goto_0

    .line 1194
    .end local v3    # "index":I
    :cond_2
    add-int v7, v2, v0

    if-ne v7, v1, :cond_3

    .line 1195
    move v3, v1

    .restart local v3    # "index":I
    goto :goto_1

    .line 1197
    .end local v3    # "index":I
    :cond_3
    div-int/lit8 v7, v0, 0x2

    add-int v3, v2, v7

    .restart local v3    # "index":I
    goto :goto_1
.end method

.method protected computeHorizontalScrollRange()I
    .locals 3

    .prologue
    .line 1208
    iget-boolean v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mScrollVertically:Z

    if-eqz v1, :cond_0

    .line 1209
    const/4 v0, 0x0

    .line 1215
    .local v0, "result":I
    :goto_0
    return v0

    .line 1210
    .end local v0    # "result":I
    :cond_0
    iget-boolean v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSmoothScrollbarEnabled:Z

    if-eqz v1, :cond_1

    .line 1211
    iget v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mItemCount:I

    mul-int/lit8 v1, v1, 0x64

    const/4 v2, 0x0

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .restart local v0    # "result":I
    goto :goto_0

    .line 1213
    .end local v0    # "result":I
    :cond_1
    iget v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mItemCount:I

    .restart local v0    # "result":I
    goto :goto_0
.end method

.method protected computeVerticalScrollExtent()I
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 1078
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildCount()I

    move-result v1

    .line 1079
    .local v1, "count":I
    if-lez v1, :cond_3

    iget-boolean v7, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mScrollVertically:Z

    if-eqz v7, :cond_3

    .line 1080
    iget-boolean v7, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSmoothScrollbarEnabled:Z

    if-eqz v7, :cond_2

    .line 1081
    mul-int/lit8 v2, v1, 0x64

    .line 1083
    .local v2, "extent":I
    invoke-virtual {p0, v6}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 1084
    .local v5, "view":Landroid/view/View;
    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    move-result v4

    .line 1085
    .local v4, "top":I
    invoke-virtual {v5}, Landroid/view/View;->getHeight()I

    move-result v3

    .line 1086
    .local v3, "height":I
    if-lez v3, :cond_0

    .line 1087
    mul-int/lit8 v6, v4, 0x64

    div-int/2addr v6, v3

    add-int/2addr v2, v6

    .line 1090
    :cond_0
    add-int/lit8 v6, v1, -0x1

    invoke-virtual {p0, v6}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 1091
    invoke-virtual {v5}, Landroid/view/View;->getBottom()I

    move-result v0

    .line 1092
    .local v0, "bottom":I
    invoke-virtual {v5}, Landroid/view/View;->getHeight()I

    move-result v3

    .line 1093
    if-lez v3, :cond_1

    .line 1094
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getHeight()I

    move-result v6

    sub-int v6, v0, v6

    mul-int/lit8 v6, v6, 0x64

    div-int/2addr v6, v3

    sub-int/2addr v2, v6

    .line 1102
    .end local v0    # "bottom":I
    .end local v2    # "extent":I
    .end local v3    # "height":I
    .end local v4    # "top":I
    .end local v5    # "view":Landroid/view/View;
    :cond_1
    :goto_0
    return v2

    .line 1099
    :cond_2
    const/4 v2, 0x1

    goto :goto_0

    :cond_3
    move v2, v6

    .line 1102
    goto :goto_0
.end method

.method protected computeVerticalScrollOffset()I
    .locals 11

    .prologue
    const/4 v7, 0x0

    .line 1107
    iget v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mFirstPosition:I

    .line 1108
    .local v2, "firstPosition":I
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildCount()I

    move-result v0

    .line 1109
    .local v0, "childCount":I
    if-ltz v2, :cond_0

    if-lez v0, :cond_0

    iget-boolean v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mScrollVertically:Z

    if-eqz v8, :cond_0

    .line 1110
    iget-boolean v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSmoothScrollbarEnabled:Z

    if-eqz v8, :cond_1

    .line 1111
    invoke-virtual {p0, v7}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    .line 1112
    .local v6, "view":Landroid/view/View;
    invoke-virtual {v6}, Landroid/view/View;->getTop()I

    move-result v5

    .line 1113
    .local v5, "top":I
    invoke-virtual {v6}, Landroid/view/View;->getHeight()I

    move-result v3

    .line 1114
    .local v3, "height":I
    if-lez v3, :cond_0

    .line 1115
    mul-int/lit8 v8, v2, 0x64

    mul-int/lit8 v9, v5, 0x64

    div-int/2addr v9, v3

    sub-int/2addr v8, v9

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getScrollY()I

    move-result v9

    int-to-float v9, v9

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getHeight()I

    move-result v10

    int-to-float v10, v10

    div-float/2addr v9, v10

    iget v10, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mItemCount:I

    int-to-float v10, v10

    mul-float/2addr v9, v10

    const/high16 v10, 0x42c80000    # 100.0f

    mul-float/2addr v9, v10

    float-to-int v9, v9

    add-int/2addr v8, v9

    invoke-static {v8, v7}, Ljava/lang/Math;->max(II)I

    move-result v7

    .line 1131
    .end local v3    # "height":I
    .end local v5    # "top":I
    .end local v6    # "view":Landroid/view/View;
    :cond_0
    :goto_0
    return v7

    .line 1120
    :cond_1
    iget v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mItemCount:I

    .line 1121
    .local v1, "count":I
    if-nez v2, :cond_2

    .line 1122
    const/4 v4, 0x0

    .line 1128
    .local v4, "index":I
    :goto_1
    int-to-float v7, v2

    int-to-float v8, v0

    int-to-float v9, v4

    int-to-float v10, v1

    div-float/2addr v9, v10

    mul-float/2addr v8, v9

    add-float/2addr v7, v8

    float-to-int v7, v7

    goto :goto_0

    .line 1123
    .end local v4    # "index":I
    :cond_2
    add-int v7, v2, v0

    if-ne v7, v1, :cond_3

    .line 1124
    move v4, v1

    .restart local v4    # "index":I
    goto :goto_1

    .line 1126
    .end local v4    # "index":I
    :cond_3
    div-int/lit8 v7, v0, 0x2

    add-int v4, v2, v7

    .restart local v4    # "index":I
    goto :goto_1
.end method

.method protected computeVerticalScrollRange()I
    .locals 3

    .prologue
    .line 1137
    iget-boolean v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mScrollVertically:Z

    if-nez v1, :cond_0

    .line 1138
    const/4 v0, 0x0

    .line 1144
    .local v0, "result":I
    :goto_0
    return v0

    .line 1139
    .end local v0    # "result":I
    :cond_0
    iget-boolean v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSmoothScrollbarEnabled:Z

    if-eqz v1, :cond_1

    .line 1140
    iget v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mItemCount:I

    mul-int/lit8 v1, v1, 0x64

    const/4 v2, 0x0

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v0

    .restart local v0    # "result":I
    goto :goto_0

    .line 1142
    .end local v0    # "result":I
    :cond_1
    iget v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mItemCount:I

    .restart local v0    # "result":I
    goto :goto_0
.end method

.method createContextMenuInfo(Landroid/view/View;IJ)Landroid/view/ContextMenu$ContextMenuInfo;
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "position"    # I
    .param p3, "id"    # J

    .prologue
    .line 1829
    new-instance v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView$AdapterContextMenuInfo;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView$AdapterContextMenuInfo;-><init>(Landroid/view/View;IJ)V

    return-object v0
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 2
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 1557
    const/4 v1, 0x0

    .line 1571
    .local v1, "saveCount":I
    iget-boolean v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mDrawSelectorOnTop:Z

    .line 1572
    .local v0, "drawSelectorOnTop":Z
    if-nez v0, :cond_0

    .line 1573
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->drawSelector(Landroid/graphics/Canvas;)V

    .line 1576
    :cond_0
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->dispatchDraw(Landroid/graphics/Canvas;)V

    .line 1578
    if-eqz v0, :cond_1

    .line 1579
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->drawSelector(Landroid/graphics/Canvas;)V

    .line 1586
    :cond_1
    return-void
.end method

.method protected dispatchSetPressed(Z)V
    .locals 0
    .param p1, "pressed"    # Z

    .prologue
    .line 1995
    return-void
.end method

.method protected drawableStateChanged()V
    .locals 2

    .prologue
    .line 1735
    invoke-super {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->drawableStateChanged()V

    .line 1736
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSelector:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 1737
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSelector:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 1739
    :cond_0
    return-void
.end method

.method abstract fillGap(Z)V
.end method

.method findClosestMotionRow(I)I
    .locals 3
    .param p1, "x"    # I

    .prologue
    const/4 v2, -0x1

    .line 2293
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildCount()I

    move-result v0

    .line 2294
    .local v0, "childCount":I
    if-nez v0, :cond_1

    move v1, v2

    .line 2299
    :cond_0
    :goto_0
    return v1

    .line 2298
    :cond_1
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->findMotionRowX(I)I

    move-result v1

    .line 2299
    .local v1, "motionRow":I
    if-ne v1, v2, :cond_0

    iget v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mFirstPosition:I

    add-int/2addr v2, v0

    add-int/lit8 v1, v2, -0x1

    goto :goto_0
.end method

.method findClosestMotionRowY(I)I
    .locals 3
    .param p1, "y"    # I

    .prologue
    const/4 v2, -0x1

    .line 2269
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildCount()I

    move-result v0

    .line 2270
    .local v0, "childCount":I
    if-nez v0, :cond_1

    move v1, v2

    .line 2275
    :cond_0
    :goto_0
    return v1

    .line 2274
    :cond_1
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->findMotionRowY(I)I

    move-result v1

    .line 2275
    .local v1, "motionRow":I
    if-ne v1, v2, :cond_0

    iget v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mFirstPosition:I

    add-int/2addr v2, v0

    add-int/lit8 v1, v2, -0x1

    goto :goto_0
.end method

.method abstract findMotionRowX(I)I
.end method

.method abstract findMotionRowY(I)I
.end method

.method public bridge synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1
    .param p1, "x0"    # Landroid/util/AttributeSet;

    .prologue
    .line 85
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->generateLayoutParams(Landroid/util/AttributeSet;)Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1
    .param p1, "p"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    .line 2822
    new-instance v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$LayoutParams;

    invoke-direct {v0, p1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$LayoutParams;
    .locals 2
    .param p1, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 2827
    new-instance v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$LayoutParams;

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method protected getBottomFadingEdgeStrength()F
    .locals 8

    .prologue
    .line 1238
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildCount()I

    move-result v1

    .line 1239
    .local v1, "count":I
    invoke-super {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->getBottomFadingEdgeStrength()F

    move-result v2

    .line 1240
    .local v2, "fadeEdge":F
    if-eqz v1, :cond_0

    iget-boolean v6, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mScrollVertically:Z

    if-nez v6, :cond_1

    .line 1251
    .end local v2    # "fadeEdge":F
    :cond_0
    :goto_0
    return v2

    .line 1243
    .restart local v2    # "fadeEdge":F
    :cond_1
    iget v6, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mFirstPosition:I

    add-int/2addr v6, v1

    add-int/lit8 v6, v6, -0x1

    iget v7, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mItemCount:I

    add-int/lit8 v7, v7, -0x1

    if-ge v6, v7, :cond_2

    .line 1244
    const/high16 v2, 0x3f800000    # 1.0f

    goto :goto_0

    .line 1247
    :cond_2
    add-int/lit8 v6, v1, -0x1

    invoke-virtual {p0, v6}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/View;->getBottom()I

    move-result v0

    .line 1248
    .local v0, "bottom":I
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getHeight()I

    move-result v4

    .line 1249
    .local v4, "height":I
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getVerticalFadingEdgeLength()I

    move-result v6

    int-to-float v3, v6

    .line 1250
    .local v3, "fadeLength":F
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getPaddingBottom()I

    move-result v5

    .line 1251
    .local v5, "paddingBottom":I
    sub-int v6, v4, v5

    if-le v0, v6, :cond_0

    sub-int v6, v0, v4

    add-int/2addr v6, v5

    int-to-float v6, v6

    div-float v2, v6, v3

    goto :goto_0
.end method

.method public getCacheColorHint()I
    .locals 1

    .prologue
    .line 2944
    iget v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mCacheColorHint:I

    return v0
.end method

.method protected getContextMenuInfo()Landroid/view/ContextMenu$ContextMenuInfo;
    .locals 1

    .prologue
    .line 1935
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mContextMenuInfo:Landroid/view/ContextMenu$ContextMenuInfo;

    return-object v0
.end method

.method public getFocusedRect(Landroid/graphics/Rect;)V
    .locals 2
    .param p1, "r"    # Landroid/graphics/Rect;

    .prologue
    .line 800
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getSelectedView()Landroid/view/View;

    move-result-object v0

    .line 801
    .local v0, "view":Landroid/view/View;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-ne v1, p0, :cond_0

    .line 804
    invoke-virtual {v0, p1}, Landroid/view/View;->getFocusedRect(Landroid/graphics/Rect;)V

    .line 805
    invoke-virtual {p0, v0, p1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 810
    :goto_0
    return-void

    .line 808
    :cond_0
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->getFocusedRect(Landroid/graphics/Rect;)V

    goto :goto_0
.end method

.method getFooterViewsCount()I
    .locals 1

    .prologue
    .line 2211
    const/4 v0, 0x0

    return v0
.end method

.method getHeaderViewsCount()I
    .locals 1

    .prologue
    .line 2201
    const/4 v0, 0x0

    return v0
.end method

.method protected getLeftFadingEdgeStrength()F
    .locals 6

    .prologue
    .line 1258
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildCount()I

    move-result v0

    .line 1259
    .local v0, "count":I
    invoke-super {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->getLeftFadingEdgeStrength()F

    move-result v1

    .line 1260
    .local v1, "fadeEdge":F
    if-eqz v0, :cond_0

    iget-boolean v5, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mScrollVertically:Z

    if-eqz v5, :cond_1

    .line 1270
    .end local v1    # "fadeEdge":F
    :cond_0
    :goto_0
    return v1

    .line 1263
    .restart local v1    # "fadeEdge":F
    :cond_1
    iget v5, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mFirstPosition:I

    if-lez v5, :cond_2

    .line 1264
    const/high16 v1, 0x3f800000    # 1.0f

    goto :goto_0

    .line 1267
    :cond_2
    const/4 v5, 0x0

    invoke-virtual {p0, v5}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getLeft()I

    move-result v3

    .line 1268
    .local v3, "left":I
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getHorizontalFadingEdgeLength()I

    move-result v5

    int-to-float v2, v5

    .line 1269
    .local v2, "fadeLength":F
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getPaddingLeft()I

    move-result v4

    .line 1270
    .local v4, "paddingLeft":I
    if-ge v3, v4, :cond_0

    sub-int v5, v3, v4

    neg-int v5, v5

    int-to-float v5, v5

    div-float v1, v5, v2

    goto :goto_0
.end method

.method public getListPaddingBottom()I
    .locals 1

    .prologue
    .line 1451
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mListPadding:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    return v0
.end method

.method public getListPaddingLeft()I
    .locals 1

    .prologue
    .line 1463
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mListPadding:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    return v0
.end method

.method public getListPaddingRight()I
    .locals 1

    .prologue
    .line 1475
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mListPadding:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    return v0
.end method

.method public getListPaddingTop()I
    .locals 1

    .prologue
    .line 1439
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mListPadding:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    return v0
.end method

.method protected getRightFadingEdgeStrength()F
    .locals 8

    .prologue
    .line 1276
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildCount()I

    move-result v0

    .line 1277
    .local v0, "count":I
    invoke-super {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->getRightFadingEdgeStrength()F

    move-result v1

    .line 1278
    .local v1, "fadeEdge":F
    if-eqz v0, :cond_0

    iget-boolean v6, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mScrollVertically:Z

    if-eqz v6, :cond_1

    .line 1289
    .end local v1    # "fadeEdge":F
    :cond_0
    :goto_0
    return v1

    .line 1281
    .restart local v1    # "fadeEdge":F
    :cond_1
    iget v6, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mFirstPosition:I

    add-int/2addr v6, v0

    add-int/lit8 v6, v6, -0x1

    iget v7, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mItemCount:I

    add-int/lit8 v7, v7, -0x1

    if-ge v6, v7, :cond_2

    .line 1282
    const/high16 v1, 0x3f800000    # 1.0f

    goto :goto_0

    .line 1285
    :cond_2
    add-int/lit8 v6, v0, -0x1

    invoke-virtual {p0, v6}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/View;->getRight()I

    move-result v4

    .line 1286
    .local v4, "right":I
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getWidth()I

    move-result v5

    .line 1287
    .local v5, "width":I
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getHorizontalFadingEdgeLength()I

    move-result v6

    int-to-float v2, v6

    .line 1288
    .local v2, "fadeLength":F
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getPaddingRight()I

    move-result v3

    .line 1289
    .local v3, "paddingRight":I
    sub-int v6, v5, v3

    if-le v4, v6, :cond_0

    sub-int v6, v4, v5

    add-int/2addr v6, v3

    int-to-float v6, v6

    div-float v1, v6, v2

    goto :goto_0
.end method

.method public getScrollDirectionLandscape()I
    .locals 1

    .prologue
    .line 2912
    iget-boolean v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mScrollVerticallyLandscape:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getScrollDirectionPortrait()I
    .locals 1

    .prologue
    .line 2884
    iget-boolean v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mScrollVerticallyPortrait:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getSelectedView()Landroid/view/View;
    .locals 2
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation

    .prologue
    .line 1423
    iget v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mItemCount:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSelectedPosition:I

    if-ltz v0, :cond_0

    .line 1424
    iget v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSelectedPosition:I

    iget v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mFirstPosition:I

    sub-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1426
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getSelector()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 1681
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSelector:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getSolidColor()I
    .locals 1

    .prologue
    .line 2917
    iget v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mCacheColorHint:I

    return v0
.end method

.method protected getTopFadingEdgeStrength()F
    .locals 6

    .prologue
    .line 1220
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildCount()I

    move-result v0

    .line 1221
    .local v0, "count":I
    invoke-super {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->getTopFadingEdgeStrength()F

    move-result v1

    .line 1222
    .local v1, "fadeEdge":F
    if-eqz v0, :cond_0

    iget-boolean v5, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mScrollVertically:Z

    if-nez v5, :cond_1

    .line 1232
    .end local v1    # "fadeEdge":F
    :cond_0
    :goto_0
    return v1

    .line 1225
    .restart local v1    # "fadeEdge":F
    :cond_1
    iget v5, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mFirstPosition:I

    if-lez v5, :cond_2

    .line 1226
    const/high16 v1, 0x3f800000    # 1.0f

    goto :goto_0

    .line 1229
    :cond_2
    const/4 v5, 0x0

    invoke-virtual {p0, v5}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    move-result v4

    .line 1230
    .local v4, "top":I
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getVerticalFadingEdgeLength()I

    move-result v5

    int-to-float v2, v5

    .line 1231
    .local v2, "fadeLength":F
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getPaddingTop()I

    move-result v3

    .line 1232
    .local v3, "paddintTop":I
    if-ge v4, v3, :cond_0

    sub-int v5, v4, v3

    neg-int v5, v5

    int-to-float v5, v5

    div-float v1, v5, v2

    goto :goto_0
.end method

.method public final getTranscriptMode()I
    .locals 1

    .prologue
    .line 2856
    iget v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mTranscriptMode:I

    return v0
.end method

.method protected handleDataChanged()V
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/4 v9, -0x1

    const/4 v8, 0x5

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2330
    iget v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mItemCount:I

    .line 2331
    .local v0, "count":I
    if-lez v0, :cond_b

    .line 2338
    iget-boolean v4, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mNeedSync:Z

    if-eqz v4, :cond_3

    .line 2340
    iput-boolean v6, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mNeedSync:Z

    .line 2342
    iget v4, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mTranscriptMode:I

    if-eq v4, v10, :cond_0

    iget v4, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mTranscriptMode:I

    if-ne v4, v7, :cond_2

    iget v4, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mFirstPosition:I

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildCount()I

    move-result v5

    add-int/2addr v4, v5

    iget v5, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mOldItemCount:I

    if-lt v4, v5, :cond_2

    .line 2345
    :cond_0
    const/4 v4, 0x3

    iput v4, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mLayoutMode:I

    .line 2441
    :cond_1
    :goto_0
    return-void

    .line 2349
    :cond_2
    iget v4, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSyncMode:I

    packed-switch v4, :pswitch_data_0

    .line 2397
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->isInTouchMode()Z

    move-result v4

    if-nez v4, :cond_a

    .line 2399
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getSelectedItemPosition()I

    move-result v1

    .line 2402
    .local v1, "newPos":I
    if-lt v1, v0, :cond_4

    .line 2403
    add-int/lit8 v1, v0, -0x1

    .line 2405
    :cond_4
    if-gez v1, :cond_5

    .line 2406
    const/4 v1, 0x0

    .line 2410
    :cond_5
    invoke-virtual {p0, v1, v7}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->lookForSelectablePosition(IZ)I

    move-result v2

    .line 2412
    .local v2, "selectablePos":I
    if-ltz v2, :cond_9

    .line 2413
    invoke-virtual {p0, v2}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->setNextSelectedPositionInt(I)V

    goto :goto_0

    .line 2351
    .end local v1    # "newPos":I
    .end local v2    # "selectablePos":I
    :pswitch_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->isInTouchMode()Z

    move-result v4

    if-eqz v4, :cond_6

    .line 2356
    iput v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mLayoutMode:I

    .line 2357
    iget v4, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSyncPosition:I

    invoke-static {v6, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    add-int/lit8 v5, v0, -0x1

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    iput v4, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSyncPosition:I

    goto :goto_0

    .line 2363
    :cond_6
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->findSyncPosition()I

    move-result v1

    .line 2364
    .restart local v1    # "newPos":I
    if-ltz v1, :cond_3

    .line 2366
    invoke-virtual {p0, v1, v7}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->lookForSelectablePosition(IZ)I

    move-result v2

    .line 2367
    .restart local v2    # "selectablePos":I
    if-ne v2, v1, :cond_3

    .line 2369
    iput v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSyncPosition:I

    .line 2370
    iget-boolean v4, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mIsVertical:Z

    if-eqz v4, :cond_7

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getHeight()I

    move-result v3

    .line 2371
    .local v3, "size":I
    :goto_1
    iget-wide v4, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSyncSize:J

    int-to-long v6, v3

    cmp-long v4, v4, v6

    if-nez v4, :cond_8

    .line 2374
    iput v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mLayoutMode:I

    .line 2382
    :goto_2
    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->setNextSelectedPositionInt(I)V

    goto :goto_0

    .line 2370
    .end local v3    # "size":I
    :cond_7
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getWidth()I

    move-result v3

    goto :goto_1

    .line 2378
    .restart local v3    # "size":I
    :cond_8
    iput v10, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mLayoutMode:I

    goto :goto_2

    .line 2390
    .end local v1    # "newPos":I
    .end local v2    # "selectablePos":I
    .end local v3    # "size":I
    :pswitch_1
    iput v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mLayoutMode:I

    .line 2391
    iget v4, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSyncPosition:I

    invoke-static {v6, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    add-int/lit8 v5, v0, -0x1

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v4

    iput v4, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSyncPosition:I

    goto :goto_0

    .line 2417
    .restart local v1    # "newPos":I
    .restart local v2    # "selectablePos":I
    :cond_9
    invoke-virtual {p0, v1, v6}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->lookForSelectablePosition(IZ)I

    move-result v2

    .line 2418
    if-ltz v2, :cond_b

    .line 2419
    invoke-virtual {p0, v2}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->setNextSelectedPositionInt(I)V

    goto :goto_0

    .line 2426
    .end local v1    # "newPos":I
    .end local v2    # "selectablePos":I
    :cond_a
    iget v4, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mResurrectToPosition:I

    if-gez v4, :cond_1

    .line 2434
    :cond_b
    iput v6, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mLayoutMode:I

    .line 2435
    iput v9, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSelectedPosition:I

    .line 2436
    const-wide/high16 v4, -0x8000000000000000L

    iput-wide v4, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSelectedRowId:J

    .line 2437
    iput v9, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mNextSelectedPosition:I

    .line 2438
    const-wide/high16 v4, -0x8000000000000000L

    iput-wide v4, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mNextSelectedRowId:J

    .line 2439
    iput-boolean v6, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mNeedSync:Z

    .line 2440
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->checkSelectionChanged()V

    goto/16 :goto_0

    .line 2349
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method hideSelector()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 2225
    iget v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSelectedPosition:I

    if-eq v0, v2, :cond_2

    .line 2226
    iget v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mLayoutMode:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    .line 2227
    iget v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSelectedPosition:I

    iput v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mResurrectToPosition:I

    .line 2229
    :cond_0
    iget v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mNextSelectedPosition:I

    if-ltz v0, :cond_1

    iget v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mNextSelectedPosition:I

    iget v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSelectedPosition:I

    if-eq v0, v1, :cond_1

    .line 2230
    iget v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mNextSelectedPosition:I

    iput v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mResurrectToPosition:I

    .line 2232
    :cond_1
    invoke-virtual {p0, v2}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->setSelectedPositionInt(I)V

    .line 2233
    invoke-virtual {p0, v2}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->setNextSelectedPositionInt(I)V

    .line 2234
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSelectedTop:I

    .line 2235
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSelectorRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 2237
    :cond_2
    return-void
.end method

.method public invalidateViews()V
    .locals 1

    .prologue
    .line 2306
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mDataChanged:Z

    .line 2309
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->invalidate()V

    .line 2310
    return-void
.end method

.method invokeOnItemScrollListener()V
    .locals 4

    .prologue
    .line 732
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mOnScrollListener:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$OnScrollListener;

    if-eqz v0, :cond_0

    .line 733
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mOnScrollListener:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$OnScrollListener;

    iget v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mFirstPosition:I

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildCount()I

    move-result v2

    iget v3, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mItemCount:I

    invoke-interface {v0, p0, v1, v2, v3}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$OnScrollListener;->onScroll(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;III)V

    .line 735
    :cond_0
    return-void
.end method

.method public isScrollingCacheEnabled()Z
    .locals 1
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation

    .prologue
    .line 748
    iget-boolean v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mScrollingCacheEnabled:Z

    return v0
.end method

.method public isSmoothScrollbarEnabled()Z
    .locals 1
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation

    .prologue
    .line 712
    iget-boolean v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSmoothScrollbarEnabled:Z

    return v0
.end method

.method public isStackFromBottom()Z
    .locals 1
    .annotation runtime Landroid/view/ViewDebug$ExportedProperty;
    .end annotation

    .prologue
    .line 825
    iget-boolean v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mStackFromBottom:Z

    return v0
.end method

.method keyPressed()V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 1689
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->isEnabled()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->isClickable()Z

    move-result v5

    if-nez v5, :cond_1

    .line 1724
    :cond_0
    :goto_0
    return-void

    .line 1693
    :cond_1
    iget-object v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSelector:Landroid/graphics/drawable/Drawable;

    .line 1694
    .local v2, "selector":Landroid/graphics/drawable/Drawable;
    iget-object v3, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSelectorRect:Landroid/graphics/Rect;

    .line 1695
    .local v3, "selectorRect":Landroid/graphics/Rect;
    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->isFocused()Z

    move-result v5

    if-nez v5, :cond_2

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->touchModeDrawsInPressedState()Z

    move-result v5

    if-eqz v5, :cond_0

    :cond_2
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_0

    .line 1698
    iget v5, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSelectedPosition:I

    iget v6, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mFirstPosition:I

    sub-int/2addr v5, v6

    invoke-virtual {p0, v5}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 1700
    .local v4, "v":Landroid/view/View;
    if-eqz v4, :cond_3

    .line 1701
    invoke-virtual {v4}, Landroid/view/View;->hasFocusable()Z

    move-result v5

    if-nez v5, :cond_0

    .line 1702
    invoke-virtual {v4, v7}, Landroid/view/View;->setPressed(Z)V

    .line 1704
    :cond_3
    invoke-virtual {p0, v7}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->setPressed(Z)V

    .line 1706
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->isLongClickable()Z

    move-result v1

    .line 1707
    .local v1, "longClickable":Z
    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getCurrent()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 1708
    .local v0, "d":Landroid/graphics/drawable/Drawable;
    if-eqz v0, :cond_4

    instance-of v5, v0, Landroid/graphics/drawable/TransitionDrawable;

    if-eqz v5, :cond_4

    .line 1709
    if-eqz v1, :cond_6

    .line 1710
    check-cast v0, Landroid/graphics/drawable/TransitionDrawable;

    .end local v0    # "d":Landroid/graphics/drawable/Drawable;
    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    move-result v5

    invoke-virtual {v0, v5}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    .line 1716
    :cond_4
    :goto_1
    if-eqz v1, :cond_0

    iget-boolean v5, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mDataChanged:Z

    if-nez v5, :cond_0

    .line 1717
    iget-object v5, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mPendingCheckForKeyLongPress:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$CheckForKeyLongPress;

    if-nez v5, :cond_5

    .line 1718
    new-instance v5, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$CheckForKeyLongPress;

    const/4 v6, 0x0

    invoke-direct {v5, p0, v6}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$CheckForKeyLongPress;-><init>(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$1;)V

    iput-object v5, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mPendingCheckForKeyLongPress:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$CheckForKeyLongPress;

    .line 1720
    :cond_5
    iget-object v5, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mPendingCheckForKeyLongPress:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$CheckForKeyLongPress;

    invoke-virtual {v5}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$CheckForKeyLongPress;->rememberWindowAttachCount()V

    .line 1721
    iget-object v5, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mPendingCheckForKeyLongPress:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$CheckForKeyLongPress;

    invoke-static {}, Landroid/view/ViewConfiguration;->getLongPressTimeout()I

    move-result v6

    int-to-long v6, v6

    invoke-virtual {p0, v5, v6, v7}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 1713
    .restart local v0    # "d":Landroid/graphics/drawable/Drawable;
    :cond_6
    check-cast v0, Landroid/graphics/drawable/TransitionDrawable;

    .end local v0    # "d":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v0}, Landroid/graphics/drawable/TransitionDrawable;->resetTransition()V

    goto :goto_1
.end method

.method protected layoutChildren()V
    .locals 0

    .prologue
    .line 1352
    return-void
.end method

.method obtainView(I[Z)Landroid/view/View;
    .locals 4
    .param p1, "position"    # I
    .param p2, "isScrap"    # [Z

    .prologue
    const/4 v3, 0x0

    .line 1491
    aput-boolean v3, p2, v3

    .line 1494
    iget-object v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mRecycler:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$RecycleBin;

    invoke-virtual {v2, p1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$RecycleBin;->getScrapView(I)Landroid/view/View;

    move-result-object v1

    .line 1497
    .local v1, "scrapView":Landroid/view/View;
    if-eqz v1, :cond_2

    .line 1503
    iget-object v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v2, p1, v1, p0}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 1510
    .local v0, "child":Landroid/view/View;
    if-eq v0, v1, :cond_1

    .line 1511
    iget-object v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mRecycler:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$RecycleBin;

    invoke-virtual {v2, v1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$RecycleBin;->addScrapView(Landroid/view/View;)V

    .line 1512
    iget v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mCacheColorHint:I

    if-eqz v2, :cond_0

    .line 1513
    iget v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mCacheColorHint:I

    invoke-virtual {v0, v2}, Landroid/view/View;->setDrawingCacheBackgroundColor(I)V

    .line 1534
    :cond_0
    :goto_0
    return-object v0

    .line 1520
    :cond_1
    const/4 v2, 0x1

    aput-boolean v2, p2, v3

    .line 1521
    invoke-virtual {v0}, Landroid/view/View;->onFinishTemporaryDetach()V

    goto :goto_0

    .line 1524
    .end local v0    # "child":Landroid/view/View;
    :cond_2
    iget-object v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mAdapter:Landroid/widget/ListAdapter;

    const/4 v3, 0x0

    invoke-interface {v2, p1, v3, p0}, Landroid/widget/ListAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 1525
    .restart local v0    # "child":Landroid/view/View;
    iget v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mCacheColorHint:I

    if-eqz v2, :cond_0

    .line 1526
    iget v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mCacheColorHint:I

    invoke-virtual {v0, v2}, Landroid/view/View;->setDrawingCacheBackgroundColor(I)V

    goto :goto_0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 1782
    invoke-super {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->onAttachedToWindow()V

    .line 1784
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 1785
    .local v0, "treeObserver":Landroid/view/ViewTreeObserver;
    if-eqz v0, :cond_0

    .line 1786
    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->addOnTouchModeChangeListener(Landroid/view/ViewTreeObserver$OnTouchModeChangeListener;)V

    .line 1792
    :cond_0
    return-void
.end method

.method protected onCreateDrawableState(I)[I
    .locals 6
    .param p1, "extraSpace"    # I

    .prologue
    .line 1744
    iget-boolean v4, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mIsChildViewEnabled:Z

    if-eqz v4, :cond_1

    .line 1746
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->onCreateDrawableState(I)[I

    move-result-object v3

    .line 1772
    :cond_0
    :goto_0
    return-object v3

    .line 1752
    :cond_1
    sget-object v4, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->ENABLED_STATE_SET:[I

    const/4 v5, 0x0

    aget v1, v4, v5

    .line 1757
    .local v1, "enabledState":I
    add-int/lit8 v4, p1, 0x1

    invoke-super {p0, v4}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->onCreateDrawableState(I)[I

    move-result-object v3

    .line 1758
    .local v3, "state":[I
    const/4 v0, -0x1

    .line 1759
    .local v0, "enabledPos":I
    array-length v4, v3

    add-int/lit8 v2, v4, -0x1

    .local v2, "i":I
    :goto_1
    if-ltz v2, :cond_2

    .line 1760
    aget v4, v3, v2

    if-ne v4, v1, :cond_3

    .line 1761
    move v0, v2

    .line 1767
    :cond_2
    if-ltz v0, :cond_0

    .line 1768
    add-int/lit8 v4, v0, 0x1

    array-length v5, v3

    sub-int/2addr v5, v0

    add-int/lit8 v5, v5, -0x1

    invoke-static {v3, v4, v3, v0, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0

    .line 1759
    :cond_3
    add-int/lit8 v2, v2, -0x1

    goto :goto_1
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    .line 1796
    invoke-super {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->onDetachedFromWindow()V

    .line 1802
    iget-object v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mRecycler:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$RecycleBin;

    invoke-virtual {v1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$RecycleBin;->clear()V

    .line 1804
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    .line 1805
    .local v0, "treeObserver":Landroid/view/ViewTreeObserver;
    if-eqz v0, :cond_0

    .line 1806
    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnTouchModeChangeListener(Landroid/view/ViewTreeObserver$OnTouchModeChangeListener;)V

    .line 1813
    :cond_0
    return-void
.end method

.method protected onFocusChanged(ZILandroid/graphics/Rect;)V
    .locals 1
    .param p1, "gainFocus"    # Z
    .param p2, "direction"    # I
    .param p3, "previouslyFocusedRect"    # Landroid/graphics/Rect;

    .prologue
    .line 1046
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->onFocusChanged(ZILandroid/graphics/Rect;)V

    .line 1047
    if-eqz p1, :cond_0

    iget v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSelectedPosition:I

    if-gez v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->isInTouchMode()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1048
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->resurrectSelection()Z

    .line 1050
    :cond_0
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 2113
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mTouchHandler:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 1963
    const/4 v0, 0x0

    return v0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 7
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v1, 0x1

    const/4 v6, 0x0

    .line 1968
    sparse-switch p1, :sswitch_data_0

    .line 1988
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v1

    :cond_1
    :goto_0
    return v1

    .line 1971
    :sswitch_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->isEnabled()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1974
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->isClickable()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->isPressed()Z

    move-result v2

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSelectedPosition:I

    if-ltz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mAdapter:Landroid/widget/ListAdapter;

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSelectedPosition:I

    iget-object v3, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v3}, Landroid/widget/ListAdapter;->getCount()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 1978
    iget v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSelectedPosition:I

    iget v3, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mFirstPosition:I

    sub-int/2addr v2, v3

    invoke-virtual {p0, v2}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1979
    .local v0, "view":Landroid/view/View;
    if-eqz v0, :cond_2

    .line 1980
    iget v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSelectedPosition:I

    iget-wide v4, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSelectedRowId:J

    invoke-virtual {p0, v0, v2, v4, v5}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->performItemClick(Landroid/view/View;IJ)Z

    .line 1981
    invoke-virtual {v0, v6}, Landroid/view/View;->setPressed(Z)V

    .line 1983
    :cond_2
    invoke-virtual {p0, v6}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->setPressed(Z)V

    goto :goto_0

    .line 1968
    nop

    :sswitch_data_0
    .sparse-switch
        0x17 -> :sswitch_0
        0x42 -> :sswitch_0
    .end sparse-switch
.end method

.method protected onLayout(ZIIII)V
    .locals 3
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 1314
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->orientationChanged()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1315
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->setupScrollInfo()V

    .line 1317
    :cond_0
    invoke-super/range {p0 .. p5}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->onLayout(ZIIII)V

    .line 1318
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mInLayout:Z

    .line 1319
    if-eqz p1, :cond_2

    .line 1320
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildCount()I

    move-result v0

    .line 1321
    .local v0, "childCount":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 1322
    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/View;->forceLayout()V

    .line 1321
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1324
    :cond_1
    iget-object v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mRecycler:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$RecycleBin;

    invoke-virtual {v2}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$RecycleBin;->markChildrenDirty()V

    .line 1327
    .end local v0    # "childCount":I
    .end local v1    # "i":I
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->layoutChildren()V

    .line 1328
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mInLayout:Z

    .line 1329
    return-void
.end method

.method protected onMeasure(II)V
    .locals 3
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 1296
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->orientationChanged()Z

    .line 1298
    iget-object v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSelector:Landroid/graphics/drawable/Drawable;

    if-nez v1, :cond_0

    .line 1299
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->useDefaultSelector()V

    .line 1301
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mListPadding:Landroid/graphics/Rect;

    .line 1302
    .local v0, "listPadding":Landroid/graphics/Rect;
    iget v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSelectionLeftPadding:I

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getPaddingLeft()I

    move-result v2

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 1303
    iget v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSelectionTopPadding:I

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getPaddingTop()I

    move-result v2

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 1304
    iget v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSelectionRightPadding:I

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getPaddingRight()I

    move-result v2

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 1305
    iget v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSelectionBottomPadding:I

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getPaddingBottom()I

    move-result v2

    add-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 1306
    return-void
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 8
    .param p1, "state"    # Landroid/os/Parcelable;

    .prologue
    const-wide/16 v6, 0x0

    const/4 v5, -0x1

    const/4 v4, 0x1

    .line 971
    move-object v0, p1

    check-cast v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$SavedState;

    .line 973
    .local v0, "ss":Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$SavedState;
    invoke-virtual {v0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-super {p0, v1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 974
    iput-boolean v4, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mDataChanged:Z

    .line 976
    iget v1, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$SavedState;->height:I

    int-to-long v2, v1

    iput-wide v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSyncSize:J

    .line 978
    iget-wide v2, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$SavedState;->selectedId:J

    cmp-long v1, v2, v6

    if-ltz v1, :cond_1

    .line 979
    iput-boolean v4, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mNeedSync:Z

    .line 980
    iget-wide v2, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$SavedState;->selectedId:J

    iput-wide v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSyncRowId:J

    .line 981
    iget v1, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$SavedState;->position:I

    iput v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSyncPosition:I

    .line 982
    iget v1, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$SavedState;->viewTop:I

    iput v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSpecificTop:I

    .line 983
    const/4 v1, 0x0

    iput v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSyncMode:I

    .line 997
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->requestLayout()V

    .line 998
    return-void

    .line 984
    :cond_1
    iget-wide v2, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$SavedState;->firstId:J

    cmp-long v1, v2, v6

    if-ltz v1, :cond_0

    .line 985
    invoke-virtual {p0, v5}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->setSelectedPositionInt(I)V

    .line 987
    invoke-virtual {p0, v5}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->setNextSelectedPositionInt(I)V

    .line 988
    iput-boolean v4, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mNeedSync:Z

    .line 989
    iget-wide v2, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$SavedState;->firstId:J

    iput-wide v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSyncRowId:J

    .line 990
    iget v1, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$SavedState;->position:I

    iput v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSyncPosition:I

    .line 991
    iget v1, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$SavedState;->viewTop:I

    iput v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSpecificTop:I

    .line 992
    iput v4, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSyncMode:I

    goto :goto_0
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 12

    .prologue
    const-wide/16 v10, -0x1

    const/4 v6, 0x0

    .line 923
    invoke-super {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v4

    .line 925
    .local v4, "superState":Landroid/os/Parcelable;
    new-instance v1, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$SavedState;

    invoke-direct {v1, v4}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 927
    .local v1, "ss":Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$SavedState;
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildCount()I

    move-result v7

    if-lez v7, :cond_0

    const/4 v0, 0x1

    .line 928
    .local v0, "haveChildren":Z
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getSelectedItemId()J

    move-result-wide v2

    .line 929
    .local v2, "selectedId":J
    iput-wide v2, v1, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$SavedState;->selectedId:J

    .line 930
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getHeight()I

    move-result v7

    iput v7, v1, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$SavedState;->height:I

    .line 932
    const-wide/16 v8, 0x0

    cmp-long v7, v2, v8

    if-ltz v7, :cond_1

    .line 934
    iget v6, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSelectedTop:I

    iput v6, v1, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$SavedState;->viewTop:I

    .line 935
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getSelectedItemPosition()I

    move-result v6

    iput v6, v1, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$SavedState;->position:I

    .line 936
    iput-wide v10, v1, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$SavedState;->firstId:J

    .line 966
    :goto_1
    return-object v1

    .end local v0    # "haveChildren":Z
    .end local v2    # "selectedId":J
    :cond_0
    move v0, v6

    .line 927
    goto :goto_0

    .line 938
    .restart local v0    # "haveChildren":Z
    .restart local v2    # "selectedId":J
    :cond_1
    if-eqz v0, :cond_3

    .line 940
    invoke-virtual {p0, v6}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    .line 941
    .local v5, "v":Landroid/view/View;
    iget-boolean v6, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mScrollVertically:Z

    if-eqz v6, :cond_2

    .line 942
    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    move-result v6

    iput v6, v1, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$SavedState;->viewTop:I

    .line 946
    :goto_2
    iget v6, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mFirstPosition:I

    iput v6, v1, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$SavedState;->position:I

    .line 947
    iget-object v6, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mAdapter:Landroid/widget/ListAdapter;

    iget v7, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mFirstPosition:I

    invoke-interface {v6, v7}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v6

    iput-wide v6, v1, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$SavedState;->firstId:J

    goto :goto_1

    .line 944
    :cond_2
    invoke-virtual {v5}, Landroid/view/View;->getLeft()I

    move-result v6

    iput v6, v1, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$SavedState;->viewTop:I

    goto :goto_2

    .line 949
    .end local v5    # "v":Landroid/view/View;
    :cond_3
    iput v6, v1, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$SavedState;->viewTop:I

    .line 950
    iput-wide v10, v1, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$SavedState;->firstId:J

    .line 951
    iput v6, v1, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$SavedState;->position:I

    goto :goto_1
.end method

.method protected onSizeChanged(IIII)V
    .locals 1
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 1590
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 1591
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mDataChanged:Z

    .line 1592
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->rememberSyncState()V

    .line 1598
    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 2099
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mTouchHandler:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onTouchModeChanged(Z)V
    .locals 1
    .param p1, "isInTouchMode"    # Z

    .prologue
    .line 2094
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mTouchHandler:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->onTouchModeChanged(Z)V

    .line 2095
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 1
    .param p1, "hasWindowFocus"    # Z

    .prologue
    .line 2157
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->onWindowFocusChanged(Z)V

    .line 2158
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mTouchHandler:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->onWindowFocusChanged(Z)V

    .line 2159
    return-void
.end method

.method public pointToPosition(II)I
    .locals 5
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 2006
    iget-object v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mTouchFrame:Landroid/graphics/Rect;

    .line 2007
    .local v2, "frame":Landroid/graphics/Rect;
    if-nez v2, :cond_0

    .line 2008
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    iput-object v4, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mTouchFrame:Landroid/graphics/Rect;

    .line 2009
    iget-object v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mTouchFrame:Landroid/graphics/Rect;

    .line 2012
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildCount()I

    move-result v1

    .line 2013
    .local v1, "count":I
    add-int/lit8 v3, v1, -0x1

    .local v3, "i":I
    :goto_0
    if-ltz v3, :cond_2

    .line 2014
    invoke-virtual {p0, v3}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 2015
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-nez v4, :cond_1

    .line 2016
    invoke-virtual {v0, v2}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 2017
    invoke-virtual {v2, p1, p2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 2018
    iget v4, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mFirstPosition:I

    add-int/2addr v4, v3

    .line 2022
    .end local v0    # "child":Landroid/view/View;
    :goto_1
    return v4

    .line 2013
    .restart local v0    # "child":Landroid/view/View;
    :cond_1
    add-int/lit8 v3, v3, -0x1

    goto :goto_0

    .line 2022
    .end local v0    # "child":Landroid/view/View;
    :cond_2
    const/4 v4, -0x1

    goto :goto_1
.end method

.method public pointToRowId(II)J
    .locals 4
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 2035
    invoke-virtual {p0, p1, p2}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->pointToPosition(II)I

    move-result v0

    .line 2036
    .local v0, "position":I
    if-ltz v0, :cond_0

    .line 2037
    iget-object v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v1, v0}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v2

    .line 2039
    :goto_0
    return-wide v2

    :cond_0
    const-wide/high16 v2, -0x8000000000000000L

    goto :goto_0
.end method

.method positionSelector(Landroid/view/View;)V
    .locals 6
    .param p1, "sel"    # Landroid/view/View;

    .prologue
    .line 1538
    iget-object v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSelectorRect:Landroid/graphics/Rect;

    .line 1539
    .local v1, "selectorRect":Landroid/graphics/Rect;
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v2

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v3

    invoke-virtual {p1}, Landroid/view/View;->getRight()I

    move-result v4

    invoke-virtual {p1}, Landroid/view/View;->getBottom()I

    move-result v5

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/Rect;->set(IIII)V

    .line 1540
    iget v2, v1, Landroid/graphics/Rect;->left:I

    iget v3, v1, Landroid/graphics/Rect;->top:I

    iget v4, v1, Landroid/graphics/Rect;->right:I

    iget v5, v1, Landroid/graphics/Rect;->bottom:I

    invoke-direct {p0, v2, v3, v4, v5}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->positionSelector(IIII)V

    .line 1543
    iget-boolean v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mIsChildViewEnabled:Z

    .line 1544
    .local v0, "isChildViewEnabled":Z
    invoke-virtual {p1}, Landroid/view/View;->isEnabled()Z

    move-result v2

    if-eq v2, v0, :cond_0

    .line 1545
    if-nez v0, :cond_1

    const/4 v2, 0x1

    :goto_0
    iput-boolean v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mIsChildViewEnabled:Z

    .line 1546
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->refreshDrawableState()V

    .line 1548
    :cond_0
    return-void

    .line 1545
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public reclaimViews(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 2955
    .local p1, "views":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildCount()I

    move-result v1

    .line 2956
    .local v1, "childCount":I
    iget-object v5, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mRecycler:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$RecycleBin;

    # getter for: Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$RecycleBin;->mRecyclerListener:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$RecyclerListener;
    invoke-static {v5}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$RecycleBin;->access$800(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$RecycleBin;)Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$RecyclerListener;

    move-result-object v3

    .line 2959
    .local v3, "listener":Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$RecyclerListener;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_1

    .line 2960
    invoke-virtual {p0, v2}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 2961
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$LayoutParams;

    .line 2963
    .local v4, "lp":Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$LayoutParams;
    if-eqz v4, :cond_0

    iget-object v5, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mRecycler:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$RecycleBin;

    iget v6, v4, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$LayoutParams;->viewType:I

    invoke-virtual {v5, v6}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$RecycleBin;->shouldRecycleViewType(I)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 2964
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2965
    if-eqz v3, :cond_0

    .line 2967
    invoke-interface {v3, v0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$RecyclerListener;->onMovedToScrapHeap(Landroid/view/View;)V

    .line 2959
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2971
    .end local v0    # "child":Landroid/view/View;
    .end local v4    # "lp":Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$LayoutParams;
    :cond_1
    iget-object v5, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mRecycler:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$RecycleBin;

    invoke-virtual {v5, p1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$RecycleBin;->reclaimScrapViews(Ljava/util/List;)V

    .line 2972
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->removeAllViewsInLayout()V

    .line 2973
    return-void
.end method

.method reconcileSelectedPosition()I
    .locals 2

    .prologue
    .line 2245
    iget v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSelectedPosition:I

    .line 2246
    .local v0, "position":I
    if-gez v0, :cond_0

    .line 2247
    iget v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mResurrectToPosition:I

    .line 2249
    :cond_0
    const/4 v1, 0x0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 2250
    iget v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mItemCount:I

    add-int/lit8 v1, v1, -0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 2251
    return v0
.end method

.method reportScrollStateChange(I)V
    .locals 1
    .param p1, "newState"    # I

    .prologue
    .line 2147
    iget v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mLastScrollState:I

    if-eq p1, v0, :cond_0

    .line 2148
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mOnScrollListener:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$OnScrollListener;

    if-eqz v0, :cond_0

    .line 2149
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mOnScrollListener:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$OnScrollListener;

    invoke-interface {v0, p0, p1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$OnScrollListener;->onScrollStateChanged(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;I)V

    .line 2150
    iput p1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mLastScrollState:I

    .line 2153
    :cond_0
    return-void
.end method

.method public requestLayout()V
    .locals 1

    .prologue
    .line 1054
    iget-boolean v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mBlockLayoutRequests:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mInLayout:Z

    if-nez v0, :cond_0

    .line 1055
    invoke-super {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->requestLayout()V

    .line 1057
    :cond_0
    return-void
.end method

.method requestLayoutIfNecessary()V
    .locals 1

    .prologue
    .line 843
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 844
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->resetList()V

    .line 845
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->requestLayout()V

    .line 846
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->invalidate()V

    .line 848
    :cond_0
    return-void
.end method

.method resetList()V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x0

    .line 1063
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->removeAllViewsInLayout()V

    .line 1064
    iput v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mFirstPosition:I

    .line 1065
    iput-boolean v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mDataChanged:Z

    .line 1066
    iput-boolean v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mNeedSync:Z

    .line 1067
    iput v3, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mOldSelectedPosition:I

    .line 1068
    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mOldSelectedRowId:J

    .line 1069
    invoke-virtual {p0, v3}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->setSelectedPositionInt(I)V

    .line 1070
    invoke-virtual {p0, v3}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->setNextSelectedPositionInt(I)V

    .line 1071
    iput v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSelectedTop:I

    .line 1072
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSelectorRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    .line 1073
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->invalidate()V

    .line 1074
    return-void
.end method

.method resurrectSelection()Z
    .locals 1

    .prologue
    .line 2325
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mTouchHandler:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;

    invoke-virtual {v0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->resurrectSelection()Z

    move-result v0

    return v0
.end method

.method public setDrawSelectorOnTop(Z)V
    .locals 0
    .param p1, "onTop"    # Z

    .prologue
    .line 1644
    iput-boolean p1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mDrawSelectorOnTop:Z

    .line 1645
    return-void
.end method

.method public setOnScrollListener(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$OnScrollListener;)V
    .locals 0
    .param p1, "l"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$OnScrollListener;

    .prologue
    .line 721
    iput-object p1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mOnScrollListener:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$OnScrollListener;

    .line 722
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->invokeOnItemScrollListener()V

    .line 723
    return-void
.end method

.method public setRecyclerListener(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$RecyclerListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$RecyclerListener;

    .prologue
    .line 3043
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mRecycler:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$RecycleBin;

    # setter for: Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$RecycleBin;->mRecyclerListener:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$RecyclerListener;
    invoke-static {v0, p1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$RecycleBin;->access$802(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$RecycleBin;Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$RecyclerListener;)Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$RecyclerListener;

    .line 3044
    return-void
.end method

.method public setScrollDirectionLandscape(I)V
    .locals 2
    .param p1, "direction"    # I

    .prologue
    .line 2896
    iget-boolean v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mScrollVerticallyLandscape:Z

    .line 2897
    .local v0, "tempDirection":Z
    if-nez p1, :cond_1

    const/4 v1, 0x1

    :goto_0
    iput-boolean v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mScrollVerticallyLandscape:Z

    .line 2898
    iget-boolean v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mScrollVerticallyLandscape:Z

    if-eq v0, v1, :cond_0

    .line 2899
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->setupScrollInfo()V

    .line 2901
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->resetList()V

    .line 2902
    iget-object v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mRecycler:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$RecycleBin;

    invoke-virtual {v1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$RecycleBin;->clear()V

    .line 2904
    :cond_0
    return-void

    .line 2897
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setScrollDirectionPortrait(I)V
    .locals 2
    .param p1, "direction"    # I

    .prologue
    .line 2868
    iget-boolean v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mScrollVerticallyPortrait:Z

    .line 2869
    .local v0, "tempDirection":Z
    if-nez p1, :cond_1

    const/4 v1, 0x1

    :goto_0
    iput-boolean v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mScrollVerticallyPortrait:Z

    .line 2870
    iget-boolean v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mScrollVerticallyPortrait:Z

    if-eq v0, v1, :cond_0

    .line 2871
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->setupScrollInfo()V

    .line 2873
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->resetList()V

    .line 2874
    iget-object v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mRecycler:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$RecycleBin;

    invoke-virtual {v1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$RecycleBin;->clear()V

    .line 2876
    :cond_0
    return-void

    .line 2869
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setScrollIndicators(Landroid/view/View;Landroid/view/View;Landroid/view/View;Landroid/view/View;)V
    .locals 0
    .param p1, "up"    # Landroid/view/View;
    .param p2, "down"    # Landroid/view/View;
    .param p3, "left"    # Landroid/view/View;
    .param p4, "right"    # Landroid/view/View;

    .prologue
    .line 1727
    iput-object p1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mScrollUp:Landroid/view/View;

    .line 1728
    iput-object p2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mScrollDown:Landroid/view/View;

    .line 1729
    iput-object p3, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mScrollLeft:Landroid/view/View;

    .line 1730
    iput-object p4, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mScrollRight:Landroid/view/View;

    .line 1731
    return-void
.end method

.method public final setScrollingCacheEnabled(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 765
    iget-boolean v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mScrollingCacheEnabled:Z

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    .line 766
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mTouchHandler:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;

    invoke-virtual {v0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->clearScrollingCache()V

    .line 768
    :cond_0
    iput-boolean p1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mScrollingCacheEnabled:Z

    .line 769
    return-void
.end method

.method abstract setSelectionInt(I)V
.end method

.method public final setSelector(I)V
    .locals 1
    .param p1, "resID"    # I

    .prologue
    .line 1655
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->setSelector(Landroid/graphics/drawable/Drawable;)V

    .line 1656
    return-void
.end method

.method public final setSelector(Landroid/graphics/drawable/Drawable;)V
    .locals 3
    .param p1, "sel"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 1659
    iget-object v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSelector:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_0

    .line 1660
    iget-object v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSelector:Landroid/graphics/drawable/Drawable;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 1661
    iget-object v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSelector:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->unscheduleDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 1663
    :cond_0
    iput-object p1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSelector:Landroid/graphics/drawable/Drawable;

    .line 1664
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    .line 1665
    .local v0, "padding":Landroid/graphics/Rect;
    invoke-virtual {p1, v0}, Landroid/graphics/drawable/Drawable;->getPadding(Landroid/graphics/Rect;)Z

    .line 1666
    iget v1, v0, Landroid/graphics/Rect;->left:I

    iput v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSelectionLeftPadding:I

    .line 1667
    iget v1, v0, Landroid/graphics/Rect;->top:I

    iput v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSelectionTopPadding:I

    .line 1668
    iget v1, v0, Landroid/graphics/Rect;->right:I

    iput v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSelectionRightPadding:I

    .line 1669
    iget v1, v0, Landroid/graphics/Rect;->bottom:I

    iput v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSelectionBottomPadding:I

    .line 1670
    invoke-virtual {p1, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 1671
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    .line 1672
    return-void
.end method

.method public final setSmoothScrollbarEnabled(Z)V
    .locals 0
    .param p1, "enabled"    # Z

    .prologue
    .line 700
    iput-boolean p1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSmoothScrollbarEnabled:Z

    .line 701
    return-void
.end method

.method public final setStackFromBottom(Z)V
    .locals 1
    .param p1, "stackFromBottom"    # Z

    .prologue
    .line 836
    iget-boolean v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mStackFromBottom:Z

    if-eq v0, p1, :cond_0

    .line 837
    iput-boolean p1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mStackFromBottom:Z

    .line 838
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->requestLayoutIfNecessary()V

    .line 840
    :cond_0
    return-void
.end method

.method public final setTranscriptMode(I)V
    .locals 0
    .param p1, "mode"    # I

    .prologue
    .line 2846
    iput p1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mTranscriptMode:I

    .line 2847
    return-void
.end method

.method shouldShowSelector()Z
    .locals 1

    .prologue
    .line 1623
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->isInTouchMode()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->touchModeDrawsInPressedState()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public showContextMenuForChild(Landroid/view/View;)Z
    .locals 7
    .param p1, "originalView"    # Landroid/view/View;

    .prologue
    .line 1940
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getPositionForView(Landroid/view/View;)I

    move-result v3

    .line 1941
    .local v3, "longPressPosition":I
    if-ltz v3, :cond_2

    .line 1942
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mAdapter:Landroid/widget/ListAdapter;

    invoke-interface {v0, v3}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v4

    .line 1943
    .local v4, "longPressId":J
    const/4 v6, 0x0

    .line 1945
    .local v6, "handled":Z
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mOnItemLongClickListener:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView$OnItemLongClickListener;

    if-eqz v0, :cond_0

    .line 1946
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mOnItemLongClickListener:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView$OnItemLongClickListener;

    move-object v1, p0

    move-object v2, p1

    invoke-interface/range {v0 .. v5}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView$OnItemLongClickListener;->onItemLongClick(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;Landroid/view/View;IJ)Z

    move-result v6

    .line 1949
    :cond_0
    if-nez v6, :cond_1

    .line 1950
    iget v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mFirstPosition:I

    sub-int v0, v3, v0

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0, v3, v4, v5}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->createContextMenuInfo(Landroid/view/View;IJ)Landroid/view/ContextMenu$ContextMenuInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mContextMenuInfo:Landroid/view/ContextMenu$ContextMenuInfo;

    .line 1953
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->showContextMenuForChild(Landroid/view/View;)Z

    move-result v6

    .line 1958
    .end local v4    # "longPressId":J
    .end local v6    # "handled":Z
    :cond_1
    :goto_0
    return v6

    :cond_2
    const/4 v6, 0x0

    goto :goto_0
.end method

.method public smoothScrollBy(II)V
    .locals 1
    .param p1, "distance"    # I
    .param p2, "duration"    # I

    .prologue
    .line 2190
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mTouchHandler:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->smoothScrollBy(II)V

    .line 2191
    return-void
.end method

.method public smoothScrollToPosition(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 2168
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mTouchHandler:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->smoothScrollToPosition(I)V

    .line 2169
    return-void
.end method

.method public smoothScrollToPosition(II)V
    .locals 1
    .param p1, "position"    # I
    .param p2, "boundPosition"    # I

    .prologue
    .line 2181
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mTouchHandler:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->smoothScrollToPosition(II)V

    .line 2182
    return-void
.end method

.method public startScrollIfNeeded(I)Z
    .locals 1
    .param p1, "delta"    # I

    .prologue
    .line 2089
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mTouchHandler:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->startScrollIfNeeded(I)Z

    move-result v0

    return v0
.end method

.method touchModeDrawsInPressedState()Z
    .locals 1

    .prologue
    .line 1606
    iget v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mTouchMode:I

    packed-switch v0, :pswitch_data_0

    .line 1611
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 1609
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 1606
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method updateScrollIndicators()V
    .locals 12

    .prologue
    const/4 v9, 0x4

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 1355
    iget-object v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mScrollUp:Landroid/view/View;

    if-eqz v8, :cond_1

    iget-boolean v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mScrollVertically:Z

    if-eqz v8, :cond_1

    .line 1358
    iget v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mFirstPosition:I

    if-lez v8, :cond_8

    move v3, v6

    .line 1361
    .local v3, "canScrollUp":Z
    :goto_0
    if-nez v3, :cond_0

    .line 1362
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildCount()I

    move-result v8

    if-lez v8, :cond_0

    .line 1363
    invoke-virtual {p0, v7}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 1364
    .local v4, "child":Landroid/view/View;
    invoke-virtual {v4}, Landroid/view/View;->getTop()I

    move-result v8

    iget-object v10, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mListPadding:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->top:I

    if-ge v8, v10, :cond_9

    move v3, v6

    .line 1368
    .end local v4    # "child":Landroid/view/View;
    :cond_0
    :goto_1
    iget-object v10, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mScrollUp:Landroid/view/View;

    if-eqz v3, :cond_a

    move v8, v7

    :goto_2
    invoke-virtual {v10, v8}, Landroid/view/View;->setVisibility(I)V

    .line 1371
    .end local v3    # "canScrollUp":Z
    :cond_1
    iget-object v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mScrollDown:Landroid/view/View;

    if-eqz v8, :cond_3

    iget-boolean v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mScrollVertically:Z

    if-eqz v8, :cond_3

    .line 1373
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildCount()I

    move-result v5

    .line 1376
    .local v5, "count":I
    iget v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mFirstPosition:I

    add-int/2addr v8, v5

    iget v10, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mItemCount:I

    if-ge v8, v10, :cond_b

    move v0, v6

    .line 1379
    .local v0, "canScrollDown":Z
    :goto_3
    if-nez v0, :cond_2

    if-lez v5, :cond_2

    .line 1380
    add-int/lit8 v8, v5, -0x1

    invoke-virtual {p0, v8}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 1381
    .restart local v4    # "child":Landroid/view/View;
    invoke-virtual {v4}, Landroid/view/View;->getBottom()I

    move-result v8

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getBottom()I

    move-result v10

    iget-object v11, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mListPadding:Landroid/graphics/Rect;

    iget v11, v11, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v10, v11

    if-le v8, v10, :cond_c

    move v0, v6

    .line 1384
    .end local v4    # "child":Landroid/view/View;
    :cond_2
    :goto_4
    iget-object v10, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mScrollDown:Landroid/view/View;

    if-eqz v0, :cond_d

    move v8, v7

    :goto_5
    invoke-virtual {v10, v8}, Landroid/view/View;->setVisibility(I)V

    .line 1387
    .end local v0    # "canScrollDown":Z
    .end local v5    # "count":I
    :cond_3
    iget-object v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mScrollLeft:Landroid/view/View;

    if-eqz v8, :cond_5

    iget-boolean v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mScrollVertically:Z

    if-nez v8, :cond_5

    .line 1390
    iget v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mFirstPosition:I

    if-lez v8, :cond_e

    move v1, v6

    .line 1393
    .local v1, "canScrollLeft":Z
    :goto_6
    if-nez v1, :cond_4

    .line 1394
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildCount()I

    move-result v8

    if-lez v8, :cond_4

    .line 1395
    invoke-virtual {p0, v7}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 1396
    .restart local v4    # "child":Landroid/view/View;
    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    move-result v8

    iget-object v10, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mListPadding:Landroid/graphics/Rect;

    iget v10, v10, Landroid/graphics/Rect;->left:I

    if-ge v8, v10, :cond_f

    move v1, v6

    .line 1400
    .end local v4    # "child":Landroid/view/View;
    :cond_4
    :goto_7
    iget-object v10, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mScrollLeft:Landroid/view/View;

    if-eqz v1, :cond_10

    move v8, v7

    :goto_8
    invoke-virtual {v10, v8}, Landroid/view/View;->setVisibility(I)V

    .line 1403
    .end local v1    # "canScrollLeft":Z
    :cond_5
    iget-object v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mScrollRight:Landroid/view/View;

    if-eqz v8, :cond_7

    iget-boolean v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mScrollVertically:Z

    if-nez v8, :cond_7

    .line 1405
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildCount()I

    move-result v5

    .line 1408
    .restart local v5    # "count":I
    iget v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mFirstPosition:I

    add-int/2addr v8, v5

    iget v10, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mItemCount:I

    if-ge v8, v10, :cond_11

    move v2, v6

    .line 1411
    .local v2, "canScrollRight":Z
    :goto_9
    if-nez v2, :cond_6

    if-lez v5, :cond_6

    .line 1412
    add-int/lit8 v8, v5, -0x1

    invoke-virtual {p0, v8}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 1413
    .restart local v4    # "child":Landroid/view/View;
    invoke-virtual {v4}, Landroid/view/View;->getRight()I

    move-result v8

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getRight()I

    move-result v10

    iget-object v11, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mListPadding:Landroid/graphics/Rect;

    iget v11, v11, Landroid/graphics/Rect;->right:I

    sub-int/2addr v10, v11

    if-le v8, v10, :cond_12

    move v2, v6

    .line 1416
    .end local v4    # "child":Landroid/view/View;
    :cond_6
    :goto_a
    iget-object v6, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mScrollRight:Landroid/view/View;

    if-eqz v2, :cond_13

    :goto_b
    invoke-virtual {v6, v7}, Landroid/view/View;->setVisibility(I)V

    .line 1418
    .end local v2    # "canScrollRight":Z
    .end local v5    # "count":I
    :cond_7
    return-void

    :cond_8
    move v3, v7

    .line 1358
    goto/16 :goto_0

    .restart local v3    # "canScrollUp":Z
    .restart local v4    # "child":Landroid/view/View;
    :cond_9
    move v3, v7

    .line 1364
    goto/16 :goto_1

    .end local v4    # "child":Landroid/view/View;
    :cond_a
    move v8, v9

    .line 1368
    goto/16 :goto_2

    .end local v3    # "canScrollUp":Z
    .restart local v5    # "count":I
    :cond_b
    move v0, v7

    .line 1376
    goto/16 :goto_3

    .restart local v0    # "canScrollDown":Z
    .restart local v4    # "child":Landroid/view/View;
    :cond_c
    move v0, v7

    .line 1381
    goto :goto_4

    .end local v4    # "child":Landroid/view/View;
    :cond_d
    move v8, v9

    .line 1384
    goto :goto_5

    .end local v0    # "canScrollDown":Z
    .end local v5    # "count":I
    :cond_e
    move v1, v7

    .line 1390
    goto :goto_6

    .restart local v1    # "canScrollLeft":Z
    .restart local v4    # "child":Landroid/view/View;
    :cond_f
    move v1, v7

    .line 1396
    goto :goto_7

    .end local v4    # "child":Landroid/view/View;
    :cond_10
    move v8, v9

    .line 1400
    goto :goto_8

    .end local v1    # "canScrollLeft":Z
    .restart local v5    # "count":I
    :cond_11
    move v2, v7

    .line 1408
    goto :goto_9

    .restart local v2    # "canScrollRight":Z
    .restart local v4    # "child":Landroid/view/View;
    :cond_12
    move v2, v7

    .line 1413
    goto :goto_a

    .end local v4    # "child":Landroid/view/View;
    :cond_13
    move v7, v9

    .line 1416
    goto :goto_b
.end method

.method public verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1
    .param p1, "dr"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 1777
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSelector:Landroid/graphics/drawable/Drawable;

    if-eq v0, p1, :cond_0

    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
