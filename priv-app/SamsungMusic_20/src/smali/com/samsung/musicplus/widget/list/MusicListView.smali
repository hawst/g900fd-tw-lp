.class public Lcom/samsung/musicplus/widget/list/MusicListView;
.super Landroid/widget/ListView;
.source "MusicListView.java"

# interfaces
.implements Lcom/samsung/musicplus/base/list/IActionMode;


# instance fields
.field private mActionModeHelper:Lcom/samsung/musicplus/widget/list/ActionModeHelper;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 32
    invoke-direct {p0, p1}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    .line 33
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/list/MusicListView;->ensureActionMode()V

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 38
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/list/MusicListView;->ensureActionMode()V

    .line 39
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 42
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 43
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/list/MusicListView;->ensureActionMode()V

    .line 44
    return-void
.end method

.method private ensureActionMode()V
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/MusicListView;->mActionModeHelper:Lcom/samsung/musicplus/widget/list/ActionModeHelper;

    if-nez v0, :cond_0

    .line 59
    new-instance v0, Lcom/samsung/musicplus/widget/list/ActionModeHelper;

    invoke-direct {v0}, Lcom/samsung/musicplus/widget/list/ActionModeHelper;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/widget/list/MusicListView;->mActionModeHelper:Lcom/samsung/musicplus/widget/list/ActionModeHelper;

    .line 61
    :cond_0
    return-void
.end method

.method private performItemClickTempChoiceMode(Landroid/view/View;IJ)Z
    .locals 3
    .param p1, "view"    # Landroid/view/View;
    .param p2, "position"    # I
    .param p3, "id"    # J

    .prologue
    .line 87
    const/4 v0, 0x0

    .line 88
    .local v0, "handled":Z
    const/4 v1, 0x0

    invoke-super {p0, v1}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 89
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ListView;->performItemClick(Landroid/view/View;IJ)Z

    move-result v0

    .line 90
    const/4 v1, 0x2

    invoke-super {p0, v1}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 91
    return v0
.end method


# virtual methods
.method public performItemClick(Landroid/view/View;IJ)Z
    .locals 5
    .param p1, "view"    # Landroid/view/View;
    .param p2, "position"    # I
    .param p3, "id"    # J

    .prologue
    const/4 v0, 0x1

    .line 72
    iget-object v1, p0, Lcom/samsung/musicplus/widget/list/MusicListView;->mActionModeHelper:Lcom/samsung/musicplus/widget/list/ActionModeHelper;

    invoke-virtual {v1, p1, p2, p3, p4}, Lcom/samsung/musicplus/widget/list/ActionModeHelper;->interceptItemClick(Landroid/view/View;IJ)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 81
    :cond_0
    :goto_0
    return v0

    .line 75
    :cond_1
    iget-object v1, p0, Lcom/samsung/musicplus/widget/list/MusicListView;->mActionModeHelper:Lcom/samsung/musicplus/widget/list/ActionModeHelper;

    invoke-virtual {v1}, Lcom/samsung/musicplus/widget/list/ActionModeHelper;->isCustomChoiceMode()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 76
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/musicplus/widget/list/MusicListView;->performItemClickTempChoiceMode(Landroid/view/View;IJ)Z

    move-result v0

    goto :goto_0

    .line 78
    :cond_2
    iget-object v1, p0, Lcom/samsung/musicplus/widget/list/MusicListView;->mActionModeHelper:Lcom/samsung/musicplus/widget/list/ActionModeHelper;

    invoke-virtual {v1}, Lcom/samsung/musicplus/widget/list/ActionModeHelper;->isMultipleChoiceModeEnabled()Z

    move-result v1

    if-eqz v1, :cond_3

    const-wide/16 v2, 0x0

    cmp-long v1, p3, v2

    if-ltz v1, :cond_0

    .line 81
    :cond_3
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ListView;->performItemClick(Landroid/view/View;IJ)Z

    move-result v0

    goto :goto_0
.end method

.method public setActionModeEnabled(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 96
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/MusicListView;->mActionModeHelper:Lcom/samsung/musicplus/widget/list/ActionModeHelper;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/widget/list/ActionModeHelper;->setActionModeEnabled(Z)V

    .line 97
    return-void
.end method

.method public setChoiceMode(I)V
    .locals 1
    .param p1, "choiceMode"    # I

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/list/MusicListView;->ensureActionMode()V

    .line 49
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/MusicListView;->mActionModeHelper:Lcom/samsung/musicplus/widget/list/ActionModeHelper;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/widget/list/ActionModeHelper;->convertChoiceMode(I)I

    move-result v0

    invoke-super {p0, v0}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 50
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/widget/list/MusicListView;->setEnableDragBlock(Z)V

    .line 51
    return-void
.end method

.method public setItemChecked(IZ)V
    .locals 1
    .param p1, "position"    # I
    .param p2, "value"    # Z

    .prologue
    .line 65
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/MusicListView;->mActionModeHelper:Lcom/samsung/musicplus/widget/list/ActionModeHelper;

    invoke-virtual {v0}, Lcom/samsung/musicplus/widget/list/ActionModeHelper;->isMultipleChoiceModeEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 66
    invoke-super {p0, p1, p2}, Landroid/widget/ListView;->setItemChecked(IZ)V

    .line 68
    :cond_0
    return-void
.end method

.method public setOnDisabledItemClickListener(Lcom/samsung/musicplus/widget/list/OnDisabledItemClickListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/samsung/musicplus/widget/list/OnDisabledItemClickListener;

    .prologue
    .line 100
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/MusicListView;->mActionModeHelper:Lcom/samsung/musicplus/widget/list/ActionModeHelper;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/widget/list/ActionModeHelper;->setOnDisabledItemClickListener(Lcom/samsung/musicplus/widget/list/OnDisabledItemClickListener;)V

    .line 101
    return-void
.end method
