.class public abstract Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;
.super Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;
.source "CommonToggleViewListFragment.java"

# interfaces
.implements Lcom/samsung/musicplus/dialog/ViewTypeDialogFragment$ViewTypeChangeListener;


# instance fields
.field private mPause:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;-><init>()V

    .line 34
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;->mPause:Z

    return-void
.end method

.method private changeViewType(I)V
    .locals 3
    .param p1, "viewType"    # I

    .prologue
    const/high16 v2, 0x40000

    .line 155
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;->finishActionMode()V

    .line 156
    if-nez p1, :cond_0

    .line 157
    iget v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;->mList:I

    xor-int/2addr v0, v2

    iput v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;->mList:I

    .line 163
    :goto_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;->initializeList()V

    .line 164
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;->saveListType()V

    .line 165
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;->setAlbumArtParsingEnabled(Z)V

    .line 166
    return-void

    .line 159
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "GRID"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/util/logging/FeatureLogger;->insertLog(Landroid/content/Context;Ljava/lang/String;)V

    .line 161
    iget v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;->mList:I

    or-int/2addr v0, v2

    iput v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;->mList:I

    goto :goto_0
.end method

.method private restoreListType()V
    .locals 4

    .prologue
    .line 191
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;->getPrefKey()Ljava/lang/String;

    move-result-object v0

    .line 192
    .local v0, "key":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 193
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;->getPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    .line 194
    .local v1, "preferences":Landroid/content/SharedPreferences;
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;->getPrefKey()Ljava/lang/String;

    move-result-object v2

    iget v3, p0, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;->mList:I

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;->mList:I

    .line 196
    .end local v1    # "preferences":Landroid/content/SharedPreferences;
    :cond_0
    return-void
.end method

.method private saveListType()V
    .locals 4

    .prologue
    .line 181
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;->getPrefKey()Ljava/lang/String;

    move-result-object v1

    .line 182
    .local v1, "key":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 183
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;->getPreferences()Landroid/content/SharedPreferences;

    move-result-object v2

    .line 184
    .local v2, "preferences":Landroid/content/SharedPreferences;
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 185
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    iget v3, p0, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;->mList:I

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 186
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 188
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v2    # "preferences":Landroid/content/SharedPreferences;
    :cond_0
    return-void
.end method


# virtual methods
.method public getListItemCount()I
    .locals 4

    .prologue
    .line 123
    iget v2, p0, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;->mList:I

    invoke-static {v2}, Lcom/samsung/musicplus/util/ListUtils;->isGridType(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 124
    invoke-super {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->getListItemCount()I

    move-result v0

    .line 135
    :cond_0
    :goto_0
    return v0

    .line 126
    :cond_1
    const/4 v0, 0x0

    .line 127
    .local v0, "count":I
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;->getAbsListView()Landroid/widget/AbsListView;

    move-result-object v1

    .line 128
    .local v1, "lv":Landroid/widget/AbsListView;
    if-eqz v1, :cond_0

    .line 129
    instance-of v2, v1, Lcom/samsung/musicplus/widget/list/MusicListView;

    if-eqz v2, :cond_2

    .line 130
    invoke-virtual {v1}, Landroid/widget/AbsListView;->getCount()I

    move-result v2

    check-cast v1, Lcom/samsung/musicplus/widget/list/MusicListView;

    .end local v1    # "lv":Landroid/widget/AbsListView;
    invoke-virtual {v1}, Lcom/samsung/musicplus/widget/list/MusicListView;->getHeaderViewsCount()I

    move-result v3

    sub-int v0, v2, v3

    goto :goto_0

    .line 132
    .restart local v1    # "lv":Landroid/widget/AbsListView;
    :cond_2
    invoke-virtual {v1}, Landroid/widget/AbsListView;->getCount()I

    move-result v0

    goto :goto_0
.end method

.method protected abstract getPrefKey()Ljava/lang/String;
.end method

.method protected initializeList()V
    .locals 1

    .prologue
    .line 93
    iget v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;->mList:I

    invoke-static {v0}, Lcom/samsung/musicplus/util/ListUtils;->isGridType(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;->setViewType(I)V

    .line 94
    invoke-super {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->initializeList()V

    .line 95
    return-void

    .line 93
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onChangeViewType(I)V
    .locals 1
    .param p1, "type"    # I

    .prologue
    .line 143
    iget-boolean v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;->mPause:Z

    if-nez v0, :cond_0

    .line 144
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;->changeViewType(I)V

    .line 147
    :cond_0
    sget-boolean v0, Lcom/samsung/musicplus/util/Features;->FLAG_SUPPORT_SPLIT_LIST_VIEW:Z

    if-eqz v0, :cond_1

    .line 148
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;->removeSubFragment()V

    .line 149
    iget v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;->mList:I

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;->initializeSplitSubList(I)V

    .line 151
    :cond_1
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 38
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 39
    if-nez p1, :cond_0

    iget v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;->mList:I

    invoke-static {v0}, Lcom/samsung/musicplus/util/ListUtils;->isTab(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 40
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;->restoreListType()V

    .line 42
    :cond_0
    return-void
.end method

.method public onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V
    .locals 3
    .param p2, "data"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 170
    .local p1, "loader":Landroid/content/Loader;, "Landroid/content/Loader<Landroid/database/Cursor;>;"
    invoke-super {p0, p1, p2}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V

    .line 171
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;->getView()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f0d0049

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 172
    .local v0, "mNoItemTextView":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 173
    invoke-virtual {p0, p2}, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;->needEmptyView(Landroid/database/Cursor;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 174
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 179
    :cond_0
    :goto_0
    return-void

    .line 176
    :cond_1
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public bridge synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Landroid/content/Loader;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 31
    check-cast p2, Landroid/database/Cursor;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;->onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 73
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 88
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v1

    return v1

    .line 77
    :pswitch_0
    iget-boolean v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;->mPause:Z

    if-nez v1, :cond_0

    .line 80
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;->getViewType()I

    move-result v1

    invoke-static {v1}, Lcom/samsung/musicplus/dialog/ViewTypeDialogFragment;->getInstance(I)Lcom/samsung/musicplus/dialog/ViewTypeDialogFragment;

    move-result-object v0

    .line 81
    .local v0, "dialog":Lcom/samsung/musicplus/dialog/ViewTypeDialogFragment;
    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lcom/samsung/musicplus/dialog/ViewTypeDialogFragment;->setTargetFragment(Landroid/app/Fragment;I)V

    .line 82
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "view_by"

    invoke-virtual {v0, v1, v2}, Lcom/samsung/musicplus/dialog/ViewTypeDialogFragment;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 73
    :pswitch_data_0
    .packed-switch 0x7f0d01e4
        :pswitch_0
    .end packed-switch
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;->mPause:Z

    .line 54
    invoke-super {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->onPause()V

    .line 55
    return-void
.end method

.method public onPrepareOptionsMenu(Landroid/view/Menu;)V
    .locals 2
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 59
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->onPrepareOptionsMenu(Landroid/view/Menu;)V

    .line 60
    const v1, 0x7f0d01e4

    invoke-interface {p1, v1}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 61
    .local v0, "viewAs":Landroid/view/MenuItem;
    if-nez v0, :cond_0

    .line 69
    :goto_0
    return-void

    .line 64
    :cond_0
    iget v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;->mList:I

    invoke-static {v1}, Lcom/samsung/musicplus/util/ListUtils;->isTab(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 65
    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0

    .line 67
    :cond_1
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    goto :goto_0
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 46
    invoke-super {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->onResume()V

    .line 47
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;->updateNumberView()V

    .line 48
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;->mPause:Z

    .line 49
    return-void
.end method

.method protected setHeaderView()V
    .locals 1

    .prologue
    .line 99
    iget v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;->mList:I

    invoke-static {v0}, Lcom/samsung/musicplus/util/ListUtils;->isGridType(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 100
    invoke-super {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->setHeaderView()V

    .line 104
    :goto_0
    return-void

    .line 102
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;->setListHeaderView()V

    goto :goto_0
.end method

.method protected updateNumberView()V
    .locals 3

    .prologue
    .line 108
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;->getView()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 109
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;->getView()Landroid/view/View;

    move-result-object v1

    const v2, 0x7f0d008f

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 110
    .local v0, "header":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 111
    iget v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonToggleViewListFragment;->mList:I

    invoke-static {v1}, Lcom/samsung/musicplus/util/ListUtils;->isGridType(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 112
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 118
    .end local v0    # "header":Landroid/view/View;
    :cond_0
    :goto_0
    invoke-super {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->updateNumberView()V

    .line 119
    return-void

    .line 114
    .restart local v0    # "header":Landroid/view/View;
    :cond_1
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method
