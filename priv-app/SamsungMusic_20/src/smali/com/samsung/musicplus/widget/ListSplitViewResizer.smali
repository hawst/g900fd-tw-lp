.class public Lcom/samsung/musicplus/widget/ListSplitViewResizer;
.super Landroid/widget/LinearLayout;
.source "ListSplitViewResizer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/widget/ListSplitViewResizer$IResizerControlListener;
    }
.end annotation


# static fields
.field private static final CLASSNAME:Ljava/lang/String;

.field private static final RESIZE_AREA:I = 0x1e

.field public static sSavedWidth:I


# instance fields
.field private mContext:Landroid/content/Context;

.field private mEnableResize:Z

.field private mLastXPosition:I

.field private mListType:I

.field private mListener:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/musicplus/widget/ListSplitViewResizer$IResizerControlListener;",
            ">;"
        }
    .end annotation
.end field

.field private mRightContainer:Landroid/view/View;

.field private mScreenWidth:I

.field private mSplitBarHoverListener:Landroid/view/View$OnHoverListener;

.field private mSplitDivider:Landroid/view/View;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/samsung/musicplus/widget/ListSplitViewResizer;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/widget/ListSplitViewResizer;->CLASSNAME:Ljava/lang/String;

    .line 94
    const/4 v0, -0x1

    sput v0, Lcom/samsung/musicplus/widget/ListSplitViewResizer;->sSavedWidth:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 84
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/widget/ListSplitViewResizer;->mEnableResize:Z

    .line 89
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/widget/ListSplitViewResizer;->mListener:Ljava/util/ArrayList;

    .line 169
    new-instance v0, Lcom/samsung/musicplus/widget/ListSplitViewResizer$1;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/widget/ListSplitViewResizer$1;-><init>(Lcom/samsung/musicplus/widget/ListSplitViewResizer;)V

    iput-object v0, p0, Lcom/samsung/musicplus/widget/ListSplitViewResizer;->mSplitBarHoverListener:Landroid/view/View$OnHoverListener;

    .line 28
    invoke-direct {p0, p1, p2}, Lcom/samsung/musicplus/widget/ListSplitViewResizer;->init(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    iput-object p1, p0, Lcom/samsung/musicplus/widget/ListSplitViewResizer;->mContext:Landroid/content/Context;

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 84
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/widget/ListSplitViewResizer;->mEnableResize:Z

    .line 89
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/widget/ListSplitViewResizer;->mListener:Ljava/util/ArrayList;

    .line 169
    new-instance v0, Lcom/samsung/musicplus/widget/ListSplitViewResizer$1;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/widget/ListSplitViewResizer$1;-><init>(Lcom/samsung/musicplus/widget/ListSplitViewResizer;)V

    iput-object v0, p0, Lcom/samsung/musicplus/widget/ListSplitViewResizer;->mSplitBarHoverListener:Landroid/view/View$OnHoverListener;

    .line 34
    invoke-direct {p0, p1, p2}, Lcom/samsung/musicplus/widget/ListSplitViewResizer;->init(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 35
    return-void
.end method

.method private getWidthKey()Ljava/lang/String;
    .locals 2

    .prologue
    .line 207
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "SavedWith"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/samsung/musicplus/widget/ListSplitViewResizer;->mListType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private init(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 38
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    .line 39
    .local v0, "metrics":Landroid/util/DisplayMetrics;
    const-string v1, "window"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/WindowManager;

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 42
    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v2, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    if-le v1, v2, :cond_0

    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    :goto_0
    iput v1, p0, Lcom/samsung/musicplus/widget/ListSplitViewResizer;->mScreenWidth:I

    .line 44
    return-void

    .line 42
    :cond_0
    iget v1, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    goto :goto_0
.end method

.method private initControlLayout()V
    .locals 10

    .prologue
    .line 53
    new-instance v2, Landroid/util/DisplayMetrics;

    invoke-direct {v2}, Landroid/util/DisplayMetrics;-><init>()V

    .line 54
    .local v2, "metrics":Landroid/util/DisplayMetrics;
    iget-object v7, p0, Lcom/samsung/musicplus/widget/ListSplitViewResizer;->mContext:Landroid/content/Context;

    const-string v8, "window"

    invoke-virtual {v7, v8}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/view/WindowManager;

    invoke-interface {v7}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v7

    invoke-virtual {v7, v2}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 56
    iget v7, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v8, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    if-le v7, v8, :cond_0

    iget v5, v2, Landroid/util/DisplayMetrics;->widthPixels:I

    .line 58
    .local v5, "screenWidth":I
    :goto_0
    const v7, 0x7f0d0088

    invoke-virtual {p0, v7}, Lcom/samsung/musicplus/widget/ListSplitViewResizer;->findViewById(I)Landroid/view/View;

    move-result-object v6

    .line 59
    .local v6, "v":Landroid/view/View;
    iget-object v7, p0, Lcom/samsung/musicplus/widget/ListSplitViewResizer;->mContext:Landroid/content/Context;

    const-string v8, "ListSplitViewResizer"

    const/4 v9, 0x0

    invoke-virtual {v7, v8, v9}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 61
    .local v3, "pref":Landroid/content/SharedPreferences;
    const/4 v0, -0x1

    .line 62
    .local v0, "iSavedWidth":I
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/ListSplitViewResizer;->getWidthKey()Ljava/lang/String;

    move-result-object v7

    const/4 v8, -0x1

    invoke-interface {v3, v7, v8}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 63
    invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    .line 64
    .local v1, "lp":Landroid/widget/LinearLayout$LayoutParams;
    if-lez v0, :cond_1

    .line 65
    mul-int v7, v5, v0

    div-int/lit8 v7, v7, 0x64

    iput v7, v1, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 66
    invoke-virtual {v6, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 72
    :goto_1
    const v7, 0x7f0d0096

    invoke-virtual {p0, v7}, Lcom/samsung/musicplus/widget/ListSplitViewResizer;->findViewById(I)Landroid/view/View;

    move-result-object v7

    iput-object v7, p0, Lcom/samsung/musicplus/widget/ListSplitViewResizer;->mSplitDivider:Landroid/view/View;

    .line 73
    iget-object v7, p0, Lcom/samsung/musicplus/widget/ListSplitViewResizer;->mSplitDivider:Landroid/view/View;

    iget-object v8, p0, Lcom/samsung/musicplus/widget/ListSplitViewResizer;->mSplitBarHoverListener:Landroid/view/View$OnHoverListener;

    invoke-virtual {v7, v8}, Landroid/view/View;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 74
    return-void

    .line 56
    .end local v0    # "iSavedWidth":I
    .end local v1    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    .end local v3    # "pref":Landroid/content/SharedPreferences;
    .end local v5    # "screenWidth":I
    .end local v6    # "v":Landroid/view/View;
    :cond_0
    iget v5, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    goto :goto_0

    .line 68
    .restart local v0    # "iSavedWidth":I
    .restart local v1    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    .restart local v3    # "pref":Landroid/content/SharedPreferences;
    .restart local v5    # "screenWidth":I
    .restart local v6    # "v":Landroid/view/View;
    :cond_1
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    .line 69
    .local v4, "prefEditor":Landroid/content/SharedPreferences$Editor;
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/ListSplitViewResizer;->getWidthKey()Ljava/lang/String;

    move-result-object v7

    iget v8, v1, Landroid/widget/LinearLayout$LayoutParams;->width:I

    mul-int/lit8 v8, v8, 0x64

    div-int/2addr v8, v5

    invoke-interface {v4, v7, v8}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 70
    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_1
.end method

.method private notifyMoveEnd()V
    .locals 3

    .prologue
    .line 192
    iget-object v2, p0, Lcom/samsung/musicplus/widget/ListSplitViewResizer;->mListener:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/musicplus/widget/ListSplitViewResizer$IResizerControlListener;

    .line 193
    .local v1, "l":Lcom/samsung/musicplus/widget/ListSplitViewResizer$IResizerControlListener;
    invoke-interface {v1}, Lcom/samsung/musicplus/widget/ListSplitViewResizer$IResizerControlListener;->onResizerMoveEnd()V

    goto :goto_0

    .line 195
    .end local v1    # "l":Lcom/samsung/musicplus/widget/ListSplitViewResizer$IResizerControlListener;
    :cond_0
    return-void
.end method

.method private notifyMoveStart()V
    .locals 3

    .prologue
    .line 186
    iget-object v2, p0, Lcom/samsung/musicplus/widget/ListSplitViewResizer;->mListener:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/musicplus/widget/ListSplitViewResizer$IResizerControlListener;

    .line 187
    .local v1, "l":Lcom/samsung/musicplus/widget/ListSplitViewResizer$IResizerControlListener;
    invoke-interface {v1}, Lcom/samsung/musicplus/widget/ListSplitViewResizer$IResizerControlListener;->onResizerMoveStart()V

    goto :goto_0

    .line 189
    .end local v1    # "l":Lcom/samsung/musicplus/widget/ListSplitViewResizer$IResizerControlListener;
    :cond_0
    return-void
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 9
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v8, 0x0

    const/4 v5, 0x1

    .line 101
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    .line 147
    :pswitch_0
    iget-boolean v6, p0, Lcom/samsung/musicplus/widget/ListSplitViewResizer;->mEnableResize:Z

    if-eqz v6, :cond_7

    .line 148
    iget-object v6, p0, Lcom/samsung/musicplus/widget/ListSplitViewResizer;->mSplitDivider:Landroid/view/View;

    if-eqz v6, :cond_0

    .line 149
    iget-object v6, p0, Lcom/samsung/musicplus/widget/ListSplitViewResizer;->mSplitDivider:Landroid/view/View;

    invoke-virtual {v6, v8}, Landroid/view/View;->setPressed(Z)V

    .line 150
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/ListSplitViewResizer;->notifyMoveEnd()V

    .line 153
    :cond_0
    iget-object v6, p0, Lcom/samsung/musicplus/widget/ListSplitViewResizer;->mContext:Landroid/content/Context;

    instance-of v6, v6, Landroid/app/Activity;

    if-eqz v6, :cond_1

    .line 154
    iget-object v6, p0, Lcom/samsung/musicplus/widget/ListSplitViewResizer;->mContext:Landroid/content/Context;

    const-string v7, "ListSplitViewResizer"

    invoke-virtual {v6, v7, v8}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 156
    .local v3, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    .line 157
    .local v4, "prefEditor":Landroid/content/SharedPreferences$Editor;
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/ListSplitViewResizer;->getWidthKey()Ljava/lang/String;

    move-result-object v6

    sget v7, Lcom/samsung/musicplus/widget/ListSplitViewResizer;->sSavedWidth:I

    mul-int/lit8 v7, v7, 0x64

    iget v8, p0, Lcom/samsung/musicplus/widget/ListSplitViewResizer;->mScreenWidth:I

    div-int/2addr v7, v8

    invoke-interface {v4, v6, v7}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 158
    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 166
    .end local v3    # "pref":Landroid/content/SharedPreferences;
    .end local v4    # "prefEditor":Landroid/content/SharedPreferences$Editor;
    :cond_1
    :goto_0
    return v5

    .line 103
    :pswitch_1
    iget-object v6, p0, Lcom/samsung/musicplus/widget/ListSplitViewResizer;->mRightContainer:Landroid/view/View;

    if-nez v6, :cond_2

    .line 104
    const v6, 0x7f0d0088

    invoke-virtual {p0, v6}, Lcom/samsung/musicplus/widget/ListSplitViewResizer;->findViewById(I)Landroid/view/View;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/musicplus/widget/ListSplitViewResizer;->mRightContainer:Landroid/view/View;

    .line 106
    :cond_2
    iget-object v6, p0, Lcom/samsung/musicplus/widget/ListSplitViewResizer;->mRightContainer:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->isShown()Z

    move-result v6

    if-eqz v6, :cond_3

    iget-object v6, p0, Lcom/samsung/musicplus/widget/ListSplitViewResizer;->mRightContainer:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getLeft()I

    move-result v6

    int-to-float v6, v6

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v7

    sub-float/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v6

    const/high16 v7, 0x41f00000    # 30.0f

    cmpg-float v6, v6, v7

    if-gez v6, :cond_3

    .line 108
    iput-boolean v5, p0, Lcom/samsung/musicplus/widget/ListSplitViewResizer;->mEnableResize:Z

    .line 110
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    float-to-int v6, v6

    iput v6, p0, Lcom/samsung/musicplus/widget/ListSplitViewResizer;->mLastXPosition:I

    .line 112
    const v6, 0x7f0d0096

    invoke-virtual {p0, v6}, Lcom/samsung/musicplus/widget/ListSplitViewResizer;->findViewById(I)Landroid/view/View;

    move-result-object v6

    iput-object v6, p0, Lcom/samsung/musicplus/widget/ListSplitViewResizer;->mSplitDivider:Landroid/view/View;

    .line 113
    iget-object v6, p0, Lcom/samsung/musicplus/widget/ListSplitViewResizer;->mSplitDivider:Landroid/view/View;

    invoke-virtual {v6, v5}, Landroid/view/View;->setPressed(Z)V

    goto :goto_0

    .line 116
    :cond_3
    iput-boolean v8, p0, Lcom/samsung/musicplus/widget/ListSplitViewResizer;->mEnableResize:Z

    .line 166
    :cond_4
    :goto_1
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v5

    goto :goto_0

    .line 121
    :pswitch_2
    iget-boolean v6, p0, Lcom/samsung/musicplus/widget/ListSplitViewResizer;->mEnableResize:Z

    if-eqz v6, :cond_4

    .line 122
    iget-object v6, p0, Lcom/samsung/musicplus/widget/ListSplitViewResizer;->mRightContainer:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout$LayoutParams;

    .line 123
    .local v2, "lp":Landroid/widget/LinearLayout$LayoutParams;
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    float-to-int v1, v6

    .line 124
    .local v1, "curX":I
    iget v6, v2, Landroid/widget/LinearLayout$LayoutParams;->width:I

    iget v7, p0, Lcom/samsung/musicplus/widget/ListSplitViewResizer;->mLastXPosition:I

    sub-int/2addr v7, v1

    add-int v0, v6, v7

    .line 125
    .local v0, "caculatedWidth":I
    const/16 v6, 0x14

    if-le v0, v6, :cond_6

    iget v6, p0, Lcom/samsung/musicplus/widget/ListSplitViewResizer;->mScreenWidth:I

    if-ge v0, v6, :cond_6

    .line 126
    const/16 v6, 0x3c

    if-ge v0, v6, :cond_5

    iget v6, p0, Lcom/samsung/musicplus/widget/ListSplitViewResizer;->mLastXPosition:I

    if-ge v6, v1, :cond_5

    .line 131
    iget-object v6, p0, Lcom/samsung/musicplus/widget/ListSplitViewResizer;->mSplitDivider:Landroid/view/View;

    invoke-virtual {v6}, Landroid/view/View;->getWidth()I

    move-result v0

    .line 133
    :cond_5
    iput v1, p0, Lcom/samsung/musicplus/widget/ListSplitViewResizer;->mLastXPosition:I

    .line 134
    iput v0, v2, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 135
    iget-object v6, p0, Lcom/samsung/musicplus/widget/ListSplitViewResizer;->mRightContainer:Landroid/view/View;

    invoke-virtual {v6, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 136
    sput v0, Lcom/samsung/musicplus/widget/ListSplitViewResizer;->sSavedWidth:I

    .line 137
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/ListSplitViewResizer;->notifyMoveStart()V

    goto/16 :goto_0

    .line 140
    :cond_6
    iput v1, p0, Lcom/samsung/musicplus/widget/ListSplitViewResizer;->mLastXPosition:I

    goto :goto_1

    .line 163
    .end local v0    # "caculatedWidth":I
    .end local v1    # "curX":I
    .end local v2    # "lp":Landroid/widget/LinearLayout$LayoutParams;
    :cond_7
    iput-boolean v8, p0, Lcom/samsung/musicplus/widget/ListSplitViewResizer;->mEnableResize:Z

    goto :goto_1

    .line 101
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method protected onFinishInflate()V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/ListSplitViewResizer;->initControlLayout()V

    .line 49
    invoke-super {p0}, Landroid/widget/LinearLayout;->onFinishInflate()V

    .line 50
    return-void
.end method

.method public registerControlListener(Lcom/samsung/musicplus/widget/ListSplitViewResizer$IResizerControlListener;)V
    .locals 1
    .param p1, "l"    # Lcom/samsung/musicplus/widget/ListSplitViewResizer$IResizerControlListener;

    .prologue
    .line 198
    iget-object v0, p0, Lcom/samsung/musicplus/widget/ListSplitViewResizer;->mListener:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 199
    return-void
.end method

.method public setListType(I)V
    .locals 0
    .param p1, "listType"    # I

    .prologue
    .line 202
    iput p1, p0, Lcom/samsung/musicplus/widget/ListSplitViewResizer;->mListType:I

    .line 203
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/ListSplitViewResizer;->initControlLayout()V

    .line 204
    return-void
.end method

.method public unregisterControlListener(Lcom/samsung/musicplus/widget/ListSplitViewResizer$IResizerControlListener;)V
    .locals 1
    .param p1, "l"    # Lcom/samsung/musicplus/widget/ListSplitViewResizer$IResizerControlListener;

    .prologue
    .line 211
    iget-object v0, p0, Lcom/samsung/musicplus/widget/ListSplitViewResizer;->mListener:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 212
    return-void
.end method
