.class abstract Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$PositionScroller;
.super Ljava/lang/Object;
.source "TwoWayAbsListView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x400
    name = "PositionScroller"
.end annotation


# static fields
.field protected static final MOVE_DOWN_BOUND:I = 0x3

.field protected static final MOVE_DOWN_POS:I = 0x1

.field protected static final MOVE_UP_BOUND:I = 0x4

.field protected static final MOVE_UP_POS:I = 0x2

.field protected static final SCROLL_DURATION:I = 0x190


# instance fields
.field protected mBoundPos:I

.field protected final mExtraScroll:I

.field protected mLastSeenPos:I

.field protected mMode:I

.field protected mScrollDuration:I

.field protected mTargetPos:I

.field protected mVertical:Z

.field final synthetic this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;)V
    .locals 1

    .prologue
    .line 3716
    iput-object p1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$PositionScroller;->this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3717
    iget-object v0, p1, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledFadingEdgeLength()I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$PositionScroller;->mExtraScroll:I

    .line 3718
    return-void
.end method


# virtual methods
.method public abstract run()V
.end method

.method start(I)V
    .locals 6
    .param p1, "position"    # I

    .prologue
    const/16 v5, 0x190

    const/4 v4, -0x1

    .line 3721
    iget-object v3, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$PositionScroller;->this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;

    iget-object v3, v3, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    iget v0, v3, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mFirstPosition:I

    .line 3722
    .local v0, "firstPos":I
    iget-object v3, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$PositionScroller;->this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;

    iget-object v3, v3, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    invoke-virtual {v3}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildCount()I

    move-result v3

    add-int/2addr v3, v0

    add-int/lit8 v1, v3, -0x1

    .line 3724
    .local v1, "lastPos":I
    const/4 v2, 0x0

    .line 3725
    .local v2, "viewTravelCount":I
    if-gt p1, v0, :cond_1

    .line 3726
    sub-int v3, v0, p1

    add-int/lit8 v2, v3, 0x1

    .line 3727
    const/4 v3, 0x2

    iput v3, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$PositionScroller;->mMode:I

    .line 3736
    :goto_0
    if-lez v2, :cond_2

    .line 3737
    div-int v3, v5, v2

    iput v3, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$PositionScroller;->mScrollDuration:I

    .line 3741
    :goto_1
    iput p1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$PositionScroller;->mTargetPos:I

    .line 3742
    iput v4, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$PositionScroller;->mBoundPos:I

    .line 3743
    iput v4, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$PositionScroller;->mLastSeenPos:I

    .line 3745
    iget-object v3, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$PositionScroller;->this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;

    iget-object v3, v3, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    invoke-virtual {v3, p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->post(Ljava/lang/Runnable;)Z

    .line 3746
    :cond_0
    return-void

    .line 3728
    :cond_1
    if-lt p1, v1, :cond_0

    .line 3729
    sub-int v3, p1, v1

    add-int/lit8 v2, v3, 0x1

    .line 3730
    const/4 v3, 0x1

    iput v3, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$PositionScroller;->mMode:I

    goto :goto_0

    .line 3739
    :cond_2
    iput v5, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$PositionScroller;->mScrollDuration:I

    goto :goto_1
.end method

.method start(II)V
    .locals 11
    .param p1, "position"    # I
    .param p2, "boundPosition"    # I

    .prologue
    const/16 v10, 0x190

    const/4 v9, -0x1

    const/4 v8, 0x1

    .line 3749
    if-ne p2, v9, :cond_1

    .line 3750
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$PositionScroller;->start(I)V

    .line 3805
    :cond_0
    :goto_0
    return-void

    .line 3754
    :cond_1
    iget-object v7, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$PositionScroller;->this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;

    iget-object v7, v7, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    iget v3, v7, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mFirstPosition:I

    .line 3755
    .local v3, "firstPos":I
    iget-object v7, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$PositionScroller;->this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;

    iget-object v7, v7, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    invoke-virtual {v7}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildCount()I

    move-result v7

    add-int/2addr v7, v3

    add-int/lit8 v4, v7, -0x1

    .line 3757
    .local v4, "lastPos":I
    const/4 v6, 0x0

    .line 3758
    .local v6, "viewTravelCount":I
    if-gt p1, v3, :cond_3

    .line 3759
    sub-int v1, v4, p2

    .line 3760
    .local v1, "boundPosFromLast":I
    if-lt v1, v8, :cond_0

    .line 3765
    sub-int v7, v3, p1

    add-int/lit8 v5, v7, 0x1

    .line 3766
    .local v5, "posTravel":I
    add-int/lit8 v2, v1, -0x1

    .line 3767
    .local v2, "boundTravel":I
    if-ge v2, v5, :cond_2

    .line 3768
    move v6, v2

    .line 3769
    const/4 v7, 0x4

    iput v7, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$PositionScroller;->mMode:I

    .line 3795
    .end local v1    # "boundPosFromLast":I
    :goto_1
    if-lez v6, :cond_5

    .line 3796
    div-int v7, v10, v6

    iput v7, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$PositionScroller;->mScrollDuration:I

    .line 3800
    :goto_2
    iput p1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$PositionScroller;->mTargetPos:I

    .line 3801
    iput p2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$PositionScroller;->mBoundPos:I

    .line 3802
    iput v9, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$PositionScroller;->mLastSeenPos:I

    .line 3804
    iget-object v7, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$PositionScroller;->this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;

    iget-object v7, v7, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    invoke-virtual {v7, p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 3771
    .restart local v1    # "boundPosFromLast":I
    :cond_2
    move v6, v5

    .line 3772
    const/4 v7, 0x2

    iput v7, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$PositionScroller;->mMode:I

    goto :goto_1

    .line 3774
    .end local v1    # "boundPosFromLast":I
    .end local v2    # "boundTravel":I
    .end local v5    # "posTravel":I
    :cond_3
    if-lt p1, v4, :cond_0

    .line 3775
    sub-int v0, p2, v3

    .line 3776
    .local v0, "boundPosFromFirst":I
    if-lt v0, v8, :cond_0

    .line 3781
    sub-int v7, p1, v4

    add-int/lit8 v5, v7, 0x1

    .line 3782
    .restart local v5    # "posTravel":I
    add-int/lit8 v2, v0, -0x1

    .line 3783
    .restart local v2    # "boundTravel":I
    if-ge v2, v5, :cond_4

    .line 3784
    move v6, v2

    .line 3785
    const/4 v7, 0x3

    iput v7, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$PositionScroller;->mMode:I

    goto :goto_1

    .line 3787
    :cond_4
    move v6, v5

    .line 3788
    iput v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$PositionScroller;->mMode:I

    goto :goto_1

    .line 3798
    .end local v0    # "boundPosFromFirst":I
    :cond_5
    iput v10, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$PositionScroller;->mScrollDuration:I

    goto :goto_2
.end method

.method stop()V
    .locals 1

    .prologue
    .line 3808
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$PositionScroller;->this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    invoke-virtual {v0, p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 3809
    return-void
.end method
