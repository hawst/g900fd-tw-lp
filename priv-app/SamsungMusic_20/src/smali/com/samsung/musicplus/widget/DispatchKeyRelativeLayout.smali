.class public Lcom/samsung/musicplus/widget/DispatchKeyRelativeLayout;
.super Landroid/widget/RelativeLayout;
.source "DispatchKeyRelativeLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/widget/DispatchKeyRelativeLayout$KeyPadDisplayListener;
    }
.end annotation


# instance fields
.field private keyPadDisplayListener:Lcom/samsung/musicplus/widget/DispatchKeyRelativeLayout$KeyPadDisplayListener;

.field private mBackkeyMsgCode:I

.field private mHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 18
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/musicplus/widget/DispatchKeyRelativeLayout;->mHandler:Landroid/os/Handler;

    .line 19
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/musicplus/widget/DispatchKeyRelativeLayout;->mBackkeyMsgCode:I

    .line 28
    return-void
.end method


# virtual methods
.method public dispatchKeyEventPreIme(Landroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 42
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    .line 43
    iget-object v1, p0, Lcom/samsung/musicplus/widget/DispatchKeyRelativeLayout;->mHandler:Landroid/os/Handler;

    if-eqz v1, :cond_0

    .line 44
    iget-object v1, p0, Lcom/samsung/musicplus/widget/DispatchKeyRelativeLayout;->mHandler:Landroid/os/Handler;

    iget v2, p0, Lcom/samsung/musicplus/widget/DispatchKeyRelativeLayout;->mBackkeyMsgCode:I

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 45
    iget-object v1, p0, Lcom/samsung/musicplus/widget/DispatchKeyRelativeLayout;->mHandler:Landroid/os/Handler;

    iget v2, p0, Lcom/samsung/musicplus/widget/DispatchKeyRelativeLayout;->mBackkeyMsgCode:I

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 46
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/samsung/musicplus/widget/DispatchKeyRelativeLayout;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 49
    .end local v0    # "msg":Landroid/os/Message;
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->dispatchKeyEventPreIme(Landroid/view/KeyEvent;)Z

    move-result v1

    return v1
.end method

.method protected onMeasure(II)V
    .locals 8
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 79
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v2

    .line 80
    .local v2, "height":I
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/DispatchKeyRelativeLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 81
    .local v0, "activity":Landroid/app/Activity;
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 82
    .local v3, "rect":Landroid/graphics/Rect;
    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v6

    invoke-virtual {v6, v3}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 83
    iget v5, v3, Landroid/graphics/Rect;->top:I

    .line 84
    .local v5, "statusBarHeight":I
    invoke-virtual {v0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v6

    invoke-interface {v6}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v6

    invoke-virtual {v6}, Landroid/view/Display;->getHeight()I

    move-result v4

    .line 85
    .local v4, "screenHeight":I
    sub-int v6, v4, v5

    sub-int v1, v6, v2

    .line 86
    .local v1, "diff":I
    iget-object v6, p0, Lcom/samsung/musicplus/widget/DispatchKeyRelativeLayout;->keyPadDisplayListener:Lcom/samsung/musicplus/widget/DispatchKeyRelativeLayout$KeyPadDisplayListener;

    if-eqz v6, :cond_0

    .line 87
    const/16 v6, 0x80

    if-le v1, v6, :cond_1

    .line 88
    iget-object v6, p0, Lcom/samsung/musicplus/widget/DispatchKeyRelativeLayout;->keyPadDisplayListener:Lcom/samsung/musicplus/widget/DispatchKeyRelativeLayout$KeyPadDisplayListener;

    const/4 v7, 0x1

    invoke-interface {v6, v7}, Lcom/samsung/musicplus/widget/DispatchKeyRelativeLayout$KeyPadDisplayListener;->onSoftKeyboardShow(Z)V

    .line 89
    const-string v6, ""

    const-string v7, "IME is showing"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Landroid/widget/RelativeLayout;->onMeasure(II)V

    .line 97
    return-void

    .line 91
    :cond_1
    iget-object v6, p0, Lcom/samsung/musicplus/widget/DispatchKeyRelativeLayout;->keyPadDisplayListener:Lcom/samsung/musicplus/widget/DispatchKeyRelativeLayout$KeyPadDisplayListener;

    const/4 v7, 0x0

    invoke-interface {v6, v7}, Lcom/samsung/musicplus/widget/DispatchKeyRelativeLayout$KeyPadDisplayListener;->onSoftKeyboardShow(Z)V

    .line 92
    const-string v6, ""

    const-string v7, "IME is gone"

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setHandler(Landroid/os/Handler;I)V
    .locals 0
    .param p1, "handler"    # Landroid/os/Handler;
    .param p2, "msgCode"    # I

    .prologue
    .line 36
    iput-object p1, p0, Lcom/samsung/musicplus/widget/DispatchKeyRelativeLayout;->mHandler:Landroid/os/Handler;

    .line 37
    iput p2, p0, Lcom/samsung/musicplus/widget/DispatchKeyRelativeLayout;->mBackkeyMsgCode:I

    .line 38
    return-void
.end method

.method public setListener(Lcom/samsung/musicplus/widget/DispatchKeyRelativeLayout$KeyPadDisplayListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/musicplus/widget/DispatchKeyRelativeLayout$KeyPadDisplayListener;

    .prologue
    .line 73
    iput-object p1, p0, Lcom/samsung/musicplus/widget/DispatchKeyRelativeLayout;->keyPadDisplayListener:Lcom/samsung/musicplus/widget/DispatchKeyRelativeLayout$KeyPadDisplayListener;

    .line 74
    return-void
.end method
