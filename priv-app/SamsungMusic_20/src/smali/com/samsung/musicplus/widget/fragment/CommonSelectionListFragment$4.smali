.class Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment$4;
.super Ljava/lang/Object;
.source "CommonSelectionListFragment.java"

# interfaces
.implements Landroid/widget/AdapterView$OnTwMultiSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mDragSelectedPositions:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;)V
    .locals 1

    .prologue
    .line 999
    iput-object p1, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment$4;->this$0:Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1001
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment$4;->mDragSelectedPositions:Ljava/util/ArrayList;

    return-void
.end method

.method private getSelectionList()[I
    .locals 4

    .prologue
    .line 1005
    iget-object v3, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment$4;->mDragSelectedPositions:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 1006
    .local v0, "count":I
    new-array v2, v0, [I

    .line 1008
    .local v2, "selectionList":[I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 1009
    iget-object v3, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment$4;->mDragSelectedPositions:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    aput v3, v2, v1

    .line 1008
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1012
    :cond_0
    return-object v2
.end method


# virtual methods
.method public OnTwMultiSelectStart(II)V
    .locals 1
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 1032
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment$4;->mDragSelectedPositions:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1033
    return-void
.end method

.method public OnTwMultiSelectStop(II)V
    .locals 7
    .param p1, "x"    # I
    .param p2, "y"    # I

    .prologue
    .line 1037
    iget-object v5, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment$4;->this$0:Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;

    # getter for: Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mIsActionMode:Z
    invoke-static {v5}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->access$200(Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;)Z

    move-result v5

    if-nez v5, :cond_1

    iget-object v5, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment$4;->mDragSelectedPositions:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-lez v5, :cond_1

    .line 1038
    iget-object v5, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment$4;->this$0:Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;

    invoke-virtual {v5}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->getAbsListView()Landroid/widget/AbsListView;

    move-result-object v3

    .line 1039
    .local v3, "lv":Landroid/widget/AbsListView;
    if-eqz v3, :cond_0

    .line 1040
    iget-object v5, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment$4;->this$0:Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;

    invoke-direct {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment$4;->getSelectionList()[I

    move-result-object v6

    # invokes: Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->startActionMode(Landroid/widget/AbsListView;[I)V
    invoke-static {v5, v3, v6}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->access$300(Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;Landroid/widget/AbsListView;[I)V

    .line 1057
    .end local v3    # "lv":Landroid/widget/AbsListView;
    :cond_0
    :goto_0
    return-void

    .line 1043
    :cond_1
    iget-object v5, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment$4;->this$0:Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;

    # getter for: Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->mActionModeCallBack:Lcom/samsung/musicplus/widget/ActionModeCallback;
    invoke-static {v5}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->access$000(Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;)Lcom/samsung/musicplus/widget/ActionModeCallback;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 1044
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment$4;->getSelectionList()[I

    move-result-object v4

    .line 1045
    .local v4, "positions":[I
    if-eqz v4, :cond_3

    .line 1046
    array-length v2, v4

    .line 1047
    .local v2, "length":I
    iget-object v5, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment$4;->this$0:Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;

    invoke-virtual {v5}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->getAbsListView()Landroid/widget/AbsListView;

    move-result-object v0

    .line 1048
    .local v0, "alv":Landroid/widget/AbsListView;
    if-eqz v0, :cond_3

    .line 1049
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v2, :cond_3

    .line 1050
    aget v6, v4, v1

    aget v5, v4, v1

    invoke-virtual {v0, v5}, Landroid/widget/AbsListView;->isItemChecked(I)Z

    move-result v5

    if-nez v5, :cond_2

    const/4 v5, 0x1

    :goto_2
    invoke-virtual {v0, v6, v5}, Landroid/widget/AbsListView;->setItemChecked(IZ)V

    .line 1049
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1050
    :cond_2
    const/4 v5, 0x0

    goto :goto_2

    .line 1054
    .end local v0    # "alv":Landroid/widget/AbsListView;
    .end local v1    # "i":I
    .end local v2    # "length":I
    :cond_3
    iget-object v5, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment$4;->this$0:Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;

    invoke-virtual {v5}, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment;->invalidateActionMode()V

    goto :goto_0
.end method

.method public onTwMultiSelected(Landroid/widget/AdapterView;Landroid/view/View;IJZZZ)V
    .locals 4
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .param p6, "isShiftpress"    # Z
    .param p7, "isCtrlpress"    # Z
    .param p8, "isPentpress"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJZZZ)V"
        }
    .end annotation

    .prologue
    .line 1017
    .local p1, "adapterView":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    const-wide/16 v2, 0x0

    cmp-long v1, p4, v2

    if-ltz v1, :cond_0

    .line 1021
    new-instance v0, Ljava/lang/Integer;

    invoke-direct {v0, p3}, Ljava/lang/Integer;-><init>(I)V

    .line 1022
    .local v0, "selectedPosition":Ljava/lang/Integer;
    iget-object v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment$4;->mDragSelectedPositions:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1023
    iget-object v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment$4;->mDragSelectedPositions:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 1028
    .end local v0    # "selectedPosition":Ljava/lang/Integer;
    :cond_0
    :goto_0
    return-void

    .line 1025
    .restart local v0    # "selectedPosition":Ljava/lang/Integer;
    :cond_1
    iget-object v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonSelectionListFragment$4;->mDragSelectedPositions:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
