.class Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$CheckForKeyLongPress;
.super Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$WindowRunnnable;
.source "TwoWayAbsListView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CheckForKeyLongPress"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;


# direct methods
.method private constructor <init>(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;)V
    .locals 1

    .prologue
    .line 1892
    iput-object p1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$CheckForKeyLongPress;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$WindowRunnnable;-><init>(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$1;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;
    .param p2, "x1"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$1;

    .prologue
    .line 1892
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$CheckForKeyLongPress;-><init>(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 1894
    iget-object v3, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$CheckForKeyLongPress;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    invoke-virtual {v3}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->isPressed()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$CheckForKeyLongPress;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    iget v3, v3, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSelectedPosition:I

    if-ltz v3, :cond_1

    .line 1895
    iget-object v3, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$CheckForKeyLongPress;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    iget v3, v3, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSelectedPosition:I

    iget-object v4, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$CheckForKeyLongPress;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    iget v4, v4, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mFirstPosition:I

    sub-int v1, v3, v4

    .line 1896
    .local v1, "index":I
    iget-object v3, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$CheckForKeyLongPress;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    invoke-virtual {v3, v1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 1898
    .local v2, "v":Landroid/view/View;
    iget-object v3, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$CheckForKeyLongPress;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    iget-boolean v3, v3, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mDataChanged:Z

    if-nez v3, :cond_2

    .line 1899
    const/4 v0, 0x0

    .line 1900
    .local v0, "handled":Z
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$CheckForKeyLongPress;->sameWindow()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1901
    iget-object v3, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$CheckForKeyLongPress;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    iget-object v4, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$CheckForKeyLongPress;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    iget v4, v4, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSelectedPosition:I

    iget-object v5, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$CheckForKeyLongPress;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    iget-wide v6, v5, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSelectedRowId:J

    # invokes: Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->performLongPress(Landroid/view/View;IJ)Z
    invoke-static {v3, v2, v4, v6, v7}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->access$500(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;Landroid/view/View;IJ)Z

    move-result v0

    .line 1903
    :cond_0
    if-eqz v0, :cond_1

    .line 1904
    iget-object v3, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$CheckForKeyLongPress;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    invoke-virtual {v3, v8}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->setPressed(Z)V

    .line 1905
    invoke-virtual {v2, v8}, Landroid/view/View;->setPressed(Z)V

    .line 1912
    .end local v0    # "handled":Z
    .end local v1    # "index":I
    .end local v2    # "v":Landroid/view/View;
    :cond_1
    :goto_0
    return-void

    .line 1908
    .restart local v1    # "index":I
    .restart local v2    # "v":Landroid/view/View;
    :cond_2
    iget-object v3, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$CheckForKeyLongPress;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    invoke-virtual {v3, v8}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->setPressed(Z)V

    .line 1909
    if-eqz v2, :cond_1

    invoke-virtual {v2, v8}, Landroid/view/View;->setPressed(Z)V

    goto :goto_0
.end method
