.class abstract Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;
.super Ljava/lang/Object;
.source "TwoWayAbsListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x400
    name = "TouchHandler"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$PositionScroller;,
        Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$FlingRunnable;
    }
.end annotation


# instance fields
.field protected mFlingRunnable:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$FlingRunnable;

.field mMotionCorrection:I

.field protected mPositionScroller:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$PositionScroller;

.field final synthetic this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;)V
    .locals 0

    .prologue
    .line 3438
    iput-object p1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3700
    return-void
.end method


# virtual methods
.method protected clearScrollingCache()V
    .locals 2

    .prologue
    .line 3618
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    # getter for: Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mClearScrollingCache:Ljava/lang/Runnable;
    invoke-static {v0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->access$2400(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;)Ljava/lang/Runnable;

    move-result-object v0

    if-nez v0, :cond_0

    .line 3619
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    new-instance v1, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$1;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$1;-><init>(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;)V

    # setter for: Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mClearScrollingCache:Ljava/lang/Runnable;
    invoke-static {v0, v1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->access$2402(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;Ljava/lang/Runnable;)Ljava/lang/Runnable;

    .line 3634
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    iget-object v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    # getter for: Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mClearScrollingCache:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->access$2400(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->post(Ljava/lang/Runnable;)Z

    .line 3635
    return-void
.end method

.method protected createScrollingCache()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 3610
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    iget-boolean v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mScrollingCacheEnabled:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    iget-boolean v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mCachingStarted:Z

    if-nez v0, :cond_0

    .line 3611
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    # invokes: Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->setChildrenDrawnWithCacheEnabled(Z)V
    invoke-static {v0, v1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->access$2200(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;Z)V

    .line 3612
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    # invokes: Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->setChildrenDrawingCacheEnabled(Z)V
    invoke-static {v0, v1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->access$2300(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;Z)V

    .line 3613
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    iput-boolean v1, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mCachingStarted:Z

    .line 3615
    :cond_0
    return-void
.end method

.method protected abstract getFlingRunnable()Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$FlingRunnable;
.end method

.method protected abstract getPositionScroller()Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$PositionScroller;
.end method

.method public abstract onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
.end method

.method public abstract onTouchEvent(Landroid/view/MotionEvent;)Z
.end method

.method public onTouchModeChanged(Z)V
    .locals 1
    .param p1, "isInTouchMode"    # Z

    .prologue
    .line 3537
    if-eqz p1, :cond_0

    .line 3539
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    invoke-virtual {v0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->hideSelector()V

    .line 3543
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    invoke-virtual {v0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getHeight()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    invoke-virtual {v0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    .line 3546
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    invoke-virtual {v0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->layoutChildren()V

    .line 3549
    :cond_0
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 5
    .param p1, "hasWindowFocus"    # Z

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 3456
    iget-object v3, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    invoke-virtual {v3}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->isInTouchMode()Z

    move-result v3

    if-eqz v3, :cond_2

    move v0, v1

    .line 3458
    .local v0, "touchMode":I
    :goto_0
    if-nez p1, :cond_3

    .line 3459
    iget-object v3, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    # invokes: Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->setChildrenDrawingCacheEnabled(Z)V
    invoke-static {v3, v1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->access$1700(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;Z)V

    .line 3460
    iget-object v3, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->mFlingRunnable:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$FlingRunnable;

    if-eqz v3, :cond_0

    .line 3461
    iget-object v3, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    iget-object v4, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->mFlingRunnable:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$FlingRunnable;

    invoke-virtual {v3, v4}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 3464
    iget-object v3, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->mFlingRunnable:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$FlingRunnable;

    invoke-virtual {v3}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$FlingRunnable;->endFling()V

    .line 3466
    iget-object v3, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    invoke-virtual {v3}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getScrollY()I

    move-result v3

    if-eqz v3, :cond_0

    .line 3467
    iget-object v3, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    iget-object v4, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    invoke-virtual {v4}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getScrollX()I

    move-result v4

    invoke-virtual {v3, v4, v1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->scrollTo(II)V

    .line 3469
    iget-object v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    invoke-virtual {v1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->invalidate()V

    .line 3475
    :cond_0
    if-ne v0, v2, :cond_1

    .line 3477
    iget-object v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    iget-object v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    iget v2, v2, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mSelectedPosition:I

    iput v2, v1, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mResurrectToPosition:I

    .line 3501
    :cond_1
    :goto_1
    iget-object v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    # setter for: Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mLastTouchMode:I
    invoke-static {v1, v0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->access$1802(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;I)I

    .line 3502
    return-void

    .end local v0    # "touchMode":I
    :cond_2
    move v0, v2

    .line 3456
    goto :goto_0

    .line 3486
    .restart local v0    # "touchMode":I
    :cond_3
    iget-object v3, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    # getter for: Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mLastTouchMode:I
    invoke-static {v3}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->access$1800(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;)I

    move-result v3

    if-eq v0, v3, :cond_1

    iget-object v3, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    # getter for: Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mLastTouchMode:I
    invoke-static {v3}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->access$1800(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;)I

    move-result v3

    const/4 v4, -0x1

    if-eq v3, v4, :cond_1

    .line 3488
    if-ne v0, v2, :cond_4

    .line 3490
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->resurrectSelection()Z

    goto :goto_1

    .line 3494
    :cond_4
    iget-object v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    invoke-virtual {v2}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->hideSelector()V

    .line 3495
    iget-object v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    iput v1, v2, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mLayoutMode:I

    .line 3496
    iget-object v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    invoke-virtual {v1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->layoutChildren()V

    goto :goto_1
.end method

.method reportScrollStateChange(I)V
    .locals 2
    .param p1, "newState"    # I

    .prologue
    .line 3559
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    # getter for: Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mLastScrollState:I
    invoke-static {v0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->access$2000(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;)I

    move-result v0

    if-eq p1, v0, :cond_0

    .line 3560
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    # getter for: Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mOnScrollListener:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$OnScrollListener;
    invoke-static {v0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->access$2100(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;)Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$OnScrollListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 3561
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    # getter for: Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mOnScrollListener:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$OnScrollListener;
    invoke-static {v0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->access$2100(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;)Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$OnScrollListener;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    invoke-interface {v0, v1, p1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$OnScrollListener;->onScrollStateChanged(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;I)V

    .line 3562
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    # setter for: Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mLastScrollState:I
    invoke-static {v0, p1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->access$2002(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;I)I

    .line 3565
    :cond_0
    return-void
.end method

.method abstract resurrectSelection()Z
.end method

.method public smoothScrollBy(II)V
    .locals 1
    .param p1, "distance"    # I
    .param p2, "duration"    # I

    .prologue
    .line 3601
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->mFlingRunnable:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$FlingRunnable;

    if-nez v0, :cond_0

    .line 3602
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->getFlingRunnable()Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$FlingRunnable;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->mFlingRunnable:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$FlingRunnable;

    .line 3606
    :goto_0
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->mFlingRunnable:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$FlingRunnable;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$FlingRunnable;->startScroll(II)V

    .line 3607
    return-void

    .line 3604
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->mFlingRunnable:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$FlingRunnable;

    invoke-virtual {v0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$FlingRunnable;->endFling()V

    goto :goto_0
.end method

.method public smoothScrollToPosition(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 3573
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->mPositionScroller:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$PositionScroller;

    if-nez v0, :cond_0

    .line 3574
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->getPositionScroller()Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$PositionScroller;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->mPositionScroller:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$PositionScroller;

    .line 3576
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->mPositionScroller:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$PositionScroller;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$PositionScroller;->start(I)V

    .line 3577
    return-void
.end method

.method public smoothScrollToPosition(II)V
    .locals 1
    .param p1, "position"    # I
    .param p2, "boundPosition"    # I

    .prologue
    .line 3589
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->mPositionScroller:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$PositionScroller;

    if-nez v0, :cond_0

    .line 3590
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->getPositionScroller()Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$PositionScroller;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->mPositionScroller:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$PositionScroller;

    .line 3592
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->mPositionScroller:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$PositionScroller;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$PositionScroller;->start(II)V

    .line 3593
    return-void
.end method

.method public startScrollIfNeeded(I)Z
    .locals 8
    .param p1, "delta"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 3508
    invoke-static {p1}, Ljava/lang/Math;->abs(I)I

    move-result v0

    .line 3509
    .local v0, "distance":I
    iget-object v5, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    # getter for: Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mTouchSlop:I
    invoke-static {v5}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->access$1900(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;)I

    move-result v5

    if-le v0, v5, :cond_2

    .line 3510
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->createScrollingCache()V

    .line 3511
    iget-object v5, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    const/4 v6, 0x3

    iput v6, v5, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mTouchMode:I

    .line 3512
    iput p1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->mMotionCorrection:I

    .line 3513
    iget-object v5, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    invoke-virtual {v5}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getHandler()Landroid/os/Handler;

    move-result-object v1

    .line 3517
    .local v1, "handler":Landroid/os/Handler;
    if-eqz v1, :cond_0

    .line 3518
    iget-object v5, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    # getter for: Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mPendingCheckForLongPress:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$CheckForLongPress;
    invoke-static {v5}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->access$600(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;)Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$CheckForLongPress;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 3520
    :cond_0
    iget-object v5, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    invoke-virtual {v5, v4}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->setPressed(Z)V

    .line 3521
    iget-object v5, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    iget-object v6, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    iget v6, v6, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mMotionPosition:I

    iget-object v7, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    iget v7, v7, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mFirstPosition:I

    sub-int/2addr v6, v7

    invoke-virtual {v5, v6}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 3522
    .local v2, "motionView":Landroid/view/View;
    if-eqz v2, :cond_1

    .line 3523
    invoke-virtual {v2, v4}, Landroid/view/View;->setPressed(Z)V

    .line 3525
    :cond_1
    invoke-virtual {p0, v3}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->reportScrollStateChange(I)V

    .line 3528
    iget-object v4, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    invoke-virtual {v4, v3}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->requestDisallowInterceptTouchEvent(Z)V

    .line 3532
    .end local v1    # "handler":Landroid/os/Handler;
    .end local v2    # "motionView":Landroid/view/View;
    :goto_0
    return v3

    :cond_2
    move v3, v4

    goto :goto_0
.end method

.method abstract trackMotionScroll(II)Z
.end method
