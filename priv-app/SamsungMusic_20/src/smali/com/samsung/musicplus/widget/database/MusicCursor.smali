.class public Lcom/samsung/musicplus/widget/database/MusicCursor;
.super Landroid/database/AbstractCursor;
.source "MusicCursor.java"


# static fields
.field private static final CLASSNAME:Ljava/lang/String;

.field private static final DEBUG:Z = false

.field private static final DEBUG_WITH_CALL_STACK:Z = false

.field public static final LIST_HEADER_DOWNLOAD_ALL_ID:J = -0x65L

.field public static final LIST_HEADER_DOWNLOAD_POSITION:I = 0x0

.field public static final LIST_HEADER_SHUFFLE_ID:J = -0x64L
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final LIST_HEADER_SHUFFLE_POSITION:I = 0x0

.field public static final USE_MUSIC_CURSOR:Z = true


# instance fields
.field private mCursor:Landroid/database/Cursor;

.field private mHasDownLoadView:Z

.field private mHasShuffleView:Z

.field private mIsDownLoad:Z

.field private mIsShuffle:Z

.field private mObserver:Landroid/database/DataSetObserver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-class v0, Lcom/samsung/musicplus/widget/database/MusicCursor;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/widget/database/MusicCursor;->CLASSNAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/database/Cursor;)V
    .locals 2
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    const/4 v0, 0x0

    .line 92
    invoke-direct {p0}, Landroid/database/AbstractCursor;-><init>()V

    .line 53
    iput-boolean v0, p0, Lcom/samsung/musicplus/widget/database/MusicCursor;->mHasShuffleView:Z

    .line 55
    iput-boolean v0, p0, Lcom/samsung/musicplus/widget/database/MusicCursor;->mIsShuffle:Z

    .line 57
    iput-boolean v0, p0, Lcom/samsung/musicplus/widget/database/MusicCursor;->mHasDownLoadView:Z

    .line 59
    iput-boolean v0, p0, Lcom/samsung/musicplus/widget/database/MusicCursor;->mIsDownLoad:Z

    .line 61
    new-instance v0, Lcom/samsung/musicplus/widget/database/MusicCursor$1;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/widget/database/MusicCursor$1;-><init>(Lcom/samsung/musicplus/widget/database/MusicCursor;)V

    iput-object v0, p0, Lcom/samsung/musicplus/widget/database/MusicCursor;->mObserver:Landroid/database/DataSetObserver;

    .line 96
    iput-object p1, p0, Lcom/samsung/musicplus/widget/database/MusicCursor;->mCursor:Landroid/database/Cursor;

    .line 97
    iget-object v0, p0, Lcom/samsung/musicplus/widget/database/MusicCursor;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 98
    iget-object v0, p0, Lcom/samsung/musicplus/widget/database/MusicCursor;->mCursor:Landroid/database/Cursor;

    iget-object v1, p0, Lcom/samsung/musicplus/widget/database/MusicCursor;->mObserver:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/database/Cursor;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 100
    :cond_0
    return-void
.end method

.method static synthetic access$002(Lcom/samsung/musicplus/widget/database/MusicCursor;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/database/MusicCursor;
    .param p1, "x1"    # I

    .prologue
    .line 30
    iput p1, p0, Lcom/samsung/musicplus/widget/database/MusicCursor;->mPos:I

    return p1
.end method

.method static synthetic access$102(Lcom/samsung/musicplus/widget/database/MusicCursor;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/database/MusicCursor;
    .param p1, "x1"    # Z

    .prologue
    .line 30
    iput-boolean p1, p0, Lcom/samsung/musicplus/widget/database/MusicCursor;->mIsShuffle:Z

    return p1
.end method

.method static synthetic access$202(Lcom/samsung/musicplus/widget/database/MusicCursor;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/database/MusicCursor;
    .param p1, "x1"    # Z

    .prologue
    .line 30
    iput-boolean p1, p0, Lcom/samsung/musicplus/widget/database/MusicCursor;->mIsDownLoad:Z

    return p1
.end method

.method static synthetic access$302(Lcom/samsung/musicplus/widget/database/MusicCursor;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/database/MusicCursor;
    .param p1, "x1"    # I

    .prologue
    .line 30
    iput p1, p0, Lcom/samsung/musicplus/widget/database/MusicCursor;->mPos:I

    return p1
.end method

.method private getRealCursorStartPosition()I
    .locals 2

    .prologue
    .line 130
    const/4 v0, 0x0

    .line 131
    .local v0, "startPos":I
    iget-boolean v1, p0, Lcom/samsung/musicplus/widget/database/MusicCursor;->mHasShuffleView:Z

    if-eqz v1, :cond_0

    .line 132
    add-int/lit8 v0, v0, 0x1

    .line 134
    :cond_0
    iget-boolean v1, p0, Lcom/samsung/musicplus/widget/database/MusicCursor;->mHasDownLoadView:Z

    if-eqz v1, :cond_1

    .line 135
    add-int/lit8 v0, v0, 0x1

    .line 137
    :cond_1
    return v0
.end method

.method private moveToHeaderPosition(I)Z
    .locals 2
    .param p1, "newPosition"    # I

    .prologue
    const/4 v0, 0x0

    .line 210
    iget-boolean v1, p0, Lcom/samsung/musicplus/widget/database/MusicCursor;->mHasShuffleView:Z

    if-eqz v1, :cond_1

    if-nez p1, :cond_1

    .line 212
    iget-boolean v1, p0, Lcom/samsung/musicplus/widget/database/MusicCursor;->mHasShuffleView:Z

    iput-boolean v1, p0, Lcom/samsung/musicplus/widget/database/MusicCursor;->mIsShuffle:Z

    .line 216
    iget-object v1, p0, Lcom/samsung/musicplus/widget/database/MusicCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    .line 226
    :cond_0
    :goto_0
    return v0

    .line 219
    :cond_1
    iget-boolean v1, p0, Lcom/samsung/musicplus/widget/database/MusicCursor;->mHasDownLoadView:Z

    if-eqz v1, :cond_0

    if-nez p1, :cond_0

    .line 220
    iget-boolean v1, p0, Lcom/samsung/musicplus/widget/database/MusicCursor;->mHasDownLoadView:Z

    iput-boolean v1, p0, Lcom/samsung/musicplus/widget/database/MusicCursor;->mIsDownLoad:Z

    .line 224
    iget-object v1, p0, Lcom/samsung/musicplus/widget/database/MusicCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 358
    iput-boolean v0, p0, Lcom/samsung/musicplus/widget/database/MusicCursor;->mHasShuffleView:Z

    .line 359
    iput-boolean v0, p0, Lcom/samsung/musicplus/widget/database/MusicCursor;->mHasDownLoadView:Z

    .line 360
    iget-object v0, p0, Lcom/samsung/musicplus/widget/database/MusicCursor;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 361
    iget-object v0, p0, Lcom/samsung/musicplus/widget/database/MusicCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 363
    :cond_0
    invoke-super {p0}, Landroid/database/AbstractCursor;->close()V

    .line 364
    return-void
.end method

.method public deactivate()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 341
    iput-boolean v0, p0, Lcom/samsung/musicplus/widget/database/MusicCursor;->mHasShuffleView:Z

    .line 342
    iput-boolean v0, p0, Lcom/samsung/musicplus/widget/database/MusicCursor;->mHasDownLoadView:Z

    .line 346
    invoke-super {p0}, Landroid/database/AbstractCursor;->deactivate()V

    .line 347
    return-void
.end method

.method public getBlob(I)[B
    .locals 1
    .param p1, "column"    # I

    .prologue
    .line 318
    iget-object v0, p0, Lcom/samsung/musicplus/widget/database/MusicCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    return-object v0
.end method

.method public getColumnNames()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 330
    iget-object v0, p0, Lcom/samsung/musicplus/widget/database/MusicCursor;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 331
    iget-object v0, p0, Lcom/samsung/musicplus/widget/database/MusicCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    move-result-object v0

    .line 333
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    goto :goto_0
.end method

.method public getCount()I
    .locals 2

    .prologue
    .line 157
    const/4 v0, 0x0

    .line 158
    .local v0, "count":I
    iget-object v1, p0, Lcom/samsung/musicplus/widget/database/MusicCursor;->mCursor:Landroid/database/Cursor;

    if-eqz v1, :cond_0

    .line 159
    iget-object v1, p0, Lcom/samsung/musicplus/widget/database/MusicCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    .line 162
    :cond_0
    if-lez v0, :cond_2

    .line 163
    iget-boolean v1, p0, Lcom/samsung/musicplus/widget/database/MusicCursor;->mHasShuffleView:Z

    if-eqz v1, :cond_1

    .line 164
    add-int/lit8 v0, v0, 0x1

    .line 166
    :cond_1
    iget-boolean v1, p0, Lcom/samsung/musicplus/widget/database/MusicCursor;->mHasDownLoadView:Z

    if-eqz v1, :cond_2

    .line 167
    add-int/lit8 v0, v0, 0x1

    .line 173
    :cond_2
    return v0
.end method

.method public getDouble(I)D
    .locals 2
    .param p1, "column"    # I

    .prologue
    .line 294
    iget-object v0, p0, Lcom/samsung/musicplus/widget/database/MusicCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v0

    return-wide v0
.end method

.method public getFloat(I)F
    .locals 1
    .param p1, "column"    # I

    .prologue
    .line 286
    iget-object v0, p0, Lcom/samsung/musicplus/widget/database/MusicCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getFloat(I)F

    move-result v0

    return v0
.end method

.method public getInt(I)I
    .locals 1
    .param p1, "column"    # I

    .prologue
    .line 258
    iget-boolean v0, p0, Lcom/samsung/musicplus/widget/database/MusicCursor;->mIsShuffle:Z

    if-eqz v0, :cond_0

    .line 259
    const/16 v0, -0x64

    .line 264
    :goto_0
    return v0

    .line 261
    :cond_0
    iget-boolean v0, p0, Lcom/samsung/musicplus/widget/database/MusicCursor;->mIsDownLoad:Z

    if-eqz v0, :cond_1

    .line 262
    const/16 v0, -0x65

    goto :goto_0

    .line 264
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/widget/database/MusicCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    goto :goto_0
.end method

.method public getLong(I)J
    .locals 2
    .param p1, "column"    # I

    .prologue
    .line 272
    iget-boolean v0, p0, Lcom/samsung/musicplus/widget/database/MusicCursor;->mIsShuffle:Z

    if-eqz v0, :cond_0

    .line 273
    const-wide/16 v0, -0x64

    .line 278
    :goto_0
    return-wide v0

    .line 275
    :cond_0
    iget-boolean v0, p0, Lcom/samsung/musicplus/widget/database/MusicCursor;->mIsDownLoad:Z

    if-eqz v0, :cond_1

    .line 276
    const-wide/16 v0, -0x65

    goto :goto_0

    .line 278
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/widget/database/MusicCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    goto :goto_0
.end method

.method public getRealCursorCount()I
    .locals 2

    .prologue
    .line 145
    const/4 v0, 0x0

    .line 146
    .local v0, "count":I
    iget-object v1, p0, Lcom/samsung/musicplus/widget/database/MusicCursor;->mCursor:Landroid/database/Cursor;

    if-eqz v1, :cond_0

    .line 147
    iget-object v1, p0, Lcom/samsung/musicplus/widget/database/MusicCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    .line 149
    :cond_0
    return v0
.end method

.method public getShort(I)S
    .locals 1
    .param p1, "column"    # I

    .prologue
    .line 250
    iget-object v0, p0, Lcom/samsung/musicplus/widget/database/MusicCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getShort(I)S

    move-result v0

    return v0
.end method

.method public getSongStartPosition(I)I
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 119
    move v0, p1

    .line 120
    .local v0, "realPosition":I
    iget-boolean v1, p0, Lcom/samsung/musicplus/widget/database/MusicCursor;->mHasShuffleView:Z

    if-eqz v1, :cond_0

    .line 121
    add-int/lit8 v0, v0, -0x1

    .line 123
    :cond_0
    iget-boolean v1, p0, Lcom/samsung/musicplus/widget/database/MusicCursor;->mHasDownLoadView:Z

    if-eqz v1, :cond_1

    .line 124
    add-int/lit8 v0, v0, -0x1

    .line 126
    :cond_1
    return v0
.end method

.method public getString(I)Ljava/lang/String;
    .locals 2
    .param p1, "column"    # I

    .prologue
    .line 235
    iget-boolean v0, p0, Lcom/samsung/musicplus/widget/database/MusicCursor;->mIsShuffle:Z

    if-eqz v0, :cond_0

    .line 236
    const-wide/16 v0, -0x64

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    .line 242
    :goto_0
    return-object v0

    .line 238
    :cond_0
    iget-boolean v0, p0, Lcom/samsung/musicplus/widget/database/MusicCursor;->mIsDownLoad:Z

    if-eqz v0, :cond_1

    .line 239
    const-wide/16 v0, -0x65

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 242
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/widget/database/MusicCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getType(I)I
    .locals 1
    .param p1, "column"    # I

    .prologue
    .line 302
    iget-object v0, p0, Lcom/samsung/musicplus/widget/database/MusicCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getType(I)I

    move-result v0

    return v0
.end method

.method public hasDownLoadView()Z
    .locals 1

    .prologue
    .line 115
    iget-boolean v0, p0, Lcom/samsung/musicplus/widget/database/MusicCursor;->mHasDownLoadView:Z

    return v0
.end method

.method public hasShuffleView()Z
    .locals 1

    .prologue
    .line 107
    iget-boolean v0, p0, Lcom/samsung/musicplus/widget/database/MusicCursor;->mHasShuffleView:Z

    return v0
.end method

.method public isNull(I)Z
    .locals 1
    .param p1, "column"    # I

    .prologue
    .line 310
    iget-object v0, p0, Lcom/samsung/musicplus/widget/database/MusicCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    return v0
.end method

.method public onMove(II)Z
    .locals 5
    .param p1, "oldPosition"    # I
    .param p2, "newPosition"    # I

    .prologue
    const/4 v3, 0x0

    .line 182
    iput-boolean v3, p0, Lcom/samsung/musicplus/widget/database/MusicCursor;->mIsShuffle:Z

    .line 183
    iput-boolean v3, p0, Lcom/samsung/musicplus/widget/database/MusicCursor;->mIsDownLoad:Z

    .line 184
    iget-object v3, p0, Lcom/samsung/musicplus/widget/database/MusicCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I

    move-result v0

    .line 185
    .local v0, "count":I
    if-lez v0, :cond_1

    .line 186
    invoke-direct {p0, p2}, Lcom/samsung/musicplus/widget/database/MusicCursor;->moveToHeaderPosition(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 187
    const/4 v2, 0x1

    .line 205
    :goto_0
    return v2

    .line 190
    :cond_0
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/database/MusicCursor;->getRealCursorStartPosition()I

    move-result v1

    .line 193
    .local v1, "cursorStartPos":I
    iget-object v3, p0, Lcom/samsung/musicplus/widget/database/MusicCursor;->mCursor:Landroid/database/Cursor;

    sub-int v4, p2, v1

    invoke-interface {v3, v4}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v2

    .line 198
    .local v2, "success":Z
    goto :goto_0

    .line 200
    .end local v1    # "cursorStartPos":I
    .end local v2    # "success":Z
    :cond_1
    iget-object v3, p0, Lcom/samsung/musicplus/widget/database/MusicCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v3, p2}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v2

    .line 205
    .restart local v2    # "success":Z
    goto :goto_0
.end method

.method public registerContentObserver(Landroid/database/ContentObserver;)V
    .locals 1
    .param p1, "observer"    # Landroid/database/ContentObserver;

    .prologue
    .line 368
    iget-object v0, p0, Lcom/samsung/musicplus/widget/database/MusicCursor;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 369
    iget-object v0, p0, Lcom/samsung/musicplus/widget/database/MusicCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->registerContentObserver(Landroid/database/ContentObserver;)V

    .line 371
    :cond_0
    return-void
.end method

.method public registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1
    .param p1, "observer"    # Landroid/database/DataSetObserver;

    .prologue
    .line 382
    iget-object v0, p0, Lcom/samsung/musicplus/widget/database/MusicCursor;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 383
    iget-object v0, p0, Lcom/samsung/musicplus/widget/database/MusicCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 385
    :cond_0
    return-void
.end method

.method public requery()Z
    .locals 1

    .prologue
    .line 401
    const/4 v0, 0x1

    return v0
.end method

.method public setDownLoadView(Z)V
    .locals 0
    .param p1, "downLoad"    # Z

    .prologue
    .line 111
    iput-boolean p1, p0, Lcom/samsung/musicplus/widget/database/MusicCursor;->mHasDownLoadView:Z

    .line 112
    return-void
.end method

.method public setShuffleView(Z)V
    .locals 0
    .param p1, "shuffle"    # Z

    .prologue
    .line 103
    iput-boolean p1, p0, Lcom/samsung/musicplus/widget/database/MusicCursor;->mHasShuffleView:Z

    .line 104
    return-void
.end method

.method public unregisterContentObserver(Landroid/database/ContentObserver;)V
    .locals 1
    .param p1, "observer"    # Landroid/database/ContentObserver;

    .prologue
    .line 375
    iget-object v0, p0, Lcom/samsung/musicplus/widget/database/MusicCursor;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 376
    iget-object v0, p0, Lcom/samsung/musicplus/widget/database/MusicCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 378
    :cond_0
    return-void
.end method

.method public unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1
    .param p1, "observer"    # Landroid/database/DataSetObserver;

    .prologue
    .line 389
    iget-object v0, p0, Lcom/samsung/musicplus/widget/database/MusicCursor;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 390
    iget-object v0, p0, Lcom/samsung/musicplus/widget/database/MusicCursor;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    .line 392
    :cond_0
    return-void
.end method
