.class Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler$HorizontalFlingRunnable;
.super Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$FlingRunnable;
.source "TwoWayAbsListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "HorizontalFlingRunnable"
.end annotation


# instance fields
.field protected mLastFlingX:I

.field final synthetic this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;


# direct methods
.method private constructor <init>(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;)V
    .locals 0

    .prologue
    .line 5302
    iput-object p1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler$HorizontalFlingRunnable;->this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;

    invoke-direct {p0, p1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$FlingRunnable;-><init>(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;
    .param p2, "x1"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$1;

    .prologue
    .line 5302
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler$HorizontalFlingRunnable;-><init>(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    .line 5336
    iget-object v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler$HorizontalFlingRunnable;->this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;

    iget-object v8, v8, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    iget v8, v8, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mTouchMode:I

    packed-switch v8, :pswitch_data_0

    .line 5395
    :goto_0
    return-void

    .line 5341
    :pswitch_0
    iget-object v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler$HorizontalFlingRunnable;->this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;

    iget-object v8, v8, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    iget v8, v8, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mItemCount:I

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler$HorizontalFlingRunnable;->this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;

    iget-object v8, v8, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    invoke-virtual {v8}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildCount()I

    move-result v8

    if-nez v8, :cond_1

    .line 5342
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler$HorizontalFlingRunnable;->endFling()V

    goto :goto_0

    .line 5346
    :cond_1
    iget-object v6, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler$HorizontalFlingRunnable;->mScroller:Landroid/widget/Scroller;

    .line 5347
    .local v6, "scroller":Landroid/widget/Scroller;
    invoke-virtual {v6}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v4

    .line 5348
    .local v4, "more":Z
    invoke-virtual {v6}, Landroid/widget/Scroller;->getCurrX()I

    move-result v7

    .line 5352
    .local v7, "x":I
    iget v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler$HorizontalFlingRunnable;->mLastFlingX:I

    sub-int v1, v8, v7

    .line 5355
    .local v1, "delta":I
    if-lez v1, :cond_2

    .line 5357
    iget-object v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler$HorizontalFlingRunnable;->this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;

    iget-object v8, v8, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    iget-object v9, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler$HorizontalFlingRunnable;->this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;

    iget-object v9, v9, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    iget v9, v9, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mFirstPosition:I

    iput v9, v8, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mMotionPosition:I

    .line 5358
    iget-object v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler$HorizontalFlingRunnable;->this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;

    iget-object v8, v8, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 5359
    .local v2, "firstView":Landroid/view/View;
    iget-object v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler$HorizontalFlingRunnable;->this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;

    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v9

    iput v9, v8, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->mMotionViewOriginalLeft:I

    .line 5362
    iget-object v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler$HorizontalFlingRunnable;->this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;

    iget-object v8, v8, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    invoke-virtual {v8}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getWidth()I

    move-result v8

    iget-object v9, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler$HorizontalFlingRunnable;->this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;

    iget-object v9, v9, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    invoke-virtual {v9}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getPaddingRight()I

    move-result v9

    sub-int/2addr v8, v9

    iget-object v9, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler$HorizontalFlingRunnable;->this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;

    iget-object v9, v9, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    invoke-virtual {v9}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getPaddingLeft()I

    move-result v9

    sub-int/2addr v8, v9

    add-int/lit8 v8, v8, -0x1

    invoke-static {v8, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 5375
    .end local v2    # "firstView":Landroid/view/View;
    :goto_1
    iget-object v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler$HorizontalFlingRunnable;->this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;

    invoke-virtual {v8, v1, v1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->trackMotionScroll(II)Z

    move-result v0

    .line 5377
    .local v0, "atEnd":Z
    if-eqz v4, :cond_3

    if-nez v0, :cond_3

    .line 5378
    iget-object v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler$HorizontalFlingRunnable;->this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;

    iget-object v8, v8, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    invoke-virtual {v8}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->invalidate()V

    .line 5379
    iput v7, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler$HorizontalFlingRunnable;->mLastFlingX:I

    .line 5380
    iget-object v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler$HorizontalFlingRunnable;->this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;

    iget-object v8, v8, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    invoke-virtual {v8, p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 5365
    .end local v0    # "atEnd":Z
    :cond_2
    iget-object v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler$HorizontalFlingRunnable;->this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;

    iget-object v8, v8, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    invoke-virtual {v8}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildCount()I

    move-result v8

    add-int/lit8 v5, v8, -0x1

    .line 5366
    .local v5, "offsetToLast":I
    iget-object v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler$HorizontalFlingRunnable;->this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;

    iget-object v8, v8, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    iget-object v9, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler$HorizontalFlingRunnable;->this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;

    iget-object v9, v9, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    iget v9, v9, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mFirstPosition:I

    add-int/2addr v9, v5

    iput v9, v8, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mMotionPosition:I

    .line 5368
    iget-object v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler$HorizontalFlingRunnable;->this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;

    iget-object v8, v8, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    invoke-virtual {v8, v5}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 5369
    .local v3, "lastView":Landroid/view/View;
    iget-object v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler$HorizontalFlingRunnable;->this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;

    invoke-virtual {v3}, Landroid/view/View;->getLeft()I

    move-result v9

    iput v9, v8, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->mMotionViewOriginalLeft:I

    .line 5372
    iget-object v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler$HorizontalFlingRunnable;->this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;

    iget-object v8, v8, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    invoke-virtual {v8}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getWidth()I

    move-result v8

    iget-object v9, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler$HorizontalFlingRunnable;->this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;

    iget-object v9, v9, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    invoke-virtual {v9}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getPaddingRight()I

    move-result v9

    sub-int/2addr v8, v9

    iget-object v9, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler$HorizontalFlingRunnable;->this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;

    iget-object v9, v9, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    invoke-virtual {v9}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getPaddingLeft()I

    move-result v9

    sub-int/2addr v8, v9

    add-int/lit8 v8, v8, -0x1

    neg-int v8, v8

    invoke-static {v8, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    goto :goto_1

    .line 5382
    .end local v3    # "lastView":Landroid/view/View;
    .end local v5    # "offsetToLast":I
    .restart local v0    # "atEnd":Z
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler$HorizontalFlingRunnable;->endFling()V

    goto/16 :goto_0

    .line 5336
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method start(I)V
    .locals 9
    .param p1, "initialVelocity"    # I

    .prologue
    const v6, 0x7fffffff

    const/4 v2, 0x0

    .line 5310
    if-gez p1, :cond_0

    move v1, v6

    .line 5311
    .local v1, "initialX":I
    :goto_0
    iput v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler$HorizontalFlingRunnable;->mLastFlingX:I

    .line 5312
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler$HorizontalFlingRunnable;->mScroller:Landroid/widget/Scroller;

    move v3, p1

    move v4, v2

    move v5, v2

    move v7, v2

    move v8, v6

    invoke-virtual/range {v0 .. v8}, Landroid/widget/Scroller;->fling(IIIIIIII)V

    .line 5314
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler$HorizontalFlingRunnable;->this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    const/4 v2, 0x4

    iput v2, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mTouchMode:I

    .line 5315
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler$HorizontalFlingRunnable;->this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    invoke-virtual {v0, p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->post(Ljava/lang/Runnable;)Z

    .line 5323
    return-void

    .end local v1    # "initialX":I
    :cond_0
    move v1, v2

    .line 5310
    goto :goto_0
.end method

.method startScroll(II)V
    .locals 6
    .param p1, "distance"    # I
    .param p2, "duration"    # I

    .prologue
    const/4 v2, 0x0

    .line 5327
    if-gez p1, :cond_0

    const v1, 0x7fffffff

    .line 5328
    .local v1, "initialX":I
    :goto_0
    iput v1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler$HorizontalFlingRunnable;->mLastFlingX:I

    .line 5329
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler$HorizontalFlingRunnable;->mScroller:Landroid/widget/Scroller;

    move v3, p1

    move v4, v2

    move v5, p2

    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    .line 5330
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler$HorizontalFlingRunnable;->this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    const/4 v2, 0x4

    iput v2, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mTouchMode:I

    .line 5331
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler$HorizontalFlingRunnable;->this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$HorizontalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    invoke-virtual {v0, p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->post(Ljava/lang/Runnable;)Z

    .line 5332
    return-void

    .end local v1    # "initialX":I
    :cond_0
    move v1, v2

    .line 5327
    goto :goto_0
.end method
