.class public Lcom/samsung/musicplus/widget/AnchorSizePopupMenu;
.super Ljava/lang/Object;
.source "AnchorSizePopupMenu.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/widget/AnchorSizePopupMenu$OnMenuItemClickListener;,
        Lcom/samsung/musicplus/widget/AnchorSizePopupMenu$SelectionMenuAdapter;
    }
.end annotation


# instance fields
.field private mAdapter:Lcom/samsung/musicplus/widget/AnchorSizePopupMenu$SelectionMenuAdapter;

.field private mAnchorView:Landroid/view/View;

.field private mContext:Landroid/content/Context;

.field private mMenuKeys:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mOnItemClickListener:Lcom/samsung/musicplus/widget/AnchorSizePopupMenu$OnMenuItemClickListener;

.field private mPopupWindow:Landroid/widget/ListPopupWindow;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/View;)V
    .locals 3
    .param p1, "c"    # Landroid/content/Context;
    .param p2, "anchorView"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x0

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/widget/AnchorSizePopupMenu;->mMenuKeys:Landroid/util/SparseArray;

    .line 30
    new-instance v0, Lcom/samsung/musicplus/widget/AnchorSizePopupMenu$SelectionMenuAdapter;

    invoke-direct {v0, p0, v2}, Lcom/samsung/musicplus/widget/AnchorSizePopupMenu$SelectionMenuAdapter;-><init>(Lcom/samsung/musicplus/widget/AnchorSizePopupMenu;Lcom/samsung/musicplus/widget/AnchorSizePopupMenu$1;)V

    iput-object v0, p0, Lcom/samsung/musicplus/widget/AnchorSizePopupMenu;->mAdapter:Lcom/samsung/musicplus/widget/AnchorSizePopupMenu$SelectionMenuAdapter;

    .line 34
    iput-object v2, p0, Lcom/samsung/musicplus/widget/AnchorSizePopupMenu;->mAnchorView:Landroid/view/View;

    .line 37
    iput-object p1, p0, Lcom/samsung/musicplus/widget/AnchorSizePopupMenu;->mContext:Landroid/content/Context;

    .line 38
    new-instance v0, Landroid/widget/ListPopupWindow;

    iget-object v1, p0, Lcom/samsung/musicplus/widget/AnchorSizePopupMenu;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, v2}, Landroid/widget/ListPopupWindow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v0, p0, Lcom/samsung/musicplus/widget/AnchorSizePopupMenu;->mPopupWindow:Landroid/widget/ListPopupWindow;

    .line 39
    iput-object p2, p0, Lcom/samsung/musicplus/widget/AnchorSizePopupMenu;->mAnchorView:Landroid/view/View;

    .line 40
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnchorSizePopupMenu;->mPopupWindow:Landroid/widget/ListPopupWindow;

    iget-object v1, p0, Lcom/samsung/musicplus/widget/AnchorSizePopupMenu;->mAnchorView:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/ListPopupWindow;->setAnchorView(Landroid/view/View;)V

    .line 41
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnchorSizePopupMenu;->mPopupWindow:Landroid/widget/ListPopupWindow;

    iget-object v1, p0, Lcom/samsung/musicplus/widget/AnchorSizePopupMenu;->mAdapter:Lcom/samsung/musicplus/widget/AnchorSizePopupMenu$SelectionMenuAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListPopupWindow;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 42
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnchorSizePopupMenu;->mPopupWindow:Landroid/widget/ListPopupWindow;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListPopupWindow;->setModal(Z)V

    .line 43
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnchorSizePopupMenu;->mPopupWindow:Landroid/widget/ListPopupWindow;

    new-instance v1, Lcom/samsung/musicplus/widget/AnchorSizePopupMenu$1;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/widget/AnchorSizePopupMenu$1;-><init>(Lcom/samsung/musicplus/widget/AnchorSizePopupMenu;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListPopupWindow;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 51
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnchorSizePopupMenu;->mPopupWindow:Landroid/widget/ListPopupWindow;

    iget-object v1, p0, Lcom/samsung/musicplus/widget/AnchorSizePopupMenu;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0006

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ListPopupWindow;->setVerticalOffset(I)V

    .line 52
    return-void
.end method

.method static synthetic access$100(Lcom/samsung/musicplus/widget/AnchorSizePopupMenu;)Lcom/samsung/musicplus/widget/AnchorSizePopupMenu$OnMenuItemClickListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/AnchorSizePopupMenu;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnchorSizePopupMenu;->mOnItemClickListener:Lcom/samsung/musicplus/widget/AnchorSizePopupMenu$OnMenuItemClickListener;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/musicplus/widget/AnchorSizePopupMenu;)Landroid/util/SparseArray;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/AnchorSizePopupMenu;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnchorSizePopupMenu;->mMenuKeys:Landroid/util/SparseArray;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/musicplus/widget/AnchorSizePopupMenu;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/AnchorSizePopupMenu;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnchorSizePopupMenu;->mContext:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public addMenu(ILjava/lang/String;)V
    .locals 3
    .param p1, "id"    # I
    .param p2, "menu"    # Ljava/lang/String;

    .prologue
    .line 55
    iget-object v1, p0, Lcom/samsung/musicplus/widget/AnchorSizePopupMenu;->mMenuKeys:Landroid/util/SparseArray;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Landroid/util/SparseArray;->get(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 56
    .local v0, "exist":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 57
    iget-object v1, p0, Lcom/samsung/musicplus/widget/AnchorSizePopupMenu;->mAdapter:Lcom/samsung/musicplus/widget/AnchorSizePopupMenu$SelectionMenuAdapter;

    invoke-virtual {v1, v0}, Lcom/samsung/musicplus/widget/AnchorSizePopupMenu$SelectionMenuAdapter;->removeMenu(Ljava/lang/String;)V

    .line 59
    :cond_0
    iget-object v1, p0, Lcom/samsung/musicplus/widget/AnchorSizePopupMenu;->mMenuKeys:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, p2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 60
    iget-object v1, p0, Lcom/samsung/musicplus/widget/AnchorSizePopupMenu;->mAdapter:Lcom/samsung/musicplus/widget/AnchorSizePopupMenu$SelectionMenuAdapter;

    invoke-virtual {v1, p2}, Lcom/samsung/musicplus/widget/AnchorSizePopupMenu$SelectionMenuAdapter;->addMenu(Ljava/lang/String;)V

    .line 61
    return-void
.end method

.method public clearMenu()V
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnchorSizePopupMenu;->mMenuKeys:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 72
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnchorSizePopupMenu;->mAdapter:Lcom/samsung/musicplus/widget/AnchorSizePopupMenu$SelectionMenuAdapter;

    invoke-virtual {v0}, Lcom/samsung/musicplus/widget/AnchorSizePopupMenu$SelectionMenuAdapter;->clearMenu()V

    .line 73
    return-void
.end method

.method public dismiss()V
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnchorSizePopupMenu;->mPopupWindow:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->dismiss()V

    .line 93
    return-void
.end method

.method public getAnchorView()Landroid/view/View;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnchorSizePopupMenu;->mAnchorView:Landroid/view/View;

    return-object v0
.end method

.method public isShowing()Z
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnchorSizePopupMenu;->mPopupWindow:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->isShowing()Z

    move-result v0

    return v0
.end method

.method public removeMenu(I)V
    .locals 3
    .param p1, "id"    # I

    .prologue
    .line 64
    iget-object v1, p0, Lcom/samsung/musicplus/widget/AnchorSizePopupMenu;->mMenuKeys:Landroid/util/SparseArray;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Landroid/util/SparseArray;->get(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 65
    .local v0, "exist":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 66
    iget-object v1, p0, Lcom/samsung/musicplus/widget/AnchorSizePopupMenu;->mAdapter:Lcom/samsung/musicplus/widget/AnchorSizePopupMenu$SelectionMenuAdapter;

    invoke-virtual {v1, v0}, Lcom/samsung/musicplus/widget/AnchorSizePopupMenu$SelectionMenuAdapter;->removeMenu(Ljava/lang/String;)V

    .line 68
    :cond_0
    return-void
.end method

.method public setOnMenuItemClickListener(Lcom/samsung/musicplus/widget/AnchorSizePopupMenu$OnMenuItemClickListener;)V
    .locals 0
    .param p1, "l"    # Lcom/samsung/musicplus/widget/AnchorSizePopupMenu$OnMenuItemClickListener;

    .prologue
    .line 88
    iput-object p1, p0, Lcom/samsung/musicplus/widget/AnchorSizePopupMenu;->mOnItemClickListener:Lcom/samsung/musicplus/widget/AnchorSizePopupMenu$OnMenuItemClickListener;

    .line 89
    return-void
.end method

.method public show()V
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnchorSizePopupMenu;->mPopupWindow:Landroid/widget/ListPopupWindow;

    invoke-virtual {v0}, Landroid/widget/ListPopupWindow;->show()V

    .line 81
    return-void
.end method
