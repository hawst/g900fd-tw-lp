.class public Lcom/samsung/musicplus/widget/fragment/CommonListFragment;
.super Lcom/samsung/musicplus/widget/fragment/ListFragment;
.source "CommonListFragment.java"

# interfaces
.implements Landroid/app/LoaderManager$LoaderCallbacks;
.implements Lcom/samsung/musicplus/contents/menu/CommonListMenuHandler;
.implements Lcom/samsung/musicplus/settings/PlayerSettingPreference;
.implements Lcom/samsung/musicplus/util/UiUtils$Defs;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/samsung/musicplus/widget/fragment/ListFragment;",
        "Lcom/samsung/musicplus/settings/PlayerSettingPreference;",
        "Landroid/app/LoaderManager$LoaderCallbacks",
        "<",
        "Landroid/database/Cursor;",
        ">;",
        "Lcom/samsung/musicplus/util/UiUtils$Defs;",
        "Lcom/samsung/musicplus/contents/menu/CommonListMenuHandler;"
    }
.end annotation


# static fields
.field public static final ALBUM_ART_PARSING:Ljava/lang/String; = "album_art_parsing"

.field private static final DEBUG:Z = false

.field private static final DEBUG_LOADER:Z = false

.field public static final EXTRA_ADDITIONAL_KEYWORD:Ljava/lang/String; = "addtional_keyword"

.field public static final EXTRA_HEADER_MODE:Ljava/lang/String; = "header_mode"

.field public static final EXTRA_KEY_WORD:Ljava/lang/String; = "keyword"

.field public static final EXTRA_LIST:Ljava/lang/String; = "list"

.field public static final EXTRA_LIST_MODE:Ljava/lang/String; = "list_mode"

.field public static final EXTRA_LIST_POSITION:Ljava/lang/String; = "list_position"

.field public static final EXTRA_LIST_TYPE:Ljava/lang/String; = "list_type"

.field public static final EXTRA_NOW_PLAYING:Ljava/lang/String; = "is_nowplaying"

.field public static final EXTRA_PLAYLIST_ID:Ljava/lang/String; = "playlist_id"

.field public static final EXTRA_SELECTED_ID:Ljava/lang/String; = "selected_id"

.field public static final EXTRA_TRACK_TITLE:Ljava/lang/String; = "track_title"

.field public static final KEY:Ljava/lang/String; = "key"

.field public static final LIST:Ljava/lang/String; = "list"

.field public static final LIST_SELECTED_KEYWORD:Ljava/lang/String; = "list_selected_keyword"

.field public static final SPLIT_SUB_FRAGMENT:Ljava/lang/String; = "split_sub_fragment"

.field public static final TABCONTENT_SPLIT_SUB_TAG:Ljava/lang/String; = "tabcontent_split_sub"

.field protected static final TAG:Ljava/lang/String; = "MusicUiList"


# instance fields
.field protected mAdapter:Landroid/widget/CursorAdapter;

.field private mAdapterLayout:I

.field protected mAlbumArtLoader:Lcom/samsung/musicplus/util/IAlbumArtLoader;

.field private mAlbumArtParsingEnabled:Z

.field protected mAlbumIdIdx:I

.field protected mAudioIdIdx:I

.field protected mBitDepthIdx:I

.field protected mCountOfData:I

.field protected mDurationIdx:I

.field protected mIndex:Lcom/samsung/musicplus/widget/database/IndexView;

.field protected mIndexIdx:I

.field private mInfo:Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;

.field private mIsEmptyList:Z

.field protected mIsPause:Z

.field private mIsSplitSub:Z

.field protected mIsStop:Z

.field protected mKey:Ljava/lang/String;

.field protected mKeyWordIdx:I

.field protected mList:I

.field private mListPosition:I

.field protected mNoItemTextId:I

.field protected mNumberTextId:I

.field private mNumberView:Landroid/widget/TextView;

.field private mPlayByForce:Z

.field protected mPreferences:Landroid/content/SharedPreferences;

.field protected mQueryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

.field protected mSamplingRateIdx:I

.field private mScrollListener:Landroid/widget/AbsListView$OnScrollListener;

.field protected mSecretBoxIdx:I

.field protected mSelectedPosionKeyword:Ljava/lang/String;

.field protected mSplitSubFragment:Landroid/app/Fragment;

.field protected mText1Idx:I

.field protected mText2Idx:I

.field protected mText3Idx:I

.field private mUiHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 85
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/fragment/ListFragment;-><init>()V

    .line 149
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mIsEmptyList:Z

    .line 151
    iput-boolean v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mIsSplitSub:Z

    .line 153
    iput-object v2, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mSelectedPosionKeyword:Ljava/lang/String;

    .line 155
    iput-object v2, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mSplitSubFragment:Landroid/app/Fragment;

    .line 189
    iput-boolean v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mIsPause:Z

    .line 191
    iput-boolean v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mIsStop:Z

    .line 195
    iput v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mAdapterLayout:I

    .line 665
    new-instance v0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment$1;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment$1;-><init>(Lcom/samsung/musicplus/widget/fragment/CommonListFragment;)V

    iput-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mScrollListener:Landroid/widget/AbsListView$OnScrollListener;

    .line 1178
    iput-boolean v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mPlayByForce:Z

    .line 1331
    new-instance v0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment$3;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment$3;-><init>(Lcom/samsung/musicplus/widget/fragment/CommonListFragment;)V

    iput-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mUiHandler:Landroid/os/Handler;

    return-void
.end method

.method private attachIndexView()V
    .locals 4

    .prologue
    .line 701
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->getListContainer()Landroid/view/ViewGroup;

    move-result-object v1

    .line 702
    .local v1, "vg":Landroid/view/ViewGroup;
    const-string v2, "index"

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    .line 703
    .local v0, "index":Landroid/view/View;
    if-nez v0, :cond_1

    .line 704
    iget-object v2, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mIndex:Lcom/samsung/musicplus/widget/database/IndexView;

    if-nez v2, :cond_0

    .line 705
    new-instance v2, Lcom/samsung/musicplus/widget/database/IndexView;

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/samsung/musicplus/widget/database/IndexView;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mIndex:Lcom/samsung/musicplus/widget/database/IndexView;

    .line 707
    :cond_0
    iget-object v2, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mIndex:Lcom/samsung/musicplus/widget/database/IndexView;

    invoke-virtual {v2}, Lcom/samsung/musicplus/widget/database/IndexView;->getIndexScrollView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 708
    iget-object v2, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mIndex:Lcom/samsung/musicplus/widget/database/IndexView;

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/samsung/musicplus/widget/database/IndexView;->setList(Landroid/widget/ListView;)V

    .line 710
    :cond_1
    return-void
.end method

.method private detachIndexView()V
    .locals 3

    .prologue
    .line 715
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->getListContainer()Landroid/view/ViewGroup;

    move-result-object v1

    .line 716
    .local v1, "vg":Landroid/view/ViewGroup;
    const-string v2, "index"

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewWithTag(Ljava/lang/Object;)Landroid/view/View;

    move-result-object v0

    .line 717
    .local v0, "index":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 718
    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 720
    :cond_0
    return-void
.end method

.method private doShuffle()V
    .locals 4

    .prologue
    .line 1143
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->getViewCount()I

    move-result v1

    if-gtz v1, :cond_0

    .line 1144
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    iget v2, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mNoItemTextId:I

    invoke-virtual {p0, v2}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v1, v2}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->showToast(Landroid/content/Context;Ljava/lang/String;)V

    .line 1149
    :goto_0
    return-void

    .line 1146
    :cond_0
    iget-object v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mAdapter:Landroid/widget/CursorAdapter;

    invoke-virtual {v1}, Landroid/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/musicplus/util/ListUtils;->getSongListForCursor(Landroid/database/Cursor;)[J

    move-result-object v0

    .line 1147
    .local v0, "list":[J
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mList:I

    iget-object v3, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mKey:Ljava/lang/String;

    invoke-static {v1, v2, v3, v0}, Lcom/samsung/musicplus/util/ServiceUtils;->doShuffle(Landroid/content/Context;ILjava/lang/String;[J)V

    goto :goto_0
.end method

.method private invalidateListWithAlbumArtParsing()V
    .locals 2

    .prologue
    .line 509
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->setAlbumArtParsingEnabled(Z)V

    .line 510
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->getAbsListView()Landroid/widget/AbsListView;

    move-result-object v0

    .line 511
    .local v0, "lv":Landroid/widget/AbsListView;
    if-eqz v0, :cond_0

    .line 512
    invoke-virtual {v0}, Landroid/widget/AbsListView;->invalidateViews()V

    .line 514
    :cond_0
    return-void
.end method

.method private isValidItem(J)Z
    .locals 3
    .param p1, "itemId"    # J

    .prologue
    .line 1353
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-ltz v0, :cond_0

    .line 1354
    const/4 v0, 0x1

    .line 1356
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private playSongs(I)V
    .locals 10
    .param p1, "position"    # I

    .prologue
    .line 1194
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->getAudioId(I)J

    move-result-wide v6

    .line 1195
    .local v6, "audioId":J
    const-wide/16 v0, 0x0

    cmp-long v0, v6, v0

    if-gez v0, :cond_0

    .line 1196
    const-string v0, "MusicUiList"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onItemClick position"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " \'s audio id : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1211
    :goto_0
    return-void

    .line 1200
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mAdapter:Landroid/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v8

    .line 1201
    .local v8, "c":Landroid/database/Cursor;
    invoke-static {v8, v6, v7, p1}, Lcom/samsung/musicplus/util/ListUtils;->getListIdForCursor(Landroid/database/Cursor;JI)Lcom/samsung/musicplus/util/ListUtils$SongList;

    move-result-object v9

    .line 1203
    .local v9, "song":Lcom/samsung/musicplus/util/ListUtils$SongList;
    iget-boolean v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mPlayByForce:Z

    if-nez v0, :cond_2

    iget v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mList:I

    invoke-static {v0, v6, v7}, Lcom/samsung/musicplus/util/ServiceUtils;->isSameSong(IJ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1205
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mList:I

    iget-object v2, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mKey:Ljava/lang/String;

    iget-object v3, v9, Lcom/samsung/musicplus/util/ListUtils$SongList;->list:[J

    iget v4, v9, Lcom/samsung/musicplus/util/ListUtils$SongList;->position:I

    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->isPlaying()Z

    move-result v5

    if-nez v5, :cond_1

    const/4 v5, 0x1

    :goto_1
    invoke-static/range {v0 .. v5}, Lcom/samsung/musicplus/util/ServiceUtils;->changeListWithoutPlaying(Landroid/content/Context;ILjava/lang/String;[JIZ)Z

    goto :goto_0

    :cond_1
    const/4 v5, 0x0

    goto :goto_1

    .line 1208
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mList:I

    iget-object v2, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mKey:Ljava/lang/String;

    iget-object v3, v9, Lcom/samsung/musicplus/util/ListUtils$SongList;->list:[J

    iget v4, v9, Lcom/samsung/musicplus/util/ListUtils$SongList;->position:I

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/musicplus/util/ServiceUtils;->openList(Landroid/content/Context;ILjava/lang/String;[JI)Z

    goto :goto_0
.end method

.method private setAdapterSelectedKeyword(Ljava/lang/String;)V
    .locals 1
    .param p1, "keyword"    # Ljava/lang/String;

    .prologue
    .line 1056
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mAdapter:Landroid/widget/CursorAdapter;

    instance-of v0, v0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;

    if-eqz v0, :cond_0

    .line 1057
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mAdapter:Landroid/widget/CursorAdapter;

    check-cast v0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->setSelectedKeyword(Ljava/lang/String;)V

    .line 1059
    :cond_0
    return-void
.end method

.method private setEmptyListState()V
    .locals 1

    .prologue
    .line 301
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mAdapter:Landroid/widget/CursorAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mAdapter:Landroid/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/widget/CursorAdapter;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 302
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mIsEmptyList:Z

    .line 306
    :goto_0
    return-void

    .line 304
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mIsEmptyList:Z

    goto :goto_0
.end method

.method private setListToTop()V
    .locals 2

    .prologue
    .line 735
    iget v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mListPosition:I

    if-lez v1, :cond_0

    .line 736
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->getAbsListView()Landroid/widget/AbsListView;

    move-result-object v0

    .line 737
    .local v0, "lv":Landroid/widget/AbsListView;
    if-nez v0, :cond_1

    .line 749
    .end local v0    # "lv":Landroid/widget/AbsListView;
    :cond_0
    :goto_0
    return-void

    .line 741
    .restart local v0    # "lv":Landroid/widget/AbsListView;
    :cond_1
    new-instance v1, Lcom/samsung/musicplus/widget/fragment/CommonListFragment$2;

    invoke-direct {v1, p0, v0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment$2;-><init>(Lcom/samsung/musicplus/widget/fragment/CommonListFragment;Landroid/widget/AbsListView;)V

    invoke-virtual {v0, v1}, Landroid/widget/AbsListView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method private setSplitSubListInfoArguments(Landroid/app/Fragment;Ljava/lang/String;)V
    .locals 4
    .param p1, "f"    # Landroid/app/Fragment;
    .param p2, "keyWord"    # Ljava/lang/String;

    .prologue
    .line 980
    iget v2, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mList:I

    invoke-static {v2}, Lcom/samsung/musicplus/util/ListUtils;->getSubTrackList(I)I

    move-result v1

    .line 981
    .local v1, "trackList":I
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 982
    .local v0, "data":Landroid/os/Bundle;
    const-string v2, "list"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 983
    const-string v2, "key"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 984
    const-string v2, "split_sub_fragment"

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 985
    invoke-virtual {p1, v0}, Landroid/app/Fragment;->setArguments(Landroid/os/Bundle;)V

    .line 986
    return-void
.end method

.method private showSublistFragment()V
    .locals 2

    .prologue
    .line 289
    sget-boolean v0, Lcom/samsung/musicplus/util/Features;->FLAG_SUPPORT_SPLIT_LIST_VIEW:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mList:I

    invoke-static {v0}, Lcom/samsung/musicplus/util/ListUtils;->isTab(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 290
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mSplitSubFragment:Landroid/app/Fragment;

    if-nez v0, :cond_0

    .line 291
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mList:I

    invoke-static {v0, v1}, Lcom/samsung/musicplus/util/ListUtils;->isSupportSplitSub(Landroid/content/Context;I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mSelectedPosionKeyword:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 293
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->showSplitSubListFragment()V

    .line 294
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mSelectedPosionKeyword:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->launchSplitSubFragment(Ljava/lang/String;)V

    .line 298
    :cond_0
    return-void
.end method

.method private showToast(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "text"    # Ljava/lang/String;

    .prologue
    .line 1265
    const/4 v0, 0x0

    invoke-static {p1, p2, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1266
    return-void
.end method


# virtual methods
.method protected addOtherView(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 6
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    const/4 v5, 0x1

    .line 320
    if-nez p1, :cond_1

    .line 352
    .end local p1    # "c":Landroid/database/Cursor;
    :cond_0
    :goto_0
    return-object p1

    .line 324
    .restart local p1    # "c":Landroid/database/Cursor;
    :cond_1
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    .line 325
    .local v0, "count":I
    iput v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mCountOfData:I

    .line 326
    if-eqz v0, :cond_0

    .line 335
    new-instance v2, Lcom/samsung/musicplus/widget/database/MusicCursor;

    invoke-direct {v2, p1}, Lcom/samsung/musicplus/widget/database/MusicCursor;-><init>(Landroid/database/Cursor;)V

    .line 336
    .local v2, "music":Lcom/samsung/musicplus/widget/database/MusicCursor;
    const/4 v1, 0x0

    .line 338
    .local v1, "hasHeaderView":Z
    iget v3, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mList:I

    invoke-virtual {p0, v3}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->isEnableListShuffle(I)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 339
    invoke-virtual {v2, v5}, Lcom/samsung/musicplus/widget/database/MusicCursor;->setShuffleView(Z)V

    .line 340
    const/4 v1, 0x1

    .line 341
    iget-object v3, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mAdapter:Landroid/widget/CursorAdapter;

    instance-of v3, v3, Lcom/samsung/musicplus/widget/list/TrackListAdapter;

    if-eqz v3, :cond_2

    .line 342
    iget-object v3, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mAdapter:Landroid/widget/CursorAdapter;

    check-cast v3, Lcom/samsung/musicplus/widget/list/TrackListAdapter;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->setShufflePosition(I)V

    .line 347
    :cond_2
    if-eqz v1, :cond_3

    .line 348
    iget-object v3, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mIndex:Lcom/samsung/musicplus/widget/database/IndexView;

    if-eqz v3, :cond_3

    .line 349
    iget-object v3, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mIndex:Lcom/samsung/musicplus/widget/database/IndexView;

    invoke-virtual {v3, v5}, Lcom/samsung/musicplus/widget/database/IndexView;->setDefaultViewCount(I)V

    :cond_3
    move-object p1, v2

    .line 352
    goto :goto_0
.end method

.method public clearListSelectedPosition()V
    .locals 1

    .prologue
    .line 1051
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mSelectedPosionKeyword:Ljava/lang/String;

    .line 1052
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mSelectedPosionKeyword:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->setAdapterSelectedKeyword(Ljava/lang/String;)V

    .line 1053
    return-void
.end method

.method protected createAdapter()Landroid/widget/CursorAdapter;
    .locals 5

    .prologue
    .line 695
    new-instance v1, Lcom/samsung/musicplus/widget/list/CommonListAdapter;

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mAdapterLayout:I

    if-nez v0, :cond_0

    const v0, 0x7f040033

    :goto_0
    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct {v1, v2, v0, v3, v4}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;I)V

    return-object v1

    :cond_0
    iget v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mAdapterLayout:I

    goto :goto_0
.end method

.method protected createMusicMenuHandler()Lcom/samsung/musicplus/common/menu/MusicMenuHandler;
    .locals 0

    .prologue
    .line 1293
    return-object p0
.end method

.method protected createMusicMenus()Lcom/samsung/musicplus/common/menu/AbstractMusicMenus;
    .locals 1

    .prologue
    .line 208
    new-instance v0, Lcom/samsung/musicplus/contents/menu/CommonListMenus;

    invoke-direct {v0}, Lcom/samsung/musicplus/contents/menu/CommonListMenus;-><init>()V

    return-object v0
.end method

.method protected detachSplitSubFragment()V
    .locals 2

    .prologue
    .line 989
    iget-object v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mSplitSubFragment:Landroid/app/Fragment;

    if-eqz v1, :cond_0

    .line 990
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->getChildFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 991
    .local v0, "transaction":Landroid/app/FragmentTransaction;
    iget-object v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mSplitSubFragment:Landroid/app/Fragment;

    invoke-virtual {v0, v1}, Landroid/app/FragmentTransaction;->detach(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 992
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 994
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mSplitSubFragment:Landroid/app/Fragment;

    .line 995
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->setHasOptionsMenu(Z)V

    .line 997
    .end local v0    # "transaction":Landroid/app/FragmentTransaction;
    :cond_0
    return-void
.end method

.method protected dispatchReCreateLoader()V
    .locals 3

    .prologue
    .line 1272
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    iget v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mList:I

    invoke-virtual {v0, v1}, Landroid/app/LoaderManager;->destroyLoader(I)V

    .line 1273
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v0

    iget v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mList:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    .line 1274
    return-void
.end method

.method protected existSplitViewContainer()Z
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1096
    sget-boolean v3, Lcom/samsung/musicplus/util/Features;->FLAG_SUPPORT_SPLIT_LIST_VIEW:Z

    if-eqz v3, :cond_0

    .line 1097
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->getView()Landroid/view/View;

    move-result-object v1

    .line 1098
    .local v1, "view":Landroid/view/View;
    if-eqz v1, :cond_0

    .line 1099
    const v3, 0x7f0d0088

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1100
    .local v0, "v":Landroid/view/View;
    if-eqz v0, :cond_0

    const/4 v2, 0x1

    .line 1103
    .end local v0    # "v":Landroid/view/View;
    .end local v1    # "view":Landroid/view/View;
    :cond_0
    return v2
.end method

.method public getAudioId(I)J
    .locals 5
    .param p1, "position"    # I

    .prologue
    .line 1214
    const-wide/16 v0, -0x1

    .line 1215
    .local v0, "audioId":J
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v3

    invoke-interface {v3, p1}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/database/Cursor;

    .line 1216
    .local v2, "c":Landroid/database/Cursor;
    if-eqz v2, :cond_0

    iget v3, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mAudioIdIdx:I

    const/4 v4, -0x1

    if-le v3, v4, :cond_0

    .line 1217
    iget v3, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mAudioIdIdx:I

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 1219
    :cond_0
    return-wide v0
.end method

.method public getContext()Landroid/content/Context;
    .locals 1

    .prologue
    .line 1278
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    return-object v0
.end method

.method public getContextSelectedId(Landroid/view/ContextMenu$ContextMenuInfo;)J
    .locals 4
    .param p1, "menuInfo"    # Landroid/view/ContextMenu$ContextMenuInfo;

    .prologue
    .line 891
    move-object v0, p1

    check-cast v0, Landroid/widget/AdapterView$AdapterContextMenuInfo;

    .line 892
    .local v0, "mi":Landroid/widget/AdapterView$AdapterContextMenuInfo;
    iget-wide v2, v0, Landroid/widget/AdapterView$AdapterContextMenuInfo;->id:J

    return-wide v2
.end method

.method public getFragment()Landroid/app/Fragment;
    .locals 0

    .prologue
    .line 1303
    return-object p0
.end method

.method public getKey()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1298
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mKey:Ljava/lang/String;

    return-object v0
.end method

.method public getKeyWord(I)Ljava/lang/String;
    .locals 5
    .param p1, "position"    # I

    .prologue
    .line 1238
    const-string v2, ""

    .line 1239
    .local v2, "keyWord":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v3

    invoke-interface {v3, p1}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 1240
    .local v0, "c":Landroid/database/Cursor;
    if-eqz v0, :cond_0

    iget v3, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mKeyWordIdx:I

    const/4 v4, -0x1

    if-le v3, v4, :cond_0

    .line 1241
    iget v3, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mKeyWordIdx:I

    invoke-interface {v0}, Landroid/database/Cursor;->getColumnCount()I

    move-result v4

    if-ge v3, v4, :cond_0

    .line 1243
    :try_start_0
    iget v3, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mKeyWordIdx:I

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 1249
    :cond_0
    :goto_0
    return-object v2

    .line 1244
    :catch_0
    move-exception v1

    .line 1245
    .local v1, "e":Ljava/lang/IndexOutOfBoundsException;
    invoke-virtual {v1}, Ljava/lang/IndexOutOfBoundsException;->printStackTrace()V

    goto :goto_0
.end method

.method public getList()I
    .locals 1

    .prologue
    .line 1283
    iget v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mList:I

    return v0
.end method

.method public getListItemCount()I
    .locals 2

    .prologue
    .line 1162
    const/4 v0, 0x0

    .line 1163
    .local v0, "count":I
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->getAbsListView()Landroid/widget/AbsListView;

    move-result-object v1

    .line 1164
    .local v1, "lv":Landroid/widget/AbsListView;
    if-eqz v1, :cond_0

    .line 1165
    invoke-virtual {v1}, Landroid/widget/AbsListView;->getCount()I

    move-result v0

    .line 1167
    :cond_0
    return v0
.end method

.method protected getListSongCount(I)I
    .locals 2
    .param p1, "listType"    # I

    .prologue
    .line 1171
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->getListItemCount()I

    move-result v0

    .line 1172
    .local v0, "count":I
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->isEnableListShuffle(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1173
    add-int/lit8 v0, v0, -0x1

    .line 1175
    :cond_0
    return v0
.end method

.method public getListType()I
    .locals 1

    .prologue
    .line 1361
    iget v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mList:I

    return v0
.end method

.method protected getNumberOfItmes()I
    .locals 1

    .prologue
    .line 1157
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->getListItemCount()I

    move-result v0

    return v0
.end method

.method protected getPreferences()Landroid/content/SharedPreferences;
    .locals 3

    .prologue
    .line 877
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mPreferences:Landroid/content/SharedPreferences;

    if-nez v0, :cond_0

    .line 878
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "music_player_pref"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mPreferences:Landroid/content/SharedPreferences;

    .line 881
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mPreferences:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method public getQueryArgs()Lcom/samsung/musicplus/util/ListUtils$QueryArgs;
    .locals 1

    .prologue
    .line 1288
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mQueryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    return-object v0
.end method

.method public getSongList(II)[J
    .locals 11
    .param p1, "listType"    # I
    .param p2, "position"    # I

    .prologue
    .line 897
    invoke-virtual {p0, p2}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->getKeyWord(I)Ljava/lang/String;

    move-result-object v9

    .line 898
    .local v9, "keyWord":Ljava/lang/String;
    iget v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mList:I

    invoke-static {v0}, Lcom/samsung/musicplus/util/ListUtils;->getSubTrackList(I)I

    move-result v0

    invoke-static {v0, v9}, Lcom/samsung/musicplus/util/ListUtils;->getMusicListInfo(ILjava/lang/String;)Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;

    move-result-object v10

    .line 899
    .local v10, "li":Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;
    iget-object v6, v10, Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;->queryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    .line 900
    .local v6, "args":Lcom/samsung/musicplus/util/ListUtils$QueryArgs;
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, v6, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->uri:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    iget-object v3, v6, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->selection:Ljava/lang/String;

    iget-object v4, v6, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->selectionArgs:[Ljava/lang/String;

    iget-object v5, v6, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->orderBy:Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 904
    .local v7, "c":Landroid/database/Cursor;
    invoke-static {v7}, Lcom/samsung/musicplus/util/ListUtils;->getSongListForCursor(Landroid/database/Cursor;)[J

    move-result-object v8

    .line 905
    .local v8, "id":[J
    if-eqz v7, :cond_0

    .line 906
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 908
    :cond_0
    return-object v8
.end method

.method public getTitleString(I)Ljava/lang/String;
    .locals 5
    .param p1, "position"    # I

    .prologue
    .line 1223
    const-string v2, ""

    .line 1224
    .local v2, "title":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->getListAdapter()Landroid/widget/ListAdapter;

    move-result-object v3

    invoke-interface {v3, p1}, Landroid/widget/ListAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    .line 1225
    .local v0, "c":Landroid/database/Cursor;
    if-eqz v0, :cond_0

    iget v3, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mText1Idx:I

    const/4 v4, -0x1

    if-le v3, v4, :cond_0

    .line 1226
    iget v3, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mText1Idx:I

    invoke-interface {v0}, Landroid/database/Cursor;->getColumnCount()I

    move-result v4

    if-ge v3, v4, :cond_0

    .line 1228
    :try_start_0
    iget v3, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mText1Idx:I

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 1234
    :cond_0
    :goto_0
    return-object v2

    .line 1229
    :catch_0
    move-exception v1

    .line 1230
    .local v1, "e":Ljava/lang/IndexOutOfBoundsException;
    invoke-virtual {v1}, Ljava/lang/IndexOutOfBoundsException;->printStackTrace()V

    goto :goto_0
.end method

.method protected getValidListItemCount()I
    .locals 6

    .prologue
    .line 1338
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->getAbsListView()Landroid/widget/AbsListView;

    move-result-object v2

    .line 1339
    .local v2, "lv":Landroid/widget/AbsListView;
    if-eqz v2, :cond_1

    .line 1340
    const/4 v3, 0x0

    .line 1341
    .local v3, "validCount":I
    invoke-virtual {v2}, Landroid/widget/AbsListView;->getCount()I

    move-result v0

    .line 1342
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_2

    .line 1343
    invoke-virtual {v2, v1}, Landroid/widget/AbsListView;->getItemIdAtPosition(I)J

    move-result-wide v4

    invoke-direct {p0, v4, v5}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->isValidItem(J)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1344
    add-int/lit8 v3, v3, 0x1

    .line 1342
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1349
    .end local v0    # "count":I
    .end local v1    # "i":I
    .end local v3    # "validCount":I
    :cond_1
    const/4 v3, 0x0

    :cond_2
    return v3
.end method

.method public handlePrepareContextMenu(Landroid/view/Menu;JI)V
    .locals 0
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "itemId"    # J
    .param p4, "position"    # I

    .prologue
    .line 1313
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->prepareContextMenu(Landroid/view/Menu;JI)V

    .line 1314
    return-void
.end method

.method public handlePrepareOptionMenu(Landroid/view/Menu;)V
    .locals 0
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 1308
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->prepareOptionMenu(Landroid/view/Menu;)V

    .line 1309
    return-void
.end method

.method protected initIndexView(I)V
    .locals 4
    .param p1, "count"    # I

    .prologue
    const/4 v3, 0x0

    .line 650
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->getContext()Landroid/content/Context;

    move-result-object v1

    iget v2, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mList:I

    invoke-static {v1, v2}, Lcom/samsung/musicplus/util/ListUtils;->isSupportSplitSub(Landroid/content/Context;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 651
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->detachIndexView()V

    .line 663
    :goto_0
    return-void

    .line 655
    :cond_0
    iget-object v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mInfo:Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mInfo:Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;

    iget-boolean v1, v1, Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;->showIndexView:Z

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mList:I

    invoke-static {v1}, Lcom/samsung/musicplus/util/ListUtils;->isGridType(I)Z

    move-result v1

    if-nez v1, :cond_1

    if-lez p1, :cond_1

    .line 656
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->getListView()Landroid/widget/ListView;

    move-result-object v0

    .line 657
    .local v0, "lv":Landroid/widget/ListView;
    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setVerticalScrollBarEnabled(Z)V

    .line 658
    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setHorizontalScrollBarEnabled(Z)V

    .line 659
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->attachIndexView()V

    goto :goto_0

    .line 661
    .end local v0    # "lv":Landroid/widget/ListView;
    :cond_1
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->detachIndexView()V

    goto :goto_0
.end method

.method protected initLoader()V
    .locals 4

    .prologue
    .line 689
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->getLoaderManager()Landroid/app/LoaderManager;

    move-result-object v1

    iget v2, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mList:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, p0}, Landroid/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/app/LoaderManager$LoaderCallbacks;)Landroid/content/Loader;

    move-result-object v0

    .line 690
    .local v0, "l":Landroid/content/Loader;, "Landroid/content/Loader<Landroid/database/Cursor;>;"
    const-string v2, "MusicUiList"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "initLoader : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " loader : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " l.isReset() : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-nez v0, :cond_0

    const-string v1, " Loader is null"

    :goto_0
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 692
    return-void

    .line 690
    :cond_0
    invoke-virtual {v0}, Landroid/content/Loader;->isReset()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto :goto_0
.end method

.method protected initializeList()V
    .locals 1

    .prologue
    .line 635
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->createAdapter()Landroid/widget/CursorAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mAdapter:Landroid/widget/CursorAdapter;

    .line 636
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mAdapter:Landroid/widget/CursorAdapter;

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 637
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->setListShown(Z)V

    .line 640
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->updateListInfo()Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mInfo:Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;

    .line 641
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->initLoader()V

    .line 642
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->setListToTop()V

    .line 647
    return-void
.end method

.method public initializeSplitSubList(I)V
    .locals 7
    .param p1, "listType"    # I

    .prologue
    .line 1062
    sget-boolean v4, Lcom/samsung/musicplus/util/Features;->FLAG_SUPPORT_SPLIT_LIST_VIEW:Z

    if-eqz v4, :cond_1

    .line 1063
    const-string v4, "MusicUiList"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "initializeSplitSubList() : listType : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1065
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->getView()Landroid/view/View;

    move-result-object v3

    .line 1066
    .local v3, "view":Landroid/view/View;
    if-eqz v3, :cond_1

    .line 1067
    const v4, 0x7f0d0088

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 1068
    .local v2, "v":Landroid/view/View;
    if-eqz v2, :cond_0

    .line 1069
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-static {v4, p1}, Lcom/samsung/musicplus/util/ListUtils;->isSupportSplitSub(Landroid/content/Context;I)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1070
    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1071
    iget-object v4, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mSelectedPosionKeyword:Ljava/lang/String;

    invoke-direct {p0, v4}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->setAdapterSelectedKeyword(Ljava/lang/String;)V

    .line 1078
    :cond_0
    :goto_0
    const v4, 0x7f0d0087

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 1079
    .local v1, "rv":Landroid/view/View;
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 1080
    .local v0, "a":Landroid/app/Activity;
    instance-of v4, v1, Lcom/samsung/musicplus/widget/ListSplitViewResizer;

    if-eqz v4, :cond_1

    instance-of v4, v0, Lcom/samsung/musicplus/widget/ListSplitViewResizer$IResizerControlListener;

    if-eqz v4, :cond_1

    move-object v4, v1

    .line 1082
    check-cast v4, Lcom/samsung/musicplus/widget/ListSplitViewResizer;

    check-cast v0, Lcom/samsung/musicplus/widget/ListSplitViewResizer$IResizerControlListener;

    .end local v0    # "a":Landroid/app/Activity;
    invoke-virtual {v4, v0}, Lcom/samsung/musicplus/widget/ListSplitViewResizer;->registerControlListener(Lcom/samsung/musicplus/widget/ListSplitViewResizer$IResizerControlListener;)V

    .line 1084
    check-cast v1, Lcom/samsung/musicplus/widget/ListSplitViewResizer;

    .end local v1    # "rv":Landroid/view/View;
    iget v4, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mList:I

    invoke-static {v4}, Lcom/samsung/musicplus/util/ListUtils;->getListContentType(I)I

    move-result v4

    invoke-virtual {v1, v4}, Lcom/samsung/musicplus/widget/ListSplitViewResizer;->setListType(I)V

    .line 1088
    .end local v2    # "v":Landroid/view/View;
    .end local v3    # "view":Landroid/view/View;
    :cond_1
    return-void

    .line 1073
    .restart local v2    # "v":Landroid/view/View;
    .restart local v3    # "view":Landroid/view/View;
    :cond_2
    const/16 v4, 0x8

    invoke-virtual {v2, v4}, Landroid/view/View;->setVisibility(I)V

    .line 1074
    const/4 v4, 0x0

    invoke-direct {p0, v4}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->setAdapterSelectedKeyword(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public isEmptyList()Z
    .locals 1

    .prologue
    .line 1366
    iget-boolean v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mIsEmptyList:Z

    return v0
.end method

.method protected isEnableListShuffle(I)Z
    .locals 1
    .param p1, "list"    # I

    .prologue
    .line 362
    invoke-static {p1}, Lcom/samsung/musicplus/util/ListUtils;->isTrack(I)Z

    move-result v0

    return v0
.end method

.method protected isShowNumberView(I)Z
    .locals 4
    .param p1, "count"    # I

    .prologue
    const/4 v3, 0x1

    .line 599
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->getView()Landroid/view/View;

    move-result-object v0

    .line 600
    .local v0, "rootView":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 601
    const v2, 0x7f0d008f

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 602
    .local v1, "view":Landroid/view/View;
    if-eqz v1, :cond_0

    .line 603
    if-nez p1, :cond_1

    .line 604
    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 611
    .end local v1    # "view":Landroid/view/View;
    :cond_0
    :goto_0
    return v3

    .line 605
    .restart local v1    # "view":Landroid/view/View;
    :cond_1
    iget v2, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mList:I

    invoke-static {v2}, Lcom/samsung/musicplus/util/ListUtils;->isGridType(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 606
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method protected isSplitSubListFragment()Z
    .locals 1

    .prologue
    .line 1000
    sget-boolean v0, Lcom/samsung/musicplus/util/Features;->FLAG_SUPPORT_SPLIT_LIST_VIEW:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mIsSplitSub:Z

    if-eqz v0, :cond_0

    .line 1001
    const/4 v0, 0x1

    .line 1003
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected launchSplitSubFragment(Ljava/lang/String;)V
    .locals 6
    .param p1, "keyWord"    # Ljava/lang/String;

    .prologue
    .line 1023
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 1024
    .local v0, "a":Landroid/app/Activity;
    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v0}, Landroid/app/Activity;->isDestroyed()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->isAdded()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1048
    :cond_0
    :goto_0
    return-void

    .line 1028
    :cond_1
    iget-object v2, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mAdapter:Landroid/widget/CursorAdapter;

    instance-of v2, v2, Lcom/samsung/musicplus/widget/list/CommonListAdapter;

    if-eqz v2, :cond_2

    .line 1029
    iget-object v2, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mAdapter:Landroid/widget/CursorAdapter;

    check-cast v2, Lcom/samsung/musicplus/widget/list/CommonListAdapter;

    invoke-virtual {v2, p1}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->setSelectedKeyword(Ljava/lang/String;)V

    .line 1032
    :cond_2
    iget-object v2, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mSplitSubFragment:Landroid/app/Fragment;

    if-eqz v2, :cond_3

    .line 1033
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->getChildFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 1034
    .local v1, "transaction":Landroid/app/FragmentTransaction;
    iget-object v2, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mSplitSubFragment:Landroid/app/Fragment;

    invoke-virtual {v1, v2}, Landroid/app/FragmentTransaction;->detach(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 1035
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 1037
    .end local v1    # "transaction":Landroid/app/FragmentTransaction;
    :cond_3
    iget v2, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mList:I

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/samsung/musicplus/contents/ContentsFragmentFactory;->getNewInstance(ILjava/lang/String;)Landroid/app/Fragment;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mSplitSubFragment:Landroid/app/Fragment;

    .line 1038
    iget-object v2, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mSplitSubFragment:Landroid/app/Fragment;

    invoke-direct {p0, v2, p1}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->setSplitSubListInfoArguments(Landroid/app/Fragment;Ljava/lang/String;)V

    .line 1040
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->setHasOptionsMenu(Z)V

    .line 1042
    iget-object v2, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mSplitSubFragment:Landroid/app/Fragment;

    invoke-virtual {v2}, Landroid/app/Fragment;->isAdded()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1043
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->getChildFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    .line 1044
    .restart local v1    # "transaction":Landroid/app/FragmentTransaction;
    const v2, 0x7f0d0097

    iget-object v3, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mSplitSubFragment:Landroid/app/Fragment;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "tabcontent_split_sub"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mList:I

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 1046
    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    goto :goto_0
.end method

.method protected launchTrackActivity(I)V
    .locals 6
    .param p1, "position"    # I

    .prologue
    .line 966
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->getKeyWord(I)Ljava/lang/String;

    move-result-object v2

    .line 967
    .local v2, "keyWord":Ljava/lang/String;
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->getTitleString(I)Ljava/lang/String;

    move-result-object v3

    .line 969
    .local v3, "title":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 970
    .local v0, "a":Landroid/app/Activity;
    new-instance v1, Landroid/content/Intent;

    const-class v5, Lcom/samsung/musicplus/contents/TrackActivity;

    invoke-direct {v1, v0, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 972
    .local v1, "i":Landroid/content/Intent;
    iget v5, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mList:I

    invoke-static {v5}, Lcom/samsung/musicplus/util/ListUtils;->getSubTrackList(I)I

    move-result v4

    .line 973
    .local v4, "trackList":I
    const-string v5, "list_type"

    invoke-virtual {v1, v5, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 974
    const-string v5, "keyword"

    invoke-virtual {v1, v5, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 975
    const-string v5, "track_title"

    invoke-virtual {v1, v5, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 976
    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    .line 977
    return-void
.end method

.method protected needEmptyView(Landroid/database/Cursor;)Z
    .locals 1
    .param p1, "data"    # Landroid/database/Cursor;

    .prologue
    .line 1091
    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 481
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->setHeaderView()V

    .line 482
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->getUserVisibleHint()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 483
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mAlbumArtParsingEnabled:Z

    .line 484
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->initializeList()V

    .line 488
    :goto_0
    iget-boolean v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mAlbumArtParsingEnabled:Z

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->setAlbumArtParsingEnabled(Z)V

    .line 490
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mList:I

    invoke-static {v0, v1}, Lcom/samsung/musicplus/util/ListUtils;->isSupportSplitSub(Landroid/content/Context;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 491
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mAdapter:Landroid/widget/CursorAdapter;

    instance-of v0, v0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;

    if-eqz v0, :cond_0

    .line 492
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mAdapter:Landroid/widget/CursorAdapter;

    check-cast v0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;

    iget-object v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mSelectedPosionKeyword:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->setSelectedKeyword(Ljava/lang/String;)V

    .line 496
    :cond_0
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/fragment/ListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 497
    return-void

    .line 486
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mUiHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 421
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/fragment/ListFragment;->onAttach(Landroid/app/Activity;)V

    .line 422
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x0

    .line 432
    invoke-static {}, Lcom/samsung/musicplus/util/AlbumArtLoader;->getAlbumArtLoader()Lcom/samsung/musicplus/util/IAlbumArtLoader;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mAlbumArtLoader:Lcom/samsung/musicplus/util/IAlbumArtLoader;

    .line 433
    if-eqz p1, :cond_1

    .line 434
    const-string v1, "list"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mList:I

    .line 435
    const-string v1, "key"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mKey:Ljava/lang/String;

    .line 436
    const-string v1, "album_art_parsing"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mAlbumArtParsingEnabled:Z

    .line 437
    sget-boolean v1, Lcom/samsung/musicplus/util/Features;->FLAG_SUPPORT_SPLIT_LIST_VIEW:Z

    if-eqz v1, :cond_0

    .line 438
    const-string v1, "split_sub_fragment"

    invoke-virtual {p1, v1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mIsSplitSub:Z

    .line 439
    const-string v1, "list_selected_keyword"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mSelectedPosionKeyword:Ljava/lang/String;

    .line 451
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/fragment/ListFragment;->onCreate(Landroid/os/Bundle;)V

    .line 452
    return-void

    .line 442
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 443
    .local v0, "data":Landroid/os/Bundle;
    const-string v1, "list"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mList:I

    .line 444
    const-string v1, "key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mKey:Ljava/lang/String;

    .line 445
    const-string v1, "album_art_parsing"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mAlbumArtParsingEnabled:Z

    .line 446
    sget-boolean v1, Lcom/samsung/musicplus/util/Features;->FLAG_SUPPORT_SPLIT_LIST_VIEW:Z

    if-eqz v1, :cond_0

    .line 447
    const-string v1, "split_sub_fragment"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mIsSplitSub:Z

    goto :goto_0
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/content/Loader;
    .locals 7
    .param p1, "id"    # I
    .param p2, "args"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Landroid/os/Bundle;",
            ")",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 213
    iget-object v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mQueryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    if-nez v1, :cond_0

    .line 214
    const-string v1, "MusicUiList"

    const-string v2, "mQueryArgs is null"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 215
    const/4 v0, 0x0

    .line 231
    :goto_0
    return-object v0

    .line 218
    :cond_0
    const-string v1, "MusicUiList"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "onCreateLoader : selection - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mQueryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    iget-object v3, v3, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->selection:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    new-instance v0, Landroid/content/CursorLoader;

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mQueryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    iget-object v2, v2, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->uri:Landroid/net/Uri;

    iget-object v3, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mQueryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    iget-object v3, v3, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->projection:[Ljava/lang/String;

    iget-object v4, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mQueryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    iget-object v4, v4, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->selection:Ljava/lang/String;

    iget-object v5, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mQueryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    iget-object v5, v5, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->selectionArgs:[Ljava/lang/String;

    iget-object v6, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mQueryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    iget-object v6, v6, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->orderBy:Ljava/lang/String;

    invoke-direct/range {v0 .. v6}, Landroid/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    .local v0, "cl":Landroid/content/CursorLoader;
    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v2, v3}, Landroid/content/CursorLoader;->setUpdateThrottle(J)V

    .line 228
    const-string v1, "MusicUiList"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onCreateLoader : loader "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 459
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/musicplus/widget/fragment/ListFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 818
    invoke-super {p0}, Lcom/samsung/musicplus/widget/fragment/ListFragment;->onDestroy()V

    .line 819
    return-void
.end method

.method public onDestroyView()V
    .locals 2

    .prologue
    .line 808
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->detachIndexView()V

    .line 809
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mUiHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 810
    invoke-super {p0}, Lcom/samsung/musicplus/widget/fragment/ListFragment;->onDestroyView()V

    .line 811
    return-void
.end method

.method public onDetach()V
    .locals 0

    .prologue
    .line 826
    invoke-super {p0}, Lcom/samsung/musicplus/widget/fragment/ListFragment;->onDetach()V

    .line 827
    return-void
.end method

.method public onListItemClick(Landroid/widget/AbsListView;Landroid/view/View;IJ)V
    .locals 4
    .param p1, "l"    # Landroid/widget/AbsListView;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    .line 913
    const-string v0, "MusicUiList"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " onListItemClick "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " position : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Fragment isPause : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mIsPause:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 915
    iget-boolean v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mIsPause:Z

    if-eqz v0, :cond_1

    .line 957
    :cond_0
    :goto_0
    return-void

    .line 919
    :cond_1
    const-wide/16 v0, -0x64

    cmp-long v0, p4, v0

    if-nez v0, :cond_2

    .line 920
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->doShuffle()V

    .line 921
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/musicplus/util/UiUtils;->launchPlayerActivity(Landroid/app/Activity;)V

    goto :goto_0

    .line 925
    :cond_2
    const-wide/16 v0, -0xf

    cmp-long v0, p4, v0

    if-eqz v0, :cond_0

    .line 929
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mList:I

    invoke-static {v0, v1}, Lcom/samsung/musicplus/util/ListUtils;->isSupportSplitSub(Landroid/content/Context;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 930
    invoke-virtual {p0, p3}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->getKeyWord(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mSelectedPosionKeyword:Ljava/lang/String;

    .line 935
    :goto_1
    iget v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mList:I

    invoke-static {v0}, Lcom/samsung/musicplus/util/ListUtils;->getListType(I)I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    .line 943
    :sswitch_0
    sget-boolean v0, Lcom/samsung/musicplus/util/Features;->FLAG_SUPPORT_SPLIT_LIST_VIEW:Z

    if-nez v0, :cond_4

    .line 944
    invoke-virtual {p0, p3}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->launchTrackActivity(I)V

    goto :goto_0

    .line 932
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->removeSubFragment()V

    goto :goto_1

    .line 938
    :sswitch_1
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/musicplus/util/UiUtils;->launchPlayerActivity(Landroid/app/Activity;)V

    .line 940
    invoke-direct {p0, p3}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->playSongs(I)V

    goto :goto_0

    .line 946
    :cond_4
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mList:I

    invoke-static {v0, v1}, Lcom/samsung/musicplus/util/ListUtils;->isSupportSplitSub(Landroid/content/Context;I)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 947
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->showSplitSubListFragment()V

    .line 948
    invoke-virtual {p0, p3}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->getKeyWord(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->launchSplitSubFragment(Ljava/lang/String;)V

    goto :goto_0

    .line 950
    :cond_5
    invoke-virtual {p0, p3}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->launchTrackActivity(I)V

    goto :goto_0

    .line 935
    :sswitch_data_0
    .sparse-switch
        0x10000 -> :sswitch_0
        0x20000 -> :sswitch_1
    .end sparse-switch
.end method

.method public onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V
    .locals 5
    .param p2, "data"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 236
    .local p1, "loader":Landroid/content/Loader;, "Landroid/content/Loader<Landroid/database/Cursor;>;"
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    .line 237
    .local v0, "a":Landroid/app/Activity;
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->getAbsListView()Landroid/widget/AbsListView;

    move-result-object v1

    .line 238
    .local v1, "lv":Landroid/widget/AbsListView;
    if-eqz v1, :cond_0

    instance-of v2, v1, Lcom/samsung/musicplus/widget/list/MusicListView;

    if-eqz v2, :cond_0

    .line 241
    check-cast v1, Lcom/samsung/musicplus/widget/list/MusicListView;

    .end local v1    # "lv":Landroid/widget/AbsListView;
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/samsung/musicplus/widget/list/MusicListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 247
    :cond_0
    const-string v3, "MusicUiList"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, "onLoadFinished : loader "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " data size : "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    if-nez p2, :cond_6

    const-string v2, "Cursor is null"

    :goto_0
    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 251
    iget v2, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mList:I

    invoke-virtual {p1}, Landroid/content/Loader;->getId()I

    move-result v3

    if-ne v2, v3, :cond_4

    .line 252
    invoke-virtual {p0, p2}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->setColumnIndicesAndSetIndex(Landroid/database/Cursor;)V

    .line 253
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->setListShown(Z)V

    .line 254
    iget-object v2, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mAdapter:Landroid/widget/CursorAdapter;

    if-eqz v2, :cond_3

    .line 255
    invoke-virtual {p0, p2}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->needEmptyView(Landroid/database/Cursor;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->existSplitViewContainer()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 256
    :cond_1
    iget v2, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mNoItemTextId:I

    invoke-virtual {p0, v2}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->setEmptyView(I)V

    .line 258
    :cond_2
    iget-object v2, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mAdapter:Landroid/widget/CursorAdapter;

    invoke-virtual {p0, p2}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->addOtherView(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/CursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 259
    iget-object v2, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mAdapter:Landroid/widget/CursorAdapter;

    instance-of v2, v2, Lcom/samsung/musicplus/widget/list/TrackListAdapter;

    if-eqz v2, :cond_3

    .line 260
    instance-of v2, v0, Lcom/samsung/musicplus/MusicBaseActivity;

    if-eqz v2, :cond_3

    .line 261
    invoke-virtual {v0}, Landroid/app/Activity;->invalidateOptionsMenu()V

    .line 265
    :cond_3
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->setEmptyListState()V

    .line 267
    :cond_4
    iget v2, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mList:I

    invoke-static {v2}, Lcom/samsung/musicplus/util/ListUtils;->isTab(I)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 268
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->updateNumberView()V

    .line 271
    :cond_5
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->showSublistFragment()V

    .line 284
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentManager;->invalidateOptionsMenu()V

    .line 285
    return-void

    .line 247
    :cond_6
    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto :goto_0
.end method

.method public bridge synthetic onLoadFinished(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Landroid/content/Loader;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 85
    check-cast p2, Landroid/database/Cursor;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->onLoadFinished(Landroid/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/content/Loader;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 310
    .local p1, "loader":Landroid/content/Loader;, "Landroid/content/Loader<Landroid/database/Cursor;>;"
    const-string v0, "MusicUiList"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "onLoaderReset : loader "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 315
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mAdapter:Landroid/widget/CursorAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    .line 317
    return-void
.end method

.method public onMultiWindowModeChanged(Z)V
    .locals 1
    .param p1, "isMultiWindow"    # Z

    .prologue
    .line 1323
    sget-boolean v0, Lcom/samsung/musicplus/util/Features;->FLAG_SUPPORT_SPLIT_LIST_VIEW:Z

    if-eqz v0, :cond_0

    .line 1324
    iget v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mList:I

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->initializeSplitSubList(I)V

    .line 1325
    if-nez p1, :cond_0

    .line 1326
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->showSublistFragment()V

    .line 1329
    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 785
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mIsPause:Z

    .line 786
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->getAbsListView()Landroid/widget/AbsListView;

    move-result-object v0

    .line 787
    .local v0, "lv":Landroid/widget/AbsListView;
    if-eqz v0, :cond_0

    .line 788
    invoke-virtual {v0}, Landroid/widget/AbsListView;->getFirstVisiblePosition()I

    move-result v1

    iput v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mListPosition:I

    .line 791
    :cond_0
    invoke-super {p0}, Lcom/samsung/musicplus/widget/fragment/ListFragment;->onPause()V

    .line 792
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 772
    invoke-super {p0}, Lcom/samsung/musicplus/widget/fragment/ListFragment;->onResume()V

    .line 773
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mIsPause:Z

    .line 775
    sget-boolean v0, Lcom/samsung/musicplus/util/Features;->FLAG_SUPPORT_SPLIT_LIST_VIEW:Z

    if-eqz v0, :cond_0

    .line 776
    iget v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mList:I

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->initializeSplitSubList(I)V

    .line 778
    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 756
    const-string v0, "list"

    iget v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mList:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 757
    const-string v0, "key"

    iget-object v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mKey:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 758
    const-string v0, "album_art_parsing"

    iget-boolean v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mAlbumArtParsingEnabled:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 760
    sget-boolean v0, Lcom/samsung/musicplus/util/Features;->FLAG_SUPPORT_SPLIT_LIST_VIEW:Z

    if-eqz v0, :cond_0

    .line 761
    const-string v0, "list_selected_keyword"

    iget-object v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mSelectedPosionKeyword:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 762
    const-string v0, "split_sub_fragment"

    iget-boolean v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mIsSplitSub:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 764
    :cond_0
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/fragment/ListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 765
    return-void
.end method

.method public onServiceConnected()V
    .locals 0

    .prologue
    .line 1319
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 724
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mIsStop:Z

    .line 728
    invoke-super {p0}, Lcom/samsung/musicplus/widget/fragment/ListFragment;->onStart()V

    .line 729
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 796
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mIsStop:Z

    .line 800
    invoke-super {p0}, Lcom/samsung/musicplus/widget/fragment/ListFragment;->onStop()V

    .line 801
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 470
    sget-boolean v0, Lcom/samsung/musicplus/util/Features;->FLAG_SUPPORT_SPLIT_LIST_VIEW:Z

    if-eqz v0, :cond_0

    .line 471
    iget v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mList:I

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->initializeSplitSubList(I)V

    .line 473
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/samsung/musicplus/widget/fragment/ListFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 474
    return-void
.end method

.method protected prepareContextMenu(Landroid/view/Menu;JI)V
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;
    .param p2, "itemId"    # J
    .param p4, "position"    # I

    .prologue
    .line 869
    iget v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mList:I

    invoke-static {v0}, Lcom/samsung/musicplus/util/ListUtils;->isTab(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 874
    :cond_0
    return-void
.end method

.method protected prepareOptionMenu(Landroid/view/Menu;)V
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 851
    iget v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mList:I

    invoke-static {v0}, Lcom/samsung/musicplus/util/ListUtils;->isTab(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 852
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->prepareViewOption(Landroid/view/Menu;)V

    .line 856
    :cond_0
    return-void
.end method

.method protected prepareViewOption(Landroid/view/Menu;)V
    .locals 0
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 841
    return-void
.end method

.method public receivePlayerState(Ljava/lang/String;)V
    .locals 1
    .param p1, "action"    # Ljava/lang/String;

    .prologue
    .line 1136
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mSplitSubFragment:Landroid/app/Fragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mSplitSubFragment:Landroid/app/Fragment;

    instance-of v0, v0, Lcom/samsung/musicplus/widget/fragment/OnMusicFragmentInterface;

    if-eqz v0, :cond_0

    .line 1137
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mSplitSubFragment:Landroid/app/Fragment;

    check-cast v0, Lcom/samsung/musicplus/widget/fragment/OnMusicFragmentInterface;

    invoke-interface {v0, p1}, Lcom/samsung/musicplus/widget/fragment/OnMusicFragmentInterface;->receivePlayerState(Ljava/lang/String;)V

    .line 1139
    :cond_0
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/fragment/ListFragment;->receivePlayerState(Ljava/lang/String;)V

    .line 1140
    return-void
.end method

.method protected removeSubFragment()V
    .locals 0

    .prologue
    .line 960
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->detachSplitSubFragment()V

    .line 961
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->clearListSelectedPosition()V

    .line 962
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->showEmptySplitSubListFragment()V

    .line 963
    return-void
.end method

.method protected setAdapterLayout(I)V
    .locals 0
    .param p1, "layout"    # I

    .prologue
    .line 836
    iput p1, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mAdapterLayout:I

    .line 837
    return-void
.end method

.method protected setAlbumArtParsingEnabled(Z)V
    .locals 1
    .param p1, "albumArtParsingEnabled"    # Z

    .prologue
    .line 517
    iput-boolean p1, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mAlbumArtParsingEnabled:Z

    .line 518
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mAdapter:Landroid/widget/CursorAdapter;

    instance-of v0, v0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;

    if-eqz v0, :cond_0

    .line 519
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mAdapter:Landroid/widget/CursorAdapter;

    check-cast v0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->setAlbumArtParsingEnabled(Z)V

    .line 521
    :cond_0
    return-void
.end method

.method protected setColumnIndicesAndSetIndex(Landroid/database/Cursor;)V
    .locals 11
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    .line 371
    if-eqz p1, :cond_4

    .line 372
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mQueryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    iget-object v0, v0, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->audioIdCol:Ljava/lang/String;

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mAudioIdIdx:I

    .line 373
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mQueryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    iget-object v0, v0, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->keyWord:Ljava/lang/String;

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mKeyWordIdx:I

    .line 374
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mQueryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    iget-object v0, v0, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->albumIdCol:Ljava/lang/String;

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mAlbumIdIdx:I

    .line 375
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mQueryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    iget-object v0, v0, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->text1Col:Ljava/lang/String;

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mText1Idx:I

    .line 376
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mQueryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    iget-object v0, v0, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->text2Col:Ljava/lang/String;

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mText2Idx:I

    .line 377
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mQueryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    iget-object v0, v0, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->text3Col:Ljava/lang/String;

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mText3Idx:I

    .line 378
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mQueryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    iget-object v0, v0, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->samplingRateCol:Ljava/lang/String;

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mSamplingRateIdx:I

    .line 379
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mQueryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    iget-object v0, v0, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->durationCol:Ljava/lang/String;

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mDurationIdx:I

    .line 380
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mQueryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    iget-object v0, v0, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->bitDepthCol:Ljava/lang/String;

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mBitDepthIdx:I

    .line 381
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mQueryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    iget-object v0, v0, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->secretBoxCol:Ljava/lang/String;

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mSecretBoxIdx:I

    .line 383
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mQueryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    iget-object v0, v0, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->indexBy:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 384
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mQueryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    iget-object v0, v0, Lcom/samsung/musicplus/util/ListUtils$QueryArgs;->indexBy:Ljava/lang/String;

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mIndexIdx:I

    .line 386
    :cond_0
    iget v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mIndexIdx:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    .line 387
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->initIndexView(I)V

    .line 388
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mIndex:Lcom/samsung/musicplus/widget/database/IndexView;

    if-eqz v0, :cond_1

    .line 389
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mIndex:Lcom/samsung/musicplus/widget/database/IndexView;

    iget v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mIndexIdx:I

    invoke-virtual {v0, p1, v1}, Lcom/samsung/musicplus/widget/database/IndexView;->setIndexer(Landroid/database/Cursor;I)V

    .line 393
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mIndex:Lcom/samsung/musicplus/widget/database/IndexView;

    if-eqz v0, :cond_5

    .line 394
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mIndex:Lcom/samsung/musicplus/widget/database/IndexView;

    iget-object v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mScrollListener:Landroid/widget/AbsListView$OnScrollListener;

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/widget/database/IndexView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 403
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mAdapter:Landroid/widget/CursorAdapter;

    instance-of v0, v0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;

    if-eqz v0, :cond_3

    .line 404
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mAdapter:Landroid/widget/CursorAdapter;

    check-cast v0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;

    iget v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mAudioIdIdx:I

    iget v2, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mText1Idx:I

    iget v3, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mText2Idx:I

    iget v4, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mText3Idx:I

    iget v5, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mAlbumIdIdx:I

    iget v6, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mKeyWordIdx:I

    iget v7, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mSamplingRateIdx:I

    iget v8, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mBitDepthIdx:I

    iget v9, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mSecretBoxIdx:I

    invoke-virtual/range {v0 .. v9}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->setIndex(IIIIIIIII)V

    .line 407
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mAdapter:Landroid/widget/CursorAdapter;

    check-cast v0, Lcom/samsung/musicplus/widget/list/CommonListAdapter;

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->getViewType()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/widget/list/CommonListAdapter;->setViewType(I)V

    .line 409
    :cond_3
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mAdapter:Landroid/widget/CursorAdapter;

    instance-of v0, v0, Lcom/samsung/musicplus/widget/list/TrackListAdapter;

    if-eqz v0, :cond_4

    .line 410
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mAdapter:Landroid/widget/CursorAdapter;

    check-cast v0, Lcom/samsung/musicplus/widget/list/TrackListAdapter;

    iget v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mNumberTextId:I

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->setNumberString(I)V

    .line 411
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mAdapter:Landroid/widget/CursorAdapter;

    check-cast v0, Lcom/samsung/musicplus/widget/list/TrackListAdapter;

    iget v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mDurationIdx:I

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->setDurationIndex(I)V

    .line 414
    :cond_4
    return-void

    .line 397
    :cond_5
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->getAbsListView()Landroid/widget/AbsListView;

    move-result-object v10

    .line 398
    .local v10, "lv":Landroid/widget/AbsListView;
    if-eqz v10, :cond_2

    instance-of v0, v10, Lcom/samsung/musicplus/widget/list/MusicListView;

    if-eqz v0, :cond_2

    .line 399
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mScrollListener:Landroid/widget/AbsListView$OnScrollListener;

    invoke-virtual {v10, v0}, Landroid/widget/AbsListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    goto :goto_0
.end method

.method public setHasOptionsMenu(Z)V
    .locals 1
    .param p1, "hasMenu"    # Z

    .prologue
    .line 1009
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mSplitSubFragment:Landroid/app/Fragment;

    if-eqz v0, :cond_1

    .line 1010
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mSplitSubFragment:Landroid/app/Fragment;

    invoke-virtual {v0, p1}, Landroid/app/Fragment;->setHasOptionsMenu(Z)V

    .line 1016
    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-super {p0, v0}, Lcom/samsung/musicplus/widget/fragment/ListFragment;->setHasOptionsMenu(Z)V

    .line 1020
    :goto_1
    return-void

    .line 1016
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1018
    :cond_1
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/fragment/ListFragment;->setHasOptionsMenu(Z)V

    goto :goto_1
.end method

.method protected setHeaderView()V
    .locals 3

    .prologue
    .line 532
    iget v2, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mList:I

    invoke-static {v2}, Lcom/samsung/musicplus/util/ListUtils;->isTrack(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 545
    :cond_0
    :goto_0
    return-void

    .line 535
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->getView()Landroid/view/View;

    move-result-object v1

    .line 536
    .local v1, "rootView":Landroid/view/View;
    const/4 v0, 0x0

    .line 537
    .local v0, "headerView":Landroid/view/View;
    if-eqz v1, :cond_0

    .line 538
    const v2, 0x7f0d0084

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 540
    instance-of v2, v0, Landroid/view/ViewStub;

    if-eqz v2, :cond_2

    move-object v2, v0

    .line 541
    check-cast v2, Landroid/view/ViewStub;

    invoke-virtual {v2}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    .line 543
    :cond_2
    const v2, 0x7f0d0090

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {p0, v2}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->setNumberView(Landroid/widget/TextView;)V

    goto :goto_0
.end method

.method protected setListHeaderView()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 553
    iget v4, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mList:I

    invoke-static {v4}, Lcom/samsung/musicplus/util/ListUtils;->isTrack(I)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 569
    :cond_0
    :goto_0
    return-void

    .line 556
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->getView()Landroid/view/View;

    move-result-object v3

    .line 557
    .local v3, "rootView":Landroid/view/View;
    const/4 v0, 0x0

    .line 558
    .local v0, "headerView":Landroid/view/View;
    if-eqz v3, :cond_0

    .line 559
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->getAbsListView()Landroid/widget/AbsListView;

    move-result-object v2

    .line 560
    .local v2, "lv":Landroid/widget/AbsListView;
    if-eqz v2, :cond_0

    instance-of v4, v2, Lcom/samsung/musicplus/widget/list/MusicListView;

    if-eqz v4, :cond_0

    .line 561
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    const-string v5, "layout_inflater"

    invoke-virtual {v4, v5}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/LayoutInflater;

    .line 562
    .local v1, "li":Landroid/view/LayoutInflater;
    const v4, 0x7f040020

    invoke-virtual {v1, v4, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    move-object v4, v2

    .line 563
    check-cast v4, Lcom/samsung/musicplus/widget/list/MusicListView;

    invoke-virtual {v4}, Lcom/samsung/musicplus/widget/list/MusicListView;->getHeaderViewsCount()I

    move-result v4

    if-nez v4, :cond_2

    .line 564
    check-cast v2, Lcom/samsung/musicplus/widget/list/MusicListView;

    .end local v2    # "lv":Landroid/widget/AbsListView;
    const/4 v4, 0x0

    invoke-virtual {v2, v0, v6, v4}, Lcom/samsung/musicplus/widget/list/MusicListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    .line 566
    :cond_2
    const v4, 0x7f0d0092

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    invoke-virtual {p0, v4}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->setNumberView(Landroid/widget/TextView;)V

    goto :goto_0
.end method

.method protected setNumberOfTextView(Landroid/widget/TextView;Landroid/content/Context;I)V
    .locals 6
    .param p1, "text"    # Landroid/widget/TextView;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "count"    # I

    .prologue
    .line 1260
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget v2, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mNumberTextId:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, p3, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1261
    .local v0, "number":Ljava/lang/String;
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1262
    return-void
.end method

.method protected setNumberView(Landroid/widget/TextView;)V
    .locals 0
    .param p1, "v"    # Landroid/widget/TextView;

    .prologue
    .line 577
    iput-object p1, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mNumberView:Landroid/widget/TextView;

    .line 578
    return-void
.end method

.method protected final setPlayListByForce(Z)V
    .locals 0
    .param p1, "force"    # Z

    .prologue
    .line 1187
    iput-boolean p1, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mPlayByForce:Z

    .line 1188
    return-void
.end method

.method public setUserVisibleHint(Z)V
    .locals 1
    .param p1, "isVisibleToUser"    # Z

    .prologue
    .line 501
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/fragment/ListFragment;->setUserVisibleHint(Z)V

    .line 502
    iget-boolean v0, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mAlbumArtParsingEnabled:Z

    if-nez v0, :cond_0

    if-nez p1, :cond_1

    .line 506
    :cond_0
    :goto_0
    return-void

    .line 505
    :cond_1
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->invalidateListWithAlbumArtParsing()V

    goto :goto_0
.end method

.method public showEmptySplitSubListFragment()V
    .locals 4

    .prologue
    .line 1121
    sget-boolean v2, Lcom/samsung/musicplus/util/Features;->FLAG_SUPPORT_SPLIT_LIST_VIEW:Z

    if-eqz v2, :cond_0

    .line 1122
    const-string v2, "MusicUiList"

    const-string v3, "showEmptySplitSubList()"

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1124
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->getView()Landroid/view/View;

    move-result-object v1

    .line 1125
    .local v1, "view":Landroid/view/View;
    if-eqz v1, :cond_0

    .line 1126
    const v2, 0x7f0d0097

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1128
    .local v0, "splitSub":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 1129
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1132
    .end local v0    # "splitSub":Landroid/view/View;
    .end local v1    # "view":Landroid/view/View;
    :cond_0
    return-void
.end method

.method public showSplitSubListFragment()V
    .locals 4

    .prologue
    .line 1107
    sget-boolean v2, Lcom/samsung/musicplus/util/Features;->FLAG_SUPPORT_SPLIT_LIST_VIEW:Z

    if-eqz v2, :cond_0

    .line 1108
    const-string v2, "MusicUiList"

    const-string v3, "showSplitSubList()"

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1110
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->getView()Landroid/view/View;

    move-result-object v1

    .line 1111
    .local v1, "view":Landroid/view/View;
    if-eqz v1, :cond_0

    .line 1112
    const v2, 0x7f0d0097

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 1114
    .local v0, "splitSub":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 1115
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 1118
    .end local v0    # "splitSub":Landroid/view/View;
    .end local v1    # "view":Landroid/view/View;
    :cond_0
    return-void
.end method

.method protected updateListInfo()Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;
    .locals 3

    .prologue
    .line 620
    iget v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mList:I

    iget-object v2, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mKey:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/samsung/musicplus/util/ListUtils;->getMusicListInfo(ILjava/lang/String;)Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;

    move-result-object v0

    .line 621
    .local v0, "info":Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;
    iget-object v1, v0, Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;->queryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    iput-object v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mQueryArgs:Lcom/samsung/musicplus/util/ListUtils$QueryArgs;

    .line 622
    iget v1, v0, Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;->numberOfTextId:I

    iput v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mNumberTextId:I

    .line 623
    iget v1, v0, Lcom/samsung/musicplus/util/ListUtils$MusicListInfo;->noItemTextId:I

    iput v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mNoItemTextId:I

    .line 625
    return-object v0
.end method

.method protected updateNumberView()V
    .locals 3

    .prologue
    .line 584
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->getNumberOfItmes()I

    move-result v0

    .line 585
    .local v0, "count":I
    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->isShowNumberView(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 586
    iget-object v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mNumberView:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 587
    iget-object v1, p0, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->mNumberView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {p0, v1, v2, v0}, Lcom/samsung/musicplus/widget/fragment/CommonListFragment;->setNumberOfTextView(Landroid/widget/TextView;Landroid/content/Context;I)V

    .line 590
    :cond_0
    return-void
.end method
