.class Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler$VerticalFlingRunnable;
.super Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$FlingRunnable;
.source "TwoWayAbsListView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "VerticalFlingRunnable"
.end annotation


# instance fields
.field protected mLastFlingY:I

.field final synthetic this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler;


# direct methods
.method private constructor <init>(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler;)V
    .locals 0

    .prologue
    .line 4448
    iput-object p1, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler$VerticalFlingRunnable;->this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler;

    invoke-direct {p0, p1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler$FlingRunnable;-><init>(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$TouchHandler;)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler;Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler;
    .param p2, "x1"    # Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$1;

    .prologue
    .line 4448
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler$VerticalFlingRunnable;-><init>(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    .line 4482
    iget-object v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler$VerticalFlingRunnable;->this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler;

    iget-object v8, v8, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    iget v8, v8, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mTouchMode:I

    packed-switch v8, :pswitch_data_0

    .line 4541
    :goto_0
    return-void

    .line 4487
    :pswitch_0
    iget-object v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler$VerticalFlingRunnable;->this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler;

    iget-object v8, v8, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    iget v8, v8, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mItemCount:I

    if-eqz v8, :cond_0

    iget-object v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler$VerticalFlingRunnable;->this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler;

    iget-object v8, v8, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    invoke-virtual {v8}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildCount()I

    move-result v8

    if-nez v8, :cond_1

    .line 4488
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler$VerticalFlingRunnable;->endFling()V

    goto :goto_0

    .line 4492
    :cond_1
    iget-object v6, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler$VerticalFlingRunnable;->mScroller:Landroid/widget/Scroller;

    .line 4493
    .local v6, "scroller":Landroid/widget/Scroller;
    invoke-virtual {v6}, Landroid/widget/Scroller;->computeScrollOffset()Z

    move-result v4

    .line 4494
    .local v4, "more":Z
    invoke-virtual {v6}, Landroid/widget/Scroller;->getCurrY()I

    move-result v7

    .line 4498
    .local v7, "y":I
    iget v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler$VerticalFlingRunnable;->mLastFlingY:I

    sub-int v1, v8, v7

    .line 4501
    .local v1, "delta":I
    if-lez v1, :cond_2

    .line 4503
    iget-object v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler$VerticalFlingRunnable;->this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler;

    iget-object v8, v8, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    iget-object v9, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler$VerticalFlingRunnable;->this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler;

    iget-object v9, v9, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    iget v9, v9, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mFirstPosition:I

    iput v9, v8, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mMotionPosition:I

    .line 4504
    iget-object v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler$VerticalFlingRunnable;->this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler;

    iget-object v8, v8, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 4505
    .local v2, "firstView":Landroid/view/View;
    iget-object v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler$VerticalFlingRunnable;->this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler;

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v9

    iput v9, v8, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler;->mMotionViewOriginalTop:I

    .line 4508
    iget-object v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler$VerticalFlingRunnable;->this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler;

    iget-object v8, v8, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    invoke-virtual {v8}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getHeight()I

    move-result v8

    iget-object v9, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler$VerticalFlingRunnable;->this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler;

    iget-object v9, v9, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    invoke-virtual {v9}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getPaddingBottom()I

    move-result v9

    sub-int/2addr v8, v9

    iget-object v9, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler$VerticalFlingRunnable;->this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler;

    iget-object v9, v9, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    invoke-virtual {v9}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getPaddingTop()I

    move-result v9

    sub-int/2addr v8, v9

    add-int/lit8 v8, v8, -0x1

    invoke-static {v8, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 4521
    .end local v2    # "firstView":Landroid/view/View;
    :goto_1
    iget-object v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler$VerticalFlingRunnable;->this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler;

    invoke-virtual {v8, v1, v1}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler;->trackMotionScroll(II)Z

    move-result v0

    .line 4523
    .local v0, "atEnd":Z
    if-eqz v4, :cond_3

    if-nez v0, :cond_3

    .line 4524
    iget-object v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler$VerticalFlingRunnable;->this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler;

    iget-object v8, v8, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    invoke-virtual {v8}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->invalidate()V

    .line 4525
    iput v7, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler$VerticalFlingRunnable;->mLastFlingY:I

    .line 4526
    iget-object v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler$VerticalFlingRunnable;->this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler;

    iget-object v8, v8, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    invoke-virtual {v8, p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 4511
    .end local v0    # "atEnd":Z
    :cond_2
    iget-object v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler$VerticalFlingRunnable;->this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler;

    iget-object v8, v8, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    invoke-virtual {v8}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildCount()I

    move-result v8

    add-int/lit8 v5, v8, -0x1

    .line 4512
    .local v5, "offsetToLast":I
    iget-object v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler$VerticalFlingRunnable;->this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler;

    iget-object v8, v8, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    iget-object v9, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler$VerticalFlingRunnable;->this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler;

    iget-object v9, v9, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    iget v9, v9, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mFirstPosition:I

    add-int/2addr v9, v5

    iput v9, v8, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mMotionPosition:I

    .line 4514
    iget-object v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler$VerticalFlingRunnable;->this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler;

    iget-object v8, v8, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    invoke-virtual {v8, v5}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 4515
    .local v3, "lastView":Landroid/view/View;
    iget-object v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler$VerticalFlingRunnable;->this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler;

    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v9

    iput v9, v8, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler;->mMotionViewOriginalTop:I

    .line 4518
    iget-object v8, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler$VerticalFlingRunnable;->this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler;

    iget-object v8, v8, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    invoke-virtual {v8}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getHeight()I

    move-result v8

    iget-object v9, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler$VerticalFlingRunnable;->this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler;

    iget-object v9, v9, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    invoke-virtual {v9}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getPaddingBottom()I

    move-result v9

    sub-int/2addr v8, v9

    iget-object v9, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler$VerticalFlingRunnable;->this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler;

    iget-object v9, v9, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    invoke-virtual {v9}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->getPaddingTop()I

    move-result v9

    sub-int/2addr v8, v9

    add-int/lit8 v8, v8, -0x1

    neg-int v8, v8

    invoke-static {v8, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    goto :goto_1

    .line 4528
    .end local v3    # "lastView":Landroid/view/View;
    .end local v5    # "offsetToLast":I
    .restart local v0    # "atEnd":Z
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler$VerticalFlingRunnable;->endFling()V

    goto/16 :goto_0

    .line 4482
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method start(I)V
    .locals 9
    .param p1, "initialVelocity"    # I

    .prologue
    const v6, 0x7fffffff

    const/4 v1, 0x0

    .line 4456
    if-gez p1, :cond_0

    move v2, v6

    .line 4457
    .local v2, "initialY":I
    :goto_0
    iput v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler$VerticalFlingRunnable;->mLastFlingY:I

    .line 4458
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler$VerticalFlingRunnable;->mScroller:Landroid/widget/Scroller;

    move v3, v1

    move v4, p1

    move v5, v1

    move v7, v1

    move v8, v6

    invoke-virtual/range {v0 .. v8}, Landroid/widget/Scroller;->fling(IIIIIIII)V

    .line 4460
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler$VerticalFlingRunnable;->this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler;

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    const/4 v1, 0x4

    iput v1, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mTouchMode:I

    .line 4461
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler$VerticalFlingRunnable;->this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler;

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    invoke-virtual {v0, p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->post(Ljava/lang/Runnable;)Z

    .line 4469
    return-void

    .end local v2    # "initialY":I
    :cond_0
    move v2, v1

    .line 4456
    goto :goto_0
.end method

.method startScroll(II)V
    .locals 6
    .param p1, "distance"    # I
    .param p2, "duration"    # I

    .prologue
    const/4 v1, 0x0

    .line 4473
    if-gez p1, :cond_0

    const v2, 0x7fffffff

    .line 4474
    .local v2, "initialY":I
    :goto_0
    iput v2, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler$VerticalFlingRunnable;->mLastFlingY:I

    .line 4475
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler$VerticalFlingRunnable;->mScroller:Landroid/widget/Scroller;

    move v3, v1

    move v4, p1

    move v5, p2

    invoke-virtual/range {v0 .. v5}, Landroid/widget/Scroller;->startScroll(IIIII)V

    .line 4476
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler$VerticalFlingRunnable;->this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler;

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    const/4 v1, 0x4

    iput v1, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->mTouchMode:I

    .line 4477
    iget-object v0, p0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler$VerticalFlingRunnable;->this$1:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler;

    iget-object v0, v0, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView$VerticalTouchHandler;->this$0:Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;

    invoke-virtual {v0, p0}, Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAbsListView;->post(Ljava/lang/Runnable;)Z

    .line 4478
    return-void

    .end local v2    # "initialY":I
    :cond_0
    move v2, v1

    .line 4473
    goto :goto_0
.end method
