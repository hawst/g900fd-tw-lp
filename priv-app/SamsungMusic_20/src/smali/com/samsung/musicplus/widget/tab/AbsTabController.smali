.class public abstract Lcom/samsung/musicplus/widget/tab/AbsTabController;
.super Ljava/lang/Object;
.source "AbsTabController.java"

# interfaces
.implements Lcom/samsung/musicplus/widget/tab/ITabController;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/widget/tab/AbsTabController$MusicPagerAdapter;
    }
.end annotation


# static fields
.field public static final DEBUG:Z = false

.field private static final OFF_SCREEN_PAGE_LIMIT:I = 0x1

.field public static final TAG:Ljava/lang/String; = "MusicTab"


# instance fields
.field protected mCurrentContent:Ljava/lang/Object;

.field protected mCurrentTabId:Ljava/lang/Integer;

.field private final mFragmentManager:Landroid/app/FragmentManager;

.field protected mFragments:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Landroid/app/Fragment;",
            ">;"
        }
    .end annotation
.end field

.field private final mHandler:Landroid/os/Handler;

.field protected mOnTabChangeEnabled:Z

.field protected mPager:Landroid/support/v4/view/ViewPager;

.field private mPagerAdapter:Lcom/samsung/musicplus/widget/tab/AbsTabController$MusicPagerAdapter;

.field private mTabIds:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field protected mTalkBack:Lcom/samsung/musicplus/widget/tab/AbsTabTalkBack;


# direct methods
.method public constructor <init>(Landroid/app/FragmentManager;)V
    .locals 1
    .param p1, "fm"    # Landroid/app/FragmentManager;

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/widget/tab/AbsTabController;->mTabIds:Ljava/util/ArrayList;

    .line 46
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/widget/tab/AbsTabController;->mFragments:Landroid/util/SparseArray;

    .line 50
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/widget/tab/AbsTabController;->mHandler:Landroid/os/Handler;

    .line 52
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/widget/tab/AbsTabController;->mOnTabChangeEnabled:Z

    .line 55
    iput-object p1, p0, Lcom/samsung/musicplus/widget/tab/AbsTabController;->mFragmentManager:Landroid/app/FragmentManager;

    .line 56
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/widget/tab/AbsTabController;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/tab/AbsTabController;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/samsung/musicplus/widget/tab/AbsTabController;->mTabIds:Ljava/util/ArrayList;

    return-object v0
.end method

.method private final getCreatedContents()[Ljava/lang/Object;
    .locals 4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 203
    iget-object v3, p0, Lcom/samsung/musicplus/widget/tab/AbsTabController;->mFragments:Landroid/util/SparseArray;

    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    move-result v2

    .line 204
    .local v2, "size":I
    new-array v0, v2, [Landroid/app/Fragment;

    .line 205
    .local v0, "fg":[Ljava/lang/Object;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 206
    iget-object v3, p0, Lcom/samsung/musicplus/widget/tab/AbsTabController;->mFragments:Landroid/util/SparseArray;

    invoke-virtual {v3, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v3

    aput-object v3, v0, v1

    .line 205
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 208
    :cond_0
    return-object v0
.end method

.method private final notifyDataSetChanged()V
    .locals 1

    .prologue
    .line 312
    iget-object v0, p0, Lcom/samsung/musicplus/widget/tab/AbsTabController;->mPagerAdapter:Lcom/samsung/musicplus/widget/tab/AbsTabController$MusicPagerAdapter;

    if-eqz v0, :cond_0

    .line 313
    iget-object v0, p0, Lcom/samsung/musicplus/widget/tab/AbsTabController;->mPagerAdapter:Lcom/samsung/musicplus/widget/tab/AbsTabController$MusicPagerAdapter;

    invoke-virtual {v0}, Lcom/samsung/musicplus/widget/tab/AbsTabController$MusicPagerAdapter;->notifyDataSetChanged()V

    .line 315
    :cond_0
    return-void
.end method


# virtual methods
.method public final addTab(Ljava/lang/Integer;)V
    .locals 2
    .param p1, "tabId"    # Ljava/lang/Integer;

    .prologue
    .line 98
    iget-object v0, p0, Lcom/samsung/musicplus/widget/tab/AbsTabController;->mTabIds:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 99
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/tab/AbsTabController;->notifyDataSetChanged()V

    .line 100
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/widget/tab/AbsTabController;->addTabInternal(I)V

    .line 101
    iget-object v0, p0, Lcom/samsung/musicplus/widget/tab/AbsTabController;->mTalkBack:Lcom/samsung/musicplus/widget/tab/AbsTabTalkBack;

    iget-object v1, p0, Lcom/samsung/musicplus/widget/tab/AbsTabController;->mCurrentTabId:Ljava/lang/Integer;

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/widget/tab/AbsTabController;->findPosition(Ljava/lang/Integer;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/widget/tab/AbsTabTalkBack;->setTabsTts(I)V

    .line 102
    return-void
.end method

.method protected abstract addTabInternal(I)V
.end method

.method public enablePagerMode(Landroid/support/v4/view/ViewPager;)V
    .locals 2
    .param p1, "pager"    # Landroid/support/v4/view/ViewPager;

    .prologue
    .line 283
    iput-object p1, p0, Lcom/samsung/musicplus/widget/tab/AbsTabController;->mPager:Landroid/support/v4/view/ViewPager;

    .line 284
    new-instance v0, Lcom/samsung/musicplus/widget/tab/AbsTabController$MusicPagerAdapter;

    iget-object v1, p0, Lcom/samsung/musicplus/widget/tab/AbsTabController;->mFragmentManager:Landroid/app/FragmentManager;

    invoke-direct {v0, p0, v1}, Lcom/samsung/musicplus/widget/tab/AbsTabController$MusicPagerAdapter;-><init>(Lcom/samsung/musicplus/widget/tab/AbsTabController;Landroid/app/FragmentManager;)V

    iput-object v0, p0, Lcom/samsung/musicplus/widget/tab/AbsTabController;->mPagerAdapter:Lcom/samsung/musicplus/widget/tab/AbsTabController$MusicPagerAdapter;

    .line 285
    iget-object v0, p0, Lcom/samsung/musicplus/widget/tab/AbsTabController;->mPager:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/samsung/musicplus/widget/tab/AbsTabController;->mPagerAdapter:Lcom/samsung/musicplus/widget/tab/AbsTabController$MusicPagerAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    .line 286
    iget-object v0, p0, Lcom/samsung/musicplus/widget/tab/AbsTabController;->mPager:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/samsung/musicplus/widget/tab/AbsTabController;->mPagerAdapter:Lcom/samsung/musicplus/widget/tab/AbsTabController$MusicPagerAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 287
    iget-object v0, p0, Lcom/samsung/musicplus/widget/tab/AbsTabController;->mPager:Landroid/support/v4/view/ViewPager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOffscreenPageLimit(I)V

    .line 288
    return-void
.end method

.method protected final findPosition(Ljava/lang/Integer;)I
    .locals 1
    .param p1, "tabId"    # Ljava/lang/Integer;

    .prologue
    .line 236
    iget-object v0, p0, Lcom/samsung/musicplus/widget/tab/AbsTabController;->mTabIds:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final getCurrentContent()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 193
    iget-object v0, p0, Lcom/samsung/musicplus/widget/tab/AbsTabController;->mCurrentContent:Ljava/lang/Object;

    return-object v0
.end method

.method public final getCurrentTabId()Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/samsung/musicplus/widget/tab/AbsTabController;->mCurrentTabId:Ljava/lang/Integer;

    return-object v0
.end method

.method protected final getFragmentManager()Landroid/app/FragmentManager;
    .locals 1

    .prologue
    .line 333
    iget-object v0, p0, Lcom/samsung/musicplus/widget/tab/AbsTabController;->mFragmentManager:Landroid/app/FragmentManager;

    return-object v0
.end method

.method protected abstract getSelectedTabPosition()I
.end method

.method public final getTabCount()I
    .locals 1

    .prologue
    .line 216
    iget-object v0, p0, Lcom/samsung/musicplus/widget/tab/AbsTabController;->mTabIds:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method protected final getTabId(I)Ljava/lang/Integer;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 226
    iget-object v0, p0, Lcom/samsung/musicplus/widget/tab/AbsTabController;->mTabIds:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    return-object v0
.end method

.method protected abstract initTab()V
.end method

.method public final initializeTabs(Ljava/util/ArrayList;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 66
    .local p1, "tabIds":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/musicplus/widget/tab/AbsTabController;->mOnTabChangeEnabled:Z

    .line 68
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/tab/AbsTabController;->removeAllTabs()V

    .line 71
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/tab/AbsTabController;->initTab()V

    .line 74
    iput-object p1, p0, Lcom/samsung/musicplus/widget/tab/AbsTabController;->mTabIds:Ljava/util/ArrayList;

    .line 75
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/tab/AbsTabController;->notifyDataSetChanged()V

    .line 78
    iget-object v2, p0, Lcom/samsung/musicplus/widget/tab/AbsTabController;->mTabIds:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 79
    .local v1, "tabId":I
    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/widget/tab/AbsTabController;->addTabInternal(I)V

    goto :goto_0

    .line 81
    .end local v1    # "tabId":I
    :cond_0
    iget-object v2, p0, Lcom/samsung/musicplus/widget/tab/AbsTabController;->mTalkBack:Lcom/samsung/musicplus/widget/tab/AbsTabTalkBack;

    const/4 v3, -0x1

    invoke-virtual {v2, v3}, Lcom/samsung/musicplus/widget/tab/AbsTabTalkBack;->setTabsTts(I)V

    .line 82
    return-void
.end method

.method public onServiceConnected()V
    .locals 2

    .prologue
    .line 269
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/tab/AbsTabController;->getCurrentContent()Ljava/lang/Object;

    move-result-object v0

    .line 270
    .local v0, "o":Ljava/lang/Object;
    instance-of v1, v0, Lcom/samsung/musicplus/widget/fragment/OnMusicFragmentInterface;

    if-eqz v1, :cond_0

    .line 271
    check-cast v0, Lcom/samsung/musicplus/widget/fragment/OnMusicFragmentInterface;

    .end local v0    # "o":Ljava/lang/Object;
    invoke-interface {v0}, Lcom/samsung/musicplus/widget/fragment/OnMusicFragmentInterface;->onServiceConnected()V

    .line 273
    :cond_0
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 324
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/widget/tab/AbsTabController;->mOnTabChangeEnabled:Z

    .line 325
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 329
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/widget/tab/AbsTabController;->mOnTabChangeEnabled:Z

    .line 330
    return-void
.end method

.method public receivePlayerState(Ljava/lang/String;)V
    .locals 4
    .param p1, "action"    # Ljava/lang/String;

    .prologue
    .line 247
    iget-object v3, p0, Lcom/samsung/musicplus/widget/tab/AbsTabController;->mPager:Landroid/support/v4/view/ViewPager;

    if-eqz v3, :cond_1

    .line 248
    iget-object v3, p0, Lcom/samsung/musicplus/widget/tab/AbsTabController;->mFragments:Landroid/util/SparseArray;

    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    move-result v2

    .line 249
    .local v2, "size":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_2

    .line 250
    iget-object v3, p0, Lcom/samsung/musicplus/widget/tab/AbsTabController;->mFragments:Landroid/util/SparseArray;

    invoke-virtual {v3, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    .line 251
    .local v1, "o":Ljava/lang/Object;
    instance-of v3, v1, Lcom/samsung/musicplus/widget/fragment/OnMusicFragmentInterface;

    if-eqz v3, :cond_0

    .line 252
    check-cast v1, Lcom/samsung/musicplus/widget/fragment/OnMusicFragmentInterface;

    .end local v1    # "o":Ljava/lang/Object;
    invoke-interface {v1, p1}, Lcom/samsung/musicplus/widget/fragment/OnMusicFragmentInterface;->receivePlayerState(Ljava/lang/String;)V

    .line 249
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 256
    .end local v0    # "i":I
    .end local v2    # "size":I
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/tab/AbsTabController;->getCurrentContent()Ljava/lang/Object;

    move-result-object v1

    .line 257
    .restart local v1    # "o":Ljava/lang/Object;
    instance-of v3, v1, Lcom/samsung/musicplus/widget/fragment/OnMusicFragmentInterface;

    if-eqz v3, :cond_2

    .line 258
    check-cast v1, Lcom/samsung/musicplus/widget/fragment/OnMusicFragmentInterface;

    .end local v1    # "o":Ljava/lang/Object;
    invoke-interface {v1, p1}, Lcom/samsung/musicplus/widget/fragment/OnMusicFragmentInterface;->receivePlayerState(Ljava/lang/String;)V

    .line 261
    :cond_2
    return-void
.end method

.method protected removeAllTabs()V
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/samsung/musicplus/widget/tab/AbsTabController;->mTabIds:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 134
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/tab/AbsTabController;->notifyDataSetChanged()V

    .line 135
    iget-object v0, p0, Lcom/samsung/musicplus/widget/tab/AbsTabController;->mFragments:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 136
    return-void
.end method

.method public removeTab(I)V
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 120
    iget-object v0, p0, Lcom/samsung/musicplus/widget/tab/AbsTabController;->mTabIds:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 121
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/tab/AbsTabController;->notifyDataSetChanged()V

    .line 122
    iget-object v0, p0, Lcom/samsung/musicplus/widget/tab/AbsTabController;->mFragments:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->remove(I)V

    .line 123
    iget-object v0, p0, Lcom/samsung/musicplus/widget/tab/AbsTabController;->mTalkBack:Lcom/samsung/musicplus/widget/tab/AbsTabTalkBack;

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/tab/AbsTabController;->getSelectedTabPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/widget/tab/AbsTabTalkBack;->setTabsTts(I)V

    .line 124
    return-void
.end method

.method public setEnableTab(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 319
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/widget/tab/AbsTabController;->setSwipable(Z)V

    .line 320
    return-void
.end method

.method public setSwipable(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 302
    iget-object v0, p0, Lcom/samsung/musicplus/widget/tab/AbsTabController;->mPager:Landroid/support/v4/view/ViewPager;

    instance-of v0, v0, Lcom/samsung/musicplus/widget/tab/MusicViewPager;

    if-eqz v0, :cond_0

    .line 303
    iget-object v0, p0, Lcom/samsung/musicplus/widget/tab/AbsTabController;->mPager:Landroid/support/v4/view/ViewPager;

    check-cast v0, Lcom/samsung/musicplus/widget/tab/MusicViewPager;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/widget/tab/MusicViewPager;->setSwipeable(Z)V

    .line 305
    :cond_0
    return-void
.end method

.method public final setTabSelected(Ljava/lang/Integer;)V
    .locals 4
    .param p1, "tabId"    # Ljava/lang/Integer;

    .prologue
    .line 145
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/widget/tab/AbsTabController;->findPosition(Ljava/lang/Integer;)I

    move-result v1

    .line 146
    .local v1, "position":I
    if-gez v1, :cond_1

    .line 148
    iget-object v2, p0, Lcom/samsung/musicplus/widget/tab/AbsTabController;->mFragmentManager:Landroid/app/FragmentManager;

    invoke-virtual {p1}, Ljava/lang/Integer;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 149
    .local v0, "fg":Landroid/app/Fragment;
    if-eqz v0, :cond_0

    .line 150
    iget-object v2, p0, Lcom/samsung/musicplus/widget/tab/AbsTabController;->mFragmentManager:Landroid/app/FragmentManager;

    invoke-virtual {v2}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/app/FragmentTransaction;->detach(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 152
    :cond_0
    const/4 v1, 0x0

    .line 155
    .end local v0    # "fg":Landroid/app/Fragment;
    :cond_1
    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/widget/tab/AbsTabController;->setTabSelectedInternal(I)V

    .line 156
    iget-object v2, p0, Lcom/samsung/musicplus/widget/tab/AbsTabController;->mTalkBack:Lcom/samsung/musicplus/widget/tab/AbsTabTalkBack;

    invoke-virtual {v2}, Lcom/samsung/musicplus/widget/tab/AbsTabTalkBack;->speechCurrentTab()V

    .line 157
    return-void
.end method

.method protected abstract setTabSelectedInternal(I)V
.end method

.method public setVisibleTab(Z)V
    .locals 0
    .param p1, "visible"    # Z

    .prologue
    .line 297
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/widget/tab/AbsTabController;->setSwipable(Z)V

    .line 298
    return-void
.end method
