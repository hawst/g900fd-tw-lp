.class public Lcom/samsung/musicplus/widget/scroll/EtcFrameLayout;
.super Landroid/widget/TabHost;
.source "EtcFrameLayout.java"


# static fields
.field private static final DEBUG_TAG:Ljava/lang/String; = "DEBUG_TH"

.field private static final TAB_MOVING:Z


# instance fields
.field private mIsPress:Z

.field private mPressHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 26
    invoke-direct {p0, p1}, Landroid/widget/TabHost;-><init>(Landroid/content/Context;)V

    .line 41
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/widget/scroll/EtcFrameLayout;->mIsPress:Z

    .line 43
    new-instance v0, Lcom/samsung/musicplus/widget/scroll/EtcFrameLayout$1;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/widget/scroll/EtcFrameLayout$1;-><init>(Lcom/samsung/musicplus/widget/scroll/EtcFrameLayout;)V

    iput-object v0, p0, Lcom/samsung/musicplus/widget/scroll/EtcFrameLayout;->mPressHandler:Landroid/os/Handler;

    .line 27
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Landroid/widget/TabHost;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 41
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/widget/scroll/EtcFrameLayout;->mIsPress:Z

    .line 43
    new-instance v0, Lcom/samsung/musicplus/widget/scroll/EtcFrameLayout$1;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/widget/scroll/EtcFrameLayout$1;-><init>(Lcom/samsung/musicplus/widget/scroll/EtcFrameLayout;)V

    iput-object v0, p0, Lcom/samsung/musicplus/widget/scroll/EtcFrameLayout;->mPressHandler:Landroid/os/Handler;

    .line 31
    return-void
.end method

.method static synthetic access$002(Lcom/samsung/musicplus/widget/scroll/EtcFrameLayout;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/scroll/EtcFrameLayout;
    .param p1, "x1"    # Z

    .prologue
    .line 18
    iput-boolean p1, p0, Lcom/samsung/musicplus/widget/scroll/EtcFrameLayout;->mIsPress:Z

    return p1
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "ev"    # Landroid/view/MotionEvent;

    .prologue
    .line 53
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/scroll/EtcFrameLayout;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    .line 54
    .local v0, "tab":Landroid/widget/TabWidget;
    if-eqz v0, :cond_0

    .line 55
    const-string v1, "DEBUG_TH"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "TabHost dispatchTouchEvent action "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " tab.getHeight() : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/widget/TabWidget;->getHeight()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ev.getY() : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 84
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/TabHost;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v1

    return v1
.end method

.method protected onMeasure(II)V
    .locals 3
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 35
    const-string v0, "DEBUG_TH"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "TabHost onMeasure widthMeasureSpec : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " heightMeasureSpec : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 37
    invoke-super {p0, p1, p2}, Landroid/widget/TabHost;->onMeasure(II)V

    .line 39
    return-void
.end method
