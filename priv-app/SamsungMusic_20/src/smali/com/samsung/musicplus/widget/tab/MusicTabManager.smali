.class public Lcom/samsung/musicplus/widget/tab/MusicTabManager;
.super Ljava/lang/Object;
.source "MusicTabManager.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;
.implements Landroid/widget/TabHost$OnTabChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/widget/tab/MusicTabManager$OnMusicTabChangedListener;,
        Lcom/samsung/musicplus/widget/tab/MusicTabManager$TabInfo;
    }
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final TAG:Ljava/lang/String;

.field private final mContainerId:I

.field private final mFragment:Landroid/app/Fragment;

.field private mLastTab:Lcom/samsung/musicplus/widget/tab/MusicTabManager$TabInfo;

.field private mLayoutInflater:Landroid/view/LayoutInflater;

.field private final mListener:Lcom/samsung/musicplus/widget/tab/MusicTabManager$OnMusicTabChangedListener;

.field private mPrevTab:Landroid/view/View;

.field private mPrevText:Ljava/lang/String;

.field private final mRootView:Landroid/view/View;

.field private final mTabHost:Landroid/widget/TabHost;

.field private mTabLists:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mTabSelectedTexts:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mTabs:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/samsung/musicplus/widget/tab/MusicTabManager$TabInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/app/Fragment;Landroid/view/View;I)V
    .locals 2
    .param p1, "fragment"    # Landroid/app/Fragment;
    .param p2, "rootView"    # Landroid/view/View;
    .param p3, "containerId"    # I

    .prologue
    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    const-string v0, "MusicTabManager"

    iput-object v0, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->TAG:Ljava/lang/String;

    .line 60
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mTabs:Ljava/util/HashMap;

    .line 64
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mTabLists:Ljava/util/List;

    .line 66
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mTabSelectedTexts:Ljava/util/List;

    .line 112
    iput-object p1, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mFragment:Landroid/app/Fragment;

    .line 113
    instance-of v0, p1, Lcom/samsung/musicplus/widget/tab/MusicTabManager$OnMusicTabChangedListener;

    if-eqz v0, :cond_0

    .line 114
    check-cast p1, Lcom/samsung/musicplus/widget/tab/MusicTabManager$OnMusicTabChangedListener;

    .end local p1    # "fragment":Landroid/app/Fragment;
    iput-object p1, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mListener:Lcom/samsung/musicplus/widget/tab/MusicTabManager$OnMusicTabChangedListener;

    .line 118
    :goto_0
    iput-object p2, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mRootView:Landroid/view/View;

    .line 119
    iput p3, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mContainerId:I

    .line 120
    iget-object v0, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mRootView:Landroid/view/View;

    const v1, 0x1020012

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TabHost;

    iput-object v0, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mTabHost:Landroid/widget/TabHost;

    .line 121
    iget-object v0, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->setup()V

    .line 122
    iget-object v0, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v0, p0}, Landroid/widget/TabHost;->setOnTabChangedListener(Landroid/widget/TabHost$OnTabChangeListener;)V

    .line 123
    return-void

    .line 116
    .restart local p1    # "fragment":Landroid/app/Fragment;
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mListener:Lcom/samsung/musicplus/widget/tab/MusicTabManager$OnMusicTabChangedListener;

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/widget/tab/MusicTabManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/tab/MusicTabManager;

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->scrollSelector()V

    return-void
.end method

.method static synthetic access$100(Lcom/samsung/musicplus/widget/tab/MusicTabManager;)Landroid/widget/TabHost;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/tab/MusicTabManager;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mTabHost:Landroid/widget/TabHost;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/musicplus/widget/tab/MusicTabManager;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/widget/tab/MusicTabManager;
    .param p1, "x1"    # Z

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->changeFocus(Z)V

    return-void
.end method

.method private addTab(Landroid/widget/TabHost$TabSpec;Ljava/lang/Class;Landroid/os/Bundle;)V
    .locals 5
    .param p1, "tabSpec"    # Landroid/widget/TabHost$TabSpec;
    .param p3, "args"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/TabHost$TabSpec;",
            "Ljava/lang/Class",
            "<*>;",
            "Landroid/os/Bundle;",
            ")V"
        }
    .end annotation

    .prologue
    .line 226
    .local p2, "clss":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    new-instance v3, Lcom/samsung/musicplus/widget/tab/MusicTabManager$TabInfo$DummyTabFactory;

    iget-object v4, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mFragment:Landroid/app/Fragment;

    invoke-virtual {v4}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/samsung/musicplus/widget/tab/MusicTabManager$TabInfo$DummyTabFactory;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1, v3}, Landroid/widget/TabHost$TabSpec;->setContent(Landroid/widget/TabHost$TabContentFactory;)Landroid/widget/TabHost$TabSpec;

    .line 228
    invoke-virtual {p1}, Landroid/widget/TabHost$TabSpec;->getTag()Ljava/lang/String;

    move-result-object v2

    .line 229
    .local v2, "tag":Ljava/lang/String;
    new-instance v1, Lcom/samsung/musicplus/widget/tab/MusicTabManager$TabInfo;

    invoke-direct {v1, v2, p2, p3}, Lcom/samsung/musicplus/widget/tab/MusicTabManager$TabInfo;-><init>(Ljava/lang/String;Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 234
    .local v1, "info":Lcom/samsung/musicplus/widget/tab/MusicTabManager$TabInfo;
    iget-object v3, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mFragment:Landroid/app/Fragment;

    invoke-virtual {v3}, Landroid/app/Fragment;->getChildFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v3

    iput-object v3, v1, Lcom/samsung/musicplus/widget/tab/MusicTabManager$TabInfo;->fragment:Landroid/app/Fragment;

    .line 235
    iget-object v3, v1, Lcom/samsung/musicplus/widget/tab/MusicTabManager$TabInfo;->fragment:Landroid/app/Fragment;

    if-eqz v3, :cond_0

    iget-object v3, v1, Lcom/samsung/musicplus/widget/tab/MusicTabManager$TabInfo;->fragment:Landroid/app/Fragment;

    invoke-virtual {v3}, Landroid/app/Fragment;->isDetached()Z

    move-result v3

    if-nez v3, :cond_0

    .line 236
    iget-object v3, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mFragment:Landroid/app/Fragment;

    invoke-virtual {v3}, Landroid/app/Fragment;->getChildFragmentManager()Landroid/app/FragmentManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 237
    .local v0, "ft":Landroid/app/FragmentTransaction;
    iget-object v3, v1, Lcom/samsung/musicplus/widget/tab/MusicTabManager$TabInfo;->fragment:Landroid/app/Fragment;

    invoke-virtual {v0, v3}, Landroid/app/FragmentTransaction;->detach(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 238
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 240
    .end local v0    # "ft":Landroid/app/FragmentTransaction;
    :cond_0
    iget-object v3, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mTabs:Ljava/util/HashMap;

    invoke-virtual {v3, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 241
    iget-object v3, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v3, p1}, Landroid/widget/TabHost;->addTab(Landroid/widget/TabHost$TabSpec;)V

    .line 242
    return-void
.end method

.method private addTabs([IILandroid/os/Bundle;[Ljava/lang/String;)V
    .locals 8
    .param p1, "tabs"    # [I
    .param p2, "index"    # I
    .param p3, "args"    # Landroid/os/Bundle;
    .param p4, "selectedTexts"    # [Ljava/lang/String;

    .prologue
    .line 180
    if-gez p2, :cond_0

    .line 181
    const/4 p2, 0x0

    .line 183
    :cond_0
    array-length v5, p1

    .line 189
    .local v5, "length":I
    const v0, 0x7fffffff

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->resetCurrnetTab(I)Z

    move-result v6

    .line 190
    .local v6, "doTrick":Z
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    if-ge v7, v5, :cond_3

    .line 191
    aget v1, p1, v7

    if-nez p4, :cond_2

    const/4 v3, 0x0

    :goto_1
    add-int/lit8 v4, v7, 0x1

    move-object v0, p0

    move-object v2, p3

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->addTab(ILandroid/os/Bundle;Ljava/lang/String;II)V

    .line 192
    iget-object v0, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    invoke-virtual {v0, v7}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 193
    if-eqz v6, :cond_1

    if-ne p2, v7, :cond_1

    .line 196
    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->resetCurrnetTab(I)Z

    .line 197
    invoke-virtual {p0, p2}, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->setCurrentTab(I)V

    .line 190
    :cond_1
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 191
    :cond_2
    aget-object v3, p4, v7

    goto :goto_1

    .line 200
    :cond_3
    if-nez v6, :cond_4

    .line 202
    invoke-virtual {p0, p2}, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->setCurrentTab(I)V

    .line 204
    :cond_4
    iget-object v0, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mTabHost:Landroid/widget/TabHost;

    new-instance v1, Lcom/samsung/musicplus/widget/tab/MusicTabManager$1;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/widget/tab/MusicTabManager$1;-><init>(Lcom/samsung/musicplus/widget/tab/MusicTabManager;)V

    invoke-virtual {v0, v1}, Landroid/widget/TabHost;->post(Ljava/lang/Runnable;)Z

    .line 211
    return-void
.end method

.method private changeFocus(Z)V
    .locals 2
    .param p1, "focusable"    # Z

    .prologue
    .line 582
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v1}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/TabWidget;->getChildCount()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 583
    iget-object v1, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v1}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/TabWidget;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/view/View;->setFocusable(Z)V

    .line 582
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 585
    :cond_0
    return-void
.end method

.method private detachList(Ljava/lang/String;)V
    .locals 3
    .param p1, "listTag"    # Ljava/lang/String;

    .prologue
    .line 389
    iget-object v2, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mFragment:Landroid/app/Fragment;

    invoke-virtual {v2}, Landroid/app/Fragment;->getChildFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    .line 390
    .local v1, "fm":Landroid/app/FragmentManager;
    invoke-virtual {v1, p1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    .line 391
    .local v0, "fg":Landroid/app/Fragment;
    if-eqz v0, :cond_0

    .line 392
    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/app/FragmentTransaction;->detach(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 394
    :cond_0
    return-void
.end method

.method private getLastSelectedTabIndex([ILjava/lang/String;)I
    .locals 4
    .param p1, "tabs"    # [I
    .param p2, "list"    # Ljava/lang/String;

    .prologue
    .line 366
    array-length v0, p1

    .line 367
    .local v0, "count":I
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 368
    .local v2, "listType":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 369
    aget v3, p1, v1

    if-ne v3, v2, :cond_0

    .line 373
    .end local v1    # "i":I
    :goto_1
    return v1

    .line 368
    .restart local v1    # "i":I
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 373
    :cond_1
    const/4 v1, -0x1

    goto :goto_1
.end method

.method private getNewTabIndex(I)I
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 397
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->getCurrentTab()I

    move-result v0

    .line 399
    .local v0, "currentTabIndex":I
    if-ne v0, p1, :cond_1

    .line 400
    const/4 v0, 0x0

    .line 404
    .end local v0    # "currentTabIndex":I
    :cond_0
    :goto_0
    return v0

    .line 401
    .restart local v0    # "currentTabIndex":I
    :cond_1
    if-ge p1, v0, :cond_0

    .line 402
    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method private resetCurrnetTab(I)Z
    .locals 6
    .param p1, "value"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 135
    :try_start_0
    iget-object v4, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    const-string v5, "mCurrentTab"

    invoke-virtual {v4, v5}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v1

    .line 136
    .local v1, "f":Ljava/lang/reflect/Field;
    const/4 v4, 0x1

    invoke-virtual {v1, v4}, Ljava/lang/reflect/Field;->setAccessible(Z)V

    .line 137
    iget-object v4, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v1, v4, p1}, Ljava/lang/reflect/Field;->setInt(Ljava/lang/Object;I)V
    :try_end_0
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2

    .line 148
    .end local v1    # "f":Ljava/lang/reflect/Field;
    :goto_0
    return v2

    .line 138
    :catch_0
    move-exception v0

    .local v0, "e":Ljava/lang/NoSuchFieldException;
    move v2, v3

    .line 140
    goto :goto_0

    .line 141
    .end local v0    # "e":Ljava/lang/NoSuchFieldException;
    :catch_1
    move-exception v0

    .local v0, "e":Ljava/lang/IllegalArgumentException;
    move v2, v3

    .line 143
    goto :goto_0

    .line 144
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v0

    .local v0, "e":Ljava/lang/IllegalAccessException;
    move v2, v3

    .line 146
    goto :goto_0
.end method

.method private scrollSelector()V
    .locals 9

    .prologue
    .line 544
    iget-object v7, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mRootView:Landroid/view/View;

    const v8, 0x7f0d0103

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/HorizontalScrollView;

    .line 546
    .local v2, "scrollView":Landroid/widget/HorizontalScrollView;
    if-eqz v2, :cond_0

    iget-object v7, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mTabHost:Landroid/widget/TabHost;

    if-nez v7, :cond_1

    .line 559
    :cond_0
    :goto_0
    return-void

    .line 549
    :cond_1
    invoke-virtual {v2}, Landroid/widget/HorizontalScrollView;->getWidth()I

    move-result v5

    .line 550
    .local v5, "w":I
    invoke-virtual {v2}, Landroid/widget/HorizontalScrollView;->getScrollX()I

    move-result v3

    .line 551
    .local v3, "scrollX":I
    iget-object v7, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v7}, Landroid/widget/TabHost;->getCurrentTabView()Landroid/view/View;

    move-result-object v4

    .line 552
    .local v4, "v":Landroid/view/View;
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    move-result v7

    sub-int/2addr v7, v3

    if-ltz v7, :cond_2

    invoke-virtual {v4}, Landroid/view/View;->getRight()I

    move-result v7

    sub-int/2addr v7, v3

    if-le v7, v5, :cond_0

    .line 554
    :cond_2
    div-int/lit8 v1, v5, 0x2

    .line 555
    .local v1, "containerCenter":I
    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    move-result v7

    invoke-virtual {v4}, Landroid/view/View;->getWidth()I

    move-result v8

    div-int/lit8 v8, v8, 0x2

    add-int v0, v7, v8

    .line 556
    .local v0, "buttonCenter":I
    sub-int v6, v0, v1

    .line 557
    .local v6, "x":I
    invoke-virtual {v2}, Landroid/widget/HorizontalScrollView;->getScrollY()I

    move-result v7

    invoke-virtual {v2, v6, v7}, Landroid/widget/HorizontalScrollView;->scrollTo(II)V

    goto :goto_0
.end method

.method private selectTabText(Landroid/view/View;Ljava/lang/String;)V
    .locals 2
    .param p1, "tab"    # Landroid/view/View;
    .param p2, "selectedText"    # Ljava/lang/String;

    .prologue
    .line 500
    if-eqz p2, :cond_0

    .line 501
    iput-object p1, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mPrevTab:Landroid/view/View;

    .line 502
    const v1, 0x7f0d01bc

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 503
    .local v0, "text":Landroid/widget/TextView;
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mPrevText:Ljava/lang/String;

    .line 504
    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 506
    .end local v0    # "text":Landroid/widget/TextView;
    :cond_0
    return-void
.end method

.method private setTabView(Landroid/widget/TextView;Ljava/lang/CharSequence;II)V
    .locals 6
    .param p1, "label"    # Landroid/widget/TextView;
    .param p2, "tabName"    # Ljava/lang/CharSequence;
    .param p3, "position"    # I
    .param p4, "total"    # I

    .prologue
    const/4 v5, 0x0

    .line 297
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 298
    iget-object v0, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mFragment:Landroid/app/Fragment;

    const v1, 0x7f1001a7

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    const/4 v3, 0x1

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/app/Fragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTag(Ljava/lang/Object;)V

    .line 299
    iget-object v0, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mFragment:Landroid/app/Fragment;

    invoke-virtual {v0}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0, p1, p2, v5}, Lcom/samsung/musicplus/util/UiUtils;->setTabTts(Landroid/content/Context;Landroid/view/View;Ljava/lang/CharSequence;Z)V

    .line 300
    return-void
.end method

.method private unselectTabText(Landroid/view/View;Ljava/lang/String;)V
    .locals 2
    .param p1, "tab"    # Landroid/view/View;
    .param p2, "unselectedText"    # Ljava/lang/String;

    .prologue
    .line 509
    if-eqz p2, :cond_0

    .line 510
    const v1, 0x7f0d01bc

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 511
    .local v0, "text":Landroid/widget/TextView;
    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 513
    .end local v0    # "text":Landroid/widget/TextView;
    :cond_0
    return-void
.end method


# virtual methods
.method public addTab(ILandroid/os/Bundle;)V
    .locals 6
    .param p1, "listType"    # I
    .param p2, "args"    # Landroid/os/Bundle;

    .prologue
    .line 253
    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->getTabCount()I

    move-result v0

    add-int/lit8 v4, v0, 0x1

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->getTabCount()I

    move-result v0

    add-int/lit8 v5, v0, 0x1

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->addTab(ILandroid/os/Bundle;Ljava/lang/String;II)V

    .line 254
    return-void
.end method

.method public addTab(ILandroid/os/Bundle;Ljava/lang/String;II)V
    .locals 7
    .param p1, "listType"    # I
    .param p2, "args"    # Landroid/os/Bundle;
    .param p3, "selectedText"    # Ljava/lang/String;
    .param p4, "position"    # I
    .param p5, "total"    # I

    .prologue
    const/4 v6, 0x0

    .line 266
    iget-object v4, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mLayoutInflater:Landroid/view/LayoutInflater;

    if-nez v4, :cond_0

    .line 270
    iget-object v4, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mFragment:Landroid/app/Fragment;

    invoke-virtual {v4}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-static {v4}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    iput-object v4, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mLayoutInflater:Landroid/view/LayoutInflater;

    .line 273
    :cond_0
    iget-object v4, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mLayoutInflater:Landroid/view/LayoutInflater;

    const v5, 0x7f040090

    invoke-virtual {v4, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 274
    .local v1, "tab":Landroid/view/View;
    const v4, 0x7f0d01bc

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 275
    .local v0, "label":Landroid/widget/TextView;
    iget-object v4, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mTabHost:Landroid/widget/TabHost;

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/widget/TabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/widget/TabHost$TabSpec;->setIndicator(Landroid/view/View;)Landroid/widget/TabHost$TabSpec;

    move-result-object v3

    .line 276
    .local v3, "tabSpec":Landroid/widget/TabHost$TabSpec;
    iget-object v4, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mFragment:Landroid/app/Fragment;

    invoke-static {p1}, Lcom/samsung/musicplus/util/ListUtils;->getDefaultTabName(I)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/app/Fragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 277
    .local v2, "tabName":Ljava/lang/String;
    iget-object v4, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mFragment:Landroid/app/Fragment;

    invoke-virtual {v4}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/musicplus/util/UiUtils;->checkHoverUI(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 278
    const/4 v4, 0x0

    invoke-static {v1, v6, v4}, Lcom/samsung/musicplus/library/hardware/AirView;->setView(Landroid/view/View;Lcom/samsung/musicplus/library/hardware/AirView$OnAirViewPopupListener;Z)V

    .line 281
    :cond_1
    invoke-direct {p0, v0, v2, p4, p5}, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->setTabView(Landroid/widget/TextView;Ljava/lang/CharSequence;II)V

    .line 283
    iget-object v4, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mTabSelectedTexts:Ljava/util/List;

    if-nez p3, :cond_2

    .end local v2    # "tabName":Ljava/lang/String;
    :goto_0
    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 284
    const-class v4, Lcom/samsung/musicplus/contents/ContentsFragmentFactory;

    invoke-direct {p0, v3, v4, p2}, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->addTab(Landroid/widget/TabHost$TabSpec;Ljava/lang/Class;Landroid/os/Bundle;)V

    .line 285
    iget-object v4, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mTabLists:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 286
    return-void

    .restart local v2    # "tabName":Ljava/lang/String;
    :cond_2
    move-object v2, p3

    .line 283
    goto :goto_0
.end method

.method public getCurrentContent()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 219
    iget-object v0, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mLastTab:Lcom/samsung/musicplus/widget/tab/MusicTabManager$TabInfo;

    if-eqz v0, :cond_0

    .line 220
    iget-object v0, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mLastTab:Lcom/samsung/musicplus/widget/tab/MusicTabManager$TabInfo;

    iget-object v0, v0, Lcom/samsung/musicplus/widget/tab/MusicTabManager$TabInfo;->fragment:Landroid/app/Fragment;

    .line 222
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getCurrentTab()I
    .locals 1

    .prologue
    .line 422
    iget-object v0, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getCurrentTab()I

    move-result v0

    return v0
.end method

.method public getIndex([ILjava/lang/String;)I
    .locals 1
    .param p1, "tabs"    # [I
    .param p2, "tag"    # Ljava/lang/String;

    .prologue
    .line 380
    invoke-direct {p0, p1, p2}, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->getLastSelectedTabIndex([ILjava/lang/String;)I

    move-result v0

    .line 381
    .local v0, "index":I
    if-gez v0, :cond_0

    .line 382
    invoke-direct {p0, p2}, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->detachList(Ljava/lang/String;)V

    .line 383
    const/4 v0, 0x0

    .line 385
    :cond_0
    return v0
.end method

.method public getLastSelectedTab()Lcom/samsung/musicplus/widget/tab/MusicTabManager$TabInfo;
    .locals 1

    .prologue
    .line 426
    iget-object v0, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mLastTab:Lcom/samsung/musicplus/widget/tab/MusicTabManager$TabInfo;

    return-object v0
.end method

.method public getTabCount()I
    .locals 1

    .prologue
    .line 430
    iget-object v0, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mTabLists:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .locals 0
    .param p1, "arg0"    # Landroid/view/View;
    .param p2, "arg1"    # Z

    .prologue
    .line 579
    return-void
.end method

.method public onTabChanged(Ljava/lang/String;)V
    .locals 7
    .param p1, "tabId"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v3, 0x0

    .line 442
    iget-object v2, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mTabs:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/musicplus/widget/tab/MusicTabManager$TabInfo;

    .line 443
    .local v1, "newTab":Lcom/samsung/musicplus/widget/tab/MusicTabManager$TabInfo;
    iget-object v2, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mLastTab:Lcom/samsung/musicplus/widget/tab/MusicTabManager$TabInfo;

    if-eq v2, v1, :cond_4

    .line 444
    iget-object v2, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mFragment:Landroid/app/Fragment;

    invoke-virtual {v2}, Landroid/app/Fragment;->getChildFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    .line 445
    .local v0, "ft":Landroid/app/FragmentTransaction;
    iget-object v2, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mLastTab:Lcom/samsung/musicplus/widget/tab/MusicTabManager$TabInfo;

    if-eqz v2, :cond_1

    .line 446
    iget-object v2, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mLastTab:Lcom/samsung/musicplus/widget/tab/MusicTabManager$TabInfo;

    iget-object v2, v2, Lcom/samsung/musicplus/widget/tab/MusicTabManager$TabInfo;->fragment:Landroid/app/Fragment;

    if-eqz v2, :cond_0

    .line 448
    iget-object v2, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mLastTab:Lcom/samsung/musicplus/widget/tab/MusicTabManager$TabInfo;

    iget-object v2, v2, Lcom/samsung/musicplus/widget/tab/MusicTabManager$TabInfo;->fragment:Landroid/app/Fragment;

    invoke-virtual {v0, v2}, Landroid/app/FragmentTransaction;->detach(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    .line 449
    iget-object v2, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mPrevTab:Landroid/view/View;

    iget-object v4, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mPrevText:Ljava/lang/String;

    invoke-direct {p0, v2, v4}, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->unselectTabText(Landroid/view/View;Ljava/lang/String;)V

    .line 450
    iget-object v2, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mFragment:Landroid/app/Fragment;

    invoke-virtual {v2}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget-object v4, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mPrevTab:Landroid/view/View;

    invoke-static {v2, v4, v5}, Lcom/samsung/musicplus/util/UiUtils;->setTabTextEffect(Landroid/content/Context;Landroid/view/View;Z)V

    .line 451
    iget-object v2, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mFragment:Landroid/app/Fragment;

    invoke-virtual {v2}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget-object v4, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mPrevTab:Landroid/view/View;

    invoke-static {v2, v4, v3, v5}, Lcom/samsung/musicplus/util/UiUtils;->setTabTts(Landroid/content/Context;Landroid/view/View;Ljava/lang/CharSequence;Z)V

    .line 453
    :cond_0
    invoke-direct {p0, v5}, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->changeFocus(Z)V

    .line 455
    :cond_1
    if-eqz v1, :cond_3

    .line 456
    iget-object v2, v1, Lcom/samsung/musicplus/widget/tab/MusicTabManager$TabInfo;->fragment:Landroid/app/Fragment;

    if-nez v2, :cond_5

    .line 457
    iget-object v2, v1, Lcom/samsung/musicplus/widget/tab/MusicTabManager$TabInfo;->tag:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2, v3}, Lcom/samsung/musicplus/contents/ContentsFragmentFactory;->getNewInstance(ILjava/lang/String;)Landroid/app/Fragment;

    move-result-object v2

    iput-object v2, v1, Lcom/samsung/musicplus/widget/tab/MusicTabManager$TabInfo;->fragment:Landroid/app/Fragment;

    .line 463
    iget v2, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mContainerId:I

    iget-object v4, v1, Lcom/samsung/musicplus/widget/tab/MusicTabManager$TabInfo;->fragment:Landroid/app/Fragment;

    iget-object v5, v1, Lcom/samsung/musicplus/widget/tab/MusicTabManager$TabInfo;->tag:Ljava/lang/String;

    invoke-virtual {v0, v2, v4, v5}, Landroid/app/FragmentTransaction;->add(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    .line 472
    :goto_0
    iget-object v2, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mListener:Lcom/samsung/musicplus/widget/tab/MusicTabManager$OnMusicTabChangedListener;

    if-eqz v2, :cond_2

    .line 473
    iget-object v4, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mListener:Lcom/samsung/musicplus/widget/tab/MusicTabManager$OnMusicTabChangedListener;

    iget-object v2, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mLastTab:Lcom/samsung/musicplus/widget/tab/MusicTabManager$TabInfo;

    if-nez v2, :cond_6

    move-object v2, v3

    :goto_1
    iget-object v5, v1, Lcom/samsung/musicplus/widget/tab/MusicTabManager$TabInfo;->fragment:Landroid/app/Fragment;

    invoke-interface {v4, p1, v2, v5}, Lcom/samsung/musicplus/widget/tab/MusicTabManager$OnMusicTabChangedListener;->onTabChanged(Ljava/lang/String;Landroid/app/Fragment;Landroid/app/Fragment;)V

    .line 477
    :cond_2
    iget-object v2, v1, Lcom/samsung/musicplus/widget/tab/MusicTabManager$TabInfo;->fragment:Landroid/app/Fragment;

    invoke-virtual {v2, v6}, Landroid/app/Fragment;->setHasOptionsMenu(Z)V

    .line 478
    iget-object v2, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v2}, Landroid/widget/TabHost;->getCurrentTabView()Landroid/view/View;

    move-result-object v4

    iget-object v2, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mTabSelectedTexts:Ljava/util/List;

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->getCurrentTab()I

    move-result v5

    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-direct {p0, v4, v2}, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->selectTabText(Landroid/view/View;Ljava/lang/String;)V

    .line 479
    iget-object v2, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mFragment:Landroid/app/Fragment;

    invoke-virtual {v2}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget-object v4, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v4}, Landroid/widget/TabHost;->getCurrentTabView()Landroid/view/View;

    move-result-object v4

    invoke-static {v2, v4, v6}, Lcom/samsung/musicplus/util/UiUtils;->setTabTextEffect(Landroid/content/Context;Landroid/view/View;Z)V

    .line 481
    iget-object v2, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mFragment:Landroid/app/Fragment;

    invoke-virtual {v2}, Landroid/app/Fragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    iget-object v4, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v4}, Landroid/widget/TabHost;->getCurrentTabView()Landroid/view/View;

    move-result-object v4

    invoke-static {v2, v4, v3, v6}, Lcom/samsung/musicplus/util/UiUtils;->setTabTts(Landroid/content/Context;Landroid/view/View;Ljava/lang/CharSequence;Z)V

    .line 484
    :cond_3
    iput-object v1, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mLastTab:Lcom/samsung/musicplus/widget/tab/MusicTabManager$TabInfo;

    .line 485
    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commitAllowingStateLoss()I

    .line 488
    .end local v0    # "ft":Landroid/app/FragmentTransaction;
    :cond_4
    invoke-direct {p0}, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->scrollSelector()V

    .line 489
    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    new-instance v3, Lcom/samsung/musicplus/widget/tab/MusicTabManager$3;

    invoke-direct {v3, p0}, Lcom/samsung/musicplus/widget/tab/MusicTabManager$3;-><init>(Lcom/samsung/musicplus/widget/tab/MusicTabManager;)V

    const-wide/16 v4, 0x12c

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 497
    return-void

    .line 469
    .restart local v0    # "ft":Landroid/app/FragmentTransaction;
    :cond_5
    iget-object v2, v1, Lcom/samsung/musicplus/widget/tab/MusicTabManager$TabInfo;->fragment:Landroid/app/Fragment;

    invoke-virtual {v0, v2}, Landroid/app/FragmentTransaction;->attach(Landroid/app/Fragment;)Landroid/app/FragmentTransaction;

    goto :goto_0

    .line 473
    :cond_6
    iget-object v2, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mLastTab:Lcom/samsung/musicplus/widget/tab/MusicTabManager$TabInfo;

    iget-object v2, v2, Lcom/samsung/musicplus/widget/tab/MusicTabManager$TabInfo;->fragment:Landroid/app/Fragment;

    goto :goto_1
.end method

.method public refreshTab([ILjava/lang/String;)V
    .locals 1
    .param p1, "tabs"    # [I
    .param p2, "listType"    # Ljava/lang/String;

    .prologue
    .line 340
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->refreshTab([ILjava/lang/String;[Ljava/lang/String;)V

    .line 341
    return-void
.end method

.method public refreshTab([ILjava/lang/String;[Ljava/lang/String;)V
    .locals 1
    .param p1, "tabs"    # [I
    .param p2, "listType"    # Ljava/lang/String;
    .param p3, "selectedTexts"    # [Ljava/lang/String;

    .prologue
    .line 351
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->removeAllTab()V

    .line 352
    invoke-direct {p0, p1, p2}, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->getLastSelectedTabIndex([ILjava/lang/String;)I

    move-result v0

    invoke-virtual {p0, p1, v0, p3}, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->setTab([II[Ljava/lang/String;)V

    .line 353
    return-void
.end method

.method public removeAllTab()V
    .locals 1

    .prologue
    .line 359
    iget-object v0, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->clearAllTabs()V

    .line 360
    iget-object v0, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mTabs:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 361
    iget-object v0, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mTabLists:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 362
    iget-object v0, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mTabSelectedTexts:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 363
    return-void
.end method

.method public removeTab(I)V
    .locals 8
    .param p1, "index"    # I

    .prologue
    .line 308
    iget-object v7, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mTabLists:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v0

    .line 309
    .local v0, "count":I
    add-int/lit8 v7, v0, -0x1

    new-array v6, v7, [I

    .line 310
    .local v6, "tabs":[I
    add-int/lit8 v7, v0, -0x1

    new-array v5, v7, [Ljava/lang/String;

    .line 311
    .local v5, "selectedTexts":[Ljava/lang/String;
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->getNewTabIndex(I)I

    move-result v2

    .line 312
    .local v2, "newTabIndex":I
    const/4 v3, 0x0

    .line 314
    .local v3, "position":I
    const/4 v1, 0x0

    .local v1, "i":I
    move v4, v3

    .end local v3    # "position":I
    .local v4, "position":I
    :goto_0
    if-ge v1, v0, :cond_2

    .line 315
    if-ne v1, p1, :cond_0

    move v3, v4

    .line 314
    .end local v4    # "position":I
    .restart local v3    # "position":I
    :goto_1
    add-int/lit8 v1, v1, 0x1

    move v4, v3

    .end local v3    # "position":I
    .restart local v4    # "position":I
    goto :goto_0

    .line 319
    :cond_0
    iget-object v7, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mTabSelectedTexts:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    if-lez v7, :cond_1

    .line 320
    iget-object v7, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mTabSelectedTexts:Ljava/util/List;

    invoke-interface {v7, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    aput-object v7, v5, v4

    .line 323
    :cond_1
    add-int/lit8 v3, v4, 0x1

    .end local v4    # "position":I
    .restart local v3    # "position":I
    iget-object v7, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mTabLists:Ljava/util/List;

    invoke-interface {v7, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/Integer;

    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v7

    aput v7, v6, v4

    goto :goto_1

    .line 325
    .end local v3    # "position":I
    .restart local v4    # "position":I
    :cond_2
    iget-object v7, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v7}, Landroid/widget/TabHost;->clearAllTabs()V

    .line 326
    iget-object v7, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mTabs:Ljava/util/HashMap;

    invoke-virtual {v7}, Ljava/util/HashMap;->clear()V

    .line 327
    iget-object v7, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mTabLists:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->clear()V

    .line 328
    iget-object v7, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mTabSelectedTexts:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->clear()V

    .line 330
    invoke-virtual {p0, v6, v2, v5}, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->setTab([II[Ljava/lang/String;)V

    .line 331
    return-void
.end method

.method public setCurrentTab(I)V
    .locals 4
    .param p1, "index"    # I

    .prologue
    .line 408
    iget-object v0, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v0, p1}, Landroid/widget/TabHost;->setCurrentTab(I)V

    .line 411
    iget-object v0, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mTabHost:Landroid/widget/TabHost;

    new-instance v1, Lcom/samsung/musicplus/widget/tab/MusicTabManager$2;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/widget/tab/MusicTabManager$2;-><init>(Lcom/samsung/musicplus/widget/tab/MusicTabManager;)V

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/TabHost;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 419
    return-void
.end method

.method public setCurrentTabText(ILjava/lang/String;)V
    .locals 4
    .param p1, "listType"    # I
    .param p2, "selectedText"    # Ljava/lang/String;

    .prologue
    .line 531
    iget-object v2, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mTabLists:Ljava/util/List;

    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->getCurrentTab()I

    move-result v3

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 533
    .local v0, "tabListType":I
    if-eqz p2, :cond_0

    if-ne v0, p1, :cond_0

    .line 534
    iget-object v2, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mTabHost:Landroid/widget/TabHost;

    invoke-virtual {v2}, Landroid/widget/TabHost;->getCurrentTabView()Landroid/view/View;

    move-result-object v2

    const v3, 0x7f0d01bc

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 536
    .local v1, "view":Landroid/widget/TextView;
    invoke-virtual {v1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 538
    .end local v1    # "view":Landroid/widget/TextView;
    :cond_0
    return-void
.end method

.method public setSelectedText(ILjava/lang/String;)V
    .locals 1
    .param p1, "index"    # I
    .param p2, "selectedText"    # Ljava/lang/String;

    .prologue
    .line 522
    iget-object v0, p0, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->mTabSelectedTexts:Ljava/util/List;

    invoke-interface {v0, p1, p2}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 523
    return-void
.end method

.method public setTab([II)V
    .locals 1
    .param p1, "tabs"    # [I
    .param p2, "index"    # I

    .prologue
    const/4 v0, 0x0

    .line 152
    invoke-virtual {p0, p1, p2, v0, v0}, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->setTab([IILandroid/os/Bundle;[Ljava/lang/String;)V

    .line 153
    return-void
.end method

.method public setTab([IILandroid/os/Bundle;[Ljava/lang/String;)V
    .locals 2
    .param p1, "tabs"    # [I
    .param p2, "index"    # I
    .param p3, "args"    # Landroid/os/Bundle;
    .param p4, "selectedTexts"    # [Ljava/lang/String;

    .prologue
    .line 160
    if-nez p4, :cond_0

    .line 161
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->addTabs([IILandroid/os/Bundle;[Ljava/lang/String;)V

    .line 169
    :goto_0
    return-void

    .line 163
    :cond_0
    array-length v0, p1

    array-length v1, p4

    if-eq v0, v1, :cond_1

    .line 164
    const-string v0, "MusicTabManager"

    const-string v1, "setTab() : Do not match the length of tabs and selectedTexts!!!"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 167
    :cond_1
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->addTabs([IILandroid/os/Bundle;[Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setTab([II[Ljava/lang/String;)V
    .locals 1
    .param p1, "tabs"    # [I
    .param p2, "index"    # I
    .param p3, "selectedTexts"    # [Ljava/lang/String;

    .prologue
    .line 156
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0, p3}, Lcom/samsung/musicplus/widget/tab/MusicTabManager;->setTab([IILandroid/os/Bundle;[Ljava/lang/String;)V

    .line 157
    return-void
.end method
