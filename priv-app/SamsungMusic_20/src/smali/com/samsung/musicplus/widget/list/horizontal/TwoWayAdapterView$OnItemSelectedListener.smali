.class public interface abstract Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView$OnItemSelectedListener;
.super Ljava/lang/Object;
.source "TwoWayAdapterView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnItemSelectedListener"
.end annotation


# virtual methods
.method public abstract onItemSelected(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;Landroid/view/View;IJ)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation
.end method

.method public abstract onNothingSelected(Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/samsung/musicplus/widget/list/horizontal/TwoWayAdapterView",
            "<*>;)V"
        }
    .end annotation
.end method
