.class Lcom/samsung/musicplus/widget/AnchorSizePopupMenu$SelectionMenuAdapter;
.super Ljava/lang/Object;
.source "AnchorSizePopupMenu.java"

# interfaces
.implements Landroid/widget/ListAdapter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/widget/AnchorSizePopupMenu;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SelectionMenuAdapter"
.end annotation


# instance fields
.field private mMenuItems:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mObserver:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/database/DataSetObserver;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/samsung/musicplus/widget/AnchorSizePopupMenu;


# direct methods
.method private constructor <init>(Lcom/samsung/musicplus/widget/AnchorSizePopupMenu;)V
    .locals 1

    .prologue
    .line 95
    iput-object p1, p0, Lcom/samsung/musicplus/widget/AnchorSizePopupMenu$SelectionMenuAdapter;->this$0:Lcom/samsung/musicplus/widget/AnchorSizePopupMenu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 97
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/widget/AnchorSizePopupMenu$SelectionMenuAdapter;->mObserver:Ljava/util/ArrayList;

    .line 99
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/widget/AnchorSizePopupMenu$SelectionMenuAdapter;->mMenuItems:Ljava/util/ArrayList;

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/musicplus/widget/AnchorSizePopupMenu;Lcom/samsung/musicplus/widget/AnchorSizePopupMenu$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/musicplus/widget/AnchorSizePopupMenu;
    .param p2, "x1"    # Lcom/samsung/musicplus/widget/AnchorSizePopupMenu$1;

    .prologue
    .line 95
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/widget/AnchorSizePopupMenu$SelectionMenuAdapter;-><init>(Lcom/samsung/musicplus/widget/AnchorSizePopupMenu;)V

    return-void
.end method


# virtual methods
.method addMenu(Ljava/lang/String;)V
    .locals 1
    .param p1, "menu"    # Ljava/lang/String;

    .prologue
    .line 102
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnchorSizePopupMenu$SelectionMenuAdapter;->mMenuItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 103
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/AnchorSizePopupMenu$SelectionMenuAdapter;->notifyDataSetChanged()V

    .line 104
    return-void
.end method

.method public areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 163
    const/4 v0, 0x1

    return v0
.end method

.method clearMenu()V
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnchorSizePopupMenu$SelectionMenuAdapter;->mMenuItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 113
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/AnchorSizePopupMenu$SelectionMenuAdapter;->notifyDataSetChanged()V

    .line 114
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnchorSizePopupMenu$SelectionMenuAdapter;->mMenuItems:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 123
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnchorSizePopupMenu$SelectionMenuAdapter;->mMenuItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 128
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnchorSizePopupMenu$SelectionMenuAdapter;->this$0:Lcom/samsung/musicplus/widget/AnchorSizePopupMenu;

    # getter for: Lcom/samsung/musicplus/widget/AnchorSizePopupMenu;->mMenuKeys:Landroid/util/SparseArray;
    invoke-static {v0}, Lcom/samsung/musicplus/widget/AnchorSizePopupMenu;->access$200(Lcom/samsung/musicplus/widget/AnchorSizePopupMenu;)Landroid/util/SparseArray;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v0

    int-to-long v0, v0

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 148
    const/4 v0, 0x0

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 138
    if-nez p2, :cond_0

    .line 139
    iget-object v1, p0, Lcom/samsung/musicplus/widget/AnchorSizePopupMenu$SelectionMenuAdapter;->this$0:Lcom/samsung/musicplus/widget/AnchorSizePopupMenu;

    # getter for: Lcom/samsung/musicplus/widget/AnchorSizePopupMenu;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/musicplus/widget/AnchorSizePopupMenu;->access$300(Lcom/samsung/musicplus/widget/AnchorSizePopupMenu;)Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f040083

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 141
    :cond_0
    const v1, 0x7f0d00d6

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 142
    .local v0, "tv":Landroid/widget/TextView;
    iget-object v1, p0, Lcom/samsung/musicplus/widget/AnchorSizePopupMenu$SelectionMenuAdapter;->mMenuItems:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 143
    return-object p2
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 153
    const/4 v0, 0x1

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 133
    const/4 v0, 0x0

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 158
    const/4 v0, 0x0

    return v0
.end method

.method public isEnabled(I)Z
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 168
    const/4 v0, 0x1

    return v0
.end method

.method public notifyDataSetChanged()V
    .locals 3

    .prologue
    .line 182
    iget-object v2, p0, Lcom/samsung/musicplus/widget/AnchorSizePopupMenu$SelectionMenuAdapter;->mObserver:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/database/DataSetObserver;

    .line 183
    .local v1, "ob":Landroid/database/DataSetObserver;
    invoke-virtual {v1}, Landroid/database/DataSetObserver;->onChanged()V

    goto :goto_0

    .line 185
    .end local v1    # "ob":Landroid/database/DataSetObserver;
    :cond_0
    return-void
.end method

.method public registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1
    .param p1, "observer"    # Landroid/database/DataSetObserver;

    .prologue
    .line 173
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnchorSizePopupMenu$SelectionMenuAdapter;->mObserver:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 174
    return-void
.end method

.method removeMenu(Ljava/lang/String;)V
    .locals 1
    .param p1, "menu"    # Ljava/lang/String;

    .prologue
    .line 107
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnchorSizePopupMenu$SelectionMenuAdapter;->mMenuItems:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 108
    invoke-virtual {p0}, Lcom/samsung/musicplus/widget/AnchorSizePopupMenu$SelectionMenuAdapter;->notifyDataSetChanged()V

    .line 109
    return-void
.end method

.method public unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1
    .param p1, "observer"    # Landroid/database/DataSetObserver;

    .prologue
    .line 178
    iget-object v0, p0, Lcom/samsung/musicplus/widget/AnchorSizePopupMenu$SelectionMenuAdapter;->mObserver:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 179
    return-void
.end method
