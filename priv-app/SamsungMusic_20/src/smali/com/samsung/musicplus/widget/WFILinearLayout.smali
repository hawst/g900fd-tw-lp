.class public Lcom/samsung/musicplus/widget/WFILinearLayout;
.super Landroid/widget/LinearLayout;
.source "WFILinearLayout.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/widget/WFILinearLayout$OnWindowFocusChangeListener;
    }
.end annotation


# instance fields
.field private mWindowFocusChangeListener:Lcom/samsung/musicplus/widget/WFILinearLayout$OnWindowFocusChangeListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 15
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 16
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 20
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 23
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 24
    return-void
.end method


# virtual methods
.method public onWindowFocusChanged(Z)V
    .locals 1
    .param p1, "hasWindowFocus"    # Z

    .prologue
    .line 32
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onWindowFocusChanged(Z)V

    .line 33
    iget-object v0, p0, Lcom/samsung/musicplus/widget/WFILinearLayout;->mWindowFocusChangeListener:Lcom/samsung/musicplus/widget/WFILinearLayout$OnWindowFocusChangeListener;

    if-eqz v0, :cond_0

    .line 34
    iget-object v0, p0, Lcom/samsung/musicplus/widget/WFILinearLayout;->mWindowFocusChangeListener:Lcom/samsung/musicplus/widget/WFILinearLayout$OnWindowFocusChangeListener;

    invoke-interface {v0, p1}, Lcom/samsung/musicplus/widget/WFILinearLayout$OnWindowFocusChangeListener;->onWindowFocusChanged(Z)V

    .line 36
    :cond_0
    return-void
.end method

.method public setOnWindowFocusChangeListener(Lcom/samsung/musicplus/widget/WFILinearLayout$OnWindowFocusChangeListener;)V
    .locals 0
    .param p1, "l"    # Lcom/samsung/musicplus/widget/WFILinearLayout$OnWindowFocusChangeListener;

    .prologue
    .line 27
    iput-object p1, p0, Lcom/samsung/musicplus/widget/WFILinearLayout;->mWindowFocusChangeListener:Lcom/samsung/musicplus/widget/WFILinearLayout$OnWindowFocusChangeListener;

    .line 28
    return-void
.end method
