.class Lcom/samsung/musicplus/widget/fragment/ListFragment$4;
.super Ljava/lang/Object;
.source "ListFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/widget/fragment/ListFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/widget/fragment/ListFragment;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/widget/fragment/ListFragment;)V
    .locals 0

    .prologue
    .line 524
    iput-object p1, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment$4;->this$0:Lcom/samsung/musicplus/widget/fragment/ListFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 527
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment$4;->this$0:Lcom/samsung/musicplus/widget/fragment/ListFragment;

    # getter for: Lcom/samsung/musicplus/widget/fragment/ListFragment;->mAdapter:Landroid/widget/ListAdapter;
    invoke-static {v0}, Lcom/samsung/musicplus/widget/fragment/ListFragment;->access$100(Lcom/samsung/musicplus/widget/fragment/ListFragment;)Landroid/widget/ListAdapter;

    move-result-object v0

    instance-of v0, v0, Lcom/samsung/musicplus/widget/list/TrackListAdapter;

    if-eqz v0, :cond_0

    .line 530
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment$4;->this$0:Lcom/samsung/musicplus/widget/fragment/ListFragment;

    # getter for: Lcom/samsung/musicplus/widget/fragment/ListFragment;->mAdapter:Landroid/widget/ListAdapter;
    invoke-static {v0}, Lcom/samsung/musicplus/widget/fragment/ListFragment;->access$100(Lcom/samsung/musicplus/widget/fragment/ListFragment;)Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/widget/list/TrackListAdapter;

    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getCurrentAudioId()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->setPlayingId(J)V

    .line 531
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment$4;->this$0:Lcom/samsung/musicplus/widget/fragment/ListFragment;

    # getter for: Lcom/samsung/musicplus/widget/fragment/ListFragment;->mListView:Landroid/widget/AbsListView;
    invoke-static {v0}, Lcom/samsung/musicplus/widget/fragment/ListFragment;->access$000(Lcom/samsung/musicplus/widget/fragment/ListFragment;)Landroid/widget/AbsListView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 532
    iget-object v0, p0, Lcom/samsung/musicplus/widget/fragment/ListFragment$4;->this$0:Lcom/samsung/musicplus/widget/fragment/ListFragment;

    # getter for: Lcom/samsung/musicplus/widget/fragment/ListFragment;->mListView:Landroid/widget/AbsListView;
    invoke-static {v0}, Lcom/samsung/musicplus/widget/fragment/ListFragment;->access$000(Lcom/samsung/musicplus/widget/fragment/ListFragment;)Landroid/widget/AbsListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/AbsListView;->invalidateViews()V

    .line 535
    :cond_0
    return-void
.end method
