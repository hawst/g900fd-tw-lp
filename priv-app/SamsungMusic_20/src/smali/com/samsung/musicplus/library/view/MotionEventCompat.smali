.class public Lcom/samsung/musicplus/library/view/MotionEventCompat;
.super Ljava/lang/Object;
.source "MotionEventCompat.java"


# static fields
.field public static final FLAG_TAINTED:I = -0x80000000


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final isTainted(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p0, "me"    # Landroid/view/MotionEvent;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 25
    const/4 v0, 0x1

    return v0
.end method

.method public static final setTainted(Landroid/view/MotionEvent;Z)V
    .locals 0
    .param p0, "me"    # Landroid/view/MotionEvent;
    .param p1, "tainted"    # Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 38
    return-void
.end method
