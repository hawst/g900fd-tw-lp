.class public Lcom/samsung/musicplus/library/audio/SecAudioManager;
.super Ljava/lang/Object;
.source "SecAudioManager.java"


# static fields
.field public static final ACTION_AUDIO_BECOMING_NOISY_SEC:Ljava/lang/String; = "android.media.AUDIO_BECOMING_NOISY_SEC"

.field public static final DEVICE_OUT_BT:I = 0x380

.field private static final DEVICE_OUT_BT_SCO:I = 0x20

.field public static final DEVICE_OUT_EAR_JACK:I = 0xc

.field public static final DEVICE_OUT_HDMI:I = 0x400

.field public static final DEVICE_OUT_LINE_OUT:I = 0x1800

.field public static final DEVICE_OUT_SPEAKER:I = 0x2

.field public static final DEVICE_OUT_UNKOWN:I = -0xa

.field public static final EXTRA_RINGER_MODE:Ljava/lang/String; = "android.media.EXTRA_RINGER_MODE"

.field public static final EXTRA_VOLUME_STREAM_TYPE:Ljava/lang/String; = "android.media.EXTRA_VOLUME_STREAM_TYPE"

.field public static final EXTRA_VOLUME_STREAM_VALUE:Ljava/lang/String; = "android.media.EXTRA_VOLUME_STREAM_VALUE"

.field private static final HEADSET_VOLUME:Ljava/lang/String; = ";device=2"

.field private static final IMPLICIT:Ljava/lang/String; = ";device=0"

.field public static final RINGER_MODE_CHANGED_ACTION:Ljava/lang/String; = "android.media.RINGER_MODE_CHANGED"

.field private static final SITUATION_MIDI:Ljava/lang/String; = "situation=6"

.field private static final SPEAKER_VOLUME:Ljava/lang/String; = ";device=1"

.field public static final VOLUME_CHANGED_ACTION:Ljava/lang/String; = "android.media.VOLUME_CHANGED_ACTION"

.field private static sSecAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;


# instance fields
.field private mAudioManager:Landroid/media/AudioManager;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 119
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 120
    const-string v0, "audio"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/samsung/musicplus/library/audio/SecAudioManager;->mAudioManager:Landroid/media/AudioManager;

    .line 121
    return-void
.end method

.method private getAudioPath()I
    .locals 6

    .prologue
    const/16 v0, -0xa

    .line 257
    iget-object v3, p0, Lcom/samsung/musicplus/library/audio/SecAudioManager;->mAudioManager:Landroid/media/AudioManager;

    const-string v4, "audioParam;outDevice"

    invoke-virtual {v3, v4}, Landroid/media/AudioManager;->getParameters(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 260
    .local v2, "path":Ljava/lang/String;
    if-eqz v2, :cond_0

    const-string v3, ""

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 261
    :cond_0
    const-string v3, "AudioManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "AudioManager.getParameters(\'audioParam;outDevice\') is wrong so return as Speaker. Path is : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 276
    :goto_0
    return v0

    .line 269
    :cond_1
    :try_start_0
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .local v0, "audio":I
    goto :goto_0

    .line 270
    .end local v0    # "audio":I
    :catch_0
    move-exception v1

    .line 271
    .local v1, "e":Ljava/lang/NumberFormatException;
    const-string v3, "AudioManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "AudioManager.getParameters(\'audioParam;outDevice\') is wrong so return as Speaker. Path is : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/samsung/musicplus/library/audio/SecAudioManager;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 133
    sget-object v0, Lcom/samsung/musicplus/library/audio/SecAudioManager;->sSecAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

    if-nez v0, :cond_0

    .line 134
    new-instance v0, Lcom/samsung/musicplus/library/audio/SecAudioManager;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/library/audio/SecAudioManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/musicplus/library/audio/SecAudioManager;->sSecAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

    .line 136
    :cond_0
    sget-object v0, Lcom/samsung/musicplus/library/audio/SecAudioManager;->sSecAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

    return-object v0
.end method

.method public static isAllSoundOff(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 495
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "all_sound_off"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    .line 498
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private isBt(I)Z
    .locals 1
    .param p1, "path"    # I

    .prologue
    .line 309
    and-int/lit16 v0, p1, 0x380

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private isBtSco(I)Z
    .locals 1
    .param p1, "path"    # I

    .prologue
    .line 313
    and-int/lit8 v0, p1, 0x20

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private isEarjack(I)Z
    .locals 1
    .param p1, "path"    # I

    .prologue
    .line 305
    and-int/lit8 v0, p1, 0xc

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private isHdmi(I)Z
    .locals 1
    .param p1, "path"    # I

    .prologue
    .line 321
    and-int/lit16 v0, p1, 0x400

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private isLineOut(I)Z
    .locals 1
    .param p1, "path"    # I

    .prologue
    .line 325
    and-int/lit16 v0, p1, 0x1800

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private isSpeaker(I)Z
    .locals 1
    .param p1, "path"    # I

    .prologue
    .line 317
    and-int/lit8 v0, p1, 0x2

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private selectRouteInt(ILandroid/content/Context;)V
    .locals 4
    .param p1, "type"    # I
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x1

    .line 669
    const-string v2, "media_router"

    invoke-virtual {p2, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/MediaRouter;

    .line 671
    .local v0, "mediaRouter":Landroid/media/MediaRouter;
    if-nez v0, :cond_0

    .line 672
    const-string v2, "AudioManager"

    const-string v3, "selectRouteInt : mediaRouter is NULL!"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 688
    :goto_0
    return-void

    .line 676
    :cond_0
    const/4 v1, 0x0

    .line 677
    .local v1, "routeInfo":Landroid/media/MediaRouter$RouteInfo;
    if-nez p1, :cond_2

    .line 678
    invoke-virtual {v0}, Landroid/media/MediaRouter;->getDefaultRoute()Landroid/media/MediaRouter$RouteInfo;

    move-result-object v1

    .line 683
    :cond_1
    :goto_1
    if-eqz v1, :cond_3

    .line 684
    invoke-virtual {v0, v3, v1}, Landroid/media/MediaRouter;->selectRoute(ILandroid/media/MediaRouter$RouteInfo;)V

    goto :goto_0

    .line 679
    :cond_2
    if-ne p1, v3, :cond_1

    .line 680
    invoke-virtual {v0}, Landroid/media/MediaRouter;->getA2dpRoute()Landroid/media/MediaRouter$RouteInfo;

    move-result-object v1

    goto :goto_1

    .line 686
    :cond_3
    const-string v2, "AudioManager"

    const-string v3, "selectRouteInt : routeInfo is NULL!"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I
    .locals 1
    .param p1, "l"    # Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .prologue
    .line 160
    iget-object v0, p0, Lcom/samsung/musicplus/library/audio/SecAudioManager;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, p1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    move-result v0

    return v0
.end method

.method public adjustStreamVolume(III)V
    .locals 1
    .param p1, "streamType"    # I
    .param p2, "direction"    # I
    .param p3, "flags"    # I

    .prologue
    .line 578
    iget-object v0, p0, Lcom/samsung/musicplus/library/audio/SecAudioManager;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, p1, p2, p3}, Landroid/media/AudioManager;->adjustStreamVolume(III)V

    .line 579
    return-void
.end method

.method public dismissVolumePanel()V
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Lcom/samsung/musicplus/library/audio/SecAudioManager;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->dismissVolumePanel()V

    .line 211
    return-void
.end method

.method public getCurrentAudioPath()I
    .locals 3

    .prologue
    const/4 v1, 0x2

    .line 288
    invoke-direct {p0}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->getAudioPath()I

    move-result v0

    .line 289
    .local v0, "path":I
    invoke-direct {p0, v0}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->isSpeaker(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 301
    :cond_0
    :goto_0
    return v1

    .line 291
    :cond_1
    invoke-direct {p0, v0}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->isBt(I)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->isBtSco(I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 292
    :cond_2
    const/16 v1, 0x380

    goto :goto_0

    .line 293
    :cond_3
    invoke-direct {p0, v0}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->isLineOut(I)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 294
    const/16 v1, 0x1800

    goto :goto_0

    .line 295
    :cond_4
    invoke-direct {p0, v0}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->isHdmi(I)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 296
    const/16 v1, 0x400

    goto :goto_0

    .line 297
    :cond_5
    invoke-direct {p0, v0}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->isEarjack(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 298
    const/16 v1, 0xc

    goto :goto_0
.end method

.method public getEarProtectLimitIndex()I
    .locals 1

    .prologue
    .line 621
    iget-object v0, p0, Lcom/samsung/musicplus/library/audio/SecAudioManager;->mAudioManager:Landroid/media/AudioManager;

    invoke-static {}, Landroid/media/AudioManager;->getEarProtectLimitIndex()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    return v0
.end method

.method public getLastAudibleStreamVolume(I)I
    .locals 1
    .param p1, "streamType"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 542
    const/4 v0, -0x1

    return v0
.end method

.method public getMidiHeadsetVolume()F
    .locals 2

    .prologue
    .line 252
    iget-object v0, p0, Lcom/samsung/musicplus/library/audio/SecAudioManager;->mAudioManager:Landroid/media/AudioManager;

    const-string v1, "situation=6;device=2"

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getParameters(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    return v0
.end method

.method public getMidiSpeakerVolume()F
    .locals 2

    .prologue
    .line 238
    iget-object v0, p0, Lcom/samsung/musicplus/library/audio/SecAudioManager;->mAudioManager:Landroid/media/AudioManager;

    const-string v1, "situation=6;device=1"

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getParameters(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    return v0
.end method

.method public getMidiVolume()F
    .locals 2

    .prologue
    .line 224
    iget-object v0, p0, Lcom/samsung/musicplus/library/audio/SecAudioManager;->mAudioManager:Landroid/media/AudioManager;

    const-string v1, "situation=6;device=0"

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->getParameters(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    return v0
.end method

.method public getStreamMaxVolume(I)I
    .locals 1
    .param p1, "streamType"    # I

    .prologue
    .line 556
    iget-object v0, p0, Lcom/samsung/musicplus/library/audio/SecAudioManager;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, p1}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v0

    return v0
.end method

.method public getStreamVolume(I)I
    .locals 1
    .param p1, "streamType"    # I

    .prologue
    .line 610
    iget-object v0, p0, Lcom/samsung/musicplus/library/audio/SecAudioManager;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, p1}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    return v0
.end method

.method public isAudioPathBT()Z
    .locals 2

    .prologue
    .line 405
    invoke-direct {p0}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->getAudioPath()I

    move-result v0

    .line 406
    .local v0, "path":I
    invoke-direct {p0, v0}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->isBt(I)Z

    move-result v1

    return v1
.end method

.method public isAudioPathBtModeSco()Z
    .locals 2

    .prologue
    .line 422
    invoke-direct {p0}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->getAudioPath()I

    move-result v0

    .line 423
    .local v0, "path":I
    invoke-direct {p0, v0}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->isBtSco(I)Z

    move-result v1

    return v1
.end method

.method public isAudioPathEarjack()Z
    .locals 2

    .prologue
    .line 392
    invoke-direct {p0}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->getAudioPath()I

    move-result v0

    .line 393
    .local v0, "path":I
    invoke-direct {p0, v0}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->isEarjack(I)Z

    move-result v1

    return v1
.end method

.method public isAudioPathLineOut()Z
    .locals 2

    .prologue
    .line 448
    invoke-direct {p0}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->getAudioPath()I

    move-result v0

    .line 449
    .local v0, "path":I
    invoke-direct {p0, v0}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->isLineOut(I)Z

    move-result v1

    return v1
.end method

.method public isAudioPathSpeaker()Z
    .locals 2

    .prologue
    .line 435
    invoke-direct {p0}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->getAudioPath()I

    move-result v0

    .line 436
    .local v0, "path":I
    invoke-direct {p0, v0}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->isSpeaker(I)Z

    move-result v1

    return v1
.end method

.method public isExtraSpeakerDockOn()Z
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/samsung/musicplus/library/audio/SecAudioManager;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isExtraSpeakerDockOn()Z

    move-result v0

    return v0
.end method

.method public isFMActive()Z
    .locals 1

    .prologue
    .line 180
    iget-object v0, p0, Lcom/samsung/musicplus/library/audio/SecAudioManager;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isFMActive()Z

    move-result v0

    return v0
.end method

.method public isHDMIConnect()Z
    .locals 2

    .prologue
    .line 459
    invoke-direct {p0}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->getAudioPath()I

    move-result v0

    .line 460
    .local v0, "path":I
    invoke-direct {p0, v0}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->isHdmi(I)Z

    move-result v1

    return v1
.end method

.method public isMediaSilentMode()Z
    .locals 1

    .prologue
    .line 482
    invoke-static {}, Landroid/media/AudioManager;->isMediaSilentMode()Z

    move-result v0

    return v0
.end method

.method public isRecordActive()Z
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/samsung/musicplus/library/audio/SecAudioManager;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isRecordActive()Z

    move-result v0

    return v0
.end method

.method public isStreamMute(I)Z
    .locals 1
    .param p1, "streamType"    # I

    .prologue
    .line 512
    iget-object v0, p0, Lcom/samsung/musicplus/library/audio/SecAudioManager;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, p1}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isWiredHeadsetOn()Z
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lcom/samsung/musicplus/library/audio/SecAudioManager;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->isWiredHeadsetOn()Z

    move-result v0

    return v0
.end method

.method public registerMediaButtonEventReceiver(Landroid/content/ComponentName;)V
    .locals 1
    .param p1, "cp"    # Landroid/content/ComponentName;

    .prologue
    .line 148
    iget-object v0, p0, Lcom/samsung/musicplus/library/audio/SecAudioManager;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, p1}, Landroid/media/AudioManager;->registerMediaButtonEventReceiver(Landroid/content/ComponentName;)V

    .line 149
    return-void
.end method

.method public registerRemoteControlClient(Landroid/media/RemoteControlClient;)V
    .locals 1
    .param p1, "rcClient"    # Landroid/media/RemoteControlClient;

    .prologue
    .line 140
    iget-object v0, p0, Lcom/samsung/musicplus/library/audio/SecAudioManager;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, p1}, Landroid/media/AudioManager;->registerRemoteControlClient(Landroid/media/RemoteControlClient;)V

    .line 141
    return-void
.end method

.method public requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I
    .locals 1
    .param p1, "l"    # Landroid/media/AudioManager$OnAudioFocusChangeListener;
    .param p2, "streamType"    # I
    .param p3, "durationHint"    # I

    .prologue
    .line 156
    iget-object v0, p0, Lcom/samsung/musicplus/library/audio/SecAudioManager;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, p1, p2, p3}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    move-result v0

    return v0
.end method

.method public setMediaSilentMode(Z)V
    .locals 0
    .param p1, "isMediaSilentMode"    # Z

    .prologue
    .line 470
    invoke-static {p1}, Landroid/media/AudioManager;->setMediaSilentMode(Z)V

    .line 471
    return-void
.end method

.method public setRingerMode(I)V
    .locals 1
    .param p1, "ringerModeNormal"    # I

    .prologue
    .line 527
    iget-object v0, p0, Lcom/samsung/musicplus/library/audio/SecAudioManager;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, p1}, Landroid/media/AudioManager;->setRingerMode(I)V

    .line 528
    return-void
.end method

.method public setSmartVoumeEnable(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 634
    invoke-static {p1}, Landroid/media/AudioManager;->setSmartVoumeEnable(Z)V

    .line 635
    return-void
.end method

.method public setSoundPathToBT(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 646
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->selectRouteInt(ILandroid/content/Context;)V

    .line 647
    return-void
.end method

.method public setSoundPathToDevice(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 658
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->selectRouteInt(ILandroid/content/Context;)V

    .line 659
    return-void
.end method

.method public setStreamVolume(III)V
    .locals 1
    .param p1, "streamType"    # I
    .param p2, "index"    # I
    .param p3, "flags"    # I

    .prologue
    .line 595
    iget-object v0, p0, Lcom/samsung/musicplus/library/audio/SecAudioManager;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, p1, p2, p3}, Landroid/media/AudioManager;->setStreamVolume(III)V

    .line 596
    return-void
.end method

.method public unregisterMediaButtonEventReceiver(Landroid/content/ComponentName;)V
    .locals 1
    .param p1, "cp"    # Landroid/content/ComponentName;

    .prologue
    .line 152
    iget-object v0, p0, Lcom/samsung/musicplus/library/audio/SecAudioManager;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, p1}, Landroid/media/AudioManager;->unregisterMediaButtonEventReceiver(Landroid/content/ComponentName;)V

    .line 153
    return-void
.end method

.method public unregisterRemoteControlClient(Landroid/media/RemoteControlClient;)V
    .locals 1
    .param p1, "rcClient"    # Landroid/media/RemoteControlClient;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/samsung/musicplus/library/audio/SecAudioManager;->mAudioManager:Landroid/media/AudioManager;

    invoke-virtual {v0, p1}, Landroid/media/AudioManager;->unregisterRemoteControlClient(Landroid/media/RemoteControlClient;)V

    .line 145
    return-void
.end method
