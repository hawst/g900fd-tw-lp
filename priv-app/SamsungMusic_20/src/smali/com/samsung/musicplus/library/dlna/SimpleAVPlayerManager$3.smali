.class Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$3;
.super Ljava/lang/Object;
.source "SimpleAVPlayerManager.java"

# interfaces
.implements Lcom/samsung/android/allshare/extension/SECAVPlayer$ISECAVPlayerStateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;)V
    .locals 0

    .prologue
    .line 841
    iput-object p1, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$3;->this$0:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onBuffering()V
    .locals 2

    .prologue
    .line 903
    # getter for: Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "mSECAVPlayerStateListener - onBuffering()"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 904
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$3;->this$0:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    const/4 v1, 0x4

    # setter for: Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mAVPlayerState:I
    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->access$302(Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;I)I

    .line 905
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$3;->this$0:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    iget-object v1, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$3;->this$0:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    # getter for: Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mAVPlayerState:I
    invoke-static {v1}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->access$300(Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;)I

    move-result v1

    # invokes: Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->onStateChanged(I)V
    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->access$400(Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;I)V

    .line 906
    return-void
.end method

.method public onError(Lcom/samsung/android/allshare/ERROR;)V
    .locals 3
    .param p1, "err"    # Lcom/samsung/android/allshare/ERROR;

    .prologue
    .line 911
    const-string v0, "MusicPlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mSECAVPlayerStateListener - Error(): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/samsung/android/allshare/ERROR;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 919
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$3;->this$0:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    const/4 v1, 0x3

    # setter for: Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mAVPlayerState:I
    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->access$302(Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;I)I

    .line 920
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$3;->this$0:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    iget-object v1, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$3;->this$0:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    # getter for: Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mAVPlayerState:I
    invoke-static {v1}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->access$300(Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;)I

    move-result v1

    # invokes: Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->onStateChanged(I)V
    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->access$400(Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;I)V

    .line 922
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$3;->this$0:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    # invokes: Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->onError(Lcom/samsung/android/allshare/ERROR;)V
    invoke-static {v0, p1}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->access$1100(Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;Lcom/samsung/android/allshare/ERROR;)V

    .line 923
    return-void
.end method

.method public onFinish()V
    .locals 2

    .prologue
    .line 888
    const-string v0, "MusicPlayer"

    const-string v1, "mSECAVPlayerStateListener - onFinish()"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 894
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$3;->this$0:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    const/4 v1, 0x3

    # setter for: Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mAVPlayerState:I
    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->access$302(Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;I)I

    .line 895
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$3;->this$0:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    iget-object v1, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$3;->this$0:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    # getter for: Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mAVPlayerState:I
    invoke-static {v1}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->access$300(Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;)I

    move-result v1

    # invokes: Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->onStateChanged(I)V
    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->access$400(Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;I)V

    .line 896
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$3;->this$0:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    # getter for: Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mOnCompletionListener:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$OnCompletionListener;
    invoke-static {v0}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->access$1000(Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;)Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$OnCompletionListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 897
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$3;->this$0:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    # getter for: Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mOnCompletionListener:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$OnCompletionListener;
    invoke-static {v0}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->access$1000(Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;)Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$OnCompletionListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$OnCompletionListener;->onCompletion()V

    .line 899
    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 880
    # getter for: Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "mSECAVPlayerStateListener - onPause()"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 881
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$3;->this$0:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    const/4 v1, 0x2

    # setter for: Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mAVPlayerState:I
    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->access$302(Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;I)I

    .line 882
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$3;->this$0:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    iget-object v1, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$3;->this$0:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    # getter for: Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mAVPlayerState:I
    invoke-static {v1}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->access$300(Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;)I

    move-result v1

    # invokes: Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->onStateChanged(I)V
    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->access$400(Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;I)V

    .line 883
    return-void
.end method

.method public onPlay()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 865
    # getter for: Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "mSECAVPlayerStateListener - onPlay()"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 866
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$3;->this$0:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    # getter for: Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mAVPlayerState:I
    invoke-static {v0}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->access$300(Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;)I

    move-result v0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    .line 867
    # getter for: Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "mSECAVPlayerStateListener - onPlay() is called after request. return."

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 876
    :goto_0
    return-void

    .line 874
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$3;->this$0:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    # setter for: Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mAVPlayerState:I
    invoke-static {v0, v2}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->access$302(Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;I)I

    .line 875
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$3;->this$0:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    # invokes: Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->onStateChanged(I)V
    invoke-static {v0, v2}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->access$400(Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;I)V

    goto :goto_0
.end method

.method public onProgress(J)V
    .locals 9
    .param p1, "progress"    # J

    .prologue
    const-wide/16 v6, 0x3e8

    const/4 v4, 0x1

    .line 853
    # getter for: Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->access$100()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mSECAVPlayerStateListener - onProgress() - progress: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    mul-long v2, p1, v6

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " msec"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " isPlaying: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$3;->this$0:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    # getter for: Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mAVPlayerState:I
    invoke-static {v0}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->access$300(Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;)I

    move-result v0

    if-ne v0, v4, :cond_1

    const-string v0, "true"

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " mDoNotUpdateProgress: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$3;->this$0:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    # getter for: Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mDoNotUpdateProgress:Z
    invoke-static {v2}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->access$200(Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;)Z

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 857
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$3;->this$0:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    # getter for: Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mAVPlayerState:I
    invoke-static {v0}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->access$300(Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;)I

    move-result v0

    if-ne v0, v4, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$3;->this$0:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    # getter for: Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mDoNotUpdateProgress:Z
    invoke-static {v0}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->access$200(Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 859
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$3;->this$0:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    mul-long v2, p1, v6

    # setter for: Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mAVPlayerCurrentPosition:J
    invoke-static {v0, v2, v3}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->access$902(Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;J)J

    .line 861
    :cond_0
    return-void

    .line 853
    :cond_1
    const-string v0, "false"

    goto :goto_0
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 846
    # getter for: Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->access$100()Ljava/lang/String;

    move-result-object v0

    const-string v1, "mSECAVPlayerStateListener - onStop()"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 847
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$3;->this$0:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    const/4 v1, 0x3

    # setter for: Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mAVPlayerState:I
    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->access$302(Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;I)I

    .line 848
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$3;->this$0:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    iget-object v1, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$3;->this$0:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    # getter for: Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mAVPlayerState:I
    invoke-static {v1}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->access$300(Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;)I

    move-result v1

    # invokes: Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->onStateChanged(I)V
    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->access$400(Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;I)V

    .line 849
    return-void
.end method
