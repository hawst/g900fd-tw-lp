.class public Lcom/samsung/musicplus/library/hover/SecHoverWindow;
.super Ljava/lang/Object;
.source "SecHoverWindow.java"


# static fields
.field public static HOVER_TYPE_NONE:I

.field public static HOVER_TYPE_TOOLTIP:I

.field public static HOVER_TYPE_USER_CUSTOM:I

.field public static HOVER_TYPE_WIDGET_DEFAULT:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const/4 v0, 0x0

    sput v0, Lcom/samsung/musicplus/library/hover/SecHoverWindow;->HOVER_TYPE_NONE:I

    .line 31
    const/4 v0, 0x1

    sput v0, Lcom/samsung/musicplus/library/hover/SecHoverWindow;->HOVER_TYPE_TOOLTIP:I

    .line 39
    const/4 v0, 0x3

    sput v0, Lcom/samsung/musicplus/library/hover/SecHoverWindow;->HOVER_TYPE_USER_CUSTOM:I

    .line 47
    const/4 v0, 0x2

    sput v0, Lcom/samsung/musicplus/library/hover/SecHoverWindow;->HOVER_TYPE_WIDGET_DEFAULT:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getHoverPopupWindow(Landroid/view/View;)Lcom/samsung/musicplus/library/hover/HoverPopupWindowCompat;
    .locals 1
    .param p0, "v"    # Landroid/view/View;

    .prologue
    .line 80
    if-eqz p0, :cond_0

    .line 81
    new-instance v0, Lcom/samsung/musicplus/library/hover/HoverPopupWindowCompat;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/library/hover/HoverPopupWindowCompat;-><init>(Landroid/view/View;)V

    .line 83
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static setHoverType(Landroid/view/View;I)V
    .locals 0
    .param p0, "v"    # Landroid/view/View;
    .param p1, "type"    # I

    .prologue
    .line 70
    invoke-virtual {p0, p1}, Landroid/view/View;->setHoverPopupType(I)V

    .line 71
    return-void
.end method

.method public static supportHoveringUI(Landroid/content/Context;)Z
    .locals 2
    .param p0, "c"    # Landroid/content/Context;

    .prologue
    .line 56
    if-nez p0, :cond_0

    .line 57
    const/4 v0, 0x0

    .line 59
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "com.sec.feature.hovering_ui"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method
