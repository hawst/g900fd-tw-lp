.class final Lcom/samsung/musicplus/library/hardware/AirView$1;
.super Ljava/lang/Object;
.source "AirView.java"

# interfaces
.implements Landroid/widget/HoverPopupWindow$HoverPopupListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/library/hardware/AirView;->setView(Landroid/view/View;Lcom/samsung/musicplus/library/hardware/AirView$OnAirViewPopupListener;ZZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$listener:Lcom/samsung/musicplus/library/hardware/AirView$OnAirViewPopupListener;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/library/hardware/AirView$OnAirViewPopupListener;)V
    .locals 0

    .prologue
    .line 355
    iput-object p1, p0, Lcom/samsung/musicplus/library/hardware/AirView$1;->val$listener:Lcom/samsung/musicplus/library/hardware/AirView$OnAirViewPopupListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSetContentView(Landroid/view/View;Landroid/widget/HoverPopupWindow;)Z
    .locals 3
    .param p1, "parentView"    # Landroid/view/View;
    .param p2, "hpw"    # Landroid/widget/HoverPopupWindow;

    .prologue
    .line 359
    const/4 v1, 0x0

    .line 360
    .local v1, "v":Landroid/view/View;
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 361
    .local v0, "context":Landroid/content/Context;
    invoke-static {v0}, Lcom/samsung/musicplus/library/hardware/AirView;->isEnableAirViewInforPreview(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {v0}, Lcom/samsung/musicplus/library/hardware/AirView;->isEnablePenHoveringInforPreview(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 363
    :cond_0
    iget-object v2, p0, Lcom/samsung/musicplus/library/hardware/AirView$1;->val$listener:Lcom/samsung/musicplus/library/hardware/AirView$OnAirViewPopupListener;

    if-eqz v2, :cond_1

    .line 364
    iget-object v2, p0, Lcom/samsung/musicplus/library/hardware/AirView$1;->val$listener:Lcom/samsung/musicplus/library/hardware/AirView$OnAirViewPopupListener;

    invoke-interface {v2, p1}, Lcom/samsung/musicplus/library/hardware/AirView$OnAirViewPopupListener;->getAirView(Landroid/view/View;)Landroid/view/View;

    move-result-object v1

    .line 366
    :cond_1
    if-eqz v1, :cond_2

    .line 367
    invoke-virtual {p2, v1}, Landroid/widget/HoverPopupWindow;->setContent(Landroid/view/View;)V

    .line 370
    :cond_2
    if-eqz v1, :cond_3

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_3
    const/4 v2, 0x0

    goto :goto_0
.end method
