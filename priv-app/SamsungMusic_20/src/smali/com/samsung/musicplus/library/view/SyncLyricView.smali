.class public Lcom/samsung/musicplus/library/view/SyncLyricView;
.super Landroid/widget/FrameLayout;
.source "SyncLyricView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/library/view/SyncLyricView$OnSyncLyricStatusListener;,
        Lcom/samsung/musicplus/library/view/SyncLyricView$OnMediaTimeListener;,
        Lcom/samsung/musicplus/library/view/SyncLyricView$OnResultListener;
    }
.end annotation


# static fields
.field public static final ACTION_SYNC_LYRIC_STATE:Ljava/lang/String; = "mediacanvas_getldplyrics_result"

.field public static final ALIGN_CENTER:I = 0x1

.field public static final ALIGN_LEFT:I = 0x0

.field public static final ALIGN_RIGHT:I = 0x2

.field private static final BACKGROUND_TRANSPARENT:I = -0x1

.field private static final CLASSNAME:Ljava/lang/String;

.field private static final DEFAULT_FONT_SIZE:I = 0x2d

.field private static final DEFAULT_NUMBER_OF_LINE:I = 0xe

.field public static final EXTRA_LYRIC_RESULTS:Ljava/lang/String; = "getldp_Lyrics_result"

.field public static final LDB_LYRIC_INSIDE_FILE:I = 0x3

.field public static final LDP_LYRIC_EXTRA_FILE:I = 0x1

.field public static final LDP_LYRIC_INSIDE_FILE:I = 0x2

.field public static final LYRIC_LOADING_FAIL:I = 0x0

.field public static final LYRIC_LOADING_SUCCESS:I = 0x1

.field public static final LYRIC_STATE_AUTO_FAIL:I = 0x1

.field public static final LYRIC_STATE_AUTO_START:I = 0x0

.field public static final LYRIC_STATE_AUTO_SUCCESS:I = 0x2

.field public static final LYRIC_STATE_METADATA_SUCCESS:I = 0x3

.field private static final NETWORK_DATA:I = 0x1

.field private static final NETWORK_NO:I = 0x3

.field private static final NETWORK_WIFI:I = 0x0

.field private static final NETWORK_WIFI_AND_DATA:I = 0x2

.field public static final NO_LYRIC:I


# instance fields
.field private mActivity:Landroid/app/Activity;

.field private mAlbum:Ljava/lang/String;

.field private mAlign:I

.field private mArtist:Ljava/lang/String;

.field private mBackground:I

.field private mClickListener:Landroid/view/View$OnClickListener;

.field private mColorOfLyric:I

.field private mColorOfLyricOutline:I

.field private mColorOfLyricSynced:I

.field private mDuration:J

.field private mFont:Landroid/graphics/Typeface;

.field private mFontSize:I

.field private mHasSetStyle:Z

.field private mHeight:I

.field private mIsNeedToSetStyle:Z

.field private mIsPause:Z

.field private mMediaTimeListener:Lcom/samsung/musicplus/library/view/SyncLyricView$OnMediaTimeListener;

.field private mNumOfLine:I

.field private mPath:Ljava/lang/String;

.field private mResultListener:Lcom/samsung/musicplus/library/view/SyncLyricView$OnResultListener;

.field private mStatusListener:Lcom/samsung/musicplus/library/view/SyncLyricView$OnSyncLyricStatusListener;

.field private final mSyncLyricReceiver:Landroid/content/BroadcastReceiver;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private mTitle:Ljava/lang/String;

.field private mWidth:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    const-class v0, Lcom/samsung/musicplus/library/view/SyncLyricView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/library/view/SyncLyricView;->CLASSNAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x0

    .line 329
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 268
    iput-boolean v1, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mHasSetStyle:Z

    .line 270
    iput-boolean v1, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mIsNeedToSetStyle:Z

    .line 285
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mBackground:I

    .line 290
    const/16 v0, 0xe

    iput v0, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mNumOfLine:I

    .line 295
    const v0, -0x2d2d2e

    iput v0, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mColorOfLyric:I

    .line 300
    const v0, -0xd05937

    iput v0, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mColorOfLyricSynced:I

    .line 305
    const v0, 0xffffff

    iput v0, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mColorOfLyricOutline:I

    .line 310
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    iput-object v0, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mFont:Landroid/graphics/Typeface;

    .line 315
    const/16 v0, 0x2d

    iput v0, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mFontSize:I

    .line 321
    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mAlign:I

    .line 677
    iput-boolean v1, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mIsPause:Z

    .line 853
    new-instance v0, Lcom/samsung/musicplus/library/view/SyncLyricView$1;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/library/view/SyncLyricView$1;-><init>(Lcom/samsung/musicplus/library/view/SyncLyricView;)V

    iput-object v0, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mSyncLyricReceiver:Landroid/content/BroadcastReceiver;

    .line 330
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/library/view/SyncLyricView;->initView(Landroid/content/Context;)V

    .line 331
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v1, 0x0

    .line 339
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 268
    iput-boolean v1, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mHasSetStyle:Z

    .line 270
    iput-boolean v1, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mIsNeedToSetStyle:Z

    .line 285
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mBackground:I

    .line 290
    const/16 v0, 0xe

    iput v0, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mNumOfLine:I

    .line 295
    const v0, -0x2d2d2e

    iput v0, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mColorOfLyric:I

    .line 300
    const v0, -0xd05937

    iput v0, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mColorOfLyricSynced:I

    .line 305
    const v0, 0xffffff

    iput v0, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mColorOfLyricOutline:I

    .line 310
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    iput-object v0, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mFont:Landroid/graphics/Typeface;

    .line 315
    const/16 v0, 0x2d

    iput v0, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mFontSize:I

    .line 321
    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mAlign:I

    .line 677
    iput-boolean v1, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mIsPause:Z

    .line 853
    new-instance v0, Lcom/samsung/musicplus/library/view/SyncLyricView$1;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/library/view/SyncLyricView$1;-><init>(Lcom/samsung/musicplus/library/view/SyncLyricView;)V

    iput-object v0, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mSyncLyricReceiver:Landroid/content/BroadcastReceiver;

    .line 340
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/library/view/SyncLyricView;->initView(Landroid/content/Context;)V

    .line 341
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defType"    # I

    .prologue
    const/4 v1, 0x0

    .line 349
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 268
    iput-boolean v1, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mHasSetStyle:Z

    .line 270
    iput-boolean v1, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mIsNeedToSetStyle:Z

    .line 285
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mBackground:I

    .line 290
    const/16 v0, 0xe

    iput v0, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mNumOfLine:I

    .line 295
    const v0, -0x2d2d2e

    iput v0, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mColorOfLyric:I

    .line 300
    const v0, -0xd05937

    iput v0, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mColorOfLyricSynced:I

    .line 305
    const v0, 0xffffff

    iput v0, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mColorOfLyricOutline:I

    .line 310
    sget-object v0, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    iput-object v0, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mFont:Landroid/graphics/Typeface;

    .line 315
    const/16 v0, 0x2d

    iput v0, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mFontSize:I

    .line 321
    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mAlign:I

    .line 677
    iput-boolean v1, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mIsPause:Z

    .line 853
    new-instance v0, Lcom/samsung/musicplus/library/view/SyncLyricView$1;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/library/view/SyncLyricView$1;-><init>(Lcom/samsung/musicplus/library/view/SyncLyricView;)V

    iput-object v0, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mSyncLyricReceiver:Landroid/content/BroadcastReceiver;

    .line 350
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/library/view/SyncLyricView;->initView(Landroid/content/Context;)V

    .line 351
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lcom/samsung/musicplus/library/view/SyncLyricView;->CLASSNAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/musicplus/library/view/SyncLyricView;)Lcom/samsung/musicplus/library/view/SyncLyricView$OnSyncLyricStatusListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/library/view/SyncLyricView;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mStatusListener:Lcom/samsung/musicplus/library/view/SyncLyricView$OnSyncLyricStatusListener;

    return-object v0
.end method

.method private getLyricInternal(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 3
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "artist"    # Ljava/lang/String;
    .param p4, "album"    # Ljava/lang/String;
    .param p5, "duration"    # J

    .prologue
    .line 632
    sget-object v0, Lcom/samsung/musicplus/library/view/SyncLyricView;->CLASSNAME:Ljava/lang/String;

    const-string v1, "getLyric() >"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 633
    iget v0, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mWidth:I

    if-gtz v0, :cond_0

    .line 636
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mIsNeedToSetStyle:Z

    .line 657
    :goto_0
    return-void

    .line 640
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mIsPause:Z

    .line 641
    sget-object v0, Lcom/samsung/musicplus/library/view/SyncLyricView;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getLyric path : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " title : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " artist : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " album : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " duration : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5, p6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 651
    iget-boolean v0, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mHasSetStyle:Z

    if-nez v0, :cond_1

    .line 654
    invoke-direct {p0}, Lcom/samsung/musicplus/library/view/SyncLyricView;->setStyle()V

    .line 656
    :cond_1
    sget-object v0, Lcom/samsung/musicplus/library/view/SyncLyricView;->CLASSNAME:Ljava/lang/String;

    const-string v1, "getLyric() <"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private initView(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 354
    sget-object v0, Lcom/samsung/musicplus/library/view/SyncLyricView;->CLASSNAME:Ljava/lang/String;

    const-string v1, "initView() >"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 358
    sget-object v0, Lcom/samsung/musicplus/library/view/SyncLyricView;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "initView() < child : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/musicplus/library/view/SyncLyricView;->getChildCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 359
    return-void
.end method

.method private resume()V
    .locals 2

    .prologue
    .line 696
    sget-object v0, Lcom/samsung/musicplus/library/view/SyncLyricView;->CLASSNAME:Ljava/lang/String;

    const-string v1, "resume()"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 698
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mIsPause:Z

    .line 699
    return-void
.end method

.method private setStyle()V
    .locals 3

    .prologue
    .line 484
    sget-object v0, Lcom/samsung/musicplus/library/view/SyncLyricView;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setStyle() > mWidth : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mWidth:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mHeight : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mHeight:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 491
    sget-object v0, Lcom/samsung/musicplus/library/view/SyncLyricView;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setStyle() < mWidth : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mWidth:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mHeight : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mHeight:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " child : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/musicplus/library/view/SyncLyricView;->getChildCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 493
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mHasSetStyle:Z

    .line 494
    return-void
.end method


# virtual methods
.method public cancel()V
    .locals 2

    .prologue
    .line 670
    sget-object v0, Lcom/samsung/musicplus/library/view/SyncLyricView;->CLASSNAME:Ljava/lang/String;

    const-string v1, "cancel >"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 674
    sget-object v0, Lcom/samsung/musicplus/library/view/SyncLyricView;->CLASSNAME:Ljava/lang/String;

    const-string v1, "cancel <"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 675
    return-void
.end method

.method public getAllLineLyric()Ljava/lang/String;
    .locals 2

    .prologue
    .line 751
    sget-object v0, Lcom/samsung/musicplus/library/view/SyncLyricView;->CLASSNAME:Ljava/lang/String;

    const-string v1, "getAllLineLyric()"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 752
    const/4 v0, 0x0

    return-object v0
.end method

.method public getLyric(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 3
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "artist"    # Ljava/lang/String;
    .param p4, "album"    # Ljava/lang/String;
    .param p5, "duration"    # J

    .prologue
    .line 617
    sget-object v0, Lcom/samsung/musicplus/library/view/SyncLyricView;->CLASSNAME:Ljava/lang/String;

    const-string v1, "getLyric() >"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 618
    invoke-virtual {p0}, Lcom/samsung/musicplus/library/view/SyncLyricView;->cancel()V

    .line 620
    iput-object p1, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mPath:Ljava/lang/String;

    .line 621
    iput-object p2, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mTitle:Ljava/lang/String;

    .line 622
    iput-object p3, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mArtist:Ljava/lang/String;

    .line 623
    iput-object p4, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mAlbum:Ljava/lang/String;

    .line 624
    iput-wide p5, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mDuration:J

    .line 626
    invoke-direct/range {p0 .. p6}, Lcom/samsung/musicplus/library/view/SyncLyricView;->getLyricInternal(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V

    .line 627
    sget-object v0, Lcom/samsung/musicplus/library/view/SyncLyricView;->CLASSNAME:Ljava/lang/String;

    const-string v1, "getLyric() <"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 628
    return-void
.end method

.method public getPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 660
    iget-object v0, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mPath:Ljava/lang/String;

    return-object v0
.end method

.method public hasLyric(Ljava/lang/String;)I
    .locals 3
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 765
    sget-object v0, Lcom/samsung/musicplus/library/view/SyncLyricView;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "hasLyric() path :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 766
    const/4 v0, 0x0

    return v0
.end method

.method protected onLayout(ZIIII)V
    .locals 3
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 381
    sget-object v0, Lcom/samsung/musicplus/library/view/SyncLyricView;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onLayout() mHeight : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mHeight:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mWidth : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mWidth:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 382
    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    .line 383
    return-void
.end method

.method protected onMeasure(II)V
    .locals 9
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 363
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 364
    .local v0, "height":I
    if-lez v0, :cond_0

    .line 365
    iput v0, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mHeight:I

    .line 367
    :cond_0
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v8

    .line 368
    .local v8, "width":I
    if-lez v8, :cond_1

    .line 369
    iput v8, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mWidth:I

    .line 371
    :cond_1
    iget v1, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mWidth:I

    if-lez v1, :cond_2

    iget-boolean v1, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mIsNeedToSetStyle:Z

    if-eqz v1, :cond_2

    .line 372
    iget-object v2, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mPath:Ljava/lang/String;

    iget-object v3, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mTitle:Ljava/lang/String;

    iget-object v4, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mArtist:Ljava/lang/String;

    iget-object v5, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mAlbum:Ljava/lang/String;

    iget-wide v6, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mDuration:J

    move-object v1, p0

    invoke-direct/range {v1 .. v7}, Lcom/samsung/musicplus/library/view/SyncLyricView;->getLyricInternal(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V

    .line 373
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mIsNeedToSetStyle:Z

    .line 375
    :cond_2
    sget-object v1, Lcom/samsung/musicplus/library/view/SyncLyricView;->CLASSNAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onMeasure() mHeight : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mHeight:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " mWidth : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mWidth:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 376
    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onMeasure(II)V

    .line 377
    return-void
.end method

.method public pause()V
    .locals 2

    .prologue
    .line 685
    sget-object v0, Lcom/samsung/musicplus/library/view/SyncLyricView;->CLASSNAME:Ljava/lang/String;

    const-string v1, "pause()"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 687
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mIsPause:Z

    .line 688
    return-void
.end method

.method public registerReceiver(Landroid/app/Activity;)V
    .locals 4
    .param p1, "a"    # Landroid/app/Activity;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 787
    sget-object v0, Lcom/samsung/musicplus/library/view/SyncLyricView;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "registerReceiver() Activity :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 788
    iget-object v0, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mActivity:Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 789
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "please check your unregisterReceiver() method did you missing unregisterReceiver() method before callig this method registerReceiver()? "

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 792
    :cond_0
    iput-object p1, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mActivity:Landroid/app/Activity;

    .line 793
    iget-object v0, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mActivity:Landroid/app/Activity;

    iget-object v1, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mSyncLyricReceiver:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "mediacanvas_getldplyrics_result"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 795
    return-void
.end method

.method public release()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 734
    sget-object v0, Lcom/samsung/musicplus/library/view/SyncLyricView;->CLASSNAME:Ljava/lang/String;

    const-string v1, "resetLyric() > "

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 735
    invoke-virtual {p0}, Lcom/samsung/musicplus/library/view/SyncLyricView;->cancel()V

    .line 737
    iput-object v2, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mMediaTimeListener:Lcom/samsung/musicplus/library/view/SyncLyricView$OnMediaTimeListener;

    .line 738
    iput-object v2, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mResultListener:Lcom/samsung/musicplus/library/view/SyncLyricView$OnResultListener;

    .line 739
    sget-object v0, Lcom/samsung/musicplus/library/view/SyncLyricView;->CLASSNAME:Ljava/lang/String;

    const-string v1, "resetLyric() < "

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 740
    return-void
.end method

.method public setColorOfLyric(I)V
    .locals 0
    .param p1, "color"    # I

    .prologue
    .line 521
    iput p1, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mColorOfLyric:I

    .line 522
    return-void
.end method

.method public setColorOfSyncedLyric(I)V
    .locals 0
    .param p1, "color"    # I

    .prologue
    .line 535
    iput p1, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mColorOfLyricSynced:I

    .line 536
    return-void
.end method

.method public setFont(Landroid/graphics/Typeface;)V
    .locals 0
    .param p1, "font"    # Landroid/graphics/Typeface;

    .prologue
    .line 563
    iput-object p1, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mFont:Landroid/graphics/Typeface;

    .line 564
    return-void
.end method

.method public setFontSize(I)V
    .locals 0
    .param p1, "size"    # I

    .prologue
    .line 549
    iput p1, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mFontSize:I

    .line 550
    return-void
.end method

.method public setMediaTimeListener(Lcom/samsung/musicplus/library/view/SyncLyricView$OnMediaTimeListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/samsung/musicplus/library/view/SyncLyricView$OnMediaTimeListener;

    .prologue
    .line 395
    sget-object v0, Lcom/samsung/musicplus/library/view/SyncLyricView;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setMediaTimeListener() OnMediaTimeListener :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 396
    iput-object p1, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mMediaTimeListener:Lcom/samsung/musicplus/library/view/SyncLyricView$OnMediaTimeListener;

    .line 397
    return-void
.end method

.method public setNumberOfLine(I)V
    .locals 0
    .param p1, "number"    # I

    .prologue
    .line 507
    iput p1, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mNumOfLine:I

    .line 508
    return-void
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0
    .param p1, "l"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 403
    iput-object p1, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mClickListener:Landroid/view/View$OnClickListener;

    .line 404
    return-void
.end method

.method public setResultListener(Lcom/samsung/musicplus/library/view/SyncLyricView$OnResultListener;)V
    .locals 3
    .param p1, "listener"    # Lcom/samsung/musicplus/library/view/SyncLyricView$OnResultListener;

    .prologue
    .line 416
    sget-object v0, Lcom/samsung/musicplus/library/view/SyncLyricView;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setMediaTimeListener() OnResultListener :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 417
    iput-object p1, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mResultListener:Lcom/samsung/musicplus/library/view/SyncLyricView$OnResultListener;

    .line 418
    return-void
.end method

.method public setStatusListener(Lcom/samsung/musicplus/library/view/SyncLyricView$OnSyncLyricStatusListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/musicplus/library/view/SyncLyricView$OnSyncLyricStatusListener;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 850
    iput-object p1, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mStatusListener:Lcom/samsung/musicplus/library/view/SyncLyricView$OnSyncLyricStatusListener;

    .line 851
    return-void
.end method

.method public setTime(I)V
    .locals 3
    .param p1, "time"    # I

    .prologue
    .line 709
    sget-object v0, Lcom/samsung/musicplus/library/view/SyncLyricView;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setTime() time : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mIsPause : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mIsPause:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 710
    iget-boolean v0, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mIsPause:Z

    if-eqz v0, :cond_0

    .line 713
    :cond_0
    return-void
.end method

.method public startSyncLyric()V
    .locals 3

    .prologue
    .line 596
    sget-object v0, Lcom/samsung/musicplus/library/view/SyncLyricView;->CLASSNAME:Ljava/lang/String;

    const-string v1, "showSyncLyric() >"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 597
    iget-boolean v0, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mIsPause:Z

    if-eqz v0, :cond_0

    .line 598
    invoke-direct {p0}, Lcom/samsung/musicplus/library/view/SyncLyricView;->resume()V

    .line 603
    :cond_0
    sget-object v0, Lcom/samsung/musicplus/library/view/SyncLyricView;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "showSyncLyric() < child : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/musicplus/library/view/SyncLyricView;->getChildCount()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 604
    return-void
.end method

.method public startSyncLyric(Ljava/lang/String;)V
    .locals 0
    .param p1, "path"    # Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 579
    return-void
.end method

.method public stop()V
    .locals 2

    .prologue
    .line 722
    sget-object v0, Lcom/samsung/musicplus/library/view/SyncLyricView;->CLASSNAME:Ljava/lang/String;

    const-string v1, "stop"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 724
    return-void
.end method

.method public unregisterReceiver()V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 811
    sget-object v0, Lcom/samsung/musicplus/library/view/SyncLyricView;->CLASSNAME:Ljava/lang/String;

    const-string v1, "unregisterReceiver()"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 813
    :try_start_0
    iget-object v0, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mActivity:Landroid/app/Activity;

    if-eqz v0, :cond_0

    .line 814
    iget-object v0, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mActivity:Landroid/app/Activity;

    iget-object v1, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mSyncLyricReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 815
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/musicplus/library/view/SyncLyricView;->mActivity:Landroid/app/Activity;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 821
    :cond_0
    :goto_0
    return-void

    .line 817
    :catch_0
    move-exception v0

    goto :goto_0
.end method
