.class public Lcom/samsung/musicplus/library/drm/DrmManager$PopupString;
.super Ljava/lang/Object;
.source "DrmManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/library/drm/DrmManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PopupString"
.end annotation


# static fields
.field public static final DRM_TEXT_ACQUIRING_LICENSE:I = 0x8

.field public static final DRM_TEXT_COUNT_INFO:I = 0x4

.field public static final DRM_TEXT_DELETE_ITEM:I = 0x3

.field public static final DRM_TEXT_FAIL_ACQUIRE_LICENSE:I = 0x5

.field public static final DRM_TEXT_LICENSE_EXPIRED:I = 0xa

.field public static final DRM_TEXT_NOT_SUPPORT:I = 0xb

.field public static final DRM_TEXT_NO_AVAILABLE:I = 0x2

.field public static final DRM_TEXT_NO_NETWORK:I = 0x9

.field public static final DRM_TEXT_SERVER_PROBLEM_TRY_LATER:I = 0x7

.field public static final DRM_TEXT_UNABLE_ACCESS_DURING_FLIGHT_MODE:I = 0x6

.field public static final DRM_TEXT_UNLOCK_NOW:I = 0x1


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 706
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
