.class public Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;
.super Ljava/lang/Object;
.source "KnoxPersonalModeManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager$PersonaInfoListCompat;
    }
.end annotation


# static fields
.field private static final ACTION_INSTALL_KNOX_APP:Ljava/lang/String; = "com.sec.knox.knoxappsinstaller.installservice.install"

.field public static final ACTION_KNOX_MODE_CHANGE:Ljava/lang/String; = "com.sec.knox.modeswitcher.receiver.INTENT_SWITCH_MODE"

.field public static final EXTRA_KEY_ERRORCODE:Ljava/lang/String; = "ERRORCODE"

.field public static final EXTRA_KEY_PACKAGE_NAME:Ljava/lang/String; = "PACKAGENAME"

.field public static final EXTRA_KEY_PATH:Ljava/lang/String; = "PATH"

.field public static final EXTRA_KEY_PROGRESS:Ljava/lang/String; = "PROGRESS"

.field public static final EXTRA_KEY_SUCCUSSCNT:Ljava/lang/String; = "SUCCESSCNT"

.field public static final INTENT_FILE_RELAY_CANCEL:Ljava/lang/String; = "com.sec.knox.container.FileRelayCancel"

.field public static final INTENT_FILE_RELAY_COMPLETE:Ljava/lang/String; = "com.sec.knox.container.FileRelayComplete"

.field public static final INTENT_FILE_RELAY_DONE:Ljava/lang/String; = "com.sec.knox.container.FileRelayDone"

.field public static final INTENT_FILE_RELAY_EXIST:Ljava/lang/String; = "com.sec.knox.container.FileRelayExist"

.field public static final INTENT_FILE_RELAY_FAIL:Ljava/lang/String; = "com.sec.knox.container.FileRelayFail"

.field public static final INTENT_FILE_RELAY_PROGRESS:Ljava/lang/String; = "com.sec.knox.container.FileRelayProgress"

.field public static final INTENT_FILE_RELAY_REQUEST:Ljava/lang/String; = "com.sec.knox.container.FileRelayRequest"

.field public static final MOVE_TO_APP_TYPE_GALLERY:I = 0x1

.field public static final MOVE_TO_APP_TYPE_MUSIC:I = 0x3

.field public static final MOVE_TO_APP_TYPE_MYFILES:I = 0x4

.field public static final MOVE_TO_APP_TYPE_VIDEO:I = 0x2

.field private static final TAG:Ljava/lang/String; = "KnoxModeManager"

.field private static mKnoxPersonalModeManager:Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;

.field private static sContainerInstallerManager:Lcom/sec/knox/containeragent/ContainerInstallerManager;

.field private static sIsVersion2:Ljava/lang/Boolean;


# instance fields
.field private COPY:I

.field private MOVE:I

.field private mConnected:Z

.field private mRcpManager:Landroid/os/RCPManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;->sContainerInstallerManager:Lcom/sec/knox/containeragent/ContainerInstallerManager;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 215
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;->mConnected:Z

    .line 506
    const/4 v1, 0x1

    iput v1, p0, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;->COPY:I

    .line 508
    const/4 v1, 0x2

    iput v1, p0, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;->MOVE:I

    .line 216
    invoke-static {p1}, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;->getKnoxInfoForAppBundle(Landroid/content/Context;)Landroid/os/Bundle;

    move-result-object v0

    .line 217
    .local v0, "bundle":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    const-string v1, "1"

    const-string v2, "ro.config.knox"

    const-string v3, "0"

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 218
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;->startContainerService(Landroid/content/Context;)V

    .line 220
    :cond_0
    return-void
.end method

.method static synthetic access$002(Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;
    .param p1, "x1"    # Z

    .prologue
    .line 37
    iput-boolean p1, p0, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;->mConnected:Z

    return p1
.end method

.method public static availableKnoxPersonalMode(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 283
    invoke-static {p0}, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;->getKnoxInfoForAppBundle(Landroid/content/Context;)Landroid/os/Bundle;

    move-result-object v0

    .line 284
    .local v0, "bundle":Landroid/os/Bundle;
    invoke-static {v0}, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;->isVersion2Internel(Landroid/os/Bundle;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 285
    const-string v4, "persona"

    invoke-virtual {p0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/PersonaManager;

    .line 286
    .local v2, "pm":Landroid/os/PersonaManager;
    invoke-virtual {v2}, Landroid/os/PersonaManager;->getPersonas()Ljava/util/List;

    move-result-object v1

    .line 287
    .local v1, "personas":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PersonaInfo;>;"
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v4

    if-lez v4, :cond_0

    .line 288
    const/4 v3, 0x1

    .line 293
    .end local v1    # "personas":Ljava/util/List;, "Ljava/util/List<Landroid/content/pm/PersonaInfo;>;"
    .end local v2    # "pm":Landroid/os/PersonaManager;
    :cond_0
    return v3
.end method

.method private doOperation(ILjava/util/List;Ljava/util/List;)J
    .locals 6
    .param p1, "requestApp"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 553
    .local p2, "srcPath":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p3, "destPath":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const-wide/16 v2, -0x1

    .line 555
    .local v2, "threadId":J
    :try_start_0
    iget-object v1, p0, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;->mRcpManager:Landroid/os/RCPManager;

    invoke-virtual {v1, p1, p2, p3}, Landroid/os/RCPManager;->moveFilesForApp(ILjava/util/List;Ljava/util/List;)J

    move-result-wide v2

    .line 556
    const-string v1, "KnoxModeManager"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "RequestApp : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", path : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " , threadId : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 562
    :goto_0
    return-wide v2

    .line 558
    :catch_0
    move-exception v0

    .line 559
    .local v0, "e":Landroid/os/RemoteException;
    const-string v1, "KnoxModeManager"

    const-string v4, "doOperation, RemoteException!!!"

    invoke-static {v1, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static getCallingUserrId()I
    .locals 1

    .prologue
    .line 584
    invoke-static {}, Landroid/os/UserHandle;->getCallingUserId()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 201
    sget-object v0, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;->mKnoxPersonalModeManager:Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;

    if-nez v0, :cond_0

    .line 202
    new-instance v0, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;->mKnoxPersonalModeManager:Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;

    .line 204
    :cond_0
    sget-object v0, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;->mKnoxPersonalModeManager:Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;

    return-object v0
.end method

.method public static getKnoxContainerInfo(Landroid/content/Context;)Ljava/util/HashMap;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 680
    const/4 v0, 0x0

    .line 681
    .local v0, "bundle":Landroid/os/Bundle;
    invoke-static {p0}, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;->getKnoxInfoForAppBundle(Landroid/content/Context;)Landroid/os/Bundle;

    move-result-object v0

    .line 682
    const-string v2, "KnoxIdNamePair"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v1

    check-cast v1, Ljava/util/HashMap;

    .line 685
    .local v1, "knoxIdNamePair":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Integer;Ljava/lang/String;>;"
    return-object v1
.end method

.method public static getKnoxFeatureDisabledString(Landroid/content/Context;)Ljava/lang/String;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 696
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v2

    const-string v3, "knox_feature_disabled_toast"

    const-string v4, "string"

    const-string v5, "android"

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 698
    .local v0, "resourceId":I
    const-string v1, ""

    .line 699
    .local v1, "text":Ljava/lang/String;
    if-lez v0, :cond_0

    .line 700
    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 702
    :cond_0
    return-object v1
.end method

.method private static getKnoxInfoForAppBundle(Landroid/content/Context;)Landroid/os/Bundle;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 414
    :try_start_0
    invoke-static {p0}, Landroid/os/PersonaManager;->getKnoxInfoForApp(Landroid/content/Context;)Landroid/os/Bundle;
    :try_end_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 419
    :goto_0
    return-object v0

    .line 415
    :catch_0
    move-exception v1

    .line 416
    .local v1, "e":Ljava/lang/NoClassDefFoundError;
    const-string v2, "KnoxModeManager"

    const-string v3, "Knox is not supported!!!"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 417
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getPersonaList(Landroid/content/Context;)Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager$PersonaInfoListCompat;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 597
    new-instance v0, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager$PersonaInfoListCompat;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager$PersonaInfoListCompat;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method public static isKnoxModeOn(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 306
    invoke-static {p0}, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;->getKnoxInfoForAppBundle(Landroid/content/Context;)Landroid/os/Bundle;

    move-result-object v0

    .line 307
    .local v0, "bundle":Landroid/os/Bundle;
    invoke-static {v0}, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;->isVersion2Internel(Landroid/os/Bundle;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 308
    if-eqz v0, :cond_0

    const-string v1, "true"

    const-string v2, "isKnoxMode"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 309
    const/4 v1, 0x1

    .line 312
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isOverVersion2(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 432
    sget-object v1, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;->sIsVersion2:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 433
    invoke-static {p0}, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;->getKnoxInfoForAppBundle(Landroid/content/Context;)Landroid/os/Bundle;

    move-result-object v0

    .line 434
    .local v0, "bundle":Landroid/os/Bundle;
    invoke-static {v0}, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;->isVersion2Internel(Landroid/os/Bundle;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    sput-object v1, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;->sIsVersion2:Ljava/lang/Boolean;

    .line 436
    .end local v0    # "bundle":Landroid/os/Bundle;
    :cond_0
    sget-object v1, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;->sIsVersion2:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    return v1
.end method

.method public static isSupportKnoxMove(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 321
    invoke-static {p0}, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;->getKnoxInfoForAppBundle(Landroid/content/Context;)Landroid/os/Bundle;

    move-result-object v0

    .line 322
    .local v0, "bundle":Landroid/os/Bundle;
    invoke-static {v0}, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;->isVersion2Internel(Landroid/os/Bundle;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 323
    if-eqz v0, :cond_0

    const-string v1, "true"

    const-string v2, "isSupportMoveTo"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 324
    const/4 v1, 0x1

    .line 327
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static isVersion2Internel(Landroid/os/Bundle;)Z
    .locals 2
    .param p0, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 405
    if-eqz p0, :cond_0

    const-string v0, "2.0"

    const-string v1, "version"

    invoke-virtual {p0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 406
    const/4 v0, 0x1

    .line 408
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public availableKnoxPersonalMode()Z
    .locals 2

    .prologue
    .line 266
    sget-object v0, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;->sContainerInstallerManager:Lcom/sec/knox/containeragent/ContainerInstallerManager;

    if-nez v0, :cond_0

    .line 268
    const-string v0, "KnoxModeManager"

    const-string v1, "availableKnoxPersonalMode() - mContainerInstallerManager is null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 269
    const/4 v0, 0x0

    .line 271
    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;->sContainerInstallerManager:Lcom/sec/knox/containeragent/ContainerInstallerManager;

    invoke-virtual {v0}, Lcom/sec/knox/containeragent/ContainerInstallerManager;->isKNOXFileRelayAvailable()Z

    move-result v0

    goto :goto_0
.end method

.method public isServiceConnected()Z
    .locals 1

    .prologue
    .line 357
    iget-boolean v0, p0, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;->mConnected:Z

    return v0
.end method

.method public makeSession(Landroid/content/Context;)Z
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 500
    const/4 v0, 0x0

    .line 501
    .local v0, "success":Z
    const-string v1, "rcp"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/RCPManager;

    iput-object v1, p0, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;->mRcpManager:Landroid/os/RCPManager;

    .line 503
    return v0
.end method

.method public move(Ljava/util/List;Ljava/util/List;)J
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 468
    .local p1, "srcPath":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p2, "destPath":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v0, 0x3

    invoke-direct {p0, v0, p1, p2}, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;->doOperation(ILjava/util/List;Ljava/util/List;)J

    move-result-wide v0

    return-wide v0
.end method

.method public releaseSession()V
    .locals 1

    .prologue
    .line 572
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;->mRcpManager:Landroid/os/RCPManager;

    .line 573
    return-void
.end method

.method public requestCancelMoveFile(Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 2
    .param p2, "packageName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 391
    .local p1, "filePath":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    sget-object v0, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;->sContainerInstallerManager:Lcom/sec/knox/containeragent/ContainerInstallerManager;

    if-nez v0, :cond_0

    .line 392
    const-string v0, "KnoxModeManager"

    const-string v1, "requestCancelMoveFile() - mContainerInstallerManager is null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 396
    :goto_0
    return-void

    .line 395
    :cond_0
    sget-object v0, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;->sContainerInstallerManager:Lcom/sec/knox/containeragent/ContainerInstallerManager;

    invoke-virtual {v0, p1, p2}, Lcom/sec/knox/containeragent/ContainerInstallerManager;->KNOXFileRelayCancel(Ljava/util/List;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public requestMoveFile(Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 2
    .param p2, "packageName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 341
    .local p1, "filePath":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    sget-object v0, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;->sContainerInstallerManager:Lcom/sec/knox/containeragent/ContainerInstallerManager;

    if-nez v0, :cond_0

    .line 342
    const-string v0, "KnoxModeManager"

    const-string v1, "requestMoveFile() - mContainerInstallerManager is null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 346
    :goto_0
    return-void

    .line 345
    :cond_0
    sget-object v0, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;->sContainerInstallerManager:Lcom/sec/knox/containeragent/ContainerInstallerManager;

    invoke-virtual {v0, p1, p2}, Lcom/sec/knox/containeragent/ContainerInstallerManager;->KNOXFileRelayRequest(Ljava/util/List;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public startContainerService(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 231
    invoke-static {p1}, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;->getKnoxInfoForAppBundle(Landroid/content/Context;)Landroid/os/Bundle;

    move-result-object v0

    .line 232
    .local v0, "bundle":Landroid/os/Bundle;
    invoke-static {v0}, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;->isVersion2Internel(Landroid/os/Bundle;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 255
    :cond_0
    :goto_0
    return-void

    .line 235
    :cond_1
    sget-object v1, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;->sContainerInstallerManager:Lcom/sec/knox/containeragent/ContainerInstallerManager;

    if-eqz v1, :cond_2

    iget-boolean v1, p0, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;->mConnected:Z

    if-nez v1, :cond_0

    .line 236
    :cond_2
    new-instance v1, Lcom/sec/knox/containeragent/ContainerInstallerManager;

    new-instance v2, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager$1;

    invoke-direct {v2, p0, p1}, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager$1;-><init>(Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;Landroid/content/Context;)V

    invoke-direct {v1, p1, v2}, Lcom/sec/knox/containeragent/ContainerInstallerManager;-><init>(Landroid/content/Context;Landroid/content/ServiceConnection;)V

    sput-object v1, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;->sContainerInstallerManager:Lcom/sec/knox/containeragent/ContainerInstallerManager;

    goto :goto_0
.end method

.method public unbindService()V
    .locals 2

    .prologue
    .line 369
    sget-object v0, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;->sContainerInstallerManager:Lcom/sec/knox/containeragent/ContainerInstallerManager;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;->isServiceConnected()Z

    move-result v0

    if-nez v0, :cond_1

    .line 370
    :cond_0
    const-string v0, "KnoxModeManager"

    const-string v1, "unbindService() - mContainerInstallerManager is null or not connected service"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 378
    :goto_0
    return-void

    .line 375
    :cond_1
    sget-object v0, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;->sContainerInstallerManager:Lcom/sec/knox/containeragent/ContainerInstallerManager;

    invoke-virtual {v0}, Lcom/sec/knox/containeragent/ContainerInstallerManager;->unbindContainerManager()V

    .line 376
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;->sContainerInstallerManager:Lcom/sec/knox/containeragent/ContainerInstallerManager;

    .line 377
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;->mConnected:Z

    goto :goto_0
.end method
