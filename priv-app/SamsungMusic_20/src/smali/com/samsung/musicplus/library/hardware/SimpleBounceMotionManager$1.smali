.class Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager$1;
.super Ljava/lang/Object;
.source "SimpleBounceMotionManager.java"

# interfaces
.implements Landroid/hardware/scontext/SContextListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager;-><init>(Landroid/content/Context;Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager$OnBounceListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager;

.field final synthetic val$listener:Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager$OnBounceListener;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager;Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager$OnBounceListener;)V
    .locals 0

    .prologue
    .line 143
    iput-object p1, p0, Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager$1;->this$0:Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager;

    iput-object p2, p0, Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager$1;->val$listener:Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager$OnBounceListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSContextChanged(Landroid/hardware/scontext/SContextEvent;)V
    .locals 5
    .param p1, "event"    # Landroid/hardware/scontext/SContextEvent;

    .prologue
    .line 146
    iget-object v2, p1, Landroid/hardware/scontext/SContextEvent;->scontext:Landroid/hardware/scontext/SContext;

    .line 147
    .local v2, "scontext":Landroid/hardware/scontext/SContext;
    invoke-virtual {v2}, Landroid/hardware/scontext/SContext;->getType()I

    move-result v3

    const/16 v4, 0x11

    if-ne v3, v4, :cond_0

    .line 148
    invoke-virtual {p1}, Landroid/hardware/scontext/SContextEvent;->getBounceShortMotionContext()Landroid/hardware/scontext/SContextBounceShortMotion;

    move-result-object v1

    .line 149
    .local v1, "motion":Landroid/hardware/scontext/SContextBounceShortMotion;
    invoke-virtual {v1}, Landroid/hardware/scontext/SContextBounceShortMotion;->getAction()I

    move-result v0

    .line 150
    .local v0, "bounce":I
    iget-object v3, p0, Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager$1;->val$listener:Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager$OnBounceListener;

    invoke-interface {v3, v0}, Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager$OnBounceListener;->onBounce(I)V

    .line 152
    .end local v0    # "bounce":I
    .end local v1    # "motion":Landroid/hardware/scontext/SContextBounceShortMotion;
    :cond_0
    return-void
.end method
