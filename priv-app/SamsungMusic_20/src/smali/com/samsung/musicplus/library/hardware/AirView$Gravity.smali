.class public interface abstract Lcom/samsung/musicplus/library/hardware/AirView$Gravity;
.super Ljava/lang/Object;
.source "AirView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/library/hardware/AirView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Gravity"
.end annotation


# static fields
.field public static final BOTTOM_UNDER:I = 0x5050

.field public static final CENTER_HORIZONTAL:I = 0x1

.field public static final LEFT:I = 0x3

.field public static final LEFT_CENTER_AXIS:I = 0x103

.field public static final NO_GRAVITY:I = 0x0

.field public static final RIGHT:I = 0x5

.field public static final RIGHT_CENTER_AXIS:I = 0x105

.field public static final TOP_ABOVE:I = 0x3030
