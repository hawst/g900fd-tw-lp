.class public Lcom/samsung/musicplus/library/audio/RemoteControlClientCompat;
.super Ljava/lang/Object;
.source "RemoteControlClientCompat.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/library/audio/RemoteControlClientCompat$OnCompatCommandSetListener;
    }
.end annotation


# static fields
.field public static final PLAYER_ATTR_EQUALIZER:B = 0x1t

.field public static final PLAYER_ATTR_REPEAT:B = 0x2t

.field public static final PLAYER_ATTR_SCAN:B = 0x4t

.field public static final PLAYER_ATTR_SHUFFLE:B = 0x3t


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    return-void
.end method

.method public static setCommandSetListener(Landroid/media/RemoteControlClient;Lcom/samsung/musicplus/library/audio/RemoteControlClientCompat$OnCompatCommandSetListener;)V
    .locals 0
    .param p0, "rcc"    # Landroid/media/RemoteControlClient;
    .param p1, "listener"    # Lcom/samsung/musicplus/library/audio/RemoteControlClientCompat$OnCompatCommandSetListener;

    .prologue
    .line 112
    return-void
.end method

.method public static setCommandToDisplay(Landroid/media/RemoteControlClient;II)V
    .locals 0
    .param p0, "rcc"    # Landroid/media/RemoteControlClient;
    .param p1, "key"    # I
    .param p2, "value"    # I

    .prologue
    .line 92
    return-void
.end method
