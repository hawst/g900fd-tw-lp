.class public Lcom/samsung/musicplus/library/hardware/SimpleMotionManager;
.super Ljava/lang/Object;
.source "SimpleMotionManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/library/hardware/SimpleMotionManager$OnMotionListener;
    }
.end annotation


# static fields
.field public static final ACTION_PALM_DOWN:Ljava/lang/String; = "android.intent.action.PALM_DOWN"

.field public static final ACTION_PALM_UP:Ljava/lang/String; = "android.intent.action.PALM_UP"

.field private static final CLASSNAME:Ljava/lang/String;

.field public static final PAUSE_MUSIC:I = 0xa

.field public static final SYSTEM_PREF_MOTION_ENGINE:Ljava/lang/String; = "motion_engine"

.field public static final SYSTEM_PREF_MOTION_OVERTURN:Ljava/lang/String; = "motion_overturn"


# instance fields
.field private mIsRegisteredListener:Z

.field private mMotionListener:Lcom/samsung/android/motion/MRListener;

.field private final mMotionSensorManager:Lcom/samsung/android/motion/MotionRecognitionManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    const-class v0, Lcom/samsung/musicplus/library/hardware/SimpleMotionManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/library/hardware/SimpleMotionManager;->CLASSNAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/samsung/musicplus/library/hardware/SimpleMotionManager$OnMotionListener;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "onMotionListener"    # Lcom/samsung/musicplus/library/hardware/SimpleMotionManager$OnMotionListener;

    .prologue
    .line 126
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 72
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/library/hardware/SimpleMotionManager;->mIsRegisteredListener:Z

    .line 127
    const-string v0, "motion_recognition"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/motion/MotionRecognitionManager;

    iput-object v0, p0, Lcom/samsung/musicplus/library/hardware/SimpleMotionManager;->mMotionSensorManager:Lcom/samsung/android/motion/MotionRecognitionManager;

    .line 129
    iget-object v0, p0, Lcom/samsung/musicplus/library/hardware/SimpleMotionManager;->mMotionSensorManager:Lcom/samsung/android/motion/MotionRecognitionManager;

    if-eqz v0, :cond_0

    .line 130
    new-instance v0, Lcom/samsung/musicplus/library/hardware/SimpleMotionManager$1;

    invoke-direct {v0, p0, p2}, Lcom/samsung/musicplus/library/hardware/SimpleMotionManager$1;-><init>(Lcom/samsung/musicplus/library/hardware/SimpleMotionManager;Lcom/samsung/musicplus/library/hardware/SimpleMotionManager$OnMotionListener;)V

    iput-object v0, p0, Lcom/samsung/musicplus/library/hardware/SimpleMotionManager;->mMotionListener:Lcom/samsung/android/motion/MRListener;

    .line 144
    :cond_0
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lcom/samsung/musicplus/library/hardware/SimpleMotionManager;->CLASSNAME:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public registerMotionListener()V
    .locals 3

    .prologue
    .line 153
    iget-object v0, p0, Lcom/samsung/musicplus/library/hardware/SimpleMotionManager;->mMotionSensorManager:Lcom/samsung/android/motion/MotionRecognitionManager;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/musicplus/library/hardware/SimpleMotionManager;->mIsRegisteredListener:Z

    if-nez v0, :cond_0

    .line 154
    sget-object v0, Lcom/samsung/musicplus/library/hardware/SimpleMotionManager;->CLASSNAME:Ljava/lang/String;

    const-string v1, "registerMotionListener"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    iget-object v0, p0, Lcom/samsung/musicplus/library/hardware/SimpleMotionManager;->mMotionSensorManager:Lcom/samsung/android/motion/MotionRecognitionManager;

    iget-object v1, p0, Lcom/samsung/musicplus/library/hardware/SimpleMotionManager;->mMotionListener:Lcom/samsung/android/motion/MRListener;

    const/high16 v2, 0x20000

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/motion/MotionRecognitionManager;->registerListenerEvent(Lcom/samsung/android/motion/MRListener;I)V

    .line 157
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/library/hardware/SimpleMotionManager;->mIsRegisteredListener:Z

    .line 159
    :cond_0
    return-void
.end method

.method public unregisterMotionListener()V
    .locals 2

    .prologue
    .line 168
    iget-object v0, p0, Lcom/samsung/musicplus/library/hardware/SimpleMotionManager;->mMotionSensorManager:Lcom/samsung/android/motion/MotionRecognitionManager;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/musicplus/library/hardware/SimpleMotionManager;->mIsRegisteredListener:Z

    if-eqz v0, :cond_0

    .line 169
    sget-object v0, Lcom/samsung/musicplus/library/hardware/SimpleMotionManager;->CLASSNAME:Ljava/lang/String;

    const-string v1, "unregisterMotionListener"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    iget-object v0, p0, Lcom/samsung/musicplus/library/hardware/SimpleMotionManager;->mMotionSensorManager:Lcom/samsung/android/motion/MotionRecognitionManager;

    iget-object v1, p0, Lcom/samsung/musicplus/library/hardware/SimpleMotionManager;->mMotionListener:Lcom/samsung/android/motion/MRListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/motion/MotionRecognitionManager;->unregisterListener(Lcom/samsung/android/motion/MRListener;)V

    .line 171
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/library/hardware/SimpleMotionManager;->mIsRegisteredListener:Z

    .line 173
    :cond_0
    return-void
.end method
