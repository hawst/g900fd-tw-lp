.class public Lcom/samsung/musicplus/library/app/SthemeManager;
.super Ljava/lang/Object;
.source "SthemeManager.java"


# static fields
.field public static final CURRENT_THEME_PACKAGE:Ljava/lang/String; = "current_sec_theme_package"

.field public static final HOME_THEME_CHANGED:Ljava/lang/String; = "temp"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# instance fields
.field private mTheme:Lcom/samsung/android/theme/SThemeManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    new-instance v0, Lcom/samsung/android/theme/SThemeManager;

    invoke-direct {v0, p1}, Lcom/samsung/android/theme/SThemeManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/musicplus/library/app/SthemeManager;->mTheme:Lcom/samsung/android/theme/SThemeManager;

    .line 40
    return-void
.end method


# virtual methods
.method public getItemBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 1
    .param p1, "resName"    # Ljava/lang/String;

    .prologue
    .line 98
    iget-object v0, p0, Lcom/samsung/musicplus/library/app/SthemeManager;->mTheme:Lcom/samsung/android/theme/SThemeManager;

    invoke-virtual {v0, p1}, Lcom/samsung/android/theme/SThemeManager;->getItemBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method public getItemColor(Ljava/lang/String;)I
    .locals 1
    .param p1, "resName"    # Ljava/lang/String;

    .prologue
    .line 107
    iget-object v0, p0, Lcom/samsung/musicplus/library/app/SthemeManager;->mTheme:Lcom/samsung/android/theme/SThemeManager;

    invoke-virtual {v0, p1}, Lcom/samsung/android/theme/SThemeManager;->getItemColor(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getItemDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p1, "resName"    # Ljava/lang/String;

    .prologue
    .line 89
    iget-object v0, p0, Lcom/samsung/musicplus/library/app/SthemeManager;->mTheme:Lcom/samsung/android/theme/SThemeManager;

    invoke-virtual {v0, p1}, Lcom/samsung/android/theme/SThemeManager;->getItemDrawable(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public getItemText(Ljava/lang/String;)Ljava/lang/CharSequence;
    .locals 1
    .param p1, "resName"    # Ljava/lang/String;

    .prologue
    .line 80
    iget-object v0, p0, Lcom/samsung/musicplus/library/app/SthemeManager;->mTheme:Lcom/samsung/android/theme/SThemeManager;

    invoke-virtual {v0, p1}, Lcom/samsung/android/theme/SThemeManager;->getItemText(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public getPackageIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p1, "resName"    # Ljava/lang/String;

    .prologue
    .line 116
    iget-object v0, p0, Lcom/samsung/musicplus/library/app/SthemeManager;->mTheme:Lcom/samsung/android/theme/SThemeManager;

    invoke-virtual {v0, p1}, Lcom/samsung/android/theme/SThemeManager;->getPackageIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method public getThemePackageName()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 60
    const/4 v0, 0x0

    return-object v0
.end method

.method public setThemePackageName(Ljava/lang/String;)V
    .locals 0
    .param p1, "name"    # Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 50
    return-void
.end method

.method public usingPackageNameFromSettings()Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 71
    const/4 v0, 0x0

    return v0
.end method
