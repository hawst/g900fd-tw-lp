.class public Lcom/samsung/musicplus/library/metadata/MetaDataConverter;
.super Ljava/lang/Object;
.source "MetaDataConverter.java"


# static fields
.field public static final METADATA_KEY_ALBUM:I = 0x1

.field public static final METADATA_KEY_ALBUMARTIST:I = 0xd

.field public static final METADATA_KEY_ARTIST:I = 0x2

.field public static final METADATA_KEY_COMPOSER:I = 0x4

.field public static final METADATA_KEY_TITLE:I = 0x7

.field public static final RCD_EOF:I = 0x0

.field public static final RCD_ERR_FILEIO:I = -0x5

.field public static final RCD_ERR_INVALID_ARG:I = -0x1

.field public static final RCD_ERR_MEMALLOC:I = -0x4

.field public static final RCD_ERR_UNEXPECTED:I = -0x3

.field public static final RCD_ERR_UNSUPPORTED:I = -0x2

.field public static final RCD_OK:I = 0x1


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static closeFile()I
    .locals 1

    .prologue
    .line 156
    invoke-static {}, Lcom/samsung/tageditor/SMediaTagEditor;->closeFile()I

    move-result v0

    return v0
.end method

.method public static getMetadata(II)[B
    .locals 1
    .param p0, "metakey"    # I
    .param p1, "opt"    # I

    .prologue
    .line 132
    invoke-static {p0, p1}, Lcom/samsung/tageditor/SMediaTagEditor;->getMetadata(II)[B

    move-result-object v0

    return-object v0
.end method

.method public static openFile(Ljava/lang/String;Ljava/lang/String;)I
    .locals 1
    .param p0, "filepath"    # Ljava/lang/String;
    .param p1, "mime"    # Ljava/lang/String;

    .prologue
    .line 109
    invoke-static {p0, p1}, Lcom/samsung/tageditor/SMediaTagEditor;->openFile(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public static saveMetadata()I
    .locals 1

    .prologue
    .line 144
    invoke-static {}, Lcom/samsung/tageditor/SMediaTagEditor;->saveMetadata()I

    move-result v0

    return v0
.end method

.method public static setMetadata(ILjava/lang/String;)I
    .locals 1
    .param p0, "metakey"    # I
    .param p1, "metadata"    # Ljava/lang/String;

    .prologue
    .line 123
    invoke-static {p0, p1}, Lcom/samsung/tageditor/SMediaTagEditor;->setMetadata(ILjava/lang/String;)I

    move-result v0

    return v0
.end method
