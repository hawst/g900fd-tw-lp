.class public Lcom/samsung/musicplus/library/drm/DrmManager;
.super Ljava/lang/Object;
.source "DrmManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/library/drm/DrmManager$OptionInfo;,
        Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo;,
        Lcom/samsung/musicplus/library/drm/DrmManager$OnPlayReadyListener;,
        Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;,
        Lcom/samsung/musicplus/library/drm/DrmManager$PopupString;
    }
.end annotation


# static fields
.field private static final DEBUG:Z = false

.field private static final DRM2_COUNT:I = 0x1

.field private static final DRM2_DATETIME:I = 0x2

.field private static final DRM2_INTERVAL:I = 0x4

.field private static final DRM2_TIMED_COUNT:I = 0x8

.field private static final DRM2_UNLIMITED:I = 0x0

.field private static final DRM_LICENSE_STATE_COUNT:I = 0x2

.field private static final DRM_LICENSE_STATE_COUNT_FROM:I = 0x6

.field private static final DRM_LICENSE_STATE_COUNT_FROM_UNTIL:I = 0x8

.field private static final DRM_LICENSE_STATE_COUNT_UNTIL:I = 0x7

.field private static final DRM_LICENSE_STATE_EXPIRATION_AFTER_FIRSTUSE:I = 0x9

.field private static final DRM_LICENSE_STATE_FORCE_SYNC:I = 0xa

.field private static final DRM_LICENSE_STATE_FROM:I = 0x3

.field private static final DRM_LICENSE_STATE_FROM_UNTIL:I = 0x5

.field private static final DRM_LICENSE_STATE_NORIGHT:I = 0x0

.field private static final DRM_LICENSE_STATE_UNLIM:I = 0x1

.field private static final DRM_LICENSE_STATE_UNTIL:I = 0x4

.field private static final DRM_OPTION_RINGTONE:Ljava/lang/String; = "bRingtone"

.field private static final DRM_OPTION_SHARE:Ljava/lang/String; = "bSendAs"

.field private static final DRM_PATH:Ljava/lang/String; = "drm_path"

.field private static final DRM_TYPE_LGT:I = 0x3

.field private static final DRM_TYPE_OMA:I = 0x1

.field private static final DRM_TYPE_PLAYREADY:I = 0x2

.field private static final DRM_TYPE_UNKNOWN:I = 0x0

.field private static final ENABLE_LGT_DRM:Z

.field private static final FLAG_SUPPORT_OMA_FL_ONLY:Z

.field public static final INFO_COUNT:Ljava/lang/String; = "count"

.field public static final INFO_PATH:Ljava/lang/String; = "path"

.field private static final INFO_STATUS:Ljava/lang/String; = "status"

.field public static final INFO_TEXT1:Ljava/lang/String; = "text1"

.field public static final INFO_TEXT2:Ljava/lang/String; = "text2"

.field public static final INFO_TYPE:Ljava/lang/String; = "type"

.field public static final INFO_URL:Ljava/lang/String; = "url"

.field private static final LICENSE_CATEGORY_COLUMN:Ljava/lang/String; = "license_category"

.field private static final LICENSE_ORIGINAL_INTERVAL:Ljava/lang/String; = "license_original_interval"

.field private static final OMA_PLUGIN_MIME:Ljava/lang/String; = "application/vnd.oma.drm.content"

.field private static final OMA_SD:I = 0x3

.field private static final OMA_SSD:I = 0x2

.field private static final OMA_TYPE:Ljava/lang/String; = "type"

.field private static final PR_PLUGIN_MIME:Ljava/lang/String; = "audio/vnd.ms-playready.media.pya"

.field private static final TAG:Ljava/lang/String; = "MusicDrm"

.field private static final TYPE_GET_DRMFILE_INFO:I = 0xe

.field private static final TYPE_GET_OPTION_MENU:I = 0x10

.field private static final UNDEFINED:I = -0x1

.field static final UNLIMITED:Ljava/lang/String; = "unlimited"

.field private static final WARNING_LEVEL:I = 0x2

.field private static sDrmManager:Lcom/samsung/musicplus/library/drm/DrmManager;


# instance fields
.field private mAcquireTry:I

.field private mClient:Landroid/drm/DrmManagerClient;

.field private final mContext:Landroid/content/Context;

.field private final mDrmErrorListener:Landroid/drm/DrmManagerClient$OnErrorListener;

.field private final mDrmEventListener:Landroid/drm/DrmManagerClient$OnEventListener;

.field private mLgDrm:Lcom/samsung/musicplus/library/drm/LgtDrmManager;

.field private mOnPlayReadyListener:Lcom/samsung/musicplus/library/drm/DrmManager$OnPlayReadyListener;

.field private mPath:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 160
    sget-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_CHECK_KOR_LGT:Z

    sput-boolean v0, Lcom/samsung/musicplus/library/drm/DrmManager;->ENABLE_LGT_DRM:Z

    .line 1094
    invoke-static {}, Lcom/samsung/musicplus/library/drm/DrmManager;->isForwardLockOnly()Z

    move-result v0

    sput-boolean v0, Lcom/samsung/musicplus/library/drm/DrmManager;->FLAG_SUPPORT_OMA_FL_ONLY:Z

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 279
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1407
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/musicplus/library/drm/DrmManager;->mAcquireTry:I

    .line 2211
    new-instance v0, Lcom/samsung/musicplus/library/drm/DrmManager$2;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/library/drm/DrmManager$2;-><init>(Lcom/samsung/musicplus/library/drm/DrmManager;)V

    iput-object v0, p0, Lcom/samsung/musicplus/library/drm/DrmManager;->mDrmEventListener:Landroid/drm/DrmManagerClient$OnEventListener;

    .line 2314
    new-instance v0, Lcom/samsung/musicplus/library/drm/DrmManager$3;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/library/drm/DrmManager$3;-><init>(Lcom/samsung/musicplus/library/drm/DrmManager;)V

    iput-object v0, p0, Lcom/samsung/musicplus/library/drm/DrmManager;->mDrmErrorListener:Landroid/drm/DrmManagerClient$OnErrorListener;

    .line 281
    iput-object p1, p0, Lcom/samsung/musicplus/library/drm/DrmManager;->mContext:Landroid/content/Context;

    .line 283
    new-instance v0, Landroid/drm/DrmManagerClient;

    invoke-direct {v0, p1}, Landroid/drm/DrmManagerClient;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/musicplus/library/drm/DrmManager;->mClient:Landroid/drm/DrmManagerClient;

    .line 285
    iget-object v0, p0, Lcom/samsung/musicplus/library/drm/DrmManager;->mClient:Landroid/drm/DrmManagerClient;

    iget-object v1, p0, Lcom/samsung/musicplus/library/drm/DrmManager;->mDrmEventListener:Landroid/drm/DrmManagerClient$OnEventListener;

    invoke-virtual {v0, v1}, Landroid/drm/DrmManagerClient;->setOnEventListener(Landroid/drm/DrmManagerClient$OnEventListener;)V

    .line 287
    iget-object v0, p0, Lcom/samsung/musicplus/library/drm/DrmManager;->mClient:Landroid/drm/DrmManagerClient;

    iget-object v1, p0, Lcom/samsung/musicplus/library/drm/DrmManager;->mDrmErrorListener:Landroid/drm/DrmManagerClient$OnErrorListener;

    invoke-virtual {v0, v1}, Landroid/drm/DrmManagerClient;->setOnErrorListener(Landroid/drm/DrmManagerClient$OnErrorListener;)V

    .line 289
    sget-boolean v0, Lcom/samsung/musicplus/library/drm/DrmManager;->ENABLE_LGT_DRM:Z

    if-eqz v0, :cond_0

    .line 295
    new-instance v0, Lcom/samsung/musicplus/library/drm/LgtDrmManager;

    invoke-direct {v0, p1}, Lcom/samsung/musicplus/library/drm/LgtDrmManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/musicplus/library/drm/DrmManager;->mLgDrm:Lcom/samsung/musicplus/library/drm/LgtDrmManager;

    .line 299
    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/library/drm/DrmManager;Ljava/lang/String;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/library/drm/DrmManager;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 120
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/library/drm/DrmManager;->acquireRights(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/samsung/musicplus/library/drm/DrmManager;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/library/drm/DrmManager;

    .prologue
    .line 120
    iget-object v0, p0, Lcom/samsung/musicplus/library/drm/DrmManager;->mPath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/musicplus/library/drm/DrmManager;)Landroid/drm/DrmManagerClient;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/library/drm/DrmManager;

    .prologue
    .line 120
    iget-object v0, p0, Lcom/samsung/musicplus/library/drm/DrmManager;->mClient:Landroid/drm/DrmManagerClient;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/musicplus/library/drm/DrmManager;I)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/library/drm/DrmManager;
    .param p1, "x1"    # I

    .prologue
    .line 120
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/library/drm/DrmManager;->isValidInternal(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/samsung/musicplus/library/drm/DrmManager;)Lcom/samsung/musicplus/library/drm/DrmManager$OnPlayReadyListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/library/drm/DrmManager;

    .prologue
    .line 120
    iget-object v0, p0, Lcom/samsung/musicplus/library/drm/DrmManager;->mOnPlayReadyListener:Lcom/samsung/musicplus/library/drm/DrmManager$OnPlayReadyListener;

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/musicplus/library/drm/DrmManager;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/library/drm/DrmManager;

    .prologue
    .line 120
    iget v0, p0, Lcom/samsung/musicplus/library/drm/DrmManager;->mAcquireTry:I

    return v0
.end method

.method private acquireRights(Ljava/lang/String;)I
    .locals 3
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 1419
    new-instance v0, Landroid/drm/DrmInfoRequest;

    const/4 v1, 0x3

    invoke-direct {p0, p1}, Lcom/samsung/musicplus/library/drm/DrmManager;->getDrmMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/drm/DrmInfoRequest;-><init>(ILjava/lang/String;)V

    .line 1423
    .local v0, "request":Landroid/drm/DrmInfoRequest;
    const-string v1, "drm_path"

    invoke-virtual {v0, v1, p1}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1425
    iget v1, p0, Lcom/samsung/musicplus/library/drm/DrmManager;->mAcquireTry:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/samsung/musicplus/library/drm/DrmManager;->mAcquireTry:I

    .line 1427
    iput-object p1, p0, Lcom/samsung/musicplus/library/drm/DrmManager;->mPath:Ljava/lang/String;

    .line 1429
    iget-object v1, p0, Lcom/samsung/musicplus/library/drm/DrmManager;->mClient:Landroid/drm/DrmManagerClient;

    invoke-virtual {v1, v0}, Landroid/drm/DrmManagerClient;->acquireRights(Landroid/drm/DrmInfoRequest;)I

    move-result v1

    return v1
.end method

.method private getCategory(Landroid/content/ContentValues;)I
    .locals 7
    .param p1, "constraint"    # Landroid/content/ContentValues;

    .prologue
    const/4 v3, -0x1

    .line 2063
    if-nez p1, :cond_0

    .line 2065
    const-string v4, "MusicDrm"

    const-string v5, "getCategory constraint is null "

    invoke-static {v4, v5}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v3

    .line 2095
    :goto_0
    return v0

    .line 2071
    :cond_0
    const-string v4, "license_category"

    invoke-virtual {p1, v4}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    .line 2073
    .local v2, "license":Ljava/lang/Object;
    if-nez v2, :cond_1

    move v0, v3

    .line 2075
    goto :goto_0

    .line 2079
    :cond_1
    const/4 v0, -0x1

    .line 2083
    .local v0, "category":I
    :try_start_0
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 2085
    const-string v4, "MusicDrm"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getCategory: categoryType : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2087
    :catch_0
    move-exception v1

    .line 2089
    .local v1, "e":Ljava/lang/NumberFormatException;
    invoke-virtual {v1}, Ljava/lang/NumberFormatException;->printStackTrace()V

    move v0, v3

    .line 2091
    goto :goto_0
.end method

.method private getDrmMimeType(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "file"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 2388
    if-nez p1, :cond_1

    .line 2430
    :cond_0
    :goto_0
    return-object v0

    .line 2394
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p1

    .line 2396
    const-string v1, ".dcf"

    invoke-virtual {p1, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2398
    const-string v0, "application/vnd.oma.drm.content"

    goto :goto_0

    .line 2400
    :cond_2
    const-string v1, ".pya"

    invoke-virtual {p1, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2402
    const-string v0, "audio/vnd.ms-playready.media.pya"

    goto :goto_0

    .line 2404
    :cond_3
    const-string v1, ".wmv"

    invoke-virtual {p1, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2406
    const-string v0, "video/x-ms-wmv"

    goto :goto_0

    .line 2408
    :cond_4
    const-string v1, ".wma"

    invoke-virtual {p1, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 2410
    const-string v0, "audio/x-ms-wma"

    goto :goto_0

    .line 2412
    :cond_5
    const-string v1, ".pyv"

    invoke-virtual {p1, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 2414
    const-string v0, "video/vnd.ms-playready.media.pyv"

    goto :goto_0

    .line 2416
    :cond_6
    const-string v1, ".avi"

    invoke-virtual {p1, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 2418
    const-string v0, "video/mux/AVI"

    goto :goto_0

    .line 2420
    :cond_7
    const-string v1, ".mkv"

    invoke-virtual {p1, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 2422
    const-string v0, "video/mux/MKV"

    goto :goto_0

    .line 2424
    :cond_8
    const-string v1, ".divx"

    invoke-virtual {p1, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2426
    const-string v0, "video/mux/DivX"

    goto :goto_0
.end method

.method private getDrmType(Ljava/lang/String;)I
    .locals 2
    .param p1, "file"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 2356
    if-nez p1, :cond_1

    .line 2380
    :cond_0
    :goto_0
    return v0

    .line 2362
    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object p1

    .line 2364
    sget-boolean v1, Lcom/samsung/musicplus/library/drm/DrmManager;->ENABLE_LGT_DRM:Z

    if-eqz v1, :cond_2

    const-string v1, ".odf"

    invoke-virtual {p1, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2366
    const/4 v0, 0x3

    goto :goto_0

    .line 2368
    :cond_2
    const-string v1, ".dcf"

    invoke-virtual {p1, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2370
    const/4 v0, 0x1

    goto :goto_0

    .line 2372
    :cond_3
    const-string v1, ".pya"

    invoke-virtual {p1, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, ".wmv"

    invoke-virtual {p1, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, ".wma"

    invoke-virtual {p1, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    const-string v1, ".pyv"

    invoke-virtual {p1, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2376
    :cond_4
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/samsung/musicplus/library/drm/DrmManager;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 140
    sget-object v0, Lcom/samsung/musicplus/library/drm/DrmManager;->sDrmManager:Lcom/samsung/musicplus/library/drm/DrmManager;

    if-nez v0, :cond_0

    .line 142
    new-instance v0, Lcom/samsung/musicplus/library/drm/DrmManager;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/library/drm/DrmManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/musicplus/library/drm/DrmManager;->sDrmManager:Lcom/samsung/musicplus/library/drm/DrmManager;

    .line 146
    :cond_0
    sget-object v0, Lcom/samsung/musicplus/library/drm/DrmManager;->sDrmManager:Lcom/samsung/musicplus/library/drm/DrmManager;

    return-object v0
.end method

.method private getInvalidOmaDrmPopup(ILjava/lang/String;Z)Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;
    .locals 10
    .param p1, "status"    # I
    .param p2, "path"    # Ljava/lang/String;
    .param p3, "fromList"    # Z

    .prologue
    const/16 v9, 0xe

    const/4 v8, 0x3

    const/4 v7, 0x2

    .line 1122
    new-instance v3, Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;

    invoke-direct {v3}, Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;-><init>()V

    .line 1124
    .local v3, "popup":Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;
    new-instance v4, Landroid/drm/DrmInfoRequest;

    const-string v6, "application/vnd.oma.drm.content"

    invoke-direct {v4, v9, v6}, Landroid/drm/DrmInfoRequest;-><init>(ILjava/lang/String;)V

    .line 1126
    .local v4, "request":Landroid/drm/DrmInfoRequest;
    const-string v6, "drm_path"

    invoke-virtual {v4, v6, p2}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1128
    iget-object v6, p0, Lcom/samsung/musicplus/library/drm/DrmManager;->mClient:Landroid/drm/DrmManagerClient;

    invoke-virtual {v6, v4}, Landroid/drm/DrmManagerClient;->acquireDrmInfo(Landroid/drm/DrmInfoRequest;)Landroid/drm/DrmInfo;

    move-result-object v2

    .line 1130
    .local v2, "info":Landroid/drm/DrmInfo;
    const/4 v0, 0x0

    .line 1134
    .local v0, "cdtype":I
    :try_start_0
    const-string v6, "type"

    invoke-virtual {v2, v6}, Landroid/drm/DrmInfo;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    .line 1136
    .local v5, "tempInfo":Ljava/lang/Object;
    if-eqz v5, :cond_0

    .line 1138
    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 1152
    :cond_0
    sget-boolean v6, Lcom/samsung/musicplus/library/drm/DrmManager;->FLAG_SUPPORT_OMA_FL_ONLY:Z

    if-eqz v6, :cond_2

    .line 1166
    if-eq v0, v8, :cond_1

    if-ne v0, v7, :cond_2

    .line 1168
    :cond_1
    iput v7, v3, Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;->type:I

    .line 1170
    const/16 v6, 0xb

    iput v6, v3, Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;->text1:I

    .line 1208
    .end local v5    # "tempInfo":Ljava/lang/Object;
    :goto_0
    return-object v3

    .line 1142
    :catch_0
    move-exception v1

    .line 1144
    .local v1, "e":Ljava/lang/NullPointerException;
    const/16 v6, 0x17

    iput v6, v3, Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;->type:I

    .line 1146
    iput v7, v3, Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;->text1:I

    goto :goto_0

    .line 1178
    .end local v1    # "e":Ljava/lang/NullPointerException;
    .restart local v5    # "tempInfo":Ljava/lang/Object;
    :cond_2
    if-eq v0, v8, :cond_3

    if-ne v0, v7, :cond_5

    :cond_3
    if-eq p1, v7, :cond_4

    if-ne p1, v8, :cond_5

    .line 1182
    :cond_4
    invoke-virtual {p0, p2}, Lcom/samsung/musicplus/library/drm/DrmManager;->getUrlInfo(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iput-object v6, v3, Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;->url:Ljava/lang/String;

    .line 1184
    iput v9, v3, Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;->type:I

    .line 1186
    const/4 v6, 0x1

    iput v6, v3, Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;->text1:I

    goto :goto_0

    .line 1190
    :cond_5
    if-eqz p3, :cond_6

    .line 1192
    const/16 v6, 0xc

    iput v6, v3, Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;->type:I

    .line 1194
    iput v7, v3, Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;->text1:I

    .line 1196
    iput v8, v3, Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;->text2:I

    goto :goto_0

    .line 1200
    :cond_6
    const/16 v6, 0xd

    iput v6, v3, Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;->type:I

    .line 1202
    iput v7, v3, Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;->text1:I

    goto :goto_0
.end method

.method private getInvalidPlayReadyDrmPopup(ILjava/lang/String;)Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;
    .locals 7
    .param p1, "status"    # I
    .param p2, "path"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    const/16 v5, 0x16

    const/4 v4, 0x2

    .line 1214
    const-string v1, "MusicDrm"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getInvalidPlayReadyDrmPopup path : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " status : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1216
    new-instance v0, Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;

    invoke-direct {v0}, Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;-><init>()V

    .line 1218
    .local v0, "info":Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;
    sget-boolean v1, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_NOT_SUPPORT_PLAYREADY_DRM:Z

    if-eqz v1, :cond_0

    .line 1220
    iput v5, v0, Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;->type:I

    .line 1222
    const/16 v1, 0xb

    iput v1, v0, Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;->text1:I

    .line 1297
    :goto_0
    return-object v0

    .line 1228
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 1291
    iput v6, v0, Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;->type:I

    goto :goto_0

    .line 1232
    :pswitch_0
    iput v4, v0, Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;->type:I

    .line 1234
    iput v4, v0, Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;->text1:I

    goto :goto_0

    .line 1242
    :pswitch_1
    iget-object v1, p0, Lcom/samsung/musicplus/library/drm/DrmManager;->mContext:Landroid/content/Context;

    invoke-direct {p0, v1}, Lcom/samsung/musicplus/library/drm/DrmManager;->isNetworkConnected(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1244
    const/16 v1, 0x18

    iput v1, v0, Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;->type:I

    .line 1246
    const/4 v1, 0x5

    iput v1, v0, Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;->text1:I

    .line 1248
    iput v6, p0, Lcom/samsung/musicplus/library/drm/DrmManager;->mAcquireTry:I

    .line 1250
    iget-object v1, p0, Lcom/samsung/musicplus/library/drm/DrmManager;->mOnPlayReadyListener:Lcom/samsung/musicplus/library/drm/DrmManager$OnPlayReadyListener;

    if-eqz v1, :cond_1

    .line 1252
    iget-object v1, p0, Lcom/samsung/musicplus/library/drm/DrmManager;->mOnPlayReadyListener:Lcom/samsung/musicplus/library/drm/DrmManager$OnPlayReadyListener;

    const/4 v2, 0x1

    invoke-interface {v1, p2, v2}, Lcom/samsung/musicplus/library/drm/DrmManager$OnPlayReadyListener;->onAcquireStatus(Ljava/lang/String;I)V

    .line 1258
    :cond_1
    new-instance v1, Lcom/samsung/musicplus/library/drm/DrmManager$1;

    invoke-direct {v1, p0, p2}, Lcom/samsung/musicplus/library/drm/DrmManager$1;-><init>(Lcom/samsung/musicplus/library/drm/DrmManager;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/samsung/musicplus/library/drm/DrmManager$1;->start()V

    goto :goto_0

    .line 1271
    :cond_2
    if-ne p1, v4, :cond_3

    .line 1273
    iput v5, v0, Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;->type:I

    .line 1275
    const/16 v1, 0xa

    iput v1, v0, Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;->text1:I

    goto :goto_0

    .line 1279
    :cond_3
    iput v5, v0, Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;->type:I

    .line 1281
    const/16 v1, 0x9

    iput v1, v0, Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;->text1:I

    goto :goto_0

    .line 1228
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private getLgtDrmPopup(Ljava/lang/String;)Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;
    .locals 2
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 615
    iget-object v1, p0, Lcom/samsung/musicplus/library/drm/DrmManager;->mLgDrm:Lcom/samsung/musicplus/library/drm/LgtDrmManager;

    invoke-virtual {v1, p1}, Lcom/samsung/musicplus/library/drm/LgtDrmManager;->isValid(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 619
    iget-object v1, p0, Lcom/samsung/musicplus/library/drm/DrmManager;->mLgDrm:Lcom/samsung/musicplus/library/drm/LgtDrmManager;

    invoke-virtual {v1}, Lcom/samsung/musicplus/library/drm/LgtDrmManager;->getInvalidDrmPopupInfo()Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;

    move-result-object v0

    .line 621
    .local v0, "info":Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;
    iput-object p1, v0, Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;->path:Ljava/lang/String;

    .line 631
    :goto_0
    return-object v0

    .line 625
    .end local v0    # "info":Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;
    :cond_0
    new-instance v0, Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;

    invoke-direct {v0}, Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;-><init>()V

    .line 627
    .restart local v0    # "info":Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;
    const/4 v1, 0x0

    iput v1, v0, Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;->type:I

    goto :goto_0
.end method

.method private getOmaDetailInfo(Ljava/lang/String;)Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo;
    .locals 8
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x1

    .line 1767
    const/4 v3, 0x0

    .line 1769
    .local v3, "info":Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo;
    iget-object v6, p0, Lcom/samsung/musicplus/library/drm/DrmManager;->mClient:Landroid/drm/DrmManagerClient;

    invoke-virtual {v6, p1, v7}, Landroid/drm/DrmManagerClient;->getConstraints(Ljava/lang/String;I)Landroid/content/ContentValues;

    move-result-object v0

    .line 1771
    .local v0, "constraint":Landroid/content/ContentValues;
    invoke-direct {p0, v0}, Lcom/samsung/musicplus/library/drm/DrmManager;->getCategory(Landroid/content/ContentValues;)I

    move-result v6

    packed-switch v6, :pswitch_data_0

    .line 1863
    :pswitch_0
    const/4 v3, 0x0

    .line 1869
    :goto_0
    return-object v3

    .line 1775
    :pswitch_1
    new-instance v3, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo;

    .end local v3    # "info":Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo;
    invoke-direct {v3}, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo;-><init>()V

    .line 1777
    .restart local v3    # "info":Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo;
    const/4 v6, 0x0

    iput v6, v3, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo;->ui:I

    goto :goto_0

    .line 1783
    :pswitch_2
    new-instance v1, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$Count;

    invoke-direct {v1}, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$Count;-><init>()V

    .line 1785
    .local v1, "count":Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$Count;
    iput v7, v1, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$Count;->ui:I

    .line 1787
    const-string v6, "remaining_repeat_count"

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v1, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$Count;->remain:Ljava/lang/String;

    .line 1791
    const-string v6, "max_repeat_count"

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v1, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$Count;->max:Ljava/lang/String;

    .line 1793
    move-object v3, v1

    .line 1795
    goto :goto_0

    .line 1799
    .end local v1    # "count":Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$Count;
    :pswitch_3
    new-instance v2, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$DateTime;

    invoke-direct {v2}, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$DateTime;-><init>()V

    .line 1801
    .local v2, "date":Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$DateTime;
    const/4 v6, 0x2

    iput v6, v2, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$DateTime;->ui:I

    .line 1805
    const-string v6, "license_start_time"

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v2, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$DateTime;->start:Ljava/lang/String;

    .line 1809
    const-string v6, "license_expiry_time"

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v2, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$DateTime;->expiry:Ljava/lang/String;

    .line 1813
    move-object v3, v2

    .line 1815
    goto :goto_0

    .line 1819
    .end local v2    # "date":Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$DateTime;
    :pswitch_4
    new-instance v4, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$Interval;

    invoke-direct {v4}, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$Interval;-><init>()V

    .line 1821
    .local v4, "interval":Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$Interval;
    const/4 v6, 0x3

    iput v6, v4, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$Interval;->ui:I

    .line 1825
    const-string v6, "license_original_interval"

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v4, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$Interval;->original:Ljava/lang/String;

    .line 1827
    const-string v6, "license_available_time"

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v4, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$Interval;->available:Ljava/lang/String;

    .line 1831
    move-object v3, v4

    .line 1833
    goto :goto_0

    .line 1837
    .end local v4    # "interval":Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$Interval;
    :pswitch_5
    new-instance v5, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$TimedCount;

    invoke-direct {v5}, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$TimedCount;-><init>()V

    .line 1839
    .local v5, "timedCount":Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$TimedCount;
    const/4 v6, 0x4

    iput v6, v5, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$TimedCount;->ui:I

    .line 1843
    const-string v6, "license_start_time"

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$TimedCount;->start:Ljava/lang/String;

    .line 1847
    const-string v6, "license_available_time"

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$TimedCount;->available:Ljava/lang/String;

    .line 1851
    const-string v6, "remaining_repeat_count"

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$TimedCount;->remain:Ljava/lang/String;

    .line 1855
    const-string v6, "max_repeat_count"

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$TimedCount;->max:Ljava/lang/String;

    goto/16 :goto_0

    .line 1771
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
    .end packed-switch
.end method

.method private getOmaDrmPopup(ILjava/lang/String;Z)Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;
    .locals 3
    .param p1, "status"    # I
    .param p2, "path"    # Ljava/lang/String;
    .param p3, "fromList"    # Z

    .prologue
    .line 663
    new-instance v1, Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;

    invoke-direct {v1}, Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;-><init>()V

    .line 665
    .local v1, "info":Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;
    const/4 v2, 0x0

    iput v2, v1, Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;->type:I

    .line 667
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/library/drm/DrmManager;->isValidInternal(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 671
    invoke-direct {p0, p2}, Lcom/samsung/musicplus/library/drm/DrmManager;->getRemainCounts(Ljava/lang/String;)I

    move-result v0

    .line 673
    .local v0, "count":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    const/4 v2, 0x2

    if-gt v0, v2, :cond_0

    .line 675
    const/16 v2, 0xb

    iput v2, v1, Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;->type:I

    .line 677
    const/4 v2, 0x4

    iput v2, v1, Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;->text1:I

    .line 679
    iput v0, v1, Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;->count:I

    .line 681
    iput-object p2, v1, Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;->path:Ljava/lang/String;

    .line 695
    .end local v0    # "count":I
    :cond_0
    :goto_0
    return-object v1

    .line 689
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/musicplus/library/drm/DrmManager;->getInvalidOmaDrmPopup(ILjava/lang/String;Z)Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;

    move-result-object v1

    .line 691
    iput-object p2, v1, Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;->path:Ljava/lang/String;

    goto :goto_0
.end method

.method private getPlayRaedyPopup(Ljava/lang/String;I)Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;
    .locals 4
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "status"    # I

    .prologue
    .line 637
    const-string v1, "MusicDrm"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getPlayRaedyPopup path : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " status : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 641
    invoke-direct {p0, p2}, Lcom/samsung/musicplus/library/drm/DrmManager;->isValidInternal(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 645
    invoke-direct {p0, p2, p1}, Lcom/samsung/musicplus/library/drm/DrmManager;->getInvalidPlayReadyDrmPopup(ILjava/lang/String;)Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;

    move-result-object v0

    .line 647
    .local v0, "info":Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;
    iput-object p1, v0, Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;->path:Ljava/lang/String;

    .line 657
    :goto_0
    return-object v0

    .line 651
    .end local v0    # "info":Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;
    :cond_0
    new-instance v0, Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;

    invoke-direct {v0}, Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;-><init>()V

    .line 653
    .restart local v0    # "info":Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;
    const/4 v1, 0x0

    iput v1, v0, Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;->type:I

    goto :goto_0
.end method

.method private getPlayReadyDetailInfo(Ljava/lang/String;)Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo;
    .locals 10
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    const/4 v9, 0x5

    const/4 v8, 0x1

    const/4 v7, 0x7

    .line 1875
    const/4 v3, 0x0

    .line 1877
    .local v3, "info":Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo;
    iget-object v6, p0, Lcom/samsung/musicplus/library/drm/DrmManager;->mClient:Landroid/drm/DrmManagerClient;

    invoke-virtual {v6, p1, v8}, Landroid/drm/DrmManagerClient;->getConstraints(Ljava/lang/String;I)Landroid/content/ContentValues;

    move-result-object v0

    .line 1879
    .local v0, "constraint":Landroid/content/ContentValues;
    invoke-direct {p0, v0}, Lcom/samsung/musicplus/library/drm/DrmManager;->getCategory(Landroid/content/ContentValues;)I

    move-result v6

    packed-switch v6, :pswitch_data_0

    .line 2053
    const/4 v3, 0x0

    .line 2057
    :goto_0
    return-object v3

    .line 1883
    :pswitch_0
    new-instance v3, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo;

    .end local v3    # "info":Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo;
    invoke-direct {v3}, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo;-><init>()V

    .line 1885
    .restart local v3    # "info":Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo;
    const/16 v6, 0x9

    iput v6, v3, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo;->ui:I

    goto :goto_0

    .line 1891
    :pswitch_1
    new-instance v3, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo;

    .end local v3    # "info":Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo;
    invoke-direct {v3}, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo;-><init>()V

    .line 1893
    .restart local v3    # "info":Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo;
    const/4 v6, 0x0

    iput v6, v3, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo;->ui:I

    goto :goto_0

    .line 1899
    :pswitch_2
    new-instance v1, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$Count;

    invoke-direct {v1}, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$Count;-><init>()V

    .line 1901
    .local v1, "count":Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$Count;
    iput v8, v1, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$Count;->ui:I

    .line 1903
    const-string v6, "remaining_repeat_count"

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v1, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$Count;->remain:Ljava/lang/String;

    .line 1907
    const-string v6, "max_repeat_count"

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v1, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$Count;->max:Ljava/lang/String;

    goto :goto_0

    .line 1913
    .end local v1    # "count":Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$Count;
    :pswitch_3
    new-instance v2, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$DateTime;

    invoke-direct {v2}, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$DateTime;-><init>()V

    .line 1915
    .local v2, "date":Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$DateTime;
    iput v9, v2, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$DateTime;->ui:I

    .line 1917
    const-string v6, "license_start_time"

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v2, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$DateTime;->start:Ljava/lang/String;

    .line 1921
    move-object v3, v2

    .line 1923
    goto :goto_0

    .line 1927
    .end local v2    # "date":Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$DateTime;
    :pswitch_4
    new-instance v2, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$DateTime;

    invoke-direct {v2}, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$DateTime;-><init>()V

    .line 1929
    .restart local v2    # "date":Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$DateTime;
    iput v9, v2, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$DateTime;->ui:I

    .line 1931
    const-string v6, "license_expiry_time"

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v2, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$DateTime;->expiry:Ljava/lang/String;

    .line 1935
    move-object v3, v2

    .line 1937
    goto :goto_0

    .line 1941
    .end local v2    # "date":Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$DateTime;
    :pswitch_5
    new-instance v2, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$DateTime;

    invoke-direct {v2}, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$DateTime;-><init>()V

    .line 1943
    .restart local v2    # "date":Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$DateTime;
    const/4 v6, 0x2

    iput v6, v2, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$DateTime;->ui:I

    .line 1945
    const-string v6, "license_start_time"

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v2, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$DateTime;->start:Ljava/lang/String;

    .line 1949
    const-string v6, "license_expiry_time"

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v2, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$DateTime;->expiry:Ljava/lang/String;

    .line 1953
    move-object v3, v2

    .line 1955
    goto :goto_0

    .line 1959
    .end local v2    # "date":Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$DateTime;
    :pswitch_6
    new-instance v5, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$TimedCount;

    invoke-direct {v5}, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$TimedCount;-><init>()V

    .line 1961
    .local v5, "timedCount":Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$TimedCount;
    iput v7, v5, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$TimedCount;->ui:I

    .line 1963
    const-string v6, "license_start_time"

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$TimedCount;->start:Ljava/lang/String;

    .line 1967
    const-string v6, "remaining_repeat_count"

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$TimedCount;->remain:Ljava/lang/String;

    .line 1971
    const-string v6, "max_repeat_count"

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$TimedCount;->max:Ljava/lang/String;

    .line 1975
    move-object v3, v5

    .line 1977
    goto/16 :goto_0

    .line 1981
    .end local v5    # "timedCount":Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$TimedCount;
    :pswitch_7
    new-instance v5, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$TimedCount;

    invoke-direct {v5}, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$TimedCount;-><init>()V

    .line 1983
    .restart local v5    # "timedCount":Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$TimedCount;
    iput v7, v5, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$TimedCount;->ui:I

    .line 1985
    const-string v6, "license_expiry_time"

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$TimedCount;->expiry:Ljava/lang/String;

    .line 1989
    const-string v6, "remaining_repeat_count"

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$TimedCount;->remain:Ljava/lang/String;

    .line 1993
    const-string v6, "max_repeat_count"

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$TimedCount;->max:Ljava/lang/String;

    .line 1997
    move-object v3, v5

    .line 1999
    goto/16 :goto_0

    .line 2003
    .end local v5    # "timedCount":Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$TimedCount;
    :pswitch_8
    new-instance v5, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$TimedCount;

    invoke-direct {v5}, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$TimedCount;-><init>()V

    .line 2005
    .restart local v5    # "timedCount":Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$TimedCount;
    iput v7, v5, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$TimedCount;->ui:I

    .line 2007
    const-string v6, "license_start_time"

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$TimedCount;->start:Ljava/lang/String;

    .line 2011
    const-string v6, "license_expiry_time"

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$TimedCount;->expiry:Ljava/lang/String;

    .line 2015
    const-string v6, "remaining_repeat_count"

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$TimedCount;->remain:Ljava/lang/String;

    .line 2019
    const-string v6, "max_repeat_count"

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v5, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$TimedCount;->max:Ljava/lang/String;

    .line 2023
    move-object v3, v5

    .line 2025
    goto/16 :goto_0

    .line 2029
    .end local v5    # "timedCount":Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$TimedCount;
    :pswitch_9
    new-instance v4, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$Interval;

    invoke-direct {v4}, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$Interval;-><init>()V

    .line 2031
    .local v4, "interval":Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$Interval;
    const/4 v6, 0x3

    iput v6, v4, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$Interval;->ui:I

    .line 2033
    const-string v6, "license_original_interval"

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v4, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$Interval;->original:Ljava/lang/String;

    .line 2035
    const-string v6, "license_available_time"

    invoke-virtual {v0, v6}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v4, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$Interval;->available:Ljava/lang/String;

    .line 2039
    move-object v3, v4

    .line 2041
    goto/16 :goto_0

    .line 2045
    .end local v4    # "interval":Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$Interval;
    :pswitch_a
    new-instance v3, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo;

    .end local v3    # "info":Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo;
    invoke-direct {v3}, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo;-><init>()V

    .line 2047
    .restart local v3    # "info":Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo;
    const/16 v6, 0x8

    iput v6, v3, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo;->ui:I

    goto/16 :goto_0

    .line 1879
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method private getRemainCounts(Ljava/lang/String;)I
    .locals 5
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    const/4 v4, 0x1

    const/4 v2, -0x1

    .line 1442
    iget-object v3, p0, Lcom/samsung/musicplus/library/drm/DrmManager;->mClient:Landroid/drm/DrmManagerClient;

    invoke-virtual {v3, p1, v4}, Landroid/drm/DrmManagerClient;->getConstraints(Ljava/lang/String;I)Landroid/content/ContentValues;

    move-result-object v0

    .line 1444
    .local v0, "constraint":Landroid/content/ContentValues;
    invoke-direct {p0, v0}, Lcom/samsung/musicplus/library/drm/DrmManager;->getCategory(Landroid/content/ContentValues;)I

    move-result v3

    if-ne v3, v4, :cond_0

    .line 1446
    const-string v3, "remaining_repeat_count"

    invoke-virtual {v0, v3}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    .line 1448
    .local v1, "remain":Ljava/lang/Object;
    if-nez v1, :cond_1

    .line 1460
    .end local v1    # "remain":Ljava/lang/Object;
    :cond_0
    :goto_0
    return v2

    .line 1454
    .restart local v1    # "remain":Ljava/lang/Object;
    :cond_1
    const-string v2, "MusicDrm"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getRemainedCounts() : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1456
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    goto :goto_0
.end method

.method private static isForwardLockOnly()Z
    .locals 3

    .prologue
    .line 1098
    const/4 v0, 0x1

    .line 1108
    .local v0, "forwardLockOnly":Z
    new-instance v1, Ljava/io/File;

    const-string v2, "/system/lib/libomafldrm.so"

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 1110
    .local v1, "omaFlDrmPath":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    .line 1112
    const/4 v0, 0x0

    .line 1116
    :cond_0
    return v0
.end method

.method private isNetworkConnected(Landroid/content/Context;)Z
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1357
    const/4 v0, 0x1

    .line 1359
    .local v0, "isConnected":Z
    const-string v2, "phone"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    .line 1363
    .local v1, "tm":Landroid/telephony/TelephonyManager;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/telephony/TelephonyManager;->getDataState()I

    move-result v2

    const/4 v3, 0x2

    if-eq v2, v3, :cond_0

    invoke-direct {p0, p1}, Lcom/samsung/musicplus/library/drm/DrmManager;->isWifiEnabled(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1367
    const/4 v0, 0x0

    .line 1371
    :cond_0
    const-string v2, "MusicDrm"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isNetworkConnected "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1373
    return v0
.end method

.method private isValidInternal(I)Z
    .locals 1
    .param p1, "status"    # I

    .prologue
    .line 396
    const/4 v0, 0x1

    .line 398
    .local v0, "isValid":Z
    packed-switch p1, :pswitch_data_0

    .line 416
    :goto_0
    return v0

    .line 406
    :pswitch_0
    const/4 v0, 0x0

    .line 408
    goto :goto_0

    .line 398
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private isWifiEnabled(Landroid/content/Context;)Z
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1379
    const/4 v1, 0x1

    .line 1381
    .local v1, "isEnable":Z
    const-string v3, "wifi"

    invoke-virtual {p1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/wifi/WifiManager;

    .line 1383
    .local v2, "wm":Landroid/net/wifi/WifiManager;
    invoke-virtual {v2}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v3

    if-nez v3, :cond_1

    .line 1385
    const/4 v1, 0x0

    .line 1401
    :cond_0
    :goto_0
    const-string v3, "MusicDrm"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "isWifiEnabled "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1403
    return v1

    .line 1389
    :cond_1
    invoke-virtual {v2}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v0

    .line 1391
    .local v0, "info":Landroid/net/wifi/WifiInfo;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getIpAddress()I

    move-result v3

    if-nez v3, :cond_0

    .line 1395
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getDrmDetailInfo(Ljava/lang/String;)Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo;
    .locals 4
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 1723
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/library/drm/DrmManager;->getDrmType(Ljava/lang/String;)I

    move-result v2

    .line 1725
    .local v2, "type":I
    const/4 v1, 0x0

    .line 1727
    .local v1, "info":Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo;
    packed-switch v2, :pswitch_data_0

    .line 1755
    const/4 v1, 0x0

    .line 1761
    :goto_0
    return-object v1

    .line 1731
    :pswitch_0
    new-instance v0, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$DateTime;

    invoke-direct {v0}, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$DateTime;-><init>()V

    .line 1733
    .local v0, "date":Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$DateTime;
    iget-object v3, p0, Lcom/samsung/musicplus/library/drm/DrmManager;->mLgDrm:Lcom/samsung/musicplus/library/drm/LgtDrmManager;

    invoke-virtual {v3, p1}, Lcom/samsung/musicplus/library/drm/LgtDrmManager;->getExpireDate(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$DateTime;->expiry:Ljava/lang/String;

    .line 1735
    move-object v1, v0

    .line 1737
    goto :goto_0

    .line 1741
    .end local v0    # "date":Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo$DateTime;
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/library/drm/DrmManager;->getOmaDetailInfo(Ljava/lang/String;)Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo;

    move-result-object v1

    .line 1743
    goto :goto_0

    .line 1747
    :pswitch_2
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/library/drm/DrmManager;->getPlayReadyDetailInfo(Ljava/lang/String;)Lcom/samsung/musicplus/library/drm/DrmManager$DrmDetailInfo;

    move-result-object v1

    .line 1749
    goto :goto_0

    .line 1727
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method

.method public getDrmInfo(Ljava/lang/String;Z)Landroid/os/Bundle;
    .locals 4
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "fromList"    # Z

    .prologue
    .line 578
    invoke-virtual {p0, p1, p2}, Lcom/samsung/musicplus/library/drm/DrmManager;->getDrmPopupInfo(Ljava/lang/String;Z)Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;

    move-result-object v1

    .line 580
    .local v1, "info":Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 582
    .local v0, "data":Landroid/os/Bundle;
    const-string v2, "type"

    iget v3, v1, Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;->type:I

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 584
    const-string v2, "text1"

    iget v3, v1, Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;->text1:I

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 586
    const-string v2, "text2"

    iget v3, v1, Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;->text2:I

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 588
    const-string v2, "count"

    iget v3, v1, Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;->count:I

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 590
    const-string v2, "url"

    iget-object v3, v1, Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;->url:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 592
    const-string v2, "path"

    iget-object v3, v1, Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;->path:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 605
    return-object v0
.end method

.method public getDrmPopupInfo(Ljava/lang/String;Z)Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;
    .locals 8
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "fromList"    # Z

    .prologue
    const/4 v7, 0x0

    .line 450
    const-string v4, "MusicDrm"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getDrmPopupInfo path : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " fromList : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 452
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/library/drm/DrmManager;->isDrm(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 454
    new-instance v1, Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;

    invoke-direct {v1}, Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;-><init>()V

    .line 456
    .local v1, "info":Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;
    iput v7, v1, Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;->type:I

    move-object v2, v1

    .line 504
    .end local v1    # "info":Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;
    .local v2, "info":Ljava/lang/Object;
    :goto_0
    return-object v2

    .line 464
    .end local v2    # "info":Ljava/lang/Object;
    :cond_0
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/library/drm/DrmManager;->getDrmType(Ljava/lang/String;)I

    move-result v0

    .line 466
    .local v0, "drmType":I
    packed-switch v0, :pswitch_data_0

    .line 494
    new-instance v1, Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;

    invoke-direct {v1}, Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;-><init>()V

    .line 496
    .restart local v1    # "info":Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;
    iput v7, v1, Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;->type:I

    .line 498
    iput-object p1, v1, Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;->path:Ljava/lang/String;

    :goto_1
    move-object v2, v1

    .line 504
    .restart local v2    # "info":Ljava/lang/Object;
    goto :goto_0

    .line 470
    .end local v1    # "info":Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;
    .end local v2    # "info":Ljava/lang/Object;
    :pswitch_0
    iget-object v4, p0, Lcom/samsung/musicplus/library/drm/DrmManager;->mClient:Landroid/drm/DrmManagerClient;

    invoke-virtual {v4, p1}, Landroid/drm/DrmManagerClient;->checkRightsStatus(Ljava/lang/String;)I

    move-result v3

    .line 472
    .local v3, "status":I
    invoke-direct {p0, p1, v3}, Lcom/samsung/musicplus/library/drm/DrmManager;->getPlayRaedyPopup(Ljava/lang/String;I)Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;

    move-result-object v1

    .line 474
    .restart local v1    # "info":Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;
    goto :goto_1

    .line 478
    .end local v1    # "info":Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;
    .end local v3    # "status":I
    :pswitch_1
    iget-object v4, p0, Lcom/samsung/musicplus/library/drm/DrmManager;->mClient:Landroid/drm/DrmManagerClient;

    invoke-virtual {v4, p1}, Landroid/drm/DrmManagerClient;->checkRightsStatus(Ljava/lang/String;)I

    move-result v3

    .line 480
    .restart local v3    # "status":I
    invoke-direct {p0, v3, p1, p2}, Lcom/samsung/musicplus/library/drm/DrmManager;->getOmaDrmPopup(ILjava/lang/String;Z)Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;

    move-result-object v1

    .line 482
    .restart local v1    # "info":Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;
    goto :goto_1

    .line 486
    .end local v1    # "info":Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;
    .end local v3    # "status":I
    :pswitch_2
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/library/drm/DrmManager;->getLgtDrmPopup(Ljava/lang/String;)Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;

    move-result-object v1

    .line 488
    .restart local v1    # "info":Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;
    goto :goto_1

    .line 466
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public getOptionInfo(Ljava/lang/String;)Lcom/samsung/musicplus/library/drm/DrmManager$OptionInfo;
    .locals 7
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 2140
    new-instance v2, Landroid/drm/DrmInfoRequest;

    const/16 v4, 0x10

    const-string v5, "application/vnd.oma.drm.content"

    invoke-direct {v2, v4, v5}, Landroid/drm/DrmInfoRequest;-><init>(ILjava/lang/String;)V

    .line 2142
    .local v2, "request":Landroid/drm/DrmInfoRequest;
    const-string v4, "drm_path"

    invoke-virtual {v2, v4, p1}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 2144
    iget-object v4, p0, Lcom/samsung/musicplus/library/drm/DrmManager;->mClient:Landroid/drm/DrmManagerClient;

    invoke-virtual {v4, v2}, Landroid/drm/DrmManagerClient;->acquireDrmInfo(Landroid/drm/DrmInfoRequest;)Landroid/drm/DrmInfo;

    move-result-object v0

    .line 2146
    .local v0, "info":Landroid/drm/DrmInfo;
    new-instance v1, Lcom/samsung/musicplus/library/drm/DrmManager$OptionInfo;

    invoke-direct {v1}, Lcom/samsung/musicplus/library/drm/DrmManager$OptionInfo;-><init>()V

    .line 2148
    .local v1, "option":Lcom/samsung/musicplus/library/drm/DrmManager$OptionInfo;
    if-eqz v0, :cond_0

    .line 2150
    const-string v4, "status"

    invoke-virtual {v0, v4}, Landroid/drm/DrmInfo;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2152
    .local v3, "status":Ljava/lang/String;
    const-string v4, "MusicDrm"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "status req1: TYPE_GET_OPTION_MENU"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/musicplus/library/iLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 2154
    const-string v4, "fail"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 2156
    const-string v4, "bRingtone"

    invoke-virtual {v0, v4}, Landroid/drm/DrmInfo;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "1"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    iput-boolean v4, v1, Lcom/samsung/musicplus/library/drm/DrmManager$OptionInfo;->ringtone:Z

    .line 2158
    const-string v4, "bSendAs"

    invoke-virtual {v0, v4}, Landroid/drm/DrmInfo;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    const-string v5, "1"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    iput-boolean v4, v1, Lcom/samsung/musicplus/library/drm/DrmManager$OptionInfo;->share:Z

    .line 2164
    .end local v3    # "status":Ljava/lang/String;
    :cond_0
    return-object v1
.end method

.method public getUrlInfo(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 1475
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/library/drm/DrmManager;->getDrmMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1477
    .local v1, "mime":Ljava/lang/String;
    new-instance v2, Landroid/drm/DrmInfoRequest;

    const/4 v3, 0x3

    invoke-direct {v2, v3, v1}, Landroid/drm/DrmInfoRequest;-><init>(ILjava/lang/String;)V

    .line 1481
    .local v2, "request":Landroid/drm/DrmInfoRequest;
    const-string v3, "drm_path"

    invoke-virtual {v2, v3, p1}, Landroid/drm/DrmInfoRequest;->put(Ljava/lang/String;Ljava/lang/Object;)V

    .line 1483
    iget-object v3, p0, Lcom/samsung/musicplus/library/drm/DrmManager;->mClient:Landroid/drm/DrmManagerClient;

    invoke-virtual {v3, v2}, Landroid/drm/DrmManagerClient;->acquireDrmInfo(Landroid/drm/DrmInfoRequest;)Landroid/drm/DrmInfo;

    move-result-object v0

    .line 1485
    .local v0, "drmInfo":Landroid/drm/DrmInfo;
    const-string v3, "URL"

    invoke-virtual {v0, v3}, Landroid/drm/DrmInfo;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    return-object v3
.end method

.method public isDrm(Ljava/lang/String;)Z
    .locals 3
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 340
    if-eqz p1, :cond_0

    const-string v2, ""

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 364
    :cond_0
    :goto_0
    return v1

    .line 346
    :cond_1
    iget-object v2, p0, Lcom/samsung/musicplus/library/drm/DrmManager;->mLgDrm:Lcom/samsung/musicplus/library/drm/LgtDrmManager;

    if-eqz v2, :cond_2

    .line 350
    iget-object v1, p0, Lcom/samsung/musicplus/library/drm/DrmManager;->mLgDrm:Lcom/samsung/musicplus/library/drm/LgtDrmManager;

    invoke-virtual {v1, p1}, Lcom/samsung/musicplus/library/drm/LgtDrmManager;->isDrm(Ljava/lang/String;)Z

    move-result v1

    goto :goto_0

    .line 354
    :cond_2
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/library/drm/DrmManager;->getDrmMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 356
    .local v0, "mime":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 364
    iget-object v1, p0, Lcom/samsung/musicplus/library/drm/DrmManager;->mClient:Landroid/drm/DrmManagerClient;

    invoke-virtual {v1, p1, v0}, Landroid/drm/DrmManagerClient;->canHandle(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    goto :goto_0
.end method

.method public isValid(Ljava/lang/String;)Z
    .locals 2
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 380
    iget-object v1, p0, Lcom/samsung/musicplus/library/drm/DrmManager;->mLgDrm:Lcom/samsung/musicplus/library/drm/LgtDrmManager;

    if-eqz v1, :cond_0

    .line 384
    iget-object v1, p0, Lcom/samsung/musicplus/library/drm/DrmManager;->mLgDrm:Lcom/samsung/musicplus/library/drm/LgtDrmManager;

    invoke-virtual {v1, p1}, Lcom/samsung/musicplus/library/drm/LgtDrmManager;->isValid(Ljava/lang/String;)Z

    move-result v1

    .line 390
    :goto_0
    return v1

    .line 388
    :cond_0
    iget-object v1, p0, Lcom/samsung/musicplus/library/drm/DrmManager;->mClient:Landroid/drm/DrmManagerClient;

    invoke-virtual {v1, p1}, Landroid/drm/DrmManagerClient;->checkRightsStatus(Ljava/lang/String;)I

    move-result v0

    .line 390
    .local v0, "status":I
    invoke-direct {p0, v0}, Lcom/samsung/musicplus/library/drm/DrmManager;->isValidInternal(I)Z

    move-result v1

    goto :goto_0
.end method

.method public isValidRingTone(Ljava/lang/String;)Z
    .locals 3
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 2205
    iget-object v1, p0, Lcom/samsung/musicplus/library/drm/DrmManager;->mClient:Landroid/drm/DrmManagerClient;

    const/4 v2, 0x2

    invoke-virtual {v1, p1, v2}, Landroid/drm/DrmManagerClient;->checkRightsStatus(Ljava/lang/String;I)I

    move-result v0

    .line 2207
    .local v0, "rightsStatus":I
    if-nez v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public release()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 311
    iget-object v0, p0, Lcom/samsung/musicplus/library/drm/DrmManager;->mLgDrm:Lcom/samsung/musicplus/library/drm/LgtDrmManager;

    if-eqz v0, :cond_0

    .line 313
    iget-object v0, p0, Lcom/samsung/musicplus/library/drm/DrmManager;->mLgDrm:Lcom/samsung/musicplus/library/drm/LgtDrmManager;

    invoke-virtual {v0}, Lcom/samsung/musicplus/library/drm/LgtDrmManager;->release()V

    .line 315
    iput-object v1, p0, Lcom/samsung/musicplus/library/drm/DrmManager;->mLgDrm:Lcom/samsung/musicplus/library/drm/LgtDrmManager;

    .line 319
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/library/drm/DrmManager;->mClient:Landroid/drm/DrmManagerClient;

    invoke-virtual {v0}, Landroid/drm/DrmManagerClient;->release()V

    .line 321
    iput-object v1, p0, Lcom/samsung/musicplus/library/drm/DrmManager;->mClient:Landroid/drm/DrmManagerClient;

    .line 323
    sput-object v1, Lcom/samsung/musicplus/library/drm/DrmManager;->sDrmManager:Lcom/samsung/musicplus/library/drm/DrmManager;

    .line 325
    return-void
.end method

.method public setPlayReadyListener(Lcom/samsung/musicplus/library/drm/DrmManager$OnPlayReadyListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/musicplus/library/drm/DrmManager$OnPlayReadyListener;

    .prologue
    .line 431
    iput-object p1, p0, Lcom/samsung/musicplus/library/drm/DrmManager;->mOnPlayReadyListener:Lcom/samsung/musicplus/library/drm/DrmManager$OnPlayReadyListener;

    .line 433
    return-void
.end method

.method public supportRingtone(Ljava/lang/String;)Z
    .locals 3
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 2179
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/library/drm/DrmManager;->getOptionInfo(Ljava/lang/String;)Lcom/samsung/musicplus/library/drm/DrmManager$OptionInfo;

    move-result-object v1

    .line 2181
    .local v1, "option":Lcom/samsung/musicplus/library/drm/DrmManager$OptionInfo;
    const/4 v0, 0x0

    .line 2183
    .local v0, "isSupport":Z
    iget-boolean v2, v1, Lcom/samsung/musicplus/library/drm/DrmManager$OptionInfo;->ringtone:Z

    if-eqz v2, :cond_0

    .line 2185
    const/4 v0, 0x1

    .line 2189
    :cond_0
    return v0
.end method
