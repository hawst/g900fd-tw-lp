.class public Lcom/samsung/musicplus/library/sdk/BezelManager;
.super Ljava/lang/Object;
.source "BezelManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/library/sdk/BezelManager$OnBezelItemListener;
    }
.end annotation


# instance fields
.field private mBezelManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

.field private mItemListener:Lcom/samsung/musicplus/library/sdk/BezelManager$OnBezelItemListener;

.field private mPrivateListener:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 105
    const-string v0, "quickconnect"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/quickconnect/QuickConnectManager;

    iput-object v0, p0, Lcom/samsung/musicplus/library/sdk/BezelManager;->mBezelManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    .line 109
    new-instance v0, Lcom/samsung/musicplus/library/sdk/BezelManager$1;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/library/sdk/BezelManager$1;-><init>(Lcom/samsung/musicplus/library/sdk/BezelManager;)V

    iput-object v0, p0, Lcom/samsung/musicplus/library/sdk/BezelManager;->mPrivateListener:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;

    .line 127
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/library/sdk/BezelManager;)Lcom/samsung/musicplus/library/sdk/BezelManager$OnBezelItemListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/library/sdk/BezelManager;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/samsung/musicplus/library/sdk/BezelManager;->mItemListener:Lcom/samsung/musicplus/library/sdk/BezelManager$OnBezelItemListener;

    return-object v0
.end method


# virtual methods
.method public register(Lcom/samsung/musicplus/library/sdk/BezelManager$OnBezelItemListener;)V
    .locals 2
    .param p1, "listener"    # Lcom/samsung/musicplus/library/sdk/BezelManager$OnBezelItemListener;

    .prologue
    .line 147
    iput-object p1, p0, Lcom/samsung/musicplus/library/sdk/BezelManager;->mItemListener:Lcom/samsung/musicplus/library/sdk/BezelManager$OnBezelItemListener;

    .line 149
    iget-object v0, p0, Lcom/samsung/musicplus/library/sdk/BezelManager;->mBezelManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    iget-object v1, p0, Lcom/samsung/musicplus/library/sdk/BezelManager;->mPrivateListener:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/quickconnect/QuickConnectManager;->registerListener(Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;)V

    .line 151
    return-void
.end method

.method public unregister()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 167
    iput-object v2, p0, Lcom/samsung/musicplus/library/sdk/BezelManager;->mItemListener:Lcom/samsung/musicplus/library/sdk/BezelManager$OnBezelItemListener;

    .line 169
    iget-object v0, p0, Lcom/samsung/musicplus/library/sdk/BezelManager;->mBezelManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    iget-object v1, p0, Lcom/samsung/musicplus/library/sdk/BezelManager;->mPrivateListener:Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/quickconnect/QuickConnectManager;->unregisterListener(Lcom/samsung/android/quickconnect/QuickConnectManager$QuickConnectListener;)V

    .line 172
    iput-object v2, p0, Lcom/samsung/musicplus/library/sdk/BezelManager;->mBezelManager:Lcom/samsung/android/quickconnect/QuickConnectManager;

    .line 173
    return-void
.end method
