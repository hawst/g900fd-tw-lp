.class public Lcom/samsung/musicplus/library/hardware/CallStateChecker;
.super Ljava/lang/Object;
.source "CallStateChecker.java"


# static fields
.field private static final CLASSNAME:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    const-class v0, Lcom/samsung/musicplus/library/hardware/CallStateChecker;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/library/hardware/CallStateChecker;->CLASSNAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static isCallIdle(Landroid/content/Context;)Z
    .locals 6
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x1

    .line 66
    sget-boolean v3, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_MIRROR_CALL:Z

    if-eqz v3, :cond_0

    invoke-static {p0}, Lcom/samsung/musicplus/library/hardware/CallStateChecker;->isRmsConnected(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    sget-boolean v3, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_UWA_CALL:Z

    if-eqz v3, :cond_2

    invoke-static {p0}, Lcom/samsung/musicplus/library/hardware/CallStateChecker;->isUseSeeingCall(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 75
    :cond_1
    :goto_0
    return v2

    .line 71
    :cond_2
    invoke-static {}, Lcom/samsung/musicplus/library/hardware/CallStateChecker;->isVoipIdle()Z

    move-result v1

    .line 72
    .local v1, "isVoipIdle":Z
    invoke-static {p0}, Lcom/samsung/musicplus/library/hardware/CallStateChecker;->isCommunicationIdle(Landroid/content/Context;)Z

    move-result v0

    .line 73
    .local v0, "isCommIdle":Z
    sget-object v3, Lcom/samsung/musicplus/library/hardware/CallStateChecker;->CLASSNAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "isCallIdle() isVoipIdle : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " isCommIdle(Such like google talk..) : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 75
    if-eqz v1, :cond_3

    if-nez v0, :cond_1

    :cond_3
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private static isCommunicationIdle(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 104
    const-string v2, "audio"

    invoke-virtual {p0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    .line 105
    .local v0, "am":Landroid/media/AudioManager;
    const/4 v1, 0x0

    .line 106
    .local v1, "mode":I
    if-eqz v0, :cond_0

    .line 107
    invoke-virtual {v0}, Landroid/media/AudioManager;->getMode()I

    move-result v1

    .line 109
    :cond_0
    if-nez v1, :cond_1

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private static isRmsConnected(Landroid/content/Context;)Z
    .locals 10
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v9, 0x1

    .line 174
    const/4 v8, 0x0

    .line 175
    .local v8, "status":I
    const/4 v6, 0x0

    .line 177
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 178
    .local v0, "cr":Landroid/content/ContentResolver;
    if-eqz v0, :cond_0

    .line 179
    const-string v1, "content://com.lguplus.rms/service"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 181
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 182
    const-string v1, "connected"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v8

    .line 188
    :cond_0
    if-eqz v6, :cond_1

    .line 189
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 190
    const/4 v6, 0x0

    .line 193
    .end local v0    # "cr":Landroid/content/ContentResolver;
    :cond_1
    :goto_0
    if-ne v8, v9, :cond_3

    move v1, v9

    :goto_1
    return v1

    .line 185
    :catch_0
    move-exception v7

    .line 186
    .local v7, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v1, "RMS"

    invoke-static {v7}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 188
    if-eqz v6, :cond_1

    .line 189
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 190
    const/4 v6, 0x0

    goto :goto_0

    .line 188
    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    if-eqz v6, :cond_2

    .line 189
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 190
    const/4 v6, 0x0

    :cond_2
    throw v1

    .line 193
    :cond_3
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private static isUseSeeingCall(Landroid/content/Context;)Z
    .locals 11
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 203
    const/4 v6, 0x0

    .line 204
    .local v6, "cursor":Landroid/database/Cursor;
    const/4 v9, 0x0

    .line 205
    .local v9, "value":Ljava/lang/String;
    const/4 v8, 0x0

    .line 208
    .local v8, "ret":Z
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 209
    .local v0, "cr":Landroid/content/ContentResolver;
    if-eqz v0, :cond_1

    .line 210
    const-string v1, "content://com.uplus.ipagent.SettingsProvider/system"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "value"

    aput-object v4, v2, v3

    const-string v3, "name=?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v10, "setting_pluscall_active"

    aput-object v10, v4, v5

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 217
    if-eqz v6, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 219
    :cond_0
    const-string v1, "value"

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v6, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 220
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_0

    .line 226
    :cond_1
    if-eqz v6, :cond_2

    .line 227
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 228
    const/4 v6, 0x0

    .line 232
    .end local v0    # "cr":Landroid/content/ContentResolver;
    :cond_2
    :goto_0
    if-eqz v9, :cond_4

    const-string v1, "1"

    invoke-virtual {v9, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 233
    const/4 v8, 0x1

    .line 237
    :goto_1
    return v8

    .line 223
    :catch_0
    move-exception v7

    .line 224
    .local v7, "e":Ljava/lang/Exception;
    :try_start_1
    const-string v1, "UWA CALL Exception"

    invoke-static {v7}, Landroid/util/Log;->getStackTraceString(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 226
    if-eqz v6, :cond_2

    .line 227
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 228
    const/4 v6, 0x0

    goto :goto_0

    .line 226
    .end local v7    # "e":Ljava/lang/Exception;
    :catchall_0
    move-exception v1

    if-eqz v6, :cond_3

    .line 227
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 228
    const/4 v6, 0x0

    :cond_3
    throw v1

    .line 235
    :cond_4
    const/4 v8, 0x0

    goto :goto_1
.end method

.method private static isVoipIdle()Z
    .locals 1

    .prologue
    .line 79
    const/4 v0, 0x1

    .line 92
    .local v0, "isVoipIdle":Z
    return v0
.end method
