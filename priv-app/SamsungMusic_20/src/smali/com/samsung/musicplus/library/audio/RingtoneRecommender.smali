.class public Lcom/samsung/musicplus/library/audio/RingtoneRecommender;
.super Ljava/lang/Object;
.source "RingtoneRecommender.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/library/audio/RingtoneRecommender$OnHighlightResultListener;
    }
.end annotation


# static fields
.field private static final CLASSNAME:Ljava/lang/String;

.field private static final HIGHLIGHT_OFFSET:Ljava/lang/String; = "highlight_offset"

.field public static final OPEN_ERR_NOT_ENOUGH_MEMORY:I = -0x2

.field public static final OPEN_ERR_NOT_OPEN_FILE:I = -0x7

.field public static final OPEN_ERR_UNSUPPORT_FILE_TYPE:I = -0x3

.field public static final OPEN_SUCCESS:I = 0x0

.field public static final RESULT_EXTRACT:I = 0x5

.field public static final RESULT_QUIT:I = 0x6

.field public static final SMAT_MODE_FAST:I = 0x0

.field public static final SMAT_MODE_FULL:I = 0x1


# instance fields
.field private mIsOpen:Z

.field private mListener:Lcom/samsung/musicplus/library/audio/RingtoneRecommender$OnHighlightResultListener;

.field private mMode:I

.field private final mRecommender:Lcom/samsung/audio/Smat;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-class v0, Lcom/samsung/musicplus/library/audio/RingtoneRecommender;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/library/audio/RingtoneRecommender;->CLASSNAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 131
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91
    iput-boolean v0, p0, Lcom/samsung/musicplus/library/audio/RingtoneRecommender;->mIsOpen:Z

    .line 93
    iput v0, p0, Lcom/samsung/musicplus/library/audio/RingtoneRecommender;->mMode:I

    .line 132
    new-instance v0, Lcom/samsung/audio/Smat;

    invoke-direct {v0}, Lcom/samsung/audio/Smat;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/library/audio/RingtoneRecommender;->mRecommender:Lcom/samsung/audio/Smat;

    .line 133
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/library/audio/RingtoneRecommender;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/library/audio/RingtoneRecommender;

    .prologue
    .line 17
    iget-boolean v0, p0, Lcom/samsung/musicplus/library/audio/RingtoneRecommender;->mIsOpen:Z

    return v0
.end method

.method static synthetic access$100(Lcom/samsung/musicplus/library/audio/RingtoneRecommender;)Lcom/samsung/audio/Smat;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/library/audio/RingtoneRecommender;

    .prologue
    .line 17
    iget-object v0, p0, Lcom/samsung/musicplus/library/audio/RingtoneRecommender;->mRecommender:Lcom/samsung/audio/Smat;

    return-object v0
.end method

.method static synthetic access$200()Ljava/lang/String;
    .locals 1

    .prologue
    .line 17
    sget-object v0, Lcom/samsung/musicplus/library/audio/RingtoneRecommender;->CLASSNAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/musicplus/library/audio/RingtoneRecommender;)Lcom/samsung/musicplus/library/audio/RingtoneRecommender$OnHighlightResultListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/library/audio/RingtoneRecommender;

    .prologue
    .line 17
    iget-object v0, p0, Lcom/samsung/musicplus/library/audio/RingtoneRecommender;->mListener:Lcom/samsung/musicplus/library/audio/RingtoneRecommender$OnHighlightResultListener;

    return-object v0
.end method

.method private extract()Z
    .locals 4

    .prologue
    .line 237
    iget-object v1, p0, Lcom/samsung/musicplus/library/audio/RingtoneRecommender;->mRecommender:Lcom/samsung/audio/Smat;

    invoke-virtual {v1}, Lcom/samsung/audio/Smat;->extract()I

    move-result v0

    .line 238
    .local v0, "result":I
    sget-object v1, Lcom/samsung/musicplus/library/audio/RingtoneRecommender;->CLASSNAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "extract() result : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 239
    if-nez v0, :cond_0

    .line 240
    new-instance v1, Lcom/samsung/musicplus/library/audio/RingtoneRecommender$1;

    const-string v2, "Recommender thread"

    invoke-direct {v1, p0, v2}, Lcom/samsung/musicplus/library/audio/RingtoneRecommender$1;-><init>(Lcom/samsung/musicplus/library/audio/RingtoneRecommender;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/samsung/musicplus/library/audio/RingtoneRecommender$1;->start()V

    .line 281
    const/4 v1, 0x1

    .line 283
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getResultIntent(Landroid/net/Uri;I)Landroid/content/Intent;
    .locals 4
    .param p0, "uri"    # Landroid/net/Uri;
    .param p1, "offset"    # I

    .prologue
    .line 316
    sget-object v1, Lcom/samsung/musicplus/library/audio/RingtoneRecommender;->CLASSNAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getResultIntent() - uri: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " offset: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " msec"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 317
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 318
    .local v0, "intent":Landroid/content/Intent;
    if-lez p1, :cond_0

    .line 319
    invoke-virtual {p0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "highlight_offset"

    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object p0

    .line 322
    :cond_0
    invoke-virtual {v0, p0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 323
    return-object v0
.end method


# virtual methods
.method public close()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 219
    sget-object v1, Lcom/samsung/musicplus/library/audio/RingtoneRecommender;->CLASSNAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "close() is opened ? "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/samsung/musicplus/library/audio/RingtoneRecommender;->mIsOpen:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    iget-boolean v1, p0, Lcom/samsung/musicplus/library/audio/RingtoneRecommender;->mIsOpen:Z

    if-eqz v1, :cond_0

    .line 221
    iget-object v1, p0, Lcom/samsung/musicplus/library/audio/RingtoneRecommender;->mRecommender:Lcom/samsung/audio/Smat;

    invoke-virtual {v1}, Lcom/samsung/audio/Smat;->deinit()I

    move-result v1

    if-nez v1, :cond_0

    .line 222
    iput-boolean v0, p0, Lcom/samsung/musicplus/library/audio/RingtoneRecommender;->mIsOpen:Z

    .line 223
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/musicplus/library/audio/RingtoneRecommender;->mListener:Lcom/samsung/musicplus/library/audio/RingtoneRecommender$OnHighlightResultListener;

    .line 224
    const/4 v0, 0x1

    .line 227
    :cond_0
    return v0
.end method

.method public doExtract(Lcom/samsung/musicplus/library/audio/RingtoneRecommender$OnHighlightResultListener;)Z
    .locals 1
    .param p1, "listener"    # Lcom/samsung/musicplus/library/audio/RingtoneRecommender$OnHighlightResultListener;

    .prologue
    .line 205
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/library/audio/RingtoneRecommender;->mIsOpen:Z

    .line 206
    iput-object p1, p0, Lcom/samsung/musicplus/library/audio/RingtoneRecommender;->mListener:Lcom/samsung/musicplus/library/audio/RingtoneRecommender$OnHighlightResultListener;

    .line 207
    invoke-direct {p0}, Lcom/samsung/musicplus/library/audio/RingtoneRecommender;->extract()Z

    move-result v0

    return v0
.end method

.method public isOpen()Z
    .locals 3

    .prologue
    .line 144
    sget-object v0, Lcom/samsung/musicplus/library/audio/RingtoneRecommender;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isOpen() - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/samsung/musicplus/library/audio/RingtoneRecommender;->mIsOpen:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    iget-boolean v0, p0, Lcom/samsung/musicplus/library/audio/RingtoneRecommender;->mIsOpen:Z

    return v0
.end method

.method public open(Ljava/lang/String;)I
    .locals 1
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 166
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/samsung/musicplus/library/audio/RingtoneRecommender;->open(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public open(Ljava/lang/String;I)I
    .locals 4
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "mode"    # I

    .prologue
    .line 186
    iput p2, p0, Lcom/samsung/musicplus/library/audio/RingtoneRecommender;->mMode:I

    .line 187
    iget-object v1, p0, Lcom/samsung/musicplus/library/audio/RingtoneRecommender;->mRecommender:Lcom/samsung/audio/Smat;

    invoke-virtual {v1, p1, p2}, Lcom/samsung/audio/Smat;->init(Ljava/lang/String;I)I

    move-result v0

    .line 188
    .local v0, "result":I
    sget-object v1, Lcom/samsung/musicplus/library/audio/RingtoneRecommender;->CLASSNAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "open() result : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    return v0
.end method

.method public quit()Z
    .locals 3

    .prologue
    .line 296
    sget-object v0, Lcom/samsung/musicplus/library/audio/RingtoneRecommender;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "quit() is opened : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/samsung/musicplus/library/audio/RingtoneRecommender;->mIsOpen:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 297
    iget-boolean v0, p0, Lcom/samsung/musicplus/library/audio/RingtoneRecommender;->mIsOpen:Z

    if-eqz v0, :cond_0

    .line 298
    iget-object v0, p0, Lcom/samsung/musicplus/library/audio/RingtoneRecommender;->mRecommender:Lcom/samsung/audio/Smat;

    invoke-virtual {v0}, Lcom/samsung/audio/Smat;->quit()I

    move-result v0

    if-nez v0, :cond_0

    .line 299
    const/4 v0, 0x1

    .line 302
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
