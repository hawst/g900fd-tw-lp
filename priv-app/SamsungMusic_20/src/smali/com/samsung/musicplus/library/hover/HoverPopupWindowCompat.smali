.class public Lcom/samsung/musicplus/library/hover/HoverPopupWindowCompat;
.super Ljava/lang/Object;
.source "HoverPopupWindowCompat.java"


# static fields
.field public static final GRAVITY_BOTTOM:I = 0x50

.field public static final GRAVITY_BOTTOM_UNDER:I = 0x5050

.field public static final GRAVITY_CENTER:I = 0x11

.field public static final GRAVITY_CENTER_HORIZONTAL:I = 0x1

.field public static final GRAVITY_CENTER_HORIZONTAL_ON_POINT:I = 0x201

.field public static final GRAVITY_CENTER_HORIZONTAL_ON_WINDOW:I = 0x101

.field public static final GRAVITY_CENTER_VERTICAL:I = 0x10

.field public static final GRAVITY_LEFT:I = 0x3

.field public static final GRAVITY_LEFT_CENTER_AXIS:I = 0x103

.field public static final GRAVITY_LEFT_OUTSIDE:I = 0x303

.field public static final GRAVITY_RIGHT:I = 0x5

.field public static final GRAVITY_RIGHT_CENTER_AXIS:I = 0x105

.field public static final GRAVITY_RIGHT_OUTSIDE:I = 0x505

.field public static final GRAVITY_TOP:I = 0x30

.field public static final GRAVITY_TOP_ABOVE:I = 0x3030


# instance fields
.field final mHoverPopupWindow:Landroid/widget/HoverPopupWindow;


# direct methods
.method constructor <init>(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    invoke-virtual {p1}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/library/hover/HoverPopupWindowCompat;->mHoverPopupWindow:Landroid/widget/HoverPopupWindow;

    .line 63
    return-void
.end method


# virtual methods
.method public setPopupGravity(I)V
    .locals 1
    .param p1, "gravity"    # I

    .prologue
    .line 72
    iget-object v0, p0, Lcom/samsung/musicplus/library/hover/HoverPopupWindowCompat;->mHoverPopupWindow:Landroid/widget/HoverPopupWindow;

    invoke-virtual {v0, p1}, Landroid/widget/HoverPopupWindow;->setPopupGravity(I)V

    .line 73
    return-void
.end method
