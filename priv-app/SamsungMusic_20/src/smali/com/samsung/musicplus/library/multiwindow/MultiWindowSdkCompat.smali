.class public Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat;
.super Ljava/lang/Object;
.source "MultiWindowSdkCompat.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat$OnMultiWindowChangeListener;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "MusicUi_MultiWindow"

.field public static final ZONE_A:I

.field public static final ZONE_B:I

.field public static final ZONE_C:I

.field public static final ZONE_D:I

.field public static final ZONE_E:I

.field public static final ZONE_ERROR:I = -0x1869f

.field public static final ZONE_F:I


# instance fields
.field private mListener:Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat$OnMultiWindowChangeListener;

.field private mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 58
    sget v0, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->ZONE_A:I

    sput v0, Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat;->ZONE_A:I

    .line 66
    sget v0, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->ZONE_B:I

    sput v0, Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat;->ZONE_B:I

    .line 74
    sget v0, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->ZONE_C:I

    sput v0, Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat;->ZONE_C:I

    .line 82
    sget v0, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->ZONE_D:I

    sput v0, Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat;->ZONE_D:I

    .line 90
    sget v0, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->ZONE_E:I

    sput v0, Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat;->ZONE_E:I

    .line 98
    sget v0, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->ZONE_F:I

    sput v0, Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat;->ZONE_F:I

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat$OnMultiWindowChangeListener;)V
    .locals 1
    .param p1, "a"    # Landroid/app/Activity;
    .param p2, "l"    # Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat$OnMultiWindowChangeListener;

    .prologue
    .line 137
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 138
    new-instance v0, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-direct {v0, p1}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    .line 139
    invoke-direct {p0, p2}, Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat;->setStateChangeListener(Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat$OnMultiWindowChangeListener;)V

    .line 140
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat;)Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat$OnMultiWindowChangeListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat;->mListener:Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat$OnMultiWindowChangeListener;

    return-object v0
.end method

.method public static isSupportMultiWindow(Landroid/content/Context;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 196
    invoke-static {p0}, Lcom/samsung/musicplus/library/multiwindow/MultiWindowCompat;->isEnableMultiWindow(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method private setStateChangeListener(Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat$OnMultiWindowChangeListener;)V
    .locals 2
    .param p1, "l"    # Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat$OnMultiWindowChangeListener;

    .prologue
    .line 151
    iget-object v0, p0, Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    if-nez v0, :cond_0

    .line 152
    const-string v0, "MusicUi_MultiWindow"

    const-string v1, "setStateChangeListener(), mMultiWindowActivity is null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 184
    :goto_0
    return-void

    .line 156
    :cond_0
    iput-object p1, p0, Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat;->mListener:Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat$OnMultiWindowChangeListener;

    .line 158
    iget-object v0, p0, Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    new-instance v1, Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat$1;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat$1;-><init>(Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat;)V

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->setStateChangeListener(Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity$StateChangeListener;)Z

    goto :goto_0
.end method


# virtual methods
.method public getRectInfo()Landroid/graphics/Rect;
    .locals 2

    .prologue
    .line 292
    iget-object v0, p0, Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    if-nez v0, :cond_0

    .line 293
    const-string v0, "MusicUi_MultiWindow"

    const-string v1, "getRectInfo(), mMultiWindowActivity is null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 294
    const/4 v0, 0x0

    .line 296
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->getRectInfo()Landroid/graphics/Rect;

    move-result-object v0

    goto :goto_0
.end method

.method public getZoneInfo()I
    .locals 2

    .prologue
    .line 278
    iget-object v0, p0, Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    if-nez v0, :cond_0

    .line 279
    const-string v0, "MusicUi_MultiWindow"

    const-string v1, "getZoneInfo(), mMultiWindowActivity is null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 280
    const v0, -0x1869f

    .line 282
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->getZoneInfo()I

    move-result v0

    goto :goto_0
.end method

.method public isMinimized()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 254
    iget-object v2, p0, Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    if-nez v2, :cond_0

    .line 255
    const-string v2, "MusicUi_MultiWindow"

    const-string v3, "isMinimized(), mMultiWindowActivity is null"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 268
    :goto_0
    return v1

    .line 260
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isMinimized()Z
    :try_end_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    move-result v1

    goto :goto_0

    .line 261
    :catch_0
    move-exception v0

    .line 262
    .local v0, "e":Ljava/lang/NoClassDefFoundError;
    const-string v2, "MusicUi_MultiWindow"

    const-string v3, "isMinimized(), NoClassDefFoundError"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 263
    .end local v0    # "e":Ljava/lang/NoClassDefFoundError;
    :catch_1
    move-exception v0

    .line 264
    .local v0, "e":Ljava/lang/NullPointerException;
    const-string v2, "MusicUi_MultiWindow"

    const-string v3, "isMinimized(), NullPointerException"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 265
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :catch_2
    move-exception v0

    .line 266
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "MusicUi_MultiWindow"

    const-string v3, "isMinimized(), Exception"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public isMultiWindow()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 206
    iget-object v2, p0, Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    if-nez v2, :cond_0

    .line 207
    const-string v2, "MusicUi_MultiWindow"

    const-string v3, "isMultiWindow(), mMultiWindowActivity is null"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 220
    :goto_0
    return v1

    .line 212
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isMultiWindow()Z
    :try_end_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    move-result v1

    goto :goto_0

    .line 213
    :catch_0
    move-exception v0

    .line 214
    .local v0, "e":Ljava/lang/NoClassDefFoundError;
    const-string v2, "MusicUi_MultiWindow"

    const-string v3, "isMultiWindow(), NoClassDefFoundError"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 215
    .end local v0    # "e":Ljava/lang/NoClassDefFoundError;
    :catch_1
    move-exception v0

    .line 216
    .local v0, "e":Ljava/lang/NullPointerException;
    const-string v2, "MusicUi_MultiWindow"

    const-string v3, "isMultiWindow(), NullPointerException"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 217
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :catch_2
    move-exception v0

    .line 218
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "MusicUi_MultiWindow"

    const-string v3, "isMultiWindow(), Exception"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public isNormalWindow()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 230
    iget-object v2, p0, Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    if-nez v2, :cond_0

    .line 231
    const-string v2, "MusicUi_MultiWindow"

    const-string v3, "isNormalWindow(), mMultiWindowActivity is null"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 244
    :goto_0
    return v1

    .line 236
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isNormalWindow()Z
    :try_end_0
    .catch Ljava/lang/NoClassDefFoundError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    move-result v1

    goto :goto_0

    .line 237
    :catch_0
    move-exception v0

    .line 238
    .local v0, "e":Ljava/lang/NoClassDefFoundError;
    const-string v2, "MusicUi_MultiWindow"

    const-string v3, "isNormalWindow(), NoClassDefFoundError"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 239
    .end local v0    # "e":Ljava/lang/NoClassDefFoundError;
    :catch_1
    move-exception v0

    .line 240
    .local v0, "e":Ljava/lang/NullPointerException;
    const-string v2, "MusicUi_MultiWindow"

    const-string v3, "isNormalWindow(), NullPointerException"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 241
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :catch_2
    move-exception v0

    .line 242
    .local v0, "e":Ljava/lang/Exception;
    const-string v2, "MusicUi_MultiWindow"

    const-string v3, "isNormalWindow(), Exception"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public release()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 306
    iput-object v0, p0, Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat;->mMultiWindowActivity:Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    .line 307
    iput-object v0, p0, Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat;->mListener:Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat$OnMultiWindowChangeListener;

    .line 308
    return-void
.end method
