.class Lcom/samsung/musicplus/library/audio/FindTag$1;
.super Ljava/lang/Thread;
.source "FindTag.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/library/audio/FindTag;->startParsingSignature(Ljava/lang/String;Lcom/samsung/musicplus/library/audio/FindTag$OnFindTagResultListener;)Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/library/audio/FindTag;

.field final synthetic val$listener:Lcom/samsung/musicplus/library/audio/FindTag$OnFindTagResultListener;

.field final synthetic val$path:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/library/audio/FindTag;Ljava/lang/String;Lcom/samsung/musicplus/library/audio/FindTag$OnFindTagResultListener;Ljava/lang/String;)V
    .locals 0
    .param p2, "x0"    # Ljava/lang/String;

    .prologue
    .line 139
    iput-object p1, p0, Lcom/samsung/musicplus/library/audio/FindTag$1;->this$0:Lcom/samsung/musicplus/library/audio/FindTag;

    iput-object p3, p0, Lcom/samsung/musicplus/library/audio/FindTag$1;->val$listener:Lcom/samsung/musicplus/library/audio/FindTag$OnFindTagResultListener;

    iput-object p4, p0, Lcom/samsung/musicplus/library/audio/FindTag$1;->val$path:Ljava/lang/String;

    invoke-direct {p0, p2}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 141
    iget-object v1, p0, Lcom/samsung/musicplus/library/audio/FindTag$1;->this$0:Lcom/samsung/musicplus/library/audio/FindTag;

    # setter for: Lcom/samsung/musicplus/library/audio/FindTag;->mWorkingTime:I
    invoke-static {v1, v3}, Lcom/samsung/musicplus/library/audio/FindTag;->access$002(Lcom/samsung/musicplus/library/audio/FindTag;I)I

    .line 142
    iget-object v1, p0, Lcom/samsung/musicplus/library/audio/FindTag$1;->this$0:Lcom/samsung/musicplus/library/audio/FindTag;

    const/4 v2, 0x0

    # setter for: Lcom/samsung/musicplus/library/audio/FindTag;->mSignature:[B
    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/audio/FindTag;->access$102(Lcom/samsung/musicplus/library/audio/FindTag;[B)[B

    .line 143
    iget-object v1, p0, Lcom/samsung/musicplus/library/audio/FindTag$1;->this$0:Lcom/samsung/musicplus/library/audio/FindTag;

    iget-object v2, p0, Lcom/samsung/musicplus/library/audio/FindTag$1;->val$listener:Lcom/samsung/musicplus/library/audio/FindTag$OnFindTagResultListener;

    # setter for: Lcom/samsung/musicplus/library/audio/FindTag;->mResultListener:Lcom/samsung/musicplus/library/audio/FindTag$OnFindTagResultListener;
    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/audio/FindTag;->access$202(Lcom/samsung/musicplus/library/audio/FindTag;Lcom/samsung/musicplus/library/audio/FindTag$OnFindTagResultListener;)Lcom/samsung/musicplus/library/audio/FindTag$OnFindTagResultListener;

    .line 144
    iget-object v1, p0, Lcom/samsung/musicplus/library/audio/FindTag$1;->this$0:Lcom/samsung/musicplus/library/audio/FindTag;

    # setter for: Lcom/samsung/musicplus/library/audio/FindTag;->mIsFinished:Z
    invoke-static {v1, v3}, Lcom/samsung/musicplus/library/audio/FindTag;->access$302(Lcom/samsung/musicplus/library/audio/FindTag;Z)Z

    .line 145
    iget-object v1, p0, Lcom/samsung/musicplus/library/audio/FindTag$1;->this$0:Lcom/samsung/musicplus/library/audio/FindTag;

    iget-object v2, p0, Lcom/samsung/musicplus/library/audio/FindTag$1;->val$path:Ljava/lang/String;

    # invokes: Lcom/samsung/musicplus/library/audio/FindTag;->startParsing(Ljava/lang/String;)V
    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/audio/FindTag;->access$400(Lcom/samsung/musicplus/library/audio/FindTag;Ljava/lang/String;)V

    .line 146
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/samsung/musicplus/library/audio/FindTag$1;->this$0:Lcom/samsung/musicplus/library/audio/FindTag;

    # getter for: Lcom/samsung/musicplus/library/audio/FindTag;->mIsFinished:Z
    invoke-static {v1}, Lcom/samsung/musicplus/library/audio/FindTag;->access$300(Lcom/samsung/musicplus/library/audio/FindTag;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 148
    const-wide/16 v2, 0x3e8

    :try_start_0
    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/audio/FindTag$1;->sleep(J)V

    .line 149
    iget-object v1, p0, Lcom/samsung/musicplus/library/audio/FindTag$1;->this$0:Lcom/samsung/musicplus/library/audio/FindTag;

    const/16 v2, 0x3e8

    # += operator for: Lcom/samsung/musicplus/library/audio/FindTag;->mWorkingTime:I
    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/audio/FindTag;->access$012(Lcom/samsung/musicplus/library/audio/FindTag;I)I
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 153
    :goto_1
    iget-object v1, p0, Lcom/samsung/musicplus/library/audio/FindTag$1;->this$0:Lcom/samsung/musicplus/library/audio/FindTag;

    iget-object v2, p0, Lcom/samsung/musicplus/library/audio/FindTag$1;->this$0:Lcom/samsung/musicplus/library/audio/FindTag;

    # invokes: Lcom/samsung/musicplus/library/audio/FindTag;->checkIsFinished()Z
    invoke-static {v2}, Lcom/samsung/musicplus/library/audio/FindTag;->access$500(Lcom/samsung/musicplus/library/audio/FindTag;)Z

    move-result v2

    # setter for: Lcom/samsung/musicplus/library/audio/FindTag;->mIsFinished:Z
    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/audio/FindTag;->access$302(Lcom/samsung/musicplus/library/audio/FindTag;Z)Z

    .line 154
    iget-object v1, p0, Lcom/samsung/musicplus/library/audio/FindTag$1;->this$0:Lcom/samsung/musicplus/library/audio/FindTag;

    # getter for: Lcom/samsung/musicplus/library/audio/FindTag;->mIsFinished:Z
    invoke-static {v1}, Lcom/samsung/musicplus/library/audio/FindTag;->access$300(Lcom/samsung/musicplus/library/audio/FindTag;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 155
    iget-object v1, p0, Lcom/samsung/musicplus/library/audio/FindTag$1;->this$0:Lcom/samsung/musicplus/library/audio/FindTag;

    # invokes: Lcom/samsung/musicplus/library/audio/FindTag;->stopMediaPlayer()V
    invoke-static {v1}, Lcom/samsung/musicplus/library/audio/FindTag;->access$600(Lcom/samsung/musicplus/library/audio/FindTag;)V

    goto :goto_0

    .line 150
    :catch_0
    move-exception v0

    .line 151
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1

    .line 158
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_1
    iget-object v1, p0, Lcom/samsung/musicplus/library/audio/FindTag$1;->this$0:Lcom/samsung/musicplus/library/audio/FindTag;

    # getter for: Lcom/samsung/musicplus/library/audio/FindTag;->mResultListener:Lcom/samsung/musicplus/library/audio/FindTag$OnFindTagResultListener;
    invoke-static {v1}, Lcom/samsung/musicplus/library/audio/FindTag;->access$200(Lcom/samsung/musicplus/library/audio/FindTag;)Lcom/samsung/musicplus/library/audio/FindTag$OnFindTagResultListener;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 159
    iget-object v1, p0, Lcom/samsung/musicplus/library/audio/FindTag$1;->this$0:Lcom/samsung/musicplus/library/audio/FindTag;

    # getter for: Lcom/samsung/musicplus/library/audio/FindTag;->mResultListener:Lcom/samsung/musicplus/library/audio/FindTag$OnFindTagResultListener;
    invoke-static {v1}, Lcom/samsung/musicplus/library/audio/FindTag;->access$200(Lcom/samsung/musicplus/library/audio/FindTag;)Lcom/samsung/musicplus/library/audio/FindTag$OnFindTagResultListener;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/library/audio/FindTag$1;->this$0:Lcom/samsung/musicplus/library/audio/FindTag;

    # getter for: Lcom/samsung/musicplus/library/audio/FindTag;->mSignature:[B
    invoke-static {v2}, Lcom/samsung/musicplus/library/audio/FindTag;->access$100(Lcom/samsung/musicplus/library/audio/FindTag;)[B

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/samsung/musicplus/library/audio/FindTag$OnFindTagResultListener;->onResult([B)V

    .line 161
    :cond_2
    return-void
.end method
