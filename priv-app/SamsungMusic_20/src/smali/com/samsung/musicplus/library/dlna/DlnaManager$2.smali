.class Lcom/samsung/musicplus/library/dlna/DlnaManager$2;
.super Ljava/lang/Object;
.source "DlnaManager.java"

# interfaces
.implements Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/library/dlna/DlnaManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/library/dlna/DlnaManager;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/library/dlna/DlnaManager;)V
    .locals 0

    .prologue
    .line 656
    iput-object p1, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager$2;->this$0:Lcom/samsung/musicplus/library/dlna/DlnaManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDeviceAdded(Lcom/samsung/android/allshare/Device$DeviceType;Lcom/samsung/android/allshare/Device;Lcom/samsung/android/allshare/ERROR;)V
    .locals 4
    .param p1, "deviceType"    # Lcom/samsung/android/allshare/Device$DeviceType;
    .param p2, "device"    # Lcom/samsung/android/allshare/Device;
    .param p3, "err"    # Lcom/samsung/android/allshare/ERROR;

    .prologue
    .line 686
    # getter for: Lcom/samsung/musicplus/library/dlna/DlnaManager;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/library/dlna/DlnaManager;->access$100()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DMS Finder - onDeviceAdded() - deviceName: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Lcom/samsung/android/allshare/Device;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " Error: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p3}, Lcom/samsung/android/allshare/ERROR;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 688
    invoke-virtual {p2}, Lcom/samsung/android/allshare/Device;->getID()Ljava/lang/String;

    move-result-object v0

    .line 689
    .local v0, "deviceId":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager$2;->this$0:Lcom/samsung/musicplus/library/dlna/DlnaManager;

    # invokes: Lcom/samsung/musicplus/library/dlna/DlnaManager;->updateDmsList()V
    invoke-static {v1}, Lcom/samsung/musicplus/library/dlna/DlnaManager;->access$300(Lcom/samsung/musicplus/library/dlna/DlnaManager;)V

    .line 690
    iget-object v1, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager$2;->this$0:Lcom/samsung/musicplus/library/dlna/DlnaManager;

    const-string v2, "com.samsung.musicplus.dlna.connectivitychanged"

    const/4 v3, 0x0

    # invokes: Lcom/samsung/musicplus/library/dlna/DlnaManager;->sendDlnaInfo(Ljava/lang/String;ILjava/lang/String;)V
    invoke-static {v1, v2, v3, v0}, Lcom/samsung/musicplus/library/dlna/DlnaManager;->access$1100(Lcom/samsung/musicplus/library/dlna/DlnaManager;Ljava/lang/String;ILjava/lang/String;)V

    .line 691
    iget-object v1, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager$2;->this$0:Lcom/samsung/musicplus/library/dlna/DlnaManager;

    # invokes: Lcom/samsung/musicplus/library/dlna/DlnaManager;->handleDeviceFinderError(Lcom/samsung/android/allshare/ERROR;Ljava/lang/String;)V
    invoke-static {v1, p3, v0}, Lcom/samsung/musicplus/library/dlna/DlnaManager;->access$1200(Lcom/samsung/musicplus/library/dlna/DlnaManager;Lcom/samsung/android/allshare/ERROR;Ljava/lang/String;)V

    .line 692
    return-void
.end method

.method public onDeviceRemoved(Lcom/samsung/android/allshare/Device$DeviceType;Lcom/samsung/android/allshare/Device;Lcom/samsung/android/allshare/ERROR;)V
    .locals 7
    .param p1, "deviceType"    # Lcom/samsung/android/allshare/Device$DeviceType;
    .param p2, "device"    # Lcom/samsung/android/allshare/Device;
    .param p3, "err"    # Lcom/samsung/android/allshare/ERROR;

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 664
    # getter for: Lcom/samsung/musicplus/library/dlna/DlnaManager;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/library/dlna/DlnaManager;->access$100()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DMS Finder - onDeviceRemoved() - deviceName: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Lcom/samsung/android/allshare/Device;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " Error: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p3}, Lcom/samsung/android/allshare/ERROR;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 666
    invoke-virtual {p2}, Lcom/samsung/android/allshare/Device;->getID()Ljava/lang/String;

    move-result-object v0

    .line 667
    .local v0, "deviceId":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager$2;->this$0:Lcom/samsung/musicplus/library/dlna/DlnaManager;

    # getter for: Lcom/samsung/musicplus/library/dlna/DlnaManager;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/musicplus/library/dlna/DlnaManager;->access$900(Lcom/samsung/musicplus/library/dlna/DlnaManager;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager$2;->this$0:Lcom/samsung/musicplus/library/dlna/DlnaManager;

    # getter for: Lcom/samsung/musicplus/library/dlna/DlnaManager;->MEDIA_SERVER_URI:Landroid/net/Uri;
    invoke-static {v2}, Lcom/samsung/musicplus/library/dlna/DlnaManager;->access$800(Lcom/samsung/musicplus/library/dlna/DlnaManager;)Landroid/net/Uri;

    move-result-object v2

    const-string v3, "provider_id = ?"

    new-array v4, v5, [Ljava/lang/String;

    aput-object v0, v4, v6

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 671
    iget-object v1, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager$2;->this$0:Lcom/samsung/musicplus/library/dlna/DlnaManager;

    # getter for: Lcom/samsung/musicplus/library/dlna/DlnaManager;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/musicplus/library/dlna/DlnaManager;->access$900(Lcom/samsung/musicplus/library/dlna/DlnaManager;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager$2;->this$0:Lcom/samsung/musicplus/library/dlna/DlnaManager;

    # getter for: Lcom/samsung/musicplus/library/dlna/DlnaManager;->MEDIA_SERVER_CONTENTS_URI:Landroid/net/Uri;
    invoke-static {v2}, Lcom/samsung/musicplus/library/dlna/DlnaManager;->access$1000(Lcom/samsung/musicplus/library/dlna/DlnaManager;)Landroid/net/Uri;

    move-result-object v2

    const-string v3, "provider_id = ?"

    new-array v4, v5, [Ljava/lang/String;

    aput-object v0, v4, v6

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 675
    iget-object v1, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager$2;->this$0:Lcom/samsung/musicplus/library/dlna/DlnaManager;

    # invokes: Lcom/samsung/musicplus/library/dlna/DlnaManager;->updateDmsList()V
    invoke-static {v1}, Lcom/samsung/musicplus/library/dlna/DlnaManager;->access$300(Lcom/samsung/musicplus/library/dlna/DlnaManager;)V

    .line 676
    iget-object v1, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager$2;->this$0:Lcom/samsung/musicplus/library/dlna/DlnaManager;

    const-string v2, "com.samsung.musicplus.dlna.connectivitychanged"

    # invokes: Lcom/samsung/musicplus/library/dlna/DlnaManager;->sendDlnaInfo(Ljava/lang/String;ILjava/lang/String;)V
    invoke-static {v1, v2, v5, v0}, Lcom/samsung/musicplus/library/dlna/DlnaManager;->access$1100(Lcom/samsung/musicplus/library/dlna/DlnaManager;Ljava/lang/String;ILjava/lang/String;)V

    .line 677
    iget-object v1, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager$2;->this$0:Lcom/samsung/musicplus/library/dlna/DlnaManager;

    # invokes: Lcom/samsung/musicplus/library/dlna/DlnaManager;->handleDeviceFinderError(Lcom/samsung/android/allshare/ERROR;Ljava/lang/String;)V
    invoke-static {v1, p3, v0}, Lcom/samsung/musicplus/library/dlna/DlnaManager;->access$1200(Lcom/samsung/musicplus/library/dlna/DlnaManager;Lcom/samsung/android/allshare/ERROR;Ljava/lang/String;)V

    .line 678
    return-void
.end method
