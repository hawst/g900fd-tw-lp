.class Lcom/samsung/musicplus/library/hardware/SbeamManager$1;
.super Ljava/lang/Object;
.source "SbeamManager.java"

# interfaces
.implements Landroid/nfc/NfcAdapter$CreateNdefMessageCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/library/hardware/SbeamManager;-><init>(Landroid/app/Activity;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/library/hardware/SbeamManager;

.field final synthetic val$nfcAdapter:Landroid/nfc/NfcAdapter;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/library/hardware/SbeamManager;Landroid/nfc/NfcAdapter;)V
    .locals 0

    .prologue
    .line 134
    iput-object p1, p0, Lcom/samsung/musicplus/library/hardware/SbeamManager$1;->this$0:Lcom/samsung/musicplus/library/hardware/SbeamManager;

    iput-object p2, p0, Lcom/samsung/musicplus/library/hardware/SbeamManager$1;->val$nfcAdapter:Landroid/nfc/NfcAdapter;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createNdefMessage(Landroid/nfc/NfcEvent;)Landroid/nfc/NdefMessage;
    .locals 8
    .param p1, "event"    # Landroid/nfc/NfcEvent;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v2, 0x0

    .line 137
    # getter for: Lcom/samsung/musicplus/library/hardware/SbeamManager;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/library/hardware/SbeamManager;->access$000()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "createNdefMessage - isEnableSBeam()"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/musicplus/library/hardware/SbeamManager$1;->this$0:Lcom/samsung/musicplus/library/hardware/SbeamManager;

    # invokes: Lcom/samsung/musicplus/library/hardware/SbeamManager;->isEnableSBeam()Z
    invoke-static {v5}, Lcom/samsung/musicplus/library/hardware/SbeamManager;->access$100(Lcom/samsung/musicplus/library/hardware/SbeamManager;)Z

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    iget-object v3, p0, Lcom/samsung/musicplus/library/hardware/SbeamManager$1;->this$0:Lcom/samsung/musicplus/library/hardware/SbeamManager;

    # getter for: Lcom/samsung/musicplus/library/hardware/SbeamManager;->mPath:Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/musicplus/library/hardware/SbeamManager;->access$200(Lcom/samsung/musicplus/library/hardware/SbeamManager;)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_1

    .line 140
    # getter for: Lcom/samsung/musicplus/library/hardware/SbeamManager;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/library/hardware/SbeamManager;->access$000()Ljava/lang/String;

    move-result-object v3

    const-string v4, "createNdefMessage - mPath is null"

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 168
    :cond_0
    :goto_0
    return-object v2

    .line 143
    :cond_1
    iget-object v3, p0, Lcom/samsung/musicplus/library/hardware/SbeamManager$1;->this$0:Lcom/samsung/musicplus/library/hardware/SbeamManager;

    # invokes: Lcom/samsung/musicplus/library/hardware/SbeamManager;->isEnableSBeam()Z
    invoke-static {v3}, Lcom/samsung/musicplus/library/hardware/SbeamManager;->access$100(Lcom/samsung/musicplus/library/hardware/SbeamManager;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 144
    iget-object v3, p0, Lcom/samsung/musicplus/library/hardware/SbeamManager$1;->this$0:Lcom/samsung/musicplus/library/hardware/SbeamManager;

    const-string v4, "text/DirectShareMusic"

    iget-object v5, p0, Lcom/samsung/musicplus/library/hardware/SbeamManager$1;->this$0:Lcom/samsung/musicplus/library/hardware/SbeamManager;

    # getter for: Lcom/samsung/musicplus/library/hardware/SbeamManager;->mPath:Ljava/lang/String;
    invoke-static {v5}, Lcom/samsung/musicplus/library/hardware/SbeamManager;->access$200(Lcom/samsung/musicplus/library/hardware/SbeamManager;)Ljava/lang/String;

    move-result-object v5

    # invokes: Lcom/samsung/musicplus/library/hardware/SbeamManager;->createMimeRecord(Ljava/lang/String;Ljava/lang/String;)Landroid/nfc/NdefRecord;
    invoke-static {v3, v4, v5}, Lcom/samsung/musicplus/library/hardware/SbeamManager;->access$300(Lcom/samsung/musicplus/library/hardware/SbeamManager;Ljava/lang/String;Ljava/lang/String;)Landroid/nfc/NdefRecord;

    move-result-object v0

    .line 146
    .local v0, "message":Landroid/nfc/NdefRecord;
    if-eqz v0, :cond_0

    .line 153
    iget-object v3, p0, Lcom/samsung/musicplus/library/hardware/SbeamManager$1;->val$nfcAdapter:Landroid/nfc/NfcAdapter;

    iget-object v4, p0, Lcom/samsung/musicplus/library/hardware/SbeamManager$1;->this$0:Lcom/samsung/musicplus/library/hardware/SbeamManager;

    # getter for: Lcom/samsung/musicplus/library/hardware/SbeamManager;->mActivity:Landroid/app/Activity;
    invoke-static {v4}, Lcom/samsung/musicplus/library/hardware/SbeamManager;->access$400(Lcom/samsung/musicplus/library/hardware/SbeamManager;)Landroid/app/Activity;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Landroid/nfc/NfcAdapter;->setBeamPushUris([Landroid/net/Uri;Landroid/app/Activity;)V

    .line 154
    new-instance v2, Landroid/nfc/NdefMessage;

    const/4 v3, 0x2

    new-array v3, v3, [Landroid/nfc/NdefRecord;

    aput-object v0, v3, v6

    const-string v4, "com.sec.android.directshare"

    invoke-static {v4}, Landroid/nfc/NdefRecord;->createApplicationRecord(Ljava/lang/String;)Landroid/nfc/NdefRecord;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-direct {v2, v3}, Landroid/nfc/NdefMessage;-><init>([Landroid/nfc/NdefRecord;)V

    goto :goto_0

    .line 161
    .end local v0    # "message":Landroid/nfc/NdefRecord;
    :cond_2
    new-instance v3, Ljava/io/File;

    iget-object v4, p0, Lcom/samsung/musicplus/library/hardware/SbeamManager$1;->this$0:Lcom/samsung/musicplus/library/hardware/SbeamManager;

    # getter for: Lcom/samsung/musicplus/library/hardware/SbeamManager;->mPath:Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/musicplus/library/hardware/SbeamManager;->access$200(Lcom/samsung/musicplus/library/hardware/SbeamManager;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    .line 162
    .local v1, "uri":Landroid/net/Uri;
    iget-object v3, p0, Lcom/samsung/musicplus/library/hardware/SbeamManager$1;->this$0:Lcom/samsung/musicplus/library/hardware/SbeamManager;

    # getter for: Lcom/samsung/musicplus/library/hardware/SbeamManager;->mDrmManager:Lcom/samsung/musicplus/library/drm/DrmManager;
    invoke-static {v3}, Lcom/samsung/musicplus/library/hardware/SbeamManager;->access$500(Lcom/samsung/musicplus/library/hardware/SbeamManager;)Lcom/samsung/musicplus/library/drm/DrmManager;

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/musicplus/library/hardware/SbeamManager$1;->this$0:Lcom/samsung/musicplus/library/hardware/SbeamManager;

    # getter for: Lcom/samsung/musicplus/library/hardware/SbeamManager;->mPath:Ljava/lang/String;
    invoke-static {v4}, Lcom/samsung/musicplus/library/hardware/SbeamManager;->access$200(Lcom/samsung/musicplus/library/hardware/SbeamManager;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/samsung/musicplus/library/drm/DrmManager;->isDrm(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 163
    iget-object v3, p0, Lcom/samsung/musicplus/library/hardware/SbeamManager$1;->val$nfcAdapter:Landroid/nfc/NfcAdapter;

    new-array v4, v7, [Landroid/net/Uri;

    aput-object v1, v4, v6

    iget-object v5, p0, Lcom/samsung/musicplus/library/hardware/SbeamManager$1;->this$0:Lcom/samsung/musicplus/library/hardware/SbeamManager;

    # getter for: Lcom/samsung/musicplus/library/hardware/SbeamManager;->mActivity:Landroid/app/Activity;
    invoke-static {v5}, Lcom/samsung/musicplus/library/hardware/SbeamManager;->access$400(Lcom/samsung/musicplus/library/hardware/SbeamManager;)Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/nfc/NfcAdapter;->setBeamPushUris([Landroid/net/Uri;Landroid/app/Activity;)V

    .line 167
    :cond_3
    iget-object v3, p0, Lcom/samsung/musicplus/library/hardware/SbeamManager$1;->this$0:Lcom/samsung/musicplus/library/hardware/SbeamManager;

    const/4 v4, 0x6

    # setter for: Lcom/samsung/musicplus/library/hardware/SbeamManager;->mMode:I
    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/hardware/SbeamManager;->access$602(Lcom/samsung/musicplus/library/hardware/SbeamManager;I)I

    goto :goto_0
.end method
