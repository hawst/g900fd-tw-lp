.class final Lcom/samsung/musicplus/library/hardware/AirView$3;
.super Ljava/lang/Object;
.source "AirView.java"

# interfaces
.implements Landroid/widget/HoverPopupWindow$HoverPopupListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/library/hardware/AirView;->setView(Landroid/view/View;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;ILcom/samsung/musicplus/library/hardware/AirView$OnAirViewPopupListener;ZZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$listener:Lcom/samsung/musicplus/library/hardware/AirView$OnAirViewPopupListener;

.field final synthetic val$text1:Landroid/widget/TextView;

.field final synthetic val$text2:Landroid/widget/TextView;

.field final synthetic val$text3:Landroid/widget/TextView;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/library/hardware/AirView$OnAirViewPopupListener;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;)V
    .locals 0

    .prologue
    .line 458
    iput-object p1, p0, Lcom/samsung/musicplus/library/hardware/AirView$3;->val$listener:Lcom/samsung/musicplus/library/hardware/AirView$OnAirViewPopupListener;

    iput-object p2, p0, Lcom/samsung/musicplus/library/hardware/AirView$3;->val$text1:Landroid/widget/TextView;

    iput-object p3, p0, Lcom/samsung/musicplus/library/hardware/AirView$3;->val$text2:Landroid/widget/TextView;

    iput-object p4, p0, Lcom/samsung/musicplus/library/hardware/AirView$3;->val$text3:Landroid/widget/TextView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSetContentView(Landroid/view/View;Landroid/widget/HoverPopupWindow;)Z
    .locals 3
    .param p1, "parentView"    # Landroid/view/View;
    .param p2, "hpw"    # Landroid/widget/HoverPopupWindow;

    .prologue
    .line 462
    const/4 v1, 0x0

    .line 463
    .local v1, "v":Landroid/view/View;
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 464
    .local v0, "context":Landroid/content/Context;
    invoke-static {v0}, Lcom/samsung/musicplus/library/hardware/AirView;->isEnableAirViewInforPreview(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {v0}, Lcom/samsung/musicplus/library/hardware/AirView;->isEnablePenHoveringInforPreview(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 466
    :cond_0
    iget-object v2, p0, Lcom/samsung/musicplus/library/hardware/AirView$3;->val$listener:Lcom/samsung/musicplus/library/hardware/AirView$OnAirViewPopupListener;

    if-eqz v2, :cond_1

    .line 467
    iget-object v2, p0, Lcom/samsung/musicplus/library/hardware/AirView$3;->val$listener:Lcom/samsung/musicplus/library/hardware/AirView$OnAirViewPopupListener;

    invoke-interface {v2, p1}, Lcom/samsung/musicplus/library/hardware/AirView$OnAirViewPopupListener;->getAirView(Landroid/view/View;)Landroid/view/View;

    move-result-object v1

    .line 470
    :cond_1
    if-eqz v1, :cond_3

    .line 471
    iget-object v2, p0, Lcom/samsung/musicplus/library/hardware/AirView$3;->val$text1:Landroid/widget/TextView;

    invoke-static {v2}, Lcom/samsung/musicplus/library/view/TextViewCompat;->isEllipsis(Landroid/widget/TextView;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/samsung/musicplus/library/hardware/AirView$3;->val$text2:Landroid/widget/TextView;

    invoke-static {v2}, Lcom/samsung/musicplus/library/view/TextViewCompat;->isEllipsis(Landroid/widget/TextView;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/samsung/musicplus/library/hardware/AirView$3;->val$text3:Landroid/widget/TextView;

    invoke-static {v2}, Lcom/samsung/musicplus/library/view/TextViewCompat;->isEllipsis(Landroid/widget/TextView;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 474
    :cond_2
    invoke-virtual {p2, v1}, Landroid/widget/HoverPopupWindow;->setContent(Landroid/view/View;)V

    .line 480
    :cond_3
    :goto_0
    if-eqz v1, :cond_5

    const/4 v2, 0x1

    :goto_1
    return v2

    .line 476
    :cond_4
    invoke-virtual {p2}, Landroid/widget/HoverPopupWindow;->dismiss()V

    goto :goto_0

    .line 480
    :cond_5
    const/4 v2, 0x0

    goto :goto_1
.end method
