.class public Lcom/samsung/musicplus/library/metadata/SecMediaMetaDataRetriever;
.super Ljava/lang/Object;
.source "SecMediaMetaDataRetriever.java"


# static fields
.field public static final METADATA_KEY_ALBUM:I = 0x1

.field public static final METADATA_KEY_ALBUMARTIST:I = 0xd

.field public static final METADATA_KEY_ARTIST:I = 0x2

.field public static final METADATA_KEY_COMPOSER:I = 0x4

.field public static final METADATA_KEY_TITLE:I = 0x7


# instance fields
.field private meta:Landroid/media/MediaMetadataRetriever;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 4
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 77
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 78
    new-instance v1, Landroid/media/MediaMetadataRetriever;

    invoke-direct {v1}, Landroid/media/MediaMetadataRetriever;-><init>()V

    iput-object v1, p0, Lcom/samsung/musicplus/library/metadata/SecMediaMetaDataRetriever;->meta:Landroid/media/MediaMetadataRetriever;

    .line 80
    :try_start_0
    iget-object v1, p0, Lcom/samsung/musicplus/library/metadata/SecMediaMetaDataRetriever;->meta:Landroid/media/MediaMetadataRetriever;

    invoke-virtual {v1, p1}, Landroid/media/MediaMetadataRetriever;->setDataSource(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 84
    :goto_0
    return-void

    .line 81
    :catch_0
    move-exception v0

    .line 82
    .local v0, "ex":Ljava/lang/IllegalArgumentException;
    const-string v1, "SecMediaMetaDataRetriever"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "meta.setDataSource:IllegalArgumentException--filePath:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public extractMetadata(I)Ljava/lang/String;
    .locals 3
    .param p1, "keyCode"    # I

    .prologue
    .line 100
    const/4 v0, 0x0

    .line 101
    .local v0, "metadata":Ljava/lang/String;
    sparse-switch p1, :sswitch_data_0

    .line 133
    :cond_0
    :goto_0
    return-object v0

    .line 103
    :sswitch_0
    iget-object v1, p0, Lcom/samsung/musicplus/library/metadata/SecMediaMetaDataRetriever;->meta:Landroid/media/MediaMetadataRetriever;

    const/4 v2, 0x7

    invoke-virtual {v1, v2}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v0

    .line 104
    if-nez v0, :cond_0

    .line 105
    iget-object v1, p0, Lcom/samsung/musicplus/library/metadata/SecMediaMetaDataRetriever;->meta:Landroid/media/MediaMetadataRetriever;

    const/16 v2, 0x1f

    invoke-virtual {v1, v2}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 109
    :sswitch_1
    iget-object v1, p0, Lcom/samsung/musicplus/library/metadata/SecMediaMetaDataRetriever;->meta:Landroid/media/MediaMetadataRetriever;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v0

    .line 110
    if-nez v0, :cond_0

    .line 111
    iget-object v1, p0, Lcom/samsung/musicplus/library/metadata/SecMediaMetaDataRetriever;->meta:Landroid/media/MediaMetadataRetriever;

    const/16 v2, 0x1a

    invoke-virtual {v1, v2}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 115
    :sswitch_2
    iget-object v1, p0, Lcom/samsung/musicplus/library/metadata/SecMediaMetaDataRetriever;->meta:Landroid/media/MediaMetadataRetriever;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v0

    .line 116
    if-nez v0, :cond_0

    .line 117
    iget-object v1, p0, Lcom/samsung/musicplus/library/metadata/SecMediaMetaDataRetriever;->meta:Landroid/media/MediaMetadataRetriever;

    const/16 v2, 0x19

    invoke-virtual {v1, v2}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 121
    :sswitch_3
    iget-object v1, p0, Lcom/samsung/musicplus/library/metadata/SecMediaMetaDataRetriever;->meta:Landroid/media/MediaMetadataRetriever;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v0

    .line 122
    if-nez v0, :cond_0

    .line 123
    iget-object v1, p0, Lcom/samsung/musicplus/library/metadata/SecMediaMetaDataRetriever;->meta:Landroid/media/MediaMetadataRetriever;

    const/16 v2, 0x1c

    invoke-virtual {v1, v2}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 127
    :sswitch_4
    iget-object v1, p0, Lcom/samsung/musicplus/library/metadata/SecMediaMetaDataRetriever;->meta:Landroid/media/MediaMetadataRetriever;

    const/16 v2, 0xd

    invoke-virtual {v1, v2}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v0

    .line 128
    if-nez v0, :cond_0

    .line 129
    iget-object v1, p0, Lcom/samsung/musicplus/library/metadata/SecMediaMetaDataRetriever;->meta:Landroid/media/MediaMetadataRetriever;

    const/16 v2, 0x21

    invoke-virtual {v1, v2}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 101
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_2
        0x2 -> :sswitch_1
        0x4 -> :sswitch_3
        0x7 -> :sswitch_0
        0xd -> :sswitch_4
    .end sparse-switch
.end method

.method public isUnicode()Z
    .locals 2

    .prologue
    .line 146
    iget-object v0, p0, Lcom/samsung/musicplus/library/metadata/SecMediaMetaDataRetriever;->meta:Landroid/media/MediaMetadataRetriever;

    const/16 v1, 0x1f

    invoke-virtual {v0, v1}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/library/metadata/SecMediaMetaDataRetriever;->meta:Landroid/media/MediaMetadataRetriever;

    const/16 v1, 0x19

    invoke-virtual {v0, v1}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/library/metadata/SecMediaMetaDataRetriever;->meta:Landroid/media/MediaMetadataRetriever;

    const/16 v1, 0x1a

    invoke-virtual {v0, v1}, Landroid/media/MediaMetadataRetriever;->extractMetadata(I)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 149
    const/4 v0, 0x1

    .line 151
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public release()V
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/samsung/musicplus/library/metadata/SecMediaMetaDataRetriever;->meta:Landroid/media/MediaMetadataRetriever;

    invoke-virtual {v0}, Landroid/media/MediaMetadataRetriever;->release()V

    .line 163
    return-void
.end method
