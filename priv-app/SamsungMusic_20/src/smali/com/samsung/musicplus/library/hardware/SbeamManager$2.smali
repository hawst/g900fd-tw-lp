.class Lcom/samsung/musicplus/library/hardware/SbeamManager$2;
.super Ljava/lang/Object;
.source "SbeamManager.java"

# interfaces
.implements Landroid/nfc/NfcAdapter$OnNdefPushCompleteCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/library/hardware/SbeamManager;-><init>(Landroid/app/Activity;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/library/hardware/SbeamManager;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/library/hardware/SbeamManager;)V
    .locals 0

    .prologue
    .line 174
    iput-object p1, p0, Lcom/samsung/musicplus/library/hardware/SbeamManager$2;->this$0:Lcom/samsung/musicplus/library/hardware/SbeamManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onNdefPushComplete(Landroid/nfc/NfcEvent;)V
    .locals 4
    .param p1, "event"    # Landroid/nfc/NfcEvent;

    .prologue
    .line 178
    iget-object v1, p0, Lcom/samsung/musicplus/library/hardware/SbeamManager$2;->this$0:Lcom/samsung/musicplus/library/hardware/SbeamManager;

    # getter for: Lcom/samsung/musicplus/library/hardware/SbeamManager;->mMode:I
    invoke-static {v1}, Lcom/samsung/musicplus/library/hardware/SbeamManager;->access$600(Lcom/samsung/musicplus/library/hardware/SbeamManager;)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 193
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.directshare.DIRECT_SHARE_START_ACTION"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 194
    .local v0, "i":Landroid/content/Intent;
    iget-object v1, p0, Lcom/samsung/musicplus/library/hardware/SbeamManager$2;->this$0:Lcom/samsung/musicplus/library/hardware/SbeamManager;

    # getter for: Lcom/samsung/musicplus/library/hardware/SbeamManager;->mActivity:Landroid/app/Activity;
    invoke-static {v1}, Lcom/samsung/musicplus/library/hardware/SbeamManager;->access$400(Lcom/samsung/musicplus/library/hardware/SbeamManager;)Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 197
    .end local v0    # "i":Landroid/content/Intent;
    :pswitch_0
    # getter for: Lcom/samsung/musicplus/library/hardware/SbeamManager;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/library/hardware/SbeamManager;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onNdefPushComplete mMode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/musicplus/library/hardware/SbeamManager$2;->this$0:Lcom/samsung/musicplus/library/hardware/SbeamManager;

    # getter for: Lcom/samsung/musicplus/library/hardware/SbeamManager;->mMode:I
    invoke-static {v3}, Lcom/samsung/musicplus/library/hardware/SbeamManager;->access$600(Lcom/samsung/musicplus/library/hardware/SbeamManager;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    return-void

    .line 178
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
