.class Lcom/samsung/musicplus/library/hardware/SimpleGestureManager$2;
.super Ljava/lang/Object;
.source "SimpleGestureManager.java"

# interfaces
.implements Lcom/samsung/android/service/gesture/GestureManager$ServiceConnectionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;->startGestureService(Ljava/lang/String;Ljava/lang/String;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;

.field final synthetic val$gestureProvider:Ljava/lang/String;

.field final synthetic val$gestureType:Ljava/lang/String;

.field final synthetic val$isSupportLandMode:Z


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 205
    iput-object p1, p0, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager$2;->this$0:Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;

    iput-object p2, p0, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager$2;->val$gestureProvider:Ljava/lang/String;

    iput-object p3, p0, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager$2;->val$gestureType:Ljava/lang/String;

    iput-boolean p4, p0, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager$2;->val$isSupportLandMode:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected()V
    .locals 5

    .prologue
    .line 213
    # getter for: Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;->access$100()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onServiceConnected mGestureManager : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager$2;->this$0:Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;

    # getter for: Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;->mGestureManager:Lcom/samsung/android/service/gesture/GestureManager;
    invoke-static {v2}, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;->access$200(Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;)Lcom/samsung/android/service/gesture/GestureManager;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mGestureListner : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager$2;->this$0:Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;

    # getter for: Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;->mGestureListner:Lcom/samsung/android/service/gesture/GestureListener;
    invoke-static {v2}, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;->access$300(Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;)Lcom/samsung/android/service/gesture/GestureListener;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    iget-object v0, p0, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager$2;->this$0:Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;

    const/4 v1, 0x1

    # setter for: Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;->mConnected:Z
    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;->access$402(Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;Z)Z

    .line 216
    iget-object v0, p0, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager$2;->this$0:Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;

    # getter for: Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;->mGestureManager:Lcom/samsung/android/service/gesture/GestureManager;
    invoke-static {v0}, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;->access$200(Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;)Lcom/samsung/android/service/gesture/GestureManager;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager$2;->this$0:Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;

    # getter for: Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;->mGestureListner:Lcom/samsung/android/service/gesture/GestureListener;
    invoke-static {v1}, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;->access$300(Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;)Lcom/samsung/android/service/gesture/GestureListener;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager$2;->val$gestureProvider:Ljava/lang/String;

    iget-object v3, p0, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager$2;->val$gestureType:Ljava/lang/String;

    iget-boolean v4, p0, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager$2;->val$isSupportLandMode:Z

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/samsung/android/service/gesture/GestureManager;->registerListener(Lcom/samsung/android/service/gesture/GestureListener;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 219
    return-void
.end method

.method public onServiceDisconnected()V
    .locals 3

    .prologue
    .line 231
    # getter for: Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;->access$100()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onServiceDisconnected mGestureManager : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager$2;->this$0:Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;

    # getter for: Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;->mGestureManager:Lcom/samsung/android/service/gesture/GestureManager;
    invoke-static {v2}, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;->access$200(Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;)Lcom/samsung/android/service/gesture/GestureManager;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    iget-object v0, p0, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager$2;->this$0:Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;->mConnected:Z
    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;->access$402(Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;Z)Z

    .line 233
    return-void
.end method
