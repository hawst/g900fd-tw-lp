.class public Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;
.super Ljava/lang/Object;
.source "SimpleAVPlayerManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$OnCompletionListener;,
        Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$OnErrorListener;,
        Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$OnStateChangedListener;
    }
.end annotation


# static fields
.field public static final AV_PLAYER_ERROR_PLAYER_IS_NOT_AVAILABLE:I = 0x1

.field public static final AV_PLAYER_ERROR_UNABLE_TO_PLAY:I = 0x0

.field public static final AV_PLAYER_ERROR_UNKNOWN:I = 0x2

.field public static final AV_PLAYER_STATE_BUFFERING:I = 0x4

.field public static final AV_PLAYER_STATE_IDLE:I = 0x0

.field public static final AV_PLAYER_STATE_PAUSED:I = 0x2

.field public static final AV_PLAYER_STATE_PLAYING:I = 0x1

.field public static final AV_PLAYER_STATE_REQUESTED:I = 0x5

.field public static final AV_PLAYER_STATE_REQUESTED_CONFIRM:I = 0x6

.field public static final AV_PLAYER_STATE_STOPPED:I = 0x3

.field private static final CLASSNAME:Ljava/lang/String;

.field private static final DEBUG:Z = true

.field private static final DRM_PROTECTED_ERROR:I = -0x1

.field private static final DRM_PROTECTED_FALSE:I = 0x0

.field private static final DRM_PROTECTED_TRUE:I = 0x1

.field private static final REQUEST_AVPLAYER_DUARTION:I = 0x1

.field private static final TAG:Ljava/lang/String; = "MusicPlayer"


# instance fields
.field private mAVPlayerCurrentPosition:J

.field private final mAVPlayerPlaybackResponseListener:Lcom/samsung/android/allshare/media/AVPlayer$IAVPlayerPlaybackResponseListener;

.field private mAVPlayerState:I

.field private final mContext:Landroid/content/Context;

.field private mDoNotUpdateProgress:Z

.field private final mDrmChecker:Lcom/samsung/android/allshare/extension/RemoteDrmChecker;

.field private mDuration:J

.field private final mHandler:Landroid/os/Handler;

.field private mItem:Lcom/samsung/android/allshare/Item;

.field private mOnCompletionListener:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$OnCompletionListener;

.field private mOnErrorListener:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$OnErrorListener;

.field private mOnStateChangedListener:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$OnStateChangedListener;

.field private final mSECAVPlayerStateListener:Lcom/samsung/android/allshare/extension/SECAVPlayer$ISECAVPlayerStateListener;

.field private mSimpleAVPlayer:Lcom/samsung/musicplus/library/dlna/DlnaManager$SimpleAVPlayer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->CLASSNAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/samsung/musicplus/library/dlna/DlnaManager$SimpleAVPlayer;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "simpleAVPlayer"    # Lcom/samsung/musicplus/library/dlna/DlnaManager$SimpleAVPlayer;

    .prologue
    const/4 v0, 0x0

    .line 181
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 156
    iput v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mAVPlayerState:I

    .line 158
    iput-boolean v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mDoNotUpdateProgress:Z

    .line 160
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mDuration:J

    .line 162
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mAVPlayerCurrentPosition:J

    .line 529
    new-instance v0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$1;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$1;-><init>(Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;)V

    iput-object v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mHandler:Landroid/os/Handler;

    .line 735
    new-instance v0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$2;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$2;-><init>(Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;)V

    iput-object v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mAVPlayerPlaybackResponseListener:Lcom/samsung/android/allshare/media/AVPlayer$IAVPlayerPlaybackResponseListener;

    .line 841
    new-instance v0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$3;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$3;-><init>(Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;)V

    iput-object v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mSECAVPlayerStateListener:Lcom/samsung/android/allshare/extension/SECAVPlayer$ISECAVPlayerStateListener;

    .line 182
    iput-object p1, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mContext:Landroid/content/Context;

    .line 183
    new-instance v0, Lcom/samsung/android/allshare/extension/RemoteDrmChecker;

    iget-object v1, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/android/allshare/extension/RemoteDrmChecker;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mDrmChecker:Lcom/samsung/android/allshare/extension/RemoteDrmChecker;

    .line 184
    if-nez p2, :cond_0

    .line 191
    :goto_0
    return-void

    .line 187
    :cond_0
    sget-object v0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "SimpleAVPlayerManager() - simpleAVPlayer: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 188
    iput-object p2, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mSimpleAVPlayer:Lcom/samsung/musicplus/library/dlna/DlnaManager$SimpleAVPlayer;

    .line 189
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mSimpleAVPlayer:Lcom/samsung/musicplus/library/dlna/DlnaManager$SimpleAVPlayer;

    iget-object v1, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mSECAVPlayerStateListener:Lcom/samsung/android/allshare/extension/SECAVPlayer$ISECAVPlayerStateListener;

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/library/dlna/DlnaManager$SimpleAVPlayer;->setSmartAVPlayerEventListener(Lcom/samsung/android/allshare/extension/SECAVPlayer$ISECAVPlayerStateListener;)V

    .line 190
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mSimpleAVPlayer:Lcom/samsung/musicplus/library/dlna/DlnaManager$SimpleAVPlayer;

    iget-object v1, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mAVPlayerPlaybackResponseListener:Lcom/samsung/android/allshare/media/AVPlayer$IAVPlayerPlaybackResponseListener;

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/library/dlna/DlnaManager$SimpleAVPlayer;->setResponseListener(Lcom/samsung/android/allshare/media/AVPlayer$IAVPlayerPlaybackResponseListener;)V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;)Lcom/samsung/musicplus/library/dlna/DlnaManager$SimpleAVPlayer;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mSimpleAVPlayer:Lcom/samsung/musicplus/library/dlna/DlnaManager$SimpleAVPlayer;

    return-object v0
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->CLASSNAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;)Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$OnCompletionListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mOnCompletionListener:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$OnCompletionListener;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;Lcom/samsung/android/allshare/ERROR;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;
    .param p1, "x1"    # Lcom/samsung/android/allshare/ERROR;

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->onError(Lcom/samsung/android/allshare/ERROR;)V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mDoNotUpdateProgress:Z

    return v0
.end method

.method static synthetic access$202(Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;
    .param p1, "x1"    # Z

    .prologue
    .line 33
    iput-boolean p1, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mDoNotUpdateProgress:Z

    return p1
.end method

.method static synthetic access$300(Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    .prologue
    .line 33
    iget v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mAVPlayerState:I

    return v0
.end method

.method static synthetic access$302(Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;
    .param p1, "x1"    # I

    .prologue
    .line 33
    iput p1, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mAVPlayerState:I

    return p1
.end method

.method static synthetic access$400(Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;
    .param p1, "x1"    # I

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->onStateChanged(I)V

    return-void
.end method

.method static synthetic access$500(Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;Lcom/samsung/android/allshare/ERROR;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;
    .param p1, "x1"    # Lcom/samsung/android/allshare/ERROR;

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->onPlayResponseReceivedError(Lcom/samsung/android/allshare/ERROR;)V

    return-void
.end method

.method static synthetic access$600(Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;)J
    .locals 2
    .param p0, "x0"    # Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    .prologue
    .line 33
    iget-wide v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mDuration:J

    return-wide v0
.end method

.method static synthetic access$602(Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;J)J
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;
    .param p1, "x1"    # J

    .prologue
    .line 33
    iput-wide p1, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mDuration:J

    return-wide p1
.end method

.method static synthetic access$700(Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$800(Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;)Lcom/samsung/android/allshare/Item;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mItem:Lcom/samsung/android/allshare/Item;

    return-object v0
.end method

.method static synthetic access$902(Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;J)J
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;
    .param p1, "x1"    # J

    .prologue
    .line 33
    iput-wide p1, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mAVPlayerCurrentPosition:J

    return-wide p1
.end method

.method private getItem(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/android/allshare/Item;
    .locals 3
    .param p1, "uriString"    # Ljava/lang/String;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "mimeType"    # Ljava/lang/String;

    .prologue
    .line 258
    const-string v2, "content://"

    invoke-virtual {p1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 259
    new-instance v0, Lcom/samsung/android/allshare/Item$LocalContentBuilder;

    const/4 v2, 0x0

    invoke-direct {v0, p1, v2}, Lcom/samsung/android/allshare/Item$LocalContentBuilder;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    .local v0, "builder":Lcom/samsung/android/allshare/Item$LocalContentBuilder;
    invoke-virtual {v0}, Lcom/samsung/android/allshare/Item$LocalContentBuilder;->build()Lcom/samsung/android/allshare/Item;

    move-result-object v1

    .line 283
    .end local v0    # "builder":Lcom/samsung/android/allshare/Item$LocalContentBuilder;
    .local v1, "item":Lcom/samsung/android/allshare/Item;
    :goto_0
    return-object v1

    .line 261
    .end local v1    # "item":Lcom/samsung/android/allshare/Item;
    :cond_0
    const-string v2, "http://"

    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 263
    new-instance v0, Lcom/samsung/android/allshare/Item$WebContentBuilder;

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v0, v2, p3}, Lcom/samsung/android/allshare/Item$WebContentBuilder;-><init>(Landroid/net/Uri;Ljava/lang/String;)V

    .line 265
    .local v0, "builder":Lcom/samsung/android/allshare/Item$WebContentBuilder;
    if-eqz p2, :cond_1

    .line 266
    invoke-virtual {v0, p2}, Lcom/samsung/android/allshare/Item$WebContentBuilder;->setTitle(Ljava/lang/String;)Lcom/samsung/android/allshare/Item$WebContentBuilder;

    .line 268
    :cond_1
    invoke-virtual {v0}, Lcom/samsung/android/allshare/Item$WebContentBuilder;->build()Lcom/samsung/android/allshare/Item;

    move-result-object v1

    .line 269
    .restart local v1    # "item":Lcom/samsung/android/allshare/Item;
    goto :goto_0

    .line 270
    .end local v0    # "builder":Lcom/samsung/android/allshare/Item$WebContentBuilder;
    .end local v1    # "item":Lcom/samsung/android/allshare/Item;
    :cond_2
    const-string v2, "file%3A"

    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 273
    invoke-static {p1}, Landroid/net/Uri;->decode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->getPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    .line 277
    :cond_3
    :goto_1
    new-instance v0, Lcom/samsung/android/allshare/Item$LocalContentBuilder;

    invoke-direct {v0, p1, p3}, Lcom/samsung/android/allshare/Item$LocalContentBuilder;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    .local v0, "builder":Lcom/samsung/android/allshare/Item$LocalContentBuilder;
    if-eqz p2, :cond_4

    .line 279
    invoke-virtual {v0, p2}, Lcom/samsung/android/allshare/Item$LocalContentBuilder;->setTitle(Ljava/lang/String;)Lcom/samsung/android/allshare/Item$LocalContentBuilder;

    .line 281
    :cond_4
    invoke-virtual {v0}, Lcom/samsung/android/allshare/Item$LocalContentBuilder;->build()Lcom/samsung/android/allshare/Item;

    move-result-object v1

    .restart local v1    # "item":Lcom/samsung/android/allshare/Item;
    goto :goto_0

    .line 274
    .end local v0    # "builder":Lcom/samsung/android/allshare/Item$LocalContentBuilder;
    .end local v1    # "item":Lcom/samsung/android/allshare/Item;
    :cond_5
    const-string v2, "file://"

    invoke-virtual {p1, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 275
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->getPath(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p1

    goto :goto_1
.end method

.method private getPath(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "uri"    # Ljava/lang/String;

    .prologue
    .line 447
    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v0

    .line 448
    .local v0, "mnt":Ljava/lang/String;
    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method private onError(Lcom/samsung/android/allshare/ERROR;)V
    .locals 3
    .param p1, "err"    # Lcom/samsung/android/allshare/ERROR;

    .prologue
    .line 660
    sget-object v0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onError() - error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mOnErrorListener: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mOnErrorListener:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$OnErrorListener;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 661
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mOnErrorListener:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$OnErrorListener;

    if-nez v0, :cond_0

    .line 672
    :goto_0
    return-void

    .line 664
    :cond_0
    sget-object v0, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/ERROR;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/samsung/android/allshare/ERROR;->ITEM_NOT_EXIST:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/ERROR;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/samsung/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/ERROR;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/samsung/android/allshare/ERROR;->INVALID_STATE:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/ERROR;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 666
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mOnErrorListener:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$OnErrorListener;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$OnErrorListener;->onError(I)V

    goto :goto_0

    .line 667
    :cond_2
    sget-object v0, Lcom/samsung/android/allshare/ERROR;->NETWORK_NOT_AVAILABLE:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/ERROR;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    sget-object v0, Lcom/samsung/android/allshare/ERROR;->INVALID_DEVICE:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/ERROR;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 668
    :cond_3
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mOnErrorListener:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$OnErrorListener;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$OnErrorListener;->onError(I)V

    goto :goto_0

    .line 670
    :cond_4
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mOnErrorListener:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$OnErrorListener;

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$OnErrorListener;->onError(I)V

    goto :goto_0
.end method

.method private onPlayResponseReceivedError(Lcom/samsung/android/allshare/ERROR;)V
    .locals 3
    .param p1, "err"    # Lcom/samsung/android/allshare/ERROR;

    .prologue
    .line 675
    sget-object v0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onPlayResponseReceivedError() - error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mOnErrorListener: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mOnErrorListener:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$OnErrorListener;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 678
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mOnErrorListener:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$OnErrorListener;

    if-nez v0, :cond_0

    .line 692
    :goto_0
    return-void

    .line 682
    :cond_0
    sget-object v0, Lcom/samsung/android/allshare/ERROR;->FAIL:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/ERROR;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/samsung/android/allshare/ERROR;->ITEM_NOT_EXIST:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/ERROR;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/samsung/android/allshare/ERROR;->INVALID_ARGUMENT:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/ERROR;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/samsung/android/allshare/ERROR;->INVALID_STATE:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/ERROR;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 684
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mOnErrorListener:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$OnErrorListener;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$OnErrorListener;->onPlayResponseReceivedError(I)V

    goto :goto_0

    .line 685
    :cond_2
    sget-object v0, Lcom/samsung/android/allshare/ERROR;->NETWORK_NOT_AVAILABLE:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/ERROR;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    sget-object v0, Lcom/samsung/android/allshare/ERROR;->INVALID_DEVICE:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/ERROR;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 686
    :cond_3
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mOnErrorListener:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$OnErrorListener;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$OnErrorListener;->onPlayResponseReceivedError(I)V

    goto :goto_0

    .line 688
    :cond_4
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mOnErrorListener:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$OnErrorListener;

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$OnErrorListener;->onPlayResponseReceivedError(I)V

    goto :goto_0
.end method

.method private onStateChanged(I)V
    .locals 3
    .param p1, "state"    # I

    .prologue
    .line 695
    sget-object v0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onStateChanged() - state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mOnStateChangedListener: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mOnStateChangedListener:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$OnStateChangedListener;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 697
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mOnStateChangedListener:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$OnStateChangedListener;

    if-eqz v0, :cond_0

    .line 698
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mOnStateChangedListener:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$OnStateChangedListener;

    invoke-interface {v0, p1}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$OnStateChangedListener;->onStateChanged(I)V

    .line 700
    :cond_0
    return-void
.end method

.method private prepare(Lcom/samsung/android/allshare/Item;)V
    .locals 1
    .param p1, "item"    # Lcom/samsung/android/allshare/Item;

    .prologue
    .line 360
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mSimpleAVPlayer:Lcom/samsung/musicplus/library/dlna/DlnaManager$SimpleAVPlayer;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/library/dlna/DlnaManager$SimpleAVPlayer;->prepare(Lcom/samsung/android/allshare/Item;)V

    .line 361
    return-void
.end method

.method private start(Lcom/samsung/android/allshare/Item;J)V
    .locals 6
    .param p1, "item"    # Lcom/samsung/android/allshare/Item;
    .param p2, "msec"    # J

    .prologue
    const/4 v5, 0x5

    .line 287
    sget-object v2, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->CLASSNAME:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "play - item: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " position: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " msec"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " state: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mAVPlayerState:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 289
    iget-object v2, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mSimpleAVPlayer:Lcom/samsung/musicplus/library/dlna/DlnaManager$SimpleAVPlayer;

    if-nez v2, :cond_0

    .line 290
    sget-object v2, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->CLASSNAME:Ljava/lang/String;

    const-string v3, "start() - mSimpleAVPlayer == null. So return "

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 312
    :goto_0
    return-void

    .line 294
    :cond_0
    iget v2, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mAVPlayerState:I

    if-ne v2, v5, :cond_1

    .line 297
    sget-object v2, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->CLASSNAME:Ljava/lang/String;

    const-string v3, "play() - AVPlayerState is requested, so return"

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 300
    :cond_1
    const/4 v2, 0x0

    iput v2, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mAVPlayerState:I

    .line 301
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mDuration:J

    .line 302
    iput-wide p2, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mAVPlayerCurrentPosition:J

    .line 303
    iput-object p1, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mItem:Lcom/samsung/android/allshare/Item;

    .line 305
    new-instance v1, Lcom/samsung/android/allshare/media/ContentInfo$Builder;

    invoke-direct {v1}, Lcom/samsung/android/allshare/media/ContentInfo$Builder;-><init>()V

    .line 306
    .local v1, "contentInfoBuilder":Lcom/samsung/android/allshare/media/ContentInfo$Builder;
    const-wide/16 v2, 0x3e8

    div-long v2, p2, v2

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/allshare/media/ContentInfo$Builder;->setStartingPosition(J)Lcom/samsung/android/allshare/media/ContentInfo$Builder;

    .line 307
    invoke-virtual {v1}, Lcom/samsung/android/allshare/media/ContentInfo$Builder;->build()Lcom/samsung/android/allshare/media/ContentInfo;

    move-result-object v0

    .line 309
    .local v0, "contentInfo":Lcom/samsung/android/allshare/media/ContentInfo;
    iget-object v2, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mSimpleAVPlayer:Lcom/samsung/musicplus/library/dlna/DlnaManager$SimpleAVPlayer;

    invoke-virtual {v2, p1, v0}, Lcom/samsung/musicplus/library/dlna/DlnaManager$SimpleAVPlayer;->play(Lcom/samsung/android/allshare/Item;Lcom/samsung/android/allshare/media/ContentInfo;)V

    .line 310
    iput v5, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mAVPlayerState:I

    .line 311
    iget v2, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mAVPlayerState:I

    invoke-direct {p0, v2}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->onStateChanged(I)V

    goto :goto_0
.end method


# virtual methods
.method public getAVPlayerState()I
    .locals 1

    .prologue
    .line 457
    iget v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mAVPlayerState:I

    return v0
.end method

.method public getCurrentPosition()J
    .locals 2

    .prologue
    .line 500
    iget-wide v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mAVPlayerCurrentPosition:J

    return-wide v0
.end method

.method public getDuration()J
    .locals 2

    .prologue
    .line 513
    iget-wide v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mDuration:J

    return-wide v0
.end method

.method public isDrmFile(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 5
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "extension"    # Ljava/lang/String;
    .param p3, "mimeType"    # Ljava/lang/String;

    .prologue
    .line 552
    const/4 v1, 0x0

    .line 554
    .local v1, "isDrmFile":Z
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-nez p3, :cond_1

    .line 555
    :cond_0
    const/4 v2, 0x1

    .line 576
    :goto_0
    return v2

    .line 558
    :cond_1
    iget-object v2, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mDrmChecker:Lcom/samsung/android/allshare/extension/RemoteDrmChecker;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, p2, p3}, Lcom/samsung/android/allshare/extension/RemoteDrmChecker;->isDrmFile(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 559
    .local v0, "drmResult":I
    packed-switch v0, :pswitch_data_0

    .line 575
    :goto_1
    sget-object v2, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->CLASSNAME:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isDrmFile() - result:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " isDrmFile : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v2, v1

    .line 576
    goto :goto_0

    .line 561
    :pswitch_0
    sget-object v2, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->CLASSNAME:Ljava/lang/String;

    const-string v3, "isDrmFile() - result: DRM_PROTECTED_ERROR"

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 562
    const/4 v1, 0x1

    .line 563
    goto :goto_1

    .line 565
    :pswitch_1
    sget-object v2, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->CLASSNAME:Ljava/lang/String;

    const-string v3, "isDrmFile() - result: DRM_PROTECTED_TRUE"

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 566
    const/4 v1, 0x1

    .line 567
    goto :goto_1

    .line 569
    :pswitch_2
    sget-object v2, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->CLASSNAME:Ljava/lang/String;

    const-string v3, "isDrmFile() - result: DRM_PROTECTED_FALSE"

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 570
    const/4 v1, 0x0

    .line 571
    goto :goto_1

    .line 559
    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public pause()V
    .locals 3

    .prologue
    .line 406
    sget-object v0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "pause() - state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mAVPlayerState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 407
    iget v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mAVPlayerState:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 408
    sget-object v0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->CLASSNAME:Ljava/lang/String;

    const-string v1, " ignore pause() already pause state"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 412
    :goto_0
    return-void

    .line 411
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mSimpleAVPlayer:Lcom/samsung/musicplus/library/dlna/DlnaManager$SimpleAVPlayer;

    invoke-virtual {v0}, Lcom/samsung/musicplus/library/dlna/DlnaManager$SimpleAVPlayer;->pause()V

    goto :goto_0
.end method

.method public play(Landroid/net/Uri;J)V
    .locals 4
    .param p1, "contentUri"    # Landroid/net/Uri;
    .param p2, "msec"    # J

    .prologue
    .line 224
    new-instance v0, Lcom/samsung/android/allshare/Item$LocalContentBuilder;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/allshare/Item$LocalContentBuilder;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/samsung/android/allshare/Item$LocalContentBuilder;->build()Lcom/samsung/android/allshare/Item;

    move-result-object v0

    invoke-direct {p0, v0, p2, p3}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->start(Lcom/samsung/android/allshare/Item;J)V

    .line 225
    return-void
.end method

.method public play(Ljava/lang/String;J)V
    .locals 2
    .param p1, "seed"    # Ljava/lang/String;
    .param p2, "msec"    # J

    .prologue
    .line 237
    invoke-static {p1}, Lcom/samsung/android/allshare/extension/ItemExtractor;->create(Ljava/lang/String;)Lcom/samsung/android/allshare/Item;

    move-result-object v0

    invoke-direct {p0, v0, p2, p3}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->start(Lcom/samsung/android/allshare/Item;J)V

    .line 238
    return-void
.end method

.method public play(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 2
    .param p1, "uriString"    # Ljava/lang/String;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "mimeType"    # Ljava/lang/String;
    .param p4, "msec"    # J

    .prologue
    .line 253
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->getItem(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/android/allshare/Item;

    move-result-object v0

    invoke-direct {p0, v0, p4, p5}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->start(Lcom/samsung/android/allshare/Item;J)V

    .line 254
    return-void
.end method

.method public prepare(Landroid/net/Uri;)V
    .locals 3
    .param p1, "contentUri"    # Landroid/net/Uri;

    .prologue
    .line 325
    new-instance v0, Lcom/samsung/android/allshare/Item$LocalContentBuilder;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/allshare/Item$LocalContentBuilder;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/samsung/android/allshare/Item$LocalContentBuilder;->build()Lcom/samsung/android/allshare/Item;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->prepare(Lcom/samsung/android/allshare/Item;)V

    .line 326
    return-void
.end method

.method public prepare(Ljava/lang/String;)V
    .locals 1
    .param p1, "seed"    # Ljava/lang/String;

    .prologue
    .line 339
    invoke-static {p1}, Lcom/samsung/android/allshare/extension/ItemExtractor;->create(Ljava/lang/String;)Lcom/samsung/android/allshare/Item;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->prepare(Lcom/samsung/android/allshare/Item;)V

    .line 340
    return-void
.end method

.method public prepare(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "uriString"    # Ljava/lang/String;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "mimeType"    # Ljava/lang/String;

    .prologue
    .line 356
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->getItem(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/android/allshare/Item;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->prepare(Lcom/samsung/android/allshare/Item;)V

    .line 357
    return-void
.end method

.method public releaseSecAVPlayer()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 200
    sget-object v0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->CLASSNAME:Ljava/lang/String;

    const-string v2, "releaseSecAVPlayer()"

    invoke-static {v0, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mAVPlayerState:I

    .line 202
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mOnStateChangedListener:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$OnStateChangedListener;

    iget v2, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mAVPlayerState:I

    invoke-interface {v0, v2}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$OnStateChangedListener;->onStateChanged(I)V

    .line 203
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mSimpleAVPlayer:Lcom/samsung/musicplus/library/dlna/DlnaManager$SimpleAVPlayer;

    if-eqz v0, :cond_0

    .line 204
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mSimpleAVPlayer:Lcom/samsung/musicplus/library/dlna/DlnaManager$SimpleAVPlayer;

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/library/dlna/DlnaManager$SimpleAVPlayer;->setSmartAVPlayerEventListener(Lcom/samsung/android/allshare/extension/SECAVPlayer$ISECAVPlayerStateListener;)V

    .line 205
    iget-object v2, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mSimpleAVPlayer:Lcom/samsung/musicplus/library/dlna/DlnaManager$SimpleAVPlayer;

    move-object v0, v1

    check-cast v0, Lcom/samsung/android/allshare/media/AVPlayer$IAVPlayerPlaybackResponseListener;

    invoke-virtual {v2, v0}, Lcom/samsung/musicplus/library/dlna/DlnaManager$SimpleAVPlayer;->setResponseListener(Lcom/samsung/android/allshare/media/AVPlayer$IAVPlayerPlaybackResponseListener;)V

    .line 206
    iput-object v1, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mSimpleAVPlayer:Lcom/samsung/musicplus/library/dlna/DlnaManager$SimpleAVPlayer;

    .line 208
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 209
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mDuration:J

    .line 210
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mAVPlayerCurrentPosition:J

    .line 211
    iput-object v1, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mItem:Lcom/samsung/android/allshare/Item;

    .line 212
    return-void
.end method

.method public resume()V
    .locals 6

    .prologue
    .line 421
    sget-object v2, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->CLASSNAME:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "resume() - state: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mAVPlayerState:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 422
    iget v2, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mAVPlayerState:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 423
    sget-object v2, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->CLASSNAME:Ljava/lang/String;

    const-string v3, " ignore resume() already playing state"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 438
    :goto_0
    return-void

    .line 426
    :cond_0
    iget v2, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mAVPlayerState:I

    const/4 v3, 0x3

    if-ne v2, v3, :cond_2

    .line 430
    const-wide/16 v0, 0x0

    .line 431
    .local v0, "position":J
    iget-wide v2, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mAVPlayerCurrentPosition:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-lez v2, :cond_1

    .line 432
    iget-wide v2, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mAVPlayerCurrentPosition:J

    const-wide/16 v4, 0x3e8

    div-long v0, v2, v4

    .line 434
    :cond_1
    iget-object v2, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mItem:Lcom/samsung/android/allshare/Item;

    invoke-direct {p0, v2, v0, v1}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->start(Lcom/samsung/android/allshare/Item;J)V

    goto :goto_0

    .line 436
    .end local v0    # "position":J
    :cond_2
    iget-object v2, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mSimpleAVPlayer:Lcom/samsung/musicplus/library/dlna/DlnaManager$SimpleAVPlayer;

    invoke-virtual {v2}, Lcom/samsung/musicplus/library/dlna/DlnaManager$SimpleAVPlayer;->resume()V

    goto :goto_0
.end method

.method public seek(J)V
    .locals 5
    .param p1, "msec"    # J

    .prologue
    .line 393
    sget-object v0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "seek() - targetTime: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 394
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mDoNotUpdateProgress:Z

    .line 395
    iput-wide p1, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mAVPlayerCurrentPosition:J

    .line 396
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mSimpleAVPlayer:Lcom/samsung/musicplus/library/dlna/DlnaManager$SimpleAVPlayer;

    const-wide/16 v2, 0x3e8

    div-long v2, p1, v2

    invoke-virtual {v0, v2, v3}, Lcom/samsung/musicplus/library/dlna/DlnaManager$SimpleAVPlayer;->seek(J)V

    .line 397
    return-void
.end method

.method public setMute()V
    .locals 1

    .prologue
    .line 487
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mSimpleAVPlayer:Lcom/samsung/musicplus/library/dlna/DlnaManager$SimpleAVPlayer;

    invoke-virtual {v0}, Lcom/samsung/musicplus/library/dlna/DlnaManager$SimpleAVPlayer;->changeMute()V

    .line 488
    return-void
.end method

.method public setOnCompletionListener(Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$OnCompletionListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$OnCompletionListener;

    .prologue
    .line 731
    iput-object p1, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mOnCompletionListener:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$OnCompletionListener;

    .line 732
    return-void
.end method

.method public setOnErrorListener(Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$OnErrorListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$OnErrorListener;

    .prologue
    .line 656
    iput-object p1, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mOnErrorListener:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$OnErrorListener;

    .line 657
    return-void
.end method

.method public setOnStateChangedListener(Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$OnStateChangedListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$OnStateChangedListener;

    .prologue
    .line 617
    iput-object p1, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mOnStateChangedListener:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$OnStateChangedListener;

    .line 618
    return-void
.end method

.method public stop()V
    .locals 3

    .prologue
    .line 371
    sget-object v0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "stop() - state : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mAVPlayerState:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 372
    iget v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mAVPlayerState:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mAVPlayerState:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mAVPlayerState:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mAVPlayerState:I

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mAVPlayerState:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_1

    .line 376
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mSimpleAVPlayer:Lcom/samsung/musicplus/library/dlna/DlnaManager$SimpleAVPlayer;

    invoke-virtual {v0}, Lcom/samsung/musicplus/library/dlna/DlnaManager$SimpleAVPlayer;->stop()V

    .line 377
    const/4 v0, 0x3

    iput v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mAVPlayerState:I

    .line 378
    iget v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mAVPlayerState:I

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->onStateChanged(I)V

    .line 380
    :cond_1
    return-void
.end method

.method public supportSeekOnPausedState()Z
    .locals 1

    .prologue
    .line 526
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mSimpleAVPlayer:Lcom/samsung/musicplus/library/dlna/DlnaManager$SimpleAVPlayer;

    invoke-virtual {v0}, Lcom/samsung/musicplus/library/dlna/DlnaManager$SimpleAVPlayer;->isSeekableOnPaused()Z

    move-result v0

    return v0
.end method

.method public volumeDown()V
    .locals 1

    .prologue
    .line 477
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mSimpleAVPlayer:Lcom/samsung/musicplus/library/dlna/DlnaManager$SimpleAVPlayer;

    invoke-virtual {v0}, Lcom/samsung/musicplus/library/dlna/DlnaManager$SimpleAVPlayer;->volumeDown()V

    .line 478
    return-void
.end method

.method public volumeUp()V
    .locals 1

    .prologue
    .line 467
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mSimpleAVPlayer:Lcom/samsung/musicplus/library/dlna/DlnaManager$SimpleAVPlayer;

    invoke-virtual {v0}, Lcom/samsung/musicplus/library/dlna/DlnaManager$SimpleAVPlayer;->volumeUp()V

    .line 468
    return-void
.end method
