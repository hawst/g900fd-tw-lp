.class public Lcom/samsung/musicplus/library/wifi/P2pManager;
.super Ljava/lang/Object;
.source "P2pManager.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field public static final ADDRESS_INFO:I = 0x1

.field private static final CLASSNAME:Ljava/lang/String;

.field private static final DEFAULT_WFD_DEVICE_TYPE:I = 0x9

.field public static final NAME_INFO:I = 0x2

.field public static final TYPE_INFO:I = 0x3

.field public static final WFD_DONGLE_TYPE:I

.field private static sManager:Lcom/samsung/musicplus/library/wifi/P2pManager;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mP2pChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

.field private mP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/samsung/musicplus/library/wifi/P2pManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/library/wifi/P2pManager;->CLASSNAME:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91
    const-string v0, "wifip2p"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/p2p/WifiP2pManager;

    iput-object v0, p0, Lcom/samsung/musicplus/library/wifi/P2pManager;->mP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    .line 92
    iget-object v0, p0, Lcom/samsung/musicplus/library/wifi/P2pManager;->mP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    if-eqz v0, :cond_0

    .line 93
    iget-object v0, p0, Lcom/samsung/musicplus/library/wifi/P2pManager;->mP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Landroid/net/wifi/p2p/WifiP2pManager;->initialize(Landroid/content/Context;Landroid/os/Looper;Landroid/net/wifi/p2p/WifiP2pManager$ChannelListener;)Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/library/wifi/P2pManager;->mP2pChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    .line 94
    iput-object p1, p0, Lcom/samsung/musicplus/library/wifi/P2pManager;->mContext:Landroid/content/Context;

    .line 96
    :cond_0
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/samsung/musicplus/library/wifi/P2pManager;->CLASSNAME:Ljava/lang/String;

    return-object v0
.end method

.method public static getConnectedDeviceId(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 138
    const-string v0, ""

    return-object v0
.end method

.method public static getConnectedDeviceName(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 127
    const-string v0, ""

    return-object v0
.end method

.method public static getConnectedDeviceType(Landroid/content/Context;)I
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 148
    const/4 v0, -0x1

    return v0
.end method

.method private static getConnectedWfdDeviceInfo(Landroid/content/Context;I)Ljava/lang/String;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "infoType"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 114
    const-string v0, ""

    return-object v0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/samsung/musicplus/library/wifi/P2pManager;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 79
    sget-object v1, Lcom/samsung/musicplus/library/wifi/P2pManager;->sManager:Lcom/samsung/musicplus/library/wifi/P2pManager;

    if-nez v1, :cond_0

    .line 80
    new-instance v0, Lcom/samsung/musicplus/library/wifi/P2pManager;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/library/wifi/P2pManager;-><init>(Landroid/content/Context;)V

    .line 81
    .local v0, "pm":Lcom/samsung/musicplus/library/wifi/P2pManager;
    invoke-direct {v0}, Lcom/samsung/musicplus/library/wifi/P2pManager;->isInitialized()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 82
    sput-object v0, Lcom/samsung/musicplus/library/wifi/P2pManager;->sManager:Lcom/samsung/musicplus/library/wifi/P2pManager;

    .line 87
    .end local v0    # "pm":Lcom/samsung/musicplus/library/wifi/P2pManager;
    :cond_0
    :goto_0
    sget-object v1, Lcom/samsung/musicplus/library/wifi/P2pManager;->sManager:Lcom/samsung/musicplus/library/wifi/P2pManager;

    return-object v1

    .line 84
    .restart local v0    # "pm":Lcom/samsung/musicplus/library/wifi/P2pManager;
    :cond_1
    const/4 v1, 0x0

    sput-object v1, Lcom/samsung/musicplus/library/wifi/P2pManager;->sManager:Lcom/samsung/musicplus/library/wifi/P2pManager;

    goto :goto_0
.end method

.method public static getWfdDeviceType(Ljava/lang/String;)I
    .locals 1
    .param p0, "primaryDeviceTypeStr"    # Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 207
    const/4 v0, -0x1

    return v0
.end method

.method public static isDongle(I)Z
    .locals 1
    .param p0, "deviceType"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 169
    const/4 v0, 0x1

    return v0
.end method

.method private isInitialized()Z
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/samsung/musicplus/library/wifi/P2pManager;->mP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isWifiBusy(Landroid/net/wifi/p2p/WifiP2pDevice;)Z
    .locals 1
    .param p0, "dev"    # Landroid/net/wifi/p2p/WifiP2pDevice;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 446
    const/4 v0, 0x1

    return v0
.end method

.method public static isWifiDisplayDevice(Landroid/net/wifi/p2p/WifiP2pDevice;)Z
    .locals 1
    .param p0, "dev"    # Landroid/net/wifi/p2p/WifiP2pDevice;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 421
    const/4 v0, 0x1

    return v0
.end method

.method private makeConnConfig(Landroid/net/wifi/p2p/WifiP2pDevice;)Landroid/net/wifi/p2p/WifiP2pConfig;
    .locals 5
    .param p1, "p2pDevice"    # Landroid/net/wifi/p2p/WifiP2pDevice;

    .prologue
    const/4 v4, 0x0

    .line 263
    sget-object v1, Lcom/samsung/musicplus/library/wifi/P2pManager;->CLASSNAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "makeConnConfig() - deviceName: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 264
    new-instance v0, Landroid/net/wifi/p2p/WifiP2pConfig;

    invoke-direct {v0}, Landroid/net/wifi/p2p/WifiP2pConfig;-><init>()V

    .line 265
    .local v0, "connectConfig":Landroid/net/wifi/p2p/WifiP2pConfig;
    iget-object v1, p1, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Landroid/net/wifi/p2p/WifiP2pConfig;->deviceAddress:Ljava/lang/String;

    .line 267
    invoke-virtual {p1}, Landroid/net/wifi/p2p/WifiP2pDevice;->wpsPbcSupported()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 268
    iget-object v1, v0, Landroid/net/wifi/p2p/WifiP2pConfig;->wps:Landroid/net/wifi/WpsInfo;

    iput v4, v1, Landroid/net/wifi/WpsInfo;->setup:I

    .line 276
    :goto_0
    return-object v0

    .line 269
    :cond_0
    invoke-virtual {p1}, Landroid/net/wifi/p2p/WifiP2pDevice;->wpsKeypadSupported()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 270
    iget-object v1, v0, Landroid/net/wifi/p2p/WifiP2pConfig;->wps:Landroid/net/wifi/WpsInfo;

    const/4 v2, 0x2

    iput v2, v1, Landroid/net/wifi/WpsInfo;->setup:I

    goto :goto_0

    .line 271
    :cond_1
    invoke-virtual {p1}, Landroid/net/wifi/p2p/WifiP2pDevice;->wpsDisplaySupported()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 272
    iget-object v1, v0, Landroid/net/wifi/p2p/WifiP2pConfig;->wps:Landroid/net/wifi/WpsInfo;

    const/4 v2, 0x1

    iput v2, v1, Landroid/net/wifi/WpsInfo;->setup:I

    goto :goto_0

    .line 274
    :cond_2
    iget-object v1, v0, Landroid/net/wifi/p2p/WifiP2pConfig;->wps:Landroid/net/wifi/WpsInfo;

    iput v4, v1, Landroid/net/wifi/WpsInfo;->setup:I

    goto :goto_0
.end method


# virtual methods
.method public connect(Landroid/net/wifi/p2p/WifiP2pDevice;)V
    .locals 4
    .param p1, "dev"    # Landroid/net/wifi/p2p/WifiP2pDevice;

    .prologue
    .line 249
    iget-object v0, p0, Lcom/samsung/musicplus/library/wifi/P2pManager;->mP2pManager:Landroid/net/wifi/p2p/WifiP2pManager;

    iget-object v1, p0, Lcom/samsung/musicplus/library/wifi/P2pManager;->mP2pChannel:Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    invoke-direct {p0, p1}, Lcom/samsung/musicplus/library/wifi/P2pManager;->makeConnConfig(Landroid/net/wifi/p2p/WifiP2pDevice;)Landroid/net/wifi/p2p/WifiP2pConfig;

    move-result-object v2

    new-instance v3, Lcom/samsung/musicplus/library/wifi/P2pManager$1;

    invoke-direct {v3, p0}, Lcom/samsung/musicplus/library/wifi/P2pManager$1;-><init>(Lcom/samsung/musicplus/library/wifi/P2pManager;)V

    invoke-virtual {v0, v1, v2, v3}, Landroid/net/wifi/p2p/WifiP2pManager;->connect(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pConfig;Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    .line 260
    return-void
.end method

.method public isP2pConnected()Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 405
    const/4 v0, 0x1

    return v0
.end method

.method public requestPeers(Landroid/net/wifi/p2p/WifiP2pManager$PeerListListener;)V
    .locals 0
    .param p1, "listener"    # Landroid/net/wifi/p2p/WifiP2pManager$PeerListListener;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 362
    return-void
.end method

.method public scanDevices()V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 290
    return-void
.end method

.method public stopPeerDiscovery()V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 237
    return-void
.end method
