.class public Lcom/samsung/musicplus/library/MusicIntent;
.super Ljava/lang/Object;
.source "MusicIntent.java"


# static fields
.field public static final ACTION_COVER_OPEN:Ljava/lang/String; = "com.samsung.cover.OPEN"

.field public static final ACTION_COVER_REMOTEVIEWS_UPDATE:Ljava/lang/String; = "com.samsung.cover.REMOTEVIEWS_UPDATE"

.field public static final ACTION_EASY_MODE_CHANGED:Ljava/lang/String; = "com.android.launcher.action.EASY_MODE_CHANGE"

.field public static final ACTION_HIDE_CONTEXTUAL_WIDGET:Ljava/lang/String; = "com.sec.android.app.music.intent.action.HIDE_CONTEXTUAL_WIDGET"

.field public static final ACTION_LAUNCH_MUSIC_PLAYER:Ljava/lang/String; = "com.sec.android.music.intent.action.LAUNCH_MUSIC_PLAYER"

.field public static final ACTION_LAUNCH_MUSIC_SQUARE:Ljava/lang/String; = "com.sec.android.music.intent.action.LAUNCH_MUSIC_SQUARE"

.field public static final ACTION_LAUNCH_SET_AS:Ljava/lang/String; = "com.sec.android.music.intent.action.LAUNCH_SET_AS"

.field public static final ACTION_MEDIA_SCAN_LAUNCH:Ljava/lang/String; = "android.intent.action.MEDIA_SCAN"

.field public static final ACTION_META_CHANGED:Ljava/lang/String; = "com.android.music.metachanged"

.field public static final ACTION_MUSIC_EASY_MODE_CHANGED:Ljava/lang/String; = "com.android.launcher.action.EASY_MODE_CHANGE_MUSIC"

.field public static final ACTION_PLAY:Ljava/lang/String; = "com.sec.android.music.intent.action.PLAY"

.field public static final ACTION_PLAYBACK_COMPLETE:Ljava/lang/String; = "com.android.music.playbackcomplete"

.field public static final ACTION_PLAYSTATE_CHANGED:Ljava/lang/String; = "com.android.music.playstatechanged"

.field public static final ACTION_PLAY_BY_MOOD:Ljava/lang/String; = "com.sec.android.app.music.intent.action.PLAY_BY_MOOD"

.field public static final ACTION_PLAY_NEXT:Ljava/lang/String; = "com.sec.android.app.music.intent.action.PLAY_NEXT"

.field public static final ACTION_PLAY_PREVIOUS:Ljava/lang/String; = "com.sec.android.app.music.intent.action.PLAY_PREVIOUS"

.field public static final ACTION_PLAY_VIA:Ljava/lang/String; = "com.sec.android.app.music.intent.action.PLAY_VIA"

.field public static final ACTION_PLAY_VIA_SSTREAM:Ljava/lang/String; = "android.intent.action.VIEW"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final ACTION_SAP_MEDIA_BUTTON:Ljava/lang/String; = "com.samsung.android.intent.action.MEDIA_BUTTON"

.field public static final ACTION_SETTING_CHANGED:Ljava/lang/String; = "com.android.music.settingchanged"

.field public static final ACTION_SHARE_MUSIC:Ljava/lang/String; = "com.samsung.groupcast.action.SEND_MUSIC_FOR_DJ_MODE"

.field public static final ACTION_SHOW_CONTEXTUAL_WIDGET:Ljava/lang/String; = "com.sec.android.app.music.intent.action.SHOW_CONTEXTUAL_WIDGET"

.field public static final ACTION_SHUFFL_OFF:Ljava/lang/String; = "com.sec.android.app.music.intent.action.SUFFLE_OFF"

.field public static final ACTION_SHUFFL_ON:Ljava/lang/String; = "com.sec.android.app.music.intent.action.SUFFLE_ON"

.field public static final ACTION_SIDESYNC_CONNECTED:Ljava/lang/String; = "com.sec.android.sidesync.source.SIDESYNC_CONNECTED"

.field public static final ACTION_SIDESYNC_DISCONNECTED:Ljava/lang/String; = "com.sec.android.sidesync.source.SIDESYNC_DISCONNECT"

.field public static final ACTION_STATUS_INFO:Ljava/lang/String; = "com.sec.android.app.music.info"

.field public static final ACTION_STOP:Ljava/lang/String; = "com.sec.android.app.music.intent.action.STOP"

.field public static final ADAPT_SOUND_PACKAGE_NAME:Ljava/lang/String; = "com.sec.hearingadjust"

.field public static final COMMAND_FASTFORWARD_DOWN:Ljava/lang/String; = "fastforward_down"

.field public static final COMMAND_FASTFORWARD_UP:Ljava/lang/String; = "fastforward_up"

.field public static final COMMAND_REWIND_DOWN:Ljava/lang/String; = "rewind_down"

.field public static final COMMAND_REWIND_UP:Ljava/lang/String; = "rewind_up"

.field public static final EXTRA_ALBUM:Ljava/lang/String; = "album"

.field public static final EXTRA_ARTIST:Ljava/lang/String; = "artist"

.field public static final EXTRA_CALM:Ljava/lang/String; = "calm"

.field public static final EXTRA_COMMAND:Ljava/lang/String; = "command"

.field public static final EXTRA_COVER_OPEN:Ljava/lang/String; = "coverOpen"

.field public static final EXTRA_COVER_REMOTE_VIEW:Ljava/lang/String; = "remote"

.field public static final EXTRA_COVER_TYPE:Ljava/lang/String; = "type"

.field public static final EXTRA_COVER_VISIBILITY:Ljava/lang/String; = "visibility"

.field public static final EXTRA_CURRENT_POSITION_OF_SONG:Ljava/lang/String; = "position"

.field public static final EXTRA_DURATION_OF_SONG:Ljava/lang/String; = "trackLength"

.field public static final EXTRA_EXCITING:Ljava/lang/String; = "exciting"

.field public static final EXTRA_GALAXY_FINDER_DATA_KEY:Ljava/lang/String; = "intent_extra_data_key"

.field public static final EXTRA_GALAXY_FINDER_FROM:Ljava/lang/String; = "intent_extra_from"

.field public static final EXTRA_GALAXY_FINDER_TARGET_TYPE:Ljava/lang/String; = "intent_extra_target_type"

.field public static final EXTRA_ISPLAYING:Ljava/lang/String; = "playing"

.field public static final EXTRA_JOYFUL:Ljava/lang/String; = "joyful"

.field public static final EXTRA_KEYWORD:Ljava/lang/String; = "extra_keyword"

.field public static final EXTRA_NAME:Ljava/lang/String; = "extra_name"

.field public static final EXTRA_PACKAGE_NAME:Ljava/lang/String; = "app_package_name"

.field public static final EXTRA_PASSIONATE:Ljava/lang/String; = "passionate"

.field public static final EXTRA_PLAYLIST:Ljava/lang/String; = "playlist"

.field public static final EXTRA_POSITION_OF_PLAYING_QUEUE:Ljava/lang/String; = "listpos"

.field public static final EXTRA_REPEAT:Ljava/lang/String; = "repeat"

.field public static final EXTRA_SHUFFLE:Ljava/lang/String; = "shuffle"

.field public static final EXTRA_SONG_COUNTS_OF_PLAYING_QUEUE:Ljava/lang/String; = "mediaCount"

.field public static final EXTRA_SONG_ID:Ljava/lang/String; = "id"

.field public static final EXTRA_SONG_TITLE:Ljava/lang/String; = "track"

.field public static final EXTRA_TITLE:Ljava/lang/String; = "title"

.field public static final EXTRA_TYPE:Ljava/lang/String; = "extra_type"

.field public static final EXTRA_URI:Ljava/lang/String; = "uri"

.field public static final GALAXY_FINDER_GLOBAL_SEARCH:Ljava/lang/String; = "global_search"

.field public static final GALAXY_FINDER_PACKAGE_NAME:Ljava/lang/String; = "com.samsung.android.app.galaxyfinder"

.field public static final POPUP_UI_RECEIVER_CLASS:Ljava/lang/String; = "com.sec.android.app.popupuireceiver"

.field public static final POPUP_UI_RECEIVER_DISABLE_APP_CLASS:Ljava/lang/String; = "com.sec.android.app.popupuireceiver.DisableApp"

.field public static final SVOICE_PACKAGE_NAME:Ljava/lang/String; = "com.vlingo.midas"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
