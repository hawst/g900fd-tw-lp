.class Lcom/samsung/musicplus/library/hardware/AirView$SeekHoverListener;
.super Ljava/lang/Object;
.source "AirView.java"

# interfaces
.implements Landroid/view/View$OnHoverListener;
.implements Landroid/widget/SeekBar$OnSeekBarHoverListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/library/hardware/AirView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "SeekHoverListener"
.end annotation


# instance fields
.field private final mAirSeekBarPopupListener:Lcom/samsung/musicplus/library/hardware/AirView$OnAirSeekBarPopupListener;


# direct methods
.method public constructor <init>(Lcom/samsung/musicplus/library/hardware/AirView$OnAirSeekBarPopupListener;)V
    .locals 0
    .param p1, "asbpl"    # Lcom/samsung/musicplus/library/hardware/AirView$OnAirSeekBarPopupListener;

    .prologue
    .line 739
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 740
    iput-object p1, p0, Lcom/samsung/musicplus/library/hardware/AirView$SeekHoverListener;->mAirSeekBarPopupListener:Lcom/samsung/musicplus/library/hardware/AirView$OnAirSeekBarPopupListener;

    .line 741
    return-void
.end method

.method private updateAirView(Landroid/widget/HoverPopupWindow;Landroid/view/View;II)V
    .locals 0
    .param p1, "hpw"    # Landroid/widget/HoverPopupWindow;
    .param p2, "v"    # Landroid/view/View;
    .param p3, "x"    # I
    .param p4, "y"    # I

    .prologue
    .line 777
    invoke-virtual {p1, p3, p4}, Landroid/widget/HoverPopupWindow;->setPopupPosOffset(II)V

    .line 778
    invoke-virtual {p1, p2}, Landroid/widget/HoverPopupWindow;->setContent(Landroid/view/View;)V

    .line 779
    return-void
.end method


# virtual methods
.method public onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 2
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 783
    iget-object v0, p0, Lcom/samsung/musicplus/library/hardware/AirView$SeekHoverListener;->mAirSeekBarPopupListener:Lcom/samsung/musicplus/library/hardware/AirView$OnAirSeekBarPopupListener;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v1

    float-to-int v1, v1

    invoke-interface {v0, v1}, Lcom/samsung/musicplus/library/hardware/AirView$OnAirSeekBarPopupListener;->setHoverXPosition(I)V

    .line 784
    const/4 v0, 0x0

    return v0
.end method

.method public onHoverChanged(Landroid/widget/SeekBar;IZ)V
    .locals 4
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "progress"    # I
    .param p3, "fromUser"    # Z

    .prologue
    .line 767
    iget-object v1, p0, Lcom/samsung/musicplus/library/hardware/AirView$SeekHoverListener;->mAirSeekBarPopupListener:Lcom/samsung/musicplus/library/hardware/AirView$OnAirSeekBarPopupListener;

    if-eqz v1, :cond_0

    .line 768
    iget-object v1, p0, Lcom/samsung/musicplus/library/hardware/AirView$SeekHoverListener;->mAirSeekBarPopupListener:Lcom/samsung/musicplus/library/hardware/AirView$OnAirSeekBarPopupListener;

    invoke-interface {v1, p1, p2, p3}, Lcom/samsung/musicplus/library/hardware/AirView$OnAirSeekBarPopupListener;->getHoverChangedView(Landroid/widget/SeekBar;IZ)Landroid/view/View;

    move-result-object v0

    .line 769
    .local v0, "v":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 770
    invoke-virtual {p1}, Landroid/widget/SeekBar;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/library/hardware/AirView$SeekHoverListener;->mAirSeekBarPopupListener:Lcom/samsung/musicplus/library/hardware/AirView$OnAirSeekBarPopupListener;

    invoke-interface {v2}, Lcom/samsung/musicplus/library/hardware/AirView$OnAirSeekBarPopupListener;->getXposition()I

    move-result v2

    iget-object v3, p0, Lcom/samsung/musicplus/library/hardware/AirView$SeekHoverListener;->mAirSeekBarPopupListener:Lcom/samsung/musicplus/library/hardware/AirView$OnAirSeekBarPopupListener;

    invoke-interface {v3}, Lcom/samsung/musicplus/library/hardware/AirView$OnAirSeekBarPopupListener;->getYposition()I

    move-result v3

    invoke-direct {p0, v1, v0, v2, v3}, Lcom/samsung/musicplus/library/hardware/AirView$SeekHoverListener;->updateAirView(Landroid/widget/HoverPopupWindow;Landroid/view/View;II)V

    .line 774
    .end local v0    # "v":Landroid/view/View;
    :cond_0
    return-void
.end method

.method public onStartTrackingHover(Landroid/widget/SeekBar;I)V
    .locals 4
    .param p1, "seekBar"    # Landroid/widget/SeekBar;
    .param p2, "hoverLevel"    # I

    .prologue
    .line 745
    iget-object v1, p0, Lcom/samsung/musicplus/library/hardware/AirView$SeekHoverListener;->mAirSeekBarPopupListener:Lcom/samsung/musicplus/library/hardware/AirView$OnAirSeekBarPopupListener;

    if-eqz v1, :cond_0

    .line 746
    iget-object v1, p0, Lcom/samsung/musicplus/library/hardware/AirView$SeekHoverListener;->mAirSeekBarPopupListener:Lcom/samsung/musicplus/library/hardware/AirView$OnAirSeekBarPopupListener;

    invoke-interface {v1, p1, p2}, Lcom/samsung/musicplus/library/hardware/AirView$OnAirSeekBarPopupListener;->getStartTrackingHoverView(Landroid/widget/SeekBar;I)Landroid/view/View;

    move-result-object v0

    .line 747
    .local v0, "v":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 748
    invoke-virtual {p1}, Landroid/widget/SeekBar;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/library/hardware/AirView$SeekHoverListener;->mAirSeekBarPopupListener:Lcom/samsung/musicplus/library/hardware/AirView$OnAirSeekBarPopupListener;

    invoke-interface {v2}, Lcom/samsung/musicplus/library/hardware/AirView$OnAirSeekBarPopupListener;->getXposition()I

    move-result v2

    iget-object v3, p0, Lcom/samsung/musicplus/library/hardware/AirView$SeekHoverListener;->mAirSeekBarPopupListener:Lcom/samsung/musicplus/library/hardware/AirView$OnAirSeekBarPopupListener;

    invoke-interface {v3}, Lcom/samsung/musicplus/library/hardware/AirView$OnAirSeekBarPopupListener;->getYposition()I

    move-result v3

    invoke-direct {p0, v1, v0, v2, v3}, Lcom/samsung/musicplus/library/hardware/AirView$SeekHoverListener;->updateAirView(Landroid/widget/HoverPopupWindow;Landroid/view/View;II)V

    .line 752
    .end local v0    # "v":Landroid/view/View;
    :cond_0
    return-void
.end method

.method public onStopTrackingHover(Landroid/widget/SeekBar;)V
    .locals 4
    .param p1, "seekBar"    # Landroid/widget/SeekBar;

    .prologue
    .line 756
    iget-object v1, p0, Lcom/samsung/musicplus/library/hardware/AirView$SeekHoverListener;->mAirSeekBarPopupListener:Lcom/samsung/musicplus/library/hardware/AirView$OnAirSeekBarPopupListener;

    if-eqz v1, :cond_0

    .line 757
    iget-object v1, p0, Lcom/samsung/musicplus/library/hardware/AirView$SeekHoverListener;->mAirSeekBarPopupListener:Lcom/samsung/musicplus/library/hardware/AirView$OnAirSeekBarPopupListener;

    invoke-interface {v1, p1}, Lcom/samsung/musicplus/library/hardware/AirView$OnAirSeekBarPopupListener;->getStopTrackingHoverView(Landroid/widget/SeekBar;)Landroid/view/View;

    move-result-object v0

    .line 758
    .local v0, "v":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 759
    invoke-virtual {p1}, Landroid/widget/SeekBar;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/library/hardware/AirView$SeekHoverListener;->mAirSeekBarPopupListener:Lcom/samsung/musicplus/library/hardware/AirView$OnAirSeekBarPopupListener;

    invoke-interface {v2}, Lcom/samsung/musicplus/library/hardware/AirView$OnAirSeekBarPopupListener;->getXposition()I

    move-result v2

    iget-object v3, p0, Lcom/samsung/musicplus/library/hardware/AirView$SeekHoverListener;->mAirSeekBarPopupListener:Lcom/samsung/musicplus/library/hardware/AirView$OnAirSeekBarPopupListener;

    invoke-interface {v3}, Lcom/samsung/musicplus/library/hardware/AirView$OnAirSeekBarPopupListener;->getYposition()I

    move-result v3

    invoke-direct {p0, v1, v0, v2, v3}, Lcom/samsung/musicplus/library/hardware/AirView$SeekHoverListener;->updateAirView(Landroid/widget/HoverPopupWindow;Landroid/view/View;II)V

    .line 763
    .end local v0    # "v":Landroid/view/View;
    :cond_0
    return-void
.end method
