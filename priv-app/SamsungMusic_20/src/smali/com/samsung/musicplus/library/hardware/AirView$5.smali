.class final Lcom/samsung/musicplus/library/hardware/AirView$5;
.super Ljava/lang/Object;
.source "AirView.java"

# interfaces
.implements Landroid/widget/HoverPopupWindow$HoverPopupListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/library/hardware/AirView;->setView(Landroid/view/View;Lcom/samsung/musicplus/library/hardware/AirView$OnAirViewMorePopupListener;IIZZZZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$listener:Lcom/samsung/musicplus/library/hardware/AirView$OnAirViewMorePopupListener;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/library/hardware/AirView$OnAirViewMorePopupListener;)V
    .locals 0

    .prologue
    .line 679
    iput-object p1, p0, Lcom/samsung/musicplus/library/hardware/AirView$5;->val$listener:Lcom/samsung/musicplus/library/hardware/AirView$OnAirViewMorePopupListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSetContentView(Landroid/view/View;Landroid/widget/HoverPopupWindow;)Z
    .locals 6
    .param p1, "parentView"    # Landroid/view/View;
    .param p2, "hpw"    # Landroid/widget/HoverPopupWindow;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 687
    const/4 v2, 0x0

    .line 688
    .local v2, "v":Landroid/view/View;
    const/4 v3, 0x2

    new-array v1, v3, [Ljava/lang/Object;

    .line 689
    .local v1, "parameter":[Ljava/lang/Object;
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 690
    .local v0, "context":Landroid/content/Context;
    invoke-static {v0}, Lcom/samsung/musicplus/library/hardware/AirView;->isEnableAirViewInforPreview(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {v0}, Lcom/samsung/musicplus/library/hardware/AirView;->isEnablePenHoveringInforPreview(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 692
    :cond_0
    iget-object v3, p0, Lcom/samsung/musicplus/library/hardware/AirView$5;->val$listener:Lcom/samsung/musicplus/library/hardware/AirView$OnAirViewMorePopupListener;

    if-eqz v3, :cond_1

    .line 693
    iget-object v3, p0, Lcom/samsung/musicplus/library/hardware/AirView$5;->val$listener:Lcom/samsung/musicplus/library/hardware/AirView$OnAirViewMorePopupListener;

    invoke-interface {v3, p1}, Lcom/samsung/musicplus/library/hardware/AirView$OnAirViewMorePopupListener;->getAirView(Landroid/view/View;)[Ljava/lang/Object;

    move-result-object v1

    .line 695
    :cond_1
    aget-object v2, v1, v5

    .end local v2    # "v":Landroid/view/View;
    check-cast v2, Landroid/view/View;

    .line 696
    .restart local v2    # "v":Landroid/view/View;
    if-eqz v2, :cond_2

    .line 697
    aget-object v3, v1, v4

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    invoke-virtual {p2, v3}, Landroid/widget/HoverPopupWindow;->setInfoPickerColorToAndMoreBottomImg(Z)V

    .line 698
    invoke-virtual {p2, v2}, Landroid/widget/HoverPopupWindow;->setContent(Landroid/view/View;)V

    .line 701
    :cond_2
    if-eqz v2, :cond_3

    move v3, v4

    :goto_0
    return v3

    :cond_3
    move v3, v5

    goto :goto_0
.end method
