.class Lcom/samsung/musicplus/library/hardware/SimpleGestureManager$1;
.super Ljava/lang/Object;
.source "SimpleGestureManager.java"

# interfaces
.implements Lcom/samsung/android/service/gesture/GestureListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;)V
    .locals 0

    .prologue
    .line 148
    iput-object p1, p0, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager$1;->this$0:Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGestureEvent(Lcom/samsung/android/service/gesture/GestureEvent;)V
    .locals 5
    .param p1, "event"    # Lcom/samsung/android/service/gesture/GestureEvent;

    .prologue
    .line 156
    invoke-virtual {p1}, Lcom/samsung/android/service/gesture/GestureEvent;->getEvent()I

    move-result v1

    .line 157
    .local v1, "motion":I
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.samsung.musicplus.GESTURE"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 158
    .local v0, "i":Landroid/content/Intent;
    const-string v2, "command"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 159
    iget-object v2, p0, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager$1;->this$0:Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;

    # getter for: Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;->access$000(Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 160
    # getter for: Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;->access$100()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "GestureListener event is : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 161
    return-void
.end method
