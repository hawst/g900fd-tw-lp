.class Lcom/samsung/musicplus/library/audio/SvoiceManager$1;
.super Ljava/lang/Object;
.source "SvoiceManager.java"

# interfaces
.implements Lcom/sec/android/app/IWSpeechRecognizer/IWSpeechRecognizerListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/library/audio/SvoiceManager;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/library/audio/SvoiceManager;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/library/audio/SvoiceManager;)V
    .locals 0

    .prologue
    .line 195
    iput-object p1, p0, Lcom/samsung/musicplus/library/audio/SvoiceManager$1;->this$0:Lcom/samsung/musicplus/library/audio/SvoiceManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResults([Ljava/lang/String;)V
    .locals 6
    .param p1, "result"    # [Ljava/lang/String;

    .prologue
    .line 198
    iget-object v3, p0, Lcom/samsung/musicplus/library/audio/SvoiceManager$1;->this$0:Lcom/samsung/musicplus/library/audio/SvoiceManager;

    # getter for: Lcom/samsung/musicplus/library/audio/SvoiceManager;->mRecognizer:Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;
    invoke-static {v3}, Lcom/samsung/musicplus/library/audio/SvoiceManager;->access$000(Lcom/samsung/musicplus/library/audio/SvoiceManager;)Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/sec/android/app/IWSpeechRecognizer/BargeInRecognizer;->getIntBargeInResult()I

    move-result v0

    .line 199
    .local v0, "command":I
    # getter for: Lcom/samsung/musicplus/library/audio/SvoiceManager;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/library/audio/SvoiceManager;->access$100()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "command : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " mSvoiceListner : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/musicplus/library/audio/SvoiceManager$1;->this$0:Lcom/samsung/musicplus/library/audio/SvoiceManager;

    # getter for: Lcom/samsung/musicplus/library/audio/SvoiceManager;->mSvoiceListner:Lcom/samsung/musicplus/library/audio/SvoiceManager$OnSvoiceListener;
    invoke-static {v5}, Lcom/samsung/musicplus/library/audio/SvoiceManager;->access$200(Lcom/samsung/musicplus/library/audio/SvoiceManager;)Lcom/samsung/musicplus/library/audio/SvoiceManager$OnSvoiceListener;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    iget-object v3, p0, Lcom/samsung/musicplus/library/audio/SvoiceManager$1;->this$0:Lcom/samsung/musicplus/library/audio/SvoiceManager;

    # getter for: Lcom/samsung/musicplus/library/audio/SvoiceManager;->mIsStart:Z
    invoke-static {v3}, Lcom/samsung/musicplus/library/audio/SvoiceManager;->access$300(Lcom/samsung/musicplus/library/audio/SvoiceManager;)Z

    move-result v3

    if-eqz v3, :cond_0

    const/4 v3, 0x1

    if-lt v0, v3, :cond_0

    const/4 v3, 0x6

    if-gt v0, v3, :cond_0

    .line 203
    iget-object v3, p0, Lcom/samsung/musicplus/library/audio/SvoiceManager$1;->this$0:Lcom/samsung/musicplus/library/audio/SvoiceManager;

    # getter for: Lcom/samsung/musicplus/library/audio/SvoiceManager;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/samsung/musicplus/library/audio/SvoiceManager;->access$400(Lcom/samsung/musicplus/library/audio/SvoiceManager;)Landroid/content/Context;

    move-result-object v3

    const-string v4, "power"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/os/PowerManager;

    .line 205
    .local v1, "pm":Landroid/os/PowerManager;
    if-eqz v1, :cond_0

    .line 207
    const v3, 0x2000000a

    const-string v4, "barge in lcd on"

    invoke-virtual {v1, v3, v4}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v2

    .line 210
    .local v2, "wl":Landroid/os/PowerManager$WakeLock;
    const-wide/16 v4, 0x1

    invoke-virtual {v2, v4, v5}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    .line 213
    .end local v1    # "pm":Landroid/os/PowerManager;
    .end local v2    # "wl":Landroid/os/PowerManager$WakeLock;
    :cond_0
    iget-object v3, p0, Lcom/samsung/musicplus/library/audio/SvoiceManager$1;->this$0:Lcom/samsung/musicplus/library/audio/SvoiceManager;

    # getter for: Lcom/samsung/musicplus/library/audio/SvoiceManager;->mSvoiceListner:Lcom/samsung/musicplus/library/audio/SvoiceManager$OnSvoiceListener;
    invoke-static {v3}, Lcom/samsung/musicplus/library/audio/SvoiceManager;->access$200(Lcom/samsung/musicplus/library/audio/SvoiceManager;)Lcom/samsung/musicplus/library/audio/SvoiceManager$OnSvoiceListener;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 214
    iget-object v3, p0, Lcom/samsung/musicplus/library/audio/SvoiceManager$1;->this$0:Lcom/samsung/musicplus/library/audio/SvoiceManager;

    # getter for: Lcom/samsung/musicplus/library/audio/SvoiceManager;->mSvoiceListner:Lcom/samsung/musicplus/library/audio/SvoiceManager$OnSvoiceListener;
    invoke-static {v3}, Lcom/samsung/musicplus/library/audio/SvoiceManager;->access$200(Lcom/samsung/musicplus/library/audio/SvoiceManager;)Lcom/samsung/musicplus/library/audio/SvoiceManager$OnSvoiceListener;

    move-result-object v3

    invoke-interface {v3, v0}, Lcom/samsung/musicplus/library/audio/SvoiceManager$OnSvoiceListener;->onResults(I)V

    .line 216
    :cond_1
    return-void
.end method
