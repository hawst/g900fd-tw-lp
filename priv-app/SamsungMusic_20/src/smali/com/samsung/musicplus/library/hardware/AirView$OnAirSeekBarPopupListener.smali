.class public interface abstract Lcom/samsung/musicplus/library/hardware/AirView$OnAirSeekBarPopupListener;
.super Ljava/lang/Object;
.source "AirView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/library/hardware/AirView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "OnAirSeekBarPopupListener"
.end annotation


# virtual methods
.method public abstract getHoverChangedView(Landroid/widget/SeekBar;IZ)Landroid/view/View;
.end method

.method public abstract getStartTrackingHoverView(Landroid/widget/SeekBar;I)Landroid/view/View;
.end method

.method public abstract getStopTrackingHoverView(Landroid/widget/SeekBar;)Landroid/view/View;
.end method

.method public abstract getXposition()I
.end method

.method public abstract getYposition()I
.end method

.method public abstract setHoverXPosition(I)V
.end method
