.class public Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager$PersonaInfoListCompat;
.super Ljava/lang/Object;
.source "KnoxPersonalModeManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PersonaInfoListCompat"
.end annotation


# instance fields
.field private mInfoList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/PersonaInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 609
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 610
    const-string v1, "persona"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PersonaManager;

    .line 611
    .local v0, "pm":Landroid/os/PersonaManager;
    invoke-virtual {v0}, Landroid/os/PersonaManager;->getPersonas()Ljava/util/List;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager$PersonaInfoListCompat;->mInfoList:Ljava/util/List;

    .line 612
    return-void
.end method


# virtual methods
.method public getIconPath(I)Ljava/lang/String;
    .locals 2
    .param p1, "index"    # I

    .prologue
    const/4 v1, 0x0

    .line 652
    iget-object v0, p0, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager$PersonaInfoListCompat;->mInfoList:Ljava/util/List;

    if-nez v0, :cond_0

    .line 655
    :cond_0
    return-object v1
.end method

.method public getId(I)I
    .locals 2
    .param p1, "index"    # I

    .prologue
    const/4 v1, -0x1

    .line 637
    iget-object v0, p0, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager$PersonaInfoListCompat;->mInfoList:Ljava/util/List;

    if-nez v0, :cond_0

    .line 640
    :cond_0
    return v1
.end method

.method public getName(I)Ljava/lang/String;
    .locals 2
    .param p1, "index"    # I

    .prologue
    const/4 v1, 0x0

    .line 667
    iget-object v0, p0, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager$PersonaInfoListCompat;->mInfoList:Ljava/util/List;

    if-nez v0, :cond_0

    .line 670
    :cond_0
    return-object v1
.end method

.method public getSize()I
    .locals 1

    .prologue
    .line 622
    iget-object v0, p0, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager$PersonaInfoListCompat;->mInfoList:Ljava/util/List;

    if-nez v0, :cond_0

    .line 623
    const/4 v0, 0x0

    .line 625
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager$PersonaInfoListCompat;->mInfoList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method
