.class public Lcom/samsung/musicplus/library/audio/WaveDataEngine;
.super Ljava/lang/Object;
.source "WaveDataEngine.java"


# instance fields
.field private mEngine:Lcom/samsung/sectionmap/MusicViewLib;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    new-instance v0, Lcom/samsung/sectionmap/MusicViewLib;

    invoke-direct {v0}, Lcom/samsung/sectionmap/MusicViewLib;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/library/audio/WaveDataEngine;->mEngine:Lcom/samsung/sectionmap/MusicViewLib;

    .line 53
    iget-object v0, p0, Lcom/samsung/musicplus/library/audio/WaveDataEngine;->mEngine:Lcom/samsung/sectionmap/MusicViewLib;

    invoke-virtual {v0}, Lcom/samsung/sectionmap/MusicViewLib;->MusicViewInit()I

    .line 54
    return-void
.end method


# virtual methods
.method public getMusicViewData(Ljava/lang/String;I)[B
    .locals 1
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "frameInterVal"    # I

    .prologue
    .line 86
    iget-object v0, p0, Lcom/samsung/musicplus/library/audio/WaveDataEngine;->mEngine:Lcom/samsung/sectionmap/MusicViewLib;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/sectionmap/MusicViewLib;->MusicViewEXE(Ljava/lang/String;I)[B

    move-result-object v0

    return-object v0
.end method

.method public release()I
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/samsung/musicplus/library/audio/WaveDataEngine;->mEngine:Lcom/samsung/sectionmap/MusicViewLib;

    invoke-virtual {v0}, Lcom/samsung/sectionmap/MusicViewLib;->MusicViewRelease()I

    move-result v0

    return v0
.end method
