.class Lcom/samsung/musicplus/library/dlna/DlnaManager$1;
.super Ljava/lang/Object;
.source "DlnaManager.java"

# interfaces
.implements Lcom/samsung/android/allshare/ServiceConnector$IServiceConnectEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/library/dlna/DlnaManager;->bindDlnaService()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/library/dlna/DlnaManager;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/library/dlna/DlnaManager;)V
    .locals 0

    .prologue
    .line 455
    iput-object p1, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager$1;->this$0:Lcom/samsung/musicplus/library/dlna/DlnaManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreated(Lcom/samsung/android/allshare/ServiceProvider;Lcom/samsung/android/allshare/ServiceConnector$ServiceState;)V
    .locals 3
    .param p1, "provider"    # Lcom/samsung/android/allshare/ServiceProvider;
    .param p2, "state"    # Lcom/samsung/android/allshare/ServiceConnector$ServiceState;

    .prologue
    .line 485
    # getter for: Lcom/samsung/musicplus/library/dlna/DlnaManager;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/library/dlna/DlnaManager;->access$100()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCreated(): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 486
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager$1;->this$0:Lcom/samsung/musicplus/library/dlna/DlnaManager;

    check-cast p1, Lcom/samsung/android/allshare/media/MediaServiceProvider;

    .end local p1    # "provider":Lcom/samsung/android/allshare/ServiceProvider;
    # setter for: Lcom/samsung/musicplus/library/dlna/DlnaManager;->mDlnaServiceProvider:Lcom/samsung/android/allshare/media/MediaServiceProvider;
    invoke-static {v0, p1}, Lcom/samsung/musicplus/library/dlna/DlnaManager;->access$202(Lcom/samsung/musicplus/library/dlna/DlnaManager;Lcom/samsung/android/allshare/media/MediaServiceProvider;)Lcom/samsung/android/allshare/media/MediaServiceProvider;

    .line 487
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager$1;->this$0:Lcom/samsung/musicplus/library/dlna/DlnaManager;

    iget-object v1, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager$1;->this$0:Lcom/samsung/musicplus/library/dlna/DlnaManager;

    # getter for: Lcom/samsung/musicplus/library/dlna/DlnaManager;->mDlnaServiceProvider:Lcom/samsung/android/allshare/media/MediaServiceProvider;
    invoke-static {v1}, Lcom/samsung/musicplus/library/dlna/DlnaManager;->access$200(Lcom/samsung/musicplus/library/dlna/DlnaManager;)Lcom/samsung/android/allshare/media/MediaServiceProvider;

    move-result-object v1

    invoke-virtual {v1}, Lcom/samsung/android/allshare/media/MediaServiceProvider;->getDeviceFinder()Lcom/samsung/android/allshare/media/MediaDeviceFinder;

    move-result-object v1

    # setter for: Lcom/samsung/musicplus/library/dlna/DlnaManager;->mDeviceFinder:Lcom/samsung/android/allshare/media/MediaDeviceFinder;
    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/dlna/DlnaManager;->access$002(Lcom/samsung/musicplus/library/dlna/DlnaManager;Lcom/samsung/android/allshare/media/MediaDeviceFinder;)Lcom/samsung/android/allshare/media/MediaDeviceFinder;

    .line 489
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager$1;->this$0:Lcom/samsung/musicplus/library/dlna/DlnaManager;

    # invokes: Lcom/samsung/musicplus/library/dlna/DlnaManager;->updateDmsList()V
    invoke-static {v0}, Lcom/samsung/musicplus/library/dlna/DlnaManager;->access$300(Lcom/samsung/musicplus/library/dlna/DlnaManager;)V

    .line 490
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager$1;->this$0:Lcom/samsung/musicplus/library/dlna/DlnaManager;

    # getter for: Lcom/samsung/musicplus/library/dlna/DlnaManager;->mDeviceFinder:Lcom/samsung/android/allshare/media/MediaDeviceFinder;
    invoke-static {v0}, Lcom/samsung/musicplus/library/dlna/DlnaManager;->access$000(Lcom/samsung/musicplus/library/dlna/DlnaManager;)Lcom/samsung/android/allshare/media/MediaDeviceFinder;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_PROVIDER:Lcom/samsung/android/allshare/Device$DeviceType;

    iget-object v2, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager$1;->this$0:Lcom/samsung/musicplus/library/dlna/DlnaManager;

    # getter for: Lcom/samsung/musicplus/library/dlna/DlnaManager;->mDMSFinderListener:Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;
    invoke-static {v2}, Lcom/samsung/musicplus/library/dlna/DlnaManager;->access$400(Lcom/samsung/musicplus/library/dlna/DlnaManager;)Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/allshare/media/MediaDeviceFinder;->setDeviceFinderEventListener(Lcom/samsung/android/allshare/Device$DeviceType;Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;)V

    .line 493
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager$1;->this$0:Lcom/samsung/musicplus/library/dlna/DlnaManager;

    # invokes: Lcom/samsung/musicplus/library/dlna/DlnaManager;->updateDmrList()V
    invoke-static {v0}, Lcom/samsung/musicplus/library/dlna/DlnaManager;->access$500(Lcom/samsung/musicplus/library/dlna/DlnaManager;)V

    .line 494
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager$1;->this$0:Lcom/samsung/musicplus/library/dlna/DlnaManager;

    # getter for: Lcom/samsung/musicplus/library/dlna/DlnaManager;->mDeviceFinder:Lcom/samsung/android/allshare/media/MediaDeviceFinder;
    invoke-static {v0}, Lcom/samsung/musicplus/library/dlna/DlnaManager;->access$000(Lcom/samsung/musicplus/library/dlna/DlnaManager;)Lcom/samsung/android/allshare/media/MediaDeviceFinder;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_AVPLAYER:Lcom/samsung/android/allshare/Device$DeviceType;

    iget-object v2, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager$1;->this$0:Lcom/samsung/musicplus/library/dlna/DlnaManager;

    # getter for: Lcom/samsung/musicplus/library/dlna/DlnaManager;->mDMRFinderListener:Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;
    invoke-static {v2}, Lcom/samsung/musicplus/library/dlna/DlnaManager;->access$600(Lcom/samsung/musicplus/library/dlna/DlnaManager;)Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/allshare/media/MediaDeviceFinder;->setDeviceFinderEventListener(Lcom/samsung/android/allshare/Device$DeviceType;Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;)V

    .line 497
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager$1;->this$0:Lcom/samsung/musicplus/library/dlna/DlnaManager;

    const-string v1, "com.samsung.musicplus.dlna.servicecreated"

    # invokes: Lcom/samsung/musicplus/library/dlna/DlnaManager;->sendDlnaInfo(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/dlna/DlnaManager;->access$700(Lcom/samsung/musicplus/library/dlna/DlnaManager;Ljava/lang/String;)V

    .line 498
    return-void
.end method

.method public onDeleted(Lcom/samsung/android/allshare/ServiceProvider;)V
    .locals 3
    .param p1, "provider"    # Lcom/samsung/android/allshare/ServiceProvider;

    .prologue
    const/4 v2, 0x0

    .line 469
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager$1;->this$0:Lcom/samsung/musicplus/library/dlna/DlnaManager;

    # getter for: Lcom/samsung/musicplus/library/dlna/DlnaManager;->mDeviceFinder:Lcom/samsung/android/allshare/media/MediaDeviceFinder;
    invoke-static {v0}, Lcom/samsung/musicplus/library/dlna/DlnaManager;->access$000(Lcom/samsung/musicplus/library/dlna/DlnaManager;)Lcom/samsung/android/allshare/media/MediaDeviceFinder;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_PROVIDER:Lcom/samsung/android/allshare/Device$DeviceType;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/allshare/media/MediaDeviceFinder;->setDeviceFinderEventListener(Lcom/samsung/android/allshare/Device$DeviceType;Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;)V

    .line 471
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager$1;->this$0:Lcom/samsung/musicplus/library/dlna/DlnaManager;

    # getter for: Lcom/samsung/musicplus/library/dlna/DlnaManager;->mDeviceFinder:Lcom/samsung/android/allshare/media/MediaDeviceFinder;
    invoke-static {v0}, Lcom/samsung/musicplus/library/dlna/DlnaManager;->access$000(Lcom/samsung/musicplus/library/dlna/DlnaManager;)Lcom/samsung/android/allshare/media/MediaDeviceFinder;

    move-result-object v0

    sget-object v1, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_AVPLAYER:Lcom/samsung/android/allshare/Device$DeviceType;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/allshare/media/MediaDeviceFinder;->setDeviceFinderEventListener(Lcom/samsung/android/allshare/Device$DeviceType;Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;)V

    .line 472
    return-void
.end method
