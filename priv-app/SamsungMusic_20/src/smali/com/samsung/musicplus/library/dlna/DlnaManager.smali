.class public Lcom/samsung/musicplus/library/dlna/DlnaManager;
.super Ljava/lang/Object;
.source "DlnaManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/library/dlna/DlnaManager$SimpleAVPlayer;,
        Lcom/samsung/musicplus/library/dlna/DlnaManager$DownloadSongInfo;
    }
.end annotation


# static fields
.field public static final ACTION_DLNA_DEVICE_CONNECTIVITY_INFO:Ljava/lang/String; = "com.samsung.musicplus.dlna.connectivitychanged"

.field public static final ACTION_DLNA_DEVICE_FINDER_ERROR:Ljava/lang/String; = "com.samsung.musicplus.dlna.devicefinder.error"

.field public static final ACTION_DLNA_FLAT_SEARCHING_ERROR:Ljava/lang/String; = "com.samsung.musicplus.dlna.flat.searching.error"

.field public static final ACTION_DLNA_FLAT_SEARCHING_INFO:Ljava/lang/String; = "com.samsung.musicplus.dlna.flat.searching.info"

.field public static final ACTION_DLNA_SERVICE_CREATED:Ljava/lang/String; = "com.samsung.musicplus.dlna.servicecreated"

.field private static final CLASSNAME:Ljava/lang/String;

.field private static final DEBUG:Z = false

.field public static final DEVICE_FINDER_ERROR_FAILED_TO_CONNECT_NETWORK:I = 0x0

.field public static final DMR_ADDED:I = 0x2

.field public static final DMR_REMOVED:I = 0x3

.field public static final DMS_ADDED:I = 0x0

.field public static final DMS_REMOVED:I = 0x1

.field public static final EXTRA_DLNA_DEVICE_CONNECTIVITY_INFO_WHAT:Ljava/lang/String; = "com.samsung.musicplus.dlna.connectivitychanged.extra.what"

.field public static final EXTRA_DLNA_DEVICE_FINDER_ERROR:Ljava/lang/String; = "com.samsung.musicplus.dlna.devicefinder.error.extra"

.field public static final EXTRA_DLNA_DEVICE_ID:Ljava/lang/String; = "com.samsung.musicplus.dlna.extra.deviceId"

.field public static final EXTRA_DLNA_FLAT_SEARCHING_ERROR:Ljava/lang/String; = "com.samsung.musicplus.dlna.flat.searching.extra.error"

.field public static final EXTRA_DLNA_FLAT_SEARCHING_WHAT:Ljava/lang/String; = "com.samsung.musicplus.dlna.flat.searching.extra.what"

.field public static final FLAT_SEARCHING_CANCEL:I = 0x3

.field public static final FLAT_SEARCHING_ERROR_BAD_REQUEST:I = 0x4

.field public static final FLAT_SEARCHING_ERROR_DMS_INTERNAL_ERROR:I = 0x5

.field public static final FLAT_SEARCHING_ERROR_INVALID_DEVICE:I = 0x3

.field public static final FLAT_SEARCHING_ERROR_NETWORK_NOT_AVAILABLE:I = 0x1

.field public static final FLAT_SEARCHING_ERROR_NO_RESPONSE:I = 0x2

.field public static final FLAT_SEARCHING_ERROR_SERVICE_NOT_CONNECTED:I = 0x6

.field public static final FLAT_SEARCHING_ERROR_UNKNOWN:I = 0x0

.field public static final FLAT_SEARCHING_FINISH:I = 0x2

.field public static final FLAT_SEARCHING_PROGRESS:I = 0x1

.field public static final FLAT_SEARCHING_START:I = 0x0

.field public static final UNDEFINED:I = -0x1

.field private static sDlnaManager:Lcom/samsung/musicplus/library/dlna/DlnaManager;


# instance fields
.field private final MEDIA_DELETE_ALL_URI:Landroid/net/Uri;

.field private final MEDIA_RENDERER_URI:Landroid/net/Uri;

.field private final MEDIA_SERVER_CONTENTS_URI:Landroid/net/Uri;

.field private final MEDIA_SERVER_URI:Landroid/net/Uri;

.field private final mContext:Landroid/content/Context;

.field private final mDMRFinderListener:Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;

.field private final mDMSFinderListener:Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;

.field private mDeviceFinder:Lcom/samsung/android/allshare/media/MediaDeviceFinder;

.field private mDlnaDBIndex:I

.field private mDlnaServiceProvider:Lcom/samsung/android/allshare/media/MediaServiceProvider;

.field private mDmrList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/Device;",
            ">;"
        }
    .end annotation
.end field

.field private mDmsList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/Device;",
            ">;"
        }
    .end annotation
.end field

.field private final mFlatBrowseResponseListener:Lcom/samsung/android/allshare/extension/FlatProvider$IFlatProviderConnection;

.field private mFlatProvider:Lcom/samsung/android/allshare/extension/FlatProvider;

.field private mItemList:Lcom/samsung/android/allshare/extension/UniqueItemArray;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 68
    const-class v0, Lcom/samsung/musicplus/library/dlna/DlnaManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/library/dlna/DlnaManager;->CLASSNAME:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "server"    # Landroid/net/Uri;
    .param p3, "serverContent"    # Landroid/net/Uri;
    .param p4, "renderer"    # Landroid/net/Uri;
    .param p5, "deleteAll"    # Landroid/net/Uri;

    .prologue
    .line 389
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 656
    new-instance v0, Lcom/samsung/musicplus/library/dlna/DlnaManager$2;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/library/dlna/DlnaManager$2;-><init>(Lcom/samsung/musicplus/library/dlna/DlnaManager;)V

    iput-object v0, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager;->mDMSFinderListener:Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;

    .line 695
    new-instance v0, Lcom/samsung/musicplus/library/dlna/DlnaManager$3;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/library/dlna/DlnaManager$3;-><init>(Lcom/samsung/musicplus/library/dlna/DlnaManager;)V

    iput-object v0, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager;->mDMRFinderListener:Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;

    .line 726
    new-instance v0, Lcom/samsung/musicplus/library/dlna/DlnaManager$4;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/library/dlna/DlnaManager$4;-><init>(Lcom/samsung/musicplus/library/dlna/DlnaManager;)V

    iput-object v0, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager;->mFlatBrowseResponseListener:Lcom/samsung/android/allshare/extension/FlatProvider$IFlatProviderConnection;

    .line 390
    iput-object p1, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager;->mContext:Landroid/content/Context;

    .line 391
    iput-object p2, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager;->MEDIA_SERVER_URI:Landroid/net/Uri;

    .line 392
    iput-object p3, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager;->MEDIA_SERVER_CONTENTS_URI:Landroid/net/Uri;

    .line 393
    iput-object p4, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager;->MEDIA_RENDERER_URI:Landroid/net/Uri;

    .line 394
    iput-object p5, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager;->MEDIA_DELETE_ALL_URI:Landroid/net/Uri;

    .line 395
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/library/dlna/DlnaManager;)Lcom/samsung/android/allshare/media/MediaDeviceFinder;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/library/dlna/DlnaManager;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager;->mDeviceFinder:Lcom/samsung/android/allshare/media/MediaDeviceFinder;

    return-object v0
.end method

.method static synthetic access$002(Lcom/samsung/musicplus/library/dlna/DlnaManager;Lcom/samsung/android/allshare/media/MediaDeviceFinder;)Lcom/samsung/android/allshare/media/MediaDeviceFinder;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/library/dlna/DlnaManager;
    .param p1, "x1"    # Lcom/samsung/android/allshare/media/MediaDeviceFinder;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager;->mDeviceFinder:Lcom/samsung/android/allshare/media/MediaDeviceFinder;

    return-object p1
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    sget-object v0, Lcom/samsung/musicplus/library/dlna/DlnaManager;->CLASSNAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/samsung/musicplus/library/dlna/DlnaManager;)Landroid/net/Uri;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/library/dlna/DlnaManager;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager;->MEDIA_SERVER_CONTENTS_URI:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/samsung/musicplus/library/dlna/DlnaManager;Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/library/dlna/DlnaManager;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # I
    .param p3, "x3"    # Ljava/lang/String;

    .prologue
    .line 66
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/musicplus/library/dlna/DlnaManager;->sendDlnaInfo(Ljava/lang/String;ILjava/lang/String;)V

    return-void
.end method

.method static synthetic access$1200(Lcom/samsung/musicplus/library/dlna/DlnaManager;Lcom/samsung/android/allshare/ERROR;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/library/dlna/DlnaManager;
    .param p1, "x1"    # Lcom/samsung/android/allshare/ERROR;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 66
    invoke-direct {p0, p1, p2}, Lcom/samsung/musicplus/library/dlna/DlnaManager;->handleDeviceFinderError(Lcom/samsung/android/allshare/ERROR;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$1300(Lcom/samsung/musicplus/library/dlna/DlnaManager;)Lcom/samsung/android/allshare/extension/FlatProvider;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/library/dlna/DlnaManager;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager;->mFlatProvider:Lcom/samsung/android/allshare/extension/FlatProvider;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/samsung/musicplus/library/dlna/DlnaManager;Ljava/lang/String;ILjava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/library/dlna/DlnaManager;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # I
    .param p3, "x3"    # Ljava/lang/String;

    .prologue
    .line 66
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/musicplus/library/dlna/DlnaManager;->sendStickyDlnaInfo(Ljava/lang/String;ILjava/lang/String;)V

    return-void
.end method

.method static synthetic access$1500(Lcom/samsung/musicplus/library/dlna/DlnaManager;)Lcom/samsung/android/allshare/extension/UniqueItemArray;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/library/dlna/DlnaManager;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager;->mItemList:Lcom/samsung/android/allshare/extension/UniqueItemArray;

    return-object v0
.end method

.method static synthetic access$1502(Lcom/samsung/musicplus/library/dlna/DlnaManager;Lcom/samsung/android/allshare/extension/UniqueItemArray;)Lcom/samsung/android/allshare/extension/UniqueItemArray;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/library/dlna/DlnaManager;
    .param p1, "x1"    # Lcom/samsung/android/allshare/extension/UniqueItemArray;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager;->mItemList:Lcom/samsung/android/allshare/extension/UniqueItemArray;

    return-object p1
.end method

.method static synthetic access$1600(Lcom/samsung/musicplus/library/dlna/DlnaManager;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/library/dlna/DlnaManager;

    .prologue
    .line 66
    iget v0, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager;->mDlnaDBIndex:I

    return v0
.end method

.method static synthetic access$1602(Lcom/samsung/musicplus/library/dlna/DlnaManager;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/library/dlna/DlnaManager;
    .param p1, "x1"    # I

    .prologue
    .line 66
    iput p1, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager;->mDlnaDBIndex:I

    return p1
.end method

.method static synthetic access$200(Lcom/samsung/musicplus/library/dlna/DlnaManager;)Lcom/samsung/android/allshare/media/MediaServiceProvider;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/library/dlna/DlnaManager;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager;->mDlnaServiceProvider:Lcom/samsung/android/allshare/media/MediaServiceProvider;

    return-object v0
.end method

.method static synthetic access$202(Lcom/samsung/musicplus/library/dlna/DlnaManager;Lcom/samsung/android/allshare/media/MediaServiceProvider;)Lcom/samsung/android/allshare/media/MediaServiceProvider;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/library/dlna/DlnaManager;
    .param p1, "x1"    # Lcom/samsung/android/allshare/media/MediaServiceProvider;

    .prologue
    .line 66
    iput-object p1, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager;->mDlnaServiceProvider:Lcom/samsung/android/allshare/media/MediaServiceProvider;

    return-object p1
.end method

.method static synthetic access$300(Lcom/samsung/musicplus/library/dlna/DlnaManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/library/dlna/DlnaManager;

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/samsung/musicplus/library/dlna/DlnaManager;->updateDmsList()V

    return-void
.end method

.method static synthetic access$400(Lcom/samsung/musicplus/library/dlna/DlnaManager;)Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/library/dlna/DlnaManager;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager;->mDMSFinderListener:Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/musicplus/library/dlna/DlnaManager;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/library/dlna/DlnaManager;

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/samsung/musicplus/library/dlna/DlnaManager;->updateDmrList()V

    return-void
.end method

.method static synthetic access$600(Lcom/samsung/musicplus/library/dlna/DlnaManager;)Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/library/dlna/DlnaManager;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager;->mDMRFinderListener:Lcom/samsung/android/allshare/DeviceFinder$IDeviceFinderEventListener;

    return-object v0
.end method

.method static synthetic access$700(Lcom/samsung/musicplus/library/dlna/DlnaManager;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/library/dlna/DlnaManager;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/library/dlna/DlnaManager;->sendDlnaInfo(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$800(Lcom/samsung/musicplus/library/dlna/DlnaManager;)Landroid/net/Uri;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/library/dlna/DlnaManager;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager;->MEDIA_SERVER_URI:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$900(Lcom/samsung/musicplus/library/dlna/DlnaManager;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/library/dlna/DlnaManager;

    .prologue
    .line 66
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private getDownloadSongInfo(J)Lcom/samsung/musicplus/library/dlna/DlnaManager$DownloadSongInfo;
    .locals 9
    .param p1, "id"    # J

    .prologue
    const/4 v8, 0x1

    const/4 v3, 0x0

    .line 982
    new-instance v7, Lcom/samsung/musicplus/library/dlna/DlnaManager$DownloadSongInfo;

    invoke-direct {v7, p0, v3}, Lcom/samsung/musicplus/library/dlna/DlnaManager$DownloadSongInfo;-><init>(Lcom/samsung/musicplus/library/dlna/DlnaManager;Lcom/samsung/musicplus/library/dlna/DlnaManager$1;)V

    .line 984
    .local v7, "info":Lcom/samsung/musicplus/library/dlna/DlnaManager$DownloadSongInfo;
    const/4 v0, 0x2

    new-array v2, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "seed"

    aput-object v1, v2, v0

    const-string v0, "provider_name"

    aput-object v0, v2, v8

    .line 987
    .local v2, "projection":[Ljava/lang/String;
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager;->MEDIA_SERVER_CONTENTS_URI:Landroid/net/Uri;

    invoke-static {v1, p1, p2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 991
    .local v6, "c":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-ne v0, v8, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 992
    const-string v0, "seed"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    # setter for: Lcom/samsung/musicplus/library/dlna/DlnaManager$DownloadSongInfo;->seed:Ljava/lang/String;
    invoke-static {v7, v0}, Lcom/samsung/musicplus/library/dlna/DlnaManager$DownloadSongInfo;->access$1702(Lcom/samsung/musicplus/library/dlna/DlnaManager$DownloadSongInfo;Ljava/lang/String;)Ljava/lang/String;

    .line 993
    const-string v0, "provider_name"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    # setter for: Lcom/samsung/musicplus/library/dlna/DlnaManager$DownloadSongInfo;->providerName:Ljava/lang/String;
    invoke-static {v7, v0}, Lcom/samsung/musicplus/library/dlna/DlnaManager$DownloadSongInfo;->access$1802(Lcom/samsung/musicplus/library/dlna/DlnaManager$DownloadSongInfo;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 997
    :cond_0
    if-eqz v6, :cond_1

    .line 998
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1001
    :cond_1
    sget-object v0, Lcom/samsung/musicplus/library/dlna/DlnaManager;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "DownloadSongInfo() - id: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "provider name"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    # getter for: Lcom/samsung/musicplus/library/dlna/DlnaManager$DownloadSongInfo;->providerName:Ljava/lang/String;
    invoke-static {v7}, Lcom/samsung/musicplus/library/dlna/DlnaManager$DownloadSongInfo;->access$1800(Lcom/samsung/musicplus/library/dlna/DlnaManager$DownloadSongInfo;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " seed: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    # getter for: Lcom/samsung/musicplus/library/dlna/DlnaManager$DownloadSongInfo;->seed:Ljava/lang/String;
    invoke-static {v7}, Lcom/samsung/musicplus/library/dlna/DlnaManager$DownloadSongInfo;->access$1700(Lcom/samsung/musicplus/library/dlna/DlnaManager$DownloadSongInfo;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1003
    return-object v7

    .line 997
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_2

    .line 998
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v0
.end method

.method public static getInstance(Landroid/content/Context;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;)Lcom/samsung/musicplus/library/dlna/DlnaManager;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "server"    # Landroid/net/Uri;
    .param p2, "serverContent"    # Landroid/net/Uri;
    .param p3, "renderer"    # Landroid/net/Uri;
    .param p4, "deleteAll"    # Landroid/net/Uri;

    .prologue
    .line 414
    sget-object v0, Lcom/samsung/musicplus/library/dlna/DlnaManager;->sDlnaManager:Lcom/samsung/musicplus/library/dlna/DlnaManager;

    if-nez v0, :cond_0

    .line 415
    new-instance v0, Lcom/samsung/musicplus/library/dlna/DlnaManager;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/samsung/musicplus/library/dlna/DlnaManager;-><init>(Landroid/content/Context;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;)V

    sput-object v0, Lcom/samsung/musicplus/library/dlna/DlnaManager;->sDlnaManager:Lcom/samsung/musicplus/library/dlna/DlnaManager;

    .line 417
    :cond_0
    sget-object v0, Lcom/samsung/musicplus/library/dlna/DlnaManager;->sDlnaManager:Lcom/samsung/musicplus/library/dlna/DlnaManager;

    return-object v0
.end method

.method private handleDeviceFinderError(Lcom/samsung/android/allshare/ERROR;Ljava/lang/String;)V
    .locals 3
    .param p1, "err"    # Lcom/samsung/android/allshare/ERROR;
    .param p2, "deviceId"    # Ljava/lang/String;

    .prologue
    .line 894
    sget-object v1, Lcom/samsung/android/allshare/ERROR;->NETWORK_NOT_AVAILABLE:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v1, p1}, Lcom/samsung/android/allshare/ERROR;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 895
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.musicplus.dlna.devicefinder.error"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 896
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.samsung.musicplus.dlna.devicefinder.error.extra"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 898
    const-string v1, "com.samsung.musicplus.dlna.extra.deviceId"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 899
    iget-object v1, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 901
    .end local v0    # "intent":Landroid/content/Intent;
    :cond_0
    return-void
.end method

.method private onUpdateDmrDB(Ljava/util/ArrayList;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/Device;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "dmr":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Device;>;"
    const/4 v10, 0x0

    const/4 v8, 0x0

    .line 624
    sget-object v7, Lcom/samsung/musicplus/library/dlna/DlnaManager;->CLASSNAME:Ljava/lang/String;

    const-string v9, "onUpdateDlnaAvplayerDB()"

    invoke-static {v7, v9}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 626
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 627
    .local v6, "values":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentValues;>;"
    const/4 v1, 0x0

    .line 629
    .local v1, "i":I
    const/4 v5, 0x0

    .line 631
    .local v5, "value":Landroid/content/ContentValues;
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/Device;

    .line 632
    .local v0, "d":Lcom/samsung/android/allshare/Device;
    new-instance v5, Landroid/content/ContentValues;

    .end local v5    # "value":Landroid/content/ContentValues;
    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 633
    .restart local v5    # "value":Landroid/content/ContentValues;
    const-string v7, "_id"

    add-int/lit8 v2, v1, 0x1

    .end local v1    # "i":I
    .local v2, "i":I
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v5, v7, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 634
    const-string v7, "avplayer_id"

    invoke-virtual {v0}, Lcom/samsung/android/allshare/Device;->getID()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v7, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 635
    const-string v7, "avplayer_name"

    invoke-virtual {v0}, Lcom/samsung/android/allshare/Device;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v7, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 636
    invoke-virtual {v0}, Lcom/samsung/android/allshare/Device;->getIcon()Landroid/net/Uri;

    move-result-object v4

    .line 637
    .local v4, "iconUri":Landroid/net/Uri;
    if-eqz v4, :cond_0

    .line 638
    const-string v7, "album_art"

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v7, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 640
    :cond_0
    const-string v7, "nic_id"

    invoke-virtual {v0}, Lcom/samsung/android/allshare/Device;->getNIC()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v7, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 641
    const-string v9, "is_seekable_on_paused"

    invoke-virtual {v0}, Lcom/samsung/android/allshare/Device;->isSeekableOnPaused()Z

    move-result v7

    if-eqz v7, :cond_1

    const/4 v7, 0x1

    :goto_1
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v5, v9, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 642
    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v1, v2

    .line 649
    .end local v2    # "i":I
    .restart local v1    # "i":I
    goto :goto_0

    .end local v1    # "i":I
    .restart local v2    # "i":I
    :cond_1
    move v7, v8

    .line 641
    goto :goto_1

    .line 651
    .end local v0    # "d":Lcom/samsung/android/allshare/Device;
    .end local v2    # "i":I
    .end local v4    # "iconUri":Landroid/net/Uri;
    .restart local v1    # "i":I
    :cond_2
    iget-object v7, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    iget-object v9, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager;->MEDIA_RENDERER_URI:Landroid/net/Uri;

    invoke-virtual {v7, v9, v10, v10}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 652
    iget-object v7, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    iget-object v10, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager;->MEDIA_RENDERER_URI:Landroid/net/Uri;

    new-array v7, v8, [Landroid/content/ContentValues;

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [Landroid/content/ContentValues;

    invoke-virtual {v9, v10, v7}, Landroid/content/ContentResolver;->bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I

    .line 654
    return-void
.end method

.method private onUpdateDmsDB(Ljava/util/ArrayList;)V
    .locals 10
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/samsung/android/allshare/Device;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "dms":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Device;>;"
    const/4 v9, 0x0

    .line 579
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 580
    .local v6, "values":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/content/ContentValues;>;"
    const/4 v1, 0x1

    .line 581
    .local v1, "i":I
    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/allshare/Device;

    .line 582
    .local v0, "d":Lcom/samsung/android/allshare/Device;
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 583
    .local v5, "value":Landroid/content/ContentValues;
    const-string v7, "_id"

    add-int/lit8 v2, v1, 0x1

    .end local v1    # "i":I
    .local v2, "i":I
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v5, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 584
    const-string v7, "provider_id"

    invoke-virtual {v0}, Lcom/samsung/android/allshare/Device;->getID()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 585
    const-string v7, "provider_name"

    invoke-virtual {v0}, Lcom/samsung/android/allshare/Device;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 587
    invoke-virtual {v0}, Lcom/samsung/android/allshare/Device;->getIcon()Landroid/net/Uri;

    move-result-object v4

    .line 588
    .local v4, "iconUri":Landroid/net/Uri;
    if-eqz v4, :cond_0

    .line 589
    const-string v7, "album_art"

    invoke-virtual {v4}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 591
    :cond_0
    const-string v7, "nic_id"

    invoke-virtual {v0}, Lcom/samsung/android/allshare/Device;->getNIC()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v7, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 592
    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move v1, v2

    .line 598
    .end local v2    # "i":I
    .restart local v1    # "i":I
    goto :goto_0

    .line 604
    .end local v0    # "d":Lcom/samsung/android/allshare/Device;
    .end local v4    # "iconUri":Landroid/net/Uri;
    .end local v5    # "value":Landroid/content/ContentValues;
    :cond_1
    iget-object v7, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    iget-object v8, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager;->MEDIA_SERVER_URI:Landroid/net/Uri;

    invoke-virtual {v7, v8, v9, v9}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 605
    iget-object v7, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    iget-object v9, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager;->MEDIA_SERVER_URI:Landroid/net/Uri;

    const/4 v7, 0x0

    new-array v7, v7, [Landroid/content/ContentValues;

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [Landroid/content/ContentValues;

    invoke-virtual {v8, v9, v7}, Landroid/content/ContentResolver;->bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I

    .line 607
    return-void
.end method

.method private sendDlnaInfo(Ljava/lang/String;)V
    .locals 2
    .param p1, "action"    # Ljava/lang/String;

    .prologue
    .line 920
    const/4 v0, -0x1

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/samsung/musicplus/library/dlna/DlnaManager;->sendDlnaInfo(Ljava/lang/String;ILjava/lang/String;)V

    .line 921
    return-void
.end method

.method private sendDlnaInfo(Ljava/lang/String;ILjava/lang/String;)V
    .locals 4
    .param p1, "action"    # Ljava/lang/String;
    .param p2, "what"    # I
    .param p3, "deviceId"    # Ljava/lang/String;

    .prologue
    .line 924
    sget-object v1, Lcom/samsung/musicplus/library/dlna/DlnaManager;->CLASSNAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sendDlnaInfo() - action: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " extraWhat: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " extraDeviceId: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 927
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 928
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.samsung.musicplus.dlna.connectivitychanged"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 929
    const-string v1, "com.samsung.musicplus.dlna.connectivitychanged.extra.what"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 930
    const-string v1, "com.samsung.musicplus.dlna.extra.deviceId"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 936
    :cond_0
    iget-object v1, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 937
    return-void
.end method

.method private sendStickyDlnaInfo(Ljava/lang/String;ILjava/lang/String;)V
    .locals 4
    .param p1, "action"    # Ljava/lang/String;
    .param p2, "what"    # I
    .param p3, "deviceId"    # Ljava/lang/String;

    .prologue
    .line 904
    sget-object v1, Lcom/samsung/musicplus/library/dlna/DlnaManager;->CLASSNAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sendStickyDlnaInfo() - action: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " extraWhat: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " extraDeviceId: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 907
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 912
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "com.samsung.musicplus.dlna.flat.searching.info"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 913
    const-string v1, "com.samsung.musicplus.dlna.flat.searching.extra.what"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 914
    const-string v1, "com.samsung.musicplus.dlna.extra.deviceId"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 916
    :cond_0
    iget-object v1, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendStickyBroadcast(Landroid/content/Intent;)V

    .line 917
    return-void
.end method

.method private updateDmrList()V
    .locals 3

    .prologue
    .line 613
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager;->mDeviceFinder:Lcom/samsung/android/allshare/media/MediaDeviceFinder;

    sget-object v1, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_AVPLAYER:Lcom/samsung/android/allshare/Device$DeviceType;

    invoke-virtual {v0, v1}, Lcom/samsung/android/allshare/media/MediaDeviceFinder;->getDevices(Lcom/samsung/android/allshare/Device$DeviceType;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager;->mContext:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/samsung/android/allshare/extension/DeviceChecker;->getDeviceCheckedList(Ljava/util/ArrayList;Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager;->mDmrList:Ljava/util/ArrayList;

    .line 616
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager;->mDmrList:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 621
    :goto_0
    return-void

    .line 619
    :cond_0
    sget-object v0, Lcom/samsung/musicplus/library/dlna/DlnaManager;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "updateDMRList() - mDMRList.size()? "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager;->mDmrList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 620
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager;->mDmrList:Ljava/util/ArrayList;

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/library/dlna/DlnaManager;->onUpdateDmrDB(Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method private updateDmsList()V
    .locals 4

    .prologue
    .line 562
    iget-object v1, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager;->mDeviceFinder:Lcom/samsung/android/allshare/media/MediaDeviceFinder;

    sget-object v2, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_PROVIDER:Lcom/samsung/android/allshare/Device$DeviceType;

    invoke-virtual {v1, v2}, Lcom/samsung/android/allshare/media/MediaDeviceFinder;->getDevices(Lcom/samsung/android/allshare/Device$DeviceType;)Ljava/util/ArrayList;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager;->mContext:Landroid/content/Context;

    invoke-static {v1, v2}, Lcom/samsung/android/allshare/extension/DeviceChecker;->getDeviceCheckedList(Ljava/util/ArrayList;Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v0

    .line 564
    .local v0, "dms":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Device;>;"
    invoke-direct {p0, v0}, Lcom/samsung/musicplus/library/dlna/DlnaManager;->onUpdateDmsDB(Ljava/util/ArrayList;)V

    .line 566
    if-nez v0, :cond_0

    .line 571
    :goto_0
    return-void

    .line 569
    :cond_0
    iput-object v0, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager;->mDmsList:Ljava/util/ArrayList;

    .line 570
    sget-object v1, Lcom/samsung/musicplus/library/dlna/DlnaManager;->CLASSNAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "updateDmsList() - mDmsList.size()? "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager;->mDmsList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public bindDlnaService()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 454
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager;->MEDIA_DELETE_ALL_URI:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 455
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager;->mContext:Landroid/content/Context;

    new-instance v1, Lcom/samsung/musicplus/library/dlna/DlnaManager$1;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/library/dlna/DlnaManager$1;-><init>(Lcom/samsung/musicplus/library/dlna/DlnaManager;)V

    const-string v2, "com.samsung.android.allshare.media"

    invoke-static {v0, v1, v2}, Lcom/samsung/android/allshare/ServiceConnector;->createServiceProvider(Landroid/content/Context;Lcom/samsung/android/allshare/ServiceConnector$IServiceConnectEventListener;Ljava/lang/String;)Lcom/samsung/android/allshare/ERROR;

    .line 500
    return-void
.end method

.method public createSecAVPlayer(Ljava/lang/String;)Lcom/samsung/musicplus/library/dlna/DlnaManager$SimpleAVPlayer;
    .locals 7
    .param p1, "dmrId"    # Ljava/lang/String;

    .prologue
    .line 1048
    const/4 v3, 0x0

    .line 1049
    .local v3, "simpleAVPlayer":Lcom/samsung/musicplus/library/dlna/DlnaManager$SimpleAVPlayer;
    iget-object v4, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager;->mDeviceFinder:Lcom/samsung/android/allshare/media/MediaDeviceFinder;

    if-eqz v4, :cond_0

    .line 1050
    iget-object v4, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager;->mDeviceFinder:Lcom/samsung/android/allshare/media/MediaDeviceFinder;

    sget-object v5, Lcom/samsung/android/allshare/Device$DeviceType;->DEVICE_AVPLAYER:Lcom/samsung/android/allshare/Device$DeviceType;

    invoke-virtual {v4, v5}, Lcom/samsung/android/allshare/media/MediaDeviceFinder;->getDevices(Lcom/samsung/android/allshare/Device$DeviceType;)Ljava/util/ArrayList;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager;->mContext:Landroid/content/Context;

    invoke-static {v4, v5}, Lcom/samsung/android/allshare/extension/DeviceChecker;->getDeviceCheckedList(Ljava/util/ArrayList;Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1052
    .local v0, "dmrList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Device;>;"
    if-eqz v0, :cond_0

    .line 1053
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 1054
    .local v2, "len":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_0

    .line 1055
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/allshare/Device;

    invoke-virtual {v4}, Lcom/samsung/android/allshare/Device;->getID()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1056
    new-instance v3, Lcom/samsung/musicplus/library/dlna/DlnaManager$SimpleAVPlayer;

    .end local v3    # "simpleAVPlayer":Lcom/samsung/musicplus/library/dlna/DlnaManager$SimpleAVPlayer;
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/samsung/android/allshare/media/AVPlayer;

    iget-object v5, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4, v5}, Lcom/samsung/musicplus/library/dlna/DlnaManager$SimpleAVPlayer;-><init>(Lcom/samsung/android/allshare/media/AVPlayer;Landroid/content/Context;)V

    .line 1062
    .end local v0    # "dmrList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Device;>;"
    .end local v1    # "i":I
    .end local v2    # "len":I
    .restart local v3    # "simpleAVPlayer":Lcom/samsung/musicplus/library/dlna/DlnaManager$SimpleAVPlayer;
    :cond_0
    sget-object v4, Lcom/samsung/musicplus/library/dlna/DlnaManager;->CLASSNAME:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "createSecAVPlayer() - simpleAVPlayer: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/musicplus/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1063
    return-object v3

    .line 1054
    .restart local v0    # "dmrList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Device;>;"
    .restart local v1    # "i":I
    .restart local v2    # "len":I
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public download([J)Z
    .locals 10
    .param p1, "ids"    # [J

    .prologue
    .line 955
    sget-object v7, Lcom/samsung/musicplus/library/dlna/DlnaManager;->CLASSNAME:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "download - items: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    array-length v9, p1

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 956
    const/4 v6, 0x0

    .line 958
    .local v6, "success":Z
    iget-object v7, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager;->mDlnaServiceProvider:Lcom/samsung/android/allshare/media/MediaServiceProvider;

    invoke-virtual {v7}, Lcom/samsung/android/allshare/media/MediaServiceProvider;->getDownloader()Lcom/samsung/android/allshare/extension/SECDownloader;

    move-result-object v0

    .line 959
    .local v0, "downloader":Lcom/samsung/android/allshare/extension/SECDownloader;
    if-eqz v0, :cond_1

    .line 960
    array-length v4, p1

    .line 961
    .local v4, "len":I
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 962
    .local v3, "itemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Item;>;"
    const/4 v5, 0x0

    .line 964
    .local v5, "providerName":Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v4, :cond_0

    .line 965
    aget-wide v8, p1, v1

    invoke-direct {p0, v8, v9}, Lcom/samsung/musicplus/library/dlna/DlnaManager;->getDownloadSongInfo(J)Lcom/samsung/musicplus/library/dlna/DlnaManager$DownloadSongInfo;

    move-result-object v2

    .line 966
    .local v2, "info":Lcom/samsung/musicplus/library/dlna/DlnaManager$DownloadSongInfo;
    # getter for: Lcom/samsung/musicplus/library/dlna/DlnaManager$DownloadSongInfo;->seed:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/musicplus/library/dlna/DlnaManager$DownloadSongInfo;->access$1700(Lcom/samsung/musicplus/library/dlna/DlnaManager$DownloadSongInfo;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/samsung/android/allshare/extension/ItemExtractor;->create(Ljava/lang/String;)Lcom/samsung/android/allshare/Item;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 967
    # getter for: Lcom/samsung/musicplus/library/dlna/DlnaManager$DownloadSongInfo;->providerName:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/musicplus/library/dlna/DlnaManager$DownloadSongInfo;->access$1800(Lcom/samsung/musicplus/library/dlna/DlnaManager$DownloadSongInfo;)Ljava/lang/String;

    move-result-object v5

    .line 964
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 969
    .end local v2    # "info":Lcom/samsung/musicplus/library/dlna/DlnaManager$DownloadSongInfo;
    :cond_0
    invoke-virtual {v0, v5, v3}, Lcom/samsung/android/allshare/extension/SECDownloader;->Download(Ljava/lang/String;Ljava/util/ArrayList;)Z

    move-result v6

    .line 978
    .end local v1    # "i":I
    .end local v3    # "itemList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/samsung/android/allshare/Item;>;"
    .end local v4    # "len":I
    .end local v5    # "providerName":Ljava/lang/String;
    :cond_1
    return v6
.end method

.method public refresh()V
    .locals 2

    .prologue
    .line 509
    sget-object v0, Lcom/samsung/musicplus/library/dlna/DlnaManager;->CLASSNAME:Ljava/lang/String;

    const-string v1, "refresh()"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 510
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager;->mDeviceFinder:Lcom/samsung/android/allshare/media/MediaDeviceFinder;

    if-eqz v0, :cond_0

    .line 511
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager;->mDeviceFinder:Lcom/samsung/android/allshare/media/MediaDeviceFinder;

    invoke-virtual {v0}, Lcom/samsung/android/allshare/media/MediaDeviceFinder;->refresh()V

    .line 513
    :cond_0
    return-void
.end method

.method public release()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 434
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager;->mDlnaServiceProvider:Lcom/samsung/android/allshare/media/MediaServiceProvider;

    if-eqz v0, :cond_0

    .line 435
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager;->mDlnaServiceProvider:Lcom/samsung/android/allshare/media/MediaServiceProvider;

    invoke-static {v0}, Lcom/samsung/android/allshare/ServiceConnector;->deleteServiceProvider(Lcom/samsung/android/allshare/ServiceProvider;)V

    .line 436
    iput-object v1, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager;->mDlnaServiceProvider:Lcom/samsung/android/allshare/media/MediaServiceProvider;

    .line 438
    :cond_0
    sput-object v1, Lcom/samsung/musicplus/library/dlna/DlnaManager;->sDlnaManager:Lcom/samsung/musicplus/library/dlna/DlnaManager;

    .line 439
    sget-object v0, Lcom/samsung/musicplus/library/dlna/DlnaManager;->CLASSNAME:Ljava/lang/String;

    const-string v1, "release()"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 440
    return-void
.end method

.method public searchAudioContents(Ljava/lang/String;)V
    .locals 7
    .param p1, "dmsId"    # Ljava/lang/String;

    .prologue
    .line 530
    sget-object v4, Lcom/samsung/musicplus/library/dlna/DlnaManager;->CLASSNAME:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "searchAudioContents() - mDMSList.size()? "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager;->mDmsList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " DMS id : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 533
    iget-object v4, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager;->mFlatProvider:Lcom/samsung/android/allshare/extension/FlatProvider;

    if-eqz v4, :cond_0

    .line 534
    iget-object v4, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager;->mFlatProvider:Lcom/samsung/android/allshare/extension/FlatProvider;

    iget-object v5, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager;->mFlatBrowseResponseListener:Lcom/samsung/android/allshare/extension/FlatProvider$IFlatProviderConnection;

    invoke-virtual {v4, v5}, Lcom/samsung/android/allshare/extension/FlatProvider;->cancelFlatBrowse(Lcom/samsung/android/allshare/extension/FlatProvider$IFlatProviderConnection;)V

    .line 537
    :cond_0
    const/4 v1, 0x0

    .line 538
    .local v1, "isValid":Z
    iget-object v4, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager;->mDmsList:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 539
    .local v2, "len":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v2, :cond_1

    .line 540
    iget-object v4, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager;->mDmsList:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/allshare/media/Provider;

    .line 541
    .local v3, "provider":Lcom/samsung/android/allshare/media/Provider;
    new-instance v4, Lcom/samsung/android/allshare/extension/FlatProvider;

    invoke-direct {v4, v3}, Lcom/samsung/android/allshare/extension/FlatProvider;-><init>(Lcom/samsung/android/allshare/media/Provider;)V

    iput-object v4, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager;->mFlatProvider:Lcom/samsung/android/allshare/extension/FlatProvider;

    .line 542
    iget-object v4, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager;->mFlatProvider:Lcom/samsung/android/allshare/extension/FlatProvider;

    invoke-virtual {v4}, Lcom/samsung/android/allshare/extension/FlatProvider;->getID()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 544
    sget-object v4, Lcom/samsung/musicplus/library/dlna/DlnaManager;->CLASSNAME:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "searchAudioContents() - "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager;->mFlatProvider:Lcom/samsung/android/allshare/extension/FlatProvider;

    invoke-virtual {v6}, Lcom/samsung/android/allshare/extension/FlatProvider;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " is a valid provider device and start search audio items"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 547
    iget-object v4, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager;->mFlatProvider:Lcom/samsung/android/allshare/extension/FlatProvider;

    sget-object v5, Lcom/samsung/android/allshare/Item$MediaType;->ITEM_AUDIO:Lcom/samsung/android/allshare/Item$MediaType;

    iget-object v6, p0, Lcom/samsung/musicplus/library/dlna/DlnaManager;->mFlatBrowseResponseListener:Lcom/samsung/android/allshare/extension/FlatProvider$IFlatProviderConnection;

    invoke-virtual {v4, v5, v6}, Lcom/samsung/android/allshare/extension/FlatProvider;->startFlatBrowse(Lcom/samsung/android/allshare/Item$MediaType;Lcom/samsung/android/allshare/extension/FlatProvider$IFlatProviderConnection;)V

    .line 549
    const/4 v1, 0x1

    .line 553
    .end local v3    # "provider":Lcom/samsung/android/allshare/media/Provider;
    :cond_1
    if-nez v1, :cond_2

    .line 554
    sget-object v4, Lcom/samsung/musicplus/library/dlna/DlnaManager;->CLASSNAME:Ljava/lang/String;

    const-string v5, "searchAudioContents() - there is no match provider."

    invoke-static {v4, v5}, Lcom/samsung/musicplus/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 556
    :cond_2
    return-void

    .line 539
    .restart local v3    # "provider":Lcom/samsung/android/allshare/media/Provider;
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
