.class public Lcom/samsung/musicplus/library/storage/StorageManagerCompat;
.super Ljava/lang/Object;
.source "StorageManagerCompat.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getVolumePaths(Landroid/os/storage/StorageManager;)[Ljava/lang/String;
    .locals 1
    .param p0, "sm"    # Landroid/os/storage/StorageManager;

    .prologue
    .line 15
    invoke-virtual {p0}, Landroid/os/storage/StorageManager;->getVolumePaths()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getVolumeState(Landroid/os/storage/StorageManager;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "sm"    # Landroid/os/storage/StorageManager;
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 25
    invoke-virtual {p0, p1}, Landroid/os/storage/StorageManager;->getVolumeState(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
