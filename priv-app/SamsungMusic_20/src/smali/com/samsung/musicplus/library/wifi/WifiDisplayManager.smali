.class public Lcom/samsung/musicplus/library/wifi/WifiDisplayManager;
.super Ljava/lang/Object;
.source "WifiDisplayManager.java"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final CLASSNAME:Ljava/lang/String;

.field public static final WFD_CAUSE:Ljava/lang/String; = "WfdManager.EXTRA_CAUSE_INFO"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final WFD_CONTROL_SERVICE:Ljava/lang/String; = "WfdManager.WIFIDISPLAY_CONTROL_FROM_SERVICE"
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static sManager:Lcom/samsung/musicplus/library/wifi/WifiDisplayManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    const-class v0, Lcom/samsung/musicplus/library/wifi/WifiDisplayManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/library/wifi/WifiDisplayManager;->CLASSNAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/samsung/musicplus/library/wifi/WifiDisplayManager;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 47
    sget-object v1, Lcom/samsung/musicplus/library/wifi/WifiDisplayManager;->sManager:Lcom/samsung/musicplus/library/wifi/WifiDisplayManager;

    if-nez v1, :cond_0

    .line 48
    new-instance v0, Lcom/samsung/musicplus/library/wifi/WifiDisplayManager;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/library/wifi/WifiDisplayManager;-><init>(Landroid/content/Context;)V

    .line 49
    .local v0, "wm":Lcom/samsung/musicplus/library/wifi/WifiDisplayManager;
    invoke-direct {v0}, Lcom/samsung/musicplus/library/wifi/WifiDisplayManager;->isInitialized()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 50
    sput-object v0, Lcom/samsung/musicplus/library/wifi/WifiDisplayManager;->sManager:Lcom/samsung/musicplus/library/wifi/WifiDisplayManager;

    .line 55
    .end local v0    # "wm":Lcom/samsung/musicplus/library/wifi/WifiDisplayManager;
    :cond_0
    :goto_0
    sget-object v1, Lcom/samsung/musicplus/library/wifi/WifiDisplayManager;->sManager:Lcom/samsung/musicplus/library/wifi/WifiDisplayManager;

    return-object v1

    .line 52
    .restart local v0    # "wm":Lcom/samsung/musicplus/library/wifi/WifiDisplayManager;
    :cond_1
    const/4 v1, 0x0

    sput-object v1, Lcom/samsung/musicplus/library/wifi/WifiDisplayManager;->sManager:Lcom/samsung/musicplus/library/wifi/WifiDisplayManager;

    goto :goto_0
.end method

.method private isInitialized()Z
    .locals 1

    .prologue
    .line 71
    const/4 v0, 0x0

    return v0
.end method


# virtual methods
.method public enableWfdPlayer()V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 99
    return-void
.end method

.method public getDisplayDeviceAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 194
    const/4 v0, 0x0

    return-object v0
.end method

.method public getDisplayDeviceName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 175
    const/4 v0, 0x0

    return-object v0
.end method

.method public isWfdConnected()Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 156
    const/4 v0, 0x0

    return v0
.end method

.method public setWfdTerminate()V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 112
    return-void
.end method

.method public setWfdTerminateByFramework()V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 138
    return-void
.end method

.method public setWfdTerminateExt(Z)V
    .locals 0
    .param p1, "intendedChangePath"    # Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 123
    return-void
.end method
