.class Lcom/samsung/musicplus/library/drm/LgtDrmManager;
.super Ljava/lang/Object;
.source "LgtDrmManager.java"


# static fields
.field private static final CLASSNAME:Ljava/lang/String;

.field public static final LGT_DRM_ERROR_CODE_FAIL_INIT:I = 0x70001

.field public static final LGT_DRM_ERROR_CODE_OK:I

.field public static final LGT_DRM_SAVE_PATH:Ljava/lang/String;


# instance fields
.field private mContext:Landroid/content/Context;

.field private mDRMInterface:LDigiCAP/LGT/DRM/DRMInterface;

.field private mPopupType:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 25
    const-class v0, Lcom/samsung/musicplus/library/drm/LgtDrmManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/library/drm/LgtDrmManager;->CLASSNAME:Ljava/lang/String;

    .line 28
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/Music/DRM"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/library/drm/LgtDrmManager;->LGT_DRM_SAVE_PATH:Ljava/lang/String;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    const/4 v1, 0x0

    iput v1, p0, Lcom/samsung/musicplus/library/drm/LgtDrmManager;->mPopupType:I

    .line 44
    iput-object p1, p0, Lcom/samsung/musicplus/library/drm/LgtDrmManager;->mContext:Landroid/content/Context;

    .line 46
    :try_start_0
    new-instance v1, LDigiCAP/LGT/DRM/DRMInterface;

    invoke-direct {v1}, LDigiCAP/LGT/DRM/DRMInterface;-><init>()V

    iput-object v1, p0, Lcom/samsung/musicplus/library/drm/LgtDrmManager;->mDRMInterface:LDigiCAP/LGT/DRM/DRMInterface;

    .line 47
    iget-object v1, p0, Lcom/samsung/musicplus/library/drm/LgtDrmManager;->mDRMInterface:LDigiCAP/LGT/DRM/DRMInterface;

    invoke-virtual {v1}, LDigiCAP/LGT/DRM/DRMInterface;->loadLibrary()V

    .line 48
    iget-object v1, p0, Lcom/samsung/musicplus/library/drm/LgtDrmManager;->mDRMInterface:LDigiCAP/LGT/DRM/DRMInterface;

    invoke-virtual {v1}, LDigiCAP/LGT/DRM/DRMInterface;->LDRMInitDRMLib()I

    move-result v1

    if-nez v1, :cond_0

    .line 49
    sget-object v1, Lcom/samsung/musicplus/library/drm/LgtDrmManager;->CLASSNAME:Ljava/lang/String;

    const-string v2, "LGT DRM LIB Init Success!!!!"

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    .line 54
    :cond_0
    :goto_0
    return-void

    .line 51
    :catch_0
    move-exception v0

    .line 52
    .local v0, "e":Ljava/lang/UnsatisfiedLinkError;
    sget-object v1, Lcom/samsung/musicplus/library/drm/LgtDrmManager;->CLASSNAME:Ljava/lang/String;

    const-string v2, "LGT DRM LIB Init Error!!!!"

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private checkValidDate(Ljava/lang/String;)Z
    .locals 10
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 247
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 248
    .local v2, "expireDate":Ljava/lang/StringBuffer;
    iget-object v7, p0, Lcom/samsung/musicplus/library/drm/LgtDrmManager;->mDRMInterface:LDigiCAP/LGT/DRM/DRMInterface;

    invoke-virtual {v7, p1, v2}, LDigiCAP/LGT/DRM/DRMInterface;->LDRMGetExpireDate(Ljava/lang/String;Ljava/lang/StringBuffer;)I

    move-result v3

    .line 249
    .local v3, "expireError":I
    const-string v7, "LGT"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, " checkValidDate() ExpireDate : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " error : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    if-nez v3, :cond_1

    .line 251
    new-instance v4, Ljava/text/SimpleDateFormat;

    const-string v7, "yyyy-MM-dd\'T\'HH:mm:ss\'Z\'"

    invoke-direct {v4, v7}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 252
    .local v4, "sdf":Ljava/text/SimpleDateFormat;
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0}, Ljava/util/Date;-><init>()V

    .line 253
    .local v0, "currentDate":Ljava/util/Date;
    new-instance v7, Ljava/util/SimpleTimeZone;

    const-string v8, "Europe/London"

    invoke-direct {v7, v5, v8}, Ljava/util/SimpleTimeZone;-><init>(ILjava/lang/String;)V

    invoke-virtual {v4, v7}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 254
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-virtual {v0, v8, v9}, Ljava/util/Date;->setTime(J)V

    .line 256
    const-string v7, "LGT"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "CurrentDate : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    :try_start_0
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/util/Date;->compareTo(Ljava/util/Date;)I
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v7

    if-gez v7, :cond_0

    :goto_0
    move v6, v5

    .line 265
    .end local v0    # "currentDate":Ljava/util/Date;
    .end local v4    # "sdf":Ljava/text/SimpleDateFormat;
    :goto_1
    return v6

    .restart local v0    # "currentDate":Ljava/util/Date;
    .restart local v4    # "sdf":Ljava/text/SimpleDateFormat;
    :cond_0
    move v5, v6

    .line 258
    goto :goto_0

    .line 259
    :catch_0
    move-exception v1

    .line 260
    .local v1, "e":Ljava/text/ParseException;
    invoke-virtual {v1}, Ljava/text/ParseException;->printStackTrace()V

    goto :goto_1

    .line 264
    .end local v0    # "currentDate":Ljava/util/Date;
    .end local v1    # "e":Ljava/text/ParseException;
    .end local v4    # "sdf":Ljava/text/SimpleDateFormat;
    :cond_1
    const/16 v5, 0x21

    iput v5, p0, Lcom/samsung/musicplus/library/drm/LgtDrmManager;->mPopupType:I

    goto :goto_1
.end method

.method private registerCRO(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 7
    .param p1, "outMsg"    # Ljava/lang/String;
    .param p2, "cid"    # Ljava/lang/String;

    .prologue
    .line 195
    const/4 v5, 0x0

    iput v5, p0, Lcom/samsung/musicplus/library/drm/LgtDrmManager;->mPopupType:I

    .line 196
    const/4 v2, 0x0

    .line 197
    .local v2, "result":Z
    new-instance v4, Ljava/util/StringTokenizer;

    const-string v5, "|"

    invoke-direct {v4, p1, v5}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    .local v4, "strToken":Ljava/util/StringTokenizer;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 199
    .local v1, "msg":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :goto_0
    invoke-virtual {v4}, Ljava/util/StringTokenizer;->hasMoreElements()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 200
    invoke-virtual {v4}, Ljava/util/StringTokenizer;->nextElement()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 202
    :cond_0
    new-instance v0, Lcom/samsung/musicplus/library/drm/LgtDrmConnect;

    iget-object v5, p0, Lcom/samsung/musicplus/library/drm/LgtDrmManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v5}, Lcom/samsung/musicplus/library/drm/LgtDrmConnect;-><init>(Landroid/content/Context;)V

    .line 203
    .local v0, "connect":Lcom/samsung/musicplus/library/drm/LgtDrmConnect;
    sget v5, Lcom/samsung/musicplus/library/drm/LgtDrmConnect;->CONTENT_RO:I

    invoke-virtual {v0, p0, v5, v1, p2}, Lcom/samsung/musicplus/library/drm/LgtDrmConnect;->post(Lcom/samsung/musicplus/library/drm/LgtDrmManager;ILjava/util/ArrayList;Ljava/lang/String;)I

    move-result v3

    .line 204
    .local v3, "roRegister":I
    if-nez v3, :cond_1

    .line 205
    const/4 v2, 0x1

    .line 218
    :goto_1
    return v2

    .line 206
    :cond_1
    const/4 v5, -0x2

    if-ne v3, v5, :cond_2

    .line 207
    const/16 v5, 0x1f

    iput v5, p0, Lcom/samsung/musicplus/library/drm/LgtDrmManager;->mPopupType:I

    .line 208
    const-string v5, "LGT"

    const-string v6, "AIRPLANE MODE"

    invoke-static {v5, v6}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    const/4 v2, 0x0

    goto :goto_1

    .line 210
    :cond_2
    const/4 v5, -0x3

    if-ne v3, v5, :cond_3

    .line 211
    const/16 v5, 0x21

    iput v5, p0, Lcom/samsung/musicplus/library/drm/LgtDrmManager;->mPopupType:I

    .line 212
    const/4 v2, 0x0

    goto :goto_1

    .line 214
    :cond_3
    const/16 v5, 0x20

    iput v5, p0, Lcom/samsung/musicplus/library/drm/LgtDrmManager;->mPopupType:I

    .line 215
    const-string v5, "LGT"

    const-string v6, "REGISTER FAIL"

    invoke-static {v5, v6}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private registerPRO(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 8
    .param p1, "outMsg"    # Ljava/lang/String;
    .param p2, "cid"    # Ljava/lang/String;

    .prologue
    .line 222
    const/4 v2, 0x0

    .line 223
    .local v2, "result":Z
    const-string v5, "LGT"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "registerPRO : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    new-instance v4, Ljava/util/StringTokenizer;

    const-string v5, "|"

    invoke-direct {v4, p1, v5}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 225
    .local v4, "strToken":Ljava/util/StringTokenizer;
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 226
    .local v1, "msg":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    :goto_0
    invoke-virtual {v4}, Ljava/util/StringTokenizer;->hasMoreElements()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 227
    invoke-virtual {v4}, Ljava/util/StringTokenizer;->nextElement()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 230
    :cond_0
    new-instance v0, Lcom/samsung/musicplus/library/drm/LgtDrmConnect;

    iget-object v5, p0, Lcom/samsung/musicplus/library/drm/LgtDrmManager;->mContext:Landroid/content/Context;

    invoke-direct {v0, v5}, Lcom/samsung/musicplus/library/drm/LgtDrmConnect;-><init>(Landroid/content/Context;)V

    .line 231
    .local v0, "connect":Lcom/samsung/musicplus/library/drm/LgtDrmConnect;
    sget v5, Lcom/samsung/musicplus/library/drm/LgtDrmConnect;->PARENT_RO:I

    invoke-virtual {v0, p0, v5, v1, p2}, Lcom/samsung/musicplus/library/drm/LgtDrmConnect;->post(Lcom/samsung/musicplus/library/drm/LgtDrmManager;ILjava/util/ArrayList;Ljava/lang/String;)I

    move-result v3

    .line 232
    .local v3, "roRegister":I
    if-nez v3, :cond_1

    .line 233
    const/4 v2, 0x1

    .line 243
    :goto_1
    return v2

    .line 234
    :cond_1
    const/4 v5, -0x2

    if-ne v3, v5, :cond_2

    .line 235
    const/16 v5, 0x1f

    iput v5, p0, Lcom/samsung/musicplus/library/drm/LgtDrmManager;->mPopupType:I

    .line 236
    const-string v5, "LGT"

    const-string v6, "AIRPLANE MODE"

    invoke-static {v5, v6}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    const/4 v2, 0x0

    goto :goto_1

    .line 239
    :cond_2
    const/16 v5, 0x20

    iput v5, p0, Lcom/samsung/musicplus/library/drm/LgtDrmManager;->mPopupType:I

    .line 240
    const-string v5, "LGT"

    const-string v6, "REGISTER FAIL"

    invoke-static {v5, v6}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    const/4 v2, 0x0

    goto :goto_1
.end method


# virtual methods
.method public getDeviceInfo(Ljava/lang/String;)Z
    .locals 7
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 277
    const-string v4, "LGT"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "getDeviceInfo: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 279
    .local v0, "cid":Ljava/lang/StringBuffer;
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 280
    .local v3, "outMsg":Ljava/lang/StringBuffer;
    iget-object v4, p0, Lcom/samsung/musicplus/library/drm/LgtDrmManager;->mDRMInterface:LDigiCAP/LGT/DRM/DRMInterface;

    invoke-virtual {v4, p1, v0}, LDigiCAP/LGT/DRM/DRMInterface;->LDRMGetContentID(Ljava/lang/String;Ljava/lang/StringBuffer;)I

    move-result v1

    .line 281
    .local v1, "code":I
    iget-object v4, p0, Lcom/samsung/musicplus/library/drm/LgtDrmManager;->mDRMInterface:LDigiCAP/LGT/DRM/DRMInterface;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v3, v5}, LDigiCAP/LGT/DRM/DRMInterface;->LDRMGetDeviceInfo(Ljava/lang/StringBuffer;Ljava/lang/String;)I

    move-result v2

    .line 283
    .local v2, "errorCode":I
    if-nez v1, :cond_0

    if-nez v2, :cond_0

    .line 284
    const/4 v4, 0x1

    .line 287
    :goto_0
    return v4

    .line 286
    :cond_0
    const-string v4, "LGT"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Dev Info err : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 287
    const/4 v4, 0x0

    goto :goto_0
.end method

.method getExpireDate(Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 293
    const-string v6, "LGT"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "getExpireDate: "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 294
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 295
    .local v2, "expireDate1":Ljava/lang/StringBuffer;
    iget-object v6, p0, Lcom/samsung/musicplus/library/drm/LgtDrmManager;->mDRMInterface:LDigiCAP/LGT/DRM/DRMInterface;

    invoke-virtual {v6, p1, v2}, LDigiCAP/LGT/DRM/DRMInterface;->LDRMGetExpireDate(Ljava/lang/String;Ljava/lang/StringBuffer;)I

    move-result v3

    .line 297
    .local v3, "expireError":I
    new-instance v4, Ljava/text/SimpleDateFormat;

    const-string v6, "yyyy-MM-dd\'T\'HH:mm:ss\'Z\'"

    invoke-direct {v4, v6}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 298
    .local v4, "sdf":Ljava/text/SimpleDateFormat;
    new-instance v6, Ljava/util/SimpleTimeZone;

    const/4 v7, 0x1

    const-string v8, "Europe/London"

    invoke-direct {v6, v7, v8}, Ljava/util/SimpleTimeZone;-><init>(ILjava/lang/String;)V

    invoke-virtual {v4, v6}, Ljava/text/SimpleDateFormat;->setTimeZone(Ljava/util/TimeZone;)V

    .line 301
    :try_start_0
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    .line 302
    .local v1, "ed":Ljava/util/Date;
    const-string v6, "9999-99-99T99:99:99Z"

    invoke-virtual {v4, v6}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v6

    invoke-virtual {v1, v6}, Ljava/util/Date;->compareTo(Ljava/util/Date;)I

    move-result v6

    if-nez v6, :cond_0

    .line 303
    const-string v5, "unlimited"

    .line 312
    .local v5, "time":Ljava/lang/String;
    :goto_0
    if-nez v3, :cond_1

    .line 315
    .end local v1    # "ed":Ljava/util/Date;
    .end local v5    # "time":Ljava/lang/String;
    :goto_1
    return-object v5

    .line 305
    .restart local v1    # "ed":Ljava/util/Date;
    :cond_0
    invoke-virtual {v1}, Ljava/util/Date;->toLocaleString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    .restart local v5    # "time":Ljava/lang/String;
    goto :goto_0

    .line 307
    .end local v1    # "ed":Ljava/util/Date;
    .end local v5    # "time":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 308
    .local v0, "e":Ljava/text/ParseException;
    invoke-virtual {v0}, Ljava/text/ParseException;->printStackTrace()V

    .line 309
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    .line 315
    .end local v0    # "e":Ljava/text/ParseException;
    .restart local v1    # "ed":Ljava/util/Date;
    .restart local v5    # "time":Ljava/lang/String;
    :cond_1
    const/4 v5, 0x0

    goto :goto_1
.end method

.method getInvalidDrmPopupInfo()Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;
    .locals 2

    .prologue
    .line 78
    new-instance v0, Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;

    invoke-direct {v0}, Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;-><init>()V

    .line 79
    .local v0, "info":Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;
    iget v1, p0, Lcom/samsung/musicplus/library/drm/LgtDrmManager;->mPopupType:I

    iput v1, v0, Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;->type:I

    .line 80
    iget v1, v0, Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;->type:I

    packed-switch v1, :pswitch_data_0

    .line 91
    const/4 v1, 0x5

    iput v1, v0, Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;->text1:I

    .line 94
    :goto_0
    return-object v0

    .line 82
    :pswitch_0
    const/4 v1, 0x6

    iput v1, v0, Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;->text1:I

    goto :goto_0

    .line 85
    :pswitch_1
    const/4 v1, 0x7

    iput v1, v0, Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;->text1:I

    goto :goto_0

    .line 88
    :pswitch_2
    const/4 v1, 0x2

    iput v1, v0, Lcom/samsung/musicplus/library/drm/DrmManager$PopupInfo;->text1:I

    goto :goto_0

    .line 80
    :pswitch_data_0
    .packed-switch 0x1f
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method handleRoap(Ljava/lang/String;)I
    .locals 1
    .param p1, "url"    # Ljava/lang/String;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/samsung/musicplus/library/drm/LgtDrmManager;->mDRMInterface:LDigiCAP/LGT/DRM/DRMInterface;

    invoke-virtual {v0, p1}, LDigiCAP/LGT/DRM/DRMInterface;->LDRMHandleROAP(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method isDrm(Ljava/lang/String;)Z
    .locals 2
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 66
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, ".odf"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 67
    const/4 v0, 0x1

    .line 69
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method isValid(Ljava/lang/String;)Z
    .locals 13
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 98
    const/4 v9, 0x0

    .line 100
    .local v9, "result":Z
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 101
    .local v0, "cid":Ljava/lang/StringBuffer;
    new-instance v8, Ljava/lang/StringBuffer;

    invoke-direct {v8}, Ljava/lang/StringBuffer;-><init>()V

    .line 103
    .local v8, "outMsg":Ljava/lang/StringBuffer;
    iget-object v10, p0, Lcom/samsung/musicplus/library/drm/LgtDrmManager;->mDRMInterface:LDigiCAP/LGT/DRM/DRMInterface;

    invoke-virtual {v10, p1, v0}, LDigiCAP/LGT/DRM/DRMInterface;->LDRMGetContentID(Ljava/lang/String;Ljava/lang/StringBuffer;)I

    move-result v1

    .line 104
    .local v1, "code":I
    if-eqz v1, :cond_0

    .line 105
    const/4 v10, 0x0

    .line 191
    :goto_0
    return v10

    .line 107
    :cond_0
    new-instance v3, Ljava/io/File;

    sget-object v10, Lcom/samsung/musicplus/library/drm/LgtDrmManager;->LGT_DRM_SAVE_PATH:Ljava/lang/String;

    invoke-direct {v3, v10}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 108
    .local v3, "drmDirectory":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v10

    if-nez v10, :cond_1

    .line 109
    invoke-virtual {v3}, Ljava/io/File;->mkdir()Z

    move-result v10

    if-nez v10, :cond_1

    .line 110
    sget-object v10, Lcom/samsung/musicplus/library/drm/LgtDrmManager;->CLASSNAME:Ljava/lang/String;

    const-string v11, "Can\'t make directory"

    invoke-static {v10, v11}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v10, v9

    .line 111
    goto :goto_0

    .line 115
    :cond_1
    iget-object v10, p0, Lcom/samsung/musicplus/library/drm/LgtDrmManager;->mDRMInterface:LDigiCAP/LGT/DRM/DRMInterface;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v8, v11}, LDigiCAP/LGT/DRM/DRMInterface;->LDRMGetDeviceInfo(Ljava/lang/StringBuffer;Ljava/lang/String;)I

    move-result v5

    .line 117
    .local v5, "errorCode":I
    sget-object v10, Lcom/samsung/musicplus/library/drm/LgtDrmManager;->CLASSNAME:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "LGT DRM! error code : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, " MSG : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    if-nez v5, :cond_5

    .line 120
    new-instance v6, Ljava/lang/StringBuffer;

    invoke-direct {v6}, Ljava/lang/StringBuffer;-><init>()V

    .line 121
    .local v6, "expireDate":Ljava/lang/StringBuffer;
    iget-object v10, p0, Lcom/samsung/musicplus/library/drm/LgtDrmManager;->mDRMInterface:LDigiCAP/LGT/DRM/DRMInterface;

    invoke-virtual {v10, p1, v6}, LDigiCAP/LGT/DRM/DRMInterface;->LDRMGetExpireDate(Ljava/lang/String;Ljava/lang/StringBuffer;)I

    move-result v7

    .line 122
    .local v7, "expireError":I
    if-nez v7, :cond_4

    .line 123
    sget-object v10, Lcom/samsung/musicplus/library/drm/LgtDrmManager;->CLASSNAME:Ljava/lang/String;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Expire Date : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/library/drm/LgtDrmManager;->checkValidDate(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 125
    const/4 v9, 0x1

    .end local v6    # "expireDate":Ljava/lang/StringBuffer;
    .end local v7    # "expireError":I
    :cond_2
    :goto_1
    move v10, v9

    .line 191
    goto :goto_0

    .line 127
    .restart local v6    # "expireDate":Ljava/lang/StringBuffer;
    .restart local v7    # "expireError":I
    :cond_3
    iget-object v10, p0, Lcom/samsung/musicplus/library/drm/LgtDrmManager;->mDRMInterface:LDigiCAP/LGT/DRM/DRMInterface;

    invoke-virtual {v10}, LDigiCAP/LGT/DRM/DRMInterface;->LDRMCleanupROStorage()I

    .line 128
    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {p0, v10, v11}, Lcom/samsung/musicplus/library/drm/LgtDrmManager;->registerCRO(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 129
    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {p0, v10, v11}, Lcom/samsung/musicplus/library/drm/LgtDrmManager;->registerPRO(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 130
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/library/drm/LgtDrmManager;->getDeviceInfo(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 131
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/library/drm/LgtDrmManager;->checkValidDate(Ljava/lang/String;)Z

    move-result v9

    goto :goto_1

    .line 137
    :cond_4
    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {p0, v10, v11}, Lcom/samsung/musicplus/library/drm/LgtDrmManager;->registerCRO(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 138
    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {p0, v10, v11}, Lcom/samsung/musicplus/library/drm/LgtDrmManager;->registerPRO(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 139
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/library/drm/LgtDrmManager;->getDeviceInfo(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 140
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/library/drm/LgtDrmManager;->checkValidDate(Ljava/lang/String;)Z

    move-result v9

    goto :goto_1

    .line 145
    .end local v6    # "expireDate":Ljava/lang/StringBuffer;
    .end local v7    # "expireError":I
    :cond_5
    const v10, -0x90097

    if-ne v5, v10, :cond_7

    .line 147
    const-string v10, "LGT"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "0x00090097 : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/samsung/musicplus/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 148
    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {p0, v10, v11}, Lcom/samsung/musicplus/library/drm/LgtDrmManager;->registerCRO(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 149
    new-instance v2, Ljava/lang/StringBuffer;

    invoke-direct {v2}, Ljava/lang/StringBuffer;-><init>()V

    .line 150
    .local v2, "croOutMsg":Ljava/lang/StringBuffer;
    iget-object v10, p0, Lcom/samsung/musicplus/library/drm/LgtDrmManager;->mDRMInterface:LDigiCAP/LGT/DRM/DRMInterface;

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v2, v11}, LDigiCAP/LGT/DRM/DRMInterface;->LDRMGetDeviceInfo(Ljava/lang/StringBuffer;Ljava/lang/String;)I

    move-result v4

    .line 151
    .local v4, "error":I
    if-nez v4, :cond_6

    .line 152
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/library/drm/LgtDrmManager;->checkValidDate(Ljava/lang/String;)Z

    move-result v9

    goto/16 :goto_1

    .line 154
    :cond_6
    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {p0, v10, v11}, Lcom/samsung/musicplus/library/drm/LgtDrmManager;->registerPRO(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 155
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/library/drm/LgtDrmManager;->getDeviceInfo(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 156
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/library/drm/LgtDrmManager;->checkValidDate(Ljava/lang/String;)Z

    move-result v9

    goto/16 :goto_1

    .line 161
    .end local v2    # "croOutMsg":Ljava/lang/StringBuffer;
    .end local v4    # "error":I
    :cond_7
    const v10, -0x90098

    if-ne v5, v10, :cond_8

    .line 163
    const-string v10, "LGT"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "0x00090098 : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Lcom/samsung/musicplus/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 164
    iget-object v10, p0, Lcom/samsung/musicplus/library/drm/LgtDrmManager;->mDRMInterface:LDigiCAP/LGT/DRM/DRMInterface;

    invoke-virtual {v10}, LDigiCAP/LGT/DRM/DRMInterface;->LDRMCleanupROStorage()I

    .line 165
    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {p0, v10, v11}, Lcom/samsung/musicplus/library/drm/LgtDrmManager;->registerCRO(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 166
    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {p0, v10, v11}, Lcom/samsung/musicplus/library/drm/LgtDrmManager;->registerPRO(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 167
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/library/drm/LgtDrmManager;->getDeviceInfo(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 168
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/library/drm/LgtDrmManager;->checkValidDate(Ljava/lang/String;)Z

    move-result v9

    goto/16 :goto_1

    .line 172
    :cond_8
    const v10, -0x70090

    if-ne v5, v10, :cond_9

    .line 173
    const-string v10, "LGT"

    const-string v11, "-458896"

    invoke-static {v10, v11}, Lcom/samsung/musicplus/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    const/4 v9, 0x0

    goto/16 :goto_1

    .line 175
    :cond_9
    const v10, -0x150030

    if-ne v5, v10, :cond_a

    .line 176
    const-string v10, "LGT"

    const-string v11, "-1376304"

    invoke-static {v10, v11}, Lcom/samsung/musicplus/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    const/4 v9, 0x0

    goto/16 :goto_1

    .line 179
    :cond_a
    const-string v10, "LGT"

    const-string v11, "Else"

    invoke-static {v10, v11}, Lcom/samsung/musicplus/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    iget-object v10, p0, Lcom/samsung/musicplus/library/drm/LgtDrmManager;->mDRMInterface:LDigiCAP/LGT/DRM/DRMInterface;

    invoke-virtual {v10}, LDigiCAP/LGT/DRM/DRMInterface;->LDRMDestroyDRMLib()V

    .line 181
    iget-object v10, p0, Lcom/samsung/musicplus/library/drm/LgtDrmManager;->mDRMInterface:LDigiCAP/LGT/DRM/DRMInterface;

    invoke-virtual {v10}, LDigiCAP/LGT/DRM/DRMInterface;->LDRMInitDRMLib()I

    .line 182
    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {p0, v10, v11}, Lcom/samsung/musicplus/library/drm/LgtDrmManager;->registerCRO(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 183
    invoke-virtual {v8}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {p0, v10, v11}, Lcom/samsung/musicplus/library/drm/LgtDrmManager;->registerPRO(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 184
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/library/drm/LgtDrmManager;->getDeviceInfo(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 185
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/library/drm/LgtDrmManager;->checkValidDate(Ljava/lang/String;)Z

    move-result v9

    goto/16 :goto_1
.end method

.method release()V
    .locals 3

    .prologue
    .line 57
    :try_start_0
    iget-object v1, p0, Lcom/samsung/musicplus/library/drm/LgtDrmManager;->mDRMInterface:LDigiCAP/LGT/DRM/DRMInterface;

    invoke-virtual {v1}, LDigiCAP/LGT/DRM/DRMInterface;->LDRMDestroyDRMLib()V

    .line 58
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/musicplus/library/drm/LgtDrmManager;->mDRMInterface:LDigiCAP/LGT/DRM/DRMInterface;

    .line 59
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/musicplus/library/drm/LgtDrmManager;->mContext:Landroid/content/Context;
    :try_end_0
    .catch Ljava/lang/UnsatisfiedLinkError; {:try_start_0 .. :try_end_0} :catch_0

    .line 63
    :goto_0
    return-void

    .line 60
    :catch_0
    move-exception v0

    .line 61
    .local v0, "e":Ljava/lang/UnsatisfiedLinkError;
    sget-object v1, Lcom/samsung/musicplus/library/drm/LgtDrmManager;->CLASSNAME:Ljava/lang/String;

    const-string v2, "LGT DRM LIB Init release Error!!!!"

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
