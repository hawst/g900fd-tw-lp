.class public Lcom/samsung/musicplus/library/audio/SoundAliveV2;
.super Ljava/lang/Object;
.source "SoundAliveV2.java"


# static fields
.field private static ID3_TO_SQUARE_POSITION:[I = null

.field private static final SEC_GENRES_STR:[Ljava/lang/String;

.field public static final SOUNDALIVE_ADVANCED_3D:I = 0x7

.field public static final SOUNDALIVE_ADVANCED_BASS:I = 0x8

.field public static final SOUNDALIVE_ADVANCED_CLARITY:I = 0x9

.field public static final SOUNDALIVE_ADVANCED_EQ:I = 0x6

.field public static final SOUNDALIVE_BAND_LEVEL_LENGTH:I = 0x7

.field public static final SOUNDALIVE_GENRE_CLASSIC:I = 0x2

.field public static final SOUNDALIVE_GENRE_JAZZ:I = 0x10

.field public static final SOUNDALIVE_GENRE_NONE:I = 0xc

.field public static final SOUNDALIVE_GENRE_POP:I = 0xd

.field public static final SOUNDALIVE_GENRE_ROCK:I = 0xa

.field public static final SOUNDALIVE_PRESET_CLUB:I = 0x4

.field public static final SOUNDALIVE_PRESET_CONCERTHALL:I = 0x5

.field public static final SOUNDALIVE_PRESET_NORMAL:I = 0x0

.field public static final SOUNDALIVE_PRESET_STUDIO:I = 0x3

.field public static final SOUNDALIVE_PRESET_TUBE:I = 0x1

.field public static final SOUNDALIVE_PRESET_VIRT71:I = 0x2

.field public static final SOUNDALIVE_SQUARE_POSITION_LENGTH:I = 0x2

.field public static final SOUNDALIVE_STRENGTH_LENGTH:I = 0x3

.field public static final SOUNDALIVE_STRENGTH_MODE_3D:I = 0x0

.field public static final SOUNDALIVE_STRENGTH_MODE_BASS:I = 0x1

.field public static final SOUNDALIVE_STRENGTH_MODE_CLARITY:I = 0x2

.field private static final TAG:Ljava/lang/String; = "MusicService"

.field private static mSoundAlive:Landroid/media/audiofx/SoundAlive;

.field private static final sGenreEqMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/16 v1, 0x1c

    .line 281
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/samsung/musicplus/library/audio/SoundAliveV2;->ID3_TO_SQUARE_POSITION:[I

    .line 293
    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "Alternative/Indie"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "Blues"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "Children\'s"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "Classical"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "Comedy/Spoken"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "Country"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "Dance"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "Easy Listening"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "Electronic"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "Folk"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "Holiday"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "House"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "Jazz"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "Latin"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "New Age"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "Others"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "Pop"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "Rap/Hip Hop"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "Reggae"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "Religious"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "R&B/Soul"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "Rock"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "Soundtrack"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "Trance"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "<unknown>"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "Vocal"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "World"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "Rap / Hip-hop"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/musicplus/library/audio/SoundAliveV2;->SEC_GENRES_STR:[Ljava/lang/String;

    .line 300
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/musicplus/library/audio/SoundAliveV2;->sGenreEqMap:Ljava/util/HashMap;

    return-void

    .line 281
    nop

    :array_0
    .array-data 4
        0xa
        0x10
        0xd
        0x2
        0xd
        0xd
        0xd
        0xd
        0xd
        0xa
        0xd
        0xd
        0x10
        0xd
        0x2
        0xd
        0xd
        0xd
        0xd
        0xd
        0xd
        0xa
        0x2
        0xd
        0xc
        0xd
        0xd
        0xd
    .end array-data
.end method

.method public constructor <init>(II)V
    .locals 1
    .param p1, "priority"    # I
    .param p2, "audioSessionId"    # I

    .prologue
    .line 181
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 182
    new-instance v0, Landroid/media/audiofx/SoundAlive;

    invoke-direct {v0, p1, p2}, Landroid/media/audiofx/SoundAlive;-><init>(II)V

    sput-object v0, Lcom/samsung/musicplus/library/audio/SoundAliveV2;->mSoundAlive:Landroid/media/audiofx/SoundAlive;

    .line 183
    return-void
.end method

.method public static getSoundaliveAudioEffect(Ljava/lang/String;)I
    .locals 3
    .param p0, "genreName"    # Ljava/lang/String;

    .prologue
    .line 315
    const/16 v0, 0xc

    .line 317
    .local v0, "soundAlive":I
    if-nez p0, :cond_0

    move v1, v0

    .line 327
    .end local v0    # "soundAlive":I
    .local v1, "soundAlive":I
    :goto_0
    return v1

    .line 320
    .end local v1    # "soundAlive":I
    .restart local v0    # "soundAlive":I
    :cond_0
    sget-object v2, Lcom/samsung/musicplus/library/audio/SoundAliveV2;->sGenreEqMap:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 321
    invoke-static {}, Lcom/samsung/musicplus/library/audio/SoundAliveV2;->initGenreHashMap()V

    .line 324
    :cond_1
    sget-object v2, Lcom/samsung/musicplus/library/audio/SoundAliveV2;->sGenreEqMap:Ljava/util/HashMap;

    invoke-virtual {v2, p0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 325
    sget-object v2, Lcom/samsung/musicplus/library/audio/SoundAliveV2;->sGenreEqMap:Ljava/util/HashMap;

    invoke-virtual {v2, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :cond_2
    move v1, v0

    .line 327
    .end local v0    # "soundAlive":I
    .restart local v1    # "soundAlive":I
    goto :goto_0
.end method

.method private static hasOneMoreSpeaker()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 351
    sget v1, Lcom/samsung/musicplus/library/MusicFeatures;->NUMBER_OF_SPEAKER:I

    if-le v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static initGenreHashMap()V
    .locals 4

    .prologue
    .line 304
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v1, Lcom/samsung/musicplus/library/audio/SoundAliveV2;->SEC_GENRES_STR:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 305
    sget-object v1, Lcom/samsung/musicplus/library/audio/SoundAliveV2;->sGenreEqMap:Ljava/util/HashMap;

    sget-object v2, Lcom/samsung/musicplus/library/audio/SoundAliveV2;->SEC_GENRES_STR:[Ljava/lang/String;

    aget-object v2, v2, v0

    sget-object v3, Lcom/samsung/musicplus/library/audio/SoundAliveV2;->ID3_TO_SQUARE_POSITION:[I

    aget v3, v3, v0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 304
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 307
    :cond_0
    return-void
.end method

.method public static isSupportSoundAliveBT(I)Z
    .locals 2
    .param p0, "soundAlive"    # I

    .prologue
    const/4 v0, 0x1

    .line 360
    if-eq p0, v0, :cond_0

    const/4 v1, 0x2

    if-eq p0, v1, :cond_0

    const/4 v1, 0x3

    if-eq p0, v1, :cond_0

    const/4 v1, 0x4

    if-eq p0, v1, :cond_0

    const/4 v1, 0x5

    if-eq p0, v1, :cond_0

    const/4 v1, 0x7

    if-eq p0, v1, :cond_0

    const/16 v1, 0x8

    if-eq p0, v1, :cond_0

    const/16 v1, 0x9

    if-eq p0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isSupportSoundAliveHDMI(I)Z
    .locals 1
    .param p0, "soundAlive"    # I

    .prologue
    .line 390
    if-nez p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isSupportSoundAliveLineOut(I)Z
    .locals 2
    .param p0, "soundAlive"    # I

    .prologue
    const/4 v0, 0x1

    .line 375
    if-eq p0, v0, :cond_0

    const/4 v1, 0x2

    if-eq p0, v1, :cond_0

    const/4 v1, 0x3

    if-eq p0, v1, :cond_0

    const/4 v1, 0x4

    if-eq p0, v1, :cond_0

    const/4 v1, 0x5

    if-eq p0, v1, :cond_0

    const/4 v1, 0x7

    if-eq p0, v1, :cond_0

    const/16 v1, 0x8

    if-eq p0, v1, :cond_0

    const/16 v1, 0x9

    if-eq p0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isSupportSoundAliveSpeaker(I)Z
    .locals 3
    .param p0, "soundAlive"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 336
    if-ne p0, v1, :cond_1

    .line 340
    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v2, 0x2

    if-eq p0, v2, :cond_2

    const/4 v2, 0x7

    if-ne p0, v2, :cond_3

    :cond_2
    invoke-static {}, Lcom/samsung/musicplus/library/audio/SoundAliveV2;->hasOneMoreSpeaker()Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public getBandLevel(I)I
    .locals 2
    .param p1, "bandLevel"    # I

    .prologue
    .line 245
    sget-object v0, Lcom/samsung/musicplus/library/audio/SoundAliveV2;->mSoundAlive:Landroid/media/audiofx/SoundAlive;

    int-to-short v1, p1

    invoke-virtual {v0, v1}, Landroid/media/audiofx/SoundAlive;->getBandLevel(S)S

    move-result v0

    return v0
.end method

.method public getCurrentPreset()I
    .locals 1

    .prologue
    .line 254
    sget-object v0, Lcom/samsung/musicplus/library/audio/SoundAliveV2;->mSoundAlive:Landroid/media/audiofx/SoundAlive;

    invoke-virtual {v0}, Landroid/media/audiofx/SoundAlive;->getCurrentPreset()S

    move-result v0

    return v0
.end method

.method public getRoundedStrength(S)I
    .locals 1
    .param p1, "effect"    # S

    .prologue
    .line 263
    sget-object v0, Lcom/samsung/musicplus/library/audio/SoundAliveV2;->mSoundAlive:Landroid/media/audiofx/SoundAlive;

    invoke-virtual {v0, p1}, Landroid/media/audiofx/SoundAlive;->getRoundedStrength(S)S

    move-result v0

    return v0
.end method

.method public release()V
    .locals 1

    .prologue
    .line 273
    sget-object v0, Lcom/samsung/musicplus/library/audio/SoundAliveV2;->mSoundAlive:Landroid/media/audiofx/SoundAlive;

    if-eqz v0, :cond_0

    .line 274
    sget-object v0, Lcom/samsung/musicplus/library/audio/SoundAliveV2;->mSoundAlive:Landroid/media/audiofx/SoundAlive;

    invoke-virtual {v0}, Landroid/media/audiofx/SoundAlive;->release()V

    .line 276
    :cond_0
    return-void
.end method

.method public setBandLevel(SS)V
    .locals 3
    .param p1, "band"    # S
    .param p2, "level"    # S

    .prologue
    .line 222
    :try_start_0
    sget-object v1, Lcom/samsung/musicplus/library/audio/SoundAliveV2;->mSoundAlive:Landroid/media/audiofx/SoundAlive;

    invoke-virtual {v1, p1, p2}, Landroid/media/audiofx/SoundAlive;->setBandLevel(SS)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 228
    :goto_0
    return-void

    .line 223
    :catch_0
    move-exception v0

    .line 226
    .local v0, "e":Ljava/lang/IllegalStateException;
    const-string v1, "MusicService"

    const-string v2, "SoundAliveV2 - setBandLevel() : IllegalStateException!!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setEnabled(Z)V
    .locals 1
    .param p1, "isEnable"    # Z

    .prologue
    .line 192
    sget-object v0, Lcom/samsung/musicplus/library/audio/SoundAliveV2;->mSoundAlive:Landroid/media/audiofx/SoundAlive;

    if-eqz v0, :cond_0

    .line 193
    sget-object v0, Lcom/samsung/musicplus/library/audio/SoundAliveV2;->mSoundAlive:Landroid/media/audiofx/SoundAlive;

    invoke-virtual {v0, p1}, Landroid/media/audiofx/SoundAlive;->setEnabled(Z)I

    .line 195
    :cond_0
    return-void
.end method

.method public setSquarePostion(II)V
    .locals 1
    .param p1, "sqRow"    # I
    .param p2, "sqCol"    # I

    .prologue
    .line 236
    sget-object v0, Lcom/samsung/musicplus/library/audio/SoundAliveV2;->mSoundAlive:Landroid/media/audiofx/SoundAlive;

    invoke-virtual {v0, p1, p2}, Landroid/media/audiofx/SoundAlive;->setSquarePostion(II)V

    .line 237
    return-void
.end method

.method public setStrength(SS)V
    .locals 1
    .param p1, "effect"    # S
    .param p2, "mode"    # S

    .prologue
    .line 212
    sget-object v0, Lcom/samsung/musicplus/library/audio/SoundAliveV2;->mSoundAlive:Landroid/media/audiofx/SoundAlive;

    invoke-virtual {v0, p1, p2}, Landroid/media/audiofx/SoundAlive;->setStrength(SS)V

    .line 213
    return-void
.end method

.method public usePreset(S)V
    .locals 1
    .param p1, "effect"    # S

    .prologue
    .line 203
    sget-object v0, Lcom/samsung/musicplus/library/audio/SoundAliveV2;->mSoundAlive:Landroid/media/audiofx/SoundAlive;

    invoke-virtual {v0, p1}, Landroid/media/audiofx/SoundAlive;->usePreset(S)V

    .line 204
    return-void
.end method
