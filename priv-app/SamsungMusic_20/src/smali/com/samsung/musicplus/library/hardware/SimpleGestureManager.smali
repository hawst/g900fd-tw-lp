.class public Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;
.super Ljava/lang/Object;
.source "SimpleGestureManager.java"


# static fields
.field public static final ACTION_GESTURE:Ljava/lang/String; = "com.samsung.musicplus.GESTURE"

.field private static final CLASSNAME:Ljava/lang/String;

.field public static final EXTRA_COMMAND:Ljava/lang/String; = "command"

.field public static final GESTURE_IR_PROVIDER:Ljava/lang/String; = "ir_provider"

.field public static final GESTURE_SWEEP_LEFT:I = 0x1

.field public static final GESTURE_SWEEP_RIGHT:I = 0x0

.field public static final GESTURE_TYPE_AIR_BROWSE:Ljava/lang/String; = "air_motion_turn"

.field private static sSimpleGestureManager:Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;


# instance fields
.field private mConnected:Z

.field private mContext:Landroid/content/Context;

.field private mGestureListner:Lcom/samsung/android/service/gesture/GestureListener;

.field private mGestureManager:Lcom/samsung/android/service/gesture/GestureManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    const-class v0, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;->CLASSNAME:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 146
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 120
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;->mConnected:Z

    .line 147
    iput-object p1, p0, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;->mContext:Landroid/content/Context;

    .line 148
    new-instance v0, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager$1;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager$1;-><init>(Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;)V

    iput-object v0, p0, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;->mGestureListner:Lcom/samsung/android/service/gesture/GestureListener;

    .line 163
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;->CLASSNAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;)Lcom/samsung/android/service/gesture/GestureManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;->mGestureManager:Lcom/samsung/android/service/gesture/GestureManager;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;)Lcom/samsung/android/service/gesture/GestureListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;->mGestureListner:Lcom/samsung/android/service/gesture/GestureListener;

    return-object v0
.end method

.method static synthetic access$402(Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;
    .param p1, "x1"    # Z

    .prologue
    .line 48
    iput-boolean p1, p0, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;->mConnected:Z

    return p1
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 140
    sget-object v0, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;->sSimpleGestureManager:Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;

    if-nez v0, :cond_0

    .line 141
    new-instance v0, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;->sSimpleGestureManager:Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;

    .line 143
    :cond_0
    sget-object v0, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;->sSimpleGestureManager:Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;

    return-object v0
.end method

.method public static isEnableAirBrowse(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 291
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 292
    .local v0, "cr":Landroid/content/ContentResolver;
    const-string v3, "air_motion_turn"

    invoke-static {v0, v3, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-ne v3, v1, :cond_0

    .line 295
    :goto_0
    return v1

    :cond_0
    move v1, v2

    goto :goto_0
.end method

.method public static isEnableAirBrowseOnLockScreen(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 340
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 341
    .local v0, "cr":Landroid/content/ContentResolver;
    const-string v3, "air_motion_turn_bgm_on_lock_screen"

    invoke-static {v0, v3, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-ne v3, v1, :cond_0

    .line 344
    :goto_0
    return v1

    :cond_0
    move v1, v2

    goto :goto_0
.end method

.method public static isEnableAirBrowseOnNowPlayingScreen(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 315
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 316
    .local v0, "cr":Landroid/content/ContentResolver;
    const-string v3, "air_motion_turn_now_playing_on_music"

    invoke-static {v0, v3, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-ne v3, v1, :cond_0

    .line 319
    :goto_0
    return v1

    :cond_0
    move v1, v2

    goto :goto_0
.end method

.method private startGestureService(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 3
    .param p1, "gestureProvider"    # Ljava/lang/String;
    .param p2, "gestureType"    # Ljava/lang/String;
    .param p3, "isSupportLandMode"    # Z

    .prologue
    .line 205
    new-instance v0, Lcom/samsung/android/service/gesture/GestureManager;

    iget-object v1, p0, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;->mContext:Landroid/content/Context;

    new-instance v2, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager$2;

    invoke-direct {v2, p0, p1, p2, p3}, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager$2;-><init>(Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-direct {v0, v1, v2}, Lcom/samsung/android/service/gesture/GestureManager;-><init>(Landroid/content/Context;Lcom/samsung/android/service/gesture/GestureManager$ServiceConnectionListener;)V

    iput-object v0, p0, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;->mGestureManager:Lcom/samsung/android/service/gesture/GestureManager;

    .line 235
    return-void
.end method


# virtual methods
.method public registerGestureListener(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 4
    .param p1, "gestureProvider"    # Ljava/lang/String;
    .param p2, "gestureType"    # Ljava/lang/String;
    .param p3, "isSupportLandMode"    # Z

    .prologue
    .line 249
    iget-object v0, p0, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;->mGestureManager:Lcom/samsung/android/service/gesture/GestureManager;

    if-nez v0, :cond_0

    .line 250
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;->startGestureService(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 259
    :goto_0
    return-void

    .line 252
    :cond_0
    iget-boolean v0, p0, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;->mConnected:Z

    if-eqz v0, :cond_1

    .line 253
    iget-object v0, p0, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;->mGestureManager:Lcom/samsung/android/service/gesture/GestureManager;

    iget-object v1, p0, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;->mGestureListner:Lcom/samsung/android/service/gesture/GestureListener;

    const-string v2, "ir_provider"

    const-string v3, "air_motion_turn"

    invoke-virtual {v0, v1, v2, v3, p3}, Lcom/samsung/android/service/gesture/GestureManager;->registerListener(Lcom/samsung/android/service/gesture/GestureListener;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    .line 256
    :cond_1
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;->startGestureService(Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public registerGestureListener(Z)V
    .locals 4
    .param p1, "isSupportLandMode"    # Z

    .prologue
    .line 173
    iget-object v0, p0, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;->mGestureManager:Lcom/samsung/android/service/gesture/GestureManager;

    if-nez v0, :cond_0

    .line 174
    const-string v0, "ir_provider"

    const-string v1, "air_motion_turn"

    invoke-direct {p0, v0, v1, p1}, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;->startGestureService(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 183
    :goto_0
    return-void

    .line 176
    :cond_0
    iget-boolean v0, p0, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;->mConnected:Z

    if-eqz v0, :cond_1

    .line 177
    iget-object v0, p0, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;->mGestureManager:Lcom/samsung/android/service/gesture/GestureManager;

    iget-object v1, p0, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;->mGestureListner:Lcom/samsung/android/service/gesture/GestureListener;

    const-string v2, "ir_provider"

    const-string v3, "air_motion_turn"

    invoke-virtual {v0, v1, v2, v3, p1}, Lcom/samsung/android/service/gesture/GestureManager;->registerListener(Lcom/samsung/android/service/gesture/GestureListener;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    .line 180
    :cond_1
    const-string v0, "ir_provider"

    const-string v1, "air_motion_turn"

    invoke-direct {p0, v0, v1, p1}, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;->startGestureService(Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public releaseGestureListener()V
    .locals 3

    .prologue
    .line 194
    sget-object v0, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "releaseGestureListener mConnected : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;->mConnected:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mGestureManager : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;->mGestureManager:Lcom/samsung/android/service/gesture/GestureManager;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mGestureListner : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;->mGestureListner:Lcom/samsung/android/service/gesture/GestureListener;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    iget-boolean v0, p0, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;->mConnected:Z

    if-eqz v0, :cond_0

    .line 197
    iget-object v0, p0, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;->mGestureManager:Lcom/samsung/android/service/gesture/GestureManager;

    if-eqz v0, :cond_0

    .line 198
    iget-object v0, p0, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;->mGestureManager:Lcom/samsung/android/service/gesture/GestureManager;

    iget-object v1, p0, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;->mGestureListner:Lcom/samsung/android/service/gesture/GestureListener;

    const-string v2, "ir_provider"

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/service/gesture/GestureManager;->unregisterListener(Lcom/samsung/android/service/gesture/GestureListener;Ljava/lang/String;)V

    .line 201
    :cond_0
    return-void
.end method

.method public releaseGestureListener(Ljava/lang/String;)V
    .locals 2
    .param p1, "gestureProvider"    # Ljava/lang/String;

    .prologue
    .line 270
    iget-boolean v0, p0, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;->mConnected:Z

    if-eqz v0, :cond_0

    .line 271
    iget-object v0, p0, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;->mGestureManager:Lcom/samsung/android/service/gesture/GestureManager;

    if-eqz v0, :cond_0

    .line 272
    iget-object v0, p0, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;->mGestureManager:Lcom/samsung/android/service/gesture/GestureManager;

    iget-object v1, p0, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;->mGestureListner:Lcom/samsung/android/service/gesture/GestureListener;

    invoke-virtual {v0, v1, p1}, Lcom/samsung/android/service/gesture/GestureManager;->unregisterListener(Lcom/samsung/android/service/gesture/GestureListener;Ljava/lang/String;)V

    .line 275
    :cond_0
    return-void
.end method
