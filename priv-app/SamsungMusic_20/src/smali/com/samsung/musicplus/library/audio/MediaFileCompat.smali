.class public Lcom/samsung/musicplus/library/audio/MediaFileCompat;
.super Ljava/lang/Object;
.source "MediaFileCompat.java"


# static fields
.field public static final FILE_TYPE_IMY:I = 0x10

.field public static final FILE_TYPE_MID:I = 0xe

.field public static final FILE_TYPE_SMF:I = 0xf


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getFileType(Ljava/lang/String;)I
    .locals 1
    .param p0, "mime"    # Ljava/lang/String;

    .prologue
    .line 12
    invoke-static {p0}, Landroid/media/MediaFile;->getFileTypeForMimeType(Ljava/lang/String;)I

    move-result v0

    return v0
.end method
