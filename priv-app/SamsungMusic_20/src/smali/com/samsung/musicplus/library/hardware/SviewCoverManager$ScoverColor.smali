.class public interface abstract Lcom/samsung/musicplus/library/hardware/SviewCoverManager$ScoverColor;
.super Ljava/lang/Object;
.source "SviewCoverManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/library/hardware/SviewCoverManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ScoverColor"
.end annotation


# static fields
.field public static final BLACK:I = 0x1

.field public static final CHARCOAL_GRAY:I = 0xa

.field public static final DEFAULT:I = 0x0

.field public static final GRAYISH_BLUE:I = 0x8

.field public static final INDIGO_BLUE:I = 0x6

.field public static final MAGENTA:I = 0x7

.field public static final MUSTARD_YELLOW:I = 0x9

.field public static final OATMEAL:I = 0x5

.field public static final ORANGE:I = 0x4

.field public static final SOFT_PINK:I = 0x3

.field public static final WHITE:I = 0x2
