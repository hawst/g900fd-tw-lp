.class public Lcom/samsung/musicplus/library/app/SearchManagerCompat;
.super Ljava/lang/Object;
.source "SearchManagerCompat.java"


# static fields
.field public static final SUGGEST_COLUMN_MIME_TYPE:Ljava/lang/String; = "suggest_mime_type"

.field public static final SUGGEST_COLUMN_TARGET_TYPE:Ljava/lang/String; = "suggest_target_type"

.field public static final SUGGEST_COLUMN_URI:Ljava/lang/String; = "suggest_uri"

.field public static final SUGGEST_PARAMETER_ETIME:Ljava/lang/String; = "etime"

.field public static final SUGGEST_PARAMETER_STIME:Ljava/lang/String; = "stime"

.field public static final SUGGEST_URI_PATH_REGEX_QUERY:Ljava/lang/String; = "search_suggest_regex_query"

.field public static final SUGGEST_URI_PATH_TAG_QUERY:Ljava/lang/String; = "search_suggest_tag_query"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
