.class public Lcom/samsung/musicplus/library/hardware/AirView;
.super Ljava/lang/Object;
.source "AirView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/library/hardware/AirView$OnAirViewMorePopupListener;,
        Lcom/samsung/musicplus/library/hardware/AirView$OnAirViewPopupListener;,
        Lcom/samsung/musicplus/library/hardware/AirView$OnAirSeekBarPopupListener;,
        Lcom/samsung/musicplus/library/hardware/AirView$SeekHoverListener;,
        Lcom/samsung/musicplus/library/hardware/AirView$Gravity;
    }
.end annotation


# static fields
.field public static final ACTION_INTENT_AIR_VIEW_SETTING_CHANGED:Ljava/lang/String; = "com.sec.gesture.FINGER_AIR_VIEW_SETTINGS_CHANGED"

.field public static final DETECT_TIME:I = 0x258

.field public static final EXTRA_COMMAND:Ljava/lang/String; = "isEnable"

.field public static final TYPE_NONE:I = 0x0

.field public static final TYPE_TOOLTIP:I = 0x1

.field public static final TYPE_USER_CUSTOM:I = 0x3

.field public static final TYPE_WIDGET_DEFAULT:I = 0x2


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 885
    return-void
.end method

.method public static isEnableAirView(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 81
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "finger_air_view"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    .line 85
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public static isEnableAirViewInforPreview(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 125
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "finger_air_view_information_preview"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    .line 129
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public static isEnableAirViewProgressPreview(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 150
    const-string v3, "americano"

    const-string v4, "ro.build.scafe"

    invoke-static {v4}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 151
    const-string v0, "finger_air_view_pregress_bar_preview"

    .line 156
    .local v0, "name":Ljava/lang/String;
    :goto_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    invoke-static {v3, v0, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-ne v3, v1, :cond_1

    .line 159
    :goto_1
    return v1

    .line 153
    .end local v0    # "name":Ljava/lang/String;
    :cond_0
    const-string v0, "finger_air_view_information_preview"

    .restart local v0    # "name":Ljava/lang/String;
    goto :goto_0

    :cond_1
    move v1, v2

    .line 159
    goto :goto_1
.end method

.method public static isEnableHapticFeedback(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 194
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "finger_air_view_sound_and_haptic_feedback"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    .line 198
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public static isEnablePenHovering(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 94
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "pen_hovering"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    .line 97
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public static isEnablePenHoveringIconLabel(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 181
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "pen_hovering_icon_label"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    .line 185
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public static isEnablePenHoveringInforPreview(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 168
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "pen_hovering_information_preview"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    .line 172
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public static isEnablePenHoveringProgressPreview(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 106
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "pen_hovering_progress_preview"

    invoke-static {v2, v3, v1}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v0, :cond_0

    .line 110
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public static isEnableProgress(Landroid/content/Context;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 904
    invoke-static {p0}, Lcom/samsung/musicplus/library/hardware/AirView;->isEnableAirViewProgressPreview(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/samsung/musicplus/library/hardware/AirView;->isEnablePenHoveringInforPreview(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isFingerHovered(Landroid/view/View;)Z
    .locals 1
    .param p0, "v"    # Landroid/view/View;

    .prologue
    .line 852
    invoke-virtual {p0}, Landroid/view/View;->isFingerHovered()Z

    move-result v0

    return v0
.end method

.method public static isHoveringUI(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 914
    if-nez p0, :cond_0

    .line 915
    const/4 v0, 0x0

    .line 917
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "com.sec.feature.hovering_ui"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public static isSimpleAirView(Landroid/content/Context;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 895
    invoke-static {p0}, Lcom/samsung/musicplus/library/hardware/AirView;->isEnablePenHoveringIconLabel(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/samsung/musicplus/library/hardware/AirView;->isEnablePenHoveringInforPreview(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static performHapticFeedback(Landroid/content/Context;Landroid/view/View;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "v"    # Landroid/view/View;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 305
    invoke-static {p0}, Lcom/samsung/musicplus/library/hardware/AirView;->isEnableHapticFeedback(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 307
    const/16 v0, 0x9

    invoke-virtual {p1, v0}, Landroid/view/View;->performHapticFeedback(I)Z

    .line 309
    :cond_0
    return-void
.end method

.method private static prepareCustomPopupWindow(Landroid/view/View;IIZZZ)Landroid/widget/HoverPopupWindow;
    .locals 2
    .param p0, "v"    # Landroid/view/View;
    .param p1, "toolType"    # I
    .param p2, "detectTime"    # I
    .param p3, "enableAnimation"    # Z
    .param p4, "enableGuideLine"    # Z
    .param p5, "enablefhGuideline"    # Z

    .prologue
    .line 573
    if-nez p1, :cond_1

    invoke-virtual {p0}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    .line 575
    .local v0, "hpw":Landroid/widget/HoverPopupWindow;
    :goto_0
    if-eqz v0, :cond_0

    .line 576
    invoke-virtual {v0, p2}, Landroid/widget/HoverPopupWindow;->setHoverDetectTime(I)V

    .line 577
    if-eqz p5, :cond_2

    .line 580
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/HoverPopupWindow;->setFHGuideLineEnabled(Z)V

    .line 585
    :goto_1
    invoke-virtual {v0, p3}, Landroid/widget/HoverPopupWindow;->setFHAnimationEnabled(Z)V

    .line 587
    :cond_0
    return-object v0

    .line 573
    .end local v0    # "hpw":Landroid/widget/HoverPopupWindow;
    :cond_1
    invoke-virtual {p0, p1}, Landroid/view/View;->getHoverPopupWindow(I)Landroid/widget/HoverPopupWindow;

    move-result-object v0

    goto :goto_0

    .line 582
    .restart local v0    # "hpw":Landroid/widget/HoverPopupWindow;
    :cond_2
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/HoverPopupWindow;->setFHGuideLineEnabled(Z)V

    .line 583
    invoke-virtual {v0, p4}, Landroid/widget/HoverPopupWindow;->setGuideLineEnabled(Z)V

    goto :goto_1
.end method

.method private static prepareCustomPopupWindow(Landroid/view/View;IIZZZZ)Landroid/widget/HoverPopupWindow;
    .locals 2
    .param p0, "v"    # Landroid/view/View;
    .param p1, "toolType"    # I
    .param p2, "detectTime"    # I
    .param p3, "enableAnimation"    # Z
    .param p4, "enableGuideLine"    # Z
    .param p5, "enablefhGuideline"    # Z
    .param p6, "enableMoreInfoPicker"    # Z

    .prologue
    .line 593
    if-nez p1, :cond_1

    invoke-virtual {p0}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    .line 595
    .local v0, "hpw":Landroid/widget/HoverPopupWindow;
    :goto_0
    if-eqz v0, :cond_0

    .line 596
    invoke-virtual {v0, p2}, Landroid/widget/HoverPopupWindow;->setHoverDetectTime(I)V

    .line 597
    if-eqz p5, :cond_2

    .line 600
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/HoverPopupWindow;->setFHGuideLineEnabled(Z)V

    .line 605
    :goto_1
    invoke-virtual {v0, p6}, Landroid/widget/HoverPopupWindow;->setInfoPickerColorToAndMoreBottomImg(Z)V

    .line 606
    invoke-virtual {v0, p3}, Landroid/widget/HoverPopupWindow;->setFHAnimationEnabled(Z)V

    .line 608
    :cond_0
    return-object v0

    .line 593
    .end local v0    # "hpw":Landroid/widget/HoverPopupWindow;
    :cond_1
    invoke-virtual {p0, p1}, Landroid/view/View;->getHoverPopupWindow(I)Landroid/widget/HoverPopupWindow;

    move-result-object v0

    goto :goto_0

    .line 602
    .restart local v0    # "hpw":Landroid/widget/HoverPopupWindow;
    :cond_2
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/HoverPopupWindow;->setFHGuideLineEnabled(Z)V

    .line 603
    invoke-virtual {v0, p4}, Landroid/widget/HoverPopupWindow;->setGuideLineEnabled(Z)V

    goto :goto_1
.end method

.method private static preparePenHoverPopup(Landroid/view/View;Landroid/view/View;Landroid/view/View;)V
    .locals 3
    .param p0, "text1"    # Landroid/view/View;
    .param p1, "text2"    # Landroid/view/View;
    .param p2, "text3"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 513
    if-eqz p0, :cond_0

    .line 514
    invoke-virtual {p0, v2}, Landroid/view/View;->getHoverPopupWindow(I)Landroid/widget/HoverPopupWindow;

    move-result-object v0

    .line 515
    .local v0, "hover":Landroid/widget/HoverPopupWindow;
    if-eqz v0, :cond_0

    .line 516
    invoke-virtual {v0, v1}, Landroid/widget/HoverPopupWindow;->setEnabled(Z)V

    .line 520
    .end local v0    # "hover":Landroid/widget/HoverPopupWindow;
    :cond_0
    if-eqz p1, :cond_1

    .line 521
    invoke-virtual {p1, v2}, Landroid/view/View;->getHoverPopupWindow(I)Landroid/widget/HoverPopupWindow;

    move-result-object v0

    .line 522
    .restart local v0    # "hover":Landroid/widget/HoverPopupWindow;
    if-eqz v0, :cond_1

    .line 523
    invoke-virtual {v0, v1}, Landroid/widget/HoverPopupWindow;->setEnabled(Z)V

    .line 527
    .end local v0    # "hover":Landroid/widget/HoverPopupWindow;
    :cond_1
    if-eqz p2, :cond_2

    .line 528
    invoke-virtual {p2, v2}, Landroid/view/View;->getHoverPopupWindow(I)Landroid/widget/HoverPopupWindow;

    move-result-object v0

    .line 529
    .restart local v0    # "hover":Landroid/widget/HoverPopupWindow;
    if-eqz v0, :cond_2

    .line 530
    invoke-virtual {v0, v1}, Landroid/widget/HoverPopupWindow;->setEnabled(Z)V

    .line 533
    .end local v0    # "hover":Landroid/widget/HoverPopupWindow;
    :cond_2
    return-void
.end method

.method private static preparePopupwindow(Landroid/view/View;ZZ)Landroid/widget/HoverPopupWindow;
    .locals 3
    .param p0, "v"    # Landroid/view/View;
    .param p1, "enableAnimation"    # Z
    .param p2, "enableGuideLine"    # Z

    .prologue
    const/4 v2, 0x1

    .line 555
    sget-boolean v1, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_FINGER_AIR_VIEW:Z

    if-eqz v1, :cond_1

    .line 556
    invoke-virtual {p0, v2}, Landroid/view/View;->getHoverPopupWindow(I)Landroid/widget/HoverPopupWindow;

    move-result-object v0

    .line 557
    .local v0, "hpw":Landroid/widget/HoverPopupWindow;
    if-eqz v0, :cond_0

    .line 558
    const/16 v1, 0x258

    invoke-virtual {v0, v1}, Landroid/widget/HoverPopupWindow;->setHoverDetectTime(I)V

    .line 559
    invoke-virtual {v0, v2}, Landroid/widget/HoverPopupWindow;->setFHGuideLineEnabled(Z)V

    .line 560
    invoke-virtual {v0, p1}, Landroid/widget/HoverPopupWindow;->setFHAnimationEnabled(Z)V

    .line 568
    :cond_0
    :goto_0
    return-object v0

    .line 563
    .end local v0    # "hpw":Landroid/widget/HoverPopupWindow;
    :cond_1
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Landroid/view/View;->getHoverPopupWindow(I)Landroid/widget/HoverPopupWindow;

    move-result-object v0

    .line 564
    .restart local v0    # "hpw":Landroid/widget/HoverPopupWindow;
    if-eqz v0, :cond_0

    .line 565
    invoke-virtual {v0, p2}, Landroid/widget/HoverPopupWindow;->setGuideLineEnabled(Z)V

    goto :goto_0
.end method

.method public static setDisable(Landroid/view/View;)V
    .locals 1
    .param p0, "v"    # Landroid/view/View;

    .prologue
    .line 843
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/view/View;->setHoverPopupType(I)V

    .line 844
    return-void
.end method

.method public static setHoverScrollMode(Landroid/widget/AbsListView;Z)V
    .locals 0
    .param p0, "v"    # Landroid/widget/AbsListView;
    .param p1, "enable"    # Z

    .prologue
    .line 861
    invoke-virtual {p0, p1}, Landroid/widget/AbsListView;->setHoverScrollMode(Z)V

    .line 862
    return-void
.end method

.method public static setView(Landroid/view/View;I)V
    .locals 2
    .param p0, "v"    # Landroid/view/View;
    .param p1, "gravity"    # I

    .prologue
    .line 321
    if-eqz p0, :cond_0

    .line 322
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Landroid/view/View;->setHoverPopupType(I)V

    .line 323
    invoke-virtual {p0}, Landroid/view/View;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    .line 324
    .local v0, "hpw":Landroid/widget/HoverPopupWindow;
    if-eqz v0, :cond_0

    .line 325
    invoke-virtual {v0, p1}, Landroid/widget/HoverPopupWindow;->setPopupGravity(I)V

    .line 326
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/HoverPopupWindow;->setFHGuideLineEnabled(Z)V

    .line 329
    .end local v0    # "hpw":Landroid/widget/HoverPopupWindow;
    :cond_0
    return-void
.end method

.method public static setView(Landroid/view/View;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;ILcom/samsung/musicplus/library/hardware/AirView$OnAirViewPopupListener;ZZ)V
    .locals 7
    .param p0, "v"    # Landroid/view/View;
    .param p1, "text1"    # Landroid/widget/TextView;
    .param p2, "text2"    # Landroid/widget/TextView;
    .param p3, "text3"    # Landroid/widget/TextView;
    .param p4, "type"    # I
    .param p5, "listener"    # Lcom/samsung/musicplus/library/hardware/AirView$OnAirViewPopupListener;
    .param p6, "enableAnimation"    # Z
    .param p7, "enableGuideLine"    # Z

    .prologue
    .line 448
    if-eqz p0, :cond_0

    .line 454
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Landroid/view/View;->setHoverPopupType(I)V

    .line 455
    const/16 v2, 0x258

    const/4 v5, 0x1

    move-object v0, p0

    move v1, p4

    move v3, p6

    move v4, p7

    invoke-static/range {v0 .. v5}, Lcom/samsung/musicplus/library/hardware/AirView;->prepareCustomPopupWindow(Landroid/view/View;IIZZZ)Landroid/widget/HoverPopupWindow;

    move-result-object v6

    .line 457
    .local v6, "hpw":Landroid/widget/HoverPopupWindow;
    if-eqz v6, :cond_0

    .line 458
    new-instance v0, Lcom/samsung/musicplus/library/hardware/AirView$3;

    invoke-direct {v0, p5, p1, p2, p3}, Lcom/samsung/musicplus/library/hardware/AirView$3;-><init>(Lcom/samsung/musicplus/library/hardware/AirView$OnAirViewPopupListener;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;)V

    invoke-virtual {v6, v0}, Landroid/widget/HoverPopupWindow;->setHoverPopupListener(Landroid/widget/HoverPopupWindow$HoverPopupListener;)V

    .line 485
    .end local v6    # "hpw":Landroid/widget/HoverPopupWindow;
    :cond_0
    invoke-static {p1, p2, p3}, Lcom/samsung/musicplus/library/hardware/AirView;->preparePenHoverPopup(Landroid/view/View;Landroid/view/View;Landroid/view/View;)V

    .line 486
    return-void
.end method

.method public static setView(Landroid/view/View;Landroid/widget/TextView;Landroid/widget/TextView;Lcom/samsung/musicplus/library/hardware/AirView$OnAirViewPopupListener;ZZ)V
    .locals 8
    .param p0, "v"    # Landroid/view/View;
    .param p1, "text1"    # Landroid/widget/TextView;
    .param p2, "text2"    # Landroid/widget/TextView;
    .param p3, "listener"    # Lcom/samsung/musicplus/library/hardware/AirView$OnAirViewPopupListener;
    .param p4, "enableAnimation"    # Z
    .param p5, "enableGuideLine"    # Z

    .prologue
    .line 508
    const/4 v3, 0x0

    const/4 v4, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v5, p3

    move v6, p4

    move v7, p5

    invoke-static/range {v0 .. v7}, Lcom/samsung/musicplus/library/hardware/AirView;->setView(Landroid/view/View;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/TextView;ILcom/samsung/musicplus/library/hardware/AirView$OnAirViewPopupListener;ZZ)V

    .line 510
    return-void
.end method

.method public static setView(Landroid/view/View;Lcom/samsung/musicplus/library/hardware/AirView$OnAirViewMorePopupListener;IIZZZZ)V
    .locals 8
    .param p0, "v"    # Landroid/view/View;
    .param p1, "listener"    # Lcom/samsung/musicplus/library/hardware/AirView$OnAirViewMorePopupListener;
    .param p2, "toolType"    # I
    .param p3, "detectTime"    # I
    .param p4, "enableAnimation"    # Z
    .param p5, "enableGuideLine"    # Z
    .param p6, "enablefhGuideline"    # Z
    .param p7, "enableMoreInfoPicker"    # Z

    .prologue
    .line 674
    if-eqz p0, :cond_0

    .line 675
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Landroid/view/View;->setHoverPopupType(I)V

    move-object v0, p0

    move v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    move v5, p6

    move v6, p7

    .line 676
    invoke-static/range {v0 .. v6}, Lcom/samsung/musicplus/library/hardware/AirView;->prepareCustomPopupWindow(Landroid/view/View;IIZZZZ)Landroid/widget/HoverPopupWindow;

    move-result-object v7

    .line 678
    .local v7, "hpw":Landroid/widget/HoverPopupWindow;
    if-eqz v7, :cond_0

    .line 679
    new-instance v0, Lcom/samsung/musicplus/library/hardware/AirView$5;

    invoke-direct {v0, p1}, Lcom/samsung/musicplus/library/hardware/AirView$5;-><init>(Lcom/samsung/musicplus/library/hardware/AirView$OnAirViewMorePopupListener;)V

    invoke-virtual {v7, v0}, Landroid/widget/HoverPopupWindow;->setHoverPopupListener(Landroid/widget/HoverPopupWindow$HoverPopupListener;)V

    .line 706
    .end local v7    # "hpw":Landroid/widget/HoverPopupWindow;
    :cond_0
    return-void
.end method

.method public static setView(Landroid/view/View;Lcom/samsung/musicplus/library/hardware/AirView$OnAirViewMorePopupListener;ZZ)V
    .locals 2
    .param p0, "v"    # Landroid/view/View;
    .param p1, "listener"    # Lcom/samsung/musicplus/library/hardware/AirView$OnAirViewMorePopupListener;
    .param p2, "enableAnimation"    # Z
    .param p3, "enableGuideLine"    # Z

    .prologue
    .line 392
    if-eqz p0, :cond_0

    .line 398
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Landroid/view/View;->setHoverPopupType(I)V

    .line 399
    invoke-static {p0, p2, p3}, Lcom/samsung/musicplus/library/hardware/AirView;->preparePopupwindow(Landroid/view/View;ZZ)Landroid/widget/HoverPopupWindow;

    move-result-object v0

    .line 400
    .local v0, "hpw":Landroid/widget/HoverPopupWindow;
    if-eqz v0, :cond_0

    .line 401
    new-instance v1, Lcom/samsung/musicplus/library/hardware/AirView$2;

    invoke-direct {v1, p1}, Lcom/samsung/musicplus/library/hardware/AirView$2;-><init>(Lcom/samsung/musicplus/library/hardware/AirView$OnAirViewMorePopupListener;)V

    invoke-virtual {v0, v1}, Landroid/widget/HoverPopupWindow;->setHoverPopupListener(Landroid/widget/HoverPopupWindow$HoverPopupListener;)V

    .line 423
    .end local v0    # "hpw":Landroid/widget/HoverPopupWindow;
    :cond_0
    return-void
.end method

.method public static setView(Landroid/view/View;Lcom/samsung/musicplus/library/hardware/AirView$OnAirViewPopupListener;IIZZZ)V
    .locals 7
    .param p0, "v"    # Landroid/view/View;
    .param p1, "listener"    # Lcom/samsung/musicplus/library/hardware/AirView$OnAirViewPopupListener;
    .param p2, "toolType"    # I
    .param p3, "detectTime"    # I
    .param p4, "enableAnimation"    # Z
    .param p5, "enableGuideLine"    # Z
    .param p6, "enablefhGuideline"    # Z

    .prologue
    .line 627
    if-eqz p0, :cond_0

    .line 628
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Landroid/view/View;->setHoverPopupType(I)V

    move-object v0, p0

    move v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    move v5, p6

    .line 629
    invoke-static/range {v0 .. v5}, Lcom/samsung/musicplus/library/hardware/AirView;->prepareCustomPopupWindow(Landroid/view/View;IIZZZ)Landroid/widget/HoverPopupWindow;

    move-result-object v6

    .line 631
    .local v6, "hpw":Landroid/widget/HoverPopupWindow;
    if-eqz v6, :cond_0

    .line 632
    new-instance v0, Lcom/samsung/musicplus/library/hardware/AirView$4;

    invoke-direct {v0, p1}, Lcom/samsung/musicplus/library/hardware/AirView$4;-><init>(Lcom/samsung/musicplus/library/hardware/AirView$OnAirViewPopupListener;)V

    invoke-virtual {v6, v0}, Landroid/widget/HoverPopupWindow;->setHoverPopupListener(Landroid/widget/HoverPopupWindow$HoverPopupListener;)V

    .line 652
    .end local v6    # "hpw":Landroid/widget/HoverPopupWindow;
    :cond_0
    return-void
.end method

.method public static setView(Landroid/view/View;Lcom/samsung/musicplus/library/hardware/AirView$OnAirViewPopupListener;Z)V
    .locals 1
    .param p0, "v"    # Landroid/view/View;
    .param p1, "listener"    # Lcom/samsung/musicplus/library/hardware/AirView$OnAirViewPopupListener;
    .param p2, "enableAnimation"    # Z

    .prologue
    .line 715
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lcom/samsung/musicplus/library/hardware/AirView;->setView(Landroid/view/View;Lcom/samsung/musicplus/library/hardware/AirView$OnAirViewPopupListener;ZZ)V

    .line 716
    return-void
.end method

.method public static setView(Landroid/view/View;Lcom/samsung/musicplus/library/hardware/AirView$OnAirViewPopupListener;ZZ)V
    .locals 2
    .param p0, "v"    # Landroid/view/View;
    .param p1, "listener"    # Lcom/samsung/musicplus/library/hardware/AirView$OnAirViewPopupListener;
    .param p2, "enableAnimation"    # Z
    .param p3, "enableGuideLine"    # Z

    .prologue
    .line 346
    if-eqz p0, :cond_0

    .line 352
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Landroid/view/View;->setHoverPopupType(I)V

    .line 353
    invoke-static {p0, p2, p3}, Lcom/samsung/musicplus/library/hardware/AirView;->preparePopupwindow(Landroid/view/View;ZZ)Landroid/widget/HoverPopupWindow;

    move-result-object v0

    .line 354
    .local v0, "hpw":Landroid/widget/HoverPopupWindow;
    if-eqz v0, :cond_0

    .line 355
    new-instance v1, Lcom/samsung/musicplus/library/hardware/AirView$1;

    invoke-direct {v1, p1}, Lcom/samsung/musicplus/library/hardware/AirView$1;-><init>(Lcom/samsung/musicplus/library/hardware/AirView$OnAirViewPopupListener;)V

    invoke-virtual {v0, v1}, Landroid/widget/HoverPopupWindow;->setHoverPopupListener(Landroid/widget/HoverPopupWindow$HoverPopupListener;)V

    .line 375
    .end local v0    # "hpw":Landroid/widget/HoverPopupWindow;
    :cond_0
    return-void
.end method

.method public static setView(Landroid/widget/SeekBar;Lcom/samsung/musicplus/library/hardware/AirView$OnAirSeekBarPopupListener;)V
    .locals 3
    .param p0, "v"    # Landroid/widget/SeekBar;
    .param p1, "listener"    # Lcom/samsung/musicplus/library/hardware/AirView$OnAirSeekBarPopupListener;

    .prologue
    .line 724
    const/4 v2, 0x3

    invoke-virtual {p0, v2}, Landroid/widget/SeekBar;->setHoverPopupType(I)V

    .line 725
    invoke-virtual {p0}, Landroid/widget/SeekBar;->getHoverPopupWindow()Landroid/widget/HoverPopupWindow;

    move-result-object v0

    .line 727
    .local v0, "hpw":Landroid/widget/HoverPopupWindow;
    if-eqz v0, :cond_0

    .line 728
    new-instance v1, Lcom/samsung/musicplus/library/hardware/AirView$SeekHoverListener;

    invoke-direct {v1, p1}, Lcom/samsung/musicplus/library/hardware/AirView$SeekHoverListener;-><init>(Lcom/samsung/musicplus/library/hardware/AirView$OnAirSeekBarPopupListener;)V

    .line 729
    .local v1, "shl":Lcom/samsung/musicplus/library/hardware/AirView$SeekHoverListener;
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/HoverPopupWindow;->setInstanceOfProgressBar(Z)V

    .line 730
    invoke-virtual {p0, v1}, Landroid/widget/SeekBar;->setOnSeekBarHoverListener(Landroid/widget/SeekBar$OnSeekBarHoverListener;)V

    .line 731
    invoke-virtual {p0, v1}, Landroid/widget/SeekBar;->setOnHoverListener(Landroid/view/View$OnHoverListener;)V

    .line 733
    .end local v1    # "shl":Lcom/samsung/musicplus/library/hardware/AirView$SeekHoverListener;
    :cond_0
    return-void
.end method
