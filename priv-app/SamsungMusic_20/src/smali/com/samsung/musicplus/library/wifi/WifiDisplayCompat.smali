.class public Lcom/samsung/musicplus/library/wifi/WifiDisplayCompat;
.super Ljava/lang/Object;
.source "WifiDisplayCompat.java"


# instance fields
.field private mDisplay:Landroid/hardware/display/WifiDisplay;


# direct methods
.method public constructor <init>(Landroid/os/Parcelable;)V
    .locals 1
    .param p1, "d"    # Landroid/os/Parcelable;

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    instance-of v0, p1, Landroid/hardware/display/WifiDisplay;

    if-eqz v0, :cond_0

    .line 23
    check-cast p1, Landroid/hardware/display/WifiDisplay;

    .end local p1    # "d":Landroid/os/Parcelable;
    iput-object p1, p0, Lcom/samsung/musicplus/library/wifi/WifiDisplayCompat;->mDisplay:Landroid/hardware/display/WifiDisplay;

    .line 25
    :cond_0
    return-void
.end method


# virtual methods
.method public getDeviceAddress()Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/samsung/musicplus/library/wifi/WifiDisplayCompat;->mDisplay:Landroid/hardware/display/WifiDisplay;

    if-nez v0, :cond_0

    .line 46
    const/4 v0, 0x0

    .line 48
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/library/wifi/WifiDisplayCompat;->mDisplay:Landroid/hardware/display/WifiDisplay;

    invoke-virtual {v0}, Landroid/hardware/display/WifiDisplay;->getDeviceAddress()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getDeviceAlias()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 74
    const-string v0, ""

    return-object v0
.end method

.method public getDeviceName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/samsung/musicplus/library/wifi/WifiDisplayCompat;->mDisplay:Landroid/hardware/display/WifiDisplay;

    if-nez v0, :cond_0

    .line 58
    const/4 v0, 0x0

    .line 60
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/library/wifi/WifiDisplayCompat;->mDisplay:Landroid/hardware/display/WifiDisplay;

    invoke-virtual {v0}, Landroid/hardware/display/WifiDisplay;->getDeviceName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getFriendlyDisplayName()Ljava/lang/String;
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 88
    const-string v0, ""

    return-object v0
.end method

.method public getPrimaryDeviceType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/samsung/musicplus/library/wifi/WifiDisplayCompat;->mDisplay:Landroid/hardware/display/WifiDisplay;

    if-nez v0, :cond_0

    .line 34
    const/4 v0, 0x0

    .line 36
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/library/wifi/WifiDisplayCompat;->mDisplay:Landroid/hardware/display/WifiDisplay;

    invoke-virtual {v0}, Landroid/hardware/display/WifiDisplay;->getPrimaryDeviceType()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public isAvailable()Z
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/samsung/musicplus/library/wifi/WifiDisplayCompat;->mDisplay:Landroid/hardware/display/WifiDisplay;

    if-nez v0, :cond_0

    .line 98
    const/4 v0, 0x0

    .line 100
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/library/wifi/WifiDisplayCompat;->mDisplay:Landroid/hardware/display/WifiDisplay;

    invoke-virtual {v0}, Landroid/hardware/display/WifiDisplay;->isAvailable()Z

    move-result v0

    goto :goto_0
.end method
