.class public Lcom/samsung/musicplus/library/wifi/DisplayManagerCompat;
.super Ljava/lang/Object;
.source "DisplayManagerCompat.java"


# static fields
.field public static final ACTION_WIFI_DISPLAY_STATUS_CHANGED:Ljava/lang/String; = "android.hardware.display.action.WIFI_DISPLAY_STATUS_CHANGED"

.field public static final ADDRESS_INFO:I = 0x1

.field public static final ALL_TOGETHER_RUNNING:I = 0x8

.field private static final CLASSNAME:Ljava/lang/String;

.field public static final DEFAULT_WFD_DEVICE_TYPE:I = 0x9

.field public static final EXTRA_WIFI_DISPLAY_STATUS:Ljava/lang/String; = "android.hardware.display.extra.WIFI_DISPLAY_STATUS"

.field public static final GLOBAL_WIFI_DISPLAY_SETTING_URI:Landroid/net/Uri;

.field public static final GROUP_PLAY_RUNNING:I = 0x7

.field public static final HDMI:I = 0x3

.field public static final LIMITED_PLAY_VIA_SCREEN_MIRRORING:I = 0x6

.field public static final NAME_INFO:I = 0x2

.field public static final NORMAL:I = 0x0

.field public static final POWER_SAVING_ON:I = 0x5

.field public static final SIDE_SYNC_RUNNING:I = 0x4

.field public static final TYPE_INFO:I = 0x3

.field public static final WFD_APP_STATE_PAUSE:I = 0x2

.field public static final WFD_APP_STATE_RESUME:I = 0x1

.field public static final WFD_APP_STATE_SETUP:I = 0x0

.field public static final WFD_APP_STATE_TEARDOWN:I = 0x3

.field public static final WFD_DONGLE_TYPE:I = 0x0

.field public static final WIFI_DIRECT:I = 0x2

.field public static final WIFI_HOTSPOT:I = 0x1


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/samsung/musicplus/library/wifi/DisplayManagerCompat;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/library/wifi/DisplayManagerCompat;->CLASSNAME:Ljava/lang/String;

    .line 69
    const-string v0, "wifi_display_on"

    invoke-static {v0}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/library/wifi/DisplayManagerCompat;->GLOBAL_WIFI_DISPLAY_SETTING_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static checkExceptionalCase(Landroid/hardware/display/DisplayManager;)I
    .locals 1
    .param p0, "dm"    # Landroid/hardware/display/DisplayManager;

    .prologue
    .line 337
    if-eqz p0, :cond_0

    .line 338
    invoke-virtual {p0}, Landroid/hardware/display/DisplayManager;->checkExceptionalCase()I

    move-result v0

    .line 340
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static connectWifiDisplayWithMode(Landroid/hardware/display/DisplayManager;ILjava/lang/String;)V
    .locals 0
    .param p0, "dm"    # Landroid/hardware/display/DisplayManager;
    .param p1, "mode"    # I
    .param p2, "address"    # Ljava/lang/String;

    .prologue
    .line 274
    invoke-virtual {p0, p1, p2}, Landroid/hardware/display/DisplayManager;->connectWifiDisplayWithMode(ILjava/lang/String;)V

    .line 275
    return-void
.end method

.method public static disconnect(Landroid/hardware/display/DisplayManager;)V
    .locals 0
    .param p0, "dm"    # Landroid/hardware/display/DisplayManager;

    .prologue
    .line 214
    invoke-virtual {p0}, Landroid/hardware/display/DisplayManager;->disconnectWifiDisplay()V

    .line 215
    return-void
.end method

.method public static disconnectExt(Landroid/hardware/display/DisplayManager;)V
    .locals 0
    .param p0, "dm"    # Landroid/hardware/display/DisplayManager;

    .prologue
    .line 225
    invoke-virtual {p0}, Landroid/hardware/display/DisplayManager;->disconnectWifiDisplay()V

    .line 226
    return-void
.end method

.method public static getConnectedWfdDeviceInfo(Landroid/hardware/display/DisplayManager;I)Ljava/lang/String;
    .locals 1
    .param p0, "dm"    # Landroid/hardware/display/DisplayManager;
    .param p1, "infoType"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 264
    const-string v0, ""

    return-object v0
.end method

.method public static getDeviceType(Ljava/lang/String;)I
    .locals 5
    .param p0, "primaryDeviceTypeStr"    # Ljava/lang/String;

    .prologue
    .line 304
    const/16 v0, 0x9

    .line 305
    .local v0, "deviceType":I
    if-eqz p0, :cond_0

    .line 307
    :try_start_0
    const-string v2, "-"

    invoke-virtual {p0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 308
    .local v1, "tokens":[Ljava/lang/String;
    const/4 v2, 0x0

    aget-object v2, v1, v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 313
    .end local v1    # "tokens":[Ljava/lang/String;
    :goto_0
    const/4 v2, 0x1

    if-lt v0, v2, :cond_1

    const/16 v2, 0xc

    if-ge v0, v2, :cond_1

    .line 316
    add-int/lit8 v0, v0, -0x1

    .line 321
    :cond_0
    :goto_1
    sget-object v2, Lcom/samsung/musicplus/library/wifi/DisplayManagerCompat;->CLASSNAME:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getWfdDeviceType() - primaryDeviceTypeStr: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " deviceType: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    return v0

    .line 318
    :cond_1
    const/16 v0, 0x9

    goto :goto_1

    .line 309
    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method private static getWfdAppState(I)Landroid/hardware/display/DisplayManager$WfdAppState;
    .locals 1
    .param p0, "state"    # I

    .prologue
    .line 362
    const/4 v0, 0x0

    .line 363
    .local v0, "result":Landroid/hardware/display/DisplayManager$WfdAppState;
    packed-switch p0, :pswitch_data_0

    .line 379
    :goto_0
    return-object v0

    .line 365
    :pswitch_0
    sget-object v0, Landroid/hardware/display/DisplayManager$WfdAppState;->SETUP:Landroid/hardware/display/DisplayManager$WfdAppState;

    .line 366
    goto :goto_0

    .line 368
    :pswitch_1
    sget-object v0, Landroid/hardware/display/DisplayManager$WfdAppState;->RESUME:Landroid/hardware/display/DisplayManager$WfdAppState;

    .line 369
    goto :goto_0

    .line 371
    :pswitch_2
    sget-object v0, Landroid/hardware/display/DisplayManager$WfdAppState;->PAUSE:Landroid/hardware/display/DisplayManager$WfdAppState;

    .line 372
    goto :goto_0

    .line 374
    :pswitch_3
    sget-object v0, Landroid/hardware/display/DisplayManager$WfdAppState;->TEARDOWN:Landroid/hardware/display/DisplayManager$WfdAppState;

    .line 375
    goto :goto_0

    .line 363
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static getWifiDisplayStatus(Landroid/content/Intent;)Lcom/samsung/musicplus/library/wifi/WifiDisplayStatusCompat;
    .locals 2
    .param p0, "i"    # Landroid/content/Intent;

    .prologue
    .line 189
    const-string v1, "android.hardware.display.extra.WIFI_DISPLAY_STATUS"

    invoke-virtual {p0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    .line 190
    .local v0, "p":Landroid/os/Parcelable;
    instance-of v1, v0, Landroid/hardware/display/WifiDisplayStatus;

    if-eqz v1, :cond_0

    .line 191
    new-instance v1, Lcom/samsung/musicplus/library/wifi/WifiDisplayStatusCompat;

    check-cast v0, Landroid/hardware/display/WifiDisplayStatus;

    .end local v0    # "p":Landroid/os/Parcelable;
    invoke-direct {v1, v0}, Lcom/samsung/musicplus/library/wifi/WifiDisplayStatusCompat;-><init>(Landroid/os/Parcelable;)V

    .line 193
    :goto_0
    return-object v1

    .restart local v0    # "p":Landroid/os/Parcelable;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static getWifiDisplayStatus(Landroid/hardware/display/DisplayManager;)Lcom/samsung/musicplus/library/wifi/WifiDisplayStatusCompat;
    .locals 2
    .param p0, "dm"    # Landroid/hardware/display/DisplayManager;

    .prologue
    .line 176
    invoke-virtual {p0}, Landroid/hardware/display/DisplayManager;->getWifiDisplayStatus()Landroid/hardware/display/WifiDisplayStatus;

    move-result-object v0

    .line 177
    .local v0, "s":Landroid/hardware/display/WifiDisplayStatus;
    if-nez v0, :cond_0

    .line 178
    const/4 v1, 0x0

    .line 180
    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Lcom/samsung/musicplus/library/wifi/WifiDisplayStatusCompat;

    invoke-direct {v1, v0}, Lcom/samsung/musicplus/library/wifi/WifiDisplayStatusCompat;-><init>(Landroid/os/Parcelable;)V

    goto :goto_0
.end method

.method public static isScreenMirroringAvailable(Landroid/hardware/display/DisplayManager;)V
    .locals 0
    .param p0, "dm"    # Landroid/hardware/display/DisplayManager;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 245
    return-void
.end method

.method public static scanWifiDisplays(Landroid/hardware/display/DisplayManager;)V
    .locals 0
    .param p0, "dm"    # Landroid/hardware/display/DisplayManager;

    .prologue
    .line 205
    invoke-virtual {p0}, Landroid/hardware/display/DisplayManager;->scanWifiDisplays()V

    .line 206
    return-void
.end method

.method public static setActivityState(Landroid/hardware/display/DisplayManager;I)V
    .locals 1
    .param p0, "dm"    # Landroid/hardware/display/DisplayManager;
    .param p1, "state"    # I

    .prologue
    .line 355
    invoke-static {p1}, Lcom/samsung/musicplus/library/wifi/DisplayManagerCompat;->getWfdAppState(I)Landroid/hardware/display/DisplayManager$WfdAppState;

    move-result-object v0

    .line 356
    .local v0, "was":Landroid/hardware/display/DisplayManager$WfdAppState;
    if-eqz v0, :cond_0

    .line 357
    invoke-virtual {p0, v0}, Landroid/hardware/display/DisplayManager;->setActivityState(Landroid/hardware/display/DisplayManager$WfdAppState;)V

    .line 359
    :cond_0
    return-void
.end method

.method public static stopScan(Landroid/hardware/display/DisplayManager;)V
    .locals 0
    .param p0, "dm"    # Landroid/hardware/display/DisplayManager;

    .prologue
    .line 234
    invoke-virtual {p0}, Landroid/hardware/display/DisplayManager;->stopScanWifiDisplays()V

    .line 235
    return-void
.end method
