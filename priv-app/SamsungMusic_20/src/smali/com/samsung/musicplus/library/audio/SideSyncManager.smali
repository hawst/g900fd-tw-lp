.class public Lcom/samsung/musicplus/library/audio/SideSyncManager;
.super Ljava/lang/Object;
.source "SideSyncManager.java"


# static fields
.field private static final SETTING_SIDESYNC_CONNECTED:Ljava/lang/String; = "sidesync_source_connect"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static isSideSyncConnected(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 27
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 29
    .local v0, "cr":Landroid/content/ContentResolver;
    const-string v3, "sidesync_source_connect"

    invoke-static {v0, v3, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-ne v3, v1, :cond_0

    .line 32
    :goto_0
    return v1

    :cond_0
    move v1, v2

    goto :goto_0
.end method
