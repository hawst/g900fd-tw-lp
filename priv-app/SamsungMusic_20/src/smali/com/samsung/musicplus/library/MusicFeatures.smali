.class public Lcom/samsung/musicplus/library/MusicFeatures;
.super Ljava/lang/Object;
.source "MusicFeatures.java"


# static fields
.field public static final FLAG_CHECK_CALL_VT_SUPPORT:Z = false

.field public static final FLAG_CHECK_IMEI_WHEN_HANDLE_ACTION_MEDIA_BUTTON:Z

.field public static final FLAG_CHECK_KOR_LGT:Z

.field public static final FLAG_CHECK_KOR_SKT:Z

.field public static final FLAG_CHECK_MTPACTIVITY:Z

.field public static final FLAG_DISABLE_MUSIC_STORE:Z = false

.field public static final FLAG_ENABLE_EDIT_META:Z

.field public static final FLAG_ENABLE_FIND_TAG:Z

.field public static final FLAG_ENABLE_SEEK_DELAYED_IN:Z

.field public static final FLAG_JPN_LYRIC:Z

.field public static final FLAG_K2HD:Z

.field public static final FLAG_MUSIC_LDB:Z

.field public static final FLAG_MUSIC_MID_VOLUME_CONTROL:Z = false
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final FLAG_MUSIC_REPEAT_AB:Z

.field public static final FLAG_MUSIC_SUPPORT_SRS_SOUNDALIVE:Z = false

.field public static final FLAG_MUSIC_VIEW:Z

.field public static final FLAG_NOT_SUPPORT_PLAYREADY_DRM:Z

.field public static final FLAG_QC_LPA_ENABLE:Z = false

.field public static final FLAG_SMART_VOLUME:Z

.field public static final FLAG_SUPPORT_ADAPT_SOUND:Z = true

.field public static final FLAG_SUPPORT_ALLSHARE:Z = true

.field public static final FLAG_SUPPORT_AUTO_RECOMMENDATION:Z = true

.field public static final FLAG_SUPPORT_BIGPOND:Z

.field public static final FLAG_SUPPORT_CALL_RINGTONE_DUOS_CDMAGSM:Z = false

.field public static final FLAG_SUPPORT_CALL_RINGTONE_DUOS_CGG:Z = false

.field public static final FLAG_SUPPORT_DATA_CHECK_POPUP:Z

.field public static final FLAG_SUPPORT_DISPLAY_SQUARE_UPDATE_COUNT:Z = false

.field public static final FLAG_SUPPORT_DLNA_DMC_ONLY:Z = false

.field public static final FLAG_SUPPORT_DLNA_MULTI_SPEAKER:Z = false

.field public static final FLAG_SUPPORT_DUALMODE:Z = false

.field public static final FLAG_SUPPORT_FINGER_AIR_VIEW:Z

.field public static final FLAG_SUPPORT_GESTURE_AIR_MOTION:Z

.field public static final FLAG_SUPPORT_INDEPENDENT_MIDI_VOLUME:Z = true

.field public static final FLAG_SUPPORT_MIRROR_CALL:Z

.field public static final FLAG_SUPPORT_MOTION_BOUNCE:Z

.field public static final FLAG_SUPPORT_MOTION_PALM_PAUSE:Z

.field public static final FLAG_SUPPORT_MOTION_TURN_OVER:Z

.field public static FLAG_SUPPORT_MOVE_TO_KNOX:Z = false
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final FLAG_SUPPORT_MULTI_SPEAKER:Z = false

.field public static final FLAG_SUPPORT_NEW_SOUNDALIVE:Z = true

.field public static final FLAG_SUPPORT_NUMERIC_KEYPAD:Z = false

.field public static final FLAG_SUPPORT_OPTION_END:Z = false

.field public static final FLAG_SUPPORT_PINYIN:Z

.field public static final FLAG_SUPPORT_PLAYING_MUSIC_DURING_CALL:Z

.field public static final FLAG_SUPPORT_PLAYLIST_REPEAT_US_ATT_FEATURE:Z

.field public static final FLAG_SUPPORT_SAMSUNG_MUSIC:Z

.field public static final FLAG_SUPPORT_SBEAM:Z = false

.field public static final FLAG_SUPPORT_SECRET_BOX:Z = true

.field public static final FLAG_SUPPORT_SET_BATTERY_ADC:Z

.field public static final FLAG_SUPPORT_SIDESYNC:Z = true
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final FLAG_SUPPORT_SMART_CLIP_META:Z

.field public static final FLAG_SUPPORT_SPLIT_LIST_VIEW:Z

.field public static final FLAG_SUPPORT_SQUARE_VISUAL_INTERACTION_ANIMATION:Z = true

.field public static final FLAG_SUPPORT_SVIEW_COVER_V2:Z

.field public static final FLAG_SUPPORT_SVOICE_FOR_GEAR:Z

.field public static final FLAG_SUPPORT_SWEEP_ON_TABS:Z

.field public static final FLAG_SUPPORT_S_STREAM:Z = true

.field public static final FLAG_SUPPORT_UHQA:Z

.field public static final FLAG_SUPPORT_UNKNOWN_TRANS:Z

.field public static final FLAG_SUPPORT_UWA_CALL:Z

.field public static final FLAG_SUPPORT_VOICE_COMMAND_CONTROL:Z

.field public static final FLAG_SUPPORT_WHITE_THEME:Z = true

.field public static final FLAG_SUPPORT_WIFIDISPLAY_OLD:Z = false

.field public static final FLAG_SUPPORT_WIFI_DISPLAY:Z = true

.field public static final FLAG_SUPPROT_ENHANCED_PLAY_SPEED:Z = true

.field public static final IS_DEFAULT_LAND_MODEL:Z

.field public static final IS_MASS_PROJECT:Z

.field public static final MUSIC_FEATURE_ENABLE_DRM_RINGTONE_CHECK:Z

.field public static final MUSIC_RINGTONE_SIZE_LIMIT:I

.field public static final NUMBER_OF_SPEAKER:I

.field public static final PRODUCT_NAME:Ljava/lang/String;

.field public static final SYSTEM_CONTRY_CODE:Ljava/lang/String;

.field public static final SYSTEM_DEVICE_NAME:Ljava/lang/String;

.field public static final SYSTEM_SALES_CODE:Ljava/lang/String;

.field public static final SYSTEM_VERSION_RELEASE:Ljava/lang/String;

.field private static T_BASE_MODEL:Z

.field private static final sCscFeature:Lcom/sec/android/app/CscFeature;

.field private static final sFloatingFeature:Lcom/samsung/android/feature/FloatingFeature;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 37
    invoke-static {}, Lcom/samsung/android/feature/FloatingFeature;->getInstance()Lcom/samsung/android/feature/FloatingFeature;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/library/MusicFeatures;->sFloatingFeature:Lcom/samsung/android/feature/FloatingFeature;

    .line 39
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/library/MusicFeatures;->sCscFeature:Lcom/sec/android/app/CscFeature;

    .line 52
    invoke-static {}, Lcom/samsung/musicplus/library/MusicFeatures;->getSalesCode()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/library/MusicFeatures;->SYSTEM_SALES_CODE:Ljava/lang/String;

    .line 97
    const-string v0, "ATT"

    invoke-static {}, Lcom/samsung/musicplus/library/MusicFeatures;->getSalesCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_PLAYLIST_REPEAT_US_ATT_FEATURE:Z

    .line 105
    const-string v0, "VZW"

    sget-object v3, Lcom/samsung/musicplus/library/MusicFeatures;->SYSTEM_SALES_CODE:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_PLAYING_MUSIC_DURING_CALL:Z

    .line 113
    const-string v0, "USA"

    invoke-static {}, Lcom/samsung/musicplus/library/MusicFeatures;->getCountryCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_SET_BATTERY_ADC:Z

    .line 120
    const-string v0, "USA"

    invoke-static {}, Lcom/samsung/musicplus/library/MusicFeatures;->getCountryCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_NOT_SUPPORT_PLAYREADY_DRM:Z

    .line 127
    invoke-static {}, Lcom/samsung/musicplus/library/MusicFeatures;->getCountryCode()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/library/MusicFeatures;->SYSTEM_CONTRY_CODE:Ljava/lang/String;

    .line 151
    invoke-static {}, Lcom/samsung/musicplus/library/MusicFeatures;->getOSDeviceName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/library/MusicFeatures;->SYSTEM_DEVICE_NAME:Ljava/lang/String;

    .line 166
    invoke-static {}, Lcom/samsung/musicplus/library/MusicFeatures;->getOSver()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/library/MusicFeatures;->SYSTEM_VERSION_RELEASE:Ljava/lang/String;

    .line 181
    const-string v0, "ro.product.name"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/library/MusicFeatures;->PRODUCT_NAME:Ljava/lang/String;

    .line 184
    sget-object v0, Lcom/samsung/musicplus/library/MusicFeatures;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "trelte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/samsung/musicplus/library/MusicFeatures;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "tbelte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/samsung/musicplus/library/MusicFeatures;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "trlte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/samsung/musicplus/library/MusicFeatures;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "tblte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/samsung/musicplus/library/MusicFeatures;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "SC-01G"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/samsung/musicplus/library/MusicFeatures;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "SCL24"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/samsung/musicplus/library/MusicFeatures;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "tr3calte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/samsung/musicplus/library/MusicFeatures;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "muscat3calte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    :cond_0
    move v0, v2

    :goto_0
    sput-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->T_BASE_MODEL:Z

    .line 211
    const-string v0, "KOREA"

    invoke-static {}, Lcom/samsung/musicplus/library/MusicFeatures;->getCountryCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_CHECK_IMEI_WHEN_HANDLE_ACTION_MEDIA_BUTTON:Z

    .line 219
    const-string v0, "KOREA"

    invoke-static {}, Lcom/samsung/musicplus/library/MusicFeatures;->getCountryCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_CHECK_MTPACTIVITY:Z

    .line 227
    const-string v0, "KOREA"

    invoke-static {}, Lcom/samsung/musicplus/library/MusicFeatures;->getCountryCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_SAMSUNG_MUSIC:Z

    .line 235
    sget-object v0, Lcom/samsung/musicplus/library/MusicFeatures;->sCscFeature:Lcom/sec/android/app/CscFeature;

    const-string v3, "CscFeature_Common_SupportMirrorCall"

    invoke-virtual {v0, v3}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_MIRROR_CALL:Z

    .line 242
    sget-object v0, Lcom/samsung/musicplus/library/MusicFeatures;->sCscFeature:Lcom/sec/android/app/CscFeature;

    const-string v3, "CscFeature_Common_SupportUwaApp"

    invoke-virtual {v0, v3}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_UWA_CALL:Z

    .line 249
    const-string v0, "SKT"

    invoke-static {}, Lcom/samsung/musicplus/library/MusicFeatures;->getSalesCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "SKC"

    invoke-static {}, Lcom/samsung/musicplus/library/MusicFeatures;->getSalesCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "SKO"

    invoke-static {}, Lcom/samsung/musicplus/library/MusicFeatures;->getSalesCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    :cond_1
    move v0, v2

    :goto_1
    sput-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_CHECK_KOR_SKT:Z

    .line 259
    const-string v0, "LGT"

    invoke-static {}, Lcom/samsung/musicplus/library/MusicFeatures;->getSalesCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "LUC"

    invoke-static {}, Lcom/samsung/musicplus/library/MusicFeatures;->getSalesCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "LUO"

    invoke-static {}, Lcom/samsung/musicplus/library/MusicFeatures;->getSalesCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    :cond_2
    move v0, v2

    :goto_2
    sput-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_CHECK_KOR_LGT:Z

    .line 281
    sget-object v0, Lcom/samsung/musicplus/library/MusicFeatures;->sCscFeature:Lcom/sec/android/app/CscFeature;

    const-string v3, "CscFeature_Music_TranslateUnknownTitle"

    invoke-virtual {v0, v3}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_UNKNOWN_TRANS:Z

    .line 292
    sget-object v0, Lcom/samsung/musicplus/library/MusicFeatures;->sCscFeature:Lcom/sec/android/app/CscFeature;

    const-string v3, "CscFeature_Music_SupportPinyinSort"

    invoke-virtual {v0, v3}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_PINYIN:Z

    .line 300
    const-string v0, "China"

    invoke-static {}, Lcom/samsung/musicplus/library/MusicFeatures;->getCountryCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_DATA_CHECK_POPUP:Z

    .line 308
    const-string v0, "JP"

    invoke-static {}, Lcom/samsung/musicplus/library/MusicFeatures;->getCountryCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_JPN_LYRIC:Z

    .line 315
    const-string v0, "JP"

    invoke-static {}, Lcom/samsung/musicplus/library/MusicFeatures;->getCountryCode()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    sget-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->T_BASE_MODEL:Z

    if-eqz v0, :cond_d

    move v0, v2

    :goto_3
    sput-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_K2HD:Z

    .line 331
    sget-object v0, Lcom/samsung/musicplus/library/MusicFeatures;->sFloatingFeature:Lcom/samsung/android/feature/FloatingFeature;

    const-string v3, "SEC_FLOATING_FEATURE_MUSICPLAYER_SUPPORT_FIND_TAG"

    invoke-virtual {v0, v3}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_e

    sget-object v0, Lcom/samsung/musicplus/library/MusicFeatures;->sCscFeature:Lcom/sec/android/app/CscFeature;

    const-string v3, "CscFeature_Music_DisableFindTag"

    invoke-virtual {v0, v3}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_e

    move v0, v2

    :goto_4
    sput-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_ENABLE_FIND_TAG:Z

    .line 338
    sget-object v0, Lcom/samsung/musicplus/library/MusicFeatures;->sFloatingFeature:Lcom/samsung/android/feature/FloatingFeature;

    const-string v3, "SEC_FLOATING_FEATURE_MUSICPLAYER_SUPPORT_METAEDIT"

    invoke-virtual {v0, v3}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_ENABLE_EDIT_META:Z

    .line 346
    sget-object v0, Lcom/samsung/musicplus/library/MusicFeatures;->sCscFeature:Lcom/sec/android/app/CscFeature;

    const-string v3, "CscFeature_Music_EnableBigPondTop10Feeds"

    invoke-virtual {v0, v3}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_BIGPOND:Z

    .line 355
    sget-object v0, Lcom/samsung/musicplus/library/MusicFeatures;->sCscFeature:Lcom/sec/android/app/CscFeature;

    const-string v3, "CscFeature_Music_RingtoneSizeLimit"

    invoke-virtual {v0, v3}, Lcom/sec/android/app/CscFeature;->getInteger(Ljava/lang/String;)I

    move-result v0

    sput v0, Lcom/samsung/musicplus/library/MusicFeatures;->MUSIC_RINGTONE_SIZE_LIMIT:I

    .line 364
    sget-object v0, Lcom/samsung/musicplus/library/MusicFeatures;->sFloatingFeature:Lcom/samsung/android/feature/FloatingFeature;

    const-string v3, "SEC_FLOATING_FEATURE_MUSICPLAYER_SUPPORT_SPLIT_VIEW"

    invoke-virtual {v0, v3}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_SPLIT_LIST_VIEW:Z

    .line 372
    sget-object v0, Lcom/samsung/musicplus/library/MusicFeatures;->sFloatingFeature:Lcom/samsung/android/feature/FloatingFeature;

    const-string v3, "SEC_FLOATING_FEATURE_SETTINGS_SUPPORT_MOTION"

    invoke-virtual {v0, v3}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lcom/samsung/musicplus/library/MusicFeatures;->sFloatingFeature:Lcom/samsung/android/feature/FloatingFeature;

    const-string v3, "SEC_FLOATING_FEATURE_SETTINGS_SUPPORT_MOTION_TRUN_OVER"

    invoke-virtual {v0, v3}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    :cond_3
    sget-object v0, Lcom/samsung/musicplus/library/MusicFeatures;->sFloatingFeature:Lcom/samsung/android/feature/FloatingFeature;

    const-string v3, "SEC_FLOATING_FEATURE_SETTINGS_MOTION_CONCEPT_2014"

    invoke-virtual {v0, v3}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_f

    :cond_4
    move v0, v2

    :goto_5
    sput-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_MOTION_TURN_OVER:Z

    .line 431
    sget-object v0, Lcom/samsung/musicplus/library/MusicFeatures;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "tr3calte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_10

    move v0, v2

    :goto_6
    sput-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SMART_VOLUME:Z

    .line 439
    sget-object v0, Lcom/samsung/musicplus/library/MusicFeatures;->sFloatingFeature:Lcom/samsung/android/feature/FloatingFeature;

    const-string v3, "SEC_FLOATING_FEATURE_MUSICPLAYER_SUPPORT_MUSIC_WAVE_VIEW"

    invoke-virtual {v0, v3}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_MUSIC_VIEW:Z

    .line 447
    sget-object v0, Lcom/samsung/musicplus/library/MusicFeatures;->sFloatingFeature:Lcom/samsung/android/feature/FloatingFeature;

    const-string v3, "SEC_FLOATING_FEATURE_MUSICPLAYER_ENABLE_REPEAT_AB"

    invoke-virtual {v0, v3}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_MUSIC_REPEAT_AB:Z

    .line 455
    sget-object v0, Lcom/samsung/musicplus/library/MusicFeatures;->sFloatingFeature:Lcom/samsung/android/feature/FloatingFeature;

    const-string v3, "SEC_FLOATING_FEATURE_MUSICPLAYER_ENABLE_LDB_LYLIC"

    invoke-virtual {v0, v3}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_MUSIC_LDB:Z

    .line 524
    sget-object v0, Lcom/samsung/musicplus/library/MusicFeatures;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "klte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    sget-object v0, Lcom/samsung/musicplus/library/MusicFeatures;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "k3g"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    sget-object v0, Lcom/samsung/musicplus/library/MusicFeatures;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "SC-04F"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    sget-object v0, Lcom/samsung/musicplus/library/MusicFeatures;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "SC-02G"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    sget-object v0, Lcom/samsung/musicplus/library/MusicFeatures;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "SCL23"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    invoke-static {}, Lcom/samsung/musicplus/library/MusicFeatures;->isHProject()Z

    move-result v0

    if-nez v0, :cond_5

    sget-object v0, Lcom/samsung/musicplus/library/MusicFeatures;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "ja3g"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    sget-object v0, Lcom/samsung/musicplus/library/MusicFeatures;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "jflte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    sget-object v0, Lcom/samsung/musicplus/library/MusicFeatures;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "ks01lte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    sget-object v0, Lcom/samsung/musicplus/library/MusicFeatures;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "SC-04F"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_5

    sget-object v0, Lcom/samsung/musicplus/library/MusicFeatures;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "SCL23"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_11

    :cond_5
    move v0, v2

    :goto_7
    sput-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_GESTURE_AIR_MOTION:Z

    .line 534
    sget-object v0, Lcom/samsung/musicplus/library/MusicFeatures;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "klte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    sget-object v0, Lcom/samsung/musicplus/library/MusicFeatures;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "k3g"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    sget-object v0, Lcom/samsung/musicplus/library/MusicFeatures;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "SC-04F"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    sget-object v0, Lcom/samsung/musicplus/library/MusicFeatures;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "SC-02G"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    sget-object v0, Lcom/samsung/musicplus/library/MusicFeatures;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "SCL23"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    invoke-static {}, Lcom/samsung/musicplus/library/MusicFeatures;->isHProject()Z

    move-result v0

    if-nez v0, :cond_6

    sget-object v0, Lcom/samsung/musicplus/library/MusicFeatures;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "ja3g"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    sget-object v0, Lcom/samsung/musicplus/library/MusicFeatures;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "jflte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    sget-object v0, Lcom/samsung/musicplus/library/MusicFeatures;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "ks01lte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    sget-object v0, Lcom/samsung/musicplus/library/MusicFeatures;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "SC-04F"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    sget-object v0, Lcom/samsung/musicplus/library/MusicFeatures;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "SCL23"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_12

    :cond_6
    move v0, v2

    :goto_8
    sput-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_FINGER_AIR_VIEW:Z

    .line 546
    sget-object v0, Lcom/samsung/musicplus/library/MusicFeatures;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "trelte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_13

    sget-object v0, Lcom/samsung/musicplus/library/MusicFeatures;->PRODUCT_NAME:Ljava/lang/String;

    const-string v3, "tr3calte"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_13

    move v0, v2

    :goto_9
    sput-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_MOTION_PALM_PAUSE:Z

    .line 605
    sget-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_CHECK_KOR_LGT:Z

    if-nez v0, :cond_14

    move v0, v2

    :goto_a
    sput-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->MUSIC_FEATURE_ENABLE_DRM_RINGTONE_CHECK:Z

    .line 649
    invoke-static {}, Lcom/samsung/musicplus/library/MusicFeatures;->isHProject()Z

    move-result v0

    sput-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_SMART_CLIP_META:Z

    .line 666
    invoke-static {}, Lcom/samsung/musicplus/library/MusicFeatures;->isFProject()Z

    move-result v0

    sput-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_MOTION_BOUNCE:Z

    .line 673
    invoke-static {}, Lcom/samsung/musicplus/library/MusicFeatures;->isHProject()Z

    move-result v0

    if-nez v0, :cond_7

    invoke-static {}, Lcom/samsung/musicplus/library/MusicFeatures;->isFProject()Z

    move-result v0

    if-eqz v0, :cond_15

    :cond_7
    move v0, v2

    :goto_b
    sput-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_SVIEW_COVER_V2:Z

    .line 680
    sget-object v0, Lcom/samsung/musicplus/library/MusicFeatures;->sFloatingFeature:Lcom/samsung/android/feature/FloatingFeature;

    const-string v3, "SEC_FLOATING_FEATURE_MUSICPLAYER_SUPPORT_BARGEIN"

    invoke-virtual {v0, v3}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_VOICE_COMMAND_CONTROL:Z

    .line 689
    invoke-static {}, Lcom/samsung/musicplus/library/MusicFeatures;->isDefaultLandModel()Z

    move-result v0

    sput-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->IS_DEFAULT_LAND_MODEL:Z

    .line 741
    invoke-static {}, Lcom/samsung/musicplus/library/MusicFeatures;->isHProject()Z

    move-result v0

    sput-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_MOVE_TO_KNOX:Z

    .line 748
    invoke-static {}, Lcom/samsung/musicplus/library/MusicFeatures;->getFlagSupportUHQA()Z

    move-result v0

    sput-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_UHQA:Z

    .line 758
    invoke-static {}, Lcom/samsung/musicplus/library/MusicFeatures;->getNumberOfSpeaker()I

    move-result v0

    sput v0, Lcom/samsung/musicplus/library/MusicFeatures;->NUMBER_OF_SPEAKER:I

    .line 772
    sget-object v0, Lcom/samsung/musicplus/library/MusicFeatures;->sFloatingFeature:Lcom/samsung/android/feature/FloatingFeature;

    const-string v3, "SEC_FLOATING_FEATURE_SVOICE_CONFIG_FOR_GEAR"

    invoke-virtual {v0, v3}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    sget-object v0, Lcom/samsung/musicplus/library/MusicFeatures;->sFloatingFeature:Lcom/samsung/android/feature/FloatingFeature;

    const-string v3, "SEC_FLOATING_FEATURE_COMMON_SUPPORT_SVOICE"

    invoke-virtual {v0, v3}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_9

    :cond_8
    move v1, v2

    :cond_9
    sput-boolean v1, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_SVOICE_FOR_GEAR:Z

    .line 780
    invoke-static {}, Lcom/samsung/musicplus/library/MusicFeatures;->isEnableSeekDelayedIn()Z

    move-result v0

    sput-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_ENABLE_SEEK_DELAYED_IN:Z

    .line 805
    sget-object v0, Lcom/samsung/musicplus/library/MusicFeatures;->sFloatingFeature:Lcom/samsung/android/feature/FloatingFeature;

    const-string v1, "SEC_FLOATING_FEATURE_MUSICPLAYER_SUPPORT_SWEEP_ON_TABS"

    invoke-virtual {v0, v1}, Lcom/samsung/android/feature/FloatingFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v0

    sput-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_SWEEP_ON_TABS:Z

    return-void

    :cond_a
    move v0, v1

    .line 184
    goto/16 :goto_0

    :cond_b
    move v0, v1

    .line 249
    goto/16 :goto_1

    :cond_c
    move v0, v1

    .line 259
    goto/16 :goto_2

    :cond_d
    move v0, v1

    .line 315
    goto/16 :goto_3

    :cond_e
    move v0, v1

    .line 331
    goto/16 :goto_4

    :cond_f
    move v0, v1

    .line 372
    goto/16 :goto_5

    :cond_10
    move v0, v1

    .line 431
    goto/16 :goto_6

    :cond_11
    move v0, v1

    .line 524
    goto/16 :goto_7

    :cond_12
    move v0, v1

    .line 534
    goto/16 :goto_8

    :cond_13
    move v0, v1

    .line 546
    goto/16 :goto_9

    :cond_14
    move v0, v1

    .line 605
    goto/16 :goto_a

    :cond_15
    move v0, v1

    .line 673
    goto :goto_b
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static getCountryCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 135
    const-string v0, "ro.csc.country_code"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getFlagSupportUHQA()Z
    .locals 2

    .prologue
    .line 750
    const-string v0, "1"

    const-string v1, "persist.audio.uhqa"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private static getNumberOfSpeaker()I
    .locals 2

    .prologue
    .line 760
    const-string v0, "1"

    const-string v1, "persist.audio.stereoapeaker"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 761
    const/4 v0, 0x2

    .line 763
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static final getOSDeviceName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 158
    const-string v0, "model.device.name"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static final getOSver()Ljava/lang/String;
    .locals 1

    .prologue
    .line 173
    const-string v0, "ro.build.version.release"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getSalesCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 143
    const-string v0, "ro.csc.sales_code"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static isDefaultLandModel()Z
    .locals 2

    .prologue
    .line 704
    sget-object v0, Lcom/samsung/musicplus/library/MusicFeatures;->PRODUCT_NAME:Ljava/lang/String;

    const-string v1, "vienna"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/samsung/musicplus/library/MusicFeatures;->PRODUCT_NAME:Ljava/lang/String;

    const-string v1, "v1a"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/samsung/musicplus/library/MusicFeatures;->PRODUCT_NAME:Ljava/lang/String;

    const-string v1, "v2"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/samsung/musicplus/library/MusicFeatures;->PRODUCT_NAME:Ljava/lang/String;

    const-string v1, "lt03"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/samsung/musicplus/library/MusicFeatures;->PRODUCT_NAME:Ljava/lang/String;

    const-string v1, "picasso"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/samsung/musicplus/library/MusicFeatures;->PRODUCT_NAME:Ljava/lang/String;

    const-string v1, "matisse"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/samsung/musicplus/library/MusicFeatures;->PRODUCT_NAME:Ljava/lang/String;

    const-string v1, "chagall"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/samsung/musicplus/library/MusicFeatures;->PRODUCT_NAME:Ljava/lang/String;

    const-string v1, "mondrian"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isEnableSeekDelayedIn()Z
    .locals 2

    .prologue
    .line 782
    const-string v0, "1"

    const-string v1, "persist.audio.mpseek"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static isFProject()Z
    .locals 2

    .prologue
    .line 664
    sget-object v0, Lcom/samsung/musicplus/library/MusicFeatures;->PRODUCT_NAME:Ljava/lang/String;

    const-string v1, "flte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static final isGateEnable()Z
    .locals 1

    .prologue
    .line 88
    invoke-static {}, Landroid/util/GateConfig;->isGateEnabled()Z

    move-result v0

    return v0
.end method

.method public static isHProject()Z
    .locals 2

    .prologue
    .line 638
    sget-object v0, Lcom/samsung/musicplus/library/MusicFeatures;->PRODUCT_NAME:Ljava/lang/String;

    const-string v1, "h3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/samsung/musicplus/library/MusicFeatures;->PRODUCT_NAME:Ljava/lang/String;

    const-string v1, "ha3g"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/samsung/musicplus/library/MusicFeatures;->PRODUCT_NAME:Ljava/lang/String;

    const-string v1, "hlte"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/samsung/musicplus/library/MusicFeatures;->PRODUCT_NAME:Ljava/lang/String;

    const-string v1, "Madrid"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/samsung/musicplus/library/MusicFeatures;->PRODUCT_NAME:Ljava/lang/String;

    const-string v1, "ASH"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/samsung/musicplus/library/MusicFeatures;->PRODUCT_NAME:Ljava/lang/String;

    const-string v1, "SC-01F"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/samsung/musicplus/library/MusicFeatures;->PRODUCT_NAME:Ljava/lang/String;

    const-string v1, "SCL22"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isSupportMultiSim(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 514
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 515
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "com.sec.feature.multisim"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    .line 517
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
