.class public Lcom/samsung/musicplus/library/audio/MoodParser;
.super Ljava/lang/Object;
.source "MoodParser.java"


# static fields
.field public static final MIME_TYPE_MUSICSQUARE_LIBS_SUPPORT:Ljava/lang/String; = "\'audio/mpeg\',\'audio/aac\',\'audio/aac-adts\',\'audio/flac\'"


# instance fields
.field private final mLib:Lcom/samsung/musicsquare/MusicSquareLib;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    new-instance v0, Lcom/samsung/musicsquare/MusicSquareLib;

    invoke-direct {v0}, Lcom/samsung/musicsquare/MusicSquareLib;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/library/audio/MoodParser;->mLib:Lcom/samsung/musicsquare/MusicSquareLib;

    .line 55
    iget-object v0, p0, Lcom/samsung/musicplus/library/audio/MoodParser;->mLib:Lcom/samsung/musicsquare/MusicSquareLib;

    invoke-virtual {v0}, Lcom/samsung/musicsquare/MusicSquareLib;->WitchMoodEngineInit()I

    .line 56
    return-void
.end method


# virtual methods
.method public getMood(Ljava/lang/String;)[D
    .locals 1
    .param p1, "filePath"    # Ljava/lang/String;

    .prologue
    .line 89
    iget-object v0, p0, Lcom/samsung/musicplus/library/audio/MoodParser;->mLib:Lcom/samsung/musicsquare/MusicSquareLib;

    invoke-virtual {v0, p1}, Lcom/samsung/musicsquare/MusicSquareLib;->WitchMoodEngineEXE(Ljava/lang/String;)[D

    move-result-object v0

    return-object v0
.end method

.method public release()I
    .locals 1

    .prologue
    .line 102
    iget-object v0, p0, Lcom/samsung/musicplus/library/audio/MoodParser;->mLib:Lcom/samsung/musicsquare/MusicSquareLib;

    invoke-virtual {v0}, Lcom/samsung/musicsquare/MusicSquareLib;->WitchMoodEngineRelease()I

    move-result v0

    return v0
.end method
