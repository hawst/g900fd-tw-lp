.class public Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager;
.super Ljava/lang/Object;
.source "SimpleBounceMotionManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager$OnMusicBounceSettingChangeListener;,
        Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager$OnBounceListener;
    }
.end annotation


# static fields
.field public static final ACTION_ARC_MOTION_SETTINGS_CHANGED:Ljava/lang/String; = "com.sec.motions.ARC_MOTION_SETTINGS_CHANGED"

.field private static final ARC_MOTION_MUSIC_PLAYBACK:Ljava/lang/String; = "arc_motion_music_playback"

.field private static final ARC_MOTION_SETTINGS:Ljava/lang/String; = "master_arc_motion"

.field private static final ARC_MOTION_SETTINGS_URI:Landroid/net/Uri;

.field private static final CLASSNAME:Ljava/lang/String;

.field public static final EXTRA_ENABLED:Ljava/lang/String; = "isEnable"

.field public static final NEXT_MUSIC:I = 0x1

.field public static final PREV_MUSIC:I = 0x2


# instance fields
.field private mContext:Landroid/content/Context;

.field private mIsRegisteredBounceListener:Z

.field private mScontextListener:Landroid/hardware/scontext/SContextListener;

.field private mScontextManager:Landroid/hardware/scontext/SContextManager;

.field private final mSettingContentObserver:Landroid/database/ContentObserver;

.field private mSettingListener:Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager$OnMusicBounceSettingChangeListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const-class v0, Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager;->CLASSNAME:Ljava/lang/String;

    .line 88
    const-string v0, "arc_motion_music_playback"

    invoke-static {v0}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager;->ARC_MOTION_SETTINGS_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager$OnBounceListener;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager$OnBounceListener;

    .prologue
    .line 139
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 93
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager;->mIsRegisteredBounceListener:Z

    .line 180
    new-instance v0, Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager$2;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager$2;-><init>(Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager;->mSettingContentObserver:Landroid/database/ContentObserver;

    .line 140
    const-string v0, "scontext"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/scontext/SContextManager;

    iput-object v0, p0, Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager;->mScontextManager:Landroid/hardware/scontext/SContextManager;

    .line 141
    iput-object p1, p0, Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager;->mContext:Landroid/content/Context;

    .line 142
    iget-object v0, p0, Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager;->mScontextManager:Landroid/hardware/scontext/SContextManager;

    if-eqz v0, :cond_0

    .line 143
    new-instance v0, Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager$1;

    invoke-direct {v0, p0, p2}, Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager$1;-><init>(Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager;Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager$OnBounceListener;)V

    iput-object v0, p0, Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager;->mScontextListener:Landroid/hardware/scontext/SContextListener;

    .line 155
    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager;)Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager$OnMusicBounceSettingChangeListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager;->mSettingListener:Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager$OnMusicBounceSettingChangeListener;

    return-object v0
.end method

.method public static isEnableMusicBounceSetting(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 245
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 246
    .local v0, "cr":Landroid/content/ContentResolver;
    const-string v3, "arc_motion_music_playback"

    invoke-static {v0, v3, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-ne v3, v1, :cond_0

    .line 249
    :goto_0
    return v1

    :cond_0
    move v1, v2

    goto :goto_0
.end method

.method public static isEnableSystemAndMusicBounceSetting(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 228
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 229
    .local v0, "cr":Landroid/content/ContentResolver;
    const-string v3, "master_arc_motion"

    invoke-static {v0, v3, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-ne v3, v1, :cond_0

    .line 230
    const-string v3, "arc_motion_music_playback"

    invoke-static {v0, v3, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-ne v3, v1, :cond_0

    .line 234
    :goto_0
    return v1

    :cond_0
    move v1, v2

    goto :goto_0
.end method

.method public static isEnableSystemBounceSetting(Landroid/content/Context;)Z
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 260
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 261
    .local v0, "cr":Landroid/content/ContentResolver;
    const-string v3, "master_arc_motion"

    invoke-static {v0, v3, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-ne v3, v1, :cond_0

    .line 264
    :goto_0
    return v1

    :cond_0
    move v1, v2

    goto :goto_0
.end method


# virtual methods
.method public registerListener()V
    .locals 3

    .prologue
    .line 196
    iget-object v0, p0, Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager;->mScontextManager:Landroid/hardware/scontext/SContextManager;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager;->mIsRegisteredBounceListener:Z

    if-nez v0, :cond_0

    .line 197
    sget-object v0, Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager;->CLASSNAME:Ljava/lang/String;

    const-string v1, "registerSContextListener"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    iget-object v0, p0, Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager;->mScontextManager:Landroid/hardware/scontext/SContextManager;

    iget-object v1, p0, Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager;->mScontextListener:Landroid/hardware/scontext/SContextListener;

    const/16 v2, 0x11

    invoke-virtual {v0, v1, v2}, Landroid/hardware/scontext/SContextManager;->registerListener(Landroid/hardware/scontext/SContextListener;I)Z

    .line 199
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager;->mIsRegisteredBounceListener:Z

    .line 201
    :cond_0
    return-void
.end method

.method public registerSettingChangedListener(Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager$OnMusicBounceSettingChangeListener;)V
    .locals 4
    .param p1, "listener"    # Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager$OnMusicBounceSettingChangeListener;

    .prologue
    .line 165
    iput-object p1, p0, Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager;->mSettingListener:Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager$OnMusicBounceSettingChangeListener;

    .line 166
    iget-object v0, p0, Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager;->ARC_MOTION_SETTINGS_URI:Landroid/net/Uri;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager;->mSettingContentObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 168
    return-void
.end method

.method public unregisterListener()V
    .locals 3

    .prologue
    .line 210
    iget-object v0, p0, Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager;->mScontextManager:Landroid/hardware/scontext/SContextManager;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager;->mIsRegisteredBounceListener:Z

    if-eqz v0, :cond_0

    .line 211
    sget-object v0, Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager;->CLASSNAME:Ljava/lang/String;

    const-string v1, "unregisterSContextListener"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    iget-object v0, p0, Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager;->mScontextManager:Landroid/hardware/scontext/SContextManager;

    iget-object v1, p0, Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager;->mScontextListener:Landroid/hardware/scontext/SContextListener;

    const/16 v2, 0x11

    invoke-virtual {v0, v1, v2}, Landroid/hardware/scontext/SContextManager;->unregisterListener(Landroid/hardware/scontext/SContextListener;I)V

    .line 215
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager;->mIsRegisteredBounceListener:Z

    .line 217
    :cond_0
    return-void
.end method

.method public unregisterSettingChangedListener()V
    .locals 2

    .prologue
    .line 177
    iget-object v0, p0, Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager;->mSettingContentObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 178
    return-void
.end method
