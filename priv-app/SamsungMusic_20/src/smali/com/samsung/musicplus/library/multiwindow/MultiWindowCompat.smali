.class public Lcom/samsung/musicplus/library/multiwindow/MultiWindowCompat;
.super Ljava/lang/Object;
.source "MultiWindowCompat.java"


# static fields
.field public static FEATURE_MULTIWINDOW:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-string v0, "com.sec.feature.multiwindow"

    sput-object v0, Lcom/samsung/musicplus/library/multiwindow/MultiWindowCompat;->FEATURE_MULTIWINDOW:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static isActivated(Landroid/app/Activity;)Z
    .locals 2
    .param p0, "a"    # Landroid/app/Activity;

    .prologue
    .line 60
    new-instance v0, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;-><init>(Landroid/app/Activity;)V

    .line 61
    .local v0, "mwActivity":Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;
    invoke-virtual {v0}, Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity;->isMultiWindow()Z

    move-result v1

    return v1
.end method

.method public static isEnableMultiWindow(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 30
    if-eqz p0, :cond_0

    .line 31
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 32
    .local v0, "pm":Landroid/content/pm/PackageManager;
    if-eqz v0, :cond_0

    .line 33
    sget-object v1, Lcom/samsung/musicplus/library/multiwindow/MultiWindowCompat;->FEATURE_MULTIWINDOW:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v1

    .line 36
    .end local v0    # "pm":Landroid/content/pm/PackageManager;
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public isActivatedMultiWindow(Landroid/app/Activity;)Z
    .locals 1
    .param p1, "a"    # Landroid/app/Activity;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 50
    const/4 v0, 0x0

    return v0
.end method
