.class public Lcom/samsung/musicplus/library/dlna/DlnaOpenIntentManager;
.super Ljava/lang/Object;
.source "DlnaOpenIntentManager.java"


# static fields
.field public static final ACTION_DLNA_DIRECT_DMC:Ljava/lang/String; = "com.android.music.DIRECT_DMC"

.field public static final ACTION_DLNA_OPEN_INTENT:Ljava/lang/String; = "com.samsung.android.allshare.intent.action.AUDIOPLAYER"

.field private static final CLASSNAME:Ljava/lang/String;

.field public static final EXTRA_DLNA_OPEN_INTENT_DMC_FUNCTION_ENABLED:Ljava/lang/String; = "com.samsung.android.allshare.intent.extra.ALLSHARE_ENABLED"

.field public static final EXTRA_DLNA_OPEN_INTENT_ITEM:Ljava/lang/String; = "com.samsung.android.allshare.intent.extra.ITEM"

.field public static final EXTRA_DMR_ID:Ljava/lang/String; = "DMRUDN"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lcom/samsung/musicplus/library/dlna/DlnaOpenIntentManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/library/dlna/DlnaOpenIntentManager;->CLASSNAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static extractSeed(Lcom/samsung/android/allshare/Item;)Ljava/lang/String;
    .locals 6
    .param p0, "item"    # Lcom/samsung/android/allshare/Item;

    .prologue
    .line 184
    const/4 v2, 0x0

    .line 186
    .local v2, "seedStr":Ljava/lang/String;
    :try_start_0
    invoke-static {p0}, Lcom/samsung/android/allshare/extension/ItemExtractor;->extract(Lcom/samsung/android/allshare/Item;)Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;

    move-result-object v1

    .line 187
    .local v1, "seed":Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;
    if-eqz v1, :cond_0

    .line 188
    invoke-virtual {v1}, Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;->getSeedString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 195
    .end local v1    # "seed":Lcom/samsung/android/allshare/extension/ItemExtractor$Seed;
    :cond_0
    :goto_0
    return-object v2

    .line 190
    :catch_0
    move-exception v0

    .line 193
    .local v0, "e":Ljava/lang/RuntimeException;
    sget-object v3, Lcom/samsung/musicplus/library/dlna/DlnaOpenIntentManager;->CLASSNAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "RuntimeException "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v0}, Ljava/lang/RuntimeException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static getContentUri(Landroid/content/Intent;)Landroid/net/Uri;
    .locals 2
    .param p0, "i"    # Landroid/content/Intent;

    .prologue
    .line 93
    invoke-static {p0}, Lcom/samsung/musicplus/library/dlna/DlnaOpenIntentManager;->getItem(Landroid/content/Intent;)Lcom/samsung/android/allshare/Item;

    move-result-object v0

    .line 94
    .local v0, "item":Lcom/samsung/android/allshare/Item;
    if-eqz v0, :cond_0

    .line 95
    invoke-virtual {v0}, Lcom/samsung/android/allshare/Item;->getURI()Landroid/net/Uri;

    move-result-object v1

    .line 97
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static getDlnaOpenIntentItem(Lcom/samsung/android/allshare/Item;)Landroid/os/Bundle;
    .locals 10
    .param p0, "item"    # Lcom/samsung/android/allshare/Item;

    .prologue
    .line 134
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 135
    .local v0, "data":Landroid/os/Bundle;
    invoke-virtual {p0}, Lcom/samsung/android/allshare/Item;->getArtist()Ljava/lang/String;

    move-result-object v1

    .line 136
    .local v1, "itemInfo":Ljava/lang/String;
    if-eqz v1, :cond_0

    const-string v4, ""

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 137
    const-string v4, "artist"

    invoke-virtual {v0, v4, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/allshare/Item;->getAlbumTitle()Ljava/lang/String;

    move-result-object v1

    .line 140
    if-eqz v1, :cond_1

    const-string v4, ""

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 141
    const-string v4, "album"

    invoke-virtual {v0, v4, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/allshare/Item;->getTitle()Ljava/lang/String;

    move-result-object v1

    .line 144
    if-eqz v1, :cond_2

    const-string v4, ""

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 145
    const-string v4, "title"

    invoke-virtual {v0, v4, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/android/allshare/Item;->getMimetype()Ljava/lang/String;

    move-result-object v1

    .line 148
    if-eqz v1, :cond_3

    const-string v4, ""

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 149
    const-string v4, "mime_type"

    invoke-virtual {v0, v4, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/android/allshare/Item;->getExtension()Ljava/lang/String;

    move-result-object v1

    .line 152
    if-eqz v1, :cond_4

    const-string v4, ""

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 153
    const-string v4, "extension"

    invoke-virtual {v0, v4, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 155
    :cond_4
    invoke-static {p0}, Lcom/samsung/musicplus/library/dlna/DlnaOpenIntentManager;->extractSeed(Lcom/samsung/android/allshare/Item;)Ljava/lang/String;

    move-result-object v2

    .line 156
    .local v2, "seed":Ljava/lang/String;
    if-eqz v1, :cond_5

    .line 157
    const-string v4, "seed"

    invoke-virtual {v0, v4, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    :cond_5
    invoke-virtual {p0}, Lcom/samsung/android/allshare/Item;->getURI()Landroid/net/Uri;

    move-result-object v3

    .line 160
    .local v3, "uri":Landroid/net/Uri;
    if-eqz v3, :cond_6

    .line 161
    const-string v4, "_data"

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    :cond_6
    invoke-virtual {p0}, Lcom/samsung/android/allshare/Item;->getThumbnail()Landroid/net/Uri;

    move-result-object v3

    .line 164
    if-eqz v3, :cond_7

    .line 165
    const-string v4, "album_art"

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    :cond_7
    invoke-virtual {p0}, Lcom/samsung/android/allshare/Item;->getGenre()Ljava/lang/String;

    move-result-object v1

    .line 168
    if-eqz v1, :cond_8

    const-string v4, ""

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_8

    .line 169
    const-string v4, "genre_name"

    invoke-virtual {v0, v4, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 172
    :cond_8
    const-string v4, "duration"

    invoke-virtual {p0}, Lcom/samsung/android/allshare/Item;->getDuration()J

    move-result-wide v6

    const-wide/16 v8, 0x3e8

    mul-long/2addr v6, v8

    invoke-virtual {v0, v4, v6, v7}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 173
    const-string v4, "file_size"

    invoke-virtual {p0}, Lcom/samsung/android/allshare/Item;->getFileSize()J

    move-result-wide v6

    invoke-virtual {v0, v4, v6, v7}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 175
    sget-object v4, Lcom/samsung/musicplus/library/dlna/DlnaOpenIntentManager;->CLASSNAME:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "updateDlnaOpenIntentContentsDB - Artist: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/samsung/android/allshare/Item;->getArtist()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " Title: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/samsung/android/allshare/Item;->getTitle()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " Album: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/samsung/android/allshare/Item;->getAlbumTitle()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " MimeType: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/samsung/android/allshare/Item;->getMimetype()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " Extension: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/samsung/android/allshare/Item;->getExtension()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " seed: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " uri: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/samsung/android/allshare/Item;->getURI()Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " albumUri: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/samsung/android/allshare/Item;->getThumbnail()Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " duration: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/samsung/android/allshare/Item;->getDuration()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " file size: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {p0}, Lcom/samsung/android/allshare/Item;->getFileSize()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 180
    return-object v0
.end method

.method public static getDlnaOpenItemData(Landroid/content/Intent;)Landroid/os/Bundle;
    .locals 2
    .param p0, "i"    # Landroid/content/Intent;

    .prologue
    .line 112
    invoke-static {p0}, Lcom/samsung/musicplus/library/dlna/DlnaOpenIntentManager;->getItem(Landroid/content/Intent;)Lcom/samsung/android/allshare/Item;

    move-result-object v0

    .line 113
    .local v0, "item":Lcom/samsung/android/allshare/Item;
    if-eqz v0, :cond_0

    .line 114
    invoke-static {v0}, Lcom/samsung/musicplus/library/dlna/DlnaOpenIntentManager;->getDlnaOpenIntentItem(Lcom/samsung/android/allshare/Item;)Landroid/os/Bundle;

    move-result-object v1

    .line 116
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static getItem(Landroid/content/Intent;)Lcom/samsung/android/allshare/Item;
    .locals 5
    .param p0, "i"    # Landroid/content/Intent;

    .prologue
    const/4 v2, 0x0

    .line 120
    invoke-virtual {p0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 121
    .local v0, "extras":Landroid/os/Bundle;
    if-eqz v0, :cond_1

    .line 122
    const-string v3, "com.samsung.android.allshare.intent.extra.ITEM"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/allshare/Item;

    .line 123
    .local v1, "item":Lcom/samsung/android/allshare/Item;
    if-nez v1, :cond_0

    .line 124
    sget-object v3, Lcom/samsung/musicplus/library/dlna/DlnaOpenIntentManager;->CLASSNAME:Ljava/lang/String;

    const-string v4, "getList - item is null"

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v2

    .line 129
    .end local v1    # "item":Lcom/samsung/android/allshare/Item;
    :cond_0
    :goto_0
    return-object v1

    :cond_1
    move-object v1, v2

    goto :goto_0
.end method
