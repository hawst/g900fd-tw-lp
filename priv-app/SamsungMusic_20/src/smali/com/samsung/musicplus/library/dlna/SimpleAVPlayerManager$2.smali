.class Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$2;
.super Ljava/lang/Object;
.source "SimpleAVPlayerManager.java"

# interfaces
.implements Lcom/samsung/android/allshare/media/AVPlayer$IAVPlayerPlaybackResponseListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;)V
    .locals 0

    .prologue
    .line 735
    iput-object p1, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$2;->this$0:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGetMediaInfoResponseReceived(Lcom/samsung/android/allshare/media/MediaInfo;Lcom/samsung/android/allshare/ERROR;)V
    .locals 7
    .param p1, "mediaInfo"    # Lcom/samsung/android/allshare/media/MediaInfo;
    .param p2, "err"    # Lcom/samsung/android/allshare/ERROR;

    .prologue
    const/4 v6, 0x1

    .line 824
    # getter for: Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->access$100()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "PlaybackResponse - onGetMediaInfoResponseReceived: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    if-nez p1, :cond_1

    const-string v0, "null"

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 826
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/samsung/android/allshare/media/MediaInfo;->getDuration()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    .line 827
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$2;->this$0:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    # getter for: Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->access$700(Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 828
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$2;->this$0:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    # getter for: Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->access$700(Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/os/Handler;->removeMessages(I)V

    .line 830
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$2;->this$0:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    invoke-virtual {p1}, Lcom/samsung/android/allshare/media/MediaInfo;->getDuration()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    # setter for: Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mDuration:J
    invoke-static {v0, v2, v3}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->access$602(Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;J)J

    .line 834
    :goto_1
    return-void

    .line 824
    :cond_1
    invoke-virtual {p1}, Lcom/samsung/android/allshare/media/MediaInfo;->getDuration()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    .line 832
    :cond_2
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$2;->this$0:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    # getter for: Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->access$700(Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;)Landroid/os/Handler;

    move-result-object v0

    const-wide/16 v2, 0x12c

    invoke-virtual {v0, v6, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_1
.end method

.method public onGetPlayPositionResponseReceived(JLcom/samsung/android/allshare/ERROR;)V
    .locals 0
    .param p1, "position"    # J
    .param p3, "err"    # Lcom/samsung/android/allshare/ERROR;

    .prologue
    .line 773
    return-void
.end method

.method public onGetStateResponseReceived(Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;Lcom/samsung/android/allshare/ERROR;)V
    .locals 0
    .param p1, "state"    # Lcom/samsung/android/allshare/media/AVPlayer$AVPlayerState;
    .param p2, "err"    # Lcom/samsung/android/allshare/ERROR;

    .prologue
    .line 838
    return-void
.end method

.method public onPauseResponseReceived(Lcom/samsung/android/allshare/ERROR;)V
    .locals 3
    .param p1, "err"    # Lcom/samsung/android/allshare/ERROR;

    .prologue
    .line 761
    # getter for: Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->access$100()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "PlaybackResponse - onPauseResponseReceived - Error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/samsung/android/allshare/ERROR;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 763
    sget-object v0, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/ERROR;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 765
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$2;->this$0:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    iget-object v1, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$2;->this$0:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    # getter for: Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mAVPlayerState:I
    invoke-static {v1}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->access$300(Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;)I

    move-result v1

    # invokes: Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->onStateChanged(I)V
    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->access$400(Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;I)V

    .line 767
    :cond_0
    return-void
.end method

.method public onPlayResponseReceived(Lcom/samsung/android/allshare/Item;Lcom/samsung/android/allshare/media/ContentInfo;Lcom/samsung/android/allshare/ERROR;)V
    .locals 6
    .param p1, "ai"    # Lcom/samsung/android/allshare/Item;
    .param p2, "ci"    # Lcom/samsung/android/allshare/media/ContentInfo;
    .param p3, "err"    # Lcom/samsung/android/allshare/ERROR;

    .prologue
    .line 777
    # getter for: Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->access$100()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "PlaybackResponse - onPlayResponseReceived: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 778
    iget-object v2, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$2;->this$0:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    const/4 v3, 0x6

    # setter for: Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mAVPlayerState:I
    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->access$302(Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;I)I

    .line 779
    iget-object v2, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$2;->this$0:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    iget-object v3, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$2;->this$0:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    # getter for: Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mAVPlayerState:I
    invoke-static {v3}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->access$300(Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;)I

    move-result v3

    # invokes: Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->onStateChanged(I)V
    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->access$400(Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;I)V

    .line 781
    sget-object v2, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v2, p3}, Lcom/samsung/android/allshare/ERROR;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 782
    iget-object v2, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$2;->this$0:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    # getter for: Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mDuration:J
    invoke-static {v2}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->access$600(Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-gez v2, :cond_0

    .line 783
    iget-object v2, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$2;->this$0:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    # getter for: Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->access$700(Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 820
    :cond_0
    :goto_0
    return-void

    .line 787
    :cond_1
    iget-object v2, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$2;->this$0:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    # getter for: Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mItem:Lcom/samsung/android/allshare/Item;
    invoke-static {v2}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->access$800(Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;)Lcom/samsung/android/allshare/Item;

    move-result-object v2

    if-nez v2, :cond_2

    .line 788
    # getter for: Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->access$100()Ljava/lang/String;

    move-result-object v2

    const-string v3, "PlaybackResponse - onPlayResponseReceived - mItem is null"

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 789
    iget-object v2, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$2;->this$0:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    # invokes: Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->onPlayResponseReceivedError(Lcom/samsung/android/allshare/ERROR;)V
    invoke-static {v2, p3}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->access$500(Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;Lcom/samsung/android/allshare/ERROR;)V

    goto :goto_0

    .line 792
    :cond_2
    iget-object v2, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$2;->this$0:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    # getter for: Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mItem:Lcom/samsung/android/allshare/Item;
    invoke-static {v2}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->access$800(Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;)Lcom/samsung/android/allshare/Item;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/android/allshare/Item;->getURI()Landroid/net/Uri;

    move-result-object v0

    .line 793
    .local v0, "currentUri":Landroid/net/Uri;
    if-nez v0, :cond_3

    .line 794
    # getter for: Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->access$100()Ljava/lang/String;

    move-result-object v2

    const-string v3, "PlaybackResponse - onPlayResponseReceived - currentUri is null"

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 796
    iget-object v2, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$2;->this$0:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    # invokes: Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->onPlayResponseReceivedError(Lcom/samsung/android/allshare/ERROR;)V
    invoke-static {v2, p3}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->access$500(Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;Lcom/samsung/android/allshare/ERROR;)V

    goto :goto_0

    .line 799
    :cond_3
    if-nez p1, :cond_4

    .line 800
    # getter for: Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->access$100()Ljava/lang/String;

    move-result-object v2

    const-string v3, "PlaybackResponse - onPlayResponseReceived - ai is null"

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 801
    iget-object v2, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$2;->this$0:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    # invokes: Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->onPlayResponseReceivedError(Lcom/samsung/android/allshare/ERROR;)V
    invoke-static {v2, p3}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->access$500(Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;Lcom/samsung/android/allshare/ERROR;)V

    goto :goto_0

    .line 804
    :cond_4
    invoke-virtual {p1}, Lcom/samsung/android/allshare/Item;->getURI()Landroid/net/Uri;

    move-result-object v1

    .line 805
    .local v1, "uri":Landroid/net/Uri;
    if-nez v1, :cond_5

    .line 806
    # getter for: Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->access$100()Ljava/lang/String;

    move-result-object v2

    const-string v3, "PlaybackResponse - onPlayResponseReceived - uri is null"

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 807
    iget-object v2, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$2;->this$0:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    # invokes: Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->onPlayResponseReceivedError(Lcom/samsung/android/allshare/ERROR;)V
    invoke-static {v2, p3}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->access$500(Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;Lcom/samsung/android/allshare/ERROR;)V

    goto :goto_0

    .line 810
    :cond_5
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 812
    # getter for: Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->access$100()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "PlaybackResponse - onPlayResponseReceived - uri is not macthed - uri.getPath: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 817
    :cond_6
    iget-object v2, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$2;->this$0:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    # invokes: Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->onPlayResponseReceivedError(Lcom/samsung/android/allshare/ERROR;)V
    invoke-static {v2, p3}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->access$500(Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;Lcom/samsung/android/allshare/ERROR;)V

    goto/16 :goto_0
.end method

.method public onResumeResponseReceived(Lcom/samsung/android/allshare/ERROR;)V
    .locals 3
    .param p1, "err"    # Lcom/samsung/android/allshare/ERROR;

    .prologue
    .line 750
    # getter for: Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->access$100()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "PlaybackResponse - onResumeResponseReceived - Error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/samsung/android/allshare/ERROR;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 752
    sget-object v0, Lcom/samsung/android/allshare/ERROR;->SUCCESS:Lcom/samsung/android/allshare/ERROR;

    invoke-virtual {v0, p1}, Lcom/samsung/android/allshare/ERROR;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 753
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$2;->this$0:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    iget-object v1, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$2;->this$0:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    # getter for: Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mAVPlayerState:I
    invoke-static {v1}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->access$300(Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;)I

    move-result v1

    # invokes: Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->onStateChanged(I)V
    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->access$400(Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;I)V

    .line 755
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$2;->this$0:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    # invokes: Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->onPlayResponseReceivedError(Lcom/samsung/android/allshare/ERROR;)V
    invoke-static {v0, p1}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->access$500(Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;Lcom/samsung/android/allshare/ERROR;)V

    .line 757
    :cond_0
    return-void
.end method

.method public onSeekResponseReceived(JLcom/samsung/android/allshare/ERROR;)V
    .locals 3
    .param p1, "position"    # J
    .param p3, "err"    # Lcom/samsung/android/allshare/ERROR;

    .prologue
    .line 743
    # getter for: Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->access$100()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "PlaybackResponse - onSeekResponseReceived - seekTo: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p3}, Lcom/samsung/android/allshare/ERROR;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 745
    iget-object v0, p0, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$2;->this$0:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->mDoNotUpdateProgress:Z
    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->access$202(Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;Z)Z

    .line 746
    return-void
.end method

.method public onStopResponseReceived(Lcom/samsung/android/allshare/ERROR;)V
    .locals 3
    .param p1, "err"    # Lcom/samsung/android/allshare/ERROR;

    .prologue
    .line 738
    # getter for: Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->access$100()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "PlaybackResponse -onStopResponseReceived - Error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/samsung/android/allshare/ERROR;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 739
    return-void
.end method
