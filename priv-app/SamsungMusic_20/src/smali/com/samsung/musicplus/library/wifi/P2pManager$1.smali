.class Lcom/samsung/musicplus/library/wifi/P2pManager$1;
.super Ljava/lang/Object;
.source "P2pManager.java"

# interfaces
.implements Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/library/wifi/P2pManager;->connect(Landroid/net/wifi/p2p/WifiP2pDevice;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/library/wifi/P2pManager;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/library/wifi/P2pManager;)V
    .locals 0

    .prologue
    .line 249
    iput-object p1, p0, Lcom/samsung/musicplus/library/wifi/P2pManager$1;->this$0:Lcom/samsung/musicplus/library/wifi/P2pManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(I)V
    .locals 3
    .param p1, "reason"    # I

    .prologue
    .line 257
    # getter for: Lcom/samsung/musicplus/library/wifi/P2pManager;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/library/wifi/P2pManager;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "autoConnecting. onFailure() : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 258
    return-void
.end method

.method public onSuccess()V
    .locals 2

    .prologue
    .line 252
    # getter for: Lcom/samsung/musicplus/library/wifi/P2pManager;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/library/wifi/P2pManager;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "autoConnecting. onSuccess()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 253
    return-void
.end method
