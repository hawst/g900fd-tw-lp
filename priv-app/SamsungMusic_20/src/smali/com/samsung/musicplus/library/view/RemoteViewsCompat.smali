.class public Lcom/samsung/musicplus/library/view/RemoteViewsCompat;
.super Ljava/lang/Object;
.source "RemoteViewsCompat.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static setLaunchPendingIntent(Landroid/widget/RemoteViews;ILandroid/app/PendingIntent;)V
    .locals 0
    .param p0, "views"    # Landroid/widget/RemoteViews;
    .param p1, "viewId"    # I
    .param p2, "i"    # Landroid/app/PendingIntent;

    .prologue
    .line 71
    invoke-virtual {p0, p1, p2}, Landroid/widget/RemoteViews;->setLaunchPendingIntent(ILandroid/app/PendingIntent;)V

    .line 72
    return-void
.end method

.method public static setOnLongClickPendingIntent(Landroid/widget/RemoteViews;Landroid/os/IBinder;ILandroid/app/PendingIntent;Landroid/app/PendingIntent;)V
    .locals 0
    .param p0, "views"    # Landroid/widget/RemoteViews;
    .param p1, "binder"    # Landroid/os/IBinder;
    .param p2, "viewId"    # I
    .param p3, "down"    # Landroid/app/PendingIntent;
    .param p4, "up"    # Landroid/app/PendingIntent;

    .prologue
    .line 56
    invoke-virtual {p0, p1, p2, p3, p4}, Landroid/widget/RemoteViews;->setOnLongClickPendingIntent(Landroid/os/IBinder;ILandroid/app/PendingIntent;Landroid/app/PendingIntent;)V

    .line 57
    return-void
.end method

.method public static setViewFingerHovered(Landroid/widget/RemoteViews;IZ)V
    .locals 0
    .param p0, "views"    # Landroid/widget/RemoteViews;
    .param p1, "viewId"    # I
    .param p2, "enabled"    # Z

    .prologue
    .line 38
    invoke-virtual {p0, p1, p2}, Landroid/widget/RemoteViews;->setViewFingerHovered(IZ)V

    .line 39
    return-void
.end method
