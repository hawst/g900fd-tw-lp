.class public Lcom/samsung/musicplus/library/audio/FindTag;
.super Ljava/lang/Object;
.source "FindTag.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/library/audio/FindTag$OnFindTagResultListener;
    }
.end annotation


# static fields
.field private static final ALL_RECORDED_BYTES_SIZE:I = 0x251c0

.field private static final BYTES_PER_SAMPLE:I = 0x2

.field private static final CLASSNAME:Ljava/lang/String;

.field private static final EXTRA_SECONDS:I = 0x2

.field private static final INTERVAL:I = 0x3e8

.field private static final IO_BUFFER_SIZE:I = 0x400

.field private static final MAKE_DEBUG_FILE:Z = false

.field private static final RECORDING_DURATION:J = 0x2710L

.field private static final RECORDING_START_POINT:I = 0x2710

.field private static final RECORD_TIME_OUT:I = 0x3a98

.field private static final SAMPLE_RATE:I = 0x1f40

.field private static final SA_GET_PUMP_DATA:I = 0x200

.field private static final SA_SET_PUMP:I = 0x100

.field private static sFindTag:Lcom/samsung/musicplus/library/audio/FindTag;


# instance fields
.field private mIsFinished:Z

.field private mPlayer:Landroid/media/MediaPlayer;

.field private mResultListener:Lcom/samsung/musicplus/library/audio/FindTag$OnFindTagResultListener;

.field private mSignature:[B

.field private mWorkingTime:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lcom/samsung/musicplus/library/audio/FindTag;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/library/audio/FindTag;->CLASSNAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/musicplus/library/audio/FindTag;->mWorkingTime:I

    .line 94
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/library/audio/FindTag;->mIsFinished:Z

    return-void
.end method

.method static synthetic access$002(Lcom/samsung/musicplus/library/audio/FindTag;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/library/audio/FindTag;
    .param p1, "x1"    # I

    .prologue
    .line 33
    iput p1, p0, Lcom/samsung/musicplus/library/audio/FindTag;->mWorkingTime:I

    return p1
.end method

.method static synthetic access$012(Lcom/samsung/musicplus/library/audio/FindTag;I)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/library/audio/FindTag;
    .param p1, "x1"    # I

    .prologue
    .line 33
    iget v0, p0, Lcom/samsung/musicplus/library/audio/FindTag;->mWorkingTime:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/samsung/musicplus/library/audio/FindTag;->mWorkingTime:I

    return v0
.end method

.method static synthetic access$100(Lcom/samsung/musicplus/library/audio/FindTag;)[B
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/library/audio/FindTag;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/samsung/musicplus/library/audio/FindTag;->mSignature:[B

    return-object v0
.end method

.method static synthetic access$102(Lcom/samsung/musicplus/library/audio/FindTag;[B)[B
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/library/audio/FindTag;
    .param p1, "x1"    # [B

    .prologue
    .line 33
    iput-object p1, p0, Lcom/samsung/musicplus/library/audio/FindTag;->mSignature:[B

    return-object p1
.end method

.method static synthetic access$200(Lcom/samsung/musicplus/library/audio/FindTag;)Lcom/samsung/musicplus/library/audio/FindTag$OnFindTagResultListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/library/audio/FindTag;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/samsung/musicplus/library/audio/FindTag;->mResultListener:Lcom/samsung/musicplus/library/audio/FindTag$OnFindTagResultListener;

    return-object v0
.end method

.method static synthetic access$202(Lcom/samsung/musicplus/library/audio/FindTag;Lcom/samsung/musicplus/library/audio/FindTag$OnFindTagResultListener;)Lcom/samsung/musicplus/library/audio/FindTag$OnFindTagResultListener;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/library/audio/FindTag;
    .param p1, "x1"    # Lcom/samsung/musicplus/library/audio/FindTag$OnFindTagResultListener;

    .prologue
    .line 33
    iput-object p1, p0, Lcom/samsung/musicplus/library/audio/FindTag;->mResultListener:Lcom/samsung/musicplus/library/audio/FindTag$OnFindTagResultListener;

    return-object p1
.end method

.method static synthetic access$300(Lcom/samsung/musicplus/library/audio/FindTag;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/library/audio/FindTag;

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/samsung/musicplus/library/audio/FindTag;->mIsFinished:Z

    return v0
.end method

.method static synthetic access$302(Lcom/samsung/musicplus/library/audio/FindTag;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/library/audio/FindTag;
    .param p1, "x1"    # Z

    .prologue
    .line 33
    iput-boolean p1, p0, Lcom/samsung/musicplus/library/audio/FindTag;->mIsFinished:Z

    return p1
.end method

.method static synthetic access$400(Lcom/samsung/musicplus/library/audio/FindTag;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/library/audio/FindTag;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/library/audio/FindTag;->startParsing(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$500(Lcom/samsung/musicplus/library/audio/FindTag;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/library/audio/FindTag;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/samsung/musicplus/library/audio/FindTag;->checkIsFinished()Z

    move-result v0

    return v0
.end method

.method static synthetic access$600(Lcom/samsung/musicplus/library/audio/FindTag;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/library/audio/FindTag;

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/samsung/musicplus/library/audio/FindTag;->stopMediaPlayer()V

    return-void
.end method

.method private checkIsFinished()Z
    .locals 18

    .prologue
    .line 179
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/musicplus/library/audio/FindTag;->mPlayer:Landroid/media/MediaPlayer;

    if-nez v12, :cond_0

    .line 180
    const/4 v12, 0x1

    .line 233
    :goto_0
    return v12

    .line 182
    :cond_0
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/samsung/musicplus/library/audio/FindTag;->mPlayer:Landroid/media/MediaPlayer;

    monitor-enter v13

    .line 183
    :try_start_0
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/musicplus/library/audio/FindTag;->mPlayer:Landroid/media/MediaPlayer;

    if-nez v12, :cond_1

    .line 184
    const/4 v12, 0x1

    monitor-exit v13

    goto :goto_0

    .line 234
    :catchall_0
    move-exception v12

    monitor-exit v13
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v12

    .line 186
    :cond_1
    :try_start_1
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/musicplus/library/audio/FindTag;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v12}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v3

    .line 187
    .local v3, "duration":I
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/musicplus/library/audio/FindTag;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v12}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v2

    .line 188
    .local v2, "current":I
    sget-object v12, Lcom/samsung/musicplus/library/audio/FindTag;->CLASSNAME:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, " checkIsFinished current : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, " duration : "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v12, v14}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 189
    move-object/from16 v0, p0

    iget v12, v0, Lcom/samsung/musicplus/library/audio/FindTag;->mWorkingTime:I

    const/16 v14, 0x3a98

    if-gt v12, v14, :cond_2

    int-to-long v14, v3

    const-wide/16 v16, 0x4e20

    cmp-long v12, v14, v16

    if-gez v12, :cond_3

    .line 193
    :cond_2
    const/4 v12, 0x1

    monitor-exit v13

    goto :goto_0

    .line 195
    :cond_3
    const/4 v4, 0x0

    .line 198
    .local v4, "readSize":I
    if-ge v2, v3, :cond_4

    add-int/lit16 v12, v2, -0x2710

    int-to-long v14, v12

    const-wide/16 v16, 0x2710

    cmp-long v12, v14, v16

    if-lez v12, :cond_5

    .line 199
    :cond_4
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/musicplus/library/audio/FindTag;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v12}, Landroid/media/MediaPlayer;->newRequest()Landroid/os/Parcel;

    move-result-object v7

    .line 201
    .local v7, "requset":Landroid/os/Parcel;
    const/16 v12, 0x200

    invoke-virtual {v7, v12}, Landroid/os/Parcel;->writeInt(I)V

    .line 203
    const v12, 0x251c0

    invoke-virtual {v7, v12}, Landroid/os/Parcel;->writeInt(I)V

    .line 204
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v6

    .line 205
    .local v6, "recordedReply":Landroid/os/Parcel;
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/samsung/musicplus/library/audio/FindTag;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v12, v7, v6}, Landroid/media/MediaPlayer;->setSoundAlive(Landroid/os/Parcel;Landroid/os/Parcel;)I

    .line 206
    invoke-virtual {v6}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 207
    sget-object v12, Lcom/samsung/musicplus/library/audio/FindTag;->CLASSNAME:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, " read size: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v12, v14}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    new-array v5, v4, [B

    .line 210
    .local v5, "recordedBuffer":[B
    invoke-virtual {v6, v5}, Landroid/os/Parcel;->readByteArray([B)V

    .line 216
    new-instance v8, Lcom/shazam/sig/SIGExtractor;

    invoke-direct {v8}, Lcom/shazam/sig/SIGExtractor;-><init>()V

    .line 217
    .local v8, "sigExtractor":Lcom/shazam/sig/SIGExtractor;
    const/16 v12, 0x1f40

    const/4 v14, 0x2

    div-int/lit8 v15, v4, 0x2

    add-int/lit16 v15, v15, 0x3e80

    invoke-virtual {v8, v12, v14, v15, v4}, Lcom/shazam/sig/SIGExtractor;->sigInit(IIII)I

    move-result v10

    .line 219
    .local v10, "sigInitStatus":I
    sget-object v12, Lcom/samsung/musicplus/library/audio/FindTag;->CLASSNAME:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, " sigInit status: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v12, v14}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    invoke-virtual {v8, v5, v4}, Lcom/shazam/sig/SIGExtractor;->sigFlow([BI)I

    move-result v9

    .line 222
    .local v9, "sigFlowStatus":I
    sget-object v12, Lcom/samsung/musicplus/library/audio/FindTag;->CLASSNAME:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, " sigFlow status: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v12, v14}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    invoke-virtual {v8}, Lcom/shazam/sig/SIGExtractor;->sigGet()[B

    move-result-object v12

    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/samsung/musicplus/library/audio/FindTag;->mSignature:[B

    .line 225
    invoke-virtual {v8}, Lcom/shazam/sig/SIGExtractor;->sigDestroy()I

    move-result v11

    .line 226
    .local v11, "sigdestroy":I
    sget-object v12, Lcom/samsung/musicplus/library/audio/FindTag;->CLASSNAME:Ljava/lang/String;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    const-string v15, " sigdestroy status: "

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v12, v14}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 231
    const/4 v12, 0x1

    monitor-exit v13

    goto/16 :goto_0

    .line 233
    .end local v5    # "recordedBuffer":[B
    .end local v6    # "recordedReply":Landroid/os/Parcel;
    .end local v7    # "requset":Landroid/os/Parcel;
    .end local v8    # "sigExtractor":Lcom/shazam/sig/SIGExtractor;
    .end local v9    # "sigFlowStatus":I
    .end local v10    # "sigInitStatus":I
    .end local v11    # "sigdestroy":I
    :cond_5
    const/4 v12, 0x0

    monitor-exit v13
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0
.end method

.method public static getInstance()Lcom/samsung/musicplus/library/audio/FindTag;
    .locals 1

    .prologue
    .line 107
    sget-object v0, Lcom/samsung/musicplus/library/audio/FindTag;->sFindTag:Lcom/samsung/musicplus/library/audio/FindTag;

    if-nez v0, :cond_0

    .line 108
    new-instance v0, Lcom/samsung/musicplus/library/audio/FindTag;

    invoke-direct {v0}, Lcom/samsung/musicplus/library/audio/FindTag;-><init>()V

    sput-object v0, Lcom/samsung/musicplus/library/audio/FindTag;->sFindTag:Lcom/samsung/musicplus/library/audio/FindTag;

    .line 110
    :cond_0
    sget-object v0, Lcom/samsung/musicplus/library/audio/FindTag;->sFindTag:Lcom/samsung/musicplus/library/audio/FindTag;

    return-object v0
.end method

.method private startParsing(Ljava/lang/String;)V
    .locals 7
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 278
    sget-object v4, Lcom/samsung/musicplus/library/audio/FindTag;->CLASSNAME:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, " startParsing path : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 279
    monitor-enter p0

    .line 280
    :try_start_0
    invoke-direct {p0}, Lcom/samsung/musicplus/library/audio/FindTag;->stopMediaPlayer()V

    .line 281
    new-instance v4, Landroid/media/MediaPlayer;

    invoke-direct {v4}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v4, p0, Lcom/samsung/musicplus/library/audio/FindTag;->mPlayer:Landroid/media/MediaPlayer;

    .line 282
    iget-object v4, p0, Lcom/samsung/musicplus/library/audio/FindTag;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v4}, Landroid/media/MediaPlayer;->newRequest()Landroid/os/Parcel;

    move-result-object v3

    .line 283
    .local v3, "request":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 284
    .local v2, "reply":Landroid/os/Parcel;
    const/4 v1, 0x0

    .line 286
    .local v1, "isError":Z
    :try_start_1
    iget-object v4, p0, Lcom/samsung/musicplus/library/audio/FindTag;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v4, p1}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V

    .line 287
    iget-object v4, p0, Lcom/samsung/musicplus/library/audio/FindTag;->mPlayer:Landroid/media/MediaPlayer;

    const/16 v5, 0xbb8

    const/4 v6, 0x1

    invoke-virtual {v4, v5, v6}, Landroid/media/MediaPlayer;->setParameter(II)Z

    .line 288
    iget-object v4, p0, Lcom/samsung/musicplus/library/audio/FindTag;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v4}, Landroid/media/MediaPlayer;->prepare()V

    .line 289
    iget-object v4, p0, Lcom/samsung/musicplus/library/audio/FindTag;->mPlayer:Landroid/media/MediaPlayer;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/media/MediaPlayer;->setVolume(FF)V

    .line 290
    iget-object v4, p0, Lcom/samsung/musicplus/library/audio/FindTag;->mPlayer:Landroid/media/MediaPlayer;

    const/16 v5, 0x2710

    invoke-virtual {v4, v5}, Landroid/media/MediaPlayer;->seekTo(I)V

    .line 291
    iget-object v4, p0, Lcom/samsung/musicplus/library/audio/FindTag;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v4}, Landroid/media/MediaPlayer;->start()V

    .line 293
    const/16 v4, 0x100

    invoke-virtual {v3, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 295
    const v4, 0x251c0

    invoke-virtual {v3, v4}, Landroid/os/Parcel;->writeInt(I)V

    .line 296
    iget-object v4, p0, Lcom/samsung/musicplus/library/audio/FindTag;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v4, v3, v2}, Landroid/media/MediaPlayer;->setSoundAlive(Landroid/os/Parcel;Landroid/os/Parcel;)I
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 319
    :try_start_2
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 320
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 322
    :goto_0
    iput-boolean v1, p0, Lcom/samsung/musicplus/library/audio/FindTag;->mIsFinished:Z

    .line 323
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 324
    return-void

    .line 297
    :catch_0
    move-exception v0

    .line 298
    .local v0, "e":Ljava/lang/NullPointerException;
    :try_start_3
    invoke-virtual {v0}, Ljava/lang/NullPointerException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 299
    const/4 v1, 0x1

    .line 319
    :try_start_4
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 320
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    goto :goto_0

    .line 323
    .end local v0    # "e":Ljava/lang/NullPointerException;
    .end local v1    # "isError":Z
    .end local v2    # "reply":Landroid/os/Parcel;
    .end local v3    # "request":Landroid/os/Parcel;
    :catchall_0
    move-exception v4

    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v4

    .line 300
    .restart local v1    # "isError":Z
    .restart local v2    # "reply":Landroid/os/Parcel;
    .restart local v3    # "request":Landroid/os/Parcel;
    :catch_1
    move-exception v0

    .line 301
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    :try_start_5
    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 302
    const/4 v1, 0x1

    .line 319
    :try_start_6
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 320
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_0

    .line 303
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v0

    .line 304
    .local v0, "e":Ljava/lang/SecurityException;
    :try_start_7
    invoke-virtual {v0}, Ljava/lang/SecurityException;->printStackTrace()V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 305
    const/4 v1, 0x1

    .line 319
    :try_start_8
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 320
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_0

    .line 306
    .end local v0    # "e":Ljava/lang/SecurityException;
    :catch_3
    move-exception v0

    .line 307
    .local v0, "e":Ljava/lang/IllegalStateException;
    :try_start_9
    invoke-virtual {v0}, Ljava/lang/IllegalStateException;->printStackTrace()V
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 308
    const/4 v1, 0x1

    .line 319
    :try_start_a
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 320
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto :goto_0

    .line 309
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_4
    move-exception v0

    .line 310
    .local v0, "e":Ljava/io/IOException;
    :try_start_b
    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    .line 311
    const/4 v1, 0x1

    .line 319
    :try_start_c
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 320
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    goto :goto_0

    .line 312
    .end local v0    # "e":Ljava/io/IOException;
    :catch_5
    move-exception v0

    .line 316
    .local v0, "e":Ljava/lang/RuntimeException;
    :try_start_d
    invoke-virtual {v0}, Ljava/lang/RuntimeException;->printStackTrace()V
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    .line 317
    const/4 v1, 0x1

    .line 319
    :try_start_e
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 320
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    goto :goto_0

    .line 319
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :catchall_1
    move-exception v4

    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    .line 320
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    throw v4
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_0
.end method

.method private stopMediaPlayer()V
    .locals 2

    .prologue
    .line 327
    iget-object v0, p0, Lcom/samsung/musicplus/library/audio/FindTag;->mPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 328
    iget-object v1, p0, Lcom/samsung/musicplus/library/audio/FindTag;->mPlayer:Landroid/media/MediaPlayer;

    monitor-enter v1

    .line 329
    :try_start_0
    iget-object v0, p0, Lcom/samsung/musicplus/library/audio/FindTag;->mPlayer:Landroid/media/MediaPlayer;

    if-nez v0, :cond_1

    .line 330
    monitor-exit v1

    .line 337
    :cond_0
    :goto_0
    return-void

    .line 332
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/library/audio/FindTag;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->stop()V

    .line 333
    iget-object v0, p0, Lcom/samsung/musicplus/library/audio/FindTag;->mPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 334
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/musicplus/library/audio/FindTag;->mPlayer:Landroid/media/MediaPlayer;

    .line 335
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private toFileFromBytes([BLjava/lang/String;)V
    .locals 6
    .param p1, "bytes"    # [B
    .param p2, "path"    # Ljava/lang/String;

    .prologue
    .line 243
    if-eqz p2, :cond_0

    if-nez p1, :cond_1

    .line 275
    :cond_0
    :goto_0
    return-void

    .line 246
    :cond_1
    new-instance v2, Ljava/io/ByteArrayInputStream;

    invoke-direct {v2, p1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    .line 247
    .local v2, "input":Ljava/io/InputStream;
    const/4 v3, 0x0

    .line 249
    .local v3, "output":Ljava/io/OutputStream;
    :try_start_0
    new-instance v3, Ljava/io/FileOutputStream;

    .end local v3    # "output":Ljava/io/OutputStream;
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, p2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {v3, v5}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_2

    .line 255
    .restart local v3    # "output":Ljava/io/OutputStream;
    const/16 v5, 0x400

    new-array v0, v5, [B

    .line 256
    .local v0, "buffer":[B
    const/4 v4, 0x0

    .line 258
    .local v4, "read":I
    :goto_1
    :try_start_1
    invoke-virtual {v2, v0}, Ljava/io/InputStream;->read([B)I

    move-result v4

    const/4 v5, -0x1

    if-eq v4, v5, :cond_2

    .line 259
    const/4 v5, 0x0

    invoke-virtual {v3, v0, v5, v4}, Ljava/io/OutputStream;->write([BII)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 261
    :catch_0
    move-exception v1

    .line 262
    .local v1, "e":Ljava/io/IOException;
    :try_start_2
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 265
    :try_start_3
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_5

    .line 270
    :goto_2
    :try_start_4
    invoke-virtual {v3}, Ljava/io/OutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_0

    .line 271
    :catch_1
    move-exception v1

    .line 272
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 250
    .end local v0    # "buffer":[B
    .end local v1    # "e":Ljava/io/IOException;
    .end local v3    # "output":Ljava/io/OutputStream;
    .end local v4    # "read":I
    :catch_2
    move-exception v1

    .line 251
    .local v1, "e":Ljava/io/FileNotFoundException;
    invoke-virtual {v1}, Ljava/io/FileNotFoundException;->printStackTrace()V

    goto :goto_0

    .line 265
    .end local v1    # "e":Ljava/io/FileNotFoundException;
    .restart local v0    # "buffer":[B
    .restart local v3    # "output":Ljava/io/OutputStream;
    .restart local v4    # "read":I
    :cond_2
    :try_start_5
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4

    .line 270
    :goto_3
    :try_start_6
    invoke-virtual {v3}, Ljava/io/OutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    goto :goto_0

    .line 271
    :catch_3
    move-exception v1

    .line 272
    .local v1, "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    .line 266
    .end local v1    # "e":Ljava/io/IOException;
    :catch_4
    move-exception v1

    .line 267
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    .line 266
    :catch_5
    move-exception v1

    .line 267
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_2

    .line 264
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v5

    .line 265
    :try_start_7
    invoke-virtual {v2}, Ljava/io/InputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_6

    .line 270
    :goto_4
    :try_start_8
    invoke-virtual {v3}, Ljava/io/OutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_7

    .line 273
    :goto_5
    throw v5

    .line 266
    :catch_6
    move-exception v1

    .line 267
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_4

    .line 271
    .end local v1    # "e":Ljava/io/IOException;
    :catch_7
    move-exception v1

    .line 272
    .restart local v1    # "e":Ljava/io/IOException;
    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_5
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 173
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/musicplus/library/audio/FindTag;->mResultListener:Lcom/samsung/musicplus/library/audio/FindTag$OnFindTagResultListener;

    .line 174
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/library/audio/FindTag;->mIsFinished:Z

    .line 175
    invoke-direct {p0}, Lcom/samsung/musicplus/library/audio/FindTag;->stopMediaPlayer()V

    .line 176
    return-void
.end method

.method public startParsingSignature(Ljava/lang/String;Lcom/samsung/musicplus/library/audio/FindTag$OnFindTagResultListener;)Z
    .locals 3
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "listener"    # Lcom/samsung/musicplus/library/audio/FindTag$OnFindTagResultListener;

    .prologue
    const/4 v0, 0x0

    .line 129
    iget-boolean v1, p0, Lcom/samsung/musicplus/library/audio/FindTag;->mIsFinished:Z

    if-nez v1, :cond_0

    .line 132
    sget-object v1, Lcom/samsung/musicplus/library/audio/FindTag;->CLASSNAME:Ljava/lang/String;

    const-string v2, "current work is not finished yet. please call cancel first"

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    :goto_0
    return v0

    .line 135
    :cond_0
    if-eqz p1, :cond_1

    const-string v1, ""

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 136
    :cond_1
    sget-object v1, Lcom/samsung/musicplus/library/audio/FindTag;->CLASSNAME:Ljava/lang/String;

    const-string v2, "current path is wrong. please check it."

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 139
    :cond_2
    new-instance v0, Lcom/samsung/musicplus/library/audio/FindTag$1;

    const-string v1, "Music find tag parser"

    invoke-direct {v0, p0, v1, p2, p1}, Lcom/samsung/musicplus/library/audio/FindTag$1;-><init>(Lcom/samsung/musicplus/library/audio/FindTag;Ljava/lang/String;Lcom/samsung/musicplus/library/audio/FindTag$OnFindTagResultListener;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/samsung/musicplus/library/audio/FindTag$1;->start()V

    .line 163
    const/4 v0, 0x1

    goto :goto_0
.end method
