.class public Lcom/samsung/musicplus/library/view/MusicWaveView;
.super Landroid/view/View;
.source "MusicWaveView.java"


# instance fields
.field private mBottomPadding:I

.field private mCurrentTime:I

.field private mFrameInterval:I

.field private mLeftPadding:I

.field private mLineYpositonMargin:I

.field private mMusicViewData:[B

.field private mRectHeight:I

.field private mRectWidth:I

.field private mRightPadding:I

.field private mTopPadding:I

.field private mTotalFrame:I

.field private mTotalTime:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x0

    .line 84
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 65
    iput v0, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mLineYpositonMargin:I

    .line 67
    iput v0, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mBottomPadding:I

    .line 69
    iput v0, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mTopPadding:I

    .line 71
    iput v0, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mLeftPadding:I

    .line 73
    iput v0, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mRightPadding:I

    .line 85
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v0, 0x0

    .line 93
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 65
    iput v0, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mLineYpositonMargin:I

    .line 67
    iput v0, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mBottomPadding:I

    .line 69
    iput v0, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mTopPadding:I

    .line 71
    iput v0, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mLeftPadding:I

    .line 73
    iput v0, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mRightPadding:I

    .line 94
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v0, 0x0

    .line 102
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 65
    iput v0, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mLineYpositonMargin:I

    .line 67
    iput v0, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mBottomPadding:I

    .line 69
    iput v0, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mTopPadding:I

    .line 71
    iput v0, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mLeftPadding:I

    .line 73
    iput v0, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mRightPadding:I

    .line 103
    return-void
.end method

.method private doDraw(IIIIII)V
    .locals 11
    .param p1, "bottomPadding"    # I
    .param p2, "topPadding"    # I
    .param p3, "leftPadding"    # I
    .param p4, "rightPadding"    # I
    .param p5, "currentTime"    # I
    .param p6, "duration"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 158
    const/16 v1, 0x32

    const/16 v2, 0x64

    const/16 v3, 0x32

    const/16 v4, 0x46

    move-object v0, p0

    move v5, p1

    move v6, p2

    move v7, p3

    move v8, p4

    move/from16 v9, p5

    move/from16 v10, p6

    invoke-virtual/range {v0 .. v10}, Lcom/samsung/musicplus/library/view/MusicWaveView;->doDraw(IIIIIIIIII)V

    .line 160
    return-void
.end method

.method private draw(III[BIIIIIII)V
    .locals 0
    .param p1, "interval"    # I
    .param p2, "initFrameNum"    # I
    .param p3, "totalFrameNum"    # I
    .param p4, "musicViewData"    # [B
    .param p5, "yPositionMargin"    # I
    .param p6, "bottomPadding"    # I
    .param p7, "topPadding"    # I
    .param p8, "leftPadding"    # I
    .param p9, "rightPadding"    # I
    .param p10, "current"    # I
    .param p11, "total"    # I

    .prologue
    .line 199
    iput-object p4, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mMusicViewData:[B

    .line 201
    iput p1, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mFrameInterval:I

    .line 202
    iput p3, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mTotalFrame:I

    .line 203
    iput p11, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mTotalTime:I

    .line 204
    iput p10, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mCurrentTime:I

    .line 206
    iput p5, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mLineYpositonMargin:I

    .line 207
    iput p6, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mBottomPadding:I

    .line 208
    iput p7, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mTopPadding:I

    .line 209
    iput p8, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mLeftPadding:I

    .line 210
    iput p9, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mRightPadding:I

    .line 211
    invoke-virtual {p0}, Lcom/samsung/musicplus/library/view/MusicWaveView;->invalidate()V

    .line 212
    return-void
.end method

.method private drawOutline(Landroid/graphics/Canvas;)V
    .locals 12
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/16 v11, 0xff

    const/high16 v10, 0x42c80000    # 100.0f

    const/4 v9, 0x0

    .line 311
    invoke-virtual {p1, v9, v11, v9, v9}, Landroid/graphics/Canvas;->drawARGB(IIII)V

    .line 314
    new-instance v3, Landroid/graphics/Path;

    invoke-direct {v3}, Landroid/graphics/Path;-><init>()V

    .line 315
    .local v3, "waveformPath":Landroid/graphics/Path;
    iget v4, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mLeftPadding:I

    int-to-float v4, v4

    iget v5, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mRectHeight:I

    iget v6, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mLineYpositonMargin:I

    sub-int/2addr v5, v6

    iget v6, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mBottomPadding:I

    sub-int/2addr v5, v6

    int-to-float v5, v5

    iget-object v6, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mMusicViewData:[B

    aget-byte v6, v6, v9

    int-to-float v6, v6

    iget v7, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mTopPadding:I

    int-to-float v7, v7

    sub-float/2addr v6, v7

    div-float/2addr v6, v10

    iget v7, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mRectHeight:I

    iget v8, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mLineYpositonMargin:I

    sub-int/2addr v7, v8

    iget v8, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mBottomPadding:I

    sub-int/2addr v7, v8

    int-to-float v7, v7

    mul-float/2addr v6, v7

    sub-float/2addr v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Path;->moveTo(FF)V

    .line 320
    const/4 v1, 0x0

    .line 321
    .local v1, "indexPos":F
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    iget-object v4, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mMusicViewData:[B

    array-length v4, v4

    add-int/lit8 v4, v4, -0x1

    if-ge v0, v4, :cond_0

    .line 322
    add-int/lit8 v4, v0, 0x1

    int-to-float v4, v4

    iget v5, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mFrameInterval:I

    int-to-float v5, v5

    iget v6, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mRectWidth:I

    int-to-float v6, v6

    mul-float/2addr v5, v6

    iget v6, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mTotalFrame:I

    int-to-float v6, v6

    div-float/2addr v5, v6

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mRectWidth:I

    iget v6, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mLeftPadding:I

    sub-int/2addr v5, v6

    iget v6, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mRightPadding:I

    sub-int/2addr v5, v6

    int-to-float v5, v5

    iget v6, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mRectWidth:I

    int-to-float v6, v6

    div-float/2addr v5, v6

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mLeftPadding:I

    int-to-float v5, v5

    add-float v1, v4, v5

    .line 326
    iget v4, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mRectHeight:I

    iget v5, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mLineYpositonMargin:I

    sub-int/2addr v4, v5

    iget v5, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mBottomPadding:I

    sub-int/2addr v4, v5

    int-to-float v4, v4

    iget-object v5, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mMusicViewData:[B

    aget-byte v5, v5, v0

    int-to-float v5, v5

    iget v6, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mTopPadding:I

    int-to-float v6, v6

    sub-float/2addr v5, v6

    div-float/2addr v5, v10

    iget v6, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mRectHeight:I

    iget v7, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mLineYpositonMargin:I

    sub-int/2addr v6, v7

    iget v7, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mBottomPadding:I

    sub-int/2addr v6, v7

    int-to-float v6, v6

    mul-float/2addr v5, v6

    sub-float/2addr v4, v5

    invoke-virtual {v3, v1, v4}, Landroid/graphics/Path;->lineTo(FF)V

    .line 321
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 331
    :cond_0
    iget v4, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mRectWidth:I

    iget v5, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mRightPadding:I

    sub-int/2addr v4, v5

    int-to-float v4, v4

    iget v5, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mRectHeight:I

    iget v6, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mBottomPadding:I

    sub-int/2addr v5, v6

    int-to-float v5, v5

    invoke-virtual {v3, v4, v5}, Landroid/graphics/Path;->lineTo(FF)V

    .line 333
    new-instance v2, Landroid/graphics/drawable/ShapeDrawable;

    new-instance v4, Landroid/graphics/drawable/shapes/PathShape;

    iget v5, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mRectWidth:I

    int-to-float v5, v5

    iget v6, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mRectHeight:I

    int-to-float v6, v6

    invoke-direct {v4, v3, v5, v6}, Landroid/graphics/drawable/shapes/PathShape;-><init>(Landroid/graphics/Path;FF)V

    invoke-direct {v2, v4}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/shapes/Shape;)V

    .line 335
    .local v2, "sdPath":Landroid/graphics/drawable/ShapeDrawable;
    invoke-virtual {v2}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v4

    const/high16 v5, 0x40000000    # 2.0f

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 336
    invoke-virtual {v2}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v4

    const/16 v5, 0x18

    const/16 v6, 0x93

    invoke-static {v5, v6, v11}, Landroid/graphics/Color;->rgb(III)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 337
    invoke-virtual {v2}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v4

    new-instance v5, Landroid/graphics/CornerPathEffect;

    const/high16 v6, 0x42480000    # 50.0f

    invoke-direct {v5, v6}, Landroid/graphics/CornerPathEffect;-><init>(F)V

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 338
    invoke-virtual {v2}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v4

    sget-object v5, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 339
    const/4 v4, 0x2

    iget v5, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mRectWidth:I

    add-int/lit8 v5, v5, -0x2

    iget v6, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mRectHeight:I

    invoke-virtual {v2, v9, v4, v5, v6}, Landroid/graphics/drawable/ShapeDrawable;->setBounds(IIII)V

    .line 341
    invoke-virtual {v2, p1}, Landroid/graphics/drawable/ShapeDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 342
    return-void
.end method


# virtual methods
.method public doDraw(II)V
    .locals 11
    .param p1, "currentTime"    # I
    .param p2, "duration"    # I

    .prologue
    const/16 v7, 0x14

    const/16 v1, 0x32

    .line 138
    const/16 v2, 0x64

    const/16 v4, 0x46

    const/16 v6, 0x5a

    move-object v0, p0

    move v3, v1

    move v5, v1

    move v8, v7

    move v9, p1

    move v10, p2

    invoke-virtual/range {v0 .. v10}, Lcom/samsung/musicplus/library/view/MusicWaveView;->doDraw(IIIIIIIIII)V

    .line 139
    return-void
.end method

.method public doDraw(IIIIIIIIII)V
    .locals 12
    .param p1, "frameInterval"    # I
    .param p2, "frameInitPos"    # I
    .param p3, "totalFrame"    # I
    .param p4, "lineYpositonMargin"    # I
    .param p5, "bottomPadding"    # I
    .param p6, "topPadding"    # I
    .param p7, "leftPadding"    # I
    .param p8, "rightPadding"    # I
    .param p9, "currentTime"    # I
    .param p10, "duration"    # I

    .prologue
    .line 186
    iget-object v0, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mMusicViewData:[B

    if-nez v0, :cond_0

    .line 187
    const-string v0, "MusicWaveView"

    const-string v1, "Hey, you were trying to draw wave view without setting MusicView Data(mMusicViewData)"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 194
    :goto_0
    return-void

    .line 191
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mMusicViewData:[B

    array-length v0, v0

    mul-int v3, p3, v0

    iget-object v4, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mMusicViewData:[B

    move-object v0, p0

    move v1, p1

    move v2, p2

    move/from16 v5, p4

    move/from16 v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    move/from16 v9, p8

    move/from16 v10, p9

    move/from16 v11, p10

    invoke-direct/range {v0 .. v11}, Lcom/samsung/musicplus/library/view/MusicWaveView;->draw(III[BIIIIIII)V

    goto :goto_0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 14
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 216
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 217
    iget-object v9, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mMusicViewData:[B

    if-nez v9, :cond_1

    .line 308
    :cond_0
    :goto_0
    return-void

    .line 220
    :cond_1
    iget v9, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mCurrentTime:I

    if-ltz v9, :cond_0

    .line 223
    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-virtual {p1, v9, v10, v11, v12}, Landroid/graphics/Canvas;->drawARGB(IIII)V

    .line 226
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    .line 227
    .local v0, "blueWavePath":Landroid/graphics/Path;
    iget v9, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mLeftPadding:I

    int-to-float v9, v9

    iget v10, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mRectHeight:I

    iget v11, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mBottomPadding:I

    sub-int/2addr v10, v11

    int-to-float v10, v10

    invoke-virtual {v0, v9, v10}, Landroid/graphics/Path;->moveTo(FF)V

    .line 228
    iget v9, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mLeftPadding:I

    int-to-float v9, v9

    iget v10, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mRectHeight:I

    iget v11, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mLineYpositonMargin:I

    sub-int/2addr v10, v11

    iget v11, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mBottomPadding:I

    sub-int/2addr v10, v11

    int-to-float v10, v10

    invoke-virtual {v0, v9, v10}, Landroid/graphics/Path;->lineTo(FF)V

    .line 231
    const/4 v5, 0x0

    .line 232
    .local v5, "indexPosBlue":F
    iget-object v9, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mMusicViewData:[B

    array-length v9, v9

    int-to-double v10, v9

    iget v9, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mCurrentTime:I

    int-to-double v12, v9

    mul-double/2addr v10, v12

    iget v9, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mTotalTime:I

    int-to-double v12, v9

    div-double/2addr v10, v12

    double-to-int v2, v10

    .line 234
    .local v2, "drawToPosition":I
    iget-object v9, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mMusicViewData:[B

    array-length v9, v9

    if-gt v2, v9, :cond_2

    if-gez v2, :cond_3

    .line 235
    :cond_2
    const-string v9, "MusicWaveView"

    const-string v10, "Did you doDraw() with proper currentMediaTime or durationTime?"

    invoke-static {v9, v10}, Lcom/samsung/musicplus/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 240
    :cond_3
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_1
    if-ge v4, v2, :cond_4

    .line 241
    add-int/lit8 v9, v4, 0x1

    int-to-float v9, v9

    iget v10, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mFrameInterval:I

    int-to-float v10, v10

    iget v11, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mRectWidth:I

    int-to-float v11, v11

    mul-float/2addr v10, v11

    iget v11, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mTotalFrame:I

    int-to-float v11, v11

    div-float/2addr v10, v11

    mul-float/2addr v9, v10

    iget v10, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mRectWidth:I

    iget v11, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mLeftPadding:I

    sub-int/2addr v10, v11

    iget v11, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mRightPadding:I

    sub-int/2addr v10, v11

    int-to-float v10, v10

    iget v11, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mRectWidth:I

    int-to-float v11, v11

    div-float/2addr v10, v11

    mul-float/2addr v9, v10

    iget v10, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mLeftPadding:I

    int-to-float v10, v10

    add-float v5, v9, v10

    .line 246
    iget v9, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mRectHeight:I

    iget v10, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mLineYpositonMargin:I

    sub-int/2addr v9, v10

    iget v10, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mBottomPadding:I

    sub-int/2addr v9, v10

    int-to-float v9, v9

    iget-object v10, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mMusicViewData:[B

    aget-byte v10, v10, v4

    int-to-float v10, v10

    iget v11, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mTopPadding:I

    int-to-float v11, v11

    sub-float/2addr v10, v11

    const/high16 v11, 0x42c80000    # 100.0f

    div-float/2addr v10, v11

    iget v11, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mRectHeight:I

    iget v12, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mLineYpositonMargin:I

    sub-int/2addr v11, v12

    iget v12, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mBottomPadding:I

    sub-int/2addr v11, v12

    int-to-float v11, v11

    mul-float/2addr v10, v11

    sub-float/2addr v9, v10

    invoke-virtual {v0, v5, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 240
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 253
    :cond_4
    const/4 v9, 0x0

    cmpl-float v9, v5, v9

    if-eqz v9, :cond_5

    .line 254
    iget v9, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mRectHeight:I

    iget v10, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mBottomPadding:I

    sub-int/2addr v9, v10

    int-to-float v9, v9

    invoke-virtual {v0, v5, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 255
    iget v9, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mRectHeight:I

    iget v10, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mBottomPadding:I

    sub-int/2addr v9, v10

    add-int/lit8 v9, v9, 0x1

    int-to-float v9, v9

    invoke-virtual {v0, v5, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 258
    :cond_5
    new-instance v7, Landroid/graphics/drawable/ShapeDrawable;

    new-instance v9, Landroid/graphics/drawable/shapes/PathShape;

    iget v10, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mRectWidth:I

    int-to-float v10, v10

    iget v11, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mRectHeight:I

    int-to-float v11, v11

    invoke-direct {v9, v0, v10, v11}, Landroid/graphics/drawable/shapes/PathShape;-><init>(Landroid/graphics/Path;FF)V

    invoke-direct {v7, v9}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/shapes/Shape;)V

    .line 260
    .local v7, "sdPath":Landroid/graphics/drawable/ShapeDrawable;
    invoke-virtual {v7}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v9

    const/16 v10, 0xff

    const/4 v11, 0x6

    const/16 v12, 0x4a

    const/16 v13, 0x87

    invoke-static {v10, v11, v12, v13}, Landroid/graphics/Color;->argb(IIII)I

    move-result v10

    invoke-virtual {v9, v10}, Landroid/graphics/Paint;->setColor(I)V

    .line 261
    invoke-virtual {v7}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v9

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 263
    new-instance v1, Landroid/graphics/BlurMaskFilter;

    const v9, 0x3dcccccd    # 0.1f

    sget-object v10, Landroid/graphics/BlurMaskFilter$Blur;->INNER:Landroid/graphics/BlurMaskFilter$Blur;

    invoke-direct {v1, v9, v10}, Landroid/graphics/BlurMaskFilter;-><init>(FLandroid/graphics/BlurMaskFilter$Blur;)V

    .line 264
    .local v1, "blur":Landroid/graphics/BlurMaskFilter;
    invoke-virtual {v7}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v9

    invoke-virtual {v9, v1}, Landroid/graphics/Paint;->setMaskFilter(Landroid/graphics/MaskFilter;)Landroid/graphics/MaskFilter;

    .line 265
    invoke-virtual {v7}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v9

    new-instance v10, Landroid/graphics/CornerPathEffect;

    const/high16 v11, 0x42480000    # 50.0f

    invoke-direct {v10, v11}, Landroid/graphics/CornerPathEffect;-><init>(F)V

    invoke-virtual {v9, v10}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 266
    invoke-virtual {v7}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v9

    sget-object v10, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v9, v10}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 267
    const/4 v9, 0x0

    const/4 v10, 0x2

    iget v11, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mRectWidth:I

    add-int/lit8 v11, v11, -0x2

    iget v12, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mRectHeight:I

    invoke-virtual {v7, v9, v10, v11, v12}, Landroid/graphics/drawable/ShapeDrawable;->setBounds(IIII)V

    .line 269
    invoke-virtual {v7, p1}, Landroid/graphics/drawable/ShapeDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 272
    new-instance v3, Landroid/graphics/Path;

    invoke-direct {v3}, Landroid/graphics/Path;-><init>()V

    .line 273
    .local v3, "grayWavePath":Landroid/graphics/Path;
    iget v9, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mRectWidth:I

    iget v10, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mRightPadding:I

    sub-int/2addr v9, v10

    int-to-float v9, v9

    iget v10, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mRectHeight:I

    iget v11, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mBottomPadding:I

    sub-int/2addr v10, v11

    int-to-float v10, v10

    invoke-virtual {v3, v9, v10}, Landroid/graphics/Path;->moveTo(FF)V

    .line 274
    iget v9, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mRectWidth:I

    iget v10, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mRightPadding:I

    sub-int/2addr v9, v10

    int-to-float v9, v9

    iget v10, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mRectHeight:I

    iget v11, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mLineYpositonMargin:I

    sub-int/2addr v10, v11

    iget v11, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mBottomPadding:I

    sub-int/2addr v10, v11

    int-to-float v10, v10

    invoke-virtual {v3, v9, v10}, Landroid/graphics/Path;->lineTo(FF)V

    .line 277
    const/4 v6, 0x0

    .line 279
    .local v6, "indexPosGray":F
    iget-object v9, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mMusicViewData:[B

    array-length v4, v9

    :goto_2
    add-int/lit8 v9, v2, 0x1

    if-lt v4, v9, :cond_6

    .line 280
    int-to-float v9, v4

    iget v10, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mFrameInterval:I

    int-to-float v10, v10

    iget v11, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mRectWidth:I

    int-to-float v11, v11

    mul-float/2addr v10, v11

    iget v11, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mTotalFrame:I

    int-to-float v11, v11

    div-float/2addr v10, v11

    mul-float/2addr v9, v10

    iget v10, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mRectWidth:I

    iget v11, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mLeftPadding:I

    sub-int/2addr v10, v11

    iget v11, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mRightPadding:I

    sub-int/2addr v10, v11

    int-to-float v10, v10

    iget v11, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mRectWidth:I

    int-to-float v11, v11

    div-float/2addr v10, v11

    mul-float/2addr v9, v10

    iget v10, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mLeftPadding:I

    int-to-float v10, v10

    add-float v6, v9, v10

    .line 284
    iget v9, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mRectHeight:I

    iget v10, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mLineYpositonMargin:I

    sub-int/2addr v9, v10

    iget v10, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mBottomPadding:I

    sub-int/2addr v9, v10

    int-to-float v9, v9

    iget-object v10, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mMusicViewData:[B

    add-int/lit8 v11, v4, -0x1

    aget-byte v10, v10, v11

    int-to-float v10, v10

    iget v11, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mTopPadding:I

    int-to-float v11, v11

    sub-float/2addr v10, v11

    const/high16 v11, 0x42c80000    # 100.0f

    div-float/2addr v10, v11

    iget v11, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mRectHeight:I

    iget v12, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mLineYpositonMargin:I

    sub-int/2addr v11, v12

    iget v12, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mBottomPadding:I

    sub-int/2addr v11, v12

    int-to-float v11, v11

    mul-float/2addr v10, v11

    sub-float/2addr v9, v10

    invoke-virtual {v3, v6, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 279
    add-int/lit8 v4, v4, -0x1

    goto :goto_2

    .line 291
    :cond_6
    const/4 v9, 0x0

    cmpl-float v9, v6, v9

    if-eqz v9, :cond_7

    .line 292
    iget v9, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mRectHeight:I

    iget v10, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mBottomPadding:I

    sub-int/2addr v9, v10

    int-to-float v9, v9

    invoke-virtual {v3, v6, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 293
    iget v9, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mRectHeight:I

    iget v10, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mBottomPadding:I

    sub-int/2addr v9, v10

    int-to-float v9, v9

    const/high16 v10, 0x3f800000    # 1.0f

    add-float/2addr v9, v10

    invoke-virtual {v3, v6, v9}, Landroid/graphics/Path;->lineTo(FF)V

    .line 296
    :cond_7
    new-instance v8, Landroid/graphics/drawable/ShapeDrawable;

    new-instance v9, Landroid/graphics/drawable/shapes/PathShape;

    iget v10, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mRectWidth:I

    int-to-float v10, v10

    iget v11, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mRectHeight:I

    int-to-float v11, v11

    invoke-direct {v9, v3, v10, v11}, Landroid/graphics/drawable/shapes/PathShape;-><init>(Landroid/graphics/Path;FF)V

    invoke-direct {v8, v9}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/shapes/Shape;)V

    .line 298
    .local v8, "sdPathPrev":Landroid/graphics/drawable/ShapeDrawable;
    invoke-virtual {v8}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v9

    const/16 v10, 0xff

    const/16 v11, 0x20

    const/16 v12, 0x21

    const/16 v13, 0x23

    invoke-static {v10, v11, v12, v13}, Landroid/graphics/Color;->argb(IIII)I

    move-result v10

    invoke-virtual {v9, v10}, Landroid/graphics/Paint;->setColor(I)V

    .line 299
    invoke-virtual {v8}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v9

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 300
    invoke-virtual {v8}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v9

    new-instance v10, Landroid/graphics/CornerPathEffect;

    const/high16 v11, 0x42480000    # 50.0f

    invoke-direct {v10, v11}, Landroid/graphics/CornerPathEffect;-><init>(F)V

    invoke-virtual {v9, v10}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    .line 301
    invoke-virtual {v8}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v9

    sget-object v10, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v9, v10}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 302
    const/4 v9, 0x0

    const/4 v10, 0x2

    iget v11, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mRectWidth:I

    add-int/lit8 v11, v11, -0x2

    iget v12, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mRectHeight:I

    invoke-virtual {v8, v9, v10, v11, v12}, Landroid/graphics/drawable/ShapeDrawable;->setBounds(IIII)V

    .line 304
    invoke-virtual {v8, p1}, Landroid/graphics/drawable/ShapeDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 306
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/library/view/MusicWaveView;->drawOutline(Landroid/graphics/Canvas;)V

    goto/16 :goto_0
.end method

.method protected onMeasure(II)V
    .locals 1
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 107
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mRectHeight:I

    .line 108
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mRectWidth:I

    .line 109
    invoke-super {p0, p1, p2}, Landroid/view/View;->onMeasure(II)V

    .line 110
    return-void
.end method

.method public setWaveData([B)V
    .locals 0
    .param p1, "musicViewData"    # [B

    .prologue
    .line 124
    iput-object p1, p0, Lcom/samsung/musicplus/library/view/MusicWaveView;->mMusicViewData:[B

    .line 125
    return-void
.end method
