.class public Lcom/samsung/musicplus/library/magazinecard/SecMusicMagazineCard;
.super Ljava/lang/Object;
.source "SecMusicMagazineCard.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static enabledMagazineCard(Landroid/content/Context;)Z
    .locals 2
    .param p0, "c"    # Landroid/content/Context;

    .prologue
    .line 29
    if-nez p0, :cond_0

    .line 30
    const/4 v1, 0x0

    .line 33
    :goto_0
    return v1

    .line 32
    :cond_0
    invoke-static {p0}, Lcom/samsung/musicplus/library/magazinecard/SecMusicMagazineCard;->getMagazineCardManger(Landroid/content/Context;)Lcom/samsung/android/magazinecard/MagazineCardManager;

    move-result-object v0

    .line 33
    .local v0, "mcm":Lcom/samsung/android/magazinecard/MagazineCardManager;
    invoke-virtual {v0}, Lcom/samsung/android/magazinecard/MagazineCardManager;->isServiceEnabled()Z

    move-result v1

    goto :goto_0
.end method

.method static getMagazineCardManger(Landroid/content/Context;)Lcom/samsung/android/magazinecard/MagazineCardManager;
    .locals 1
    .param p0, "c"    # Landroid/content/Context;

    .prologue
    .line 19
    const-string v0, "magazinecardservice"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/magazinecard/MagazineCardManager;

    return-object v0
.end method

.method public static makeMagazineCard(Landroid/content/Context;ILandroid/widget/RemoteViews;Landroid/app/PendingIntent;)Lcom/samsung/musicplus/library/magazinecard/MagazineCardInfoCompat;
    .locals 3
    .param p0, "c"    # Landroid/content/Context;
    .param p1, "id"    # I
    .param p2, "rv"    # Landroid/widget/RemoteViews;
    .param p3, "pi"    # Landroid/app/PendingIntent;

    .prologue
    .line 73
    if-nez p0, :cond_0

    .line 74
    const/4 v1, 0x0

    .line 92
    :goto_0
    return-object v1

    .line 77
    :cond_0
    new-instance v0, Lcom/samsung/android/magazinecard/MagazineCardInfo$Builder;

    invoke-direct {v0, p0}, Lcom/samsung/android/magazinecard/MagazineCardInfo$Builder;-><init>(Landroid/content/Context;)V

    .line 78
    .local v0, "builder":Lcom/samsung/android/magazinecard/MagazineCardInfo$Builder;
    sget v1, Lcom/samsung/android/magazinecard/MagazineCardInfo;->CATEGORY_MUSIC_PLAYER:I

    invoke-virtual {v0, v1}, Lcom/samsung/android/magazinecard/MagazineCardInfo$Builder;->setCategory(I)Lcom/samsung/android/magazinecard/MagazineCardInfo$Builder;

    .line 80
    if-lez p1, :cond_1

    .line 81
    invoke-virtual {v0, p1}, Lcom/samsung/android/magazinecard/MagazineCardInfo$Builder;->setCardId(I)Lcom/samsung/android/magazinecard/MagazineCardInfo$Builder;

    .line 84
    :cond_1
    if-eqz p2, :cond_2

    .line 85
    invoke-virtual {v0, p2}, Lcom/samsung/android/magazinecard/MagazineCardInfo$Builder;->setContentView(Landroid/widget/RemoteViews;)Lcom/samsung/android/magazinecard/MagazineCardInfo$Builder;

    .line 88
    :cond_2
    if-eqz p3, :cond_3

    .line 89
    invoke-virtual {v0, p3}, Lcom/samsung/android/magazinecard/MagazineCardInfo$Builder;->setLaunchIntent(Landroid/app/PendingIntent;)Lcom/samsung/android/magazinecard/MagazineCardInfo$Builder;

    .line 92
    :cond_3
    new-instance v1, Lcom/samsung/musicplus/library/magazinecard/MagazineCardInfoCompat;

    invoke-virtual {v0}, Lcom/samsung/android/magazinecard/MagazineCardInfo$Builder;->build()Lcom/samsung/android/magazinecard/MagazineCardInfo;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/samsung/musicplus/library/magazinecard/MagazineCardInfoCompat;-><init>(Lcom/samsung/android/magazinecard/MagazineCardInfo;)V

    goto :goto_0
.end method

.method public static removeAllCard(Landroid/content/Context;)V
    .locals 1
    .param p0, "c"    # Landroid/content/Context;

    .prologue
    .line 44
    if-eqz p0, :cond_0

    .line 45
    invoke-static {p0}, Lcom/samsung/musicplus/library/magazinecard/SecMusicMagazineCard;->getMagazineCardManger(Landroid/content/Context;)Lcom/samsung/android/magazinecard/MagazineCardManager;

    move-result-object v0

    .line 46
    .local v0, "mcm":Lcom/samsung/android/magazinecard/MagazineCardManager;
    if-eqz v0, :cond_0

    .line 47
    invoke-virtual {v0}, Lcom/samsung/android/magazinecard/MagazineCardManager;->removeAllCard()Z

    .line 50
    .end local v0    # "mcm":Lcom/samsung/android/magazinecard/MagazineCardManager;
    :cond_0
    return-void
.end method

.method public static updateCard(Landroid/content/Context;Lcom/samsung/musicplus/library/magazinecard/MagazineCardInfoCompat;)V
    .locals 2
    .param p0, "c"    # Landroid/content/Context;
    .param p1, "mcc"    # Lcom/samsung/musicplus/library/magazinecard/MagazineCardInfoCompat;

    .prologue
    .line 59
    if-eqz p0, :cond_0

    .line 60
    invoke-static {p0}, Lcom/samsung/musicplus/library/magazinecard/SecMusicMagazineCard;->getMagazineCardManger(Landroid/content/Context;)Lcom/samsung/android/magazinecard/MagazineCardManager;

    move-result-object v0

    .line 61
    .local v0, "mcm":Lcom/samsung/android/magazinecard/MagazineCardManager;
    invoke-virtual {p1}, Lcom/samsung/musicplus/library/magazinecard/MagazineCardInfoCompat;->getMagazineCardInfo()Lcom/samsung/android/magazinecard/MagazineCardInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/magazinecard/MagazineCardManager;->updateCard(Lcom/samsung/android/magazinecard/MagazineCardInfo;)Z

    .line 63
    .end local v0    # "mcm":Lcom/samsung/android/magazinecard/MagazineCardManager;
    :cond_0
    return-void
.end method
