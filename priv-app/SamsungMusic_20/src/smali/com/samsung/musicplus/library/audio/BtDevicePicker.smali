.class public Lcom/samsung/musicplus/library/audio/BtDevicePicker;
.super Ljava/lang/Object;
.source "BtDevicePicker.java"


# static fields
.field public static final ACTION_LAUNCH:Ljava/lang/String; = "android.bluetooth.devicepicker.action.LAUNCH"

.field public static final EXTRA_FILTER_TYPE:Ljava/lang/String; = "android.bluetooth.devicepicker.extra.FILTER_TYPE"

.field public static final EXTRA_IS_FROM_BT_HEADSET:Ljava/lang/String; = "android.bluetooth.FromHeadsetActivity"

.field public static final EXTRA_NEED_AUTH:Ljava/lang/String; = "android.bluetooth.devicepicker.extra.NEED_AUTH"

.field public static final FILTER_TYPE_AUDIO_AV:I = 0x6


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
