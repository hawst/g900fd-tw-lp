.class public interface abstract Lcom/samsung/musicplus/library/dlna/DlnaStore$MediaContentsColumns;
.super Ljava/lang/Object;
.source "DlnaStore.java"

# interfaces
.implements Landroid/provider/BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/library/dlna/DlnaStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "MediaContentsColumns"
.end annotation


# static fields
.field public static final ALBUM:Ljava/lang/String; = "album"

.field public static final ALBUM_ID:Ljava/lang/String; = "album_id"

.field public static final ARTIST:Ljava/lang/String; = "artist"

.field public static final DATA:Ljava/lang/String; = "_data"

.field public static final DURATION:Ljava/lang/String; = "duration"

.field public static final EXTENSION:Ljava/lang/String; = "extension"

.field public static final FILE_SIZE:Ljava/lang/String; = "file_size"

.field public static final GENRE_NAME:Ljava/lang/String; = "genre_name"

.field public static final MIME_TYPE:Ljava/lang/String; = "mime_type"

.field public static final SEED:Ljava/lang/String; = "seed"

.field public static final TITLE:Ljava/lang/String; = "title"
