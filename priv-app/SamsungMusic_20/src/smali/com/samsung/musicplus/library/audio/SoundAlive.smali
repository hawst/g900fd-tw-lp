.class public Lcom/samsung/musicplus/library/audio/SoundAlive;
.super Ljava/lang/Object;
.source "SoundAlive.java"


# static fields
.field private static final CLASSNAME:Ljava/lang/String;

.field private static final DEFAULT_USER_EQ_VALUE:I = 0xa

.field private static final DEFULT_USER_EFFECT_VALUE:I = 0x0

.field private static ID3_TO_DEFINED_EQ:[I = null

.field private static final SA_SET_EXTENDED_PARAM:I = 0x800

.field private static final SA_SET_PRESET:I = 0x10

.field private static final SA_SET_SPEED:I = 0x400

.field private static final SA_SET_USEREQ_DATA:I = 0x20

.field private static final SEC_GENRES_STR:[Ljava/lang/String;

.field public static final SOUNDALIVE_FIRST_ITEM:I = 0x0

.field public static final SOUNDALIVE_LAST_ITEM:I = 0x10

.field public static final SOUNDALIVE_NOT_SUPPORTED_BT:I = 0x1

.field public static final SOUNDALIVE_NOT_SUPPORTED_HDMI:I = 0x3

.field public static final SOUNDALIVE_NOT_SUPPORTED_LINEOUT:I = 0x2

.field public static final SOUNDALIVE_NOT_SUPPORTED_SPEAKER:I = 0x0

.field public static final SOUNDALIVE_NOT_SUPPORTED_UNKOWN_PATH:I = 0x4

.field public static final SOUNDALIVE_PRESET_AUTO:I = -0x1

.field public static final SOUNDALIVE_PRESET_BASS_BOOST:I = 0x7

.field public static final SOUNDALIVE_PRESET_CAFE:I = 0xb

.field public static final SOUNDALIVE_PRESET_CLASSIC:I = 0x5

.field public static final SOUNDALIVE_PRESET_CONCERT_HALL:I = 0xc

.field public static final SOUNDALIVE_PRESET_DANCE:I = 0x3

.field public static final SOUNDALIVE_PRESET_EXTERNALIZATION:I = 0xa

.field public static final SOUNDALIVE_PRESET_JAZZ:I = 0x4

.field public static final SOUNDALIVE_PRESET_MOVIE:I = 0xf

.field public static final SOUNDALIVE_PRESET_MTHEATER:I = 0x9

.field public static final SOUNDALIVE_PRESET_NORMAL:I = 0x0

.field public static final SOUNDALIVE_PRESET_POP:I = 0x1

.field public static final SOUNDALIVE_PRESET_ROCK:I = 0x2

.field public static final SOUNDALIVE_PRESET_SAMSUNG_TUBE_SOUND:I = 0x11

.field public static final SOUNDALIVE_PRESET_SRS_SURROUND_MUSIC:I = 0x12

.field public static final SOUNDALIVE_PRESET_TREBLE_BOOST:I = 0x8

.field public static final SOUNDALIVE_PRESET_USER:I = 0xd

.field public static final SOUNDALIVE_PRESET_VIRT51:I = 0x10

.field public static final SOUNDALIVE_PRESET_VOCAL:I = 0x6

.field public static final SOUNDALIVE_PRESET_VOICE:I = 0xe

.field public static final SOUNDALIVE_PRESET_WOW_HD:I = 0x13

.field public static final SOUNDALIVE_SUPPORTED_NORMAL:I = -0x1

.field public static final SOUNDALIVE_USER_EQ_BAND_LENGTH:I = 0x7

.field public static final SOUNDALIVE_USER_EXT_BAND_LENGTH:I = 0x5

.field private static final sGenreEqMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/16 v1, 0x1c

    .line 66
    const-class v0, Lcom/samsung/musicplus/library/audio/SoundAlive;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/library/audio/SoundAlive;->CLASSNAME:Ljava/lang/String;

    .line 510
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/musicplus/library/audio/SoundAlive;->sGenreEqMap:Ljava/util/HashMap;

    .line 515
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/samsung/musicplus/library/audio/SoundAlive;->ID3_TO_DEFINED_EQ:[I

    .line 528
    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "Alternative/Indie"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "Blues"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "Children\'s"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "Classical"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "Comedy/Spoken"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "Country"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "Dance"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "Easy Listening"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "Electronic"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "Folk"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "Holiday"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "House"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "Jazz"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "Latin"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "New Age"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "Others"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "Pop"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "Rap/Hip Hop"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "Reggae"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "Religious"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "R&B/Soul"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "Rock"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "Soundtrack"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "Trance"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "<unknown>"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "Vocal"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string v2, "World"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string v2, "Rap / Hip-hop"

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/musicplus/library/audio/SoundAlive;->SEC_GENRES_STR:[Ljava/lang/String;

    return-void

    .line 515
    nop

    :array_0
    .array-data 4
        0x2
        0x4
        0x1
        0x5
        0x1
        0x1
        0x3
        0x1
        0x3
        0x2
        0x1
        0x3
        0x4
        0x3
        0x5
        0x1
        0x1
        0x3
        0x1
        0x1
        0x1
        0x2
        0x5
        0x3
        0x0
        0x1
        0x1
        0x1
    .end array-data
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    return-void
.end method

.method public static getAudioEffect(Ljava/lang/String;)I
    .locals 3
    .param p0, "genreName"    # Ljava/lang/String;

    .prologue
    .line 549
    const/4 v0, 0x0

    .line 551
    .local v0, "soundAlive":I
    if-nez p0, :cond_0

    move v1, v0

    .line 561
    .end local v0    # "soundAlive":I
    .local v1, "soundAlive":I
    :goto_0
    return v1

    .line 554
    .end local v1    # "soundAlive":I
    .restart local v0    # "soundAlive":I
    :cond_0
    sget-object v2, Lcom/samsung/musicplus/library/audio/SoundAlive;->sGenreEqMap:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 555
    invoke-static {}, Lcom/samsung/musicplus/library/audio/SoundAlive;->initGenreHashMap()V

    .line 558
    :cond_1
    sget-object v2, Lcom/samsung/musicplus/library/audio/SoundAlive;->sGenreEqMap:Ljava/util/HashMap;

    invoke-virtual {v2, p0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 559
    sget-object v2, Lcom/samsung/musicplus/library/audio/SoundAlive;->sGenreEqMap:Ljava/util/HashMap;

    invoke-virtual {v2, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :cond_2
    move v1, v0

    .line 561
    .end local v0    # "soundAlive":I
    .restart local v1    # "soundAlive":I
    goto :goto_0
.end method

.method private static hasOneMoreSpeaker()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 670
    sget v1, Lcom/samsung/musicplus/library/MusicFeatures;->NUMBER_OF_SPEAKER:I

    if-le v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static initGenreHashMap()V
    .locals 4

    .prologue
    .line 537
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v1, Lcom/samsung/musicplus/library/audio/SoundAlive;->SEC_GENRES_STR:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 538
    sget-object v1, Lcom/samsung/musicplus/library/audio/SoundAlive;->sGenreEqMap:Ljava/util/HashMap;

    sget-object v2, Lcom/samsung/musicplus/library/audio/SoundAlive;->SEC_GENRES_STR:[Ljava/lang/String;

    aget-object v2, v2, v0

    sget-object v3, Lcom/samsung/musicplus/library/audio/SoundAlive;->ID3_TO_DEFINED_EQ:[I

    aget v3, v3, v0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 537
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 540
    :cond_0
    return-void
.end method

.method private static isSoundAliveSupportedInCurrentAudioPath(Landroid/content/Context;I)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "soundAlive"    # I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 612
    invoke-static {p0}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->getInstance(Landroid/content/Context;)Lcom/samsung/musicplus/library/audio/SecAudioManager;

    move-result-object v0

    .line 613
    .local v0, "am":Lcom/samsung/musicplus/library/audio/SecAudioManager;
    const/4 v1, -0x1

    .line 615
    .local v1, "notSupportId":I
    invoke-virtual {v0}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->isAudioPathSpeaker()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 616
    invoke-static {p1}, Lcom/samsung/musicplus/library/audio/SoundAlive;->isSupportSoundAliveSpeaker(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 617
    const/4 v1, 0x0

    .line 635
    :cond_0
    :goto_0
    return v1

    .line 619
    :cond_1
    invoke-virtual {v0}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->isAudioPathBT()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v0}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->isAudioPathBtModeSco()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 620
    :cond_2
    invoke-static {p1}, Lcom/samsung/musicplus/library/audio/SoundAlive;->isSupportSoundAliveBT(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 621
    const/4 v1, 0x1

    goto :goto_0

    .line 623
    :cond_3
    invoke-virtual {v0}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->isAudioPathLineOut()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 624
    invoke-static {p1}, Lcom/samsung/musicplus/library/audio/SoundAlive;->isSupportSoundAliveLineOut(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 625
    const/4 v1, 0x2

    goto :goto_0

    .line 627
    :cond_4
    invoke-virtual {v0}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->isHDMIConnect()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 628
    invoke-static {p1}, Lcom/samsung/musicplus/library/audio/SoundAlive;->isSupportSoundAliveHDMI(I)Z

    move-result v2

    if-nez v2, :cond_0

    .line 629
    const/4 v1, 0x3

    goto :goto_0

    .line 631
    :cond_5
    invoke-virtual {v0}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->isWiredHeadsetOn()Z

    move-result v2

    if-nez v2, :cond_0

    if-eqz p1, :cond_0

    .line 632
    const/4 v1, 0x4

    goto :goto_0
.end method

.method public static isSupportSoundAliveBT(I)Z
    .locals 2
    .param p0, "soundAlive"    # I

    .prologue
    const/4 v0, 0x1

    .line 694
    const/4 v1, -0x1

    if-eq p0, v1, :cond_0

    if-eqz p0, :cond_0

    if-eq p0, v0, :cond_0

    const/4 v1, 0x2

    if-eq p0, v1, :cond_0

    const/4 v1, 0x3

    if-eq p0, v1, :cond_0

    const/4 v1, 0x4

    if-eq p0, v1, :cond_0

    const/4 v1, 0x5

    if-eq p0, v1, :cond_0

    const/16 v1, 0xd

    if-ne p0, v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isSupportSoundAliveHDMI(I)Z
    .locals 1
    .param p0, "soundAlive"    # I

    .prologue
    .line 740
    if-nez p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isSupportSoundAliveLineOut(I)Z
    .locals 2
    .param p0, "soundAlive"    # I

    .prologue
    const/4 v0, 0x1

    .line 721
    const/4 v1, -0x1

    if-eq p0, v1, :cond_0

    if-eqz p0, :cond_0

    if-eq p0, v0, :cond_0

    const/4 v1, 0x2

    if-eq p0, v1, :cond_0

    const/4 v1, 0x3

    if-eq p0, v1, :cond_0

    const/4 v1, 0x4

    if-eq p0, v1, :cond_0

    const/4 v1, 0x5

    if-eq p0, v1, :cond_0

    const/16 v1, 0xd

    if-ne p0, v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isSupportSoundAliveSpeaker(I)Z
    .locals 1
    .param p0, "soundAlive"    # I

    .prologue
    .line 657
    const/16 v0, 0x9

    if-eq p0, v0, :cond_0

    const/16 v0, 0xa

    if-eq p0, v0, :cond_0

    const/16 v0, 0x11

    if-eq p0, v0, :cond_0

    const/4 v0, 0x7

    if-ne p0, v0, :cond_1

    :cond_0
    invoke-static {}, Lcom/samsung/musicplus/library/audio/SoundAlive;->hasOneMoreSpeaker()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static setAudioEffect(Landroid/media/MediaPlayer;I)V
    .locals 6
    .param p0, "mp"    # Landroid/media/MediaPlayer;
    .param p1, "soundEffect"    # I

    .prologue
    .line 336
    if-gez p1, :cond_0

    .line 337
    sget-object v3, Lcom/samsung/musicplus/library/audio/SoundAlive;->CLASSNAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "call setAudioEffect method with soundEffect < 0 value, is something wrong in your codes? Because SoundAlive can\'t handle \'-1\' "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 355
    :goto_0
    return-void

    .line 342
    :cond_0
    sget-object v3, Lcom/samsung/musicplus/library/audio/SoundAlive;->CLASSNAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setAudioEffect soundEffect is : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 343
    invoke-virtual {p0}, Landroid/media/MediaPlayer;->newRequest()Landroid/os/Parcel;

    move-result-object v2

    .line 344
    .local v2, "request":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 346
    .local v1, "reply":Landroid/os/Parcel;
    const/16 v3, 0x10

    :try_start_0
    invoke-virtual {v2, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 347
    invoke-virtual {v2, p1}, Landroid/os/Parcel;->writeInt(I)V

    .line 348
    invoke-virtual {p0, v2, v1}, Landroid/media/MediaPlayer;->setSoundAlive(Landroid/os/Parcel;Landroid/os/Parcel;)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 352
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 353
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    goto :goto_0

    .line 349
    :catch_0
    move-exception v0

    .line 350
    .local v0, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v3, Lcom/samsung/musicplus/library/audio/SoundAlive;->CLASSNAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setAudioEffect, we might invoke with error state"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 352
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 353
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    goto :goto_0

    .line 352
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v3

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 353
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v3
.end method

.method public static setAudioEffectByGenre(Landroid/media/MediaPlayer;Ljava/lang/String;)V
    .locals 1
    .param p0, "mp"    # Landroid/media/MediaPlayer;
    .param p1, "genre"    # Ljava/lang/String;

    .prologue
    .line 370
    invoke-static {p1}, Lcom/samsung/musicplus/library/audio/SoundAlive;->getAudioEffect(Ljava/lang/String;)I

    move-result v0

    invoke-static {p0, v0}, Lcom/samsung/musicplus/library/audio/SoundAlive;->setAudioEffect(Landroid/media/MediaPlayer;I)V

    .line 371
    return-void
.end method

.method public static setPlaySpeed(Landroid/media/MediaPlayer;F)V
    .locals 6
    .param p0, "mp"    # Landroid/media/MediaPlayer;
    .param p1, "speed"    # F

    .prologue
    .line 495
    sget-object v3, Lcom/samsung/musicplus/library/audio/SoundAlive;->CLASSNAME:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setPlaySpeed speed value is : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 496
    invoke-virtual {p0}, Landroid/media/MediaPlayer;->newRequest()Landroid/os/Parcel;

    move-result-object v2

    .line 497
    .local v2, "request":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 499
    .local v1, "reply":Landroid/os/Parcel;
    const/16 v3, 0x400

    :try_start_0
    invoke-virtual {v2, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 500
    invoke-virtual {v2, p1}, Landroid/os/Parcel;->writeFloat(F)V

    .line 501
    invoke-virtual {p0, v2, v1}, Landroid/media/MediaPlayer;->setSoundAlive(Landroid/os/Parcel;Landroid/os/Parcel;)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 505
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 506
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 508
    :goto_0
    return-void

    .line 502
    :catch_0
    move-exception v0

    .line 503
    .local v0, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v3, Lcom/samsung/musicplus/library/audio/SoundAlive;->CLASSNAME:Ljava/lang/String;

    const-string v4, "setPlaySpeed, we might invoke with error state"

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 505
    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 506
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    goto :goto_0

    .line 505
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v3

    invoke-virtual {v2}, Landroid/os/Parcel;->recycle()V

    .line 506
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v3
.end method

.method public static setUserAudioEffect(Landroid/media/MediaPlayer;[I)V
    .locals 8
    .param p0, "mp"    # Landroid/media/MediaPlayer;
    .param p1, "userEq"    # [I

    .prologue
    const/4 v6, 0x7

    .line 386
    if-nez p1, :cond_0

    .line 387
    new-array p1, v6, [I

    .line 388
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v6, :cond_0

    .line 389
    const/16 v5, 0xa

    aput v5, p1, v1

    .line 388
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 392
    .end local v1    # "i":I
    :cond_0
    array-length v5, p1

    if-ge v5, v6, :cond_1

    .line 393
    sget-object v5, Lcom/samsung/musicplus/library/audio/SoundAlive;->CLASSNAME:Ljava/lang/String;

    const-string v6, "setUserAudioEffect userEq length is under 7 please check your userEq value again"

    invoke-static {v5, v6}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 425
    :goto_1
    return-void

    .line 397
    :cond_1
    invoke-virtual {p0}, Landroid/media/MediaPlayer;->newRequest()Landroid/os/Parcel;

    move-result-object v4

    .line 398
    .local v4, "request":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v3

    .line 400
    .local v3, "reply":Landroid/os/Parcel;
    const/16 v5, 0x20

    :try_start_0
    invoke-virtual {v4, v5}, Landroid/os/Parcel;->writeInt(I)V

    .line 401
    const/4 v5, 0x0

    aget v5, p1, v5

    invoke-virtual {v4, v5}, Landroid/os/Parcel;->writeInt(I)V

    .line 402
    const/4 v5, 0x1

    aget v5, p1, v5

    invoke-virtual {v4, v5}, Landroid/os/Parcel;->writeInt(I)V

    .line 403
    const/4 v5, 0x2

    aget v5, p1, v5

    invoke-virtual {v4, v5}, Landroid/os/Parcel;->writeInt(I)V

    .line 404
    const/4 v5, 0x3

    aget v5, p1, v5

    invoke-virtual {v4, v5}, Landroid/os/Parcel;->writeInt(I)V

    .line 405
    const/4 v5, 0x4

    aget v5, p1, v5

    invoke-virtual {v4, v5}, Landroid/os/Parcel;->writeInt(I)V

    .line 406
    const/4 v5, 0x5

    aget v5, p1, v5

    invoke-virtual {v4, v5}, Landroid/os/Parcel;->writeInt(I)V

    .line 407
    const/4 v5, 0x6

    aget v5, p1, v5

    invoke-virtual {v4, v5}, Landroid/os/Parcel;->writeInt(I)V

    .line 408
    invoke-virtual {p0, v4, v3}, Landroid/media/MediaPlayer;->setSoundAlive(Landroid/os/Parcel;Landroid/os/Parcel;)I

    .line 409
    sget v5, Lcom/samsung/musicplus/library/Build$VERSION;->CURRENT:I

    const v6, 0x1877a

    if-ne v5, v6, :cond_3

    .line 410
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v5, "setUserAudioEffect userEq value is : "

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 411
    .local v2, "log":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    array-length v5, p1

    if-ge v1, v5, :cond_2

    .line 412
    const-string v5, "\nuserEq["

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 413
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 414
    const-string v5, "] : "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 415
    aget v5, p1, v1

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 411
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 417
    :cond_2
    sget-object v5, Lcom/samsung/musicplus/library/audio/SoundAlive;->CLASSNAME:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 422
    .end local v1    # "i":I
    .end local v2    # "log":Ljava/lang/StringBuilder;
    :cond_3
    invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V

    .line 423
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    goto :goto_1

    .line 419
    :catch_0
    move-exception v0

    .line 420
    .local v0, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v5, Lcom/samsung/musicplus/library/audio/SoundAlive;->CLASSNAME:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "setUserAudioEffect, we might invoke with error state"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 422
    invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V

    .line 423
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    goto/16 :goto_1

    .line 422
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v5

    invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V

    .line 423
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    throw v5
.end method

.method public static setUserExtendAudioEffect(Landroid/media/MediaPlayer;[I)V
    .locals 8
    .param p0, "mp"    # Landroid/media/MediaPlayer;
    .param p1, "effect"    # [I

    .prologue
    const/4 v5, 0x0

    const/4 v6, 0x5

    .line 445
    if-nez p1, :cond_0

    .line 446
    new-array p1, v6, [I

    .line 447
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v6, :cond_0

    .line 448
    aput v5, p1, v1

    .line 447
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 451
    .end local v1    # "i":I
    :cond_0
    array-length v5, p1

    if-ge v5, v6, :cond_1

    .line 452
    sget-object v5, Lcom/samsung/musicplus/library/audio/SoundAlive;->CLASSNAME:Ljava/lang/String;

    const-string v6, "setUserExtendAudioEffect effect length is under 5 please check your effect value again"

    invoke-static {v5, v6}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 483
    :goto_1
    return-void

    .line 456
    :cond_1
    invoke-virtual {p0}, Landroid/media/MediaPlayer;->newRequest()Landroid/os/Parcel;

    move-result-object v4

    .line 457
    .local v4, "request":Landroid/os/Parcel;
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v3

    .line 459
    .local v3, "reply":Landroid/os/Parcel;
    const/16 v5, 0x800

    :try_start_0
    invoke-virtual {v4, v5}, Landroid/os/Parcel;->writeInt(I)V

    .line 460
    const/4 v5, 0x0

    aget v5, p1, v5

    invoke-virtual {v4, v5}, Landroid/os/Parcel;->writeInt(I)V

    .line 461
    const/4 v5, 0x1

    aget v5, p1, v5

    invoke-virtual {v4, v5}, Landroid/os/Parcel;->writeInt(I)V

    .line 462
    const/4 v5, 0x2

    aget v5, p1, v5

    invoke-virtual {v4, v5}, Landroid/os/Parcel;->writeInt(I)V

    .line 463
    const/4 v5, 0x3

    aget v5, p1, v5

    invoke-virtual {v4, v5}, Landroid/os/Parcel;->writeInt(I)V

    .line 464
    const/4 v5, 0x4

    aget v5, p1, v5

    invoke-virtual {v4, v5}, Landroid/os/Parcel;->writeInt(I)V

    .line 465
    invoke-virtual {p0, v4, v3}, Landroid/media/MediaPlayer;->setSoundAlive(Landroid/os/Parcel;Landroid/os/Parcel;)I

    .line 467
    sget v5, Lcom/samsung/musicplus/library/Build$VERSION;->CURRENT:I

    const v6, 0x1877a

    if-ne v5, v6, :cond_3

    .line 468
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v5, "setUserExtendAudioEffect effect value is : "

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 469
    .local v2, "log":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_2
    array-length v5, p1

    if-ge v1, v5, :cond_2

    .line 470
    const-string v5, "\neffect["

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 471
    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 472
    const-string v5, "] : "

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 473
    aget v5, p1, v1

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 469
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 475
    :cond_2
    sget-object v5, Lcom/samsung/musicplus/library/audio/SoundAlive;->CLASSNAME:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 480
    .end local v1    # "i":I
    .end local v2    # "log":Ljava/lang/StringBuilder;
    :cond_3
    invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V

    .line 481
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    goto :goto_1

    .line 477
    :catch_0
    move-exception v0

    .line 478
    .local v0, "e":Ljava/lang/RuntimeException;
    :try_start_1
    sget-object v5, Lcom/samsung/musicplus/library/audio/SoundAlive;->CLASSNAME:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "setUserExtendAudioEffect, we might invoke with error state"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 480
    invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V

    .line 481
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    goto/16 :goto_1

    .line 480
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :catchall_0
    move-exception v5

    invoke-virtual {v4}, Landroid/os/Parcel;->recycle()V

    .line 481
    invoke-virtual {v3}, Landroid/os/Parcel;->recycle()V

    throw v5
.end method
