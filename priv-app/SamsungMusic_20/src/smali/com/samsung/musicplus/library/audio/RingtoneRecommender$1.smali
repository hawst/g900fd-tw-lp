.class Lcom/samsung/musicplus/library/audio/RingtoneRecommender$1;
.super Ljava/lang/Thread;
.source "RingtoneRecommender.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/library/audio/RingtoneRecommender;->extract()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/library/audio/RingtoneRecommender;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/library/audio/RingtoneRecommender;Ljava/lang/String;)V
    .locals 0
    .param p2, "x0"    # Ljava/lang/String;

    .prologue
    .line 240
    iput-object p1, p0, Lcom/samsung/musicplus/library/audio/RingtoneRecommender$1;->this$0:Lcom/samsung/musicplus/library/audio/RingtoneRecommender;

    invoke-direct {p0, p2}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 243
    const/4 v2, -0x1

    .line 244
    .local v2, "oldstatus":I
    const/4 v3, -0x1

    .line 245
    .local v3, "status":I
    :cond_0
    :goto_0
    iget-object v4, p0, Lcom/samsung/musicplus/library/audio/RingtoneRecommender$1;->this$0:Lcom/samsung/musicplus/library/audio/RingtoneRecommender;

    # getter for: Lcom/samsung/musicplus/library/audio/RingtoneRecommender;->mIsOpen:Z
    invoke-static {v4}, Lcom/samsung/musicplus/library/audio/RingtoneRecommender;->access$000(Lcom/samsung/musicplus/library/audio/RingtoneRecommender;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 247
    const-wide/16 v4, 0xc8

    :try_start_0
    invoke-static {v4, v5}, Lcom/samsung/musicplus/library/audio/RingtoneRecommender$1;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 251
    :goto_1
    iget-object v4, p0, Lcom/samsung/musicplus/library/audio/RingtoneRecommender$1;->this$0:Lcom/samsung/musicplus/library/audio/RingtoneRecommender;

    # getter for: Lcom/samsung/musicplus/library/audio/RingtoneRecommender;->mRecommender:Lcom/samsung/audio/Smat;
    invoke-static {v4}, Lcom/samsung/musicplus/library/audio/RingtoneRecommender;->access$100(Lcom/samsung/musicplus/library/audio/RingtoneRecommender;)Lcom/samsung/audio/Smat;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/audio/Smat;->get_stat()I

    move-result v3

    .line 252
    # getter for: Lcom/samsung/musicplus/library/audio/RingtoneRecommender;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/library/audio/RingtoneRecommender;->access$200()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "extract() and background thread\'s run() status : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 254
    if-eq v2, v3, :cond_0

    .line 255
    move v2, v3

    .line 256
    iget-object v4, p0, Lcom/samsung/musicplus/library/audio/RingtoneRecommender$1;->this$0:Lcom/samsung/musicplus/library/audio/RingtoneRecommender;

    # getter for: Lcom/samsung/musicplus/library/audio/RingtoneRecommender;->mListener:Lcom/samsung/musicplus/library/audio/RingtoneRecommender$OnHighlightResultListener;
    invoke-static {v4}, Lcom/samsung/musicplus/library/audio/RingtoneRecommender;->access$300(Lcom/samsung/musicplus/library/audio/RingtoneRecommender;)Lcom/samsung/musicplus/library/audio/RingtoneRecommender$OnHighlightResultListener;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 257
    const/4 v1, 0x0

    .line 258
    .local v1, "offset":I
    packed-switch v3, :pswitch_data_0

    goto :goto_0

    .line 260
    :pswitch_0
    iget-object v4, p0, Lcom/samsung/musicplus/library/audio/RingtoneRecommender$1;->this$0:Lcom/samsung/musicplus/library/audio/RingtoneRecommender;

    # getter for: Lcom/samsung/musicplus/library/audio/RingtoneRecommender;->mRecommender:Lcom/samsung/audio/Smat;
    invoke-static {v4}, Lcom/samsung/musicplus/library/audio/RingtoneRecommender;->access$100(Lcom/samsung/musicplus/library/audio/RingtoneRecommender;)Lcom/samsung/audio/Smat;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/audio/Smat;->get_info()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    div-long/2addr v4, v6

    long-to-int v4, v4

    mul-int/lit16 v1, v4, 0x3e8

    .line 264
    :pswitch_1
    # getter for: Lcom/samsung/musicplus/library/audio/RingtoneRecommender;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/library/audio/RingtoneRecommender;->access$200()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "extract()  status :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " position : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 266
    iget-object v4, p0, Lcom/samsung/musicplus/library/audio/RingtoneRecommender$1;->this$0:Lcom/samsung/musicplus/library/audio/RingtoneRecommender;

    # getter for: Lcom/samsung/musicplus/library/audio/RingtoneRecommender;->mListener:Lcom/samsung/musicplus/library/audio/RingtoneRecommender$OnHighlightResultListener;
    invoke-static {v4}, Lcom/samsung/musicplus/library/audio/RingtoneRecommender;->access$300(Lcom/samsung/musicplus/library/audio/RingtoneRecommender;)Lcom/samsung/musicplus/library/audio/RingtoneRecommender$OnHighlightResultListener;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 269
    iget-object v4, p0, Lcom/samsung/musicplus/library/audio/RingtoneRecommender$1;->this$0:Lcom/samsung/musicplus/library/audio/RingtoneRecommender;

    # getter for: Lcom/samsung/musicplus/library/audio/RingtoneRecommender;->mListener:Lcom/samsung/musicplus/library/audio/RingtoneRecommender$OnHighlightResultListener;
    invoke-static {v4}, Lcom/samsung/musicplus/library/audio/RingtoneRecommender;->access$300(Lcom/samsung/musicplus/library/audio/RingtoneRecommender;)Lcom/samsung/musicplus/library/audio/RingtoneRecommender$OnHighlightResultListener;

    move-result-object v4

    invoke-interface {v4, v3, v1}, Lcom/samsung/musicplus/library/audio/RingtoneRecommender$OnHighlightResultListener;->onResult(II)V

    .line 270
    iget-object v4, p0, Lcom/samsung/musicplus/library/audio/RingtoneRecommender$1;->this$0:Lcom/samsung/musicplus/library/audio/RingtoneRecommender;

    invoke-virtual {v4}, Lcom/samsung/musicplus/library/audio/RingtoneRecommender;->close()Z

    goto/16 :goto_0

    .line 248
    .end local v1    # "offset":I
    :catch_0
    move-exception v0

    .line 249
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto/16 :goto_1

    .line 279
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_1
    return-void

    .line 258
    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
