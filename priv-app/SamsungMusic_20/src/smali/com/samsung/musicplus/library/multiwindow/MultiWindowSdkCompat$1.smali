.class Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat$1;
.super Ljava/lang/Object;
.source "MultiWindowSdkCompat.java"

# interfaces
.implements Lcom/samsung/android/sdk/multiwindow/SMultiWindowActivity$StateChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat;->setStateChangeListener(Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat$OnMultiWindowChangeListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat;)V
    .locals 0

    .prologue
    .line 158
    iput-object p1, p0, Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat$1;->this$0:Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onModeChanged(Z)V
    .locals 2
    .param p1, "isMultiWindow"    # Z

    .prologue
    .line 178
    iget-object v0, p0, Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat$1;->this$0:Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat;

    # getter for: Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat;->mListener:Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat$OnMultiWindowChangeListener;
    invoke-static {v0}, Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat;->access$000(Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat;)Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat$OnMultiWindowChangeListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 179
    const-string v0, "MusicUi_MultiWindow"

    const-string v1, "onModeChanged()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 180
    iget-object v0, p0, Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat$1;->this$0:Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat;

    # getter for: Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat;->mListener:Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat$OnMultiWindowChangeListener;
    invoke-static {v0}, Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat;->access$000(Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat;)Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat$OnMultiWindowChangeListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat$OnMultiWindowChangeListener;->onModeChanged(Z)V

    .line 182
    :cond_0
    return-void
.end method

.method public onSizeChanged(Landroid/graphics/Rect;)V
    .locals 2
    .param p1, "rectInfo"    # Landroid/graphics/Rect;

    .prologue
    .line 170
    iget-object v0, p0, Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat$1;->this$0:Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat;

    # getter for: Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat;->mListener:Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat$OnMultiWindowChangeListener;
    invoke-static {v0}, Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat;->access$000(Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat;)Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat$OnMultiWindowChangeListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 171
    const-string v0, "MusicUi_MultiWindow"

    const-string v1, "onSizeChanged()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 172
    iget-object v0, p0, Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat$1;->this$0:Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat;

    # getter for: Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat;->mListener:Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat$OnMultiWindowChangeListener;
    invoke-static {v0}, Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat;->access$000(Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat;)Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat$OnMultiWindowChangeListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat$OnMultiWindowChangeListener;->onSizeChanged(Landroid/graphics/Rect;)V

    .line 174
    :cond_0
    return-void
.end method

.method public onZoneChanged(I)V
    .locals 2
    .param p1, "zoneInfo"    # I

    .prologue
    .line 162
    iget-object v0, p0, Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat$1;->this$0:Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat;

    # getter for: Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat;->mListener:Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat$OnMultiWindowChangeListener;
    invoke-static {v0}, Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat;->access$000(Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat;)Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat$OnMultiWindowChangeListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 163
    const-string v0, "MusicUi_MultiWindow"

    const-string v1, "onZoneChanged()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 164
    iget-object v0, p0, Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat$1;->this$0:Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat;

    # getter for: Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat;->mListener:Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat$OnMultiWindowChangeListener;
    invoke-static {v0}, Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat;->access$000(Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat;)Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat$OnMultiWindowChangeListener;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/samsung/musicplus/library/multiwindow/MultiWindowSdkCompat$OnMultiWindowChangeListener;->onZoneChanged(I)V

    .line 166
    :cond_0
    return-void
.end method
