.class Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager$1;
.super Ljava/lang/Object;
.source "KnoxPersonalModeManager.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;->startContainerService(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;

.field final synthetic val$context:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 237
    iput-object p1, p0, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager$1;->this$0:Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;

    iput-object p2, p0, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager$1;->val$context:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 247
    const-string v1, "KnoxModeManager"

    const-string v2, "ContainerInstallerManager service is connected"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 249
    iget-object v1, p0, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager$1;->this$0:Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;

    const/4 v2, 0x1

    # setter for: Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;->mConnected:Z
    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;->access$002(Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;Z)Z

    .line 250
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.knox.knoxappsinstaller.installservice.install"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 251
    .local v0, "intent":Landroid/content/Intent;
    iget-object v1, p0, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager$1;->val$context:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 252
    return-void
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 241
    const-string v0, "KnoxModeManager"

    const-string v1, "ContainerInstallerManager service is disconnected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 242
    iget-object v0, p0, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager$1;->this$0:Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;

    const/4 v1, 0x0

    # setter for: Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;->mConnected:Z
    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;->access$002(Lcom/samsung/musicplus/library/storage/KnoxPersonalModeManager;Z)Z

    .line 243
    return-void
.end method
