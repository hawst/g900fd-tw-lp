.class public Lcom/samsung/musicplus/library/hardware/SviewCoverManager;
.super Ljava/lang/Object;
.source "SviewCoverManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/library/hardware/SviewCoverManager$OnCoverStateChangeListener;,
        Lcom/samsung/musicplus/library/hardware/SviewCoverManager$ScoverError;,
        Lcom/samsung/musicplus/library/hardware/SviewCoverManager$ScoverColor;,
        Lcom/samsung/musicplus/library/hardware/SviewCoverManager$ScoverType;,
        Lcom/samsung/musicplus/library/hardware/SviewCoverManager$ScoverSwitchState;,
        Lcom/samsung/musicplus/library/hardware/SviewCoverManager$ScoverMode;
    }
.end annotation


# static fields
.field private static final CLASSNAME:Ljava/lang/String;

.field public static final ERROR_CODE:I = -0x1

.field public static final SCOVER_FALSE_VALUE:I = 0x0

.field public static final SCOVER_TRUE_VALUE:I = 0x1


# instance fields
.field private mScover:Lcom/samsung/android/sdk/cover/Scover;

.field private mScoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

.field private mScoverState:Lcom/samsung/android/sdk/cover/ScoverState;

.field private mScoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const-class v0, Lcom/samsung/musicplus/library/hardware/SviewCoverManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/library/hardware/SviewCoverManager;->CLASSNAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/samsung/musicplus/library/hardware/SviewCoverManager$OnCoverStateChangeListener;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/samsung/musicplus/library/hardware/SviewCoverManager$OnCoverStateChangeListener;

    .prologue
    .line 390
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 391
    invoke-direct {p0, p1, p2}, Lcom/samsung/musicplus/library/hardware/SviewCoverManager;->initializeScover(Landroid/content/Context;Lcom/samsung/musicplus/library/hardware/SviewCoverManager$OnCoverStateChangeListener;)V

    .line 392
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/samsung/musicplus/library/hardware/SviewCoverManager;->CLASSNAME:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$102(Lcom/samsung/musicplus/library/hardware/SviewCoverManager;Lcom/samsung/android/sdk/cover/ScoverState;)Lcom/samsung/android/sdk/cover/ScoverState;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/library/hardware/SviewCoverManager;
    .param p1, "x1"    # Lcom/samsung/android/sdk/cover/ScoverState;

    .prologue
    .line 41
    iput-object p1, p0, Lcom/samsung/musicplus/library/hardware/SviewCoverManager;->mScoverState:Lcom/samsung/android/sdk/cover/ScoverState;

    return-object p1
.end method

.method private initializeScover(Landroid/content/Context;Lcom/samsung/musicplus/library/hardware/SviewCoverManager$OnCoverStateChangeListener;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "listener"    # Lcom/samsung/musicplus/library/hardware/SviewCoverManager$OnCoverStateChangeListener;

    .prologue
    .line 408
    new-instance v1, Lcom/samsung/android/sdk/cover/Scover;

    invoke-direct {v1}, Lcom/samsung/android/sdk/cover/Scover;-><init>()V

    iput-object v1, p0, Lcom/samsung/musicplus/library/hardware/SviewCoverManager;->mScover:Lcom/samsung/android/sdk/cover/Scover;

    .line 411
    :try_start_0
    iget-object v1, p0, Lcom/samsung/musicplus/library/hardware/SviewCoverManager;->mScover:Lcom/samsung/android/sdk/cover/Scover;

    invoke-virtual {v1, p1}, Lcom/samsung/android/sdk/cover/Scover;->initialize(Landroid/content/Context;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/samsung/android/sdk/SsdkUnsupportedException; {:try_start_0 .. :try_end_0} :catch_1

    .line 418
    :goto_0
    new-instance v1, Lcom/samsung/android/sdk/cover/ScoverManager;

    invoke-direct {v1, p1}, Lcom/samsung/android/sdk/cover/ScoverManager;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/musicplus/library/hardware/SviewCoverManager;->mScoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    .line 419
    new-instance v1, Lcom/samsung/musicplus/library/hardware/SviewCoverManager$1;

    invoke-direct {v1, p0, p2}, Lcom/samsung/musicplus/library/hardware/SviewCoverManager$1;-><init>(Lcom/samsung/musicplus/library/hardware/SviewCoverManager;Lcom/samsung/musicplus/library/hardware/SviewCoverManager$OnCoverStateChangeListener;)V

    iput-object v1, p0, Lcom/samsung/musicplus/library/hardware/SviewCoverManager;->mScoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

    .line 427
    return-void

    .line 412
    :catch_0
    move-exception v0

    .line 413
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    sget-object v1, Lcom/samsung/musicplus/library/hardware/SviewCoverManager;->CLASSNAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Scover initailize failed : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 414
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 415
    .local v0, "e":Lcom/samsung/android/sdk/SsdkUnsupportedException;
    sget-object v1, Lcom/samsung/musicplus/library/hardware/SviewCoverManager;->CLASSNAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Scover initialize failed : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/samsung/android/sdk/SsdkUnsupportedException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public getCurrentCoverState()V
    .locals 1

    .prologue
    .line 479
    iget-object v0, p0, Lcom/samsung/musicplus/library/hardware/SviewCoverManager;->mScoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    if-eqz v0, :cond_0

    .line 480
    iget-object v0, p0, Lcom/samsung/musicplus/library/hardware/SviewCoverManager;->mScoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/cover/ScoverManager;->getCoverState()Lcom/samsung/android/sdk/cover/ScoverState;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/library/hardware/SviewCoverManager;->mScoverState:Lcom/samsung/android/sdk/cover/ScoverState;

    .line 482
    :cond_0
    return-void
.end method

.method public getScoverColor()I
    .locals 2

    .prologue
    .line 533
    iget-object v0, p0, Lcom/samsung/musicplus/library/hardware/SviewCoverManager;->mScoverState:Lcom/samsung/android/sdk/cover/ScoverState;

    if-nez v0, :cond_0

    .line 534
    sget-object v0, Lcom/samsung/musicplus/library/hardware/SviewCoverManager;->CLASSNAME:Ljava/lang/String;

    const-string v1, "getScoverColor : mCoverState is null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 535
    const/4 v0, -0x1

    .line 538
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/library/hardware/SviewCoverManager;->mScoverState:Lcom/samsung/android/sdk/cover/ScoverState;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/cover/ScoverState;->getColor()I

    move-result v0

    goto :goto_0
.end method

.method public getScoverHeightPixel()I
    .locals 2

    .prologue
    .line 569
    iget-object v0, p0, Lcom/samsung/musicplus/library/hardware/SviewCoverManager;->mScoverState:Lcom/samsung/android/sdk/cover/ScoverState;

    if-nez v0, :cond_0

    .line 570
    sget-object v0, Lcom/samsung/musicplus/library/hardware/SviewCoverManager;->CLASSNAME:Ljava/lang/String;

    const-string v1, "getScoverHeightPixel : mCoverState is null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 571
    const/4 v0, -0x1

    .line 574
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/library/hardware/SviewCoverManager;->mScoverState:Lcom/samsung/android/sdk/cover/ScoverState;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/cover/ScoverState;->getWindowHeight()I

    move-result v0

    goto :goto_0
.end method

.method public getScoverSwitchState()I
    .locals 2

    .prologue
    .line 496
    iget-object v0, p0, Lcom/samsung/musicplus/library/hardware/SviewCoverManager;->mScoverState:Lcom/samsung/android/sdk/cover/ScoverState;

    if-nez v0, :cond_0

    .line 497
    sget-object v0, Lcom/samsung/musicplus/library/hardware/SviewCoverManager;->CLASSNAME:Ljava/lang/String;

    const-string v1, "getScoverSwitchState : mCoverState is null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 498
    const/4 v0, -0x1

    .line 501
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/library/hardware/SviewCoverManager;->mScoverState:Lcom/samsung/android/sdk/cover/ScoverState;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/cover/ScoverState;->getSwitchState()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getScoverType()I
    .locals 2

    .prologue
    .line 515
    iget-object v0, p0, Lcom/samsung/musicplus/library/hardware/SviewCoverManager;->mScoverState:Lcom/samsung/android/sdk/cover/ScoverState;

    if-nez v0, :cond_0

    .line 516
    sget-object v0, Lcom/samsung/musicplus/library/hardware/SviewCoverManager;->CLASSNAME:Ljava/lang/String;

    const-string v1, "getScoverType : mCoverState is null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 517
    const/4 v0, -0x1

    .line 520
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/library/hardware/SviewCoverManager;->mScoverState:Lcom/samsung/android/sdk/cover/ScoverState;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/cover/ScoverState;->getType()I

    move-result v0

    goto :goto_0
.end method

.method public getScoverVersionCode()I
    .locals 2

    .prologue
    .line 587
    iget-object v0, p0, Lcom/samsung/musicplus/library/hardware/SviewCoverManager;->mScover:Lcom/samsung/android/sdk/cover/Scover;

    if-nez v0, :cond_0

    .line 588
    sget-object v0, Lcom/samsung/musicplus/library/hardware/SviewCoverManager;->CLASSNAME:Ljava/lang/String;

    const-string v1, "getScoverVersionCode : mScover is null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 589
    const/4 v0, -0x1

    .line 592
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/library/hardware/SviewCoverManager;->mScover:Lcom/samsung/android/sdk/cover/Scover;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/cover/Scover;->getVersionCode()I

    move-result v0

    goto :goto_0
.end method

.method public getScoverVersionName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 605
    iget-object v0, p0, Lcom/samsung/musicplus/library/hardware/SviewCoverManager;->mScover:Lcom/samsung/android/sdk/cover/Scover;

    if-nez v0, :cond_0

    .line 606
    sget-object v0, Lcom/samsung/musicplus/library/hardware/SviewCoverManager;->CLASSNAME:Ljava/lang/String;

    const-string v1, "getScoverVersionName : mScover is null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 607
    const/4 v0, 0x0

    .line 610
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/library/hardware/SviewCoverManager;->mScover:Lcom/samsung/android/sdk/cover/Scover;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/cover/Scover;->getVersionName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getScoverWidthPixel()I
    .locals 2

    .prologue
    .line 551
    iget-object v0, p0, Lcom/samsung/musicplus/library/hardware/SviewCoverManager;->mScoverState:Lcom/samsung/android/sdk/cover/ScoverState;

    if-nez v0, :cond_0

    .line 552
    sget-object v0, Lcom/samsung/musicplus/library/hardware/SviewCoverManager;->CLASSNAME:Ljava/lang/String;

    const-string v1, "getScoverWidthPixel : mCoverState is null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 553
    const/4 v0, -0x1

    .line 556
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/library/hardware/SviewCoverManager;->mScoverState:Lcom/samsung/android/sdk/cover/ScoverState;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/cover/ScoverState;->getWindowWidth()I

    move-result v0

    goto :goto_0
.end method

.method public isFeatureEnabled(I)I
    .locals 2
    .param p1, "type"    # I

    .prologue
    .line 625
    iget-object v0, p0, Lcom/samsung/musicplus/library/hardware/SviewCoverManager;->mScover:Lcom/samsung/android/sdk/cover/Scover;

    if-nez v0, :cond_0

    .line 626
    sget-object v0, Lcom/samsung/musicplus/library/hardware/SviewCoverManager;->CLASSNAME:Ljava/lang/String;

    const-string v1, "isFeatureEnabled : mScover is null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 627
    const/4 v0, -0x1

    .line 630
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/library/hardware/SviewCoverManager;->mScover:Lcom/samsung/android/sdk/cover/Scover;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/cover/Scover;->isFeatureEnabled(I)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public registerListener()V
    .locals 2

    .prologue
    .line 437
    iget-object v0, p0, Lcom/samsung/musicplus/library/hardware/SviewCoverManager;->mScoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    if-eqz v0, :cond_0

    .line 438
    iget-object v0, p0, Lcom/samsung/musicplus/library/hardware/SviewCoverManager;->mScoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    iget-object v1, p0, Lcom/samsung/musicplus/library/hardware/SviewCoverManager;->mScoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/cover/ScoverManager;->registerListener(Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;)V

    .line 440
    :cond_0
    return-void
.end method

.method public setCoverModeToWindow(Landroid/view/Window;I)V
    .locals 1
    .param p1, "window"    # Landroid/view/Window;
    .param p2, "coverMode"    # I

    .prologue
    .line 466
    iget-object v0, p0, Lcom/samsung/musicplus/library/hardware/SviewCoverManager;->mScoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    if-eqz v0, :cond_0

    .line 467
    iget-object v0, p0, Lcom/samsung/musicplus/library/hardware/SviewCoverManager;->mScoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/sdk/cover/ScoverManager;->setCoverModeToWindow(Landroid/view/Window;I)V

    .line 469
    :cond_0
    return-void
.end method

.method public unregisterListener()V
    .locals 2

    .prologue
    .line 450
    iget-object v0, p0, Lcom/samsung/musicplus/library/hardware/SviewCoverManager;->mScoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    if-eqz v0, :cond_0

    .line 451
    iget-object v0, p0, Lcom/samsung/musicplus/library/hardware/SviewCoverManager;->mScoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    iget-object v1, p0, Lcom/samsung/musicplus/library/hardware/SviewCoverManager;->mScoverStateListener:Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/cover/ScoverManager;->unregisterListener(Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;)V

    .line 453
    :cond_0
    return-void
.end method
