.class public interface abstract Lcom/samsung/musicplus/library/hardware/SviewCoverManager$ScoverError;
.super Ljava/lang/Object;
.source "SviewCoverManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/library/hardware/SviewCoverManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "ScoverError"
.end annotation


# static fields
.field public static final DEVICE_NOT_SUPPORTED:I = 0x1

.field public static final SDK_VERSION_MISMATCH:I = 0x2

.field public static final VENDOR_NOT_SUPPORTED:I
