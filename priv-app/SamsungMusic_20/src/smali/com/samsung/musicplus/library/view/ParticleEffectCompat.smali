.class public Lcom/samsung/musicplus/library/view/ParticleEffectCompat;
.super Ljava/lang/Object;
.source "ParticleEffectCompat.java"


# static fields
.field private static final sParticleParams:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 84
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/samsung/musicplus/library/view/ParticleEffectCompat;->sParticleParams:Ljava/util/HashMap;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static addDots(Landroid/view/View;IFFI)V
    .locals 3
    .param p0, "particle"    # Landroid/view/View;
    .param p1, "amount"    # I
    .param p2, "x"    # F
    .param p3, "y"    # F
    .param p4, "color"    # I

    .prologue
    .line 52
    sget-object v0, Lcom/samsung/musicplus/library/view/ParticleEffectCompat;->sParticleParams:Ljava/util/HashMap;

    const-string v1, "Amount"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    sget-object v0, Lcom/samsung/musicplus/library/view/ParticleEffectCompat;->sParticleParams:Ljava/util/HashMap;

    const-string v1, "X"

    invoke-static {p2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 54
    sget-object v0, Lcom/samsung/musicplus/library/view/ParticleEffectCompat;->sParticleParams:Ljava/util/HashMap;

    const-string v1, "Y"

    invoke-static {p3}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    sget-object v0, Lcom/samsung/musicplus/library/view/ParticleEffectCompat;->sParticleParams:Ljava/util/HashMap;

    const-string v1, "Color"

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    check-cast p0, Lcom/samsung/android/visualeffect/EffectView;

    .end local p0    # "particle":Landroid/view/View;
    const-string v0, "touch"

    sget-object v1, Lcom/samsung/musicplus/library/view/ParticleEffectCompat;->sParticleParams:Ljava/util/HashMap;

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/visualeffect/EffectView;->onCommand(Ljava/lang/String;Ljava/util/HashMap;)V

    .line 57
    return-void
.end method

.method public static clearEffect(Landroid/view/View;)V
    .locals 2
    .param p0, "particle"    # Landroid/view/View;

    .prologue
    .line 68
    check-cast p0, Lcom/samsung/android/visualeffect/EffectView;

    .end local p0    # "particle":Landroid/view/View;
    const-string v0, "clear"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/samsung/android/visualeffect/EffectView;->onCommand(Ljava/lang/String;Ljava/util/HashMap;)V

    .line 69
    return-void
.end method

.method public static destroy(Landroid/view/View;)V
    .locals 0
    .param p0, "particle"    # Landroid/view/View;

    .prologue
    .line 80
    invoke-static {p0}, Lcom/samsung/musicplus/library/view/ParticleEffectCompat;->clearEffect(Landroid/view/View;)V

    .line 81
    return-void
.end method

.method public static getInstance(Landroid/content/Context;)Landroid/view/View;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 34
    new-instance v0, Lcom/samsung/android/visualeffect/EffectView;

    invoke-direct {v0, p0}, Lcom/samsung/android/visualeffect/EffectView;-><init>(Landroid/content/Context;)V

    .line 35
    .local v0, "res":Lcom/samsung/android/visualeffect/EffectView;
    const-string v1, "particle"

    invoke-virtual {v0, v1}, Lcom/samsung/android/visualeffect/EffectView;->setEffect(Ljava/lang/String;)V

    .line 36
    return-object v0
.end method
