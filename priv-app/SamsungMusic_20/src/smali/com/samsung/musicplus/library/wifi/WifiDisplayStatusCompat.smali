.class public Lcom/samsung/musicplus/library/wifi/WifiDisplayStatusCompat;
.super Ljava/lang/Object;
.source "WifiDisplayStatusCompat.java"


# static fields
.field public static final CONN_STATE_CHANGEPLAYER_MUSIC:I = 0x8

.field public static final DISPLAY_STATE_CONNECTED:I = 0x2

.field public static final DISPLAY_STATE_CONNECTING:I = 0x1

.field public static final SCAN_STATE_NOT_SCANNING:I = 0x0

.field public static final SCAN_STATE_SCANNING:I = 0x1


# instance fields
.field private mStatus:Landroid/hardware/display/WifiDisplayStatus;


# direct methods
.method public constructor <init>(Landroid/os/Parcelable;)V
    .locals 1
    .param p1, "status"    # Landroid/os/Parcelable;

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    instance-of v0, p1, Landroid/hardware/display/WifiDisplayStatus;

    if-eqz v0, :cond_0

    .line 60
    check-cast p1, Landroid/hardware/display/WifiDisplayStatus;

    .end local p1    # "status":Landroid/os/Parcelable;
    iput-object p1, p0, Lcom/samsung/musicplus/library/wifi/WifiDisplayStatusCompat;->mStatus:Landroid/hardware/display/WifiDisplayStatus;

    .line 62
    :cond_0
    return-void
.end method

.method public static getExtraWifiDisplayStatusCompat(Landroid/content/Intent;)Lcom/samsung/musicplus/library/wifi/WifiDisplayStatusCompat;
    .locals 2
    .param p0, "i"    # Landroid/content/Intent;

    .prologue
    .line 154
    if-nez p0, :cond_0

    .line 155
    const/4 v0, 0x0

    .line 158
    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lcom/samsung/musicplus/library/wifi/WifiDisplayStatusCompat;

    const-string v0, "android.hardware.display.extra.WIFI_DISPLAY_STATUS"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/hardware/display/WifiDisplayStatus;

    invoke-direct {v1, v0}, Lcom/samsung/musicplus/library/wifi/WifiDisplayStatusCompat;-><init>(Landroid/os/Parcelable;)V

    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public getActiveDisplay()Lcom/samsung/musicplus/library/wifi/WifiDisplayCompat;
    .locals 2

    .prologue
    .line 141
    iget-object v0, p0, Lcom/samsung/musicplus/library/wifi/WifiDisplayStatusCompat;->mStatus:Landroid/hardware/display/WifiDisplayStatus;

    if-nez v0, :cond_0

    .line 142
    const/4 v0, 0x0

    .line 145
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/samsung/musicplus/library/wifi/WifiDisplayCompat;

    iget-object v1, p0, Lcom/samsung/musicplus/library/wifi/WifiDisplayStatusCompat;->mStatus:Landroid/hardware/display/WifiDisplayStatus;

    invoke-virtual {v1}, Landroid/hardware/display/WifiDisplayStatus;->getActiveDisplay()Landroid/hardware/display/WifiDisplay;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/musicplus/library/wifi/WifiDisplayCompat;-><init>(Landroid/os/Parcelable;)V

    goto :goto_0
.end method

.method public getActiveDisplayState()I
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/samsung/musicplus/library/wifi/WifiDisplayStatusCompat;->mStatus:Landroid/hardware/display/WifiDisplayStatus;

    if-nez v0, :cond_0

    .line 92
    const/4 v0, -0x1

    .line 94
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/library/wifi/WifiDisplayStatusCompat;->mStatus:Landroid/hardware/display/WifiDisplayStatus;

    invoke-virtual {v0}, Landroid/hardware/display/WifiDisplayStatus;->getActiveDisplayState()I

    move-result v0

    goto :goto_0
.end method

.method public getAvailableDisplays()[Lcom/samsung/musicplus/library/wifi/WifiDisplayCompat;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 70
    iget-object v3, p0, Lcom/samsung/musicplus/library/wifi/WifiDisplayStatusCompat;->mStatus:Landroid/hardware/display/WifiDisplayStatus;

    if-nez v3, :cond_1

    .line 82
    :cond_0
    return-object v1

    .line 73
    :cond_1
    iget-object v3, p0, Lcom/samsung/musicplus/library/wifi/WifiDisplayStatusCompat;->mStatus:Landroid/hardware/display/WifiDisplayStatus;

    invoke-virtual {v3}, Landroid/hardware/display/WifiDisplayStatus;->getDisplays()[Landroid/hardware/display/WifiDisplay;

    move-result-object v0

    .line 74
    .local v0, "d":[Landroid/hardware/display/WifiDisplay;
    if-eqz v0, :cond_0

    .line 78
    array-length v3, v0

    new-array v1, v3, [Lcom/samsung/musicplus/library/wifi/WifiDisplayCompat;

    .line 79
    .local v1, "d2":[Lcom/samsung/musicplus/library/wifi/WifiDisplayCompat;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v3, v0

    if-ge v2, v3, :cond_0

    .line 80
    new-instance v3, Lcom/samsung/musicplus/library/wifi/WifiDisplayCompat;

    aget-object v4, v0, v2

    invoke-direct {v3, v4}, Lcom/samsung/musicplus/library/wifi/WifiDisplayCompat;-><init>(Landroid/os/Parcelable;)V

    aput-object v3, v1, v2

    .line 79
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public getConnectedState()I
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 108
    const/4 v0, -0x1

    return v0
.end method

.method public getScanState()I
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/samsung/musicplus/library/wifi/WifiDisplayStatusCompat;->mStatus:Landroid/hardware/display/WifiDisplayStatus;

    if-nez v0, :cond_0

    .line 171
    const/4 v0, 0x1

    .line 173
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/library/wifi/WifiDisplayStatusCompat;->mStatus:Landroid/hardware/display/WifiDisplayStatus;

    invoke-virtual {v0}, Landroid/hardware/display/WifiDisplayStatus;->getScanState()I

    move-result v0

    goto :goto_0
.end method

.method public isDisplayConnected()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 129
    iget-object v1, p0, Lcom/samsung/musicplus/library/wifi/WifiDisplayStatusCompat;->mStatus:Landroid/hardware/display/WifiDisplayStatus;

    if-nez v1, :cond_1

    .line 132
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/samsung/musicplus/library/wifi/WifiDisplayStatusCompat;->getConnectedState()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isMusicStateConnected()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 117
    iget-object v1, p0, Lcom/samsung/musicplus/library/wifi/WifiDisplayStatusCompat;->mStatus:Landroid/hardware/display/WifiDisplayStatus;

    if-nez v1, :cond_1

    .line 120
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/samsung/musicplus/library/wifi/WifiDisplayStatusCompat;->getConnectedState()I

    move-result v1

    const/16 v2, 0x8

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method
