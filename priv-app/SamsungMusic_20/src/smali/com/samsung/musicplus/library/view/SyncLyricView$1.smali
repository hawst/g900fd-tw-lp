.class Lcom/samsung/musicplus/library/view/SyncLyricView$1;
.super Landroid/content/BroadcastReceiver;
.source "SyncLyricView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/library/view/SyncLyricView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/library/view/SyncLyricView;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/library/view/SyncLyricView;)V
    .locals 0

    .prologue
    .line 854
    iput-object p1, p0, Lcom/samsung/musicplus/library/view/SyncLyricView$1;->this$0:Lcom/samsung/musicplus/library/view/SyncLyricView;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 858
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 860
    .local v0, "data":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 861
    const-string v2, "getldp_Lyrics_result"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 863
    .local v1, "status":I
    # getter for: Lcom/samsung/musicplus/library/view/SyncLyricView;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/library/view/SyncLyricView;->access$000()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "mSyncLyricReceiver() status :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 864
    iget-object v2, p0, Lcom/samsung/musicplus/library/view/SyncLyricView$1;->this$0:Lcom/samsung/musicplus/library/view/SyncLyricView;

    # getter for: Lcom/samsung/musicplus/library/view/SyncLyricView;->mStatusListener:Lcom/samsung/musicplus/library/view/SyncLyricView$OnSyncLyricStatusListener;
    invoke-static {v2}, Lcom/samsung/musicplus/library/view/SyncLyricView;->access$100(Lcom/samsung/musicplus/library/view/SyncLyricView;)Lcom/samsung/musicplus/library/view/SyncLyricView$OnSyncLyricStatusListener;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 865
    iget-object v2, p0, Lcom/samsung/musicplus/library/view/SyncLyricView$1;->this$0:Lcom/samsung/musicplus/library/view/SyncLyricView;

    # getter for: Lcom/samsung/musicplus/library/view/SyncLyricView;->mStatusListener:Lcom/samsung/musicplus/library/view/SyncLyricView$OnSyncLyricStatusListener;
    invoke-static {v2}, Lcom/samsung/musicplus/library/view/SyncLyricView;->access$100(Lcom/samsung/musicplus/library/view/SyncLyricView;)Lcom/samsung/musicplus/library/view/SyncLyricView$OnSyncLyricStatusListener;

    move-result-object v2

    invoke-interface {v2, v1}, Lcom/samsung/musicplus/library/view/SyncLyricView$OnSyncLyricStatusListener;->onLyricStatus(I)V

    .line 868
    .end local v1    # "status":I
    :cond_0
    return-void
.end method
