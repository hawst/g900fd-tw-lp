.class Lcom/samsung/musicplus/library/hardware/SviewCoverManager$1;
.super Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;
.source "SviewCoverManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/library/hardware/SviewCoverManager;->initializeScover(Landroid/content/Context;Lcom/samsung/musicplus/library/hardware/SviewCoverManager$OnCoverStateChangeListener;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/library/hardware/SviewCoverManager;

.field final synthetic val$listener:Lcom/samsung/musicplus/library/hardware/SviewCoverManager$OnCoverStateChangeListener;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/library/hardware/SviewCoverManager;Lcom/samsung/musicplus/library/hardware/SviewCoverManager$OnCoverStateChangeListener;)V
    .locals 0

    .prologue
    .line 419
    iput-object p1, p0, Lcom/samsung/musicplus/library/hardware/SviewCoverManager$1;->this$0:Lcom/samsung/musicplus/library/hardware/SviewCoverManager;

    iput-object p2, p0, Lcom/samsung/musicplus/library/hardware/SviewCoverManager$1;->val$listener:Lcom/samsung/musicplus/library/hardware/SviewCoverManager$OnCoverStateChangeListener;

    invoke-direct {p0}, Lcom/samsung/android/sdk/cover/ScoverManager$StateListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onCoverStateChanged(Lcom/samsung/android/sdk/cover/ScoverState;)V
    .locals 3
    .param p1, "state"    # Lcom/samsung/android/sdk/cover/ScoverState;

    .prologue
    .line 422
    # getter for: Lcom/samsung/musicplus/library/hardware/SviewCoverManager;->CLASSNAME:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/library/hardware/SviewCoverManager;->access$000()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "initializeScover : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/samsung/android/sdk/cover/ScoverState;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 423
    iget-object v0, p0, Lcom/samsung/musicplus/library/hardware/SviewCoverManager$1;->val$listener:Lcom/samsung/musicplus/library/hardware/SviewCoverManager$OnCoverStateChangeListener;

    invoke-virtual {p1}, Lcom/samsung/android/sdk/cover/ScoverState;->getSwitchState()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/samsung/musicplus/library/hardware/SviewCoverManager$OnCoverStateChangeListener;->onSviewCoverStateChanged(Z)V

    .line 424
    iget-object v0, p0, Lcom/samsung/musicplus/library/hardware/SviewCoverManager$1;->this$0:Lcom/samsung/musicplus/library/hardware/SviewCoverManager;

    # setter for: Lcom/samsung/musicplus/library/hardware/SviewCoverManager;->mScoverState:Lcom/samsung/android/sdk/cover/ScoverState;
    invoke-static {v0, p1}, Lcom/samsung/musicplus/library/hardware/SviewCoverManager;->access$102(Lcom/samsung/musicplus/library/hardware/SviewCoverManager;Lcom/samsung/android/sdk/cover/ScoverState;)Lcom/samsung/android/sdk/cover/ScoverState;

    .line 425
    return-void
.end method
