.class Lcom/samsung/musicplus/service/PlayerService$14;
.super Ljava/lang/Object;
.source "PlayerService.java"

# interfaces
.implements Lcom/samsung/musicplus/library/audio/RemoteControlClientCompat$OnCompatCommandSetListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/service/PlayerService;->createControlClient(Landroid/content/ComponentName;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/service/PlayerService;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/service/PlayerService;)V
    .locals 0

    .prologue
    .line 1503
    iput-object p1, p0, Lcom/samsung/musicplus/service/PlayerService$14;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCommandSet(II)V
    .locals 3
    .param p1, "key"    # I
    .param p2, "value"    # I

    .prologue
    .line 1506
    const-string v0, "MusicService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onCommandSet key : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", value : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1507
    const/4 v0, 0x2

    if-ne p1, v0, :cond_2

    .line 1508
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$14;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->getRepeat()I

    move-result v0

    if-eq p2, v0, :cond_1

    .line 1509
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$14;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0, p2}, Lcom/samsung/musicplus/service/PlayerService;->setRepeat(I)V

    .line 1522
    :cond_0
    :goto_0
    return-void

    .line 1512
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$14;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->notifySettingToRemoteClient()V
    invoke-static {v0}, Lcom/samsung/musicplus/service/PlayerService;->access$5300(Lcom/samsung/musicplus/service/PlayerService;)V

    goto :goto_0

    .line 1514
    :cond_2
    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    .line 1515
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$14;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->getShuffle()I

    move-result v0

    if-eq p2, v0, :cond_3

    .line 1516
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$14;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0, p2}, Lcom/samsung/musicplus/service/PlayerService;->setShuffle(I)V

    goto :goto_0

    .line 1519
    :cond_3
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$14;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->notifySettingToRemoteClient()V
    invoke-static {v0}, Lcom/samsung/musicplus/service/PlayerService;->access$5300(Lcom/samsung/musicplus/service/PlayerService;)V

    goto :goto_0
.end method
