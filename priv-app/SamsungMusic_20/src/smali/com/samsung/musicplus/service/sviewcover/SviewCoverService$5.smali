.class Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$5;
.super Ljava/lang/Object;
.source "SviewCoverService.java"

# interfaces
.implements Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction$OnPlayerServiceListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;)V
    .locals 0

    .prologue
    .line 278
    iput-object p1, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$5;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMetaChanged()V
    .locals 2

    .prologue
    .line 281
    const-string v0, "MusicSCover"

    const-string v1, "onMetaChanged()"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 282
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getListItemCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 283
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$5;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;

    # invokes: Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->handleClose()V
    invoke-static {v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->access$100(Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;)V

    .line 285
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$5;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;

    # getter for: Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->mPlayerView:Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;
    invoke-static {v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->access$000(Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;)Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;->updateTrackAllInfo()V

    .line 286
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$5;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;

    # getter for: Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->mPlayerView:Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;
    invoke-static {v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->access$000(Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;)Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;->invalidateViews()V

    .line 287
    return-void
.end method

.method public onMetaEditedOrQueueChanged()V
    .locals 2

    .prologue
    .line 301
    const-string v0, "MusicSCover"

    const-string v1, "onMetaEditedOrQueueChanged()"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 302
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$5;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;

    # getter for: Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->mPlayerView:Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;
    invoke-static {v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->access$000(Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;)Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;->invalidateViews()V

    .line 303
    return-void
.end method

.method public onPlayStateChanged()V
    .locals 2

    .prologue
    .line 291
    const-string v0, "MusicSCover"

    const-string v1, "onPlayStateChanged()"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 292
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getListItemCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 293
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$5;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;

    # invokes: Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->handleClose()V
    invoke-static {v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->access$100(Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;)V

    .line 295
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$5;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;

    # getter for: Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->mPlayerView:Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;
    invoke-static {v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->access$000(Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;)Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;

    move-result-object v0

    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->isPlaying()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;->updatePlayState(Z)V

    .line 296
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$5;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;

    # getter for: Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->mPlayerView:Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;
    invoke-static {v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->access$000(Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;)Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;->invalidateViews()V

    .line 297
    return-void
.end method

.method public onPrepareCompleted()V
    .locals 2

    .prologue
    .line 307
    const-string v0, "MusicSCover"

    const-string v1, "onPrepareCompleted()"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 308
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$5;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;

    # getter for: Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->mPlayerView:Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;
    invoke-static {v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->access$000(Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;)Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;

    move-result-object v0

    # invokes: Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;->updateBuffering()V
    invoke-static {v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;->access$300(Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;)V

    .line 309
    return-void
.end method
