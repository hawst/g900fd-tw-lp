.class Lcom/samsung/musicplus/service/PlayerService$9;
.super Landroid/content/BroadcastReceiver;
.source "PlayerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/service/PlayerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/service/PlayerService;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/service/PlayerService;)V
    .locals 0

    .prologue
    .line 1048
    iput-object p1, p0, Lcom/samsung/musicplus/service/PlayerService$9;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x0

    .line 1051
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 1052
    .local v0, "action":Ljava/lang/String;
    const-string v2, "android.media.VOLUME_CHANGED_ACTION"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1053
    const-string v2, "android.media.EXTRA_VOLUME_STREAM_TYPE"

    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 1054
    .local v1, "stream":I
    if-ne v1, v4, :cond_0

    .line 1055
    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService$9;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mSmartVolume:Lcom/samsung/musicplus/library/audio/SmartVolumeManager;
    invoke-static {v2}, Lcom/samsung/musicplus/service/PlayerService;->access$4000(Lcom/samsung/musicplus/service/PlayerService;)Lcom/samsung/musicplus/library/audio/SmartVolumeManager;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 1056
    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService$9;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mIgnoreVolumeCount:I
    invoke-static {v2}, Lcom/samsung/musicplus/service/PlayerService;->access$4100(Lcom/samsung/musicplus/service/PlayerService;)I

    move-result v2

    if-lez v2, :cond_1

    .line 1062
    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService$9;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # operator-- for: Lcom/samsung/musicplus/service/PlayerService;->mIgnoreVolumeCount:I
    invoke-static {v2}, Lcom/samsung/musicplus/service/PlayerService;->access$4110(Lcom/samsung/musicplus/service/PlayerService;)I

    .line 1073
    .end local v1    # "stream":I
    :cond_0
    :goto_0
    return-void

    .line 1064
    .restart local v1    # "stream":I
    :cond_1
    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService$9;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # setter for: Lcom/samsung/musicplus/service/PlayerService;->mIgnoreVolumeCount:I
    invoke-static {v2, v3}, Lcom/samsung/musicplus/service/PlayerService;->access$4102(Lcom/samsung/musicplus/service/PlayerService;I)I

    .line 1066
    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService$9;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mSmartVolume:Lcom/samsung/musicplus/library/audio/SmartVolumeManager;
    invoke-static {v2}, Lcom/samsung/musicplus/service/PlayerService;->access$4000(Lcom/samsung/musicplus/service/PlayerService;)Lcom/samsung/musicplus/library/audio/SmartVolumeManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/musicplus/library/audio/SmartVolumeManager;->resetBaseValue()V

    .line 1068
    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService$9;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    iget-object v3, p0, Lcom/samsung/musicplus/service/PlayerService$9;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;
    invoke-static {v3}, Lcom/samsung/musicplus/service/PlayerService;->access$3400(Lcom/samsung/musicplus/service/PlayerService;)Lcom/samsung/musicplus/library/audio/SecAudioManager;

    move-result-object v3

    invoke-virtual {v3, v4}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->getStreamVolume(I)I

    move-result v3

    # setter for: Lcom/samsung/musicplus/service/PlayerService;->mBaeVolume:I
    invoke-static {v2, v3}, Lcom/samsung/musicplus/service/PlayerService;->access$4202(Lcom/samsung/musicplus/service/PlayerService;I)I

    goto :goto_0
.end method
