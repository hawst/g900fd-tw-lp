.class Lcom/samsung/musicplus/service/PlayerService$MediaSessionCallback;
.super Landroid/media/session/MediaSession$Callback;
.source "PlayerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/service/PlayerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MediaSessionCallback"
.end annotation


# static fields
.field public static final ACTION_AVRCP:Ljava/lang/String; = "com.samsung.android.bt.AVRCP"

.field private static final GEAR_DEVICE_NAME:Ljava/lang/String; = "SA_MUSIC_REMOTE_CONTROL"


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/service/PlayerService;


# direct methods
.method private constructor <init>(Lcom/samsung/musicplus/service/PlayerService;)V
    .locals 0

    .prologue
    .line 1533
    iput-object p1, p0, Lcom/samsung/musicplus/service/PlayerService$MediaSessionCallback;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-direct {p0}, Landroid/media/session/MediaSession$Callback;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/musicplus/service/PlayerService;Lcom/samsung/musicplus/service/PlayerService$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/samsung/musicplus/service/PlayerService;
    .param p2, "x1"    # Lcom/samsung/musicplus/service/PlayerService$1;

    .prologue
    .line 1533
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/service/PlayerService$MediaSessionCallback;-><init>(Lcom/samsung/musicplus/service/PlayerService;)V

    return-void
.end method

.method private doCancelOnlyUpEvent(Landroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "ke"    # Landroid/view/KeyEvent;

    .prologue
    .line 1635
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getDevice()Landroid/view/InputDevice;

    move-result-object v0

    .line 1636
    .local v0, "id":Landroid/view/InputDevice;
    const-string v2, "SA_MUSIC_REMOTE_CONTROL"

    if-nez v0, :cond_0

    const-string v1, ""

    :goto_0
    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    return v1

    :cond_0
    invoke-virtual {v0}, Landroid/view/InputDevice;->getName()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private onMediaKeyDown(Landroid/view/KeyEvent;)V
    .locals 12
    .param p1, "ke"    # Landroid/view/KeyEvent;

    .prologue
    .line 1640
    iget-object v8, p0, Lcom/samsung/musicplus/service/PlayerService$MediaSessionCallback;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mMediaSession:Landroid/media/session/MediaSession;
    invoke-static {v8}, Lcom/samsung/musicplus/service/PlayerService;->access$5600(Lcom/samsung/musicplus/service/PlayerService;)Landroid/media/session/MediaSession;

    move-result-object v8

    invoke-virtual {v8}, Landroid/media/session/MediaSession;->getController()Landroid/media/session/MediaController;

    move-result-object v8

    invoke-virtual {v8}, Landroid/media/session/MediaController;->getPlaybackState()Landroid/media/session/PlaybackState;

    move-result-object v3

    .line 1641
    .local v3, "state":Landroid/media/session/PlaybackState;
    if-nez v3, :cond_1

    const-wide/16 v6, 0x0

    .line 1642
    .local v6, "validActions":J
    :goto_0
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v8

    sparse-switch v8, :sswitch_data_0

    .line 1740
    :cond_0
    :goto_1
    return-void

    .line 1641
    .end local v6    # "validActions":J
    :cond_1
    invoke-virtual {v3}, Landroid/media/session/PlaybackState;->getActions()J

    move-result-wide v6

    goto :goto_0

    .line 1644
    .restart local v6    # "validActions":J
    :sswitch_0
    const-wide/16 v8, 0x4

    and-long/2addr v8, v6

    const-wide/16 v10, 0x0

    cmp-long v8, v8, v10

    if-eqz v8, :cond_0

    .line 1645
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService$MediaSessionCallback;->onPlay()V

    goto :goto_1

    .line 1649
    :sswitch_1
    const-wide/16 v8, 0x2

    and-long/2addr v8, v6

    const-wide/16 v10, 0x0

    cmp-long v8, v8, v10

    if-eqz v8, :cond_0

    .line 1650
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService$MediaSessionCallback;->onPause()V

    goto :goto_1

    .line 1654
    :sswitch_2
    const-wide/16 v8, 0x20

    and-long/2addr v8, v6

    const-wide/16 v10, 0x0

    cmp-long v8, v8, v10

    if-eqz v8, :cond_0

    .line 1655
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService$MediaSessionCallback;->onSkipToNext()V

    goto :goto_1

    .line 1659
    :sswitch_3
    const-wide/16 v8, 0x10

    and-long/2addr v8, v6

    const-wide/16 v10, 0x0

    cmp-long v8, v8, v10

    if-eqz v8, :cond_0

    .line 1660
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService$MediaSessionCallback;->onSkipToPrevious()V

    goto :goto_1

    .line 1664
    :sswitch_4
    const-wide/16 v8, 0x1

    and-long/2addr v8, v6

    const-wide/16 v10, 0x0

    cmp-long v8, v8, v10

    if-eqz v8, :cond_0

    .line 1665
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService$MediaSessionCallback;->onStop()V

    goto :goto_1

    .line 1669
    :sswitch_5
    const-wide/16 v8, 0x40

    and-long/2addr v8, v6

    const-wide/16 v10, 0x0

    cmp-long v8, v8, v10

    if-eqz v8, :cond_0

    .line 1676
    iget-object v8, p0, Lcom/samsung/musicplus/service/PlayerService$MediaSessionCallback;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mControlTask:Lcom/samsung/musicplus/widget/control/FowardRewindControlTaskMediaButton;
    invoke-static {v8}, Lcom/samsung/musicplus/service/PlayerService;->access$700(Lcom/samsung/musicplus/service/PlayerService;)Lcom/samsung/musicplus/widget/control/FowardRewindControlTaskMediaButton;

    move-result-object v8

    if-eqz v8, :cond_2

    iget-object v8, p0, Lcom/samsung/musicplus/service/PlayerService$MediaSessionCallback;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mControlTask:Lcom/samsung/musicplus/widget/control/FowardRewindControlTaskMediaButton;
    invoke-static {v8}, Lcom/samsung/musicplus/service/PlayerService;->access$700(Lcom/samsung/musicplus/service/PlayerService;)Lcom/samsung/musicplus/widget/control/FowardRewindControlTaskMediaButton;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/musicplus/widget/control/FowardRewindControlTaskMediaButton;->isCancelled()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 1677
    :cond_2
    iget-object v8, p0, Lcom/samsung/musicplus/service/PlayerService$MediaSessionCallback;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    new-instance v9, Lcom/samsung/musicplus/widget/control/FowardRewindControlTaskMediaButton;

    iget-object v10, p0, Lcom/samsung/musicplus/service/PlayerService$MediaSessionCallback;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v10}, Lcom/samsung/musicplus/service/PlayerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v10

    const/4 v11, 0x1

    invoke-direct {v9, v10, v11}, Lcom/samsung/musicplus/widget/control/FowardRewindControlTaskMediaButton;-><init>(Landroid/content/Context;I)V

    # setter for: Lcom/samsung/musicplus/service/PlayerService;->mControlTask:Lcom/samsung/musicplus/widget/control/FowardRewindControlTaskMediaButton;
    invoke-static {v8, v9}, Lcom/samsung/musicplus/service/PlayerService;->access$702(Lcom/samsung/musicplus/service/PlayerService;Lcom/samsung/musicplus/widget/control/FowardRewindControlTaskMediaButton;)Lcom/samsung/musicplus/widget/control/FowardRewindControlTaskMediaButton;

    .line 1679
    iget-object v8, p0, Lcom/samsung/musicplus/service/PlayerService$MediaSessionCallback;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mControlTask:Lcom/samsung/musicplus/widget/control/FowardRewindControlTaskMediaButton;
    invoke-static {v8}, Lcom/samsung/musicplus/service/PlayerService;->access$700(Lcom/samsung/musicplus/service/PlayerService;)Lcom/samsung/musicplus/widget/control/FowardRewindControlTaskMediaButton;

    move-result-object v8

    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/samsung/musicplus/service/PlayerService$MediaSessionCallback;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v11}, Lcom/samsung/musicplus/service/PlayerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    const/4 v11, 0x0

    aput-object v11, v9, v10

    const/4 v10, 0x2

    const/4 v11, 0x1

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {v8, v9}, Lcom/samsung/musicplus/widget/control/FowardRewindControlTaskMediaButton;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 1685
    :goto_2
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/service/PlayerService$MediaSessionCallback;->doCancelOnlyUpEvent(Landroid/view/KeyEvent;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 1686
    iget-object v8, p0, Lcom/samsung/musicplus/service/PlayerService$MediaSessionCallback;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mPlayerHandler:Landroid/os/Handler;
    invoke-static {v8}, Lcom/samsung/musicplus/service/PlayerService;->access$600(Lcom/samsung/musicplus/service/PlayerService;)Landroid/os/Handler;

    move-result-object v8

    const/16 v9, 0x12

    const-wide/16 v10, 0x7d0

    invoke-virtual {v8, v9, v10, v11}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_1

    .line 1682
    :cond_3
    iget-object v8, p0, Lcom/samsung/musicplus/service/PlayerService$MediaSessionCallback;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mPlayerHandler:Landroid/os/Handler;
    invoke-static {v8}, Lcom/samsung/musicplus/service/PlayerService;->access$600(Lcom/samsung/musicplus/service/PlayerService;)Landroid/os/Handler;

    move-result-object v8

    const/16 v9, 0x12

    invoke-virtual {v8, v9}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_2

    .line 1691
    :sswitch_6
    const-wide/16 v8, 0x8

    and-long/2addr v8, v6

    const-wide/16 v10, 0x0

    cmp-long v8, v8, v10

    if-eqz v8, :cond_0

    .line 1692
    iget-object v8, p0, Lcom/samsung/musicplus/service/PlayerService$MediaSessionCallback;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mControlTask:Lcom/samsung/musicplus/widget/control/FowardRewindControlTaskMediaButton;
    invoke-static {v8}, Lcom/samsung/musicplus/service/PlayerService;->access$700(Lcom/samsung/musicplus/service/PlayerService;)Lcom/samsung/musicplus/widget/control/FowardRewindControlTaskMediaButton;

    move-result-object v8

    if-eqz v8, :cond_4

    iget-object v8, p0, Lcom/samsung/musicplus/service/PlayerService$MediaSessionCallback;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mControlTask:Lcom/samsung/musicplus/widget/control/FowardRewindControlTaskMediaButton;
    invoke-static {v8}, Lcom/samsung/musicplus/service/PlayerService;->access$700(Lcom/samsung/musicplus/service/PlayerService;)Lcom/samsung/musicplus/widget/control/FowardRewindControlTaskMediaButton;

    move-result-object v8

    invoke-virtual {v8}, Lcom/samsung/musicplus/widget/control/FowardRewindControlTaskMediaButton;->isCancelled()Z

    move-result v8

    if-eqz v8, :cond_5

    .line 1693
    :cond_4
    iget-object v8, p0, Lcom/samsung/musicplus/service/PlayerService$MediaSessionCallback;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    new-instance v9, Lcom/samsung/musicplus/widget/control/FowardRewindControlTaskMediaButton;

    iget-object v10, p0, Lcom/samsung/musicplus/service/PlayerService$MediaSessionCallback;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v10}, Lcom/samsung/musicplus/service/PlayerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v10

    const/4 v11, 0x2

    invoke-direct {v9, v10, v11}, Lcom/samsung/musicplus/widget/control/FowardRewindControlTaskMediaButton;-><init>(Landroid/content/Context;I)V

    # setter for: Lcom/samsung/musicplus/service/PlayerService;->mControlTask:Lcom/samsung/musicplus/widget/control/FowardRewindControlTaskMediaButton;
    invoke-static {v8, v9}, Lcom/samsung/musicplus/service/PlayerService;->access$702(Lcom/samsung/musicplus/service/PlayerService;Lcom/samsung/musicplus/widget/control/FowardRewindControlTaskMediaButton;)Lcom/samsung/musicplus/widget/control/FowardRewindControlTaskMediaButton;

    .line 1695
    iget-object v8, p0, Lcom/samsung/musicplus/service/PlayerService$MediaSessionCallback;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mControlTask:Lcom/samsung/musicplus/widget/control/FowardRewindControlTaskMediaButton;
    invoke-static {v8}, Lcom/samsung/musicplus/service/PlayerService;->access$700(Lcom/samsung/musicplus/service/PlayerService;)Lcom/samsung/musicplus/widget/control/FowardRewindControlTaskMediaButton;

    move-result-object v8

    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/samsung/musicplus/service/PlayerService$MediaSessionCallback;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v11}, Lcom/samsung/musicplus/service/PlayerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    const/4 v11, 0x0

    aput-object v11, v9, v10

    const/4 v10, 0x2

    const/4 v11, 0x2

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {v8, v9}, Lcom/samsung/musicplus/widget/control/FowardRewindControlTaskMediaButton;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 1700
    :goto_3
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/service/PlayerService$MediaSessionCallback;->doCancelOnlyUpEvent(Landroid/view/KeyEvent;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 1701
    iget-object v8, p0, Lcom/samsung/musicplus/service/PlayerService$MediaSessionCallback;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mPlayerHandler:Landroid/os/Handler;
    invoke-static {v8}, Lcom/samsung/musicplus/service/PlayerService;->access$600(Lcom/samsung/musicplus/service/PlayerService;)Landroid/os/Handler;

    move-result-object v8

    const/16 v9, 0x12

    const-wide/16 v10, 0x7d0

    invoke-virtual {v8, v9, v10, v11}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_1

    .line 1698
    :cond_5
    iget-object v8, p0, Lcom/samsung/musicplus/service/PlayerService$MediaSessionCallback;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mPlayerHandler:Landroid/os/Handler;
    invoke-static {v8}, Lcom/samsung/musicplus/service/PlayerService;->access$600(Lcom/samsung/musicplus/service/PlayerService;)Landroid/os/Handler;

    move-result-object v8

    const/16 v9, 0x12

    invoke-virtual {v8, v9}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_3

    .line 1707
    :sswitch_7
    if-nez v3, :cond_7

    const/4 v2, 0x0

    .line 1709
    .local v2, "isPlaying":Z
    :goto_4
    const-wide/16 v8, 0x204

    and-long/2addr v8, v6

    const-wide/16 v10, 0x0

    cmp-long v8, v8, v10

    if-eqz v8, :cond_9

    const/4 v1, 0x1

    .line 1710
    .local v1, "canPlay":Z
    :goto_5
    const-wide/16 v8, 0x202

    and-long/2addr v8, v6

    const-wide/16 v10, 0x0

    cmp-long v8, v8, v10

    if-eqz v8, :cond_a

    const/4 v0, 0x1

    .line 1711
    .local v0, "canPause":Z
    :goto_6
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getEventTime()J

    move-result-wide v4

    .line 1712
    .local v4, "time":J
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v8

    const/16 v9, 0x4f

    if-ne v8, v9, :cond_e

    .line 1713
    # getter for: Lcom/samsung/musicplus/service/PlayerService;->sLastHookClickTime:J
    invoke-static {}, Lcom/samsung/musicplus/service/PlayerService;->access$5700()J

    move-result-wide v8

    sub-long v8, v4, v8

    const-wide/16 v10, 0x12c

    cmp-long v8, v8, v10

    if-gez v8, :cond_b

    .line 1716
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService$MediaSessionCallback;->onSkipToNext()V

    .line 1717
    if-nez v2, :cond_6

    .line 1718
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService$MediaSessionCallback;->onPlay()V

    .line 1720
    :cond_6
    const-wide/16 v8, 0x0

    # setter for: Lcom/samsung/musicplus/service/PlayerService;->sLastHookClickTime:J
    invoke-static {v8, v9}, Lcom/samsung/musicplus/service/PlayerService;->access$5702(J)J

    goto/16 :goto_1

    .line 1707
    .end local v0    # "canPause":Z
    .end local v1    # "canPlay":Z
    .end local v2    # "isPlaying":Z
    .end local v4    # "time":J
    :cond_7
    invoke-virtual {v3}, Landroid/media/session/PlaybackState;->getState()I

    move-result v8

    const/4 v9, 0x3

    if-ne v8, v9, :cond_8

    const/4 v2, 0x1

    goto :goto_4

    :cond_8
    const/4 v2, 0x0

    goto :goto_4

    .line 1709
    .restart local v2    # "isPlaying":Z
    :cond_9
    const/4 v1, 0x0

    goto :goto_5

    .line 1710
    .restart local v1    # "canPlay":Z
    :cond_a
    const/4 v0, 0x0

    goto :goto_6

    .line 1722
    .restart local v0    # "canPause":Z
    .restart local v4    # "time":J
    :cond_b
    if-eqz v2, :cond_d

    if-eqz v0, :cond_d

    .line 1723
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService$MediaSessionCallback;->onPause()V

    .line 1727
    :cond_c
    :goto_7
    # setter for: Lcom/samsung/musicplus/service/PlayerService;->sLastHookClickTime:J
    invoke-static {v4, v5}, Lcom/samsung/musicplus/service/PlayerService;->access$5702(J)J

    goto/16 :goto_1

    .line 1724
    :cond_d
    if-nez v2, :cond_c

    if-eqz v1, :cond_c

    .line 1725
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService$MediaSessionCallback;->onPlay()V

    goto :goto_7

    .line 1730
    :cond_e
    if-eqz v2, :cond_f

    if-eqz v0, :cond_f

    .line 1731
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService$MediaSessionCallback;->onPause()V

    goto/16 :goto_1

    .line 1732
    :cond_f
    if-nez v2, :cond_0

    if-eqz v1, :cond_0

    .line 1733
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService$MediaSessionCallback;->onPlay()V

    goto/16 :goto_1

    .line 1642
    nop

    :sswitch_data_0
    .sparse-switch
        0x4f -> :sswitch_7
        0x55 -> :sswitch_7
        0x56 -> :sswitch_4
        0x57 -> :sswitch_2
        0x58 -> :sswitch_3
        0x59 -> :sswitch_6
        0x5a -> :sswitch_5
        0x7e -> :sswitch_0
        0x7f -> :sswitch_1
    .end sparse-switch
.end method


# virtual methods
.method public onCustomAction(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 5
    .param p1, "action"    # Ljava/lang/String;
    .param p2, "extras"    # Landroid/os/Bundle;

    .prologue
    .line 1744
    const-string v2, "com.samsung.android.bt.AVRCP"

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1745
    const-string v2, "repeat"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 1746
    .local v0, "repeat":I
    const-string v2, "shuffle"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 1748
    .local v1, "shuffle":I
    const-string v2, "MusicService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onCustomAction AVRCP repeat "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", shuffle : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1749
    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService$MediaSessionCallback;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v2}, Lcom/samsung/musicplus/service/PlayerService;->getShuffle()I

    move-result v2

    if-ne v1, v2, :cond_1

    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService$MediaSessionCallback;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v2}, Lcom/samsung/musicplus/service/PlayerService;->getRepeat()I

    move-result v2

    if-ne v0, v2, :cond_1

    .line 1751
    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService$MediaSessionCallback;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->notifySettingToRemoteClient()V
    invoke-static {v2}, Lcom/samsung/musicplus/service/PlayerService;->access$5300(Lcom/samsung/musicplus/service/PlayerService;)V

    .line 1764
    .end local v0    # "repeat":I
    .end local v1    # "shuffle":I
    :cond_0
    :goto_0
    return-void

    .line 1753
    .restart local v0    # "repeat":I
    .restart local v1    # "shuffle":I
    :cond_1
    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService$MediaSessionCallback;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v2}, Lcom/samsung/musicplus/service/PlayerService;->getRepeat()I

    move-result v2

    if-eq v0, v2, :cond_2

    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService$MediaSessionCallback;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v2}, Lcom/samsung/musicplus/service/PlayerService;->getShuffle()I

    move-result v2

    if-eq v1, v2, :cond_2

    .line 1754
    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService$MediaSessionCallback;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->setShuffleAndRepeat(II)V
    invoke-static {v2, v1, v0}, Lcom/samsung/musicplus/service/PlayerService;->access$5800(Lcom/samsung/musicplus/service/PlayerService;II)V

    goto :goto_0

    .line 1756
    :cond_2
    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService$MediaSessionCallback;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v2}, Lcom/samsung/musicplus/service/PlayerService;->getRepeat()I

    move-result v2

    if-eq v0, v2, :cond_3

    .line 1757
    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService$MediaSessionCallback;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v2, v0}, Lcom/samsung/musicplus/service/PlayerService;->setRepeat(I)V

    goto :goto_0

    .line 1758
    :cond_3
    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService$MediaSessionCallback;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v2}, Lcom/samsung/musicplus/service/PlayerService;->getShuffle()I

    move-result v2

    if-eq v1, v2, :cond_0

    .line 1759
    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService$MediaSessionCallback;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v2, v1}, Lcom/samsung/musicplus/service/PlayerService;->setShuffle(I)V

    goto :goto_0
.end method

.method public onFastForward()V
    .locals 2

    .prologue
    .line 1557
    const-string v0, "MusicService"

    const-string v1, "MediaSession onFastForward()"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1558
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$MediaSessionCallback;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->forward()V
    invoke-static {v0}, Lcom/samsung/musicplus/service/PlayerService;->access$5400(Lcom/samsung/musicplus/service/PlayerService;)V

    .line 1559
    return-void
.end method

.method public onMediaButtonEvent(Landroid/content/Intent;)Z
    .locals 4
    .param p1, "mediaButtonIntent"    # Landroid/content/Intent;

    .prologue
    .line 1592
    const-string v1, "MusicService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "MediaSession onMediaButtonEvent() mediaButtonIntent : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1599
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService$MediaSessionCallback;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mMediaSession:Landroid/media/session/MediaSession;
    invoke-static {v1}, Lcom/samsung/musicplus/service/PlayerService;->access$5600(Lcom/samsung/musicplus/service/PlayerService;)Landroid/media/session/MediaSession;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string v1, "android.intent.action.MEDIA_BUTTON"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1601
    const-string v1, "android.intent.extra.KEY_EVENT"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/view/KeyEvent;

    .line 1602
    .local v0, "ke":Landroid/view/KeyEvent;
    if-eqz v0, :cond_0

    .line 1603
    invoke-virtual {v0}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 1619
    .end local v0    # "ke":Landroid/view/KeyEvent;
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/media/session/MediaSession$Callback;->onMediaButtonEvent(Landroid/content/Intent;)Z

    move-result v1

    :goto_1
    return v1

    .line 1605
    .restart local v0    # "ke":Landroid/view/KeyEvent;
    :pswitch_0
    invoke-direct {p0, v0}, Lcom/samsung/musicplus/service/PlayerService$MediaSessionCallback;->onMediaKeyDown(Landroid/view/KeyEvent;)V

    .line 1606
    const/4 v1, 0x1

    goto :goto_1

    .line 1612
    :pswitch_1
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService$MediaSessionCallback;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mPlayerHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/musicplus/service/PlayerService;->access$600(Lcom/samsung/musicplus/service/PlayerService;)Landroid/os/Handler;

    move-result-object v1

    const/16 v2, 0x12

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 1603
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 1545
    const-string v0, "MusicService"

    const-string v1, "MediaSession onPause()"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1546
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$MediaSessionCallback;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->pause()V

    .line 1547
    return-void
.end method

.method public onPlay()V
    .locals 2

    .prologue
    .line 1539
    const-string v0, "MusicService"

    const-string v1, "MediaSession onPlay()"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1540
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$MediaSessionCallback;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/service/PlayerService;->play(Z)V

    .line 1541
    return-void
.end method

.method public onRewind()V
    .locals 2

    .prologue
    .line 1563
    const-string v0, "MusicService"

    const-string v1, "MediaSession onRewind()"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1564
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$MediaSessionCallback;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->rewind()V
    invoke-static {v0}, Lcom/samsung/musicplus/service/PlayerService;->access$5500(Lcom/samsung/musicplus/service/PlayerService;)V

    .line 1565
    return-void
.end method

.method public onSeekTo(J)V
    .locals 3
    .param p1, "pos"    # J

    .prologue
    .line 1569
    const-string v0, "MusicService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MediaSession onSeekTo() pos : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1570
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$MediaSessionCallback;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mControlTask:Lcom/samsung/musicplus/widget/control/FowardRewindControlTaskMediaButton;
    invoke-static {v0}, Lcom/samsung/musicplus/service/PlayerService;->access$700(Lcom/samsung/musicplus/service/PlayerService;)Lcom/samsung/musicplus/widget/control/FowardRewindControlTaskMediaButton;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$MediaSessionCallback;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mControlTask:Lcom/samsung/musicplus/widget/control/FowardRewindControlTaskMediaButton;
    invoke-static {v0}, Lcom/samsung/musicplus/service/PlayerService;->access$700(Lcom/samsung/musicplus/service/PlayerService;)Lcom/samsung/musicplus/widget/control/FowardRewindControlTaskMediaButton;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/musicplus/widget/control/FowardRewindControlTaskMediaButton;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1571
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$MediaSessionCallback;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/musicplus/service/PlayerService;->seek(J)J

    .line 1576
    :goto_0
    return-void

    .line 1573
    :cond_1
    const-string v0, "MusicService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "MediaSession onSeekTo() pos : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ignore this, our ff/rew task is working now"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onSkipToNext()V
    .locals 2

    .prologue
    .line 1580
    const-string v0, "MusicService"

    const-string v1, "MediaSession onSkipToNext()"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1581
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$MediaSessionCallback;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/service/PlayerService;->next(Z)V

    .line 1582
    return-void
.end method

.method public onSkipToPrevious()V
    .locals 2

    .prologue
    .line 1586
    const-string v0, "MusicService"

    const-string v1, "MediaSession onSkipToPrevious()"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1587
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$MediaSessionCallback;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/service/PlayerService;->prev(Z)V

    .line 1588
    return-void
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 1551
    const-string v0, "MusicService"

    const-string v1, "MediaSession onStop()"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1552
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$MediaSessionCallback;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->stop()V
    invoke-static {v0}, Lcom/samsung/musicplus/service/PlayerService;->access$4400(Lcom/samsung/musicplus/service/PlayerService;)V

    .line 1553
    return-void
.end method
