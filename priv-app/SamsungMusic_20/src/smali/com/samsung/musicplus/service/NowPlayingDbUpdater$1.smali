.class final Lcom/samsung/musicplus/service/NowPlayingDbUpdater$1;
.super Ljava/lang/Thread;
.source "NowPlayingDbUpdater.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/service/NowPlayingDbUpdater;->saveNowPlayingQueue(Landroid/content/Context;[J)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$context:Landroid/content/Context;

.field final synthetic val$list:[J


# direct methods
.method constructor <init>(Landroid/content/Context;[J)V
    .locals 0

    .prologue
    .line 222
    iput-object p1, p0, Lcom/samsung/musicplus/service/NowPlayingDbUpdater$1;->val$context:Landroid/content/Context;

    iput-object p2, p0, Lcom/samsung/musicplus/service/NowPlayingDbUpdater$1;->val$list:[J

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 225
    iget-object v4, p0, Lcom/samsung/musicplus/service/NowPlayingDbUpdater$1;->val$context:Landroid/content/Context;

    # invokes: Lcom/samsung/musicplus/service/NowPlayingDbUpdater;->getNowPlayingListId(Landroid/content/Context;)J
    invoke-static {v4}, Lcom/samsung/musicplus/service/NowPlayingDbUpdater;->access$000(Landroid/content/Context;)J

    move-result-wide v2

    .line 226
    .local v2, "id":J
    const-string v4, "MusicNowPlayingDbUpdater"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "saveNowPlayingQueue now playing id : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    const-wide/16 v4, -0x1

    cmp-long v4, v2, v4

    if-eqz v4, :cond_0

    .line 231
    invoke-static {v2, v3}, Lcom/samsung/musicplus/util/ListUtils;->getPlaylistMembersContentUri(J)Landroid/net/Uri;

    move-result-object v4

    invoke-static {v4}, Lcom/samsung/musicplus/provider/MusicContents;->getNotifyDisabledUri(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v1

    .line 234
    .local v1, "uri":Landroid/net/Uri;
    :try_start_0
    iget-object v4, p0, Lcom/samsung/musicplus/service/NowPlayingDbUpdater$1;->val$context:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v4, v1, v5, v6}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 237
    iget-object v4, p0, Lcom/samsung/musicplus/service/NowPlayingDbUpdater$1;->val$context:Landroid/content/Context;

    iget-object v5, p0, Lcom/samsung/musicplus/service/NowPlayingDbUpdater$1;->val$list:[J

    invoke-static {v4, v5, v2, v3}, Lcom/samsung/musicplus/util/FileOperationTask;->addToPlaylistInternal(Landroid/content/Context;[JJ)I

    .line 238
    const-string v4, "MusicNowPlayingDbUpdater"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "addToPlaylistInternal now playing id : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 243
    .end local v1    # "uri":Landroid/net/Uri;
    :cond_0
    :goto_0
    return-void

    .line 239
    .restart local v1    # "uri":Landroid/net/Uri;
    :catch_0
    move-exception v0

    .line 240
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v4, "MusicNowPlayingDbUpdater"

    const-string v5, "saveNowPlayingQueue() - IllegalArgumentException!!"

    invoke-static {v4, v5}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
