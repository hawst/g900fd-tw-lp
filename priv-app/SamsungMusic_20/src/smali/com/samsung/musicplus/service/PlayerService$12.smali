.class Lcom/samsung/musicplus/service/PlayerService$12;
.super Ljava/lang/Object;
.source "PlayerService.java"

# interfaces
.implements Landroid/media/RemoteControlClient$OnPlaybackPositionUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/service/PlayerService;->createControlClient(Landroid/content/ComponentName;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/service/PlayerService;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/service/PlayerService;)V
    .locals 0

    .prologue
    .line 1480
    iput-object p1, p0, Lcom/samsung/musicplus/service/PlayerService$12;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPlaybackPositionUpdate(J)V
    .locals 3
    .param p1, "newPositionMs"    # J

    .prologue
    .line 1485
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gtz v0, :cond_0

    .line 1486
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$12;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->rewind(J)V
    invoke-static {v0, p1, p2}, Lcom/samsung/musicplus/service/PlayerService;->access$5200(Lcom/samsung/musicplus/service/PlayerService;J)V

    .line 1491
    :goto_0
    return-void

    .line 1488
    :cond_0
    const-string v0, "MusicService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onPlaybackPositionUpdate newPositionMs : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1489
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$12;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/musicplus/service/PlayerService;->seek(J)J

    goto :goto_0
.end method
