.class Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction$2;
.super Landroid/content/BroadcastReceiver;
.source "SviewCoverCommonAction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;)V
    .locals 0

    .prologue
    .line 109
    iput-object p1, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction$2;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 112
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 113
    .local v0, "action":Ljava/lang/String;
    # getter for: Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;->access$000()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mSystemReceiver - "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 114
    const-string v3, "com.samsung.cover.OPEN"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 115
    const-string v3, "coverOpen"

    const/4 v4, 0x0

    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    .line 116
    .local v2, "isCoverOpened":Z
    if-eqz v2, :cond_0

    .line 117
    iget-object v3, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction$2;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;

    # getter for: Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;->mOnSystemListener:Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction$OnSystemListener;
    invoke-static {v3}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;->access$200(Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;)Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction$OnSystemListener;

    move-result-object v3

    invoke-interface {v3}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction$OnSystemListener;->onSviewCoverOpened()V

    .line 133
    .end local v2    # "isCoverOpened":Z
    :cond_0
    :goto_0
    return-void

    .line 119
    :cond_1
    const-string v3, "com.samsung.musicplus.dlna.connectivitychanged"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 120
    const-string v3, "com.samsung.musicplus.dlna.connectivitychanged.extra.what"

    const/4 v4, -0x1

    invoke-virtual {p2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 122
    .local v1, "event":I
    packed-switch v1, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 125
    :pswitch_1
    iget-object v3, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction$2;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;

    # getter for: Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;->mOnSystemListener:Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction$OnSystemListener;
    invoke-static {v3}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;->access$200(Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;)Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction$OnSystemListener;

    move-result-object v3

    invoke-interface {v3}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction$OnSystemListener;->onDlnaDeviceRemoved()V

    goto :goto_0

    .line 130
    .end local v1    # "event":I
    :cond_2
    const-string v3, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 131
    iget-object v3, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction$2;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;

    # getter for: Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;->mOnSystemListener:Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction$OnSystemListener;
    invoke-static {v3}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;->access$200(Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;)Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction$OnSystemListener;

    move-result-object v3

    invoke-interface {v3}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction$OnSystemListener;->onScreenOff()V

    goto :goto_0

    .line 122
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
