.class Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver;
.super Landroid/database/ContentObserver;
.source "PlayerListManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/service/PlayerListManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "PlayerListContentObserver"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver$OnContentChangeListener;
    }
.end annotation


# instance fields
.field private mHandler:Landroid/os/Handler;

.field private mLastLoadCompleteTime:J

.field private mListener:Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver$OnContentChangeListener;

.field private mUpdateNotifier:Ljava/lang/Runnable;

.field private mUpdateThrottle:I

.field private mWaiting:Z


# direct methods
.method public constructor <init>(Landroid/os/Handler;)V
    .locals 2
    .param p1, "handler"    # Landroid/os/Handler;

    .prologue
    .line 1600
    invoke-direct {p0, p1}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 1585
    const/16 v0, 0x7d0

    iput v0, p0, Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver;->mUpdateThrottle:I

    .line 1587
    const-wide/16 v0, -0x2710

    iput-wide v0, p0, Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver;->mLastLoadCompleteTime:J

    .line 1593
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver;->mWaiting:Z

    .line 1622
    new-instance v0, Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver$1;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver$1;-><init>(Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver;)V

    iput-object v0, p0, Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver;->mUpdateNotifier:Ljava/lang/Runnable;

    .line 1601
    iput-object p1, p0, Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver;->mHandler:Landroid/os/Handler;

    .line 1602
    return-void
.end method

.method static synthetic access$400(Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver;)J
    .locals 2
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver;

    .prologue
    .line 1583
    iget-wide v0, p0, Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver;->mLastLoadCompleteTime:J

    return-wide v0
.end method

.method static synthetic access$402(Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver;J)J
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver;
    .param p1, "x1"    # J

    .prologue
    .line 1583
    iput-wide p1, p0, Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver;->mLastLoadCompleteTime:J

    return-wide p1
.end method

.method static synthetic access$500(Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver;)Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver$OnContentChangeListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver;

    .prologue
    .line 1583
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver;->mListener:Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver$OnContentChangeListener;

    return-object v0
.end method

.method static synthetic access$602(Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver;
    .param p1, "x1"    # Z

    .prologue
    .line 1583
    iput-boolean p1, p0, Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver;->mWaiting:Z

    return p1
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 8
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    .line 1610
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 1611
    .local v0, "now":J
    const-string v2, "MusicListManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onChange ! now : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Uri :  "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1612
    iget-wide v2, p0, Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver;->mLastLoadCompleteTime:J

    iget v4, p0, Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver;->mUpdateThrottle:I

    int-to-long v4, v4

    add-long/2addr v2, v4

    cmp-long v2, v0, v2

    if-gez v2, :cond_1

    .line 1613
    iget-boolean v2, p0, Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver;->mWaiting:Z

    if-nez v2, :cond_0

    .line 1614
    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver;->mHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver;->mUpdateNotifier:Ljava/lang/Runnable;

    iget-wide v4, p0, Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver;->mLastLoadCompleteTime:J

    iget v6, p0, Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver;->mUpdateThrottle:I

    int-to-long v6, v6

    add-long/2addr v4, v6

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postAtTime(Ljava/lang/Runnable;J)Z

    .line 1615
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver;->mWaiting:Z

    .line 1620
    :cond_0
    :goto_0
    return-void

    .line 1619
    :cond_1
    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver;->mHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver;->mUpdateNotifier:Ljava/lang/Runnable;

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method setContentChangeListener(Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver$OnContentChangeListener;)V
    .locals 0
    .param p1, "l"    # Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver$OnContentChangeListener;

    .prologue
    .line 1605
    iput-object p1, p0, Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver;->mListener:Lcom/samsung/musicplus/service/PlayerListManager$PlayerListContentObserver$OnContentChangeListener;

    .line 1606
    return-void
.end method
