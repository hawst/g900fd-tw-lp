.class Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog$6;
.super Ljava/lang/Object;
.source "SviewCoverDialog.java"

# interfaces
.implements Lcom/samsung/musicplus/library/hardware/SviewCoverManager$OnCoverStateChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;)V
    .locals 0

    .prologue
    .line 298
    iput-object p1, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog$6;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSviewCoverStateChanged(Z)V
    .locals 3
    .param p1, "isOpen"    # Z

    .prologue
    .line 301
    # getter for: Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->access$500()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mOnCoverStateChangeListener - isOpen: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 302
    if-eqz p1, :cond_0

    .line 303
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog$6;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;

    # invokes: Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->startMusicMainActivity()V
    invoke-static {v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->access$1000(Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;)V

    .line 305
    :cond_0
    return-void
.end method
