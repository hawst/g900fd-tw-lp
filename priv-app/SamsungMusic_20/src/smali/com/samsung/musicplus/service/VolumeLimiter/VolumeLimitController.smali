.class public Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;
.super Ljava/lang/Object;
.source "VolumeLimitController.java"


# static fields
.field private static final VOLUME_CHANGE_DURATION_DEFAULT:I = 0x3e8

.field private static final VOLUME_CHANGE_GRADE_COUNT:I = 0x32


# instance fields
.field private mCurLeftLimitVolume:F

.field private mCurRightLimitVolume:F

.field private mIsLocking:Z

.field private mLeftVolumeChangeValueGap:F

.field private mNowFadeProgressing:Z

.field private mRightVolumeChangeValueGap:F

.field private mSetLeftLimitVolume:F

.field private mSetRightLimitVolume:F

.field private mVolumeChangeTimeGap:I

.field mVolumeLimitControlHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;->mNowFadeProgressing:Z

    .line 21
    iput v1, p0, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;->mLeftVolumeChangeValueGap:F

    .line 22
    iput v1, p0, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;->mRightVolumeChangeValueGap:F

    .line 23
    iput v1, p0, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;->mSetLeftLimitVolume:F

    .line 24
    iput v1, p0, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;->mSetRightLimitVolume:F

    .line 25
    iput v1, p0, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;->mCurLeftLimitVolume:F

    .line 26
    iput v1, p0, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;->mCurRightLimitVolume:F

    .line 27
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;->mIsLocking:Z

    .line 29
    const/16 v0, 0x14

    iput v0, p0, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;->mVolumeChangeTimeGap:I

    .line 31
    new-instance v0, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController$1;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController$1;-><init>(Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;)V

    iput-object v0, p0, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;->mVolumeLimitControlHandler:Landroid/os/Handler;

    .line 43
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;->changeCurVolumeLimit()Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;

    .prologue
    .line 13
    iget v0, p0, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;->mVolumeChangeTimeGap:I

    return v0
.end method

.method private changeCurVolumeLimit()Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x0

    .line 46
    iget v1, p0, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;->mCurLeftLimitVolume:F

    iget v2, p0, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;->mLeftVolumeChangeValueGap:F

    add-float/2addr v1, v2

    iput v1, p0, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;->mCurLeftLimitVolume:F

    .line 47
    iget v1, p0, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;->mCurRightLimitVolume:F

    iget v2, p0, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;->mRightVolumeChangeValueGap:F

    add-float/2addr v1, v2

    iput v1, p0, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;->mCurRightLimitVolume:F

    .line 49
    iget v1, p0, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;->mLeftVolumeChangeValueGap:F

    cmpl-float v1, v1, v3

    if-lez v1, :cond_2

    .line 50
    iget v1, p0, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;->mCurLeftLimitVolume:F

    iget v2, p0, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;->mSetLeftLimitVolume:F

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_0

    .line 51
    iget v1, p0, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;->mSetLeftLimitVolume:F

    iput v1, p0, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;->mCurLeftLimitVolume:F

    .line 59
    :cond_0
    :goto_0
    iget v1, p0, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;->mRightVolumeChangeValueGap:F

    cmpl-float v1, v1, v3

    if-lez v1, :cond_3

    .line 60
    iget v1, p0, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;->mCurRightLimitVolume:F

    iget v2, p0, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;->mSetRightLimitVolume:F

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_1

    .line 61
    iget v1, p0, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;->mSetRightLimitVolume:F

    iput v1, p0, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;->mCurRightLimitVolume:F

    .line 69
    :cond_1
    :goto_1
    invoke-direct {p0}, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;->setServiceLimitVolume()V

    .line 71
    iget v1, p0, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;->mCurLeftLimitVolume:F

    iget v2, p0, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;->mSetLeftLimitVolume:F

    cmpl-float v1, v1, v2

    if-nez v1, :cond_4

    iget v1, p0, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;->mCurRightLimitVolume:F

    iget v2, p0, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;->mSetRightLimitVolume:F

    cmpl-float v1, v1, v2

    if-nez v1, :cond_4

    .line 72
    iput-boolean v0, p0, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;->mNowFadeProgressing:Z

    .line 75
    :goto_2
    return v0

    .line 54
    :cond_2
    iget v1, p0, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;->mCurLeftLimitVolume:F

    iget v2, p0, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;->mSetLeftLimitVolume:F

    cmpg-float v1, v1, v2

    if-gtz v1, :cond_0

    .line 55
    iget v1, p0, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;->mSetLeftLimitVolume:F

    iput v1, p0, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;->mCurLeftLimitVolume:F

    goto :goto_0

    .line 64
    :cond_3
    iget v1, p0, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;->mCurRightLimitVolume:F

    iget v2, p0, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;->mSetRightLimitVolume:F

    cmpg-float v1, v1, v2

    if-gtz v1, :cond_1

    .line 65
    iget v1, p0, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;->mSetRightLimitVolume:F

    iput v1, p0, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;->mCurRightLimitVolume:F

    goto :goto_1

    .line 75
    :cond_4
    const/4 v0, 0x1

    goto :goto_2
.end method

.method private progressChangeVolume()V
    .locals 6

    .prologue
    const/high16 v4, 0x42480000    # 50.0f

    .line 79
    iget v2, p0, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;->mSetLeftLimitVolume:F

    iget v3, p0, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;->mCurLeftLimitVolume:F

    sub-float v0, v2, v3

    .line 80
    .local v0, "leftGap":F
    iget v2, p0, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;->mSetRightLimitVolume:F

    iget v3, p0, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;->mCurRightLimitVolume:F

    sub-float v1, v2, v3

    .line 81
    .local v1, "rightGap":F
    div-float v2, v0, v4

    iput v2, p0, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;->mLeftVolumeChangeValueGap:F

    .line 82
    div-float v2, v1, v4

    iput v2, p0, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;->mRightVolumeChangeValueGap:F

    .line 83
    iget-object v2, p0, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;->mVolumeLimitControlHandler:Landroid/os/Handler;

    const/4 v3, 0x0

    iget v4, p0, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;->mVolumeChangeTimeGap:I

    int-to-long v4, v4

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 84
    return-void
.end method

.method private setServiceLimitVolume()V
    .locals 2

    .prologue
    .line 87
    iget v0, p0, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;->mCurLeftLimitVolume:F

    iget v1, p0, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;->mCurRightLimitVolume:F

    invoke-static {v0, v1}, Lcom/samsung/musicplus/util/ServiceUtils;->setLimitVolume(FF)V

    .line 88
    return-void
.end method


# virtual methods
.method public isFadeProgressing()Z
    .locals 1

    .prologue
    .line 120
    iget-boolean v0, p0, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;->mNowFadeProgressing:Z

    return v0
.end method

.method public setLimitVolume(FFI)V
    .locals 2
    .param p1, "l"    # F
    .param p2, "r"    # F
    .param p3, "fadeDuration"    # I

    .prologue
    const/4 v1, 0x0

    .line 98
    iget-boolean v0, p0, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;->mIsLocking:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;->mSetLeftLimitVolume:F

    cmpl-float v0, v0, p1

    if-nez v0, :cond_1

    iget v0, p0, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;->mSetRightLimitVolume:F

    cmpl-float v0, v0, p2

    if-nez v0, :cond_1

    .line 117
    :cond_0
    :goto_0
    return-void

    .line 102
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;->mVolumeLimitControlHandler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 104
    iput p1, p0, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;->mSetLeftLimitVolume:F

    .line 105
    iput p2, p0, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;->mSetRightLimitVolume:F

    .line 107
    if-gtz p3, :cond_2

    .line 108
    iget v0, p0, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;->mSetLeftLimitVolume:F

    iput v0, p0, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;->mCurLeftLimitVolume:F

    .line 109
    iget v0, p0, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;->mSetRightLimitVolume:F

    iput v0, p0, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;->mCurRightLimitVolume:F

    .line 110
    invoke-direct {p0}, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;->setServiceLimitVolume()V

    .line 111
    iput-boolean v1, p0, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;->mNowFadeProgressing:Z

    goto :goto_0

    .line 113
    :cond_2
    div-int/lit8 v0, p3, 0x32

    iput v0, p0, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;->mVolumeChangeTimeGap:I

    .line 114
    invoke-direct {p0}, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;->progressChangeVolume()V

    .line 115
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;->mNowFadeProgressing:Z

    goto :goto_0
.end method

.method public setLockVolumeLimitControl(Z)V
    .locals 2
    .param p1, "isLocking"    # Z

    .prologue
    const/high16 v1, 0x3f800000    # 1.0f

    .line 91
    iput-boolean p1, p0, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;->mIsLocking:Z

    .line 92
    iget-boolean v0, p0, Lcom/samsung/musicplus/service/VolumeLimiter/VolumeLimitController;->mIsLocking:Z

    if-eqz v0, :cond_0

    .line 93
    invoke-static {v1, v1}, Lcom/samsung/musicplus/util/ServiceUtils;->setLimitVolume(FF)V

    .line 95
    :cond_0
    return-void
.end method
