.class Lcom/samsung/musicplus/service/PlayerService$ServiceStub;
.super Lcom/samsung/musicplus/service/IPlayerService$Stub;
.source "PlayerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/service/PlayerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "ServiceStub"
.end annotation


# instance fields
.field mService:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/samsung/musicplus/service/PlayerService;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/service/PlayerService;)V
    .locals 1
    .param p1, "service"    # Lcom/samsung/musicplus/service/PlayerService;

    .prologue
    .line 5554
    invoke-direct {p0}, Lcom/samsung/musicplus/service/IPlayerService$Stub;-><init>()V

    .line 5555
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    .line 5556
    return-void
.end method


# virtual methods
.method public buffering()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 5728
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->buffering()I

    move-result v0

    return v0
.end method

.method public changeListWithoutPlaying(ILjava/lang/String;[JI)V
    .locals 1
    .param p1, "listType"    # I
    .param p2, "keyWord"    # Ljava/lang/String;
    .param p3, "list"    # [J
    .param p4, "position"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 5567
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/samsung/musicplus/service/PlayerService;->changeListWithoutPlaying(ILjava/lang/String;[JI)V

    .line 5568
    return-void
.end method

.method public changeToDefaultPlayer()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 5848
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/service/PlayerService;->changeToDefaultPlayer(Z)V

    .line 5849
    return-void
.end method

.method public changeToDmrPlayer(Ljava/lang/String;)V
    .locals 1
    .param p1, "dmrId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 5843
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/service/PlayerService;->changeToDmrPlayer(Ljava/lang/String;)V

    .line 5844
    return-void
.end method

.method public changeToWfdDevice()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 5853
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->changeToWfdDevice()V

    .line 5854
    return-void
.end method

.method public dlnaDmrVolumeDown()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 5923
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->dlnaDmrVolumeDown()V

    .line 5924
    return-void
.end method

.method public dlnaDmrVolumeUp()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 5918
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->dlnaDmrVolumeUp()V

    .line 5919
    return-void
.end method

.method public duration()J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 5733
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->duration()J

    move-result-wide v0

    return-wide v0
.end method

.method public enqueue([JI)V
    .locals 1
    .param p1, "list"    # [J
    .param p2, "action"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 5603
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/musicplus/service/PlayerService;->enqueue([JI)V

    .line 5604
    return-void
.end method

.method public getAlbumArt()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 5808
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->getAlbumArt()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAlbumId()J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 5793
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->getAlbumId()J

    move-result-wide v0

    return-wide v0
.end method

.method public getAlbumName()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 5788
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->getAlbum()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getArtistName()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 5783
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->getArtist()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAudioId()J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 5718
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->getAudioId()J

    move-result-wide v0

    return-wide v0
.end method

.method public getAudioSessionId()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 5643
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->getAudioSessionId()I

    move-result v0

    return v0
.end method

.method public getBitDepth()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 5763
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->getBitDepth()I

    move-result v0

    return v0
.end method

.method public getCurrentBaseUri()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 5828
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->getCurrentBaseUri()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentPath()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 5753
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->getCurrentPath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentUri()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 5823
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->getCurrentUri()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDeviceId()J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 5803
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->getDeviceId()J

    move-result-wide v0

    return-wide v0
.end method

.method public getDlnaPlayingDmrId()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 5908
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->getDlnaPlayingDmrId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDlnaPlayingNic()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 5903
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->getDlnaPlayingNic()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getGenre()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 5798
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->getGenre()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getKeyWord()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 5818
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->getKeyWord()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getListItemCount()I
    .locals 1

    .prologue
    .line 5883
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->getListItemCount()I

    move-result v0

    return v0
.end method

.method public getListPosition()I
    .locals 1

    .prologue
    .line 5888
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->getListPosition()I

    move-result v0

    return v0
.end method

.method public getListType()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 5813
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->getListType()I

    move-result v0

    return v0
.end method

.method public getMediaType()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 5758
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->getMediaType()I

    move-result v0

    return v0
.end method

.method public getMimeType()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 5773
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->getMimeType()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMoodValue()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 6028
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->getMoodValue()I

    move-result v0

    return v0
.end method

.method public getNewSoundAliveBandLevel(I)I
    .locals 1
    .param p1, "bandNum"    # I

    .prologue
    .line 5968
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/service/PlayerService;->getNewSoundAliveBandLevel(I)I

    move-result v0

    return v0
.end method

.method public getNewSoundAliveCurrentPreset()I
    .locals 1

    .prologue
    .line 5958
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->getNewSoundAliveCurrentPreset()I

    move-result v0

    return v0
.end method

.method public getNewSoundAliveRoundedStrength(I)I
    .locals 1
    .param p1, "type"    # I

    .prologue
    .line 5973
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/service/PlayerService;->getNewSoundAliveRoundedStrength(I)I

    move-result v0

    return v0
.end method

.method public getNewSoundAliveSquarePosition()[I
    .locals 1

    .prologue
    .line 5963
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->getNewSoundAliveSquarePosition()[I

    move-result-object v0

    return-object v0
.end method

.method public getNextUri()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 5833
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->getNextUri()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getOrganizedQueue()[J
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 5748
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->getOrganizedQueue()[J

    move-result-object v0

    return-object v0
.end method

.method public getPersonalMode()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 6043
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->getPersonalMode()I

    move-result v0

    return v0
.end method

.method public getPlaySpeed()F
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 5663
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->getPlaySpeed()F

    move-result v0

    return v0
.end method

.method public getPreferencesFloat(Ljava/lang/String;F)F
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # F
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 6033
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/musicplus/service/PlayerService;->getPreferencesFloat(Ljava/lang/String;F)F

    move-result v0

    return v0
.end method

.method public getPrevUri()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 5838
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->getPrevUri()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getQueue()[J
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 5743
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->getQueue()[J

    move-result-object v0

    return-object v0
.end method

.method public getRepeat()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 5873
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->getRepeat()I

    move-result v0

    return v0
.end method

.method public getSamplingRate()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 5768
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->getSamplingRate()I

    move-result v0

    return v0
.end method

.method public getShuffle()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 5878
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->getShuffle()I

    move-result v0

    return v0
.end method

.method public getSoundAlive()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 5638
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->getSoundAlive()I

    move-result v0

    return v0
.end method

.method public getSoundAliveUserEQ()[I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 5648
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->getSoundAliveUserEQ()[I

    move-result-object v0

    return-object v0
.end method

.method public getSoundAliveUserExt()[I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 5653
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->getSoundAliveUserExt()[I

    move-result-object v0

    return-object v0
.end method

.method public getTitleName()Ljava/lang/String;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 5778
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->getTitle()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isActiveSmartVolume()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 6023
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->isActiveSmartVolume()Z
    invoke-static {v0}, Lcom/samsung/musicplus/service/PlayerService;->access$8500(Lcom/samsung/musicplus/service/PlayerService;)Z

    move-result v0

    return v0
.end method

.method public isAudioShockWarningEnabled()Z
    .locals 1

    .prologue
    .line 6056
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->isAudioShockWarningEnabled()Z

    move-result v0

    return v0
.end method

.method public isBtConnected()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 5713
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->isBtConnected()Z

    move-result v0

    return v0
.end method

.method public isConnectingWfd()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 5708
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->isConnectingWfd()Z

    move-result v0

    return v0
.end method

.method public isEnableToPlaying()Z
    .locals 1

    .prologue
    .line 6051
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->isEnableToPlaying()Z

    move-result v0

    return v0
.end method

.method public isLocalTrack()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 5693
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->isLocalMediaTrack()Z

    move-result v0

    return v0
.end method

.method public isPlaying()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 5698
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->isPlaying()Z

    move-result v0

    return v0
.end method

.method public isPreparing()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 5703
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->isPreparing()Z

    move-result v0

    return v0
.end method

.method public isSupportPlaySpeed()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 5668
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->isSupportPlaySpeed()Z

    move-result v0

    return v0
.end method

.method public loadNewSoundAliveAuto()Z
    .locals 1

    .prologue
    .line 5998
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->loadNewSoundAliveAuto()Z
    invoke-static {v0}, Lcom/samsung/musicplus/service/PlayerService;->access$8000(Lcom/samsung/musicplus/service/PlayerService;)Z

    move-result v0

    return v0
.end method

.method public loadNewSoundAliveBandLevel()[I
    .locals 1

    .prologue
    .line 5988
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->loadNewSoundAliveBandLevel()[I
    invoke-static {v0}, Lcom/samsung/musicplus/service/PlayerService;->access$7800(Lcom/samsung/musicplus/service/PlayerService;)[I

    move-result-object v0

    return-object v0
.end method

.method public loadNewSoundAliveSquarePosition()[I
    .locals 1

    .prologue
    .line 5983
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->loadNewSoundAliveSquarePosition()[I
    invoke-static {v0}, Lcom/samsung/musicplus/service/PlayerService;->access$7700(Lcom/samsung/musicplus/service/PlayerService;)[I

    move-result-object v0

    return-object v0
.end method

.method public loadNewSoundAliveStrength()[I
    .locals 1

    .prologue
    .line 5993
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->loadNewSoundAliveStrength()[I
    invoke-static {v0}, Lcom/samsung/musicplus/service/PlayerService;->access$7900(Lcom/samsung/musicplus/service/PlayerService;)[I

    move-result-object v0

    return-object v0
.end method

.method public loadNewSoundAliveUsePreset()I
    .locals 1

    .prologue
    .line 5978
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->loadNewSoundAliveUsePreset()I
    invoke-static {v0}, Lcom/samsung/musicplus/service/PlayerService;->access$7600(Lcom/samsung/musicplus/service/PlayerService;)I

    move-result v0

    return v0
.end method

.method public moveQueueItem(II)V
    .locals 1
    .param p1, "from"    # I
    .param p2, "to"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 5608
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/musicplus/service/PlayerService;->moveQueueItem(II)V

    .line 5609
    return-void
.end method

.method public next()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 5683
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/service/PlayerService;->next(Z)V

    .line 5684
    return-void
.end method

.method public openList(ILjava/lang/String;[JIZ)V
    .locals 6
    .param p1, "listType"    # I
    .param p2, "keyWord"    # Ljava/lang/String;
    .param p3, "list"    # [J
    .param p4, "position"    # I
    .param p5, "play"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 5561
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/musicplus/service/PlayerService;->open(ILjava/lang/String;[JIZ)V

    .line 5562
    return-void
.end method

.method public pause()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 5673
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->pause()V

    .line 5674
    return-void
.end method

.method public play()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 5678
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/service/PlayerService;->play(Z)V

    .line 5679
    return-void
.end method

.method public position()J
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 5723
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->position()J

    move-result-wide v0

    return-wide v0
.end method

.method public prev(Z)V
    .locals 1
    .param p1, "force"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 5688
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/service/PlayerService;->prev(Z)V

    .line 5689
    return-void
.end method

.method public refreshDlna()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 5858
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->refreshDlna()V

    .line 5859
    return-void
.end method

.method public removeTrack(J)I
    .locals 1
    .param p1, "id"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 5583
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/musicplus/service/PlayerService;->removeTrack(J)I

    move-result v0

    return v0
.end method

.method public removeTracks(II)I
    .locals 1
    .param p1, "first"    # I
    .param p2, "last"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 5593
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/musicplus/service/PlayerService;->removeTracks(II)I

    move-result v0

    return v0
.end method

.method public removeTracksAudioIds([J)I
    .locals 1
    .param p1, "ids"    # [J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 5588
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/service/PlayerService;->removeTracks([J)I

    move-result v0

    return v0
.end method

.method public removeTracksPosition([I)I
    .locals 1
    .param p1, "positions"    # [I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 5598
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/service/PlayerService;->removeTracks([I)I

    move-result v0

    return v0
.end method

.method public reorderQueue(ILjava/lang/String;[JI)V
    .locals 1
    .param p1, "listType"    # I
    .param p2, "keyWord"    # Ljava/lang/String;
    .param p3, "list"    # [J
    .param p4, "position"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 5578
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/samsung/musicplus/service/PlayerService;->reorderQueue(ILjava/lang/String;[JI)V

    .line 5579
    return-void
.end method

.method public savePreferencesBoolean(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 6013
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->savePreferences(Ljava/lang/String;Z)V
    invoke-static {v0, p1, p2}, Lcom/samsung/musicplus/service/PlayerService;->access$8300(Lcom/samsung/musicplus/service/PlayerService;Ljava/lang/String;Z)V

    .line 6014
    return-void
.end method

.method public savePreferencesFloat(Ljava/lang/String;F)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # F
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 6008
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->savePreferences(Ljava/lang/String;F)V
    invoke-static {v0, p1, p2}, Lcom/samsung/musicplus/service/PlayerService;->access$8200(Lcom/samsung/musicplus/service/PlayerService;Ljava/lang/String;F)V

    .line 6009
    return-void
.end method

.method public savePreferencesInt(Ljava/lang/String;I)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 6003
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->savePreferences(Ljava/lang/String;I)V
    invoke-static {v0, p1, p2}, Lcom/samsung/musicplus/service/PlayerService;->access$8100(Lcom/samsung/musicplus/service/PlayerService;Ljava/lang/String;I)V

    .line 6004
    return-void
.end method

.method public savePreferencesString(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 6018
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->savePreferences(Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v0, p1, p2}, Lcom/samsung/musicplus/service/PlayerService;->access$8400(Lcom/samsung/musicplus/service/PlayerService;Ljava/lang/String;Ljava/lang/String;)V

    .line 6019
    return-void
.end method

.method public seek(J)J
    .locals 3
    .param p1, "pos"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 5738
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/musicplus/service/PlayerService;->seek(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public selectDlnaDms(Ljava/lang/String;)V
    .locals 1
    .param p1, "dmsId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 5863
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/service/PlayerService;->selectDlnaDms(Ljava/lang/String;)V

    .line 5864
    return-void
.end method

.method public setAdaptSound(Z)V
    .locals 2
    .param p1, "isOn"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 5633
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/samsung/musicplus/service/PlayerService;->setAdaptSound(ZZ)V

    .line 5634
    return-void
.end method

.method public setDlnaDmrMute()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 5913
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->setDlnaDmrMute()V

    .line 5914
    return-void
.end method

.method public setK2HD(Z)V
    .locals 1
    .param p1, "isOn"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 5628
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/service/PlayerService;->setK2HD(Z)V

    .line 5629
    return-void
.end method

.method public setLimitVolume(FF)V
    .locals 1
    .param p1, "l"    # F
    .param p2, "r"    # F

    .prologue
    .line 6047
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/musicplus/service/PlayerService;->setLimitVolume(FF)V

    return-void
.end method

.method public setNewSoundAliveAuto(Z)V
    .locals 1
    .param p1, "auto"    # Z

    .prologue
    .line 5928
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/service/PlayerService;->setNewSoundAliveAuto(Z)V

    .line 5929
    return-void
.end method

.method public setNewSoundAliveBandLevel([I)V
    .locals 1
    .param p1, "bandLevel"    # [I

    .prologue
    .line 5943
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->setNewSoundAliveBandLevel([I)V
    invoke-static {v0, p1}, Lcom/samsung/musicplus/service/PlayerService;->access$7300(Lcom/samsung/musicplus/service/PlayerService;[I)V

    .line 5944
    return-void
.end method

.method public setNewSoundAliveEachStrength(II)V
    .locals 1
    .param p1, "id"    # I
    .param p2, "level"    # I

    .prologue
    .line 5953
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->setNewSoundAliveEachStrength(II)V
    invoke-static {v0, p1, p2}, Lcom/samsung/musicplus/service/PlayerService;->access$7500(Lcom/samsung/musicplus/service/PlayerService;II)V

    .line 5954
    return-void
.end method

.method public setNewSoundAliveSquarePosition([I)V
    .locals 1
    .param p1, "squarePosition"    # [I

    .prologue
    .line 5938
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->setNewSoundAliveSquarePosition([I)V
    invoke-static {v0, p1}, Lcom/samsung/musicplus/service/PlayerService;->access$7200(Lcom/samsung/musicplus/service/PlayerService;[I)V

    .line 5939
    return-void
.end method

.method public setNewSoundAliveStrength([I)V
    .locals 1
    .param p1, "level"    # [I

    .prologue
    .line 5948
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->setNewSoundAliveStrength([I)V
    invoke-static {v0, p1}, Lcom/samsung/musicplus/service/PlayerService;->access$7400(Lcom/samsung/musicplus/service/PlayerService;[I)V

    .line 5949
    return-void
.end method

.method public setNewSoundAliveUsePreset(I)V
    .locals 2
    .param p1, "mode"    # I

    .prologue
    .line 5933
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    int-to-short v1, p1

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->setNewSoundAliveUsePreset(I)V
    invoke-static {v0, v1}, Lcom/samsung/musicplus/service/PlayerService;->access$7100(Lcom/samsung/musicplus/service/PlayerService;I)V

    .line 5934
    return-void
.end method

.method public setPlaySpeed(F)V
    .locals 2
    .param p1, "speed"    # F
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 5658
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/samsung/musicplus/service/PlayerService;->setPlaySpeed(FZ)V

    .line 5659
    return-void
.end method

.method public setQueuePosition(IZ)V
    .locals 1
    .param p1, "index"    # I
    .param p2, "play"    # Z

    .prologue
    .line 5572
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/musicplus/service/PlayerService;->setQueuePosition(IZ)V

    .line 5573
    return-void
.end method

.method public setSlinkWakeLock()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 6038
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->setSlinkWakeLock()V

    .line 6039
    return-void
.end method

.method public setSmartVolume(Z)V
    .locals 1
    .param p1, "isOn"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 5623
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/service/PlayerService;->setSmartVolume(Z)V

    .line 5624
    return-void
.end method

.method public setSoundAlive(I)V
    .locals 2
    .param p1, "effect"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 5613
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/samsung/musicplus/service/PlayerService;->setSoundAlive(IZ)V

    .line 5614
    return-void
.end method

.method public setSoundAliveUser([I[I)V
    .locals 2
    .param p1, "eq"    # [I
    .param p2, "effect"    # [I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 5618
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, p2, v1}, Lcom/samsung/musicplus/service/PlayerService;->setSoundAliveUser([I[IZ)V

    .line 5619
    return-void
.end method

.method public stopMusicPackage()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 5868
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->stopMusicPackage()V

    .line 5869
    return-void
.end method

.method public toggleRepeat()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 5893
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->toggleRepeat()V

    .line 5894
    return-void
.end method

.method public toggleShuffle()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 5898
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->toggleShuffle()V

    .line 5899
    return-void
.end method
