.class public Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;
.super Ljava/lang/Object;
.source "SviewCoverDialog.java"

# interfaces
.implements Landroid/content/DialogInterface$OnShowListener;
.implements Lcom/samsung/musicplus/service/PlayerServiceStateAction;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog$OnDismissCallback;
    }
.end annotation


# static fields
.field private static final INTENT_COVER_REMOTEVIEWS_UPDATE:Ljava/lang/String; = "com.samsung.cover.REMOTEVIEWS_UPDATE"

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private mAlbumArtImageView:Landroid/widget/ImageView;

.field private mAlbumArtProgress:Landroid/widget/ProgressBar;

.field private final mAlbumArtSize:I

.field private final mAlbumArtUpdateHandler:Landroid/os/Handler;

.field private mArtistTextView:Landroid/widget/TextView;

.field private mBackgroundView:Landroid/widget/ImageView;

.field private mCloseButton:Landroid/view/View;

.field private final mContext:Landroid/content/Context;

.field private final mDialog:Landroid/app/Dialog;

.field private mDismissCallback:Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog$OnDismissCallback;

.field private mIsSetBackground:Z

.field private final mMediaListener:Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction$OnMediaListener;

.field private mNowPlayingList:Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;

.field private final mOnCoverStateChangeListener:Lcom/samsung/musicplus/library/hardware/SviewCoverManager$OnCoverStateChangeListener;

.field private final mPlayerServiceListener:Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction$OnPlayerServiceListener;

.field private mSviewCoverCommonAction:Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;

.field private mSviewCoverManager:Lcom/samsung/musicplus/library/hardware/SviewCoverManager;

.field private final mSystemListener:Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction$OnSystemListener;

.field private mTitleLayout:Landroid/view/View;

.field private mTitleTextView:Landroid/widget/TextView;

.field private mVolumeAlertLayout:Landroid/widget/FrameLayout;

.field private mVolumeAlertReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    const-class v0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .prologue
    const/4 v10, 0x0

    const v9, 0x7f0c0181

    const/4 v8, 0x1

    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 204
    new-instance v6, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog$2;

    invoke-direct {v6, p0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog$2;-><init>(Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;)V

    iput-object v6, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mAlbumArtUpdateHandler:Landroid/os/Handler;

    .line 232
    new-instance v6, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog$3;

    invoke-direct {v6, p0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog$3;-><init>(Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;)V

    iput-object v6, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mPlayerServiceListener:Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction$OnPlayerServiceListener;

    .line 270
    new-instance v6, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog$4;

    invoke-direct {v6, p0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog$4;-><init>(Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;)V

    iput-object v6, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mSystemListener:Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction$OnSystemListener;

    .line 290
    new-instance v6, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog$5;

    invoke-direct {v6, p0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog$5;-><init>(Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;)V

    iput-object v6, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mMediaListener:Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction$OnMediaListener;

    .line 298
    new-instance v6, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog$6;

    invoke-direct {v6, p0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog$6;-><init>(Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;)V

    iput-object v6, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mOnCoverStateChangeListener:Lcom/samsung/musicplus/library/hardware/SviewCoverManager$OnCoverStateChangeListener;

    .line 362
    new-instance v6, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog$7;

    invoke-direct {v6, p0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog$7;-><init>(Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;)V

    iput-object v6, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mVolumeAlertReceiver:Landroid/content/BroadcastReceiver;

    .line 92
    iput-object p1, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mContext:Landroid/content/Context;

    .line 93
    new-instance v6, Landroid/app/Dialog;

    invoke-direct {v6, p1}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    iput-object v6, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mDialog:Landroid/app/Dialog;

    .line 95
    iget-object v6, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v6, v8}, Landroid/app/Dialog;->requestWindowFeature(I)Z

    .line 96
    iget-object v6, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mDialog:Landroid/app/Dialog;

    const/4 v7, 0x0

    invoke-virtual {v6, v7}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 97
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v6

    const v7, 0x7f04008b

    invoke-virtual {v6, v7, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 98
    .local v3, "root":Landroid/view/View;
    iget-object v6, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v6, v3}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 99
    iget-object v6, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v6, p0}, Landroid/app/Dialog;->setOnShowListener(Landroid/content/DialogInterface$OnShowListener;)V

    .line 100
    invoke-direct {p0, v3}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->initView(Landroid/view/View;)V

    .line 102
    iget-object v6, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v6}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v5

    .line 103
    .local v5, "window":Landroid/view/Window;
    invoke-virtual {v5, v10}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 104
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 105
    .local v2, "resources":Landroid/content/res/Resources;
    invoke-virtual {v5}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v1

    .line 106
    .local v1, "params":Landroid/view/WindowManager$LayoutParams;
    const/4 v6, 0x0

    iput v6, v1, Landroid/view/WindowManager$LayoutParams;->verticalMargin:F

    .line 107
    const/16 v6, 0x30

    iput v6, v1, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 108
    iget v6, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    and-int/lit8 v6, v6, -0x3

    const/high16 v7, 0x80000

    or-int/2addr v6, v7

    const/high16 v7, 0x4000000

    or-int/2addr v6, v7

    iput v6, v1, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 111
    invoke-virtual {v5, v1}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 113
    new-instance v6, Lcom/samsung/musicplus/library/hardware/SviewCoverManager;

    iget-object v7, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mOnCoverStateChangeListener:Lcom/samsung/musicplus/library/hardware/SviewCoverManager$OnCoverStateChangeListener;

    invoke-direct {v6, p1, v7}, Lcom/samsung/musicplus/library/hardware/SviewCoverManager;-><init>(Landroid/content/Context;Lcom/samsung/musicplus/library/hardware/SviewCoverManager$OnCoverStateChangeListener;)V

    iput-object v6, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mSviewCoverManager:Lcom/samsung/musicplus/library/hardware/SviewCoverManager;

    .line 117
    iget-object v6, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mSviewCoverManager:Lcom/samsung/musicplus/library/hardware/SviewCoverManager;

    invoke-virtual {v6}, Lcom/samsung/musicplus/library/hardware/SviewCoverManager;->getCurrentCoverState()V

    .line 119
    iget-object v6, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mSviewCoverManager:Lcom/samsung/musicplus/library/hardware/SviewCoverManager;

    invoke-virtual {v6}, Lcom/samsung/musicplus/library/hardware/SviewCoverManager;->getScoverWidthPixel()I

    move-result v4

    .line 120
    .local v4, "w":I
    iget-object v6, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mSviewCoverManager:Lcom/samsung/musicplus/library/hardware/SviewCoverManager;

    invoke-virtual {v6}, Lcom/samsung/musicplus/library/hardware/SviewCoverManager;->getScoverHeightPixel()I

    move-result v0

    .line 121
    .local v0, "h":I
    if-lez v4, :cond_0

    if-gtz v0, :cond_1

    .line 124
    :cond_0
    invoke-virtual {v2, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 125
    const v6, 0x7f0c0180

    invoke-virtual {v2, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 127
    :cond_1
    invoke-virtual {v5, v4, v0}, Landroid/view/Window;->setLayout(II)V

    .line 129
    iget-object v6, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mSviewCoverManager:Lcom/samsung/musicplus/library/hardware/SviewCoverManager;

    invoke-virtual {v6, v5, v8}, Lcom/samsung/musicplus/library/hardware/SviewCoverManager;->setCoverModeToWindow(Landroid/view/Window;I)V

    .line 131
    invoke-virtual {v2, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    iput v6, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mAlbumArtSize:I

    .line 134
    new-instance v6, Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;

    iget-object v7, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mContext:Landroid/content/Context;

    invoke-direct {v6, v7}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;-><init>(Landroid/content/Context;)V

    iput-object v6, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mSviewCoverCommonAction:Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;

    .line 135
    iget-object v6, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mSviewCoverCommonAction:Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;

    iget-object v7, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mPlayerServiceListener:Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction$OnPlayerServiceListener;

    invoke-virtual {v6, v7}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;->setOnPlayerServiceListener(Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction$OnPlayerServiceListener;)V

    .line 136
    iget-object v6, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mSviewCoverCommonAction:Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;

    iget-object v7, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mSystemListener:Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction$OnSystemListener;

    invoke-virtual {v6, v7}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;->setOnSystemListener(Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction$OnSystemListener;)V

    .line 137
    iget-object v6, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mSviewCoverCommonAction:Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;

    iget-object v7, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mMediaListener:Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction$OnMediaListener;

    invoke-virtual {v6, v7}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;->setOnMediaListener(Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction$OnMediaListener;)V

    .line 138
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->invokeDismissCallback()V

    return-void
.end method

.method static synthetic access$100(Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->showTitleLayout()V

    return-void
.end method

.method static synthetic access$1000(Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->startMusicMainActivity()V

    return-void
.end method

.method static synthetic access$1100(Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;)Landroid/widget/FrameLayout;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mVolumeAlertLayout:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mCloseButton:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$200(Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mAlbumArtImageView:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;)Landroid/widget/ImageView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mBackgroundView:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$400(Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$500()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$600(Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 52
    invoke-direct {p0, p1, p2}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->updateSongInfo(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$700(Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;JI)V
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;
    .param p1, "x1"    # J
    .param p3, "x2"    # I

    .prologue
    .line 52
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->updateAlbumArt(JI)V

    return-void
.end method

.method static synthetic access$800(Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;)Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mNowPlayingList:Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;

    return-object v0
.end method

.method static synthetic access$900(Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;)Landroid/widget/ProgressBar;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mAlbumArtProgress:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method private initView(Landroid/view/View;)V
    .locals 4
    .param p1, "root"    # Landroid/view/View;

    .prologue
    .line 162
    new-instance v0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;

    iget-object v1, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mContext:Landroid/content/Context;

    const v2, 0x7f04008f

    const v3, 0x7f0d0109

    invoke-virtual {p1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;-><init>(Landroid/content/Context;ILandroid/view/View;)V

    iput-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mNowPlayingList:Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;

    .line 166
    const v0, 0x7f0d01b0

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mBackgroundView:Landroid/widget/ImageView;

    .line 168
    const v0, 0x7f0d00d6

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mTitleTextView:Landroid/widget/TextView;

    .line 169
    const v0, 0x7f0d00d5

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mArtistTextView:Landroid/widget/TextView;

    .line 170
    const v0, 0x7f0d0118

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mAlbumArtImageView:Landroid/widget/ImageView;

    .line 171
    const v0, 0x7f0d01b3

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mAlbumArtProgress:Landroid/widget/ProgressBar;

    .line 172
    const v0, 0x7f0d01b2

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mTitleLayout:Landroid/view/View;

    .line 173
    const v0, 0x7f0d01b4

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mCloseButton:Landroid/view/View;

    .line 175
    const v0, 0x7f0d01b5

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mVolumeAlertLayout:Landroid/widget/FrameLayout;

    .line 177
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mCloseButton:Landroid/view/View;

    new-instance v1, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog$1;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog$1;-><init>(Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 183
    return-void
.end method

.method private invokeDismissCallback()V
    .locals 1

    .prologue
    .line 349
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mDismissCallback:Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog$OnDismissCallback;

    if-eqz v0, :cond_0

    .line 350
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mDismissCallback:Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog$OnDismissCallback;

    invoke-interface {v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog$OnDismissCallback;->onDismiss()V

    .line 352
    :cond_0
    return-void
.end method

.method private isCoverOpen()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 414
    iget-object v1, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mSviewCoverManager:Lcom/samsung/musicplus/library/hardware/SviewCoverManager;

    invoke-virtual {v1}, Lcom/samsung/musicplus/library/hardware/SviewCoverManager;->getScoverSwitchState()I

    move-result v1

    if-ne v1, v0, :cond_0

    .line 415
    .local v0, "coverOpen":Z
    :goto_0
    sget-object v1, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "isCoverOpen() - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 416
    return v0

    .line 414
    .end local v0    # "coverOpen":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isMusicActivityInLastStack()Z
    .locals 8

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 320
    iget-object v3, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mContext:Landroid/content/Context;

    const-string v6, "activity"

    invoke-virtual {v3, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 323
    .local v0, "am":Landroid/app/ActivityManager;
    const/4 v3, 0x2

    invoke-virtual {v0, v3}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v2

    .line 325
    .local v2, "taskInfo":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    if-eqz v2, :cond_1

    .line 326
    sget-object v6, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "isMusicActivityInLastStack() Previous Activity ::"

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v3, v3, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    invoke-virtual {v3}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v6, v3}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 330
    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v1, v3, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    .line 332
    .local v1, "componentInfo":Landroid/content/ComponentName;
    iget-object v3, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v3

    const-string v6, "com.samsung.musicplus.service.sviewcover.SviewCoverActivity"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v3, v4

    .line 345
    .end local v1    # "componentInfo":Landroid/content/ComponentName;
    :goto_0
    return v3

    .restart local v1    # "componentInfo":Landroid/content/ComponentName;
    :cond_0
    move v3, v5

    .line 340
    goto :goto_0

    .end local v1    # "componentInfo":Landroid/content/ComponentName;
    :cond_1
    move v3, v5

    .line 345
    goto :goto_0
.end method

.method private showTitleLayout()V
    .locals 2

    .prologue
    .line 226
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mTitleLayout:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    .line 227
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mTitleLayout:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 230
    :cond_0
    return-void
.end method

.method private startMusicMainActivity()V
    .locals 4

    .prologue
    .line 313
    invoke-direct {p0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->invokeDismissCallback()V

    .line 314
    invoke-direct {p0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->isMusicActivityInLastStack()Z

    move-result v0

    if-nez v0, :cond_0

    .line 315
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mContext:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mContext:Landroid/content/Context;

    const-class v3, Lcom/samsung/musicplus/MusicMainActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 317
    :cond_0
    return-void
.end method

.method private updateAlbumArt(JI)V
    .locals 7
    .param p1, "artId"    # J
    .param p3, "listType"    # I

    .prologue
    .line 194
    invoke-static {}, Lcom/samsung/musicplus/util/AlbumArtLoader;->getAlbumArtLoader()Lcom/samsung/musicplus/util/IAlbumArtLoader;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mContext:Landroid/content/Context;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iget v4, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mAlbumArtSize:I

    iget-object v5, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mAlbumArtUpdateHandler:Landroid/os/Handler;

    move v2, p3

    invoke-interface/range {v0 .. v5}, Lcom/samsung/musicplus/util/IAlbumArtLoader;->loadArtwork(Landroid/content/Context;ILjava/lang/Object;ILandroid/os/Handler;)V

    .line 196
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->isLocalTrack()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->isPreparing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 197
    invoke-direct {p0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->showTitleLayout()V

    .line 198
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mAlbumArtProgress:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 199
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mAlbumArtImageView:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mContext:Landroid/content/Context;

    const v2, 0x7f02003b

    invoke-static {v1, v2}, Lcom/samsung/musicplus/util/AlbumArtUtils;->getDefaulAlbumArt(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 200
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mBackgroundView:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mContext:Landroid/content/Context;

    const v2, 0x7f020039

    invoke-static {v1, v2}, Lcom/samsung/musicplus/util/AlbumArtUtils;->getDefaulAlbumArt(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 202
    :cond_0
    return-void
.end method

.method private updateSongInfo(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "artist"    # Ljava/lang/String;

    .prologue
    .line 186
    sget-object v0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "updateSongInfo() title: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " artist: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    invoke-direct {p0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->showTitleLayout()V

    .line 189
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mTitleTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 190
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mArtistTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 191
    return-void
.end method


# virtual methods
.method public dismiss()V
    .locals 2

    .prologue
    .line 153
    sget-object v0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->TAG:Ljava/lang/String;

    const-string v1, "dismiss()"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mSviewCoverCommonAction:Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;->unregisterReceiver()V

    .line 155
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mSviewCoverManager:Lcom/samsung/musicplus/library/hardware/SviewCoverManager;

    invoke-virtual {v0}, Lcom/samsung/musicplus/library/hardware/SviewCoverManager;->unregisterListener()V

    .line 156
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mVolumeAlertReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 157
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mNowPlayingList:Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;->onDestroy()V

    .line 158
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 159
    return-void
.end method

.method public onShow(Landroid/content/DialogInterface;)V
    .locals 1
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 407
    invoke-direct {p0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->isCoverOpen()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 409
    invoke-direct {p0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->startMusicMainActivity()V

    .line 411
    :cond_0
    return-void
.end method

.method public setOnDismissCallback(Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog$OnDismissCallback;)V
    .locals 0
    .param p1, "callback"    # Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog$OnDismissCallback;

    .prologue
    .line 359
    iput-object p1, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mDismissCallback:Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog$OnDismissCallback;

    .line 360
    return-void
.end method

.method public show()V
    .locals 4

    .prologue
    .line 142
    sget-object v0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->TAG:Ljava/lang/String;

    const-string v1, "show()"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 143
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    .line 144
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mNowPlayingList:Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;->show()V

    .line 145
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mSviewCoverCommonAction:Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;->registerReceiver()V

    .line 146
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mSviewCoverManager:Lcom/samsung/musicplus/library/hardware/SviewCoverManager;

    invoke-virtual {v0}, Lcom/samsung/musicplus/library/hardware/SviewCoverManager;->registerListener()V

    .line 147
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mContext:Landroid/content/Context;

    iget-object v1, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mVolumeAlertReceiver:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "com.samsung.cover.REMOTEVIEWS_UPDATE"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 149
    return-void
.end method
