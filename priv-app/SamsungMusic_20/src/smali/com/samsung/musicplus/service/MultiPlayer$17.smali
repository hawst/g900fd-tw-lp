.class Lcom/samsung/musicplus/service/MultiPlayer$17;
.super Ljava/lang/Object;
.source "MultiPlayer.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnPreparedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/service/MultiPlayer;->prepareNextMedia(Landroid/net/Uri;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/service/MultiPlayer;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/service/MultiPlayer;)V
    .locals 0

    .prologue
    .line 1956
    iput-object p1, p0, Lcom/samsung/musicplus/service/MultiPlayer$17;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 3
    .param p1, "mp"    # Landroid/media/MediaPlayer;

    .prologue
    .line 1961
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer$17;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # getter for: Lcom/samsung/musicplus/service/MultiPlayer;->mNextMedia:Lcom/samsung/musicplus/widget/NextMediaPlayer;
    invoke-static {v0}, Lcom/samsung/musicplus/service/MultiPlayer;->access$100(Lcom/samsung/musicplus/service/MultiPlayer;)Lcom/samsung/musicplus/widget/NextMediaPlayer;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1962
    const-string v0, "MusicPlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "NextMediaPlayer mNextMedia() prepared uri : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/service/MultiPlayer$17;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # getter for: Lcom/samsung/musicplus/service/MultiPlayer;->mNextMedia:Lcom/samsung/musicplus/widget/NextMediaPlayer;
    invoke-static {v2}, Lcom/samsung/musicplus/service/MultiPlayer;->access$100(Lcom/samsung/musicplus/service/MultiPlayer;)Lcom/samsung/musicplus/widget/NextMediaPlayer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/musicplus/widget/NextMediaPlayer;->getUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1965
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer$17;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # getter for: Lcom/samsung/musicplus/service/MultiPlayer;->mNextMedia:Lcom/samsung/musicplus/widget/NextMediaPlayer;
    invoke-static {v0}, Lcom/samsung/musicplus/service/MultiPlayer;->access$100(Lcom/samsung/musicplus/service/MultiPlayer;)Lcom/samsung/musicplus/widget/NextMediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/musicplus/widget/NextMediaPlayer;->setPrepared()V

    .line 1967
    :cond_0
    return-void
.end method
