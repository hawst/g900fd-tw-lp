.class Lcom/samsung/musicplus/service/MultiPlayer$11;
.super Ljava/lang/Object;
.source "MultiPlayer.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/service/MultiPlayer;->start()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/service/MultiPlayer;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/service/MultiPlayer;)V
    .locals 0

    .prologue
    .line 930
    iput-object p1, p0, Lcom/samsung/musicplus/service/MultiPlayer$11;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    const/4 v3, 0x1

    .line 935
    const-wide/16 v4, 0x0

    .line 936
    .local v4, "position":J
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer$11;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # getter for: Lcom/samsung/musicplus/service/MultiPlayer;->mIsDmrErrorState:Z
    invoke-static {v0}, Lcom/samsung/musicplus/service/MultiPlayer;->access$2600(Lcom/samsung/musicplus/service/MultiPlayer;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer$11;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # getter for: Lcom/samsung/musicplus/service/MultiPlayer;->mDmrPlayer:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;
    invoke-static {v0}, Lcom/samsung/musicplus/service/MultiPlayer;->access$2700(Lcom/samsung/musicplus/service/MultiPlayer;)Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 937
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer$11;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # getter for: Lcom/samsung/musicplus/service/MultiPlayer;->mDmrPlayer:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;
    invoke-static {v0}, Lcom/samsung/musicplus/service/MultiPlayer;->access$2700(Lcom/samsung/musicplus/service/MultiPlayer;)Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->getCurrentPosition()J

    move-result-wide v4

    .line 939
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer$11;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    iget-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer$11;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # getter for: Lcom/samsung/musicplus/service/MultiPlayer;->mDmrId:Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/musicplus/service/MultiPlayer;->access$2800(Lcom/samsung/musicplus/service/MultiPlayer;)Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/samsung/musicplus/service/MultiPlayer;->createDmrPlayer(Ljava/lang/String;)Z
    invoke-static {v0, v1}, Lcom/samsung/musicplus/service/MultiPlayer;->access$2900(Lcom/samsung/musicplus/service/MultiPlayer;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 940
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer$11;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    const/4 v1, 0x0

    # invokes: Lcom/samsung/musicplus/service/MultiPlayer;->handleDmrError(Z)V
    invoke-static {v0, v1}, Lcom/samsung/musicplus/service/MultiPlayer;->access$3000(Lcom/samsung/musicplus/service/MultiPlayer;Z)V

    .line 947
    :goto_0
    return-void

    .line 944
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer$11;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # setter for: Lcom/samsung/musicplus/service/MultiPlayer;->mCurrentState:I
    invoke-static {v0, v3}, Lcom/samsung/musicplus/service/MultiPlayer;->access$1202(Lcom/samsung/musicplus/service/MultiPlayer;I)I

    .line 945
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer$11;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    const/4 v1, 0x2

    # setter for: Lcom/samsung/musicplus/service/MultiPlayer;->mMode:I
    invoke-static {v0, v1}, Lcom/samsung/musicplus/service/MultiPlayer;->access$3102(Lcom/samsung/musicplus/service/MultiPlayer;I)I

    .line 946
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer$11;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    iget-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer$11;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # getter for: Lcom/samsung/musicplus/service/MultiPlayer;->mUri:Landroid/net/Uri;
    invoke-static {v1}, Lcom/samsung/musicplus/service/MultiPlayer;->access$2300(Lcom/samsung/musicplus/service/MultiPlayer;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/service/MultiPlayer$11;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # getter for: Lcom/samsung/musicplus/service/MultiPlayer;->mPath:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/musicplus/service/MultiPlayer;->access$2200(Lcom/samsung/musicplus/service/MultiPlayer;)Ljava/lang/String;

    move-result-object v2

    # invokes: Lcom/samsung/musicplus/service/MultiPlayer;->setDataSource(Landroid/net/Uri;Ljava/lang/String;ZJ)Z
    invoke-static/range {v0 .. v5}, Lcom/samsung/musicplus/service/MultiPlayer;->access$600(Lcom/samsung/musicplus/service/MultiPlayer;Landroid/net/Uri;Ljava/lang/String;ZJ)Z

    goto :goto_0
.end method
