.class public Lcom/samsung/musicplus/service/MediaButtonReceiver;
.super Landroid/content/BroadcastReceiver;
.source "MediaButtonReceiver.java"


# static fields
.field private static ACCEPT_ONLY_DOWN_UP:Z = false

.field private static final ACTION_MUSIC_BUTTON:Ljava/lang/String; = "android.intent.action.MUSIC_BUTTON"

.field public static final TAG:Ljava/lang/String; = "MusicButton"

.field private static sControlTask:Lcom/samsung/musicplus/widget/control/FowardRewindControlTask;

.field private static sDown:Z

.field private static sLastClickTime:J

.field private static sLastHookClickTime:J


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 28
    const/4 v0, 0x0

    sput-boolean v0, Lcom/samsung/musicplus/service/MediaButtonReceiver;->sDown:Z

    .line 33
    sput-wide v2, Lcom/samsung/musicplus/service/MediaButtonReceiver;->sLastHookClickTime:J

    .line 38
    sput-wide v2, Lcom/samsung/musicplus/service/MediaButtonReceiver;->sLastClickTime:J

    .line 150
    const/4 v0, 0x1

    sput-boolean v0, Lcom/samsung/musicplus/service/MediaButtonReceiver;->ACCEPT_ONLY_DOWN_UP:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method private getMusicCommand(II)Ljava/lang/String;
    .locals 1
    .param p1, "keycode"    # I
    .param p2, "action"    # I

    .prologue
    .line 212
    const/4 v0, 0x0

    .line 213
    .local v0, "command":Ljava/lang/String;
    sparse-switch p1, :sswitch_data_0

    .line 242
    :goto_0
    return-object v0

    .line 216
    :sswitch_0
    const-string v0, "togglepause"

    .line 217
    goto :goto_0

    .line 219
    :sswitch_1
    const-string v0, "play"

    .line 220
    goto :goto_0

    .line 222
    :sswitch_2
    const-string v0, "pause"

    .line 223
    goto :goto_0

    .line 225
    :sswitch_3
    const-string v0, "next"

    .line 226
    goto :goto_0

    .line 228
    :sswitch_4
    const-string v0, "previous"

    .line 229
    goto :goto_0

    .line 231
    :sswitch_5
    const-string v0, "rewind"

    .line 232
    goto :goto_0

    .line 234
    :sswitch_6
    const-string v0, "fastforward"

    .line 235
    goto :goto_0

    .line 237
    :sswitch_7
    const-string v0, "stop"

    .line 238
    goto :goto_0

    .line 213
    nop

    :sswitch_data_0
    .sparse-switch
        0x4f -> :sswitch_0
        0x55 -> :sswitch_0
        0x56 -> :sswitch_7
        0x57 -> :sswitch_3
        0x58 -> :sswitch_4
        0x59 -> :sswitch_5
        0x5a -> :sswitch_6
        0x7e -> :sswitch_1
        0x7f -> :sswitch_2
    .end sparse-switch
.end method

.method private handleForwardRewind(Landroid/content/Context;Landroid/view/KeyEvent;ILjava/lang/String;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "event"    # Landroid/view/KeyEvent;
    .param p3, "action"    # I
    .param p4, "command"    # Ljava/lang/String;

    .prologue
    const-wide/16 v8, 0x0

    const/4 v4, 0x2

    const/4 v7, 0x0

    const/4 v1, 0x1

    .line 159
    sget-boolean v5, Lcom/samsung/musicplus/service/MediaButtonReceiver;->ACCEPT_ONLY_DOWN_UP:Z

    if-eqz v5, :cond_5

    .line 160
    if-nez p3, :cond_3

    .line 166
    sget-boolean v5, Lcom/samsung/musicplus/service/MediaButtonReceiver;->sDown:Z

    if-nez v5, :cond_1

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v5

    if-nez v5, :cond_1

    .line 167
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getEventTime()J

    move-result-wide v2

    .line 169
    .local v2, "time":J
    sput-boolean v1, Lcom/samsung/musicplus/service/MediaButtonReceiver;->sDown:Z

    .line 170
    sput-wide v2, Lcom/samsung/musicplus/service/MediaButtonReceiver;->sLastClickTime:J

    .line 171
    sget-object v5, Lcom/samsung/musicplus/service/MediaButtonReceiver;->sControlTask:Lcom/samsung/musicplus/widget/control/FowardRewindControlTask;

    if-eqz v5, :cond_0

    .line 172
    sget-object v5, Lcom/samsung/musicplus/service/MediaButtonReceiver;->sControlTask:Lcom/samsung/musicplus/widget/control/FowardRewindControlTask;

    invoke-virtual {v5}, Lcom/samsung/musicplus/widget/control/FowardRewindControlTask;->setCancel()V

    .line 174
    :cond_0
    const-string v5, "fastforward"

    invoke-virtual {v5, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    move v0, v1

    .line 176
    .local v0, "direction":I
    :goto_0
    new-instance v5, Lcom/samsung/musicplus/widget/control/FowardRewindControlTaskMediaButton;

    invoke-direct {v5, p1, v0}, Lcom/samsung/musicplus/widget/control/FowardRewindControlTaskMediaButton;-><init>(Landroid/content/Context;I)V

    sput-object v5, Lcom/samsung/musicplus/service/MediaButtonReceiver;->sControlTask:Lcom/samsung/musicplus/widget/control/FowardRewindControlTask;

    .line 177
    sget-object v5, Lcom/samsung/musicplus/service/MediaButtonReceiver;->sControlTask:Lcom/samsung/musicplus/widget/control/FowardRewindControlTask;

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    aput-object p1, v6, v7

    const/4 v7, 0x0

    aput-object v7, v6, v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v6, v4

    invoke-virtual {v5, v6}, Lcom/samsung/musicplus/widget/control/FowardRewindControlTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 209
    .end local v0    # "direction":I
    .end local v2    # "time":J
    :cond_1
    :goto_1
    return-void

    .restart local v2    # "time":J
    :cond_2
    move v0, v4

    .line 174
    goto :goto_0

    .line 180
    .end local v2    # "time":J
    :cond_3
    sget-object v1, Lcom/samsung/musicplus/service/MediaButtonReceiver;->sControlTask:Lcom/samsung/musicplus/widget/control/FowardRewindControlTask;

    if-eqz v1, :cond_4

    .line 181
    sget-object v1, Lcom/samsung/musicplus/service/MediaButtonReceiver;->sControlTask:Lcom/samsung/musicplus/widget/control/FowardRewindControlTask;

    invoke-virtual {v1}, Lcom/samsung/musicplus/widget/control/FowardRewindControlTask;->setCancel()V

    .line 183
    :cond_4
    sput-boolean v7, Lcom/samsung/musicplus/service/MediaButtonReceiver;->sDown:Z

    .line 186
    sput-wide v8, Lcom/samsung/musicplus/service/MediaButtonReceiver;->sLastClickTime:J

    goto :goto_1

    .line 189
    :cond_5
    if-nez p3, :cond_7

    .line 190
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getEventTime()J

    move-result-wide v2

    .line 191
    .restart local v2    # "time":J
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v4

    if-nez v4, :cond_6

    .line 192
    sput-boolean v1, Lcom/samsung/musicplus/service/MediaButtonReceiver;->sDown:Z

    .line 193
    invoke-static {p1, p4}, Lcom/samsung/musicplus/util/player/PlayUtils;->startCommand(Landroid/content/Context;Ljava/lang/String;)V

    .line 194
    sput-wide v2, Lcom/samsung/musicplus/service/MediaButtonReceiver;->sLastClickTime:J

    .line 196
    :cond_6
    sget-boolean v1, Lcom/samsung/musicplus/service/MediaButtonReceiver;->sDown:Z

    if-eqz v1, :cond_1

    sget-wide v4, Lcom/samsung/musicplus/service/MediaButtonReceiver;->sLastClickTime:J

    sub-long v4, v2, v4

    const-wide/16 v6, 0x190

    cmp-long v1, v4, v6

    if-lez v1, :cond_1

    .line 199
    invoke-static {p1, p4}, Lcom/samsung/musicplus/util/player/PlayUtils;->startCommand(Landroid/content/Context;Ljava/lang/String;)V

    .line 200
    sput-wide v2, Lcom/samsung/musicplus/service/MediaButtonReceiver;->sLastClickTime:J

    goto :goto_1

    .line 203
    .end local v2    # "time":J
    :cond_7
    sput-boolean v7, Lcom/samsung/musicplus/service/MediaButtonReceiver;->sDown:Z

    .line 206
    sput-wide v8, Lcom/samsung/musicplus/service/MediaButtonReceiver;->sLastClickTime:J

    goto :goto_1
.end method

.method private handleMediaKey(Landroid/content/Context;Landroid/view/KeyEvent;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 54
    if-nez p2, :cond_1

    .line 100
    :cond_0
    :goto_0
    return-void

    .line 58
    :cond_1
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v3

    .line 59
    .local v3, "keycode":I
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v4

    .line 61
    .local v4, "action":I
    invoke-direct {p0, v3, v4}, Lcom/samsung/musicplus/service/MediaButtonReceiver;->getMusicCommand(II)Ljava/lang/String;

    move-result-object v5

    .line 62
    .local v5, "command":Ljava/lang/String;
    const-string v0, "MusicButton"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleMediaKey() keycode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " action : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " command : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    if-eqz v5, :cond_0

    .line 70
    const-string v0, "audio"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/media/AudioManager;

    .line 71
    .local v6, "am":Landroid/media/AudioManager;
    invoke-virtual {v6}, Landroid/media/AudioManager;->isBluetoothA2dpOn()Z

    move-result v0

    if-nez v0, :cond_2

    .line 86
    const-string v0, "MusicButton"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isBluetoothA2dpOn() is false, playing with speaker and key code : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 90
    :cond_2
    const/16 v0, 0x59

    if-eq v3, v0, :cond_3

    const/16 v0, 0x5a

    if-ne v3, v0, :cond_4

    .line 92
    :cond_3
    invoke-direct {p0, p1, p2, v4, v5}, Lcom/samsung/musicplus/service/MediaButtonReceiver;->handleForwardRewind(Landroid/content/Context;Landroid/view/KeyEvent;ILjava/lang/String;)V

    .line 97
    :goto_1
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/MediaButtonReceiver;->isOrderedBroadcast()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 98
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/MediaButtonReceiver;->abortBroadcast()V

    goto :goto_0

    :cond_4
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    .line 94
    invoke-direct/range {v0 .. v5}, Lcom/samsung/musicplus/service/MediaButtonReceiver;->handleNormalKey(Landroid/content/Context;Landroid/view/KeyEvent;IILjava/lang/String;)V

    goto :goto_1
.end method

.method private handleMusicKey(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 254
    return-void
.end method

.method private handleNormalKey(Landroid/content/Context;Landroid/view/KeyEvent;IILjava/lang/String;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "event"    # Landroid/view/KeyEvent;
    .param p3, "keycode"    # I
    .param p4, "action"    # I
    .param p5, "command"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x1

    .line 104
    if-nez p4, :cond_4

    .line 105
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v2

    if-nez v2, :cond_1

    .line 106
    sget-boolean v2, Lcom/samsung/musicplus/service/MediaButtonReceiver;->ACCEPT_ONLY_DOWN_UP:Z

    if-eqz v2, :cond_0

    .line 108
    sget-object v2, Lcom/samsung/musicplus/service/MediaButtonReceiver;->sControlTask:Lcom/samsung/musicplus/widget/control/FowardRewindControlTask;

    if-eqz v2, :cond_0

    .line 109
    sget-object v2, Lcom/samsung/musicplus/service/MediaButtonReceiver;->sControlTask:Lcom/samsung/musicplus/widget/control/FowardRewindControlTask;

    invoke-virtual {v2}, Lcom/samsung/musicplus/widget/control/FowardRewindControlTask;->setCancel()V

    .line 112
    :cond_0
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getEventTime()J

    move-result-wide v0

    .line 123
    .local v0, "time":J
    const/16 v2, 0x4f

    if-ne p3, v2, :cond_3

    .line 124
    sget-wide v2, Lcom/samsung/musicplus/service/MediaButtonReceiver;->sLastHookClickTime:J

    sub-long v2, v0, v2

    const-wide/16 v4, 0x12c

    cmp-long v2, v2, v4

    if-gez v2, :cond_2

    .line 127
    const-string v2, "next"

    invoke-static {p1, v2, v6}, Lcom/samsung/musicplus/util/player/PlayUtils;->startCommand(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 128
    const-wide/16 v2, 0x0

    sput-wide v2, Lcom/samsung/musicplus/service/MediaButtonReceiver;->sLastHookClickTime:J

    .line 136
    :goto_0
    sput-boolean v6, Lcom/samsung/musicplus/service/MediaButtonReceiver;->sDown:Z

    .line 141
    .end local v0    # "time":J
    :cond_1
    :goto_1
    return-void

    .line 130
    .restart local v0    # "time":J
    :cond_2
    invoke-static {p1, p5}, Lcom/samsung/musicplus/util/player/PlayUtils;->startCommand(Landroid/content/Context;Ljava/lang/String;)V

    .line 131
    sput-wide v0, Lcom/samsung/musicplus/service/MediaButtonReceiver;->sLastHookClickTime:J

    goto :goto_0

    .line 134
    :cond_3
    invoke-static {p1, p5}, Lcom/samsung/musicplus/util/player/PlayUtils;->startCommand(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 139
    .end local v0    # "time":J
    :cond_4
    const/4 v2, 0x0

    sput-boolean v2, Lcom/samsung/musicplus/service/MediaButtonReceiver;->sDown:Z

    goto :goto_1
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 42
    const-string v2, "MusicButton"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onReceive() intent  : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 43
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 44
    .local v0, "action":Ljava/lang/String;
    const-string v2, "android.intent.action.MEDIA_BUTTON"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "com.samsung.android.intent.action.MEDIA_BUTTON"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 46
    :cond_0
    const-string v2, "android.intent.extra.KEY_EVENT"

    invoke-virtual {p2, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/view/KeyEvent;

    .line 47
    .local v1, "event":Landroid/view/KeyEvent;
    invoke-direct {p0, p1, v1}, Lcom/samsung/musicplus/service/MediaButtonReceiver;->handleMediaKey(Landroid/content/Context;Landroid/view/KeyEvent;)V

    .line 51
    .end local v1    # "event":Landroid/view/KeyEvent;
    :cond_1
    :goto_0
    return-void

    .line 48
    :cond_2
    const-string v2, "android.intent.action.MUSIC_BUTTON"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 49
    invoke-direct {p0, p1, p2}, Lcom/samsung/musicplus/service/MediaButtonReceiver;->handleMusicKey(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_0
.end method
