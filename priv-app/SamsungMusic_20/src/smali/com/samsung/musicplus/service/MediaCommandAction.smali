.class public interface abstract Lcom/samsung/musicplus/service/MediaCommandAction;
.super Ljava/lang/Object;
.source "MediaCommandAction.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/service/MediaCommandAction$CommandMood;,
        Lcom/samsung/musicplus/service/MediaCommandAction$CommandVia;,
        Lcom/samsung/musicplus/service/MediaCommandAction$CommandExtra;
    }
.end annotation


# static fields
.field public static final ACTION_NEXT:Ljava/lang/String; = "com.sec.android.app.music.musicservicecommand.next"

.field public static final ACTION_PAUSE:Ljava/lang/String; = "com.sec.android.app.music.musicservicecommand.pause"

.field public static final ACTION_PLAY:Ljava/lang/String; = "com.sec.android.app.music.musicservicecommand.play"

.field public static final ACTION_PLAY_BY_MOOD:Ljava/lang/String; = "com.sec.android.app.music.intent.action.PLAY_BY_MOOD"

.field public static final ACTION_PLAY_CONTENTS:Ljava/lang/String; = "com.samsung.musicplus.intent.action.PLAY_CONTENTS"

.field public static final ACTION_PLAY_NEXT:Ljava/lang/String; = "com.sec.android.app.music.musicservicecommand.playnext"

.field public static final ACTION_PLAY_PREVIOUS:Ljava/lang/String; = "com.sec.android.app.music.musicservicecommand.playprevious"

.field public static final ACTION_PLAY_VIA:Ljava/lang/String; = "com.sec.android.app.music.intent.action.PLAY_VIA"

.field public static final ACTION_PREV:Ljava/lang/String; = "com.sec.android.app.music.musicservicecommand.prev"

.field public static final ACTION_REQUEST_MUSIC_INFO:Ljava/lang/String; = "com.sec.android.app.music.musicservicecommand.checkplaystatus"

.field public static final ACTION_TOGGLE_PAUSE:Ljava/lang/String; = "com.sec.android.app.music.musicservicecommand.togglepause"
