.class Lcom/samsung/musicplus/service/MultiPlayer$8;
.super Ljava/lang/Object;
.source "MultiPlayer.java"

# interfaces
.implements Lcom/samsung/musicplus/account/SamsungAccountHelper$OnAccountInfoListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/service/MultiPlayer;-><init>(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private mInitialized:Z

.field final synthetic this$0:Lcom/samsung/musicplus/service/MultiPlayer;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/service/MultiPlayer;)V
    .locals 1

    .prologue
    .line 434
    iput-object p1, p0, Lcom/samsung/musicplus/service/MultiPlayer$8;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 438
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/service/MultiPlayer$8;->mInitialized:Z

    return-void
.end method

.method private initializeScsConnection()V
    .locals 6

    .prologue
    .line 451
    iget-object v3, p0, Lcom/samsung/musicplus/service/MultiPlayer$8;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # getter for: Lcom/samsung/musicplus/service/MultiPlayer;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/samsung/musicplus/service/MultiPlayer;->access$2100(Lcom/samsung/musicplus/service/MultiPlayer;)Landroid/content/Context;

    move-result-object v3

    const-string v4, "ntcl://Null?NTSCoreStart?"

    iget-object v5, p0, Lcom/samsung/musicplus/service/MultiPlayer$8;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # getter for: Lcom/samsung/musicplus/service/MultiPlayer;->mInfo:Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;
    invoke-static {v5}, Lcom/samsung/musicplus/service/MultiPlayer;->access$2000(Lcom/samsung/musicplus/service/MultiPlayer;)Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/samsung/musicplus/provider/MusicContents$Audio$DeviceContents;->getScsConfigPath(Landroid/content/Context;Ljava/lang/String;Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;)Ljava/lang/String;

    move-result-object v1

    .line 453
    .local v1, "path":Ljava/lang/String;
    const-string v3, "MusicPlayer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "initializeScsConnection path : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 454
    new-instance v2, Landroid/media/MediaPlayer;

    invoke-direct {v2}, Landroid/media/MediaPlayer;-><init>()V

    .line 456
    .local v2, "player":Landroid/media/MediaPlayer;
    :try_start_0
    invoke-virtual {v2, v1}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V

    .line 457
    iget-object v3, p0, Lcom/samsung/musicplus/service/MultiPlayer$8;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    invoke-virtual {v3}, Lcom/samsung/musicplus/service/MultiPlayer;->setParamForK2HD()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 468
    invoke-virtual {v2}, Landroid/media/MediaPlayer;->release()V

    .line 469
    const/4 v2, 0x0

    .line 471
    :goto_0
    return-void

    .line 458
    :catch_0
    move-exception v0

    .line 459
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    :try_start_1
    const-string v3, "MusicPlayer"

    const-string v4, "initializeScsConnection IllegalArgumentException"

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 468
    invoke-virtual {v2}, Landroid/media/MediaPlayer;->release()V

    .line 469
    const/4 v2, 0x0

    .line 470
    goto :goto_0

    .line 460
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 461
    .local v0, "e":Ljava/lang/SecurityException;
    :try_start_2
    const-string v3, "MusicPlayer"

    const-string v4, "initializeScsConnection SecurityException"

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 468
    invoke-virtual {v2}, Landroid/media/MediaPlayer;->release()V

    .line 469
    const/4 v2, 0x0

    .line 470
    goto :goto_0

    .line 462
    .end local v0    # "e":Ljava/lang/SecurityException;
    :catch_2
    move-exception v0

    .line 463
    .local v0, "e":Ljava/lang/IllegalStateException;
    :try_start_3
    const-string v3, "MusicPlayer"

    const-string v4, "initializeScsConnection IllegalStateException"

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 468
    invoke-virtual {v2}, Landroid/media/MediaPlayer;->release()V

    .line 469
    const/4 v2, 0x0

    .line 470
    goto :goto_0

    .line 464
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_3
    move-exception v0

    .line 465
    .local v0, "e":Ljava/io/IOException;
    :try_start_4
    const-string v3, "MusicPlayer"

    const-string v4, "initializeScsConnection IOException"

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 468
    invoke-virtual {v2}, Landroid/media/MediaPlayer;->release()V

    .line 469
    const/4 v2, 0x0

    .line 470
    goto :goto_0

    .line 468
    .end local v0    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v3

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->release()V

    .line 469
    const/4 v2, 0x0

    throw v3
.end method


# virtual methods
.method public onAccountInfo(Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;)V
    .locals 3
    .param p1, "info"    # Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;

    .prologue
    .line 442
    const-string v0, "MusicPlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onAccountInfo info "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 443
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer$8;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # setter for: Lcom/samsung/musicplus/service/MultiPlayer;->mInfo:Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;
    invoke-static {v0, p1}, Lcom/samsung/musicplus/service/MultiPlayer;->access$2002(Lcom/samsung/musicplus/service/MultiPlayer;Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;)Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;

    .line 444
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer$8;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # getter for: Lcom/samsung/musicplus/service/MultiPlayer;->mInfo:Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;
    invoke-static {v0}, Lcom/samsung/musicplus/service/MultiPlayer;->access$2000(Lcom/samsung/musicplus/service/MultiPlayer;)Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/musicplus/service/MultiPlayer$8;->mInitialized:Z

    if-nez v0, :cond_0

    .line 445
    invoke-direct {p0}, Lcom/samsung/musicplus/service/MultiPlayer$8;->initializeScsConnection()V

    .line 446
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/service/MultiPlayer$8;->mInitialized:Z

    .line 448
    :cond_0
    return-void
.end method
