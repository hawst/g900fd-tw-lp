.class public abstract Lcom/samsung/musicplus/service/IPlayerService$Stub;
.super Landroid/os/Binder;
.source "IPlayerService.java"

# interfaces
.implements Lcom/samsung/musicplus/service/IPlayerService;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/service/IPlayerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/service/IPlayerService$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.samsung.musicplus.service.IPlayerService"

.field static final TRANSACTION_buffering:I = 0x1e

.field static final TRANSACTION_changeListWithoutPlaying:I = 0x2

.field static final TRANSACTION_changeToDefaultPlayer:I = 0x40

.field static final TRANSACTION_changeToDmrPlayer:I = 0x3f

.field static final TRANSACTION_changeToWfdDevice:I = 0x41

.field static final TRANSACTION_dlnaDmrVolumeDown:I = 0x48

.field static final TRANSACTION_dlnaDmrVolumeUp:I = 0x47

.field static final TRANSACTION_duration:I = 0x1c

.field static final TRANSACTION_enqueue:I = 0x9

.field static final TRANSACTION_getAlbumArt:I = 0x35

.field static final TRANSACTION_getAlbumId:I = 0x33

.field static final TRANSACTION_getAlbumName:I = 0x32

.field static final TRANSACTION_getArtistName:I = 0x31

.field static final TRANSACTION_getAudioId:I = 0x2a

.field static final TRANSACTION_getAudioSessionId:I = 0x62

.field static final TRANSACTION_getBitDepth:I = 0x2e

.field static final TRANSACTION_getCurrentBaseUri:I = 0x38

.field static final TRANSACTION_getCurrentPath:I = 0x2b

.field static final TRANSACTION_getCurrentUri:I = 0x37

.field static final TRANSACTION_getDeviceId:I = 0x36

.field static final TRANSACTION_getDlnaPlayingDmrId:I = 0x45

.field static final TRANSACTION_getDlnaPlayingNic:I = 0x44

.field static final TRANSACTION_getGenre:I = 0x34

.field static final TRANSACTION_getKeyWord:I = 0x29

.field static final TRANSACTION_getListItemCount:I = 0x26

.field static final TRANSACTION_getListPosition:I = 0x27

.field static final TRANSACTION_getListType:I = 0x28

.field static final TRANSACTION_getMediaType:I = 0x2c

.field static final TRANSACTION_getMimeType:I = 0x2d

.field static final TRANSACTION_getMoodValue:I = 0x5e

.field static final TRANSACTION_getNewSoundAliveBandLevel:I = 0x52

.field static final TRANSACTION_getNewSoundAliveCurrentPreset:I = 0x50

.field static final TRANSACTION_getNewSoundAliveRoundedStrength:I = 0x53

.field static final TRANSACTION_getNewSoundAliveSquarePosition:I = 0x51

.field static final TRANSACTION_getNextUri:I = 0x39

.field static final TRANSACTION_getOrganizedQueue:I = 0x25

.field static final TRANSACTION_getPersonalMode:I = 0x60

.field static final TRANSACTION_getPlaySpeed:I = 0x11

.field static final TRANSACTION_getPreferencesFloat:I = 0x5d

.field static final TRANSACTION_getPrevUri:I = 0x3a

.field static final TRANSACTION_getQueue:I = 0x24

.field static final TRANSACTION_getRepeat:I = 0x3b

.field static final TRANSACTION_getSamplingRate:I = 0x2f

.field static final TRANSACTION_getShuffle:I = 0x3c

.field static final TRANSACTION_getSoundAlive:I = 0xd

.field static final TRANSACTION_getSoundAliveUserEQ:I = 0xe

.field static final TRANSACTION_getSoundAliveUserExt:I = 0xf

.field static final TRANSACTION_getTitleName:I = 0x30

.field static final TRANSACTION_isActiveSmartVolume:I = 0x14

.field static final TRANSACTION_isAudioShockWarningEnabled:I = 0x64

.field static final TRANSACTION_isBtConnected:I = 0x23

.field static final TRANSACTION_isConnectingWfd:I = 0x22

.field static final TRANSACTION_isEnableToPlaying:I = 0x63

.field static final TRANSACTION_isLocalTrack:I = 0x1f

.field static final TRANSACTION_isPlaying:I = 0x20

.field static final TRANSACTION_isPreparing:I = 0x21

.field static final TRANSACTION_isSupportPlaySpeed:I = 0x12

.field static final TRANSACTION_loadNewSoundAliveAuto:I = 0x58

.field static final TRANSACTION_loadNewSoundAliveBandLevel:I = 0x56

.field static final TRANSACTION_loadNewSoundAliveSquarePosition:I = 0x55

.field static final TRANSACTION_loadNewSoundAliveStrength:I = 0x57

.field static final TRANSACTION_loadNewSoundAliveUsePreset:I = 0x54

.field static final TRANSACTION_moveQueueItem:I = 0xa

.field static final TRANSACTION_next:I = 0x19

.field static final TRANSACTION_openList:I = 0x1

.field static final TRANSACTION_pause:I = 0x18

.field static final TRANSACTION_play:I = 0x17

.field static final TRANSACTION_position:I = 0x1d

.field static final TRANSACTION_prev:I = 0x1a

.field static final TRANSACTION_refreshDlna:I = 0x42

.field static final TRANSACTION_removeTrack:I = 0x5

.field static final TRANSACTION_removeTracks:I = 0x6

.field static final TRANSACTION_removeTracksAudioIds:I = 0x8

.field static final TRANSACTION_removeTracksPosition:I = 0x7

.field static final TRANSACTION_reorderQueue:I = 0x4

.field static final TRANSACTION_savePreferencesBoolean:I = 0x5b

.field static final TRANSACTION_savePreferencesFloat:I = 0x5a

.field static final TRANSACTION_savePreferencesInt:I = 0x59

.field static final TRANSACTION_savePreferencesString:I = 0x5c

.field static final TRANSACTION_seek:I = 0x1b

.field static final TRANSACTION_selectDlnaDms:I = 0x43

.field static final TRANSACTION_setAdaptSound:I = 0x16

.field static final TRANSACTION_setDlnaDmrMute:I = 0x46

.field static final TRANSACTION_setK2HD:I = 0x15

.field static final TRANSACTION_setLimitVolume:I = 0x61

.field static final TRANSACTION_setNewSoundAliveAuto:I = 0x4a

.field static final TRANSACTION_setNewSoundAliveBandLevel:I = 0x4d

.field static final TRANSACTION_setNewSoundAliveEachStrength:I = 0x4f

.field static final TRANSACTION_setNewSoundAliveSquarePosition:I = 0x4c

.field static final TRANSACTION_setNewSoundAliveStrength:I = 0x4e

.field static final TRANSACTION_setNewSoundAliveUsePreset:I = 0x4b

.field static final TRANSACTION_setPlaySpeed:I = 0x10

.field static final TRANSACTION_setQueuePosition:I = 0x3

.field static final TRANSACTION_setSlinkWakeLock:I = 0x5f

.field static final TRANSACTION_setSmartVolume:I = 0x13

.field static final TRANSACTION_setSoundAlive:I = 0xb

.field static final TRANSACTION_setSoundAliveUser:I = 0xc

.field static final TRANSACTION_stopMusicPackage:I = 0x49

.field static final TRANSACTION_toggleRepeat:I = 0x3d

.field static final TRANSACTION_toggleShuffle:I = 0x3e


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 15
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p0, p0, v0}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 16
    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/samsung/musicplus/service/IPlayerService;
    .locals 2
    .param p0, "obj"    # Landroid/os/IBinder;

    .prologue
    .line 23
    if-nez p0, :cond_0

    .line 24
    const/4 v0, 0x0

    .line 30
    :goto_0
    return-object v0

    .line 26
    :cond_0
    const-string v1, "com.samsung.musicplus.service.IPlayerService"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 27
    .local v0, "iin":Landroid/os/IInterface;
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/samsung/musicplus/service/IPlayerService;

    if-eqz v1, :cond_1

    .line 28
    check-cast v0, Lcom/samsung/musicplus/service/IPlayerService;

    goto :goto_0

    .line 30
    :cond_1
    new-instance v0, Lcom/samsung/musicplus/service/IPlayerService$Stub$Proxy;

    .end local v0    # "iin":Landroid/os/IInterface;
    invoke-direct {v0, p0}, Lcom/samsung/musicplus/service/IPlayerService$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 34
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 11
    .param p1, "code"    # I
    .param p2, "data"    # Landroid/os/Parcel;
    .param p3, "reply"    # Landroid/os/Parcel;
    .param p4, "flags"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x0

    const/4 v10, 0x1

    .line 38
    sparse-switch p1, :sswitch_data_0

    .line 919
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v10

    :goto_0
    return v10

    .line 42
    :sswitch_0
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 47
    :sswitch_1
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 49
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 51
    .local v1, "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 53
    .local v2, "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->createLongArray()[J

    move-result-object v3

    .line 55
    .local v3, "_arg2":[J
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 57
    .local v4, "_arg3":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    move v5, v10

    .local v5, "_arg4":Z
    :cond_0
    move-object v0, p0

    .line 58
    invoke-virtual/range {v0 .. v5}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->openList(ILjava/lang/String;[JIZ)V

    .line 59
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 64
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":[J
    .end local v4    # "_arg3":I
    .end local v5    # "_arg4":Z
    :sswitch_2
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 66
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 68
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 70
    .restart local v2    # "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->createLongArray()[J

    move-result-object v3

    .line 72
    .restart local v3    # "_arg2":[J
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 73
    .restart local v4    # "_arg3":I
    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->changeListWithoutPlaying(ILjava/lang/String;[JI)V

    .line 74
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 79
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":[J
    .end local v4    # "_arg3":I
    :sswitch_3
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 81
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 83
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1

    move v2, v10

    .line 84
    .local v2, "_arg1":Z
    :goto_1
    invoke-virtual {p0, v1, v2}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->setQueuePosition(IZ)V

    .line 85
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .end local v2    # "_arg1":Z
    :cond_1
    move v2, v5

    .line 83
    goto :goto_1

    .line 90
    .end local v1    # "_arg0":I
    :sswitch_4
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 92
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 94
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 96
    .local v2, "_arg1":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->createLongArray()[J

    move-result-object v3

    .line 98
    .restart local v3    # "_arg2":[J
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    .line 99
    .restart local v4    # "_arg3":I
    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->reorderQueue(ILjava/lang/String;[JI)V

    .line 100
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto :goto_0

    .line 105
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":Ljava/lang/String;
    .end local v3    # "_arg2":[J
    .end local v4    # "_arg3":I
    :sswitch_5
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 107
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v6

    .line 108
    .local v6, "_arg0":J
    invoke-virtual {p0, v6, v7}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->removeTrack(J)I

    move-result v8

    .line 109
    .local v8, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 110
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 115
    .end local v6    # "_arg0":J
    .end local v8    # "_result":I
    :sswitch_6
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 117
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 119
    .restart local v1    # "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 120
    .local v2, "_arg1":I
    invoke-virtual {p0, v1, v2}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->removeTracks(II)I

    move-result v8

    .line 121
    .restart local v8    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 122
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 127
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":I
    .end local v8    # "_result":I
    :sswitch_7
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 129
    invoke-virtual {p2}, Landroid/os/Parcel;->createIntArray()[I

    move-result-object v1

    .line 130
    .local v1, "_arg0":[I
    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->removeTracksPosition([I)I

    move-result v8

    .line 131
    .restart local v8    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 132
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 137
    .end local v1    # "_arg0":[I
    .end local v8    # "_result":I
    :sswitch_8
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 139
    invoke-virtual {p2}, Landroid/os/Parcel;->createLongArray()[J

    move-result-object v1

    .line 140
    .local v1, "_arg0":[J
    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->removeTracksAudioIds([J)I

    move-result v8

    .line 141
    .restart local v8    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 142
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 147
    .end local v1    # "_arg0":[J
    .end local v8    # "_result":I
    :sswitch_9
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 149
    invoke-virtual {p2}, Landroid/os/Parcel;->createLongArray()[J

    move-result-object v1

    .line 151
    .restart local v1    # "_arg0":[J
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 152
    .restart local v2    # "_arg1":I
    invoke-virtual {p0, v1, v2}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->enqueue([JI)V

    .line 153
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 158
    .end local v1    # "_arg0":[J
    .end local v2    # "_arg1":I
    :sswitch_a
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 160
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 162
    .local v1, "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 163
    .restart local v2    # "_arg1":I
    invoke-virtual {p0, v1, v2}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->moveQueueItem(II)V

    .line 164
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 169
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":I
    :sswitch_b
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 171
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 172
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->setSoundAlive(I)V

    .line 173
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 178
    .end local v1    # "_arg0":I
    :sswitch_c
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 180
    invoke-virtual {p2}, Landroid/os/Parcel;->createIntArray()[I

    move-result-object v1

    .line 182
    .local v1, "_arg0":[I
    invoke-virtual {p2}, Landroid/os/Parcel;->createIntArray()[I

    move-result-object v2

    .line 183
    .local v2, "_arg1":[I
    invoke-virtual {p0, v1, v2}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->setSoundAliveUser([I[I)V

    .line 184
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 189
    .end local v1    # "_arg0":[I
    .end local v2    # "_arg1":[I
    :sswitch_d
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 190
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->getSoundAlive()I

    move-result v8

    .line 191
    .restart local v8    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 192
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 197
    .end local v8    # "_result":I
    :sswitch_e
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 198
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->getSoundAliveUserEQ()[I

    move-result-object v8

    .line 199
    .local v8, "_result":[I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 200
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeIntArray([I)V

    goto/16 :goto_0

    .line 205
    .end local v8    # "_result":[I
    :sswitch_f
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 206
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->getSoundAliveUserExt()[I

    move-result-object v8

    .line 207
    .restart local v8    # "_result":[I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 208
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeIntArray([I)V

    goto/16 :goto_0

    .line 213
    .end local v8    # "_result":[I
    :sswitch_10
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 215
    invoke-virtual {p2}, Landroid/os/Parcel;->readFloat()F

    move-result v1

    .line 216
    .local v1, "_arg0":F
    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->setPlaySpeed(F)V

    .line 217
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 222
    .end local v1    # "_arg0":F
    :sswitch_11
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 223
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->getPlaySpeed()F

    move-result v8

    .line 224
    .local v8, "_result":F
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 225
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeFloat(F)V

    goto/16 :goto_0

    .line 230
    .end local v8    # "_result":F
    :sswitch_12
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 231
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->isSupportPlaySpeed()Z

    move-result v8

    .line 232
    .local v8, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 233
    if-eqz v8, :cond_2

    move v5, v10

    :cond_2
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 238
    .end local v8    # "_result":Z
    :sswitch_13
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 240
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_3

    move v1, v10

    .line 241
    .local v1, "_arg0":Z
    :goto_2
    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->setSmartVolume(Z)V

    .line 242
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .end local v1    # "_arg0":Z
    :cond_3
    move v1, v5

    .line 240
    goto :goto_2

    .line 247
    :sswitch_14
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 248
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->isActiveSmartVolume()Z

    move-result v8

    .line 249
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 250
    if-eqz v8, :cond_4

    move v5, v10

    :cond_4
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 255
    .end local v8    # "_result":Z
    :sswitch_15
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 257
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_5

    move v1, v10

    .line 258
    .restart local v1    # "_arg0":Z
    :goto_3
    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->setK2HD(Z)V

    .line 259
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .end local v1    # "_arg0":Z
    :cond_5
    move v1, v5

    .line 257
    goto :goto_3

    .line 264
    :sswitch_16
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 266
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_6

    move v1, v10

    .line 267
    .restart local v1    # "_arg0":Z
    :goto_4
    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->setAdaptSound(Z)V

    .line 268
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .end local v1    # "_arg0":Z
    :cond_6
    move v1, v5

    .line 266
    goto :goto_4

    .line 273
    :sswitch_17
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 274
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->play()V

    .line 275
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 280
    :sswitch_18
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 281
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->pause()V

    .line 282
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 287
    :sswitch_19
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 288
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->next()V

    .line 289
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 294
    :sswitch_1a
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 296
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_7

    move v1, v10

    .line 297
    .restart local v1    # "_arg0":Z
    :goto_5
    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->prev(Z)V

    .line 298
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .end local v1    # "_arg0":Z
    :cond_7
    move v1, v5

    .line 296
    goto :goto_5

    .line 303
    :sswitch_1b
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 305
    invoke-virtual {p2}, Landroid/os/Parcel;->readLong()J

    move-result-wide v6

    .line 306
    .restart local v6    # "_arg0":J
    invoke-virtual {p0, v6, v7}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->seek(J)J

    move-result-wide v8

    .line 307
    .local v8, "_result":J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 308
    invoke-virtual {p3, v8, v9}, Landroid/os/Parcel;->writeLong(J)V

    goto/16 :goto_0

    .line 313
    .end local v6    # "_arg0":J
    .end local v8    # "_result":J
    :sswitch_1c
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 314
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->duration()J

    move-result-wide v8

    .line 315
    .restart local v8    # "_result":J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 316
    invoke-virtual {p3, v8, v9}, Landroid/os/Parcel;->writeLong(J)V

    goto/16 :goto_0

    .line 321
    .end local v8    # "_result":J
    :sswitch_1d
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 322
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->position()J

    move-result-wide v8

    .line 323
    .restart local v8    # "_result":J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 324
    invoke-virtual {p3, v8, v9}, Landroid/os/Parcel;->writeLong(J)V

    goto/16 :goto_0

    .line 329
    .end local v8    # "_result":J
    :sswitch_1e
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 330
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->buffering()I

    move-result v8

    .line 331
    .local v8, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 332
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 337
    .end local v8    # "_result":I
    :sswitch_1f
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 338
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->isLocalTrack()Z

    move-result v8

    .line 339
    .local v8, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 340
    if-eqz v8, :cond_8

    move v5, v10

    :cond_8
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 345
    .end local v8    # "_result":Z
    :sswitch_20
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 346
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->isPlaying()Z

    move-result v8

    .line 347
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 348
    if-eqz v8, :cond_9

    move v5, v10

    :cond_9
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 353
    .end local v8    # "_result":Z
    :sswitch_21
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 354
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->isPreparing()Z

    move-result v8

    .line 355
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 356
    if-eqz v8, :cond_a

    move v5, v10

    :cond_a
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 361
    .end local v8    # "_result":Z
    :sswitch_22
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 362
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->isConnectingWfd()Z

    move-result v8

    .line 363
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 364
    if-eqz v8, :cond_b

    move v5, v10

    :cond_b
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 369
    .end local v8    # "_result":Z
    :sswitch_23
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 370
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->isBtConnected()Z

    move-result v8

    .line 371
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 372
    if-eqz v8, :cond_c

    move v5, v10

    :cond_c
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 377
    .end local v8    # "_result":Z
    :sswitch_24
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 378
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->getQueue()[J

    move-result-object v8

    .line 379
    .local v8, "_result":[J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 380
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeLongArray([J)V

    goto/16 :goto_0

    .line 385
    .end local v8    # "_result":[J
    :sswitch_25
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 386
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->getOrganizedQueue()[J

    move-result-object v8

    .line 387
    .restart local v8    # "_result":[J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 388
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeLongArray([J)V

    goto/16 :goto_0

    .line 393
    .end local v8    # "_result":[J
    :sswitch_26
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 394
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->getListItemCount()I

    move-result v8

    .line 395
    .local v8, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 396
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 401
    .end local v8    # "_result":I
    :sswitch_27
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 402
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->getListPosition()I

    move-result v8

    .line 403
    .restart local v8    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 404
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 409
    .end local v8    # "_result":I
    :sswitch_28
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 410
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->getListType()I

    move-result v8

    .line 411
    .restart local v8    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 412
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 417
    .end local v8    # "_result":I
    :sswitch_29
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 418
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->getKeyWord()Ljava/lang/String;

    move-result-object v8

    .line 419
    .local v8, "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 420
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 425
    .end local v8    # "_result":Ljava/lang/String;
    :sswitch_2a
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 426
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->getAudioId()J

    move-result-wide v8

    .line 427
    .local v8, "_result":J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 428
    invoke-virtual {p3, v8, v9}, Landroid/os/Parcel;->writeLong(J)V

    goto/16 :goto_0

    .line 433
    .end local v8    # "_result":J
    :sswitch_2b
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 434
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->getCurrentPath()Ljava/lang/String;

    move-result-object v8

    .line 435
    .local v8, "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 436
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 441
    .end local v8    # "_result":Ljava/lang/String;
    :sswitch_2c
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 442
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->getMediaType()I

    move-result v8

    .line 443
    .local v8, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 444
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 449
    .end local v8    # "_result":I
    :sswitch_2d
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 450
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->getMimeType()Ljava/lang/String;

    move-result-object v8

    .line 451
    .local v8, "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 452
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 457
    .end local v8    # "_result":Ljava/lang/String;
    :sswitch_2e
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 458
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->getBitDepth()I

    move-result v8

    .line 459
    .local v8, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 460
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 465
    .end local v8    # "_result":I
    :sswitch_2f
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 466
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->getSamplingRate()I

    move-result v8

    .line 467
    .restart local v8    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 468
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 473
    .end local v8    # "_result":I
    :sswitch_30
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 474
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->getTitleName()Ljava/lang/String;

    move-result-object v8

    .line 475
    .local v8, "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 476
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 481
    .end local v8    # "_result":Ljava/lang/String;
    :sswitch_31
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 482
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->getArtistName()Ljava/lang/String;

    move-result-object v8

    .line 483
    .restart local v8    # "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 484
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 489
    .end local v8    # "_result":Ljava/lang/String;
    :sswitch_32
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 490
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->getAlbumName()Ljava/lang/String;

    move-result-object v8

    .line 491
    .restart local v8    # "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 492
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 497
    .end local v8    # "_result":Ljava/lang/String;
    :sswitch_33
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 498
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->getAlbumId()J

    move-result-wide v8

    .line 499
    .local v8, "_result":J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 500
    invoke-virtual {p3, v8, v9}, Landroid/os/Parcel;->writeLong(J)V

    goto/16 :goto_0

    .line 505
    .end local v8    # "_result":J
    :sswitch_34
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 506
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->getGenre()Ljava/lang/String;

    move-result-object v8

    .line 507
    .local v8, "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 508
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 513
    .end local v8    # "_result":Ljava/lang/String;
    :sswitch_35
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 514
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->getAlbumArt()Ljava/lang/String;

    move-result-object v8

    .line 515
    .restart local v8    # "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 516
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 521
    .end local v8    # "_result":Ljava/lang/String;
    :sswitch_36
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 522
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->getDeviceId()J

    move-result-wide v8

    .line 523
    .local v8, "_result":J
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 524
    invoke-virtual {p3, v8, v9}, Landroid/os/Parcel;->writeLong(J)V

    goto/16 :goto_0

    .line 529
    .end local v8    # "_result":J
    :sswitch_37
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 530
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->getCurrentUri()Ljava/lang/String;

    move-result-object v8

    .line 531
    .local v8, "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 532
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 537
    .end local v8    # "_result":Ljava/lang/String;
    :sswitch_38
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 538
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->getCurrentBaseUri()Ljava/lang/String;

    move-result-object v8

    .line 539
    .restart local v8    # "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 540
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 545
    .end local v8    # "_result":Ljava/lang/String;
    :sswitch_39
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 546
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->getNextUri()Ljava/lang/String;

    move-result-object v8

    .line 547
    .restart local v8    # "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 548
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 553
    .end local v8    # "_result":Ljava/lang/String;
    :sswitch_3a
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 554
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->getPrevUri()Ljava/lang/String;

    move-result-object v8

    .line 555
    .restart local v8    # "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 556
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 561
    .end local v8    # "_result":Ljava/lang/String;
    :sswitch_3b
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 562
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->getRepeat()I

    move-result v8

    .line 563
    .local v8, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 564
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 569
    .end local v8    # "_result":I
    :sswitch_3c
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 570
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->getShuffle()I

    move-result v8

    .line 571
    .restart local v8    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 572
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 577
    .end local v8    # "_result":I
    :sswitch_3d
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 578
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->toggleRepeat()V

    .line 579
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 584
    :sswitch_3e
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 585
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->toggleShuffle()V

    .line 586
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 591
    :sswitch_3f
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 593
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 594
    .local v1, "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->changeToDmrPlayer(Ljava/lang/String;)V

    .line 595
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 600
    .end local v1    # "_arg0":Ljava/lang/String;
    :sswitch_40
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 601
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->changeToDefaultPlayer()V

    .line 602
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 607
    :sswitch_41
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 608
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->changeToWfdDevice()V

    .line 609
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 614
    :sswitch_42
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 615
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->refreshDlna()V

    .line 616
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 621
    :sswitch_43
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 623
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 624
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->selectDlnaDms(Ljava/lang/String;)V

    .line 625
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 630
    .end local v1    # "_arg0":Ljava/lang/String;
    :sswitch_44
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 631
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->getDlnaPlayingNic()Ljava/lang/String;

    move-result-object v8

    .line 632
    .local v8, "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 633
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 638
    .end local v8    # "_result":Ljava/lang/String;
    :sswitch_45
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 639
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->getDlnaPlayingDmrId()Ljava/lang/String;

    move-result-object v8

    .line 640
    .restart local v8    # "_result":Ljava/lang/String;
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 641
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 646
    .end local v8    # "_result":Ljava/lang/String;
    :sswitch_46
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 647
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->setDlnaDmrMute()V

    .line 648
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 653
    :sswitch_47
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 654
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->dlnaDmrVolumeUp()V

    .line 655
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 660
    :sswitch_48
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 661
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->dlnaDmrVolumeDown()V

    .line 662
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 667
    :sswitch_49
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 668
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->stopMusicPackage()V

    .line 669
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 674
    :sswitch_4a
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 676
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_d

    move v1, v10

    .line 677
    .local v1, "_arg0":Z
    :goto_6
    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->setNewSoundAliveAuto(Z)V

    .line 678
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .end local v1    # "_arg0":Z
    :cond_d
    move v1, v5

    .line 676
    goto :goto_6

    .line 683
    :sswitch_4b
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 685
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 686
    .local v1, "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->setNewSoundAliveUsePreset(I)V

    .line 687
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 692
    .end local v1    # "_arg0":I
    :sswitch_4c
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 694
    invoke-virtual {p2}, Landroid/os/Parcel;->createIntArray()[I

    move-result-object v1

    .line 695
    .local v1, "_arg0":[I
    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->setNewSoundAliveSquarePosition([I)V

    .line 696
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 701
    .end local v1    # "_arg0":[I
    :sswitch_4d
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 703
    invoke-virtual {p2}, Landroid/os/Parcel;->createIntArray()[I

    move-result-object v1

    .line 704
    .restart local v1    # "_arg0":[I
    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->setNewSoundAliveBandLevel([I)V

    .line 705
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 710
    .end local v1    # "_arg0":[I
    :sswitch_4e
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 712
    invoke-virtual {p2}, Landroid/os/Parcel;->createIntArray()[I

    move-result-object v1

    .line 713
    .restart local v1    # "_arg0":[I
    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->setNewSoundAliveStrength([I)V

    .line 714
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 719
    .end local v1    # "_arg0":[I
    :sswitch_4f
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 721
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 723
    .local v1, "_arg0":I
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 724
    .local v2, "_arg1":I
    invoke-virtual {p0, v1, v2}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->setNewSoundAliveEachStrength(II)V

    .line 725
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 730
    .end local v1    # "_arg0":I
    .end local v2    # "_arg1":I
    :sswitch_50
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 731
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->getNewSoundAliveCurrentPreset()I

    move-result v8

    .line 732
    .local v8, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 733
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 738
    .end local v8    # "_result":I
    :sswitch_51
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 739
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->getNewSoundAliveSquarePosition()[I

    move-result-object v8

    .line 740
    .local v8, "_result":[I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 741
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeIntArray([I)V

    goto/16 :goto_0

    .line 746
    .end local v8    # "_result":[I
    :sswitch_52
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 748
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 749
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->getNewSoundAliveBandLevel(I)I

    move-result v8

    .line 750
    .local v8, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 751
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 756
    .end local v1    # "_arg0":I
    .end local v8    # "_result":I
    :sswitch_53
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 758
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 759
    .restart local v1    # "_arg0":I
    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->getNewSoundAliveRoundedStrength(I)I

    move-result v8

    .line 760
    .restart local v8    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 761
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 766
    .end local v1    # "_arg0":I
    .end local v8    # "_result":I
    :sswitch_54
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 767
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->loadNewSoundAliveUsePreset()I

    move-result v8

    .line 768
    .restart local v8    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 769
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 774
    .end local v8    # "_result":I
    :sswitch_55
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 775
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->loadNewSoundAliveSquarePosition()[I

    move-result-object v8

    .line 776
    .local v8, "_result":[I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 777
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeIntArray([I)V

    goto/16 :goto_0

    .line 782
    .end local v8    # "_result":[I
    :sswitch_56
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 783
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->loadNewSoundAliveBandLevel()[I

    move-result-object v8

    .line 784
    .restart local v8    # "_result":[I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 785
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeIntArray([I)V

    goto/16 :goto_0

    .line 790
    .end local v8    # "_result":[I
    :sswitch_57
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 791
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->loadNewSoundAliveStrength()[I

    move-result-object v8

    .line 792
    .restart local v8    # "_result":[I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 793
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeIntArray([I)V

    goto/16 :goto_0

    .line 798
    .end local v8    # "_result":[I
    :sswitch_58
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 799
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->loadNewSoundAliveAuto()Z

    move-result v8

    .line 800
    .local v8, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 801
    if-eqz v8, :cond_e

    move v5, v10

    :cond_e
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 806
    .end local v8    # "_result":Z
    :sswitch_59
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 808
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 810
    .local v1, "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    .line 811
    .restart local v2    # "_arg1":I
    invoke-virtual {p0, v1, v2}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->savePreferencesInt(Ljava/lang/String;I)V

    .line 812
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 817
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":I
    :sswitch_5a
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 819
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 821
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readFloat()F

    move-result v2

    .line 822
    .local v2, "_arg1":F
    invoke-virtual {p0, v1, v2}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->savePreferencesFloat(Ljava/lang/String;F)V

    .line 823
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 828
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":F
    :sswitch_5b
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 830
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 832
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_f

    move v2, v10

    .line 833
    .local v2, "_arg1":Z
    :goto_7
    invoke-virtual {p0, v1, v2}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->savePreferencesBoolean(Ljava/lang/String;Z)V

    .line 834
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .end local v2    # "_arg1":Z
    :cond_f
    move v2, v5

    .line 832
    goto :goto_7

    .line 839
    .end local v1    # "_arg0":Ljava/lang/String;
    :sswitch_5c
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 841
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 843
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v2

    .line 844
    .local v2, "_arg1":Ljava/lang/String;
    invoke-virtual {p0, v1, v2}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->savePreferencesString(Ljava/lang/String;Ljava/lang/String;)V

    .line 845
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 850
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":Ljava/lang/String;
    :sswitch_5d
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 852
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 854
    .restart local v1    # "_arg0":Ljava/lang/String;
    invoke-virtual {p2}, Landroid/os/Parcel;->readFloat()F

    move-result v2

    .line 855
    .local v2, "_arg1":F
    invoke-virtual {p0, v1, v2}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->getPreferencesFloat(Ljava/lang/String;F)F

    move-result v8

    .line 856
    .local v8, "_result":F
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 857
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeFloat(F)V

    goto/16 :goto_0

    .line 862
    .end local v1    # "_arg0":Ljava/lang/String;
    .end local v2    # "_arg1":F
    .end local v8    # "_result":F
    :sswitch_5e
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 863
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->getMoodValue()I

    move-result v8

    .line 864
    .local v8, "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 865
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 870
    .end local v8    # "_result":I
    :sswitch_5f
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 871
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->setSlinkWakeLock()V

    .line 872
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 877
    :sswitch_60
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 878
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->getPersonalMode()I

    move-result v8

    .line 879
    .restart local v8    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 880
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 885
    .end local v8    # "_result":I
    :sswitch_61
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 887
    invoke-virtual {p2}, Landroid/os/Parcel;->readFloat()F

    move-result v1

    .line 889
    .local v1, "_arg0":F
    invoke-virtual {p2}, Landroid/os/Parcel;->readFloat()F

    move-result v2

    .line 890
    .restart local v2    # "_arg1":F
    invoke-virtual {p0, v1, v2}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->setLimitVolume(FF)V

    .line 891
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    .line 896
    .end local v1    # "_arg0":F
    .end local v2    # "_arg1":F
    :sswitch_62
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 897
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->getAudioSessionId()I

    move-result v8

    .line 898
    .restart local v8    # "_result":I
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 899
    invoke-virtual {p3, v8}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 904
    .end local v8    # "_result":I
    :sswitch_63
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 905
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->isEnableToPlaying()Z

    move-result v8

    .line 906
    .local v8, "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 907
    if-eqz v8, :cond_10

    move v5, v10

    :cond_10
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 912
    .end local v8    # "_result":Z
    :sswitch_64
    const-string v0, "com.samsung.musicplus.service.IPlayerService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 913
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/IPlayerService$Stub;->isAudioShockWarningEnabled()Z

    move-result v8

    .line 914
    .restart local v8    # "_result":Z
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 915
    if-eqz v8, :cond_11

    move v5, v10

    :cond_11
    invoke-virtual {p3, v5}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    .line 38
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0xd -> :sswitch_d
        0xe -> :sswitch_e
        0xf -> :sswitch_f
        0x10 -> :sswitch_10
        0x11 -> :sswitch_11
        0x12 -> :sswitch_12
        0x13 -> :sswitch_13
        0x14 -> :sswitch_14
        0x15 -> :sswitch_15
        0x16 -> :sswitch_16
        0x17 -> :sswitch_17
        0x18 -> :sswitch_18
        0x19 -> :sswitch_19
        0x1a -> :sswitch_1a
        0x1b -> :sswitch_1b
        0x1c -> :sswitch_1c
        0x1d -> :sswitch_1d
        0x1e -> :sswitch_1e
        0x1f -> :sswitch_1f
        0x20 -> :sswitch_20
        0x21 -> :sswitch_21
        0x22 -> :sswitch_22
        0x23 -> :sswitch_23
        0x24 -> :sswitch_24
        0x25 -> :sswitch_25
        0x26 -> :sswitch_26
        0x27 -> :sswitch_27
        0x28 -> :sswitch_28
        0x29 -> :sswitch_29
        0x2a -> :sswitch_2a
        0x2b -> :sswitch_2b
        0x2c -> :sswitch_2c
        0x2d -> :sswitch_2d
        0x2e -> :sswitch_2e
        0x2f -> :sswitch_2f
        0x30 -> :sswitch_30
        0x31 -> :sswitch_31
        0x32 -> :sswitch_32
        0x33 -> :sswitch_33
        0x34 -> :sswitch_34
        0x35 -> :sswitch_35
        0x36 -> :sswitch_36
        0x37 -> :sswitch_37
        0x38 -> :sswitch_38
        0x39 -> :sswitch_39
        0x3a -> :sswitch_3a
        0x3b -> :sswitch_3b
        0x3c -> :sswitch_3c
        0x3d -> :sswitch_3d
        0x3e -> :sswitch_3e
        0x3f -> :sswitch_3f
        0x40 -> :sswitch_40
        0x41 -> :sswitch_41
        0x42 -> :sswitch_42
        0x43 -> :sswitch_43
        0x44 -> :sswitch_44
        0x45 -> :sswitch_45
        0x46 -> :sswitch_46
        0x47 -> :sswitch_47
        0x48 -> :sswitch_48
        0x49 -> :sswitch_49
        0x4a -> :sswitch_4a
        0x4b -> :sswitch_4b
        0x4c -> :sswitch_4c
        0x4d -> :sswitch_4d
        0x4e -> :sswitch_4e
        0x4f -> :sswitch_4f
        0x50 -> :sswitch_50
        0x51 -> :sswitch_51
        0x52 -> :sswitch_52
        0x53 -> :sswitch_53
        0x54 -> :sswitch_54
        0x55 -> :sswitch_55
        0x56 -> :sswitch_56
        0x57 -> :sswitch_57
        0x58 -> :sswitch_58
        0x59 -> :sswitch_59
        0x5a -> :sswitch_5a
        0x5b -> :sswitch_5b
        0x5c -> :sswitch_5c
        0x5d -> :sswitch_5d
        0x5e -> :sswitch_5e
        0x5f -> :sswitch_5f
        0x60 -> :sswitch_60
        0x61 -> :sswitch_61
        0x62 -> :sswitch_62
        0x63 -> :sswitch_63
        0x64 -> :sswitch_64
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
