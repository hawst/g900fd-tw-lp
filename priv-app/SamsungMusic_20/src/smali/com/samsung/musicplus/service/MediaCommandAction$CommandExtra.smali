.class public interface abstract Lcom/samsung/musicplus/service/MediaCommandAction$CommandExtra;
.super Ljava/lang/Object;
.source "MediaCommandAction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/service/MediaCommandAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "CommandExtra"
.end annotation


# static fields
.field public static final BASE_URI:Ljava/lang/String; = "base_uri"

.field public static final LIST:Ljava/lang/String; = "list"

.field public static final LIST_POSITION:Ljava/lang/String; = "listPosition"

.field public static final NAME:Ljava/lang/String; = "extra_name"

.field public static final TYPE:Ljava/lang/String; = "extra_type"
