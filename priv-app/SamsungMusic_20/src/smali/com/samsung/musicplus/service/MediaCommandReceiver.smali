.class public Lcom/samsung/musicplus/service/MediaCommandReceiver;
.super Landroid/content/BroadcastReceiver;
.source "MediaCommandReceiver.java"

# interfaces
.implements Lcom/samsung/musicplus/service/MediaCommandAction;


# static fields
.field public static final TAG:Ljava/lang/String; = "MusicCommand"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v4, 0x1

    .line 17
    const-string v1, "MusicCommand"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onReceive() intent  : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 18
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 19
    .local v0, "action":Ljava/lang/String;
    const-string v1, "com.samsung.musicplus.intent.action.PLAY_CONTENTS"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 20
    invoke-static {p1, p2}, Lcom/samsung/musicplus/util/player/PlayUtils;->playContents(Landroid/content/Context;Landroid/content/Intent;)V

    .line 42
    :cond_0
    :goto_0
    return-void

    .line 21
    :cond_1
    const-string v1, "com.sec.android.app.music.intent.action.PLAY_VIA"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 22
    invoke-static {p1, p2}, Lcom/samsung/musicplus/util/player/PlayUtils;->playVia(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_0

    .line 23
    :cond_2
    const-string v1, "com.sec.android.app.music.intent.action.PLAY_BY_MOOD"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 24
    invoke-static {p1, p2}, Lcom/samsung/musicplus/util/player/PlayUtils;->playByMood(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_0

    .line 25
    :cond_3
    const-string v1, "com.sec.android.app.music.musicservicecommand.play"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 26
    const-string v1, "play"

    invoke-static {p1, v1}, Lcom/samsung/musicplus/util/player/PlayUtils;->startCommand(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 27
    :cond_4
    const-string v1, "com.sec.android.app.music.musicservicecommand.pause"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 28
    const-string v1, "pause"

    invoke-static {p1, v1}, Lcom/samsung/musicplus/util/player/PlayUtils;->startCommand(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 29
    :cond_5
    const-string v1, "com.sec.android.app.music.musicservicecommand.playprevious"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 30
    const-string v1, "previous"

    invoke-static {p1, v1, v4}, Lcom/samsung/musicplus/util/player/PlayUtils;->startCommand(Landroid/content/Context;Ljava/lang/String;Z)V

    goto :goto_0

    .line 31
    :cond_6
    const-string v1, "com.sec.android.app.music.musicservicecommand.playnext"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 32
    const-string v1, "next"

    invoke-static {p1, v1, v4}, Lcom/samsung/musicplus/util/player/PlayUtils;->startCommand(Landroid/content/Context;Ljava/lang/String;Z)V

    goto :goto_0

    .line 33
    :cond_7
    const-string v1, "com.sec.android.app.music.musicservicecommand.togglepause"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 34
    const-string v1, "togglepause"

    invoke-static {p1, v1}, Lcom/samsung/musicplus/util/player/PlayUtils;->startCommand(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 35
    :cond_8
    const-string v1, "com.sec.android.app.music.musicservicecommand.prev"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 36
    const-string v1, "previous"

    invoke-static {p1, v1}, Lcom/samsung/musicplus/util/player/PlayUtils;->startCommand(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 37
    :cond_9
    const-string v1, "com.sec.android.app.music.musicservicecommand.next"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 38
    const-string v1, "next"

    invoke-static {p1, v1}, Lcom/samsung/musicplus/util/player/PlayUtils;->startCommand(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 39
    :cond_a
    const-string v1, "com.sec.android.app.music.musicservicecommand.checkplaystatus"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 40
    const-string v1, "com.sec.android.app.music.musicservicecommand.checkplaystatus"

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {p1, v1, v2, v3}, Lcom/samsung/musicplus/util/player/PlayUtils;->startCommand(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)V

    goto/16 :goto_0
.end method
