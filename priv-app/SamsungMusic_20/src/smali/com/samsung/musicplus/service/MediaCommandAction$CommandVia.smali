.class public interface abstract Lcom/samsung/musicplus/service/MediaCommandAction$CommandVia;
.super Ljava/lang/Object;
.source "MediaCommandAction.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/service/MediaCommandAction;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "CommandVia"
.end annotation


# static fields
.field public static final ALBUM:Ljava/lang/String; = "album"

.field public static final ARTIST:Ljava/lang/String; = "artist"

.field public static final PLAYLIST:Ljava/lang/String; = "playlist"

.field public static final TITLE:Ljava/lang/String; = "title"
