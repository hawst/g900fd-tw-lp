.class public Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;
.super Landroid/app/Service;
.source "SviewCoverService.java"

# interfaces
.implements Lcom/samsung/musicplus/service/PlayerServiceStateAction;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;,
        Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$CustomDialog;
    }
.end annotation


# static fields
.field private static final ACTION_CLEAR_COVER_STATE_CHANGE:Ljava/lang/String; = "com.samsung.cover.STATE_CHANGE"

.field private static final BG_ALPHA:I = 0xef

.field private static final BG_DEFAULT_COLOR:I = 0x7f0b009c

.field private static final BG_DEFAULT_PATTERN_COLOR:I = 0x7f0b0091

.field private static final CLASSNAME:Ljava/lang/String;

.field private static final SVIEW_COVER_DISPLAY_TIMEOUT_DEFAULT:I = 0x1388

.field private static final TAG:Ljava/lang/String; = "MusicSCover"


# instance fields
.field private mBuffering:Landroid/widget/ProgressBar;

.field private mCloseButton:Landroid/view/View;

.field private final mConnection:Landroid/content/ServiceConnection;

.field private mDialog:Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$CustomDialog;

.field private final mMediaListener:Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction$OnMediaListener;

.field private final mPlayerServiceListener:Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction$OnPlayerServiceListener;

.field private mPlayerView:Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;

.field private mServiceToken:Lcom/samsung/musicplus/util/ServiceUtils$ServiceToken;

.field private mSviewCoverCommonAction:Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;

.field private final mSystemListener:Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction$OnSystemListener;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 64
    const-class v0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->CLASSNAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 60
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 89
    new-instance v0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$1;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$1;-><init>(Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;)V

    iput-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->mConnection:Landroid/content/ServiceConnection;

    .line 278
    new-instance v0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$5;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$5;-><init>(Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;)V

    iput-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->mPlayerServiceListener:Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction$OnPlayerServiceListener;

    .line 312
    new-instance v0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$6;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$6;-><init>(Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;)V

    iput-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->mSystemListener:Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction$OnSystemListener;

    .line 333
    new-instance v0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$7;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$7;-><init>(Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;)V

    iput-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->mMediaListener:Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction$OnMediaListener;

    .line 352
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;)Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->mPlayerView:Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->handleClose()V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;
    .param p1, "x1"    # Z

    .prologue
    .line 60
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->sendMusicCoverBroadcast(Z)V

    return-void
.end method

.method static synthetic access$400(Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;)Landroid/widget/ProgressBar;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->mBuffering:Landroid/widget/ProgressBar;

    return-object v0
.end method

.method static synthetic access$500(Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;

    .prologue
    .line 60
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->mCloseButton:Landroid/view/View;

    return-object v0
.end method

.method private createWindow(Landroid/view/View;)V
    .locals 6
    .param p1, "root"    # Landroid/view/View;

    .prologue
    const/4 v5, 0x1

    .line 193
    const-string v2, "MusicSCover"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "createWindow() root "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    new-instance v2, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;

    invoke-virtual {p0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, p0, v3, p1}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;-><init>(Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;Landroid/content/Context;Landroid/view/View;)V

    iput-object v2, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->mPlayerView:Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;

    .line 198
    const v2, 0x7f0d01b4

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->mCloseButton:Landroid/view/View;

    .line 199
    iget-object v2, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->mCloseButton:Landroid/view/View;

    new-instance v3, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$3;

    invoke-direct {v3, p0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$3;-><init>(Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 206
    new-instance v2, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$CustomDialog;

    invoke-direct {v2, p0, p0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$CustomDialog;-><init>(Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;Landroid/content/Context;)V

    iput-object v2, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->mDialog:Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$CustomDialog;

    .line 207
    iget-object v2, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->mDialog:Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$CustomDialog;

    new-instance v3, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$4;

    invoke-direct {v3, p0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$4;-><init>(Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;)V

    invoke-virtual {v2, v3}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$CustomDialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 218
    const v2, 0x7f0d01b8

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ProgressBar;

    iput-object v2, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->mBuffering:Landroid/widget/ProgressBar;

    .line 221
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "sview_color_use_all"

    invoke-static {v2, v3, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v2

    if-ne v2, v5, :cond_0

    .line 224
    const v2, 0x7f0d01af

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const v3, 0x7f02009f

    invoke-virtual {v2, v3}, Landroid/view/View;->setBackgroundResource(I)V

    .line 227
    :cond_0
    const v2, 0x7f0d01b1

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-direct {p0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->getBackgroundColor()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/view/View;->setBackgroundColor(I)V

    .line 230
    iget-object v2, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->mDialog:Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$CustomDialog;

    invoke-virtual {v2}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$CustomDialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 231
    .local v1, "w":Landroid/view/Window;
    invoke-virtual {v1, v5}, Landroid/view/Window;->requestFeature(I)Z

    .line 232
    iget-object v2, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->mDialog:Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$CustomDialog;

    invoke-virtual {v2, p1}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$CustomDialog;->setContentView(Landroid/view/View;)V

    .line 234
    const/16 v2, 0x1388

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/view/WindowManagerCompat;->getSviewCoverLayoutParam(Landroid/view/Window;I)Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 236
    .local v0, "p":Landroid/view/WindowManager$LayoutParams;
    iget v2, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    const v3, -0x1000001

    and-int/2addr v2, v3

    and-int/lit8 v2, v2, -0x3

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 238
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c0181

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 240
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c0180

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 242
    const/16 v2, 0x30

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 243
    const/4 v2, 0x0

    iput v2, v0, Landroid/view/WindowManager$LayoutParams;->verticalMargin:F

    .line 244
    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    .line 245
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 246
    return-void
.end method

.method private getBackgroundColor()I
    .locals 8

    .prologue
    const v7, 0x7f0b0091

    const/16 v6, 0xef

    .line 249
    const/4 v1, 0x0

    .line 250
    .local v1, "color":I
    const/4 v0, 0x0

    .line 251
    .local v0, "bgColor":I
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 253
    .local v2, "context":Landroid/content/Context;
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "sview_color_use_all"

    const/4 v5, 0x1

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-nez v3, :cond_0

    .line 256
    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b009c

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 275
    :goto_0
    return v1

    .line 259
    :cond_0
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "sview_color_random"

    const/4 v5, 0x0

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v3

    if-nez v3, :cond_1

    .line 261
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "s_vew_cover_background_color"

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 264
    invoke-static {v0}, Landroid/graphics/Color;->red(I)I

    move-result v3

    invoke-static {v0}, Landroid/graphics/Color;->green(I)I

    move-result v4

    invoke-static {v0}, Landroid/graphics/Color;->blue(I)I

    move-result v5

    invoke-static {v6, v3, v4, v5}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    goto :goto_0

    .line 268
    :cond_1
    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "sview_bg_display_random"

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    .line 271
    invoke-static {v0}, Landroid/graphics/Color;->red(I)I

    move-result v3

    invoke-static {v0}, Landroid/graphics/Color;->green(I)I

    move-result v4

    invoke-static {v0}, Landroid/graphics/Color;->blue(I)I

    move-result v5

    invoke-static {v6, v3, v4, v5}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    goto :goto_0
.end method

.method private handleClose()V
    .locals 2

    .prologue
    .line 161
    const-string v0, "MusicSCover"

    const-string v1, "handleClose() "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 163
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->mDialog:Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$CustomDialog;

    if-eqz v0, :cond_0

    .line 164
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->mDialog:Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$CustomDialog;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$CustomDialog;->dismiss()V

    .line 165
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->mDialog:Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$CustomDialog;

    .line 168
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->stopSelf()V

    .line 169
    return-void
.end method

.method private sendMusicCoverBroadcast(Z)V
    .locals 3
    .param p1, "miniModeUI"    # Z

    .prologue
    .line 342
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.cover.STATE_CHANGE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 344
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "suppressCoverUI"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 346
    const-string v1, "miniModeUI"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 347
    const-string v1, "sender"

    sget-object v2, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->CLASSNAME:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 348
    const-string v1, "color"

    invoke-direct {p0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->getBackgroundColor()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 349
    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->sendBroadcast(Landroid/content/Intent;)V

    .line 350
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 157
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 6

    .prologue
    .line 108
    const-string v1, "MusicSCover"

    const-string v2, "onCreate()"

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 109
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 111
    const v1, 0x103012b

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->setTheme(I)V

    .line 112
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f04008c

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 113
    .local v0, "root":Landroid/view/View;
    invoke-direct {p0, v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->createWindow(Landroid/view/View;)V

    .line 114
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/musicplus/util/UiUtils;->checkHoverUI(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 115
    iget-object v1, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->mPlayerView:Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;

    invoke-virtual {v1}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;->updateHoverView()V

    .line 118
    :cond_0
    iget-object v1, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->mConnection:Landroid/content/ServiceConnection;

    invoke-static {p0, v1}, Lcom/samsung/musicplus/util/ServiceUtils;->bindToService(Landroid/content/Context;Landroid/content/ServiceConnection;)Lcom/samsung/musicplus/util/ServiceUtils$ServiceToken;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->mServiceToken:Lcom/samsung/musicplus/util/ServiceUtils$ServiceToken;

    .line 120
    new-instance v1, Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->mSviewCoverCommonAction:Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;

    .line 121
    iget-object v1, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->mSviewCoverCommonAction:Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;

    iget-object v2, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->mPlayerServiceListener:Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction$OnPlayerServiceListener;

    invoke-virtual {v1, v2}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;->setOnPlayerServiceListener(Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction$OnPlayerServiceListener;)V

    .line 122
    iget-object v1, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->mSviewCoverCommonAction:Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;

    iget-object v2, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->mSystemListener:Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction$OnSystemListener;

    invoke-virtual {v1, v2}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;->setOnSystemListener(Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction$OnSystemListener;)V

    .line 123
    iget-object v1, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->mSviewCoverCommonAction:Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;

    iget-object v2, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->mMediaListener:Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction$OnMediaListener;

    invoke-virtual {v1, v2}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;->setOnMediaListener(Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction$OnMediaListener;)V

    .line 124
    iget-object v1, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->mSviewCoverCommonAction:Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;

    invoke-virtual {v1}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;->registerReceiver()V

    .line 126
    iget-object v1, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->mDialog:Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$CustomDialog;

    invoke-virtual {v1}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$CustomDialog;->show()V

    .line 127
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    new-instance v2, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$2;

    invoke-direct {v2, p0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$2;-><init>(Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;)V

    const-wide/16 v4, 0x190

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 133
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 137
    const-string v0, "MusicSCover"

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->mDialog:Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$CustomDialog;

    if-eqz v0, :cond_0

    .line 140
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->mDialog:Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$CustomDialog;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$CustomDialog;->dismiss()V

    .line 141
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->mDialog:Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$CustomDialog;

    .line 144
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->mPlayerView:Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;->onDestroy()V

    .line 146
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->mServiceToken:Lcom/samsung/musicplus/util/ServiceUtils$ServiceToken;

    invoke-static {v0}, Lcom/samsung/musicplus/util/ServiceUtils;->unbindFromService(Lcom/samsung/musicplus/util/ServiceUtils$ServiceToken;)V

    .line 148
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->mSviewCoverCommonAction:Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction;->unregisterReceiver()V

    .line 150
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->sendMusicCoverBroadcast(Z)V

    .line 152
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 153
    return-void
.end method
