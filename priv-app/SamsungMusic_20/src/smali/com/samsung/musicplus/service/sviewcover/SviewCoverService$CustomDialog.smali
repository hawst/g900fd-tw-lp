.class Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$CustomDialog;
.super Landroid/app/Dialog;
.source "SviewCoverService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "CustomDialog"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;


# direct methods
.method public constructor <init>(Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;Landroid/content/Context;)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 177
    iput-object p1, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$CustomDialog;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;

    .line 178
    invoke-direct {p0, p2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;)V

    .line 179
    return-void
.end method


# virtual methods
.method public onWindowFocusChanged(Z)V
    .locals 3
    .param p1, "hasFocus"    # Z

    .prologue
    .line 183
    const-string v0, "MusicSCover"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onWindowFocusChanged() - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    invoke-super {p0, p1}, Landroid/app/Dialog;->onWindowFocusChanged(Z)V

    .line 186
    if-nez p1, :cond_0

    .line 187
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$CustomDialog;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;

    # invokes: Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->handleClose()V
    invoke-static {v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->access$100(Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;)V

    .line 189
    :cond_0
    return-void
.end method
