.class public Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;
.super Ljava/lang/Object;
.source "SviewCoverNowPlayingList.java"

# interfaces
.implements Lcom/samsung/musicplus/player/NowPlayingCursorLoader$OnQueryCompleteListener;


# static fields
.field private static final NOW_PLAYING_LIST_ANIMATION:I = 0x7f050014


# instance fields
.field private mAdapter:Lcom/samsung/musicplus/widget/list/NowPlayingListAdapter;

.field private mListView:Landroid/widget/ListView;

.field private mLoader:Lcom/samsung/musicplus/player/NowPlayingCursorLoader;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILandroid/view/View;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "layout"    # I
    .param p3, "container"    # Landroid/view/View;

    .prologue
    const/4 v6, 0x0

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    new-instance v0, Lcom/samsung/musicplus/player/NowPlayingCursorLoader;

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/samsung/musicplus/util/ServiceUtils;->sService:Lcom/samsung/musicplus/service/IPlayerService;

    invoke-direct {v0, v1, v2, p0}, Lcom/samsung/musicplus/player/NowPlayingCursorLoader;-><init>(Landroid/content/ContentResolver;Lcom/samsung/musicplus/service/IPlayerService;Lcom/samsung/musicplus/player/NowPlayingCursorLoader$OnQueryCompleteListener;)V

    iput-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;->mLoader:Lcom/samsung/musicplus/player/NowPlayingCursorLoader;

    .line 56
    new-instance v0, Lcom/samsung/musicplus/widget/list/NowPlayingListAdapter;

    const v3, 0x7f020024

    const v4, 0x7f050014

    const/4 v5, 0x0

    move-object v1, p1

    move v2, p2

    move v7, v6

    invoke-direct/range {v0 .. v7}, Lcom/samsung/musicplus/widget/list/NowPlayingListAdapter;-><init>(Landroid/content/Context;IIILandroid/database/Cursor;IZ)V

    iput-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;->mAdapter:Lcom/samsung/musicplus/widget/list/NowPlayingListAdapter;

    .line 59
    const v0, 0x7f0d0062

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;->mListView:Landroid/widget/ListView;

    .line 61
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;->mListView:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    .line 62
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;->mAdapter:Lcom/samsung/musicplus/widget/list/NowPlayingListAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 63
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;->mListView:Landroid/widget/ListView;

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;->setOnItemClickListener(Landroid/widget/ListView;)V

    .line 64
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, v6}, Landroid/widget/ListView;->setEnableDragBlock(Z)V

    .line 66
    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;)Landroid/widget/ListView;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;->mListView:Landroid/widget/ListView;

    return-object v0
.end method

.method private getColumnIndicesAndSetIndex(Landroid/database/Cursor;)V
    .locals 10
    .param p1, "c"    # Landroid/database/Cursor;

    .prologue
    const/4 v4, -0x1

    .line 85
    if-eqz p1, :cond_0

    .line 86
    const-string v0, "_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    .line 87
    .local v1, "mAudioIdIdx":I
    const-string v0, "title"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 88
    .local v2, "mText1Idx":I
    const-string v0, "artist"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 89
    .local v3, "mText2Idx":I
    const-string v0, "sampling_rate"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v7

    .line 90
    .local v7, "mSamplingRateIdx":I
    const-string v0, "bit_depth"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v8

    .line 91
    .local v8, "mBitDepthIdx":I
    const-string v0, "is_secretbox"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v9

    .line 93
    .local v9, "mSecretBoxIdx":I
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;->mAdapter:Lcom/samsung/musicplus/widget/list/NowPlayingListAdapter;

    move v5, v4

    move v6, v4

    invoke-virtual/range {v0 .. v9}, Lcom/samsung/musicplus/widget/list/NowPlayingListAdapter;->setIndex(IIIIIIIII)V

    .line 96
    .end local v1    # "mAudioIdIdx":I
    .end local v2    # "mText1Idx":I
    .end local v3    # "mText2Idx":I
    .end local v7    # "mSamplingRateIdx":I
    .end local v8    # "mBitDepthIdx":I
    .end local v9    # "mSecretBoxIdx":I
    :cond_0
    return-void
.end method

.method private setOnItemClickListener(Landroid/widget/ListView;)V
    .locals 1
    .param p1, "v"    # Landroid/widget/ListView;

    .prologue
    .line 69
    new-instance v0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList$1;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList$1;-><init>(Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;)V

    invoke-virtual {p1, v0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 75
    return-void
.end method


# virtual methods
.method public hide()V
    .locals 2

    .prologue
    .line 118
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;->stopAnimation()V

    .line 119
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;->mListView:Landroid/widget/ListView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 120
    return-void
.end method

.method public invalidateViews()V
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;->mListView:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    .line 165
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->invalidateViews()V

    .line 167
    :cond_0
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 143
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;->stopAnimation()V

    .line 144
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;->mLoader:Lcom/samsung/musicplus/player/NowPlayingCursorLoader;

    if-eqz v0, :cond_0

    .line 145
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;->mLoader:Lcom/samsung/musicplus/player/NowPlayingCursorLoader;

    invoke-virtual {v0}, Lcom/samsung/musicplus/player/NowPlayingCursorLoader;->release()V

    .line 146
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;->mAdapter:Lcom/samsung/musicplus/widget/list/NowPlayingListAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/widget/list/NowPlayingListAdapter;->changeCursor(Landroid/database/Cursor;)V

    .line 148
    :cond_0
    return-void
.end method

.method public onNowPlayingQueryComplete(Lcom/samsung/musicplus/player/NowPlayingCursor;)V
    .locals 1
    .param p1, "c"    # Lcom/samsung/musicplus/player/NowPlayingCursor;

    .prologue
    .line 79
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;->getColumnIndicesAndSetIndex(Landroid/database/Cursor;)V

    .line 80
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;->mAdapter:Lcom/samsung/musicplus/widget/list/NowPlayingListAdapter;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/widget/list/NowPlayingListAdapter;->changeCursor(Landroid/database/Cursor;)V

    .line 81
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;->setCurrentPosition()V

    .line 82
    return-void
.end method

.method public refreshListView()V
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;->mLoader:Lcom/samsung/musicplus/player/NowPlayingCursorLoader;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;->mListView:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    .line 155
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;->mLoader:Lcom/samsung/musicplus/player/NowPlayingCursorLoader;

    invoke-virtual {v0}, Lcom/samsung/musicplus/player/NowPlayingCursorLoader;->startRequery()V

    .line 156
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->invalidateViews()V

    .line 158
    :cond_0
    return-void
.end method

.method public setCurrentPosition()V
    .locals 3

    .prologue
    .line 102
    iget-object v1, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;->mListView:Landroid/widget/ListView;

    if-eqz v1, :cond_0

    .line 103
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getListPosition()I

    move-result v0

    .line 104
    .local v0, "current":I
    if-ltz v0, :cond_0

    .line 105
    iget-object v1, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;->mListView:Landroid/widget/ListView;

    new-instance v2, Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList$2;

    invoke-direct {v2, p0, v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList$2;-><init>(Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;I)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->post(Ljava/lang/Runnable;)Z

    .line 115
    .end local v0    # "current":I
    :cond_0
    return-void
.end method

.method public setEnable(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 128
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;->mListView:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    .line 129
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, p1}, Landroid/widget/ListView;->setEnabled(Z)V

    .line 131
    :cond_0
    return-void
.end method

.method public setPlayingId()V
    .locals 4

    .prologue
    .line 170
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;->mAdapter:Lcom/samsung/musicplus/widget/list/NowPlayingListAdapter;

    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getCurrentAudioId()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->setPlayingId(J)V

    .line 171
    return-void
.end method

.method public show()V
    .locals 2

    .prologue
    .line 123
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;->invalidateViews()V

    .line 124
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;->mListView:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setVisibility(I)V

    .line 125
    return-void
.end method

.method public stopAnimation()V
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;->mAdapter:Lcom/samsung/musicplus/widget/list/NowPlayingListAdapter;

    invoke-virtual {v0}, Lcom/samsung/musicplus/widget/list/TrackListAdapter;->stopUpdateView()V

    .line 137
    return-void
.end method
