.class Lcom/samsung/musicplus/service/MultiPlayer$15;
.super Ljava/lang/Object;
.source "MultiPlayer.java"

# interfaces
.implements Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$OnErrorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/service/MultiPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/service/MultiPlayer;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/service/MultiPlayer;)V
    .locals 0

    .prologue
    .line 1788
    iput-object p1, p0, Lcom/samsung/musicplus/service/MultiPlayer$15;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(I)V
    .locals 3
    .param p1, "error"    # I

    .prologue
    .line 1792
    const-string v0, "MusicPlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "AV player error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1795
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer$15;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    const/4 v1, 0x0

    # invokes: Lcom/samsung/musicplus/service/MultiPlayer;->handleDmrError(Z)V
    invoke-static {v0, v1}, Lcom/samsung/musicplus/service/MultiPlayer;->access$3000(Lcom/samsung/musicplus/service/MultiPlayer;Z)V

    .line 1797
    return-void
.end method

.method public onPlayResponseReceivedError(I)V
    .locals 5
    .param p1, "error"    # I

    .prologue
    const/4 v4, 0x0

    .line 1801
    const-string v1, "MusicPlayer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onPlayResponseReceivedError error: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1803
    iget-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer$15;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # invokes: Lcom/samsung/musicplus/service/MultiPlayer;->handleDmrError(Z)V
    invoke-static {v1, v4}, Lcom/samsung/musicplus/service/MultiPlayer;->access$3000(Lcom/samsung/musicplus/service/MultiPlayer;Z)V

    .line 1806
    if-nez p1, :cond_1

    .line 1807
    const v0, 0x7f1001b2

    .line 1814
    .local v0, "errorMsg":I
    :goto_0
    iget-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer$15;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # getter for: Lcom/samsung/musicplus/service/MultiPlayer;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/musicplus/service/MultiPlayer;->access$2100(Lcom/samsung/musicplus/service/MultiPlayer;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/musicplus/util/UiUtils;->isMusicUiTop(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1815
    iget-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer$15;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # getter for: Lcom/samsung/musicplus/service/MultiPlayer;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/musicplus/service/MultiPlayer;->access$2100(Lcom/samsung/musicplus/service/MultiPlayer;)Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 1818
    :cond_0
    return-void

    .line 1808
    .end local v0    # "errorMsg":I
    :cond_1
    const/4 v1, 0x1

    if-ne p1, v1, :cond_2

    .line 1809
    const v0, 0x7f100125

    .restart local v0    # "errorMsg":I
    goto :goto_0

    .line 1811
    .end local v0    # "errorMsg":I
    :cond_2
    const v0, 0x7f1001b2

    .restart local v0    # "errorMsg":I
    goto :goto_0
.end method
