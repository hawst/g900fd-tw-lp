.class Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;
.super Lcom/samsung/musicplus/widget/RemoteViewBuilder;
.source "PlayerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/service/PlayerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "NotificationRemoteViewBuilder"
.end annotation


# instance fields
.field private mIsBig:Z

.field final synthetic this$0:Lcom/samsung/musicplus/service/PlayerService;


# direct methods
.method public constructor <init>(Lcom/samsung/musicplus/service/PlayerService;Landroid/content/Context;IZ)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "layoutId"    # I
    .param p4, "isBig"    # Z

    .prologue
    .line 5358
    iput-object p1, p0, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    .line 5359
    invoke-virtual {p1}, Lcom/samsung/musicplus/service/PlayerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0, p3}, Lcom/samsung/musicplus/widget/RemoteViewBuilder;-><init>(Landroid/content/Context;I)V

    .line 5356
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->mIsBig:Z

    .line 5360
    iput-boolean p4, p0, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->mIsBig:Z

    .line 5361
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->setButtons()Lcom/samsung/musicplus/widget/RemoteViewBuilder;

    .line 5362
    return-void
.end method


# virtual methods
.method public setAlbumArt(Landroid/graphics/Bitmap;)Lcom/samsung/musicplus/widget/RemoteViewBuilder;
    .locals 4
    .param p1, "album"    # Landroid/graphics/Bitmap;

    .prologue
    const v3, 0x7f0d0187

    .line 5419
    const-string v0, "MusicService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setAlbumArt() - bm: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 5420
    if-nez p1, :cond_0

    .line 5421
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->mRemoteView:Landroid/widget/RemoteViews;

    const v1, 0x7f02003b

    invoke-virtual {v0, v3, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 5427
    :goto_0
    return-object p0

    .line 5424
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->mRemoteView:Landroid/widget/RemoteViews;

    invoke-virtual {v0, v3, p1}, Landroid/widget/RemoteViews;->setImageViewBitmap(ILandroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method protected setButtons()Lcom/samsung/musicplus/widget/RemoteViewBuilder;
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 5366
    invoke-super {p0}, Lcom/samsung/musicplus/widget/RemoteViewBuilder;->setButtons()Lcom/samsung/musicplus/widget/RemoteViewBuilder;

    .line 5368
    iget-object v5, p0, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->mContext:Landroid/content/Context;

    new-instance v6, Landroid/content/Intent;

    const-string v7, "com.samsung.musicplus.musicservicecommand.rew.down"

    invoke-direct {v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {v5, v9, v6, v9}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    .line 5370
    .local v3, "rewindButtonDown":Landroid/app/PendingIntent;
    iget-object v5, p0, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->mContext:Landroid/content/Context;

    new-instance v6, Landroid/content/Intent;

    const-string v7, "com.samsung.musicplus.musicservicecommand.rew.up"

    invoke-direct {v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {v5, v9, v6, v9}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v4

    .line 5373
    .local v4, "rewindButtonUp":Landroid/app/PendingIntent;
    iget-object v5, p0, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->mContext:Landroid/content/Context;

    new-instance v6, Landroid/content/Intent;

    const-string v7, "com.samsung.musicplus.musicservicecommand.ff.down"

    invoke-direct {v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {v5, v9, v6, v9}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 5375
    .local v1, "fastforwardButtonDown":Landroid/app/PendingIntent;
    iget-object v5, p0, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->mContext:Landroid/content/Context;

    new-instance v6, Landroid/content/Intent;

    const-string v7, "com.samsung.musicplus.musicservicecommand.ff.up"

    invoke-direct {v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {v5, v9, v6, v9}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    .line 5378
    .local v2, "fastforwardButtonUp":Landroid/app/PendingIntent;
    iget-object v5, p0, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    const v6, 0x7010002

    new-instance v7, Landroid/content/Intent;

    const-string v8, "com.samsung.musicplus.musicservicecommand.ACTION_MUSIC_CLOSE"

    invoke-direct {v7, v8}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {v5, v6, v7, v9}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 5380
    .local v0, "close":Landroid/app/PendingIntent;
    iget-boolean v5, p0, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->mIsBig:Z

    if-eqz v5, :cond_0

    .line 5381
    iget-object v5, p0, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->mRemoteView:Landroid/widget/RemoteViews;

    const v6, 0x7f0d00f7

    iget-object v7, p0, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->mContext:Landroid/content/Context;

    const-string v8, "com.samsung.musicplus.musicservicecommand.shuffle"

    invoke-virtual {p0, v7, v8}, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->getPlayerServiceIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 5383
    iget-object v5, p0, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->mRemoteView:Landroid/widget/RemoteViews;

    const v6, 0x7f0d00f8

    iget-object v7, p0, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->mContext:Landroid/content/Context;

    const-string v8, "com.samsung.musicplus.musicservicecommand.repeat"

    invoke-virtual {p0, v7, v8}, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->getPlayerServiceIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 5386
    :cond_0
    iget-object v5, p0, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->mRemoteView:Landroid/widget/RemoteViews;

    const v6, 0x7f0d018c

    invoke-virtual {v5, v6, v0}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 5393
    return-object p0
.end method

.method public setMarqueeEnabled(Z)Lcom/samsung/musicplus/widget/RemoteViewBuilder;
    .locals 6
    .param p1, "enable"    # Z

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 5489
    iget-object v3, p0, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->mRemoteView:Landroid/widget/RemoteViews;

    const v4, 0x7f0d00d6

    const-string v5, "setSelected"

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v4, v5, v0}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 5490
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->mRemoteView:Landroid/widget/RemoteViews;

    const v3, 0x7f0d00d5

    const-string v4, "setSelected"

    if-eqz p1, :cond_1

    :goto_1
    invoke-virtual {v0, v3, v4, v1}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 5491
    return-object p0

    :cond_0
    move v0, v2

    .line 5489
    goto :goto_0

    :cond_1
    move v1, v2

    .line 5490
    goto :goto_1
.end method

.method public setPlayController(I)Lcom/samsung/musicplus/widget/RemoteViewBuilder;
    .locals 0
    .param p1, "listType"    # I

    .prologue
    .line 5412
    invoke-super {p0, p1}, Lcom/samsung/musicplus/widget/RemoteViewBuilder;->setPlayController(I)Lcom/samsung/musicplus/widget/RemoteViewBuilder;

    .line 5414
    return-object p0
.end method

.method public setPlayStatus(IZ)Lcom/samsung/musicplus/widget/RemoteViewBuilder;
    .locals 4
    .param p1, "listType"    # I
    .param p2, "isPlaying"    # Z

    .prologue
    const v3, 0x7f0d00fa

    .line 5398
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 5399
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->mRemoteView:Landroid/widget/RemoteViews;

    const v1, 0x7f0200b0

    invoke-virtual {v0, v3, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 5401
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->mRemoteView:Landroid/widget/RemoteViews;

    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    const v2, 0x7f100193

    invoke-virtual {v1, v2}, Lcom/samsung/musicplus/service/PlayerService;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Landroid/widget/RemoteViews;->setContentDescription(ILjava/lang/CharSequence;)V

    .line 5407
    :goto_0
    return-object p0

    .line 5403
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->mRemoteView:Landroid/widget/RemoteViews;

    const v1, 0x7f0200b1

    invoke-virtual {v0, v3, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 5405
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->mRemoteView:Landroid/widget/RemoteViews;

    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    const v2, 0x7f100194

    invoke-virtual {v1, v2}, Lcom/samsung/musicplus/service/PlayerService;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Landroid/widget/RemoteViews;->setContentDescription(ILjava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public setTitles(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/musicplus/widget/RemoteViewBuilder;
    .locals 10
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "artist"    # Ljava/lang/String;

    .prologue
    const v9, 0x7f0d00d6

    const v8, 0x7f0d00d5

    const/4 v4, 0x1

    const/4 v5, 0x0

    const v6, 0x7f1001bb

    .line 5432
    if-nez p1, :cond_1

    iget-object v3, p0, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v3, v6}, Lcom/samsung/musicplus/service/PlayerService;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 5433
    .local v2, "title_noti":Ljava/lang/String;
    :goto_0
    if-nez p1, :cond_2

    iget-object v3, p0, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v3, v6}, Lcom/samsung/musicplus/service/PlayerService;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 5435
    :goto_1
    if-nez p1, :cond_3

    iget-object v3, p0, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v3, v6}, Lcom/samsung/musicplus/service/PlayerService;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 5436
    .local v0, "artist_noti":Ljava/lang/String;
    :goto_2
    if-eqz p2, :cond_0

    const-string v3, "<unknown>"

    invoke-virtual {v3, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    :cond_0
    iget-object v3, p0, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v3, v6}, Lcom/samsung/musicplus/service/PlayerService;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 5439
    :goto_3
    iget-object v3, p0, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->mRemoteView:Landroid/widget/RemoteViews;

    invoke-virtual {v3, v9, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 5440
    iget-object v3, p0, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->mRemoteView:Landroid/widget/RemoteViews;

    invoke-virtual {v3, v8, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 5445
    iget-object v3, p0, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    const-string v6, "power"

    invoke-virtual {v3, v6}, Lcom/samsung/musicplus/service/PlayerService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/PowerManager;

    invoke-virtual {v3}, Landroid/os/PowerManager;->isInteractive()Z

    move-result v1

    .line 5447
    .local v1, "isInteractive":Z
    iget-object v6, p0, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->mRemoteView:Landroid/widget/RemoteViews;

    const-string v7, "setSelected"

    if-eqz v1, :cond_5

    move v3, v4

    :goto_4
    invoke-virtual {v6, v9, v7, v3}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 5448
    iget-object v3, p0, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->mRemoteView:Landroid/widget/RemoteViews;

    const-string v6, "setSelected"

    if-eqz v1, :cond_6

    :goto_5
    invoke-virtual {v3, v8, v6, v4}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 5450
    return-object p0

    .end local v0    # "artist_noti":Ljava/lang/String;
    .end local v1    # "isInteractive":Z
    .end local v2    # "title_noti":Ljava/lang/String;
    :cond_1
    move-object v2, p1

    .line 5432
    goto :goto_0

    .restart local v2    # "title_noti":Ljava/lang/String;
    :cond_2
    move-object v2, p1

    .line 5433
    goto :goto_1

    :cond_3
    move-object v0, p1

    .line 5435
    goto :goto_2

    .restart local v0    # "artist_noti":Ljava/lang/String;
    :cond_4
    move-object v0, p2

    .line 5436
    goto :goto_3

    .restart local v1    # "isInteractive":Z
    :cond_5
    move v3, v5

    .line 5447
    goto :goto_4

    :cond_6
    move v4, v5

    .line 5448
    goto :goto_5
.end method

.method public updateExtraButtons(II)Lcom/samsung/musicplus/widget/RemoteViewBuilder;
    .locals 6
    .param p1, "shuffle"    # I
    .param p2, "repeat"    # I

    .prologue
    const v5, 0x7f10019b

    const v4, 0x7f0d00f7

    const v3, 0x7f0d00f8

    .line 5455
    packed-switch p1, :pswitch_data_0

    .line 5468
    :goto_0
    packed-switch p2, :pswitch_data_1

    .line 5484
    :goto_1
    return-object p0

    .line 5457
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->mRemoteView:Landroid/widget/RemoteViews;

    const v1, 0x7f0200ae

    invoke-virtual {v0, v4, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 5458
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->mRemoteView:Landroid/widget/RemoteViews;

    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    const v2, 0x7f1001a0

    invoke-virtual {v1, v2}, Lcom/samsung/musicplus/service/PlayerService;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v4, v1}, Landroid/widget/RemoteViews;->setContentDescription(ILjava/lang/CharSequence;)V

    goto :goto_0

    .line 5461
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->mRemoteView:Landroid/widget/RemoteViews;

    const v1, 0x7f0200ad

    invoke-virtual {v0, v4, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 5462
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->mRemoteView:Landroid/widget/RemoteViews;

    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    const v2, 0x7f10019f

    invoke-virtual {v1, v2}, Lcom/samsung/musicplus/service/PlayerService;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v4, v1}, Landroid/widget/RemoteViews;->setContentDescription(ILjava/lang/CharSequence;)V

    goto :goto_0

    .line 5470
    :pswitch_2
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->mRemoteView:Landroid/widget/RemoteViews;

    const v1, 0x7f0200aa

    invoke-virtual {v0, v3, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 5471
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->mRemoteView:Landroid/widget/RemoteViews;

    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v1, v5}, Lcom/samsung/musicplus/service/PlayerService;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Landroid/widget/RemoteViews;->setContentDescription(ILjava/lang/CharSequence;)V

    goto :goto_1

    .line 5474
    :pswitch_3
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->mRemoteView:Landroid/widget/RemoteViews;

    const v1, 0x7f0200ab

    invoke-virtual {v0, v3, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 5475
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->mRemoteView:Landroid/widget/RemoteViews;

    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v1, v5}, Lcom/samsung/musicplus/service/PlayerService;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Landroid/widget/RemoteViews;->setContentDescription(ILjava/lang/CharSequence;)V

    goto :goto_1

    .line 5478
    :pswitch_4
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->mRemoteView:Landroid/widget/RemoteViews;

    const v1, 0x7f0200a9

    invoke-virtual {v0, v3, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 5479
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->mRemoteView:Landroid/widget/RemoteViews;

    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    const v2, 0x7f10019a

    invoke-virtual {v1, v2}, Lcom/samsung/musicplus/service/PlayerService;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v3, v1}, Landroid/widget/RemoteViews;->setContentDescription(ILjava/lang/CharSequence;)V

    goto :goto_1

    .line 5455
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch

    .line 5468
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
