.class Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog$7;
.super Landroid/content/BroadcastReceiver;
.source "SviewCoverDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# static fields
.field private static final SVIEW_TYPE_VOLUME:Ljava/lang/String; = "volume"


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;)V
    .locals 0

    .prologue
    .line 362
    iput-object p1, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog$7;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v10, 0x1

    const/4 v9, -0x1

    const/4 v8, 0x0

    .line 368
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 369
    .local v0, "action":Ljava/lang/String;
    # getter for: Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->access$500()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "mVolumeAlertReceiver() - "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 370
    const-string v5, "com.samsung.cover.REMOTEVIEWS_UPDATE"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 371
    const-string v5, "type"

    invoke-virtual {p2, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 374
    .local v2, "type":Ljava/lang/String;
    const-string v5, "volume"

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 375
    const-string v5, "visibility"

    invoke-virtual {p2, v5, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    .line 376
    .local v4, "visibility":Z
    const-string v5, "remote"

    invoke-virtual {p2, v5}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/widget/RemoteViews;

    .line 377
    .local v1, "rv":Landroid/widget/RemoteViews;
    const/4 v3, 0x0

    .line 378
    .local v3, "v":Landroid/view/View;
    if-eqz v1, :cond_0

    .line 379
    iget-object v5, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog$7;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;

    # getter for: Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mContext:Landroid/content/Context;
    invoke-static {v5}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->access$400(Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;)Landroid/content/Context;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog$7;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;

    # getter for: Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mVolumeAlertLayout:Landroid/widget/FrameLayout;
    invoke-static {v6}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->access$1100(Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;)Landroid/widget/FrameLayout;

    move-result-object v6

    invoke-virtual {v1, v5, v6}, Landroid/widget/RemoteViews;->apply(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 381
    :cond_0
    iget-object v5, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog$7;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;

    # getter for: Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mVolumeAlertLayout:Landroid/widget/FrameLayout;
    invoke-static {v5}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->access$1100(Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;)Landroid/widget/FrameLayout;

    move-result-object v5

    invoke-virtual {v5}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 382
    iget-object v5, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog$7;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;

    # getter for: Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mNowPlayingList:Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;
    invoke-static {v5}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->access$800(Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;)Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 383
    iget-object v5, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog$7;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;

    # getter for: Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mNowPlayingList:Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;
    invoke-static {v5}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->access$800(Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;)Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;

    move-result-object v5

    invoke-virtual {v5, v10}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;->setEnable(Z)V

    .line 385
    :cond_1
    iget-object v5, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog$7;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;

    # getter for: Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mCloseButton:Landroid/view/View;
    invoke-static {v5}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->access$1200(Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;)Landroid/view/View;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 386
    iget-object v5, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog$7;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;

    # getter for: Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mCloseButton:Landroid/view/View;
    invoke-static {v5}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->access$1200(Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v10}, Landroid/view/View;->setEnabled(Z)V

    .line 388
    :cond_2
    if-eqz v4, :cond_4

    if-eqz v3, :cond_4

    .line 389
    iget-object v5, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog$7;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;

    # getter for: Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mVolumeAlertLayout:Landroid/widget/FrameLayout;
    invoke-static {v5}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->access$1100(Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;)Landroid/widget/FrameLayout;

    move-result-object v5

    invoke-virtual {v5, v3, v9, v9}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;II)V

    .line 391
    iget-object v5, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog$7;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;

    # getter for: Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mNowPlayingList:Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;
    invoke-static {v5}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->access$800(Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;)Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;

    move-result-object v5

    if-eqz v5, :cond_3

    .line 392
    iget-object v5, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog$7;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;

    # getter for: Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mNowPlayingList:Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;
    invoke-static {v5}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->access$800(Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;)Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;

    move-result-object v5

    invoke-virtual {v5, v8}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;->setEnable(Z)V

    .line 394
    :cond_3
    iget-object v5, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog$7;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;

    # getter for: Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mCloseButton:Landroid/view/View;
    invoke-static {v5}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->access$1200(Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;)Landroid/view/View;

    move-result-object v5

    if-eqz v5, :cond_4

    .line 395
    iget-object v5, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog$7;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;

    # getter for: Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->mCloseButton:Landroid/view/View;
    invoke-static {v5}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->access$1200(Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/view/View;->setEnabled(Z)V

    .line 398
    :cond_4
    # getter for: Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverDialog;->access$500()Ljava/lang/String;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "mVolumeAlertReceiver() - "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " - visibility: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", rv: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 402
    .end local v1    # "rv":Landroid/widget/RemoteViews;
    .end local v2    # "type":Ljava/lang/String;
    .end local v3    # "v":Landroid/view/View;
    .end local v4    # "visibility":Z
    :cond_5
    return-void
.end method
