.class Lcom/samsung/musicplus/service/PlayerService$19;
.super Landroid/content/BroadcastReceiver;
.source "PlayerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/service/PlayerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/service/PlayerService;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/service/PlayerService;)V
    .locals 0

    .prologue
    .line 2355
    iput-object p1, p0, Lcom/samsung/musicplus/service/PlayerService$19;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v11, 0x3

    const/4 v10, 0x1

    .line 2359
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 2360
    .local v0, "action":Ljava/lang/String;
    const-string v7, "MusicService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "mOtherDeviceReceiver action : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2361
    const-string v7, "com.samsung.musicplus.dlna.connectivitychanged"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 2362
    const-string v7, "com.samsung.musicplus.dlna.connectivitychanged.extra.what"

    const/4 v8, -0x1

    invoke-virtual {p2, v7, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 2364
    .local v2, "errType":I
    if-eq v2, v10, :cond_1

    if-eq v2, v11, :cond_1

    .line 2418
    .end local v2    # "errType":I
    :cond_0
    :goto_0
    return-void

    .line 2368
    .restart local v2    # "errType":I
    :cond_1
    const-string v7, "com.samsung.musicplus.dlna.extra.deviceId"

    invoke-virtual {p2, v7}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 2369
    .local v1, "deviceId":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 2370
    if-ne v2, v10, :cond_2

    .line 2371
    iget-object v7, p0, Lcom/samsung/musicplus/service/PlayerService$19;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v7}, Lcom/samsung/musicplus/service/PlayerService;->getKeyWord()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 2372
    iget-object v7, p0, Lcom/samsung/musicplus/service/PlayerService$19;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;
    invoke-static {v7}, Lcom/samsung/musicplus/service/PlayerService;->access$100(Lcom/samsung/musicplus/service/PlayerService;)Lcom/samsung/musicplus/service/PlayerListManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/samsung/musicplus/service/PlayerListManager;->getDeviceName()Ljava/lang/String;

    move-result-object v4

    .line 2374
    .local v4, "name":Ljava/lang/String;
    iget-object v7, p0, Lcom/samsung/musicplus/service/PlayerService$19;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->resetNowPlayingList()V
    invoke-static {v7}, Lcom/samsung/musicplus/service/PlayerService;->access$4900(Lcom/samsung/musicplus/service/PlayerService;)V

    .line 2375
    invoke-static {p1}, Lcom/samsung/musicplus/util/UiUtils;->isMusicUiTop(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 2376
    invoke-static {p1, v4}, Lcom/samsung/musicplus/util/UiUtils;->launchDlnaErrorDialog(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 2379
    .end local v4    # "name":Ljava/lang/String;
    :cond_2
    if-ne v2, v11, :cond_0

    .line 2380
    iget-object v7, p0, Lcom/samsung/musicplus/service/PlayerService$19;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v7}, Lcom/samsung/musicplus/service/PlayerService;->getDlnaPlayingDmrId()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 2382
    iget-object v7, p0, Lcom/samsung/musicplus/service/PlayerService$19;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v7}, Lcom/samsung/musicplus/service/PlayerService;->pause()V

    .line 2384
    iget-object v7, p0, Lcom/samsung/musicplus/service/PlayerService$19;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v7, v10}, Lcom/samsung/musicplus/service/PlayerService;->changeToDefaultPlayer(Z)V

    .line 2385
    invoke-static {p1}, Lcom/samsung/musicplus/util/UiUtils;->isMusicUiTop(Landroid/content/Context;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 2386
    const/4 v7, 0x0

    invoke-static {p1, v7}, Lcom/samsung/musicplus/util/UiUtils;->launchDlnaErrorDialog(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 2391
    .end local v1    # "deviceId":Ljava/lang/String;
    .end local v2    # "errType":I
    :cond_3
    sget-object v7, Lcom/samsung/musicplus/provider/MusicContents$Audio$DeviceContents;->ACTION_S_LINK_INITIALIZE_CHANGED:Ljava/lang/String;

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    .line 2393
    iget-object v7, p0, Lcom/samsung/musicplus/service/PlayerService$19;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->isSlinkTrackList()Z
    invoke-static {v7}, Lcom/samsung/musicplus/service/PlayerService;->access$4800(Lcom/samsung/musicplus/service/PlayerService;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 2394
    invoke-static {p1}, Lcom/samsung/musicplus/provider/MusicContents$Audio$DeviceContents;->isNetworkInitialized(Landroid/content/Context;)Z

    move-result v5

    .line 2396
    .local v5, "network":Z
    if-nez v5, :cond_4

    .line 2399
    :cond_4
    const-string v7, "MusicService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "s link network initializ changed "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 2401
    .end local v5    # "network":Z
    :cond_5
    const-string v7, "android.intent.action.SAMSUNGACCOUNT_SIGNOUT_COMPLETED"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    .line 2404
    iget-object v7, p0, Lcom/samsung/musicplus/service/PlayerService$19;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->isSlinkTrackList()Z
    invoke-static {v7}, Lcom/samsung/musicplus/service/PlayerService;->access$4800(Lcom/samsung/musicplus/service/PlayerService;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 2405
    iget-object v7, p0, Lcom/samsung/musicplus/service/PlayerService$19;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->resetNowPlayingList()V
    invoke-static {v7}, Lcom/samsung/musicplus/service/PlayerService;->access$4900(Lcom/samsung/musicplus/service/PlayerService;)V

    goto/16 :goto_0

    .line 2407
    :cond_6
    sget-object v7, Lcom/samsung/musicplus/provider/MusicContents$Audio$DeviceContents;->ACTION_S_LINK_SIGNIN_CHANGED:Ljava/lang/String;

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 2408
    iget-object v7, p0, Lcom/samsung/musicplus/service/PlayerService$19;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->isSlinkTrackList()Z
    invoke-static {v7}, Lcom/samsung/musicplus/service/PlayerService;->access$4800(Lcom/samsung/musicplus/service/PlayerService;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 2409
    invoke-static {p1}, Lcom/samsung/musicplus/provider/MusicContents$Audio$Devices;->isSingedIn(Landroid/content/Context;)Z

    move-result v6

    .line 2410
    .local v6, "signIn":Z
    invoke-static {p1}, Lcom/samsung/musicplus/provider/MusicContents$Audio$Devices;->hasSamsungAccount(Landroid/content/Context;)Z

    move-result v3

    .line 2411
    .local v3, "hasAccount":Z
    const-string v7, "MusicService"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "S link signin state changed : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " hasAccount : "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2413
    if-eqz v6, :cond_7

    if-nez v3, :cond_0

    .line 2414
    :cond_7
    iget-object v7, p0, Lcom/samsung/musicplus/service/PlayerService$19;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->resetNowPlayingList()V
    invoke-static {v7}, Lcom/samsung/musicplus/service/PlayerService;->access$4900(Lcom/samsung/musicplus/service/PlayerService;)V

    goto/16 :goto_0
.end method
