.class Lcom/samsung/musicplus/service/PlayerService$7;
.super Landroid/content/BroadcastReceiver;
.source "PlayerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/service/PlayerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field isEntranceBT:Z

.field isEntranceHeadset:Z

.field final synthetic this$0:Lcom/samsung/musicplus/service/PlayerService;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/service/PlayerService;)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 721
    iput-object p1, p0, Lcom/samsung/musicplus/service/PlayerService$7;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 723
    iput-boolean v0, p0, Lcom/samsung/musicplus/service/PlayerService$7;->isEntranceBT:Z

    .line 724
    iput-boolean v0, p0, Lcom/samsung/musicplus/service/PlayerService$7;->isEntranceHeadset:Z

    return-void
.end method

.method private handleBtStateChange(Landroid/content/Intent;Z)V
    .locals 8
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "isFirst"    # Z

    .prologue
    const/4 v4, 0x1

    const/4 v0, 0x0

    .line 808
    const-string v5, "android.bluetooth.profile.extra.STATE"

    invoke-virtual {p1, v5, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 809
    .local v3, "state":I
    const-string v5, "android.bluetooth.profile.extra.PREVIOUS_STATE"

    invoke-virtual {p1, v5, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 810
    .local v2, "prevState":I
    const-string v5, "MusicService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "BT ACTION_SINK_STATE_CHANGED "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " -> "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 812
    if-eqz v3, :cond_0

    const/4 v5, 0x3

    if-ne v3, v5, :cond_6

    :cond_0
    move v1, v4

    .line 814
    .local v1, "disConnected":Z
    :goto_0
    if-eqz v1, :cond_2

    .line 818
    iget-object v5, p0, Lcom/samsung/musicplus/service/PlayerService$7;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mPausedByTransientLossOfFocus:Z
    invoke-static {v5}, Lcom/samsung/musicplus/service/PlayerService;->access$1800(Lcom/samsung/musicplus/service/PlayerService;)Z

    move-result v5

    if-nez v5, :cond_1

    iget-object v5, p0, Lcom/samsung/musicplus/service/PlayerService$7;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;
    invoke-static {v5}, Lcom/samsung/musicplus/service/PlayerService;->access$3400(Lcom/samsung/musicplus/service/PlayerService;)Lcom/samsung/musicplus/library/audio/SecAudioManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->isAudioPathBT()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 819
    :cond_1
    iget-object v5, p0, Lcom/samsung/musicplus/service/PlayerService$7;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->isDmrPlaying()Z
    invoke-static {v5}, Lcom/samsung/musicplus/service/PlayerService;->access$1400(Lcom/samsung/musicplus/service/PlayerService;)Z

    move-result v5

    if-eqz v5, :cond_7

    .line 820
    const-string v5, "MusicService"

    const-string v6, "DLNA DMR playing that means it\'s sound make other device like TV, so do not pause here"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 830
    :cond_2
    :goto_1
    sget-boolean v5, Lcom/samsung/musicplus/util/Version;->LIBRARY_OVER_API_1:Z

    if-eqz v5, :cond_5

    .line 836
    iget-object v5, p0, Lcom/samsung/musicplus/service/PlayerService$7;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;
    invoke-static {v5}, Lcom/samsung/musicplus/service/PlayerService;->access$3400(Lcom/samsung/musicplus/service/PlayerService;)Lcom/samsung/musicplus/library/audio/SecAudioManager;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->isWiredHeadsetOn()Z

    move-result v5

    if-nez v5, :cond_3

    if-nez v1, :cond_4

    :cond_3
    move v0, v4

    .line 837
    .local v0, "checkConnected":Z
    :cond_4
    iget-object v4, p0, Lcom/samsung/musicplus/service/PlayerService$7;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->updateAdaptSound(Z)V
    invoke-static {v4, v0}, Lcom/samsung/musicplus/service/PlayerService;->access$3500(Lcom/samsung/musicplus/service/PlayerService;Z)V

    .line 839
    const-string v4, "MusicService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "handleBtStateChange() : headset - "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/musicplus/service/PlayerService$7;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;
    invoke-static {v6}, Lcom/samsung/musicplus/service/PlayerService;->access$3400(Lcom/samsung/musicplus/service/PlayerService;)Lcom/samsung/musicplus/library/audio/SecAudioManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->isWiredHeadsetOn()Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", bt - "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/samsung/musicplus/service/PlayerService$7;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;
    invoke-static {v6}, Lcom/samsung/musicplus/service/PlayerService;->access$3400(Lcom/samsung/musicplus/service/PlayerService;)Lcom/samsung/musicplus/library/audio/SecAudioManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->isAudioPathBT()Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", disConnected : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 842
    if-nez v0, :cond_5

    if-nez p2, :cond_5

    .line 844
    iget-object v4, p0, Lcom/samsung/musicplus/service/PlayerService$7;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->warningAdaptSound()V
    invoke-static {v4}, Lcom/samsung/musicplus/service/PlayerService;->access$3600(Lcom/samsung/musicplus/service/PlayerService;)V

    .line 847
    .end local v0    # "checkConnected":Z
    :cond_5
    return-void

    .end local v1    # "disConnected":Z
    :cond_6
    move v1, v0

    .line 812
    goto/16 :goto_0

    .line 825
    .restart local v1    # "disConnected":Z
    :cond_7
    iget-object v5, p0, Lcom/samsung/musicplus/service/PlayerService$7;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v5}, Lcom/samsung/musicplus/service/PlayerService;->pause()V

    goto :goto_1
.end method

.method private handleHeadsetStateChange(ZZ)V
    .locals 4
    .param p1, "connected"    # Z
    .param p2, "isFirst"    # Z

    .prologue
    .line 852
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService$7;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->isMidi()Z
    invoke-static {v1}, Lcom/samsung/musicplus/service/PlayerService;->access$3700(Lcom/samsung/musicplus/service/PlayerService;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 854
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService$7;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService$7;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->getMidiVolumeRatio(Z)F
    invoke-static {v2, p1}, Lcom/samsung/musicplus/service/PlayerService;->access$3800(Lcom/samsung/musicplus/service/PlayerService;Z)F

    move-result v2

    # setter for: Lcom/samsung/musicplus/service/PlayerService;->mMaxVolume:F
    invoke-static {v1, v2}, Lcom/samsung/musicplus/service/PlayerService;->access$002(Lcom/samsung/musicplus/service/PlayerService;F)F

    .line 855
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService$7;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;
    invoke-static {v1}, Lcom/samsung/musicplus/service/PlayerService;->access$400(Lcom/samsung/musicplus/service/PlayerService;)Lcom/samsung/musicplus/service/MultiPlayer;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService$7;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mMaxVolume:F
    invoke-static {v2}, Lcom/samsung/musicplus/service/PlayerService;->access$000(Lcom/samsung/musicplus/service/PlayerService;)F

    move-result v2

    invoke-virtual {v1, v2}, Lcom/samsung/musicplus/service/MultiPlayer;->setVolume(F)V

    .line 861
    :cond_0
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService$7;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->isEnableAdaptSoundPath()Z
    invoke-static {v1}, Lcom/samsung/musicplus/service/PlayerService;->access$3900(Lcom/samsung/musicplus/service/PlayerService;)Z

    move-result v1

    if-nez v1, :cond_1

    if-eqz p1, :cond_3

    :cond_1
    const/4 v0, 0x1

    .line 862
    .local v0, "checkConnected":Z
    :goto_0
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService$7;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->updateAdaptSound(Z)V
    invoke-static {v1, v0}, Lcom/samsung/musicplus/service/PlayerService;->access$3500(Lcom/samsung/musicplus/service/PlayerService;Z)V

    .line 864
    const-string v1, "MusicService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "handleHeadsetStateChange() : headset - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/musicplus/service/PlayerService$7;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;
    invoke-static {v3}, Lcom/samsung/musicplus/service/PlayerService;->access$3400(Lcom/samsung/musicplus/service/PlayerService;)Lcom/samsung/musicplus/library/audio/SecAudioManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->isWiredHeadsetOn()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", bt - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/musicplus/service/PlayerService$7;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;
    invoke-static {v3}, Lcom/samsung/musicplus/service/PlayerService;->access$3400(Lcom/samsung/musicplus/service/PlayerService;)Lcom/samsung/musicplus/library/audio/SecAudioManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->isAudioPathBT()Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", connected : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 866
    if-nez v0, :cond_2

    if-nez p2, :cond_2

    .line 868
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService$7;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->warningAdaptSound()V
    invoke-static {v1}, Lcom/samsung/musicplus/service/PlayerService;->access$3600(Lcom/samsung/musicplus/service/PlayerService;)V

    .line 870
    :cond_2
    return-void

    .line 861
    .end local v0    # "checkConnected":Z
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isSideSyncOn(Ljava/lang/String;)Z
    .locals 1
    .param p1, "action"    # Ljava/lang/String;

    .prologue
    .line 803
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$7;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->getWifiDisplayAction()Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/musicplus/service/PlayerService;->access$2800(Lcom/samsung/musicplus/service/PlayerService;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$7;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/musicplus/library/audio/SideSyncManager;->isSideSyncConnected(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private needToNotify(Ljava/lang/String;)Z
    .locals 1
    .param p1, "action"    # Ljava/lang/String;

    .prologue
    .line 799
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/service/PlayerService$7;->isSideSyncOn(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 12
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const-wide/16 v10, 0x1f4

    const/4 v6, 0x1

    const/4 v9, 0x0

    .line 728
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v7

    .line 729
    .local v7, "action":Ljava/lang/String;
    const-string v0, "MusicService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "mHeadsetReceiver "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 731
    const-string v0, "android.bluetooth.a2dp.profile.action.CONNECTION_STATE_CHANGED"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 732
    iget-boolean v0, p0, Lcom/samsung/musicplus/service/PlayerService$7;->isEntranceBT:Z

    invoke-direct {p0, p2, v0}, Lcom/samsung/musicplus/service/PlayerService$7;->handleBtStateChange(Landroid/content/Intent;Z)V

    .line 733
    iget-boolean v0, p0, Lcom/samsung/musicplus/service/PlayerService$7;->isEntranceBT:Z

    if-eqz v0, :cond_0

    .line 734
    iput-boolean v9, p0, Lcom/samsung/musicplus/service/PlayerService$7;->isEntranceBT:Z

    .line 758
    :cond_0
    :goto_0
    sget-boolean v0, Lcom/samsung/musicplus/util/Features;->FLAG_SUPPORT_EXTERNAL_SOUNDALIVE:Z

    if-nez v0, :cond_1

    .line 760
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$7;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mPreferences:Landroid/content/SharedPreferences;
    invoke-static {v0}, Lcom/samsung/musicplus/service/PlayerService;->access$3100(Lcom/samsung/musicplus/service/PlayerService;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v2, "sa_auto_check"

    invoke-interface {v0, v2, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 762
    .local v1, "auto":Z
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$7;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService$7;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;
    invoke-static {v2}, Lcom/samsung/musicplus/service/PlayerService;->access$400(Lcom/samsung/musicplus/service/PlayerService;)Lcom/samsung/musicplus/service/MultiPlayer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/musicplus/service/MultiPlayer;->getNewSoundAliveCurrentPreset()I

    move-result v2

    iget-object v3, p0, Lcom/samsung/musicplus/service/PlayerService$7;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;
    invoke-static {v3}, Lcom/samsung/musicplus/service/PlayerService;->access$400(Lcom/samsung/musicplus/service/PlayerService;)Lcom/samsung/musicplus/service/MultiPlayer;

    move-result-object v3

    invoke-virtual {v3}, Lcom/samsung/musicplus/service/MultiPlayer;->getNewSoundAliveSquarePosition()[I

    move-result-object v3

    iget-object v4, p0, Lcom/samsung/musicplus/service/PlayerService$7;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;
    invoke-static {v4}, Lcom/samsung/musicplus/service/PlayerService;->access$400(Lcom/samsung/musicplus/service/PlayerService;)Lcom/samsung/musicplus/service/MultiPlayer;

    move-result-object v4

    invoke-virtual {v4}, Lcom/samsung/musicplus/service/MultiPlayer;->getNewSoundAliveRoundedStrength()[I

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/musicplus/service/PlayerService$7;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;
    invoke-static {v5}, Lcom/samsung/musicplus/service/PlayerService;->access$400(Lcom/samsung/musicplus/service/PlayerService;)Lcom/samsung/musicplus/service/MultiPlayer;

    move-result-object v5

    invoke-virtual {v5}, Lcom/samsung/musicplus/service/MultiPlayer;->getNewSoundAliveBandLevel()[I

    move-result-object v5

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->setSoundAliveV2(ZI[I[I[IZ)V
    invoke-static/range {v0 .. v6}, Lcom/samsung/musicplus/service/PlayerService;->access$3200(Lcom/samsung/musicplus/service/PlayerService;ZI[I[I[IZ)V

    .line 774
    .end local v1    # "auto":Z
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$7;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService$7;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;
    invoke-static {v2}, Lcom/samsung/musicplus/service/PlayerService;->access$400(Lcom/samsung/musicplus/service/PlayerService;)Lcom/samsung/musicplus/service/MultiPlayer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/samsung/musicplus/service/MultiPlayer;->getPlaySpeed()F

    move-result v2

    invoke-virtual {v0, v2, v6}, Lcom/samsung/musicplus/service/PlayerService;->setPlaySpeed(FZ)V

    .line 779
    invoke-direct {p0, v7}, Lcom/samsung/musicplus/service/PlayerService$7;->needToNotify(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 780
    const-string v0, "android.bluetooth.a2dp.profile.action.PLAYING_STATE_CHANGED"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 782
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$7;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mAudioPathHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/musicplus/service/PlayerService;->access$3300(Lcom/samsung/musicplus/service/PlayerService;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v6}, Landroid/os/Handler;->removeMessages(I)V

    .line 783
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$7;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mAudioPathHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/musicplus/service/PlayerService;->access$3300(Lcom/samsung/musicplus/service/PlayerService;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v6, v10, v11}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 789
    :cond_2
    :goto_1
    return-void

    .line 736
    :cond_3
    const-string v0, "android.intent.action.HEADSET_PLUG"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 737
    const-string v0, "state"

    invoke-virtual {p2, v0, v9}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v8

    .line 738
    .local v8, "connect":I
    const-string v0, "MusicService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Headset plug changed to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 739
    if-ne v8, v6, :cond_4

    move v0, v6

    :goto_2
    iget-boolean v2, p0, Lcom/samsung/musicplus/service/PlayerService$7;->isEntranceHeadset:Z

    invoke-direct {p0, v0, v2}, Lcom/samsung/musicplus/service/PlayerService$7;->handleHeadsetStateChange(ZZ)V

    .line 740
    iget-boolean v0, p0, Lcom/samsung/musicplus/service/PlayerService$7;->isEntranceHeadset:Z

    if-eqz v0, :cond_0

    .line 741
    iput-boolean v9, p0, Lcom/samsung/musicplus/service/PlayerService$7;->isEntranceHeadset:Z

    goto/16 :goto_0

    :cond_4
    move v0, v9

    .line 739
    goto :goto_2

    .line 743
    .end local v8    # "connect":I
    :cond_5
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$7;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->getWifiDisplayAction()Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/musicplus/service/PlayerService;->access$2800(Lcom/samsung/musicplus/service/PlayerService;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 744
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$7;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->sendPlayerBuffering(Z)V
    invoke-static {v0, v9}, Lcom/samsung/musicplus/service/PlayerService;->access$800(Lcom/samsung/musicplus/service/PlayerService;Z)V

    .line 745
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$7;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # setter for: Lcom/samsung/musicplus/service/PlayerService;->mIsConnectingWfd:Z
    invoke-static {v0, v9}, Lcom/samsung/musicplus/service/PlayerService;->access$2902(Lcom/samsung/musicplus/service/PlayerService;Z)Z

    goto/16 :goto_0

    .line 746
    :cond_6
    const-string v0, "com.sec.samsungsound.ACTION_SOUNDALIVE_CHANGED"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 747
    sget-boolean v0, Lcom/samsung/musicplus/util/Features;->FLAG_SUPPORT_EXTERNAL_SOUNDALIVE:Z

    if-nez v0, :cond_7

    .line 748
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$7;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->resetSoundAlive()V
    invoke-static {v0}, Lcom/samsung/musicplus/service/PlayerService;->access$3000(Lcom/samsung/musicplus/service/PlayerService;)V

    .line 752
    :cond_7
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$7;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.samsung.musicplus.action.SOUND_AVLIE_CHANGED"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Lcom/samsung/musicplus/service/PlayerService;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_1

    .line 754
    :cond_8
    const-string v0, "android.media.action.HDMI_AUDIO_PLUG"

    invoke-virtual {v0, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 755
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$7;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->isDmrPlaying()Z
    invoke-static {v0}, Lcom/samsung/musicplus/service/PlayerService;->access$1400(Lcom/samsung/musicplus/service/PlayerService;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 756
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$7;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0, v6}, Lcom/samsung/musicplus/service/PlayerService;->changeToDefaultPlayer(Z)V

    goto/16 :goto_0

    .line 785
    :cond_9
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$7;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mAudioPathHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/musicplus/service/PlayerService;->access$3300(Lcom/samsung/musicplus/service/PlayerService;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/os/Handler;->removeMessages(I)V

    .line 786
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$7;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mAudioPathHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/musicplus/service/PlayerService;->access$3300(Lcom/samsung/musicplus/service/PlayerService;)Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0, v9, v10, v11}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_1
.end method
