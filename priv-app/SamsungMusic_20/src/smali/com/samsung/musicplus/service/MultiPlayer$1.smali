.class Lcom/samsung/musicplus/service/MultiPlayer$1;
.super Landroid/os/Handler;
.source "MultiPlayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/service/MultiPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/service/MultiPlayer;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/service/MultiPlayer;)V
    .locals 0

    .prologue
    .line 247
    iput-object p1, p0, Lcom/samsung/musicplus/service/MultiPlayer$1;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 250
    const-string v0, "MusicPlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "mPrepareHandler handleMessage : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 251
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 283
    :goto_0
    return-void

    .line 262
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer$1;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # getter for: Lcom/samsung/musicplus/service/MultiPlayer;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;
    invoke-static {v0}, Lcom/samsung/musicplus/service/MultiPlayer;->access$000(Lcom/samsung/musicplus/service/MultiPlayer;)Lcom/samsung/musicplus/service/PlayerListManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerListManager;->getListType()I

    move-result v0

    const v1, 0x2000d

    if-ne v0, v1, :cond_0

    .line 263
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer$1;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # getter for: Lcom/samsung/musicplus/service/MultiPlayer;->mNextMedia:Lcom/samsung/musicplus/widget/NextMediaPlayer;
    invoke-static {v0}, Lcom/samsung/musicplus/service/MultiPlayer;->access$100(Lcom/samsung/musicplus/service/MultiPlayer;)Lcom/samsung/musicplus/widget/NextMediaPlayer;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer$1;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # getter for: Lcom/samsung/musicplus/service/MultiPlayer;->mNextMedia:Lcom/samsung/musicplus/widget/NextMediaPlayer;
    invoke-static {v0}, Lcom/samsung/musicplus/service/MultiPlayer;->access$100(Lcom/samsung/musicplus/service/MultiPlayer;)Lcom/samsung/musicplus/widget/NextMediaPlayer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/musicplus/widget/NextMediaPlayer;->isPrepared()Z

    move-result v0

    if-nez v0, :cond_1

    .line 264
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer$1;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # getter for: Lcom/samsung/musicplus/service/MultiPlayer;->mPrepareHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/samsung/musicplus/service/MultiPlayer;->access$200(Lcom/samsung/musicplus/service/MultiPlayer;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer$1;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # getter for: Lcom/samsung/musicplus/service/MultiPlayer;->mPrepareHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/samsung/musicplus/service/MultiPlayer;->access$200(Lcom/samsung/musicplus/service/MultiPlayer;)Landroid/os/Handler;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    .line 269
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer$1;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # invokes: Lcom/samsung/musicplus/service/MultiPlayer;->releaseNextMedia()V
    invoke-static {v0}, Lcom/samsung/musicplus/service/MultiPlayer;->access$300(Lcom/samsung/musicplus/service/MultiPlayer;)V

    .line 271
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer$1;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    const/4 v1, 0x1

    # invokes: Lcom/samsung/musicplus/service/MultiPlayer;->reset(Z)V
    invoke-static {v0, v1}, Lcom/samsung/musicplus/service/MultiPlayer;->access$400(Lcom/samsung/musicplus/service/MultiPlayer;Z)V

    .line 272
    iget-object v6, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v6, Lcom/samsung/musicplus/service/MultiPlayer$DataInfo;

    .line 273
    .local v6, "info":Lcom/samsung/musicplus/service/MultiPlayer$DataInfo;
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer$1;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    iget-object v1, v6, Lcom/samsung/musicplus/service/MultiPlayer$DataInfo;->uri:Landroid/net/Uri;

    iget-object v2, v6, Lcom/samsung/musicplus/service/MultiPlayer$DataInfo;->path:Ljava/lang/String;

    iget-object v3, p0, Lcom/samsung/musicplus/service/MultiPlayer$1;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # getter for: Lcom/samsung/musicplus/service/MultiPlayer;->mIsSupposedToBePlaying:Z
    invoke-static {v3}, Lcom/samsung/musicplus/service/MultiPlayer;->access$500(Lcom/samsung/musicplus/service/MultiPlayer;)Z

    move-result v3

    iget-wide v4, v6, Lcom/samsung/musicplus/service/MultiPlayer$DataInfo;->seekPosition:J

    # invokes: Lcom/samsung/musicplus/service/MultiPlayer;->setDataSource(Landroid/net/Uri;Ljava/lang/String;ZJ)Z
    invoke-static/range {v0 .. v5}, Lcom/samsung/musicplus/service/MultiPlayer;->access$600(Lcom/samsung/musicplus/service/MultiPlayer;Landroid/net/Uri;Ljava/lang/String;ZJ)Z

    goto :goto_0

    .line 276
    .end local v6    # "info":Lcom/samsung/musicplus/service/MultiPlayer$DataInfo;
    :pswitch_1
    iget-object v7, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v7, Landroid/net/Uri;

    .line 277
    .local v7, "uri":Landroid/net/Uri;
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer$1;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # invokes: Lcom/samsung/musicplus/service/MultiPlayer;->prepareNextMedia(Landroid/net/Uri;)V
    invoke-static {v0, v7}, Lcom/samsung/musicplus/service/MultiPlayer;->access$700(Lcom/samsung/musicplus/service/MultiPlayer;Landroid/net/Uri;)V

    goto :goto_0

    .line 251
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
