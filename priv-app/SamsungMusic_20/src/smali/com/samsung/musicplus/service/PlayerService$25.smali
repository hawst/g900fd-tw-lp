.class Lcom/samsung/musicplus/service/PlayerService$25;
.super Ljava/lang/Object;
.source "PlayerService.java"

# interfaces
.implements Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager$OnMusicBounceSettingChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/service/PlayerService;->ensureBounceManager()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/service/PlayerService;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/service/PlayerService;)V
    .locals 0

    .prologue
    .line 3644
    iput-object p1, p0, Lcom/samsung/musicplus/service/PlayerService$25;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSettingChanged()V
    .locals 2

    .prologue
    .line 3648
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService$25;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v1}, Lcom/samsung/musicplus/service/PlayerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager;->isEnableSystemAndMusicBounceSetting(Landroid/content/Context;)Z

    move-result v0

    .line 3650
    .local v0, "enable":Z
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService$25;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->handleBounceSetting(Z)V
    invoke-static {v1, v0}, Lcom/samsung/musicplus/service/PlayerService;->access$6600(Lcom/samsung/musicplus/service/PlayerService;Z)V

    .line 3651
    return-void
.end method
