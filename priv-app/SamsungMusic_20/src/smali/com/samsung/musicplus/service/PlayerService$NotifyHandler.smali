.class Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;
.super Landroid/os/Handler;
.source "PlayerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/service/PlayerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "NotifyHandler"
.end annotation


# static fields
.field private static final NOTIFY_META_CHANGED:I = 0x1

.field private static final NOTIFY_MUSIC_INFO:I = 0x7

.field private static final NOTIFY_PLAYSTATE_CHANGED:I = 0x2

.field private static final NOTIFY_PREPARE_COMPLETED:I = 0x3

.field private static final NOTIFY_QUEUE_CHANGED:I = 0x4

.field private static final THROTTLE:I = 0x64

.field private static final THROTTLE_NOTIFICATION:I = 0x1f4

.field private static final UPDATE_NOTIFICATION_ALL_INFO:I = 0x5

.field private static final UPDATE_NOTIFICATION_PLAYSTATUS:I = 0x6


# instance fields
.field private mIsPendingUpdateNotificationAllInfo:Z

.field private mService:Lcom/samsung/musicplus/service/PlayerService;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/service/PlayerService;)V
    .locals 1
    .param p1, "s"    # Lcom/samsung/musicplus/service/PlayerService;

    .prologue
    .line 4596
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 4594
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;->mIsPendingUpdateNotificationAllInfo:Z

    .line 4597
    iput-object p1, p0, Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;->mService:Lcom/samsung/musicplus/service/PlayerService;

    .line 4598
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v1, 0x0

    .line 4657
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 4687
    :goto_0
    return-void

    .line 4659
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;->mService:Lcom/samsung/musicplus/service/PlayerService;

    const-string v1, "com.android.music.metachanged"

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->notifyChange(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/samsung/musicplus/service/PlayerService;->access$5100(Lcom/samsung/musicplus/service/PlayerService;Ljava/lang/String;)V

    .line 4660
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;->sendMusicInfo()V

    goto :goto_0

    .line 4663
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;->mService:Lcom/samsung/musicplus/service/PlayerService;

    const-string v1, "com.android.music.playstatechanged"

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->notifyChange(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/samsung/musicplus/service/PlayerService;->access$5100(Lcom/samsung/musicplus/service/PlayerService;Ljava/lang/String;)V

    .line 4664
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;->mService:Lcom/samsung/musicplus/service/PlayerService;

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->setAgingTimer()V
    invoke-static {v0}, Lcom/samsung/musicplus/service/PlayerService;->access$6700(Lcom/samsung/musicplus/service/PlayerService;)V

    .line 4665
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;->sendMusicInfo()V

    goto :goto_0

    .line 4668
    :pswitch_2
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;->mService:Lcom/samsung/musicplus/service/PlayerService;

    const-string v1, "com.samsung.musicplus.action.PREPARE_COMPLETED"

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->notifyChange(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/samsung/musicplus/service/PlayerService;->access$5100(Lcom/samsung/musicplus/service/PlayerService;Ljava/lang/String;)V

    goto :goto_0

    .line 4671
    :pswitch_3
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;->mService:Lcom/samsung/musicplus/service/PlayerService;

    const-string v1, "com.samsung.musicplus.action.QUEUE_CHANGED"

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->notifyChange(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/samsung/musicplus/service/PlayerService;->access$5100(Lcom/samsung/musicplus/service/PlayerService;Ljava/lang/String;)V

    goto :goto_0

    .line 4674
    :pswitch_4
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;->mService:Lcom/samsung/musicplus/service/PlayerService;

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->updateNotificationAllInfo()V
    invoke-static {v0}, Lcom/samsung/musicplus/service/PlayerService;->access$4300(Lcom/samsung/musicplus/service/PlayerService;)V

    .line 4675
    iput-boolean v1, p0, Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;->mIsPendingUpdateNotificationAllInfo:Z

    goto :goto_0

    .line 4678
    :pswitch_5
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;->mService:Lcom/samsung/musicplus/service/PlayerService;

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->updateNotificationPlayStatus()V
    invoke-static {v0}, Lcom/samsung/musicplus/service/PlayerService;->access$6800(Lcom/samsung/musicplus/service/PlayerService;)V

    goto :goto_0

    .line 4682
    :pswitch_6
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;->mService:Lcom/samsung/musicplus/service/PlayerService;

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->notifyMusicInfo(Z)V
    invoke-static {v0, v1}, Lcom/samsung/musicplus/service/PlayerService;->access$6900(Lcom/samsung/musicplus/service/PlayerService;Z)V

    goto :goto_0

    .line 4657
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method notifyMetaChanged()V
    .locals 1

    .prologue
    .line 4602
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 4603
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;->sendEmptyMessage(I)Z

    .line 4604
    return-void
.end method

.method notifyPlayStatusChanged()V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 4608
    const/4 v0, 0x7

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;->removeMessages(I)V

    .line 4610
    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;->removeMessages(I)V

    .line 4611
    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;->sendEmptyMessage(I)Z

    .line 4612
    return-void
.end method

.method notifyPrepareCompleted()V
    .locals 1

    .prologue
    const/4 v0, 0x3

    .line 4616
    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;->removeMessages(I)V

    .line 4617
    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;->sendEmptyMessage(I)Z

    .line 4618
    return-void
.end method

.method notifyQueueChange(I)V
    .locals 3
    .param p1, "delay"    # I

    .prologue
    const/4 v2, 0x4

    .line 4651
    invoke-virtual {p0, v2}, Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;->removeMessages(I)V

    .line 4652
    int-to-long v0, p1

    invoke-virtual {p0, v2, v0, v1}, Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 4653
    return-void
.end method

.method removeMusicInfoMsg()V
    .locals 1

    .prologue
    .line 4641
    const/4 v0, 0x7

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;->removeMessages(I)V

    .line 4642
    return-void
.end method

.method sendMusicInfo()V
    .locals 3

    .prologue
    const/4 v2, 0x7

    .line 4636
    invoke-virtual {p0, v2}, Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;->removeMessages(I)V

    .line 4637
    const-wide/16 v0, 0x64

    invoke-virtual {p0, v2, v0, v1}, Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 4638
    return-void
.end method

.method updateNotificationAllInfo()V
    .locals 3

    .prologue
    const/4 v2, 0x5

    .line 4621
    invoke-virtual {p0, v2}, Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;->removeMessages(I)V

    .line 4622
    iget-boolean v0, p0, Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;->mIsPendingUpdateNotificationAllInfo:Z

    if-eqz v0, :cond_0

    .line 4623
    const-wide/16 v0, 0x1f4

    invoke-virtual {p0, v2, v0, v1}, Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;->sendEmptyMessageDelayed(IJ)Z

    .line 4628
    :goto_0
    return-void

    .line 4625
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;->mIsPendingUpdateNotificationAllInfo:Z

    .line 4626
    const-wide/16 v0, 0x64

    invoke-virtual {p0, v2, v0, v1}, Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method

.method updateNotificationPlayStatus()V
    .locals 1

    .prologue
    const/4 v0, 0x6

    .line 4631
    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;->removeMessages(I)V

    .line 4632
    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;->sendEmptyMessage(I)Z

    .line 4633
    return-void
.end method
