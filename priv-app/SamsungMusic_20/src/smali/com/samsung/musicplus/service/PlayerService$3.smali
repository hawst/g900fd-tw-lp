.class Lcom/samsung/musicplus/service/PlayerService$3;
.super Landroid/os/Handler;
.source "PlayerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/service/PlayerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/service/PlayerService;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/service/PlayerService;)V
    .locals 0

    .prologue
    .line 657
    iput-object p1, p0, Lcom/samsung/musicplus/service/PlayerService$3;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 660
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    instance-of v1, v1, Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    .line 661
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    .line 662
    .local v0, "album":Landroid/graphics/Bitmap;
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService$3;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->updateNotificationAlbumArt(Landroid/graphics/Bitmap;)V
    invoke-static {v1, v0}, Lcom/samsung/musicplus/service/PlayerService;->access$1900(Lcom/samsung/musicplus/service/PlayerService;Landroid/graphics/Bitmap;)V

    .line 663
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService$3;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mControlClient:Landroid/media/RemoteControlClient;
    invoke-static {v1}, Lcom/samsung/musicplus/service/PlayerService;->access$2000(Lcom/samsung/musicplus/service/PlayerService;)Landroid/media/RemoteControlClient;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 664
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService$3;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->updateRemoteClient(Landroid/graphics/Bitmap;)V
    invoke-static {v1, v0}, Lcom/samsung/musicplus/service/PlayerService;->access$2100(Lcom/samsung/musicplus/service/PlayerService;Landroid/graphics/Bitmap;)V

    .line 669
    .end local v0    # "album":Landroid/graphics/Bitmap;
    :cond_0
    :goto_0
    return-void

    .line 666
    .restart local v0    # "album":Landroid/graphics/Bitmap;
    :cond_1
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService$3;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->updateMediaSessionMeta(Landroid/graphics/Bitmap;)V
    invoke-static {v1, v0}, Lcom/samsung/musicplus/service/PlayerService;->access$2200(Lcom/samsung/musicplus/service/PlayerService;Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method
