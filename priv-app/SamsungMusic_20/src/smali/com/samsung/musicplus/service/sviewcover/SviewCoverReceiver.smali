.class public Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SviewCoverReceiver.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;
    }
.end annotation


# static fields
.field public static final CLASSNAME:Ljava/lang/String;

.field private static final COVER_TYPE:Ljava/lang/String; = "music"

.field private static sInstance:Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;


# instance fields
.field private mAlbumArtCache:Landroid/graphics/Bitmap;

.field private final mAlbumArtHandler:Landroid/os/Handler;

.field private mRemoteViewBuilder:Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    const-class v0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;->CLASSNAME:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 61
    new-instance v0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$1;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$1;-><init>(Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;)V

    iput-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;->mAlbumArtHandler:Landroid/os/Handler;

    .line 207
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;)Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;->mRemoteViewBuilder:Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;->clearAlbumCache()V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;)Landroid/graphics/Bitmap;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;

    .prologue
    .line 44
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;->mAlbumArtCache:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic access$202(Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;
    .param p1, "x1"    # Landroid/graphics/Bitmap;

    .prologue
    .line 44
    iput-object p1, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;->mAlbumArtCache:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method static synthetic access$400(Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;Landroid/content/Context;Landroid/widget/RemoteViews;Ljava/lang/Boolean;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # Landroid/widget/RemoteViews;
    .param p3, "x3"    # Ljava/lang/Boolean;

    .prologue
    .line 44
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;->sendScoverBroadcast(Landroid/content/Context;Landroid/widget/RemoteViews;Ljava/lang/Boolean;)V

    return-void
.end method

.method private clearAlbumCache()V
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;->mAlbumArtCache:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 107
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;->mAlbumArtCache:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 108
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;->mAlbumArtCache:Landroid/graphics/Bitmap;

    .line 110
    :cond_0
    return-void
.end method

.method public static declared-synchronized getInstance()Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;
    .locals 2

    .prologue
    .line 81
    const-class v1, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;

    monitor-enter v1

    :try_start_0
    sget-boolean v0, Lcom/samsung/musicplus/util/Version;->LIBRARY_API_1:Z

    if-eqz v0, :cond_0

    .line 83
    sget-object v0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;->sInstance:Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;

    if-nez v0, :cond_0

    .line 84
    new-instance v0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;

    invoke-direct {v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;-><init>()V

    sput-object v0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;->sInstance:Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;

    .line 87
    :cond_0
    sget-object v0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;->sInstance:Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 81
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private hideMusicCover(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 121
    sget-object v0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;->CLASSNAME:Ljava/lang/String;

    const-string v1, "hideMusicCover()"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 123
    invoke-direct {p0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;->clearAlbumCache()V

    .line 124
    new-instance v0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;

    const v1, 0x7f04008d

    invoke-direct {v0, p1, v1}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;->mRemoteViewBuilder:Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;

    .line 127
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;->mRemoteViewBuilder:Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;->build()Landroid/widget/RemoteViews;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;->sendScoverBroadcast(Landroid/content/Context;Landroid/widget/RemoteViews;Ljava/lang/Boolean;)V

    .line 128
    return-void
.end method

.method private prepareBuilder(Lcom/samsung/musicplus/service/PlayerService;)V
    .locals 4
    .param p1, "s"    # Lcom/samsung/musicplus/service/PlayerService;

    .prologue
    .line 164
    iget-object v1, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;->mRemoteViewBuilder:Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;

    if-nez v1, :cond_0

    .line 165
    sget-object v1, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;->CLASSNAME:Ljava/lang/String;

    const-string v2, "Failed to prepare builder!"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 167
    invoke-direct {p0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;->clearAlbumCache()V

    .line 176
    :goto_0
    return-void

    .line 169
    :cond_0
    invoke-virtual {p1}, Lcom/samsung/musicplus/service/PlayerService;->getListType()I

    move-result v0

    .line 171
    .local v0, "listType":I
    iget-object v1, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;->mRemoteViewBuilder:Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;

    invoke-virtual {v1}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;->setButtons()Lcom/samsung/musicplus/widget/RemoteViewBuilder;

    .line 172
    iget-object v1, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;->mRemoteViewBuilder:Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;

    invoke-virtual {v1, v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;->setPlayController(I)Lcom/samsung/musicplus/widget/RemoteViewBuilder;

    .line 173
    iget-object v1, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;->mRemoteViewBuilder:Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;

    invoke-virtual {p1}, Lcom/samsung/musicplus/service/PlayerService;->isPlaying()Z

    move-result v2

    invoke-virtual {v1, v0, v2}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;->setPlayStatus(IZ)Lcom/samsung/musicplus/widget/RemoteViewBuilder;

    .line 174
    iget-object v1, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;->mRemoteViewBuilder:Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;

    invoke-virtual {p1}, Lcom/samsung/musicplus/service/PlayerService;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/samsung/musicplus/service/PlayerService;->getArtist()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;->setTitles(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/musicplus/widget/RemoteViewBuilder;

    goto :goto_0
.end method

.method private sendScoverBroadcast(Landroid/content/Context;Landroid/widget/RemoteViews;Ljava/lang/Boolean;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "v"    # Landroid/widget/RemoteViews;
    .param p3, "visibility"    # Ljava/lang/Boolean;

    .prologue
    .line 179
    sget-object v1, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;->CLASSNAME:Ljava/lang/String;

    const-string v2, "sendMusicCoverBroadcast"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 180
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.cover.REMOTEVIEWS_UPDATE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 181
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "visibility"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 182
    const-string v1, "type"

    const-string v2, "music"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 199
    const-string v1, "remote"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 200
    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 201
    return-void
.end method

.method private showMusicCover(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 113
    sget-object v1, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;->CLASSNAME:Ljava/lang/String;

    const-string v2, "showMusicCover()"

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.musicplus.intent.action.CLEAR_COVER_MUSIC_UPDATE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 116
    .local v0, "i":Landroid/content/Intent;
    new-instance v1, Landroid/content/ComponentName;

    const-class v2, Lcom/samsung/musicplus/service/PlayerService;

    invoke-direct {v1, p1, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 117
    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 118
    return-void
.end method

.method private updateAlbumArt(Landroid/content/Context;Lcom/samsung/musicplus/service/PlayerService;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "s"    # Lcom/samsung/musicplus/service/PlayerService;

    .prologue
    .line 153
    invoke-virtual {p2}, Lcom/samsung/musicplus/service/PlayerService;->getAlbumId()J

    move-result-wide v6

    .line 154
    .local v6, "id":J
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c017c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 157
    .local v4, "size":I
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;->mRemoteViewBuilder:Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;->setAlbumArt(Landroid/graphics/Bitmap;)Lcom/samsung/musicplus/widget/RemoteViewBuilder;

    .line 158
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;->mRemoteViewBuilder:Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;

    const/4 v1, 0x1

    # invokes: Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;->showProgress(Z)V
    invoke-static {v0, v1}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;->access$300(Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;Z)V

    .line 159
    invoke-static {}, Lcom/samsung/musicplus/util/AlbumArtLoader;->getAlbumArtLoader()Lcom/samsung/musicplus/util/IAlbumArtLoader;

    move-result-object v0

    invoke-virtual {p2}, Lcom/samsung/musicplus/service/PlayerService;->getListType()I

    move-result v2

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iget-object v5, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;->mAlbumArtHandler:Landroid/os/Handler;

    move-object v1, p1

    invoke-interface/range {v0 .. v5}, Lcom/samsung/musicplus/util/IAlbumArtLoader;->loadArtwork(Landroid/content/Context;ILjava/lang/Object;ILandroid/os/Handler;)V

    .line 161
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 92
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 93
    .local v0, "intentAction":Ljava/lang/String;
    sget-object v1, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;->CLASSNAME:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onReceive() Action : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    const-string v1, "com.samsung.cover.OPEN"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 95
    sget-boolean v1, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_SVIEW_COVER_V2:Z

    if-eqz v1, :cond_0

    .line 96
    const-string v1, "coverOpen"

    const/4 v2, 0x0

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 97
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;->hideMusicCover(Landroid/content/Context;)V

    .line 103
    :cond_0
    :goto_0
    return-void

    .line 99
    :cond_1
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;->showMusicCover(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public updateMusicCover(Landroid/content/Context;Lcom/samsung/musicplus/service/PlayerService;Ljava/lang/String;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "s"    # Lcom/samsung/musicplus/service/PlayerService;
    .param p3, "action"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    .line 131
    sget-object v0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;->CLASSNAME:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "updateMusicCover() : action - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    new-instance v0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;

    const v1, 0x7f04008d

    invoke-direct {v0, p1, v1}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;->mRemoteViewBuilder:Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;

    .line 141
    const-string v0, "com.android.music.metachanged"

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 142
    invoke-direct {p0, p1, p2}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;->updateAlbumArt(Landroid/content/Context;Lcom/samsung/musicplus/service/PlayerService;)V

    .line 143
    invoke-direct {p0, p2}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;->prepareBuilder(Lcom/samsung/musicplus/service/PlayerService;)V

    .line 144
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;->mRemoteViewBuilder:Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;->build()Landroid/widget/RemoteViews;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;->sendScoverBroadcast(Landroid/content/Context;Landroid/widget/RemoteViews;Ljava/lang/Boolean;)V

    .line 150
    :cond_0
    :goto_0
    return-void

    .line 145
    :cond_1
    const-string v0, "com.android.music.playstatechanged"

    invoke-virtual {v0, p3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 146
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;->mRemoteViewBuilder:Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;

    iget-object v1, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;->mAlbumArtCache:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;->setAlbumArt(Landroid/graphics/Bitmap;)Lcom/samsung/musicplus/widget/RemoteViewBuilder;

    .line 147
    invoke-direct {p0, p2}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;->prepareBuilder(Lcom/samsung/musicplus/service/PlayerService;)V

    .line 148
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;->mRemoteViewBuilder:Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver$SviewCoverRemoteViewBuilder;->build()Landroid/widget/RemoteViews;

    move-result-object v0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;->sendScoverBroadcast(Landroid/content/Context;Landroid/widget/RemoteViews;Ljava/lang/Boolean;)V

    goto :goto_0
.end method
