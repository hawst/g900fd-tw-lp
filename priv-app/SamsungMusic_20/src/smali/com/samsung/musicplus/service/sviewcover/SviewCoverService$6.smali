.class Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$6;
.super Ljava/lang/Object;
.source "SviewCoverService.java"

# interfaces
.implements Lcom/samsung/musicplus/service/sviewcover/SviewCoverCommonAction$OnSystemListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;)V
    .locals 0

    .prologue
    .line 312
    iput-object p1, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$6;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDlnaDeviceRemoved()V
    .locals 2

    .prologue
    .line 321
    const-string v0, "MusicSCover"

    const-string v1, "onDlnaDeviceRemoved()"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 322
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$6;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;

    # invokes: Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->handleClose()V
    invoke-static {v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->access$100(Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;)V

    .line 323
    return-void
.end method

.method public onScreenOff()V
    .locals 2

    .prologue
    .line 327
    const-string v0, "MusicSCover"

    const-string v1, "onScreenOff()"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 328
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$6;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;

    # getter for: Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->mPlayerView:Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;
    invoke-static {v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->access$000(Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;)Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;->stopUiUpdate()V

    .line 329
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$6;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;

    # invokes: Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->handleClose()V
    invoke-static {v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->access$100(Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;)V

    .line 330
    return-void
.end method

.method public onSviewCoverOpened()V
    .locals 2

    .prologue
    .line 315
    const-string v0, "MusicSCover"

    const-string v1, "onSviewCoverOpened()"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 316
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$6;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;

    # invokes: Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->handleClose()V
    invoke-static {v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->access$100(Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;)V

    .line 317
    return-void
.end method
