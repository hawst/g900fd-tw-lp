.class Lcom/samsung/musicplus/service/MultiPlayer$5;
.super Ljava/lang/Object;
.source "MultiPlayer.java"

# interfaces
.implements Landroid/media/MediaPlayer$OnPreparedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/service/MultiPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/service/MultiPlayer;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/service/MultiPlayer;)V
    .locals 0

    .prologue
    .line 320
    iput-object p1, p0, Lcom/samsung/musicplus/service/MultiPlayer$5;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 8
    .param p1, "mp"    # Landroid/media/MediaPlayer;

    .prologue
    const-wide/16 v6, 0x0

    .line 323
    iget-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer$5;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    const/4 v2, 0x5

    # setter for: Lcom/samsung/musicplus/service/MultiPlayer;->mCurrentState:I
    invoke-static {v1, v2}, Lcom/samsung/musicplus/service/MultiPlayer;->access$1202(Lcom/samsung/musicplus/service/MultiPlayer;I)I

    .line 324
    const-string v1, "MusicPlayer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "------------- onPrepared --------- audiosession id : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/media/MediaPlayer;->getAudioSessionId()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " mIsSupposedToBePlaying : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/musicplus/service/MultiPlayer$5;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # getter for: Lcom/samsung/musicplus/service/MultiPlayer;->mIsSupposedToBePlaying:Z
    invoke-static {v3}, Lcom/samsung/musicplus/service/MultiPlayer;->access$500(Lcom/samsung/musicplus/service/MultiPlayer;)Z

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " mSeekPosition : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/musicplus/service/MultiPlayer$5;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # getter for: Lcom/samsung/musicplus/service/MultiPlayer;->mSeekPosition:J
    invoke-static {v3}, Lcom/samsung/musicplus/service/MultiPlayer;->access$1300(Lcom/samsung/musicplus/service/MultiPlayer;)J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 328
    sget-boolean v1, Lcom/samsung/musicplus/util/Features;->FLAG_SUPPORT_EXTERNAL_SOUNDALIVE:Z

    if-nez v1, :cond_1

    .line 329
    sget-boolean v1, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_SUPPORT_NEW_SOUNDALIVE:Z

    if-eqz v1, :cond_4

    .line 331
    :try_start_0
    iget-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer$5;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # getter for: Lcom/samsung/musicplus/service/MultiPlayer;->mSoundAliveV2:Lcom/samsung/musicplus/library/audio/SoundAliveV2;
    invoke-static {v1}, Lcom/samsung/musicplus/service/MultiPlayer;->access$1400(Lcom/samsung/musicplus/service/MultiPlayer;)Lcom/samsung/musicplus/library/audio/SoundAliveV2;

    move-result-object v1

    if-nez v1, :cond_0

    .line 332
    const-string v1, "MusicPlayer"

    const-string v2, "onPrepared() : NewSoundAlive create"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 333
    iget-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer$5;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    new-instance v2, Lcom/samsung/musicplus/library/audio/SoundAliveV2;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/samsung/musicplus/service/MultiPlayer$5;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    invoke-virtual {v4}, Lcom/samsung/musicplus/service/MultiPlayer;->getAudioSessionId()I

    move-result v4

    invoke-direct {v2, v3, v4}, Lcom/samsung/musicplus/library/audio/SoundAliveV2;-><init>(II)V

    # setter for: Lcom/samsung/musicplus/service/MultiPlayer;->mSoundAliveV2:Lcom/samsung/musicplus/library/audio/SoundAliveV2;
    invoke-static {v1, v2}, Lcom/samsung/musicplus/service/MultiPlayer;->access$1402(Lcom/samsung/musicplus/service/MultiPlayer;Lcom/samsung/musicplus/library/audio/SoundAliveV2;)Lcom/samsung/musicplus/library/audio/SoundAliveV2;

    .line 334
    iget-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer$5;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # getter for: Lcom/samsung/musicplus/service/MultiPlayer;->mSoundAliveV2:Lcom/samsung/musicplus/library/audio/SoundAliveV2;
    invoke-static {v1}, Lcom/samsung/musicplus/service/MultiPlayer;->access$1400(Lcom/samsung/musicplus/service/MultiPlayer;)Lcom/samsung/musicplus/library/audio/SoundAliveV2;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/samsung/musicplus/library/audio/SoundAliveV2;->setEnabled(Z)V

    .line 336
    :cond_0
    iget-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer$5;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    invoke-virtual {v1}, Lcom/samsung/musicplus/service/MultiPlayer;->setSoundAliveV2()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 347
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer$5;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # getter for: Lcom/samsung/musicplus/service/MultiPlayer;->mSeekPosition:J
    invoke-static {v1}, Lcom/samsung/musicplus/service/MultiPlayer;->access$1300(Lcom/samsung/musicplus/service/MultiPlayer;)J

    move-result-wide v2

    cmp-long v1, v2, v6

    if-lez v1, :cond_2

    .line 348
    iget-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer$5;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    iget-object v2, p0, Lcom/samsung/musicplus/service/MultiPlayer$5;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # getter for: Lcom/samsung/musicplus/service/MultiPlayer;->mSeekPosition:J
    invoke-static {v2}, Lcom/samsung/musicplus/service/MultiPlayer;->access$1300(Lcom/samsung/musicplus/service/MultiPlayer;)J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Lcom/samsung/musicplus/service/MultiPlayer;->seek(J)J

    .line 349
    iget-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer$5;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # setter for: Lcom/samsung/musicplus/service/MultiPlayer;->mSeekPosition:J
    invoke-static {v1, v6, v7}, Lcom/samsung/musicplus/service/MultiPlayer;->access$1302(Lcom/samsung/musicplus/service/MultiPlayer;J)J

    .line 352
    :cond_2
    iget-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer$5;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # getter for: Lcom/samsung/musicplus/service/MultiPlayer;->mMultiPlayerPreparedListener:Lcom/samsung/musicplus/service/MultiPlayer$OnPreparedListener;
    invoke-static {v1}, Lcom/samsung/musicplus/service/MultiPlayer;->access$1600(Lcom/samsung/musicplus/service/MultiPlayer;)Lcom/samsung/musicplus/service/MultiPlayer$OnPreparedListener;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 353
    iget-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer$5;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # getter for: Lcom/samsung/musicplus/service/MultiPlayer;->mMultiPlayerPreparedListener:Lcom/samsung/musicplus/service/MultiPlayer$OnPreparedListener;
    invoke-static {v1}, Lcom/samsung/musicplus/service/MultiPlayer;->access$1600(Lcom/samsung/musicplus/service/MultiPlayer;)Lcom/samsung/musicplus/service/MultiPlayer$OnPreparedListener;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/service/MultiPlayer$5;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # getter for: Lcom/samsung/musicplus/service/MultiPlayer;->mIsSupposedToBePlaying:Z
    invoke-static {v2}, Lcom/samsung/musicplus/service/MultiPlayer;->access$500(Lcom/samsung/musicplus/service/MultiPlayer;)Z

    move-result v2

    invoke-interface {v1, v2}, Lcom/samsung/musicplus/service/MultiPlayer$OnPreparedListener;->onPrepared(Z)V

    .line 355
    :cond_3
    return-void

    .line 337
    :catch_0
    move-exception v0

    .line 340
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "MusicPlayer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "sound alive runtime exceptoin "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 343
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_4
    iget-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer$5;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    iget-object v2, p0, Lcom/samsung/musicplus/service/MultiPlayer$5;->this$0:Lcom/samsung/musicplus/service/MultiPlayer;

    # getter for: Lcom/samsung/musicplus/service/MultiPlayer;->mSoundAlive:I
    invoke-static {v2}, Lcom/samsung/musicplus/service/MultiPlayer;->access$1500(Lcom/samsung/musicplus/service/MultiPlayer;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/samsung/musicplus/service/MultiPlayer;->setSoundAlive(I)V

    goto :goto_0
.end method
