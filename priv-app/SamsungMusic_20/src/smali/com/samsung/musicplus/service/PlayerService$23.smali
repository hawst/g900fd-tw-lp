.class Lcom/samsung/musicplus/service/PlayerService$23;
.super Ljava/lang/Object;
.source "PlayerService.java"

# interfaces
.implements Lcom/samsung/musicplus/library/hardware/SimpleMotionManager$OnMotionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/musicplus/service/PlayerService;->ensureMotionManager()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/service/PlayerService;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/service/PlayerService;)V
    .locals 0

    .prologue
    .line 3579
    iput-object p1, p0, Lcom/samsung/musicplus/service/PlayerService$23;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMotionEvent(I)V
    .locals 2
    .param p1, "event"    # I

    .prologue
    .line 3583
    const/16 v0, 0xa

    if-ne p1, v0, :cond_0

    .line 3584
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$23;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;
    invoke-static {v0}, Lcom/samsung/musicplus/service/PlayerService;->access$3400(Lcom/samsung/musicplus/service/PlayerService;)Lcom/samsung/musicplus/library/audio/SecAudioManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->isAudioPathSpeaker()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3585
    const-string v0, "MusicService"

    const-string v1, "OnMotionListener MREvent.FLIP_TOP_TO_BOTTOM happends"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3587
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$23;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->pause()V

    .line 3588
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$23;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "TURN"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/util/logging/FeatureLogger;->insertLog(Landroid/content/Context;Ljava/lang/String;)V

    .line 3592
    :cond_0
    return-void
.end method
