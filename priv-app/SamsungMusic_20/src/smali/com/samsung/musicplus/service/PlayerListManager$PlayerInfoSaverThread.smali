.class Lcom/samsung/musicplus/service/PlayerListManager$PlayerInfoSaverThread;
.super Ljava/lang/Thread;
.source "PlayerListManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/service/PlayerListManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "PlayerInfoSaverThread"
.end annotation


# instance fields
.field private mAudioId:J

.field private mContext:Landroid/content/Context;

.field private mUri:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/net/Uri;J)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "uri"    # Landroid/net/Uri;
    .param p3, "id"    # J

    .prologue
    .line 996
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 997
    iput-object p1, p0, Lcom/samsung/musicplus/service/PlayerListManager$PlayerInfoSaverThread;->mContext:Landroid/content/Context;

    .line 998
    iput-object p2, p0, Lcom/samsung/musicplus/service/PlayerListManager$PlayerInfoSaverThread;->mUri:Landroid/net/Uri;

    .line 999
    iput-wide p3, p0, Lcom/samsung/musicplus/service/PlayerListManager$PlayerInfoSaverThread;->mAudioId:J

    .line 1000
    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 1004
    const/4 v6, 0x0

    .line 1006
    .local v6, "c":Landroid/database/Cursor;
    :try_start_0
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerListManager$PlayerInfoSaverThread;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 1007
    .local v0, "cr":Landroid/content/ContentResolver;
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerListManager$PlayerInfoSaverThread;->mUri:Landroid/net/Uri;

    iget-wide v2, p0, Lcom/samsung/musicplus/service/PlayerListManager$PlayerInfoSaverThread;->mAudioId:J

    invoke-static {v1, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "most_played"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 1011
    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1012
    new-instance v7, Landroid/content/ContentValues;

    const/4 v1, 0x2

    invoke-direct {v7, v1}, Landroid/content/ContentValues;-><init>(I)V

    .line 1013
    .local v7, "values":Landroid/content/ContentValues;
    const-string v1, "most_played"

    const/4 v2, 0x0

    invoke-interface {v6, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v7, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1014
    const-string v1, "recently_played"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v7, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1020
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerListManager$PlayerInfoSaverThread;->mUri:Landroid/net/Uri;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "_id = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lcom/samsung/musicplus/service/PlayerListManager$PlayerInfoSaverThread;->mAudioId:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v7, v2, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1032
    .end local v7    # "values":Landroid/content/ContentValues;
    :cond_0
    if-eqz v6, :cond_1

    .line 1033
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 1037
    :cond_1
    return-void

    .line 1032
    .end local v0    # "cr":Landroid/content/ContentResolver;
    :catchall_0
    move-exception v1

    if-eqz v6, :cond_2

    .line 1033
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_2
    throw v1
.end method
