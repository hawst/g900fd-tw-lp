.class Lcom/samsung/musicplus/service/PlayerService$ToastHandler;
.super Landroid/os/Handler;
.source "PlayerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/service/PlayerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ToastHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/service/PlayerService;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/service/PlayerService;)V
    .locals 0

    .prologue
    .line 5326
    iput-object p1, p0, Lcom/samsung/musicplus/service/PlayerService$ToastHandler;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 5329
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$ToastHandler;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget v1, p1, Landroid/os/Message;->what:I

    iget v2, p1, Landroid/os/Message;->arg1:I

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 5330
    return-void
.end method

.method public showToast(I)V
    .locals 1
    .param p1, "resId"    # I

    .prologue
    .line 5338
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/samsung/musicplus/service/PlayerService$ToastHandler;->showToast(II)V

    .line 5339
    return-void
.end method

.method public showToast(II)V
    .locals 1
    .param p1, "resId"    # I
    .param p2, "duration"    # I

    .prologue
    .line 5341
    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/service/PlayerService$ToastHandler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 5342
    .local v0, "m":Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->what:I

    .line 5343
    iput p2, v0, Landroid/os/Message;->arg1:I

    .line 5344
    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/service/PlayerService$ToastHandler;->sendMessage(Landroid/os/Message;)Z

    .line 5345
    return-void
.end method
