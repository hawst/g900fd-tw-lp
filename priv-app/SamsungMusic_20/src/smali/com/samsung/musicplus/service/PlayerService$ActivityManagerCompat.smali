.class Lcom/samsung/musicplus/service/PlayerService$ActivityManagerCompat;
.super Ljava/lang/Object;
.source "PlayerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/service/PlayerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ActivityManagerCompat"
.end annotation


# static fields
.field private static final sForceStopMethod:Ljava/lang/reflect/Method;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 5502
    invoke-static {}, Lcom/samsung/musicplus/service/PlayerService$ActivityManagerCompat;->findForceStopMethod()Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/samsung/musicplus/service/PlayerService$ActivityManagerCompat;->sForceStopMethod:Ljava/lang/reflect/Method;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 5500
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static findForceStopMethod()Ljava/lang/reflect/Method;
    .locals 6

    .prologue
    .line 5506
    :try_start_0
    const-class v0, Landroid/app/ActivityManager;

    .line 5507
    .local v0, "cls":Ljava/lang/Class;, "Ljava/lang/Class<Landroid/app/ActivityManager;>;"
    const-string v2, "forceStopPackage"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {v0, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 5511
    :goto_0
    return-object v2

    .line 5508
    :catch_0
    move-exception v1

    .line 5509
    .local v1, "e":Ljava/lang/NoSuchMethodException;
    invoke-virtual {v1}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    .line 5511
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static forceStopPackage(Landroid/app/ActivityManager;Ljava/lang/String;)V
    .locals 4
    .param p0, "am"    # Landroid/app/ActivityManager;
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 5530
    sget-object v1, Lcom/samsung/musicplus/service/PlayerService$ActivityManagerCompat;->sForceStopMethod:Ljava/lang/reflect/Method;

    if-eqz v1, :cond_0

    .line 5532
    :try_start_0
    sget-object v1, Lcom/samsung/musicplus/service/PlayerService$ActivityManagerCompat;->sForceStopMethod:Ljava/lang/reflect/Method;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v1, p0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    .line 5543
    :goto_0
    return-void

    .line 5534
    :catch_0
    move-exception v0

    .line 5535
    .local v0, "e":Ljava/lang/reflect/InvocationTargetException;
    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    .line 5542
    .end local v0    # "e":Ljava/lang/reflect/InvocationTargetException;
    :cond_0
    :goto_1
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v1

    invoke-static {v1}, Landroid/os/Process;->killProcess(I)V

    goto :goto_0

    .line 5536
    :catch_1
    move-exception v0

    .line 5537
    .local v0, "e":Ljava/lang/IllegalAccessException;
    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_1
.end method
