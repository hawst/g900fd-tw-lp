.class Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;
.super Lcom/samsung/musicplus/player/PlayerView;
.source "SviewCoverService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SviewCoverPlayer"
.end annotation


# instance fields
.field private mNowPlayingList:Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;

.field final synthetic this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;


# direct methods
.method public constructor <init>(Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;Landroid/content/Context;Landroid/view/View;)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "root"    # Landroid/view/View;

    .prologue
    .line 356
    iput-object p1, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;

    .line 357
    invoke-direct {p0, p2, p3}, Lcom/samsung/musicplus/player/PlayerView;-><init>(Landroid/content/Context;Landroid/view/View;)V

    .line 358
    return-void
.end method

.method static synthetic access$300(Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;

    .prologue
    .line 352
    invoke-direct {p0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;->updateBuffering()V

    return-void
.end method

.method private updateBuffering()V
    .locals 4

    .prologue
    .line 402
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->isPreparing()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getDuration()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    .line 403
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;

    # getter for: Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->mBuffering:Landroid/widget/ProgressBar;
    invoke-static {v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->access$400(Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;)Landroid/widget/ProgressBar;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 407
    :goto_0
    return-void

    .line 405
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;

    # getter for: Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->mBuffering:Landroid/widget/ProgressBar;
    invoke-static {v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->access$400(Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;)Landroid/widget/ProgressBar;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public getAirView(Landroid/view/View;)Landroid/view/View;
    .locals 5
    .param p1, "parent"    # Landroid/view/View;

    .prologue
    const v4, 0x7f0c0168

    .line 469
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 491
    invoke-super {p0, p1}, Lcom/samsung/musicplus/player/PlayerView;->getAirView(Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    .line 493
    :goto_0
    return-object v0

    .line 471
    :sswitch_0
    iget-object v1, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;->mContext:Landroid/content/Context;

    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getPrevUri()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/musicplus/util/UiUtils;->getTitle(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/samsung/musicplus/util/UiUtils;->getTheme()I

    move-result v3

    invoke-static {v1, v2, v3}, Lcom/samsung/musicplus/util/UiUtils;->getAirTextView(Landroid/content/Context;Ljava/lang/String;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 474
    .local v0, "popup":Landroid/widget/TextView;
    iget-object v1, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxWidth(I)V

    goto :goto_0

    .line 478
    .end local v0    # "popup":Landroid/widget/TextView;
    :sswitch_1
    iget-object v1, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;->mContext:Landroid/content/Context;

    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getNextUri()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/musicplus/util/UiUtils;->getTitle(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/samsung/musicplus/util/UiUtils;->getTheme()I

    move-result v3

    invoke-static {v1, v2, v3}, Lcom/samsung/musicplus/util/UiUtils;->getAirTextView(Landroid/content/Context;Ljava/lang/String;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 481
    .restart local v0    # "popup":Landroid/widget/TextView;
    iget-object v1, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxWidth(I)V

    goto :goto_0

    .line 485
    .end local v0    # "popup":Landroid/widget/TextView;
    :sswitch_2
    iget-object v1, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;->mContext:Landroid/content/Context;

    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getAlbum()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/samsung/musicplus/util/UiUtils;->getTheme()I

    move-result v3

    invoke-static {v1, v2, v3}, Lcom/samsung/musicplus/util/UiUtils;->getAirTextView(Landroid/content/Context;Ljava/lang/String;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 487
    .restart local v0    # "popup":Landroid/widget/TextView;
    iget-object v1, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0167

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMaxWidth(I)V

    goto :goto_0

    .line 469
    :sswitch_data_0
    .sparse-switch
        0x7f0d00f9 -> :sswitch_0
        0x7f0d00fc -> :sswitch_1
        0x7f0d0118 -> :sswitch_2
    .end sparse-switch
.end method

.method public invalidateViews()V
    .locals 1

    .prologue
    .line 507
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;->getAlbumViewVisibility()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 514
    :cond_0
    :goto_0
    return-void

    .line 511
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;->mNowPlayingList:Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;

    if-eqz v0, :cond_0

    .line 512
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;->mNowPlayingList:Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;->invalidateViews()V

    goto :goto_0
.end method

.method protected onCreateView(Landroid/view/View;)V
    .locals 2
    .param p1, "root"    # Landroid/view/View;

    .prologue
    .line 366
    const v0, 0x7f0200e1

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;->setPlayButtonImage(I)V

    .line 367
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c017c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;->setThumbNailSize(I)V

    .line 369
    invoke-super {p0, p1}, Lcom/samsung/musicplus/player/PlayerView;->onCreateView(Landroid/view/View;)V

    .line 370
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 517
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;->mNowPlayingList:Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;

    if-eqz v0, :cond_0

    .line 518
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;->mNowPlayingList:Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;->onDestroy()V

    .line 520
    :cond_0
    return-void
.end method

.method protected setHoverFocusImage(Landroid/view/View;I)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;
    .param p2, "event"    # I

    .prologue
    .line 450
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 463
    .end local p1    # "v":Landroid/view/View;
    :goto_0
    return-void

    .line 452
    .restart local p1    # "v":Landroid/view/View;
    :sswitch_0
    check-cast p1, Landroid/widget/ImageView;

    .end local p1    # "v":Landroid/view/View;
    const v0, 0x7f0200eb

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 455
    .restart local p1    # "v":Landroid/view/View;
    :sswitch_1
    check-cast p1, Landroid/widget/ImageView;

    .end local p1    # "v":Landroid/view/View;
    const v0, 0x7f0200d6

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 458
    .restart local p1    # "v":Landroid/view/View;
    :sswitch_2
    check-cast p1, Landroid/widget/ImageView;

    .end local p1    # "v":Landroid/view/View;
    const v0, 0x7f0200e7

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    .line 450
    nop

    :sswitch_data_0
    .sparse-switch
        0x7f0d00f9 -> :sswitch_0
        0x7f0d00fc -> :sswitch_1
        0x7f0d0116 -> :sswitch_2
    .end sparse-switch
.end method

.method protected setNextPrevListener(Landroid/view/View;Landroid/view/View;)V
    .locals 1
    .param p1, "prev"    # Landroid/view/View;
    .param p2, "next"    # Landroid/view/View;

    .prologue
    .line 374
    new-instance v0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer$1;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer$1;-><init>(Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 383
    new-instance v0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer$2;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer$2;-><init>(Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 392
    invoke-super {p0, p1, p2}, Lcom/samsung/musicplus/player/PlayerView;->setNextPrevListener(Landroid/view/View;Landroid/view/View;)V

    .line 393
    return-void
.end method

.method public stopUiUpdate()V
    .locals 1

    .prologue
    .line 500
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;->setTitleScroll(Z)V

    .line 501
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;->mNowPlayingList:Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;

    if-eqz v0, :cond_0

    .line 502
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;->mNowPlayingList:Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;->stopAnimation()V

    .line 504
    :cond_0
    return-void
.end method

.method protected togglePlayerView()V
    .locals 4

    .prologue
    .line 425
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;->togglePlayerViewWithoutAnimation()V

    .line 427
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;->mNowPlayingList:Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;

    if-nez v0, :cond_0

    .line 428
    new-instance v0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;

    iget-object v1, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;

    invoke-virtual {v1}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f04008f

    invoke-virtual {p0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;->getListContainer()Landroid/view/View;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;-><init>(Landroid/content/Context;ILandroid/view/View;)V

    iput-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;->mNowPlayingList:Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;

    .line 432
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;->getAlbumViewVisibility()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 433
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;->mNowPlayingList:Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;->hide()V

    .line 439
    :goto_0
    return-void

    .line 437
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;->mNowPlayingList:Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;->show()V

    .line 438
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;->mNowPlayingList:Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverNowPlayingList;->setCurrentPosition()V

    goto :goto_0
.end method

.method protected updateHoverView()V
    .locals 2

    .prologue
    .line 443
    invoke-super {p0}, Lcom/samsung/musicplus/player/PlayerView;->updateHoverView()V

    .line 444
    iget-object v0, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;

    # getter for: Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->mCloseButton:Landroid/view/View;
    invoke-static {v0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->access$500(Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x5153

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/hardware/AirView;->setView(Landroid/view/View;I)V

    .line 446
    return-void
.end method

.method public updatePlayState(Z)V
    .locals 3
    .param p1, "isPlaying"    # Z

    .prologue
    .line 411
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;->getPlayButtonView()Landroid/widget/ImageView;

    move-result-object v1

    .line 412
    .local v1, "v":Landroid/widget/ImageView;
    iget-object v2, p0, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;->this$0:Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;

    invoke-virtual {v2}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 414
    .local v0, "context":Landroid/content/Context;
    if-eqz p1, :cond_0

    .line 415
    const v2, 0x7f0200dd

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 416
    const v2, 0x7f100193

    invoke-virtual {v0, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 421
    :goto_0
    return-void

    .line 418
    :cond_0
    const v2, 0x7f0200e1

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 419
    const v2, 0x7f100194

    invoke-virtual {v0, v2}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method protected updateTrackAllInfo()V
    .locals 0

    .prologue
    .line 397
    invoke-direct {p0}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverService$SviewCoverPlayer;->updateBuffering()V

    .line 398
    invoke-super {p0}, Lcom/samsung/musicplus/player/PlayerView;->updateTrackAllInfo()V

    .line 399
    return-void
.end method
