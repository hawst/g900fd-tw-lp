.class Lcom/samsung/musicplus/service/PlayerService$18;
.super Landroid/content/BroadcastReceiver;
.source "PlayerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/service/PlayerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/service/PlayerService;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/service/PlayerService;)V
    .locals 0

    .prologue
    .line 2234
    iput-object p1, p0, Lcom/samsung/musicplus/service/PlayerService$18;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2237
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 2238
    .local v0, "action":Ljava/lang/String;
    const-string v3, "MusicService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mWifiReceiver "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2239
    const-string v3, "android.hardware.display.action.WIFI_DISPLAY_STATUS_CHANGED"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2240
    invoke-static {p2}, Lcom/samsung/musicplus/library/wifi/WifiDisplayStatusCompat;->getExtraWifiDisplayStatusCompat(Landroid/content/Intent;)Lcom/samsung/musicplus/library/wifi/WifiDisplayStatusCompat;

    move-result-object v2

    .line 2242
    .local v2, "status":Lcom/samsung/musicplus/library/wifi/WifiDisplayStatusCompat;
    if-nez v2, :cond_2

    .line 2243
    iget-object v3, p0, Lcom/samsung/musicplus/service/PlayerService$18;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mIsConnectingWfd:Z
    invoke-static {v3}, Lcom/samsung/musicplus/service/PlayerService;->access$2900(Lcom/samsung/musicplus/service/PlayerService;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2244
    iget-object v3, p0, Lcom/samsung/musicplus/service/PlayerService$18;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # setter for: Lcom/samsung/musicplus/service/PlayerService;->mIsConnectingWfd:Z
    invoke-static {v3, v6}, Lcom/samsung/musicplus/service/PlayerService;->access$2902(Lcom/samsung/musicplus/service/PlayerService;Z)Z

    .line 2245
    iget-object v3, p0, Lcom/samsung/musicplus/service/PlayerService$18;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->sendPlayerBuffering(Z)V
    invoke-static {v3, v6}, Lcom/samsung/musicplus/service/PlayerService;->access$800(Lcom/samsung/musicplus/service/PlayerService;Z)V

    .line 2273
    :cond_0
    :goto_0
    iget-object v3, p0, Lcom/samsung/musicplus/service/PlayerService$18;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mIsShowNotification:Z
    invoke-static {v3}, Lcom/samsung/musicplus/service/PlayerService;->access$2400(Lcom/samsung/musicplus/service/PlayerService;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2274
    iget-object v3, p0, Lcom/samsung/musicplus/service/PlayerService$18;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->updateNotificationConnectivityStatus()V
    invoke-static {v3}, Lcom/samsung/musicplus/service/PlayerService;->access$1100(Lcom/samsung/musicplus/service/PlayerService;)V

    .line 2277
    .end local v2    # "status":Lcom/samsung/musicplus/library/wifi/WifiDisplayStatusCompat;
    :cond_1
    return-void

    .line 2248
    .restart local v2    # "status":Lcom/samsung/musicplus/library/wifi/WifiDisplayStatusCompat;
    :cond_2
    invoke-virtual {v2}, Lcom/samsung/musicplus/library/wifi/WifiDisplayStatusCompat;->getActiveDisplayState()I

    move-result v1

    .line 2249
    .local v1, "state":I
    const-string v3, "MusicService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "mWifiReceiver state "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2250
    packed-switch v1, :pswitch_data_0

    .line 2266
    :cond_3
    :goto_1
    iget-object v3, p0, Lcom/samsung/musicplus/service/PlayerService$18;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # getter for: Lcom/samsung/musicplus/service/PlayerService;->mIsConnectingWfd:Z
    invoke-static {v3}, Lcom/samsung/musicplus/service/PlayerService;->access$2900(Lcom/samsung/musicplus/service/PlayerService;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2267
    iget-object v3, p0, Lcom/samsung/musicplus/service/PlayerService$18;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # setter for: Lcom/samsung/musicplus/service/PlayerService;->mIsConnectingWfd:Z
    invoke-static {v3, v6}, Lcom/samsung/musicplus/service/PlayerService;->access$2902(Lcom/samsung/musicplus/service/PlayerService;Z)Z

    .line 2268
    iget-object v3, p0, Lcom/samsung/musicplus/service/PlayerService$18;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->sendPlayerBuffering(Z)V
    invoke-static {v3, v6}, Lcom/samsung/musicplus/service/PlayerService;->access$800(Lcom/samsung/musicplus/service/PlayerService;Z)V

    goto :goto_0

    .line 2252
    :pswitch_0
    iget-object v3, p0, Lcom/samsung/musicplus/service/PlayerService$18;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # setter for: Lcom/samsung/musicplus/service/PlayerService;->mIsConnectingWfd:Z
    invoke-static {v3, v7}, Lcom/samsung/musicplus/service/PlayerService;->access$2902(Lcom/samsung/musicplus/service/PlayerService;Z)Z

    .line 2253
    iget-object v3, p0, Lcom/samsung/musicplus/service/PlayerService$18;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->sendPlayerBuffering(Z)V
    invoke-static {v3, v7}, Lcom/samsung/musicplus/service/PlayerService;->access$800(Lcom/samsung/musicplus/service/PlayerService;Z)V

    goto :goto_0

    .line 2256
    :pswitch_1
    iget-object v3, p0, Lcom/samsung/musicplus/service/PlayerService$18;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    # invokes: Lcom/samsung/musicplus/service/PlayerService;->isDmrPlaying()Z
    invoke-static {v3}, Lcom/samsung/musicplus/service/PlayerService;->access$1400(Lcom/samsung/musicplus/service/PlayerService;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2257
    iget-object v3, p0, Lcom/samsung/musicplus/service/PlayerService$18;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v3}, Lcom/samsung/musicplus/service/PlayerService;->changeToWfdDevice()V

    goto :goto_1

    .line 2250
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
