.class Lcom/samsung/musicplus/service/PlayerService$24;
.super Ljava/lang/Object;
.source "PlayerService.java"

# interfaces
.implements Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager$OnBounceListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/service/PlayerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/service/PlayerService;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/service/PlayerService;)V
    .locals 0

    .prologue
    .line 3616
    iput-object p1, p0, Lcom/samsung/musicplus/service/PlayerService$24;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onBounce(I)V
    .locals 3
    .param p1, "event"    # I

    .prologue
    .line 3620
    const-string v0, "MusicService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "bounce : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 3622
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$24;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$24;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/musicplus/util/UiUtils;->isScreenOn(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3623
    :cond_0
    const-string v0, "MusicService"

    const-string v1, "Bound motion effects only in screen off and  playing status!"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 3636
    :goto_0
    return-void

    .line 3626
    :cond_1
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 3631
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$24;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/service/PlayerService;->next(Z)V

    goto :goto_0

    .line 3628
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService$24;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/service/PlayerService;->prev(Z)V

    goto :goto_0

    .line 3626
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
