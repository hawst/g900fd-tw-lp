.class Lcom/samsung/musicplus/service/MultiPlayer;
.super Ljava/lang/Object;
.source "MultiPlayer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/service/MultiPlayer$DataInfo;,
        Lcom/samsung/musicplus/service/MultiPlayer$PlayerState;,
        Lcom/samsung/musicplus/service/MultiPlayer$OnPreparedListener;
    }
.end annotation


# static fields
.field static final BY_USER:Ljava/lang/String; = "byUser"

.field private static final DEBUG_DMR_PLAYER:Z = true

.field private static final DMR_ERROR_NORMAL:I = -0x2

.field static final DRM_REQUEST:I = 0x4

.field static final ERROR_IO:I = -0x3ec

.field static final INTERNAL_PLAYER_CHANGED:I = 0x7

.field static final INTERNAL_PLAYER_STATE_CHANGED:I = 0x8

.field public static final KEY_PARAMETER_MUTE_CONTROL_K2HD:I = 0x761

.field public static final KEY_PARAMETER_VIRTUAL_ULTRA_HIGH_QUALITY:I = 0x760

.field static final LAST_MSG:I = 0x9

.field static final MEDIA_ERROR:I = 0x3

.field static final MEDIA_ERROR_CONNECTION_LOST:I = -0x3ed

.field private static final MEDIA_ERROR_NORMAL:I = -0x1

.field private static final MEDIA_ERROR_NO_INIT:I = -0x13

.field static final MODE_DMR_PLAYER:I = 0x2

.field static final MODE_MEDIA_PLAYER:I = 0x1

.field static final PLAYER_BUFFERING_START:I = 0x5

.field static final PLAYER_BUFFERING_STOP:I = 0x6

.field private static final PREPARE_CURRENT:I = 0x0

.field private static final PREPARE_NEXT:I = 0x1

.field private static final PRE_FETCHING:Z = true

.field static final SEEK_COMPLETED:I = 0x9

.field static final SERVER_DIED:I = 0x2

.field private static final TAG:Ljava/lang/String; = "MusicPlayer"

.field static final TRACK_ENDED:I = 0x1

.field private static final UNKNOWN_EXTRA_ERROR:I = -0x1


# instance fields
.field private mAudioSession:I

.field private mAuto:Z

.field private mBandLevel:[I

.field private mBufferPercent:I

.field private final mBufferingUpdateListener:Landroid/media/MediaPlayer$OnBufferingUpdateListener;

.field private final mCompleteListener:Landroid/media/MediaPlayer$OnCompletionListener;

.field private mContext:Landroid/content/Context;

.field private mCurLeftLimitVolume:F

.field private mCurRightLimitVolume:F

.field private mCurrentSetVolume:F

.field private mCurrentState:I

.field private mDmrId:Ljava/lang/String;

.field private mDmrPlayer:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

.field private mDuration:J

.field private final mErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

.field private mFlagK2HD:Z

.field private mHandler:Landroid/os/Handler;

.field private mInfo:Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;

.field private final mInfoListener:Landroid/media/MediaPlayer$OnInfoListener;

.field private mIsBuffering:Z

.field private mIsDmrErrorState:Z

.field private mIsDrm:Z

.field private mIsOpenSession:Z

.field private mIsPauseRequest:Z

.field private mIsPlayRequest:Z

.field private mIsPrepareRequest:Z

.field private mIsSeekRequest:Z

.field private mIsSupposedToBePlaying:Z

.field private mLastSoundAliveGenre:I

.field private mLastSoundAliveSessionId:I

.field private mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

.field private mMediaPlayer:Landroid/media/MediaPlayer;

.field private mMode:I

.field private mMultiPlayerPreparedListener:Lcom/samsung/musicplus/service/MultiPlayer$OnPreparedListener;

.field private mNextMedia:Lcom/samsung/musicplus/widget/NextMediaPlayer;

.field private mOnAvPlayerErrorListener:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$OnErrorListener;

.field private mOnCompletionListener:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$OnCompletionListener;

.field private mOnStateChangedListener:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$OnStateChangedListener;

.field private mPath:Ljava/lang/String;

.field private mPauseByK2HD:Z

.field private mPosition:J

.field private final mPrepareHandler:Landroid/os/Handler;

.field private mPrepareRadio:Z

.field private final mPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

.field private mPresetEffect:I

.field private mRestorePlaySpeed:Z

.field private mResumePosition:J

.field private mSA:Lcom/samsung/musicplus/account/SamsungAccountHelper;

.field private final mSeekCompleteListener:Landroid/media/MediaPlayer$OnSeekCompleteListener;

.field private mSeekPosition:J

.field private mSkipDrmPath:Ljava/lang/String;

.field private mSoundAlive:I

.field private mSoundAliveV2:Lcom/samsung/musicplus/library/audio/SoundAliveV2;

.field private mSpeed:F

.field private mSquarePosition:[I

.field private mStrengthEffect:[I

.field private mUri:Landroid/net/Uri;

.field private mUserEffect:[I

.field private mUserEq:[I

.field private mWakeLock:Landroid/os/PowerManager$WakeLock;

.field private mWasPlaying:Z


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v8, -0x1

    const/4 v7, 0x0

    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 418
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 144
    iput v5, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mCurrentState:I

    .line 207
    iput v4, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mBufferPercent:I

    .line 209
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mSeekPosition:J

    .line 213
    iput-boolean v4, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mIsSupposedToBePlaying:Z

    .line 215
    iput-boolean v4, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mIsDmrErrorState:Z

    .line 219
    iput-boolean v4, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mIsDrm:Z

    .line 225
    iput-boolean v4, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mPrepareRadio:Z

    .line 227
    iput-boolean v4, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mIsOpenSession:Z

    .line 229
    iput v6, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mCurrentSetVolume:F

    .line 231
    iput v6, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mCurLeftLimitVolume:F

    .line 233
    iput v6, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mCurRightLimitVolume:F

    .line 235
    iput-boolean v4, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mFlagK2HD:Z

    .line 236
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mResumePosition:J

    .line 237
    iput-boolean v4, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mPauseByK2HD:Z

    .line 247
    new-instance v1, Lcom/samsung/musicplus/service/MultiPlayer$1;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/service/MultiPlayer$1;-><init>(Lcom/samsung/musicplus/service/MultiPlayer;)V

    iput-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mPrepareHandler:Landroid/os/Handler;

    .line 286
    new-instance v1, Lcom/samsung/musicplus/service/MultiPlayer$2;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/service/MultiPlayer$2;-><init>(Lcom/samsung/musicplus/service/MultiPlayer;)V

    iput-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mBufferingUpdateListener:Landroid/media/MediaPlayer$OnBufferingUpdateListener;

    .line 294
    new-instance v1, Lcom/samsung/musicplus/service/MultiPlayer$3;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/service/MultiPlayer$3;-><init>(Lcom/samsung/musicplus/service/MultiPlayer;)V

    iput-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mInfoListener:Landroid/media/MediaPlayer$OnInfoListener;

    .line 312
    new-instance v1, Lcom/samsung/musicplus/service/MultiPlayer$4;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/service/MultiPlayer$4;-><init>(Lcom/samsung/musicplus/service/MultiPlayer;)V

    iput-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mSeekCompleteListener:Landroid/media/MediaPlayer$OnSeekCompleteListener;

    .line 320
    new-instance v1, Lcom/samsung/musicplus/service/MultiPlayer$5;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/service/MultiPlayer$5;-><init>(Lcom/samsung/musicplus/service/MultiPlayer;)V

    iput-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    .line 358
    new-instance v1, Lcom/samsung/musicplus/service/MultiPlayer$6;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/service/MultiPlayer$6;-><init>(Lcom/samsung/musicplus/service/MultiPlayer;)V

    iput-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mCompleteListener:Landroid/media/MediaPlayer$OnCompletionListener;

    .line 371
    iput-boolean v4, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mRestorePlaySpeed:Z

    .line 387
    new-instance v1, Lcom/samsung/musicplus/service/MultiPlayer$7;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/service/MultiPlayer$7;-><init>(Lcom/samsung/musicplus/service/MultiPlayer;)V

    iput-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    .line 833
    iput v8, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mLastSoundAliveGenre:I

    .line 834
    iput v8, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mLastSoundAliveSessionId:I

    .line 1335
    iput v4, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mSoundAlive:I

    .line 1341
    iput v6, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mSpeed:F

    .line 1434
    iput v5, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMode:I

    .line 1646
    iput-boolean v4, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mIsPrepareRequest:Z

    .line 1648
    iput-boolean v4, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mIsSeekRequest:Z

    .line 1650
    iput-boolean v4, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mIsPlayRequest:Z

    .line 1652
    iput-boolean v4, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mIsPauseRequest:Z

    .line 1654
    iput-boolean v4, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mIsBuffering:Z

    .line 1682
    new-instance v1, Lcom/samsung/musicplus/service/MultiPlayer$14;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/service/MultiPlayer$14;-><init>(Lcom/samsung/musicplus/service/MultiPlayer;)V

    iput-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mOnStateChangedListener:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$OnStateChangedListener;

    .line 1788
    new-instance v1, Lcom/samsung/musicplus/service/MultiPlayer$15;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/service/MultiPlayer$15;-><init>(Lcom/samsung/musicplus/service/MultiPlayer;)V

    iput-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mOnAvPlayerErrorListener:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$OnErrorListener;

    .line 1821
    new-instance v1, Lcom/samsung/musicplus/service/MultiPlayer$16;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/service/MultiPlayer$16;-><init>(Lcom/samsung/musicplus/service/MultiPlayer;)V

    iput-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mOnCompletionListener:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$OnCompletionListener;

    .line 2025
    iput-object v7, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mSquarePosition:[I

    .line 2027
    iput-object v7, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mBandLevel:[I

    .line 2029
    iput-object v7, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mStrengthEffect:[I

    .line 2110
    iput-boolean v4, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mAuto:Z

    .line 419
    new-instance v1, Landroid/media/MediaPlayer;

    invoke-direct {v1}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 420
    iget-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1, p1, v5}, Landroid/media/MediaPlayer;->setWakeMode(Landroid/content/Context;I)V

    .line 421
    iget-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->getAudioSessionId()I

    move-result v1

    iput v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mAudioSession:I

    .line 422
    iput-object p1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mContext:Landroid/content/Context;

    .line 423
    const-string v1, "power"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 424
    .local v0, "pm":Landroid/os/PowerManager;
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v5, v1}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 425
    iget-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1, v4}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    .line 426
    iput v5, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mCurrentState:I

    .line 431
    invoke-direct {p0, v5, v4}, Lcom/samsung/musicplus/service/MultiPlayer;->notifyOpenSessionForSoundAlive(ZZ)V

    .line 433
    new-instance v1, Lcom/samsung/musicplus/account/SamsungAccountHelper;

    invoke-direct {v1, p1}, Lcom/samsung/musicplus/account/SamsungAccountHelper;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mSA:Lcom/samsung/musicplus/account/SamsungAccountHelper;

    .line 434
    iget-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mSA:Lcom/samsung/musicplus/account/SamsungAccountHelper;

    new-instance v2, Lcom/samsung/musicplus/service/MultiPlayer$8;

    invoke-direct {v2, p0}, Lcom/samsung/musicplus/service/MultiPlayer$8;-><init>(Lcom/samsung/musicplus/service/MultiPlayer;)V

    invoke-virtual {v1, v2}, Lcom/samsung/musicplus/account/SamsungAccountHelper;->setOnAccountInfoListener(Lcom/samsung/musicplus/account/SamsungAccountHelper$OnAccountInfoListener;)V

    .line 473
    iget-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mSA:Lcom/samsung/musicplus/account/SamsungAccountHelper;

    invoke-virtual {v1}, Lcom/samsung/musicplus/account/SamsungAccountHelper;->requestInfo()Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;

    .line 474
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/service/MultiPlayer;)Lcom/samsung/musicplus/service/PlayerListManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/MultiPlayer;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    return-object v0
.end method

.method static synthetic access$100(Lcom/samsung/musicplus/service/MultiPlayer;)Lcom/samsung/musicplus/widget/NextMediaPlayer;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/MultiPlayer;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mNextMedia:Lcom/samsung/musicplus/widget/NextMediaPlayer;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/samsung/musicplus/service/MultiPlayer;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/MultiPlayer;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/samsung/musicplus/service/MultiPlayer;->stopBuffering()V

    return-void
.end method

.method static synthetic access$1100(Lcom/samsung/musicplus/service/MultiPlayer;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/MultiPlayer;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/samsung/musicplus/service/MultiPlayer;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/MultiPlayer;

    .prologue
    .line 56
    iget v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mCurrentState:I

    return v0
.end method

.method static synthetic access$1202(Lcom/samsung/musicplus/service/MultiPlayer;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/MultiPlayer;
    .param p1, "x1"    # I

    .prologue
    .line 56
    iput p1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mCurrentState:I

    return p1
.end method

.method static synthetic access$1300(Lcom/samsung/musicplus/service/MultiPlayer;)J
    .locals 2
    .param p0, "x0"    # Lcom/samsung/musicplus/service/MultiPlayer;

    .prologue
    .line 56
    iget-wide v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mSeekPosition:J

    return-wide v0
.end method

.method static synthetic access$1302(Lcom/samsung/musicplus/service/MultiPlayer;J)J
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/MultiPlayer;
    .param p1, "x1"    # J

    .prologue
    .line 56
    iput-wide p1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mSeekPosition:J

    return-wide p1
.end method

.method static synthetic access$1400(Lcom/samsung/musicplus/service/MultiPlayer;)Lcom/samsung/musicplus/library/audio/SoundAliveV2;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/MultiPlayer;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mSoundAliveV2:Lcom/samsung/musicplus/library/audio/SoundAliveV2;

    return-object v0
.end method

.method static synthetic access$1402(Lcom/samsung/musicplus/service/MultiPlayer;Lcom/samsung/musicplus/library/audio/SoundAliveV2;)Lcom/samsung/musicplus/library/audio/SoundAliveV2;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/MultiPlayer;
    .param p1, "x1"    # Lcom/samsung/musicplus/library/audio/SoundAliveV2;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mSoundAliveV2:Lcom/samsung/musicplus/library/audio/SoundAliveV2;

    return-object p1
.end method

.method static synthetic access$1500(Lcom/samsung/musicplus/service/MultiPlayer;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/MultiPlayer;

    .prologue
    .line 56
    iget v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mSoundAlive:I

    return v0
.end method

.method static synthetic access$1600(Lcom/samsung/musicplus/service/MultiPlayer;)Lcom/samsung/musicplus/service/MultiPlayer$OnPreparedListener;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/MultiPlayer;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMultiPlayerPreparedListener:Lcom/samsung/musicplus/service/MultiPlayer$OnPreparedListener;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/samsung/musicplus/service/MultiPlayer;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/MultiPlayer;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/samsung/musicplus/service/MultiPlayer;->playComplete()V

    return-void
.end method

.method static synthetic access$1800(Lcom/samsung/musicplus/service/MultiPlayer;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/MultiPlayer;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/samsung/musicplus/service/MultiPlayer;->recreateMediaPlayer()V

    return-void
.end method

.method static synthetic access$1900(Lcom/samsung/musicplus/service/MultiPlayer;II)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/MultiPlayer;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Lcom/samsung/musicplus/service/MultiPlayer;->handlingExtraErrors(II)Z

    move-result v0

    return v0
.end method

.method static synthetic access$200(Lcom/samsung/musicplus/service/MultiPlayer;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/MultiPlayer;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mPrepareHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/samsung/musicplus/service/MultiPlayer;)Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/MultiPlayer;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mInfo:Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;

    return-object v0
.end method

.method static synthetic access$2002(Lcom/samsung/musicplus/service/MultiPlayer;Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;)Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/MultiPlayer;
    .param p1, "x1"    # Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mInfo:Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;

    return-object p1
.end method

.method static synthetic access$2100(Lcom/samsung/musicplus/service/MultiPlayer;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/MultiPlayer;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/samsung/musicplus/service/MultiPlayer;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/MultiPlayer;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mPath:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/samsung/musicplus/service/MultiPlayer;)Landroid/net/Uri;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/MultiPlayer;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mUri:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/samsung/musicplus/service/MultiPlayer;Landroid/net/Uri;Ljava/lang/String;ZJ)Z
    .locals 2
    .param p0, "x0"    # Lcom/samsung/musicplus/service/MultiPlayer;
    .param p1, "x1"    # Landroid/net/Uri;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Z
    .param p4, "x4"    # J

    .prologue
    .line 56
    invoke-direct/range {p0 .. p5}, Lcom/samsung/musicplus/service/MultiPlayer;->setDataSourceInternal(Landroid/net/Uri;Ljava/lang/String;ZJ)Z

    move-result v0

    return v0
.end method

.method static synthetic access$2600(Lcom/samsung/musicplus/service/MultiPlayer;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/MultiPlayer;

    .prologue
    .line 56
    iget-boolean v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mIsDmrErrorState:Z

    return v0
.end method

.method static synthetic access$2700(Lcom/samsung/musicplus/service/MultiPlayer;)Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/MultiPlayer;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mDmrPlayer:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    return-object v0
.end method

.method static synthetic access$2800(Lcom/samsung/musicplus/service/MultiPlayer;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/MultiPlayer;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mDmrId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2900(Lcom/samsung/musicplus/service/MultiPlayer;Ljava/lang/String;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/MultiPlayer;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/service/MultiPlayer;->createDmrPlayer(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/samsung/musicplus/service/MultiPlayer;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/MultiPlayer;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/samsung/musicplus/service/MultiPlayer;->releaseNextMedia()V

    return-void
.end method

.method static synthetic access$3000(Lcom/samsung/musicplus/service/MultiPlayer;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/MultiPlayer;
    .param p1, "x1"    # Z

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/service/MultiPlayer;->handleDmrError(Z)V

    return-void
.end method

.method static synthetic access$3100(Lcom/samsung/musicplus/service/MultiPlayer;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/MultiPlayer;

    .prologue
    .line 56
    iget v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMode:I

    return v0
.end method

.method static synthetic access$3102(Lcom/samsung/musicplus/service/MultiPlayer;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/MultiPlayer;
    .param p1, "x1"    # I

    .prologue
    .line 56
    iput p1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMode:I

    return p1
.end method

.method static synthetic access$3202(Lcom/samsung/musicplus/service/MultiPlayer;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/MultiPlayer;
    .param p1, "x1"    # Z

    .prologue
    .line 56
    iput-boolean p1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mIsBuffering:Z

    return p1
.end method

.method static synthetic access$3300(Lcom/samsung/musicplus/service/MultiPlayer;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/MultiPlayer;

    .prologue
    .line 56
    iget-boolean v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mIsPlayRequest:Z

    return v0
.end method

.method static synthetic access$3302(Lcom/samsung/musicplus/service/MultiPlayer;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/MultiPlayer;
    .param p1, "x1"    # Z

    .prologue
    .line 56
    iput-boolean p1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mIsPlayRequest:Z

    return p1
.end method

.method static synthetic access$3400(Lcom/samsung/musicplus/service/MultiPlayer;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/MultiPlayer;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/samsung/musicplus/service/MultiPlayer;->updatePlayingState()V

    return-void
.end method

.method static synthetic access$3502(Lcom/samsung/musicplus/service/MultiPlayer;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/MultiPlayer;
    .param p1, "x1"    # Z

    .prologue
    .line 56
    iput-boolean p1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mIsPauseRequest:Z

    return p1
.end method

.method static synthetic access$3600(Lcom/samsung/musicplus/service/MultiPlayer;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/MultiPlayer;

    .prologue
    .line 56
    iget-boolean v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mIsPrepareRequest:Z

    return v0
.end method

.method static synthetic access$3602(Lcom/samsung/musicplus/service/MultiPlayer;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/MultiPlayer;
    .param p1, "x1"    # Z

    .prologue
    .line 56
    iput-boolean p1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mIsPrepareRequest:Z

    return p1
.end method

.method static synthetic access$3700(Lcom/samsung/musicplus/service/MultiPlayer;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/MultiPlayer;

    .prologue
    .line 56
    iget-boolean v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mIsSeekRequest:Z

    return v0
.end method

.method static synthetic access$3702(Lcom/samsung/musicplus/service/MultiPlayer;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/MultiPlayer;
    .param p1, "x1"    # Z

    .prologue
    .line 56
    iput-boolean p1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mIsSeekRequest:Z

    return p1
.end method

.method static synthetic access$3800(Lcom/samsung/musicplus/service/MultiPlayer;)Landroid/os/PowerManager$WakeLock;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/MultiPlayer;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    return-object v0
.end method

.method static synthetic access$3900(Lcom/samsung/musicplus/service/MultiPlayer;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/MultiPlayer;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/samsung/musicplus/service/MultiPlayer;->resetDmrRequest()V

    return-void
.end method

.method static synthetic access$400(Lcom/samsung/musicplus/service/MultiPlayer;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/MultiPlayer;
    .param p1, "x1"    # Z

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/service/MultiPlayer;->reset(Z)V

    return-void
.end method

.method static synthetic access$500(Lcom/samsung/musicplus/service/MultiPlayer;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/MultiPlayer;

    .prologue
    .line 56
    iget-boolean v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mIsSupposedToBePlaying:Z

    return v0
.end method

.method static synthetic access$502(Lcom/samsung/musicplus/service/MultiPlayer;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/MultiPlayer;
    .param p1, "x1"    # Z

    .prologue
    .line 56
    iput-boolean p1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mIsSupposedToBePlaying:Z

    return p1
.end method

.method static synthetic access$600(Lcom/samsung/musicplus/service/MultiPlayer;Landroid/net/Uri;Ljava/lang/String;ZJ)Z
    .locals 2
    .param p0, "x0"    # Lcom/samsung/musicplus/service/MultiPlayer;
    .param p1, "x1"    # Landroid/net/Uri;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Z
    .param p4, "x4"    # J

    .prologue
    .line 56
    invoke-direct/range {p0 .. p5}, Lcom/samsung/musicplus/service/MultiPlayer;->setDataSource(Landroid/net/Uri;Ljava/lang/String;ZJ)Z

    move-result v0

    return v0
.end method

.method static synthetic access$700(Lcom/samsung/musicplus/service/MultiPlayer;Landroid/net/Uri;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/MultiPlayer;
    .param p1, "x1"    # Landroid/net/Uri;

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/service/MultiPlayer;->prepareNextMedia(Landroid/net/Uri;)V

    return-void
.end method

.method static synthetic access$802(Lcom/samsung/musicplus/service/MultiPlayer;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/MultiPlayer;
    .param p1, "x1"    # I

    .prologue
    .line 56
    iput p1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mBufferPercent:I

    return p1
.end method

.method static synthetic access$900(Lcom/samsung/musicplus/service/MultiPlayer;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/MultiPlayer;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/samsung/musicplus/service/MultiPlayer;->startBuffering()V

    return-void
.end method

.method private convertToPathFromContentUri(Landroid/net/Uri;Z)Ljava/lang/String;
    .locals 7
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "isDmr"    # Z

    .prologue
    .line 795
    if-nez p1, :cond_1

    .line 796
    const/4 v1, 0x0

    .line 815
    :cond_0
    :goto_0
    return-object v1

    .line 798
    :cond_1
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    .line 800
    .local v2, "uriString":Ljava/lang/String;
    sget-object v3, Lcom/samsung/musicplus/provider/MusicContents$Audio$DeviceContents;->CONTENT_URI_STRING:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 802
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    .line 803
    .local v0, "id":Ljava/lang/String;
    iget-object v3, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mInfo:Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;

    if-nez v3, :cond_2

    .line 804
    iget-object v3, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mSA:Lcom/samsung/musicplus/account/SamsungAccountHelper;

    invoke-virtual {v3}, Lcom/samsung/musicplus/account/SamsungAccountHelper;->requestInfo()Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;

    move-result-object v3

    iput-object v3, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mInfo:Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;

    .line 807
    :cond_2
    iget-object v3, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mContext:Landroid/content/Context;

    invoke-static {v0}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iget-object v6, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mInfo:Lcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;

    invoke-static {v3, v4, v5, v6, p2}, Lcom/samsung/musicplus/provider/MusicContents$Audio$DeviceContents;->getPlayingUri(Landroid/content/Context;JLcom/samsung/musicplus/account/SamsungAccountHelper$AccountInfo;Z)Ljava/lang/String;

    move-result-object v1

    .line 808
    .local v1, "path":Ljava/lang/String;
    const-string v3, "MusicPlayer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "DeviceContents getPlayingUri path : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 809
    invoke-direct {p0, v1}, Lcom/samsung/musicplus/service/MultiPlayer;->isAndroidMediaPath(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 810
    iget-object v3, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/musicplus/util/UiUtils;->getAudioFilePath(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 813
    .end local v0    # "id":Ljava/lang/String;
    .end local v1    # "path":Ljava/lang/String;
    :cond_3
    iget-object v3, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mContext:Landroid/content/Context;

    invoke-static {v3, p1}, Lcom/samsung/musicplus/util/UiUtils;->getAudioFilePath(Landroid/content/Context;Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .restart local v1    # "path":Ljava/lang/String;
    goto :goto_0
.end method

.method private createDmrPlayer(Ljava/lang/String;)Z
    .locals 7
    .param p1, "dmrId"    # Ljava/lang/String;

    .prologue
    .line 1448
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/MultiPlayer;->releaseDmrPlayer()V

    .line 1450
    const-string v2, "MusicPlayer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "createDmrPlayer DMR id "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1452
    iget-object v2, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mContext:Landroid/content/Context;

    sget-object v3, Lcom/samsung/musicplus/contents/dlna/DlnaStore$Server;->CONTENT_URI:Landroid/net/Uri;

    sget-object v4, Lcom/samsung/musicplus/contents/dlna/DlnaStore$ServerContents;->CONTENT_URI:Landroid/net/Uri;

    sget-object v5, Lcom/samsung/musicplus/contents/dlna/DlnaStore$Renderer;->CONTENT_URI:Landroid/net/Uri;

    sget-object v6, Lcom/samsung/musicplus/contents/dlna/DlnaStore;->DLNA_ALL_DELETE_URI:Landroid/net/Uri;

    invoke-static {v2, v3, v4, v5, v6}, Lcom/samsung/musicplus/library/dlna/DlnaManager;->getInstance(Landroid/content/Context;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;)Lcom/samsung/musicplus/library/dlna/DlnaManager;

    move-result-object v1

    .line 1455
    .local v1, "dm":Lcom/samsung/musicplus/library/dlna/DlnaManager;
    invoke-virtual {v1, p1}, Lcom/samsung/musicplus/library/dlna/DlnaManager;->createSecAVPlayer(Ljava/lang/String;)Lcom/samsung/musicplus/library/dlna/DlnaManager$SimpleAVPlayer;

    move-result-object v0

    .line 1456
    .local v0, "av":Lcom/samsung/musicplus/library/dlna/DlnaManager$SimpleAVPlayer;
    if-nez v0, :cond_0

    .line 1457
    const-string v2, "MusicPlayer"

    const-string v3, "Fail to create SecAVPlayer. Please check DMR id value"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1458
    const/4 v2, 0x0

    .line 1465
    :goto_0
    return v2

    .line 1460
    :cond_0
    new-instance v2, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    iget-object v3, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mContext:Landroid/content/Context;

    invoke-direct {v2, v3, v0}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;-><init>(Landroid/content/Context;Lcom/samsung/musicplus/library/dlna/DlnaManager$SimpleAVPlayer;)V

    iput-object v2, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mDmrPlayer:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    .line 1461
    iget-object v2, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mDmrPlayer:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    iget-object v3, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mOnStateChangedListener:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$OnStateChangedListener;

    invoke-virtual {v2, v3}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->setOnStateChangedListener(Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$OnStateChangedListener;)V

    .line 1462
    iget-object v2, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mDmrPlayer:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    iget-object v3, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mOnAvPlayerErrorListener:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$OnErrorListener;

    invoke-virtual {v2, v3}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->setOnErrorListener(Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$OnErrorListener;)V

    .line 1463
    iget-object v2, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mDmrPlayer:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    iget-object v3, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mOnCompletionListener:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$OnCompletionListener;

    invoke-virtual {v2, v3}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->setOnCompletionListener(Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$OnCompletionListener;)V

    .line 1464
    iput-object p1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mDmrId:Ljava/lang/String;

    .line 1465
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private enableGoogleAudioEffect()V
    .locals 0

    .prologue
    .line 2013
    return-void
.end method

.method private getNextMedia()Lcom/samsung/musicplus/widget/NextMediaPlayer;
    .locals 1

    .prologue
    .line 2001
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mNextMedia:Lcom/samsung/musicplus/widget/NextMediaPlayer;

    return-object v0
.end method

.method private getSquareColumn(II)I
    .locals 1
    .param p1, "position"    # I
    .param p2, "columns"    # I

    .prologue
    .line 2107
    rem-int v0, p1, p2

    return v0
.end method

.method private getSquareRow(II)I
    .locals 1
    .param p1, "position"    # I
    .param p2, "columns"    # I

    .prologue
    .line 2103
    div-int v0, p1, p2

    return v0
.end method

.method private handleDmrError(Z)V
    .locals 3
    .param p1, "notify"    # Z

    .prologue
    const/4 v2, 0x1

    const/4 v1, -0x1

    .line 1572
    iput v2, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mCurrentState:I

    .line 1573
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mIsSupposedToBePlaying:Z

    .line 1574
    iput-boolean v2, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mIsDmrErrorState:Z

    .line 1575
    invoke-direct {p0}, Lcom/samsung/musicplus/service/MultiPlayer;->resetDmrRequest()V

    .line 1576
    invoke-direct {p0}, Lcom/samsung/musicplus/service/MultiPlayer;->updatePlayingState()V

    .line 1577
    invoke-direct {p0}, Lcom/samsung/musicplus/service/MultiPlayer;->stopBuffering()V

    .line 1578
    if-eqz p1, :cond_0

    .line 1579
    invoke-direct {p0, v1, v1}, Lcom/samsung/musicplus/service/MultiPlayer;->handlingExtraErrors(II)Z

    .line 1581
    :cond_0
    return-void
.end method

.method private handlingExtraErrors(II)Z
    .locals 4
    .param p1, "what"    # I
    .param p2, "extra"    # I

    .prologue
    const v3, 0x7f100124

    const/4 v0, 0x1

    const/4 v2, 0x3

    .line 1842
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/MultiPlayer;->playerStop()V

    .line 1843
    sparse-switch p1, :sswitch_data_0

    .line 1862
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 1846
    :sswitch_0
    invoke-direct {p0, v2, v3, p1, p2}, Lcom/samsung/musicplus/service/MultiPlayer;->notifyError(IIII)V

    goto :goto_0

    .line 1850
    :sswitch_1
    const/16 v1, -0x3ed

    if-ne p2, v1, :cond_0

    .line 1851
    const v1, 0x7f1000e3

    invoke-direct {p0, v2, v1, p1, p2}, Lcom/samsung/musicplus/service/MultiPlayer;->notifyError(IIII)V

    goto :goto_0

    .line 1853
    :cond_0
    invoke-direct {p0, v2, v3, p1, p2}, Lcom/samsung/musicplus/service/MultiPlayer;->notifyError(IIII)V

    goto :goto_0

    .line 1857
    :sswitch_2
    invoke-direct {p0, v2, v3, p1, p2}, Lcom/samsung/musicplus/service/MultiPlayer;->notifyError(IIII)V

    goto :goto_0

    .line 1843
    :sswitch_data_0
    .sparse-switch
        -0x13 -> :sswitch_2
        -0x1 -> :sswitch_0
        0x1 -> :sswitch_1
    .end sparse-switch
.end method

.method private hasNextPlayer(Landroid/net/Uri;)Z
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 2005
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mNextMedia:Lcom/samsung/musicplus/widget/NextMediaPlayer;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mNextMedia:Lcom/samsung/musicplus/widget/NextMediaPlayer;

    invoke-virtual {v0}, Lcom/samsung/musicplus/widget/NextMediaPlayer;->getUri()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isAndroidMediaPath(Ljava/lang/String;)Z
    .locals 1
    .param p1, "uri"    # Ljava/lang/String;

    .prologue
    .line 819
    if-eqz p1, :cond_0

    sget-object v0, Landroid/provider/MediaStore$Audio$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isPlayableDrm(Ljava/lang/String;ZLcom/samsung/musicplus/library/drm/DrmManager;)Z
    .locals 7
    .param p1, "path"    # Ljava/lang/String;
    .param p2, "byUser"    # Z
    .param p3, "dm"    # Lcom/samsung/musicplus/library/drm/DrmManager;

    .prologue
    const/4 v6, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 596
    const/4 v4, 0x3

    iput v4, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mCurrentState:I

    .line 597
    iput-boolean v2, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mIsDrm:Z

    .line 598
    iput-object v6, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mSkipDrmPath:Ljava/lang/String;

    .line 599
    const-string v4, "MusicPlayer"

    const-string v5, "setDataSource() this is DRM case"

    invoke-static {v4, v5}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 603
    if-eqz p2, :cond_0

    .line 606
    invoke-direct {p0, p3}, Lcom/samsung/musicplus/service/MultiPlayer;->setPlayReadyListener(Lcom/samsung/musicplus/library/drm/DrmManager;)V

    .line 610
    :goto_0
    invoke-virtual {p3, p1, v2}, Lcom/samsung/musicplus/library/drm/DrmManager;->getDrmInfo(Ljava/lang/String;Z)Landroid/os/Bundle;

    move-result-object v0

    .line 611
    .local v0, "data":Landroid/os/Bundle;
    const-string v4, "type"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 612
    .local v1, "type":I
    const-string v4, "MusicPlayer"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setDataSource() drm type :"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 613
    if-nez v1, :cond_1

    .line 629
    :goto_1
    return v2

    .line 608
    .end local v0    # "data":Landroid/os/Bundle;
    .end local v1    # "type":I
    :cond_0
    invoke-virtual {p3, v6}, Lcom/samsung/musicplus/library/drm/DrmManager;->setPlayReadyListener(Lcom/samsung/musicplus/library/drm/DrmManager$OnPlayReadyListener;)V

    goto :goto_0

    .line 616
    .restart local v0    # "data":Landroid/os/Bundle;
    .restart local v1    # "type":I
    :cond_1
    const/16 v2, 0x18

    if-ne v1, v2, :cond_2

    move v2, v3

    .line 619
    goto :goto_1

    .line 621
    :cond_2
    const/16 v2, 0xb

    if-ne v1, v2, :cond_3

    .line 623
    iput-object p1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mSkipDrmPath:Ljava/lang/String;

    .line 626
    :cond_3
    const-string v2, "byUser"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 627
    iget-object v2, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mHandler:Landroid/os/Handler;

    const/4 v5, 0x4

    invoke-virtual {v4, v5, v1, v3, v0}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 628
    const-string v2, "MusicPlayer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setDataSource() send drm type :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v2, v3

    .line 629
    goto :goto_1
.end method

.method private notifyError(IIII)V
    .locals 2
    .param p1, "what"    # I
    .param p2, "errorMsg"    # I
    .param p3, "arg1"    # I
    .param p4, "arg2"    # I

    .prologue
    .line 1884
    invoke-direct {p0}, Lcom/samsung/musicplus/service/MultiPlayer;->recreateMediaPlayer()V

    .line 1886
    iget-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    .line 1887
    .local v0, "msg":Landroid/os/Message;
    iput p1, v0, Landroid/os/Message;->what:I

    .line 1888
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 1889
    iput p3, v0, Landroid/os/Message;->arg1:I

    .line 1890
    iput p4, v0, Landroid/os/Message;->arg2:I

    .line 1891
    iget-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mHandler:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 1892
    return-void
.end method

.method private notifyOpenSessionForSoundAlive(ZZ)V
    .locals 8
    .param p1, "openSession"    # Z
    .param p2, "force"    # Z

    .prologue
    .line 838
    sget-boolean v5, Lcom/samsung/musicplus/util/Features;->FLAG_SUPPORT_EXTERNAL_SOUNDALIVE:Z

    if-nez v5, :cond_1

    .line 880
    :cond_0
    :goto_0
    return-void

    .line 846
    :cond_1
    const/4 v4, -0x1

    .line 848
    .local v4, "sessionId":I
    :try_start_0
    iget-object v5, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v5}, Landroid/media/MediaPlayer;->getAudioSessionId()I

    move-result v4

    .line 849
    const-string v5, "MusicPlayer"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "notifyOpenSessionForSoundAlive : new AudioSessionID "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 856
    :goto_1
    const/4 v1, -0x1

    .line 858
    .local v1, "genre":I
    iget-object v5, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    if-eqz v5, :cond_2

    .line 859
    iget-object v5, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v5}, Lcom/samsung/musicplus/service/PlayerListManager;->getGenre()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/musicplus/library/audio/SoundAliveV2;->getSoundaliveAudioEffect(Ljava/lang/String;)I

    move-result v1

    .line 862
    :cond_2
    if-nez p2, :cond_3

    iget-boolean v5, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mIsOpenSession:Z

    if-ne p1, v5, :cond_3

    iget v5, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mLastSoundAliveGenre:I

    if-ne v5, v1, :cond_3

    iget v5, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mLastSoundAliveSessionId:I

    if-eq v4, v5, :cond_0

    .line 865
    :cond_3
    iput v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mLastSoundAliveGenre:I

    .line 866
    iput v4, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mLastSoundAliveSessionId:I

    .line 867
    iput-boolean p1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mIsOpenSession:Z

    .line 869
    iget-boolean v5, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mIsOpenSession:Z

    if-eqz v5, :cond_4

    const-string v3, "android.media.action.OPEN_AUDIO_EFFECT_CONTROL_SESSION"

    .line 872
    .local v3, "intentString":Ljava/lang/String;
    :goto_2
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 873
    .local v2, "i":Landroid/content/Intent;
    const-string v5, "android.media.extra.AUDIO_SESSION"

    iget v6, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mLastSoundAliveSessionId:I

    invoke-virtual {v2, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 874
    const-string v5, "android.media.extra.PACKAGE_NAME"

    iget-object v6, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 875
    const-string v5, "com.sec.android.app.Auto"

    iget v6, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mLastSoundAliveGenre:I

    invoke-virtual {v2, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 877
    iget-object v5, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mContext:Landroid/content/Context;

    invoke-virtual {v5, v2}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 878
    const-string v5, "MusicPlayer"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "notifyOpenSessionForSoundAlive OPEN ? "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 850
    .end local v1    # "genre":I
    .end local v2    # "i":Landroid/content/Intent;
    .end local v3    # "intentString":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 853
    .local v0, "e":Ljava/lang/IllegalStateException;
    iget v4, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mAudioSession:I

    .line 854
    const-string v5, "MusicPlayer"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "notifyOpenSessionForSoundAlive : could not get AudioSessionID "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 869
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    .restart local v1    # "genre":I
    :cond_4
    const-string v3, "android.media.action.CLOSE_AUDIO_EFFECT_CONTROL_SESSION"

    goto :goto_2
.end method

.method private notifyReopenSessionForSoundAlive()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 884
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1}, Lcom/samsung/musicplus/service/MultiPlayer;->notifyOpenSessionForSoundAlive(ZZ)V

    .line 885
    invoke-direct {p0, v1, v1}, Lcom/samsung/musicplus/service/MultiPlayer;->notifyOpenSessionForSoundAlive(ZZ)V

    .line 886
    return-void
.end method

.method private playComplete()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 374
    iput-boolean v4, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mRestorePlaySpeed:Z

    .line 380
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    .line 381
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    const-wide/16 v2, 0x7530

    invoke-virtual {v0, v2, v3}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    .line 383
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 384
    invoke-direct {p0, v1, v1}, Lcom/samsung/musicplus/service/MultiPlayer;->notifyOpenSessionForSoundAlive(ZZ)V

    .line 385
    return-void
.end method

.method private prepareNextMedia(Landroid/net/Uri;)V
    .locals 6
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 1918
    const-string v4, "MusicPlayer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "prepareNextMedia() path : "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    if-nez p1, :cond_1

    const-string v3, "null"

    :goto_0
    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1920
    if-eqz p1, :cond_0

    iget-object v3, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mNextMedia:Lcom/samsung/musicplus/widget/NextMediaPlayer;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mNextMedia:Lcom/samsung/musicplus/widget/NextMediaPlayer;

    invoke-virtual {v3}, Lcom/samsung/musicplus/widget/NextMediaPlayer;->getUri()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 1981
    :cond_0
    :goto_1
    return-void

    :cond_1
    move-object v3, p1

    .line 1918
    goto :goto_0

    .line 1925
    :cond_2
    const/4 v1, 0x0

    .line 1926
    .local v1, "path":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    .line 1927
    .local v2, "uriString":Ljava/lang/String;
    const-string v3, "content://"

    invoke-virtual {v2, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 1928
    const/4 v3, 0x0

    invoke-direct {p0, p1, v3}, Lcom/samsung/musicplus/service/MultiPlayer;->convertToPathFromContentUri(Landroid/net/Uri;Z)Ljava/lang/String;

    move-result-object v1

    .line 1931
    :cond_3
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 1935
    iget-object v3, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mNextMedia:Lcom/samsung/musicplus/widget/NextMediaPlayer;

    if-nez v3, :cond_4

    .line 1936
    new-instance v3, Lcom/samsung/musicplus/widget/NextMediaPlayer;

    invoke-direct {v3}, Lcom/samsung/musicplus/widget/NextMediaPlayer;-><init>()V

    iput-object v3, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mNextMedia:Lcom/samsung/musicplus/widget/NextMediaPlayer;

    .line 1937
    iget-object v3, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mNextMedia:Lcom/samsung/musicplus/widget/NextMediaPlayer;

    iget-object v4, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mContext:Landroid/content/Context;

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Lcom/samsung/musicplus/widget/NextMediaPlayer;->setWakeMode(Landroid/content/Context;I)V

    .line 1942
    :goto_2
    iget-object v3, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mNextMedia:Lcom/samsung/musicplus/widget/NextMediaPlayer;

    iget v4, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mAudioSession:I

    invoke-virtual {v3, v4}, Lcom/samsung/musicplus/widget/NextMediaPlayer;->setAudioSessionId(I)V

    .line 1945
    :try_start_0
    iget-object v3, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mNextMedia:Lcom/samsung/musicplus/widget/NextMediaPlayer;

    iget-object v4, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mContext:Landroid/content/Context;

    invoke-virtual {v3, v4, p1, v1}, Lcom/samsung/musicplus/widget/NextMediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    .line 1955
    :goto_3
    iget-object v3, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mNextMedia:Lcom/samsung/musicplus/widget/NextMediaPlayer;

    if-eqz v3, :cond_0

    .line 1956
    iget-object v3, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mNextMedia:Lcom/samsung/musicplus/widget/NextMediaPlayer;

    new-instance v4, Lcom/samsung/musicplus/service/MultiPlayer$17;

    invoke-direct {v4, p0}, Lcom/samsung/musicplus/service/MultiPlayer$17;-><init>(Lcom/samsung/musicplus/service/MultiPlayer;)V

    invoke-virtual {v3, v4}, Lcom/samsung/musicplus/widget/NextMediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 1969
    iget-object v3, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mNextMedia:Lcom/samsung/musicplus/widget/NextMediaPlayer;

    new-instance v4, Lcom/samsung/musicplus/service/MultiPlayer$18;

    invoke-direct {v4, p0}, Lcom/samsung/musicplus/service/MultiPlayer$18;-><init>(Lcom/samsung/musicplus/service/MultiPlayer;)V

    invoke-virtual {v3, v4}, Lcom/samsung/musicplus/widget/NextMediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 1979
    iget-object v3, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mNextMedia:Lcom/samsung/musicplus/widget/NextMediaPlayer;

    invoke-virtual {v3}, Lcom/samsung/musicplus/widget/NextMediaPlayer;->prepareAsync()V

    goto :goto_1

    .line 1939
    :cond_4
    iget-object v3, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mNextMedia:Lcom/samsung/musicplus/widget/NextMediaPlayer;

    invoke-virtual {v3}, Lcom/samsung/musicplus/widget/NextMediaPlayer;->reset()V

    goto :goto_2

    .line 1946
    :catch_0
    move-exception v0

    .line 1947
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    invoke-direct {p0}, Lcom/samsung/musicplus/service/MultiPlayer;->releaseNextMedia()V

    goto :goto_3

    .line 1948
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catch_1
    move-exception v0

    .line 1949
    .local v0, "e":Ljava/lang/SecurityException;
    invoke-direct {p0}, Lcom/samsung/musicplus/service/MultiPlayer;->releaseNextMedia()V

    goto :goto_3

    .line 1950
    .end local v0    # "e":Ljava/lang/SecurityException;
    :catch_2
    move-exception v0

    .line 1951
    .local v0, "e":Ljava/lang/IllegalStateException;
    invoke-direct {p0}, Lcom/samsung/musicplus/service/MultiPlayer;->releaseNextMedia()V

    goto :goto_3

    .line 1952
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :catch_3
    move-exception v0

    .line 1953
    .local v0, "e":Ljava/io/IOException;
    invoke-direct {p0}, Lcom/samsung/musicplus/service/MultiPlayer;->releaseNextMedia()V

    goto :goto_3
.end method

.method private recreateMediaPlayer()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1900
    const/4 v0, 0x2

    iput v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mCurrentState:I

    .line 1901
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 1902
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 1907
    :cond_0
    new-instance v0, Landroid/media/MediaPlayer;

    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 1908
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mContext:Landroid/content/Context;

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaPlayer;->setWakeMode(Landroid/content/Context;I)V

    .line 1909
    iput v2, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mCurrentState:I

    .line 1910
    return-void
.end method

.method private registerListeners(Landroid/media/MediaPlayer;)V
    .locals 1
    .param p1, "mp"    # Landroid/media/MediaPlayer;

    .prologue
    .line 824
    const/4 v0, 0x3

    invoke-virtual {p1, v0}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    .line 825
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mBufferingUpdateListener:Landroid/media/MediaPlayer$OnBufferingUpdateListener;

    invoke-virtual {p1, v0}, Landroid/media/MediaPlayer;->setOnBufferingUpdateListener(Landroid/media/MediaPlayer$OnBufferingUpdateListener;)V

    .line 826
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mInfoListener:Landroid/media/MediaPlayer$OnInfoListener;

    invoke-virtual {p1, v0}, Landroid/media/MediaPlayer;->setOnInfoListener(Landroid/media/MediaPlayer$OnInfoListener;)V

    .line 827
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mCompleteListener:Landroid/media/MediaPlayer$OnCompletionListener;

    invoke-virtual {p1, v0}, Landroid/media/MediaPlayer;->setOnCompletionListener(Landroid/media/MediaPlayer$OnCompletionListener;)V

    .line 828
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mErrorListener:Landroid/media/MediaPlayer$OnErrorListener;

    invoke-virtual {p1, v0}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 829
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    invoke-virtual {p1, v0}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    .line 830
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mSeekCompleteListener:Landroid/media/MediaPlayer$OnSeekCompleteListener;

    invoke-virtual {p1, v0}, Landroid/media/MediaPlayer;->setOnSeekCompleteListener(Landroid/media/MediaPlayer$OnSeekCompleteListener;)V

    .line 831
    return-void
.end method

.method private releaseNextMedia()V
    .locals 3

    .prologue
    .line 1987
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mNextMedia:Lcom/samsung/musicplus/widget/NextMediaPlayer;

    if-eqz v0, :cond_0

    .line 1988
    const-string v0, "MusicPlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "NextMediaPlayer releaseNextMedia() uri : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mNextMedia:Lcom/samsung/musicplus/widget/NextMediaPlayer;

    invoke-virtual {v2}, Lcom/samsung/musicplus/widget/NextMediaPlayer;->getUri()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1989
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mNextMedia:Lcom/samsung/musicplus/widget/NextMediaPlayer;

    invoke-virtual {v0}, Lcom/samsung/musicplus/widget/NextMediaPlayer;->release()V

    .line 1990
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mNextMedia:Lcom/samsung/musicplus/widget/NextMediaPlayer;

    .line 1992
    :cond_0
    return-void
.end method

.method private reset(Z)V
    .locals 4
    .param p1, "changeState"    # Z

    .prologue
    .line 1043
    const-string v0, "MusicPlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "reset() state : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Lcom/samsung/musicplus/service/MultiPlayer$13;

    invoke-direct {v2, p0}, Lcom/samsung/musicplus/service/MultiPlayer$13;-><init>(Lcom/samsung/musicplus/service/MultiPlayer;)V

    iget v3, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mCurrentState:I

    invoke-static {v2, v3}, Lcom/samsung/musicplus/util/DebugUtils;->getFieldsStringValueNameForDebugging(Ljava/lang/Object;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1047
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-nez v0, :cond_1

    .line 1064
    :cond_0
    :goto_0
    return-void

    .line 1055
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    .line 1056
    if-eqz p1, :cond_2

    .line 1057
    const/4 v0, 0x2

    iput v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mCurrentState:I

    .line 1059
    :cond_2
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->reset()V

    .line 1060
    const-string v0, "MusicPlayer"

    const-string v1, "reset() completed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1061
    if-eqz p1, :cond_0

    .line 1062
    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mCurrentState:I

    goto :goto_0
.end method

.method private resetDmrRequest()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1675
    iput-boolean v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mIsPauseRequest:Z

    .line 1676
    iput-boolean v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mIsPlayRequest:Z

    .line 1677
    iput-boolean v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mIsSeekRequest:Z

    .line 1678
    iput-boolean v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mIsPrepareRequest:Z

    .line 1679
    iput-boolean v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mIsBuffering:Z

    .line 1680
    return-void
.end method

.method private setDataSource(Landroid/net/Uri;Ljava/lang/String;ZJ)Z
    .locals 8
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "path"    # Ljava/lang/String;
    .param p3, "play"    # Z
    .param p4, "position"    # J

    .prologue
    .line 534
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-wide v4, p4

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/musicplus/service/MultiPlayer;->setDataSource(Landroid/net/Uri;Ljava/lang/String;ZJZ)Z

    move-result v0

    return v0
.end method

.method private setDataSourceAfterPrepared(Landroid/net/Uri;Ljava/lang/String;J)V
    .locals 7
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "path"    # Ljava/lang/String;
    .param p3, "position"    # J

    .prologue
    .line 688
    const-string v1, "MusicPlayer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setDataSourceAfterPrepared() path : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " position : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 689
    new-instance v0, Lcom/samsung/musicplus/service/MultiPlayer$DataInfo;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/samsung/musicplus/service/MultiPlayer$DataInfo;-><init>(Lcom/samsung/musicplus/service/MultiPlayer$1;)V

    .line 690
    .local v0, "info":Lcom/samsung/musicplus/service/MultiPlayer$DataInfo;
    iput-object p1, v0, Lcom/samsung/musicplus/service/MultiPlayer$DataInfo;->uri:Landroid/net/Uri;

    .line 691
    iput-object p2, v0, Lcom/samsung/musicplus/service/MultiPlayer$DataInfo;->path:Ljava/lang/String;

    .line 692
    iput-wide p3, v0, Lcom/samsung/musicplus/service/MultiPlayer$DataInfo;->seekPosition:J

    .line 693
    iget-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mPrepareHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mPrepareHandler:Landroid/os/Handler;

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    const-wide/16 v4, 0x1f4

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 695
    return-void
.end method

.method private setDataSourceDmrPlayer(Landroid/net/Uri;Ljava/lang/String;J)Z
    .locals 9
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "path"    # Ljava/lang/String;
    .param p3, "position"    # J

    .prologue
    const/4 v0, 0x0

    const/4 v4, -0x1

    const/4 v7, 0x1

    .line 1594
    const-string v1, "MusicPlayer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setDataSourceDmrPlayer() start play with DMR path : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " position : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1596
    iget-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mDmrPlayer:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    if-nez v1, :cond_0

    .line 1597
    const-string v1, "MusicPlayer"

    const-string v2, "DMR player wasn\'t prepared"

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1631
    :goto_0
    return v0

    .line 1605
    :cond_0
    iget-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mDmrPlayer:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    invoke-virtual {v1}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->getAVPlayerState()I

    move-result v1

    const/4 v2, 0x5

    if-ne v1, v2, :cond_1

    .line 1606
    iget-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mDmrId:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/samsung/musicplus/service/MultiPlayer;->createDmrPlayer(Ljava/lang/String;)Z

    .line 1609
    :cond_1
    const/4 v1, 0x4

    iput v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mCurrentState:I

    .line 1610
    invoke-direct {p0}, Lcom/samsung/musicplus/service/MultiPlayer;->startBuffering()V

    .line 1612
    iget-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v1}, Lcom/samsung/musicplus/service/PlayerListManager;->getDlnaContentsSeed()Ljava/lang/String;

    move-result-object v6

    .line 1613
    .local v6, "seed":Ljava/lang/String;
    if-eqz v6, :cond_2

    invoke-virtual {v6}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1616
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mDmrPlayer:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    invoke-virtual {v0, v6, p3, p4}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->play(Ljava/lang/String;J)V

    :goto_1
    move v0, v7

    .line 1631
    goto :goto_0

    .line 1618
    :cond_2
    const-string v1, "content://"

    invoke-virtual {p2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1619
    invoke-direct {p0, p1, v7}, Lcom/samsung/musicplus/service/MultiPlayer;->convertToPathFromContentUri(Landroid/net/Uri;Z)Ljava/lang/String;

    move-result-object p2

    .line 1621
    :cond_3
    if-nez p2, :cond_4

    .line 1622
    invoke-direct {p0, v4, v4}, Lcom/samsung/musicplus/service/MultiPlayer;->handlingExtraErrors(II)Z

    goto :goto_0

    .line 1625
    :cond_4
    iput-object p2, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mPath:Ljava/lang/String;

    .line 1626
    iput-boolean v7, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mIsPlayRequest:Z

    .line 1629
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mDmrPlayer:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    iget-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v1}, Lcom/samsung/musicplus/service/PlayerListManager;->getTitle()Ljava/lang/String;

    move-result-object v2

    const-string v3, "audio/mpeg"

    move-object v1, p2

    move-wide v4, p3

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->play(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V

    goto :goto_1
.end method

.method private setDataSourceInternal(Landroid/net/Uri;Ljava/lang/String;ZJ)Z
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "path"    # Ljava/lang/String;
    .param p3, "play"    # Z
    .param p4, "position"    # J

    .prologue
    .line 678
    iget v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMode:I

    packed-switch v0, :pswitch_data_0

    .line 683
    invoke-direct/range {p0 .. p5}, Lcom/samsung/musicplus/service/MultiPlayer;->setDataSourceMediaPlayer(Landroid/net/Uri;Ljava/lang/String;ZJ)Z

    move-result v0

    :goto_0
    return v0

    .line 680
    :pswitch_0
    invoke-direct {p0, p1, p2, p4, p5}, Lcom/samsung/musicplus/service/MultiPlayer;->setDataSourceDmrPlayer(Landroid/net/Uri;Ljava/lang/String;J)Z

    move-result v0

    goto :goto_0

    .line 678
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method private setDataSourceMediaPlayer(Landroid/net/Uri;Ljava/lang/String;ZJ)Z
    .locals 8
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "path"    # Ljava/lang/String;
    .param p3, "play"    # Z
    .param p4, "position"    # J

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v6, -0x1

    .line 708
    const-string v1, "MusicPlayer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setDataSourceMediaPlayer() path : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " makeToPlay : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " position : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " mAudioSession "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mAudioSession:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 710
    iget-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-nez v1, :cond_0

    .line 711
    const-string v1, "MusicPlayer"

    const-string v3, "setDataSourceMediaPlayer() mMediaPlayer is null, so this service looks going to destroy. If this method call did not intented then check the logic."

    invoke-static {v1, v3}, Lcom/samsung/musicplus/library/iLog;->w(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v2

    .line 791
    :goto_0
    return v1

    .line 717
    :cond_0
    const/4 v1, 0x4

    :try_start_0
    iput v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mCurrentState:I

    .line 721
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/samsung/musicplus/service/MultiPlayer;->reset(Z)V

    .line 722
    sget-boolean v1, Lcom/samsung/musicplus/util/Features;->FLAG_SUPPORT_EXTERNAL_SOUNDALIVE:Z

    if-nez v1, :cond_1

    .line 723
    iget-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mSoundAliveV2:Lcom/samsung/musicplus/library/audio/SoundAliveV2;

    if-eqz v1, :cond_1

    .line 724
    const-string v1, "MusicPlayer"

    const-string v4, "setDataSourceMediaPlayer() : NewSoundAlive released"

    invoke-static {v1, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 725
    iget-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mSoundAliveV2:Lcom/samsung/musicplus/library/audio/SoundAliveV2;

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Lcom/samsung/musicplus/library/audio/SoundAliveV2;->setEnabled(Z)V

    .line 726
    iget-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mSoundAliveV2:Lcom/samsung/musicplus/library/audio/SoundAliveV2;

    invoke-virtual {v1}, Lcom/samsung/musicplus/library/audio/SoundAliveV2;->release()V

    .line 727
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mSoundAliveV2:Lcom/samsung/musicplus/library/audio/SoundAliveV2;

    .line 730
    :cond_1
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/service/MultiPlayer;->hasNextPlayer(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 731
    const-string v4, "MusicPlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "switchNextMedia() uri : "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, " mNextMedia uri "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mNextMedia:Lcom/samsung/musicplus/widget/NextMediaPlayer;

    if-nez v1, :cond_5

    const-string v1, "null"

    :goto_1
    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 733
    iget-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mNextMedia:Lcom/samsung/musicplus/widget/NextMediaPlayer;

    if-eqz v1, :cond_2

    .line 734
    iget-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mNextMedia:Lcom/samsung/musicplus/widget/NextMediaPlayer;

    invoke-virtual {v1}, Lcom/samsung/musicplus/widget/NextMediaPlayer;->getPath()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mPath:Ljava/lang/String;

    .line 736
    :cond_2
    invoke-direct {p0}, Lcom/samsung/musicplus/service/MultiPlayer;->getNextMedia()Lcom/samsung/musicplus/widget/NextMediaPlayer;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 737
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mNextMedia:Lcom/samsung/musicplus/widget/NextMediaPlayer;

    .line 738
    iget-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-direct {p0, v1}, Lcom/samsung/musicplus/service/MultiPlayer;->registerListeners(Landroid/media/MediaPlayer;)V

    .line 739
    iget-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    instance-of v1, v1, Lcom/samsung/musicplus/widget/NextMediaPlayer;

    if-eqz v1, :cond_3

    .line 740
    iget-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    check-cast v1, Lcom/samsung/musicplus/widget/NextMediaPlayer;

    invoke-virtual {v1}, Lcom/samsung/musicplus/widget/NextMediaPlayer;->isPrepared()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 741
    iget-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mPreparedListener:Landroid/media/MediaPlayer$OnPreparedListener;

    iget-object v4, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-interface {v1, v4}, Landroid/media/MediaPlayer$OnPreparedListener;->onPrepared(Landroid/media/MediaPlayer;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_3

    .line 780
    :cond_3
    :goto_2
    iget-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    if-eqz v1, :cond_4

    .line 781
    iget-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v1}, Lcom/samsung/musicplus/service/PlayerListManager;->getListType()I

    move-result v1

    const v2, 0x2000d

    if-ne v1, v2, :cond_4

    .line 783
    iget-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mPrepareHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mPrepareHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v4}, Lcom/samsung/musicplus/service/PlayerListManager;->getNextMediaUri()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    const-wide/16 v4, 0x12c

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 790
    :cond_4
    invoke-direct {p0}, Lcom/samsung/musicplus/service/MultiPlayer;->notifyReopenSessionForSoundAlive()V

    move v1, v3

    .line 791
    goto/16 :goto_0

    .line 731
    :cond_5
    :try_start_1
    iget-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mNextMedia:Lcom/samsung/musicplus/widget/NextMediaPlayer;

    invoke-virtual {v1}, Lcom/samsung/musicplus/widget/NextMediaPlayer;->getPath()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    .line 747
    :cond_6
    iget-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget v4, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mAudioSession:I

    invoke-virtual {v1, v4}, Landroid/media/MediaPlayer;->setAudioSessionId(I)V

    .line 750
    const-string v1, "content://"

    invoke-virtual {p2, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 751
    const/4 v1, 0x0

    invoke-direct {p0, p1, v1}, Lcom/samsung/musicplus/service/MultiPlayer;->convertToPathFromContentUri(Landroid/net/Uri;Z)Ljava/lang/String;

    move-result-object p2

    .line 753
    :cond_7
    if-nez p2, :cond_8

    .line 754
    const/4 v1, -0x1

    const/4 v3, -0x1

    invoke-direct {p0, v1, v3}, Lcom/samsung/musicplus/service/MultiPlayer;->handlingExtraErrors(II)Z

    move v1, v2

    .line 755
    goto/16 :goto_0

    .line 757
    :cond_8
    iput-object p2, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mPath:Ljava/lang/String;

    .line 758
    iget-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1, p2}, Landroid/media/MediaPlayer;->setDataSource(Ljava/lang/String;)V

    .line 759
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/MultiPlayer;->setParamForK2HD()V

    .line 760
    iget-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-direct {p0, v1}, Lcom/samsung/musicplus/service/MultiPlayer;->registerListeners(Landroid/media/MediaPlayer;)V

    .line 761
    iget-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->prepareAsync()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_3

    goto :goto_2

    .line 763
    :catch_0
    move-exception v0

    .line 764
    .local v0, "ex":Ljava/io/IOException;
    const-string v1, "MusicPlayer"

    const-string v3, "MP-setDataSourceAsync:IOException"

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 765
    invoke-direct {p0, v6, v6}, Lcom/samsung/musicplus/service/MultiPlayer;->handlingExtraErrors(II)Z

    move v1, v2

    .line 766
    goto/16 :goto_0

    .line 767
    .end local v0    # "ex":Ljava/io/IOException;
    :catch_1
    move-exception v0

    .line 768
    .local v0, "ex":Ljava/lang/IllegalArgumentException;
    const-string v1, "MusicPlayer"

    const-string v3, "MP-setDataSourceAsync:IllegalArgumentException"

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 769
    invoke-direct {p0, v6, v6}, Lcom/samsung/musicplus/service/MultiPlayer;->handlingExtraErrors(II)Z

    move v1, v2

    .line 770
    goto/16 :goto_0

    .line 771
    .end local v0    # "ex":Ljava/lang/IllegalArgumentException;
    :catch_2
    move-exception v0

    .line 772
    .local v0, "ex":Ljava/lang/IllegalStateException;
    const-string v1, "MusicPlayer"

    const-string v3, "MP-setDataSourceAsync:IllegalStateException"

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 773
    invoke-direct {p0, v6, v6}, Lcom/samsung/musicplus/service/MultiPlayer;->handlingExtraErrors(II)Z

    move v1, v2

    .line 774
    goto/16 :goto_0

    .line 775
    .end local v0    # "ex":Ljava/lang/IllegalStateException;
    :catch_3
    move-exception v0

    .line 776
    .local v0, "ex":Ljava/lang/SecurityException;
    const-string v1, "MusicPlayer"

    const-string v3, "MP-setDataSourceAsync:SecurityException"

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 777
    invoke-direct {p0, v6, v6}, Lcom/samsung/musicplus/service/MultiPlayer;->handlingExtraErrors(II)Z

    move v1, v2

    .line 778
    goto/16 :goto_0
.end method

.method private setPlayReadyListener(Lcom/samsung/musicplus/library/drm/DrmManager;)V
    .locals 1
    .param p1, "dm"    # Lcom/samsung/musicplus/library/drm/DrmManager;

    .prologue
    .line 634
    new-instance v0, Lcom/samsung/musicplus/service/MultiPlayer$10;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/service/MultiPlayer$10;-><init>(Lcom/samsung/musicplus/service/MultiPlayer;)V

    invoke-virtual {p1, v0}, Lcom/samsung/musicplus/library/drm/DrmManager;->setPlayReadyListener(Lcom/samsung/musicplus/library/drm/DrmManager$OnPlayReadyListener;)V

    .line 675
    return-void
.end method

.method private setRealVolume(FF)V
    .locals 1
    .param p1, "l"    # F
    .param p2, "r"    # F

    .prologue
    .line 1203
    sget-boolean v0, Lcom/samsung/musicplus/util/Features;->FLAG_SUPPORT_GALLERY_VOLUME_EFFECT:Z

    if-eqz v0, :cond_1

    .line 1204
    iget v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mCurLeftLimitVolume:F

    cmpg-float v0, v0, p1

    if-gez v0, :cond_0

    .line 1205
    iget p1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mCurLeftLimitVolume:F

    .line 1207
    :cond_0
    iget v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mCurRightLimitVolume:F

    cmpg-float v0, v0, p2

    if-gez v0, :cond_1

    .line 1208
    iget p2, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mCurRightLimitVolume:F

    .line 1211
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p1, p2}, Landroid/media/MediaPlayer;->setVolume(FF)V

    .line 1212
    return-void
.end method

.method private startBuffering()V
    .locals 2

    .prologue
    .line 1643
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1644
    return-void
.end method

.method private stopBuffering()V
    .locals 2

    .prologue
    .line 1639
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1640
    return-void
.end method

.method private updatePlayingState()V
    .locals 2

    .prologue
    .line 1635
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mHandler:Landroid/os/Handler;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1636
    return-void
.end method


# virtual methods
.method changeToDefaultPlayer(Z)V
    .locals 7
    .param p1, "autoPlay"    # Z

    .prologue
    const/4 v3, 0x1

    .line 1496
    const-string v0, "MusicPlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "changeToDefaultPlayer mMode "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMode:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " autoPlay ? "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1499
    iget v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMode:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 1500
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mDmrId:Ljava/lang/String;

    .line 1501
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/MultiPlayer;->isPlaying()Z

    move-result v6

    .line 1502
    .local v6, "wasPlaying":Z
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/MultiPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1503
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/MultiPlayer;->pause()V

    .line 1505
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/MultiPlayer;->position()J

    move-result-wide v4

    .line 1507
    .local v4, "position":J
    iput v3, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mCurrentState:I

    .line 1508
    iput v3, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMode:I

    .line 1509
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mUri:Landroid/net/Uri;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mUri:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/samsung/musicplus/provider/MusicContents$Audio$DeviceContents;->CONTENT_URI_STRING:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1512
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mUri:Landroid/net/Uri;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/samsung/musicplus/service/MultiPlayer;->convertToPathFromContentUri(Landroid/net/Uri;Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mPath:Ljava/lang/String;

    .line 1514
    :cond_1
    iget-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mUri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mPath:Ljava/lang/String;

    if-eqz p1, :cond_3

    :goto_0
    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/samsung/musicplus/service/MultiPlayer;->setDataSource(Landroid/net/Uri;Ljava/lang/String;ZJ)Z

    .line 1515
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1516
    invoke-direct {p0}, Lcom/samsung/musicplus/service/MultiPlayer;->stopBuffering()V

    .line 1517
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/MultiPlayer;->releaseDmrPlayer()V

    .line 1518
    if-eqz p1, :cond_2

    .line 1519
    invoke-direct {p0}, Lcom/samsung/musicplus/service/MultiPlayer;->updatePlayingState()V

    .line 1522
    .end local v4    # "position":J
    .end local v6    # "wasPlaying":Z
    :cond_2
    return-void

    .restart local v4    # "position":J
    .restart local v6    # "wasPlaying":Z
    :cond_3
    move v3, v6

    .line 1514
    goto :goto_0
.end method

.method changeToDmrPlayer(Ljava/lang/String;)V
    .locals 8
    .param p1, "dmrId"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x0

    const/4 v3, 0x1

    .line 1526
    const-string v0, "MusicPlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "changeToDmrPlayer DMR id "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1529
    iget v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMode:I

    if-ne v0, v3, :cond_4

    .line 1530
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mPath:Ljava/lang/String;

    invoke-static {v0}, Lcom/samsung/musicplus/provider/MusicContents$Audio$DeviceContents;->isScsOrSameApConnection(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1532
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mUri:Landroid/net/Uri;

    invoke-direct {p0, v0, v3}, Lcom/samsung/musicplus/service/MultiPlayer;->convertToPathFromContentUri(Landroid/net/Uri;Z)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mPath:Ljava/lang/String;

    .line 1534
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/MultiPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1535
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/MultiPlayer;->pause()V

    .line 1537
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/MultiPlayer;->position()J

    move-result-wide v4

    .line 1538
    .local v4, "position":J
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/service/MultiPlayer;->createDmrPlayer(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1539
    invoke-direct {p0, v6}, Lcom/samsung/musicplus/service/MultiPlayer;->handleDmrError(Z)V

    .line 1569
    .end local v4    # "position":J
    :cond_2
    :goto_0
    return-void

    .line 1543
    .restart local v4    # "position":J
    :cond_3
    iput v3, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mCurrentState:I

    .line 1544
    iput v7, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMode:I

    .line 1545
    iget-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mUri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mPath:Ljava/lang/String;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/samsung/musicplus/service/MultiPlayer;->setDataSource(Landroid/net/Uri;Ljava/lang/String;ZJ)Z

    .line 1546
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1550
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mContext:Landroid/content/Context;

    const-string v1, "DLNA"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/util/logging/FeatureLogger;->insertLog(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    .line 1554
    .end local v4    # "position":J
    :cond_4
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mDmrId:Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mDmrId:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1555
    :cond_5
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/MultiPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1556
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/MultiPlayer;->pause()V

    .line 1558
    :cond_6
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/MultiPlayer;->position()J

    move-result-wide v4

    .line 1559
    .restart local v4    # "position":J
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/service/MultiPlayer;->createDmrPlayer(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 1560
    invoke-direct {p0, v6}, Lcom/samsung/musicplus/service/MultiPlayer;->handleDmrError(Z)V

    goto :goto_0

    .line 1564
    :cond_7
    iput v3, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mCurrentState:I

    .line 1565
    iput v7, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMode:I

    .line 1566
    iget-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mUri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mPath:Ljava/lang/String;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/samsung/musicplus/service/MultiPlayer;->setDataSource(Landroid/net/Uri;Ljava/lang/String;ZJ)Z

    goto :goto_0
.end method

.method duration()J
    .locals 5

    .prologue
    const/4 v3, 0x4

    .line 1097
    const-wide/16 v0, -0x1

    .line 1098
    .local v0, "duration":J
    iget v2, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMode:I

    packed-switch v2, :pswitch_data_0

    .line 1108
    iget v2, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mCurrentState:I

    if-le v2, v3, :cond_0

    .line 1109
    iget-object v2, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v2, :cond_0

    .line 1110
    iget-object v2, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->getDuration()I

    move-result v2

    int-to-long v0, v2

    .line 1115
    :cond_0
    :goto_0
    iget-wide v2, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mDuration:J

    cmp-long v2, v2, v0

    if-eqz v2, :cond_1

    .line 1116
    iput-wide v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mDuration:J

    .line 1117
    const-string v2, "MusicPlayer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "duration() : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " mCurrentState : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mCurrentState:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " mode : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMode:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1120
    :cond_1
    return-wide v0

    .line 1100
    :pswitch_0
    iget v2, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mCurrentState:I

    if-le v2, v3, :cond_0

    .line 1101
    iget-object v2, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mDmrPlayer:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    if-eqz v2, :cond_0

    .line 1102
    iget-object v2, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mDmrPlayer:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    invoke-virtual {v2}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->getDuration()J

    move-result-wide v0

    goto :goto_0

    .line 1098
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method getAudioSessionId()I
    .locals 4

    .prologue
    .line 1327
    const/4 v0, -0x1

    .line 1328
    .local v0, "id":I
    iget-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v1, :cond_0

    .line 1329
    iget-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->getAudioSessionId()I

    move-result v0

    .line 1331
    :cond_0
    const-string v1, "MusicPlayer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getAudioSessionId() : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1332
    return v0
.end method

.method getBufferingPercent()I
    .locals 1

    .prologue
    .line 503
    iget v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mBufferPercent:I

    return v0
.end method

.method getDmrId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1487
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mDmrId:Ljava/lang/String;

    return-object v0
.end method

.method getMode()I
    .locals 1

    .prologue
    .line 1491
    iget v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMode:I

    return v0
.end method

.method getNewSoundAliveBandLevel(I)I
    .locals 4
    .param p1, "bandLevel"    # I

    .prologue
    .line 2206
    iget-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mSoundAliveV2:Lcom/samsung/musicplus/library/audio/SoundAliveV2;

    if-eqz v1, :cond_0

    .line 2207
    iget-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mSoundAliveV2:Lcom/samsung/musicplus/library/audio/SoundAliveV2;

    int-to-short v2, p1

    invoke-virtual {v1, v2}, Lcom/samsung/musicplus/library/audio/SoundAliveV2;->getBandLevel(I)I

    move-result v0

    .line 2208
    .local v0, "level":I
    const-string v1, "MusicPlayer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getBandLevel bandNum : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " Level : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2211
    .end local v0    # "level":I
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method getNewSoundAliveBandLevel()[I
    .locals 1

    .prologue
    .line 2147
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mBandLevel:[I

    return-object v0
.end method

.method getNewSoundAliveCurrentPreset()I
    .locals 1

    .prologue
    .line 2060
    iget v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mPresetEffect:I

    return v0
.end method

.method getNewSoundAliveRoundedStrength(I)I
    .locals 4
    .param p1, "effect"    # I

    .prologue
    .line 2215
    iget-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mSoundAliveV2:Lcom/samsung/musicplus/library/audio/SoundAliveV2;

    if-eqz v1, :cond_0

    .line 2216
    iget-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mSoundAliveV2:Lcom/samsung/musicplus/library/audio/SoundAliveV2;

    int-to-short v2, p1

    invoke-virtual {v1, v2}, Lcom/samsung/musicplus/library/audio/SoundAliveV2;->getRoundedStrength(S)I

    move-result v0

    .line 2217
    .local v0, "strength":I
    const-string v1, "MusicPlayer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getRoundedStrength type : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " result : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2220
    .end local v0    # "strength":I
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method getNewSoundAliveRoundedStrength()[I
    .locals 1

    .prologue
    .line 2202
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mStrengthEffect:[I

    return-object v0
.end method

.method getNewSoundAliveSquarePosition()[I
    .locals 1

    .prologue
    .line 2117
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mSquarePosition:[I

    return-object v0
.end method

.method getPath()Ljava/lang/String;
    .locals 1

    .prologue
    .line 520
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mPath:Ljava/lang/String;

    return-object v0
.end method

.method getPlaySpeed()F
    .locals 1

    .prologue
    .line 1406
    iget v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mSpeed:F

    return v0
.end method

.method getSoundAlive()I
    .locals 1

    .prologue
    .line 1344
    iget v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mSoundAlive:I

    return v0
.end method

.method getSoundAliveUserEq()[I
    .locals 1

    .prologue
    .line 1398
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mUserEq:[I

    return-object v0
.end method

.method getSoundAliveUserExtended()[I
    .locals 1

    .prologue
    .line 1402
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mUserEffect:[I

    return-object v0
.end method

.method getState()I
    .locals 1

    .prologue
    .line 507
    iget v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mCurrentState:I

    return v0
.end method

.method isDrm()Z
    .locals 1

    .prologue
    .line 913
    iget-boolean v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mIsDrm:Z

    return v0
.end method

.method isInitialized()Z
    .locals 2

    .prologue
    .line 494
    iget v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMode:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mIsDmrErrorState:Z

    if-nez v0, :cond_1

    :cond_0
    iget v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mCurrentState:I

    const/4 v1, 0x4

    if-lt v0, v1, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method isNetworkPath()Z
    .locals 3

    .prologue
    .line 1866
    const-string v0, "MusicPlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isNetworkPath path : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mPath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1868
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mPath:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mPath:Ljava/lang/String;

    const-string v1, "http:"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mPath:Ljava/lang/String;

    const-string v1, "ntcl:"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mPath:Ljava/lang/String;

    const-string v1, "slhttp:"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method isPlaying()Z
    .locals 3

    .prologue
    .line 905
    iget-boolean v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mWasPlaying:Z

    iget-boolean v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mIsSupposedToBePlaying:Z

    if-eq v0, v1, :cond_0

    .line 906
    iget-boolean v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mIsSupposedToBePlaying:Z

    iput-boolean v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mWasPlaying:Z

    .line 907
    const-string v0, "MusicPlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "isPlaying() "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mIsSupposedToBePlaying:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 909
    :cond_0
    iget-boolean v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mIsSupposedToBePlaying:Z

    return v0
.end method

.method isPreparing()Z
    .locals 2

    .prologue
    .line 1662
    iget v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mCurrentState:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mIsSeekRequest:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mPrepareRadio:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method isReqestingDmrCommend()Z
    .locals 1

    .prologue
    .line 1670
    iget-boolean v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mIsPauseRequest:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mIsPlayRequest:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mIsSeekRequest:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mIsPrepareRequest:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mIsBuffering:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method isStop()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 1033
    iget v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mCurrentState:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mCurrentState:I

    if-ne v1, v0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method pause()V
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x0

    .line 974
    const-string v0, "MusicPlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "pause() mode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMode:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " mCurrentState : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Lcom/samsung/musicplus/service/MultiPlayer$12;

    invoke-direct {v2, p0}, Lcom/samsung/musicplus/service/MultiPlayer$12;-><init>(Lcom/samsung/musicplus/service/MultiPlayer;)V

    iget v3, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mCurrentState:I

    invoke-static {v2, v3}, Lcom/samsung/musicplus/util/DebugUtils;->getFieldsStringValueNameForDebugging(Ljava/lang/Object;I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 978
    iget v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMode:I

    packed-switch v0, :pswitch_data_0

    .line 998
    iput-boolean v4, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mIsSupposedToBePlaying:Z

    .line 999
    iget v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mCurrentState:I

    if-le v0, v5, :cond_0

    .line 1000
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 1001
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1004
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->pause()V

    .line 1005
    invoke-direct {p0, v4, v4}, Lcom/samsung/musicplus/service/MultiPlayer;->notifyOpenSessionForSoundAlive(ZZ)V

    .line 1011
    :cond_0
    :goto_0
    return-void

    .line 980
    :pswitch_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/MultiPlayer;->isReqestingDmrCommend()Z

    move-result v0

    if-nez v0, :cond_0

    .line 985
    iget v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mCurrentState:I

    if-le v0, v5, :cond_0

    .line 986
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mDmrPlayer:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    if-eqz v0, :cond_0

    .line 987
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mIsPauseRequest:Z

    .line 988
    invoke-direct {p0}, Lcom/samsung/musicplus/service/MultiPlayer;->startBuffering()V

    .line 989
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mDmrPlayer:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    invoke-virtual {v0}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->pause()V

    goto :goto_0

    .line 978
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method playerStop()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1020
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/MultiPlayer;->isStop()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1021
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/service/MultiPlayer;->reset(Z)V

    .line 1022
    invoke-direct {p0, v1, v1}, Lcom/samsung/musicplus/service/MultiPlayer;->notifyOpenSessionForSoundAlive(ZZ)V

    .line 1024
    :cond_0
    iget v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMode:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 1025
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mDmrPlayer:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    if-eqz v0, :cond_1

    .line 1026
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mDmrPlayer:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    invoke-virtual {v0}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->stop()V

    .line 1027
    invoke-direct {p0}, Lcom/samsung/musicplus/service/MultiPlayer;->resetDmrRequest()V

    .line 1030
    :cond_1
    return-void
.end method

.method position()J
    .locals 6

    .prologue
    const/4 v3, 0x4

    .line 1129
    const-wide/16 v0, 0x0

    .line 1130
    .local v0, "position":J
    iget v2, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMode:I

    packed-switch v2, :pswitch_data_0

    .line 1140
    iget v2, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mCurrentState:I

    if-le v2, v3, :cond_0

    .line 1141
    iget-object v2, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v2, :cond_0

    .line 1142
    iget-object v2, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v2}, Landroid/media/MediaPlayer;->getCurrentPosition()I

    move-result v2

    int-to-long v0, v2

    .line 1148
    :cond_0
    :goto_0
    iget-wide v2, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mPosition:J

    const-wide/16 v4, 0x1388

    sub-long v4, v0, v4

    cmp-long v2, v2, v4

    if-gez v2, :cond_1

    .line 1149
    iput-wide v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mPosition:J

    .line 1150
    const-string v2, "MusicPlayer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "position() : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " mCurrentState : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mCurrentState:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " mode : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMode:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1153
    :cond_1
    return-wide v0

    .line 1132
    :pswitch_0
    iget v2, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mCurrentState:I

    if-le v2, v3, :cond_0

    .line 1133
    iget-object v2, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mDmrPlayer:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    if-eqz v2, :cond_0

    .line 1134
    iget-object v2, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mDmrPlayer:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    invoke-virtual {v2}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->getCurrentPosition()J

    move-result-wide v0

    goto :goto_0

    .line 1130
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method release()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1071
    const-string v0, "MusicPlayer"

    const-string v1, "release()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1072
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mSA:Lcom/samsung/musicplus/account/SamsungAccountHelper;

    invoke-virtual {v0}, Lcom/samsung/musicplus/account/SamsungAccountHelper;->release()V

    .line 1073
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/MultiPlayer;->stop()V

    .line 1074
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 1075
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    .line 1076
    iput-object v2, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    .line 1078
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_1

    .line 1079
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 1080
    iput-object v2, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 1082
    :cond_1
    sget-boolean v0, Lcom/samsung/musicplus/util/Features;->FLAG_SUPPORT_EXTERNAL_SOUNDALIVE:Z

    if-nez v0, :cond_2

    .line 1083
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mSoundAliveV2:Lcom/samsung/musicplus/library/audio/SoundAliveV2;

    if-eqz v0, :cond_2

    .line 1084
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mSoundAliveV2:Lcom/samsung/musicplus/library/audio/SoundAliveV2;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/library/audio/SoundAliveV2;->setEnabled(Z)V

    .line 1085
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mSoundAliveV2:Lcom/samsung/musicplus/library/audio/SoundAliveV2;

    invoke-virtual {v0}, Lcom/samsung/musicplus/library/audio/SoundAliveV2;->release()V

    .line 1086
    iput-object v2, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mSoundAliveV2:Lcom/samsung/musicplus/library/audio/SoundAliveV2;

    .line 1089
    :cond_2
    return-void
.end method

.method releaseDmrPlayer()V
    .locals 4

    .prologue
    const/4 v1, 0x2

    const/4 v3, 0x0

    .line 1469
    iget v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMode:I

    if-ne v0, v1, :cond_0

    .line 1470
    iput v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mCurrentState:I

    .line 1473
    :cond_0
    const-string v0, "MusicPlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "releaseDmrPlayer mDmrPlayer "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mDmrPlayer:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1475
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mDmrPlayer:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    if-eqz v0, :cond_1

    .line 1476
    invoke-direct {p0}, Lcom/samsung/musicplus/service/MultiPlayer;->resetDmrRequest()V

    .line 1477
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mDmrPlayer:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    invoke-virtual {v0}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->stop()V

    .line 1478
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mDmrPlayer:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    invoke-virtual {v0}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->releaseSecAVPlayer()V

    .line 1479
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mDmrPlayer:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    invoke-virtual {v0, v3}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->setOnStateChangedListener(Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$OnStateChangedListener;)V

    .line 1480
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mDmrPlayer:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    invoke-virtual {v0, v3}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->setOnErrorListener(Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$OnErrorListener;)V

    .line 1481
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mDmrPlayer:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    invoke-virtual {v0, v3}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->setOnCompletionListener(Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager$OnCompletionListener;)V

    .line 1482
    iput-object v3, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mDmrPlayer:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    .line 1484
    :cond_1
    return-void
.end method

.method seek(J)J
    .locals 9
    .param p1, "whereto"    # J

    .prologue
    const-wide/16 v2, -0x1

    const/4 v7, 0x4

    .line 1157
    const-string v4, "MusicPlayer"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "seek() : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1158
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/MultiPlayer;->duration()J

    move-result-wide v0

    .line 1159
    .local v0, "duration":J
    const-wide/16 v4, 0x0

    cmp-long v4, p1, v4

    if-gez v4, :cond_0

    .line 1160
    const-wide/16 p1, 0x0

    .line 1162
    :cond_0
    cmp-long v4, v0, v2

    if-eqz v4, :cond_1

    cmp-long v4, p1, v0

    if-lez v4, :cond_1

    .line 1163
    invoke-direct {p0}, Lcom/samsung/musicplus/service/MultiPlayer;->playComplete()V

    .line 1189
    .end local v0    # "duration":J
    :goto_0
    return-wide v0

    .line 1166
    .restart local v0    # "duration":J
    :cond_1
    iget v4, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMode:I

    packed-switch v4, :pswitch_data_0

    .line 1182
    iget v2, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mCurrentState:I

    if-le v2, v7, :cond_2

    .line 1183
    iget-object v2, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v2, :cond_2

    .line 1184
    iget-object v2, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    long-to-int v3, p1

    invoke-virtual {v2, v3}, Landroid/media/MediaPlayer;->seekTo(I)V

    :cond_2
    :goto_1
    move-wide v0, p1

    .line 1189
    goto :goto_0

    .line 1168
    :pswitch_0
    iget v4, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mCurrentState:I

    if-le v4, v7, :cond_2

    .line 1169
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/MultiPlayer;->isPlaying()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {p0}, Lcom/samsung/musicplus/service/MultiPlayer;->isReqestingDmrCommend()Z

    move-result v4

    if-eqz v4, :cond_4

    :cond_3
    move-wide v0, v2

    .line 1171
    goto :goto_0

    .line 1173
    :cond_4
    invoke-direct {p0}, Lcom/samsung/musicplus/service/MultiPlayer;->startBuffering()V

    .line 1174
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mIsSeekRequest:Z

    .line 1175
    iget-object v2, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mDmrPlayer:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    if-eqz v2, :cond_2

    .line 1176
    iget-object v2, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mDmrPlayer:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    invoke-virtual {v2, p1, p2}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->seek(J)V

    goto :goto_1

    .line 1166
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method setAudioSessionId(I)V
    .locals 3
    .param p1, "sessionId"    # I

    .prologue
    .line 1320
    const-string v0, "MusicPlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setAudioSessionId() : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1321
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 1322
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p1}, Landroid/media/MediaPlayer;->setAudioSessionId(I)V

    .line 1324
    :cond_0
    return-void
.end method

.method setAutoSoundAlive(Ljava/lang/String;)V
    .locals 2
    .param p1, "genre"    # Ljava/lang/String;

    .prologue
    .line 1360
    invoke-static {p1}, Lcom/samsung/musicplus/library/audio/SoundAlive;->getAudioEffect(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mSoundAlive:I

    .line 1361
    iget v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMode:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1362
    iget v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mCurrentState:I

    const/4 v1, 0x4

    if-le v0, v1, :cond_0

    .line 1363
    iget v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mSoundAlive:I

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/service/MultiPlayer;->setSoundAlive(I)V

    .line 1366
    :cond_0
    return-void
.end method

.method setAutoSquare(Z)V
    .locals 0
    .param p1, "value"    # Z

    .prologue
    .line 2113
    iput-boolean p1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mAuto:Z

    .line 2114
    return-void
.end method

.method setDataSource(Landroid/net/Uri;Ljava/lang/String;ZJZ)Z
    .locals 8
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "path"    # Ljava/lang/String;
    .param p3, "play"    # Z
    .param p4, "position"    # J
    .param p6, "byUser"    # Z

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 549
    const-string v3, "MusicPlayer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setDataSource() uri : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " path : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " makeToPlay : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " position : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " byUser : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " mCurrentState : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    new-instance v5, Lcom/samsung/musicplus/service/MultiPlayer$9;

    invoke-direct {v5, p0}, Lcom/samsung/musicplus/service/MultiPlayer$9;-><init>(Lcom/samsung/musicplus/service/MultiPlayer;)V

    iget v6, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mCurrentState:I

    invoke-static {v5, v6}, Lcom/samsung/musicplus/util/DebugUtils;->getFieldsStringValueNameForDebugging(Ljava/lang/Object;I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 553
    if-nez p2, :cond_1

    .line 591
    :cond_0
    :goto_0
    return v1

    .line 557
    :cond_1
    iput-boolean v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mIsDmrErrorState:Z

    .line 558
    iput-boolean v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mPrepareRadio:Z

    .line 559
    iput-object p1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mUri:Landroid/net/Uri;

    .line 560
    iput-object p2, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mPath:Ljava/lang/String;

    .line 561
    iput-boolean p3, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mIsSupposedToBePlaying:Z

    .line 562
    iput-wide p4, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mSeekPosition:J

    .line 563
    iput v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mBufferPercent:I

    .line 565
    iget-object v3, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mPrepareHandler:Landroid/os/Handler;

    invoke-virtual {v3, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 566
    iget v3, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mCurrentState:I

    const/4 v4, 0x4

    if-eq v3, v4, :cond_2

    iget-object v3, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mNextMedia:Lcom/samsung/musicplus/widget/NextMediaPlayer;

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mNextMedia:Lcom/samsung/musicplus/widget/NextMediaPlayer;

    invoke-virtual {v3}, Lcom/samsung/musicplus/widget/NextMediaPlayer;->isPrepared()Z

    move-result v3

    if-nez v3, :cond_4

    .line 569
    :cond_2
    iget-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mNextMedia:Lcom/samsung/musicplus/widget/NextMediaPlayer;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mNextMedia:Lcom/samsung/musicplus/widget/NextMediaPlayer;

    invoke-virtual {v1}, Lcom/samsung/musicplus/widget/NextMediaPlayer;->isPrepared()Z

    move-result v1

    if-nez v1, :cond_3

    .line 570
    invoke-direct {p0, v2}, Lcom/samsung/musicplus/service/MultiPlayer;->reset(Z)V

    .line 572
    :cond_3
    invoke-direct {p0, p1, p2, p4, p5}, Lcom/samsung/musicplus/service/MultiPlayer;->setDataSourceAfterPrepared(Landroid/net/Uri;Ljava/lang/String;J)V

    move v1, v2

    .line 573
    goto :goto_0

    .line 576
    :cond_4
    iget-object v3, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/samsung/musicplus/library/drm/DrmManager;->getInstance(Landroid/content/Context;)Lcom/samsung/musicplus/library/drm/DrmManager;

    move-result-object v0

    .line 577
    .local v0, "dm":Lcom/samsung/musicplus/library/drm/DrmManager;
    const-string v3, "MusicPlayer"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setDataSource() drm path :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " mSkipDrmPath : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mSkipDrmPath:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 578
    iget-object v3, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mSkipDrmPath:Ljava/lang/String;

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    invoke-virtual {v0, p2}, Lcom/samsung/musicplus/library/drm/DrmManager;->isDrm(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 579
    invoke-direct {p0, p2, p6, v0}, Lcom/samsung/musicplus/service/MultiPlayer;->isPlayableDrm(Ljava/lang/String;ZLcom/samsung/musicplus/library/drm/DrmManager;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 580
    invoke-direct/range {p0 .. p5}, Lcom/samsung/musicplus/service/MultiPlayer;->setDataSourceInternal(Landroid/net/Uri;Ljava/lang/String;ZJ)Z

    move-result v1

    goto :goto_0

    .line 584
    :cond_5
    iget-object v3, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mSkipDrmPath:Ljava/lang/String;

    invoke-virtual {p2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 585
    iput-boolean v2, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mIsDrm:Z

    .line 589
    :goto_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mSkipDrmPath:Ljava/lang/String;

    .line 591
    invoke-direct/range {p0 .. p5}, Lcom/samsung/musicplus/service/MultiPlayer;->setDataSourceInternal(Landroid/net/Uri;Ljava/lang/String;ZJ)Z

    move-result v1

    goto/16 :goto_0

    .line 587
    :cond_6
    iput-boolean v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mIsDrm:Z

    goto :goto_1
.end method

.method setHandler(Landroid/os/Handler;)V
    .locals 0
    .param p1, "handler"    # Landroid/os/Handler;

    .prologue
    .line 511
    iput-object p1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mHandler:Landroid/os/Handler;

    .line 512
    return-void
.end method

.method setK2HD(Z)V
    .locals 7
    .param p1, "bIsOn"    # Z

    .prologue
    const/4 v6, 0x0

    const/4 v3, 0x1

    .line 1215
    sget-boolean v0, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_K2HD:Z

    if-eqz v0, :cond_0

    .line 1216
    const-string v0, "MusicPlayer"

    const-string v1, "setK2HD : Start"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1217
    iput-boolean p1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mFlagK2HD:Z

    .line 1219
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-nez v0, :cond_1

    .line 1220
    const-string v0, "MusicPlayer"

    const-string v1, "setK2HD : mMediaPlayer is null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1243
    :cond_0
    :goto_0
    return-void

    .line 1223
    :cond_1
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/MultiPlayer;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1224
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/MultiPlayer;->pause()V

    .line 1225
    iput-boolean v3, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mPauseByK2HD:Z

    .line 1227
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/MultiPlayer;->position()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mResumePosition:J

    .line 1228
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/MultiPlayer;->getSoundAlive()I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mSoundAlive:I

    .line 1229
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/MultiPlayer;->getSoundAliveUserEq()[I

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mUserEq:[I

    .line 1230
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/MultiPlayer;->getSoundAliveUserExtended()[I

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mUserEffect:[I

    .line 1231
    invoke-direct {p0, v6, v3}, Lcom/samsung/musicplus/service/MultiPlayer;->notifyOpenSessionForSoundAlive(ZZ)V

    .line 1233
    invoke-direct {p0}, Lcom/samsung/musicplus/service/MultiPlayer;->recreateMediaPlayer()V

    .line 1236
    iget v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mSoundAlive:I

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/service/MultiPlayer;->setSoundAlive(I)V

    .line 1237
    invoke-direct {p0, v3, v3}, Lcom/samsung/musicplus/service/MultiPlayer;->notifyOpenSessionForSoundAlive(ZZ)V

    .line 1239
    iget-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mUri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mPath:Ljava/lang/String;

    iget-boolean v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mPauseByK2HD:Z

    if-eqz v0, :cond_3

    :goto_1
    iget-wide v4, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mResumePosition:J

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/samsung/musicplus/service/MultiPlayer;->setDataSource(Landroid/net/Uri;Ljava/lang/String;ZJ)Z

    .line 1240
    iput-boolean v6, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mPauseByK2HD:Z

    .line 1241
    const-string v0, "MusicPlayer"

    const-string v1, "setK2HD : End"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    move v3, v6

    .line 1239
    goto :goto_1
.end method

.method setLimitVolume(FF)V
    .locals 2
    .param p1, "l"    # F
    .param p2, "r"    # F

    .prologue
    .line 1280
    iget v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mCurrentState:I

    const/4 v1, 0x4

    if-le v0, v1, :cond_0

    .line 1281
    iput p1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mCurLeftLimitVolume:F

    .line 1282
    iput p2, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mCurRightLimitVolume:F

    .line 1283
    iget v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mCurrentSetVolume:F

    iget v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mCurrentSetVolume:F

    invoke-direct {p0, v0, v1}, Lcom/samsung/musicplus/service/MultiPlayer;->setRealVolume(FF)V

    .line 1285
    :cond_0
    return-void
.end method

.method setListManager(Lcom/samsung/musicplus/service/PlayerListManager;)V
    .locals 0
    .param p1, "manager"    # Lcom/samsung/musicplus/service/PlayerListManager;

    .prologue
    .line 896
    iput-object p1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    .line 897
    return-void
.end method

.method setMute()V
    .locals 2

    .prologue
    .line 1290
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mDmrPlayer:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    if-nez v0, :cond_0

    .line 1291
    const-string v0, "MusicPlayer"

    const-string v1, "setMute() DMR player wasn\'t prepared"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1295
    :goto_0
    return-void

    .line 1294
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mDmrPlayer:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    invoke-virtual {v0}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->setMute()V

    goto :goto_0
.end method

.method setNewSoundAliveBandLevel([I)V
    .locals 5
    .param p1, "bandLevel"    # [I

    .prologue
    .line 2121
    if-eqz p1, :cond_0

    array-length v2, p1

    if-nez v2, :cond_2

    .line 2122
    :cond_0
    const-string v2, "MusicPlayer"

    const-string v3, "setNewSoundAliveBandLevel - bandLevel[] is null or size is 0"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2144
    :cond_1
    return-void

    .line 2126
    :cond_2
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mSquarePosition:[I

    .line 2127
    iput-object p1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mBandLevel:[I

    .line 2129
    iget v2, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMode:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 2130
    iget v2, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mCurrentState:I

    const/4 v3, 0x4

    if-le v2, v3, :cond_1

    .line 2131
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/4 v2, 0x7

    if-ge v1, v2, :cond_1

    .line 2132
    const-string v2, "MusicPlayer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setNewSoundAliveBandLevel bandNum : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Level : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget v4, p1, v1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2134
    iget-object v2, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mSoundAliveV2:Lcom/samsung/musicplus/library/audio/SoundAliveV2;

    if-eqz v2, :cond_3

    .line 2136
    :try_start_0
    iget-object v2, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mSoundAliveV2:Lcom/samsung/musicplus/library/audio/SoundAliveV2;

    int-to-short v3, v1

    aget v4, p1, v1

    int-to-short v4, v4

    invoke-virtual {v2, v3, v4}, Lcom/samsung/musicplus/library/audio/SoundAliveV2;->setBandLevel(SS)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2131
    :cond_3
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2137
    :catch_0
    move-exception v0

    .line 2138
    .local v0, "e":Ljava/lang/IllegalStateException;
    const-string v2, "MusicPlayer"

    const-string v3, "setNewSoundAliveBandLevel() - IllegalStateException"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method setNewSoundAliveEachStrength(II)V
    .locals 4
    .param p1, "effect"    # I
    .param p2, "mode"    # I

    .prologue
    const/4 v3, 0x1

    .line 2175
    iget v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMode:I

    if-ne v0, v3, :cond_2

    .line 2176
    iget v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mCurrentState:I

    const/4 v1, 0x4

    if-le v0, v1, :cond_2

    .line 2177
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mSoundAliveV2:Lcom/samsung/musicplus/library/audio/SoundAliveV2;

    if-eqz v0, :cond_0

    .line 2178
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mSoundAliveV2:Lcom/samsung/musicplus/library/audio/SoundAliveV2;

    int-to-short v1, p1

    int-to-short v2, p2

    invoke-virtual {v0, v1, v2}, Lcom/samsung/musicplus/library/audio/SoundAliveV2;->setStrength(SS)V

    .line 2181
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mStrengthEffect:[I

    if-nez v0, :cond_1

    .line 2182
    const/4 v0, 0x3

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mStrengthEffect:[I

    .line 2184
    :cond_1
    packed-switch p1, :pswitch_data_0

    .line 2199
    :cond_2
    :goto_0
    return-void

    .line 2186
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mStrengthEffect:[I

    const/4 v1, 0x0

    aput p2, v0, v1

    goto :goto_0

    .line 2189
    :pswitch_1
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mStrengthEffect:[I

    aput p2, v0, v3

    goto :goto_0

    .line 2192
    :pswitch_2
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mStrengthEffect:[I

    const/4 v1, 0x2

    aput p2, v0, v1

    goto :goto_0

    .line 2184
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method setNewSoundAliveSquarePosition([I)V
    .locals 11
    .param p1, "squarePosition"    # [I

    .prologue
    const/4 v10, 0x0

    const/4 v7, 0x5

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 2064
    if-nez p1, :cond_1

    .line 2065
    iput-object v10, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mSquarePosition:[I

    .line 2066
    const-string v5, "MusicPlayer"

    const-string v6, "setNewSoundAliveSquarePosition - squarePosition[] is null"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2100
    :cond_0
    :goto_0
    return-void

    .line 2070
    :cond_1
    const/4 v1, 0x0

    .line 2071
    .local v1, "ROW":I
    const/4 v0, 0x1

    .line 2072
    .local v0, "COLUMN":I
    const/4 v2, 0x5

    .line 2074
    .local v2, "SQUARE_COLUMN_COUNT":I
    iget-boolean v5, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mAuto:Z

    if-eqz v5, :cond_2

    .line 2075
    iget-object v5, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v5}, Lcom/samsung/musicplus/service/PlayerListManager;->getGenre()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/samsung/musicplus/library/audio/SoundAliveV2;->getSoundaliveAudioEffect(Ljava/lang/String;)I

    move-result v4

    .line 2077
    .local v4, "squareGenrePosition":I
    const/4 v5, 0x2

    new-array v5, v5, [I

    invoke-direct {p0, v4, v7}, Lcom/samsung/musicplus/service/MultiPlayer;->getSquareRow(II)I

    move-result v6

    aput v6, v5, v9

    invoke-direct {p0, v4, v7}, Lcom/samsung/musicplus/service/MultiPlayer;->getSquareColumn(II)I

    move-result v6

    aput v6, v5, v8

    iput-object v5, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mSquarePosition:[I

    .line 2084
    .end local v4    # "squareGenrePosition":I
    :goto_1
    iput-object v10, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mBandLevel:[I

    .line 2086
    iget v5, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMode:I

    if-ne v5, v8, :cond_0

    .line 2087
    iget v5, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mCurrentState:I

    const/4 v6, 0x4

    if-le v5, v6, :cond_0

    .line 2088
    const-string v5, "MusicPlayer"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "setNewSoundAliveSquarePosition ROW : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mSquarePosition:[I

    aget v7, v7, v9

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " COL : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mSquarePosition:[I

    aget v7, v7, v8

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2090
    iget-object v5, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mSoundAliveV2:Lcom/samsung/musicplus/library/audio/SoundAliveV2;

    if-eqz v5, :cond_0

    .line 2092
    :try_start_0
    iget-object v5, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mSoundAliveV2:Lcom/samsung/musicplus/library/audio/SoundAliveV2;

    iget-object v6, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mSquarePosition:[I

    const/4 v7, 0x0

    aget v6, v6, v7

    iget-object v7, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mSquarePosition:[I

    const/4 v8, 0x1

    aget v7, v7, v8

    invoke-virtual {v5, v6, v7}, Lcom/samsung/musicplus/library/audio/SoundAliveV2;->setSquarePostion(II)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2094
    :catch_0
    move-exception v3

    .line 2095
    .local v3, "e":Ljava/lang/IllegalStateException;
    const-string v5, "MusicPlayer"

    const-string v6, "setNewSoundAliveSquarePosition() - IllegalStateException"

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2082
    .end local v3    # "e":Ljava/lang/IllegalStateException;
    :cond_2
    iput-object p1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mSquarePosition:[I

    goto :goto_1
.end method

.method setNewSoundAliveStrength([I)V
    .locals 5
    .param p1, "mode"    # [I

    .prologue
    .line 2151
    if-eqz p1, :cond_0

    array-length v2, p1

    if-nez v2, :cond_2

    .line 2152
    :cond_0
    const-string v2, "MusicPlayer"

    const-string v3, "setNewSoundAliveStrength - mode[] is null or size is 0"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2172
    :cond_1
    return-void

    .line 2156
    :cond_2
    iput-object p1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mStrengthEffect:[I

    .line 2158
    iget v2, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMode:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 2159
    iget v2, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mCurrentState:I

    const/4 v3, 0x4

    if-le v2, v3, :cond_1

    .line 2160
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/4 v2, 0x3

    if-ge v1, v2, :cond_1

    .line 2161
    const-string v2, "MusicPlayer"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setNewSoundAliveStrength strengthNum : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " mode : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget v4, p1, v1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2162
    iget-object v2, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mSoundAliveV2:Lcom/samsung/musicplus/library/audio/SoundAliveV2;

    if-eqz v2, :cond_3

    .line 2164
    :try_start_0
    iget-object v2, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mSoundAliveV2:Lcom/samsung/musicplus/library/audio/SoundAliveV2;

    int-to-short v3, v1

    aget v4, p1, v1

    int-to-short v4, v4

    invoke-virtual {v2, v3, v4}, Lcom/samsung/musicplus/library/audio/SoundAliveV2;->setStrength(SS)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2160
    :cond_3
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2165
    :catch_0
    move-exception v0

    .line 2166
    .local v0, "e":Ljava/lang/IllegalStateException;
    const-string v2, "MusicPlayer"

    const-string v3, "setNewSoundAliveStrength() - IllegalStateException"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method setNewSoundAliveUsePreset(I)V
    .locals 4
    .param p1, "effect"    # I

    .prologue
    .line 2043
    iput p1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mPresetEffect:I

    .line 2045
    iget v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMode:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 2046
    iget v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mCurrentState:I

    const/4 v2, 0x4

    if-le v1, v2, :cond_0

    .line 2047
    const-string v1, "MusicPlayer"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setNewSoundAliveUsePreset mode : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2048
    iget-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mSoundAliveV2:Lcom/samsung/musicplus/library/audio/SoundAliveV2;

    if-eqz v1, :cond_0

    .line 2050
    :try_start_0
    iget-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mSoundAliveV2:Lcom/samsung/musicplus/library/audio/SoundAliveV2;

    int-to-short v2, p1

    invoke-virtual {v1, v2}, Lcom/samsung/musicplus/library/audio/SoundAliveV2;->usePreset(S)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 2057
    :cond_0
    :goto_0
    return-void

    .line 2051
    :catch_0
    move-exception v0

    .line 2052
    .local v0, "e":Ljava/lang/IllegalStateException;
    const-string v1, "MusicPlayer"

    const-string v2, "setNewSoundAliveUsePreset() - IllegalStateException"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method setOnPreparedListener(Lcom/samsung/musicplus/service/MultiPlayer$OnPreparedListener;)V
    .locals 0
    .param p1, "l"    # Lcom/samsung/musicplus/service/MultiPlayer$OnPreparedListener;

    .prologue
    .line 484
    iput-object p1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMultiPlayerPreparedListener:Lcom/samsung/musicplus/service/MultiPlayer$OnPreparedListener;

    .line 485
    return-void
.end method

.method setParamForK2HD()V
    .locals 5

    .prologue
    const/16 v4, 0x761

    const/16 v3, 0x760

    const/4 v2, 0x0

    .line 1249
    sget-boolean v0, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_K2HD:Z

    if-eqz v0, :cond_0

    .line 1250
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-nez v0, :cond_1

    .line 1251
    const-string v0, "MusicPlayer"

    const-string v1, "setParamForK2HD : mMediaPlayer is null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1277
    :cond_0
    :goto_0
    return-void

    .line 1255
    :cond_1
    iget-boolean v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mFlagK2HD:Z

    if-eqz v0, :cond_2

    .line 1256
    const-string v0, "MusicPlayer"

    const-string v1, "setParamForK2HD : KEY_PARAMETER_VIRTUAL_ULTRA_HIGH_QUALITY on"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1257
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    const-string v1, "1"

    invoke-virtual {v0, v3, v1}, Landroid/media/MediaPlayer;->setParameter(ILjava/lang/String;)Z

    .line 1265
    :goto_1
    iget-boolean v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mPauseByK2HD:Z

    if-eqz v0, :cond_0

    .line 1266
    iget-boolean v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mFlagK2HD:Z

    if-eqz v0, :cond_3

    .line 1267
    const-string v0, "MusicPlayer"

    const-string v1, "setParamForK2HD : KEY_PARAMETER_MUTE_CONTROL_K2HD on"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1268
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    const-string v1, "1"

    invoke-virtual {v0, v4, v1}, Landroid/media/MediaPlayer;->setParameter(ILjava/lang/String;)Z

    goto :goto_0

    .line 1261
    :cond_2
    const-string v0, "MusicPlayer"

    const-string v1, "setParamForK2HD : KEY_PARAMETER_VIRTUAL_ULTRA_HIGH_QUALITY off"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1262
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, v3, v2}, Landroid/media/MediaPlayer;->setParameter(II)Z

    goto :goto_1

    .line 1272
    :cond_3
    const-string v0, "MusicPlayer"

    const-string v1, "setParamForK2HD : KEY_PARAMETER_MUTE_CONTROL_K2HD off"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1273
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0, v4, v2}, Landroid/media/MediaPlayer;->setParameter(II)Z

    goto :goto_0
.end method

.method setPlaySpeed(F)V
    .locals 2
    .param p1, "speed"    # F

    .prologue
    .line 1415
    iput p1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mSpeed:F

    .line 1416
    iget v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMode:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1417
    iget v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mCurrentState:I

    const/4 v1, 0x4

    if-le v0, v1, :cond_0

    .line 1418
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    iget v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mSpeed:F

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/audio/SoundAlive;->setPlaySpeed(Landroid/media/MediaPlayer;F)V

    .line 1419
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mRestorePlaySpeed:Z

    .line 1422
    :cond_0
    return-void
.end method

.method setRadioPrepare(Z)V
    .locals 0
    .param p1, "prepare"    # Z

    .prologue
    .line 1666
    iput-boolean p1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mPrepareRadio:Z

    .line 1667
    return-void
.end method

.method setSoundAlive(I)V
    .locals 2
    .param p1, "effect"    # I

    .prologue
    .line 1348
    iput p1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mSoundAlive:I

    .line 1349
    iget v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMode:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mPauseByK2HD:Z

    if-eqz v0, :cond_1

    .line 1350
    :cond_0
    iget v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mCurrentState:I

    const/4 v1, 0x4

    if-le v0, v1, :cond_1

    .line 1351
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-static {v0, p1}, Lcom/samsung/musicplus/library/audio/SoundAlive;->setAudioEffect(Landroid/media/MediaPlayer;I)V

    .line 1352
    const/16 v0, 0xd

    if-ne p1, v0, :cond_1

    .line 1353
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mUserEq:[I

    iget-object v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mUserEffect:[I

    invoke-virtual {p0, v0, v1}, Lcom/samsung/musicplus/service/MultiPlayer;->setUserSoundAliveInternal([I[I)V

    .line 1357
    :cond_1
    return-void
.end method

.method setSoundAliveV2()V
    .locals 1

    .prologue
    .line 2036
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mSquarePosition:[I

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/service/MultiPlayer;->setNewSoundAliveSquarePosition([I)V

    .line 2037
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mStrengthEffect:[I

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/service/MultiPlayer;->setNewSoundAliveStrength([I)V

    .line 2038
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mBandLevel:[I

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/service/MultiPlayer;->setNewSoundAliveBandLevel([I)V

    .line 2039
    iget v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mPresetEffect:I

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/service/MultiPlayer;->setNewSoundAliveUsePreset(I)V

    .line 2040
    return-void
.end method

.method setSoundAliveV2InitValue(Z[I[I[II)V
    .locals 7
    .param p1, "auto"    # Z
    .param p2, "squarePosition"    # [I
    .param p3, "strength"    # [I
    .param p4, "bandLevel"    # [I
    .param p5, "preset"    # I

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 2226
    const/4 v2, 0x0

    .line 2227
    .local v2, "squarePositionByAuto":[I
    const/4 v0, -0x1

    .line 2229
    .local v0, "nsaSquarePositionNum":I
    if-eqz p2, :cond_0

    .line 2230
    aget v3, p2, v5

    mul-int/lit8 v3, v3, 0x5

    aget v4, p2, v6

    add-int v0, v3, v4

    .line 2233
    :cond_0
    if-eqz p1, :cond_1

    .line 2234
    invoke-static {}, Lcom/samsung/musicplus/util/ServiceUtils;->getGenre()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/musicplus/library/audio/SoundAliveV2;->getSoundaliveAudioEffect(Ljava/lang/String;)I

    move-result v1

    .line 2236
    .local v1, "squareGenrePosition":I
    const/4 v3, 0x2

    new-array v2, v3, [I

    .end local v2    # "squarePositionByAuto":[I
    div-int/lit8 v3, v1, 0x5

    aput v3, v2, v5

    rem-int/lit8 v3, v1, 0x5

    aput v3, v2, v6

    .line 2239
    .restart local v2    # "squarePositionByAuto":[I
    const/4 p3, 0x0

    .line 2240
    const/4 p4, 0x0

    .line 2241
    const/4 p2, 0x0

    .line 2242
    iput-boolean p1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mAuto:Z

    .line 2250
    .end local v1    # "squareGenrePosition":I
    :goto_0
    if-eqz v2, :cond_3

    .end local v2    # "squarePositionByAuto":[I
    :goto_1
    iput-object v2, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mSquarePosition:[I

    .line 2251
    iput-object p3, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mStrengthEffect:[I

    .line 2252
    iput-object p4, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mBandLevel:[I

    .line 2253
    iput p5, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mPresetEffect:I

    .line 2254
    return-void

    .line 2243
    .restart local v2    # "squarePositionByAuto":[I
    :cond_1
    if-gez v0, :cond_2

    .line 2244
    const/4 p2, 0x0

    goto :goto_0

    .line 2246
    :cond_2
    const/4 p3, 0x0

    .line 2247
    const/4 p4, 0x0

    goto :goto_0

    :cond_3
    move-object v2, p2

    .line 2250
    goto :goto_1
.end method

.method setUserSoundAlive([I[I)V
    .locals 2
    .param p1, "userEq"    # [I
    .param p2, "extEffect"    # [I

    .prologue
    const/16 v1, 0xd

    .line 1375
    iput v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mSoundAlive:I

    .line 1376
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/audio/SoundAlive;->setAudioEffect(Landroid/media/MediaPlayer;I)V

    .line 1377
    invoke-virtual {p0, p1, p2}, Lcom/samsung/musicplus/service/MultiPlayer;->setUserSoundAliveInternal([I[I)V

    .line 1378
    return-void
.end method

.method setUserSoundAliveInternal([I[I)V
    .locals 2
    .param p1, "userEq"    # [I
    .param p2, "effect"    # [I

    .prologue
    .line 1387
    iput-object p1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mUserEq:[I

    .line 1388
    iput-object p2, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mUserEffect:[I

    .line 1389
    iget v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMode:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 1390
    iget v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mCurrentState:I

    const/4 v1, 0x4

    if-le v0, v1, :cond_0

    .line 1391
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-static {v0, p1}, Lcom/samsung/musicplus/library/audio/SoundAlive;->setUserAudioEffect(Landroid/media/MediaPlayer;[I)V

    .line 1392
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-static {v0, p2}, Lcom/samsung/musicplus/library/audio/SoundAlive;->setUserExtendAudioEffect(Landroid/media/MediaPlayer;[I)V

    .line 1395
    :cond_0
    return-void
.end method

.method setVolume(F)V
    .locals 3
    .param p1, "vol"    # F

    .prologue
    .line 1193
    const-string v0, "MusicPlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setVolume() : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1194
    iget v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mCurrentState:I

    const/4 v1, 0x4

    if-le v0, v1, :cond_0

    .line 1195
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    .line 1196
    iput p1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mCurrentSetVolume:F

    .line 1197
    iget v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mCurrentSetVolume:F

    iget v1, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mCurrentSetVolume:F

    invoke-direct {p0, v0, v1}, Lcom/samsung/musicplus/service/MultiPlayer;->setRealVolume(FF)V

    .line 1200
    :cond_0
    return-void
.end method

.method start()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 917
    const-string v0, "MusicPlayer"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "start() mode : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMode:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 918
    iget v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMode:I

    packed-switch v0, :pswitch_data_0

    .line 959
    iput-boolean v3, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mIsSupposedToBePlaying:Z

    .line 960
    iget v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mCurrentState:I

    const/4 v1, 0x4

    if-le v0, v1, :cond_1

    .line 961
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_1

    .line 962
    iget-boolean v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mRestorePlaySpeed:Z

    if-eqz v0, :cond_0

    .line 963
    iget v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mSpeed:F

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/service/MultiPlayer;->setPlaySpeed(F)V

    .line 965
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v3, v0}, Lcom/samsung/musicplus/service/MultiPlayer;->notifyOpenSessionForSoundAlive(ZZ)V

    .line 966
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mMediaPlayer:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->start()V

    .line 971
    :cond_1
    :goto_0
    return-void

    .line 920
    :pswitch_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/MultiPlayer;->isReqestingDmrCommend()Z

    move-result v0

    if-nez v0, :cond_1

    .line 925
    invoke-direct {p0}, Lcom/samsung/musicplus/service/MultiPlayer;->startBuffering()V

    .line 926
    iput-boolean v3, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mIsPlayRequest:Z

    .line 928
    iget v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mCurrentState:I

    if-ne v0, v3, :cond_2

    .line 930
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/samsung/musicplus/service/MultiPlayer$11;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/service/MultiPlayer$11;-><init>(Lcom/samsung/musicplus/service/MultiPlayer;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 950
    :cond_2
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mDmrPlayer:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    if-eqz v0, :cond_1

    .line 951
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mDmrPlayer:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    invoke-virtual {v0}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->resume()V

    goto :goto_0

    .line 918
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method stop()V
    .locals 2

    .prologue
    .line 1014
    const-string v0, "MusicPlayer"

    const-string v1, "stop()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1015
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/MultiPlayer;->playerStop()V

    .line 1016
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mIsSupposedToBePlaying:Z

    .line 1017
    return-void
.end method

.method volumeDown()V
    .locals 2

    .prologue
    .line 1312
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mDmrPlayer:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    if-nez v0, :cond_0

    .line 1313
    const-string v0, "MusicPlayer"

    const-string v1, "volumeDown() DMR player wasn\'t prepared"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1317
    :goto_0
    return-void

    .line 1316
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mDmrPlayer:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    invoke-virtual {v0}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->volumeDown()V

    goto :goto_0
.end method

.method volumeUp()V
    .locals 2

    .prologue
    .line 1301
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mDmrPlayer:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    if-nez v0, :cond_0

    .line 1302
    const-string v0, "MusicPlayer"

    const-string v1, "volumeUp() DMR player wasn\'t prepared"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1306
    :goto_0
    return-void

    .line 1305
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/service/MultiPlayer;->mDmrPlayer:Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;

    invoke-virtual {v0}, Lcom/samsung/musicplus/library/dlna/SimpleAVPlayerManager;->volumeUp()V

    goto :goto_0
.end method
