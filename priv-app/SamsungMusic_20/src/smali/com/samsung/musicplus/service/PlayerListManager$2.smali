.class Lcom/samsung/musicplus/service/PlayerListManager$2;
.super Landroid/database/ContentObserver;
.source "PlayerListManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/service/PlayerListManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/service/PlayerListManager;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/service/PlayerListManager;Landroid/os/Handler;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Handler;

    .prologue
    .line 1563
    iput-object p1, p0, Lcom/samsung/musicplus/service/PlayerListManager$2;->this$0:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(ZLandroid/net/Uri;)V
    .locals 3
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    .line 1566
    invoke-super {p0, p1, p2}, Landroid/database/ContentObserver;->onChange(ZLandroid/net/Uri;)V

    .line 1568
    invoke-static {}, Lcom/samsung/musicplus/provider/MusicContents;->getMusicSquareCellUpdator()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1569
    const-string v1, "MusicSquareFragment"

    const-string v2, "MusicSquare square cell are relocated. so change list type"

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1571
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerListManager$2;->this$0:Lcom/samsung/musicplus/service/PlayerListManager;

    # invokes: Lcom/samsung/musicplus/service/PlayerListManager;->unRegisterMusicSquareUpdateObserver()V
    invoke-static {v1}, Lcom/samsung/musicplus/service/PlayerListManager;->access$100(Lcom/samsung/musicplus/service/PlayerListManager;)V

    .line 1572
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerListManager$2;->this$0:Lcom/samsung/musicplus/service/PlayerListManager;

    const v2, 0x2000a

    # invokes: Lcom/samsung/musicplus/service/PlayerListManager;->setListType(I)V
    invoke-static {v1, v2}, Lcom/samsung/musicplus/service/PlayerListManager;->access$200(Lcom/samsung/musicplus/service/PlayerListManager;I)V

    .line 1573
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.musicplus.action.QUEUE_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1574
    .local v0, "i":Landroid/content/Intent;
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerListManager$2;->this$0:Lcom/samsung/musicplus/service/PlayerListManager;

    # getter for: Lcom/samsung/musicplus/service/PlayerListManager;->mContext:Landroid/content/Context;
    invoke-static {v1}, Lcom/samsung/musicplus/service/PlayerListManager;->access$300(Lcom/samsung/musicplus/service/PlayerListManager;)Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1576
    .end local v0    # "i":Landroid/content/Intent;
    :cond_0
    return-void
.end method
