.class Lcom/samsung/musicplus/service/PlayerService$8;
.super Landroid/content/BroadcastReceiver;
.source "PlayerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/musicplus/service/PlayerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/musicplus/service/PlayerService;


# direct methods
.method constructor <init>(Lcom/samsung/musicplus/service/PlayerService;)V
    .locals 0

    .prologue
    .line 899
    iput-object p1, p0, Lcom/samsung/musicplus/service/PlayerService$8;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v7, -0x1

    .line 904
    const-string v4, "MusicPlayer"

    const-string v5, "from"

    invoke-virtual {p2, v5}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 933
    :cond_0
    :goto_0
    return-void

    .line 908
    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 909
    .local v0, "action":Ljava/lang/String;
    const-string v4, "MusicService"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "mSettingReceiver "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 910
    const-string v4, "com.android.music.settingchanged"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 912
    const-string v4, "repeat"

    invoke-virtual {p2, v4, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 913
    .local v2, "repeat":I
    if-eq v2, v7, :cond_2

    iget-object v4, p0, Lcom/samsung/musicplus/service/PlayerService$8;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v4}, Lcom/samsung/musicplus/service/PlayerService;->getRepeat()I

    move-result v4

    if-eq v4, v2, :cond_2

    .line 914
    iget-object v4, p0, Lcom/samsung/musicplus/service/PlayerService$8;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v4, v2}, Lcom/samsung/musicplus/service/PlayerService;->setRepeat(I)V

    .line 916
    :cond_2
    const-string v4, "shuffle"

    invoke-virtual {p2, v4, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 917
    .local v3, "shuffle":I
    if-eq v3, v7, :cond_0

    iget-object v4, p0, Lcom/samsung/musicplus/service/PlayerService$8;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v4}, Lcom/samsung/musicplus/service/PlayerService;->getShuffle()I

    move-result v4

    if-eq v4, v3, :cond_0

    .line 918
    iget-object v4, p0, Lcom/samsung/musicplus/service/PlayerService$8;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    invoke-virtual {v4, v3}, Lcom/samsung/musicplus/service/PlayerService;->setShuffle(I)V

    goto :goto_0

    .line 920
    .end local v2    # "repeat":I
    .end local v3    # "shuffle":I
    :cond_3
    const-string v4, "com.sec.hearingadjust.checkmusic"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 925
    invoke-static {p1}, Lcom/samsung/musicplus/library/audio/AdaptSound;->getAdaptSoundOn(Landroid/content/Context;)Z

    move-result v1

    .line 926
    .local v1, "isOn":Z
    iget-object v4, p0, Lcom/samsung/musicplus/service/PlayerService$8;->this$0:Lcom/samsung/musicplus/service/PlayerService;

    const/4 v5, 0x0

    invoke-virtual {v4, v1, v5}, Lcom/samsung/musicplus/service/PlayerService;->setAdaptSound(ZZ)V

    .line 927
    if-eqz v1, :cond_0

    .line 930
    const-string v4, "ADSD"

    invoke-static {p1, v4}, Lcom/samsung/musicplus/util/logging/FeatureLogger;->insertLog(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0
.end method
