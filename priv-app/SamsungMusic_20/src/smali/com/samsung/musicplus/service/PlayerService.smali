.class public Lcom/samsung/musicplus/service/PlayerService;
.super Landroid/app/Service;
.source "PlayerService.java"

# interfaces
.implements Lcom/samsung/musicplus/service/MultiPlayer$OnPreparedListener;
.implements Lcom/samsung/musicplus/service/PlayerServiceCommandAction;
.implements Lcom/samsung/musicplus/service/PlayerServiceStateAction;
.implements Lcom/samsung/musicplus/settings/PlayerSettingPreference;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/samsung/musicplus/service/PlayerService$ServiceStub;,
        Lcom/samsung/musicplus/service/PlayerService$ActivityManagerCompat;,
        Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;,
        Lcom/samsung/musicplus/service/PlayerService$ToastHandler;,
        Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;,
        Lcom/samsung/musicplus/service/PlayerService$MediaSessionCallback;
    }
.end annotation


# static fields
.field private static final AUDIO_PATH_CHANGED:I = 0x0

.field private static final AUDIO_PATH_CHANGED_BT:I = 0x1

.field private static final CHANGE_PLAYER:I = 0xa

.field private static final CHANGE_PLAYER_AUTO_PLAY:I = 0x64

.field private static final COMPONENT_NAME:Landroid/content/ComponentName;

.field private static final DRM_REQUEST:I = 0x4

.field private static final FADE_DOWN:I = 0xb

.field private static final FADE_UP:I = 0xc

.field private static final FOCUS_CHANGE:I = 0xd

.field private static final IDLE_DELAY:I = 0xea60

.field private static final IGNORE_BY_USER:Z = true

.field private static final INTERNAL_PLAYER_CHANGED:I = 0x7

.field private static final INTERNAL_PLAYER_STATE_CHANGED:I = 0x8

.field public static final KEY_PARAMETER_VIRTUAL_ULTRA_HIGH_QUALITY:I = 0x760

.field private static final MAX_VOLUME:F = 1.0f

.field private static final MEDIA_ERROR:I = 0x3

.field private static final NOTIFICATION_PERFORMANCE_LOG:Z = true

.field private static final NOTI_STATUS:I = 0x7010002

.field private static final NOTI_STATUS_FOR_NOX:I = 0x7010003

.field private static final PLAYER_BUFFERING_START:I = 0x5

.field private static final PLAYER_BUFFERING_STOP:I = 0x6

.field private static final PREV_MOVE:I = 0xbb8

.field private static final REFRESH_DEVICE:I = 0x1

.field public static final REPEAT_ALL:I = 0x2

.field public static final REPEAT_OFF:I = 0x0

.field public static final REPEAT_ONE:I = 0x1

.field private static final REPEAT_RATIO:I = 0x2

.field private static final RESET_IGNORE_NOISY:I = 0x11

.field private static final RESET_REPEAT_COUNT:I = 0xe

.field private static final SAVE_QUEUE:I = 0x10

.field private static final SEEK_COMPLETED:I = 0x9

.field private static final SEEK_READY:I = 0xf

.field private static final SELECT_DEVICE:I = 0x0

.field private static final SERVER_DIED:I = 0x2

.field public static final SHUFFLE_NONE:I = 0x0

.field public static final SHUFFLE_NORMAL:I = 0x1

.field private static final STOP_FF_REW_TASK:I = 0x12

.field private static final STOP_MSG_KILL_PROCESS:I = 0xb

.field private static final SUPPORT_DLNA:Z = true

.field private static final SUPPORT_QC_MEDIABUTTON_FUNCTION:Z = true

.field private static final SUPPORT_REMOTE_CONTROL_CLIENT:Z

.field private static final TAG:Ljava/lang/String; = "MusicService"

.field private static final TEST_FLAG_REWIND_WITH_DB_DURATION:Z = true

.field private static final TIME_SKIP_MIDDLE_INDEX:I = 0x2

.field private static final TIME_SKIP_VALUE:[I

.field private static final TRACK_ENDED:I = 0x1

.field private static final UNDEFINED:I = -0x1

.field private static sLastHookClickTime:J

.field private static sService:Lcom/samsung/musicplus/service/PlayerService;


# instance fields
.field private bErrorHandleToPrev:Z

.field private mAdaptSound:Lcom/samsung/musicplus/library/audio/AdaptSound;

.field private final mAlbumArtHandler:Landroid/os/Handler;

.field private mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field private mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

.field private final mAudioPathHandler:Landroid/os/Handler;

.field private mBaeVolume:I

.field private final mBinder:Landroid/os/IBinder;

.field private mBluetoothA2dp:Landroid/bluetooth/BluetoothA2dp;

.field private mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

.field private final mBluetoothServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

.field private mBounceEventListener:Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager$OnBounceListener;

.field private mBounceMotionManager:Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager;

.field private final mCommandReceiver:Landroid/content/BroadcastReceiver;

.field private mControlClient:Landroid/media/RemoteControlClient;

.field private mControlTask:Lcom/samsung/musicplus/widget/control/FowardRewindControlTaskMediaButton;

.field private mCurrentSongObserver:Landroid/database/ContentObserver;

.field private final mDelayedStopHandler:Landroid/os/Handler;

.field private final mDlnaHandler:Landroid/os/Handler;

.field private mDlnaManager:Lcom/samsung/musicplus/library/dlna/DlnaManager;

.field private mEnableSideSyncToast:Z

.field private mErrorCount:I

.field private mFirstLoader:Z

.field private mGestureManager:Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;

.field private final mHdmiReceiver:Landroid/content/BroadcastReceiver;

.field private final mHeadsetReceiver:Landroid/content/BroadcastReceiver;

.field private mIgnoreNoisy:Z

.field private mIgnoreVolumeCount:I

.field private mIsActiveSmartVolume:Z

.field private mIsAdaptSoundOn:Z

.field private mIsConnectingWfd:Z

.field private mIsGestureRegistered:Z

.field private mIsHdmiConnected:Z

.field private mIsNoxMode:Ljava/lang/Boolean;

.field private mIsRegistered:Z

.field private mIsSeekReady:Z

.field private mIsShowNotification:Z

.field private mIsStopPacakgeAfterDestroyed:Z

.field private final mListLoader:Landroid/os/Handler;

.field private mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

.field private mMaxVolume:F

.field private mMediaSession:Landroid/media/session/MediaSession;

.field private mMediaStateBuilder:Landroid/media/session/PlaybackState$Builder;

.field private mMotionManager:Lcom/samsung/musicplus/library/hardware/SimpleMotionManager;

.field private mNotiManager:Landroid/app/NotificationManager;

.field private mNotification:Landroid/app/Notification;

.field private mNotifyHandler:Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;

.field private mOpenUriFailed:I

.field private mOtherDeviceReceiver:Landroid/content/BroadcastReceiver;

.field private final mPackageStopHandler:Landroid/os/Handler;

.field private mPausedByTransientLossOfFocus:Z

.field private mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

.field private final mPlayerHandler:Landroid/os/Handler;

.field private mPreferences:Landroid/content/SharedPreferences;

.field private mRemoteBigViewBuilder:Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;

.field private mRemoteViewBuilder:Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;

.field private mRepeatCount:I

.field private mServiceAppWidggetManger:Lcom/samsung/musicplus/widget/appwidget/ServiceAppWidgetManager;

.field private mServiceInUse:Z

.field private mServiceStartId:I

.field private final mSettingReceiver:Landroid/content/BroadcastReceiver;

.field private mSmartVolume:Lcom/samsung/musicplus/library/audio/SmartVolumeManager;

.field private final mSviewCoverReceiver:Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;

.field private final mSystemReceiver:Landroid/content/BroadcastReceiver;

.field private mToast:Landroid/widget/Toast;

.field private mToastHandler:Lcom/samsung/musicplus/service/PlayerService$ToastHandler;

.field private final mVolumeReceiver:Landroid/content/BroadcastReceiver;

.field private mWarnAdaptSound:Z

.field private final mWifiReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 148
    new-instance v0, Landroid/content/ComponentName;

    const-string v1, "com.sec.android.app.music"

    const-class v2, Lcom/samsung/musicplus/service/MediaButtonReceiver;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/samsung/musicplus/service/PlayerService;->COMPONENT_NAME:Landroid/content/ComponentName;

    .line 205
    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/samsung/musicplus/service/PlayerService;->TIME_SKIP_VALUE:[I

    .line 264
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/samsung/musicplus/service/PlayerService;->SUPPORT_REMOTE_CONTROL_CLIENT:Z

    .line 1531
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/samsung/musicplus/service/PlayerService;->sLastHookClickTime:J

    return-void

    .line 264
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 205
    nop

    :array_0
    .array-data 4
        0x7d0
        0xbb8
        0xfa0
        0x1f40
        0x3e80
    .end array-data
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 144
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 190
    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mServiceStartId:I

    .line 221
    iput v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mRepeatCount:I

    .line 227
    invoke-static {}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;->getInstance()Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mSviewCoverReceiver:Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;

    .line 230
    iput-boolean v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mPausedByTransientLossOfFocus:Z

    .line 237
    new-instance v0, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/service/PlayerService$ServiceStub;-><init>(Lcom/samsung/musicplus/service/PlayerService;)V

    iput-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mBinder:Landroid/os/IBinder;

    .line 243
    iput-boolean v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mIsShowNotification:Z

    .line 276
    iput-boolean v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mServiceInUse:Z

    .line 293
    iput-boolean v2, p0, Lcom/samsung/musicplus/service/PlayerService;->mEnableSideSyncToast:Z

    .line 296
    iput v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mErrorCount:I

    .line 299
    iput v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mOpenUriFailed:I

    .line 305
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mMaxVolume:F

    .line 362
    iput-boolean v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mIsConnectingWfd:Z

    .line 364
    iput-boolean v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mIsHdmiConnected:Z

    .line 373
    iput-boolean v2, p0, Lcom/samsung/musicplus/service/PlayerService;->mIsSeekReady:Z

    .line 375
    new-instance v0, Lcom/samsung/musicplus/service/PlayerService$1;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/service/PlayerService$1;-><init>(Lcom/samsung/musicplus/service/PlayerService;)V

    iput-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayerHandler:Landroid/os/Handler;

    .line 650
    new-instance v0, Lcom/samsung/musicplus/service/PlayerService$2;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/service/PlayerService$2;-><init>(Lcom/samsung/musicplus/service/PlayerService;)V

    iput-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 657
    new-instance v0, Lcom/samsung/musicplus/service/PlayerService$3;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/service/PlayerService$3;-><init>(Lcom/samsung/musicplus/service/PlayerService;)V

    iput-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mAlbumArtHandler:Landroid/os/Handler;

    .line 674
    iput-boolean v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mIsStopPacakgeAfterDestroyed:Z

    .line 676
    new-instance v0, Lcom/samsung/musicplus/service/PlayerService$4;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/service/PlayerService$4;-><init>(Lcom/samsung/musicplus/service/PlayerService;)V

    iput-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mDelayedStopHandler:Landroid/os/Handler;

    .line 697
    new-instance v0, Lcom/samsung/musicplus/service/PlayerService$5;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/service/PlayerService$5;-><init>(Lcom/samsung/musicplus/service/PlayerService;)V

    iput-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mCommandReceiver:Landroid/content/BroadcastReceiver;

    .line 704
    new-instance v0, Lcom/samsung/musicplus/service/PlayerService$6;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/service/PlayerService$6;-><init>(Lcom/samsung/musicplus/service/PlayerService;)V

    iput-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mAudioPathHandler:Landroid/os/Handler;

    .line 721
    new-instance v0, Lcom/samsung/musicplus/service/PlayerService$7;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/service/PlayerService$7;-><init>(Lcom/samsung/musicplus/service/PlayerService;)V

    iput-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mHeadsetReceiver:Landroid/content/BroadcastReceiver;

    .line 899
    new-instance v0, Lcom/samsung/musicplus/service/PlayerService$8;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/service/PlayerService$8;-><init>(Lcom/samsung/musicplus/service/PlayerService;)V

    iput-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mSettingReceiver:Landroid/content/BroadcastReceiver;

    .line 968
    iput-boolean v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mWarnAdaptSound:Z

    .line 970
    iput-boolean v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mIsAdaptSoundOn:Z

    .line 1046
    iput v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mIgnoreVolumeCount:I

    .line 1048
    new-instance v0, Lcom/samsung/musicplus/service/PlayerService$9;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/service/PlayerService$9;-><init>(Lcom/samsung/musicplus/service/PlayerService;)V

    iput-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mVolumeReceiver:Landroid/content/BroadcastReceiver;

    .line 1096
    iput-boolean v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mIsActiveSmartVolume:Z

    .line 1207
    new-instance v0, Lcom/samsung/musicplus/service/PlayerService$10;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/service/PlayerService$10;-><init>(Lcom/samsung/musicplus/service/PlayerService;)V

    iput-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mSystemReceiver:Landroid/content/BroadcastReceiver;

    .line 1834
    iput-boolean v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mFirstLoader:Z

    .line 1837
    new-instance v0, Lcom/samsung/musicplus/service/PlayerService$15;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/service/PlayerService$15;-><init>(Lcom/samsung/musicplus/service/PlayerService;)V

    iput-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mListLoader:Landroid/os/Handler;

    .line 2170
    new-instance v0, Lcom/samsung/musicplus/service/PlayerService$16;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/service/PlayerService$16;-><init>(Lcom/samsung/musicplus/service/PlayerService;)V

    iput-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mDlnaHandler:Landroid/os/Handler;

    .line 2194
    new-instance v0, Lcom/samsung/musicplus/service/PlayerService$17;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/service/PlayerService$17;-><init>(Lcom/samsung/musicplus/service/PlayerService;)V

    iput-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mHdmiReceiver:Landroid/content/BroadcastReceiver;

    .line 2234
    new-instance v0, Lcom/samsung/musicplus/service/PlayerService$18;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/service/PlayerService$18;-><init>(Lcom/samsung/musicplus/service/PlayerService;)V

    iput-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mWifiReceiver:Landroid/content/BroadcastReceiver;

    .line 2355
    new-instance v0, Lcom/samsung/musicplus/service/PlayerService$19;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/service/PlayerService$19;-><init>(Lcom/samsung/musicplus/service/PlayerService;)V

    iput-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mOtherDeviceReceiver:Landroid/content/BroadcastReceiver;

    .line 3257
    new-instance v0, Lcom/samsung/musicplus/service/PlayerService$22;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/service/PlayerService$22;-><init>(Lcom/samsung/musicplus/service/PlayerService;)V

    iput-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mBluetoothServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    .line 3490
    iput-boolean v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mIsGestureRegistered:Z

    .line 3616
    new-instance v0, Lcom/samsung/musicplus/service/PlayerService$24;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/service/PlayerService$24;-><init>(Lcom/samsung/musicplus/service/PlayerService;)V

    iput-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mBounceEventListener:Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager$OnBounceListener;

    .line 3996
    iput-boolean v1, p0, Lcom/samsung/musicplus/service/PlayerService;->bErrorHandleToPrev:Z

    .line 4308
    iput-boolean v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mIgnoreNoisy:Z

    .line 4405
    new-instance v0, Lcom/samsung/musicplus/service/PlayerService$26;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/service/PlayerService$26;-><init>(Lcom/samsung/musicplus/service/PlayerService;)V

    iput-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mPackageStopHandler:Landroid/os/Handler;

    .line 5287
    iput-boolean v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mIsRegistered:Z

    .line 5304
    new-instance v0, Lcom/samsung/musicplus/service/PlayerService$27;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/samsung/musicplus/service/PlayerService$27;-><init>(Lcom/samsung/musicplus/service/PlayerService;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mCurrentSongObserver:Landroid/database/ContentObserver;

    .line 5318
    new-instance v0, Lcom/samsung/musicplus/service/PlayerService$ToastHandler;

    invoke-direct {v0, p0}, Lcom/samsung/musicplus/service/PlayerService$ToastHandler;-><init>(Lcom/samsung/musicplus/service/PlayerService;)V

    iput-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mToastHandler:Lcom/samsung/musicplus/service/PlayerService$ToastHandler;

    .line 5551
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/musicplus/service/PlayerService;)F
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;

    .prologue
    .line 144
    iget v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mMaxVolume:F

    return v0
.end method

.method static synthetic access$002(Lcom/samsung/musicplus/service/PlayerService;F)F
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;
    .param p1, "x1"    # F

    .prologue
    .line 144
    iput p1, p0, Lcom/samsung/musicplus/service/PlayerService;->mMaxVolume:F

    return p1
.end method

.method static synthetic access$100(Lcom/samsung/musicplus/service/PlayerService;)Lcom/samsung/musicplus/service/PlayerListManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/samsung/musicplus/service/PlayerService;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;
    .param p1, "x1"    # Z

    .prologue
    .line 144
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/service/PlayerService;->errorHandleToPrevNext(Z)V

    return-void
.end method

.method static synthetic access$1100(Lcom/samsung/musicplus/service/PlayerService;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;

    .prologue
    .line 144
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->updateNotificationConnectivityStatus()V

    return-void
.end method

.method static synthetic access$1200(Lcom/samsung/musicplus/service/PlayerService;)Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mNotifyHandler:Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/samsung/musicplus/service/PlayerService;ZZJF)V
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;
    .param p1, "x1"    # Z
    .param p2, "x2"    # Z
    .param p3, "x3"    # J
    .param p5, "x4"    # F

    .prologue
    .line 144
    invoke-direct/range {p0 .. p5}, Lcom/samsung/musicplus/service/PlayerService;->setPlaybackState(ZZJF)V

    return-void
.end method

.method static synthetic access$1400(Lcom/samsung/musicplus/service/PlayerService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;

    .prologue
    .line 144
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->isDmrPlaying()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1500(Lcom/samsung/musicplus/service/PlayerService;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;
    .param p1, "x1"    # I

    .prologue
    .line 144
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/service/PlayerService;->showToast(I)V

    return-void
.end method

.method static synthetic access$1602(Lcom/samsung/musicplus/service/PlayerService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;
    .param p1, "x1"    # I

    .prologue
    .line 144
    iput p1, p0, Lcom/samsung/musicplus/service/PlayerService;->mRepeatCount:I

    return p1
.end method

.method static synthetic access$1702(Lcom/samsung/musicplus/service/PlayerService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;
    .param p1, "x1"    # Z

    .prologue
    .line 144
    iput-boolean p1, p0, Lcom/samsung/musicplus/service/PlayerService;->mIgnoreNoisy:Z

    return p1
.end method

.method static synthetic access$1800(Lcom/samsung/musicplus/service/PlayerService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;

    .prologue
    .line 144
    iget-boolean v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mPausedByTransientLossOfFocus:Z

    return v0
.end method

.method static synthetic access$1802(Lcom/samsung/musicplus/service/PlayerService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;
    .param p1, "x1"    # Z

    .prologue
    .line 144
    iput-boolean p1, p0, Lcom/samsung/musicplus/service/PlayerService;->mPausedByTransientLossOfFocus:Z

    return p1
.end method

.method static synthetic access$1900(Lcom/samsung/musicplus/service/PlayerService;Landroid/graphics/Bitmap;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;
    .param p1, "x1"    # Landroid/graphics/Bitmap;

    .prologue
    .line 144
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/service/PlayerService;->updateNotificationAlbumArt(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/musicplus/service/PlayerService;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;

    .prologue
    .line 144
    iget v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mErrorCount:I

    return v0
.end method

.method static synthetic access$2000(Lcom/samsung/musicplus/service/PlayerService;)Landroid/media/RemoteControlClient;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mControlClient:Landroid/media/RemoteControlClient;

    return-object v0
.end method

.method static synthetic access$202(Lcom/samsung/musicplus/service/PlayerService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;
    .param p1, "x1"    # I

    .prologue
    .line 144
    iput p1, p0, Lcom/samsung/musicplus/service/PlayerService;->mErrorCount:I

    return p1
.end method

.method static synthetic access$208(Lcom/samsung/musicplus/service/PlayerService;)I
    .locals 2
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;

    .prologue
    .line 144
    iget v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mErrorCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mErrorCount:I

    return v0
.end method

.method static synthetic access$2100(Lcom/samsung/musicplus/service/PlayerService;Landroid/graphics/Bitmap;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;
    .param p1, "x1"    # Landroid/graphics/Bitmap;

    .prologue
    .line 144
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/service/PlayerService;->updateRemoteClient(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method static synthetic access$2200(Lcom/samsung/musicplus/service/PlayerService;Landroid/graphics/Bitmap;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;
    .param p1, "x1"    # Landroid/graphics/Bitmap;

    .prologue
    .line 144
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/service/PlayerService;->updateMediaSessionMeta(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method static synthetic access$2300(Lcom/samsung/musicplus/service/PlayerService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;

    .prologue
    .line 144
    iget-boolean v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mServiceInUse:Z

    return v0
.end method

.method static synthetic access$2400(Lcom/samsung/musicplus/service/PlayerService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;

    .prologue
    .line 144
    iget-boolean v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mIsShowNotification:Z

    return v0
.end method

.method static synthetic access$2500(Lcom/samsung/musicplus/service/PlayerService;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;

    .prologue
    .line 144
    iget v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mServiceStartId:I

    return v0
.end method

.method static synthetic access$2602(Lcom/samsung/musicplus/service/PlayerService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;
    .param p1, "x1"    # Z

    .prologue
    .line 144
    iput-boolean p1, p0, Lcom/samsung/musicplus/service/PlayerService;->mIsStopPacakgeAfterDestroyed:Z

    return p1
.end method

.method static synthetic access$2700(Lcom/samsung/musicplus/service/PlayerService;Landroid/content/Intent;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;
    .param p1, "x1"    # Landroid/content/Intent;

    .prologue
    .line 144
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/service/PlayerService;->handleCommandIntent(Landroid/content/Intent;)V

    return-void
.end method

.method static synthetic access$2800(Lcom/samsung/musicplus/service/PlayerService;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;

    .prologue
    .line 144
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->getWifiDisplayAction()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2900(Lcom/samsung/musicplus/service/PlayerService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;

    .prologue
    .line 144
    iget-boolean v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mIsConnectingWfd:Z

    return v0
.end method

.method static synthetic access$2902(Lcom/samsung/musicplus/service/PlayerService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;
    .param p1, "x1"    # Z

    .prologue
    .line 144
    iput-boolean p1, p0, Lcom/samsung/musicplus/service/PlayerService;->mIsConnectingWfd:Z

    return p1
.end method

.method static synthetic access$300(Lcom/samsung/musicplus/service/PlayerService;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;
    .param p1, "x1"    # Z

    .prologue
    .line 144
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/service/PlayerService;->openInternal(Z)V

    return-void
.end method

.method static synthetic access$3000(Lcom/samsung/musicplus/service/PlayerService;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;

    .prologue
    .line 144
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->resetSoundAlive()V

    return-void
.end method

.method static synthetic access$3100(Lcom/samsung/musicplus/service/PlayerService;)Landroid/content/SharedPreferences;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mPreferences:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic access$3200(Lcom/samsung/musicplus/service/PlayerService;ZI[I[I[IZ)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;
    .param p1, "x1"    # Z
    .param p2, "x2"    # I
    .param p3, "x3"    # [I
    .param p4, "x4"    # [I
    .param p5, "x5"    # [I
    .param p6, "x6"    # Z

    .prologue
    .line 144
    invoke-direct/range {p0 .. p6}, Lcom/samsung/musicplus/service/PlayerService;->setSoundAliveV2(ZI[I[I[IZ)V

    return-void
.end method

.method static synthetic access$3300(Lcom/samsung/musicplus/service/PlayerService;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mAudioPathHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$3400(Lcom/samsung/musicplus/service/PlayerService;)Lcom/samsung/musicplus/library/audio/SecAudioManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

    return-object v0
.end method

.method static synthetic access$3500(Lcom/samsung/musicplus/service/PlayerService;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;
    .param p1, "x1"    # Z

    .prologue
    .line 144
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/service/PlayerService;->updateAdaptSound(Z)V

    return-void
.end method

.method static synthetic access$3600(Lcom/samsung/musicplus/service/PlayerService;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;

    .prologue
    .line 144
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->warningAdaptSound()V

    return-void
.end method

.method static synthetic access$3700(Lcom/samsung/musicplus/service/PlayerService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;

    .prologue
    .line 144
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->isMidi()Z

    move-result v0

    return v0
.end method

.method static synthetic access$3800(Lcom/samsung/musicplus/service/PlayerService;Z)F
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;
    .param p1, "x1"    # Z

    .prologue
    .line 144
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/service/PlayerService;->getMidiVolumeRatio(Z)F

    move-result v0

    return v0
.end method

.method static synthetic access$3900(Lcom/samsung/musicplus/service/PlayerService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;

    .prologue
    .line 144
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->isEnableAdaptSoundPath()Z

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/samsung/musicplus/service/PlayerService;)Lcom/samsung/musicplus/service/MultiPlayer;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

    return-object v0
.end method

.method static synthetic access$4000(Lcom/samsung/musicplus/service/PlayerService;)Lcom/samsung/musicplus/library/audio/SmartVolumeManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mSmartVolume:Lcom/samsung/musicplus/library/audio/SmartVolumeManager;

    return-object v0
.end method

.method static synthetic access$4100(Lcom/samsung/musicplus/service/PlayerService;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;

    .prologue
    .line 144
    iget v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mIgnoreVolumeCount:I

    return v0
.end method

.method static synthetic access$4102(Lcom/samsung/musicplus/service/PlayerService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;
    .param p1, "x1"    # I

    .prologue
    .line 144
    iput p1, p0, Lcom/samsung/musicplus/service/PlayerService;->mIgnoreVolumeCount:I

    return p1
.end method

.method static synthetic access$4110(Lcom/samsung/musicplus/service/PlayerService;)I
    .locals 2
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;

    .prologue
    .line 144
    iget v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mIgnoreVolumeCount:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mIgnoreVolumeCount:I

    return v0
.end method

.method static synthetic access$4202(Lcom/samsung/musicplus/service/PlayerService;I)I
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;
    .param p1, "x1"    # I

    .prologue
    .line 144
    iput p1, p0, Lcom/samsung/musicplus/service/PlayerService;->mBaeVolume:I

    return p1
.end method

.method static synthetic access$4300(Lcom/samsung/musicplus/service/PlayerService;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;

    .prologue
    .line 144
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->updateNotificationAllInfo()V

    return-void
.end method

.method static synthetic access$4400(Lcom/samsung/musicplus/service/PlayerService;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;

    .prologue
    .line 144
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->stop()V

    return-void
.end method

.method static synthetic access$4500(Lcom/samsung/musicplus/service/PlayerService;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;
    .param p1, "x1"    # I

    .prologue
    .line 144
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/service/PlayerService;->simpleWarningNoti(I)V

    return-void
.end method

.method static synthetic access$4600(Lcom/samsung/musicplus/service/PlayerService;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 144
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/service/PlayerService;->unregisterGestureListener(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$4700(Lcom/samsung/musicplus/service/PlayerService;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 144
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/service/PlayerService;->registerGestureListener(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$4800(Lcom/samsung/musicplus/service/PlayerService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;

    .prologue
    .line 144
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->isSlinkTrackList()Z

    move-result v0

    return v0
.end method

.method static synthetic access$4900(Lcom/samsung/musicplus/service/PlayerService;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;

    .prologue
    .line 144
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->resetNowPlayingList()V

    return-void
.end method

.method static synthetic access$5000(Lcom/samsung/musicplus/service/PlayerService;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;
    .param p1, "x1"    # Z

    .prologue
    .line 144
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/service/PlayerService;->updateNotificationTextMarquee(Z)V

    return-void
.end method

.method static synthetic access$502(Lcom/samsung/musicplus/service/PlayerService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;
    .param p1, "x1"    # Z

    .prologue
    .line 144
    iput-boolean p1, p0, Lcom/samsung/musicplus/service/PlayerService;->mIsSeekReady:Z

    return p1
.end method

.method static synthetic access$5100(Lcom/samsung/musicplus/service/PlayerService;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 144
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/service/PlayerService;->notifyChange(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$5200(Lcom/samsung/musicplus/service/PlayerService;J)V
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;
    .param p1, "x1"    # J

    .prologue
    .line 144
    invoke-direct {p0, p1, p2}, Lcom/samsung/musicplus/service/PlayerService;->rewind(J)V

    return-void
.end method

.method static synthetic access$5300(Lcom/samsung/musicplus/service/PlayerService;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;

    .prologue
    .line 144
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->notifySettingToRemoteClient()V

    return-void
.end method

.method static synthetic access$5400(Lcom/samsung/musicplus/service/PlayerService;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;

    .prologue
    .line 144
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->forward()V

    return-void
.end method

.method static synthetic access$5500(Lcom/samsung/musicplus/service/PlayerService;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;

    .prologue
    .line 144
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->rewind()V

    return-void
.end method

.method static synthetic access$5600(Lcom/samsung/musicplus/service/PlayerService;)Landroid/media/session/MediaSession;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mMediaSession:Landroid/media/session/MediaSession;

    return-object v0
.end method

.method static synthetic access$5700()J
    .locals 2

    .prologue
    .line 144
    sget-wide v0, Lcom/samsung/musicplus/service/PlayerService;->sLastHookClickTime:J

    return-wide v0
.end method

.method static synthetic access$5702(J)J
    .locals 0
    .param p0, "x0"    # J

    .prologue
    .line 144
    sput-wide p0, Lcom/samsung/musicplus/service/PlayerService;->sLastHookClickTime:J

    return-wide p0
.end method

.method static synthetic access$5800(Lcom/samsung/musicplus/service/PlayerService;II)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 144
    invoke-direct {p0, p1, p2}, Lcom/samsung/musicplus/service/PlayerService;->setShuffleAndRepeat(II)V

    return-void
.end method

.method static synthetic access$600(Lcom/samsung/musicplus/service/PlayerService;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayerHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$6000(Lcom/samsung/musicplus/service/PlayerService;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;

    .prologue
    .line 144
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->reloadQueue()V

    return-void
.end method

.method static synthetic access$6102(Lcom/samsung/musicplus/service/PlayerService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;
    .param p1, "x1"    # Z

    .prologue
    .line 144
    iput-boolean p1, p0, Lcom/samsung/musicplus/service/PlayerService;->mFirstLoader:Z

    return p1
.end method

.method static synthetic access$6200(Lcom/samsung/musicplus/service/PlayerService;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;
    .param p1, "x1"    # Z

    .prologue
    .line 144
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/service/PlayerService;->saveQueue(Z)V

    return-void
.end method

.method static synthetic access$6300(Lcom/samsung/musicplus/service/PlayerService;)Lcom/samsung/musicplus/library/dlna/DlnaManager;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mDlnaManager:Lcom/samsung/musicplus/library/dlna/DlnaManager;

    return-object v0
.end method

.method static synthetic access$6402(Lcom/samsung/musicplus/service/PlayerService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;
    .param p1, "x1"    # Z

    .prologue
    .line 144
    iput-boolean p1, p0, Lcom/samsung/musicplus/service/PlayerService;->mIsHdmiConnected:Z

    return p1
.end method

.method static synthetic access$6502(Lcom/samsung/musicplus/service/PlayerService;Landroid/bluetooth/BluetoothA2dp;)Landroid/bluetooth/BluetoothA2dp;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;
    .param p1, "x1"    # Landroid/bluetooth/BluetoothA2dp;

    .prologue
    .line 144
    iput-object p1, p0, Lcom/samsung/musicplus/service/PlayerService;->mBluetoothA2dp:Landroid/bluetooth/BluetoothA2dp;

    return-object p1
.end method

.method static synthetic access$6600(Lcom/samsung/musicplus/service/PlayerService;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;
    .param p1, "x1"    # Z

    .prologue
    .line 144
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/service/PlayerService;->handleBounceSetting(Z)V

    return-void
.end method

.method static synthetic access$6700(Lcom/samsung/musicplus/service/PlayerService;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;

    .prologue
    .line 144
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->setAgingTimer()V

    return-void
.end method

.method static synthetic access$6800(Lcom/samsung/musicplus/service/PlayerService;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;

    .prologue
    .line 144
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->updateNotificationPlayStatus()V

    return-void
.end method

.method static synthetic access$6900(Lcom/samsung/musicplus/service/PlayerService;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;
    .param p1, "x1"    # Z

    .prologue
    .line 144
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/service/PlayerService;->notifyMusicInfo(Z)V

    return-void
.end method

.method static synthetic access$700(Lcom/samsung/musicplus/service/PlayerService;)Lcom/samsung/musicplus/widget/control/FowardRewindControlTaskMediaButton;
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;

    .prologue
    .line 144
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mControlTask:Lcom/samsung/musicplus/widget/control/FowardRewindControlTaskMediaButton;

    return-object v0
.end method

.method static synthetic access$7000(Lcom/samsung/musicplus/service/PlayerService;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;

    .prologue
    .line 144
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->handleMetaEdited()V

    return-void
.end method

.method static synthetic access$702(Lcom/samsung/musicplus/service/PlayerService;Lcom/samsung/musicplus/widget/control/FowardRewindControlTaskMediaButton;)Lcom/samsung/musicplus/widget/control/FowardRewindControlTaskMediaButton;
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;
    .param p1, "x1"    # Lcom/samsung/musicplus/widget/control/FowardRewindControlTaskMediaButton;

    .prologue
    .line 144
    iput-object p1, p0, Lcom/samsung/musicplus/service/PlayerService;->mControlTask:Lcom/samsung/musicplus/widget/control/FowardRewindControlTaskMediaButton;

    return-object p1
.end method

.method static synthetic access$7100(Lcom/samsung/musicplus/service/PlayerService;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;
    .param p1, "x1"    # I

    .prologue
    .line 144
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/service/PlayerService;->setNewSoundAliveUsePreset(I)V

    return-void
.end method

.method static synthetic access$7200(Lcom/samsung/musicplus/service/PlayerService;[I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;
    .param p1, "x1"    # [I

    .prologue
    .line 144
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/service/PlayerService;->setNewSoundAliveSquarePosition([I)V

    return-void
.end method

.method static synthetic access$7300(Lcom/samsung/musicplus/service/PlayerService;[I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;
    .param p1, "x1"    # [I

    .prologue
    .line 144
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/service/PlayerService;->setNewSoundAliveBandLevel([I)V

    return-void
.end method

.method static synthetic access$7400(Lcom/samsung/musicplus/service/PlayerService;[I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;
    .param p1, "x1"    # [I

    .prologue
    .line 144
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/service/PlayerService;->setNewSoundAliveStrength([I)V

    return-void
.end method

.method static synthetic access$7500(Lcom/samsung/musicplus/service/PlayerService;II)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;
    .param p1, "x1"    # I
    .param p2, "x2"    # I

    .prologue
    .line 144
    invoke-direct {p0, p1, p2}, Lcom/samsung/musicplus/service/PlayerService;->setNewSoundAliveEachStrength(II)V

    return-void
.end method

.method static synthetic access$7600(Lcom/samsung/musicplus/service/PlayerService;)I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;

    .prologue
    .line 144
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->loadNewSoundAliveUsePreset()I

    move-result v0

    return v0
.end method

.method static synthetic access$7700(Lcom/samsung/musicplus/service/PlayerService;)[I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;

    .prologue
    .line 144
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->loadNewSoundAliveSquarePosition()[I

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$7800(Lcom/samsung/musicplus/service/PlayerService;)[I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;

    .prologue
    .line 144
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->loadNewSoundAliveBandLevel()[I

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$7900(Lcom/samsung/musicplus/service/PlayerService;)[I
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;

    .prologue
    .line 144
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->loadNewSoundAliveStrength()[I

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$800(Lcom/samsung/musicplus/service/PlayerService;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;
    .param p1, "x1"    # Z

    .prologue
    .line 144
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/service/PlayerService;->sendPlayerBuffering(Z)V

    return-void
.end method

.method static synthetic access$8000(Lcom/samsung/musicplus/service/PlayerService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;

    .prologue
    .line 144
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->loadNewSoundAliveAuto()Z

    move-result v0

    return v0
.end method

.method static synthetic access$8100(Lcom/samsung/musicplus/service/PlayerService;Ljava/lang/String;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # I

    .prologue
    .line 144
    invoke-direct {p0, p1, p2}, Lcom/samsung/musicplus/service/PlayerService;->savePreferences(Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic access$8200(Lcom/samsung/musicplus/service/PlayerService;Ljava/lang/String;F)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # F

    .prologue
    .line 144
    invoke-direct {p0, p1, p2}, Lcom/samsung/musicplus/service/PlayerService;->savePreferences(Ljava/lang/String;F)V

    return-void
.end method

.method static synthetic access$8300(Lcom/samsung/musicplus/service/PlayerService;Ljava/lang/String;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Z

    .prologue
    .line 144
    invoke-direct {p0, p1, p2}, Lcom/samsung/musicplus/service/PlayerService;->savePreferences(Ljava/lang/String;Z)V

    return-void
.end method

.method static synthetic access$8400(Lcom/samsung/musicplus/service/PlayerService;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 144
    invoke-direct {p0, p1, p2}, Lcom/samsung/musicplus/service/PlayerService;->savePreferences(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$8500(Lcom/samsung/musicplus/service/PlayerService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;

    .prologue
    .line 144
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->isActiveSmartVolume()Z

    move-result v0

    return v0
.end method

.method static synthetic access$900(Lcom/samsung/musicplus/service/PlayerService;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/musicplus/service/PlayerService;

    .prologue
    .line 144
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->savePlayPosition()V

    return-void
.end method

.method private adjustSmartVolume(Ljava/lang/String;)V
    .locals 6
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x3

    .line 1137
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->isDmrPlaying()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1138
    const-string v2, "MusicService"

    const-string v3, "Don\'t Set Smart volume in AVPlayer mode"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1164
    :cond_0
    :goto_0
    return-void

    .line 1140
    :cond_1
    if-eqz p1, :cond_0

    invoke-static {p1}, Lcom/samsung/musicplus/util/UiUtils;->isLocalContents(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService;->mSmartVolume:Lcom/samsung/musicplus/library/audio/SmartVolumeManager;

    invoke-virtual {v2}, Lcom/samsung/musicplus/library/audio/SmartVolumeManager;->getSongPath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1145
    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService;->mSmartVolume:Lcom/samsung/musicplus/library/audio/SmartVolumeManager;

    iget v3, p0, Lcom/samsung/musicplus/service/PlayerService;->mBaeVolume:I

    invoke-virtual {v2, v3, p1}, Lcom/samsung/musicplus/library/audio/SmartVolumeManager;->getVolume(ILjava/lang/String;)I

    move-result v1

    .line 1147
    .local v1, "smart":I
    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

    invoke-virtual {v2, v5}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->getStreamMaxVolume(I)I

    move-result v0

    .line 1148
    .local v0, "max":I
    const-string v2, "MusicService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "adjustSmartVolume mBaeVolume : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/samsung/musicplus/service/PlayerService;->mBaeVolume:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1150
    if-le v1, v0, :cond_2

    .line 1151
    move v1, v0

    .line 1160
    :cond_2
    iget v2, p0, Lcom/samsung/musicplus/service/PlayerService;->mIgnoreVolumeCount:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/samsung/musicplus/service/PlayerService;->mIgnoreVolumeCount:I

    .line 1161
    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

    const/4 v3, 0x0

    invoke-virtual {v2, v5, v1, v3}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->setStreamVolume(III)V

    goto :goto_0
.end method

.method private canChangeSoundAlive(IZ)Z
    .locals 5
    .param p1, "soundAlive"    # I
    .param p2, "enableToast"    # Z

    .prologue
    .line 3281
    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

    invoke-virtual {v2}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->getCurrentAudioPath()I

    move-result v1

    .line 3282
    .local v1, "path":I
    invoke-static {v1, p1}, Lcom/samsung/musicplus/util/SoundAliveUtils;->getSoundAliveErrorMessage(II)I

    move-result v0

    .line 3284
    .local v0, "message":I
    const-string v2, "MusicService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "canChangeSoundAlive sound alive : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " current audio path : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " enableToast : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " message : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3286
    sget v2, Lcom/samsung/musicplus/util/SoundAliveUtils;->UNDEFINED:I

    if-ne v0, v2, :cond_0

    .line 3287
    const/4 v2, 0x1

    .line 3292
    :goto_0
    return v2

    .line 3289
    :cond_0
    if-eqz p2, :cond_1

    .line 3290
    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService;->mToastHandler:Lcom/samsung/musicplus/service/PlayerService$ToastHandler;

    invoke-virtual {v2, v0}, Lcom/samsung/musicplus/service/PlayerService$ToastHandler;->showToast(I)V

    .line 3292
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private canChangeSoundAlivePreset(IZ)Z
    .locals 6
    .param p1, "preset"    # I
    .param p2, "enableToast"    # Z

    .prologue
    const/4 v2, 0x1

    .line 3297
    iget-object v3, p0, Lcom/samsung/musicplus/service/PlayerService;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

    invoke-virtual {v3}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->getCurrentAudioPath()I

    move-result v1

    .line 3298
    .local v1, "path":I
    invoke-static {v1, p1}, Lcom/samsung/musicplus/util/SoundAliveUtilsV2;->getNewSoundAliveErrorMessage(II)I

    move-result v0

    .line 3299
    .local v0, "message":I
    const-string v3, "MusicService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "canChangeSoundAliveV2 - preset : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3301
    sget v3, Lcom/samsung/musicplus/util/SoundAliveUtils;->UNDEFINED:I

    if-ne v0, v3, :cond_0

    .line 3307
    :goto_0
    return v2

    .line 3304
    :cond_0
    if-eqz p2, :cond_1

    .line 3305
    iget-object v3, p0, Lcom/samsung/musicplus/service/PlayerService;->mToastHandler:Lcom/samsung/musicplus/service/PlayerService$ToastHandler;

    invoke-virtual {v3, v0, v2}, Lcom/samsung/musicplus/service/PlayerService$ToastHandler;->showToast(II)V

    .line 3307
    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private canChangeSoundAliveStrength([IZ)[I
    .locals 8
    .param p1, "strength"    # [I
    .param p2, "enableToast"    # Z

    .prologue
    .line 3312
    iget-object v5, p0, Lcom/samsung/musicplus/service/PlayerService;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

    invoke-virtual {v5}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->getCurrentAudioPath()I

    move-result v4

    .line 3313
    .local v4, "path":I
    sget v3, Lcom/samsung/musicplus/util/SoundAliveUtils;->UNDEFINED:I

    .line 3315
    .local v3, "messageStrength":I
    if-eqz p1, :cond_1

    .line 3316
    const/4 v0, 0x0

    .line 3318
    .local v0, "EFFECT_OFF":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    const/4 v5, 0x3

    if-ge v1, v5, :cond_1

    .line 3319
    const-string v5, "MusicService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "canChangeSoundAliveV2 - strength["

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "]"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    aget v7, p1, v1

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3321
    aget v5, p1, v1

    if-eqz v5, :cond_0

    .line 3322
    add-int/lit8 v5, v1, 0x7

    invoke-static {v4, v5}, Lcom/samsung/musicplus/util/SoundAliveUtilsV2;->getNewSoundAliveErrorMessage(II)I

    move-result v2

    .line 3324
    .local v2, "message":I
    sget v5, Lcom/samsung/musicplus/util/SoundAliveUtils;->UNDEFINED:I

    if-eq v2, v5, :cond_0

    .line 3325
    const/4 v5, 0x0

    aput v5, p1, v1

    .line 3326
    move v3, v2

    .line 3318
    .end local v2    # "message":I
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 3332
    .end local v0    # "EFFECT_OFF":I
    .end local v1    # "i":I
    :cond_1
    sget v5, Lcom/samsung/musicplus/util/SoundAliveUtils;->UNDEFINED:I

    if-eq v3, v5, :cond_2

    .line 3333
    iget-object v5, p0, Lcom/samsung/musicplus/service/PlayerService;->mToastHandler:Lcom/samsung/musicplus/service/PlayerService$ToastHandler;

    invoke-virtual {v5, v3}, Lcom/samsung/musicplus/service/PlayerService$ToastHandler;->showToast(I)V

    .line 3336
    .end local p1    # "strength":[I
    :goto_1
    return-object p1

    .restart local p1    # "strength":[I
    :cond_2
    const/4 p1, 0x0

    goto :goto_1
.end method

.method private cancelAgingTimeOut()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 5184
    const-string v1, "alarm"

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/service/PlayerService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 5185
    .local v0, "am":Landroid/app/AlarmManager;
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.sec.android.app.music.intent.action.WRITE_LOG"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p0, v3, v1, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 5186
    return-void
.end method

.method private cancelServiceTimeOut()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 5161
    const-string v1, "MusicService"

    const-string v2, "cancelServiceTimeOut()"

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 5162
    const-string v1, "alarm"

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/service/PlayerService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 5163
    .local v0, "am":Landroid/app/AlarmManager;
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.samsung.musicplus.musicservicecommand.ACTION_MUSIC_CLOSE"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p0, v3, v1, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 5164
    return-void
.end method

.method private createBigNotificationView()Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;
    .locals 4

    .prologue
    .line 4879
    new-instance v0, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;

    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f04007f

    const/4 v3, 0x1

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;-><init>(Lcom/samsung/musicplus/service/PlayerService;Landroid/content/Context;IZ)V

    return-object v0
.end method

.method private createControlClient(Landroid/content/ComponentName;)V
    .locals 4
    .param p1, "cm"    # Landroid/content/ComponentName;

    .prologue
    const/4 v3, 0x0

    .line 1466
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.MEDIA_BUTTON"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1467
    .local v0, "mediaButton":Landroid/content/Intent;
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 1468
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v3, v0, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 1470
    .local v1, "pi":Landroid/app/PendingIntent;
    new-instance v2, Landroid/media/RemoteControlClient;

    invoke-direct {v2, v1}, Landroid/media/RemoteControlClient;-><init>(Landroid/app/PendingIntent;)V

    iput-object v2, p0, Lcom/samsung/musicplus/service/PlayerService;->mControlClient:Landroid/media/RemoteControlClient;

    .line 1471
    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService;->mControlClient:Landroid/media/RemoteControlClient;

    const/16 v3, 0x1ff

    invoke-virtual {v2, v3}, Landroid/media/RemoteControlClient;->setTransportControlFlags(I)V

    .line 1480
    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService;->mControlClient:Landroid/media/RemoteControlClient;

    new-instance v3, Lcom/samsung/musicplus/service/PlayerService$12;

    invoke-direct {v3, p0}, Lcom/samsung/musicplus/service/PlayerService$12;-><init>(Lcom/samsung/musicplus/service/PlayerService;)V

    invoke-virtual {v2, v3}, Landroid/media/RemoteControlClient;->setPlaybackPositionUpdateListener(Landroid/media/RemoteControlClient$OnPlaybackPositionUpdateListener;)V

    .line 1494
    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService;->mControlClient:Landroid/media/RemoteControlClient;

    new-instance v3, Lcom/samsung/musicplus/service/PlayerService$13;

    invoke-direct {v3, p0}, Lcom/samsung/musicplus/service/PlayerService$13;-><init>(Lcom/samsung/musicplus/service/PlayerService;)V

    invoke-virtual {v2, v3}, Landroid/media/RemoteControlClient;->setOnGetPlaybackPositionListener(Landroid/media/RemoteControlClient$OnGetPlaybackPositionListener;)V

    .line 1502
    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService;->mControlClient:Landroid/media/RemoteControlClient;

    new-instance v3, Lcom/samsung/musicplus/service/PlayerService$14;

    invoke-direct {v3, p0}, Lcom/samsung/musicplus/service/PlayerService$14;-><init>(Lcom/samsung/musicplus/service/PlayerService;)V

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/audio/RemoteControlClientCompat;->setCommandSetListener(Landroid/media/RemoteControlClient;Lcom/samsung/musicplus/library/audio/RemoteControlClientCompat$OnCompatCommandSetListener;)V

    .line 1525
    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

    iget-object v3, p0, Lcom/samsung/musicplus/service/PlayerService;->mControlClient:Landroid/media/RemoteControlClient;

    invoke-virtual {v2, v3}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->registerRemoteControlClient(Landroid/media/RemoteControlClient;)V

    .line 1526
    return-void
.end method

.method private createMediaSession(Landroid/content/Context;)Landroid/media/session/MediaSession;
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 1768
    new-instance v0, Landroid/media/session/MediaSession;

    const-string v1, "MusicService"

    invoke-direct {v0, p1, v1}, Landroid/media/session/MediaSession;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 1769
    .local v0, "m":Landroid/media/session/MediaSession;
    new-instance v1, Lcom/samsung/musicplus/service/PlayerService$MediaSessionCallback;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/samsung/musicplus/service/PlayerService$MediaSessionCallback;-><init>(Lcom/samsung/musicplus/service/PlayerService;Lcom/samsung/musicplus/service/PlayerService$1;)V

    invoke-virtual {v0, v1}, Landroid/media/session/MediaSession;->setCallback(Landroid/media/session/MediaSession$Callback;)V

    .line 1771
    new-instance v1, Landroid/media/session/PlaybackState$Builder;

    invoke-direct {v1}, Landroid/media/session/PlaybackState$Builder;-><init>()V

    iput-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mMediaStateBuilder:Landroid/media/session/PlaybackState$Builder;

    .line 1772
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mMediaStateBuilder:Landroid/media/session/PlaybackState$Builder;

    const-wide/16 v2, 0x37f

    invoke-virtual {v1, v2, v3}, Landroid/media/session/PlaybackState$Builder;->setActions(J)Landroid/media/session/PlaybackState$Builder;

    .line 1777
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mMediaStateBuilder:Landroid/media/session/PlaybackState$Builder;

    invoke-virtual {v1}, Landroid/media/session/PlaybackState$Builder;->build()Landroid/media/session/PlaybackState;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/media/session/MediaSession;->setPlaybackState(Landroid/media/session/PlaybackState;)V

    .line 1778
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/media/session/MediaSession;->setFlags(I)V

    .line 1780
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/media/session/MediaSession;->setActive(Z)V

    .line 1781
    return-object v0
.end method

.method private createNotificationView()Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;
    .locals 4

    .prologue
    .line 4874
    new-instance v0, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;

    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f040080

    const/4 v3, 0x0

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;-><init>(Lcom/samsung/musicplus/service/PlayerService;Landroid/content/Context;IZ)V

    return-object v0
.end method

.method private ensureAdaptSound()V
    .locals 4

    .prologue
    .line 955
    sget-boolean v0, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_SUPPORT_ADAPT_SOUND:Z

    if-eqz v0, :cond_0

    .line 956
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mAdaptSound:Lcom/samsung/musicplus/library/audio/AdaptSound;

    if-nez v0, :cond_0

    .line 957
    new-instance v0, Lcom/samsung/musicplus/library/audio/AdaptSound;

    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

    invoke-virtual {v3}, Lcom/samsung/musicplus/service/MultiPlayer;->getAudioSessionId()I

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/samsung/musicplus/library/audio/AdaptSound;-><init>(Landroid/content/Context;II)V

    iput-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mAdaptSound:Lcom/samsung/musicplus/library/audio/AdaptSound;

    .line 959
    const-string v0, "MusicService"

    const-string v1, "Adapt sound created"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 962
    :cond_0
    return-void
.end method

.method private ensureBounceManager()V
    .locals 3

    .prologue
    .line 3640
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mBounceMotionManager:Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager;

    if-nez v0, :cond_0

    .line 3641
    new-instance v0, Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager;

    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService;->mBounceEventListener:Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager$OnBounceListener;

    invoke-direct {v0, v1, v2}, Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager;-><init>(Landroid/content/Context;Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager$OnBounceListener;)V

    iput-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mBounceMotionManager:Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager;

    .line 3643
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mBounceMotionManager:Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager;

    new-instance v1, Lcom/samsung/musicplus/service/PlayerService$25;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/service/PlayerService$25;-><init>(Lcom/samsung/musicplus/service/PlayerService;)V

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager;->registerSettingChangedListener(Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager$OnMusicBounceSettingChangeListener;)V

    .line 3654
    :cond_0
    return-void
.end method

.method private ensureGestureManager()V
    .locals 1

    .prologue
    .line 3507
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mGestureManager:Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;

    if-eqz v0, :cond_0

    .line 3511
    :goto_0
    return-void

    .line 3510
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;->getInstance(Landroid/content/Context;)Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mGestureManager:Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;

    goto :goto_0
.end method

.method private ensureMotionManager()V
    .locals 3

    .prologue
    .line 3577
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mMotionManager:Lcom/samsung/musicplus/library/hardware/SimpleMotionManager;

    if-nez v0, :cond_0

    .line 3578
    new-instance v0, Lcom/samsung/musicplus/library/hardware/SimpleMotionManager;

    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Lcom/samsung/musicplus/service/PlayerService$23;

    invoke-direct {v2, p0}, Lcom/samsung/musicplus/service/PlayerService$23;-><init>(Lcom/samsung/musicplus/service/PlayerService;)V

    invoke-direct {v0, v1, v2}, Lcom/samsung/musicplus/library/hardware/SimpleMotionManager;-><init>(Landroid/content/Context;Lcom/samsung/musicplus/library/hardware/SimpleMotionManager$OnMotionListener;)V

    iput-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mMotionManager:Lcom/samsung/musicplus/library/hardware/SimpleMotionManager;

    .line 3595
    :cond_0
    return-void
.end method

.method private ensureSmartVolume()V
    .locals 3

    .prologue
    .line 1081
    sget-boolean v1, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_SMART_VOLUME:Z

    if-eqz v1, :cond_0

    .line 1082
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mSmartVolume:Lcom/samsung/musicplus/library/audio/SmartVolumeManager;

    if-nez v1, :cond_0

    .line 1083
    invoke-static {}, Lcom/samsung/musicplus/library/audio/SmartVolumeManager;->getInstance()Lcom/samsung/musicplus/library/audio/SmartVolumeManager;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mSmartVolume:Lcom/samsung/musicplus/library/audio/SmartVolumeManager;

    .line 1086
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.media.VOLUME_CHANGED_ACTION"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 1087
    .local v0, "f":Landroid/content/IntentFilter;
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mVolumeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/samsung/musicplus/service/PlayerService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1088
    const-string v1, "MusicService"

    const-string v2, "Smart volume created"

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1091
    .end local v0    # "f":Landroid/content/IntentFilter;
    :cond_0
    return-void
.end method

.method private errorHandleToPrevNext(Z)V
    .locals 4
    .param p1, "byUser"    # Z

    .prologue
    const/4 v3, 0x1

    .line 4050
    const-string v0, "MusicService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "errorHandleToPrevNext() bErrorHandleToPrev:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/samsung/musicplus/service/PlayerService;->bErrorHandleToPrev:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " byUser:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4051
    iget-boolean v0, p0, Lcom/samsung/musicplus/service/PlayerService;->bErrorHandleToPrev:Z

    if-eqz v0, :cond_1

    .line 4052
    if-eqz p1, :cond_0

    .line 4053
    invoke-virtual {p0, v3}, Lcom/samsung/musicplus/service/PlayerService;->prev(Z)V

    .line 4064
    :goto_0
    return-void

    .line 4055
    :cond_0
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->prevInternal()V

    goto :goto_0

    .line 4058
    :cond_1
    if-eqz p1, :cond_2

    .line 4059
    invoke-virtual {p0, v3}, Lcom/samsung/musicplus/service/PlayerService;->next(Z)V

    goto :goto_0

    .line 4061
    :cond_2
    invoke-direct {p0, v3}, Lcom/samsung/musicplus/service/PlayerService;->nextInternal(Z)V

    goto :goto_0
.end method

.method private forward()V
    .locals 6

    .prologue
    .line 3884
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->isSupportForward()Z

    move-result v2

    if-nez v2, :cond_0

    .line 3890
    :goto_0
    return-void

    .line 3888
    :cond_0
    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

    invoke-virtual {v2}, Lcom/samsung/musicplus/service/MultiPlayer;->position()J

    move-result-wide v2

    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->getSkippingTime()I

    move-result v4

    int-to-long v4, v4

    add-long v0, v2, v4

    .line 3889
    .local v0, "time":J
    invoke-virtual {p0, v0, v1}, Lcom/samsung/musicplus/service/PlayerService;->seek(J)J

    goto :goto_0
.end method

.method private getDurationFromDb()J
    .locals 2

    .prologue
    .line 4170
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerListManager;->getDuration()J

    move-result-wide v0

    return-wide v0
.end method

.method public static getInstance()Lcom/samsung/musicplus/service/PlayerService;
    .locals 1

    .prologue
    .line 1311
    sget-object v0, Lcom/samsung/musicplus/service/PlayerService;->sService:Lcom/samsung/musicplus/service/PlayerService;

    return-object v0
.end method

.method private getLaunchPendingIntent()Landroid/app/PendingIntent;
    .locals 3

    .prologue
    .line 4799
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/samsung/musicplus/MusicMainActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 4800
    .local v0, "i":Landroid/content/Intent;
    const v1, 0x14008000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 4802
    const-string v1, "track_launch"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 4803
    const v1, 0x7010002

    const/4 v2, 0x0

    invoke-static {p0, v1, v0, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    return-object v1
.end method

.method private getMaxVolumeRatio()F
    .locals 2

    .prologue
    .line 4427
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->isMidi()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4430
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

    invoke-virtual {v1}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->isAudioPathEarjack()Z

    move-result v1

    invoke-direct {p0, v1}, Lcom/samsung/musicplus/service/PlayerService;->getMidiVolumeRatio(Z)F

    move-result v0

    .line 4434
    .local v0, "volume":F
    :goto_0
    return v0

    .line 4432
    .end local v0    # "volume":F
    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    .restart local v0    # "volume":F
    goto :goto_0
.end method

.method private getMidiVolumeRatio(Z)F
    .locals 4
    .param p1, "isEarJackConnected"    # Z

    .prologue
    .line 4446
    if-eqz p1, :cond_0

    .line 4447
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

    invoke-virtual {v1}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->getMidiHeadsetVolume()F

    move-result v0

    .line 4451
    .local v0, "volume":F
    :goto_0
    const-string v1, "MusicService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getMidiVolume "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " isEarJackConnected : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 4452
    return v0

    .line 4449
    .end local v0    # "volume":F
    :cond_0
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

    invoke-virtual {v1}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->getMidiSpeakerVolume()F

    move-result v0

    .restart local v0    # "volume":F
    goto :goto_0
.end method

.method private getResizedBitmapForNotification(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 3
    .param p1, "bm"    # Landroid/graphics/Bitmap;

    .prologue
    .line 4981
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c014b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 4982
    .local v0, "size":I
    const/4 v1, 0x1

    invoke-static {p1, v0, v0, v1}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v1

    return-object v1
.end method

.method private getSkippingTime()I
    .locals 5

    .prologue
    const/16 v4, 0xe

    .line 3870
    iget v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mRepeatCount:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/samsung/musicplus/service/PlayerService;->mRepeatCount:I

    div-int/lit8 v0, v1, 0x2

    .line 3872
    .local v0, "index":I
    sget-object v1, Lcom/samsung/musicplus/service/PlayerService;->TIME_SKIP_VALUE:[I

    array-length v1, v1

    if-lt v0, v1, :cond_0

    .line 3873
    sget-object v1, Lcom/samsung/musicplus/service/PlayerService;->TIME_SKIP_VALUE:[I

    array-length v1, v1

    add-int/lit8 v0, v1, -0x1

    .line 3877
    :cond_0
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayerHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 3878
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayerHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v1, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 3879
    sget-object v1, Lcom/samsung/musicplus/service/PlayerService;->TIME_SKIP_VALUE:[I

    aget v1, v1, v0

    return v1
.end method

.method private getWifiDisplayAction()Ljava/lang/String;
    .locals 1

    .prologue
    .line 2295
    sget-boolean v0, Lcom/samsung/musicplus/util/Version;->LIBRARY_API_1:Z

    if-eqz v0, :cond_0

    .line 2296
    const-string v0, "com.deprecated.action.WIFI_DISPLAY"

    .line 2298
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "android.intent.action.WIFI_DISPLAY"

    goto :goto_0
.end method

.method private handleBounceSetting(Z)V
    .locals 3
    .param p1, "isEnabled"    # Z

    .prologue
    .line 3663
    const-string v0, "MusicService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handleBounceSetting "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3664
    if-eqz p1, :cond_1

    .line 3665
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3666
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->registerBounceListener()V

    .line 3673
    :cond_0
    :goto_0
    return-void

    .line 3671
    :cond_1
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->unregisterBounceListener()V

    goto :goto_0
.end method

.method private handleCommandIntent(Landroid/content/Intent;)V
    .locals 28
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 2423
    invoke-virtual/range {p1 .. p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    .line 2424
    .local v2, "action":Ljava/lang/String;
    const-string v3, "command"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 2425
    .local v16, "cmd":Ljava/lang/String;
    const-string v3, "MusicService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, " handleCommandIntent action "

    invoke-virtual {v11, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v14, " "

    invoke-virtual {v11, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, p0

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v14, " command : "

    invoke-virtual {v11, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, v16

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v3, v11}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2429
    const-string v3, "MusicPlayer"

    const-string v11, "from"

    move-object/from16 v0, p1

    invoke-virtual {v0, v11}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v3, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2676
    :cond_0
    :goto_0
    return-void

    .line 2433
    :cond_1
    const-string v3, "com.samsung.musicplus.musicservicecommand.togglepause"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "togglepause"

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 2434
    :cond_2
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/service/PlayerService;->isPlaying()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 2435
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/service/PlayerService;->pause()V

    goto :goto_0

    .line 2437
    :cond_3
    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/samsung/musicplus/service/PlayerService;->play(Z)V

    goto :goto_0

    .line 2439
    :cond_4
    const-string v3, "play"

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 2440
    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/samsung/musicplus/service/PlayerService;->play(Z)V

    goto :goto_0

    .line 2441
    :cond_5
    const-string v3, "com.samsung.musicplus.musicservicecommand.pause"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    const-string v3, "pause"

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 2442
    :cond_6
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/service/PlayerService;->pause()V

    goto :goto_0

    .line 2443
    :cond_7
    const-string v3, "android.media.AUDIO_BECOMING_NOISY"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_8

    const-string v3, "android.media.AUDIO_BECOMING_NOISY_SEC"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    .line 2445
    :cond_8
    invoke-direct/range {p0 .. p0}, Lcom/samsung/musicplus/service/PlayerService;->isDmrPlaying()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 2446
    const-string v3, "MusicService"

    const-string v11, "received Noisy in DMR case so ignore it."

    invoke-static {v3, v11}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 2449
    :cond_9
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/samsung/musicplus/service/PlayerService;->mIgnoreNoisy:Z

    if-nez v3, :cond_0

    .line 2450
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/service/PlayerService;->pause()V

    goto :goto_0

    .line 2452
    :cond_a
    sget-boolean v3, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_MOTION_PALM_PAUSE:Z

    if-eqz v3, :cond_b

    const-string v3, "android.intent.action.PALM_DOWN"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 2453
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/musicplus/service/PlayerService;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

    invoke-virtual {v3}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->isAudioPathSpeaker()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2454
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/service/PlayerService;->isPlaying()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2455
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/service/PlayerService;->pause()V

    .line 2456
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/service/PlayerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-string v11, "PALM"

    invoke-static {v3, v11}, Lcom/samsung/musicplus/util/logging/FeatureLogger;->insertLog(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2459
    :cond_b
    const-string v3, "com.samsung.musicplus.musicservicecommand.next"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_c

    const-string v3, "next"

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_e

    .line 2460
    :cond_c
    invoke-direct/range {p0 .. p0}, Lcom/samsung/musicplus/service/PlayerService;->reloadQueueIfNoList()V

    .line 2462
    const-string v3, "force"

    const/4 v11, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v19

    .line 2463
    .local v19, "force":Z
    if-eqz v19, :cond_d

    .line 2464
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/service/PlayerService;->isPlaying()Z

    move-result v25

    .line 2465
    .local v25, "wasPlaying":Z
    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/samsung/musicplus/service/PlayerService;->next(Z)V

    .line 2466
    if-nez v25, :cond_0

    .line 2468
    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/samsung/musicplus/service/PlayerService;->play(Z)V

    goto/16 :goto_0

    .line 2471
    .end local v25    # "wasPlaying":Z
    :cond_d
    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/samsung/musicplus/service/PlayerService;->next(Z)V

    goto/16 :goto_0

    .line 2473
    .end local v19    # "force":Z
    :cond_e
    const-string v3, "com.samsung.musicplus.musicservicecommand.previous"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_f

    const-string v3, "previous"

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_12

    .line 2474
    :cond_f
    invoke-direct/range {p0 .. p0}, Lcom/samsung/musicplus/service/PlayerService;->reloadQueueIfNoList()V

    .line 2476
    const-string v3, "force"

    const/4 v11, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v19

    .line 2477
    .restart local v19    # "force":Z
    const-string v3, "svoice_command"

    const/4 v11, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v23

    .line 2478
    .local v23, "sVoiceCommand":Z
    if-eqz v19, :cond_10

    .line 2479
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/service/PlayerService;->isPlaying()Z

    move-result v25

    .line 2481
    .restart local v25    # "wasPlaying":Z
    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/samsung/musicplus/service/PlayerService;->prev(Z)V

    .line 2482
    if-nez v25, :cond_0

    .line 2484
    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/samsung/musicplus/service/PlayerService;->play(Z)V

    goto/16 :goto_0

    .line 2486
    .end local v25    # "wasPlaying":Z
    :cond_10
    if-eqz v23, :cond_11

    .line 2487
    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/samsung/musicplus/service/PlayerService;->prev(Z)V

    goto/16 :goto_0

    .line 2489
    :cond_11
    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/samsung/musicplus/service/PlayerService;->prev(Z)V

    goto/16 :goto_0

    .line 2491
    .end local v19    # "force":Z
    .end local v23    # "sVoiceCommand":Z
    :cond_12
    const-string v3, "fastforward_down"

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_13

    const-string v3, "com.samsung.musicplus.musicservicecommand.ff.down"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_15

    .line 2492
    :cond_13
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/musicplus/service/PlayerService;->mControlTask:Lcom/samsung/musicplus/widget/control/FowardRewindControlTaskMediaButton;

    if-eqz v3, :cond_14

    .line 2493
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/musicplus/service/PlayerService;->mControlTask:Lcom/samsung/musicplus/widget/control/FowardRewindControlTaskMediaButton;

    invoke-virtual {v3}, Lcom/samsung/musicplus/widget/control/FowardRewindControlTaskMediaButton;->setCancel()V

    .line 2495
    :cond_14
    new-instance v3, Lcom/samsung/musicplus/widget/control/FowardRewindControlTaskMediaButton;

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/service/PlayerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v11

    const/4 v14, 0x1

    invoke-direct {v3, v11, v14}, Lcom/samsung/musicplus/widget/control/FowardRewindControlTaskMediaButton;-><init>(Landroid/content/Context;I)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/musicplus/service/PlayerService;->mControlTask:Lcom/samsung/musicplus/widget/control/FowardRewindControlTaskMediaButton;

    .line 2497
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/musicplus/service/PlayerService;->mControlTask:Lcom/samsung/musicplus/widget/control/FowardRewindControlTaskMediaButton;

    const/4 v11, 0x3

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v14, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/service/PlayerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v15

    aput-object v15, v11, v14

    const/4 v14, 0x1

    const/4 v15, 0x0

    aput-object v15, v11, v14

    const/4 v14, 0x2

    const/4 v15, 0x1

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v11, v14

    invoke-virtual {v3, v11}, Lcom/samsung/musicplus/widget/control/FowardRewindControlTaskMediaButton;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0

    .line 2499
    :cond_15
    const-string v3, "fastforward_up"

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_16

    const-string v3, "com.samsung.musicplus.musicservicecommand.ff.up"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_17

    .line 2500
    :cond_16
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/musicplus/service/PlayerService;->mControlTask:Lcom/samsung/musicplus/widget/control/FowardRewindControlTaskMediaButton;

    if-eqz v3, :cond_0

    .line 2501
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/musicplus/service/PlayerService;->mControlTask:Lcom/samsung/musicplus/widget/control/FowardRewindControlTaskMediaButton;

    invoke-virtual {v3}, Lcom/samsung/musicplus/widget/control/FowardRewindControlTaskMediaButton;->setCancel()V

    goto/16 :goto_0

    .line 2503
    :cond_17
    const-string v3, "rewind_down"

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_18

    const-string v3, "com.samsung.musicplus.musicservicecommand.rew.down"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1a

    .line 2504
    :cond_18
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/musicplus/service/PlayerService;->mControlTask:Lcom/samsung/musicplus/widget/control/FowardRewindControlTaskMediaButton;

    if-eqz v3, :cond_19

    .line 2505
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/musicplus/service/PlayerService;->mControlTask:Lcom/samsung/musicplus/widget/control/FowardRewindControlTaskMediaButton;

    invoke-virtual {v3}, Lcom/samsung/musicplus/widget/control/FowardRewindControlTaskMediaButton;->setCancel()V

    .line 2507
    :cond_19
    new-instance v3, Lcom/samsung/musicplus/widget/control/FowardRewindControlTaskMediaButton;

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/service/PlayerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v11

    const/4 v14, 0x2

    invoke-direct {v3, v11, v14}, Lcom/samsung/musicplus/widget/control/FowardRewindControlTaskMediaButton;-><init>(Landroid/content/Context;I)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/musicplus/service/PlayerService;->mControlTask:Lcom/samsung/musicplus/widget/control/FowardRewindControlTaskMediaButton;

    .line 2509
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/musicplus/service/PlayerService;->mControlTask:Lcom/samsung/musicplus/widget/control/FowardRewindControlTaskMediaButton;

    const/4 v11, 0x3

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v14, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/service/PlayerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v15

    aput-object v15, v11, v14

    const/4 v14, 0x1

    const/4 v15, 0x0

    aput-object v15, v11, v14

    const/4 v14, 0x2

    const/4 v15, 0x2

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v11, v14

    invoke-virtual {v3, v11}, Lcom/samsung/musicplus/widget/control/FowardRewindControlTaskMediaButton;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0

    .line 2510
    :cond_1a
    const-string v3, "rewind_up"

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1b

    const-string v3, "com.samsung.musicplus.musicservicecommand.rew.up"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1c

    .line 2511
    :cond_1b
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/musicplus/service/PlayerService;->mControlTask:Lcom/samsung/musicplus/widget/control/FowardRewindControlTaskMediaButton;

    if-eqz v3, :cond_0

    .line 2512
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/musicplus/service/PlayerService;->mControlTask:Lcom/samsung/musicplus/widget/control/FowardRewindControlTaskMediaButton;

    invoke-virtual {v3}, Lcom/samsung/musicplus/widget/control/FowardRewindControlTaskMediaButton;->setCancel()V

    goto/16 :goto_0

    .line 2514
    :cond_1c
    const-string v3, "com.samsung.musicplus.action.PLAYBACK_FORWARD"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1d

    const-string v3, "fastforward"

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1e

    .line 2515
    :cond_1d
    invoke-direct/range {p0 .. p0}, Lcom/samsung/musicplus/service/PlayerService;->forward()V

    goto/16 :goto_0

    .line 2516
    :cond_1e
    const-string v3, "com.samsung.musicplus.action.PLAYBACK_REWIND"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1f

    const-string v3, "rewind"

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_20

    .line 2517
    :cond_1f
    invoke-direct/range {p0 .. p0}, Lcom/samsung/musicplus/service/PlayerService;->rewind()V

    goto/16 :goto_0

    .line 2518
    :cond_20
    const-string v3, "stop"

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_22

    .line 2519
    const-string v3, "from"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 2520
    .local v20, "from":Ljava/lang/String;
    if-eqz v20, :cond_21

    .line 2521
    const-string v3, "MusicService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "EXTRA_CMD_STOP from : "

    invoke-virtual {v11, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, v20

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v3, v11}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2523
    :cond_21
    invoke-direct/range {p0 .. p0}, Lcom/samsung/musicplus/service/PlayerService;->stop()V

    goto/16 :goto_0

    .line 2524
    .end local v20    # "from":Ljava/lang/String;
    :cond_22
    const-string v3, "com.samsung.musicplus.musicservicecommand.ACTION_MUSIC_CLOSE"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_25

    .line 2525
    const-string v3, "from"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 2526
    .restart local v20    # "from":Ljava/lang/String;
    if-eqz v20, :cond_23

    .line 2527
    const-string v3, "MusicService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "ACTION_MUSIC_CLOSE from : "

    invoke-virtual {v11, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    move-object/from16 v0, v20

    invoke-virtual {v11, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v3, v11}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2529
    :cond_23
    const-string v3, "time_out"

    move-object/from16 v0, v20

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_24

    .line 2531
    invoke-direct/range {p0 .. p0}, Lcom/samsung/musicplus/service/PlayerService;->hideNotification()V

    goto/16 :goto_0

    .line 2533
    :cond_24
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/service/PlayerService;->stopMusicPackage()V

    goto/16 :goto_0

    .line 2535
    .end local v20    # "from":Ljava/lang/String;
    :cond_25
    const-string v3, "com.samsung.musicplus.intent.action.UPDATE_WIDGET"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_26

    .line 2539
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/musicplus/service/PlayerService;->mServiceAppWidggetManger:Lcom/samsung/musicplus/widget/appwidget/ServiceAppWidgetManager;

    const-string v11, "com.android.music.metachanged"

    invoke-virtual {v3, v11}, Lcom/samsung/musicplus/widget/appwidget/ServiceAppWidgetManager;->performUpdate(Ljava/lang/String;)V

    .line 2540
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/musicplus/service/PlayerService;->mDelayedStopHandler:Landroid/os/Handler;

    const/16 v11, 0xb

    const-wide/16 v14, 0x7d0

    invoke-virtual {v3, v11, v14, v15}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto/16 :goto_0

    .line 2546
    :cond_26
    const-string v3, "com.samsung.musicplus.intent.action.PLAY_WIDGET_LIST"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_27

    .line 2547
    const-string v3, "list"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getLongArrayExtra(Ljava/lang/String;)[J

    move-result-object v6

    .line 2548
    .local v6, "list":[J
    const-string v3, "listPosition"

    const/4 v11, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    .line 2549
    .local v7, "position":I
    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v7, v3}, Lcom/samsung/musicplus/service/PlayerService;->open([JIZ)V

    goto/16 :goto_0

    .line 2550
    .end local v6    # "list":[J
    .end local v7    # "position":I
    :cond_27
    const-string v3, "com.samsung.musicplus.action.SERVICE_COMMAND"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2b

    const-string v3, "openList"

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2b

    .line 2551
    const-string v3, "listType"

    const v11, 0x20001

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 2552
    .local v4, "type":I
    const-string v3, "listQueryKey"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 2553
    .local v5, "key":Ljava/lang/String;
    const-string v3, "list"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getLongArrayExtra(Ljava/lang/String;)[J

    move-result-object v6

    .line 2554
    .restart local v6    # "list":[J
    const-string v3, "listPosition"

    const/4 v11, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    .line 2555
    .restart local v7    # "position":I
    const-string v3, "dmr_device"

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 2557
    .local v18, "dmrId":Ljava/lang/String;
    const-wide/16 v8, 0x0

    .line 2558
    .local v8, "seekPosition":J
    if-eqz v6, :cond_28

    array-length v3, v6

    if-lez v3, :cond_28

    aget-wide v14, v6, v7

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/service/PlayerService;->getAudioId()J

    move-result-wide v26

    cmp-long v3, v14, v26

    if-nez v3, :cond_28

    .line 2559
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/service/PlayerService;->position()J

    move-result-wide v8

    .line 2562
    :cond_28
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/service/PlayerService;->getDlnaPlayingDmrId()Ljava/lang/String;

    move-result-object v17

    .line 2563
    .local v17, "currentDmr":Ljava/lang/String;
    if-eqz v18, :cond_29

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_29

    .line 2567
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

    invoke-virtual {v3}, Lcom/samsung/musicplus/service/MultiPlayer;->releaseDmrPlayer()V

    .line 2569
    :cond_29
    const/4 v10, 0x1

    move-object/from16 v3, p0

    invoke-virtual/range {v3 .. v10}, Lcom/samsung/musicplus/service/PlayerService;->open(ILjava/lang/String;[JIJZ)V

    .line 2570
    if-eqz v18, :cond_0

    const-string v3, ""

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 2571
    sget-boolean v3, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_DATA_CHECK_POPUP:Z

    if-eqz v3, :cond_2a

    .line 2572
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/musicplus/service/PlayerService;->mDlnaManager:Lcom/samsung/musicplus/library/dlna/DlnaManager;

    if-nez v3, :cond_2a

    .line 2573
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/service/PlayerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    sget-object v11, Lcom/samsung/musicplus/contents/dlna/DlnaStore$Server;->CONTENT_URI:Landroid/net/Uri;

    sget-object v14, Lcom/samsung/musicplus/contents/dlna/DlnaStore$ServerContents;->CONTENT_URI:Landroid/net/Uri;

    sget-object v15, Lcom/samsung/musicplus/contents/dlna/DlnaStore$Renderer;->CONTENT_URI:Landroid/net/Uri;

    sget-object v26, Lcom/samsung/musicplus/contents/dlna/DlnaStore;->DLNA_ALL_DELETE_URI:Landroid/net/Uri;

    move-object/from16 v0, v26

    invoke-static {v3, v11, v14, v15, v0}, Lcom/samsung/musicplus/library/dlna/DlnaManager;->getInstance(Landroid/content/Context;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;)Lcom/samsung/musicplus/library/dlna/DlnaManager;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/samsung/musicplus/service/PlayerService;->mDlnaManager:Lcom/samsung/musicplus/library/dlna/DlnaManager;

    .line 2576
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/musicplus/service/PlayerService;->mDlnaManager:Lcom/samsung/musicplus/library/dlna/DlnaManager;

    invoke-virtual {v3}, Lcom/samsung/musicplus/library/dlna/DlnaManager;->bindDlnaService()V

    .line 2580
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/musicplus/service/PlayerService;->mDlnaHandler:Landroid/os/Handler;

    const/4 v11, 0x1

    const-wide/16 v14, 0x3c

    invoke-virtual {v3, v11, v14, v15}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 2583
    :cond_2a
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/service/PlayerService;->changeToDmrPlayer(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2585
    .end local v4    # "type":I
    .end local v5    # "key":Ljava/lang/String;
    .end local v6    # "list":[J
    .end local v7    # "position":I
    .end local v8    # "seekPosition":J
    .end local v17    # "currentDmr":Ljava/lang/String;
    .end local v18    # "dmrId":Ljava/lang/String;
    :cond_2b
    const-string v3, "com.samsung.musicplus.GESTURE"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2e

    .line 2586
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/service/PlayerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v10

    .line 2587
    .local v10, "context":Landroid/content/Context;
    invoke-static {v10}, Lcom/samsung/musicplus/util/UiUtils;->isLockScreenOn(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_2c

    .line 2588
    const-string v3, "Lock screen is not on"

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/samsung/musicplus/service/PlayerService;->unregisterGestureListener(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2592
    :cond_2c
    const-string v3, "command"

    const/4 v11, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v21

    .line 2593
    .local v21, "gesture":I
    const/4 v3, 0x1

    move/from16 v0, v21

    if-ne v3, v0, :cond_2d

    .line 2594
    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/samsung/musicplus/service/PlayerService;->next(Z)V

    .line 2595
    const-string v3, "GEST"

    invoke-static {v10, v3}, Lcom/samsung/musicplus/util/logging/FeatureLogger;->insertLog(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2596
    :cond_2d
    if-nez v21, :cond_0

    .line 2597
    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/samsung/musicplus/service/PlayerService;->prev(Z)V

    .line 2598
    const-string v3, "GEST"

    invoke-static {v10, v3}, Lcom/samsung/musicplus/util/logging/FeatureLogger;->insertLog(Landroid/content/Context;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2600
    .end local v10    # "context":Landroid/content/Context;
    .end local v21    # "gesture":I
    :cond_2e
    const-string v3, "com.samsung.musicplus.intent.action.MUSIC_AUTO_OFF"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2f

    .line 2601
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/service/PlayerService;->pause()V

    .line 2602
    const-string v3, "MusicService"

    const-string v11, "Music Playing paused because of music auto off setting"

    invoke-static {v3, v11}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2604
    const-string v3, "music_auto_off"

    const-string v11, "completed"

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v11}, Lcom/samsung/musicplus/service/PlayerService;->savePreferences(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2605
    :cond_2f
    const-string v3, "com.android.music.settingchanged"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_30

    .line 2608
    const-string v3, "shuffle"

    const/4 v11, -0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v24

    .line 2609
    .local v24, "shuffle":I
    const/4 v3, -0x1

    move/from16 v0, v24

    if-eq v0, v3, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/service/PlayerService;->getShuffle()I

    move-result v3

    move/from16 v0, v24

    if-eq v3, v0, :cond_0

    .line 2610
    move-object/from16 v0, p0

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/service/PlayerService;->setShuffle(I)V

    goto/16 :goto_0

    .line 2612
    .end local v24    # "shuffle":I
    :cond_30
    const-string v3, "com.samsung.musicplus.musicservicecommand.shuffle"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_31

    .line 2613
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/service/PlayerService;->toggleShuffle()V

    goto/16 :goto_0

    .line 2614
    :cond_31
    const-string v3, "com.samsung.musicplus.musicservicecommand.repeat"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_32

    .line 2615
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/service/PlayerService;->toggleRepeat()V

    goto/16 :goto_0

    .line 2616
    :cond_32
    const-string v3, "volume_up"

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_33

    .line 2617
    new-instance v3, Ljava/lang/Thread;

    new-instance v11, Lcom/samsung/musicplus/service/PlayerService$20;

    move-object/from16 v0, p0

    invoke-direct {v11, v0}, Lcom/samsung/musicplus/service/PlayerService$20;-><init>(Lcom/samsung/musicplus/service/PlayerService;)V

    invoke-direct {v3, v11}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v3}, Ljava/lang/Thread;->start()V

    goto/16 :goto_0

    .line 2623
    :cond_33
    const-string v3, "volume_down"

    move-object/from16 v0, v16

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_34

    .line 2624
    new-instance v3, Ljava/lang/Thread;

    new-instance v11, Lcom/samsung/musicplus/service/PlayerService$21;

    move-object/from16 v0, p0

    invoke-direct {v11, v0}, Lcom/samsung/musicplus/service/PlayerService$21;-><init>(Lcom/samsung/musicplus/service/PlayerService;)V

    invoke-direct {v3, v11}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v3}, Ljava/lang/Thread;->start()V

    goto/16 :goto_0

    .line 2630
    :cond_34
    const-string v3, "com.samsung.musicplus.action.TOGGLE_FAVORITE"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_36

    .line 2631
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/service/PlayerService;->getAudioId()J

    move-result-wide v12

    .line 2632
    .local v12, "id":J
    const-wide/16 v14, 0x0

    cmp-long v3, v12, v14

    if-gez v3, :cond_35

    .line 2633
    const-string v3, "MusicService"

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v14, "handleCommandIntent : Toggle favourite song failed! - id : "

    invoke-virtual {v11, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v12, v13}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v3, v11}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 2636
    :cond_35
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/service/PlayerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v10

    .line 2637
    .restart local v10    # "context":Landroid/content/Context;
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/service/PlayerService;->getCurrentPath()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v12, v13}, Lcom/samsung/musicplus/util/UiUtils;->isFavorite(Landroid/content/Context;J)Z

    move-result v14

    const/4 v15, 0x0

    invoke-static/range {v10 .. v15}, Lcom/samsung/musicplus/util/FileOperationTask;->toggleFavorites(Landroid/content/Context;Ljava/lang/String;JZZ)Z

    goto/16 :goto_0

    .line 2639
    .end local v10    # "context":Landroid/content/Context;
    .end local v12    # "id":J
    :cond_36
    const-string v3, "com.samsung.musicplus.intent.action.CLEAR_COVER_MUSIC_UPDATE"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_37

    .line 2640
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/musicplus/service/PlayerService;->mSviewCoverReceiver:Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;

    if-eqz v3, :cond_0

    .line 2641
    const-string v3, "MusicService"

    const-string v11, "Sview cover service, ACTION_CLEAR_COVER_MUSIC_UPDATE got! update!"

    invoke-static {v3, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2643
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/musicplus/service/PlayerService;->mSviewCoverReceiver:Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/service/PlayerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v11

    const-string v14, "com.android.music.metachanged"

    move-object/from16 v0, p0

    invoke-virtual {v3, v11, v0, v14}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;->updateMusicCover(Landroid/content/Context;Lcom/samsung/musicplus/service/PlayerService;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2646
    :cond_37
    const-string v3, "com.sec.android.app.music.musicservicecommand.checkplaystatus"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_38

    .line 2647
    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/samsung/musicplus/service/PlayerService;->notifyMusicInfo(Z)V

    goto/16 :goto_0

    .line 2648
    :cond_38
    const-string v3, "com.sec.android.sidesync.source.SIDESYNC_CONNECTED"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_39

    .line 2649
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/service/PlayerService;->isPlaying()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2650
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/musicplus/service/PlayerService;->mToastHandler:Lcom/samsung/musicplus/service/PlayerService$ToastHandler;

    const v11, 0x7f10006e

    invoke-virtual {v3, v11}, Lcom/samsung/musicplus/service/PlayerService$ToastHandler;->showToast(I)V

    .line 2651
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/samsung/musicplus/service/PlayerService;->mEnableSideSyncToast:Z

    goto/16 :goto_0

    .line 2653
    :cond_39
    const-string v3, "com.sec.motions.ARC_MOTION_SETTINGS_CHANGED"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3c

    .line 2656
    const-string v3, "isEnable"

    const/4 v11, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v3, v11}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v22

    .line 2658
    .local v22, "isEnabled":Z
    if-eqz v22, :cond_3b

    .line 2659
    invoke-virtual/range {p0 .. p0}, Lcom/samsung/musicplus/service/PlayerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager;->isEnableMusicBounceSetting(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_3a

    .line 2660
    const/4 v3, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/samsung/musicplus/service/PlayerService;->handleBounceSetting(Z)V

    goto/16 :goto_0

    .line 2662
    :cond_3a
    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/samsung/musicplus/service/PlayerService;->handleBounceSetting(Z)V

    goto/16 :goto_0

    .line 2665
    :cond_3b
    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/samsung/musicplus/service/PlayerService;->handleBounceSetting(Z)V

    goto/16 :goto_0

    .line 2667
    .end local v22    # "isEnabled":Z
    :cond_3c
    const-string v3, "com.sec.android.app.music.intent.action.WRITE_LOG"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2669
    const-string v3, "AgingTest"

    const-string v11, "Disabled AGING_LOG_WRITER feature"

    invoke-static {v3, v11}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method private handleMetaEdited()V
    .locals 2

    .prologue
    .line 937
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v1}, Lcom/samsung/musicplus/service/PlayerListManager;->updateMediaInfo()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 938
    const-string v1, "com.samsung.musicplus.action.META_EDITED"

    invoke-direct {p0, v1}, Lcom/samsung/musicplus/service/PlayerService;->notifyChange(Ljava/lang/String;)V

    .line 939
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->parsingAlbumArt()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 940
    .local v0, "bm":Landroid/graphics/Bitmap;
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mRemoteViewBuilder:Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mRemoteBigViewBuilder:Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mIsShowNotification:Z

    if-eqz v1, :cond_0

    .line 941
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->updateNotificationTitle()V

    .line 942
    invoke-direct {p0, v0}, Lcom/samsung/musicplus/service/PlayerService;->updateNotificationAlbumArt(Landroid/graphics/Bitmap;)V

    .line 944
    :cond_0
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mControlClient:Landroid/media/RemoteControlClient;

    if-eqz v1, :cond_2

    .line 945
    invoke-direct {p0, v0}, Lcom/samsung/musicplus/service/PlayerService;->updateRemoteClient(Landroid/graphics/Bitmap;)V

    .line 950
    .end local v0    # "bm":Landroid/graphics/Bitmap;
    :cond_1
    :goto_0
    return-void

    .line 947
    .restart local v0    # "bm":Landroid/graphics/Bitmap;
    :cond_2
    invoke-direct {p0, v0}, Lcom/samsung/musicplus/service/PlayerService;->updateMediaSessionMeta(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method private hideNotification()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 5096
    const-string v0, "MusicService"

    const-string v1, "hideNotification()"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 5097
    iget-boolean v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mIsShowNotification:Z

    if-eqz v0, :cond_0

    .line 5098
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.android.app.music.intent.action.HIDE_CONTEXTUAL_WIDGET"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/service/PlayerService;->sendBroadcast(Landroid/content/Intent;)V

    .line 5103
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mControlClient:Landroid/media/RemoteControlClient;

    if-eqz v0, :cond_3

    .line 5104
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mControlClient:Landroid/media/RemoteControlClient;

    invoke-virtual {v0, v4}, Landroid/media/RemoteControlClient;->setPlaybackState(I)V

    .line 5119
    :cond_1
    :goto_0
    invoke-virtual {p0, v4}, Lcom/samsung/musicplus/service/PlayerService;->stopForeground(Z)V

    .line 5120
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mIsShowNotification:Z

    .line 5121
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mNotiManager:Landroid/app/NotificationManager;

    if-eqz v0, :cond_2

    .line 5122
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mNotifyHandler:Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 5123
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/service/PlayerService;->isNoxMode(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 5124
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mNotiManager:Landroid/app/NotificationManager;

    const v1, 0x7010003

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 5130
    :cond_2
    :goto_1
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->cancelServiceTimeOut()V

    .line 5131
    return-void

    .line 5108
    :cond_3
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mMediaSession:Landroid/media/session/MediaSession;

    if-eqz v0, :cond_1

    .line 5110
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mMediaStateBuilder:Landroid/media/session/PlaybackState$Builder;

    const-wide/16 v2, -0x1

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v4, v2, v3, v1}, Landroid/media/session/PlaybackState$Builder;->setState(IJF)Landroid/media/session/PlaybackState$Builder;

    .line 5112
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mMediaSession:Landroid/media/session/MediaSession;

    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mMediaStateBuilder:Landroid/media/session/PlaybackState$Builder;

    invoke-virtual {v1}, Landroid/media/session/PlaybackState$Builder;->build()Landroid/media/session/PlaybackState;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/media/session/MediaSession;->setPlaybackState(Landroid/media/session/PlaybackState;)V

    goto :goto_0

    .line 5126
    :cond_4
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mNotiManager:Landroid/app/NotificationManager;

    const v1, 0x7010002

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    goto :goto_1
.end method

.method private isActiveSmartVolume()Z
    .locals 3

    .prologue
    .line 5284
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mPreferences:Landroid/content/SharedPreferences;

    const-string v1, "smart_volume"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private isConnectedHDMI()Z
    .locals 1

    .prologue
    .line 5036
    iget-boolean v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mIsHdmiConnected:Z

    return v0
.end method

.method private isConnectedWfd()Z
    .locals 4

    .prologue
    .line 5029
    const-string v2, "display"

    invoke-virtual {p0, v2}, Lcom/samsung/musicplus/service/PlayerService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/display/DisplayManager;

    .line 5030
    .local v0, "dm":Landroid/hardware/display/DisplayManager;
    invoke-static {v0}, Lcom/samsung/musicplus/library/wifi/DisplayManagerCompat;->getWifiDisplayStatus(Landroid/hardware/display/DisplayManager;)Lcom/samsung/musicplus/library/wifi/WifiDisplayStatusCompat;

    move-result-object v1

    .line 5031
    .local v1, "status":Lcom/samsung/musicplus/library/wifi/WifiDisplayStatusCompat;
    if-eqz v1, :cond_0

    const/4 v2, 0x2

    invoke-virtual {v1}, Lcom/samsung/musicplus/library/wifi/WifiDisplayStatusCompat;->getActiveDisplayState()I

    move-result v3

    if-ne v2, v3, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private isDlnaTrackList()Z
    .locals 2

    .prologue
    .line 4344
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerListManager;->getListType()I

    move-result v0

    const v1, 0x2000b

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isDmrPlaying()Z
    .locals 1

    .prologue
    .line 4380
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getDlnaPlayingDmrId()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isEnableAdaptSoundPath()Z
    .locals 1

    .prologue
    .line 998
    sget-boolean v0, Lcom/samsung/musicplus/util/Version;->LIBRARY_OVER_API_1:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

    invoke-virtual {v0}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->isWiredHeadsetOn()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

    invoke-virtual {v0}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->isAudioPathBT()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

    invoke-virtual {v0}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->isWiredHeadsetOn()Z

    move-result v0

    goto :goto_0
.end method

.method private isEnableGesture()Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 3519
    sget-boolean v3, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_SUPPORT_GESTURE_AIR_MOTION:Z

    if-eqz v3, :cond_4

    .line 3524
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getListItemCount()I

    move-result v3

    if-gt v3, v2, :cond_0

    .line 3552
    :goto_0
    return v1

    .line 3528
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 3529
    .local v0, "context":Landroid/content/Context;
    invoke-static {v0}, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;->isEnableAirBrowse(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 3530
    invoke-static {v0}, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;->isEnableAirBrowseOnLockScreen(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 3531
    invoke-static {v0}, Lcom/samsung/musicplus/util/UiUtils;->isLockScreenOn(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-static {v0}, Lcom/samsung/musicplus/util/UiUtils;->isScreenOn(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 3534
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->ensureGestureManager()V

    .line 3535
    const-string v1, "MusicService"

    const-string v3, "Gesture enabled"

    invoke-static {v1, v3}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v1, v2

    .line 3536
    goto :goto_0

    .line 3538
    :cond_1
    const-string v2, "MusicService"

    const-string v3, "Gesture disabled GUiUtils.isLockScreenOn(context) && UiUtils.isScreenOn(context) is false"

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 3542
    :cond_2
    const-string v2, "MusicService"

    const-string v3, "Gesture disabled SimpleGestureManager.isEnableAirBrowseOnLockScreen(...) is false"

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 3546
    :cond_3
    const-string v2, "MusicService"

    const-string v3, "Gesture disabled SimpleGestureManager.isEnableAirBrowse(...) is false"

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 3549
    .end local v0    # "context":Landroid/content/Context;
    :cond_4
    const-string v2, "MusicService"

    const-string v3, "Gesture disabled MusicStaticFeatures.FLAG_SUPPORT_GESTURE_AIR_MOTION is false"

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private isExtraTrack()Z
    .locals 2

    .prologue
    .line 2848
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerListManager;->getListType()I

    move-result v0

    const v1, 0x2000f

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isGoingToStopPacakge()Z
    .locals 1

    .prologue
    .line 4402
    iget-boolean v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mIsStopPacakgeAfterDestroyed:Z

    return v0
.end method

.method private isMidi()Z
    .locals 5

    .prologue
    .line 4461
    const/4 v1, 0x0

    .line 4462
    .local v1, "isMidi":Z
    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v2}, Lcom/samsung/musicplus/service/PlayerListManager;->getMime()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/musicplus/library/audio/MediaFileCompat;->getFileType(Ljava/lang/String;)I

    move-result v0

    .line 4463
    .local v0, "fileType":I
    const/16 v2, 0xe

    if-eq v0, v2, :cond_0

    const/16 v2, 0xf

    if-eq v0, v2, :cond_0

    const/16 v2, 0x10

    if-ne v0, v2, :cond_1

    .line 4466
    :cond_0
    const/4 v1, 0x1

    .line 4468
    :cond_1
    const-string v2, "MusicService"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "isMidi() "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " fileType : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 4469
    return v1
.end method

.method private isNoxMode(Landroid/content/Context;)Z
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 5056
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mIsNoxMode:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    .line 5057
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/musicplus/util/KnoxGateManager;->isKnoxModeOn(Landroid/content/Context;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mIsNoxMode:Ljava/lang/Boolean;

    .line 5059
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mIsNoxMode:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method private isOtherContnetsList()Z
    .locals 1

    .prologue
    .line 2826
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->isDlnaTrackList()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->isSlinkTrackList()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->isExtraTrack()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isSlinkTrackList()Z
    .locals 2

    .prologue
    .line 4348
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerListManager;->getListType()I

    move-result v0

    const v1, 0x2000d

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isStop()Z
    .locals 1

    .prologue
    .line 3473
    iget-boolean v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mIsShowNotification:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/MultiPlayer;->isStop()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isSupportForward()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 3893
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->isDmrPlaying()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3897
    const v1, 0x7f1001b9

    invoke-direct {p0, v1}, Lcom/samsung/musicplus/service/PlayerService;->showToast(I)V

    .line 3908
    :goto_0
    return v0

    .line 3900
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->isLocalMediaTrack()Z

    move-result v1

    if-nez v1, :cond_1

    .line 3905
    const v1, 0x7f10010f

    invoke-direct {p0, v1}, Lcom/samsung/musicplus/service/PlayerService;->showToast(I)V

    goto :goto_0

    .line 3908
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private isSupportRewind()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 3912
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->isDmrPlaying()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3916
    const v1, 0x7f1001b9

    invoke-direct {p0, v1}, Lcom/samsung/musicplus/service/PlayerService;->showToast(I)V

    .line 3927
    :goto_0
    return v0

    .line 3919
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->isLocalMediaTrack()Z

    move-result v1

    if-nez v1, :cond_1

    .line 3924
    const v1, 0x7f100110

    invoke-direct {p0, v1}, Lcom/samsung/musicplus/service/PlayerService;->showToast(I)V

    goto :goto_0

    .line 3927
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private loadNewSoundAliveAuto()Z
    .locals 3

    .prologue
    .line 1917
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mPreferences:Landroid/content/SharedPreferences;

    const-string v1, "sa_auto_check"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private loadNewSoundAliveBandLevel()[I
    .locals 9

    .prologue
    const/4 v8, 0x7

    .line 1881
    new-array v1, v8, [I

    .line 1882
    .local v1, "currentBandLevel":[I
    iget-object v5, p0, Lcom/samsung/musicplus/service/PlayerService;->mPreferences:Landroid/content/SharedPreferences;

    const-string v6, "sa_band_level"

    const-string v7, "0|0|0|0|0|0|0|"

    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1884
    .local v0, "bandLevel":Ljava/lang/String;
    new-instance v3, Ljava/util/StringTokenizer;

    const-string v5, "|"

    invoke-direct {v3, v0, v5}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1886
    .local v3, "strToken":Ljava/util/StringTokenizer;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v8, :cond_1

    .line 1887
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->hasMoreElements()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1888
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->nextElement()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1889
    .local v4, "token":Ljava/lang/String;
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    aput v5, v1, v2

    .line 1886
    .end local v4    # "token":Ljava/lang/String;
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1891
    :cond_0
    const/4 v5, 0x0

    aput v5, v1, v2

    goto :goto_1

    .line 1894
    :cond_1
    return-object v1
.end method

.method private loadNewSoundAliveSquarePosition()[I
    .locals 9

    .prologue
    const/4 v8, 0x2

    .line 1864
    new-array v0, v8, [I

    .line 1865
    .local v0, "currentSquarePosition":[I
    iget-object v5, p0, Lcom/samsung/musicplus/service/PlayerService;->mPreferences:Landroid/content/SharedPreferences;

    const-string v6, "sa_square_position"

    const-string v7, "2|2|"

    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1867
    .local v2, "squarePosition":Ljava/lang/String;
    new-instance v3, Ljava/util/StringTokenizer;

    const-string v5, "|"

    invoke-direct {v3, v2, v5}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1869
    .local v3, "strToken":Ljava/util/StringTokenizer;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v8, :cond_1

    .line 1870
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->hasMoreElements()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1871
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->nextElement()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1872
    .local v4, "token":Ljava/lang/String;
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    aput v5, v0, v1

    .line 1869
    .end local v4    # "token":Ljava/lang/String;
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1874
    :cond_0
    const/4 v5, 0x0

    aput v5, v0, v1

    goto :goto_1

    .line 1877
    :cond_1
    return-object v0
.end method

.method private loadNewSoundAliveStrength()[I
    .locals 9

    .prologue
    const/4 v8, 0x3

    .line 1898
    new-array v0, v8, [I

    .line 1899
    .local v0, "currentStrength":[I
    iget-object v5, p0, Lcom/samsung/musicplus/service/PlayerService;->mPreferences:Landroid/content/SharedPreferences;

    const-string v6, "sa_use_strength"

    const-string v7, "0|0|0|"

    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 1901
    .local v3, "strength":Ljava/lang/String;
    new-instance v2, Ljava/util/StringTokenizer;

    const-string v5, "|"

    invoke-direct {v2, v3, v5}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1903
    .local v2, "strToken":Ljava/util/StringTokenizer;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v8, :cond_1

    .line 1904
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->hasMoreElements()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1905
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextElement()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1906
    .local v4, "token":Ljava/lang/String;
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    aput v5, v0, v1

    .line 1903
    .end local v4    # "token":Ljava/lang/String;
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1908
    :cond_0
    const/4 v5, 0x0

    aput v5, v0, v1

    goto :goto_1

    .line 1911
    :cond_1
    return-object v0
.end method

.method private loadNewSoundAliveUsePreset()I
    .locals 3

    .prologue
    .line 1859
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mPreferences:Landroid/content/SharedPreferences;

    const-string v1, "sa_use_preset_effect"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method private loadNewSoundAliveValues()V
    .locals 7

    .prologue
    .line 1850
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->loadNewSoundAliveAuto()Z

    move-result v1

    .line 1851
    .local v1, "auto":Z
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->loadNewSoundAliveUsePreset()I

    move-result v2

    .line 1852
    .local v2, "presetEffect":I
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->loadNewSoundAliveSquarePosition()[I

    move-result-object v3

    .line 1854
    .local v3, "squarePosition":[I
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->loadNewSoundAliveStrength()[I

    move-result-object v4

    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->loadNewSoundAliveBandLevel()[I

    move-result-object v5

    const/4 v6, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/samsung/musicplus/service/PlayerService;->setSoundAliveV2(ZI[I[I[IZ)V

    .line 1856
    return-void
.end method

.method private loadSavedValues()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1932
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mPreferences:Landroid/content/SharedPreferences;

    const-string v1, "shuffle"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mPreferences:Landroid/content/SharedPreferences;

    const-string v2, "repeat"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/samsung/musicplus/service/PlayerService;->setShuffleAndRepeat(II)V

    .line 1935
    sget-boolean v0, Lcom/samsung/musicplus/util/Features;->FLAG_SUPPORT_EXTERNAL_SOUNDALIVE:Z

    if-nez v0, :cond_0

    .line 1937
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->loadNewSoundAliveValues()V

    .line 1944
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mPreferences:Landroid/content/SharedPreferences;

    const-string v1, "play_speed"

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/samsung/musicplus/service/PlayerService;->setPlaySpeed(FZ)V

    .line 1947
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mPreferences:Landroid/content/SharedPreferences;

    const-string v1, "smart_volume"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/service/PlayerService;->setSmartVolume(Z)V

    .line 1950
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mPreferences:Landroid/content/SharedPreferences;

    const-string v1, "K2HD"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/service/PlayerService;->setK2HD(Z)V

    .line 1953
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/musicplus/library/audio/AdaptSound;->getAdaptSoundOn(Landroid/content/Context;)Z

    move-result v0

    invoke-virtual {p0, v0, v3}, Lcom/samsung/musicplus/service/PlayerService;->setAdaptSound(ZZ)V

    .line 1954
    return-void
.end method

.method private loadSoundAliveUserEQ()[I
    .locals 9

    .prologue
    const/4 v8, 0x7

    .line 1968
    new-array v0, v8, [I

    .line 1969
    .local v0, "eq":[I
    iget-object v5, p0, Lcom/samsung/musicplus/service/PlayerService;->mPreferences:Landroid/content/SharedPreferences;

    const-string v6, "user_eq"

    const-string v7, "0|0|0|0|0|0|0|"

    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1970
    .local v2, "savedEq":Ljava/lang/String;
    new-instance v3, Ljava/util/StringTokenizer;

    const-string v5, "|"

    invoke-direct {v3, v2, v5}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1972
    .local v3, "strToken":Ljava/util/StringTokenizer;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v8, :cond_1

    .line 1973
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->hasMoreElements()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1974
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->nextElement()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1975
    .local v4, "token":Ljava/lang/String;
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    aput v5, v0, v1

    .line 1972
    .end local v4    # "token":Ljava/lang/String;
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1977
    :cond_0
    const/4 v5, 0x0

    aput v5, v0, v1

    goto :goto_1

    .line 1980
    :cond_1
    return-object v0
.end method

.method private loadSoundAliveUserExt()[I
    .locals 9

    .prologue
    const/4 v8, 0x5

    .line 1984
    new-array v0, v8, [I

    .line 1985
    .local v0, "extraEffect":[I
    iget-object v5, p0, Lcom/samsung/musicplus/service/PlayerService;->mPreferences:Landroid/content/SharedPreferences;

    const-string v6, "user_ext"

    const-string v7, "0|0|0|0|0|"

    invoke-interface {v5, v6, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1986
    .local v2, "savedEffect":Ljava/lang/String;
    new-instance v3, Ljava/util/StringTokenizer;

    const-string v5, "|"

    invoke-direct {v3, v2, v5}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1988
    .local v3, "strToken":Ljava/util/StringTokenizer;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v8, :cond_1

    .line 1989
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->hasMoreElements()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1990
    invoke-virtual {v3}, Ljava/util/StringTokenizer;->nextElement()Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1991
    .local v4, "token":Ljava/lang/String;
    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    aput v5, v0, v1

    .line 1988
    .end local v4    # "token":Ljava/lang/String;
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1993
    :cond_0
    const/4 v5, 0x0

    aput v5, v0, v1

    goto :goto_1

    .line 1996
    :cond_1
    return-object v0
.end method

.method private loadSoundAliveValue()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1958
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mPreferences:Landroid/content/SharedPreferences;

    const-string v2, "sound_alive"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 1960
    .local v0, "soundAlive":I
    const/16 v1, 0xd

    if-ne v0, v1, :cond_0

    .line 1961
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->loadSoundAliveUserEQ()[I

    move-result-object v1

    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->loadSoundAliveUserExt()[I

    move-result-object v2

    invoke-virtual {p0, v1, v2, v3}, Lcom/samsung/musicplus/service/PlayerService;->setSoundAliveUser([I[IZ)V

    .line 1965
    :goto_0
    return-void

    .line 1963
    :cond_0
    invoke-virtual {p0, v0, v3}, Lcom/samsung/musicplus/service/PlayerService;->setSoundAlive(IZ)V

    goto :goto_0
.end method

.method private nextInternal(Z)V
    .locals 4
    .param p1, "ignoreRepeatOne"    # Z

    .prologue
    .line 4007
    monitor-enter p0

    .line 4008
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/samsung/musicplus/service/PlayerService;->bErrorHandleToPrev:Z

    .line 4009
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/service/PlayerListManager;->moveToNext(Z)Z

    .line 4010
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->isPlaying()Z

    move-result v0

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v2, v3, v1}, Lcom/samsung/musicplus/service/PlayerService;->openCurrent(ZJZ)V

    .line 4011
    monitor-exit p0

    .line 4012
    return-void

    .line 4011
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private notifyChange(Ljava/lang/String;)V
    .locals 4
    .param p1, "what"    # Ljava/lang/String;

    .prologue
    .line 4486
    const-string v1, "MusicService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "notifyChange() "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 4487
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 4488
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "base_uri"

    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getCurrentBaseUri()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 4489
    const-string v1, "id"

    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getAudioId()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 4491
    const-string v1, "com.android.music.metachanged"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4492
    const-string v1, "artist"

    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getArtist()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 4493
    const-string v1, "album"

    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getAlbum()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 4494
    const-string v1, "albumId"

    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getAlbumId()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 4495
    const-string v1, "track"

    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 4496
    const-string v1, "playing"

    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->isPlaying()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 4497
    const-string v1, "mediaCount"

    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getListItemCount()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 4500
    const-string v1, "listPosition"

    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getListPosition()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 4503
    :cond_0
    const-string v1, "com.android.music.playstatechanged"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 4504
    const-string v1, "playing"

    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->isPlaying()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 4505
    const-string v1, "trackLength"

    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->duration()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 4506
    const-string v1, "position"

    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->position()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 4507
    const-string v1, "artist"

    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getArtist()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 4510
    :cond_1
    const-string v1, "com.samsung.musicplus.action.QUEUE_CHANGED"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 4514
    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/service/PlayerService;->sendStickyBroadcast(Landroid/content/Intent;)V

    .line 4515
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Lcom/samsung/musicplus/service/PlayerService;->saveQueue(Z)V

    .line 4521
    :goto_0
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mServiceAppWidggetManger:Lcom/samsung/musicplus/widget/appwidget/ServiceAppWidgetManager;

    invoke-virtual {v1, p1}, Lcom/samsung/musicplus/widget/appwidget/ServiceAppWidgetManager;->performUpdate(Ljava/lang/String;)V

    .line 4522
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mSviewCoverReceiver:Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;

    if-eqz v1, :cond_2

    .line 4523
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mSviewCoverReceiver:Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;

    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2, p0, p1}, Lcom/samsung/musicplus/service/sviewcover/SviewCoverReceiver;->updateMusicCover(Landroid/content/Context;Lcom/samsung/musicplus/service/PlayerService;Ljava/lang/String;)V

    .line 4525
    :cond_2
    return-void

    .line 4517
    :cond_3
    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/service/PlayerService;->sendStickyBroadcast(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private notifyMusicInfo(Z)V
    .locals 6
    .param p1, "isStopped"    # Z

    .prologue
    .line 4528
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->isDlnaTrackList()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->isSlinkTrackList()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 4533
    :cond_0
    const-string v3, "MusicService"

    const-string v4, "notifyMusicInfo() - return due to Other CP contents playing."

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 4551
    :goto_0
    return-void

    .line 4537
    :cond_1
    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.sec.android.music.musicservicecommnad.mediainfo"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 4540
    .local v2, "i":Landroid/content/Intent;
    :try_start_0
    const-string v3, "uri"

    iget-object v4, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v4}, Lcom/samsung/musicplus/service/PlayerListManager;->getCurrentUri()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 4541
    const-string v4, "isPlaying"

    if-eqz p1, :cond_2

    const/4 v3, 0x0

    :goto_1
    invoke-virtual {v2, v4, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 4542
    const-string v3, "isStopped"

    invoke-virtual {v2, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 4543
    const-string v3, "artist"

    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getArtist()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 4545
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getAudioId()J

    move-result-wide v4

    invoke-static {v3, v4, v5}, Lcom/samsung/musicplus/util/UiUtils;->isFavorite(Landroid/content/Context;J)Z

    move-result v1

    .line 4546
    .local v1, "favorite":Z
    const-string v3, "isfavorite"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 4547
    invoke-virtual {p0, v2}, Lcom/samsung/musicplus/service/PlayerService;->sendStickyBroadcast(Landroid/content/Intent;)V
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 4548
    .end local v1    # "favorite":Z
    :catch_0
    move-exception v0

    .line 4549
    .local v0, "e":Ljava/lang/ClassCastException;
    invoke-virtual {v0}, Ljava/lang/ClassCastException;->printStackTrace()V

    goto :goto_0

    .line 4541
    .end local v0    # "e":Ljava/lang/ClassCastException;
    :cond_2
    :try_start_1
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->isPlaying()Z
    :try_end_1
    .catch Ljava/lang/ClassCastException; {:try_start_1 .. :try_end_1} :catch_0

    move-result v3

    goto :goto_1
.end method

.method private notifyNotification()V
    .locals 4

    .prologue
    .line 5063
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->isGoingToStopPacakge()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 5064
    const-string v1, "MusicService"

    const-string v2, "Music service is going to destroy so don\'t notify to the notification"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 5079
    :cond_0
    :goto_0
    return-void

    .line 5068
    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mNotiManager:Landroid/app/NotificationManager;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mNotification:Landroid/app/Notification;

    if-eqz v1, :cond_0

    .line 5069
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/samsung/musicplus/service/PlayerService;->isNoxMode(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 5070
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mNotiManager:Landroid/app/NotificationManager;

    const v2, 0x7010003

    iget-object v3, p0, Lcom/samsung/musicplus/service/PlayerService;->mNotification:Landroid/app/Notification;

    invoke-virtual {v1, v2, v3}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 5075
    :catch_0
    move-exception v0

    .line 5077
    .local v0, "e":Ljava/lang/IllegalStateException;
    const-string v1, "MusicService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "updateNotificationAlbumArt : IllegalStateException occurred"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 5072
    .end local v0    # "e":Ljava/lang/IllegalStateException;
    :cond_2
    :try_start_1
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mNotiManager:Landroid/app/NotificationManager;

    const v2, 0x7010002

    iget-object v3, p0, Lcom/samsung/musicplus/service/PlayerService;->mNotification:Landroid/app/Notification;

    invoke-virtual {v1, v2, v3}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method private notifySettingChange()V
    .locals 3

    .prologue
    .line 4710
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.music.settingchanged"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 4711
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "from"

    const-string v2, "MusicPlayer"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 4712
    const-string v1, "repeat"

    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getRepeat()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 4713
    const-string v1, "shuffle"

    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getShuffle()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 4714
    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/service/PlayerService;->sendBroadcast(Landroid/content/Intent;)V

    .line 4715
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mServiceAppWidggetManger:Lcom/samsung/musicplus/widget/appwidget/ServiceAppWidgetManager;

    const-string v2, "com.android.music.settingchanged"

    invoke-virtual {v1, v2}, Lcom/samsung/musicplus/widget/appwidget/ServiceAppWidgetManager;->performUpdate(Ljava/lang/String;)V

    .line 4717
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->notifySettingToRemoteClient()V

    .line 4718
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->updateSettingToNotification()V

    .line 4719
    return-void
.end method

.method private notifySettingToRemoteClient()V
    .locals 4

    .prologue
    .line 4722
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mControlClient:Landroid/media/RemoteControlClient;

    if-eqz v1, :cond_0

    .line 4723
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mControlClient:Landroid/media/RemoteControlClient;

    const/4 v2, 0x2

    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getRepeat()I

    move-result v3

    invoke-static {v1, v2, v3}, Lcom/samsung/musicplus/library/audio/RemoteControlClientCompat;->setCommandToDisplay(Landroid/media/RemoteControlClient;II)V

    .line 4725
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mControlClient:Landroid/media/RemoteControlClient;

    const/4 v2, 0x3

    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getShuffle()I

    move-result v3

    invoke-static {v1, v2, v3}, Lcom/samsung/musicplus/library/audio/RemoteControlClientCompat;->setCommandToDisplay(Landroid/media/RemoteControlClient;II)V

    .line 4733
    :goto_0
    return-void

    .line 4728
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 4729
    .local v0, "data":Landroid/os/Bundle;
    const-string v1, "repeat"

    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getRepeat()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 4730
    const-string v1, "shuffle"

    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getShuffle()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 4731
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mMediaSession:Landroid/media/session/MediaSession;

    const-string v2, "com.samsung.android.bt.AVRCP"

    invoke-virtual {v1, v2, v0}, Landroid/media/session/MediaSession;->sendSessionEvent(Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method private open(Z)V
    .locals 3
    .param p1, "play"    # Z

    .prologue
    .line 2964
    const-wide/16 v0, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/samsung/musicplus/service/PlayerService;->openCurrent(ZJZ)V

    .line 2965
    return-void
.end method

.method private open([JIZ)V
    .locals 3
    .param p1, "list"    # [J
    .param p2, "position"    # I
    .param p3, "play"    # Z

    .prologue
    .line 2947
    const-string v0, "MusicService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "open position : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " play : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2948
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->resetListPreset()V

    .line 2949
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/musicplus/service/PlayerListManager;->setList([JI)V

    .line 2950
    invoke-direct {p0, p3}, Lcom/samsung/musicplus/service/PlayerService;->open(Z)V

    .line 2951
    return-void
.end method

.method private openCurrent(ZJZ)V
    .locals 10
    .param p1, "play"    # Z
    .param p2, "position"    # J
    .param p4, "byUser"    # Z

    .prologue
    .line 3141
    monitor-enter p0

    .line 3143
    :try_start_0
    const-string v0, "MusicService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "openCurrent() set byUser as true by force, because I have doubt byUser parameter the real value is  : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3146
    const/4 p4, 0x1

    .line 3148
    const-string v0, "MusicService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "open play : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3154
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerListManager;->updateMediaInfo()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 3155
    const-string v0, "MusicService"

    const-string v1, "Current media info is normal, so try to setDataSource"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3156
    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mOpenUriFailed:I

    .line 3158
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->isPlaying()Z

    move-result v7

    .line 3159
    .local v7, "wasPlaying":Z
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerListManager;->getFilePath()Ljava/lang/String;

    move-result-object v2

    .line 3160
    .local v2, "path":Ljava/lang/String;
    const-string v0, "MusicService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "open path : "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3162
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v1}, Lcom/samsung/musicplus/service/PlayerListManager;->getCurrentUri()Landroid/net/Uri;

    move-result-object v1

    move v3, p1

    move-wide v4, p2

    move v6, p4

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/musicplus/service/MultiPlayer;->setDataSource(Landroid/net/Uri;Ljava/lang/String;ZJZ)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 3165
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->getMaxVolumeRatio()F

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mMaxVolume:F

    .line 3166
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

    iget v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mMaxVolume:F

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/service/MultiPlayer;->setVolume(F)V

    .line 3170
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mNotifyHandler:Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;->notifyMetaChanged()V

    .line 3171
    if-eq v7, p1, :cond_0

    .line 3172
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mNotifyHandler:Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;->notifyPlayStatusChanged()V

    .line 3174
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getCurrentUri()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 3175
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getCurrentUri()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/service/PlayerService;->registerCurrentSongObserver(Landroid/net/Uri;)V

    .line 3177
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/service/PlayerService;->bErrorHandleToPrev:Z

    .line 3185
    :goto_0
    iget-boolean v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mFirstLoader:Z

    if-nez v0, :cond_2

    .line 3187
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mNotifyHandler:Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;->updateNotificationAllInfo()V

    .line 3209
    .end local v2    # "path":Ljava/lang/String;
    .end local v7    # "wasPlaying":Z
    :cond_2
    :goto_1
    monitor-exit p0

    .line 3210
    :goto_2
    return-void

    .line 3181
    .restart local v2    # "path":Ljava/lang/String;
    .restart local v7    # "wasPlaying":Z
    :cond_3
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mNotifyHandler:Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;->notifyMetaChanged()V

    .line 3182
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mNotifyHandler:Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;->notifyPlayStatusChanged()V

    .line 3183
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mNotifyHandler:Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;->notifyPrepareCompleted()V

    goto :goto_0

    .line 3209
    .end local v2    # "path":Ljava/lang/String;
    .end local v7    # "wasPlaying":Z
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    .line 3191
    :cond_4
    :try_start_1
    const-string v0, "MusicService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Current media info is abnormal, it looks current uri is wrong "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 3192
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getListItemCount()I

    move-result v0

    if-nez v0, :cond_5

    .line 3193
    const-string v0, "MusicService"

    const-string v1, ", but list item count is 0 ! Do stop remove loof!"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 3194
    const-string v0, "com.android.music.metachanged"

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/service/PlayerService;->notifyChange(Ljava/lang/String;)V

    .line 3195
    const-string v0, "com.samsung.musicplus.action.QUEUE_CHANGED"

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/service/PlayerService;->notifyChange(Ljava/lang/String;)V

    .line 3196
    monitor-exit p0

    goto :goto_2

    .line 3199
    :cond_5
    iget v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mOpenUriFailed:I

    const/16 v1, 0x14

    if-le v0, v1, :cond_6

    .line 3202
    monitor-exit p0

    goto :goto_2

    .line 3204
    :cond_6
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getAudioId()J

    move-result-wide v8

    .line 3205
    .local v8, "id":J
    iget v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mOpenUriFailed:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mOpenUriFailed:I

    .line 3206
    invoke-direct {p0, p4}, Lcom/samsung/musicplus/service/PlayerService;->errorHandleToPrevNext(Z)V

    .line 3207
    invoke-virtual {p0, v8, v9}, Lcom/samsung/musicplus/service/PlayerService;->removeTrack(J)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method private openInternal(Z)V
    .locals 3
    .param p1, "play"    # Z

    .prologue
    .line 2960
    const-wide/16 v0, 0x0

    const/4 v2, 0x0

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/samsung/musicplus/service/PlayerService;->openCurrent(ZJZ)V

    .line 2961
    return-void
.end method

.method private parsingAlbumArt()Landroid/graphics/Bitmap;
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 4884
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mAlbumArtHandler:Landroid/os/Handler;

    invoke-virtual {v0, v8}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 4886
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerListManager;->getListType()I

    move-result v2

    .line 4887
    .local v2, "listType":I
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerListManager;->getAlbumId()J

    move-result-wide v6

    .line 4896
    .local v6, "id":J
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c00da

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 4898
    .local v4, "size":I
    invoke-static {v2}, Lcom/samsung/musicplus/util/ListUtils;->isDefaultMusicContent(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4899
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v6, v7, v4, v4}, Lcom/samsung/musicplus/util/AlbumArtUtils;->getArtworkQuick(Landroid/content/Context;JII)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 4904
    :goto_0
    return-object v0

    .line 4902
    :cond_0
    invoke-static {}, Lcom/samsung/musicplus/util/AlbumArtLoader;->getAlbumArtLoader()Lcom/samsung/musicplus/util/IAlbumArtLoader;

    move-result-object v0

    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iget-object v5, p0, Lcom/samsung/musicplus/service/PlayerService;->mAlbumArtHandler:Landroid/os/Handler;

    invoke-interface/range {v0 .. v5}, Lcom/samsung/musicplus/util/IAlbumArtLoader;->loadArtwork(Landroid/content/Context;ILjava/lang/Object;ILandroid/os/Handler;)V

    move-object v0, v8

    .line 4904
    goto :goto_0
.end method

.method private prev(J)V
    .locals 3
    .param p1, "position"    # J

    .prologue
    .line 4034
    monitor-enter p0

    .line 4035
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/samsung/musicplus/service/PlayerService;->bErrorHandleToPrev:Z

    .line 4036
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerListManager;->moveToPrev()V

    .line 4037
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->isPlaying()Z

    move-result v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, p1, p2, v1}, Lcom/samsung/musicplus/service/PlayerService;->openCurrent(ZJZ)V

    .line 4038
    monitor-exit p0

    .line 4039
    return-void

    .line 4038
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private prevInternal()V
    .locals 4

    .prologue
    .line 4042
    monitor-enter p0

    .line 4043
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/samsung/musicplus/service/PlayerService;->bErrorHandleToPrev:Z

    .line 4044
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerListManager;->moveToPrev()V

    .line 4045
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->isPlaying()Z

    move-result v0

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v2, v3, v1}, Lcom/samsung/musicplus/service/PlayerService;->openCurrent(ZJZ)V

    .line 4046
    monitor-exit p0

    .line 4047
    return-void

    .line 4046
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private registerBounceListener()V
    .locals 2

    .prologue
    .line 3609
    sget-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_MOTION_BOUNCE:Z

    if-eqz v0, :cond_0

    .line 3610
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->ensureBounceManager()V

    .line 3611
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mBounceMotionManager:Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager;

    invoke-virtual {v0}, Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager;->registerListener()V

    .line 3612
    const-string v0, "MusicService"

    const-string v1, "registerBounceListener"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3614
    :cond_0
    return-void
.end method

.method private registerCommandReceiver()V
    .locals 2

    .prologue
    .line 2303
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 2304
    .local v0, "f":Landroid/content/IntentFilter;
    const-string v1, "com.samsung.musicplus.musicservicecommand.togglepause"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2305
    const-string v1, "com.samsung.musicplus.musicservicecommand.pause"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2306
    const-string v1, "com.samsung.musicplus.musicservicecommand.next"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2307
    const-string v1, "com.samsung.musicplus.musicservicecommand.previous"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2308
    const-string v1, "com.samsung.musicplus.musicservicecommand.ff.down"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2309
    const-string v1, "com.samsung.musicplus.musicservicecommand.ff.up"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2310
    const-string v1, "com.android.music.musicservicecommand"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2311
    const-string v1, "com.samsung.musicplus.musicservicecommand.rew.down"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2312
    const-string v1, "com.samsung.musicplus.musicservicecommand.rew.up"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2313
    const-string v1, "com.samsung.musicplus.action.PLAYBACK_FORWARD"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2314
    const-string v1, "com.samsung.musicplus.action.PLAYBACK_REWIND"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2315
    const-string v1, "com.samsung.musicplus.musicservicecommand.ACTION_MUSIC_CLOSE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2316
    sget-boolean v1, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_MOTION_PALM_PAUSE:Z

    if-eqz v1, :cond_0

    .line 2317
    const-string v1, "android.intent.action.PALM_DOWN"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2319
    :cond_0
    const-string v1, "android.media.AUDIO_BECOMING_NOISY"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2320
    const-string v1, "android.media.AUDIO_BECOMING_NOISY_SEC"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2321
    const-string v1, "com.samsung.musicplus.GESTURE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2322
    const-string v1, "com.samsung.musicplus.intent.action.MUSIC_AUTO_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2323
    const-string v1, "com.samsung.musicplus.action.TOGGLE_FAVORITE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2324
    const-string v1, "com.sec.android.app.music.musicservicecommand.checkplaystatus"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2325
    const-string v1, "com.sec.android.sidesync.source.SIDESYNC_CONNECTED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2326
    sget-boolean v1, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_MOTION_BOUNCE:Z

    if-eqz v1, :cond_1

    .line 2327
    const-string v1, "com.sec.motions.ARC_MOTION_SETTINGS_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2329
    :cond_1
    const-string v1, "com.sec.android.app.music.intent.action.WRITE_LOG"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2330
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mCommandReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/samsung/musicplus/service/PlayerService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 2331
    return-void
.end method

.method private registerCurrentSongObserver(Landroid/net/Uri;)V
    .locals 3
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 5290
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->unregisterCurrentSongObserver()V

    .line 5291
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService;->mCurrentSongObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 5292
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mIsRegistered:Z

    .line 5293
    return-void
.end method

.method private registerGestureListener(Ljava/lang/String;)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 3493
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3494
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->isEnableGesture()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3495
    iget-boolean v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mIsGestureRegistered:Z

    if-nez v0, :cond_0

    .line 3498
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mGestureManager:Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;->registerGestureListener(Z)V

    .line 3499
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mIsGestureRegistered:Z

    .line 3500
    const-string v0, "MusicService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", register gesture listner"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3504
    :cond_0
    return-void
.end method

.method private registerHdmiReceiver()V
    .locals 3

    .prologue
    .line 2191
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mHdmiReceiver:Landroid/content/BroadcastReceiver;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.intent.action.HDMI_PLUGGED"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Lcom/samsung/musicplus/service/PlayerService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 2192
    return-void
.end method

.method private registerHeadsetStateReceiver()V
    .locals 2

    .prologue
    .line 2220
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 2221
    .local v0, "f":Landroid/content/IntentFilter;
    const-string v1, "android.bluetooth.a2dp.profile.action.CONNECTION_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2222
    const-string v1, "android.bluetooth.a2dp.profile.action.PLAYING_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2223
    const-string v1, "com.sec.android.intent.action.INTERNAL_SPEAKER"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2224
    const-string v1, "android.media.action.HDMI_AUDIO_PLUG"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2225
    const-string v1, "com.sec.samsungsound.ACTION_SOUNDALIVE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2229
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->getWifiDisplayAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2230
    const-string v1, "android.intent.action.HEADSET_PLUG"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2231
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mHeadsetReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/samsung/musicplus/service/PlayerService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 2232
    return-void
.end method

.method private registerMediaButtonReceiver()V
    .locals 2

    .prologue
    .line 3822
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

    sget-object v1, Lcom/samsung/musicplus/service/PlayerService;->COMPONENT_NAME:Landroid/content/ComponentName;

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->registerMediaButtonEventReceiver(Landroid/content/ComponentName;)V

    .line 3836
    return-void
.end method

.method private registerMotionListener()V
    .locals 1

    .prologue
    .line 3570
    sget-boolean v0, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_SUPPORT_MOTION_TURN_OVER:Z

    if-eqz v0, :cond_0

    .line 3571
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->ensureMotionManager()V

    .line 3572
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mMotionManager:Lcom/samsung/musicplus/library/hardware/SimpleMotionManager;

    invoke-virtual {v0}, Lcom/samsung/musicplus/library/hardware/SimpleMotionManager;->registerMotionListener()V

    .line 3574
    :cond_0
    return-void
.end method

.method private registerOtherDeviceWatcher()V
    .locals 2

    .prologue
    .line 2343
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 2344
    .local v0, "f":Landroid/content/IntentFilter;
    const-string v1, "com.samsung.musicplus.dlna.connectivitychanged"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2345
    sget-object v1, Lcom/samsung/musicplus/provider/MusicContents$Audio$DeviceContents;->ACTION_S_LINK_INITIALIZE_CHANGED:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2346
    const-string v1, "android.intent.action.SAMSUNGACCOUNT_SIGNOUT_COMPLETED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2347
    sget-object v1, Lcom/samsung/musicplus/provider/MusicContents$Audio$DeviceContents;->ACTION_S_LINK_SIGNIN_CHANGED:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2348
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mOtherDeviceReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/samsung/musicplus/service/PlayerService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 2349
    return-void
.end method

.method private registerSettingReceiver()V
    .locals 2

    .prologue
    .line 2334
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 2335
    .local v0, "f":Landroid/content/IntentFilter;
    const-string v1, "com.android.music.settingchanged"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2336
    sget-boolean v1, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_SUPPORT_ADAPT_SOUND:Z

    if-eqz v1, :cond_0

    .line 2337
    const-string v1, "com.sec.hearingadjust.checkmusic"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2339
    :cond_0
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mSettingReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/samsung/musicplus/service/PlayerService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 2340
    return-void
.end method

.method private registerSystemReceiver()V
    .locals 2

    .prologue
    .line 2205
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 2206
    .local v0, "f":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.CONFIGURATION_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2207
    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2208
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2209
    const-string v1, "android.intent.action.ACTION_SHUTDOWN"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2210
    const-string v1, "android.intent.action.BATTERY_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2213
    const-string v1, "android.intent.action.USER_PRESENT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2214
    const-string v1, "android.intent.action.PHONE_STATE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2215
    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2216
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mSystemReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/samsung/musicplus/service/PlayerService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 2217
    return-void
.end method

.method private registerWifiDisplayReceiver()V
    .locals 2

    .prologue
    .line 2281
    sget-boolean v1, Lcom/samsung/musicplus/util/Version;->LIBRARY_OVER_API_1:Z

    if-eqz v1, :cond_0

    .line 2282
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 2283
    .local v0, "f":Landroid/content/IntentFilter;
    const-string v1, "android.hardware.display.action.WIFI_DISPLAY_STATUS_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 2284
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mWifiReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/samsung/musicplus/service/PlayerService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 2286
    .end local v0    # "f":Landroid/content/IntentFilter;
    :cond_0
    return-void
.end method

.method private reloadQueue()V
    .locals 11

    .prologue
    const-wide/16 v6, 0x0

    .line 2866
    iget-object v8, p0, Lcom/samsung/musicplus/service/PlayerService;->mPreferences:Landroid/content/SharedPreferences;

    const-string v9, "queue"

    const-string v10, ""

    invoke-interface {v8, v9, v10}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 2867
    .local v2, "q":Ljava/lang/String;
    iget-object v8, p0, Lcom/samsung/musicplus/service/PlayerService;->mPreferences:Landroid/content/SharedPreferences;

    const-string v9, "list_type"

    const v10, 0x20001

    invoke-interface {v8, v9, v10}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    .line 2868
    .local v3, "type":I
    iget-object v8, p0, Lcom/samsung/musicplus/service/PlayerService;->mPreferences:Landroid/content/SharedPreferences;

    const-string v9, "query_key"

    const/4 v10, 0x0

    invoke-interface {v8, v9, v10}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2869
    .local v0, "key":Ljava/lang/String;
    iget-object v8, p0, Lcom/samsung/musicplus/service/PlayerService;->mPreferences:Landroid/content/SharedPreferences;

    const-string v9, "list_position"

    const/4 v10, 0x0

    invoke-interface {v8, v9, v10}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    .line 2871
    .local v1, "position":I
    iget-object v8, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v8, v3, v0, v2, v1}, Lcom/samsung/musicplus/service/PlayerListManager;->reloadList(ILjava/lang/String;Ljava/lang/String;I)V

    .line 2873
    iget-object v8, p0, Lcom/samsung/musicplus/service/PlayerService;->mPreferences:Landroid/content/SharedPreferences;

    const-string v9, "seekpos"

    invoke-interface {v8, v9, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    .line 2874
    .local v4, "seekpos":J
    cmp-long v8, v4, v6

    if-ltz v8, :cond_0

    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->duration()J

    move-result-wide v8

    cmp-long v8, v4, v8

    if-gez v8, :cond_0

    move-wide v6, v4

    :cond_0
    invoke-virtual {p0, v6, v7}, Lcom/samsung/musicplus/service/PlayerService;->seek(J)J

    .line 2875
    const-string v6, "MusicService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "restored queue, currently at seek position "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->position()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->duration()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " (requested "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ")"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2877
    return-void
.end method

.method private reloadQueueIfNoList()V
    .locals 2

    .prologue
    .line 2679
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getListItemCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 2680
    const-string v0, "MusicService"

    const-string v1, ", but list item count is 0! So reload queue"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 2681
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->resetListPreset()V

    .line 2684
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->reloadQueue()V

    .line 2686
    :cond_0
    return-void
.end method

.method private reloadSongList()[J
    .locals 8

    .prologue
    .line 2769
    const-string v0, "MusicService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "reloadSongList start "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2770
    const/4 v6, 0x0

    .line 2771
    .local v6, "c":Landroid/database/Cursor;
    const/4 v7, 0x0

    .line 2774
    .local v7, "list":[J
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/samsung/musicplus/provider/MusicContents;->MUSIC_CONTENTS_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "_id"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const-string v5, "title COLLATE LOCALIZED ASC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 2777
    invoke-static {v6}, Lcom/samsung/musicplus/util/ListUtils;->getSongListForCursor(Landroid/database/Cursor;)[J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v7

    .line 2779
    if-eqz v6, :cond_0

    .line 2780
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 2783
    :cond_0
    const-string v0, "MusicService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " reloadSongList end "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2784
    return-object v7

    .line 2779
    :catchall_0
    move-exception v0

    if-eqz v6, :cond_1

    .line 2780
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v0
.end method

.method private resetListPreset()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2954
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mListLoader:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 2955
    iput v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mOpenUriFailed:I

    .line 2956
    iput v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mErrorCount:I

    .line 2957
    return-void
.end method

.method private resetNowPlayingList()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 2715
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->stop()V

    .line 2716
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    const/4 v1, -0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v3, v3, v2}, Lcom/samsung/musicplus/service/PlayerListManager;->setList(ILjava/lang/String;[JI)V

    .line 2717
    const-string v0, "com.android.music.metachanged"

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/service/PlayerService;->notifyChange(Ljava/lang/String;)V

    .line 2718
    const-string v0, "com.samsung.musicplus.action.QUEUE_CHANGED"

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/service/PlayerService;->notifyChange(Ljava/lang/String;)V

    .line 2719
    return-void
.end method

.method private resetSoundAlive()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 881
    const-string v0, "sa_auto_check"

    invoke-direct {p0, v0, v1}, Lcom/samsung/musicplus/service/PlayerService;->savePreferences(Ljava/lang/String;Z)V

    .line 886
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

    sget-object v2, Lcom/samsung/musicplus/util/SoundAliveUtilsV2;->DEFAULT_SQUARE_POSITION:[I

    sget-object v3, Lcom/samsung/musicplus/util/SoundAliveUtilsV2;->DEFAULT_STRENTH:[I

    sget-object v4, Lcom/samsung/musicplus/util/SoundAliveUtilsV2;->DEFAULT_BAND_LEVEL:[I

    iget-object v5, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

    invoke-virtual {v5}, Lcom/samsung/musicplus/service/MultiPlayer;->getNewSoundAliveCurrentPreset()I

    move-result v5

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/musicplus/service/MultiPlayer;->setSoundAliveV2InitValue(Z[I[I[II)V

    .line 889
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/MultiPlayer;->setSoundAliveV2()V

    .line 890
    const-string v0, "sa_square_position"

    const-string v1, "2|2|"

    invoke-direct {p0, v0, v1}, Lcom/samsung/musicplus/service/PlayerService;->savePreferences(Ljava/lang/String;Ljava/lang/String;)V

    .line 892
    const-string v0, "sa_band_level"

    const-string v1, "0|0|0|0|0|0|0|"

    invoke-direct {p0, v0, v1}, Lcom/samsung/musicplus/service/PlayerService;->savePreferences(Ljava/lang/String;Ljava/lang/String;)V

    .line 897
    return-void
.end method

.method private rewind()V
    .locals 13

    .prologue
    const/4 v12, 0x2

    const-wide/16 v6, 0x0

    .line 3967
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->isSupportRewind()Z

    move-result v8

    if-nez v8, :cond_0

    .line 3994
    :goto_0
    return-void

    .line 3971
    :cond_0
    iget-object v8, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

    invoke-virtual {v8}, Lcom/samsung/musicplus/service/MultiPlayer;->position()J

    move-result-wide v8

    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->getSkippingTime()I

    move-result v10

    int-to-long v10, v10

    sub-long v4, v8, v10

    .line 3972
    .local v4, "time":J
    cmp-long v8, v4, v6

    if-gtz v8, :cond_3

    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->isPlaying()Z

    move-result v8

    if-eqz v8, :cond_3

    .line 3974
    iget-object v8, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v8}, Lcom/samsung/musicplus/service/PlayerListManager;->getPrevMediaUri()Landroid/net/Uri;

    move-result-object v3

    .line 3975
    .local v3, "uri":Landroid/net/Uri;
    if-eqz v3, :cond_3

    .line 3976
    iget-object v8, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v8, v3}, Lcom/samsung/musicplus/service/PlayerListManager;->getMediaInfo(Landroid/net/Uri;)Lcom/samsung/musicplus/util/ListUtils$MediaInfo;

    move-result-object v2

    .line 3977
    .local v2, "info":Lcom/samsung/musicplus/util/ListUtils$MediaInfo;
    if-eqz v2, :cond_1

    iget-wide v0, v2, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->duration:J

    .line 3978
    .local v0, "duration":J
    :goto_1
    sget-object v8, Lcom/samsung/musicplus/service/PlayerService;->TIME_SKIP_VALUE:[I

    aget v8, v8, v12

    int-to-long v8, v8

    cmp-long v8, v0, v8

    if-lez v8, :cond_2

    sget-object v6, Lcom/samsung/musicplus/service/PlayerService;->TIME_SKIP_VALUE:[I

    aget v6, v6, v12

    int-to-long v6, v6

    sub-long v4, v0, v6

    .line 3980
    :goto_2
    invoke-direct {p0, v4, v5}, Lcom/samsung/musicplus/service/PlayerService;->prev(J)V

    goto :goto_0

    .end local v0    # "duration":J
    :cond_1
    move-wide v0, v6

    .line 3977
    goto :goto_1

    .restart local v0    # "duration":J
    :cond_2
    move-wide v4, v6

    .line 3978
    goto :goto_2

    .line 3993
    .end local v0    # "duration":J
    .end local v2    # "info":Lcom/samsung/musicplus/util/ListUtils$MediaInfo;
    .end local v3    # "uri":Landroid/net/Uri;
    :cond_3
    invoke-virtual {p0, v4, v5}, Lcom/samsung/musicplus/service/PlayerService;->seek(J)J

    goto :goto_0
.end method

.method private rewind(J)V
    .locals 13
    .param p1, "pos"    # J

    .prologue
    const/4 v12, 0x2

    const-wide/16 v6, 0x0

    .line 3932
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->isSupportRewind()Z

    move-result v8

    if-nez v8, :cond_0

    .line 3963
    :goto_0
    return-void

    .line 3936
    :cond_0
    iget-object v8, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

    invoke-virtual {v8}, Lcom/samsung/musicplus/service/MultiPlayer;->position()J

    move-result-wide v8

    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->getSkippingTime()I

    move-result v10

    int-to-long v10, v10

    sub-long v4, v8, v10

    .line 3937
    .local v4, "time":J
    cmp-long v8, v4, v6

    if-gtz v8, :cond_4

    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->isPlaying()Z

    move-result v8

    if-eqz v8, :cond_4

    .line 3939
    iget-object v8, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v8}, Lcom/samsung/musicplus/service/PlayerListManager;->getPrevMediaUri()Landroid/net/Uri;

    move-result-object v3

    .line 3940
    .local v3, "uri":Landroid/net/Uri;
    if-eqz v3, :cond_3

    .line 3941
    iget-object v8, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v8, v3}, Lcom/samsung/musicplus/service/PlayerListManager;->getMediaInfo(Landroid/net/Uri;)Lcom/samsung/musicplus/util/ListUtils$MediaInfo;

    move-result-object v2

    .line 3942
    .local v2, "info":Lcom/samsung/musicplus/util/ListUtils$MediaInfo;
    if-eqz v2, :cond_1

    iget-wide v0, v2, Lcom/samsung/musicplus/util/ListUtils$MediaInfo;->duration:J

    .line 3943
    .local v0, "duration":J
    :goto_1
    sget-object v8, Lcom/samsung/musicplus/service/PlayerService;->TIME_SKIP_VALUE:[I

    aget v8, v8, v12

    int-to-long v8, v8

    cmp-long v8, v0, v8

    if-lez v8, :cond_2

    sget-object v6, Lcom/samsung/musicplus/service/PlayerService;->TIME_SKIP_VALUE:[I

    aget v6, v6, v12

    int-to-long v6, v6

    sub-long v4, v0, v6

    .line 3946
    :goto_2
    invoke-direct {p0, v4, v5}, Lcom/samsung/musicplus/service/PlayerService;->prev(J)V

    goto :goto_0

    .end local v0    # "duration":J
    :cond_1
    move-wide v0, v6

    .line 3942
    goto :goto_1

    .restart local v0    # "duration":J
    :cond_2
    move-wide v4, v6

    .line 3943
    goto :goto_2

    .line 3959
    .end local v0    # "duration":J
    .end local v2    # "info":Lcom/samsung/musicplus/util/ListUtils$MediaInfo;
    :cond_3
    invoke-virtual {p0, v4, v5}, Lcom/samsung/musicplus/service/PlayerService;->seek(J)J

    goto :goto_0

    .line 3961
    .end local v3    # "uri":Landroid/net/Uri;
    :cond_4
    invoke-virtual {p0, p1, p2}, Lcom/samsung/musicplus/service/PlayerService;->seek(J)J

    goto :goto_0
.end method

.method private saveNewSoundAliveStrength([I)V
    .locals 6
    .param p1, "strength"    # [I

    .prologue
    .line 1921
    if-eqz p1, :cond_1

    .line 1922
    const-string v1, ""

    .line 1923
    .local v1, "strengthString":Ljava/lang/String;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/4 v2, 0x3

    if-ge v0, v2, :cond_0

    .line 1924
    const-string v2, "%s%d|"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    const/4 v4, 0x1

    aget v5, p1, v0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 1923
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1926
    :cond_0
    const-string v2, "sa_use_strength"

    invoke-direct {p0, v2, v1}, Lcom/samsung/musicplus/service/PlayerService;->savePreferences(Ljava/lang/String;Ljava/lang/String;)V

    .line 1928
    .end local v0    # "i":I
    .end local v1    # "strengthString":Ljava/lang/String;
    :cond_1
    return-void
.end method

.method private savePlayPosition()V
    .locals 3

    .prologue
    .line 2852
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->isOtherContnetsList()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2855
    const-string v1, "MusicService"

    const-string v2, "this is not local list, so do not save playing position."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2861
    :goto_0
    return-void

    .line 2858
    :cond_0
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 2859
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "list_position"

    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v2}, Lcom/samsung/musicplus/service/PlayerListManager;->getListPosition()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 2860
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method

.method private savePreferences(Ljava/lang/String;F)V
    .locals 4
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # F

    .prologue
    .line 4237
    const-string v1, "MusicService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "savePreferences key : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " value : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4238
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 4239
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putFloat(Ljava/lang/String;F)Landroid/content/SharedPreferences$Editor;

    .line 4240
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 4241
    return-void
.end method

.method private savePreferences(Ljava/lang/String;I)V
    .locals 4
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # I

    .prologue
    .line 4230
    const-string v1, "MusicService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "savePreferences key : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " value : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4231
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 4232
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 4233
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 4234
    return-void
.end method

.method private savePreferences(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Ljava/lang/String;

    .prologue
    .line 4260
    const-string v1, "MusicService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "savePreferences key : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " value : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4261
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 4262
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 4263
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 4264
    return-void
.end method

.method private savePreferences(Ljava/lang/String;Z)V
    .locals 4
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # Z

    .prologue
    .line 4244
    const-string v1, "MusicService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "savePreferences key : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " value : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4245
    sget-boolean v1, Lcom/samsung/musicplus/util/Features;->FLAG_SUPPORT_EXTERNAL_SOUNDALIVE:Z

    if-nez v1, :cond_0

    .line 4246
    const-string v1, "sa_auto_check"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4247
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

    invoke-virtual {v1, p2}, Lcom/samsung/musicplus/service/MultiPlayer;->setAutoSquare(Z)V

    .line 4250
    :cond_0
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 4251
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    invoke-interface {v0, p1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 4252
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 4254
    sget-boolean v1, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_K2HD:Z

    if-eqz v1, :cond_1

    const-string v1, "K2HD"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 4255
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->notiChangePrefK2HD()V

    .line 4257
    :cond_1
    return-void
.end method

.method private saveQueue(Z)V
    .locals 5
    .param p1, "force"    # Z

    .prologue
    const/16 v4, 0x10

    .line 2794
    const-string v1, "MusicService"

    const-string v2, "saveQueue"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2795
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->isOtherContnetsList()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2798
    const-string v1, "MusicService"

    const-string v2, "this is not local list, so do not save queue."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2818
    :goto_0
    return-void

    .line 2802
    :cond_0
    if-eqz p1, :cond_2

    .line 2803
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getOrganizedQueue()[J

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/service/NowPlayingDbUpdater;->saveNowPlayingQueue(Landroid/content/Context;[J)V

    .line 2809
    :goto_1
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 2810
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "queue"

    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v2}, Lcom/samsung/musicplus/service/PlayerListManager;->getQueueToSave()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 2811
    const-string v1, "list_position"

    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v2}, Lcom/samsung/musicplus/service/PlayerListManager;->getListPosition()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 2812
    const-string v1, "list_type"

    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v2}, Lcom/samsung/musicplus/service/PlayerListManager;->getListType()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 2813
    const-string v1, "query_key"

    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v2}, Lcom/samsung/musicplus/service/PlayerListManager;->getKeyWord()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 2814
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

    invoke-virtual {v1}, Lcom/samsung/musicplus/service/MultiPlayer;->isInitialized()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2815
    const-string v1, "seekpos"

    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

    invoke-virtual {v2}, Lcom/samsung/musicplus/service/MultiPlayer;->position()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 2817
    :cond_1
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0

    .line 2805
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_2
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayerHandler:Landroid/os/Handler;

    invoke-virtual {v1, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 2806
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayerHandler:Landroid/os/Handler;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v1, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_1
.end method

.method private saveSoundAliveUserEqExt([I[I)V
    .locals 5
    .param p1, "eqList"    # [I
    .param p2, "extList"    # [I

    .prologue
    .line 2000
    iget-object v3, p0, Lcom/samsung/musicplus/service/PlayerService;->mPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 2001
    .local v0, "edit":Landroid/content/SharedPreferences$Editor;
    if-eqz p1, :cond_1

    array-length v3, p1

    const/4 v4, 0x7

    if-ne v3, v4, :cond_1

    .line 2002
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 2003
    .local v1, "eqbuild":Ljava/lang/StringBuilder;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    array-length v3, p1

    if-ge v2, v3, :cond_0

    .line 2004
    aget v3, p1, v2

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 2005
    const-string v3, "|"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2003
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2007
    :cond_0
    const-string v3, "user_eq"

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 2010
    .end local v1    # "eqbuild":Ljava/lang/StringBuilder;
    .end local v2    # "i":I
    :cond_1
    if-eqz p2, :cond_3

    array-length v3, p2

    const/4 v4, 0x5

    if-ne v3, v4, :cond_3

    .line 2011
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 2012
    .restart local v1    # "eqbuild":Ljava/lang/StringBuilder;
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_1
    array-length v3, p2

    if-ge v2, v3, :cond_2

    .line 2013
    aget v3, p2, v2

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 2014
    const-string v3, "|"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 2012
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2016
    :cond_2
    const-string v3, "user_ext"

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 2018
    .end local v1    # "eqbuild":Ljava/lang/StringBuilder;
    .end local v2    # "i":I
    :cond_3
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 2019
    return-void
.end method

.method private sendPlayerBuffering(Z)V
    .locals 2
    .param p1, "isBuffring"    # Z

    .prologue
    .line 5189
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.samsung.musicplus.action.PLAYER_BUFFERING"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 5190
    .local v0, "i":Landroid/content/Intent;
    const-string v1, "is_buffering"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 5191
    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/service/PlayerService;->sendBroadcast(Landroid/content/Intent;)V

    .line 5192
    return-void
.end method

.method private setAgingTimeOut()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 5177
    const v0, 0x493e0

    .line 5178
    .local v0, "PAUSE_TIMEOUT":I
    const-string v2, "alarm"

    invoke-virtual {p0, v2}, Lcom/samsung/musicplus/service/PlayerService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/AlarmManager;

    .line 5179
    .local v1, "am":Landroid/app/AlarmManager;
    const/4 v2, 0x3

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    const-wide/32 v6, 0x493e0

    add-long/2addr v4, v6

    new-instance v3, Landroid/content/Intent;

    const-string v6, "com.sec.android.app.music.intent.action.WRITE_LOG"

    invoke-direct {v3, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p0, v8, v3, v8}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v3

    invoke-virtual {v1, v2, v4, v5, v3}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 5181
    return-void
.end method

.method private setAgingTimer()V
    .locals 0

    .prologue
    .line 5174
    return-void
.end method

.method private setNewSoundAliveBandLevel([I)V
    .locals 3
    .param p1, "bandLevel"    # [I

    .prologue
    .line 5229
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

    if-nez v0, :cond_0

    .line 5230
    const-string v0, "MusicService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setNewSoundAliveBandLevel() mPlayer is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 5234
    :goto_0
    return-void

    .line 5233
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/service/MultiPlayer;->setNewSoundAliveBandLevel([I)V

    goto :goto_0
.end method

.method private setNewSoundAliveEachStrength(II)V
    .locals 3
    .param p1, "id"    # I
    .param p2, "level"    # I

    .prologue
    .line 5260
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

    if-nez v0, :cond_0

    .line 5261
    const-string v0, "MusicService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setNewSoundAliveStrength() mPlayer is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 5265
    :goto_0
    return-void

    .line 5264
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/musicplus/service/MultiPlayer;->setNewSoundAliveEachStrength(II)V

    goto :goto_0
.end method

.method private setNewSoundAliveSquarePosition([I)V
    .locals 3
    .param p1, "squarePosition"    # [I

    .prologue
    .line 5221
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

    if-nez v0, :cond_0

    .line 5222
    const-string v0, "MusicService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setNewSoundAliveSquarePosition() mPlayer is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 5226
    :goto_0
    return-void

    .line 5225
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/service/MultiPlayer;->setNewSoundAliveSquarePosition([I)V

    goto :goto_0
.end method

.method private setNewSoundAliveStrength([I)V
    .locals 3
    .param p1, "level"    # [I

    .prologue
    .line 5244
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

    if-nez v0, :cond_0

    .line 5245
    const-string v0, "MusicService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setNewSoundAliveStrength() mPlayer is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 5249
    :goto_0
    return-void

    .line 5248
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/service/MultiPlayer;->setNewSoundAliveStrength([I)V

    goto :goto_0
.end method

.method private setNewSoundAliveUsePreset(I)V
    .locals 3
    .param p1, "mode"    # I

    .prologue
    .line 5213
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

    if-nez v0, :cond_0

    .line 5214
    const-string v0, "MusicService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setNewSoundAliveUsePreset() mPlayer is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 5218
    :goto_0
    return-void

    .line 5217
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

    int-to-short v1, p1

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/service/MultiPlayer;->setNewSoundAliveUsePreset(I)V

    goto :goto_0
.end method

.method private setPlaybackState(ZZJF)V
    .locals 3
    .param p1, "isPreparing"    # Z
    .param p2, "isPlaying"    # Z
    .param p3, "position"    # J
    .param p5, "speed"    # F

    .prologue
    .line 3456
    const-string v0, "MusicService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setPlaybackState isPreparing ? "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " isPlaying ? "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " position : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " speed : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3458
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mControlClient:Landroid/media/RemoteControlClient;

    if-eqz v0, :cond_3

    .line 3461
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mControlClient:Landroid/media/RemoteControlClient;

    if-eqz p1, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0, p3, p4, p5}, Landroid/media/RemoteControlClient;->setPlaybackState(IJF)V

    .line 3468
    :goto_1
    return-void

    .line 3461
    :cond_0
    if-eqz p2, :cond_1

    const/4 v0, 0x3

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->isStop()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x2

    goto :goto_0

    .line 3466
    :cond_3
    invoke-direct/range {p0 .. p5}, Lcom/samsung/musicplus/service/PlayerService;->updateMediaSessionState(ZZJF)V

    goto :goto_1
.end method

.method private setServiceTimeout()V
    .locals 8

    .prologue
    .line 5140
    const-string v3, "MusicService"

    const-string v4, "setServiceTimeout() notification will hide after 120000 milli sec"

    invoke-static {v3, v4}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 5143
    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.samsung.musicplus.musicservicecommand.ACTION_MUSIC_CLOSE"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 5146
    .local v2, "i":Landroid/content/Intent;
    const-string v3, "from"

    const-string v4, "time_out"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 5149
    const/4 v3, 0x0

    const/high16 v4, 0x8000000

    invoke-static {p0, v3, v2, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 5151
    .local v1, "close":Landroid/app/PendingIntent;
    const-string v3, "alarm"

    invoke-virtual {p0, v3}, Lcom/samsung/musicplus/service/PlayerService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 5152
    .local v0, "am":Landroid/app/AlarmManager;
    const/4 v3, 0x3

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    const-wide/32 v6, 0x1d4c0

    add-long/2addr v4, v6

    invoke-virtual {v0, v3, v4, v5, v1}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 5154
    return-void
.end method

.method private setShuffleAndRepeat(II)V
    .locals 1
    .param p1, "shuffle"    # I
    .param p2, "repeat"    # I

    .prologue
    .line 4202
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/service/PlayerListManager;->setShuffle(I)V

    .line 4203
    const-string v0, "shuffle"

    invoke-direct {p0, v0, p1}, Lcom/samsung/musicplus/service/PlayerService;->savePreferences(Ljava/lang/String;I)V

    .line 4204
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v0, p2}, Lcom/samsung/musicplus/service/PlayerListManager;->setRepeatMode(I)V

    .line 4205
    const-string v0, "repeat"

    invoke-direct {p0, v0, p2}, Lcom/samsung/musicplus/service/PlayerService;->savePreferences(Ljava/lang/String;I)V

    .line 4206
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->notifySettingChange()V

    .line 4207
    return-void
.end method

.method private setSmartVolumeEarSafety(Z)V
    .locals 2
    .param p1, "isOn"    # Z

    .prologue
    .line 1133
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->setSmartVoumeEnable(Z)V

    .line 1134
    return-void

    .line 1133
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setSoundAliveV2(ZI[I[I[IZ)V
    .locals 7
    .param p1, "auto"    # Z
    .param p2, "nsaUsePreset"    # I
    .param p3, "nsaSquarePosition"    # [I
    .param p4, "nsaStrength"    # [I
    .param p5, "nsaBandLevel"    # [I
    .param p6, "enableToast"    # Z

    .prologue
    .line 3373
    invoke-direct {p0, p2, p6}, Lcom/samsung/musicplus/service/PlayerService;->canChangeSoundAlivePreset(IZ)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3374
    const/4 p2, 0x0

    .line 3375
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

    invoke-virtual {v0, p2}, Lcom/samsung/musicplus/service/MultiPlayer;->setNewSoundAliveUsePreset(I)V

    .line 3376
    const-string v0, "sa_use_preset_effect"

    invoke-direct {p0, v0, p2}, Lcom/samsung/musicplus/service/PlayerService;->savePreferences(Ljava/lang/String;I)V

    .line 3379
    :cond_0
    invoke-direct {p0, p4, p6}, Lcom/samsung/musicplus/service/PlayerService;->canChangeSoundAliveStrength([IZ)[I

    move-result-object v6

    .line 3380
    .local v6, "strength":[I
    if-eqz v6, :cond_1

    .line 3381
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

    invoke-virtual {v0, v6}, Lcom/samsung/musicplus/service/MultiPlayer;->setNewSoundAliveStrength([I)V

    .line 3382
    invoke-direct {p0, v6}, Lcom/samsung/musicplus/service/PlayerService;->saveNewSoundAliveStrength([I)V

    .line 3386
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

    invoke-virtual {v0}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->isHDMIConnect()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3387
    const-string v0, "sa_auto_check"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/samsung/musicplus/service/PlayerService;->savePreferences(Ljava/lang/String;Z)V

    .line 3388
    const-string v0, "sa_square_position"

    const-string v1, "2|2|"

    invoke-direct {p0, v0, v1}, Lcom/samsung/musicplus/service/PlayerService;->savePreferences(Ljava/lang/String;Ljava/lang/String;)V

    .line 3390
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

    sget-object v1, Lcom/samsung/musicplus/util/SoundAliveUtilsV2;->DEFAULT_SQUARE_POSITION:[I

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/service/MultiPlayer;->setNewSoundAliveSquarePosition([I)V

    .line 3393
    :cond_2
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

    move v1, p1

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/musicplus/service/MultiPlayer;->setSoundAliveV2InitValue(Z[I[I[II)V

    .line 3395
    return-void
.end method

.method private showToast(I)V
    .locals 2
    .param p1, "stringId"    # I

    .prologue
    .line 2724
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mToast:Landroid/widget/Toast;

    if-nez v0, :cond_0

    .line 2725
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mToast:Landroid/widget/Toast;

    .line 2729
    :goto_0
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 2730
    return-void

    .line 2727
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mToast:Landroid/widget/Toast;

    invoke-virtual {v0, p1}, Landroid/widget/Toast;->setText(I)V

    goto :goto_0
.end method

.method private simpleWarningNoti(I)V
    .locals 4
    .param p1, "strId"    # I

    .prologue
    .line 1293
    new-instance v0, Landroid/app/Notification$Builder;

    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    .line 1294
    .local v0, "builder":Landroid/app/Notification$Builder;
    const v2, 0x108008a

    invoke-virtual {v0, v2}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v2

    invoke-virtual {p0, p1}, Lcom/samsung/musicplus/service/PlayerService;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/Notification$Builder;->setTicker(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    .line 1296
    const-string v2, "notification"

    invoke-virtual {p0, v2}, Lcom/samsung/musicplus/service/PlayerService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/NotificationManager;

    .line 1299
    .local v1, "nm":Landroid/app/NotificationManager;
    invoke-virtual {v0}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v2

    invoke-virtual {v1, p1, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 1300
    invoke-virtual {v1, p1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 1301
    return-void
.end method

.method private startForeground()V
    .locals 4

    .prologue
    .line 4808
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/samsung/musicplus/service/PlayerService;->isNoxMode(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4809
    const v1, 0x7010003

    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService;->mNotification:Landroid/app/Notification;

    invoke-virtual {p0, v1, v2}, Lcom/samsung/musicplus/service/PlayerService;->startForeground(ILandroid/app/Notification;)V

    .line 4817
    :goto_0
    return-void

    .line 4811
    :cond_0
    const v1, 0x7010002

    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService;->mNotification:Landroid/app/Notification;

    invoke-virtual {p0, v1, v2}, Lcom/samsung/musicplus/service/PlayerService;->startForeground(ILandroid/app/Notification;)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 4813
    :catch_0
    move-exception v0

    .line 4815
    .local v0, "e":Ljava/lang/IllegalStateException;
    const-string v1, "MusicService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "startForeground : IllegalStateException occurred"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private stop()V
    .locals 2

    .prologue
    .line 2692
    const-string v0, "MusicService"

    const-string v1, "stop()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2693
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->pause()V

    .line 2694
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/MultiPlayer;->stop()V

    .line 2696
    const-string v0, "com.android.music.playstatechanged"

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/service/PlayerService;->notifyChange(Ljava/lang/String;)V

    .line 2698
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->isDmrPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2701
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/service/PlayerService;->changeToDefaultPlayer(Z)V

    .line 2704
    :cond_0
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->hideNotification()V

    .line 2705
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mNotifyHandler:Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;->removeMusicInfoMsg()V

    .line 2706
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/service/PlayerService;->notifyMusicInfo(Z)V

    .line 2707
    return-void
.end method

.method private unregisterBounceListener()V
    .locals 2

    .prologue
    .line 3676
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mBounceMotionManager:Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager;

    if-eqz v0, :cond_0

    .line 3677
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mBounceMotionManager:Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager;

    invoke-virtual {v0}, Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager;->unregisterListener()V

    .line 3678
    const-string v0, "MusicService"

    const-string v1, "unregisterBounceListener"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3680
    :cond_0
    return-void
.end method

.method private unregisterCurrentSongObserver()V
    .locals 2

    .prologue
    .line 5296
    iget-boolean v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mIsRegistered:Z

    if-eqz v0, :cond_0

    .line 5297
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mCurrentSongObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 5298
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mIsRegistered:Z

    .line 5300
    :cond_0
    return-void
.end method

.method private unregisterDlanWatcher()V
    .locals 1

    .prologue
    .line 2352
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mOtherDeviceReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/service/PlayerService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 2353
    return-void
.end method

.method private unregisterGestureListener(Ljava/lang/String;)V
    .locals 3
    .param p1, "name"    # Ljava/lang/String;

    .prologue
    .line 3556
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mGestureManager:Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;

    if-eqz v0, :cond_0

    .line 3557
    iget-boolean v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mIsGestureRegistered:Z

    if-eqz v0, :cond_0

    .line 3558
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mGestureManager:Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;

    invoke-virtual {v0}, Lcom/samsung/musicplus/library/hardware/SimpleGestureManager;->releaseGestureListener()V

    .line 3559
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mIsGestureRegistered:Z

    .line 3560
    const-string v0, "MusicService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", unregister gesture listner"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3563
    :cond_0
    return-void
.end method

.method private unregisterMotionListener()V
    .locals 1

    .prologue
    .line 3598
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mMotionManager:Lcom/samsung/musicplus/library/hardware/SimpleMotionManager;

    if-eqz v0, :cond_0

    .line 3599
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mMotionManager:Lcom/samsung/musicplus/library/hardware/SimpleMotionManager;

    invoke-virtual {v0}, Lcom/samsung/musicplus/library/hardware/SimpleMotionManager;->unregisterMotionListener()V

    .line 3601
    :cond_0
    return-void
.end method

.method private unregisterWifiDisplayReceiver()V
    .locals 1

    .prologue
    .line 2289
    sget-boolean v0, Lcom/samsung/musicplus/util/Version;->LIBRARY_OVER_API_1:Z

    if-eqz v0, :cond_0

    .line 2290
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mWifiReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/service/PlayerService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 2292
    :cond_0
    return-void
.end method

.method private updateAdaptSound(Z)V
    .locals 3
    .param p1, "connected"    # Z

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1003
    iput-boolean v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mWarnAdaptSound:Z

    .line 1004
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mAdaptSound:Lcom/samsung/musicplus/library/audio/AdaptSound;

    if-nez v0, :cond_0

    .line 1021
    :goto_0
    return-void

    .line 1008
    :cond_0
    iget-boolean v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mIsAdaptSoundOn:Z

    if-eqz v0, :cond_2

    .line 1009
    if-eqz p1, :cond_1

    .line 1010
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mAdaptSound:Lcom/samsung/musicplus/library/audio/AdaptSound;

    invoke-virtual {v0, v2}, Lcom/samsung/musicplus/library/audio/AdaptSound;->activate(Z)V

    .line 1019
    :goto_1
    const-string v0, "MusicService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "updateAdaptSound mWarnAdaptSound "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/samsung/musicplus/service/PlayerService;->mWarnAdaptSound:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " Connected ? "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 1013
    :cond_1
    iput-boolean v2, p0, Lcom/samsung/musicplus/service/PlayerService;->mWarnAdaptSound:Z

    .line 1014
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mAdaptSound:Lcom/samsung/musicplus/library/audio/AdaptSound;

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/library/audio/AdaptSound;->activate(Z)V

    goto :goto_1

    .line 1017
    :cond_2
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mAdaptSound:Lcom/samsung/musicplus/library/audio/AdaptSound;

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/library/audio/AdaptSound;->activate(Z)V

    goto :goto_1
.end method

.method private updateAllViews(ILandroid/graphics/Bitmap;)V
    .locals 11
    .param p1, "listType"    # I
    .param p2, "album"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v3, 0x0

    .line 4820
    if-eqz p2, :cond_0

    .line 4821
    const-string v8, "MusicService"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "updateAllViews() before resize - width: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " height: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 4823
    invoke-direct {p0, p2}, Lcom/samsung/musicplus/service/PlayerService;->getResizedBitmapForNotification(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object p2

    .line 4824
    const-string v8, "MusicService"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "updateAllViews() after resize - width: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, " height: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 4827
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->isPlaying()Z

    move-result v2

    .line 4828
    .local v2, "isPlaying":Z
    iget-object v8, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v8}, Lcom/samsung/musicplus/service/PlayerListManager;->getTitle()Ljava/lang/String;

    move-result-object v7

    .line 4829
    .local v7, "title":Ljava/lang/String;
    iget-object v8, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v8}, Lcom/samsung/musicplus/service/PlayerListManager;->getArtist()Ljava/lang/String;

    move-result-object v0

    .line 4830
    .local v0, "artist":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getPersonalMode()I

    move-result v4

    .line 4832
    .local v4, "personalMode":I
    iget-object v8, p0, Lcom/samsung/musicplus/service/PlayerService;->mRemoteViewBuilder:Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;

    invoke-virtual {v8, p2}, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->setAlbumArt(Landroid/graphics/Bitmap;)Lcom/samsung/musicplus/widget/RemoteViewBuilder;

    move-result-object v8

    invoke-virtual {v8, p1, v2}, Lcom/samsung/musicplus/widget/RemoteViewBuilder;->setPlayStatus(IZ)Lcom/samsung/musicplus/widget/RemoteViewBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Lcom/samsung/musicplus/widget/RemoteViewBuilder;->setPlayController(I)Lcom/samsung/musicplus/widget/RemoteViewBuilder;

    move-result-object v8

    invoke-virtual {v8, v7, v0}, Lcom/samsung/musicplus/widget/RemoteViewBuilder;->setTitles(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/musicplus/widget/RemoteViewBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Lcom/samsung/musicplus/widget/RemoteViewBuilder;->setPersonalIcon(I)Lcom/samsung/musicplus/widget/RemoteViewBuilder;

    .line 4837
    iget-object v8, p0, Lcom/samsung/musicplus/service/PlayerService;->mRemoteBigViewBuilder:Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;

    invoke-virtual {v8, p2}, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->setAlbumArt(Landroid/graphics/Bitmap;)Lcom/samsung/musicplus/widget/RemoteViewBuilder;

    move-result-object v8

    invoke-virtual {v8, p1, v2}, Lcom/samsung/musicplus/widget/RemoteViewBuilder;->setPlayStatus(IZ)Lcom/samsung/musicplus/widget/RemoteViewBuilder;

    move-result-object v8

    invoke-virtual {v8, p1}, Lcom/samsung/musicplus/widget/RemoteViewBuilder;->setPlayController(I)Lcom/samsung/musicplus/widget/RemoteViewBuilder;

    move-result-object v8

    invoke-virtual {v8, v7, v0}, Lcom/samsung/musicplus/widget/RemoteViewBuilder;->setTitles(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/musicplus/widget/RemoteViewBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Lcom/samsung/musicplus/widget/RemoteViewBuilder;->setPersonalIcon(I)Lcom/samsung/musicplus/widget/RemoteViewBuilder;

    .line 4843
    iget-object v8, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v8}, Lcom/samsung/musicplus/service/PlayerListManager;->getShuffle()I

    move-result v6

    .line 4844
    .local v6, "shuffle":I
    iget-object v8, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v8}, Lcom/samsung/musicplus/service/PlayerListManager;->getRepeatMode()I

    move-result v5

    .line 4845
    .local v5, "repeatMode":I
    iget-object v8, p0, Lcom/samsung/musicplus/service/PlayerService;->mRemoteBigViewBuilder:Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;

    invoke-virtual {v8, v6, v5}, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->updateExtraButtons(II)Lcom/samsung/musicplus/widget/RemoteViewBuilder;

    .line 4849
    iget-object v8, p0, Lcom/samsung/musicplus/service/PlayerService;->mRemoteViewBuilder:Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;

    invoke-virtual {v8, v3}, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->setUHQ(Z)Lcom/samsung/musicplus/widget/RemoteViewBuilder;

    .line 4850
    iget-object v8, p0, Lcom/samsung/musicplus/service/PlayerService;->mRemoteViewBuilder:Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;

    invoke-virtual {v8, v3}, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->setK2HD(Z)Lcom/samsung/musicplus/widget/RemoteViewBuilder;

    .line 4851
    iget-object v8, p0, Lcom/samsung/musicplus/service/PlayerService;->mRemoteBigViewBuilder:Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;

    invoke-virtual {v8, v3}, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->setUHQ(Z)Lcom/samsung/musicplus/widget/RemoteViewBuilder;

    .line 4852
    iget-object v8, p0, Lcom/samsung/musicplus/service/PlayerService;->mRemoteBigViewBuilder:Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;

    invoke-virtual {v8, v3}, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->setK2HD(Z)Lcom/samsung/musicplus/widget/RemoteViewBuilder;

    .line 4854
    invoke-static {}, Lcom/samsung/musicplus/util/UiUtils;->isSupportUhqa()Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getSamplingRate()I

    move-result v8

    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getBitDepth()I

    move-result v9

    invoke-static {v8, v9}, Lcom/samsung/musicplus/util/UiUtils;->isUhqa(II)Z

    move-result v8

    if-eqz v8, :cond_1

    const/4 v3, 0x1

    .line 4856
    .local v3, "isUHQ":Z
    :cond_1
    if-eqz v3, :cond_2

    .line 4857
    iget-object v8, p0, Lcom/samsung/musicplus/service/PlayerService;->mRemoteViewBuilder:Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;

    invoke-virtual {v8, v3}, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->setUHQ(Z)Lcom/samsung/musicplus/widget/RemoteViewBuilder;

    .line 4858
    iget-object v8, p0, Lcom/samsung/musicplus/service/PlayerService;->mRemoteBigViewBuilder:Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;

    invoke-virtual {v8, v3}, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->setUHQ(Z)Lcom/samsung/musicplus/widget/RemoteViewBuilder;

    .line 4865
    :goto_0
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->updateNotificationConnectivityStatus()V

    .line 4866
    return-void

    .line 4860
    :cond_2
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getCurrentPath()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {p0, v8}, Lcom/samsung/musicplus/service/PlayerService;->isK2HD(Ljava/lang/String;)Z

    move-result v1

    .line 4861
    .local v1, "isK2HD":Z
    iget-object v8, p0, Lcom/samsung/musicplus/service/PlayerService;->mRemoteViewBuilder:Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;

    invoke-virtual {v8, v1}, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->setK2HD(Z)Lcom/samsung/musicplus/widget/RemoteViewBuilder;

    .line 4862
    iget-object v8, p0, Lcom/samsung/musicplus/service/PlayerService;->mRemoteBigViewBuilder:Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;

    invoke-virtual {v8, v1}, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->setK2HD(Z)Lcom/samsung/musicplus/widget/RemoteViewBuilder;

    goto :goto_0
.end method

.method private updateMediaSessionMeta(Landroid/graphics/Bitmap;)V
    .locals 10
    .param p1, "bm"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v3, 0x0

    .line 1785
    new-instance v0, Landroid/media/MediaMetadata$Builder;

    invoke-direct {v0}, Landroid/media/MediaMetadata$Builder;-><init>()V

    .line 1786
    .local v0, "b":Landroid/media/MediaMetadata$Builder;
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getAudioId()J

    move-result-wide v4

    .line 1787
    .local v4, "id":J
    const-wide/16 v6, 0x0

    cmp-long v6, v4, v6

    if-gez v6, :cond_0

    .line 1789
    iget-object v3, p0, Lcom/samsung/musicplus/service/PlayerService;->mMediaSession:Landroid/media/session/MediaSession;

    invoke-virtual {v0}, Landroid/media/MediaMetadata$Builder;->build()Landroid/media/MediaMetadata;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/media/session/MediaSession;->setMetadata(Landroid/media/MediaMetadata;)V

    .line 1819
    :goto_0
    return-void

    .line 1792
    :cond_0
    const-string v6, "android.media.metadata.TITLE"

    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getTitle()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Landroid/media/MediaMetadata$Builder;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/media/MediaMetadata$Builder;

    .line 1793
    const-string v6, "android.media.metadata.ALBUM"

    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getAlbum()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Landroid/media/MediaMetadata$Builder;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/media/MediaMetadata$Builder;

    .line 1794
    const-string v6, "android.media.metadata.ARTIST"

    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getArtist()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Landroid/media/MediaMetadata$Builder;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/media/MediaMetadata$Builder;

    .line 1795
    const-string v6, "android.media.metadata.ALBUM_ARTIST"

    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getArtist()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Landroid/media/MediaMetadata$Builder;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/media/MediaMetadata$Builder;

    .line 1796
    const-string v6, "android.media.metadata.GENRE"

    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getGenre()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v0, v6, v7}, Landroid/media/MediaMetadata$Builder;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/media/MediaMetadata$Builder;

    .line 1797
    const-string v6, "android.media.metadata.DISC_NUMBER"

    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getListItemCount()I

    move-result v7

    int-to-long v8, v7

    invoke-virtual {v0, v6, v8, v9}, Landroid/media/MediaMetadata$Builder;->putLong(Ljava/lang/String;J)Landroid/media/MediaMetadata$Builder;

    .line 1800
    const-string v6, "android.media.metadata.TRACK_NUMBER"

    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getListPosition()I

    move-result v7

    add-int/lit8 v7, v7, 0x1

    int-to-long v8, v7

    invoke-virtual {v0, v6, v8, v9}, Landroid/media/MediaMetadata$Builder;->putLong(Ljava/lang/String;J)Landroid/media/MediaMetadata$Builder;

    .line 1801
    const-string v6, "android.media.metadata.DURATION"

    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->getDurationFromDb()J

    move-result-wide v8

    invoke-virtual {v0, v6, v8, v9}, Landroid/media/MediaMetadata$Builder;->putLong(Ljava/lang/String;J)Landroid/media/MediaMetadata$Builder;

    .line 1803
    if-nez p1, :cond_1

    .line 1804
    const v6, 0x7f020039

    invoke-static {p0, v6}, Lcom/samsung/musicplus/util/AlbumArtUtils;->getDefaulAlbumArt(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object p1

    .line 1806
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v2

    .line 1808
    .local v2, "config":Landroid/graphics/Bitmap$Config;
    if-nez v2, :cond_2

    sget-object v2, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    .end local v2    # "config":Landroid/graphics/Bitmap$Config;
    :cond_2
    invoke-virtual {p1, v2, v3}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object p1

    .line 1810
    const-string v6, "android.media.metadata.ALBUM_ART"

    invoke-virtual {v0, v6, p1}, Landroid/media/MediaMetadata$Builder;->putBitmap(Ljava/lang/String;Landroid/graphics/Bitmap;)Landroid/media/MediaMetadata$Builder;

    .line 1812
    sget-boolean v6, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_K2HD:Z

    if-eqz v6, :cond_3

    .line 1813
    invoke-static {}, Lcom/samsung/musicplus/util/UiUtils;->isSupportUhqa()Z

    move-result v6

    if-eqz v6, :cond_4

    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getSamplingRate()I

    move-result v6

    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getBitDepth()I

    move-result v7

    invoke-static {v6, v7}, Lcom/samsung/musicplus/util/UiUtils;->isUhqa(II)Z

    move-result v6

    if-eqz v6, :cond_4

    const/4 v1, 0x1

    .line 1815
    .local v1, "bIsUHQ":Z
    :goto_1
    new-instance v6, Landroid/content/Intent;

    const-string v7, "com.sec.android.app.music.intent.action.k2hd"

    invoke-direct {v6, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v7, "isk2hd"

    if-eqz v1, :cond_5

    :goto_2
    invoke-virtual {v6, v7, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v3

    const-string v6, "com.sec.android.app.musiccontroller.broastcasting.permission"

    invoke-virtual {p0, v3, v6}, Lcom/samsung/musicplus/service/PlayerService;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 1818
    .end local v1    # "bIsUHQ":Z
    :cond_3
    iget-object v3, p0, Lcom/samsung/musicplus/service/PlayerService;->mMediaSession:Landroid/media/session/MediaSession;

    invoke-virtual {v0}, Landroid/media/MediaMetadata$Builder;->build()Landroid/media/MediaMetadata;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/media/session/MediaSession;->setMetadata(Landroid/media/MediaMetadata;)V

    goto/16 :goto_0

    :cond_4
    move v1, v3

    .line 1813
    goto :goto_1

    .line 1815
    .restart local v1    # "bIsUHQ":Z
    :cond_5
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getCurrentPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/samsung/musicplus/service/PlayerService;->isK2HD(Ljava/lang/String;)Z

    move-result v3

    goto :goto_2
.end method

.method private updateMediaSessionState(ZZJF)V
    .locals 3
    .param p1, "isPreparing"    # Z
    .param p2, "isPlaying"    # Z
    .param p3, "position"    # J
    .param p5, "speed"    # F

    .prologue
    .line 1823
    if-eqz p1, :cond_0

    const/4 v0, 0x6

    .line 1826
    .local v0, "state":I
    :goto_0
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mMediaStateBuilder:Landroid/media/session/PlaybackState$Builder;

    invoke-virtual {v1, v0, p3, p4, p5}, Landroid/media/session/PlaybackState$Builder;->setState(IJF)Landroid/media/session/PlaybackState$Builder;

    .line 1827
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mMediaSession:Landroid/media/session/MediaSession;

    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService;->mMediaStateBuilder:Landroid/media/session/PlaybackState$Builder;

    invoke-virtual {v2}, Landroid/media/session/PlaybackState$Builder;->build()Landroid/media/session/PlaybackState;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/media/session/MediaSession;->setPlaybackState(Landroid/media/session/PlaybackState;)V

    .line 1828
    return-void

    .line 1823
    .end local v0    # "state":I
    :cond_0
    if-eqz p2, :cond_1

    const/4 v0, 0x3

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->isStop()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x2

    goto :goto_0
.end method

.method private updateNotificationAlbumArt(Landroid/graphics/Bitmap;)V
    .locals 3
    .param p1, "bm"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v2, 0x1

    .line 4943
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mRemoteViewBuilder:Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mRemoteBigViewBuilder:Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;

    if-nez v0, :cond_1

    .line 4960
    :cond_0
    :goto_0
    return-void

    .line 4948
    :cond_1
    if-eqz p1, :cond_2

    .line 4949
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/service/PlayerService;->getResizedBitmapForNotification(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object p1

    .line 4951
    :cond_2
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mRemoteViewBuilder:Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->setAlbumArt(Landroid/graphics/Bitmap;)Lcom/samsung/musicplus/widget/RemoteViewBuilder;

    .line 4952
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mRemoteBigViewBuilder:Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->setAlbumArt(Landroid/graphics/Bitmap;)Lcom/samsung/musicplus/widget/RemoteViewBuilder;

    .line 4954
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mNotiManager:Landroid/app/NotificationManager;

    if-eqz v0, :cond_0

    .line 4955
    const-string v0, "MusicService"

    const-string v1, "updateNotificationAlbumArt notify start "

    invoke-static {v2, v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(ZLjava/lang/String;Ljava/lang/String;)V

    .line 4956
    iput-boolean v2, p0, Lcom/samsung/musicplus/service/PlayerService;->mIsShowNotification:Z

    .line 4957
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->notifyNotification()V

    .line 4958
    const-string v0, "MusicService"

    const-string v1, "updateNotificationAlbumArt notify end "

    invoke-static {v2, v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(ZLjava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private updateNotificationAllInfo()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 4752
    const-string v1, "MusicService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "updateNotificationAllInfo mNotification "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/samsung/musicplus/service/PlayerService;->mNotification:Landroid/app/Notification;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 4754
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mNotification:Landroid/app/Notification;

    if-nez v1, :cond_0

    .line 4755
    new-instance v1, Landroid/app/Notification$Builder;

    invoke-direct {v1, p0}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v8}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    move-result-object v7

    .line 4756
    .local v7, "builder":Landroid/app/Notification$Builder;
    invoke-virtual {v7}, Landroid/app/Notification$Builder;->build()Landroid/app/Notification;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mNotification:Landroid/app/Notification;

    .line 4762
    .end local v7    # "builder":Landroid/app/Notification$Builder;
    :cond_0
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->createNotificationView()Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mRemoteViewBuilder:Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;

    .line 4763
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->createBigNotificationView()Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mRemoteBigViewBuilder:Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;

    .line 4764
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mNotification:Landroid/app/Notification;

    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService;->mRemoteViewBuilder:Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;

    invoke-virtual {v2}, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->build()Landroid/widget/RemoteViews;

    move-result-object v2

    iput-object v2, v1, Landroid/app/Notification;->contentView:Landroid/widget/RemoteViews;

    .line 4765
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mNotification:Landroid/app/Notification;

    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService;->mRemoteBigViewBuilder:Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;

    invoke-virtual {v2}, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->build()Landroid/widget/RemoteViews;

    move-result-object v2

    iput-object v2, v1, Landroid/app/Notification;->bigContentView:Landroid/widget/RemoteViews;

    .line 4767
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mNotification:Landroid/app/Notification;

    iput v8, v1, Landroid/app/Notification;->visibility:I

    .line 4769
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mNotification:Landroid/app/Notification;

    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->getLaunchPendingIntent()Landroid/app/PendingIntent;

    move-result-object v2

    iput-object v2, v1, Landroid/app/Notification;->contentIntent:Landroid/app/PendingIntent;

    .line 4772
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->parsingAlbumArt()Landroid/graphics/Bitmap;

    move-result-object v0

    .line 4773
    .local v0, "bm":Landroid/graphics/Bitmap;
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v1}, Lcom/samsung/musicplus/service/PlayerListManager;->getListType()I

    move-result v1

    invoke-direct {p0, v1, v0}, Lcom/samsung/musicplus/service/PlayerService;->updateAllViews(ILandroid/graphics/Bitmap;)V

    .line 4775
    invoke-direct {p0, v0}, Lcom/samsung/musicplus/service/PlayerService;->updateMediaSessionMeta(Landroid/graphics/Bitmap;)V

    .line 4776
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->isPreparing()Z

    move-result v2

    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->isPlaying()Z

    move-result v3

    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->position()J

    move-result-wide v4

    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getPlaySpeed()F

    move-result v6

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/samsung/musicplus/service/PlayerService;->updateMediaSessionState(ZZJF)V

    .line 4781
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 4782
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mNotification:Landroid/app/Notification;

    const v2, 0x7f0200c0

    iput v2, v1, Landroid/app/Notification;->icon:I

    .line 4787
    :goto_0
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mNotiManager:Landroid/app/NotificationManager;

    if-eqz v1, :cond_2

    .line 4788
    iget-boolean v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mIsShowNotification:Z

    if-nez v1, :cond_1

    .line 4789
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.sec.android.app.music.intent.action.SHOW_CONTEXTUAL_WIDGET"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/service/PlayerService;->sendBroadcast(Landroid/content/Intent;)V

    .line 4791
    :cond_1
    const-string v1, "MusicService"

    const-string v2, "updateNotificationAllInfo notify start "

    invoke-static {v8, v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(ZLjava/lang/String;Ljava/lang/String;)V

    .line 4792
    iput-boolean v8, p0, Lcom/samsung/musicplus/service/PlayerService;->mIsShowNotification:Z

    .line 4793
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->startForeground()V

    .line 4794
    const-string v1, "MusicService"

    const-string v2, "updateNotificationAllInfo notify end "

    invoke-static {v8, v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(ZLjava/lang/String;Ljava/lang/String;)V

    .line 4796
    :cond_2
    return-void

    .line 4784
    :cond_3
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mNotification:Landroid/app/Notification;

    const v2, 0x7f0200bf

    iput v2, v1, Landroid/app/Notification;->icon:I

    goto :goto_0
.end method

.method private updateNotificationConnectivityStatus()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 5015
    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService;->mRemoteViewBuilder:Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService;->mRemoteBigViewBuilder:Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;

    if-nez v2, :cond_1

    .line 5026
    :cond_0
    :goto_0
    return-void

    .line 5018
    :cond_1
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->isConnectedWfd()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->isDmrPlaying()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->isConnectedHDMI()Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_2
    move v0, v1

    .line 5019
    .local v0, "show":Z
    :goto_1
    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService;->mRemoteViewBuilder:Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;

    invoke-virtual {v2, v0}, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->setConnectivityImage(Z)Lcom/samsung/musicplus/widget/RemoteViewBuilder;

    .line 5020
    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService;->mRemoteBigViewBuilder:Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;

    invoke-virtual {v2, v0}, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->setConnectivityImage(Z)Lcom/samsung/musicplus/widget/RemoteViewBuilder;

    .line 5022
    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService;->mNotiManager:Landroid/app/NotificationManager;

    if-eqz v2, :cond_0

    .line 5023
    iput-boolean v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mIsShowNotification:Z

    .line 5024
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->notifyNotification()V

    goto :goto_0

    .line 5018
    .end local v0    # "show":Z
    :cond_3
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private updateNotificationPlayStatus()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 4986
    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService;->mRemoteViewBuilder:Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService;->mRemoteBigViewBuilder:Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;

    if-nez v2, :cond_1

    .line 5012
    :cond_0
    :goto_0
    return-void

    .line 4990
    :cond_1
    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v2}, Lcom/samsung/musicplus/service/PlayerListManager;->getListType()I

    move-result v1

    .line 4991
    .local v1, "listType":I
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->isPlaying()Z

    move-result v0

    .line 4993
    .local v0, "isPlaying":Z
    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService;->mRemoteViewBuilder:Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;

    invoke-virtual {v2, v1, v0}, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->setPlayStatus(IZ)Lcom/samsung/musicplus/widget/RemoteViewBuilder;

    .line 4994
    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService;->mRemoteViewBuilder:Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;

    invoke-virtual {v2, v1}, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->setPlayController(I)Lcom/samsung/musicplus/widget/RemoteViewBuilder;

    .line 4996
    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService;->mRemoteBigViewBuilder:Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;

    invoke-virtual {v2, v1, v0}, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->setPlayStatus(IZ)Lcom/samsung/musicplus/widget/RemoteViewBuilder;

    .line 4997
    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService;->mRemoteBigViewBuilder:Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;

    invoke-virtual {v2, v1}, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->setPlayController(I)Lcom/samsung/musicplus/widget/RemoteViewBuilder;

    .line 5001
    if-eqz v0, :cond_2

    .line 5002
    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService;->mNotification:Landroid/app/Notification;

    const v3, 0x7f0200c0

    iput v3, v2, Landroid/app/Notification;->icon:I

    .line 5006
    :goto_1
    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService;->mNotiManager:Landroid/app/NotificationManager;

    if-eqz v2, :cond_0

    .line 5007
    const-string v2, "MusicService"

    const-string v3, "updateNotificationPlayStatus notify start "

    invoke-static {v4, v2, v3}, Lcom/samsung/musicplus/library/iLog;->d(ZLjava/lang/String;Ljava/lang/String;)V

    .line 5008
    iput-boolean v4, p0, Lcom/samsung/musicplus/service/PlayerService;->mIsShowNotification:Z

    .line 5009
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->notifyNotification()V

    .line 5010
    const-string v2, "MusicService"

    const-string v3, "updateNotificationPlayStatus notify end "

    invoke-static {v4, v2, v3}, Lcom/samsung/musicplus/library/iLog;->d(ZLjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 5004
    :cond_2
    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService;->mNotification:Landroid/app/Notification;

    const v3, 0x7f0200bf

    iput v3, v2, Landroid/app/Notification;->icon:I

    goto :goto_1
.end method

.method private updateNotificationTextMarquee(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 4963
    iget-boolean v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mIsShowNotification:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mRemoteViewBuilder:Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mRemoteBigViewBuilder:Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;

    if-nez v0, :cond_1

    .line 4971
    :cond_0
    :goto_0
    return-void

    .line 4968
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mRemoteViewBuilder:Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->setMarqueeEnabled(Z)Lcom/samsung/musicplus/widget/RemoteViewBuilder;

    .line 4969
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mRemoteBigViewBuilder:Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->setMarqueeEnabled(Z)Lcom/samsung/musicplus/widget/RemoteViewBuilder;

    .line 4970
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->notifyNotification()V

    goto :goto_0
.end method

.method private updateNotificationTitle()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 5082
    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v2}, Lcom/samsung/musicplus/service/PlayerListManager;->getTitle()Ljava/lang/String;

    move-result-object v1

    .line 5083
    .local v1, "title":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v2}, Lcom/samsung/musicplus/service/PlayerListManager;->getArtist()Ljava/lang/String;

    move-result-object v0

    .line 5084
    .local v0, "artist":Ljava/lang/String;
    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService;->mRemoteViewBuilder:Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;

    invoke-virtual {v2, v1, v0}, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->setTitles(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/musicplus/widget/RemoteViewBuilder;

    .line 5085
    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService;->mRemoteBigViewBuilder:Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;

    invoke-virtual {v2, v1, v0}, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->setTitles(Ljava/lang/String;Ljava/lang/String;)Lcom/samsung/musicplus/widget/RemoteViewBuilder;

    .line 5087
    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService;->mNotiManager:Landroid/app/NotificationManager;

    if-eqz v2, :cond_0

    .line 5088
    const-string v2, "MusicService"

    const-string v3, "updateNotificationTitle notify start "

    invoke-static {v4, v2, v3}, Lcom/samsung/musicplus/library/iLog;->d(ZLjava/lang/String;Ljava/lang/String;)V

    .line 5089
    iput-boolean v4, p0, Lcom/samsung/musicplus/service/PlayerService;->mIsShowNotification:Z

    .line 5090
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->notifyNotification()V

    .line 5091
    const-string v2, "MusicService"

    const-string v3, "updateNotificationTitle notify end "

    invoke-static {v4, v2, v3}, Lcom/samsung/musicplus/library/iLog;->d(ZLjava/lang/String;Ljava/lang/String;)V

    .line 5093
    :cond_0
    return-void
.end method

.method private updateRemoteClient(Landroid/graphics/Bitmap;)V
    .locals 8
    .param p1, "bm"    # Landroid/graphics/Bitmap;

    .prologue
    const/4 v0, 0x1

    const/4 v3, 0x0

    .line 4910
    iget-object v4, p0, Lcom/samsung/musicplus/service/PlayerService;->mControlClient:Landroid/media/RemoteControlClient;

    invoke-virtual {v4, v0}, Landroid/media/RemoteControlClient;->editMetadata(Z)Landroid/media/RemoteControlClient$MetadataEditor;

    move-result-object v2

    .line 4911
    .local v2, "ed":Landroid/media/RemoteControlClient$MetadataEditor;
    const/4 v4, 0x7

    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getTitle()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/media/RemoteControlClient$MetadataEditor;->putString(ILjava/lang/String;)Landroid/media/RemoteControlClient$MetadataEditor;

    .line 4912
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getAlbum()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v0, v4}, Landroid/media/RemoteControlClient$MetadataEditor;->putString(ILjava/lang/String;)Landroid/media/RemoteControlClient$MetadataEditor;

    .line 4913
    const/4 v4, 0x2

    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getArtist()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/media/RemoteControlClient$MetadataEditor;->putString(ILjava/lang/String;)Landroid/media/RemoteControlClient$MetadataEditor;

    .line 4914
    const/16 v4, 0xd

    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getArtist()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/media/RemoteControlClient$MetadataEditor;->putString(ILjava/lang/String;)Landroid/media/RemoteControlClient$MetadataEditor;

    .line 4915
    const/4 v4, 0x6

    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getGenre()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/media/RemoteControlClient$MetadataEditor;->putString(ILjava/lang/String;)Landroid/media/RemoteControlClient$MetadataEditor;

    .line 4916
    const/16 v4, 0xe

    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getListItemCount()I

    move-result v5

    int-to-long v6, v5

    invoke-virtual {v2, v4, v6, v7}, Landroid/media/RemoteControlClient$MetadataEditor;->putLong(IJ)Landroid/media/RemoteControlClient$MetadataEditor;

    .line 4919
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getListPosition()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    int-to-long v4, v4

    invoke-virtual {v2, v3, v4, v5}, Landroid/media/RemoteControlClient$MetadataEditor;->putLong(IJ)Landroid/media/RemoteControlClient$MetadataEditor;

    .line 4924
    const/16 v4, 0x9

    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->getDurationFromDb()J

    move-result-wide v6

    invoke-virtual {v2, v4, v6, v7}, Landroid/media/RemoteControlClient$MetadataEditor;->putLong(IJ)Landroid/media/RemoteControlClient$MetadataEditor;

    .line 4926
    if-nez p1, :cond_0

    .line 4927
    const v4, 0x7f020039

    invoke-static {p0, v4}, Lcom/samsung/musicplus/util/AlbumArtUtils;->getDefaulAlbumArt(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object p1

    .line 4930
    :cond_0
    sget-boolean v4, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_K2HD:Z

    if-eqz v4, :cond_1

    .line 4931
    invoke-static {}, Lcom/samsung/musicplus/util/UiUtils;->isSupportUhqa()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getSamplingRate()I

    move-result v4

    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getBitDepth()I

    move-result v5

    invoke-static {v4, v5}, Lcom/samsung/musicplus/util/UiUtils;->isUhqa(II)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 4932
    .local v0, "bIsUHQ":Z
    :goto_0
    new-instance v5, Landroid/content/Intent;

    const-string v4, "com.sec.android.app.music.intent.action.k2hd"

    invoke-direct {v5, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v6, "isk2hd"

    if-eqz v0, :cond_4

    move v4, v3

    :goto_1
    invoke-virtual {v5, v6, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v4

    const-string v5, "com.sec.android.app.musiccontroller.broastcasting.permission"

    invoke-virtual {p0, v4, v5}, Lcom/samsung/musicplus/service/PlayerService;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 4935
    .end local v0    # "bIsUHQ":Z
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v1

    .line 4937
    .local v1, "config":Landroid/graphics/Bitmap$Config;
    if-nez v1, :cond_2

    sget-object v1, Landroid/graphics/Bitmap$Config;->RGB_565:Landroid/graphics/Bitmap$Config;

    .end local v1    # "config":Landroid/graphics/Bitmap$Config;
    :cond_2
    invoke-virtual {p1, v1, v3}, Landroid/graphics/Bitmap;->copy(Landroid/graphics/Bitmap$Config;Z)Landroid/graphics/Bitmap;

    move-result-object p1

    .line 4938
    const/16 v3, 0x64

    invoke-virtual {v2, v3, p1}, Landroid/media/RemoteControlClient$MetadataEditor;->putBitmap(ILandroid/graphics/Bitmap;)Landroid/media/RemoteControlClient$MetadataEditor;

    .line 4939
    invoke-virtual {v2}, Landroid/media/RemoteControlClient$MetadataEditor;->apply()V

    .line 4940
    return-void

    :cond_3
    move v0, v3

    .line 4931
    goto :goto_0

    .line 4932
    .restart local v0    # "bIsUHQ":Z
    :cond_4
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getCurrentPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/samsung/musicplus/service/PlayerService;->isK2HD(Ljava/lang/String;)Z

    move-result v4

    goto :goto_1
.end method

.method private updateSettingToNotification()V
    .locals 3

    .prologue
    .line 4736
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mRemoteBigViewBuilder:Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;

    if-eqz v0, :cond_0

    .line 4737
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mRemoteBigViewBuilder:Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;

    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getShuffle()I

    move-result v1

    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getRepeat()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->updateExtraButtons(II)Lcom/samsung/musicplus/widget/RemoteViewBuilder;

    .line 4740
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mNotiManager:Landroid/app/NotificationManager;

    if-eqz v0, :cond_1

    .line 4741
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mIsShowNotification:Z

    .line 4742
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->notifyNotification()V

    .line 4744
    :cond_1
    return-void
.end method

.method private warningAdaptSound()V
    .locals 4

    .prologue
    .line 1025
    const-string v1, "MusicService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "warningAdaptSound() : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/samsung/musicplus/service/PlayerService;->mWarnAdaptSound:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1027
    iget-boolean v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mWarnAdaptSound:Z

    if-eqz v1, :cond_0

    .line 1028
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mWarnAdaptSound:Z

    .line 1031
    sget-boolean v1, Lcom/samsung/musicplus/util/Version;->LIBRARY_OVER_API_1:Z

    if-eqz v1, :cond_1

    .line 1032
    const v0, 0x7f10016b

    .line 1037
    .local v0, "resId":I
    :goto_0
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mToastHandler:Lcom/samsung/musicplus/service/PlayerService$ToastHandler;

    invoke-virtual {v1, v0}, Lcom/samsung/musicplus/service/PlayerService$ToastHandler;->showToast(I)V

    .line 1039
    .end local v0    # "resId":I
    :cond_0
    return-void

    .line 1034
    :cond_1
    const v0, 0x7f10016c

    .restart local v0    # "resId":I
    goto :goto_0
.end method


# virtual methods
.method public buffering()I
    .locals 1

    .prologue
    .line 4071
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/MultiPlayer;->getBufferingPercent()I

    move-result v0

    return v0
.end method

.method public changeListWithoutPlaying(ILjava/lang/String;[JI)V
    .locals 1
    .param p1, "listType"    # I
    .param p2, "keyWord"    # Ljava/lang/String;
    .param p3, "list"    # [J
    .param p4, "position"    # I

    .prologue
    .line 2894
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->resetListPreset()V

    .line 2895
    monitor-enter p0

    .line 2896
    :try_start_0
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/samsung/musicplus/service/PlayerListManager;->setList(ILjava/lang/String;[JI)V

    .line 2897
    const-string v0, "com.samsung.musicplus.action.QUEUE_CHANGED"

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/service/PlayerService;->notifyChange(Ljava/lang/String;)V

    .line 2898
    monitor-exit p0

    .line 2899
    return-void

    .line 2898
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public changeToDefaultPlayer(Z)V
    .locals 5
    .param p1, "autoPlay"    # Z

    .prologue
    const/4 v4, 0x1

    .line 4311
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayerHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayerHandler:Landroid/os/Handler;

    const/16 v3, 0xa

    if-eqz p1, :cond_1

    const/16 v0, 0x64

    :goto_0
    invoke-virtual {v2, v3, v4, v0}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 4313
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/MultiPlayer;->getMode()I

    move-result v0

    if-ne v0, v4, :cond_0

    .line 4318
    iput-boolean v4, p0, Lcom/samsung/musicplus/service/PlayerService;->mIgnoreNoisy:Z

    .line 4319
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayerHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayerHandler:Landroid/os/Handler;

    const/16 v2, 0x11

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 4322
    :cond_0
    return-void

    .line 4311
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public changeToDmrPlayer(Ljava/lang/String;)V
    .locals 5
    .param p1, "dmrId"    # Ljava/lang/String;

    .prologue
    .line 4304
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayerHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayerHandler:Landroid/os/Handler;

    const/16 v2, 0xa

    const/4 v3, 0x2

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4, p1}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 4306
    return-void
.end method

.method public changeToWfdDevice()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 4325
    const-string v0, "MusicService"

    const-string v1, "changeToWfdDevice"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4326
    iput-boolean v4, p0, Lcom/samsung/musicplus/service/PlayerService;->mIsConnectingWfd:Z

    .line 4327
    invoke-direct {p0, v4}, Lcom/samsung/musicplus/service/PlayerService;->sendPlayerBuffering(Z)V

    .line 4330
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayerHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayerHandler:Landroid/os/Handler;

    const/16 v2, 0xa

    const/16 v3, 0x64

    invoke-virtual {v1, v2, v4, v3}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 4332
    return-void
.end method

.method public dlnaDmrVolumeDown()V
    .locals 1

    .prologue
    .line 4368
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->isDmrPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4369
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/MultiPlayer;->volumeDown()V

    .line 4371
    :cond_0
    return-void
.end method

.method public dlnaDmrVolumeUp()V
    .locals 1

    .prologue
    .line 4362
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->isDmrPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4363
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/MultiPlayer;->volumeUp()V

    .line 4365
    :cond_0
    return-void
.end method

.method public duration()J
    .locals 4

    .prologue
    .line 4075
    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

    invoke-virtual {v2}, Lcom/samsung/musicplus/service/MultiPlayer;->duration()J

    move-result-wide v0

    .line 4076
    .local v0, "duration":J
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gtz v2, :cond_0

    .line 4080
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->getDurationFromDb()J

    move-result-wide v0

    .line 4082
    :cond_0
    return-wide v0
.end method

.method public enqueue([JI)V
    .locals 1
    .param p1, "list"    # [J
    .param p2, "action"    # I

    .prologue
    .line 2988
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/musicplus/service/PlayerListManager;->enqueue([JI)V

    .line 2989
    const-string v0, "com.samsung.musicplus.action.QUEUE_CHANGED"

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/service/PlayerService;->notifyChange(Ljava/lang/String;)V

    .line 2990
    return-void
.end method

.method public getAlbum()Ljava/lang/String;
    .locals 1

    .prologue
    .line 4154
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerListManager;->getAlbum()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAlbumArt()Ljava/lang/String;
    .locals 1

    .prologue
    .line 4174
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerListManager;->getAlbumArt()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAlbumId()J
    .locals 2

    .prologue
    .line 4158
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerListManager;->getAlbumId()J

    move-result-wide v0

    return-wide v0
.end method

.method public getArtist()Ljava/lang/String;
    .locals 1

    .prologue
    .line 4150
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerListManager;->getArtist()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getAudioId()J
    .locals 2

    .prologue
    .line 4095
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerListManager;->getCurAudioId()J

    move-result-wide v0

    return-wide v0
.end method

.method public getAudioSessionId()I
    .locals 1

    .prologue
    .line 3431
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/MultiPlayer;->getAudioSessionId()I

    move-result v0

    return v0
.end method

.method public getBitDepth()I
    .locals 1

    .prologue
    .line 4134
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerListManager;->getBitDepth()I

    move-result v0

    return v0
.end method

.method public getCurrentBaseUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 4281
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerListManager;->getCurrentBaseUriString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCurrentPath()Ljava/lang/String;
    .locals 2

    .prologue
    .line 4111
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->isSlinkTrackList()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 4112
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v1}, Lcom/samsung/musicplus/service/PlayerListManager;->getFilePath()Ljava/lang/String;

    move-result-object v0

    .line 4113
    .local v0, "path":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v1, "content://"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4120
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

    invoke-virtual {v1}, Lcom/samsung/musicplus/service/MultiPlayer;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 4125
    .end local v0    # "path":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v1}, Lcom/samsung/musicplus/service/PlayerListManager;->getFilePath()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getCurrentUri()Ljava/lang/String;
    .locals 2

    .prologue
    .line 4273
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v1}, Lcom/samsung/musicplus/service/PlayerListManager;->getCurrentUri()Landroid/net/Uri;

    move-result-object v0

    .line 4274
    .local v0, "uri":Landroid/net/Uri;
    if-eqz v0, :cond_0

    .line 4275
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    .line 4277
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getDeviceId()J
    .locals 2

    .prologue
    .line 4166
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerListManager;->getDeviceId()J

    move-result-wide v0

    return-wide v0
.end method

.method public getDlnaPlayingDmrId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 4352
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/MultiPlayer;->getDmrId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getDlnaPlayingNic()Ljava/lang/String;
    .locals 4

    .prologue
    .line 4335
    const/4 v0, 0x0

    .line 4336
    .local v0, "nic":Ljava/lang/String;
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->isDlnaTrackList()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4337
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v2}, Lcom/samsung/musicplus/service/PlayerListManager;->getKeyWord()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/util/DlnaUtils;->getNic(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 4339
    :cond_0
    const-string v1, "MusicService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getDlnaPlayingNic "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 4340
    return-object v0
.end method

.method public getGenre()Ljava/lang/String;
    .locals 1

    .prologue
    .line 4162
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerListManager;->getGenre()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getKeyWord()Ljava/lang/String;
    .locals 1

    .prologue
    .line 4182
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerListManager;->getKeyWord()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getListItemCount()I
    .locals 1

    .prologue
    .line 4190
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerListManager;->getListItemCount()I

    move-result v0

    return v0
.end method

.method public getListPosition()I
    .locals 1

    .prologue
    .line 4194
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerListManager;->getListPosition()I

    move-result v0

    return v0
.end method

.method public getListType()I
    .locals 1

    .prologue
    .line 4178
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerListManager;->getListType()I

    move-result v0

    return v0
.end method

.method public getMediaType()I
    .locals 1

    .prologue
    .line 4130
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerListManager;->getMediaType()I

    move-result v0

    return v0
.end method

.method public getMimeType()Ljava/lang/String;
    .locals 1

    .prologue
    .line 4142
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerListManager;->getMime()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMoodValue()I
    .locals 1

    .prologue
    .line 4222
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerListManager;->getMoodValue()I

    move-result v0

    return v0
.end method

.method public getNewSoundAliveBandLevel(I)I
    .locals 1
    .param p1, "bandNum"    # I

    .prologue
    .line 5276
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/service/MultiPlayer;->getNewSoundAliveBandLevel(I)I

    move-result v0

    return v0
.end method

.method public getNewSoundAliveCurrentPreset()I
    .locals 1

    .prologue
    .line 5268
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/MultiPlayer;->getNewSoundAliveCurrentPreset()I

    move-result v0

    return v0
.end method

.method public getNewSoundAliveRoundedStrength(I)I
    .locals 1
    .param p1, "type"    # I

    .prologue
    .line 5280
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/service/MultiPlayer;->getNewSoundAliveRoundedStrength(I)I

    move-result v0

    return v0
.end method

.method public getNewSoundAliveSquarePosition()[I
    .locals 1

    .prologue
    .line 5272
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/MultiPlayer;->getNewSoundAliveSquarePosition()[I

    move-result-object v0

    return-object v0
.end method

.method public getNextUri()Ljava/lang/String;
    .locals 2

    .prologue
    .line 4285
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v1}, Lcom/samsung/musicplus/service/PlayerListManager;->getNextMediaUri()Landroid/net/Uri;

    move-result-object v0

    .line 4286
    .local v0, "uri":Landroid/net/Uri;
    if-eqz v0, :cond_0

    .line 4287
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    .line 4289
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getOrganizedQueue()[J
    .locals 1

    .prologue
    .line 2975
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerListManager;->getOrganizedQueue()[J

    move-result-object v0

    return-object v0
.end method

.method public getPersonalMode()I
    .locals 1

    .prologue
    .line 4226
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerListManager;->getPersonalMode()I

    move-result v0

    return v0
.end method

.method public getPlaySpeed()F
    .locals 1

    .prologue
    .line 3477
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/MultiPlayer;->getPlaySpeed()F

    move-result v0

    return v0
.end method

.method public getPreferencesFloat(Ljava/lang/String;F)F
    .locals 4
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "value"    # F

    .prologue
    .line 4267
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mPreferences:Landroid/content/SharedPreferences;

    invoke-interface {v1, p1, p2}, Landroid/content/SharedPreferences;->getFloat(Ljava/lang/String;F)F

    move-result v0

    .line 4268
    .local v0, "realValue":F
    const-string v1, "MusicService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getPreferencesFloat key : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " value : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 4269
    return v0
.end method

.method public getPrevUri()Ljava/lang/String;
    .locals 2

    .prologue
    .line 4293
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v1}, Lcom/samsung/musicplus/service/PlayerListManager;->getPrevMediaUri()Landroid/net/Uri;

    move-result-object v0

    .line 4294
    .local v0, "uri":Landroid/net/Uri;
    if-eqz v0, :cond_0

    .line 4295
    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    .line 4297
    :goto_0
    return-object v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getQueue()[J
    .locals 1

    .prologue
    .line 2979
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerListManager;->getQueue()[J

    move-result-object v0

    return-object v0
.end method

.method public getRepeat()I
    .locals 1

    .prologue
    .line 4198
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerListManager;->getRepeatMode()I

    move-result v0

    return v0
.end method

.method public getSamplingRate()I
    .locals 1

    .prologue
    .line 4138
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerListManager;->getSamplingRate()I

    move-result v0

    return v0
.end method

.method public getShuffle()I
    .locals 1

    .prologue
    .line 4186
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerListManager;->getShuffle()I

    move-result v0

    return v0
.end method

.method public getSoundAlive()I
    .locals 1

    .prologue
    .line 3427
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/MultiPlayer;->getSoundAlive()I

    move-result v0

    return v0
.end method

.method public getSoundAliveUserEQ()[I
    .locals 1

    .prologue
    .line 3439
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/MultiPlayer;->getSoundAliveUserEq()[I

    move-result-object v0

    return-object v0
.end method

.method public getSoundAliveUserExt()[I
    .locals 1

    .prologue
    .line 3435
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/MultiPlayer;->getSoundAliveUserExtended()[I

    move-result-object v0

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 4146
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerListManager;->getTitle()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public isAudioShockWarningEnabled()Z
    .locals 1

    .prologue
    .line 718
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

    invoke-virtual {v0}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->isAudioPathEarjack()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

    invoke-virtual {v0}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->isAudioPathBT()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isBtConnected()Z
    .locals 2

    .prologue
    .line 3271
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mBluetoothA2dp:Landroid/bluetooth/BluetoothA2dp;

    if-eqz v1, :cond_0

    .line 3272
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mBluetoothA2dp:Landroid/bluetooth/BluetoothA2dp;

    invoke-virtual {v1}, Landroid/bluetooth/BluetoothA2dp;->getConnectedDevices()Ljava/util/List;

    move-result-object v0

    .line 3273
    .local v0, "deviceList":Ljava/util/List;, "Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_0

    .line 3274
    const/4 v1, 0x1

    .line 3277
    .end local v0    # "deviceList":Ljava/util/List;, "Ljava/util/List<Landroid/bluetooth/BluetoothDevice;>;"
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public isConnectingWfd()Z
    .locals 1

    .prologue
    .line 4107
    iget-boolean v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mIsConnectingWfd:Z

    return v0
.end method

.method public isEnableToPlaying()Z
    .locals 3

    .prologue
    .line 3689
    sget-boolean v1, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_PLAYING_MUSIC_DURING_CALL:Z

    if-nez v1, :cond_0

    .line 3690
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->isDmrPlaying()Z

    move-result v1

    if-nez v1, :cond_0

    .line 3691
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/musicplus/library/hardware/CallStateChecker;->isCallIdle(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 3692
    const-string v1, "MusicService"

    const-string v2, "startPlay Can\'t play during call"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3693
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mToastHandler:Lcom/samsung/musicplus/service/PlayerService$ToastHandler;

    const v2, 0x7f1001b3

    invoke-virtual {v1, v2}, Lcom/samsung/musicplus/service/PlayerService$ToastHandler;->showToast(I)V

    .line 3698
    iget-boolean v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mPausedByTransientLossOfFocus:Z

    .line 3699
    .local v0, "focus":Z
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->pause()V

    .line 3700
    iput-boolean v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mPausedByTransientLossOfFocus:Z

    .line 3701
    const/4 v1, 0x0

    .line 3706
    .end local v0    # "focus":Z
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public isK2HD(Ljava/lang/String;)Z
    .locals 3
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 1202
    sget-boolean v1, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_K2HD:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mPreferences:Landroid/content/SharedPreferences;

    const-string v2, "K2HD"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p1}, Lcom/samsung/musicplus/util/UiUtils;->isLocalContents(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public isLocalMediaTrack()Z
    .locals 1

    .prologue
    .line 2843
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->isDlnaTrackList()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->isSlinkTrackList()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/MultiPlayer;->isNetworkPath()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->isExtraTrack()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPlaying()Z
    .locals 1

    .prologue
    .line 4099
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/MultiPlayer;->isPlaying()Z

    move-result v0

    return v0
.end method

.method public isPreparing()Z
    .locals 1

    .prologue
    .line 4103
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/MultiPlayer;->isPreparing()Z

    move-result v0

    return v0
.end method

.method public isSupportPlaySpeed()Z
    .locals 1

    .prologue
    .line 3481
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->isSlinkTrackList()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->isDlnaTrackList()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->isDmrPlaying()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public moveQueueItem(II)V
    .locals 1
    .param p1, "from"    # I
    .param p2, "to"    # I

    .prologue
    .line 2983
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/musicplus/service/PlayerListManager;->moveQueueItem(II)V

    .line 2984
    const-string v0, "com.samsung.musicplus.action.QUEUE_CHANGED"

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/service/PlayerService;->notifyChange(Ljava/lang/String;)V

    .line 2985
    return-void
.end method

.method public next(Z)V
    .locals 4
    .param p1, "ignoreRepeatOne"    # Z

    .prologue
    .line 3999
    monitor-enter p0

    .line 4000
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/samsung/musicplus/service/PlayerService;->bErrorHandleToPrev:Z

    .line 4001
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/service/PlayerListManager;->moveToNext(Z)Z

    .line 4002
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->isPlaying()Z

    move-result v0

    const-wide/16 v2, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v2, v3, v1}, Lcom/samsung/musicplus/service/PlayerService;->openCurrent(ZJZ)V

    .line 4003
    monitor-exit p0

    .line 4004
    return-void

    .line 4003
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public notiChangePrefK2HD()V
    .locals 2

    .prologue
    .line 1196
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mNotifyHandler:Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;->notifyPlayStatusChanged()V

    .line 1197
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mNotifyHandler:Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;->updateNotificationAllInfo()V

    .line 1198
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mServiceAppWidggetManger:Lcom/samsung/musicplus/widget/appwidget/ServiceAppWidgetManager;

    const-string v1, "com.android.music.metachanged"

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/widget/appwidget/ServiceAppWidgetManager;->performUpdate(Ljava/lang/String;)V

    .line 1199
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 2098
    const-string v0, "MusicService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onBind "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2100
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mDelayedStopHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 2101
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mServiceInUse:Z

    .line 2102
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mBinder:Landroid/os/IBinder;

    return-object v0
.end method

.method public onCreate()V
    .locals 6

    .prologue
    .line 1319
    const-string v1, "MusicService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onCreate "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1320
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 1321
    .local v0, "context":Landroid/content/Context;
    invoke-static {v0}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->getInstance(Landroid/content/Context;)Lcom/samsung/musicplus/library/audio/SecAudioManager;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

    .line 1322
    new-instance v1, Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;-><init>(Lcom/samsung/musicplus/service/PlayerService;)V

    iput-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mNotifyHandler:Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;

    .line 1323
    new-instance v1, Lcom/samsung/musicplus/widget/appwidget/ServiceAppWidgetManager;

    invoke-direct {v1, p0}, Lcom/samsung/musicplus/widget/appwidget/ServiceAppWidgetManager;-><init>(Lcom/samsung/musicplus/service/PlayerService;)V

    iput-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mServiceAppWidggetManger:Lcom/samsung/musicplus/widget/appwidget/ServiceAppWidgetManager;

    .line 1325
    sget-boolean v1, Lcom/samsung/musicplus/service/PlayerService;->SUPPORT_REMOTE_CONTROL_CLIENT:Z

    if-eqz v1, :cond_2

    .line 1326
    sget-object v1, Lcom/samsung/musicplus/service/PlayerService;->COMPONENT_NAME:Landroid/content/ComponentName;

    invoke-direct {p0, v1}, Lcom/samsung/musicplus/service/PlayerService;->createControlClient(Landroid/content/ComponentName;)V

    .line 1330
    :goto_0
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->registerMediaButtonReceiver()V

    .line 1338
    new-instance v1, Lcom/samsung/musicplus/service/MultiPlayer;

    invoke-direct {v1, v0}, Lcom/samsung/musicplus/service/MultiPlayer;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

    .line 1339
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

    invoke-virtual {v1, p0}, Lcom/samsung/musicplus/service/MultiPlayer;->setOnPreparedListener(Lcom/samsung/musicplus/service/MultiPlayer$OnPreparedListener;)V

    .line 1340
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayerHandler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Lcom/samsung/musicplus/service/MultiPlayer;->setHandler(Landroid/os/Handler;)V

    .line 1345
    new-instance v1, Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-direct {v1, v0}, Lcom/samsung/musicplus/service/PlayerListManager;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    .line 1346
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v1, v2}, Lcom/samsung/musicplus/service/MultiPlayer;->setListManager(Lcom/samsung/musicplus/service/PlayerListManager;)V

    .line 1347
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    new-instance v2, Lcom/samsung/musicplus/service/PlayerService$11;

    invoke-direct {v2, p0}, Lcom/samsung/musicplus/service/PlayerService$11;-><init>(Lcom/samsung/musicplus/service/PlayerService;)V

    invoke-virtual {v1, v2}, Lcom/samsung/musicplus/service/PlayerListManager;->startContentObserving(Lcom/samsung/musicplus/service/PlayerListManager$OnListChangeListener;)V

    .line 1391
    const-string v1, "music_service_pref"

    const/4 v2, 0x4

    invoke-virtual {p0, v1, v2}, Lcom/samsung/musicplus/service/PlayerService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mPreferences:Landroid/content/SharedPreferences;

    .line 1395
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->loadSavedValues()V

    .line 1408
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mListLoader:Landroid/os/Handler;

    const/4 v2, 0x0

    const-wide/16 v4, 0x12c

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1417
    const-string v1, "notification"

    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/service/PlayerService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/NotificationManager;

    iput-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mNotiManager:Landroid/app/NotificationManager;

    .line 1423
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->registerCommandReceiver()V

    .line 1424
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->registerHeadsetStateReceiver()V

    .line 1425
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->registerSystemReceiver()V

    .line 1426
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->registerSettingReceiver()V

    .line 1427
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->registerWifiDisplayReceiver()V

    .line 1428
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->registerHdmiReceiver()V

    .line 1429
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->registerOtherDeviceWatcher()V

    .line 1434
    sget-boolean v1, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_DATA_CHECK_POPUP:Z

    if-nez v1, :cond_0

    .line 1435
    sget-object v1, Lcom/samsung/musicplus/contents/dlna/DlnaStore$Server;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/samsung/musicplus/contents/dlna/DlnaStore$ServerContents;->CONTENT_URI:Landroid/net/Uri;

    sget-object v3, Lcom/samsung/musicplus/contents/dlna/DlnaStore$Renderer;->CONTENT_URI:Landroid/net/Uri;

    sget-object v4, Lcom/samsung/musicplus/contents/dlna/DlnaStore;->DLNA_ALL_DELETE_URI:Landroid/net/Uri;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/musicplus/library/dlna/DlnaManager;->getInstance(Landroid/content/Context;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;)Lcom/samsung/musicplus/library/dlna/DlnaManager;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mDlnaManager:Lcom/samsung/musicplus/library/dlna/DlnaManager;

    .line 1438
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mDlnaManager:Lcom/samsung/musicplus/library/dlna/DlnaManager;

    invoke-virtual {v1}, Lcom/samsung/musicplus/library/dlna/DlnaManager;->bindDlnaService()V

    .line 1445
    :cond_0
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    .line 1446
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v1, :cond_1

    .line 1447
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService;->mBluetoothServiceListener:Landroid/bluetooth/BluetoothProfile$ServiceListener;

    const/4 v3, 0x2

    invoke-virtual {v1, v0, v2, v3}, Landroid/bluetooth/BluetoothAdapter;->getProfileProxy(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;I)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1449
    const-string v1, "MusicService"

    const-string v2, "onStart. Getting Headset Proxy failed"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1456
    :cond_1
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 1460
    const-string v1, "MusicService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onCreate end"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1461
    sput-object p0, Lcom/samsung/musicplus/service/PlayerService;->sService:Lcom/samsung/musicplus/service/PlayerService;

    .line 1462
    return-void

    .line 1328
    :cond_2
    invoke-direct {p0, v0}, Lcom/samsung/musicplus/service/PlayerService;->createMediaSession(Landroid/content/Context;)Landroid/media/session/MediaSession;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mMediaSession:Landroid/media/session/MediaSession;

    goto/16 :goto_0
.end method

.method public onDestroy()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 2023
    const-string v0, "MusicService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onDestroy "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2024
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mNotifyHandler:Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;

    invoke-virtual {v0, v3}, Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 2025
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mAudioPathHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 2026
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayerHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 2027
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mAlbumArtHandler:Landroid/os/Handler;

    invoke-virtual {v0, v3}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 2029
    sput-object v3, Lcom/samsung/musicplus/service/PlayerService;->sService:Lcom/samsung/musicplus/service/PlayerService;

    .line 2030
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/service/PlayerService;->saveQueue(Z)V

    .line 2032
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerListManager;->release()V

    .line 2034
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->hideNotification()V

    .line 2036
    const-string v0, "com.android.music.playstatechanged"

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/service/PlayerService;->notifyChange(Ljava/lang/String;)V

    .line 2039
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 2040
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mControlClient:Landroid/media/RemoteControlClient;

    if-eqz v0, :cond_8

    .line 2041
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mControlClient:Landroid/media/RemoteControlClient;

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->unregisterRemoteControlClient(Landroid/media/RemoteControlClient;)V

    .line 2046
    :goto_0
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mCommandReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/service/PlayerService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 2047
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mHeadsetReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/service/PlayerService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 2048
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mSystemReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/service/PlayerService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 2049
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mSettingReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/service/PlayerService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 2050
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->unregisterCurrentSongObserver()V

    .line 2051
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->unregisterWifiDisplayReceiver()V

    .line 2052
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mHdmiReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/service/PlayerService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 2053
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->unregisterDlanWatcher()V

    .line 2055
    sget-boolean v0, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_SUPPORT_SET_BATTERY_ADC:Z

    if-eqz v0, :cond_0

    .line 2056
    invoke-static {v4}, Lcom/samsung/musicplus/library/hardware/BatteryState;->setTemperatureCheck(Z)V

    .line 2059
    :cond_0
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->unregisterMotionListener()V

    .line 2060
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->unregisterBounceListener()V

    .line 2061
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mBounceMotionManager:Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager;

    if-eqz v0, :cond_1

    .line 2062
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mBounceMotionManager:Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager;

    invoke-virtual {v0}, Lcom/samsung/musicplus/library/hardware/SimpleBounceMotionManager;->unregisterSettingChangedListener()V

    .line 2065
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mSmartVolume:Lcom/samsung/musicplus/library/audio/SmartVolumeManager;

    if-eqz v0, :cond_2

    .line 2066
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

    invoke-virtual {v0, v4}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->setSmartVoumeEnable(Z)V

    .line 2067
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mSmartVolume:Lcom/samsung/musicplus/library/audio/SmartVolumeManager;

    invoke-virtual {v0}, Lcom/samsung/musicplus/library/audio/SmartVolumeManager;->release()I

    .line 2068
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mVolumeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/service/PlayerService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 2070
    :cond_2
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mAdaptSound:Lcom/samsung/musicplus/library/audio/AdaptSound;

    if-eqz v0, :cond_3

    .line 2071
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mAdaptSound:Lcom/samsung/musicplus/library/audio/AdaptSound;

    invoke-virtual {v0}, Lcom/samsung/musicplus/library/audio/AdaptSound;->release()V

    .line 2073
    :cond_3
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/MultiPlayer;->release()V

    .line 2074
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mDlnaManager:Lcom/samsung/musicplus/library/dlna/DlnaManager;

    if-eqz v0, :cond_4

    .line 2075
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mDlnaManager:Lcom/samsung/musicplus/library/dlna/DlnaManager;

    invoke-virtual {v0}, Lcom/samsung/musicplus/library/dlna/DlnaManager;->release()V

    .line 2076
    iput-object v3, p0, Lcom/samsung/musicplus/service/PlayerService;->mDlnaManager:Lcom/samsung/musicplus/library/dlna/DlnaManager;

    .line 2079
    :cond_4
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mBluetoothA2dp:Landroid/bluetooth/BluetoothA2dp;

    if-eqz v0, :cond_6

    .line 2080
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    if-eqz v0, :cond_5

    .line 2081
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mBluetoothAdapter:Landroid/bluetooth/BluetoothAdapter;

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService;->mBluetoothA2dp:Landroid/bluetooth/BluetoothA2dp;

    invoke-virtual {v0, v1, v2}, Landroid/bluetooth/BluetoothAdapter;->closeProfileProxy(ILandroid/bluetooth/BluetoothProfile;)V

    .line 2083
    :cond_5
    iput-object v3, p0, Lcom/samsung/musicplus/service/PlayerService;->mBluetoothA2dp:Landroid/bluetooth/BluetoothA2dp;

    .line 2085
    :cond_6
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 2086
    iget-boolean v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mIsStopPacakgeAfterDestroyed:Z

    if-eqz v0, :cond_7

    .line 2088
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mPackageStopHandler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 2090
    :cond_7
    return-void

    .line 2043
    :cond_8
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mMediaSession:Landroid/media/session/MediaSession;

    invoke-virtual {v0}, Landroid/media/session/MediaSession;->release()V

    goto/16 :goto_0
.end method

.method public onPrepared(Z)V
    .locals 4
    .param p1, "needToPlay"    # Z

    .prologue
    .line 3214
    const-string v1, "MusicService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onPrepared needToPlay : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3215
    monitor-enter p0

    .line 3218
    :try_start_0
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

    invoke-virtual {v1}, Lcom/samsung/musicplus/service/MultiPlayer;->getPlaySpeed()F

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/samsung/musicplus/service/PlayerService;->setPlaySpeed(FZ)V

    .line 3220
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mAdaptSound:Lcom/samsung/musicplus/library/audio/AdaptSound;

    if-eqz v1, :cond_0

    .line 3225
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mAdaptSound:Lcom/samsung/musicplus/library/audio/AdaptSound;

    invoke-virtual {v1}, Lcom/samsung/musicplus/library/audio/AdaptSound;->release()V

    .line 3226
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mAdaptSound:Lcom/samsung/musicplus/library/audio/AdaptSound;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3232
    :try_start_1
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->ensureAdaptSound()V

    .line 3233
    iget-boolean v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mIsAdaptSoundOn:Z

    const/4 v2, 0x0

    invoke-virtual {p0, v1, v2}, Lcom/samsung/musicplus/service/PlayerService;->setAdaptSound(ZZ)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3241
    :cond_0
    :goto_0
    if-eqz p1, :cond_1

    .line 3242
    const/4 v1, 0x0

    :try_start_2
    invoke-virtual {p0, v1}, Lcom/samsung/musicplus/service/PlayerService;->play(Z)V

    .line 3247
    :cond_1
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v1}, Lcom/samsung/musicplus/service/PlayerListManager;->savePlayedInfo()V

    .line 3248
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mNotifyHandler:Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;

    invoke-virtual {v1}, Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;->notifyPrepareCompleted()V

    .line 3249
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->savePlayPosition()V

    .line 3250
    monitor-exit p0

    .line 3251
    return-void

    .line 3234
    :catch_0
    move-exception v0

    .line 3235
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    const-string v1, "MusicService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onPrepared() - IllegalArgumentException!! - mIsAdaptSoundOn : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/samsung/musicplus/service/PlayerService;->mIsAdaptSoundOn:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 3250
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v1
.end method

.method public onRebind(Landroid/content/Intent;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 2111
    const-string v0, "MusicService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onBind "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2113
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mDelayedStopHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 2114
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mServiceInUse:Z

    .line 2115
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 2136
    const-string v0, "MusicService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onStartCommand intent "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " flags "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " startId "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2139
    iput p3, p0, Lcom/samsung/musicplus/service/PlayerService;->mServiceStartId:I

    .line 2140
    if-eqz p1, :cond_0

    .line 2141
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/service/PlayerService;->handleCommandIntent(Landroid/content/Intent;)V

    .line 2143
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 2119
    const-string v0, "MusicService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onUnbind "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2125
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mDelayedStopHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mDelayedStopHandler:Landroid/os/Handler;

    invoke-virtual {v1}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v1

    const-wide/32 v2, 0xea60

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 2126
    const/4 v0, 0x1

    return v0
.end method

.method public open(ILjava/lang/String;[JIJZ)V
    .locals 3
    .param p1, "listType"    # I
    .param p2, "keyWord"    # Ljava/lang/String;
    .param p3, "list"    # [J
    .param p4, "position"    # I
    .param p5, "seekPosition"    # J
    .param p7, "play"    # Z

    .prologue
    .line 2914
    const-string v0, "MusicService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " open listType : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " key : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " position : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " seekPosition "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p5, p6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " play : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p7}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2916
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->resetListPreset()V

    .line 2917
    monitor-enter p0

    .line 2918
    :try_start_0
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/samsung/musicplus/service/PlayerListManager;->setList(ILjava/lang/String;[JI)V

    .line 2919
    const/4 v0, 0x1

    invoke-direct {p0, p7, p5, p6, v0}, Lcom/samsung/musicplus/service/PlayerService;->openCurrent(ZJZ)V

    .line 2920
    const-string v0, "com.samsung.musicplus.action.QUEUE_CHANGED"

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/service/PlayerService;->notifyChange(Ljava/lang/String;)V

    .line 2921
    monitor-exit p0

    .line 2922
    return-void

    .line 2921
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public open(ILjava/lang/String;[JIZ)V
    .locals 9
    .param p1, "listType"    # I
    .param p2, "keyWord"    # Ljava/lang/String;
    .param p3, "list"    # [J
    .param p4, "position"    # I
    .param p5, "play"    # Z

    .prologue
    .line 2890
    const-wide/16 v6, 0x0

    move-object v1, p0

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move v8, p5

    invoke-virtual/range {v1 .. v8}, Lcom/samsung/musicplus/service/PlayerService;->open(ILjava/lang/String;[JIJZ)V

    .line 2891
    return-void
.end method

.method public pause()V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 3839
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayerHandler:Landroid/os/Handler;

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 3841
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 3842
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/MultiPlayer;->pause()V

    .line 3843
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->isPlaying()Z

    move-result v3

    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->position()J

    move-result-wide v4

    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getPlaySpeed()F

    move-result v6

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/samsung/musicplus/service/PlayerService;->setPlaybackState(ZZJF)V

    .line 3845
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mNotifyHandler:Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;->notifyPlayStatusChanged()V

    .line 3846
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mNotifyHandler:Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;->updateNotificationPlayStatus()V

    .line 3847
    iget-boolean v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mPausedByTransientLossOfFocus:Z

    if-nez v0, :cond_0

    .line 3848
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->setServiceTimeout()V

    .line 3850
    :cond_0
    invoke-static {}, Lcom/samsung/musicplus/library/MusicFeatures;->isGateEnable()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3851
    const-string v0, "GATE"

    const-string v1, "<GATE-M> PAUSE_MUSIC </GATE-M>"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3853
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mEnableSideSyncToast:Z

    .line 3859
    :goto_0
    invoke-direct {p0, v2}, Lcom/samsung/musicplus/service/PlayerService;->setSmartVolumeEarSafety(Z)V

    .line 3860
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->unregisterMotionListener()V

    .line 3861
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->unregisterBounceListener()V

    .line 3862
    const-string v0, "pause"

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/service/PlayerService;->unregisterGestureListener(Ljava/lang/String;)V

    .line 3863
    sget-boolean v0, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_SUPPORT_SET_BATTERY_ADC:Z

    if-eqz v0, :cond_2

    .line 3864
    invoke-static {v2}, Lcom/samsung/musicplus/library/hardware/BatteryState;->setTemperatureCheck(Z)V

    .line 3866
    :cond_2
    return-void

    .line 3856
    :cond_3
    iput-boolean v2, p0, Lcom/samsung/musicplus/service/PlayerService;->mPausedByTransientLossOfFocus:Z

    goto :goto_0
.end method

.method public play(Z)V
    .locals 12
    .param p1, "byUser"    # Z

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x1

    .line 3710
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->isEnableToPlaying()Z

    move-result v1

    if-nez v1, :cond_1

    .line 3809
    :cond_0
    :goto_0
    return-void

    .line 3716
    :cond_1
    iput-boolean v11, p0, Lcom/samsung/musicplus/service/PlayerService;->mPausedByTransientLossOfFocus:Z

    .line 3717
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->isDmrPlaying()Z

    move-result v1

    if-nez v1, :cond_2

    .line 3718
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService;->mAudioFocusListener:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    const/4 v3, 0x3

    invoke-virtual {v1, v2, v3, v10}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    move-result v8

    .line 3720
    .local v8, "result":I
    const-string v1, "MusicService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "request audio focus result : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3723
    .end local v8    # "result":I
    :cond_2
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->registerMediaButtonReceiver()V

    .line 3724
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->cancelServiceTimeOut()V

    .line 3726
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

    invoke-virtual {v1}, Lcom/samsung/musicplus/service/MultiPlayer;->isInitialized()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 3727
    iget-boolean v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mIsActiveSmartVolume:Z

    if-eqz v1, :cond_3

    .line 3728
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getCurrentPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/samsung/musicplus/service/PlayerService;->adjustSmartVolume(Ljava/lang/String;)V

    .line 3730
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->isPlaying()Z

    move-result v9

    .line 3731
    .local v9, "wasPlaying":Z
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

    invoke-virtual {v1}, Lcom/samsung/musicplus/service/MultiPlayer;->start()V

    .line 3732
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->isPreparing()Z

    move-result v2

    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->isPlaying()Z

    move-result v3

    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->position()J

    move-result-wide v4

    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getPlaySpeed()F

    move-result v6

    move-object v1, p0

    invoke-direct/range {v1 .. v6}, Lcom/samsung/musicplus/service/PlayerService;->setPlaybackState(ZZJF)V

    .line 3736
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayerHandler:Landroid/os/Handler;

    const/16 v2, 0xb

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 3737
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayerHandler:Landroid/os/Handler;

    const/16 v2, 0xc

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 3738
    if-nez v9, :cond_4

    .line 3739
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mNotifyHandler:Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;

    invoke-virtual {v1}, Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;->notifyPlayStatusChanged()V

    .line 3740
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mNotifyHandler:Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;

    invoke-virtual {v1}, Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;->updateNotificationPlayStatus()V

    .line 3742
    :cond_4
    iget-boolean v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mIsActiveSmartVolume:Z

    invoke-direct {p0, v1}, Lcom/samsung/musicplus/service/PlayerService;->setSmartVolumeEarSafety(Z)V

    .line 3743
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->registerMotionListener()V

    .line 3744
    sget-boolean v1, Lcom/samsung/musicplus/util/MusicStaticFeatures;->FLAG_SUPPORT_SET_BATTERY_ADC:Z

    if-eqz v1, :cond_5

    .line 3745
    invoke-static {v10}, Lcom/samsung/musicplus/library/hardware/BatteryState;->setTemperatureCheck(Z)V

    .line 3747
    :cond_5
    invoke-static {}, Lcom/samsung/musicplus/library/MusicFeatures;->isGateEnable()Z

    move-result v1

    if-eqz v1, :cond_6

    .line 3748
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

    invoke-virtual {v1}, Lcom/samsung/musicplus/service/MultiPlayer;->position()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-nez v1, :cond_8

    .line 3749
    const-string v1, "GATE"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "<GATE-M> MUSIC_PLAYING: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getCurrentPath()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "</GATE-M>"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 3754
    :cond_6
    :goto_1
    const-string v1, "playing"

    invoke-direct {p0, v1}, Lcom/samsung/musicplus/service/PlayerService;->registerGestureListener(Ljava/lang/String;)V

    .line 3755
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->registerBounceListener()V

    .line 3776
    .end local v9    # "wasPlaying":Z
    :goto_2
    iget-boolean v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mEnableSideSyncToast:Z

    if-eqz v1, :cond_7

    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/musicplus/library/audio/SideSyncManager;->isSideSyncConnected(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 3778
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mToastHandler:Lcom/samsung/musicplus/service/PlayerService$ToastHandler;

    const v2, 0x7f10006e

    invoke-virtual {v1, v2}, Lcom/samsung/musicplus/service/PlayerService$ToastHandler;->showToast(I)V

    .line 3779
    iput-boolean v11, p0, Lcom/samsung/musicplus/service/PlayerService;->mEnableSideSyncToast:Z

    .line 3783
    :cond_7
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getCurrentPath()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 3791
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mNotification:Landroid/app/Notification;

    if-nez v1, :cond_c

    .line 3792
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->updateNotificationAllInfo()V

    goto/16 :goto_0

    .line 3751
    .restart local v9    # "wasPlaying":Z
    :cond_8
    const-string v1, "GATE"

    const-string v2, "<GATE-M> RESUME_MUSIC </GATE-M>"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 3757
    .end local v9    # "wasPlaying":Z
    :cond_9
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->resetListPreset()V

    .line 3762
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->isDlnaTrackList()Z

    move-result v1

    if-nez v1, :cond_a

    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->isSlinkTrackList()Z

    move-result v1

    if-nez v1, :cond_a

    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->isExtraTrack()Z

    move-result v1

    if-nez v1, :cond_a

    .line 3763
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->reloadQueue()V

    .line 3764
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v1}, Lcom/samsung/musicplus/service/PlayerListManager;->reloadAllTrackIfNoListExist()V

    .line 3765
    invoke-direct {p0, v11}, Lcom/samsung/musicplus/service/PlayerService;->saveQueue(Z)V

    .line 3769
    :cond_a
    if-eqz p1, :cond_b

    .line 3770
    invoke-direct {p0, v10}, Lcom/samsung/musicplus/service/PlayerService;->open(Z)V

    goto :goto_2

    .line 3772
    :cond_b
    invoke-direct {p0, v10}, Lcom/samsung/musicplus/service/PlayerService;->openInternal(Z)V

    goto :goto_2

    .line 3794
    :cond_c
    const-string v1, "MusicService"

    const-string v2, "play startForeground start "

    invoke-static {v10, v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(ZLjava/lang/String;Ljava/lang/String;)V

    .line 3795
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mRemoteViewBuilder:Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;

    if-eqz v1, :cond_d

    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mRemoteBigViewBuilder:Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;

    if-eqz v1, :cond_d

    .line 3796
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getListType()I

    move-result v7

    .line 3797
    .local v7, "listType":I
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->isPlaying()Z

    move-result v0

    .line 3798
    .local v0, "isPlaying":Z
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mRemoteViewBuilder:Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;

    invoke-virtual {v1, v7, v0}, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->setPlayStatus(IZ)Lcom/samsung/musicplus/widget/RemoteViewBuilder;

    .line 3799
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mRemoteBigViewBuilder:Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;

    invoke-virtual {v1, v7, v0}, Lcom/samsung/musicplus/service/PlayerService$NotificationRemoteViewBuilder;->setPlayStatus(IZ)Lcom/samsung/musicplus/widget/RemoteViewBuilder;

    .line 3801
    .end local v0    # "isPlaying":Z
    .end local v7    # "listType":I
    :cond_d
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->isPlaying()Z

    move-result v1

    if-eqz v1, :cond_e

    .line 3802
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mNotification:Landroid/app/Notification;

    const v2, 0x7f0200c0

    iput v2, v1, Landroid/app/Notification;->icon:I

    .line 3806
    :goto_3
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->startForeground()V

    .line 3807
    const-string v1, "MusicService"

    const-string v2, "play startForeground end "

    invoke-static {v10, v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(ZLjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 3804
    :cond_e
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mNotification:Landroid/app/Notification;

    const v2, 0x7f0200bf

    iput v2, v1, Landroid/app/Notification;->icon:I

    goto :goto_3
.end method

.method public position()J
    .locals 2

    .prologue
    .line 4067
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/MultiPlayer;->position()J

    move-result-wide v0

    return-wide v0
.end method

.method public prev(Z)V
    .locals 6
    .param p1, "force"    # Z

    .prologue
    const-wide/16 v4, 0x0

    .line 4022
    if-eqz p1, :cond_0

    .line 4023
    invoke-direct {p0, v4, v5}, Lcom/samsung/musicplus/service/PlayerService;->prev(J)V

    .line 4031
    :goto_0
    return-void

    .line 4025
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->position()J

    move-result-wide v0

    const-wide/16 v2, 0xbb8

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 4026
    invoke-virtual {p0, v4, v5}, Lcom/samsung/musicplus/service/PlayerService;->seek(J)J

    goto :goto_0

    .line 4028
    :cond_1
    invoke-direct {p0, v4, v5}, Lcom/samsung/musicplus/service/PlayerService;->prev(J)V

    goto :goto_0
.end method

.method public refreshDlna()V
    .locals 5

    .prologue
    .line 2147
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mDlnaManager:Lcom/samsung/musicplus/library/dlna/DlnaManager;

    if-nez v0, :cond_1

    .line 2149
    sget-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_SUPPORT_DATA_CHECK_POPUP:Z

    if-eqz v0, :cond_0

    .line 2150
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    sget-object v1, Lcom/samsung/musicplus/contents/dlna/DlnaStore$Server;->CONTENT_URI:Landroid/net/Uri;

    sget-object v2, Lcom/samsung/musicplus/contents/dlna/DlnaStore$ServerContents;->CONTENT_URI:Landroid/net/Uri;

    sget-object v3, Lcom/samsung/musicplus/contents/dlna/DlnaStore$Renderer;->CONTENT_URI:Landroid/net/Uri;

    sget-object v4, Lcom/samsung/musicplus/contents/dlna/DlnaStore;->DLNA_ALL_DELETE_URI:Landroid/net/Uri;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/samsung/musicplus/library/dlna/DlnaManager;->getInstance(Landroid/content/Context;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;Landroid/net/Uri;)Lcom/samsung/musicplus/library/dlna/DlnaManager;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mDlnaManager:Lcom/samsung/musicplus/library/dlna/DlnaManager;

    .line 2153
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mDlnaManager:Lcom/samsung/musicplus/library/dlna/DlnaManager;

    invoke-virtual {v0}, Lcom/samsung/musicplus/library/dlna/DlnaManager;->bindDlnaService()V

    .line 2156
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mDlnaHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    const-wide/16 v2, 0x3c

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 2161
    :cond_0
    :goto_0
    return-void

    .line 2159
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mDlnaManager:Lcom/samsung/musicplus/library/dlna/DlnaManager;

    invoke-virtual {v0}, Lcom/samsung/musicplus/library/dlna/DlnaManager;->refresh()V

    goto :goto_0
.end method

.method public removeTrack(J)I
    .locals 7
    .param p1, "id"    # J

    .prologue
    .line 3068
    monitor-enter p0

    .line 3069
    const/4 v0, 0x0

    .line 3071
    .local v0, "gotonext":Z
    :try_start_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getAudioId()J

    move-result-wide v4

    cmp-long v3, p1, v4

    if-nez v3, :cond_0

    .line 3072
    const/4 v0, 0x1

    .line 3074
    :cond_0
    iget-object v3, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v3, p1, p2}, Lcom/samsung/musicplus/service/PlayerListManager;->removeTrack(J)I

    move-result v1

    .line 3075
    .local v1, "numremoved":I
    if-eqz v0, :cond_1

    .line 3076
    iget-object v3, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v3}, Lcom/samsung/musicplus/service/PlayerListManager;->getListItemCount()I

    move-result v2

    .line 3077
    .local v2, "playListLength":I
    if-nez v2, :cond_3

    .line 3078
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->stop()V

    .line 3082
    :goto_0
    const-string v3, "com.android.music.metachanged"

    invoke-direct {p0, v3}, Lcom/samsung/musicplus/service/PlayerService;->notifyChange(Ljava/lang/String;)V

    .line 3084
    .end local v2    # "playListLength":I
    :cond_1
    if-lez v1, :cond_2

    .line 3085
    iget-object v3, p0, Lcom/samsung/musicplus/service/PlayerService;->mNotifyHandler:Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;

    const/16 v4, 0x64

    invoke-virtual {v3, v4}, Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;->notifyQueueChange(I)V

    .line 3087
    :cond_2
    monitor-exit p0

    return v1

    .line 3080
    .restart local v2    # "playListLength":I
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->isPlaying()Z

    move-result v3

    invoke-direct {p0, v3}, Lcom/samsung/musicplus/service/PlayerService;->openInternal(Z)V

    goto :goto_0

    .line 3088
    .end local v1    # "numremoved":I
    .end local v2    # "playListLength":I
    :catchall_0
    move-exception v3

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v3
.end method

.method public removeTracks(II)I
    .locals 5
    .param p1, "first"    # I
    .param p2, "last"    # I

    .prologue
    .line 3002
    monitor-enter p0

    .line 3003
    :try_start_0
    iget-object v4, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v4}, Lcom/samsung/musicplus/service/PlayerListManager;->getListPosition()I

    move-result v3

    .line 3004
    .local v3, "position":I
    const/4 v0, 0x0

    .line 3005
    .local v0, "gotonext":Z
    if-gt p1, v3, :cond_0

    if-gt v3, p2, :cond_0

    .line 3006
    const/4 v0, 0x1

    .line 3008
    :cond_0
    iget-object v4, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v4, p1, p2}, Lcom/samsung/musicplus/service/PlayerListManager;->removeTracksInternal(II)I

    move-result v1

    .line 3009
    .local v1, "numremoved":I
    if-eqz v0, :cond_1

    .line 3010
    iget-object v4, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v4}, Lcom/samsung/musicplus/service/PlayerListManager;->getListItemCount()I

    move-result v2

    .line 3011
    .local v2, "playListLength":I
    if-nez v2, :cond_3

    .line 3012
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->stop()V

    .line 3013
    const-string v4, "com.android.music.metachanged"

    invoke-direct {p0, v4}, Lcom/samsung/musicplus/service/PlayerService;->notifyChange(Ljava/lang/String;)V

    .line 3018
    .end local v2    # "playListLength":I
    :cond_1
    :goto_0
    if-lez v1, :cond_2

    .line 3019
    const-string v4, "com.samsung.musicplus.action.QUEUE_CHANGED"

    invoke-direct {p0, v4}, Lcom/samsung/musicplus/service/PlayerService;->notifyChange(Ljava/lang/String;)V

    .line 3021
    :cond_2
    monitor-exit p0

    return v1

    .line 3015
    .restart local v2    # "playListLength":I
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->isPlaying()Z

    move-result v4

    invoke-direct {p0, v4}, Lcom/samsung/musicplus/service/PlayerService;->openInternal(Z)V

    goto :goto_0

    .line 3022
    .end local v0    # "gotonext":Z
    .end local v1    # "numremoved":I
    .end local v2    # "playListLength":I
    .end local v3    # "position":I
    :catchall_0
    move-exception v4

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4
.end method

.method public removeTracks([I)I
    .locals 7
    .param p1, "positions"    # [I

    .prologue
    .line 3035
    monitor-enter p0

    .line 3036
    :try_start_0
    iget-object v6, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v6}, Lcom/samsung/musicplus/service/PlayerListManager;->getListPosition()I

    move-result v5

    .line 3037
    .local v5, "position":I
    const/4 v0, 0x0

    .line 3038
    .local v0, "gotonext":Z
    array-length v2, p1

    .line 3039
    .local v2, "length":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 3040
    aget v6, p1, v1

    if-ne v6, v5, :cond_0

    .line 3041
    const/4 v0, 0x1

    .line 3039
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 3044
    :cond_1
    iget-object v6, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v6, p1}, Lcom/samsung/musicplus/service/PlayerListManager;->removeTracksInternal([I)I

    move-result v3

    .line 3045
    .local v3, "numremoved":I
    if-eqz v0, :cond_2

    .line 3046
    iget-object v6, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v6}, Lcom/samsung/musicplus/service/PlayerListManager;->getListItemCount()I

    move-result v4

    .line 3047
    .local v4, "playListLength":I
    if-nez v4, :cond_4

    .line 3048
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->stop()V

    .line 3049
    const-string v6, "com.android.music.metachanged"

    invoke-direct {p0, v6}, Lcom/samsung/musicplus/service/PlayerService;->notifyChange(Ljava/lang/String;)V

    .line 3054
    .end local v4    # "playListLength":I
    :cond_2
    :goto_1
    if-lez v3, :cond_3

    .line 3055
    const-string v6, "com.samsung.musicplus.action.QUEUE_CHANGED"

    invoke-direct {p0, v6}, Lcom/samsung/musicplus/service/PlayerService;->notifyChange(Ljava/lang/String;)V

    .line 3057
    :cond_3
    monitor-exit p0

    return v3

    .line 3051
    .restart local v4    # "playListLength":I
    :cond_4
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->isPlaying()Z

    move-result v6

    invoke-direct {p0, v6}, Lcom/samsung/musicplus/service/PlayerService;->openInternal(Z)V

    goto :goto_1

    .line 3058
    .end local v0    # "gotonext":Z
    .end local v1    # "i":I
    .end local v2    # "length":I
    .end local v3    # "numremoved":I
    .end local v4    # "playListLength":I
    .end local v5    # "position":I
    :catchall_0
    move-exception v6

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v6
.end method

.method public removeTracks([J)I
    .locals 10
    .param p1, "ids"    # [J

    .prologue
    .line 3098
    monitor-enter p0

    .line 3099
    const/4 v0, 0x0

    .line 3101
    .local v0, "gotonext":Z
    :try_start_0
    array-length v2, p1

    .line 3102
    .local v2, "length":I
    const/4 v3, 0x0

    .line 3103
    .local v3, "numremoved":I
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getAudioId()J

    move-result-wide v4

    .line 3105
    .local v4, "nowPlayingId":J
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v2, :cond_1

    .line 3106
    aget-wide v8, p1, v1

    cmp-long v7, v8, v4

    if-nez v7, :cond_0

    .line 3107
    const/4 v0, 0x1

    .line 3109
    :cond_0
    iget-object v7, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    aget-wide v8, p1, v1

    invoke-virtual {v7, v8, v9}, Lcom/samsung/musicplus/service/PlayerListManager;->removeTrack(J)I

    move-result v3

    .line 3105
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 3112
    :cond_1
    if-eqz v0, :cond_2

    .line 3113
    iget-object v7, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v7}, Lcom/samsung/musicplus/service/PlayerListManager;->getListItemCount()I

    move-result v6

    .line 3114
    .local v6, "playListLength":I
    if-nez v6, :cond_4

    .line 3115
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->stop()V

    .line 3119
    :goto_1
    const-string v7, "com.android.music.metachanged"

    invoke-direct {p0, v7}, Lcom/samsung/musicplus/service/PlayerService;->notifyChange(Ljava/lang/String;)V

    .line 3121
    .end local v6    # "playListLength":I
    :cond_2
    if-lez v3, :cond_3

    .line 3122
    iget-object v7, p0, Lcom/samsung/musicplus/service/PlayerService;->mNotifyHandler:Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;

    const/16 v8, 0x64

    invoke-virtual {v7, v8}, Lcom/samsung/musicplus/service/PlayerService$NotifyHandler;->notifyQueueChange(I)V

    .line 3124
    :cond_3
    monitor-exit p0

    return v3

    .line 3117
    .restart local v6    # "playListLength":I
    :cond_4
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->isPlaying()Z

    move-result v7

    invoke-direct {p0, v7}, Lcom/samsung/musicplus/service/PlayerService;->openInternal(Z)V

    goto :goto_1

    .line 3125
    .end local v1    # "i":I
    .end local v2    # "length":I
    .end local v3    # "numremoved":I
    .end local v4    # "nowPlayingId":J
    .end local v6    # "playListLength":I
    :catchall_0
    move-exception v7

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v7
.end method

.method public reorderQueue(ILjava/lang/String;[JI)V
    .locals 1
    .param p1, "listType"    # I
    .param p2, "keyWord"    # Ljava/lang/String;
    .param p3, "list"    # [J
    .param p4, "position"    # I

    .prologue
    .line 2968
    monitor-enter p0

    .line 2969
    :try_start_0
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/samsung/musicplus/service/PlayerListManager;->setList(ILjava/lang/String;[JI)V

    .line 2970
    const-string v0, "com.samsung.musicplus.action.QUEUE_CHANGED"

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/service/PlayerService;->notifyChange(Ljava/lang/String;)V

    .line 2971
    monitor-exit p0

    .line 2972
    return-void

    .line 2971
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public seek(J)J
    .locals 7
    .param p1, "pos"    # J

    .prologue
    .line 4086
    iget-boolean v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mIsSeekReady:Z

    if-nez v0, :cond_0

    .line 4088
    const-wide/16 v0, 0x0

    .line 4091
    :goto_0
    return-wide v0

    .line 4090
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->isPreparing()Z

    move-result v2

    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->isPlaying()Z

    move-result v3

    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getPlaySpeed()F

    move-result v6

    move-object v1, p0

    move-wide v4, p1

    invoke-direct/range {v1 .. v6}, Lcom/samsung/musicplus/service/PlayerService;->setPlaybackState(ZZJF)V

    .line 4091
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/musicplus/service/MultiPlayer;->seek(J)J

    move-result-wide v0

    goto :goto_0
.end method

.method public selectDlnaDms(Ljava/lang/String;)V
    .locals 3
    .param p1, "dmsId"    # Ljava/lang/String;

    .prologue
    .line 2166
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mDlnaHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mDlnaHandler:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 2167
    return-void
.end method

.method public setAdaptSound(ZZ)V
    .locals 4
    .param p1, "isOn"    # Z
    .param p2, "showToast"    # Z

    .prologue
    .line 973
    const-string v1, "MusicService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setAdaptSound "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " show toast ? "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 974
    if-eqz p1, :cond_0

    .line 975
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->ensureAdaptSound()V

    .line 977
    :cond_0
    iput-boolean p1, p0, Lcom/samsung/musicplus/service/PlayerService;->mIsAdaptSoundOn:Z

    .line 979
    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mAdaptSound:Lcom/samsung/musicplus/library/audio/AdaptSound;

    if-nez v1, :cond_2

    .line 981
    if-nez p1, :cond_1

    .line 982
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/samsung/musicplus/library/audio/AdaptSound;->setAdaptSoundOn(Landroid/content/Context;Z)V

    .line 995
    :cond_1
    :goto_0
    return-void

    .line 987
    :cond_2
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->isEnableAdaptSoundPath()Z

    move-result v0

    .line 988
    .local v0, "connected":Z
    invoke-direct {p0, v0}, Lcom/samsung/musicplus/service/PlayerService;->updateAdaptSound(Z)V

    .line 989
    if-eqz p2, :cond_3

    if-eqz p1, :cond_3

    if-nez v0, :cond_3

    .line 990
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->warningAdaptSound()V

    .line 994
    :cond_3
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, p1}, Lcom/samsung/musicplus/library/audio/AdaptSound;->setAdaptSoundOn(Landroid/content/Context;Z)V

    goto :goto_0
.end method

.method public setDlnaDmrMute()V
    .locals 1

    .prologue
    .line 4356
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->isDmrPlaying()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4357
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/MultiPlayer;->setMute()V

    .line 4359
    :cond_0
    return-void
.end method

.method public setK2HD(Z)V
    .locals 3
    .param p1, "isOn"    # Z

    .prologue
    .line 1174
    sget-boolean v0, Lcom/samsung/musicplus/library/MusicFeatures;->FLAG_K2HD:Z

    if-nez v0, :cond_0

    .line 1193
    :goto_0
    return-void

    .line 1177
    :cond_0
    const-string v0, "MusicService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setK2HD "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1182
    if-eqz p1, :cond_1

    .line 1183
    const-string v0, "MusicService"

    const-string v1, "setK2HD : KEY_PARAMETER_VIRTUAL_ULTRA_HIGH_QUALITY on"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1184
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/service/MultiPlayer;->setK2HD(Z)V

    .line 1191
    :goto_1
    const-string v0, "K2HD"

    invoke-direct {p0, v0, p1}, Lcom/samsung/musicplus/service/PlayerService;->savePreferences(Ljava/lang/String;Z)V

    goto :goto_0

    .line 1187
    :cond_1
    const-string v0, "MusicService"

    const-string v1, "setK2HD : KEY_PARAMETER_VIRTUAL_ULTRA_HIGH_QUALITY off"

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1188
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/service/MultiPlayer;->setK2HD(Z)V

    goto :goto_1
.end method

.method public setLimitVolume(FF)V
    .locals 1
    .param p1, "l"    # F
    .param p2, "r"    # F

    .prologue
    .line 4374
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

    if-eqz v0, :cond_0

    .line 4375
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/musicplus/service/MultiPlayer;->setLimitVolume(FF)V

    .line 4377
    :cond_0
    return-void
.end method

.method public setNewSoundAliveAuto(Z)V
    .locals 3
    .param p1, "auto"    # Z

    .prologue
    .line 5195
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

    if-nez v0, :cond_0

    .line 5196
    const-string v0, "MusicService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setNewSoundAliveAuto() mPlayer is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 5200
    :goto_0
    return-void

    .line 5199
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/service/MultiPlayer;->setAutoSquare(Z)V

    goto :goto_0
.end method

.method public setPlaySpeed(FZ)V
    .locals 7
    .param p1, "speed"    # F
    .param p2, "notify"    # Z

    .prologue
    .line 3443
    const-string v0, "MusicService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setPlaySpeed "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3444
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->isSupportPlaySpeed()Z

    move-result v0

    if-nez v0, :cond_0

    .line 3445
    const/high16 p1, 0x3f800000    # 1.0f

    .line 3446
    const-string v0, "play_speed"

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {p0, v0, v1}, Lcom/samsung/musicplus/service/PlayerService;->savePreferences(Ljava/lang/String;F)V

    .line 3447
    const-string v0, "MusicService"

    const-string v1, " current item did not support play speed so set as 1.0"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 3449
    :cond_0
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/service/MultiPlayer;->setPlaySpeed(F)V

    .line 3450
    if-eqz p2, :cond_1

    .line 3451
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->isPreparing()Z

    move-result v2

    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->isPlaying()Z

    move-result v3

    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->position()J

    move-result-wide v4

    move-object v1, p0

    move v6, p1

    invoke-direct/range {v1 .. v6}, Lcom/samsung/musicplus/service/PlayerService;->setPlaybackState(ZZJF)V

    .line 3453
    :cond_1
    return-void
.end method

.method public setQueuePosition(IZ)V
    .locals 3
    .param p1, "position"    # I
    .param p2, "play"    # Z

    .prologue
    .line 2931
    const-string v0, "MusicService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " setQueuePosition position : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " play : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 2932
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->resetListPreset()V

    .line 2933
    monitor-enter p0

    .line 2934
    :try_start_0
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/service/PlayerListManager;->movePosition(I)V

    .line 2935
    invoke-direct {p0, p2}, Lcom/samsung/musicplus/service/PlayerService;->open(Z)V

    .line 2936
    monitor-exit p0

    .line 2937
    return-void

    .line 2936
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public setRepeat(I)V
    .locals 1
    .param p1, "repeat"    # I

    .prologue
    .line 4216
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/service/PlayerListManager;->setRepeatMode(I)V

    .line 4217
    const-string v0, "repeat"

    invoke-direct {p0, v0, p1}, Lcom/samsung/musicplus/service/PlayerService;->savePreferences(Ljava/lang/String;I)V

    .line 4218
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->notifySettingChange()V

    .line 4219
    return-void
.end method

.method public setShuffle(I)V
    .locals 1
    .param p1, "shuffle"    # I

    .prologue
    .line 4210
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/service/PlayerListManager;->setShuffle(I)V

    .line 4211
    const-string v0, "shuffle"

    invoke-direct {p0, v0, p1}, Lcom/samsung/musicplus/service/PlayerService;->savePreferences(Ljava/lang/String;I)V

    .line 4212
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->notifySettingChange()V

    .line 4213
    return-void
.end method

.method public setSlinkWakeLock()V
    .locals 1

    .prologue
    .line 4384
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mListManager:Lcom/samsung/musicplus/service/PlayerListManager;

    invoke-virtual {v0}, Lcom/samsung/musicplus/service/PlayerListManager;->setSlinkWakeLock()V

    .line 4385
    return-void
.end method

.method public setSmartVolume(Z)V
    .locals 3
    .param p1, "isOn"    # Z

    .prologue
    .line 1106
    const-string v0, "MusicService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setSmartVolume "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1107
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->ensureSmartVolume()V

    .line 1108
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mSmartVolume:Lcom/samsung/musicplus/library/audio/SmartVolumeManager;

    if-nez v0, :cond_1

    .line 1125
    :cond_0
    :goto_0
    return-void

    .line 1112
    :cond_1
    iget-boolean v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mIsActiveSmartVolume:Z

    if-eq v0, p1, :cond_0

    .line 1113
    iput-boolean p1, p0, Lcom/samsung/musicplus/service/PlayerService;->mIsActiveSmartVolume:Z

    .line 1114
    invoke-direct {p0, p1}, Lcom/samsung/musicplus/service/PlayerService;->setSmartVolumeEarSafety(Z)V

    .line 1116
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mSmartVolume:Lcom/samsung/musicplus/library/audio/SmartVolumeManager;

    invoke-virtual {v0}, Lcom/samsung/musicplus/library/audio/SmartVolumeManager;->resetValues()V

    .line 1117
    if-eqz p1, :cond_2

    .line 1118
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->getStreamVolume(I)I

    move-result v0

    iput v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mBaeVolume:I

    .line 1119
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->isPlaying()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1120
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getCurrentPath()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/service/PlayerService;->adjustSmartVolume(Ljava/lang/String;)V

    .line 1123
    :cond_2
    const-string v0, "smart_volume"

    invoke-direct {p0, v0, p1}, Lcom/samsung/musicplus/service/PlayerService;->savePreferences(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public setSoundAlive(IZ)V
    .locals 4
    .param p1, "effect"    # I
    .param p2, "enableToast"    # Z

    .prologue
    const/4 v3, 0x0

    .line 3347
    const-string v0, "MusicService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setSoundAlive "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " enableToast ? "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3348
    const/16 v0, 0xd

    if-ne p1, v0, :cond_0

    .line 3350
    invoke-virtual {p0, v3, v3, p2}, Lcom/samsung/musicplus/service/PlayerService;->setSoundAliveUser([I[IZ)V

    .line 3351
    const-string v0, "sound_alive"

    invoke-direct {p0, v0, p1}, Lcom/samsung/musicplus/service/PlayerService;->savePreferences(Ljava/lang/String;I)V

    .line 3365
    :goto_0
    return-void

    .line 3353
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/samsung/musicplus/service/PlayerService;->canChangeSoundAlive(IZ)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3355
    const-string v0, "sound_alive"

    invoke-direct {p0, v0, p1}, Lcom/samsung/musicplus/service/PlayerService;->savePreferences(Ljava/lang/String;I)V

    .line 3356
    const/4 v0, -0x1

    if-ne p1, v0, :cond_1

    .line 3357
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getGenre()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/musicplus/library/audio/SoundAlive;->getAudioEffect(Ljava/lang/String;)I

    move-result p1

    .line 3363
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

    invoke-virtual {v0, p1}, Lcom/samsung/musicplus/service/MultiPlayer;->setSoundAlive(I)V

    goto :goto_0

    .line 3360
    :cond_2
    const/4 p1, 0x0

    .line 3361
    const-string v0, "sound_alive"

    invoke-direct {p0, v0, p1}, Lcom/samsung/musicplus/service/PlayerService;->savePreferences(Ljava/lang/String;I)V

    goto :goto_1
.end method

.method public setSoundAliveUser([I[IZ)V
    .locals 4
    .param p1, "userEq"    # [I
    .param p2, "extEffect"    # [I
    .param p3, "enableToast"    # Z

    .prologue
    const/4 v3, 0x0

    .line 3406
    const-string v1, "MusicService"

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setSoundAliveUser  enableToast ? "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " userEq null ?"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    const-string v0, "null"

    :goto_0
    invoke-static {v1, v0}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3410
    const/16 v0, 0xd

    invoke-direct {p0, v0, p3}, Lcom/samsung/musicplus/service/PlayerService;->canChangeSoundAlive(IZ)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 3412
    if-nez p1, :cond_0

    .line 3413
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->loadSoundAliveUserEQ()[I

    move-result-object p1

    .line 3415
    :cond_0
    if-nez p2, :cond_1

    .line 3416
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->loadSoundAliveUserExt()[I

    move-result-object p2

    .line 3418
    :cond_1
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/musicplus/service/MultiPlayer;->setUserSoundAlive([I[I)V

    .line 3419
    invoke-direct {p0, p1, p2}, Lcom/samsung/musicplus/service/PlayerService;->saveSoundAliveUserEqExt([I[I)V

    .line 3424
    :goto_1
    return-void

    .line 3406
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "not null extEffect null ? "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_3

    const-string v0, "null"

    goto :goto_0

    :cond_3
    const-string v0, "not null"

    goto :goto_0

    .line 3421
    :cond_4
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mPlayer:Lcom/samsung/musicplus/service/MultiPlayer;

    invoke-virtual {v0, v3}, Lcom/samsung/musicplus/service/MultiPlayer;->setSoundAlive(I)V

    .line 3422
    const-string v0, "sound_alive"

    invoke-direct {p0, v0, v3}, Lcom/samsung/musicplus/service/PlayerService;->savePreferences(Ljava/lang/String;I)V

    goto :goto_1
.end method

.method public stopMusicPackage()V
    .locals 2

    .prologue
    .line 4391
    const-string v0, "MusicService"

    const-string v1, "stopMusicPackage start "

    invoke-static {v0, v1}, Lcom/samsung/musicplus/library/iLog;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 4392
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/samsung/musicplus/service/PlayerService;->saveQueue(Z)V

    .line 4393
    invoke-direct {p0}, Lcom/samsung/musicplus/service/PlayerService;->stop()V

    .line 4394
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mControlClient:Landroid/media/RemoteControlClient;

    if-eqz v0, :cond_0

    .line 4395
    iget-object v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mAudioManager:Lcom/samsung/musicplus/library/audio/SecAudioManager;

    iget-object v1, p0, Lcom/samsung/musicplus/service/PlayerService;->mControlClient:Landroid/media/RemoteControlClient;

    invoke-virtual {v0, v1}, Lcom/samsung/musicplus/library/audio/SecAudioManager;->unregisterRemoteControlClient(Landroid/media/RemoteControlClient;)V

    .line 4397
    :cond_0
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->stopSelf()V

    .line 4398
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/musicplus/service/PlayerService;->mIsStopPacakgeAfterDestroyed:Z

    .line 4399
    return-void
.end method

.method public toggleRepeat()V
    .locals 2

    .prologue
    .line 2748
    const/4 v0, 0x0

    .line 2749
    .local v0, "change":I
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getRepeat()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 2762
    :goto_0
    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/service/PlayerService;->setRepeat(I)V

    .line 2763
    return-void

    .line 2751
    :pswitch_0
    const/4 v0, 0x2

    .line 2752
    goto :goto_0

    .line 2754
    :pswitch_1
    const/4 v0, 0x1

    .line 2755
    goto :goto_0

    .line 2757
    :pswitch_2
    const/4 v0, 0x0

    .line 2758
    goto :goto_0

    .line 2749
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public toggleShuffle()V
    .locals 2

    .prologue
    .line 2733
    const/4 v0, 0x0

    .line 2734
    .local v0, "change":I
    invoke-virtual {p0}, Lcom/samsung/musicplus/service/PlayerService;->getShuffle()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 2744
    :goto_0
    invoke-virtual {p0, v0}, Lcom/samsung/musicplus/service/PlayerService;->setShuffle(I)V

    .line 2745
    return-void

    .line 2736
    :pswitch_0
    const/4 v0, 0x1

    .line 2737
    goto :goto_0

    .line 2739
    :pswitch_1
    const/4 v0, 0x0

    .line 2740
    goto :goto_0

    .line 2734
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
